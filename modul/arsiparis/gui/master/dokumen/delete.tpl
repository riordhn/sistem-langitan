
<div class="center_title_bar">ID Dokumen : {$arsip_dokumen.ID_ARSIP_DOKUMEN}</div>

{if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

<form action="master-dokumen.php" method="post">
<input type="hidden" name="mode" value="delete" />
<input type="hidden" name="id_arsip_dokumen" value="{$arsip_dokumen.ID_ARSIP_DOKUMEN}" />
<!--<input type="hidden" name="id_join_table" value="{$pengguna.JOIN_TABLE}" /> -->
<table>
    <tr>
        <th colspan="2">Detail Dokumen</th>
    </tr>
    <tr>
        <td>ID</td>
        <td>{$arsip_dokumen.ID_ARSIP_DOKUMEN}</td>
    </tr>
    <tr>
        <td>Scan Photo</td>
        <td><img src="{$PHOTO}" border="0" width="100" />
        </td>
    </tr>
    <tr>
        <td>Kategori Arsip</td>
        <td>
            <select id="arsip_kategori" name="arsip_kategori" disabled >
                <option value="">-- Pilih Kategori --</option>
                {foreach $arsip_kategori_set as $data}
                    <option value="{$data.ID_ARSIP_KATEGORI}" {if $arsip_dokumen.ID_ARSIP_KATEGORI==$data.ID_ARSIP_KATEGORI}selected="true"{/if}>{$data.NM_ARSIP_KATEGORI}</option>
                {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Sub-Kategori Arsip</td>
        <td>
            <select id="arsip_subkategori" name="arsip_subkategori" disabled >
                <option value="">-- Pilih Sub-Kategori --</option>
                {foreach $arsip_subkategori_set as $data}
                    <option value="{$data.ID_ARSIP_SUBKATEGORI}" {if $arsip_dokumen.ID_ARSIP_SUBKATEGORI==$data.ID_ARSIP_SUBKATEGORI}selected="true"{/if}>{$data.NM_ARSIP_SUBKATEGORI}</option>
                {/foreach} 
            </select>
        </td>
    </tr>
    <tr>
        <td>Kode Katalog</td>
        <td><input class="form-control" id="kode_katalog" type="text" name="kode_katalog" value="{$arsip_dokumen.KODE_KATALOG}" size="75" disabled />
        </td>
    </tr>
    <tr>
        <td>Nomor Dokumen (Nomor SK/Surat, dll)</td>
        <td><input class="form-control" id="nomor_dokumen" type="text" name="nomor_dokumen" value="{$arsip_dokumen.NOMOR_ARSIP_DOKUMEN}" size="75" />
        </td>
    </tr>
    <tr>
        <td>Nama Dokumen</td>
        <td><input class="form-control" id="nm_arsip_dokumen" type="text" name="nm_arsip_dokumen" value="{$arsip_dokumen.NM_ARSIP_DOKUMEN}" size="75" disabled />
        </td>
    </tr>
    <tr>
        <td>Pemilik</td>
        <td>
            <select name="id_arsip_pemilik" disabled >
                <option value="0">-- Tidak Ada --</option>
            {foreach $arsip_pemilik_set as $pemilik}
                <option value="{$pemilik.ID_ARSIP_PEMILIK}" {if $pemilik.ID_ARSIP_PEMILIK == $arsip_dokumen.ID_ARSIP_PEMILIK}selected="selected"{/if}>{$pemilik.NM_ARSIP_PEMILIK}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Tgl Penyusunan</td>
        <td><input class="form-control" id="tgl_penyusunan" type="text" name="tgl_penyusunan" value="{$arsip_dokumen.TGL_PENYUSUNAN}" size="75" disabled />
        </td>
    </tr>
    <tr>
        <td>Unit Kerja</td>
        <td>
            <select name="id_unit_kerja" disabled >
                <option value="0">-- Tidak Ada --</option>
            {foreach $unit_kerja_set as $uk}
                <option value="{$uk.ID_UNIT_KERJA}" {if $uk.ID_UNIT_KERJA == $arsip_dokumen.ID_UNIT_KERJA}selected="selected"{/if}>{$uk.NM_JENJANG} {$uk.NM_UNIT_KERJA}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Almari / Loker</td>
        <td>
            <select name="id_arsip_loker" disabled >
                <option value="0">-- Tidak Ada --</option>
            {foreach $arsip_loker_set as $loker}
                <option value="{$loker.ID_ARSIP_LOKER}" {if $loker.ID_ARSIP_LOKER == $arsip_dokumen.ID_ARSIP_LOKER}selected="selected"{/if}>{$loker.NM_ARSIP_LOKER}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Jumlah Halaman</td>
        <td><input class="form-control" id="jml_halaman" type="text" name="jml_halaman" value="{$arsip_dokumen.JML_HALAMAN}" size="75" disabled />
        </td>
    </tr>
    <tr>
        <td>Contact Person</td>
        <td><input class="form-control" id="contact_person" type="text" name="contact_person" value="{$arsip_dokumen.CONTACT_PERSON}" size="75" disabled />
        </td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <input type="submit" value="Hapus" />
        </td>
    </tr>
</table>
</form>

{literal}
    <script type="text/javascript">
        $('#arsip_kategori').change(function(){
            $.ajax({
                type:'post',
                url:'getSubkategori.php',
                data:'id_kategori='+$('#arsip_kategori').val(),
                success:function(data){
                    $('#arsip_subkategori').html(data);
                }                    
            })
        });
        
    </script>
{/literal}

<a href="master-dokumen.php">Kembali</a>