<div class="center_title_bar">Master Kategori Arsip</div>

{if isset($arsip_kategori_set)}
	<table class="tableabout">
		<thead>
			<tr>
				<th>No</th>
				<th>Nama Kategori Arsip</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			{foreach $arsip_kategori_set as $uk}
				<tr {if $uk@index is not div by 2}class="odd"{/if}>
					<td class="center">{$uk@index + 1}</td>
					<td><strong>{$uk.NM_ARSIP_KATEGORI}</strong></td>
					<td class="center">
						<a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;"  href="master-kategori.php?mode=edit&id_arsip_kategori={$uk.ID_ARSIP_KATEGORI}"><b style="color: green">Edit</b></a>
						{if $uk.RELASI == 0}
							<a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="master-kategori.php?mode=delete&id_arsip_kategori={$uk.ID_ARSIP_KATEGORI}"><b style="color: red">Delete</b></a>
						{/if}
					</td>
				</tr>
			{/foreach}
			<tr>
				<td colspan="9" class="center">
					<a href="master-kategori.php?mode=add"><b style="color: red">Tambah Master Kategori</b></a>
				</td>
			</tr>
		</tbody>
	</table>
{/if}