<?php
//Yudi Sulistya, 09/11/2012

if(isset($_POST['submit'])){

	include ("../../../config.php");
	require ("../includes/class.imageupload.php");

	$nama_singkat_pt = $nama_singkat;

	if (post('mode') == 'update')
    {
    	$id_arsip_dokumen = post('photo');

    	// update is_upload
    	$db->Query("update arsip_dokumen set is_upload = '1'
        						where id_arsip_dokumen = '{$id_arsip_dokumen}'");
    }
	
	//$image = new ImageUloader($max_size, $max_width, $max_height, $upload_dir)
	$image = new ImageUploader(1000, 1000, 1000, '../../../files/arsiparis/');
	
	$image->setImage('file_photo');
	
	$errors = array();
	if(!$image->checkSize()){ //check image size
		$errors[] = "- Besar file terlalu besar. (Maks. 1000KB)";
	}
	
	if(!$image->checkHeight()){ //check image height (tinggi)
		$errors[] = "- Ukuran tinggi file terlalu besar. (Maks. 1000 px)";
	}
	
	if(!$image->checkWidth()){ //check image width (lebar)
		$errors[] = "- Ukuran lebar file terlalu besar. (Maks. 1000 px)";
	}
	
	if(!$image->checkExt()){ //check image extension
		$errors[] = "- Format file salah. (format harus jpg atau JPG)";
	}
	
	if(empty($errors)){
		$image->setImageName($_POST['photo']); //set image name
		$image->deleteExisting();
		$image->upload();
		
		echo '<script>alert("Photo berhasil di upload")</script>';
		
		echo '
		<html>
		<head>
		<title>Upload Photo</title>
		<link rel="stylesheet" type="text/css" href="../includes/iframe.css" />
		<script>
		function CloseWindow() {
		  timer=setTimeout("window.close()",100);
		  return true;
		}
		</script>
		</head>
		<body onload="CloseWindow();" onunload="window.opener.location.reload();">
		</body>
		</html>';
	} else {
		foreach ($errors as $msg){
			$display .= '\r\n'.$msg;
		}
		
		echo '<script>alert("Photo gagal di upload:'.$display.'")</script>';
		echo '<script>location.href="javascript:history.go(-1)";</script>';
	}
} else {

require ("../../../config.php");
if ($user->Role() == AUCC_ROLE_SEKRETARIAT_ARSIPARIS) {
include ("../includes/encrypt.php");

$nama_singkat_pt = $nama_singkat;

$var = decode($_SERVER["REQUEST_URI"]);
$to = $var["yth"];
$id = $var["file"];

$filename="../../../files/arsiparis/".$id.".JPG";
if (file_exists($filename)) {
	$photo = $filename.'?t=timestamp';
} else {
	$photo = '../includes/images/cancel.png';
}

echo '
<html>
<head>
<title>Upload Photo</title>
<link rel="stylesheet" type="text/css" href="../includes/iframe.css" />
</head>
<body oncontextmenu="return false;">
<center>
<p>
<table>
	<tr>
		<td align="center">Upload photo untuk : '.$to.'</td>
	</tr>
	<tr>
		<td align="center"><img src="'.$photo.'" border="0" width="160" /></td>
	</tr>
	<tr>
		<td align="center"><font color="grey">Besar file maks. 1 MB.<br/>Lebar photo maks. 1000px dan tinggi photo maks. 1000px</font></td>
	</tr>
</table>
</p>
<form enctype="multipart/form-data" method="post" name="upload">
	<input name="photo" type="hidden" value="'.$id.'" />
	<input name="mode" type="hidden" value="update" />
	<input name="file_photo" type="file" /><br/><br/>
	<input type="submit" name="submit" value="Upload" />
</form>
</center>
</body>
</html>';

} else {
    header("location: /logout.php");
    exit();
}
}
?>