<?php
error_reporting (E_ALL & ~E_NOTICE);
include 'config.php';
include 'class/bank.class.php';
$bank = new bank($db);
ob_start();
?>

<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #1d5700;
    }
    td{
        text-align: left;
    }
</style>
<table style="font-size:13px;border:solid">
<thead style="border:solid">
	<tr>
		<th style="text-align:center">NIM</th>
		<th style="text-align:center">NAMA</th>
        <th style="text-align:center">FAKULTAS</th>
        <th style="text-align:center">JENJANG</th>
		<th style="text-align:center">PROGRAM STUDI</th>
        <th style="text-align:center">ALAMAT MAHASISWA</th>  
        <th style="text-align:center">TANGGAL CETAK KTM</th>   
        <th style="text-align:center">FOTO</th>    
	</tr>
</thead>
<tbody>
<?PHP	
$data = $bank->biodata($_REQUEST['nim'], $_SESSION['ID_BANK']);
	echo '<tr><td>'.$data['NIM_MHS'].'</td>
			<td>'.$data['NM_PENGGUNA'].'</td>
			<td>'.strtoupper($data['NM_FAKULTAS']).'</td>
			<td>'.$data['NM_JENJANG'].'</td>
			<td>'.$data['NM_PROGRAM_STUDI'].'</td>
			<td>'.$data['ALAMAT_MHS'].'</td>
			<td>'.$data['TGL_CETAK_KTM'].'</td>
			<td><img src="/foto_mhs/'.$data['NIM_MHS'].'.JPG" width="180" height="230"/></td>
			</tr>';

	
echo '</tbody>
</table>';


$filename = $_REQUEST['nim'];
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=$filename");
header("Pragma: ");
header("Cache-Control: ");
ob_flush();
?>
