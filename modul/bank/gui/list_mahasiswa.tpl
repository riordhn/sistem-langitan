<form action="list_mahasiswa.php" method="post">
	<table>
		<tr>
			<th colspan="5">LIST MAHASISWA</th>
		</tr>
		<tr>
			<td>
				Nama Penerimaan
			</td>
			<td>				
            	<select name="id_penerimaan">
                    <option value="">Pilih Penerimaan</option>
                    {foreach $penerimaan_set as $p}
                        <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                            {foreach $p.p_set as $p2}
                                <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.post.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="text-align:center"><input type="submit" value="Tampil" /></td>
		</tr>
	</table>
</form>

{if isset($smarty.request.id_penerimaan)}

<table style="font-size:13px;border:solid">
        <thead style="border:solid">
            <tr>
            	<th style="text-align:center">NO</th>
                <th style="text-align:center">NIM</th>
                <th style="text-align:center">NO UJIAN</th>
                <th style="text-align:center">NAMA</th>
                <th style="text-align:center">FAKULTAS</th>
                <th style="text-align:center">JENJANG</th>
                <th style="text-align:center">PROGRAM STUDI</th>
                <th style="text-align:center">ALAMAT MAHASISWA</th>  
                <th style="text-align:center">TANGGAL CETAK KTM</th>   
                <th style="text-align:center">FOTO</th>    
            </tr>
        </thead>
        {$no = 1}
	{foreach $list_mahasiswa as $data}
    	<tr>
        	<td>{$no++}</td>
        	<td>{$data.NIM_MHS}</td>
            <td>{$data.NO_UJIAN}</td>
			<td>{$data.NM_PENGGUNA}</td>
			<td>{$data.NM_FAKULTAS|upper}</td>
			<td>{$data.NM_JENJANG}</td>
			<td>{$data.NM_PROGRAM_STUDI}</td>
			<td>{$data.ALAMAT_MHS}</td>
			<td>{$data.TGL_CETAK_KTM}</td>
			<td><img src="/foto_mhs/{$data.NIM_MHS}.JPG" width="130" height="180"/></td>
		</tr>
    {/foreach}
</table>

{/if}