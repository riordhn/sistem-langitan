<html>
<head>
    <title>Cybercampus - Universitas Airlangga</title>
    <meta name="google-site-verification" content="ihkYFpHX_zbP4SfrkqIEF7c2Lp08dZFIxJ2MKI6m_Hk" />
    <link rel="shortcut icon" href="/img/icon.ico"/>
    <link rel="stylesheet" type="text/css" href="/css/reset.css" />
    <link rel="stylesheet" type="text/css" href="/css/style.css" />
    <meta name="description" content="Universitas Airlangga">
    <script>
		
    </script>
</head>

<body>
    <div class="wrap-box">
    

	    <div class="wrap-box">
            <div class="login-box">
			<br />
                <form action="index.php" method="post">
                    <input type="hidden" name="mode" value="login">
                    <table>
                        <tr>
                            <td class="right-align"><strong>Username</strong></td>
                            <td>
                                <input name="username" type="text" maxlength="12" style="width: 110px">
                            </td>
                        </tr>
                        <tr>
                            <td class="right-align"><strong>Password</strong></td>
                            <td>
                                <input name="password" type="password" maxlength="16" style="width: 110px" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" value="Login" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
