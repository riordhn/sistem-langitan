<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="/css/text.css" />
<link rel="stylesheet" type="text/css" href="/css/maba.css" />
<link rel="stylesheet" type="text/css" href="/css/jquery-ui-1.8.11.custom.css" />
<link rel="SHORTCUT ICON" href="/img/maba/iconunair.ico" />
<title>Cybercampus - Universitas Airlangga</title>

<!-- javascript mahasiswa -->

<script type="text/javascript" src="/js/jquery-1.5.1.js"></script>
		<script type="text/javascript" src="/js/jquery-ui-1.8.11.custom.min.js"></script>
		<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
		<script language="javascript" src="/js/jquery-input-format.js"></script>
		<script type="text/javascript">var defaultRel = 'mahasiswa'; var defaultPage = 'mahasiswa.php';</script>
		<script type="text/javascript" src="/js/cybercampus.ajax-1.0.js"></script>
		<script type="text/javascript" src="/js/jquery-ui-timepicker-addon.js"></script>
<script language="javascript" src="/js/maba.js"></script>

</head>

<body>
    <!--  -->
<div id="main_container">
  <div id="header"></div>
  <div id="main_content">
    <div id="menu_tab">
    <div class="left_menu_corner"></div>
        <ul class="menu">
        	<li><a href="mahasiswa.php" rel="mahasiswa" class="nav1" >Biodata</a></li>
            <li class="divider"></li>
            <li><a href="list_mahasiswa.php" rel="mahasiswa" class="nav1" >List Mahasiswa</a></li>
            <li class="divider"></li>
           <li><a class="disable-ajax" href="?mode=logout" style="color: #fff">Logout</a></li>
        </ul>
    <div class="right_menu_corner"></div>

  </div>
      <div class="left_content" >
        <div class="border_box"><img src="/img/maba/photo.jpg" /><br />          
				<!--<?php //include "leftMenu.php";?>-->
				<div id="menu"></div>
                <br />
        </div>
      </div>
      <div class="center_content" id="content" style="min-height: 400px">
        Loading data...
   	  </div>             
  
   <div class="footer">
   

     <div class="left_footer">
        <img src="/img/maba/footer_logo.jpg" />
     </div>
        
     <div class="center_footer">
       Copyright &copy; 2011 - Universitas Airlangga. <br/>All Rights Reserved
     </div>
        
     <div class="right_footer">
     </div>   
   
   </div>  
 </div>

</body>
</html>
