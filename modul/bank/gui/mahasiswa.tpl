<form method="post" action="mahasiswa.php">
<table>
<tr>
	<td>No Ujian/ NIM</td>
    <td><input type="text" name="nim" /></td>
    <td><input type="submit" value="Tampil" /></td>
</tr>
</table>
</form>

{if isset($smarty.request.nim) and $error == ''}
<form name="f2" id="f2" action="mahasiswa.php" method="post">
<table>
	<tr>
            <th colspan="2" style="text-align:center">BIODATA MAHASISWA</th>
        </tr>
        <tr>
            <td colspan="2" style="text-align:center">
                <img src="/foto_mhs/{$biodata_mahasiswa.NIM_MHS}.JPG" width="180" height="230"/>
            </td>
        </tr>
        <tr>
            <td>NAMA</td>
            <td>{$biodata_mahasiswa.NM_PENGGUNA}</td>
        </tr>
        <tr>
            <td>NIM</td>
            <td>{$biodata_mahasiswa.NIM_MHS}</td>
        </tr>
        <tr>
            <td>NO UJIAN</td>
            <td>{$biodata_mahasiswa.NO_UJIAN}</td>
        </tr>
        <tr>
            <td>JENJANG</td>
            <td>{$biodata_mahasiswa.NM_JENJANG}</td>
        </tr>
        <tr>
            <td>FAKULTAS</td>
            <td>{$biodata_mahasiswa.NM_FAKULTAS|upper}</td>
        </tr>
        <tr>
            <td>PROGRAM STUDI</td>
            <td>{$biodata_mahasiswa.NM_PROGRAM_STUDI}</td>
        </tr>
        <tr>
            <td>KELAMIN</td>
            <td>
                {if $biodata_mahasiswa.KELAMIN_PENGGUNA==1}
                    Laki-laki
                {else}
                    Perempuan
                {/if}
            </td>
        </tr>
        <tr>
            <td>ALAMAT MAHASISWA</td>
            <td>{$biodata_mahasiswa.ALAMAT_MHS}</td>
        </tr>        
        <tr>
        	<td>Tanggal Cetak KTM</td>
            <td><input type="text" id="tgl_cetak" name="tgl_cetak"  value="{$biodata_mahasiswa.TGL_CETAK_KTM}" class="required" /></td>
        </tr>
        <tr>
        	<td>Status Cetak KTM</td>
            <td>{if $biodata_mahasiswa.TGL_CETAK_KTM != ''}Sudah Dicetak {else} Belum Dicetak{/if}</td>
        </tr>
        <tr>
        	<td colspan="2" style="text-align:center">
            	<input type="submit" value="Simpan" />
                <input type="button" value="Download Excel" onclick="window.open('excel-mahasiswa.php?nim={$smarty.request.nim}');" />
            	<input type="hidden" value="{$biodata_mahasiswa['ID_C_MHS']}" name="id_c_mhs" />
                <input type="hidden" value="{$smarty.request.nim}" name="nim" />
            </td>
        </tr>
</table>
</form>

{literal}
    <script>
            $("#tgl_cetak").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
			$('#f2').validate();
    </script>
{/literal}

{else if isset($smarty.request.nim) and $error != ''}

	<h4><b>{$error}</b></h4>
{/if}