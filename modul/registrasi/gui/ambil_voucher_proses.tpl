{extends file='base.tpl'}

{block name='stylesheet'}
	<style>
		.width-490 { width: 490px; }
		.center-block { margin-left: auto; margin-right: auto; }
	</style>
{/block}

{block name='content'}
	
	<div class="page-header">
		<h2>Halaman Pengambilan Voucher</h2>
	</div>
	
	<div class="row">
		<div class="text-center width-490 center-block">
			<p>Silahkan menggunakan nomor voucher <br /><br />
				<font color="#000033" style="font-size: 36px; border:#000000 1px solid; background-color:#99FFFF">&nbsp;{$kode_voucher}&nbsp;</font><br /><br />	
				untuk melakukan pembayaran ke Bank Mandiri.<br />
				Anda akan memperoleh No. PIN PPMB yang dapat digunakan<br />
				untuk melakukan pendaftaran calon mahasiswa baru secara online.<br /><br />
				<a class="btn" href="./">Kembali ke depan</a>
			</p>
		</div>
	</div>
	
{/block}