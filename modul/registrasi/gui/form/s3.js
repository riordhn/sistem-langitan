var ajax_loading_html       = '<div style="text-align: center"><img src="http://pendaftaran.unair.ac.id/img/ajax-loading.gif" /></div>';

var dialog_kota             = '#dialog-kota';
var dialog_kota_item_text   = '#dialog-kota-item-kota :selected';
var dialog_kota_item        = '#dialog-kota-item-kota';

var dialog_sekolah           = '#dialog-sekolah';
var dialog_sekolah_item_text = '#dialog-sekolah-item-sekolah :selected';
var dialog_sekolah_item      = '#dialog-sekolah-item-sekolah';

$(document).ready(function() {
    
    /* --------------------------------------------------------------- AUTOCOMPLETE KOTA LAHIR */
    $('#button-kota-lahir').click(function() {
        
        $(dialog_kota).dialog({ 
            modal: true, width: 450, draggable: true, title: 'Pilih Kota Kelahiran',
            buttons: {
                Batal: function() { $(dialog_kota).dialog('destroy'); },
                Pilih : function() { 
                    $('#nama-kota-lahir').val($(dialog_kota_item_text).text());
                    $('#id-kota-lahir').val($(dialog_kota_item).val());
                    $(dialog_kota).dialog('destroy');
                }
            },
            close: function() { $(dialog_kota).dialog('destroy'); }
        });
        
        $.ajax({
            url: 'form-kota.php',
            data: 'id_kota='+$('#id-kota-lahir').val(),
            beforeSend: function() { $(dialog_kota).html(ajax_loading_html); },
            success: function(r) { $(dialog_kota).html(r); }
        });
        
        return false;
    });
    /* --------------------------------------------------------------- */
    
    
    /* --------------------------------------------------------------- AUTOCOMPLETE KOTA */
    $('#button-kota').click(function() {
        
        $(dialog_kota).dialog({ 
            modal: true, width: 450, draggable: true, title: 'Pilih Kota',
            buttons: {
                Batal: function() { $(dialog_kota).dialog('destroy'); },
                Pilih : function() { 
                    $('#nama-kota').val($(dialog_kota_item_text).text());
                    $('#id-kota').val($(dialog_kota_item).val());
                    $(dialog_kota).dialog('destroy');
                }
            },
            close: function() { $(dialog_kota).dialog('destroy'); }
        });
        
        $.ajax({
            url: 'form-kota.php',
            data: 'id_kota='+$('#id-kota').val(),
            beforeSend: function() { $(dialog_kota).html(ajax_loading_html); },
            success: function(r) { $(dialog_kota).html(r); }
        });
        
        return false;
    });
    /* --------------------------------------------------------------- */
    
    
    /* --------------------------------------------------------------- AJAX PRODI PILIHAN */
    $('select[name="id_pilihan_1"]').change(function() {
        
        $('select[name="id_pilihan_2"]').html('');  // clear next pilihan
        $('select[name="id_pilihan_3"]').html('');  // clear next pilihan
        $('select[name="id_pilihan_4"]').html('');  // clear next pilihan
        
        $.ajax({
            url: 'form.php',
            type: 'GET',
            data: 'mode=get-prodi&terpilih[]='+$('select[name="id_pilihan_1"]').val(),
            success: function(r) {
                $('select[name="id_pilihan_2"]').html(r);
            }
        });
        
        // prodi minat
        $.ajax({
            url: 'form.php',
            type: 'GET',
            data: 'mode=get-prodi-minat&id_program_studi='+$('select[name="id_pilihan_1"]').val(),
            beforeSend: function() { $('select[name="id_prodi_minat"]').html(''); },
            success: function(r) {
                if (r.length > 0)
                {
                    $('select[name="id_prodi_minat"]').html(r);
                    $('select[name="id_prodi_minat"]').show();
                }
                else
                {
                    $('select[name="id_prodi_minat"]').html(r);
                    $('select[name="id_prodi_minat"]').hide();
                }
            }
        });
        
        // Kelas pilihan
        $.ajax({
            url: 'form.php',
            type: 'GET',
            data: 'mode=get-prodi-kelas&id_program_studi='+$('select[name="id_pilihan_1"]').val(),
            beforeSend: function() { $('select[name="id_kelas_pilihan"]').html(''); },
            success: function(r) {
                if (r.length > 0)
                {
                    $('select[name="id_kelas_pilihan"]').html(r);
                    $('select[name="id_kelas_pilihan"]').show();
                }
                else
                {
                    $('select[name="id_kelas_pilihan"]').html(r);
                    $('select[name="id_kelas_pilihan"]').hide();
                }
            }
        });
    });
    
    $('select[name="id_pilihan_2"]').change(function() {
        
        $('select[name="id_pilihan_3"]').html('');  // clear next pilihan
        $('select[name="id_pilihan_4"]').html('');  // clear next pilihan
        
        $.ajax({
            url: 'form.php',
            type: 'GET',
            data: 'mode=get-prodi&terpilih[]='+$('select[name="id_pilihan_1"]').val()+','+$('select[name="id_pilihan_2"]').val(),
            success: function(r) {
                $('select[name="id_pilihan_3"]').html(r);
            }
        });
    });
    
    $('select[name="id_pilihan_3"]').change(function() {
        
        $('select[name="id_pilihan_4"]').html('');  // clear next pilihan
        
        $.ajax({
            url: 'form.php',
            type: 'GET',
            data: 'mode=get-prodi&terpilih[]='+$('select[name="id_pilihan_1"]').val()+','+$('select[name="id_pilihan_2"]').val()+','+$('select[name="id_pilihan_3"]').val(),
            success: function(r) {
                $('select[name="id_pilihan_4"]').html(r);
            }
        });
    });
    /* --------------------------------------------------------------- */
    
    $('form').validate({
		rules: {
			nm_c_mhs:           { required: true },
            gelar:              { required: true },
            nm_kota_lahir:      { required: true },
            tgl_lahir_Year:     { required: true, number: true },
            alamat:             { required: true },
            nm_kota:            { required: true },
            telp:               { required: true },
            jenis_kelamin:      { required: true },
            kewarganegaraan:    { required: true },
            id_agama:           { required: true },
            sumber_biaya:       { required: true },
            email:              { email: true },
            
            ptn_s1:             { required: true },
            status_ptn_s1:      { required: true },
            prodi_s1:           { required: true },
            tgl_masuk_s1_Year:  { required: true },
            tgl_lulus_s1_Year:  { required: true },
            lama_studi_s1:      { required: true, number: true},
            ip_s1:              { required: true, number: true },
            
            ptn_s2:             { required: true },
            status_ptn_s2:      { required: true },
            prodi_s2:           { required: true },
            tgl_masuk_s2_Year:  { required: true },
            tgl_lulus_s2_Year:  { required: true },
            lama_studi_s2:      { required: true, number: true},
            ip_s2:              { required: true, number: true },
            jumlah_karya_ilmiah:{ required: true, number: true },
            
            id_pilihan_1:       { required: true }
            
		},
		messages: {
			nm_c_mhs:           { required: 'Silahkan isi nama anda disini' },
            gelar:              { required: 'Silahkan isi gelar' },
            nm_kota_lahir:      { required: 'Pilih kota kelahiran, apabila tidak ada dalam daftar silahkan menghubungi panitia pendaftaran' },
            tgl_lahir_Year:     { required: 'Masukkan tanggal kelahiran', number: 'Masukkan tanggal kelahiran dengan benar' },
            alamat:             { required: 'Masukkan alamat anda' },
            nm_kota:            { required: 'Masukkan kota alamat anda' },
            telp:               { required: 'Masukkan telp anda, bisa rumah / hp' },
            jenis_kelamin:      { required: 'Pilih jenis kelamin' },
            kewarganegaraan:    { required: 'Pilih kewarganegaraan anda' },
            id_agama:           { required: 'Pilih agama anda' },
            sumber_biaya:       { required: 'Pilih sumber biaya yang anda' },
            email:              { email: 'Masukkan email yang benar' },
            
            ptn_s1:             { required: 'Masukkan perguruan tinggi asal' },
            status_ptn_s1:      { required: 'Pilih status perguruan tinggi asal' },
            prodi_s1:           { required: 'Masukkan asal prodi anda ketika S1' },
            tgl_masuk_s1_Year:  { required: 'Masukkan tanggal masuk S1' },
            tgl_lulus_s1_Year:  { required: 'Masukkan tanggal lulus S1' },
            lama_studi_s1:      { required: 'Masukkan lama tempuh studi S1', number: 'Masukkan lama tempuh studi S1 dengan benar (koma diganti titik)' },
            ip_s1:              { required: 'Masukkan IPK kelulusan ', number: 'Masukkan IPK kelulusan dengan benar (koma diganti titik)' },
            
            ptn_s2:             { required: 'Masukkan perguruan tinggi asal' },
            status_ptn_s2:      { required: 'Pilih status perguruan tinggi asal' },
            prodi_s2:           { required: 'Masukkan asal prodi anda ketika S2' },
            tgl_masuk_s2_Year:  { required: 'Masukkan tanggal masuk S2' },
            tgl_lulus_s2_Year:  { required: 'Masukkan tanggal lulus S2' },
            lama_studi_s2:      { required: 'Masukkan lama tempuh studi S2', number: 'Masukkan lama tempuh studi S2 dengan benar (koma diganti titik)' },
            ip_s2:              { required: 'Masukkan IPK kelulusan ', number: 'Masukkan IPK kelulusan dengan benar (koma diganti titik)' },
            jumlah_karya_ilmiah:{ required: 'Masukkan jumlah karya ilmiah yang sudah di buat', number: 'Masukkan dengan format angka (koma diganti titik)' },
            
            id_pilihan_1:       { required: 'Tentukan prodi pilihan' }
		},
        ignore : '.ignore',
        errorPlacement: function(error, element) { error.appendTo(element.parent()); }
	});
    
    // Menampilkan tombol submit
    //$('#row-submit').show('slow');
    $('#table-formulir').fadeIn('slow');
});