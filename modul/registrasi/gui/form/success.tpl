{include file="header.tpl"}

<table>
    <tr>
        <th class="center-align">Informasi</th>
    </tr>
    <tr>
        <td class="center-align">
            <h2>Data isian formulir registrasi telah berhasil di simpan.</h2>
            <p><a href="print.php" target="_blank" id="print">Klik disini untuk mencetak form (Logout otomatis)</a></p>
            <p><a href="form.php">Klik untuk mengedit Kembali</a></p>
        </td>
    </tr>
</table>

<script type="text/javascript">
	$(document).ready(function() {
		$('#print').click(function() {
			setTimeout(function() {
				window.location = 'index.php?mode=logout';
			}, 60000);
		});
	});
</script>

{include file="footer.tpl"}