<table>
    <tr>
        <td>Negara</td>
        <td>
            <select id="dialog-sekolah-item-negara">
                <option value=""></option>
            {foreach $negara_set as $n}
                <option value="{$n.ID_NEGARA}" {if $n.ID_NEGARA == $sekolah.ID_NEGARA}selected="selected"{/if}>{$n.NM_NEGARA}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Provinsi</td>
        <td>
            <select id="dialog-sekolah-item-provinsi">
            {if !empty($provinsi_set)}
                <option value=""></option>
                {foreach $provinsi_set as $p}
                    <option value="{$p.ID_PROVINSI}" {if $p.ID_PROVINSI == $sekolah.ID_PROVINSI}selected="selected"{/if}>{$p.NM_PROVINSI}</option>
                {/foreach}
            {/if}
            </select>
        </td>
    </tr>
    <tr>
        <td>Kota</td>
        <td>
            <select id="dialog-sekolah-item-kota">
            {if !empty($kota_set)}
                <option value=""></option>
                {foreach $kota_set as $k}
                    <option value="{$k.ID_KOTA}" {if $k.ID_KOTA == $sekolah.ID_KOTA}selected="selected"{/if}>{$k.NM_KOTA}</option>
                {/foreach}
            {/if}
            </select>
        </td>
    </tr>
    <tr>
        <td>Sekolah</td>
        <td>
            <select id="dialog-sekolah-item-sekolah">
            {if !empty($sekolah_set)}
                <option value=""></option>
                {foreach $sekolah_set as $s}
                    <option value="{$s.ID_SEKOLAH}" {if $s.ID_SEKOLAH == $sekolah.ID_SEKOLAH}selected="selected"{/if}>{$s.NM_SEKOLAH}</option>
                {/foreach}
            {/if}
            </select>
        </td>
    </tr>
</table>
            
<div style="display: none; text-align: center" id="loading-dialog-sekolah"><img src="http://pendaftaran.unair.ac.id/img/ajax-loading.gif" /></div>
        
<script type="text/javascript">
    $('#dialog-sekolah-item-negara').change(function() {
        $.ajax({
            url: 'form-sekolah.php',
            data: 'mode=get-provinsi&id_negara=' + $('#dialog-sekolah-item-negara').val(),
            beforeSend: function() { 
                $('#dialog-sekolah-item-provinsi').html('');
                $('#dialog-sekolah-item-kota').html(''); 
                $('#dialog-sekolah-item-sekolah').html('');
                $('#loading-dialog-sekolah').css('display', 'block'); 
            },
            success: function(r) { 
                $('#dialog-sekolah-item-provinsi').html(r); 
                $('#loading-dialog-sekolah').css('display', 'none'); 
            }
        });
    });
    
    $('#dialog-sekolah-item-provinsi').change(function() {
        $.ajax({
            url: 'form-sekolah.php',
            data: 'mode=get-kota&id_provinsi=' + $('#dialog-sekolah-item-provinsi').val(),
            beforeSend: function() { 
                $('#dialog-sekolah-item-kota').html('');
                $('#dialog-sekolah-item-sekolah').html('');
                $('#loading-dialog-sekolah').css('display', 'block'); 
            },
            success: function(r) { 
                $('#dialog-sekolah-item-kota').html(r); 
                $('#loading-dialog-sekolah').css('display', 'none');
            }
        });
    });
    
    $('#dialog-sekolah-item-kota').change(function() {
        $.ajax({
            url: 'form-sekolah.php',
            data: 'mode=get-sekolah&id_kota=' + $('#dialog-sekolah-item-kota').val(),
            beforeSend: function() { 
                $('#dialog-sekolah-item-sekolah').html('');
                $('#loading-dialog-sekolah').css('display', 'block'); 
            },
            success: function(r) { 
                $('#dialog-sekolah-item-sekolah').html(r); 
                $('#loading-dialog-sekolah').css('display', 'none');
            }
        });
    });
</script>