{include file="header.tpl"}

<form action="form.php" method="post">
<input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
<table id="table-formulir" style="display: none">
    <caption>REGISTRATION FORM FOR UNDER GRADUATE of AIRLANGGA UNIVERSITAS</caption>
    {if $cmb.NM_C_MHS != '' and $cmb.ID_PILIHAN_1 != ''} {* PERTANDA SUDAH NGISI *}
        <tr>
            <td class="center-align" colspan="2"><a href="print.php" target="_blank">Print</a></td>
        </tr>
    {/if}
    <tr>
        <th colspan="2">Application Form</th>
    </tr>
    <tr>
        <td>Voucher Code</td>
        <td><strong>{$cmb.KODE_VOUCHER}</strong></td>
    </tr>
    <tr>
        <td>Form Type</td>
        <td><strong>{if $cmb.KODE_JURUSAN == '01'}Natural Science{else if $cmb.KODE_JURUSAN == '02'}IPS{else if $cmb.KODE_JURUSAN == '03'}IPC{/if}</strong></td>
    </tr>
    <tr>
        <th colspan="2">Personal Data</th>
    </tr>
    <tr>
        <td>Name</td>
        <td>
            <input type="text" name="nm_c_mhs" maxlength="64" size="64" value="{$cmb.NM_C_MHS}"/>
        </td>
    </tr>
    <tr>
        <td>Place of Birth (City)</td>
        <td>
            <input type="hidden" name="id_kota_lahir" id="id-kota-lahir" value="{$cmb.ID_KOTA_LAHIR}" />
            <input type="text" name="nm_kota_lahir" id="nama-kota-lahir" size="50" value="{$cmb.NM_KOTA_LAHIR}" readonly="readonly"/>
            <button id="button-kota-lahir">Edit</button>
        </td>
    </tr>
    <tr>
        <td>Date of Birth</td>
        <td>{html_select_date prefix="tgl_lahir_" time=$cmb.TGL_LAHIR day_empty="" month_empty="" year_empty="" field_order="DMY" start_year=-80 end_year=-10}</td>
    </tr>
    <tr>
        <td>Address</td>
        <td>
            <input type="text" name="alamat" size="65" maxlength="250" value="{$cmb.ALAMAT}" />
        </td>
    </tr>
    <tr>
        <td>City</td>
        <td>
            <input type="hidden" name="id_kota" id="id-kota" value="{$cmb.ID_KOTA}" />
            <input type="text" name="nm_kota" id="nama-kota" size="50" value="{$cmb.NM_KOTA}" readonly="readonly"/>
            <button id="button-kota">Edit</button>
        </td>
    </tr>
    <tr>
        <td>Phone Number</td>
        <td><input type="text" name="telp"  maxlength="32" value="{$cmb.TELP}"/></td>
    </tr>
    <tr>
        <td>Gender</td>
        <td>
            <select name="jenis_kelamin">
            {foreach $jenis_kelamin_set as $jk}
                <option value="{$jk.JENIS_KELAMIN}" {if $cmb.JENIS_KELAMIN == $jk.JENIS_KELAMIN}selected="selected"{/if}>{$jk.NM_JENIS_KELAMIN}</option>
            {/foreach}
            </select>
		</td>
    </tr>
    <tr>
        <td>Nationality</td>
        <td>
			<select name="kewarganegaraan">
			{foreach $kewarganegaraan_set as $kw}
				<option value="{$kw.ID_KEWARGANEGARAAN}" {if $cmb.KEWARGANEGARAAN == $kw.ID_KEWARGANEGARAAN}selected="selected"{/if}>{$kw.NM_KEWARGANEGARAAN}</option>
			{/foreach}
			</select>
        </td>
    </tr>
    <tr>
        <td>Religion</td>
        <td>
			<select name="id_agama">
			{foreach $agama_set as $a}
				<option value="{$a.ID_AGAMA}" {if $cmb.ID_AGAMA == $a.ID_AGAMA}selected="selected"{/if}>{$a.NM_AGAMA}</option>
			{/foreach}
			</select>
		</td>
    </tr>
    <tr>
        <td>Financial Support</td>
        <td>
			<select name="sumber_biaya">
			{foreach $sumber_biaya_set as $sb}
				<option value="{$sb.ID_SUMBER_BIAYA}" {if $cmb.SUMBER_BIAYA == $sb.ID_SUMBER_BIAYA}selected="selected"{/if}>{$sb.NM_SUMBER_BIAYA}</option>
			{/foreach}
			</select>
		</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>
            <input type="text" name="email" size="30" maxlength="64" value="{$cmb.EMAIL}" />
        </td>
    </tr>
	<tr>
		<td>Disabilitas / Difabel</td>
		<td>
			<select name="id_disabilitas">
			{foreach $disabilitas_set as $da}
				<option value="{$da.ID_DISABILITAS}" {if $cmb.ID_DISABILITAS == $da.ID_DISABILITAS}selected{/if}>{$da.NM_DISABILITAS}</option>
			{/foreach}
			</select>
		</td>
	</tr>
    <tr>
        <th colspan="2">Education Data</th>
    </tr>
    <tr>
        <td>Name of High School</td>
        <td>
            <input type="hidden" name="id_sekolah_asal" id="id-sekolah-asal" value="{$cmb.ID_SEKOLAH_ASAL}" />
            <input type="text" name="nm_sekolah_asal" id="nama-sekolah-asal" size="50" value="{$cmb.NM_SEKOLAH_ASAL}" readonly="readonly"/>
            <button id="button-sekolah-asal">Edit</button>
        </td>
    </tr>
    <tr>
        <td>Major of High School</td>
        <td>
			<select name="jurusan_sekolah">
			{foreach $jurusan_sekolah_set as $js}
				<option value="{$js.ID_JURUSAN_SEKOLAH}" {if $cmb.JURUSAN_SEKOLAH == $js.ID_JURUSAN_SEKOLAH}selected="selected"{/if}>{$js.NM_JURUSAN_SEKOLAH}</option>
			{/foreach}
			</select>
		</td>
    </tr>
    <tr>
        <td>Number and date issued of your Diploma Certificate</td>
        <td>Diploma Certificate Number:
            <input type="text" name="no_ijazah"  maxlength="40" value="{$cmb.NO_IJAZAH}" />
            <br/>
            Date issued of Diploma Certificate: {html_select_date prefix="tgl_ijazah_" day_empty="" month_empty="" year_empty="" field_order="DMY" start_year="-15" time=$cmb.TGL_IJAZAH}
        </td>
    </tr>
    <tr>
        <td>Diploma Certificate</td>
        <td> Year :
            <input type="text" name="tahun_lulus" size="4" maxlength="4" value="{$cmb.TAHUN_LULUS}"/>
            <br/>
            Number of Subject :
            <input type="text" name="jumlah_pelajaran_ijazah" size="2" maxlength="2" value="{$cmb.JUMLAH_PELAJARAN_IJAZAH}"/>
            <br/>
            Total Score of National Final Exam:
            <input type="text" name="nilai_ijazah" size="5" maxlength="5" value="{$cmb.NILAI_IJAZAH}" /> 
            * Not the mean of score</td>
    </tr>
    <tr>
        <td>National Examination (UNAS)</td>
        <td> Year of issue :
            <input type="text" name="tahun_uan" size="4" maxlength="4" value="{$cmb.TAHUN_UAN}"/>
            <br/>
            Number of Subject :
            <input type="text" name="jumlah_pelajaran_uan" size="2" maxlength="2" value="{$cmb.JUMLAH_PELAJARAN_UAN}"/>
            <br/>
            Total Score :
            <input type="text" name="nilai_uan" size="5" maxlength="5" value="{$cmb.NILAI_UAN}"/>
            * Not the mean of score</td>
    </tr>
    <tr>
        <th colspan="2" >Parent's Information - Father</td>
    </tr>
    <tr>
        <td>Father Name</td>
        <td><input type="text" name="nama_ayah" maxlength="64"  value="{$cmb.NAMA_AYAH}"/></td>
    </tr>
    <tr>
        <td>Address</td>
        <td><input type="text" name="alamat_ayah" size="65" maxlength="200" value="{$cmb.ALAMAT_AYAH}" /></td>
    </tr>
    <tr>
        <td>City</td>
        <td>
            <input type="hidden" name="id_kota_ayah" id="id-kota-ayah" value="{$cmb.ID_KOTA_AYAH}" />
            <input type="text" name="nm_kota_ayah" id="nama-kota-ayah" size="50" value="{$cmb.NM_KOTA_AYAH}" readonly="readonly"/>
            <button id="button-kota-ayah">Edit</button>
        </td>
    </tr>
    <tr>
        <td>Phone Number</td>
        <td><input type="text" name="telp_ayah"  maxlength="32" value="{$cmb.TELP_AYAH}"/></td>
    </tr>
    <tr>
        <td>Education</td>
        <td>
            <select name="pendidikan_ayah">
            {foreach $pendidikan_ortu_set as $po}
                <option value="{$po.ID_PENDIDIKAN_ORTU}" {if $cmb.PENDIDIKAN_AYAH == $po.ID_PENDIDIKAN_ORTU}selected="selected"{/if}>{$po.NM_PENDIDIKAN_ORTU}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Occupation</td>
        <td>
            <select name="pekerjaan_ayah">
            {foreach $pekerjaan_set as $p}
                <option value="{$p.ID_PEKERJAAN}" {if $cmb.PEKERJAAN_AYAH == $p.ID_PEKERJAAN}selected="selected"{/if}>{$p.NM_PEKERJAAN}</option>
            {/foreach}
            </select>
        </td>
    </tr>
	<tr>
		<td>Income (per Month)</td>
		<td>
			<input type="text" name="penghasilan_ayah" maxlength="18" value="{$cmb.PENGHASILAN_AYAH}" />
		</td>
	</tr>
    <tr>
        <td>Name Company / Agency</td>
        <td><input type="text" name="instansi_ayah" value="{$cmb.INSTANSI_AYAH}" maxlength="50" /></td>
    </tr>
    <tr>
        <td>Position in the company / Agency</td>
        <td><input type="text" name="jabatan_ayah" value="{$cmb.JABATAN_AYAH}" maxlength="50"/></td>
    </tr>
    <tr>
        <td>Working life</td>
        <td><input type="text" name="masa_kerja_ayah" value="{$cmb.MASA_KERJA_AYAH}" size="4" maxlength="4" />Tahun</td>
    </tr>
	<tr>
        <th colspan="2" >Parent's Information - Mother</td>
    </tr>
    <tr>
        <td>Mother Name</td>
        <td><input type="text" name="nama_ibu" maxlength="64"  value="{$cmb.NAMA_IBU}"/></td>
    </tr>
    <tr>
        <td>Address</td>
        <td><input type="text" name="alamat_ibu" size="65" maxlength="200" value="{$cmb.ALAMAT_IBU}" /></td>
    </tr>
    <tr>
        <td>City</td>
        <td>
            <input type="hidden" name="id_kota_ibu" id="id-kota-ibu" value="{$cmb.ID_KOTA_IBU}" />
            <input type="text" name="nm_kota_ibu" id="nama-kota-ibu" size="50" value="{$cmb.NM_KOTA_IBU}" readonly="readonly"/>
            <button id="button-kota-ibu">Edit</button>
        </td>
    </tr>
    <tr>
        <td>Phone</td>
        <td><input type="text" name="telp_ibu"  maxlength="32" value="{$cmb.TELP_IBU}"/></td>
    </tr>
    <tr>
        <td>Education</td>
        <td>
            <select name="pendidikan_ibu">
            {foreach $pendidikan_ortu_set as $po}
                <option value="{$po.ID_PENDIDIKAN_ORTU}" {if $cmb.PENDIDIKAN_IBU == $po.ID_PENDIDIKAN_ORTU}selected="selected"{/if}>{$po.NM_PENDIDIKAN_ORTU}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Occupation</td>
        <td>
            <select name="pekerjaan_ibu">
            {foreach $pekerjaan_set as $p}
                <option value="{$p.ID_PEKERJAAN}" {if $cmb.PEKERJAAN_IBU == $p.ID_PEKERJAAN}selected="selected"{/if}>{$p.NM_PEKERJAAN}</option>
            {/foreach}
            </select>
        </td>
    </tr>
	<tr>
		<td>Income (per Month)</td>
		<td>
			<input type="text" name="penghasilan_ibu" maxlength="18" value="{$cmb.PENGHASILAN_IBU}" />
		</td>
	</tr>
    <tr>
        <td>Name company / Agency</td>
        <td><input type="text" name="instansi_ibu" value="{$cmb.INSTANSI_IBU}" maxlength="50" /></td>
    </tr>
    <tr>
        <td>Position in the company / Agency</td>
        <td><input type="text" name="jabatan_ibu" value="{$cmb.JABATAN_IBU}" maxlength="50"/></td>
    </tr>
    <tr>
        <td>Working life</td>
        <td><input type="text" name="masa_kerja_ibu" value="{$cmb.MASA_KERJA_IBU}" size="4" maxlength="4" />Tahun</td>
    </tr>
	<tr>
        <th colspan="2" >Other information</td>
    </tr>	
	<tr>
        <td>Business scale/Occupation</td>
        <td>
            <select name="skala_pekerjaan_ortu">
            {foreach $skala_pekerjaan_set as $sp}
                <option value="{$sp.SKALA_PEKERJAAN}" {if $cmb.SKALA_PEKERJAAN_ORTU == $sp.SKALA_PEKERJAAN}selected="selected"{/if}>{$sp.NM_SKALA_PEKERJAAN}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Number of Brother</td>
        <td><input type="text" name="jumlah_kakak" size="2" maxlength="2"  value="{$cmb.JUMLAH_KAKAK}" /></td>
    </tr>
    <tr>
        <td>Number of Sister</td>
        <td><input type="text" name="jumlah_adik" size="2" maxlength="2"  value="{$cmb.JUMLAH_ADIK}" /></td>
    </tr>
	<tr>
        <th colspan="2">Residence and vehicle of family</th>
    </tr>
    <tr>
        <td>Residence of parents</td>
        <td>
            <select name="kediaman_ortu">
            {foreach $kediaman_ortu_set as $ko}
                <option value="{$ko.KEDIAMAN_ORTU}" {if $cmb.KEDIAMAN_ORTU == $ko.KEDIAMAN_ORTU}selected="selected"{/if}>{$ko.NM_KEDIAMAN_ORTU}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Land's area</td>
        <td>
            <select name="luas_tanah">
            {foreach $luas_tanah_set as $lt}
                <option value="{$lt.LUAS_TANAH}" {if $cmb.LUAS_TANAH == $lt.LUAS_TANAH}selected="selected"{/if}>{$lt.NM_LUAS_TANAH}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Building's area</td>
        <td>
            <select name="luas_bangunan">
            {foreach $luas_bangunan_set as $lb}
                <option value="{$lb.LUAS_BANGUNAN}" {if $cmb.LUAS_BANGUNAN == $lb.LUAS_BANGUNAN}selected="selected"{/if}>{$lb.NM_LUAS_BANGUNAN}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Selling value of tax object (NJOP)</td>
        <td>
            <select name="njop">
            {foreach $njop_set as $n}
                <option value="{$n.NJOP}" {if $cmb.NJOP == $n.NJOP}selected="selected"{/if}>{$n.NM_NJOP}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Electric Power</td>
        <td>
            <select name="listrik">
            {foreach $listrik_set as $l}
                <option value="{$l.LISTRIK}" {if $cmb.LISTRIK == $l.LISTRIK}selected="selected"{/if}>{$l.NM_LISTRIK}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Number of car</td>
        <td>
            <select name="kendaraan_r4">
            {foreach $kendaraan_r4_set as $kr4}
                <option value="{$kr4.KENDARAAN}" {if $cmb.KENDARAAN_R4 == $kr4.KENDARAAN}selected="selected"{/if}>{$kr4.NM_KENDARAAN}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Number of motorcycles</td>
        <td>
            <select name="kendaraan_r2">
            {foreach $kendaraan_r2_set as $kr2}
                <option value="{$kr2.KENDARAAN}" {if $cmb.KENDARAAN_R2 == $kr2.KENDARAAN}selected="selected"{/if}>{$kr2.NM_KENDARAAN}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Other property / treasure :</td>
        <td>
            <textarea name="kekayaan_lain" cols="75" rows="2" maxlength="256">{$cmb.KEKAYAAN_LAIN}</textarea>
        </td>
    </tr>
    <tr>
        <td>Other Property/ Treasure/ Ownership other assets: :</td>
        <td>
            <textarea name="info_lain" cols="75" rows="2" maxlength="256">{$cmb.INFO_LAIN}</textarea>
        </td>
    </tr>
    <tr>
        <th colspan="2">Study Program Choice</th>
    </tr>
    <tr>
        <td colspan="2" class="center-align"><font style="color: #f00;">Please note, the course has been selected will not be changed again.</font></td>
    </tr>
    {if $jumlah_pilihan >= 1}
    <tr>
        <td>Choosen 1<br/></td>
        <td>
            {if $cmb.ID_PILIHAN_1}
                <input type="hidden" name="id_pilihan_1" value="{$cmb.ID_PILIHAN_1}" />
                {$cmb.NM_PILIHAN_1}
            {else}
                <select name="id_pilihan_1">
                {foreach $program_studi_set as $ps}
                    <option value="{$ps.ID_PROGRAM_STUDI}">{$ps.NM_PROGRAM_STUDI}</option>
                {/foreach}
                </select>
            {/if}
        </td>
    </tr>
    {/if}
    {if $jumlah_pilihan >= 2}
	<tr>
        <td>Choosen 2</td>
        <td>
            {if $cmb.ID_PILIHAN_2}
                <input type="hidden" name="id_pilihan_2" value="{$cmb.ID_PILIHAN_2}" />
                {$cmb.NM_PILIHAN_2}
            {else}
                <select name="id_pilihan_2">
                {if $cmb.ID_PILIHAN_2}
                    {foreach $program_studi_set as $ps}
                        <option value="{$ps.ID_PROGRAM_STUDI}">{$ps.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                {/if}
                </select>
            {/if}
        </td>
    </tr>
    {/if}
    {if $jumlah_pilihan >= 3}
	<tr>
        <td>Pilihan 3</td>
        <td>
            {if $cmb.ID_PILIHAN_3}
                <input type="hidden" name="id_pilihan_3" value="{$cmb.ID_PILIHAN_3}" />
                {$cmb.NM_PILIHAN_3}
            {else}
                <select name="id_pilihan_3">
                {if $cmb.ID_PILIHAN_3}
                    {foreach $program_studi_set as $ps}
                        <option value="{$ps.ID_PROGRAM_STUDI}">{$ps.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                {/if}
                </select>
            {/if}
        </td>
    </tr>
    {/if}
    {if $jumlah_pilihan >= 4}
	<tr>
        <td>Pilihan 4</td>
        <td>
            {if $cmb.ID_PILIHAN_4}
                <input type="hidden" name="id_pilihan_4" value="{$cmb.ID_PILIHAN_4}" />
                {$cmb.NM_PILIHAN_4}
            {else}
                <select name="id_pilihan_4">
                {if $cmb.ID_PILIHAN_4}
                    {foreach $program_studi_set as $ps}
                        <option value="{$ps.ID_PROGRAM_STUDI}">{$ps.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                {/if}
                </select>
            {/if}
        </td>
    </tr>
    {/if}
    <tr>
        <td colspan="2" class="center-align">
            <input type="submit" value="Submit" />
        </td>
    </tr>
</table>
</form>

<!-- UNTUK menampung dialog -->
<div id="dialog-kota"></div><div id="dialog-sekolah"></div>
    
<script type="text/javascript">$(document).ready(function() { $.getScript('gui/form/s1-internasional.js'); });</script>

{include file="footer.tpl"}