<!DOCTYPE html>
<html>
<head>
    <title>Formulir Online Pendaftaran Mahasiswa Baru Universitas Airlangga</title>
    
    <link rel="shorcut icon" href="http://ppmb.unair.ac.id/images/favicon.ico" />
    
    <link rel="stylesheet" type="text/css" href="css/cssreset-min.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="css/cssgrids-min.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="css/portal.css" />
    <link rel="Stylesheet" type="text/css" href="css/jui/jquery-ui-1.8.20.custom.css" />
    
    <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.8.20.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/additional-methods.min.js"></script>
    
    <style>
	.ui-autocomplete {
		max-height: 200px;
		overflow-y: auto;
		/* prevent horizontal scrollbar */
		overflow-x: hidden;
		/* add padding to account for vertical scrollbar */
		padding-right: 20px;
	}
	/* IE 6 doesn't support max-height
	 * we use height instead, but this forces the menu to always be this tall
	 */
	* html .ui-autocomplete {
		height: 200px;
	}
	</style>
    </style>
</head>
<body>
    <div class="yui3-g" id="wrap-layout">
        <div class="yui3-u-1" id="header-layout">
            <div id="header-content"></div>
        </div>
        <div class="yui3-u-1" id="nav-layout">
            <div id="nav-content">
                <a class="nav-item" href="index.php?mode=logout">Logout</a>
            </div>
        </div>
        <div class="yui3-u-1" id="content-layout">
            <div id="content">