{extends file='base.tpl'}

{block name='stylesheet'}
	<style>
		.width-490 { width: 490px; }
		.center-block { margin-left: auto; margin-right: auto; }
		#kode_voucher {
			background-color: lime;
			border: 1px solid black;
			border-radius: 4px;
			-moz-border-radius: 4px;
			-webkit-border-radius: 4px;
			font-size: 36px;
			padding: 10px;
			display: inline-block;
		}
	</style>
{/block}

{block name='content'}

	<div class="page-header">
		<h2>Halaman Pengambilan Voucher</h2>
	</div>

	<div class="row">
		<div class="text-center width-490 center-block" style="background: navy;">
			<h4 style="color: #fff">Pengambilan No. Voucher</h4>
			<h3 style="color: #33ff00">{$nm_penerimaan}</h3>
		</div>
		{if $kode_voucher != ''}
			<div class="text-center width-490 center-block">
				<p style="margin-top: 10px;">Berikut adalah nomor yang bisa Anda gunakan untuk membayar di Bank Mandiri.</p>
			</div>
			<div class="text-center width-490 center-block"><p id="kode_voucher">{$kode_voucher}</p></div>
			<div class="text-center width-490 center-block">
				<form action="ambil_voucher_proses.php" method="post" style="margin-top: 10px;">
					<input type="hidden" name="verifikasi" value="{md5($kode_voucher)}">
					<input type="submit" class="btn btn-primary" value="Ambil" name="ok" />
				</form>
				<p>Tekan tombol "Ambil" untuk menggunakannya</p>
				<p style="color: red"><strong>Catatan :</strong><br/>Nomor voucher harus dicatat untuk digunakan pada saat melakukan pembayaran ke channel-channel pembayaran bank.</p>
				<p><a class="btn btn-small" href="./pengambilan_voucher.php">Kembali ke Depan</a></p>
			</div>
		{else}
			<div class="text-center width-490 center-block">
				<p style="margin-top: 10px;">No. Voucher tidak tersedia, silahkan menghubungi PPMB.</p>
				<p><a class="btn btn-small" href="./pengambilan_voucher.php">Kembali ke Depan</a></p>
			</div>
		{/if}
	</div>
					
	<div class="row">
		
	</div>

{/block}