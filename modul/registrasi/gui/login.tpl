{extends file='base.tpl'}
{block name='content'}
	
	<div class="page-header">
		<h2>Pendaftaran Online Universitas Airlangga</h2>
	</div>

	<form class="form-horizontal form-login" method="post">

		{if $error_info != ''}
			<div class="alert alert-error alert-block">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				{$error_info}
			</div>
		{/if}

		<div class="control-group">
			<label class="control-label" for="kode_voucher">Kode Voucher</label>
			<div class="controls">
				<input type="text" name="kode_voucher" placeholder="Kode Voucher" required maxlength="11"/>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="pin_voucher">Pin Voucher</label>
			<div class="controls">
				<input type="password" name="pin_voucher" placeholder="Pin Voucher" required maxlength="20"/>
			</div>
		</div>
		
		<div class="control-group">
			<div class="controls">
				<input type="hidden" name="mode" value="login" />
				<input type="submit" class="btn btn-primary" value="Login" />
			</div>
		</div>
	</form>

	<ul>
		<li>Untuk mendapatkan voucher, silahkan klik menu <strong>Pengambilan Voucher</strong> diatas.</li>
		<li>Pembayaran voucher hanya bisa dilakukan di <u>Bank Mandiri</u></li>
		<li>Pastikan kode voucher dan pin voucher sesuai dengan yang ada di bukti pembayaran.</li>
		<li>Untuk pertanyaan seputar pendaftaran, bisa hubungi helpdesk PPMB dibawah ini :
			<ul>
				<li>Telp : <strong>(031) 5956009</strong>, <strong>(031) 5956010</strong>, <strong>(031) 5956013</strong>, <strong>(031) 5956027</strong></li>
				<li>Email : <a href="mailto:ppmb@unair.ac.id">ppmb@unair.ac.id</a></li>
				<li>Web : <a href="http://ppmb.unair.ac.id" target="_blank">http://ppmb.unair.ac.id</a></li>
				<li>Alamat : Airlangga Convention Center (ACC), Kampus C Universitas Airlangga, Mulyorejo, Surabaya 60115</li>			
			</ul>
		</li>
	</ul>
	
{/block}