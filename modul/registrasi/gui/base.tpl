<!DOCTYPE html>
<html lang="id">
	<head>
		<title>Pendaftaran Online Universitas Airlangga</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="description" content="Pendaftaran online untuk calon mahasiswa baru Universitas Airlangga" />
		<meta name="author" content="Universitas Airlangga" />
		<meta name="keywords" content="pendaftaran, unair, {date('Y')}, universitas airlangga, mandiri, pascasarjana, doktor, profesi, spesialis" />
		
		<!-- GOOGLE WEBMASTER TOOLS -->
		<meta name="google-site-verification" content="ihkYFpHX_zbP4SfrkqIEF7c2Lp08dZFIxJ2MKI6m_Hk" />
		
		<!-- Bootstrap + Google Fonts -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="css/bootstrap.min.css" />
		<link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
		
		<!-- Custom Style -->
		<style>
			@font-face {
				font-family: "Droid Sans";
				src: url('css/DroidSans.ttf');
			}
			
			@font-face {
				font-family: "Droid Sans";
				src: url('css/DroidSans-Bold.ttf');
				font-weight: bold;
			}
			
			html, body { height: 100%; }
			
			body { 
				font-family: "Droid Sans", Arial, sans-serif;
			}
			
			/* Wrapper for page content to push down footer */
			#wrap {
				min-height: 100%;
				height: auto !important;
				height: 100%;
				/* Negative indent footer by it's height */
				margin: 0 auto -60px;
			}
			
			#wrap > .container { 
				padding-top: 60px;
			}
			
			#push, #footer {
				height: 60px;
			}
			
			#footer {
				background-color: #f5f5f5;
			}
			
			.brand > img { height: 30px; }
			
			input, button, select, textarea {
				font-family: "Droid Sans", Arial, sans-serif;
			}
			
			.form-login input, .form-login label {
				font-size: 16px;
			}
			
			.form-login input[type=text], .form-login input[type=password] {
				padding: 7px 9px;
				color: black;
			}
		</style>
		{block name="stylesheet"}{/block}
	</head>
	<body>
		<div id="wrap">
			
			<div class="navbar navbar-inverse navbar-fixed-top">
				<div class="navbar-inner">
					<div class="container">
						<a class="brand" href="http://www.unair.ac.id" target="_blank" style="float: right"><img src="http://ppmb.unair.ac.id/id/images/logo.png" /> Universitas Airlangga</a>
						<ul class="nav">	
							<li {if $nav_active == 'login'}class="active"{/if}><a href="./index.php">Login</a></li>
							<li {if $nav_active == 'voucher'}class="active"{/if}><a href="./pengambilan_voucher.php">Pengambilan Voucher</a></li>
							<li><a href="http://ppmb.unair.ac.id" target="_blank" title="Pusat Penerimaan Mahasiswa Baru">PPMB</a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="container">
				{block name='content'}{/block}
			</div>
			
			<!-- Footer Supress -->
			<div id="push"></div>
		</div>
			
		<div id="footer">
			<div class="container">
				<p class="muted" style="margin: 20px 0px;">&copy; Universitas Airlangga {date('Y')} - Pusat Penerimaan Mahasiswa Baru</p>
			</div>
		</div>
		
		<!-- Script -->
		<script src="../js/jquery-1.7.2.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		{literal}
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-41992721-2', 'unair.ac.id');
		  ga('send', 'pageview');

		</script>
		{/literal}
	</body>
</html>