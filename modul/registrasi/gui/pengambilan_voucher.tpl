{extends file='base.tpl'}
{block name='content'}
	
	<div class="page-header">
		<h2>Pengambilan Voucher Pendaftaran</h2>
	</div>
	
	<div class="well well-small text-center">
		<h3 style="color: red">PERHATIAN</h3>
		<p class="lead">SELURUH BIAYA PENDAFTARAN YANG TELAH DIBAYARKAN TIDAK DAPAT DITARIK KEMBALI<br/>
			JIKA TERDAPAT KESALAHAN DARI PIHAK PESERTA<br/>
			(Mohon Prodi Yang dituju dicek di <a href="http://ppmb.unair.ac.id" target="_blank">http://ppmb.unair.ac.id</a>)
		</p>
	</div>
	
	<hr/>
	
	<div class="row-fluid">
		
		<div class="span4">
			<h3 {if count($penerimaan_d3_set) == 0}class="muted"{/if}>Diploma (D3)</h3>
			{foreach $penerimaan_d3_set as $d3}
				<p><a class="btn" href="./ambil_voucher.php?id_penerimaan={$d3.ID_PENERIMAAN}">Diploma 3 Gelombang {$d3.GELOMBANG}</a></p>
			{foreachelse}
				<p class="muted">Tidak ada pendaftaran</p>
			{/foreach}
		</div>
		
		<div class="span4">
			<h3 {if count($penerimaan_s1_set) == 0}class="muted"{/if}>Sarjana (S1)</h3>
			{foreach $penerimaan_s1_set as $s1}
				{if $s1.ID_JALUR == 4}
					<p><a class="btn" href="./ambil_voucher.php?id_penerimaan={$s1.ID_PENERIMAAN}">Alih Jenis Gelombang {$s1.GELOMBANG}</a></p>
				{else if $s1.ID_JALUR == 3}
					<p><a class="btn" href="./ambil_voucher.php?id_penerimaan={$s1.ID_PENERIMAAN}">{$s1.NM_PENERIMAAN}</a></p>
				{else if $s1.ID_JALUR == 20}
					<p><a class="btn" href="./ambil_voucher.php?id_penerimaan={$s1.ID_PENERIMAAN}">PBSB Gelombang {$s1.GELOMBANG}</a></p>
				{else if $s1.ID_JALUR == 27}
					<p><a class="btn" href="./ambil_voucher.php?id_penerimaan={$s1.ID_PENERIMAAN}">Mandiri - {$s1.NM_PENERIMAAN}</a></p>
				{else}
					<p><a class="btn" href="./ambil_voucher.php?id_penerimaan={$s1.ID_PENERIMAAN}">{$s1.NM_PENERIMAAN}</a></p>
				{/if}
			{foreachelse}
				<p class="muted">Tidak ada pendaftaran</p>
			{/foreach}
		</div>
		
		<div class="span4">
			<h3 {if count($penerimaan_s2_set) == 0}class="muted"{/if}>Magister (S2)</h3>
			{foreach $penerimaan_s2_set as $s2}
				<p><a class="btn" href="./ambil_voucher.php?id_penerimaan={$s2.ID_PENERIMAAN}">{$s2.NM_PENERIMAAN}</a></p>
			{foreachelse}
				<p class="muted">Tidak ada pendaftaran</p>
			{/foreach}
		</div>
	</div>
	
	<hr />
	
	<div class="row-fluid">
		
		<div class="span4">
			<h3 {if count($penerimaan_s3_set) == 0}class="muted"{/if}>Doktor (S3)</h3>
			{foreach $penerimaan_s3_set as $s3}
				<p><a class="btn" href="./ambil_voucher.php?id_penerimaan={$s3.ID_PENERIMAAN}">{$s3.NM_PENERIMAAN}</a></p>
			{foreachelse}
				<p class="muted">Tidak ada pendaftaran</p>
			{/foreach}
		</div>
		
		<div class="span4">
			<h3 {if count($penerimaan_pr_set) == 0}class="muted"{/if}>Profesi</h3>
			{foreach $penerimaan_pr_set as $pr}
				<p><a class="btn" href="./ambil_voucher.php?id_penerimaan={$pr.ID_PENERIMAAN}">{$pr.NM_PENERIMAAN}</a></p>
			{foreachelse}
				<p class="muted">Tidak ada pendaftaran</p>
			{/foreach}
		</div>
		
		<div class="span4">
			<h3 {if count($penerimaan_sp_set) == 0}class="muted"{/if}>Spesialis</h3>
			{foreach $penerimaan_sp_set as $sp}
				<p><a class="btn" href="./ambil_voucher.php?id_penerimaan={$sp.ID_PENERIMAAN}">{$sp.NM_PENERIMAAN}</a></p>
			{foreachelse}
				<p class="muted">Tidak ada pendaftaran</p>
			{/foreach}
		</div>
	</div>
		
	<hr/>
	
{/block}