<?php
include('config.php');

$id_program_studi = $_POST['id_program_studi'];

$prodi_kelas_set = $db->QueryToArray("select * from prodi_kelas where id_program_studi = {$id_program_studi}");

if (count($prodi_kelas_set) > 0)
{
	echo "<option value=\"\">-- Pilih Kelas Pilihan --</option>";
	foreach ($prodi_kelas_set as $pk)
	{
		echo "<option value=\"{$pk['SINGKATAN']}\">{$pk['NM_PRODI_KELAS']}</option>";
	}
}
else
{
	echo "empty";
}

exit();

if ($id_program_studi == 55) // S2 ILMU HUKUM
{
    echo "<option value=\"KRUA\">Kelas Reguler Universitas Airlangga</option>";
    //echo "<option value=\"KKUBT\">Kelas Kerjasama Universitas Borneo Tarakan</option>";
    //echo "<option value=\"KKUHK\">Kelas Kerjasama Universitas Haluoleo Kendari</option>";
    //echo "<option value=\"KJ\">Kelas di luar domisili (Jakarta)</option>";
}
else if ($id_program_studi == 52 || $id_program_studi == 53) // SAINS HUKUM PEMBANGUNAN / MAGISTER HUKUM
{
    echo "<option value=\"KRUA\">Kelas Reguler Universitas Airlangga</option>";
    echo "<option value=\"KKUBT\">Kelas Kerjasama Universitas Borneo Tarakan</option>";
    echo "<option value=\"KKUHK\">Kelas Kerjasama Universitas Haluoleo Kendari</option>";
    //echo "<option value=\"KJ\">Kelas di luar domisili (Jakarta)</option>";
}
else if ($id_program_studi == 73) // MAGISTER MANAJEMEN
{
    echo "<option value=\"KS\">Kelas Sore</option>";
    echo "<option value=\"KAP\">Kelas Akhir Pekan</option>";
}
else if ($id_program_studi == 72) // ILMU MANAJEMEN
{
    echo "<option value=\"KP\">Kelas Pagi</option>";
    echo "<option value=\"KS\">Kelas Sore</option>";
}
else if ($id_program_studi == 126) // ILMU KEDOKTERAN (S3)
{
    echo "<option value=\"K-A\">Kelas A (Reguler)</option>";
    echo "<option value=\"K-B\">Kelas B (Paralel)</option>";
}
else
{
    echo "empty";
}
?>
