<?php
include 'config.php';
include 'class/Kota.class.php';

$Kota = new Kota($db);

$mode = get('mode', 'view');
$id_sekolah = get('id_sekolah', '');

if ($mode == 'view')
{
    $smarty->assign('negara_set', $Kota->GetListNegara());
    
    if ($id_sekolah != '')
    {
        $sekolah = $Kota->GetRowSekolah($id_sekolah);
        $smarty->assign('provinsi_set', $Kota->GetListProvinsi($sekolah['ID_NEGARA']));
        $smarty->assign('kota_set', $Kota->GetListKota($sekolah['ID_PROVINSI']));
        $smarty->assign('sekolah_set', $Kota->GetListSekolah($sekolah['ID_KOTA']));
        $smarty->assign('sekolah', $sekolah);
    }
}

if ($mode == 'get-provinsi')
{
    $id_negara = get('id_negara');
    
    $rows = $Kota->GetListProvinsi($id_negara);
    
    echo '<option value=""></option>';
    
    foreach ($rows as $row)
    {
        echo "<option value=\"{$row['ID_PROVINSI']}\">{$row['NM_PROVINSI']}</option>";
    }
    
    exit();
}

if ($mode == 'get-kota')
{
    $id_provinsi = get('id_provinsi');
    
    $rows = $Kota->GetListKota($id_provinsi);
    
    echo '<option value=""></option>';
    
    foreach ($rows as $row)
    {
        echo "<option value=\"{$row['ID_KOTA']}\">{$row['NM_KOTA']}</option>";
    }
    
    exit();
}

if ($mode == 'get-sekolah')
{
    $id_kota = get('id_kota');
    
    $rows = $Kota->GetListSekolah($id_kota);
    
    echo '<option value=""></option>';
    
    foreach ($rows as $row)
    {
        echo "<option value=\"{$row['ID_SEKOLAH']}\">{$row['NM_SEKOLAH']}</option>";
    }
    
    exit();
}

$smarty->display("form/sekolah.tpl");
?>
