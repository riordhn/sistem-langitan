<?php
session_start();

include 'config.php';
include 'class/CalonMahasiswa.class.php';

$CalonMahasiswa = new CalonMahasiswa($db);
$mode = get('mode', 'view');

if (empty($_SESSION['ID_C_MHS'])) { header("location: /index.php"); exit(); } 
$id_c_mhs = $_SESSION['ID_C_MHS'];

function get_template(&$cmb)
{        
        // Jumlah pilihan
		if ($cmb['KODE_JURUSAN'] == '01') { $jumlah_pilihan = 2; $jurusan_sekolah = 1; } //ipa
		if ($cmb['KODE_JURUSAN'] == '02') { $jumlah_pilihan = 2; $jurusan_sekolah = 2; } //ips
		if ($cmb['KODE_JURUSAN'] == '03') { $jumlah_pilihan = 4; $jurusan_sekolah = 3; } //ipc

		// Template Form
		if ($cmb['ID_JALUR'] == 5) { $form_template = 'd3'; }
		if ($cmb['ID_JALUR'] == 4) { $form_template = 's1-aj'; $jumlah_pilihan = 1; }
		if ($cmb['ID_JALUR'] == 3 || $cmb['ID_JALUR'] == 20) { $form_template = 's1'; }
		if ($cmb['ID_JALUR'] == 27) { $form_template = 's1-internasional'; }

		if ($cmb['ID_JALUR'] == 6 || $cmb['ID_JALUR'] == 34 || $cmb['ID_JALUR'] == 35 || $cmb['ID_JALUR'] == 41)  // Pasca S2 / S3 / Jakarta / Kerjasama Haluoleo / Internasional
		{
			if ($cmb['ID_JENJANG'] == 2) $form_template = 's2';
			if ($cmb['ID_JENJANG'] == 3) $form_template = 's3';

			$jumlah_pilihan = 1;    
		}

		if ($cmb['ID_JALUR'] == 23) { $form_template = 'pr'; $jumlah_pilihan = 1; }
		if ($cmb['ID_JALUR'] == 24) { $form_template = 'sp'; $jumlah_pilihan = 1; }

		return $form_template;
}

function get_jumlah_pilihan(&$cmb)
{
        // Jumlah pilihan
		if ($cmb['KODE_JURUSAN'] == '01') { $jumlah_pilihan = 2; $jurusan_sekolah = 1; } //ipa
		if ($cmb['KODE_JURUSAN'] == '02') { $jumlah_pilihan = 2; $jurusan_sekolah = 2; } //ips
		if ($cmb['KODE_JURUSAN'] == '03') { $jumlah_pilihan = 4; $jurusan_sekolah = 3; } //ipc

		// Template Form
		if ($cmb['ID_JALUR'] == 5) { $form_template = 'd3'; }
		if ($cmb['ID_JALUR'] == 4) { $form_template = 's1-aj'; $jumlah_pilihan = 1; }
		if ($cmb['ID_JALUR'] == 3 || $cmb['ID_JALUR'] == 20) { $form_template = 's1'; }
		if ($cmb['ID_JALUR'] == 27) { $form_template = 's1-internasional'; }

		if ($cmb['ID_JALUR'] == 6 || $cmb['ID_JALUR'] == 34 || $cmb['ID_JALUR'] == 35 || $cmb['ID_JALUR'] == 41)  // Pasca S2 / S3 / Jakarta / Kerjasama Haluoleo / Internasional
		{
			if ($cmb['ID_JENJANG'] == 2) $form_template = 's2';
			if ($cmb['ID_JENJANG'] == 3) $form_template = 's3';

			$jumlah_pilihan = 1;    
		}

		if ($cmb['ID_JALUR'] == 23) { $form_template = 'pr'; $jumlah_pilihan = 1; }
		if ($cmb['ID_JALUR'] == 24) { $form_template = 'sp'; $jumlah_pilihan = 1; }

		return $jumlah_pilihan;
}

function get_jurusan_sekolah(&$cmb)
{
        // Jumlah pilihan
		if ($cmb['KODE_JURUSAN'] == '01') { $jumlah_pilihan = 2; $jurusan_sekolah = 1; } //ipa
		if ($cmb['KODE_JURUSAN'] == '02') { $jumlah_pilihan = 2; $jurusan_sekolah = 2; } //ips
		if ($cmb['KODE_JURUSAN'] == '03') { $jumlah_pilihan = 4; $jurusan_sekolah = 3; } //ipc

		// Template Form
		if ($cmb['ID_JALUR'] == 5) { $form_template = 'd3'; }
		if ($cmb['ID_JALUR'] == 4) { $form_template = 's1-aj'; $jumlah_pilihan = 1; }
		if ($cmb['ID_JALUR'] == 3 || $cmb['ID_JALUR'] == 20) { $form_template = 's1'; }
		if ($cmb['ID_JALUR'] == 27) { $form_template = 's1-internasional'; }

		if ($cmb['ID_JALUR'] == 6 || $cmb['ID_JALUR'] == 34 || $cmb['ID_JALUR'] == 35 || $cmb['ID_JALUR'] == 41)  // Pasca S2 / S3 / Jakarta / Kerjasama Haluoleo / Internasional
		{
			if ($cmb['ID_JENJANG'] == 2) $form_template = 's2';
			if ($cmb['ID_JENJANG'] == 3) $form_template = 's3';

			$jumlah_pilihan = 1;    
		}

		if ($cmb['ID_JALUR'] == 23) { $form_template = 'pr'; $jumlah_pilihan = 1; }
		if ($cmb['ID_JALUR'] == 24) { $form_template = 'sp'; $jumlah_pilihan = 1; }

		return $jurusan_sekolah;
}

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $result = $CalonMahasiswa->Update($id_c_mhs, $_POST);
    
    if (!$result)
    {
    	$cmb = $CalonMahasiswa->GetData($id_c_mhs);
    	$form_template		= get_template($cmb);
		$jumlah_pilihan 	= get_jumlah_pilihan($cmb);
		$jurusan_sekolah 	= get_jurusan_sekolah($cmb);
    }
    else
    {
    	$form_template = 'success';
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
    // Melakukan cek apakah sudah login atau belum
    
    if ($mode == 'view')
    {   
        $cmb = $CalonMahasiswa->GetData($id_c_mhs);
        $smarty->assign('cmb', $cmb);
        
        /**
		// Jumlah pilihan
		if ($cmb['KODE_JURUSAN'] == '01') { $jumlah_pilihan = 2; $jurusan_sekolah = 1; } //ipa
		if ($cmb['KODE_JURUSAN'] == '02') { $jumlah_pilihan = 2; $jurusan_sekolah = 2; } //ips
		if ($cmb['KODE_JURUSAN'] == '03') { $jumlah_pilihan = 4; $jurusan_sekolah = 3; } //ipc

		// Template Form
		if ($cmb['ID_JALUR'] == 5) { $form_template = 'd3'; }
		if ($cmb['ID_JALUR'] == 4) { $form_template = 's1-aj'; $jumlah_pilihan = 1; }
		if ($cmb['ID_JALUR'] == 3 || $cmb['ID_JALUR'] == 20) { $form_template = 's1'; }
		if ($cmb['ID_JALUR'] == 27) { $form_template = 's1-internasional'; }

		if ($cmb['ID_JALUR'] == 6 || $cmb['ID_JALUR'] == 34 || $cmb['ID_JALUR'] == 35 || $cmb['ID_JALUR'] == 41)  // Pasca S2 / S3 / Jakarta / Kerjasama Haluoleo / Internasional
		{
			if ($cmb['ID_JENJANG'] == 2) $form_template = 's2';
			if ($cmb['ID_JENJANG'] == 3) $form_template = 's3';

			$jumlah_pilihan = 1;    
		}

		if ($cmb['ID_JALUR'] == 23) { $form_template = 'pr'; $jumlah_pilihan = 1; }
		if ($cmb['ID_JALUR'] == 24) { $form_template = 'sp'; $jumlah_pilihan = 1; }
		*/
		

		$form_template		= get_template($cmb);
		$jumlah_pilihan 	= get_jumlah_pilihan($cmb);
		$jurusan_sekolah 	= get_jurusan_sekolah($cmb);
    
        $smarty->assign('kota_set',             $CalonMahasiswa->GetListKota());
        $smarty->assign('jenis_kelamin_set',    $CalonMahasiswa->GetListJenisKelamin());
        $smarty->assign('kewarganegaraan_set',  $CalonMahasiswa->GetListKewarganegaraan());
        $smarty->assign('agama_set',            $CalonMahasiswa->GetListAgama());
        $smarty->assign('sumber_biaya_set',     $CalonMahasiswa->GetListSumberBiaya());
        $smarty->assign('jurusan_sekolah_set',  $CalonMahasiswa->GetListJurusanSekolah());
        $smarty->assign('pendidikan_ortu_set',  $CalonMahasiswa->GetListPendidikanOrtu());
        $smarty->assign('pekerjaan_set',        $CalonMahasiswa->GetListPekerjaan());
        $smarty->assign('penghasilan_ortu_set', $CalonMahasiswa->GetListPenghasilanOrtu());
        $smarty->assign('skala_pekerjaan_set',  $CalonMahasiswa->GetListSkalaPekerjaan());
        $smarty->assign('kediaman_ortu_set',    $CalonMahasiswa->GetListKediamanOrtu());
        $smarty->assign('luas_tanah_set',       $CalonMahasiswa->GetListLuasTanah());
        $smarty->assign('luas_bangunan_set',    $CalonMahasiswa->GetListLuasBangunan());
        $smarty->assign('njop_set',             $CalonMahasiswa->GetListNJOP());
        $smarty->assign('listrik_set',          $CalonMahasiswa->GetListListrik());
        $smarty->assign('kendaraan_r4_set',     $CalonMahasiswa->GetListKendaraanR4());
        $smarty->assign('kendaraan_r2_set',     $CalonMahasiswa->GetListKendaraanR2());
        $smarty->assign('status_ptn_set',       $CalonMahasiswa->GetListStatusPTN());
        $smarty->assign('program_studi_set',    $CalonMahasiswa->GetListProgramStudi($id_c_mhs));
		$smarty->assign('disabilitas_set',		$CalonMahasiswa->GetListDisabilitas());
        
        // Sekolah dan jumlah pilihan
        $smarty->assign('jumlah_pilihan', $jumlah_pilihan);
        $smarty->assign('jurusan_sekolah', $jurusan_sekolah);
        
        // Prodi minat
        if ($cmb['ID_PILIHAN_1'] != '')
        {
            $smarty->assign('prodi_minat_set', $CalonMahasiswa->GetListProgramStudiMinat($id_c_mhs, $cmb['ID_PILIHAN_1']));
        }
    }
    
    // Autocomplete kota
    if ($mode == 'get-kota')
    {
        $term = trim(strip_tags(get('term')));
        
        echo json_encode($CalonMahasiswa->GetListKotaSearch(strtoupper($term)));
        exit();
    }
    
    // autocomplete sekolah
    if ($mode == 'get-sekolah')
    {
        $term = trim(strip_tags(get('term')));
        
        echo json_encode($CalonMahasiswa->GetListSekolahSearch(strtoupper($term)));
        exit();
    }
    
    if ($mode == 'get-prodi')
    {
        $terpilih = implode(",",$_GET['terpilih']);
        
        foreach ($CalonMahasiswa->GetListProgramStudi($id_c_mhs, $terpilih) as $ps)
        {
            echo "<option value=\"{$ps['ID_PROGRAM_STUDI']}\">{$ps['NM_PROGRAM_STUDI']}</option>";
        }
        
        exit();
    }
    
    if ($mode == 'get-prodi-minat')
    {
        $id_program_studi = $_GET['id_program_studi'];
        
        foreach ($CalonMahasiswa->GetListProgramStudiMinat($id_c_mhs, $id_program_studi) as $ps)
        {
            echo "<option value=\"{$ps['ID_PRODI_MINAT']}\">{$ps['NM_PRODI_MINAT']}</option>";
        }
        exit();
    }
    
    if ($mode == 'get-prodi-kelas')
    {
        $id_program_studi = get('id_program_studi','');
        
        foreach ($CalonMahasiswa->GetListProdiKelas($id_program_studi) as $pk)
        {
            echo "<option value=\"{$pk['ID_PRODI_KELAS']}\">{$pk['NM_PRODI_KELAS']}</option>";
        }
        
        exit();
    }
}

$smarty->display("form/{$form_template}.tpl");
?>
