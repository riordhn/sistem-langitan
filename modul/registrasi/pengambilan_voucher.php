<?php
include 'config.php';

$smarty->assignByRef('penerimaan_d3_set', $db->QueryToArray("
	select id_penerimaan, nm_penerimaan, id_jenjang, id_jalur, tahun, semester, gelombang from penerimaan
	where id_jenjang = 5 and is_aktif = 1 and (current_timestamp between tgl_awal_voucher and tgl_akhir_voucher)"));

$smarty->assignByRef('penerimaan_s1_set', $db->QueryToArray("
	select id_penerimaan, nm_penerimaan, id_jenjang, id_jalur, tahun, semester, gelombang from penerimaan
	where id_jenjang = 1 and is_aktif = 1 and (current_timestamp between tgl_awal_voucher and tgl_akhir_voucher)"));

$smarty->assignByRef('penerimaan_s2_set', $db->QueryToArray("
	select id_penerimaan, nm_penerimaan, id_jenjang, id_jalur, tahun, semester, gelombang from penerimaan
	where id_jenjang = 2 and is_aktif = 1 and (current_timestamp between tgl_awal_voucher and tgl_akhir_voucher)"));

$smarty->assignByRef('penerimaan_s3_set', $db->QueryToArray("
	select id_penerimaan, nm_penerimaan, id_jenjang, id_jalur, tahun, semester, gelombang from penerimaan
	where id_jenjang = 3 and is_aktif = 1 and (current_timestamp between tgl_awal_voucher and tgl_akhir_voucher)"));

$smarty->assignByRef('penerimaan_pr_set', $db->QueryToArray("
	select id_penerimaan, nm_penerimaan, id_jenjang, id_jalur, tahun, semester, gelombang from penerimaan
	where id_jenjang = 9 and is_aktif = 1 and (current_timestamp between tgl_awal_voucher and tgl_akhir_voucher)"));

$smarty->assignByRef('penerimaan_sp_set', $db->QueryToArray("
	select id_penerimaan, nm_penerimaan, id_jenjang, id_jalur, tahun, semester, gelombang from penerimaan
	where id_jenjang = 10 and is_aktif = 1 and (current_timestamp between tgl_awal_voucher and tgl_akhir_voucher)"));

$smarty->assign('nav_active', 'voucher');
$smarty->display('pengambilan_voucher.tpl');
?>