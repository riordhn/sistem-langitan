<?php
session_start();

include 'config.php';
include 'class/CalonMahasiswa.class.php';
include '../../includes/fpdf/fpdf.php';

if (empty($_SESSION['ID_C_MHS'])) { header("location: /index.php"); exit(); } 

$id_c_mhs = $_SESSION['ID_C_MHS'];
$CalonMahasiswa = new CalonMahasiswa($db);

$cmb = $CalonMahasiswa->GetData($id_c_mhs);

if ($cmb['ID_JALUR'] != 27)
{
	$CalonMahasiswa->PrintPdf($id_c_mhs, $pdf = new FPDF());
}
else
{
	$CalonMahasiswa->PrintPdfEnglish($id_c_mhs, $pdf = new FPDF());
}

// Otomatis logout
$_SESSION['ID_C_MHS'] = '';
?>