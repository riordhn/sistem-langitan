<?php
include('config.php');

$id_program_studi = post('id_program_studi', '');

if ($id_program_studi != '')
{
    $db->Query("select id_penerimaan from calon_mahasiswa_baru where id_c_mhs = {$_SESSION['camaba']}");
    $row = $db->FetchAssoc();
    
    $prodi_minat_set = $db->QueryToArray("
        SELECT ID_PRODI_MINAT, NM_PRODI_MINAT FROM PRODI_MINAT
        WHERE ID_PROGRAM_STUDI = {$id_program_studi} AND ID_PRODI_MINAT NOT IN (27, 29)");
        
    $prodi_minat_set = $db->QueryToArray("
        select pm.id_prodi_minat, pm.nm_prodi_minat from penerimaan_prodi_minat ppm
        join prodi_minat pm on PM.ID_PRODI_MINAT = ppm.ID_PRODI_MINAT
        where ppm.id_penerimaan = {$row['ID_PENERIMAAN']} and pm.id_program_studi = {$id_program_studi} and ppm.is_aktif = 1");

    if (count($prodi_minat_set) > 0)
    {
		echo "<option value=\"\">-- Pilih Prodi Minat --</option>";
        foreach ($prodi_minat_set as $pm)
            echo "<option value=\"{$pm['ID_PRODI_MINAT']}\">{$pm['NM_PRODI_MINAT']}</option>";
    }
    else
    {
        echo 'empty';
    }
}
else
{
    echo 'empty';
}
?>
