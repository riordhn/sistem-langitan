<?php
include 'config.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	if (post('mode') == 'login')
	{
		$kode_voucher = str_replace("'", "''", post('kode_voucher'));
		$pin_voucher = str_replace("'", "''", post('pin_voucher'));
		
		// Waktu saat ini
		$time = time();

		// query di tabel voucher
		$db->Parse("
			select
				v.id_voucher, p.id_penerimaan, kode_voucher, pin_voucher, id_jalur, 
				to_char(tgl_awal_registrasi,'YYYY-MM-DD') as tgl_awal_registrasi,
				to_char(tgl_akhir_registrasi, 'YYYY-MM-DD') as tgl_akhir_registrasi
			from voucher v
			join penerimaan p on p.id_penerimaan = v.id_penerimaan
			where kode_voucher = :kode_voucher and pin_voucher = :pin_voucher and p.is_aktif = 1 and v.tgl_bayar is not null");
		$db->BindByName(':kode_voucher', $kode_voucher);
		$db->BindByName(':pin_voucher', $pin_voucher);
		$db->Execute();
		
		// mendapatkan row voucher
		$voucher = $db->FetchAssoc();
		
		// saat voucher sudah di beli dan password benar
		if ($voucher['ID_VOUCHER'] != '')
		{
			// Cek range tanggal
			if (strtotime($voucher['TGL_AWAL_REGISTRASI'] . " 00:00:00") <= $time && $time <= strtotime($voucher['TGL_AKHIR_REGISTRASI'] . " 23:59:59"))
			{
				
				// cek di tabel calon_mahasiswa_baru
				$db->Query("
					select id_c_mhs from calon_mahasiswa_baru
					where kode_voucher = '{$voucher['KODE_VOUCHER']}' and pin_voucher = '{$voucher['PIN_VOUCHER']}'");
				$cmb = $db->FetchAssoc();
				
				// saat belum ada di data calon
				if ( ! $cmb)
				{					   
					// insert data calon baru
					$db->Query("
						insert into calon_mahasiswa_baru (id_penerimaan, kode_voucher, pin_voucher, id_jalur, tgl_registrasi) values
						({$voucher['ID_PENERIMAAN']}, '{$voucher['KODE_VOUCHER']}', '{$voucher['PIN_VOUCHER']}', {$voucher['ID_JALUR']}, current_date)");
				}
				
				// di select kembali
				$db->Query("
					select id_c_mhs from calon_mahasiswa_baru
					where kode_voucher = '{$voucher['KODE_VOUCHER']}' and pin_voucher = '{$voucher['PIN_VOUCHER']}'");
				$cmb = $db->FetchAssoc();
				
				if ($cmb)
				{
					
					// menyimpan session
					$_SESSION['ID_C_MHS'] = $cmb['ID_C_MHS'];
					
					// redirect ke halaman form
					header('Location: /modul/registrasi/form.php');
					exit();
				}
			}
			else
			{
				$smarty->assign('error_info', $error_info = "Mohon maaf, sudah melewati batas tanggal pendaftaran");
			}
		}
		else
		{
			$smarty->assign('error_info', $error_info = "Mohon maaf, voucher anda tidak ditemukan atau pin salah");
		}
	}
}
 
// jika logout
if (get('mode') == 'logout')
{
	//$_SESSION['camaba'] = '';
	$_SESSION['ID_C_MHS'] = ''; // form baru
	session_destroy();
	header('Location: /modul/registrasi/');
	exit();
}

// Jika sudah ada session
if ( ! empty($_SESSION['ID_C_MHS'])) 
{
	// Redirect ke halaman form
	header('Location: /modul/registrasi/form.php');
} 
else //jika belum ada
{
	$smarty->assign('nav_active', 'login');
	$smarty->display('login.tpl');
}
?>