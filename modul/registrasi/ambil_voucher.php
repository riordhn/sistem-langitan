<?php
include 'config.php';

$id_penerimaan = get('id_penerimaan');

if ( ! is_numeric($id_penerimaan))
{
	header("Location: pengambilan_voucher.php");
	exit();
}

// Ambil Penerimaan
$db->Query("select * from penerimaan where id_penerimaan = {$id_penerimaan}");
$penerimaan = $db->FetchAssoc();

// Ambil Voucher
$db->Query("
	select kode_voucher from (
		select kode_voucher from aucc.voucher a, aucc.penerimaan b 
		where a.id_penerimaan=b.id_penerimaan and a.id_penerimaan='{$id_penerimaan}' and a.is_aktif='1' 
		and (a.tgl_ambil is null or to_char(a.tgl_ambil,'YYYYMMDDHH24MISS')='00000000000000')
		and (b.tgl_awal_voucher <= sysdate and b.tgl_akhir_voucher >= sysdate)
		order by dbms_random.value
	) where rownum=1");
$voucher = $db->FetchAssoc();

// Setting session voucher
if($_SESSION['VOUCHER'] != '') {
	unset($_SESSION['VOUCHER']);
}

if ($voucher)
{
	$kode_voucher = $voucher['KODE_VOUCHER'];
	$_SESSION['VOUCHER'] = $voucher['KODE_VOUCHER'];
}
else
{
	$kode_voucher = '';
	$_SESSION['VOUCHER'] = '';
}

// Diploma
if ($penerimaan['ID_JENJANG'] == 5)
{
	$nm_penerimaan = "Diploma 3 Gelombang " . $penerimaan['GELOMBANG'];
}
else if ($penerimaan['ID_JENJANG'] == 1)
{
	if ($penerimaan['ID_JALUR'] == 4)
		$nm_penerimaan = "Alih Jenis Gelombang " . $penerimaan['GELOMBANG'];
	else if ($penerimaan['ID_JALUR'] == 3)
		$nm_penerimaan = "Mandiri Gelombang " . $penerimaan['GELOMBANG'];
	else if ($penerimaan['ID_JALUR'] == 20)
		$nm_penerimaan = "PBSB Gelombang " . $penerimaan['GELOMBANG'];
	else if ($penerimaan['ID_JALUR'] == 27)
		$nm_penerimaan = "Mandiri (Internasional)";
	else
		$nm_penerimaan = $penerimaan['NM_PENERIMAAN'];
}
else
{
	$nm_penerimaan = $penerimaan['NM_PENERIMAAN'];
}

$smarty->assign('nm_penerimaan', $nm_penerimaan);
$smarty->assign('kode_voucher', $kode_voucher);
$smarty->assign('nav_active', 'voucher');
$smarty->display("ambil_voucher.tpl");
?>
