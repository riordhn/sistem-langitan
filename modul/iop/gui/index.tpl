<html>
	<head>
		<title>IOP - Universitas Airlangga</title>
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		
		<link rel="stylesheet" type="text/css" href="../../css/reset.css" />
		<link rel="stylesheet" type="text/css" href="../../css/text.css" />
		<link rel="stylesheet" type="text/css" href="../../css/iop.css" />
		<link rel="stylesheet" type="text/css" href="../../css/custom-theme/jquery-ui-smoothness.css" />
		
		<link rel="shortcut icon" href="../../img/icon.ico" />
		
	</head>
	<body>
		<table class="clear-margin-bottom">
			<colgroup>
				<col />
				<col class="main-width"/>
				<col />
			</colgroup>
			<thead>
				<tr>
					<td class="header-left"></td>
					<td class="header-center"></td>
					<td class="header-right"></td>
				</tr>
				<tr>
					<td class="tab-left"></td>
					<td class="tab-center">
						<ul>
						<!-- Untuk Menampilkan Menu Utama  -->
						{foreach $modul_set as $m}
							{if $m.AKSES == 1}
								<li><a href="#{$m.NM_MODUL}!{$m.PAGE}" class="nav">{$m.TITLE}</a></li>
								<li class="divider"></li>
							{/if}
						{/foreach}
							<li><a class="disable-ajax" href="../../logout.php">Logout</a></li>
						</ul>
					</td>
					<td class="tab-right"></td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="body-left">&nbsp;</td>
					<td class="body-center">
						<table class="content-table">
							<colgroup>
								<col />
								<col />
							</colgroup>
							<tr>
								<td colspan="2" id="breadcrumbs" class="breadcrumbs" ></td>
							</tr>
							<tr>
								<td id="menu" class="menu"></td>
								<td id="content" class="content">Loading data...</td>
							</tr>
						</table>
					</td>
					<td class="body-right">&nbsp;</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td class="foot-left">&nbsp;</td>
					<td class="foot-center">
						<div class="footer-nav">
							<a href="">Home</a> | <a href="">About</a> | <a href="">Sitemap</a> | <a href="">RSS</a> | <a href="">Contact Us</a>
						</div>
						<div class="footer">Copyright &copy; 2015 - {$nama_pt} <br />Sistem Langitan NU <br />oleh <a target="_blank" href="http://unair.ac.id" class="disable-ajax">Universitas Airlangga</a></div>
					</td>
					<td class="foot-right">&nbsp;</td>
				</tr>
			</tfoot>
		</table>

		<!-- script -->
		<script type="text/javascript" src="../../js/jquery-1.7.2.min.js"></script>
		<script type="text/javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
		<script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
		<script type="text/javascript" src="../../js/additional-methods.min.js"></script>
		<script type="text/javascript" src="../../js/jquery.price_format.1.4.js"></script>
		
		<script type="text/javascript">var defaultRel = 'account'; var defaultPage = 'validasi-mhs.php';</script>
		<script type="text/javascript" src="../../js/cybercampus.ajax-1.0.js"></script>
		{literal}
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-41992721-1', 'unair.ac.id');
		  ga('send', 'pageview');

		</script>
		{/literal}
	</body>
</html>