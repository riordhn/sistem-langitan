<div class="center_title_bar">Validasi Mahasiswa Asing Reguler</div>
{if isset($mhs)}
<form action="validasi-mhs.php" method="post">
    <table>
        <tr>
        	<th>NO</th>
            <th>FOTO</th>
            <th>NAMA</th>
            <th>NIM</th>
            <th>FAKULTAS</th>
            <th>PROGRAM STUDI</th>
			<th>STATUS AKADEMIK</th>
			<th>NEGARA</th>
            <th><input type="checkbox" name="cek_all"  id="cek_all"/></th>
        </tr>
        {if $mhs==null}
            <tr>
                <td colspan="8" class="center">Hasil Pencarian Tidak Ditemukan</td>
            </tr>
        {else}
        	{$no = 1}
            {foreach $mhs as $data}
                <tr>
                	<td>{$no++}</td>
                    <td><img src="/foto_mhs/{$data.NIM_MHS}.JPG" width="100" height="140"/></td>
                    <td>{$data.NM_PENGGUNA}</td>
                    <td>{$data.NIM_MHS}</td>
                    <td>{$data.NM_FAKULTAS}</td>
                    <td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
					<td>{$data.NM_STATUS_PENGGUNA}</td>
					<td>
                    	<select name="negara{$no}">
                        	{foreach $negara as $neg}
                        	<option value="{$neg.ID_NEGARA}"  {if $neg.ID_NEGARA == $data.ID_NEGARA} selected="selected"{/if}>{$neg.NM_NEGARA}</option>
                            {/foreach}
                        </select>
                    </td>
                    <td>
                    	<input type="checkbox" name="cek{$no}" value="1" class="cek1"  />
                    	<input type="hidden" value="$data.ID_MHS" name="id_mhs{$no}" />
                        <input type="hidden" name="no" value="{$no}" />
                    </td>
                </tr>
            {/foreach}
        {/if}
        <tr><td colspan="10" style="text-align:center"><input type="submit" name="simpan" value="SIMPAN" /></td></tr>
    </table>
 </form>   
    
    
{literal}
    <script type="text/javascript">
			
		$("#cek_all").click(function(){
				var checked_status = this.checked;
				$(".cek1").each(function()
				{
					this.checked = checked_status;
				});
		});		
		
    </script>
{/literal}
{/if}