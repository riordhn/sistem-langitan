<div class="center_title_bar">Entry Biodata Mahasiswa Asing Reguler</div>
   {literal}
    <style>
        .span_button{
            padding: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            background-color: #009933;
            color: #ffffff;
            cursor: pointer;
        }
    </style>
{/literal}  
{if isset($mhs)}
    <table>
        <tr>
            <th>NAMA</th>
            <th>NIM</th>
            <th>FAKULTAS</th>
            <th>PROGRAM STUDI</th>
			<th>STATUS AKADEMIK</th>
			<th>NEGARA</th>
            <th></th>
        </tr>
        {if $mhs==null}
            <tr>
                <td colspan="8" class="center">Hasil Pencarian Tidak Ditemukan</td>
            </tr>
        {else}
            {foreach $mhs as $data}
                <tr>
                    <td>{$data.NM_PENGGUNA}</td>
                    <td>{$data.NIM_MHS}</td>
                    <td>{$data.NM_FAKULTAS}</td>
                    <td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
					<td>{$data.NM_STATUS_PENGGUNA}</td>
					<td>{$data.NM_NEGARA}</td>
                    <td><a href="entry-mhs.php?menu=entry&y={$data.NIM_MHS}">Entry</a></td>
                </tr>
            {/foreach}
        {/if}
    </table>
	<a href="cari-mhs.php" class="button">Kembali</a>
{else if $smarty.get.menu == 'entry'}    

<table style="width: 80%">
        <tr>
            <td colspan="8" style="border: none;padding: 5px 0px 15px 0px;">    
                <span class="span_button" onclick="history.back()">Kembali</span>
            </td>
        </tr>
        <tr>
            <th colspan="2" class="center">BIODATA MAHASISWA</th>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <img src="/foto_mhs/{$biodata_mahasiswa.NIM_MHS}.JPG" width="180" height="230"/>
            </td>
        </tr>
        <tr>
            <td>NAMA</td>
            <td>{$biodata_mahasiswa.NM_PENGGUNA}</td>
        </tr>
        <tr>
            <td>NIM</td>
            <td>{$biodata_mahasiswa.NIM_MHS}</td>
        </tr>
        <tr>
            <td>JENJANG</td>
            <td>{$biodata_mahasiswa.NM_JENJANG}</td>
        </tr>
        <tr>
            <td>FAKULTAS</td>
            <td>{$biodata_mahasiswa.NM_FAKULTAS|upper}</td>
        </tr>
        <tr>
            <td>PROGRAM STUDI</td>
            <td>{$biodata_mahasiswa.NM_PROGRAM_STUDI}</td>
        </tr>
        <tr>
            <td>ASAL NEGARA</td>
            <td>
            			<select name="negara{$no}">
                        	{foreach $negara as $neg}
                        	<option value="{$neg.ID_NEGARA}"  {if $neg.ID_NEGARA == $biodata_mahasiswa.ID_NEGARA} selected="selected"{/if}>{$neg.NM_NEGARA}</option>
                            {/foreach}
                        </select>
             </td>
        </tr>
        <tr>
            <td>TELP MHS</td>
            <td><input type="text" value="{$biodata_mahasiswa.MOBILE_MHS}" name="telp" /></td>
        </tr>
        <tr>
            <td>KELAMIN</td>
            <td>
            	<input type="radio" value="1" {if $biodata_mahasiswa.KELAMIN_PENGGUNA==1} checked="checked"{/if} />Laki-laki
                <input type="radio" value="2" {if $biodata_mahasiswa.KELAMIN_PENGGUNA==2} checked="checked"{/if} />Perempuan
            </td>
        </tr>
        <tr>
            <td>ALAMAT MAHASISWA</td>
            <td><textarea name="alamat_mhs" cols="50">{$biodata_mahasiswa.ALAMAT_MHS}</textarea></td>
        </tr>
        <tr>
            <td>NAMA AYAH</td>
            <td><input type="text" name="nm_ayah" value="{$biodata_mahasiswa.NM_AYAH_MHS}" /></td>
        </tr>
        <tr>
            <td>ALAMAT AYAH</td>
            <td><textarea name="alamat_ayah" cols="50">{$biodata_mahasiswa.ALAMAT_AYAH_MHS}</textarea></td>
        </tr>
		<tr>
            <td>TELP AYAH</td>
            <td><input type="text" name="telp_ayah" value="{$biodata_mahasiswa.TELP_AYAH}" /></td>
        </tr>
		<tr>
            <td>NAMA IBU</td>
            <td><input type="text" name="nm_ibu" value="{$biodata_mahasiswa.NM_IBU_MHS}" /></td>
        </tr>
        <tr>
            <td>ALAMAT IBU</td>
            <td><textarea name="alamat_ibu" cols="50">{$biodata_mahasiswa.ALAMAT_IBU_MHS}</textarea></td>
        </tr>
		<tr>
            <td>TELP IBU</td>
            <td><input type="text" name="telp_ibu" value="{$biodata_mahasiswa.TELP_IBU}" /></td>
        </tr>
    </table>

{else}    
<form action="entry-mhs.php" method="get">
        <table>
            <tr>
                <td>NAMA / NIM MAHASISWA</td>
                <td><input type="text" name="x"/></td>
				<td><input type="submit" value="Cari" /></td>
            </tr>
        </table>
    </form>
{/if}