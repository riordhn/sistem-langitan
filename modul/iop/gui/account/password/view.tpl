<div class="center_title_bar">Ganti Password</div>

{if $changed}
    <h2>Password Berhasil Diganti</h2>
{else}
<form action="account-password.php" method="post">
<input type="hidden" name="mode" value="change-password" />
<table>
    <tr>
        <th colspan="2">Password</th>
    </tr>
    <tr>
        <td>Password Lama</td>
        <td><input type="password" name="old_password" /></td>
    </tr>
    <tr>
        <td>Password Baru</td>
        <td><input type="password" name="new_password" /></td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>
{/if}

<script>
    function GantiRole()
    {
        if (confirm('Apakah Anda yakin akan ganti role ?') == true)
        {
            var id_role = $('select[name="id_role"]').val();
            var id_pengguna = $('input[name="id_pengguna"]').val();
            var path = (id_fakultas == 8) ? '../' : '';

            $.ajax({
                type: 'POST',
                url: path + 'account-password.php',
                data: 'mode=change-role&id_pengguna='+id_pengguna+'&id_role='+id_role,
                success: function(data) {
                    if (data == 1)
                    {
                        alert('Anda sudah ganti role. Akun anda akan di logout.');
                        window.location = path + '../../logout.php';
                    }
                    else
                    {
                        alert('Gagal ganti role');
                    }
                }
            });
        }
    }
</script>

<table>
	<tr>
		<th colspan="2">Multi Role</th>
	</tr>
	<tr>
		<td>Role</td>
		<td>
			<input type="hidden" name="id_pengguna" value="{$id_pengguna}" />
            <select name="id_role">
            {foreach $role_set as $r}
                <option value="{$r.ID_ROLE}" {if $r.ID_ROLE == $id_role}selected="selected"{/if}>{$r.NM_ROLE}</option>
            {/foreach}
            </select>
            <button onclick="GantiRole(); return false;">Ganti Role</button>
		</td>
	</tr>
</table>