<?php

include('config.php');
include 'class/kesehatan.class.php';

$sehat = new kesehatan($db);

if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        if (isset($_POST)) {
            if (post('mode') == 'ubah') {
                $sehat->update_jadwal_kesehatan_cmhs(post('id_c_mhs'), post('jadwal_lama'), post('jadwal_baru'));
            }
        }
        $smarty->assign('data_jadwal_cmhs', $sehat->get_data_jadwal_cmhs(get('no_ujian')));
    }
}
$smarty->assign('data_jadwal', $sehat->load_jadwal_kesehatan());
$smarty->display("kesehatan-ubah-jadwal.tpl");
?>
