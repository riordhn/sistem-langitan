<?php

include 'config.php';
include '../keuangan/class/list_data.class.php';
include '../ppmb/class/Penerimaan.class.php';
include 'class/kesehatan.class.php';


$list = new list_data($db);
$penerimaan = new Penerimaan($db);
$sehat = new kesehatan($db);

if (isset($_GET)) {
    if (get('mode') == 'laporan') {
        $smarty->assign('data_laporan', $sehat->load_hasil_tes_kesehatan_cmhs(get('jadwal'), get('hasil'), get('penerimaan')));
    }
}

$smarty->assign('data_jadwal_kesehatan', $sehat->load_jadwal_kesehatan('',get('penerimaan')));
$smarty->assign('penerimaan_set', $penerimaan->GetAllForView());
$smarty->assign('data_prodi', $list->load_list_prodi(get('fakultas')));
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->display('kesehatan-laporan-hasil.tpl');
?>
