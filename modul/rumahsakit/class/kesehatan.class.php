
<?php

class kesehatan {

    public $db;

    function __construct($database) {
        $this->db = $database;
    }

    // Master Ruang Kesehatan

    function load_ruang_kesehatan($id_ruangan = '') {
        if (empty($id_ruangan)) {
            $data_ruang_kesehatan = $this->db->QueryToArray("SELECT * FROM RUANG_KESEHATAN ORDER BY ID_RUANG_KESEHATAN");
        } else {
            $this->db->Query("SELECT * FROM RUANG_KESEHATAN WHERE ID_RUANG_KESEHATAN='{$id_ruangan}'");
            $data_ruang_kesehatan = $this->db->FetchAssoc();
        }
        return $data_ruang_kesehatan;
    }

    function update_ruang_kesehatan($id_ruang, $nm_ruang, $kapasitas) {
        $this->db->Query("UPDATE RUANG_KESEHATAN SET NM_RUANG='$nm_ruang',KAPASITAS='$kapasitas' WHERE ID_RUANG_KESEHATAN='$id_ruang'");
    }

    function insert_ruang_kesehatan($nm_ruang, $kapasitas) {
        $this->db->Query("INSERT INTO RUANG_KESEHATAN (NM_RUANG,KAPASITAS) VALUES ('$nm_ruang','$kapasitas')");
    }

    function delete_ruang_kesehatan($id_ruang) {
        $this->db->Query("DELETE RUANG_KESEHATAN WHERE ID_RUANG_KESEHATAN ='$id_ruang'");
    }

    // Master Jadwal Kesehatan

    function load_jadwal_kesehatan($tanggal = null, $penerimaan = null) {
        if ($tanggal == '' && $penerimaan == '') {
            $data_jadwal_kesehatan = $this->db->QueryToArray("
                SELECT JK.*,PEN.*,JAL.NM_JALUR FROM JADWAL_KESEHATAN JK
                LEFT JOIN PENERIMAAN PEN ON PEN.ID_PENERIMAAN=JK.ID_PENERIMAAN
                LEFT JOIN JALUR JAL ON JAL.ID_JALUR=PEN.ID_JALUR
                WHERE TO_CHAR(TGL_TEST,'YYYY')= TO_CHAR(SYSDATE, 'YYYY')
                ORDER BY TGL_TEST,JAM_MULAI");
        } else if ($penerimaan != '') {
            $data_jadwal_kesehatan = $this->db->QueryToArray("
                SELECT JK.*,PEN.*,JAL.NM_JALUR FROM JADWAL_KESEHATAN JK
                LEFT JOIN PENERIMAAN PEN ON PEN.ID_PENERIMAAN=JK.ID_PENERIMAAN
                LEFT JOIN JALUR JAL ON JAL.ID_JALUR=PEN.ID_JALUR
                WHERE PEN.ID_PENERIMAAN='{$penerimaan}'
                ORDER BY TGL_TEST,JAM_MULAI
                ");
        } else {
            $data_jadwal_kesehatan = $this->db->QueryToArray("
                SELECT JK.*,PEN.*,JAL.NM_JALUR FROM JADWAL_KESEHATAN JK
                LEFT JOIN PENERIMAAN PEN ON PEN.ID_PENERIMAAN=JK.ID_PENERIMAAN
                LEFT JOIN JALUR JAL ON JAL.ID_JALUR=PEN.ID_JALUR
                WHERE TGL_TEST ='{$tanggal}'
                ORDER BY TGL_TEST,JAM_MULAI
                ");
        }
        return $data_jadwal_kesehatan;
    }

    function get_jadwal_kesehatan($id) {
        $data_jadwal_kesehatan = array();
        $this->db->Query("SELECT * FROM JADWAL_KESEHATAN WHERE ID_JADWAL_KESEHATAN='$id' ORDER BY ID_JADWAL_KESEHATAN");
        $data_jadwal_kesehatan = $this->db->FetchArray();
        return $data_jadwal_kesehatan;
    }

    function update_jadwal_kesehatan($id_jadwal, $penerimaan, $kelompok_jam, $tanggal, $jam_mulai, $menit_mulai, $jam_selesai, $menit_selesai, $kapasitas) {
        $this->db->Query("
            UPDATE JADWAL_KESEHATAN 
            SET 
                ID_PENERIMAAN='{$penerimaan}',
                KELOMPOK_JAM='{$kelompok_jam}',
                TGL_TEST='{$tanggal}',
                JAM_MULAI='{$jam_mulai}',
                MENIT_MULAI='{$menit_mulai}',
                JAM_SELESAI='{$jam_selesai}',
                MENIT_SELESAI='{$menit_selesai}',
                KAPASITAS='{$kapasitas}' 
            WHERE ID_JADWAL_KESEHATAN='{$id_jadwal}'");
    }

    function insert_jadwal_kesehatan($penerimaan, $kelompok_jam, $tanggal, $jam_mulai, $menit_mulai, $jam_selesai, $menit_selesai, $kapasitas) {
        $this->db->Query("
            INSERT INTO JADWAL_KESEHATAN 
                (ID_PENERIMAAN,KELOMPOK_JAM,TGL_TEST,JAM_MULAI,MENIT_MULAI,JAM_SELESAI,MENIT_SELESAI,KAPASITAS) 
            VALUES 
                ('{$penerimaan}','{$kelompok_jam}','{$tanggal}','{$jam_mulai}','{$menit_mulai}','{$jam_selesai}','{$menit_selesai}','{$kapasitas}')");
    }

    function delete_jadwal_kesehatan($id_jadwal) {
        $this->db->Query("DELETE JADWAL_KESEHATAN WHERE ID_JADWAL_KESEHATAN ='{$id_jadwal}'");
    }

    // Menu Cetak Form

    function load_calon_mahasiswa_by_kelompok($tanggal, $kelompok) {
        return $this->db->QueryToArray("
                SELECT CMB.NM_C_MHS,CMB.ID_C_MHS,CMB.NO_UJIAN,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,F.NM_FAKULTAS
                FROM CALON_MAHASISWA_BARU CMB
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
                JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                JOIN CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                JOIN JADWAL_KESEHATAN JK ON JK.ID_JADWAL_KESEHATAN=CMD.ID_JADWAL_KESEHATAN
                LEFT JOIN URUTAN_KESEHATAN UK ON UK.ID_C_MHS=CMB.ID_C_MHS
                WHERE JK.TGL_TEST='{$tanggal}' AND JK.ID_JADWAL_KESEHATAN='{$kelompok}'
                ORDER BY UK.URUTAN
                ");
    }

    function get_data_cetak_kesehatan_cmhs($id_c_mhs) {
        $this->db->Query("
                SELECT CMB.NM_C_MHS,CMB.NO_UJIAN,CMB.ALAMAT,CMB.TELP,JAL.NM_JALUR,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,F.NM_FAKULTAS,
                JK.ID_JADWAL_KESEHATAN,JK.KELOMPOK_JAM,JK.TGL_TEST,JK.JAM_MULAI,JK.MENIT_MULAI,JK.JAM_SELESAI,JK.MENIT_SELESAI 
                FROM CALON_MAHASISWA_BARU CMB
                JOIN JALUR JAL ON JAL.ID_JALUR=CMB.ID_JALUR
                JOIN CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
                JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                JOIN JADWAL_KESEHATAN JK ON JK.ID_JADWAL_KESEHATAN=CMD.ID_JADWAL_KESEHATAN
                WHERE CMB.ID_C_MHS='{$id_c_mhs}'");
        return $this->db->FetchAssoc();
    }

    // Menu Edit Nilai

    function get_data_kesehatan_cmhs($no_ujian) {
        $this->db->Query("
            SELECT CMB.NO_UJIAN,CMB.NM_C_MHS,CMD.*,CMB.TGL_VERIFIKASI_KESEHATAN,CMB.ID_VERIFIKATOR_KESEHATAN
            FROM CALON_MAHASISWA_BARU CMB
            JOIN CALON_MAHASISWA_DATA CMD ON CMB.ID_C_MHS=CMD.ID_C_MHS
            WHERE CMB.NO_UJIAN='{$no_ujian}' AND CMD.ID_JADWAL_KESEHATAN IS NOT NULL
            ");
        return $this->db->FetchAssoc();
    }

    function load_data_kesehatan_cmhs($jadwal) {
        return $this->db->QueryToArray("
            SELECT CMB.NO_UJIAN,CMB.NM_C_MHS,CMD.*,CMB.TGL_VERIFIKASI_KESEHATAN
            FROM CALON_MAHASISWA_BARU CMB
            JOIN CALON_MAHASISWA_DATA CMD ON CMB.ID_C_MHS=CMD.ID_C_MHS
            LEFT JOIN URUTAN_KESEHATAN UK ON UK.ID_C_MHS=CMD.ID_C_MHS 
            WHERE CMD.ID_JADWAL_KESEHATAN='{$jadwal}'
            ORDER BY UK.URUTAN");
    }

    function update_data_kesehatan($id_c_mhs, $kesimpulan_mata, $kesimpulan_tht, $kesimpulan_akhir, $catatan) {
        $this->db->Query("
        UPDATE CALON_MAHASISWA_DATA 
        SET 
            KESEHATAN_KESIMPULAN_MATA='{$kesimpulan_mata}',
            KESEHATAN_KESIMPULAN_THT='{$kesimpulan_tht}',
            KESEHATAN_KESIMPULAN_AKHIR='{$kesimpulan_akhir}',
            KESEHATAN_CATATAN='{$catatan}' 
        WHERE ID_C_MHS = '{$id_c_mhs}'");
    }

    function update_verifikasi_kesehatan($id_c_mhs, $id_pengguna) {
        $this->db->Query("
        UPDATE CALON_MAHASISWA_BARU
        SET
            TGL_VERIFIKASI_KESEHATAN=CURRENT_TIMESTAMP,
            ID_VERIFIKATOR_KESEHATAN='{$id_pengguna}'
        WHERE ID_C_MHS='{$id_c_mhs}'");
    }

    function insert_log_nilai_kesehatan($id_c_mhs, $id_pengguna, $kesimpulan_mata, $kesimpulan_tht, $kesimpulan_akhir) {
        $address = $_SERVER['REMOTE_ADDR'];
        $this->db->Query("
            INSERT INTO LOG_TES_KESEHATAN
                (ID_C_MHS,ID_PENGGUNA,NILAI_THT,NILAI_MATA,NILAI_AKHIR,REMOTE_ADDR)
            VALUES
                ('{$id_c_mhs}','{$id_pengguna}','{$kesimpulan_tht}','{$kesimpulan_mata}','{$kesimpulan_akhir}','{$address}')");
    }

    function load_tanggal_jadwal_kesehatan() {
        return $this->db->QueryToArray("
            SELECT DISTINCT(JK.TGL_TEST) TGL_TEST 
            FROM JADWAL_KESEHATAN JK 
            WHERE TO_CHAR(TGL_TEST,'YYYY')= TO_CHAR(SYSDATE, 'YYYY')
            ORDER BY TGL_TEST");
    }

    // Menu Laporan Verifikasi 

    function load_verifikasi_data($fakultas, $penerimaan) {
        if ($fakultas != '') {
            return $this->db->QueryToArray("
                SELECT ID_PROGRAM_STUDI,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                    ) DITERIMA,
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI AND CMB.TGL_REGMABA IS NOT NULL
                    ) REGMABA,
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        JOIN CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI AND CMD.ID_JADWAL_KESEHATAN IS NOT NULL
                    ) SUDAH_JADWAL,
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        JOIN CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI AND CMB.TGL_VERIFIKASI_PENDIDIKAN IS NOT NULL
                    ) SUDAH_VERIFIKASI,
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI AND CMB.TGL_VERIFIKASI_KESEHATAN IS NOT NULL
                    ) SUDAH_TES
                FROM AUCC.PROGRAM_STUDI PS 
                JOIN AUCC.JENJANG J ON PS.ID_JENJANG=J.ID_JENJANG
                WHERE PS.ID_FAKULTAS='{$fakultas}'
                GROUP BY ID_PROGRAM_STUDI,J.NM_JENJANG,PS.NM_PROGRAM_STUDI");
        } else {
            return $this->db->QueryToArray("
                SELECT F.ID_FAKULTAS,F.NM_FAKULTAS,
                    (
                        SELECT COUNT(*) FROM CALON_MAHASISWA_BARU CMB
                        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND PS.ID_FAKULTAS=F.ID_FAKULTAS
                    ) DITERIMA,
                    (
                        SELECT COUNT(*) FROM CALON_MAHASISWA_BARU CMB
                        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND PS.ID_FAKULTAS=F.ID_FAKULTAS AND CMB.TGL_REGMABA IS NOT NULL
                    ) REGMABA,
                    (
                        SELECT COUNT(*) FROM CALON_MAHASISWA_BARU CMB
                        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND PS.ID_FAKULTAS=F.ID_FAKULTAS AND CMB.TGL_VERIFIKASI_PENDIDIKAN IS NOT NULL
                    ) SUDAH_VERIFIKASI,
                    (
                        SELECT COUNT(*) FROM CALON_MAHASISWA_BARU CMB
                        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND PS.ID_FAKULTAS=F.ID_FAKULTAS AND CMB.TGL_VERIFIKASI_KESEHATAN IS NOT NULL
                    ) SUDAH_TES,
                    (
                        SELECT COUNT(*) FROM CALON_MAHASISWA_BARU CMB
                        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                        JOIN CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND PS.ID_FAKULTAS=F.ID_FAKULTAS AND CMD.ID_JADWAL_KESEHATAN IS NOT NULL
                    ) SUDAH_JADWAL
                FROM FAKULTAS F
                GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS");
        }
    }

    function load_detail_verifikasi($fakultas, $status, $penerimaan) {
        $f_fakultas = $fakultas != '' ? "AND PS.ID_FAKULTAS='{$fakultas}'" : "";
        if ($status == 1) {
            $filter = "AND CMB.TGL_DITERIMA IS NOT NULL";
        } else if ($status == 2) {
            $filter = "AND CMB.TGL_REGMABA IS NOT NULL";
        } else if ($status == 3) {
            $filter = "AND CMB.TGL_VERIFIKASI_PENDIDIKAN IS NOT NULL";
        } else if ($status == 4) {
            $filter = "AND CMD.ID_JADWAL_KESEHATAN IS NOT NULL";
        } else if ($status == 5) {
            $filter = "AND CMB.TGL_VERIFIKASI_KESEHATAN IS NOT NULL";
        }
        return $this->db->QueryToArray("
            SELECT CMB.ID_C_MHS,CMB.NM_C_MHS,CMB.NO_UJIAN,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,CMB.TGL_VERIFIKASI_PENDIDIKAN,CMB.TGL_PRINT_JADWAL,JK.TGL_TEST,TGL_VERIFIKASI_KESEHATAN
            FROM CALON_MAHASISWA_BARU CMB
            JOIN PROGRAM_STUDI PS ON CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
            LEFT JOIN JADWAL_KESEHATAN JK ON JK.ID_JADWAL_KESEHATAN=CMD.ID_JADWAL_KESEHATAN
            WHERE CMB.ID_PENERIMAAN='{$penerimaan}' {$f_fakultas} {$filter}
            ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI,CMB.NO_UJIAN,CMB.NM_C_MHS
            ");
    }

    //menu ubah jadwal

    function get_data_jadwal_cmhs($no_ujian) {
        $this->db->Query("
            SELECT CMB.*,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JT.ID_JADWAL_TOEFL,JT.JAM_MULAI JAM_ELPT,JT.MENIT_MULAI MENIT_ELPT,JT.TGL_TEST TGL_ELPT,JK.*,RT.NM_RUANG
            FROM CALON_MAHASISWA_BARU CMB
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
            JOIN JADWAL_TOEFL JT ON JT.ID_JADWAL_TOEFL=CMD.ID_JADWAL_TOEFL
            JOIN RUANG_TOEFL RT ON RT.ID_RUANG_TOEFL=JT.ID_RUANG_TOEFL
            JOIN JADWAL_KESEHATAN JK ON JK.ID_JADWAL_KESEHATAN=CMD.ID_JADWAL_KESEHATAN
            WHERE CMB.NO_UJIAN='{$no_ujian}'
            ");
        return $this->db->FetchAssoc();
    }

    function update_jadwal_kesehatan_cmhs($id_c_mhs, $jadwal_lama, $jadwal_baru) {
        $this->db->Query("
            UPDATE JADWAL_KESEHATAN
            SET
                TERISI=TERISI-1
            WHERE ID_JADWAL_KESEHATAN='{$jadwal_lama}'");
        $this->db->Query("
            UPDATE CALON_MAHASISWA_DATA
            SET
                ID_JADWAL_KESEHATAN='{$jadwal_baru}'
            WHERE ID_C_MHS='{$id_c_mhs}'");
        $this->db->Query("
            UPDATE JADWAL_KESEHATAN
            SET
                TERISI=TERISI+1
            WHERE ID_JADWAL_KESEHATAN='{$jadwal_baru}'");
		$this->db->Query("
            UPDATE URUTAN_KESEHATAN
            SET
                ID_JADWAL_KESEHATAN='{$jadwal_baru}'
            WHERE ID_C_MHS='{$id_c_mhs}'");
    }

    function load_hasil_tes_kesehatan_cmhs($jadwal, $hasil, $penerimaan) {
        if ($hasil == '' && $jadwal == '') {
            $query = "
            SELECT CMB.NO_UJIAN,CMB.NM_C_MHS,CMD.*,CMB.TGL_VERIFIKASI_KESEHATAN,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JK.*
            FROM CALON_MAHASISWA_BARU CMB
            JOIN CALON_MAHASISWA_DATA CMD ON CMB.ID_C_MHS=CMD.ID_C_MHS
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
            JOIN JADWAL_KESEHATAN JK ON CMD.ID_JADWAL_KESEHATAN=JK.ID_JADWAL_KESEHATAN
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            LEFT JOIN URUTAN_KESEHATAN UK ON UK.ID_C_MHS=CMD.ID_C_MHS 
            WHERE CMB.ID_PENERIMAAN='{$penerimaan}' AND CMB.TGL_VERIFIKASI_KESEHATAN IS NOT NULL
            ORDER BY JK.TGL_TEST,JK.KELOMPOK_JAM,UK.URUTAN";
        } else if ($jadwal == '') {
            $query = "
            SELECT CMB.NO_UJIAN,CMB.NM_C_MHS,CMD.*,CMB.TGL_VERIFIKASI_KESEHATAN,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JK.*
            FROM CALON_MAHASISWA_BARU CMB
            JOIN CALON_MAHASISWA_DATA CMD ON CMB.ID_C_MHS=CMD.ID_C_MHS
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
            JOIN JADWAL_KESEHATAN JK ON CMD.ID_JADWAL_KESEHATAN=JK.ID_JADWAL_KESEHATAN      
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            LEFT JOIN URUTAN_KESEHATAN UK ON UK.ID_C_MHS=CMD.ID_C_MHS 
            WHERE CMD.KESEHATAN_KESIMPULAN_AKHIR='{$hasil}' AND CMB.ID_PENERIMAAN='{$penerimaan}' AND CMB.TGL_VERIFIKASI_KESEHATAN IS NOT NULL
            ORDER BY JK.TGL_TEST,JK.KELOMPOK_JAM,UK.URUTAN";
        } else if ($hasil == '') {
            $query = "
            SELECT CMB.NO_UJIAN,CMB.NM_C_MHS,CMD.*,CMB.TGL_VERIFIKASI_KESEHATAN,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JK.*
            FROM CALON_MAHASISWA_BARU CMB
            JOIN CALON_MAHASISWA_DATA CMD ON CMB.ID_C_MHS=CMD.ID_C_MHS
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
            JOIN JADWAL_KESEHATAN JK ON CMD.ID_JADWAL_KESEHATAN=JK.ID_JADWAL_KESEHATAN      
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            LEFT JOIN URUTAN_KESEHATAN UK ON UK.ID_C_MHS=CMD.ID_C_MHS 
            WHERE CMD.ID_JADWAL_KESEHATAN='{$jadwal}' AND CMB.ID_PENERIMAAN='{$penerimaan}' AND CMB.TGL_VERIFIKASI_KESEHATAN IS NOT NULL
            ORDER BY JK.TGL_TEST,JK.KELOMPOK_JAM,UK.URUTAN";
        } else {
            $query = "
            SELECT CMB.NO_UJIAN,CMB.NM_C_MHS,CMD.*,CMB.TGL_VERIFIKASI_KESEHATAN,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JK.*
            FROM CALON_MAHASISWA_BARU CMB
            JOIN CALON_MAHASISWA_DATA CMD ON CMB.ID_C_MHS=CMD.ID_C_MHS
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
            JOIN JADWAL_KESEHATAN JK ON CMD.ID_JADWAL_KESEHATAN=JK.ID_JADWAL_KESEHATAN      
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            LEFT JOIN URUTAN_KESEHATAN UK ON UK.ID_C_MHS=CMD.ID_C_MHS 
            WHERE CMD.ID_JADWAL_KESEHATAN='{$jadwal}' AND CMD.KESEHATAN_KESIMPULAN_AKHIR='{$hasil}' AND CMB.ID_PENERIMAAN='{$penerimaan}' AND CMB.TGL_VERIFIKASI_KESEHATAN IS NOT NULL
            ORDER BY JK.TGL_TEST,JK.KELOMPOK_JAM, UK.URUTAN";
        }
        return $this->db->QueryToArray($query);
    }

}

?>
