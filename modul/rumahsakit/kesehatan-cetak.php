<?php

include'config.php';
include 'class/kesehatan.class.php';

$kesehatan = new kesehatan($db);

if (isset($_GET)) {
    if (get('tanggal') != null && get('kelompok') != null) {
        $smarty->assign('data_kelompok',$kesehatan->load_jadwal_kesehatan(get('tanggal')));
        $smarty->assign('data_cmhs', $kesehatan->load_calon_mahasiswa_by_kelompok(get('tanggal'), get('kelompok')));
    }
}

$smarty->assign("data_tanggal", $kesehatan->load_tanggal_jadwal_kesehatan());
$smarty->display("kesehatan-cetak.tpl");
?>
