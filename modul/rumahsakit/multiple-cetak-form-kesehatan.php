<?php

include'config.php';

include '../../tcpdf/config/lang/ind.php';
include '../../tcpdf/tcpdf.php';

$jadwal = $_GET['jadwal'];
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$data_cmhs = $db->QueryToArray("SELECT CMB.ID_C_MHS,CMB.NM_C_MHS,CMB.NO_UJIAN,CMB.ALAMAT,CMB.TELP,JAL.NM_JALUR,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,F.NM_FAKULTAS,
                JK.ID_JADWAL_KESEHATAN,JK.KELOMPOK_JAM,JK.TGL_TEST,JK.JAM_MULAI,JK.MENIT_MULAI,JK.JAM_SELESAI,JK.MENIT_SELESAI,UK.URUTAN,CMB.JENIS_KELAMIN
                FROM CALON_MAHASISWA_BARU CMB
                JOIN JALUR JAL ON JAL.ID_JALUR=CMB.ID_JALUR
                JOIN CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
                JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                JOIN JADWAL_KESEHATAN JK ON JK.ID_JADWAL_KESEHATAN=CMD.ID_JADWAL_KESEHATAN
                JOIN URUTAN_KESEHATAN UK ON UK.ID_C_MHS=CMB.ID_C_MHS
                WHERE CMD.ID_JADWAL_KESEHATAN='{$jadwal}'
                ORDER BY UK.URUTAN");



$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
foreach ($data_cmhs as $d) {
    $html = <<<EOF
<style>
    div { margin-top: 0pt; }
    .small {font-size:9pt}
    .header { font-size: 11pt; font-family: times; font-weight: bold; text-align:center;}
    .address { font-size: 10pt; font-family: serif; margin-top: 0px ;text-align:center; }
    .urutan {font-size: 25pt; font-family: Trebuchet MS;font-weight:bolder}
    td { font-size: 8pt; }
    td.paraf {font-size: 6pt;vertical-align:bottom;text-align:center;} 
</style>
<table width="100%" border="0">
    <tr>
        <td width="10%" align="right"><img src="../../img/unair.gif" width="50px" height="50px"/></td>
        <td width="80%" align="center">
            <span class="header">UNIVERSITAS AIRLANGGA<br/>RUMAH SAKIT</span><br/>
            <span class="address">Kampus C Mulyorejo Surabaya 60115,Telp.0315916291,Fax.031-5916291</span><br/>
            <span class="address">Website : www.rumahsakit.unair.ac.id,email:rsua@unair.ac.id</span>
        </td>
         <td  width="10%" border="1" align="center">
            <br/><br/>
            <span class="urutan">{urutan}</span>
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
</table>
<table width="100%">
    <tr>
        <td border="1">
            <table>
                <tr>
                    <td colspan="4" align="center"><h3>PEMERIKSAAN UJI KETUNAAN CALON MAHASISWA</h3></td>
                </tr>
                <tr>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td width="20%"><b>PROGRAM STUDI</b></td>
                    <td width="35%"><b>: {program_studi}</b></td>
                    <td width="20%"><b>Di periksa tgl</b></td>
                    <td width="25%"><b>: {tanggal}</b></td>
                </tr>
                <tr>
                    <td><b>FAKULTAS</b></td>
                    <td><b>: {fakultas}</b></td>
                    <td><b>Jam</b></td>
                    <td><b>: {jam}</b></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Diterima Di Jalur</td>
                    <td>: {jalur}</td>
                    <td>Kelompok Test</td>
                    <td>: {kelompok_jam}</td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td>: {nama}</td>
                    <td>Jenis Kelamin</td>
                    <td>: {kelamin}</td>
                </tr>
                <tr>
                    <td>No Ujian</td>
                    <td>: {no_ujian}</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="20%">Alamat</td>
                    <td width="80%" colspan=3>: {alamat}</td>
                </tr>
                <tr>
                    <td>Alamat di Surabaya</td>
                    <td>: </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Telp</td>
                    <td>: {telp}</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan=4></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table border="0" cellpadding="2" width="100%">
    <tr>
        <td colspan="4"></td>
    </tr>
    <tr>
        <td bgcolor="#000" color="#fff">PEMERIKSAAN TELINGA</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td width="35%">- Membran timpani kanan intak :</td>
        <td colspan="2" width="45%">
            <table width="90%">
                <tr>
                    <td border="1"></td>
                    <td align="center">YA</td>
                    <td border="1"></td>
                    <td align="center">TIDAK</td>                    
                </tr>
            </table>
        </td>
        <td width="20%" rowspan="2">
            <table width="50%">
                <tr>
                    <td border="1" valign="bottom" height="20px"></td>                    
                </tr>
                <tr>
                    <td class="paraf" border="0">paraf & nama</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="35%">- Membran timpani kiri intak :</td>
        <td colspan="2" width="45%">
            <table width="90%">
                <tr>
                    <td border="1"></td>
                    <td align="center">YA</td>
                    <td border="1"></td>
                    <td align="center">TIDAK</td>
                    </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="35%">- Tes suara bisik kanan :</td>
        <td colspan="2" width="45%">
            <table width="90%">
                <tr>
                    <td border="1"></td>
                    <td align="center">NORMAL</td>
                    <td border="1"></td>
                    <td align="center">TIDAK</td>
                    </tr>
            </table>
        </td>        
        <td width="20%" rowspan="2">
            <table width="50%">
                <tr>
                    <td border="1" valign="bottom" height="20px"></td>                    
                </tr>
                <tr>
                    <td class="paraf" border="0">paraf & nama</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="35%">- Tes suara bisik kiri :</td>
        <td colspan="2" width="45%">
            <table width="90%">
                <tr>
                    <td border="1"></td>
                    <td align="center">NORMAL</td>
                    <td border="1"></td>
                    <td align="center">TIDAK</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</table>
<table cellpadding="0" width="100%">
    <tr>
        <td width="20%"><b>Kesimpulan</b></td>
        <td width="5%">:</td>
        <td width="75%">Memenuhi syarat / tidak memenuhi syarat*</td>
    </tr>
    <tr>
        <td>Catatan</td>
        <td>:</td>
        <td> ________________________________________________________ </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td> ________________________________________________________ </td>
    </tr>
</table>
<table width="100%">
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td width="50%" align="center"></td>
        <td width="50%" align="center">Pemeriksa</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td width="50%" align="center"></td>
        <td width="50%" align="center"> (...............................) </td>
    </tr>
</table>
<hr>
<table border="0" cellpadding="2" width="100%">
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td bgcolor="#000" color="#fff">PEMERIKSAAN MATA</td>
        <td colspan="3"></td>
    </tr>
    <tr>
        <td width="40%">1. WARNA :</td>
        <td colspan="3" width="60%"></td>
    </tr>
    <tr>
        <td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;Buta Warna Total</td>
        <td colspan="2">
            <table width="80%">
                <tr>
                    <td align="center">YA</td>
                    <td></td>
                    <td align="center">TIDAK</td>
                </tr>
                <tr>
                    <td border="1" height="16px"></td>
                    <td></td>
                    <td border="1" height="16px"></td>
                </tr>
            </table>
        </td>
        <td width="20%" rowspan="2">
            <table width="50%">
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td border="1" valign="bottom" height="25px"></td>                    
                </tr>
                <tr>
                    <td class="paraf" border="0">paraf & nama</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;Buta Warna Parsial</td>
        <td colspan="2">
            <table width="80%">
                <tr>
                    <td align="center">YA</td>
                    <td></td>
                    <td align="center">TIDAK</td>
                </tr>
                <tr>
                    <td border="1" height="16px"></td>
                    <td></td>
                    <td border="1" height="16px"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="30%">2. VISUS / Tajam Penglihatan :<br/>&nbsp;&nbsp;&nbsp;&nbsp;(Tanpa / dengan kacamata)</td>
        <td colspan="2">
            <table width="80%">
                <tr>
                    <td align="center">OD</td>
                    <td></td>
                    <td align="center">OS</td>
                </tr>
                <tr>
                    <td border="1" height="16px"></td>
                    <td></td>
                    <td border="1" height="16px"></td>
                </tr>
            </table>
        </td>
        <td width="20%">
            <table width="50%">
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td border="1" valign="bottom" height="20px"></td>                    
                </tr>
                <tr>
                    <td class="paraf" border="0">paraf & nama</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>3. FUNDUS OKULI :</td>
        <td colspan="3"></td>
    </tr>
    <tr>
        <td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;Papil N II</td>
        <td colspan="2">
            <table width="80%">
                <tr>
                    <td align="center">NORMAL</td>
                    <td></td>
                    <td align="center">TIDAK</td>
                </tr>
                <tr>
                    <td border="1" height="16px"></td>
                    <td></td>
                    <td border="1" height="16px"></td>
                </tr>
            </table>
        </td>
        <td width="20%" rowspan="2">
            <table width="50%">
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td border="1" valign="bottom" height="25px"></td>                    
                </tr>
                <tr>
                    <td class="paraf" border="0">paraf & nama</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;Makula Fovea</td>
        <td colspan="2">
            <table width="80%">
                <tr>
                    <td align="center">NORMAL</td>
                    <td></td>
                    <td align="center">TIDAK</td>
                </tr>
                <tr>
                    <td border="1" height="16px"></td>
                    <td></td>
                    <td border="1" height="16px"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</table>
<table cellpadding="0" width="100%">
    <tr>
        <td width="20%"><b>Kesimpulan</b></td>
        <td width="5%">:</td>
        <td width="75%">Memenuhi syarat / tidak memenuhi syarat*</td>
    </tr>
    <tr>
        <td>Catatan</td>
        <td>:</td>
        <td> ________________________________________________________ </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td> ________________________________________________________ </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</table>
<table border="0" width="100%">
    <tr>
        <td width="15%" align="center"></td>
        <td width="35%" align="center"></td>
        <td width="50%" align="center">Pemeriksa</td>
    </tr>
        <tr>
        <td>Catatan</td>
        <td>: .................. </td>
        <td></td>
    </tr>
    <tr>
        <td>Tinggi Badan</td>
        <td>: .................. cm</td>
        <td></td>
    </tr>
    <tr>
        <td>Berat Badan</td>
        <td>: .................. kg</td>
        <td></td>
    </tr>
    <tr>
        <td width="15%" align="center"></td>
        <td width="35%" align="center"></td>
        <td width="50%" align="center"> (...............................) </td>
    </tr>
</table>
<table width="100%" border="0">
    <tr>
        <td width="40%"></td>
        <td width="20%" align="center">
            <table border="2" cellpadding="2">
                <tr>
                    <td height="27px"></td>
                    <td height="27px"></td>
                </tr>
                <tr>
                    <td>VALIDASI</td>
                    <td>ENTERED</td>
                </tr>
            </table>
        </td>
        <td width="40%"></td>
    </tr>
</table>
EOF;

    $html = str_replace('{program_studi}', $d['NM_JENJANG'] . ' ' . ucwords(strtolower($d['NM_PROGRAM_STUDI'])), $html);
    $html = str_replace('{fakultas}', strtoupper($d['NM_FAKULTAS']), $html);
    $html = str_replace('{tanggal}', $d['TGL_TEST'], $html);
    $html = str_replace('{jam}', '[' . str_pad($d['JAM_MULAI'], 2, '0', STR_PAD_LEFT) . ':' . str_pad($d['MENIT_MULAI'], 2, '0') . '] -' . ' [' . str_pad($d['JAM_SELESAI'], 2, '0', STR_PAD_LEFT) . ':' . str_pad($d['MENIT_SELESAI'], 2, '0') . ']', $html);
    $html = str_replace('{jalur}', $d['NM_JALUR'], $html);
    $html = str_replace('{kelompok_jam}', $d['KELOMPOK_JAM'], $html);
    $html = str_replace('{nama}', $d['NM_C_MHS'], $html);
    $html = str_replace('{no_ujian}', $d['NO_UJIAN'], $html);
    $html = str_replace('{alamat}', $d['ALAMAT'], $html);
    $html = str_replace('{telp}', $d['TELP'], $html);
    $html = str_replace('{urutan}', $d['URUTAN'], $html);
    $html = str_replace('{kelamin}', $d['JENIS_KELAMIN'] == 1 ? "Laki-laki" : "Perempuan", $html);

    $pdf->AddPage();
    $pdf->writeHTML($html);
    $pdf->endPage();
}
$pdf->Output();
?>
