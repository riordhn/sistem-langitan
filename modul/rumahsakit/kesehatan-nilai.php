<?php

include'config.php';
include 'class/kesehatan.class.php';


$kesehatan = new kesehatan($db);
$mode = get('mode', 'input');

if (isset($_GET)) {
    if ($mode == 'input') {
        if (get('jadwal') != '') {
            if (isset($_POST)) {
                if (post('mode') == 'save') {
                    for ($i = 1; $i <= post('jumlah_data'); $i++) {
                        if (post('kesimpulan_mata' . $i) != null && post('kesimpulan_tht' . $i) != null && post('kesimpulan_akhir' . $i) != null) {
                            $kesehatan->update_data_kesehatan(post('id_c_mhs' . $i), post('kesimpulan_mata' . $i), post('kesimpulan_tht' . $i), post('kesimpulan_akhir' . $i), post('catatan' . $i));
                            $kesehatan->update_verifikasi_kesehatan(post('id_c_mhs' . $i), $user->ID_PENGGUNA);
                            $kesehatan->insert_log_nilai_kesehatan(post('id_c_mhs' . $i), $user->ID_PENGGUNA, post('kesimpulan_mata' . $i), post('kesimpulan_tht' . $i), post('kesimpulan_akhir' . $i));
                        }
                    }
                }
            }
            $smarty->assign('data_cmhs', $kesehatan->load_data_kesehatan_cmhs(get('jadwal')));
        }
    }
}
$smarty->assign('data_jadwal_kesehatan', $kesehatan->load_jadwal_kesehatan());

$smarty->display("kesehatan-nilai-input.tpl");
?>
