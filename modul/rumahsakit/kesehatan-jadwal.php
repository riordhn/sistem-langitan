<?php

include('config.php');
include('class/kesehatan.class.php');
include('../ppmb/class/Penerimaan.class.php');

$pen = new Penerimaan($db);
$ks = new kesehatan($db);
$mode = get('mode', 'view');

if ($mode == 'edit') {
    $smarty->assign('data_jadwal_kesehatan', $ks->get_jadwal_kesehatan(get('id_jadwal')));
} else if ($mode == 'delete') {
    $smarty->assign('data_jadwal_kesehatan', $ks->get_jadwal_kesehatan(get('id_jadwal')));
} else {
    if ((post('mode')) == 'edit') {
        $ks->update_jadwal_kesehatan(post('id_jadwal'),post('penerimaan'), post('kelompok_jam'), date('d-M-Y', mktime(0, 0, 0, post('tgl_test_Month'), post('tgl_test_Day'), post('tgl_test_Year'))), post('mulai_Hour'), post('mulai_Minute'), post('selesai_Hour'), post('selesai_Minute'), post('kapasitas'));
    } else if (post('mode') == 'add') {
        $ks->insert_jadwal_kesehatan(post('penerimaan'),post('kelompok_jam'), date('d-M-Y', mktime(0, 0, 0, post('tgl_test_Month'), post('tgl_test_Day'), post('tgl_test_Year'))), post('mulai_Hour'), post('mulai_Minute'), post('selesai_Hour'), post('selesai_Minute'), post('kapasitas'));
    } else if (post('mode') == 'delete') {
        $ks->delete_jadwal_kesehatan(post('id_jadwal'));
    }
    $smarty->assign('data_jadwal_kesehatan', $ks->load_jadwal_kesehatan());
}
$smarty->assign('penerimaan_set', $pen->GetAllForView());
$smarty->display("kesehatan-jadwal-{$mode}.tpl");
?>
