<div class="center_title_bar">Laporan Hasil Tes Kesehatan</div> 
<form method="get" id="report_form" action="kesehatan-laporan-hasil.php">
    <table style="width:98%;">
        <tr>
            <th colspan="4" class="center">Parameter</th>
        </tr>
        <tr>
            <td>Penerimaan</td>
            <td>
                <select name="penerimaan" id="penerimaan">
                    <option value="">Pilih Penerimaan</option>
                    {foreach $penerimaan_set as $p}
                        <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                            {foreach $p.p_set as $p2}
                                {if ($p2.ID_JENJANG==1||$p2.ID_JENJANG==5)}
                                    <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                                {/if}
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
            </td>
            <td>Hasil Tes</td>
            <td>
                <select name="hasil">
                    <option value=""  {if $smarty.get.hasil==''}selected="true"{/if}>Semua</option>
                    <option value="1" {if $smarty.get.hasil=='1'}selected="true"{/if}>Lulus</option>
                    <option value="0" {if $smarty.get.hasil=='0'}selected="true"{/if}>Tidak Lulus</option>
                </select>
            </td>
        </tr>
        <tr>
            <td width="15%">Jadwal</td>
            <td width="25%">
                <select name="jadwal" id="jadwal">
                    <option value="">Semua</option>
                    {foreach $data_jadwal_kesehatan as $jadwal}
                        <option value="{$jadwal.ID_JADWAL_KESEHATAN}" {if $jadwal.ID_JADWAL_KESEHATAN==$smarty.get.jadwal}selected="true"{/if}>{$jadwal.NM_PENERIMAAN} {$jadwal.TGL_TEST} : Kelompok {$jadwal.KELOMPOK_JAM}</option>
                    {/foreach}
                </select>
            </td>
            <td colspan="2"></td>
        </tr>

        <tr>
            <td colspan="4" class="center">
                <input type="hidden" name="mode" value="laporan" />
                <input type="submit" name="submit" class="button" value="Tampilkan"/>
            </td>
        </tr>
    </table>
</form>
{if isset($data_laporan)}
    <table style="width:98%;">
        <tr>
            <th colspan="6" class="center">HASIL TES KESEHATAN</th>
        </tr>
        <tr>
            <th>NO</th>
            <th>NO UJIAN</th>
            <th>NAMA</th>
            <th>PROGRAM STUDI</th>
            <th style="width: 270px">JADWAL</th>
            <th>HASIL TES</th>
        </tr>
        {foreach $data_laporan as $lap}
            <tr>
                <td>{$lap@index+1}</td>
                <td>{$lap.NO_UJIAN}</td>
                <td>{$lap.NM_C_MHS}</td>
                <td>{$lap.NM_JENJANG} {$lap.NM_PROGRAM_STUDI}</td>
                <td>{$lap.TGL_TEST} Kelompok {$lap.KELOMPOK_JAM} Jam [{$lap.JAM_MULAI|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}:{$lap.MENIT_MULAI|str_pad:2:"0"}] - [{$lap.JAM_SELESAI|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}:{$lap.MENIT_SELESAI|str_pad:2:"0"}]</td>
                <td>
                    {if $lap.KESEHATAN_KESIMPULAN_AKHIR==1}
                        Lulus
                    {else $lap.KESEHATAN_KESIMPULAN_AKHIR==0}
                        TIdak Lulus
                    {/if}
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="6" class="data-kosong">Data Tidak Ada</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="6" class="center">
                <a class="disable-ajax button" href="cetak-hasil-tes.php?{$smarty.server.QUERY_STRING}" target="_blank">Cetak Hasil</a>
            </td>
        </tr>
    </table>
{/if}
{literal}
    <script>
            $('#penerimaan').change(function(){
                    $.ajax({
                            type:'post',
                            url:'getJadwal.php',
                            data:'penerimaan='+$(this).val(),
                            success:function(data){
                                    $('#jadwal').html(data);
                            }                    
                    })
            });
    </script>
{/literal}