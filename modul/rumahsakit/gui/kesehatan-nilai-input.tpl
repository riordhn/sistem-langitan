<div class="center_title_bar">Input Nilai Pemeriksaan Kesehatan Calon Mahasiswa</div>
<form action="kesehatan-nilai.php" method="get">
    <table style="width: 60%">
        <tr>
            <th colspan="3" class="center">Parameter</th>
        </tr>
        <tr>
            <td><label>Kelompok Jadwal Kesehatan</label></td>
            <td>
                <select name="jadwal">
                    {foreach $data_jadwal_kesehatan as $jadwal}
                        <option value="{$jadwal.ID_JADWAL_KESEHATAN}" {if $jadwal.ID_JADWAL_KESEHATAN==$smarty.get.jadwal}selected="true"{/if}>{$jadwal.NM_PENERIMAAN} {$jadwal.TGL_TEST} : Kelompok {$jadwal.KELOMPOK_JAM}</option>
                    {/foreach}
                </select>
            </td>
            <td>
                <input class="button" type="submit" value="Tampil"/>
                <input type="hidden" name="mode" value="input"/>
            </td>
        </tr>
    </table>
</form>

{if isset($data_cmhs)}
    <form action="kesehatan-nilai.php?{$smarty.server.QUERY_STRING}" method="post">
        <table style="width: 90%">
            <tr><th class="center">Keterangan</th></tr>
            <tr>
                <td>&nbsp;Y&nbsp;:&nbsp;Ya&nbsp;|&nbsp;N&nbsp;:&nbsp;Normal&nbsp;|&nbsp;T&nbsp;:&nbsp;Tidak&nbsp;|&nbsp;MS&nbsp;:&nbsp;Memenuhi&nbsp;Syarat&nbsp;|&nbsp;TMS&nbsp;:&nbsp;Tidak&nbsp;Memenuhi&nbsp;Syarat&nbsp;</td>
            </tr>
        </table>
        <table style="width: 90%">
            <tr>
                <th rowspan="3">No</th>
                <th rowspan="3">No Ujian</th>
                <th rowspan="3">Nama</th>
            </tr>
            <tr>
                <th colspan="2">Mata</th>
                <th colspan="2">THT</th>
                <th colspan="2">Kesimpulan Akhir</th>
                <th rowspan="2">Catatan</th>
            </tr>
            <tr>
                <th>MS</th>
                <th>TMS</th>
                <th>MS</th>
                <th>TMS</th>
                <th>MS</th>
                <th>TMS</th>
            </tr>
            {foreach $data_cmhs as $cmhs}
                <tr>
                    <td>{$cmhs@index+1}
                        <input type="hidden" name="id_c_mhs{$cmhs@index+1}" value="{$cmhs.ID_C_MHS}"/>
                    </td>
                    <td>{$cmhs.NO_UJIAN}</td>
                    <td>{$cmhs.NM_C_MHS}</td>
                    <td>
                        <input name="kesimpulan_mata{$cmhs@index+1}" type="radio" value="Memenuhi Syarat" 
                               {if isset($cmhs.KESEHATAN_KESIMPULAN_MATA)} disabled="true" 
                                   {if $cmhs.KESEHATAN_KESIMPULAN_MATA=='Memenuhi Syarat'}checked="true"
                                   {/if} 
                               {/if}/>
                    </td>
                    <td>
                        <input name="kesimpulan_mata{$cmhs@index+1}" type="radio" value="Tidak Memenuhi Syarat" 
                               {if isset($cmhs.KESEHATAN_KESIMPULAN_MATA)} disabled="true" 
                                   {if $cmhs.KESEHATAN_KESIMPULAN_MATA=='Tidak Memenuhi Syarat'}checked="true"
                                   {/if} 
                               {/if}/>
                    </td>
                    <td>
                        <input name="kesimpulan_tht{$cmhs@index+1}" type="radio" value="Memenuhi Syarat" 
                               {if isset($cmhs.KESEHATAN_KESIMPULAN_THT)} disabled="true" 
                                   {if $cmhs.KESEHATAN_KESIMPULAN_THT=='Memenuhi Syarat'}checked="true"
                                   {/if}
                               {/if}/>
                    </td>
                    <td>
                        <input name="kesimpulan_tht{$cmhs@index+1}" type="radio" value="Tidak Memenuhi Syarat"
                               {if isset($cmhs.KESEHATAN_KESIMPULAN_THT)} disabled="true"
                                   {if $cmhs.KESEHATAN_KESIMPULAN_THT=='Tidak Memenuhi Syarat'}checked="true"
                                   {/if}
                               {/if}/>
                    </td>
                    <td>
                        <input name="kesimpulan_akhir{$cmhs@index+1}" type="radio" value="1" 
                               {if isset($cmhs.TGL_VERIFIKASI_KESEHATAN)} disabled="true" 
                                   {if $cmhs.KESEHATAN_KESIMPULAN_AKHIR=='1'} checked="true"
                                   {/if}
                               {/if}/>
                    </td>
                    <td>
                        <input name="kesimpulan_akhir{$cmhs@index+1}" type="radio" value="0"
                               {if isset($cmhs.TGL_VERIFIKASI_KESEHATAN)} disabled="true" 
                                   {if $cmhs.KESEHATAN_KESIMPULAN_AKHIR=='0'}checked="true"
                                   {/if}
                               {/if}/>
                    </td>
                    <td>
                        <textarea name="catatan{$cmhs@index+1}" cols="30" rows="3">{$cmhs.KESEHATAN_CATATAN}</textarea>
                    </td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="10" class="data-kosong">Data Kosong</td>
                </tr>
            {/foreach}
            {if count($data_cmhs)!=0}
                <tr>
                    <td colspan="10" class="center">
                        <input type="hidden" name="jumlah_data" value="{count($data_cmhs)}"/>
                        <input type="hidden" name="mode" value="save"/>
                        <a class="button" href="cetak-hasil-tes-kesehatan.php?kelompok={$id_jadwal_kesehatan}" target="_blank" class="disable-ajax">Cetak Hasil</a>
                        <input type="submit" class="button" value="Simpan" />
                        <input type="reset" class="button" value="Reset" />
                    </td>
                </tr>
            {/if}
        </table>
    </form>
{/if}