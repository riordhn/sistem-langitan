<div class="center_title_bar">Ubah Jadwal ELPT</div>
<form method="get" action="kesehatan-ubah-jadwal.php">
    <table style="width: 30%">
        <tr>
            <th colspan="2" class="center">Parameter</th>
        </tr>
        <tr>
            <td>NO UJIAN</td>
            <td><input type="text" name="no_ujian" value="{$smarty.get.no_ujian}" /></td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <input type="hidden" name="mode" value="tampil"/>
                <input class="button" type="submit" value="Tampil"/>
            </td>
        </tr>
    </table>
</form>
{if isset($data_jadwal_cmhs)}
    {if $data_jadwal_cmhs!=''}
        <table style="width: 50%">
            <tr>
                <th colspan="2">DATA DIRI CALON MAHASISWA</th>
            </tr>
            <tr>
                <td>NAMA</td>
                <td>{$data_jadwal_cmhs.NM_C_MHS}</td>
            </tr>
            <tr>
                <td>NO UJIAN</td>
                <td>{$data_jadwal_cmhs.NO_UJIAN}</td>
            </tr>
            <tr>
                <td>PROGRAM STUDI</td>
                <td>{$data_jadwal_cmhs.NM_JENJANG} {$data_jadwal_cmhs.NM_PROGRAM_STUDI}</td>
            </tr>
        </table>
        <table style="width: 50%">
            <tr>
                <th colspan="2">JADWAL KESEHATAN CALON MAHASISWA</th>
            </tr>
            <tr>
                <td>Tanggal</td>
                <td>{$data_jadwal_cmhs.TGL_TEST}</td>
            </tr>
            <tr>
                <td>Kelompok </td>
                <td>{$data_jadwal_cmhs.KELOMPOK_JAM}</td>
            </tr>
            <tr>
                <td>Waktu</td>
                <td>{$data_jadwal_cmhs.JAM_MULAI|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}:{$data_jadwal_cmhs.MENIT_MULAI|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}-
                    {$data_jadwal_cmhs.JAM_SELESAI|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}:{$data_jadwal_cmhs.MENIT_SELESAI|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}</td>
            </tr>
        </table>
        <table style="width: 50%">
            <tr>
                <th colspan="2">JADWAL ELPT CALON MAHASISWA</th>
            </tr>
            <tr>
                <td>Tanggal</td>
                <td>{$data_jadwal_cmhs.TGL_ELPT}</td>
            </tr>
            <tr>
                <td>Ruang </td>
                <td>{$data_jadwal_cmhs.NM_RUANG}</td>
            </tr>
            <tr>
                <td>Waktu</td>
                <td>{$data_jadwal_cmhs.JAM_ELPT|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}:{$data_jadwal_cmhs.MENIT_ELPT|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}</td>
            </tr>
        </table>
        <form action="kesehatan-ubah-jadwal.php?{$smarty.server.QUERY_STRING}" method="post">
            <table style="width: 70%">
                <tr>
                    <th colspan="2">PILIH JADWAL PENGGANTI</th>
                </tr>
                <tr>
                    <td>PILIH JADWAL</td>
                    <td>
                        <select name="jadwal_baru">
                            <option value="">Pilih</option>
                            {foreach $data_jadwal as $jdl}
                                {if $jdl.ID_PENERIMAAN==$data_jadwal_cmhs.ID_PENERIMAAN}
                                    <option value="{$jdl.ID_JADWAL_KESEHATAN}" {if $jdl.TERISI==$jdl.KAPASITAS}style="background-color:coral "{/if} {if $jdl.ID_JADWAL_KESEHATAN==$data_jadwal_cmhs.ID_JADWAL_KESEHATAN}selected="true"{/if}>
                                        {$jdl.NM_PENERIMAAN} {$jdl.TGL_TEST} : Kelompok {$jdl.KELOMPOK_JAM}
                                        Terisi {$jdl.TERISI} 
                                    </option>
                                {/if}
                            {/foreach}
                        </select><br/>
                        <label for="jadwal_baru" style="color: red;font-size: 12px">Background Berwarna Merah Menandakan Jadwal Sudah Penuh</label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="center">
                        <input type="hidden" name="mode" value="ubah"/>
                        <input type="hidden" name="id_c_mhs" value="{$data_jadwal_cmhs.ID_C_MHS}"/>
                        <input type="hidden" name="jadwal_lama" value="{$data_jadwal_cmhs.ID_JADWAL_KESEHATAN}"/>
                        <input class="button" type="submit" value="Update"/>
                    </td>
                </tr>
            </table>
        </form>
    {else}
        <span class="data-kosong">Data Jadwal Calon Mahasiswa Kosong</span>
    {/if}
{/if}