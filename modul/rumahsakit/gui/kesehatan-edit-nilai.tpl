<div class="center_title_bar">Edit Nilai Pemeriksaan Kesehatan Calon Mahasiswa</div>
<form action="kesehatan-edit-nilai.php" method="get">
    <table>
        <tr>
            <th colspan="3">PARAMETER</th>
        </tr>
        <tr>
            <td><label>Nomer Ujian</label></td>
            <td>
                <input type="text" name="no_ujian" value="{$smarty.get.no_ujian}"/>
            </td>
            <td>
                <input class="button" type="submit" value="Tampil"/>
            </td>
        </tr>
    </table>
</form>
{if isset($cmhs)}
    {if $cmhs!=''}
        <form action="kesehatan-edit-nilai.php?{$smarty.server.QUERY_STRING}" method="post">
            <table style="width: 90%">
                <tr>
                    <th class="center">Keterangan</th>
                </tr>
                <tr>
                    <td>&nbsp;Y&nbsp;:&nbsp;Ya&nbsp;|&nbsp;N&nbsp;:&nbsp;Normal&nbsp;|&nbsp;T&nbsp;:&nbsp;Tidak&nbsp;|&nbsp;MS&nbsp;:&nbsp;Memenuhi&nbsp;Syarat&nbsp;|&nbsp;TMS&nbsp;:&nbsp;Tidak&nbsp;Memenuhi&nbsp;Syarat&nbsp;|&nbsp;L&nbsp;:&nbsp;Lulus&nbsp;|&nbsp;TL&nbsp;:&nbsp;Tidak Lulus</td>
                </tr>
            </table>
            <table>
                <tr>
                    <th rowspan="3">No Ujian</th>
                    <th rowspan="3">Nama</th>
                </tr>
                <tr>
                    <th colspan="2">Mata</th>
                    <th colspan="2">THT</th>
                    <th colspan="2">Kesimpulan Akhir</th>
                    <th rowspan="2">Catatan</th>
                </tr>
                <tr>
                    <th>MS</th>
                    <th>TMS</th>
                    <th>MS</th>
                    <th>TMS</th>
                    <th>L</th>
                    <th>TL</th>
                </tr>
                <tr>
                    <td>{$cmhs.NO_UJIAN}</td>
                    <td>{$cmhs.NM_C_MHS}</td>
                    <td><input name="kesimpulan_mata" type="radio" value="Memenuhi Syarat" {if $cmhs.KESEHATAN_KESIMPULAN_MATA=='Memenuhi Syarat'}checked="true"{/if}/></td>
                    <td><input name="kesimpulan_mata" type="radio" value="Tidak Memenuhi Syarat" {if $cmhs.KESEHATAN_KESIMPULAN_MATA=='Tidak Memenuhi Syarat'}checked="true"{/if}/></td>
                    <td><input name="kesimpulan_tht" type="radio" value="Memenuhi Syarat" {if $cmhs.KESEHATAN_KESIMPULAN_THT=='Memenuhi Syarat'}checked="true"{/if}/></td>
                    <td><input name="kesimpulan_tht" type="radio" value="Tidak Memenuhi Syarat" {if $cmhs.KESEHATAN_KESIMPULAN_THT=='Tidak Memenuhi Syarat'}checked="true"{/if}/></td>
                    <td><input name="kesimpulan_akhir" type="radio" value="1"  {if $cmhs.KESEHATAN_KESIMPULAN_AKHIR=='1'&&$cmhs.TGL_VERIFIKASI_KESEHATAN!=''} checked="true"{/if}/></td>
                    <td><input name="kesimpulan_akhir" type="radio" value="0" {if $cmhs.KESEHATAN_KESIMPULAN_AKHIR=='0'&&$cmhs.TGL_VERIFIKASI_KESEHATAN!=''}checked="true"{/if}/></td>
                    <td><textarea name="catatan" cols="30" rows="3">{$cmhs.KESEHATAN_CATATAN}</textarea></td>
                </tr>
                <tr>
                    <td colspan="9" class="center">
                        <input type="hidden" name="id_c_mhs" value="{$cmhs.ID_C_MHS}"/>
                        <input type="hidden" name="mode" value="save"/>
                        <input class="button" type="submit" value="Simpan"/>
                        <input type="reset" class="button" value="Reset" />
                    </td>
                </tr>
            </table>
        </form>
    {else}
        <span style="color: red;padding: 5px;margin: 10px">Data Tidak Ditemukan</span>
    {/if}
{/if}