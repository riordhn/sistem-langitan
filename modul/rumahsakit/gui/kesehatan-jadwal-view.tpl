<div class="center_title_bar">Jadwal Tes kesehatan</div>
<table style="width: 100%">
    <tr>
        <th class="center" style="width: 5%">No</th>
        <th>Penerimaan</th>
        <th>Kelompok Jam</th>
        <th>Kapasitas</th>
        <th>Jadwal</th>
        <th>Terisi</th>
        <th>Operasi</th>
    </tr>
    {foreach $data_jadwal_kesehatan as $ks}
        <tr>
            <td class="center">{$ks@index+1}</td>
            <td>{if $ks.NM_PENERIMAAN!=''}Gelombang {$ks.GELOMBANG} {$ks.NM_PENERIMAAN} Jalur ({$ks.NM_JALUR}){/if}</td>
            <td>{$ks.KELOMPOK_JAM}</td>
            <td>{$ks.KAPASITAS}</td>
            <td>{$ks.TGL_TEST|date_format:"%d %B %Y"} [{$ks.JAM_MULAI|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}:{$ks.MENIT_MULAI|str_pad:2:"0"}] - [{$ks.JAM_SELESAI|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}:{$ks.MENIT_SELESAI|str_pad:2:"0"}]</td>
            <td>{$ks.TERISI}</td>
            <td class="center">
                <a class="button" href="kesehatan-jadwal.php?mode=edit&id_jadwal={$ks.ID_JADWAL_KESEHATAN}">Edit</a>
                <a class="button" href="kesehatan-jadwal.php?mode=delete&id_jadwal={$ks.ID_JADWAL_KESEHATAN}">Hapus</a>
            </td>
        </tr>
    {foreachelse}
        <tr>
            <td colspan="7" class="data-kosong">Data Masih Kosong</td>
        </tr>
    {/foreach}
    <tr>
        <td colspan="7" class="center">
            <a class="button" href="kesehatan-jadwal.php?mode=add">Tambah</a>
        </td>
    </tr>
</table>