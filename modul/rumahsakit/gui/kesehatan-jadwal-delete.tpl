<div class="center_title_bar">Jadwal Tes Kesehatan - Hapus Jadwal</div>

<form action="kesehatan-jadwal.php" method="post">
<input type="hidden" name="mode" value="delete" />
<input type="hidden" name="id_jadwal" value="{$data_jadwal_kesehatan['ID_JADWAL_KESEHATAN']}" />
<table>
    <tr>
            <th colspan="2">Edit Jadwal Kesehatan</th>
        </tr>
    <tr>
        <td>Kelompok Jam</td>
        <td>{$data_jadwal_kesehatan.KELOMPOK_JAM}</td>
    </tr>
    <tr>
        <td>Tanggal</td>
        <td>
            {$data_jadwal_kesehatan.TGL_TEST}
        </td>
    </tr>
    <tr>
        <td>Jam</td>
        <td>
            [{$data_jadwal_kesehatan.JAM_MULAI|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}:{$data_jadwal_kesehatan.MENIT_MULAI|str_pad:2:"0"}] - [{$data_jadwal_kesehatan.JAM_SELESAI|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}:{$data_jadwal_kesehatan.MENIT_SELESAI|str_pad:2:"0"}]
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center">
            <a class="button" href="kesehatan-jadwal.php">Batal</a>
            <input class="button" type="submit" value="Hapus" />
        </td>
    </tr>
</table>
</form>