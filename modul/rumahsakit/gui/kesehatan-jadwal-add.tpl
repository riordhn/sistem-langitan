<div class="center_title_bar">Jadwal Tes Kesehatan - Tambah</div>

<form action="kesehatan-jadwal.php" method="post">
    <table>
        <tr>
            <th colspan="2">Tambah Jadwal Kesehatan</th>
        </tr>
        <tr>
            <td>Penerimaan</td>
            <td>
                <select name="penerimaan" class="required">
                    <option value="">Pilih Penerimaan</option>
                    {foreach $penerimaan_set as $p}
                        <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                            {foreach $p.p_set as $p2}
                                {if ($p2.ID_JENJANG==1||$p2.ID_JENJANG==5)}
                                    <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                                {/if}
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Kelompok Jam</td>
            <td>
                <input type="text" class="required" name="kelompok_jam"/>
            </td>
        </tr>
        <tr>
            <td>Tanggal</td>
            <td>
                {html_select_date prefix="tgl_test_" field_order="DMY"}
            </td>
        </tr>
        <tr>
            <td>Jam Mulai</td>
            <td>
                {html_select_time prefix="mulai_" display_seconds=false use_24_hours=true minute_interval=5}
            </td>
        </tr>
        <tr>
            <td>Jam Selesai</td>
            <td>
                {html_select_time prefix="selesai_" display_seconds=false use_24_hours=true minute_interval=5}
            </td>
        </tr>
        <tr>
            <td>Kapasitas</td>
            <td>
                <input type="text" class="required" name="kapasitas" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <a class="button" href="kesehatan-jadwal.php">Batal</a>
                <input class="button" type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
    <input type="hidden" name="mode" value="add" />
</form>
{literal}
    <script type="text/javascript">
        $('form').validate();
    </script>
{/literal}