<div class="center_title_bar">Print Pemeriksaan Kesehatan Calon Mahasiswa</div>
<form action="kesehatan-cetak.php" method="get">
    <table style="width: 90%">
        <tr>
            <th colspan="4" class="center">Parameter</th>
        </tr>
        <tr>
            <td><label>Tanggal Jadwal Kesehatan</label></td>
            <td>
                <select id="tanggal" name="tanggal">
                    <option value="">Pilih Tanggal</option>
                    {foreach $data_tanggal as $tgl}
                        <option value="{$tgl['TGL_TEST']}" {if $tgl['TGL_TEST']==$smarty.get.tanggal} selected="true"{/if}>{$tgl['TGL_TEST']}</option>
                    {/foreach}
                </select>                
            </td>
            <td><label>Kelompok Jadwal Kesehatan</label></td>
            <td>           
                <select id="kelompok" name="kelompok">
                    <option value="">Pilih Kelompok</option>
                    {foreach $data_kelompok as $kel}
                        <option value='{$kel.ID_JADWAL_KESEHATAN}' {if $kel.ID_JADWAL_KESEHATAN==$smarty.get.kelompok}selected="true"{/if}>Penerimaan {$kel.NM_PENERIMAAN}  Kelompok {$kel.KELOMPOK_JAM}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="4" class="center">
                <input class="button" type="submit" value="Lihat"/>
            </td>
        </tr>
    </table>
</form>
{if isset($data_cmhs)}
    <table style="width: 90%">
        <tr>
            <th class="center">No</th>
            <th>No Ujian</th>
            <th>Nama</th>
            <th>Program Studi</th>
            <th>fakultas</th>
            <th>Form</th>
        </tr>
        {foreach $data_cmhs as $cmhs}
            <tr>
                <td>{$cmhs@index+1}</td>
                <td>{$cmhs.NO_UJIAN}</td>
                <td>{$cmhs.NM_C_MHS}</td>
                <td>{$cmhs.NM_JENJANG} {$cmhs.NM_PROGRAM_STUDI}</td>
                <td>{$cmhs.NM_FAKULTAS}</td>
                <td class="center">
                    <a class="button disable-ajax" href="cetak-form-kesehatan.php?cmhs={$cmhs.ID_C_MHS}" target="_blank">Cetak</a>
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="6" class="data-kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
        {if count($data_cmhs)>0}
            <tr>
                <td colspan="6" class="center">
                    <a class="button disable-ajax" href="multiple-cetak-form-kesehatan.php?jadwal={$smarty.get.kelompok}" target="_blank">Cetak Semua</a>
                </td>
            </tr>
        {/if}
    </table>
{/if}
{literal}
    <script>
        $('#tanggal').change(function() {
            $.ajax({
                type: 'post',
                url: 'get-kelompok-jam-tanggal.php',
                data: 'tgl=' + $(this).val(),
                success: function(data) {
                    $('#kelompok').html(data);
                }
            })
        });
    </script>
{/literal}