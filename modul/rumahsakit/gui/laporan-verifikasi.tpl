<div class="center_title_bar">Laporan Verifikasi Data</div> 
<form method="get" id="report_form" action="laporan-verifikasi.php">
    <table style="width:98%;">
        <tr>
            <th colspan="4" class="center">Parameter</th>
        </tr>
        <tr>
            <td width="15%">Fakultas</td>
            <td width="25%">
                <select name="fakultas" id="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
            <td>Penerimaan</td>
            <td>
                <select name="penerimaan">
                    <option value="">Pilih Penerimaan</option>
                    {foreach $penerimaan_set as $p}
                        <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                            {foreach $p.p_set as $p2}
                                {if ($p2.ID_JENJANG==1||$p2.ID_JENJANG==5)}
                                    <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                                {/if}
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="6" class="center">
                <input type="hidden" name="mode" value="laporan" />
                <input type="submit" name="submit" class="button" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if $smarty.get.mode=='laporan'}
    <table style="width: 98%">
        <tr>
            <th colspan="7" class="center">Laporan Lapor Diri Calon Mahasiswa
            </th>
        </tr>
        <tr>
            <th>NO</th>
            <th>
                {if $smarty.get.fakultas!=''}
                    PROGRAM STUDI
                {else}
                    FAKULTAS
                {/if}
            </th>
            <th>JUMLAH DITERIMA</th>
            <th>JUMLAH ISI REGMABA</th>
            <th>SUDAH VERIFIKASI</th>
            <th>SUDAH PUNYA JADWAL</th>
            <th>SUDAH IKUT TES</th>
        </tr>
        <tr>
            {foreach $data_jadwal as $jadwal}
                <th>{$jadwal.TGL_JADWAL}</th>
            {/foreach}
        </tr>
        {$diterima=0}
        {$regmaba=0}
        {$sudah_verifikasi=0}
        {$sudah_jadwal=0}
        {$sudah_tes=0}
        {foreach $data_laporan as $lap}
            <tr style="font-size: 13px">
                <td>{$lap@index+1}</td>
                <td>
                    {if $smarty.get.fakultas!=''}
                        {$lap.NM_JENJANG} {$lap.NM_PROGRAM_STUDI}
                    {else}
                        {$lap.NM_FAKULTAS}
                    {/if}
                </td>
                <td><a href="laporan-verifikasi.php?mode=detail&penerimaan={$smarty.get.penerimaan}&fakultas={$lap.ID_FAKULTAS}&status=1">{$lap.DITERIMA}</a></td>
                <td><a href="laporan-verifikasi.php?mode=detail&penerimaan={$smarty.get.penerimaan}&fakultas={$lap.ID_FAKULTAS}&status=2">{$lap.REGMABA}</a></td>
                <td><a href="laporan-verifikasi.php?mode=detail&penerimaan={$smarty.get.penerimaan}&fakultas={$lap.ID_FAKULTAS}&status=3">{$lap.SUDAH_VERIFIKASI}</a></td>
                <td><a href="laporan-verifikasi.php?mode=detail&penerimaan={$smarty.get.penerimaan}&fakultas={$lap.ID_FAKULTAS}&status=4">{$lap.SUDAH_JADWAL}</a></td>
                <td><a href="laporan-verifikasi.php?mode=detail&penerimaan={$smarty.get.penerimaan}&fakultas={$lap.ID_FAKULTAS}&status=5">{$lap.SUDAH_TES}</a></td>
            </tr>
            {$diterima=$diterima+$lap.DITERIMA}
            {$regmaba=$regmaba+$lap.REGMABA}
            {$sudah_verifikasi=$sudah_verifikasi+$lap.SUDAH_VERIFIKASI}
            {$sudah_jadwal=$sudah_jadwal+$lap.SUDAH_JADWAL}
            {$sudah_tes=$sudah_tes+$lap.SUDAH_TES}
        {foreachelse}
            <tr>
                <td colspan="7" class="data-kosong">Data Kosong</td>
            </tr>
        {/foreach}
        <tr class="total">
            <td class="center" colspan="2">TOTAL</td>
            <td><a href="laporan-verifikasi.php?mode=detail&fakultas=&penerimaan={$smarty.get.penerimaan}&status=1">{$diterima}</a></td>
            <td><a href="laporan-verifikasi.php?mode=detail&fakultas=&penerimaan={$smarty.get.penerimaan}&status=2">{$regmaba}</a></td>
            <td><a href="laporan-verifikasi.php?mode=detail&fakultas=&penerimaan={$smarty.get.penerimaan}&status=3">{$sudah_verifikasi}</a></td>
            <td><a href="laporan-verifikasi.php?mode=detail&fakultas=&penerimaan={$smarty.get.penerimaan}&status=4">{$sudah_jadwal}</a></td>
            <td><a href="laporan-verifikasi.php?mode=detail&fakultas=&penerimaan={$smarty.get.penerimaan}&status=5">{$sudah_tes}</a></td>
        </tr>
    </table>
{else if $smarty.get.mode=='detail'}
    <a class="disable-ajax button" style="cursor: pointer" onclick="window.history.back()">Kembali</a>
    <p></p>
    <table style="width: 98%">
        <tr>
            <th colspan="8" class="center">DETAIL LAPORAN VERIFIKASI<br/>
                {if $smarty.get.status==1}
                    SUDAH DITERIMA
                {else if $smarty.get.status==2}
                    SUDAH ISI REGMABA
                {else if $smarty.get.status==3}
                    SUDAH VERIFIKASI
                {else if $smarty.get.status==4}
                    SUDAH PUNYA JADWAL
                {else if $smarty.get.status==5}
                    SUDAH TES KESEHATAN
                {/if}</th>
        </tr>
        <tr>
            <th>NO</th>
            <th>NO UJIAN</th>
            <th>NAMA</th>
            <th>PRODI</th>
            <th>TGL VERIFIKASI PENDIDIKAN</th>
            <th>TGL PRINT JADWAL</th>
            <th>JADWAL TES KESEHATAN</th>
            <th>TGL VERIFIKASI TES KESEHATAN</th>
        </tr>
        {foreach $data_detail as $d}
            <tr>
                <td>{$d@index+1}</td>
                <td>{$d.NO_UJIAN}</td>
                <td>{$d.NM_C_MHS}</td>
                <td>{$d.NM_JENJANG} {$d.NM_PROGRAM_STUDI}</td>
                <td>{$d.TGL_VERIFIKASI_PENDIDIKAN}</td>
                <td>{$d.TGL_PRINT_JADWAL}</td>
                <td>{$d.TGL_TEST}</td>
                <td>{$d.TGL_VERIFIKASI_KESEHATAN}</td>
            </tr>
        {foreachelse}
            <tr>
                <td class="data-kosong" colspan="8">Data Kosong</td>
            </tr>
        {/foreach}
    </table>
{/if}
{literal}
    <script>
            $('#fakultas').change(function(){
                    $.ajax({
                            type:'post',
                            url:'getProdi.php',
                            data:'id_fakultas='+$(this).val(),
                            success:function(data){
                                    $('#prodi').html(data);
                            }                    
                    })
            });
    </script>
{/literal}