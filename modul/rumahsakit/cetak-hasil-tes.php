<?php

include 'config.php';
include '../../tcpdf/config/lang/ind.php';
include '../../tcpdf/tcpdf.php';
include 'class/cetak.class.php';

$cetak = new cetak($db);

ob_clean();
$data_laporan = $cetak->load_hasil_tes_kesehatan_cmhs(get('jadwal'), get('hasil'), get('penerimaan'));


$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->AddPage();

$html = <<<EOF
<style>
    div { margin-top: 0pt; }
    .header { font-size: 16pt; font-family: times; font-weight: bold; text-align:center;}
    .address { font-size: 10pt; font-family: serif; margin-top: 0px ;text-align:center; }
    td { font-size: 8pt; }
    td.paraf {font-size: 6pt;vertical-align:bottom;text-align:center;}
    h1{text-align:center}
</style>
<table width="95%" border="0" align="center">
    <tr>
        <td width="10%"><img src="../../img/unair.gif" width="50px" height="50px"/></td>
        <td width="85%">
            <span class="header">UNIVERSITAS AIRLANGGA<br/>RUMAH SAKIT</span><br/>
            <span class="address">Kampus C Mulyorejo Surabaya 60115,Telp.0315916291,Fax.031-5916291</span><br/>
            <span class="address">Website : www.rumahsakit.unair.ac.id,email:rsua@unair.ac.id</span>
        </td>
    </tr>
</table>
<hr/>
<h1>PENGUMUMAN HASIL TES KESEHATAN</h1>
<p></p>
<table width="100%" cellpadding="2" border="1">
        <tr bgcolor="#000" color="#fff">
            <th width="5%">NO</th>
            <th width="15%">NO UJIAN</th>
            <th width="25%">NAMA</th>
            <th width="22%">PROGRAM STUDI</th>
            <th width="18%">JADWAL</th>
            <th width="15%">HASIL TES</th>
        </tr>
    {data_mahasiswa}
</table>
<p><p/>
EOF;

$index = 1;


$data_mahasiswa = '';
foreach ($data_laporan as $data) {
    $data_mahasiswa .= '<tr>
            <td>' . $index++ . '</td>
            <td>' . $data['NO_UJIAN'] . '</td>
            <td>' . $data['NM_C_MHS'] . '</td>
            <td>' . $data['NM_JENJANG'] . ' ' . $data['NM_PROGRAM_STUDI'] . '</td>
            <td>' . $data['TGL_TEST'] . ' Kelompok :' . $data['KELOMPOK_JAM'] . '</td>';
    if ($data['KESEHATAN_KESIMPULAN_AKHIR'] == 1) {
        $data_mahasiswa .= '<td>Lulus</td>';
    } else {
        $data_mahasiswa .= '<td>Tidak Lulus</td>';
    }

    $data_mahasiswa.='</tr>';
}

$html = str_replace('{data_mahasiswa}', $data_mahasiswa, $html);

$pdf->writeHTML($html);


$pdf->Output();

?>
