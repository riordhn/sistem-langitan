<?php

include'config.php';
include 'class/kesehatan.class.php';


$kesehatan = new kesehatan($db);

if (isset($_GET)) {
    if (get('no_ujian') != '') {
        if (isset($_POST)) {
            if (post('mode') == 'save') {
                if (post('kesimpulan_mata') != null && post('kesimpulan_tht') != null && post('kesimpulan_akhir') != null) {
                    $kesehatan->update_data_kesehatan(post('id_c_mhs'), post('kesimpulan_mata'), post('kesimpulan_tht'), post('kesimpulan_akhir'), post('catatan'));
                    $kesehatan->update_verifikasi_kesehatan(post('id_c_mhs'), $user->ID_PENGGUNA);
                    $kesehatan->insert_log_nilai_kesehatan(post('id_c_mhs'), $user->ID_PENGGUNA, post('kesimpulan_mata'), post('kesimpulan_tht'), post('kesimpulan_akhir'));
                }
            }
            $smarty->assign('cmhs', $kesehatan->get_data_kesehatan_cmhs(get('no_ujian')));
        }
    }
}
$smarty->display("kesehatan-edit-nilai.tpl");
?>
