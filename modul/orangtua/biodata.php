<?php
    include 'config.php';
	$username = $user->USERNAME;
	$username_mhs = substr($username, 0, -2);
	
	$id = $username_mhs;
	
	$nama_lengkap_ayah = "";
	$pendidikan_ayah = "";
	$pekerjaan_ayah = "";
	$alamat_ayah = "";
	
	$nama_lengkap_ibu = "";
	$pendidikan_ibu = "";
	$pekerjaan_ibu = "";
	$alamat_ibu = "";
	
	$nm_mhs = "";
	$username = "";
	$kelamin_pengguna = "";
	$tgl_lahir_pengguna = "";
	$tempat_lahir = "";
	$alamat_asal_mhs_kota = ""; 
	$mobile_mhs = "";
	$agama_mhs = "";
	$email_pengguna = ""; 
	$blog_pengguna = "";
	$lulus_dayat = "";
	$nilai_skhun_mhs = "";
	$cita_cita_mhs = "";
	$fakultas_mhs = "";
	$nm_program_studi = "";
	
	$db->Query("select mhs.NM_AYAH_MHS, 
				mhs.ALAMAT_AYAH_MHS, 
				payah.nama_pendidikan_akhir as PENDIDIKAN_AYAH_MHS, 
				krj_ayah.nm_pekerjaan as PEKERJAAN_AYAH_MHS, 
				mhs.NM_IBU_MHS, 
				mhs.ALAMAT_IBU_MHS, 
				pabu.nama_pendidikan_akhir as PENDIDIKAN_IBU_MHS, 
				krj_IBU.nm_pekerjaan as PEKERJAAN_IBU_MHS,
				p.NM_PENGGUNA, 
				p.USERNAME, 
				p.KELAMIN_PENGGUNA, 
				to_char(p.TGL_LAHIR_PENGGUNA, 'DD-MM-YYYY') TGL_LAHIR, 
				PROV.NM_PROVINSI, 
				KOTA.NM_KOTA, mhs.ALAMAT_ASAL_MHS, 
				mhs.MOBILE_MHS, 
				a.nm_agama as agama_mhs, 
				p.EMAIL_PENGGUNA, 
				p.BLOG_PENGGUNA, 
				to_char(pw.TGL_LULUS_PENGAJUAN,'YYYY') TAHUN_LULUS, 
				MHS.NILAI_SKHUN_MHS, 
				mhs.CITA_CITA_MHS, 
				f.nm_fakultas as fakultas_mhs, 
				ps.nm_program_studi,
				no_ujian
				from pengguna p
				left join mahasiswa mhs on mhs.ID_PENGGUNA = p.ID_PENGGUNA
				left join PENDIDIKAN_AKHIR payah on payah.id_pendidikan_akhir = mhs.PENDIDIKAN_AYAH_MHS
				left join PENDIDIKAN_AKHIR pabu on pabu.id_pendidikan_akhir = mhs.PENDIDIKAN_IBU_MHS
				left join PEKERJAAN krj_ayah on KRJ_AYAH.ID_PEKERJAAN = mhs.PEKERJAAN_AYAH_MHS
				left join PEKERJAAN krj_ibu on KRJ_IBU.ID_PEKERJAAN = mhs.PEKERJAAN_IBU_MHS
				left join agama a on a.ID_AGAMA = p.ID_AGAMA
				left join PROGRAM_STUDI ps on ps.ID_PROGRAM_STUDI = mhs.ID_PROGRAM_STUDI
				left join fakultas f on f.id_fakultas = ps.ID_FAKULTAS
				left join PROVINSI prov on PROV.ID_PROVINSI = MHS.LAHIR_PROP_MHS
				left join kota kota on kota.ID_KOTA = mhs.LAHIR_KOTA_MHS
				left join calon_mahasiswa_baru on calon_mahasiswa_baru.id_c_mhs = mhs.id_c_mhs
				left join pengajuan_wisuda pw on pw.id_mhs=mhs.id_mhs
				where p.username = '$id'
	
	");
	
	
	$row = $db->FetchAssoc();

	$nama_lengkap_ayah = $row['NM_AYAH_MHS'];
	$alamat_ayah = $row['ALAMAT_AYAH_MHS'];
	$pendidikan_ayah = $row['PENDIDIKAN_AYAH_MHS'];
	$pekerjaan_ayah= $row['PEKERJAAN_AYAH_MHS'];

	
	$nama_lengkap_ibu = $row['NM_IBU_MHS'];
	$alamat_ibu = $row['ALAMAT_IBU_MHS'];
	$pendidikan_ibu = $row['PENDIDIKAN_IBU_MHS'];
	$pekerjaan_ibu = $row['PEKERJAAN_IBU_MHS'];
	
	$nm_mhs = $row['NM_PENGGUNA'];
	$username = $row['USERNAME'];
	$kelamin_pengguna = $row['KELAMIN_PENGGUNA'];
	$tgl_lahir_pengguna = $row['TGL_LAHIR'];
	$tempat_lahir_prov = $row['NM_PROVINSI'];
	$tempat_lahir_kota = $row['NM_KOTA'];
	$alamat_asal_mhs_kota = $row['ALAMAT_ASAL_MHS']; 
	$mobile_mhs = $row['MOBILE_MHS'];
	$agama_mhs = $row['AGAMA_MHS'];
	$email_pengguna = $row['EMAIL_PENGGUNA']; 
	$blog_pengguna = $row['BLOG_PENGGUNA'];
	$lulus_dayat = $row['TAHUN_LULUS'];
	$nilai_skhun_mhs = $row['NILAI_SKHUN_MHS'];
	$cita_cita_mhs = $row['CITA_CITA_MHS'];
	$fakultas_mhs = $row['FAKULTAS_MHS'];
	$nm_program_studi = $row['NM_PROGRAM_STUDI'];
	$noujian = $row['NO_UJIAN'];



	$fotomahasiswa = "../../foto_mhs/".$username_mhs.".JPG";
	if (file_exists($fotomahasiswa)==0) {
		$fotomahasiswa = "../../foto_mhs/".$noujian.".JPG";	
		if (file_exists($fotomahasiswa)==0) {
			$fotomahasiswa = "../../img/mhs/photo.jpg";	
		}
	}
		
		
	$smarty->assign('nim', $username_mhs);
	$smarty->assign('fotomahasiswa', $fotomahasiswa);
	$smarty->assign('nama_ayah', $nama_lengkap_ayah);
	$smarty->assign('pendidikan_ayah', $pendidikan_ayah);
	$smarty->assign('pekerjaan_ayah', $pekerjaan_ayah);
	$smarty->assign('alamat_ayah', $alamat_ayah);
	
	$smarty->assign('nama_ibu', $nama_lengkap_ibu);
	$smarty->assign('pendidikan_ibu', $pendidikan_ibu);
	$smarty->assign('pekerjaan_ibu', $pekerjaan_ibu);
	$smarty->assign('alamat_ibu', $alamat_ibu);
	
   $smarty->assign('nm_mhs', $nm_mhs);
   //$smarty->assign('username_pengguna', $username_pengguna);
   $smarty->assign('kelamin_pengguna', $kelamin_pengguna);
   $smarty->assign('tgl_lahir_pengguna', $tgl_lahir_pengguna);
   $smarty->assign('tempat_lahir_prov', $tempat_lahir_prov);
   $smarty->assign('tempat_lahir_kota', $tempat_lahir_kota);
   $smarty->assign('alamat_asal_mhs_kota', $alamat_asal_mhs_kota); 
   $smarty->assign('mobile_mhs', $mobile_mhs);
   $smarty->assign('agama_mhs', $agama_mhs);
   $smarty->assign('email_pengguna', $email_pengguna); 
   $smarty->assign('blog_pengguna' , $blog_pengguna);
   $smarty->assign('lulus_dayat', $lulus_dayat);
   $smarty->assign('nilai_skhun_mhs', $nilai_skhun_mhs);
   $smarty->assign('cita_cita_mhs', $cita_cita_mhs);
   $smarty->assign('fakultas_mhs', $fakultas_mhs);
   $smarty->assign('nm_program_studi', $nm_program_studi);
	
   $smarty->display('biodata.tpl');
	
?>