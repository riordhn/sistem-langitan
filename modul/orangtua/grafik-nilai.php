<?php
/*
	include 'config.php';
	require_once "../../includes/dbconnect.php";
	require_once "../../includes/FusionCharts.php";
	require_once "../../includes/modconfig.php";
	
	$db->Query("
		select mhs.id_mhs from pengguna p 
		left join MAHASISWA mhs on mhs.ID_PENGGUNA = p.ID_PENGGUNA
		where p.USERNAME = '080810374F'
	");
	while($row = $db->FetchArray()) {
		$id_mhs = $row[0];
	}
	
	$ipk_mhs = array();
	$thn_ajaran = array();
	

	$db->Query("
		select MS.ipk_mhs_status, s.NM_SEMESTER, s.TAHUN_AJARAN from MHS_STATUS_120106 ms
		left join SEMESTER s on s.id_semester = ms.ID_SEMESTER
		where MS.id_mhs='$id_mhs'
	");
	
	$i = 0;
	$jml_nilai = 0;
	while($row = $db->FetchArray()) {
		$ipk_mhs[$i] = $row[0];
		$thn_ajaran[$i] = $row[2];
		$i++;
		$jml_nilai++;
	}
	//$smarty->display('grafik-nilai.tpl');
?>

<?php

$strXML = "";
$strXML .= "<chart caption='Profil Grafik Nilai Per Semester' xAxisName='Semester' yAxisName='Index Nilai' shownames='1' showvalues='0' decimals='2' numberPrefix='' formatNumber='0' formatNumberScale='0'>";

$strXML .= "<categories >";
        for($i = 0; $i < $jml_nilai; $i++ ){
            $strXML .= "<category label='" . $thn_ajaran[$i] . "' />";
        }
$strXML .= "</categories>";

//Universitas
$strXML .="<dataset seriesName='UA' color='AFD8F8' showValues='0'>";

$stid = oci_parse($conn, "select sum(dana_penelitian) / (select total_dana_penelitian from view_total_dana_penelitian where thn_penelitian = '" . $tahun[$i] . "') as proporsi from penelitian where sumber_dana_penelitian = 'Universitas' and thn_penelitian = '" . $tahun[$i] . "' group by thn_penelitian");
if (!$stid) {
    $e = oci_error($conn);
    trigger_error(htmlentities($e['message'], ENT_QUOTES),
E_USER_ERROR);
}

// Perform the logic of the query
$r = oci_execute($stid);
if (!$r) {
    $e = oci_error($stid);
    trigger_error(htmlentities($e['message'], ENT_QUOTES),
E_USER_ERROR);
}

// Fetch the results of the query
while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
    foreach ($row as $item) {
        //$item = (string)$item;
        $strXML .= "<set value='" . $item . "' />";
    }
}
$strXML .= "</dataset>";
//End Universitas

$strXML .="</chart>";
echo renderChartHTML("../../swf/Charts/MSColumn3D.swf", "", $strXML, "myNext", 600, 400, false);
?>
</center>
*/


include('config.php');

	$username = $user->USERNAME;
	$username_mhs = substr($username, 0, -2);
	$id_mhs = "";
	$kueri = "select id_mhs from mahasiswa where nim_mhs = '$username_mhs'";
	
	$result = $db->Query($kueri)or die("salah kueri : ");
	while($r = $db->FetchRow()) {
		$id_mhs = $r[0];
	}

//if ($user->IsLogged())
//{
    $ips_set = $db->QueryToArray("
        SELECT 
            a.id_semester, s.thn_akademik_semester, s.nm_semester,
            round(sum(D.KREDIT_SEMESTER*(select nilai_standar_nilai from standar_nilai sn where sn.nm_standar_nilai = a.nilai_huruf))/sum(D.KREDIT_SEMESTER),2) IPS
        FROM PENGAMBILAN_MK A
        LEFT JOIN KELAS_MK B ON (A.ID_KELAS_MK=B.ID_KELAS_MK)
        LEFT JOIN KURIKULUM_MK C ON (B.ID_KURIKULUM_MK=C.ID_KURIKULUM_MK)
        LEFT JOIN MATA_KULIAH D ON (D.ID_MATA_KULIAH=C.ID_MATA_KULIAH)
        join semester s on s.id_semester = a.id_semester
        WHERE A.ID_MHS={$id_mhs} AND A.FLAGNILAI=1 AND NOT (d.status_praktikum=2 and a.nilai_huruf='E')
        group by a.id_semester, s.thn_akademik_semester, s.nm_semester
        order by s.thn_akademik_semester asc, s.nm_semester asc
    ");
    
    $rows = array();
    foreach ($ips_set as $ips)
    {
        array_push($rows, "['{$ips['THN_AKADEMIK_SEMESTER']} {$ips['NM_SEMESTER']}', {$ips['IPS']}]");
    }
    $json_rows = "[".implode(",", $rows)."]";
//}
?>

<!--
You are free to copy and use this sample in accordance with the terms of the
Apache license (http://www.apache.org/licenses/LICENSE-2.0.html)
-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>
      Google Visualization API Sample
    </title>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load('visualization', '1', {packages: ['corechart']});
    </script>
    <script type="text/javascript">
      function drawVisualization() {
        // Create and populate the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'x');
        data.addColumn('number', 'IPS');
        data.addRows(<?php echo $json_rows; ?>);
       
        // Create and draw the visualization.
        new google.visualization.LineChart(document.getElementById('visualization')).
            draw(data, {curveType: "function",
                        width: 600, height: 400,
                        vAxis: {maxValue: 4},
                        title: 'Grafik IPS'
                        }
                );
      }
      

      google.setOnLoadCallback(drawVisualization);
    </script>
  </head>
  <body style="font-family: Arial;border: 0 none; margin: 0px; padding: 0px;">
    <div id="visualization" style="width: 600px; height: 400px;"></div>
  </body>
</html>
