<?php
	require('../../../config.php');


if($_POST["aksi"]=="tampil" and $_POST["semes"]) {
	$username = $user->USERNAME;
	$username_mhs = substr($username, 0, -2);

	// ambil id_mhs
	$result = $db->Query("select id_mhs,id_program_studi from mahasiswa where id_pengguna in (select id_pengguna from pengguna where username = '$username_mhs')")or die("salah kueri : 1");
	while($r = $db->FetchRow()) {
		$id_mhs = $r[0];
		$prodi = $r[1];
	}
	// ambil fakultas
	$result = $db->Query("select id_fakultas from program_studi where id_program_studi='".$prodi."'")or die("salah kueri : 1a");
	while($r = $db->FetchRow()) {
		$fakul = $r[0];
	}

	$idsemes = sprintf("%d",$_POST["semes"]);
	// ambil semester
	$kueri2 = "select thn_akademik_semester,nm_semester from semester where id_semester='".$idsemes."' ";
	$sem=""; $tahun="";
	$result = $db->Query($kueri2)or die("salah kueri : 2");
	while($r = $db->FetchRow()) {
		$tahun = $r[0]."/".($r[0]+1);
		$sem = $r[1];
	}
	$semester = $sem." ".$tahun." ";

	$isi = '
	<table class="ui-widget" style="width:100%">
	<tr class="ui-widget-header">
		<td colspan="4"><strong>Hasil Studi Semester '.$semester.'</strong></td>
	</tr>
	<tr class="ui-widget-header">
		<td><center><strong>No.</strong></center></td>
		<td><center><strong>Mata Ajar</strong></center></td>
		<td><center><strong>SKS</strong></center></td>
		<td><center><strong>Nilai</strong></center></td>
	</tr>
	';

	if($fakul=='1') {
		$param_fk = " and d.flagnilai='1' ";
	}else{
		$param_fk = "";
	}

	$kueriygmana = "0";
	$kueri = "select count(*) from pengambilan_mk where id_semester='".$idsemes."' and id_mhs='".$id_mhs."' and id_kelas_mk is not null";
	$result = $db->Query($kueri)or die("salah kueri : 3");
	while($r = $db->FetchRow()) {
		$kueriygmana = $r[0];
	}

	if($kueriygmana>0) {
		$kueri2 = "
		select a.kd_mata_kuliah,a.nm_mata_kuliah,e.kredit_semester,c.id_kelas_mk, d.nilai_angka, case when flagnilai=1 then d.nilai_huruf else '-' end, d.id_pengambilan_mk
		from mata_kuliah a, semester b, kelas_mk c, pengambilan_mk d, kurikulum_mk e
		where c.id_kurikulum_mk=e.id_kurikulum_mk and a.id_mata_kuliah=e.id_mata_kuliah and c.id_semester=b.id_semester and d.id_semester=b.id_semester and c.id_kelas_mk=d.id_kelas_mk
		and b.id_semester='".$idsemes."' and d.STATUS_APV_PENGAMBILAN_MK='1' and d.STATUS_PENGAMBILAN_MK!='4'
		and d.id_mhs='".$id_mhs."' ".$param_fk."
		";
	}else{
		$kueri2 = "
		select a.kd_mata_kuliah,a.nm_mata_kuliah,e.kredit_semester,'tes', d.nilai_angka, case when flagnilai=1 then d.nilai_huruf else '-' end, d.id_pengambilan_mk
		from mata_kuliah a, semester b, pengambilan_mk d, kurikulum_mk e
		where d.id_kurikulum_mk=e.id_kurikulum_mk and a.id_mata_kuliah=e.id_mata_kuliah and d.id_semester=b.id_semester
		and b.id_semester='".$idsemes."' and d.id_mhs='".$id_mhs."' ".$param_fk."
		";
	}
	
	$isi_tabel = ''; $ips_atas='0'; $ips_bawah='0';
	$bobot["A"] = 4; $bobot["AB"] = 3.5; $bobot["B"] = 3; $bobot["BC"] = 2.5; $bobot["C"] = 2; $bobot["D"] = 1; $bobot["E"] = 0;
	$result = $db->Query($kueri2)or die("salah kueri : 4");
	$i=1;
	while($r = $db->FetchRow()) {
		$ips_bawah += $r[2];
		$ips_atas += ($bobot[$r[5]]*$r[2]);
		
		if($r[5] == 'D'){
			$bg='yellow';
		} elseif($r[5] == 'E') {
			$bg='pink';
		} else {
			$bg='white';
		}
		
		$isi .= '
		<tr bgcolor="'.$bg.'">
			<td><center>'.$i.'.</center></td>
			<td>'.$r[0].' -- '.$r[1].'</td>
			<td><center>'.$r[2].'</center></td>
			<td><center>'.$r[5].'</center></td>
		</tr>
		';
	$i++;
	}

	if($ips_bawah>0) {
		$ips = number_format(($ips_atas/$ips_bawah),2);
	}else{
		$ips = '0.00';
	}

	// ambil IPK pada semester tsb
	$ipk="0.00";
	//$kueri2 = "select ipk_mhs_status from mhs_status where id_mhs='".$id_mhs."' and id_semester='".$idsemes."' ";
	$kueri2="select
	round(sum(a.kredit_semester*(case a.nilai_huruf
	when 'A' then 4
	when 'AB' then 3.5
	when 'B' then 3
	when 'BC' then 2.5
	when 'C' then 2
	when 'D' then 1
	end))/sum(a.kredit_semester),2) IPK
	from
	(
	select a.id_mhs,e.nm_mata_kuliah,d.kredit_semester kredit_semester,a.nilai_huruf from (
	select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
	from pengambilan_mk a
	left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
	left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
	where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.id_semester is not null and a.flagnilai=1 and a.status_hapus=0
	) a
	left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
	left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
	where rangking=1 and id_mhs='".$id_mhs."'
	) a
	left join mahasiswa b on a.id_mhs=b.id_mhs
	left join program_studi f on b.id_program_studi=f.id_program_studi
	where a.id_mhs='".$id_mhs."'
	group by a.id_mhs";
	$result = $db->Query($kueri2)or die("salah kueri : 5");
	while($r = $db->FetchRow()) {
		if($r[0] == null) {
			$ipk = "0.00";
		}else{
			$ipk = $r[0];
		}
	}

	$isi .= '
	</table>
	<p>IPS : '.$ips.'<br> IPK : '.$ipk.'</p>';

	echo $isi;

}
?>