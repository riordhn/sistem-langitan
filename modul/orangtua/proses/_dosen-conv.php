<?php

require('../../../config.php');
$db2 = new MyOracle();
$db3 = new MyOracle();

if($_POST["aksi"]=="tampil") {
	$isi = '
	<script>
	$(function() {
		$("#btnInboxHapus").button({
			icons: {
				primary: "ui-icon-trash",
			},
			text: true
		});
		
		$("#btnComposeKirim").button({
			icons: {
				primary: "ui-icon-check",
			},
			text: true
		});
	});
	</script>
	<form name="frmconv" id="frmconv">
	<table class="ui-widget" style="width:100%">
	';
	// ambil id_mhs
	$kueri = "
	select id_pesan,id_pengirim,id_penerima,to_char(waktu_pesan,'DD-MM-YYYY HH24:MI:SS'),judul_pesan,isi_pesan
	from pesan
	where ((id_penerima='".$user->ID_PENGGUNA."' and (hapus_penerima='0' or hapus_penerima is NULL)) or (id_pengirim='".$user->ID_PENGGUNA."' and (hapus_pengirim='0' or hapus_pengirim is NULL))) and (id_balasan='0' or id_balasan is NULL) 
	order by waktu_pesan desc
	";
	$pengirim_penerima = "";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		if($r[1]==$user->ID_PENGGUNA) {
			$pengirim_penerima = "Pesan dikirim kpd ";
			// ambil penerima
			$kueri2 = "select nm_pengguna from pengguna where id_pengguna='".$r[2]."'";
			$result2 = $db2->Query($kueri2)or die("salah kueri : ".$kueri2);
			while($r2 = $db2->FetchRow()) {
				//$nama_penerima = $r2[0];
				$tujuan = $r2[0];
			}
		}else if($r[2]==$user->ID_PENGGUNA) {
			$pengirim_penerima = "Pesan diterima dari ";
			// ambil pengirim
			$kueri2 = "select nm_pengguna from pengguna where id_pengguna='".$r[1]."'";
			$result2 = $db2->Query($kueri2)or die("salah kueri : ".$kueri2);
			while($r2 = $db2->FetchRow()) {
				//$nama_pengirim = $r2[0];
				$tujuan = $r2[0];
			}
		}
		$isi .= '
		<tr class="ui-widget-content">
			<td>
			<table class="ui-widget" style="width:100%">
			<tr>
				<td rowspan="4" style="border: 0px solid;" width="5%"><input type="checkbox" name="hapus[]" value="'.$r[0].'"></td>
				<td style="border: 0px solid;">'.$pengirim_penerima.' : '.$tujuan.'</td>
				<td style="border: 0px solid;" align=right>'.$r[3].'</td>
			</tr>
			<tr>
				<td colspan="2" style="border: 0px solid;"><b>'.$r[4].'</b></td>
			</tr>
			<tr>
				<td colspan="2" style="border: 0px solid;">'.nl2br($r[5]).'</td>
			</tr>
			<tr>
				<td colspan="2" style="border: 0px solid;">
				[<a href="conversation.php" onclick="hapustunggal(\''.$r[0].'\')">Hapus</a>]
				[<a href="conversation.php" onclick="conv_balas(\''.$r[0].'\')">Reply</a>]
				</td>
			</tr>
			';
			// ambil pesan reply
			$kueri2 = "
			select id_pesan,id_pengirim,id_penerima,to_char(waktu_pesan,'DD-MM-YYYY HH24:MI:SS'),judul_pesan,isi_pesan
			from pesan
			where ((id_penerima='".$user->ID_PENGGUNA."' and (hapus_penerima='0' or hapus_penerima is NULL)) or (id_pengirim='".$user->ID_PENGGUNA."' and (hapus_pengirim='0' or hapus_pengirim is NULL))) and (id_balasan='".$r[0]."' or id_balasan is NULL)
			order by waktu_pesan desc
			";
			$result2 = $db2->Query($kueri2)or die("salah kueri : ".$kueri2);
			while($r2 = $db2->FetchRow()) {
				if($r2[1]==$user->ID_PENGGUNA) {
					$pengirim_penerima = "Pesan dikirim kpd ";
					// ambil penerima
					$kueri3 = "select nm_pengguna from pengguna where id_pengguna='".$r2[2]."'";
					$result3 = $db3->Query($kueri3)or die("salah kueri : ".$kueri3);
					while($r3 = $db3->FetchRow()) {
						//$nama_penerima = $r2[0];
						$tujuan = $r3[0];
					}
				}else if($r2[2]==$user->ID_PENGGUNA) {
					$pengirim_penerima = "Pesan diterima dari ";
					// ambil pengirim
					$kueri3 = "select nm_pengguna from pengguna where id_pengguna='".$r2[1]."'";
					$result3 = $db3->Query($kueri3)or die("salah kueri : ".$kueri3);
					while($r3 = $db3->FetchRow()) {
						//$nama_pengirim = $r2[0];
						$tujuan = $r3[0];
					}
				}
				$isi .= '
				<tr>
					<td style="border: 0px solid;">&nbsp;</td>
					<td colspan="2" style="border: 0px solid; padding-left:25px;">
					'.$r2[3].'<br>
					<b>'.$r2[4].'</b><br>
					<b>'.$pengirim_penerima.' :</b> '.$tujuan.'<br>
					'.nl2br($r2[5]).'<br>
					[<a href="conversation.php" onclick="hapustunggalbalasan(\''.$r2[0].'\')">Hapus</a>]
					</td>
				</tr>
				';
			}
			$isi .= '
			</table>
			</td>
		</tr>
		';
	}
	$isi .= '
	<tr class="ui-widget-content"> 
		<td style="border: 0px solid;">
		<input type=hidden name="aksi" value="hapus">
		<div id="btnInboxHapus" onclick="hapus_kirim(\'frmconv\')">Hapus</div>
		<div id="btnComposeKirim" onclick="conv_baru()">Baru</div>
		</td>
	</tr>
	</table>
	</form>
	';
/*
	$isi .= '
	<tr class="ui-widget-content"> 
		<td style="border: 0px solid;">
		<input type=hidden name="aksi" value="hapus">
		<input type=button name="busek" value="Hapus" onclick="hapus_kirim(\'frmconv\')">
		<input type=button name="anyar" value="Baru" onclick="conv_baru()">
		</td>
	</tr>
	</table>
	</form>
	';
*/
	echo $isi;

}else if($_POST["aksi"]=="baru") {
	$kueri = "select id_mhs from mahasiswa where id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$id_mhs = $r[0];
	}
	$isi = '
		<form name="frmcompose" id="frmcompose">
		<table class="ui-widget" style="width:100%">
		<tr>
			<td class="clean">Kepada</td>
			<td>:</td>
			<td class="clean">
				<select name="kepada" id="kepada">
					<option value="0">------</option>
				';
				// hanya kepada dosen wali&pembimbing
				$id_dosen = "";
				$kueri = "select id_dosen from dosen_wali where id_mhs='".$id_mhs."' and rownum=1 order by id_semester desc";
				$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
				while($r = $db->FetchRow()) {
					$id_dosen .= ",'".$r[0]."'";
				}
				$kueri = "select id_dosen from pembimbing_ta where id_mhs='".$id_mhs."' and rownum=1 order by id_semester desc";
				$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
				while($r = $db->FetchRow()) {
					$id_dosen .= ",'".$r[0]."'";
				}
				$kueri = "
				select a.id_pengguna, b.nm_pengguna
				from dosen a, pengguna b
				where a.id_pengguna=b.id_pengguna and a.id_dosen in (".substr($id_dosen,1).")";
				$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
				while($r = $db->FetchRow()) {
					$isi .= '<option value="'.$r[0].'">'.$r[1].'</option>';
				}
				$isi .= '
				</select>
			</td>
		</tr>
		<tr>
			<td class="clean">Judul</td>
			<td class="clean">:</td>
			<td class="clean"><input type="text" name="judul"></td>
		</tr>
		<tr>
			<td class="clean">Pesan</td>
			<td class="clean">:</td>
			<td class="clean">
				<textarea name="pesan" rows="5" cols="40"></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="3" class="clean">
				<input type="hidden" name="aksi" value="barusimpan">
				<input type="button" name="ok" value="kirim" onclick="conv_baru_simpan(\'frmcompose\')">
			</td>
		</tr>
		</table>
		</form>
	';
	
	echo $isi;

}else if($_POST["aksi"]=="hapus"){
	$dihapus = $_POST["hapus"];
	//print_r($_POST);
	
	if(count($dihapus)>0) {
		for($a=0; $a<count($dihapus); $a++) {
			$kueri2 = "";
			$kueri = "select id_pengirim, id_penerima from pesan where id_pesan='".$dihapus[$a]."' ";
			$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
			while($r = $db->FetchRow()) {
				if($r[0]==$user->ID_PENGGUNA) {
					$kueri2 = "update pesan set hapus_pengirim='2' where id_pesan='".$dihapus[$a]."' ";
				}else if($r[1]==$user->ID_PENGGUNA) {
					$kueri2 = "update pesan set hapus_penerima='2' where id_pesan='".$dihapus[$a]."' ";
				}
			}
			$result = $db->Query($kueri2)or die("salah kueri : ".$kueri2);
			$kueri3 = "update pesan set hapus_penerima='2' where id_balasan='".$dihapus[$a]."' and id_penerima='".$user->ID_PENGGUNA."' ";
			$result = $db->Query($kueri3)or die("salah kueri : ".$kueri3);
			$kueri3 = "update pesan set hapus_pengirim='2' where id_balasan='".$dihapus[$a]."' and id_pengirim='".$user->ID_PENGGUNA."' ";
			$result = $db->Query($kueri3)or die("salah kueri : ".$kueri3);
		}
	}
	/*
	if($db->numRows() > 0) {
		echo 'pesan berhasil dihapus';
	}
	*/
	echo 'Proses selesai';

}else if($_POST["aksi"]=="hapustunggal"){
	//echo "sini";
	$kueri2 = "";
	$kueri = "select id_pengirim, id_penerima from pesan where id_pesan='".$_POST["ygdihapus"]."' ";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		if($r[0]==$user->ID_PENGGUNA) {
			$kueri2 = "update pesan set hapus_pengirim='2' where id_pesan='".$_POST["ygdihapus"]."' ";
		}else if($r[1]==$user->ID_PENGGUNA) {
			$kueri2 = "update pesan set hapus_penerima='2' where id_pesan='".$_POST["ygdihapus"]."' ";
		}
	}
	$result = $db->Query($kueri2)or die("salah kueri : ".$kueri2);

	$kueri3 = "update pesan set hapus_penerima='2' where id_balasan='".$_POST["ygdihapus"]."' and id_penerima='".$user->ID_PENGGUNA."' ";
	$result = $db->Query($kueri3)or die("salah kueri : ".$kueri3);
	$kueri3 = "update pesan set hapus_pengirim='2' where id_balasan='".$_POST["ygdihapus"]."' and id_pengirim='".$user->ID_PENGGUNA."' ";
	$result = $db->Query($kueri3)or die("salah kueri : ".$kueri3);

	echo "Proses selesai";
	/*
	if($db->numRows() > 0) {
		echo 'hapus berhasil';
	}
	*/
	

}else if($_POST["aksi"]=="hapustunggalbalas"){
	$kueri2 = "";
	$kueri = "select id_pengirim, id_penerima from pesan where id_pesan='".$_POST["ygdihapus"]."' ";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		if($r[0]==$user->ID_PENGGUNA) {
			$kueri2 = "update pesan set hapus_pengirim='2' where id_pesan='".$_POST["ygdihapus"]."' ";
		}else if($r[1]==$user->ID_PENGGUNA) {
			$kueri2 = "update pesan set hapus_penerima='2' where id_pesan='".$_POST["ygdihapus"]."' ";
		}
	}
	$result = $db->Query($kueri2)or die("salah kueri : ".$kueri2);
	if($db->numRows() > 0) {
		echo 'hapus berhasil';
	}

}else if($_POST["aksi"]=="convbalas" and $_POST["det"]) {
	// ambil id_pengirim pesan
	$pengirim=""; $judul=""; $idpesan='0';
	$kueri = "select id_pengirim,judul_pesan,id_pesan from pesan where id_pesan='".$_POST["det"]."'";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$pengirim = $r[0];
		$judul = $r[1];
		$idpesan = $r[2];
	}

	$nm_pengguna = "";
	$kueri = "select nm_pengguna from pengguna where id_pengguna='".$pengirim."'";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$nm_pengguna = $r[0];
	}

	$isi = '
		<form name="frmbalasconv" id="frmbalasconv">
		<table class="ui-widget" style="width:100%">
		<tr>
			<td class="clean">Kepada</td>
			<td>:</td>
			<td class="clean">'.$nm_pengguna.'</td>
		</tr>
		<tr>
			<td class="clean">Judul</td>
			<td class="clean">:</td>
			<td class="clean"><input type="text" name="judul" value="re: '.$judul.'"></td>
		</tr>
		<tr>
			<td class="clean">Pesan</td>
			<td class="clean">:</td>
			<td class="clean">
				<textarea name="pesan" rows="5" cols="40"></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="3" class="clean">
				<input type="hidden" name="aksi" value="balasconvsimpan">
				<input type="hidden" name="kepada" value="'.$idpesan.'">
				<input type="button" name="ok" value="Posting" onclick="conv_balas_simpan(\'frmbalasconv\')">
			</td>
		</tr>
		</table>
		</form>
	';
	echo $isi;

}else if($_POST["aksi"]=="barusimpan" and $_POST["judul"] and $_POST["pesan"] and $_POST["kepada"] ) {
	//print_r($_POST);

	$kueri = "INSERT INTO pesan (JUDUL_PESAN, ISI_PESAN, TIPE_PESAN, ID_PENGIRIM, ID_PENERIMA, ID_BALASAN, WAKTU_PESAN, BACA_PENGIRIM, HAPUS_PENGIRIM ) VALUES ('".$_POST["judul"]."', '".$_POST["pesan"]."', 'conv', '".$user->ID_PENGGUNA."','".$_POST["kepada"]."', '', to_date('".date("d-m-Y")."','DD-MM-YYYY'), '0','0')";
	$result =  $db->Query($kueri)or die("salah kueri : ".$kueri);
	if($db->NumRows() >0) {
		echo 'Input berhasil';
	}

}else if($_POST["aksi"]=="balasconvsimpan" and $_POST["judul"] and $_POST["pesan"] and $_POST["kepada"] ) {
	//print_r($_POST);

	// ambil id_pengirim pesan
	$kueri = "select id_pengirim from pesan where id_pesan='".$_POST["kepada"]."'";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$pengirim = $r[0];
	}

	$kueri = "INSERT INTO pesan (JUDUL_PESAN, ISI_PESAN, TIPE_PESAN, ID_PENGIRIM, ID_PENERIMA, ID_BALASAN, WAKTU_PESAN, BACA_PENGIRIM, HAPUS_PENGIRIM ) VALUES ('".$_POST["judul"]."', '".$_POST["pesan"]."', 'conv', '".$user->ID_PENGGUNA."','".$pengirim."', '".$_POST["kepada"]."', to_date('".date("d-m-Y")."','DD-MM-YYYY'), '0','0')";
	$result =  $db->Query($kueri)or die("salah kueri : ".$kueri);
	if($db->NumRows() >0) {
		echo 'Reply berhasil';
	}

}

?>
