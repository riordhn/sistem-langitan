<?php

	include '../config.php';
	$id = $_GET['id'];
	
	$nama_lengkap_ayah = "";
	$pendidikan_ayah = "";
	$pekerjaan_ayah = "";
	$alamat_ayah = "";
	
	$nama_lengkap_ibu = "";
	$pendidikan_ibu = "";
	$pekerjaan_ibu = "";
	$alamat_ibu = "";
	
	$nm_mhs = "";
	$username = "";
	$kelamin_pengguna = "";
	$tgl_lahir_pengguna = "";
	$tempat_lahir = "";
	$alamat_asal_mhs_kota = ""; 
	$mobile_mhs = "";
	$agama_mhs = "";
	$email_pengguna = ""; 
	$blog_pengguna = "";
	$lulus_dayat = "";
	$nilai_skhun_mhs = "";
	$cita_cita_mhs = "";
	$fakultas_mhs = "";
	$nm_program_studi = "";
	
	$db->query("select mhs.NM_AYAH_MHS, 
				mhs.ALAMAT_AYAH_MHS, 
				payah.nama_pendidikan_akhir as PENDIDIKAN_AYAH_MHS, 
				krj_ayah.nm_pekerjaan as PEKERJAAN_AYAH_MHS, 
				mhs.NM_IBU_MHS, 
				mhs.ALAMAT_IBU_MHS, 
				pabu.nama_pendidikan_akhir as PENDIDIKAN_IBU_MHS, 
				krj_IBU.nm_pekerjaan as PEKERJAAN_IBU_MHS,
				p.NM_PENGGUNA, 
				p.USERNAME, 
				p.KELAMIN_PENGGUNA, 
				to_char(p.TGL_LAHIR_PENGGUNA, 'DD-MM-YYYY'), 
				PROV.NM_PROVINSI, 
				KOTA.NM_KOTA, mhs.ALAMAT_ASAL_MHS_KOTA, 
				mhs.MOBILE_MHS, 
				a.nm_agama as agama_mhs, 
				p.EMAIL_PENGGUNA, 
				p.BLOG_PENGGUNA, 
				mhs.LULUS_DAYAT, 
				MHS.NILAI_SKHUN_MHS, 
				mhs.CITA_CITA_MHS, 
				f.nm_fakultas as fakultas_mhs, 
				ps.nm_program_studi
				from pengguna p
				left join mahasiswa mhs on mhs.ID_PENGGUNA = p.ID_PENGGUNA
				left join PENDIDIKAN_AKHIR payah on payah.id_pendidikan_akhir = mhs.PENDIDIKAN_AYAH_MHS
				left join PENDIDIKAN_AKHIR pabu on pabu.id_pendidikan_akhir = mhs.PENDIDIKAN_IBU_MHS
				left join PEKERJAAN krj_ayah on KRJ_AYAH.ID_PEKERJAAN = mhs.PEKERJAAN_AYAH_MHS
				left join PEKERJAAN krj_ibu on KRJ_IBU.ID_PEKERJAAN = mhs.PEKERJAAN_IBU_MHS
				left join agama a on a.ID_AGAMA = p.ID_AGAMA
				left join PROGRAM_STUDI ps on ps.ID_PROGRAM_STUDI = mhs.ID_PROGRAM_STUDI
				left join fakultas f on f.id_fakultas = ps.ID_FAKULTAS
				left join PROVINSI prov on PROV.ID_PROVINSI = MHS.LAHIR_PROP_MHS
				left join kota kota on kota.ID_KOTA = mhs.LAHIR_KOTA_MHS
				where p.username = '$id'
	
	");
	
	
	$i = 0;
	$jml_data = 0;
	while ($row = $db->fetchrow()){
		$nama_lengkap_ayah[$i] = $row[0];
		$alamat_ayah[$i] = $row[1];
		$pendidikan_ayah[$i] = $row[2];
		$pekerjaan_ayah[$i] = $row[3];

		
		$nama_lengkap_ibu[$i] = $row[4];
		$alamat_ibu[$i] = $row[5];
		$pendidikan_ibu[$i] = $row[6];
		$pekerjaan_ibu[$i] = $row[7];
		
		$nm_mhs[$i] = $row[8];
		$username[$i] = $row[9];
		$kelamin_pengguna[$i] = $row[10];
		$tgl_lahir_pengguna[$i] = $row[11];
		$tempat_lahir_prov[$i] = $row[12];
		$tempat_lahir_kota[$i] = $row[13];
		$alamat_asal_mhs_kota[$i] = $row[14]; 
		$mobile_mhs[$i] = $row[15];
		$agama_mhs[$i] = $row[16];
		$email_pengguna[$i] = $row[17]; 
		$blog_pengguna[$i] = $row[18];
		$lulus_dayat[$i] = $row[19];
		$nilai_skhun_mhs[$i] = $row[20];
		$cita_cita_mhs[$i] = $row[21];
		$fakultas_mhs[$i] = $row[22];
		$nm_program_studi[$i] = $row[23];
		
		$jml_data++;
		$i++;
	}
	
		header("content-type: application/json");
		// data should be written out as json.
		
		//echo '[';
		for ( $i = 0; $i < $jml_data; $i++ ) {
			if ( $i ) {
				echo ",";
			}
			echo '{"nama_ayah":"' . $nama_lengkap_ayah[$i] . '", 
				   "pendidikan_ayah":"' . $pendidikan_ayah[$i] . '", 
				   "pekerjaan_ayah":"' . $pekerjaan_ayah[$i] . '", 
				   "alamat_ayah":"' . $alamat_ayah[$i] . '", 
				   "nama_ibu":"' . $nama_lengkap_ibu[$i] . '", 
				   "pendidikan_ibu":"' . $pendidikan_ibu[$i] . '", 
				   "pekerjaan_ibu":"' . $pekerjaan_ibu[$i] . '", 
				   "alamat_ibu":"' . $alamat_ibu[$i] . '",
				   "nm_mhs":"' . $nm_mhs[$i] . '",
				   "username":"' . $username[$i] . '",
				   "kelamin_pengguna":"' . $kelamin_pengguna[$i] . '",
				   "tgl_lahir_pengguna":"' . $tgl_lahir_pengguna[$i] . '",
				   "tempat_lahir_prov":"' . $tempat_lahir_prov[$i] . '",
				   "tempat_lahir_kota":"' . $tempat_lahir_kota[$i] . '",
				   "alamat_asal_mhs_kota":"' . $alamat_asal_mhs_kota[$i] . '", 
				   "mobile_mhs":"' . $mobile_mhs[$i] . '",
				   "agama_mhs":"' . $agama_mhs[$i] . '",
				   "email_pengguna":"' . $email_pengguna[$i] . '", 
				   "blog_pengguna":"' . $blog_pengguna[$i] . '",
				   "lulus_dayat":"' . $lulus_dayat[$i] . '",
				   "nilai_skhun_mhs":"' . $nilai_skhun_mhs[$i] . '",
				   "cita_cita_mhs":"' . $cita_cita_mhs[$i] . '",
				   "fakultas_mhs":"' . $fakultas_mhs[$i] . '",
				   "nm_program_studi":"' . $nm_program_studi[$i] . '"
				   }';
		}
		//echo "]";
?>