<?php
	// ambil nama di tabel pengguna
	include 'config.php';
	$db2 = new MyOracle();
	$username = $user->USERNAME;
	$username_mhs = substr($username, 0, -2);
	$mhs_nama = "";
	$kueri = "select NM_PENGGUNA from pengguna where username = '$username_mhs'";

	$result = $db->Query($kueri)or die("salah kueri : ");
	while($r = $db->FetchRow()) {
		$mhs_nama = $r[0];
	}

	// ambil id_mhs
	$id_mhs=""; $mhs_nim="";
	$kueri = "
	select a.id_mhs, a.NIM_MHS, c.nm_fakultas, b.nm_program_studi, d.nm_jenjang, a.TGL_LULUS_MHS, a.TGL_TERDAFTAR_MHS, a.no_ijazah, a.id_program_studi
	from mahasiswa a, program_studi b, fakultas c, jenjang d
	where a.id_program_studi=b.id_program_studi and b.id_fakultas=c.id_fakultas and b.id_jenjang=d.id_jenjang and a.id_pengguna in (select id_pengguna from pengguna where username = '$username_mhs')";
	$result = $db->Query($kueri)or die("salah kueri : ");
	while($r = $db->FetchRow()) {
		$id_mhs = $r[0];
		$mhs_nim = $r[1];
		$mhs_fakul = $r[2];
		$mhs_prodi = $r[3];
		$mhs_jenjang = $r[4];
		$mhs_tgl_lulus = $r[5];
		$mhs_tgl_terdaftar = $r[6];
		$mhs_noijazah = $r[7];
		$mhs_prodi_id = $r[8];
	}

	$jum_sks=0; $jum_bobot=0; $ipk=0;
	// data lama
	$kueri = "
	select a.id_semester,c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai
	from pengambilan_mk a, mata_kuliah c, kurikulum_mk d
	where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_mhs='".$id_mhs."' and a.STATUS_APV_PENGAMBILAN_MK='1'
	order by a.id_semester,c.kd_mata_kuliah
	";
	// pakai kueri ini untuk mengambil nilai terbaik
		/*
		select c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,Min(a.nilai_huruf)
		from aucc.pengambilan_mk a, aucc.mata_kuliah c, aucc.kurikulum_mk d
		where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah
		and a.id_mhs='10383' and d.id_program_studi='66'
		group by c.kd_mata_kuliah, c.nm_mata_kuliah, d.kredit_semester
		order by c.kd_mata_kuliah
		*/
	$result = $db->Query($kueri)or die("salah kueri : ");
	while($r = $db->FetchRow()) {
		if($r[5]=='1') { // fileter FLAGNILAI
			// ambil bobot nilai huruf
			$bobot=0;
			$kueri2 = "select NILAI_STANDAR_NILAI from standar_nilai where NM_STANDAR_NILAI='".$r[4]."'";
			$result2 = $db2->Query($kueri2)or die("salah kueri : ");
			while($r2 = $db2->FetchRow()) {
				$bobot = $r2[0];
			}
			$jum_sks += $r[3];
			$jum_bobot += ($bobot*$r[3]);
		}
	}
	/*
	// data baru
/*
		$kueri = "
		select a.id_semester,c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf
		from pengambilan_mk a, kelas_mk b, mata_kuliah c, kurikulum_mk d
		where a.id_kelas_mk=b.id_kelas_mk and b.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_mhs='".$id_mhs."' and a.STATUS_APV_PENGAMBILAN_MK='1' and (a.STATUS_PENGAMBILAN_MK!='4' or a.STATUS_PENGAMBILAN_MK is null) and a.nilai_huruf is not null
		order by a.id_semester,c.kd_mata_kuliah
		";
		$result = $db->Query($kueri)or die("salah kueri : ");
		while($r = $db->FetchRow()) {
			// ambil bobot nilai huruf
			$bobot=0;
			$kueri2 = "select NILAI_STANDAR_NILAI from standar_nilai where NM_STANDAR_NILAI='".$r[4]."'";
			$result2 = $db2->Query($kueri2)or die("salah kueri : ");
			while($r2 = $db2->FetchRow()) {
				$bobot = $r2[0];
			}
			$jum_sks += $r[3];
			$jum_bobot += ($bobot*$r[3]);
		}

	if($jum_sks==0) {
		$ipk='0.00';
	}else{
		$ipk = number_format(($jum_bobot/$jum_sks),2);
	}
*/
// ipk versi lukman, ngulang diambil nilai terbaik.
	$sql="select a.id_mhs,sum(a.kredit_semester) skstotal,
round(sum(a.kredit_semester*(case a.nilai_huruf
when 'A' then 4
when 'AB' then 3.5
when 'B' then 3
when 'BC' then 2.5
when 'C' then 2
when 'D' then 1
end))/sum(a.kredit_semester),2) IPK
from
(
select a.id_mhs,e.nm_mata_kuliah,d.kredit_semester kredit_semester,a.nilai_huruf from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.id_semester is not null and a.flagnilai=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where rangking=1 and id_mhs='{$id_mhs}'
) a
left join mahasiswa b on a.id_mhs=b.id_mhs
left join program_studi f on b.id_program_studi=f.id_program_studi
where a.id_mhs='{$id_mhs}'
group by a.id_mhs order by a.id_mhs";

// ngulang -> nilai terakhir yg berlaku
/*
if($user->ID_FAKULTAS==10 or $user->ID_FAKULTAS==11){
$sql="select a.id_mhs,sum(a.kredit_semester) skstotal,
round(sum(a.kredit_semester*(case a.nilai_huruf
when 'A' then 4
when 'AB' then 3.5
when 'B' then 3
when 'BC' then 2.5
when 'C' then 2
when 'D' then 1
end))/sum(a.kredit_semester),2) IPK
from
(
select a.id_mhs,e.nm_mata_kuliah,d.kredit_semester kredit_semester,a.nilai_huruf from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester desc,sm.nm_semester desc) rangking
from pengambilan_mk a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join semester sm on sm.id_semester=a.id_semester
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.flagnilai=1 and a.id_semester is not null and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where rangking=1 and id_mhs='{$id_mhs}'
) a
left join mahasiswa b on a.id_mhs=b.id_mhs
left join program_studi f on b.id_program_studi=f.id_program_studi
where a.id_mhs='{$id_mhs}'
group by a.id_mhs order by a.id_mhs";
}
*/

$result2 = $db2->Query($sql);
			while($r2 = $db2->FetchRow()) {
				$jum_sks = $r2[1];
				$ipk = $r2[2];
			}


	$isi_transkrip = '
		<table cellspacing="0" cellpadding="0" style="width:100%;">
		<tr style="border:none;">
			<td><center></center></td>
			<td>
			UNIVERSITAS AIRLANGGA<BR>
			FAKULTAS '.strtoupper($mhs_fakul).'<BR>
			Kampus C Mulyorejo Surabaya 60115<br>
			Telp. (031) 5914042, 5914043, 5912564 Fax (031) 5981841<br>
			Website : http://www.unair.ac.id; e-mail:rektor@unair.ac.id
			</td>
		</tr>
		</table>
		<table cellspacing="0" cellpadding="0" style="width:100%;">
		<tbody>
		<tr>
			<td>Nama</td>
			<td><strong>'.$mhs_nama.'</strong></td>
			<td>&nbsp;</td>
			<td>Nomor Ijazah</td>
			<td><strong></strong></td>
		</tr>
		<tr>
			<td>NIM</td>
			<td><strong>'.$mhs_nim.'</strong></td>
			<td>&nbsp;</td>
			<td>Jumlah SKS</td>
			<td><strong>'.$jum_sks.'</strong></td>
		 </tr>
		<tr>
			<td>Program Studi</td>
			<td><strong>'.$mhs_jenjang.' - '.$mhs_prodi.'</strong></td>
			<td>&nbsp;</td>
			<td>IP Kumulatif</td>
			<td><strong>'.$ipk.'</strong></td>
		</tr>
		<tr>
			<td width="32%">Tanggal Terdaftar Pertama Kali</td>
			<td width="25%"><strong>'.$mhs_tgl_terdaftar.'</strong></td>
			<td width="7%">&nbsp;</td>
			<td width="16%">Tanggal Lulus</td>
			<td width="20%"><strong></strong></td>
		</tr>
		</tbody>
		</table>

		<table cellspacing="0" cellpadding="0" border="0" width="100%" style="width:100%;">
		<tbody>
		<tr class="ui-widget-header">
			<td><center><strong>Semester</strong></center></td>
			<td><center><strong>Kode MA</strong></center></td>
			<td><center><strong>Nama Mata Ajar</strong></center></td>
			<td><center><strong>SKS</strong></center></td>
			<td><center><strong>Nilai</strong></center></td>
			<td><center><strong>Bobot</strong></center></td>
			<!--
			<td bgcolor="#333333" width="14%"><font color="#FFFFFF">Semester</font></td>
			<td bgcolor="#333333" width="12%"><font color="#FFFFFF">Kode MA</font></td>
			<td bgcolor="#333333" width="53%"><font color="#FFFFFF">Nama Mata Ajar </font></td>
			<td bgcolor="#333333" width="7%"><font color="#FFFFFF">SKS</font> </td>
			<td bgcolor="#333333" width="7%"><font color="#FFFFFF">Nilai</font></td>
			<td bgcolor="#333333" width="7%"><font color="#FFFFFF">Bobot</font></td>
			-->
		</tr>
	';
	$jum_sks=0; $jum_bobot=0; $ipk=0;
	// data lama
	$kueri = "
	select e.tahun_ajaran, e.nm_semester, c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf asc,e.thn_akademik_semester desc,e.nm_semester desc) rangking,count(*) over(partition by c.nm_mata_kuliah) terulang,c.status_praktikum,a.status_hapus
	from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e
	where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester and a.id_mhs='".$id_mhs."' and a.STATUS_APV_PENGAMBILAN_MK='1'
	order by e.tahun_ajaran desc, e.nm_semester desc, c.kd_mata_kuliah
	";

if($user->ID_FAKULTAS==10){
	$kueri = "
	select e.tahun_ajaran, e.nm_semester, c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by e.thn_akademik_semester desc,e.nm_semester desc) rangking,count(*) over(partition by c.nm_mata_kuliah) terulang,c.status_praktikum,a.status_hapus
	from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e
	where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester and a.id_mhs='".$id_mhs."' and a.STATUS_APV_PENGAMBILAN_MK='1'
	order by e.tahun_ajaran desc, e.nm_semester desc, c.kd_mata_kuliah
	";
}
	$result = $db->Query($kueri)or die("salah kueri1 : ");
	while($r = $db->FetchRow()) {
		if($r[6]=='1') { // filter FLAGNILAI
			$semester = $r[0].' - '.$r[1];

			// ambil bobot nilai huruf
			$bobot=0;
			$kueri2 = "select NILAI_STANDAR_NILAI from standar_nilai where NM_STANDAR_NILAI='".trim($r[5])."'";
			$result2 = $db2->Query($kueri2)or die("salah kueri : ");
			while($r2 = $db2->FetchRow()) {
				$bobot = $r2[0];
			}
			if($r[7]==1 and $r[8]>1)$warna='#00FF00';
			else {
				if($r[7]>1)$warna='#FF0000';
				else {
					if($r[5]=='-')$warna='yellow';
					$warna='#FFFFFF';
				}
			}
			if(($r[5]=='E' or $r[5]=='') and ($r[7]==1 or $r[9]>1) ){
				$warna='#cccccc';
			}
			if($r[10]=='1' ){
				$warna='#772244';
			}
			
			$isi_transkrip .= '
				<tr bgcolor="'.$warna.'">
					<td><center>'.$semester.'</center></td>
					<td><center>'.$r[2].'</center></td>
					<td>'.$r[3].'</td>
					<td><center>'.$r[4].'</center></td>
					<td><center>'.$r[5].'</center></td>
					<td><center>'.($bobot*$r[4]).'</center></td>
				</tr>
			';
			if($r[5]<'E' and $r[5]!='' and $r[5]!='-' and $r[7]==1 and $r[10]==0 ){
				$jum_sks += $r[4];
				$jum_bobot += ($bobot*$r[4]);
			}
		}else{
			$semester = $r[0].' '.$r[1];
			$isi_transkrip .= '
				<tr bgcolor="yellow">
					<td><center>'.$semester.'</center></td>
					<td><center>'.$r[2].'</center></td>
					<td>'.$r[3].'</td>
					<td><center>'.$r[4].'</center></td>
					<td><center>-</center></td>
					<td><center>-</center></td>
				</tr>
			';
		}
	}

	/*
	// data baru
		$kueri = "
		select a.id_semester,c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf
		from pengambilan_mk a, kelas_mk b, mata_kuliah c, kurikulum_mk d
		where a.id_kelas_mk=b.id_kelas_mk and b.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_mhs='".$id_mhs."' and a.STATUS_APV_PENGAMBILAN_MK='1' and a.STATUS_PENGAMBILAN_MK!='4' and a.nilai_huruf is not null
		order by a.id_semester,c.kd_mata_kuliah
		";
		$result = $db->Query($kueri)or die("salah kueri : ");
		while($r = $db->FetchRow()) {
			// ambil nama semes
			$kueri2 = "select tahun_ajaran,nm_semester from semester where id_semester='".$r[0]."'";
			$result2 = $db2->Query($kueri2)or die("salah kueri : ");
			while($r2 = $db2->FetchRow()) {
				$semester = $r2[0].' '.$r2[1];
			}
			// ambil bobot nilai huruf
			$bobot=0;
			$kueri2 = "select NILAI_STANDAR_NILAI from standar_nilai where NM_STANDAR_NILAI='".$r[4]."'";
			$result2 = $db2->Query($kueri2)or die("salah kueri : ");
			while($r2 = $db2->FetchRow()) {
				$bobot = $r2[0];
			}
			$isi_transkrip .= '
				<tr>
					<td>'.$semester.'</td>
					<td>'.$r[1].'</td>
					<td>'.$r[2].'</td>
					<td>'.$r[3].'</td>
					<td>'.$r[4].'</td>
					<td>'.($bobot*$r[3]).'</td>
				</tr>
			';
			$jum_sks += $r[3];
			$jum_bobot += ($bobot*$r[3]);
		}
	*/
	if($jum_sks==0) {
		$ipk='0.00';
	}else{
		$ipk = number_format(($jum_bobot/$jum_sks),2);
	}

	$isi_transkrip .= '
		<tr>
			<td rowspan="3" colspan="2"></td>
			<td><div align="left"><strong>Jumlah SKS dan Bobot<strong></div></td>
			<td><center><strong>'.$jum_sks.'</strong></center></td>
			<td>&nbsp;</td>
			<td><center><strong>'.$jum_bobot.'</strong></center></td>
		</tr>
		<tr>
			<td><strong>IP kumulatif</strong></td>
			<td colspan="3"><div><center><strong>'.$ipk.'</strong></center></div></td>
		</tr>
		<tr>
			<td><strong>Predikat Kelulusan</strong></td>
			<td colspan="3"><div><center><strong>&nbsp;</strong></center></div></td>
		</tr>
		</tbody>
		</table>
		Keterangan: <br>
		<span>Putih</span> : Normal, masuk transkrip dan perhitungan IP kumulatif dan sks kumulatif.<br>
		<span style="background-color:green">Hijau</span> : Ulangan, masuk transkrip dan perhitungan IP kumulatif dan sks kumulatif.<br>
		<span style="background-color:yellow">Kuning</span> : Nilai belum dikeluarkan, tidak masuk transkrip dan tidak masuk perhitungan IP kumulatif dan sks kumulatif.<br>
		<span style="background-color:red">Merah</span> : Sudah diulang, tidak masuk transkrip dan tidak masuk perhitungan IP kumulatif dan sks kumulatif.<br>
		<span style="background-color:#cccccc">Abu-abu</span> : PKL,KKN,TA,PROPOSAL,SEMINAR,SKRIPSI,THESIS yang belum masuk nilainya. Tidak masuk perhitungan IPK dan sks kumulatif.<br>
		<span style="background-color:#772244">Ungu</span> : MK Pilihan yang dihapus oleh akademik atas permintaan mahasiswa. Tidak masuk perhitungan IPK dan sks kumulatif.<br>
	';
	//<input type=button name="dicetak" value="Cetak" onclick="window.open(\'proses/_akademik-transkrip_cetak.php\',\'baru2\');">
	$grafiknilai = file_get_contents('grafik-nilai.php');
	$smarty->assign('grafik-nilai', $grafiknilai);
	$smarty->assign('isitranskrip', $isi_transkrip);

	$smarty->display('transkrip.tpl');
?>