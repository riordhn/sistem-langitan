<?php
require_once('config.php');

$username = $user->USERNAME;
$mhs_nim = substr($username, 0, -2);


	$tampil_data = '
	<br>
	<table class="ui-widget-content">
	<tr class="ui-widget-header">
		<td align=center>Semester</td>
		<td align=center>Status</td>
		<td align=center>Nomer</td>
		<td align=center>Tanggal</td>
		<td align=center>Keterangan</td>
	</tr>
	';

	$kueri = "
	SELECT 	NM_STATUS_PENGGUNA, THN_AKADEMIK_SEMESTER, NM_SEMESTER, COALESCE(ADMISI.NO_SK, ADMISI.NO_IJASAH, '') as NO_SK, COALESCE(TGL_SK, ADMISI.TGL_LULUS, NULL) as TGL_SK, 
				TGL_USULAN, TGL_APV, KETERANGAN, KURUN_WAKTU, ALASAN
				FROM MAHASISWA 
				LEFT JOIN PENGGUNA ON MAHASISWA.ID_PENGGUNA = PENGGUNA.ID_PENGGUNA   
				LEFT JOIN PROGRAM_STUDI ON MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
				LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS 
				LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
				LEFT JOIN ADMISI ON MAHASISWA.ID_MHS = ADMISI.ID_MHS
				LEFT JOIN SEMESTER ON ADMISI.ID_SEMESTER = SEMESTER.ID_SEMESTER
				LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = ADMISI.STATUS_AKD_MHS 
				WHERE MAHASISWA.NIM_MHS = '$mhs_nim' AND STATUS_APV = 1
				ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC
	";
	
	$result = $db->Query($kueri)or die("salah kueri : 35");
	while($r = $db->FetchArray()) {
		$tampil_data .= '
		<tr>
			<td>'.$r[1]. ' / ' . $r[2] .'</td>
			<td>'.$r[0].'</td>
			<td>'.$r[3].'</td>
			<td>'.$r[4].'</td>
			<td>'.$r[9].'</td>
		</tr>
		';
	}

	$smarty->assign('tampil_data', $tampil_data);

	$smarty->display('akademik-status.tpl');

?>
