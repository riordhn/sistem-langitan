<!-- author Sani Iman Pribadi -->

<script>
	$(function() {
		$("#btnComposeKirim").button({
			icons: {
				primary: "ui-icon-check",
			},
			text: true
		});
		
		$("#btnComposeKirim").click(function(){
			$('#frmCompose').submit();
		});
		
		$('#frmCompose').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {	
							$('#compose').load('compose.php');					
						}
					})
					return false;
		});
		
	});
</script>

<div id="compose">
<div class="center_title_bar">Pesan Baru</div>
<form id="frmCompose" name="frmCompose" action="proses/pesan-kirim.php">
<table class="ui-widget" style="width:100%">
	<tr class="ui-widget-content">
		<td>Kepada</td>
		<td>
			<select>
				<optgroup label="Dosen Wali">
					<option value="{$id_doli}">{$nm_doli}</option>
					<input type="hidden" name="id_penerima" value="{$id_doli}" />
				</optgroup>
			</select>
		</td>
	</tr>
	<tr class="ui-widget-content">
		<td>Judul</td><td><textarea cols="80" rows="1" name="judul_pesan"></textarea></td>
	</tr>
	<tr class="ui-widget-content">
		<td valign="top" style="text-align:top;">Pesan</td>
		<td><textarea cols="80" rows="6" name="isi_pesan"></textarea></td>
	</tr>
	<tr class="ui-widget-content">
		<td colspan="2">
			<div id="btnComposeKirim">Kirim</div>
		</td>
	</tr>
</table>
</form>
</div>