<html>
    <head>
        <title>Orang Tua Mahasiswa - Universitas Airlangga</title>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" type="text/css" href="../../css/reset.css" />
        <link rel="stylesheet" type="text/css" href="../../css/text.css" />
        <link rel="stylesheet" type="text/css" href="../../css/orangtua.css" />
        <link rel="stylesheet" type="text/css" href="../../css/jquery-ui-custom-keu.css" />



        <link rel="shortcut icon" href="../../img/icon.ico" />

        <script type="text/javascript" src="../../js/jquery-1.5.1.js"></script>

        <script language="JavaScript" src="../../js/FusionCharts.js"></script>
        <!--
<script type="text/javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
        -->
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>

        <script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="../../js/jquery.price_format.1.4.js"></script>
        <script src="../../js/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="../../js/amcharts/raphael.js" type="text/javascript"></script>
        <script type="text/javascript">var defaultRel = 'biodata'; var defaultPage = 'biodata.php';</script>

        <script type="text/javascript" src="../../js/cybercampus.ajax-1.0.js"></script>

        <!--
        <script language="javascript" src="../../js/aucc.js"></script>
        -->
        {literal}
            <style>
                .content .center_title_bar{
                    background: none repeat scroll 0 0 #4a4314;
                    border-radius: 5px 5px 5px 5px;
                    color: #FFFFFF;
                    font-size: 14px;
                    font-weight: bold;
                    margin: 0 -5px 20px;
                    padding: 10px;
                    text-transform: uppercase;
                }
                .center{
                    text-align: center
                }
                .ui-widget-header{
                    font-size: 1.2em;
                }
                .content tr.ui-widget-header td{
                    padding: 10px;
                    font-weight: bold;
                }
                .menu {
                    height: 425px;
                    padding: 5px 5px 5px 5px;
                    width: 200px;
                }
                .menu ul {
                    list-style: none;
                    margin-bottom: 0px;
                    margin-left: 15px;
                }
                .menu ul li {
                    margin-left: 0px;
                }
                .menu ul li a {
                    background: url('../../img/list-menu-right.png') 5px 5px no-repeat;
                    border-bottom: 2px dotted #ccc;
                    color: #000;
                    display: block;
                    height: 22px;
                    margin: 5px 0px 5px 0px;
                    padding: 5px 0px 0px 25px;
                    text-decoration: none;
                }
                .menu ul li a:hover {
                    background: url('../../img/list-menu-right-hover.png') 5px 5px no-repeat;
                    color: green;
                }
            </style>
        {/literal}
    </head>
    <body>
        <table class="clear-margin-bottom">
            <colgroup>
                <col />
                <col class="main-width"/>
                <col />
            </colgroup>
            <thead>
                <tr>
                    <td class="header-left"></td>
                    <td class="header-center"></td>
                    <td class="header-right"></td>
                </tr>
                <tr>
                    <td class="tab-left"></td>
                    <td class="tab-center">
                        <ul>
                            <!-- Untuk Menampilkan Menu Utama  -->
                            {foreach $modul_set as $m}
                                {if $m.AKSES == 1}
                                    <li><a href="#{$m.NM_MODUL}!{$m.PAGE}" class="nav">{$m.TITLE}</a></li>
                                    <li class="divider"></li>
                                {/if}
                            {/foreach}
                            <li><a class="disable-ajax" href="../../logout.php">Logout</a></li>
                        </ul>
                    </td>
                    <td class="tab-right"></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="body-left">&nbsp;</td>
                    <td class="body-center">
                        <table class="content-table">
                            <colgroup>
                                <col />
                                <col />
                            </colgroup>
                            <tr>
                                <td colspan="2" id="breadcrumbs" class="breadcrumbs" ></td>
                            </tr>
                            <tr>
                                <td id="menu" class="menu"></td>
                                <td id="content" class="content">Loading data...</td>
                            </tr>
                        </table>
                    </td>
                    <td class="body-right">&nbsp;</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td class="foot-left">&nbsp;</td>
                    <td class="foot-center">
                        <div class="footer-nav">
                            <a href="">Home</a> | <a href="">About</a> | <a href="">Sitemap</a> | <a href="">RSS</a> | <a href="">Contact Us</a>
                        </div>
                        <div class="footer">Copyright &copy; 2015 - {$nama_pt} <br />Sistem Langitan NU <br />oleh <a target="_blank" href="http://unair.ac.id" class="disable-ajax">Universitas Airlangga</a></div>
                    </td>
                    <td class="foot-right">&nbsp;</td>
                </tr>
            </tfoot>
        </table>
    </body>
</html>