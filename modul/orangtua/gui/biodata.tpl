<div class="center_title_bar">Biodata</div>
<table class="ui-widget-content" style="width:100%;">
    <tr>
        <td class="ui-widget-header" colspan="3"><center>DATA MAHASISWA</center></td>
</tr>
<tr>
    <td class="ui-widget-content" colspan="3"><center><img src="/foto_mhs/{$nim}.JPG" height="200" width="150" /></center></td>
</tr>
<tr>
    <td class="ui-widget-content">Nama</td><td style="width:10px;">:</td><td>{$nm_mhs}</td>
</tr>
<tr>
    <td class="ui-widget-content">NIM</td><td>:</td><td>{$nim}</td>
</tr>
<tr>
    <td class="ui-widget-content">Jenis Kelamin</td><td>:</td><td>{if $kelamin_pengguna=='1'}Laki-laki{else}Perempuan{/if}</td>
</tr>
<tr>
    <td class="ui-widget-content">Tanggal Lahir</td><td>:</td><td>{$tgl_lahir_pengguna}</td>
</tr>
<tr>
    <td class="ui-widget-content">Tempat Lahir(Propinsi)</td><td>:</td><td>{$tempat_lahir_prov}</td>
</tr>
<tr>
    <td class="ui-widget-content">Kota Lahir</td><td>:</td><td>{$tempat_lahir_kota}</td>
</tr>
<tr>
    <td class="ui-widget-content">Alamat Surabaya/Telp</td><td>:</td><td>{$alamat_asal_mhs_kota}</td>
</tr>
<tr>
    <td class="ui-widget-content">No Handphone</td><td>:</td><td>{$mobile_mhs}</td>
</tr>
<tr>
    <td class="ui-widget-content">Agama</td><td>:</td><td>{$agama_mhs}</td>
</tr>
<tr>
    <td class="ui-widget-content">Alamat Email</td><td>:</td><td>{$email_pengguna}</td>
</tr>
<tr>
    <td class="ui-widget-content">Alamat Blog</td><td>:</td><td>{$blog_pengguna}</td>
</tr>
<tr>
    <td class="ui-widget-content">Tahun Lulus</td><td>:</td><td>{$lulus_dayat}</td>
</tr>
<tr>
    <td class="ui-widget-content">NEM / NUN / Nilai Ujian Nasional</td><td>:</td><td>{$nilai_skhun_mhs}</td>
</tr>
<tr>
    <td class="ui-widget-content">Cita - cita</td><td>:</td><td>{$cita_cita_mhs}</td>
</tr>
<tr>
    <td class="ui-widget-content">Fakultas</td><td>:</td><td>{$fakultas_mhs}</td>
</tr>
<tr>
    <td class="ui-widget-content">Prodi</td><td>:</td><td>{$nm_program_studi}</td>
</tr>


<tr>
    <td class="ui-widget-header" colspan="3"><center>DATA AYAH</center></td>
</tr>
<tr>
    <td class="ui-widget-content">Nama</td><td>:</td><td>{$nama_ayah}</td>
</tr>
<tr>
    <td class="ui-widget-content">Pendidikan</td><td>:</td><td>{$pendidikan_ayah}</td>
</tr>
<tr>
    <td class="ui-widget-content">Pekerjaan</td><td>:</td><td>{$pekerjaan_ayah}</td>
</tr>
<tr>
    <td class="ui-widget-content">Alamat</td><td>:</td><td>{$alamat_ayah}</td>
</tr>

<tr>
    <td class="ui-widget-header" colspan="3"><center>DATA IBU</center></td>
</tr>
<tr>
    <td class="ui-widget-content">Nama</td><td>:</td><td>{$nama_ibu}</td>
</tr>
<tr>
    <td class="ui-widget-content">Pendidikan</td><td>:</td><td>{$pendidikan_ibu}</td>
</tr>
<tr>
    <td class="ui-widget-content">Pekerjaan</td><td>:</td><td>{$pekerjaan_ibu}</td>
</tr>
<tr>
    <td class="ui-widget-content">Alamat</td><td>:</td><td>{$alamat_ibu}</td>
</tr>
</table>