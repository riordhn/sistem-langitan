<!-- author Sani Iman Pribadi -->

<script>
	$(function() {
		$("#btnInboxHapus").button({
			icons: {
				primary: "ui-icon-trash",
			},
			text: true
		});
		
		$("#btnComposeKirim").click(function(){
			$('#frmCompose').submit();
		});
		
		$('#frmCompose').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						//$('#new_comment{$id_status[$i]}').val('');	
						$('#compose').load('compose.php');	
						//$('#status{$id_status[$i]}').slideDown('fast');				
						}
					})
					return false;
		});
		
	});
</script>

<div id="inbox">
<div class="center_title_bar">Inbox</div>

	<table class="ui-widget" style="width:100%">
		<tr class="ui-widget-header">
			<td>&nbsp;</td><td><strong>Dari</strong></td><td><strong>Subyek</strong></td><td><strong>Tanggal</strong></td><td><strong>Aksi</strong></td>
		</tr>
		<tr>
			<td colspan="5">
				<div id="btnInboxHapus">Hapus</div>
			</td>
		</tr>
	</table>

</div>