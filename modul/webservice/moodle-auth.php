<?php
require '../webservice-pddikti/nusoap/nusoap.php';

class food {
 
    public function getFood($type) {
        switch ($type) {
            case 'starter':
                return 'Soup';
                break;
            case 'Main':
                return 'Curry';
                break;
            case 'Desert':
                return 'Ice Cream';
                break;
            default:
                break;
        }
    }
}

class AuthenticationResult
{
    public function authenticate($username, $password)
    {

    }
}
 
$server = new soap_server();
$server->configureWSDL("MoodleAuthService", "https://langitan.umaha.ac.id/modul/webservice/moodle-auth.php");
 
$server->register("AuthenticationResult.authenticate",
    array("username" => "xsd:string", "password" => "xsd:string"),
    array("return" => "xsd:string"),
    "https://langitan.umaha.ac.id/modul/webservice/moodle-auth.php",
    "https://langitan.umaha.ac.id/modul/webservice/moodle-auth.php#authenticate",
    "rpc",
    "encoded",
    "Get food by type");
 
@$server->service($HTTP_RAW_POST_DATA);