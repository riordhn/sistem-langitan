<?php
include '../../config.php';

// Fungsi merubah ke xml
function array_to_xml(array $arr, SimpleXMLElement $xml)
{
	foreach ($arr as $k => $v)
	{
		is_array($v) ? array_to_xml($v, $xml->addChild($k)) : $xml->addChild($k, $v);
	}
	
	return $xml;
}

function write_logs($text)
{
	$filename = "/var/www/html/modul/webservice/log-ppmb-penilaian/log.txt";
	$time_log = date('d-m-Y H:i:s');
	$file = fopen($filename, 'a', 1);
	fwrite($file, "{$time_log} : {$text}\n");
	fclose($file);
}

// Membuat agar outputnya XML
header("Content-type: text/xml");

if (isset($_GET['list-program-studi']))
{
	echo $db->QueryToXML("
		select id_program_studi as kode_cyber, ps.kode_ppmb, j.nm_jenjang as jenjang, nm_program_studi as nama_prodi, gelar_pendek as gelar
		from program_studi ps
		join jenjang j on j.id_jenjang = ps.id_jenjang
		where ps.status_aktif_prodi = 1
		order by nm_jenjang, nm_program_studi");
	exit();
}

if (isset($_GET['list-penerimaan']))
{
	echo $db->QueryToXML("
		select id_penerimaan as id, nm_penerimaan as penerimaan, nm_jenjang as jenjang, nm_jalur as jalur from penerimaan p
		join jenjang j on j.id_jenjang = p.id_jenjang
		join jalur j on j.id_jalur = p.id_jalur
		where is_aktif = 1
		order by j.nm_jenjang, nm_jalur");
	exit();
}

if (isset($_GET['get-peserta']))
{
	$id_penerimaan = get('id');
		
	echo $db->QueryToXML("
		select
			no_ujian, nm_c_mhs as nama_peserta,
			to_char(cmb.tgl_lahir,'YYYY-MM-DD') as tgl_lahir,
			id_pilihan_1 as pilihan_1,
			id_pilihan_2 as pilihan_2,
			id_pilihan_3 as pilihan_3,
			id_pilihan_4 as pilihan_4,
			(select kode_jurusan from voucher v where v.kode_voucher = cmb.kode_voucher) as kode_jurusan,
			id_program_studi,
			telp,
			id_sekolah_asal,
			cmp.jenis_akreditasi_s1 as jenis_akreditasi,
			cmp.peringkat_akreditasi_s1 as peringkat_akreditasi,
			'http://pendaftaran.unair.ac.id/files/'||p.id_penerimaan||'/'||cmb.id_c_mhs||'/'||cmf.file_foto as foto
		from calon_mahasiswa_baru cmb
		join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
		join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
		join calon_mahasiswa_pasca cmp on cmp.id_c_mhs = cmb.id_c_mhs
		join calon_mahasiswa_file cmf on cmf.id_c_mhs = cmb.id_c_mhs
		where no_ujian is not null and p.id_penerimaan = {$id_penerimaan}
		order by no_ujian");
	exit();
}

if (isset($_GET['view-peserta']))
{
	$no_ujian = get('no_ujian');
	
	$db->Query("
		select no_ujian,
			(select nilai_tpa from nilai_cmhs n where n.id_c_mhs = cmb.id_c_mhs and jurusan_sekolah = 1) as nilai_tpa,
			(select nilai_prestasi from nilai_cmhs n where n.id_c_mhs = cmb.id_c_mhs and jurusan_sekolah = 1) as nilai_prestasi_ipa,
			(select nilai_prestasi from nilai_cmhs n where n.id_c_mhs = cmb.id_c_mhs and jurusan_sekolah = 2) as nilai_prestasi_ips
		from calon_mahasiswa_baru cmb
		where no_ujian = '{$no_ujian}'");
	$rows = $db->FetchAssoc();
	
	echo "<?xml version=\"1.0\"?><result>";
	echo "<NO_UJIAN>{$rows['NO_UJIAN']}</NO_UJIAN>";
	echo "<NILAI_TPA>{$rows['NILAI_TPA']}</NILAI_TPA>";
	echo "<NILAI_PRESTASI_IPA>{$rows['NILAI_PRESTASI_IPA']}</NILAI_PRESTASI_IPA>";
	echo "<NILAI_PRESTASI_IPS>{$rows['NILAI_PRESTASI_IPS']}</NILAI_PRESTASI_IPS>";
	echo "</result>";
	
	exit();
}

if (isset($_GET['view-peserta-pasca']))
{
	$no_ujian = get('no_ujian');
	
	$db->Query("
		select 
			no_ujian, nilai_tpa, nilai_inggris, nilai_ilmu, nilai_wawancara, nilai_ipk, nilai_karya_ilmiah, nilai_rekomendasi,
			nilai_matrikulasi, nilai_psiko, nilai_tulis, nilai_lain, nilai_gab_ppds,
			nilai_bakordik_mentah, nilai_bakordik_terbobot, nilai_tpa_mentah, nilai_inggris_mentah,
			nilai_kesantrian, nilai_kesantrian_mentah,
			total_nilai_ppds, total_nilai
		from calon_mahasiswa_baru  cmb
		join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
		join nilai_cmhs_pasca n on n.id_c_mhs = cmb.id_c_mhs
		where p.is_aktif = 1 and cmb.no_ujian = '{$no_ujian}'");
	$row = $db->FetchAssoc();
	
	// cetak 
	print array_to_xml($row, new SimpleXMLElement('<root/>'))->asXML();
	
	exit();
}

if (isset($_GET['update-nilai']))
{
	$no_ujian = get('no_ujian');
	$keyword = get('keyword');
	
	$nilai_tpa = get('tpa', 0);
	$nilai_prestasi_ipa = get('prestasi_ipa', 0);
	$nilai_prestasi_ips = get('prestasi_ips', 0);
	$status_ujian = get('status', '');
	
	if ($keyword == md5(date('Ymd')))
	{       
		$db->BeginTransaction();
		
		// Update yg IPA
		$db->Query("
			update nilai_cmhs set
				nilai_tpa = '{$nilai_tpa}',
				nilai_prestasi = '{$nilai_prestasi_ipa}'
			where jurusan_sekolah = 1 and id_c_mhs = (
				select id_c_mhs from calon_mahasiswa_baru cmb
				join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
				where no_ujian = '{$no_ujian}' and p.is_penilaian = 1)");
		
		// Update yg IPS
		$db->Query("
			update nilai_cmhs set
				nilai_tpa = '{$nilai_tpa}',
				nilai_prestasi = '{$nilai_prestasi_ips}'
			where jurusan_sekolah = 2 and id_c_mhs = (
				select id_c_mhs from calon_mahasiswa_baru cmb
				join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
				where no_ujian = '{$no_ujian}' and p.is_penilaian = 1)");
		
		if (in_array($status_ujian, array(0, 1, 2, 3, 4)))
		{
			$db->Query("
				update calon_mahasiswa_baru set status_ujian = {$status_ujian}
				where no_ujian = '{$no_ujian}' and
					id_penerimaan in (select id_penerimaan from penerimaan where is_aktif = 1 and is_penilaian = 1)");
		}
		
		if (in_array($status_ujian, array(2, 3, 4)))
		{
			// write_logs("{$no_ujian}, {$status_ujian}");
		}
		
		$result = $db->Commit();
		
		if ($result)
		{
			$result = "1";
		}
		else
		{
			$result = "0";
		}

		write_logs("Key={$keyword}, {$no_ujian}, TPA={$nilai_tpa}, P.IPA={$nilai_prestasi_ipa}, P.IPS={$nilai_prestasi_ips}, Status={$status_ujian}, Result={$result}");
	}
	else
	{
		$result = "Keyword salah";
	}
	
	echo "<?xml version=\"1.0\"?><result>";
	echo $result;
	echo "</result>";
	exit();
}

if (isset($_GET['update-nilai-pasca']))
{
	$no_ujian = get('no_ujian');
	$keyword = get('keyword');
	
	$nilai_tpa          = get('nilai_tpa', 0);
	$nilai_inggris      = get('nilai_inggris', 0);
	$nilai_ilmu         = get('nilai_ilmu', 0);
	$nilai_wawancara    = get('nilai_wawancara', 0);
	$nilai_ipk          = get('nilai_ipk', 0);
	$nilai_karya_ilmiah = get('nilai_karya_ilmiah', 0);
	$nilai_rekomendasi  = get('nilai_rekomendasi', 0);
	$nilai_matrikulasi  = get('nilai_matrikulasi', 0);
	$nilai_psiko        = get('nilai_psiko', 0);
	$nilai_tulis        = get('nilai_tulis', 0);
	$nilai_lain         = get('nilai_lain', 0);
	$nilai_gab_ppds     = get('nilai_gab_ppds', 0);
	
	// Tambahan baru untuk PPDS 2014
	$nilai_bakordik_mentah		= get('nilai_bakordik_mentah', 0);
	$nilai_bakordik_terbobot	= get('nilai_bakordik_terbobot', 0);
	$nilai_tpa_mentah			= get('nilai_tpa_mentah', 0);
	$nilai_inggris_mentah		= get('nilai_inggris_mentah', 0);
	$total_nilai_ppds			= get('total_nilai_ppds', 0);
	
	// Tambahan baru untuk Alih-Jenis 2014
	$nilai_prestasi				= get('nilai_prestasi', 0);
	$nilai_prestasi_mentah		= get('nilai_prestasi_mentah', 0);
	$nilai_akreditasi			= get('nilai_akreditasi', 0);  // per tanggal 10-AUG-2014
	$nilai_akreditasi_mentah	= get('nilai_akreditasi_mentah', 0);

	// Tambahan baru untuk Mandiri PBSB Kemenag 2014
	$nilai_kesantrian			= get('nilai_kesantrian', 0);
	$nilai_kesantrian_mentah	= get('nilai_kesantrian_mentah', 0);
	
	// Total Nilai Akhir
	$total_nilai				= get('total_nilai', 0);
	
	$status_ujian				= get('status', '');
	
	if ($keyword == md5(date('Ymd')))
	{
		$db->BeginTransaction();
		
		$db->Query(
			"UPDATE nilai_cmhs_pasca set
				nilai_tpa				= {$nilai_tpa},
				nilai_inggris			= {$nilai_inggris},
				nilai_ilmu				= {$nilai_ilmu},
				nilai_wawancara			= {$nilai_wawancara},
				nilai_ipk				= {$nilai_ipk},
				nilai_karya_ilmiah		= {$nilai_karya_ilmiah},
				nilai_rekomendasi		= {$nilai_rekomendasi},
				nilai_matrikulasi		= {$nilai_matrikulasi},
				nilai_psiko				= {$nilai_psiko},
				nilai_tulis				= {$nilai_tulis},
				nilai_lain				= {$nilai_lain},
				nilai_gab_ppds			= {$nilai_gab_ppds},
				nilai_bakordik_mentah	= {$nilai_bakordik_mentah},
				nilai_bakordik_terbobot	= {$nilai_bakordik_terbobot},
				nilai_tpa_mentah		= {$nilai_tpa_mentah},
				nilai_inggris_mentah	= {$nilai_inggris_mentah},
				nilai_prestasi			= {$nilai_prestasi},
				nilai_prestasi_mentah	= {$nilai_prestasi_mentah},
				nilai_akreditasi		= {$nilai_akreditasi},
				nilai_akreditasi_mentah	= {$nilai_akreditasi_mentah},
				nilai_kesantrian 		= {$nilai_kesantrian},
				nilai_kesantrian_mentah = {$nilai_kesantrian_mentah},
				total_nilai_ppds		= {$total_nilai_ppds},
				total_nilai				= {$total_nilai}
			where id_c_mhs in (
				select id_c_mhs from calon_mahasiswa_baru cmb
				join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
				where p.is_aktif = 1 and p.is_penilaian = 1 and cmb.no_ujian = '{$no_ujian}')");

		if (in_array($status_ujian, array(0, 1, 2, 3, 4)))
		{
			$db->Query("
				update calon_mahasiswa_baru set status_ujian = {$status_ujian}
				where no_ujian = '{$no_ujian}' and
					id_penerimaan in (select id_penerimaan from penerimaan where is_aktif = 1 and is_penilaian = 1)");
		}
		
		$result = $db->Commit();
	 
		if ($result)
			$result = "1";
		else
			$result = "0";
	}
	else
	{
		$result = "Keyword salah";
	}
	
	echo "<?xml version=\"1.0\"?><result>";
	echo $result;
	echo "</result>";
	
	exit();
}

if (isset($_GET['penilaian']))
{
	$id = get('id', 0);
	$status = get('status');
	
	if ($status == 'open')
	{
		$result = $db->Query("update penerimaan set is_penilaian = 1 where id_penerimaan = {$id}");
		
		if ($result) $result = "Berhasil update"; else $result = "Gagal update";
		
		echo "<?xml version=\"1.0\"?><result>";
		echo $result;
		echo "</result>";
	}
	
	if ($status == 'close')
	{
		$result = $db->Query("update penerimaan set is_penilaian = 0 where id_penerimaan = {$id}");
		
		if ($result) $result = "Berhasil update"; else $result = "Gagal update";
		
		echo "<?xml version=\"1.0\"?><result>";
		echo $result;
		echo "</result>";
	}
	
	if ($status == 'view')
	{
		$db->Query("select is_penilaian from penerimaan where id_penerimaan = {$id}");
		$row = $db->FetchAssoc();
		
		echo "<?xml version=\"1.0\"?><result>";
		echo $row['IS_PENILAIAN'];
		echo "</result>";
	}
	
	exit();
}

if (isset($_GET['status-ujian']))
{
	$id = get('id', 0);
	
	echo $db->QueryToXML("
		select no_ujian, nm_c_mhs as nama, status_ujian from calon_mahasiswa_baru
		where id_penerimaan = {$id} and no_ujian is not null and status_ujian in (0, 3)
		order by no_ujian");
		
	exit();
}

if (isset($_GET['master_sekolah']))
{
	$id = get('id', 0);

	echo $db->QueryToXML("
		select s.id_sekolah, s.nm_sekolah as sekolah, s.kode_snmptn, k.nm_kota as kota, p.nm_provinsi as provinsi
		from sekolah s
		join kota k on k.id_kota = s.id_kota
		join provinsi p on p.id_provinsi = k.id_provinsi
		where id_sekolah in (
		  select distinct id_sekolah_asal from calon_mahasiswa_sekolah cms
		  join calon_mahasiswa_baru cmb on cmb.id_c_mhs = cms.id_c_mhs
		  join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
		  where cmb.id_penerimaan = {$id}
		)");

	exit();
}
?>