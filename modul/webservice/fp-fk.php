<?php
include '../../config.php';

$mode = !empty($_GET['mode']) ? $_GET['mode'] : '';

if ($mode == 'get-semester')
{
	$data_set = $db->QueryToArray("
		select * from (
			select * from semester where tipe_semester = 'REG'
			order by THN_AKADEMIK_SEMESTER desc, NM_SEMESTER desc
		) t where rownum <= 10");
	echo json_encode($data_set); exit();
}

if ($mode == 'get-info-kelas')
{
	$id_fakultas = get('id_fakultas','');
	$id_semester = get('id_semester','');
	
	$data_set = $db->QueryToArray("
		SELECT
			(select count(*) from kelas_mk
			join program_studi on PROGRAM_STUDI.id_program_studi = KELAS_MK.ID_PROGRAM_STUDI
			where PROGRAM_STUDI.id_fakultas = {$id_fakultas} and KELAS_MK.ID_SEMESTER = {$id_semester}) jumlah_kelas,
			(select count(*) from FINGERPRINT_PRESENSI
			where id_fakultas = {$id_fakultas} and id_semester = {$id_semester} and jenis = 'DOSEN') jumlah_dosen,
			(select count(*) from FINGERPRINT_PRESENSI
			where id_fakultas = {$id_fakultas} and id_semester = {$id_semester} and jenis = 'MAHASISWA') jumlah_MAHASISWA,
			(select count(*) from FINGERPRINT_PRESENSI
			where id_fakultas = {$id_fakultas} and id_semester = {$id_semester} and jenis = 'DOSEN' and template is not null) fingerprint_dosen,
			(select count(*) from FINGERPRINT_PRESENSI
			where id_fakultas = {$id_fakultas} and id_semester = {$id_semester} and jenis = 'MAHASISWA' and template is not null) fingerprint_MAHASISWA
		from dual");
	echo json_encode($data_set); exit();
}

if ($mode == 'refresh-data')
{
	$id_fakultas = get('id_fakultas','');
	$id_semester = get('id_semester','');
	
	$db->BeginTransaction();
	
	// di hapuss hanya yg belum register sidik jari
	$db->Query("delete from fingerprint_presensi where id_semester = {$id_semester} and id_fakultas = {$id_fakultas} and template is null");
	
	// re insert data yg belum ada di tabel fingerprint_presensi
	$db->Query("
		insert into fingerprint_presensi (id_semester, id_fakultas, id_pengguna, username, jenis)
		select * from (
			select KELAS_MK.ID_SEMESTER, PROGRAM_STUDI.ID_FAKULTAS, PENGGUNA.ID_PENGGUNA, PENGGUNA.USERNAME, 'DOSEN' AS JENIS
			from KELAS_MK
			join PENGAMPU_MK on PENGAMPU_MK.ID_KELAS_MK = KELAS_MK.ID_KELAS_MK
			join PROGRAM_STUDI on PROGRAM_STUDI.ID_PROGRAM_studi = KELAS_MK.id_program_studi
			join DOSEN on DOSEN.ID_DOSEN = PENGAMPU_MK.ID_DOSEN
			join PENGGUNA ON PENGGUNA.ID_PENGGUNA = DOSEN.ID_PENGGUNA
			where PROGRAM_STUDI.id_fakultas = {$id_fakultas} and KELAS_MK.ID_SEMESTER = {$id_semester}
			group by KELAS_MK.ID_SEMESTER, PROGRAM_STUDI.ID_FAKULTAS,PENGGUNA.ID_PENGGUNA, PENGGUNA.USERNAME
			UNION ALL
			select KELAS_MK.ID_SEMESTER, PROGRAM_STUDI.ID_FAKULTAS, pengguna.ID_PENGGUNA, PENGGUNA.USERNAME, 'MAHASISWA' AS JENIS
			from KELAS_MK
			join PROGRAM_STUDI on PROGRAM_STUDI.ID_PROGRAM_studi = KELAS_MK.id_program_studi
			join PENGAMBILAN_MK on PENGAMBILAN_MK.ID_KELAS_MK = KELAS_MK.ID_KELAS_MK
			join MAHASISWA on MAHASISWA.ID_MHS = PENGAMBILAN_MK.ID_MHS
			join pengguna on pengguna.id_pengguna = mahasiswa.id_pengguna 
			where PENGAMBILAN_MK.STATUS_APV_PENGAMBILAN_MK = 1 and PROGRAM_STUDI.id_fakultas = {$id_fakultas} and KELAS_MK.ID_SEMESTER = {$id_semester}
			group by KELAS_MK.ID_SEMESTER, PROGRAM_STUDI.ID_FAKULTAS, PENGGUNA.id_pengguna, pengguna.username) t
		where t.id_pengguna not in (select id_pengguna from fingerprint_presensi where id_fakultas = {$id_fakultas} and id_semester = {$id_semester} and template is not null)");
	
	$db->Commit();
	
	echo "1";
}

if ($mode == 'get-user')
{
	$id_fakultas = get('id_fakultas','');
	$id_semester = get('id_semester','');
	
	$data_set = $db->QueryToArray("
		select pengguna.username, substr(pengguna.nm_pengguna,1,40) nm_pengguna, jenis, template, versi_template from fingerprint_presensi
		join pengguna on pengguna.id_pengguna = fingerprint_presensi.id_pengguna
		where id_fakultas = {$id_fakultas} and id_semester = {$id_semester}");
	
	header("Content-type: application/json");
	echo json_encode($data_set); exit();
}

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	if ($_POST['mode'] == 'upload-template')
	{
		$db->Query("update fingerprint_presensi set template = '{$_POST['template']}' where username = '{$_POST['username']}'");
	}
	
	if ($_POST[''] == 'upload-absensi')
	{
		
	}
}

?>