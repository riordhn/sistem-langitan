<?php
include('../../config.php');

include('includes/nusoap.php');

$ws = new nusoap_server();

$ws->configureWSDL("Akademik");

function Login($username, $password)
{
    global $db, $user;
    
    $db->Parse("select count(*) as ketemu from pengguna where id_role = 7 and username = :username and se1 = :password");
    $db->BindByName(':username', $username);
    $db->BindByName(':password', $user->Encrypt($password));
    $db->Execute();
    
    $row = $db->FetchAssoc();
    if ($row['KETEMU'] == 1)
        return true;
    else
        return false;
}

$ws->register("Login", array('username' => 'xsd:string', 'password' => 'xsd:string'), array('return' => 'xsd:boolean'));

$POST_DATA = isset($GLOBALS['HTTP_RAW_POST_DATA']) ? $GLOBALS['HTTP_RAW_POST_DATA'] : '';
$ws->service($POST_DATA);
exit();
?>
