<?php 
include '../../config.php';

$mode = get('mode');

function array2xml($array, $xml = false){
    if($xml === false){
        $xml = new SimpleXMLElement('<root/>');
    }
    foreach($array as $key => $value){
        if(is_array($value)){
            array2xml($value, $xml->addChild($key));
        }else{
            $xml->addChild($key, $value);
        }
    }
    return $xml->asXML();
}

header('Content-type: text/xml');

if ($mode == 'get')
{
	$no_ujian = get('no_ujian');

	$query = "
		select no_ujian, nm_c_mhs as nama, nm_jalur as jalur, nm_jenjang as jenjang from calon_mahasiswa_baru
		join penerimaan on penerimaan.id_penerimaan = calon_mahasiswa_baru.id_penerimaan
		join jalur on jalur.id_jalur = penerimaan.id_jalur
		join jenjang on jenjang.id_jenjang = penerimaan.id_jenjang
		where NO_UJIAN = '{$no_ujian}'";

	$db->Query($query);
	$row = $db->FetchAssoc();

	if ($row)
	{
		print array2xml($row);
	}	
}
?>