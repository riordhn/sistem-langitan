<?php
include '../../config.php';

// Fungsi merubah ke xml
function array_to_xml(array $arr, SimpleXMLElement $xml)
{
    foreach ($arr as $k => $v)
    {
        is_array($v) ? array_to_xml($v, $xml->addChild($k)) : $xml->addChild($k, $v);
    }
    
    return $xml;
}

header("Content-type: text/xml");

if (isset($_GET['list-penerimaan']))
{
    echo $db->QueryToXML("
        select id_penerimaan as id, nm_penerimaan as penerimaan, nm_jenjang as jenjang, nm_jalur as jalur from penerimaan p
        join jenjang j on j.id_jenjang = p.id_jenjang
        join jalur j on j.id_jalur = p.id_jalur
        where is_aktif = 1
        order by j.nm_jenjang, nm_jalur");
    exit();
}

if (isset($_GET['get-list-ruang']))
{
	$id_penerimaan = $_GET['id_penerimaan'];
	echo $db->QueryToXML("
		select id_jadwal_ppmb, nomer_awal, nomer_akhir, lokasi, kapasitas, kode_jalur, nm_ruang, alamat 
		from jadwal_ppmb where id_penerimaan = {$id_penerimaan} and status_aktif = 1");
	exit();
}

?>