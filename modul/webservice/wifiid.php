<?php
require '../../config.php';

$result_message = "";
$error = false;

if (isset($_GET['username']) && isset($_GET['password']))
{
	$username = str_replace("@umaha", "", $_GET['username']);
	$password = $_GET['password'];
	
	if (strlen(trim($_GET['username'])) > 0 && strlen(trim($_GET['password'])) > 0) 
	{
		$login_result = User::ERR_USERPASS;

		// Jika login 88XXX@umaha, maka untuk guest, baca file wifiid-guest.txt
		if ((substr($username, 0, 2) == '88') && (strlen($username) == 5))
		{
			// Baca file password
			$file = fopen('wifiid-guest.txt', 'r');
			$file_content = fread($file, filesize('wifiid-guest.txt'));
			fclose($file);
		
			// Decode sebagai array
			$login_list = json_decode($file_content, TRUE);
		
			// Komparasi
			foreach ($login_list as $login)
			{
				if ($login['username'] == $username && $login['password'] == $password)
				{
					$login_result = User::OK;
				}
			}
		}
		else
		{
			$login_result = $user->LoginWifiid($username, $_GET['password'], $PT->id_perguruan_tinggi);
		}
	} 
	else 
	{
		if (strlen(trim($_GET['username'])) == 0)
			$login_result = User::ERR_USERPASS;
		else if (strlen(trim($_GET['password'])) == 0)
			$login_result = User::ERR_USERPASS;
	}
	
	// Proses Result Message
	// $login_result = User::OK;
	
	if ($login_result == User::ERR_USERPASS) 
	{
		$result_message = "ERR_USERPASS";
		$error = false;
	} 
	else if ($login_result == User::ERR_ADM) 
	{
		$result_message = "OK";
		$error = false;
	}
	else if ($login_result == User::ERR_XX) 
	{
		$result_message = "OK";
		$error = false;
	}
	else if ($login_result == User::OK) 
	{
		$result_message = "OK";
		$error = false;
	}
	
	// Return
	echo $result_message;
}