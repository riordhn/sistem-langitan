<?php
$soap = new SoapServer(null, array(
    'soap_version' => SOAP_1_2,
    'uri' => 'https://langitan.umaha.ac.id/modul/webservice/example1.php?wsdl'
));

function HelloWorld()
{
    return "X";
}

$soap->addFunction("HelloWorld");

$soap->handle(isset($GLOBALS['HTTP_RAW_POST_DATA']) ? $GLOBALS['HTTP_RAW_POST_DATA'] : '');