<?php
include('../../config.php');

include('includes/nusoap.php');

$ws = new nusoap_server();

$ws->configureWSDL("FingerprintMahasiswa");

function GetAppCMBVersion()
{
    return "2.0.135.0317;http://cybercampus.unair.ac.id/fingerprint/fpcm/update.zip";
}

$ws->register("GetAppCMBVersion", array(), array('return' => 'xsd:string'));


/**
 * 
 * @global type $db
 * @return type 
 */
function GetAllExistDataCMB()
{
    global $db; // memanggil variabel dari luar fungsi
    
    $result_array = array();
    
    $executed = $db->Query("select no_ujian from fingerprint_cmhs fc join calon_mahasiswa_baru cmb on cmb.id_c_mhs = fc.id_c_mhs where fc.finger_data is not null");
    while ($row = $db->FetchAssoc())
        array_push ($result_array, $row['NO_UJIAN']);
    
    return implode(":", $result_array);
}

$ws->register("GetAllExistDataCMB", array(), array('return' => 'xsd:string'));


/**
 * Mendapatkan data finger calon mahasiswa
 * @global type $db
 * @param type $no_ujian
 * @return type 
 */
function GetFingerDataCMB($no_ujian)
{
    global $db;
    
    $db->Query("select finger_data from fingerprint_cmhs where id_c_mhs = (select id_c_mhs from calon_mahasiswa_baru where no_ujian = '{$no_ujian}')");
    $row = $db->FetchAssoc();
    if ($row)
    {
        return base64_encode($row['FINGER_DATA']);
    }
    else
    {
        throw new nusoap_fault();
    }
}

$ws->register('GetFingerDataCMB', array('no_ujian' => 'xsd:string'), array('return' => 'xsd:base64Binary'));

function UploadFingerdataCMB($no_ujian, $finger_data)
{
    global $db;
    
    $db->Query("select id_c_mhs from calon_mahasiswa_baru cmb where cmb.no_ujian = '{$no_ujian}'");
    $row = $db->FetchAssoc();
    
    if ($row)
    {   
        $db->Query("select count(*) jumlah from fingerprint_cmhs fc where id_c_mhs = {$row['ID_C_MHS']}");
        $row1 = $db->FetchAssoc();
        
        // jika record belum ada
        if ($row1['JUMLAH'] == 0)
            $db->Parse("insert into fingerprint_cmhs (finger_data, id_c_mhs) values (:finger_data, :id_c_mhs)");
        else
            $db->Parse("update fingerprint_cmhs set finger_data = :finger_data where id_c_mhs = :id_c_mhs");
        
        $db->BindByName(':finger_data', bin2hex($finger_data));
        $db->BindByName(':id_c_mhs', $row['ID_C_MHS']);
        $result = $db->Execute();
        
        if ($result)
            return "1";
        else
            return "Gagal kesalahan pada server. Hubungi Admin.";
    }
    else
    {
        return "Nomer ujian tidak ditemukan"; // Nomer ujian tidak ada
    }
}

$ws->register("UploadFingerdataCMB", array('no_ujian' => 'xsd:string', 'finger_data' => 'xsd:base64Binary'), array('return' => 'xsd:string'));

function UploadFingerdataCMB1($id_c_mhs, $finger_data)
{
	global $db;
	
	$db->Query("select count(*) from fingerprint_cmhs where id_c_mhs = {$id_c_mhs}");
	$row = $db->FetchRow();
	
	if ($row[0] == 0)
		$db->Parse("insert into fingerprint_cmhs (finger_data, id_c_mhs) values (:finger_data, :id_c_mhs)");
	else
		$db->Parse("update fingerprint_cmhs set finger_data = :finger_data where id_c_mhs = :id_c_mhs");
        
	$db->BindByName(':finger_data', bin2hex($finger_data));
	$db->BindByName(':id_c_mhs', $id_c_mhs);
	$result = $db->Execute();
	
	return "1";
}

$ws->register("UploadFingerdataCMB1", array('id_c_mhs' => 'xsd:string', 'finger_data' => 'xsd:base64Binary'), array('return' => 'xsd:string'));

function GetNamaPeserta($no_ujian)
{
    global $db;
    
    $db->Query("select nm_c_mhs from calon_mahasiswa_baru cmb where cmb.no_ujian = '{$no_ujian}'");
    $row = $db->FetchAssoc();
    
    if ($row)
    {
        return $row['NM_C_MHS'];
    }
    else
    {
        return "-1";
    }
}

$ws->register("GetNamaPeserta", array('no_ujian' => 'xsd:string'), array('return' => 'xsd:string'));

function GetAllExistDataMahasiswa()
{
    global $db;
    
    $db->Query("select nim_mhs from fingerprint_mahasiswa where finger_data is not null");
    $return = "";
    
}

$ws->register('GetAllExistDataMahasiswa', array(), array('return' => 'xsd:string'));

/**
 * Mendapatkan finger_data mahasiswa
 * @global type $db
 * @param type $nim_mhs
 * @return type 
 */
function GetFingerDataMahasiswa($nim_mhs)
{
    global $db;
    
    $db->Query("select finger_data from fingerprint_mahasiswa where nim_mhs = '{$nim_mhs}'");
    $row = $db->FetchAssoc();
    if ($row)
    {
        return base64_encode($row['FINGER_DATA']);
    }
    else
    {
        throw new nusoap_fault();
    }
}

$ws->register('GetFingerDataMahasiswa', array('nim_mhs' => 'xsd:string'), array('return' => 'xsd:base64Binary'));


/**
 * Update finger_data mahasiswa
 * @param type $nim_mhs
 * @param type $finger_data
 * @return type 
 */
function UpdateFingerDataMahasiswa($nim_mhs, $finger_data, $tanggal_lahir)
{
	global $db;
	
	$db->Parse("update fingerprint_mahasiswa set finger_data = :finger_data, tgl_lahir = to_date(:tgl_lahir, 'dd-mm-yyyy') where nim_mhs = :nim_mhs");
	$db->BindByName(':finger_data', bin2hex($finger_data));
    $db->BindByName(':tgl_lahir', $tanggal_lahir);
	$db->BindByName(':nim_mhs', $nim_mhs);
	$db->Execute();
	
    return "1";
}

$ws->register('UpdateFingerDataMahasiswa', array('nim_mhs' => 'xsd:string', 'finger_data' => 'xsd:base64Binary', 'tanggal_lahir' => 'xsd:string'), array('return' => 'xsd:string'));

$POST_DATA = isset($GLOBALS['HTTP_RAW_POST_DATA']) ? $GLOBALS['HTTP_RAW_POST_DATA'] : '';
$ws->service($POST_DATA);
exit();
?>
