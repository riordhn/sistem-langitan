<?php
include '../../config.php';

function hex2bin($data)
{
    $bin    = "";
    $i      = 0;
    do {
        $bin    .= chr(hexdec($data{$i}.$data{($i + 1)}));
        $i      += 2;
    } while ($i < strlen($data));

    return $bin;
}

if (get('mode') == 'get-profil-mahasiswa')
{
    $nim_mhs = get('nim_mhs');
    
    $rows = $db->QueryToArray("
        select username, nm_pengguna, nm_jenjang, nm_program_studi, to_char(tgl_lahir_pengguna, 'DD-MM-YYYY') tgl_lahir_pengguna, thn_angkatan_mhs from pengguna p
        join mahasiswa m on m.id_pengguna = p.id_pengguna
        join program_studi ps on ps.id_program_studi = m.id_program_studi
        join jenjang j on j.id_jenjang = ps.id_jenjang
        where id_role = 3 and username = '{$nim_mhs}'");
    
    if (count($rows) > 0)
        echo "{$rows[0]['NM_PENGGUNA']};{$rows[0]['NM_JENJANG']} {$rows[0]['NM_PROGRAM_STUDI']};{$rows[0]['TGL_LAHIR_PENGGUNA']};{$rows[0]['THN_ANGKATAN_MHS']}";
    exit();
}

if (get('mode') == 'get-all-mahasiswa')
{
    $rows = $db->QueryToArray("select nim_mhs, to_char(tgl_lahir,'dd-mm-yyyy') tgl_lahir from fingerprint_mahasiswa where finger_data is not null");
    
    foreach ($rows as $row)
        echo "{$row['NIM_MHS']}:{$row['TGL_LAHIR']};";
    exit();
}

if (get('mode') == 'get-fp-mahasiswa')
{
    $nim_mhs = get('nim_mhs');
    
    $rows = $db->QueryToArray("
        select rawtohex(utl_raw.cast_to_varchar2(dbms_lob.substr(finger_data))) hex_data
        from fingerprint_mahasiswa where nim_mhs = '{$nim_mhs}'");
    
    echo $rows[0]['HEX_DATA'];
    exit();
}

if (get('mode') == 'set-fp-mahasiswa')
{
    $nim_mhs = get('nim_mhs');
    $finger_data = get('finger_data');
    $tanggal_lahir = get('tanggal_lahir');
    
    $db->Parse("
        update fingerprint_mahasiswa set
            finger_data = hextoraw(:finger_data),
            tgl_lahir = to_date(:tanggal_lahir, 'dd-mm-yyyy')
        where nim_mhs = :nim_mhs");
    $db->BindByName(':finger_data', $finger_data);
    $db->BindByName(':tanggal_lahir', $tanggal_lahir);
    $db->BindByName(':nim_mhs', $nim_mhs);
    $result = $db->Execute();
    
    if ($result)
        echo "1";
    else
        echo "0";
    exit();
}

if (get('mode') == 'get-all-maba')
{
    $rows = $db->QueryToArray("
        select no_ujian, to_char(tgl_lahir,'dd-mm-yyyy') tgl_lahir from fingerprint_cmhs f
        join calon_mahasiswa_baru cmb on cmb.id_c_mhs = f.id_c_mhs
        where f.finger_data is not null and cmb.nim_mhs not in (
            select nim_mhs from fingerprint_mahasiswa where finger_data is not null)");
    foreach ($rows as $row)
        echo "{$row['NO_UJIAN']}:{$row['TGL_LAHIR']};";
    exit();
}

if (get('mode') == 'get-fp-maba')
{
    $no_ujian = get('no_ujian');
    
    $rows = $db->QueryToArray("
        select rawtohex(utl_raw.cast_to_varchar2(dbms_lob.substr(finger_data))) hex_data
        from fingerprint_cmhs where id_c_mhs = (select id_c_mhs from calon_mahasiswa_baru where no_ujian = '{$no_ujian}')");
    
    echo $rows[0]['HEX_DATA'];
    exit();
}

if (get('mode') == 'get-profil-maba')
{
    $no_ujian = get('no_ujian');
    
    $rows = $db->QueryToArray("
        select nm_c_mhs, nm_jenjang, nm_program_studi, to_char(tgl_lahir,'dd-mm-yyyy') tgl_lahir from calon_mahasiswa_baru cmb
        join program_studi ps on ps.id_program_studi = cmb.id_program_studi
        join jenjang j on j.id_jenjang = ps.id_jenjang
        where cmb.no_ujian = '{$no_ujian}'");
    
    if (count($rows) > 0)
        echo "{$rows[0]['NM_C_MHS']};{$rows[0]['NM_JENJANG']} {$rows[0]['NM_PROGRAM_STUDI']};{$rows[0]['TGL_LAHIR']}";
    exit();
}

if (get('mode') == 'set-fp-maba')
{
    $no_ujian = get('no_ujian');
    $finger_data = get('finger_data');
    
    // mendapatkan id_c_mhs
    $db->Query("select id_c_mhs from calon_mahasiswa_baru where no_ujian = '{$no_ujian}'");
    $row = $db->FetchRow();
    $id_c_mhs = $row[0];
    
    // Cek kondisi row
    $db->Query("select count(*) from fingerprint_cmhs where id_c_mhs = (select id_c_mhs from calon_mahasiswa_baru where no_ujian = '{$no_ujian}')");
    $row = $db->FetchRow();
    
    if ($row[0] == 0)
        $db->Parse("insert into fingerprint_cmhs (id_c_mhs, finger_data) values ({$id_c_mhs}, hextoraw(:finger_data))");
    else
        $db->Parse("
            update fingerprint_cmhs set finger_data = hextoraw(:finger_data)
            where id_c_mhs = (select id_c_mhs from calon_mahasiswa_baru where no_ujian = '{$no_ujian}')");
    
    $db->BindByName(':finger_data', $finger_data);
    $result = $db->Execute();
    
    if ($result)
        echo "1";
    else
        echo "0";
    exit();
}

if (get('mode') == 'get-url-foto')
{
    $no_ujian = get('no_ujian');
    
    $base_url    = "http://cybercampus.unair.ac.id/img/foto";
    $base_folder = "/var/www/html/img/foto";
    
    $db->Query("select * from penerimaan where id_penerimaan in (select id_penerimaan from calon_mahasiswa_baru where no_ujian = '{$no_ujian}')");
    $penerimaan = $db->FetchAssoc();
    
    // SNMPTN
    if ($penerimaan['ID_JALUR'] == 1)
    {
        echo $base_url . "/ujian-snmptn/" . $penerimaan['TAHUN'] . "/" . $no_ujian . ".jpg";
    }
    
    // MANDIRI
    if ($penerimaan['ID_JALUR'] == 3 or $penerimaan['ID_JALUR'] == 4 or $penerimaan['ID_JALUR'] == 5 or $penerimaan['ID_JALUR'] == 20 or $penerimaan['ID_JALUR'] == 27)
    {
        $file_path = "/ujian-mandiri/" . $penerimaan['TAHUN'] . "/" . $penerimaan['SEMESTER'] . $penerimaan['GELOMBANG'] . "/" . $no_ujian;
        
        if (file_exists($base_folder . $file_path . ".JPG"))
            echo $base_url . $file_path . ".JPG";
        else if (file_exists($base_folder . $file_path . ".jpg"))
            echo $base_url . $file_path . ".jpg";
    }
    
    // PASCA
    if ($penerimaan['ID_JALUR'] == 6 or $penerimaan['ID_JALUR'] == 23 or $penerimaan['ID_JALUR'] == 24)
    {
        $file_path = "/ujian-pasca/" . $penerimaan['TAHUN'] . "/" . $penerimaan['SEMESTER'] . $penerimaan['GELOMBANG'] . "/" . $no_ujian;
        
        if (file_exists($base_folder . $file_path . ".JPG"))
            echo $base_url . $file_path . ".JPG";
        else if (file_exists($base_folder . $file_path . ".jpg"))
            echo $base_url . $file_path . ".jpg";
    }
    
    exit();
}
?>