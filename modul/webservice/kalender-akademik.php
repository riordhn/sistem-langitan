<?php
include '../../config.php';

$kegiatan_set = $db->QueryToArray(
	"SELECT 
		k.nm_kegiatan as nama_kegiatan,
		k.deskripsi_kegiatan as deskripsi,
		to_char(tgl_mulai_jks, 'YYYY-MM-DD') as tgl_mulai_kegiatan,
		to_char(tgl_selesai_jks, 'YYYY-MM-DD') as tgl_selesai_kegiatan, 
		s.nm_semester as semester,
		s.tahun_ajaran
	from jadwal_kegiatan_semester jks
	inner join semester s on s.id_semester = jks.id_semester
	inner join kegiatan k on K.ID_KEGIATAN = JKS.ID_KEGIATAN
	where s.status_aktif_semester = 'True'
	order by tgl_mulai_jks asc");

echo json_encode($kegiatan_set);