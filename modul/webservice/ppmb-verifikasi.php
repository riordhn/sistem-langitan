<?php
include '../../config.php';

// Untuk kebutuhan intelligent
if (1 == 2) { $db = new MyOracle(); }

// Fungsi merubah ke xml
function array_to_xml(array $arr, SimpleXMLElement $xml)
{
    foreach ($arr as $k => $v)
    {
        is_array($v) ? array_to_xml($v, $xml->addChild($k)) : $xml->addChild($k, $v);
    }
    
    return $xml;
}

if (isset($_GET['get-peserta']))
{
    $no_ujian = get('no_ujian');
    
    // cek eksis
    $db->Query("
        select
            cmb.no_ujian, cmb.nm_c_mhs, 
            to_char(jp.tgl_test,'YYYY-MM-DD') as tgl_test,
			to_char(jp.tgl_test2, 'YYYY-MM-DD') as tgl_test2,
            cmb.kode_voucher, v.kode_jurusan, cmb.id_jalur, jp.lokasi, jp.alamat, jp.nm_ruang,
            ps1.nm_program_studi as nm_pilihan_1,
            ps2.nm_program_studi as nm_pilihan_2,
            ps3.nm_program_studi as nm_pilihan_3,
            ps4.nm_program_studi as nm_pilihan_4,
            p.id_jenjang, jp.materi, jp.dua_hari_test,
            to_char(p.tgl_awal_verifikasi,'YYYY-MM-DD HH24:MI:SS') as tgl_awal_verifikasi,
            to_char(p.tgl_akhir_verifikasi, 'YYYY-MM-DD HH24:MI:SS') as tgl_akhir_verifikasi,
            to_char(tgl_verifikasi_ppmb, 'YYYY-MM-DD') as tgl_verifikasi_ppmb,
            to_char(systimestamp, 'YYYY-MM-DD HH24:MI:SS') as waktu_sekarang,
			jp.waktu,
			p.nm_penerimaan,
			to_char(jp.tgl_test3, 'YYYY-MM-DD') as tgl_test3,
			jp.tiga_hari_test
        from aucc.calon_mahasiswa_baru cmb
        join aucc.calon_mahasiswa_data cmd on cmd.id_c_mhs = cmb.id_c_mhs
        join aucc.penerimaan p on p.id_penerimaan = cmb.id_penerimaan
        join aucc.voucher v on v.kode_voucher = cmb.kode_voucher
        left join aucc.jadwal_ppmb jp on jp.id_jadwal_ppmb = cmd.id_jadwal_ppmb
        left join program_studi ps1 on ps1.id_program_studi = cmb.id_pilihan_1
        left join program_studi ps2 on ps2.id_program_studi = cmb.id_pilihan_2
        left join program_studi ps3 on ps3.id_program_studi = cmb.id_pilihan_3
        left join program_studi ps4 on ps4.id_program_studi = cmb.id_pilihan_4
        where no_ujian = '{$no_ujian}'");
    $row = $db->FetchRow();
    
    if ($row)
    {
        foreach ($row as $col)
        {
            echo "{$col}|";
        }
    }
    
    exit();
}
?>
