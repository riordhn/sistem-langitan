<?php
ini_set('memory_limit', '-1');

require '../../config.php';

if ($_POST['function'] == 'Query')
{
	$result = $db->Query($_POST['sql']);
	
	if ($result == TRUE)
	{
		echo 'TRUE';
	}
	else
	{
		echo 'FALSE';
	}
}

if ($_POST['function'] == 'QueryToArray')
{
	header("Content-type: application/json");
	echo json_encode($db->QueryToArray($_POST['sql']));
}
