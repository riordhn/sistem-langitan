<?php
include ('common.php');
require_once ('ociFunction.php');

if ($user->Role() == 24) {
    $id = $_GET['id'];

    if (isset($_POST['submit'])) {
        $id_pengguna = $_POST['id_pengguna'];
        $nama = $_POST['nama'];
        $lokasi = $_POST['lokasi'];
        $status_luar = $_POST['status_luar'];
        $kota_lokasi = $_POST['kota_lokasi'];
        $tgl_mulai = $_POST['tgl_mulai'];
        $tgl_selesai = $_POST['tgl_selesai'];
        $penyelenggara = $_POST['penyelenggara'];
        $jenis = $_POST['jenis'];
        $kedudukan = $_POST['kedudukan'];
        $tingkat = $_POST['tingkat'];
        
        InsertData("insert into sejarah_kegaiatan_pegawai (id_pengguna,jenis_kegiatan_pegawai, nama_kegiatan_pegawai, status_luar_negeri,lokasi_kegiatan_pegawai, id_kota_kegiatan_pegawai, tgl_mulai_kegiatan_pegawai, tgl_selesai_kegiatan_pegawai,kedudukan_kegiatan_pegawai,penyelenggara_kegiatan_pegawai,tingkat_kegiatan_pegawai,status_valid) 
                    values ('{$id_pengguna}','{$jenis}',upper('{$nama}'),'{$status_luar}','{$lokasi}','{$kota_lokasi}',to_date('{$tgl_mulai}','DD-MM-YYYY'),to_date('{$tgl_selesai}','DD-MM-YYYY'),'{$kedudukan}','{$penyelenggara}','{$tingkat}',1)");

        echo '<script>alert("Data berhasil ditambahkan")</script>';
        echo '<script>window.parent.document.location.reload();</script>';
    }
    $negara = getData("SELECT * FROM NEGARA ORDER BY NM_NEGARA");
    $smarty->assign('negara',$negara);
    $smarty->display('insert/insert_kegpeg.tpl');
} else {
    header("location: /logout.php");
    exit();
}
?>