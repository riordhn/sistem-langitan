<?php
include ('common.php');
require_once ('ociFunction.php');

if ($user->Role() == 24) {
    $id = $_GET['id'];

    if (isset($_POST['submit'])) {
        $id_pengguna = $_POST['id_pengguna'];
        $jabatan = $_POST['jabatan'];
        $nomor = $_POST['nomor'];
        $tgl_sk = $_POST['tgl_sk'];
        $tgl_tmt = $_POST['tgl_tmt'];
        $asal = $_POST['asal'];
        $keterangan = $_POST['keterangan'];
        $ttd = $_POST['ttd'];

        InsertData("insert into sejarah_jabatan_struktural (id_pengguna, id_jabatan_struktural, no_sk_sej_jab_struktural, asal_sk_sej_jab_struktural, ket_sk_sej_jab_struktural, tgl_sk_sej_jab_struktural,tmt_sej_jab_struktural,ttd_sk_sej_jab_struktural,valid_sd) 
                    values ('{$id_pengguna}','{$jabatan}','{$nomor}','{$asal}','{$keterangan}',to_date('{$tgl_sk}','DD-MM-YYYY'),to_date('{$tgl_tmt}','DD-MM-YYYY'),'{$ttd}',1)");

        echo '<script>alert("Data berhasil ditambahkan")</script>';
        echo '<script>window.parent.document.location.reload();</script>';
    }
    $id_struk = getData("SELECT * FROM JABATAN_STRUKTURAL ORDER BY ID_JABATAN_STRUKTURAL");
    $smarty->assign('ID_STRUK', $id_struk);
    $smarty->display('insert/insert_tambahan.tpl');
} else {
    header("location: /logout.php");
    exit();
}
?>