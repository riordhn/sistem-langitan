<?php
include ('common.php');
require_once ('ociFunction.php');

if (isset($_GET['action']) == 'edit_biodata') {
    $id_dsn = $_GET['id'];
    $id_pengguna = $user->ID_PENGGUNA;
    $id_fak = $user->ID_FAKULTAS;

    $dosen = getData("select dsn.id_pengguna, dsn.id_dosen, dsn.nip_dosen as username, dsn.nidn_dosen, dsn.serdos, upper(dsn.alamat_rumah_dosen) as alamat_rumah_dosen, dsn.tlp_dosen, dsn.mobile_dosen, dsn.status_dosen,
		upper(pgg.nm_pengguna) as nm_pengguna, pgg.gelar_depan, pgg.gelar_belakang, pgg.id_kota_lahir, TO_CHAR(pgg.tgl_lahir_pengguna, 'DD-MM-YYYY') as tgl_lahir_pengguna, pgg.email_pengguna, pgg.email_alternate,
		pgg.email_pengguna, pgg.kelamin_pengguna, dsn.id_golongan, upper(gol.nm_golongan) as nm_golongan, upper(gol.nm_pangkat) as nm_pangkat, dsn.id_jabatan_fungsional, upper(fgs.nm_jabatan_fungsional) as nm_jabatan_fungsional,
		dsn.id_jabatan_pegawai, upper(jab.nm_jabatan_pegawai) as nm_jabatan_pegawai, dsn.id_status_pengguna, dsn.id_program_studi_sd, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_fakultas) as nm_fakultas,
		dep.nm_departemen, dpf.id_departemen, dsn.npwp, dsn.rekening, dsn.asal_institusi
		from dosen dsn
		left join pengguna pgg on dsn.id_pengguna=pgg.id_pengguna
		left join dosen_departemen dpf on dsn.id_dosen=dpf.id_dosen
		left join departemen dep on dep.id_departemen=dpf.id_departemen
		left join program_studi pst on dsn.id_program_studi_sd=pst.id_program_studi
		left join fakultas fak on pst.id_fakultas=fak.id_fakultas
		left join jenjang jjg on pst.id_jenjang=jjg.id_jenjang
		left join golongan gol on gol.id_golongan=dsn.id_golongan
		left join jabatan_fungsional fgs on fgs.id_jabatan_fungsional=dsn.id_jabatan_fungsional
		left join jabatan_pegawai jab on jab.id_jabatan_pegawai=dsn.id_jabatan_pegawai
		where dsn.id_dosen=$id_dsn");
    $smarty->assign('DOSEN', $dosen);

    $get_photo = getvar("select nip_dosen as photo from dosen where id_dosen=$id_dsn");
    $filename = "../../foto_pegawai/" . $get_photo['PHOTO'] . ".JPG";
    if (file_exists($filename)) {
        $photo = $filename;
    } else {
        $photo = 'includes/images/unknown.png';
    }
    $smarty->assign('PHOTO', $photo);

    $sts_dsn = getData("select distinct status_dosen from dosen where status_dosen is not null order by status_dosen");
    $smarty->assign('STS_DSN', $sts_dsn);

    $sts_aktif = getData("select id_status_pengguna, upper(nm_status_pengguna) as nm_status_pengguna from status_pengguna where id_role=4 order by status_aktif desc, nm_status_pengguna");
    $smarty->assign('STS_AKTIF', $sts_aktif);

    $jk = getData("select distinct kelamin_pengguna, case when kelamin_pengguna='1' then 'LAKI-LAKI' else 'PEREMPUAN' end as nm_kelamin_pengguna from pengguna where kelamin_pengguna in ('1', '2') order by kelamin_pengguna");
    $smarty->assign('JK', $jk);

    $jab = getData("select id_jabatan_pegawai, upper(nm_jabatan_pegawai) as nm_jabatan_pegawai from jabatan_pegawai order by id_jabatan_pegawai");
    $smarty->assign('JAB', $jab);

    $status = getData("select id_status_pengguna, upper(nm_status_pengguna) as nm_status_pengguna from status_pengguna order by id_status_pengguna");
    $smarty->assign('STATUS', $status);

    $dept = getData("select id_departemen, upper(nm_departemen) as nm_departemen from departemen where id_fakultas=$id_fak order by nm_departemen");
    $smarty->assign('DEPT', $dept);

    $kdprodi = getData("
		select pst.id_program_studi, upper(substr(jjg.nm_jenjang,1,2)||' - '||pst.nm_program_studi) as nm_program_studi 
		from program_studi pst 
		left join jenjang jjg on pst.id_jenjang=jjg.id_jenjang 
		where pst.id_fakultas=$id_fak
		order by jjg.nm_jenjang, pst.nm_program_studi
		");
    $smarty->assign('PRO', $kdprodi);

    $data_kota = array();
    $provinsi = getData("SELECT * FROM PROVINSI WHERE ID_NEGARA=114 ORDER BY NM_PROVINSI");
    foreach ($provinsi as $data) {
        array_push($data_kota, array(
            'nama' => $data['NM_PROVINSI'],
            'kota' => getData("SELECT id_kota, tipe_dati2||' '||nm_kota as kota FROM KOTA WHERE ID_PROVINSI='{$data['ID_PROVINSI']}' ORDER BY TIPE_DATI2, NM_KOTA")
        ));
    }
    $smarty->assign('data_kota', $data_kota);
    $smarty->assign('count_provinsi', count($data_kota));

    $smarty->display('dosen_edit_biodata_fk.tpl');
}

if (isset($_POST['action']) == 'update') {
    $id_dosen = $_POST['id_dosen'];
    $id_pengguna = $_POST['id_pengguna'];

// tabel pengguna
    $gelar_depan = $_POST['gelar_dpn'];
    $gelar_belakang = $_POST['gelar_blkg'];
    $nm_pengguna = $_POST['nm_lengkap'];
    $id_kota_lahir = $_POST['id_kota_lahir'];
    $tgl_lahir_pengguna = $_POST['tgl_lahir'];
    $email_pengguna = $_POST['email'];
    $kelamin_pengguna = $_POST['jk'];

    gantidata("update pengguna set gelar_depan=trim('" . $gelar_depan . "'), gelar_belakang=trim('" . $gelar_belakang . "'), email_alternate=trim(lower('" . $email_pengguna . "')),
	nm_pengguna=trim(upper('" . $nm_pengguna . "')), id_kota_lahir=$id_kota_lahir, tgl_lahir_pengguna=to_date('" . $tgl_lahir_pengguna . "','DD-MM-YYYY'), kelamin_pengguna='" . $kelamin_pengguna . "'	
	where id_pengguna=$id_pengguna");

// tabel dosen
    $nip_dosen = $_POST['nip'];
    $nidn = $_POST['nidn'];
    $serdos = $_POST['serdos'];
    $id_program_studi = $_POST['prodi'];
    $status_dosen = $_POST['status_dsn'];
    $id_status_pengguna = $_POST['aktif'];
    $alamat_rumah_dosen = $_POST['alamat'];
    $npwp = $_POST['npwp'];
    $rekening = $_POST['rekening'];
    $tlp = $_POST['tlp'];
    $hp = $_POST['hp'];
    $asal = $_POST['asal'];

    gantidata("update dosen set nidn_dosen=trim('" . $nidn . "'), serdos=trim('" . $serdos . "'), id_program_studi_sd=$id_program_studi, npwp=trim('" . $npwp . "'), rekening=trim('" . $rekening . "'),
	mobile_dosen=trim('" . $hp . "'), tlp_dosen=trim('" . $tlp . "'), asal_institusi=trim(upper('" . $asal . "')),
	id_program_studi=$id_program_studi, status_dosen='" . $status_dosen . "', id_status_pengguna=$id_status_pengguna, alamat_rumah_dosen=trim(upper('" . $alamat_rumah_dosen . "'))
	where id_dosen=$id_dosen");

// tabel departemen
    $id_departemen = $_POST['dept'];

    $cek = getvar("select count(*) as ada from dosen_departemen where id_dosen=$id_dosen");
    $ada = $cek['ADA'];

    if ($ada >= 1) {
        gantidata("update dosen_departemen set id_departemen=$id_departemen where id_dosen=$id_dosen");
    } else {
        tambahdata("dosen_departemen", "id_departemen,id_dosen", "$id_departemen,$id_dosen");
    }

    header("location:http://{$_SERVER['HTTP_HOST']}/modul/kepegawaian-fakultas/#data_dosen-dosen_tetap!dosen_detail_fk.php?action=detail&id=" . $id_pengguna . "");
    //echo '<script>location.href="javascript:history.go(-2)";</script>';}
}
?>