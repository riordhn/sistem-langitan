<?php
//Yudi Sulistya, 29/03/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

$id_pengguna=$user->ID_PENGGUNA;
$id_fak=$user->ID_FAKULTAS;

if(!empty($_POST["bulan"]) ) {
			$isi .= '
			<br><br>
			<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
			<tr>
			<td valign=top>';
			
			$isi .='<table cellspacing=0 border=1>
				<tr>
					<th><b>Kategori</b></th>
					<th><b>Status</b></th>
					<th><b>Nilai</b></th>
				</tr>
				';
				$q = getData("
				SELECT RENUM_KATEGORI_NILAI.KATEGORI, RENUM_KATEGORI_NILAI.STATUSTK, RENUM_NILAI_PEGAWAI.NILAI, RENUM_KATEGORI_NILAI.ID_RENUM_KATEGORI_NILAI 
				FROM RENUM_KATEGORI_NILAI
				LEFT JOIN RENUM_NILAI_PEGAWAI ON RENUM_NILAI_PEGAWAI.ID_RENUM_KATEGORI_NILAI = RENUM_KATEGORI_NILAI.ID_RENUM_KATEGORI_NILAI 
				LEFT JOIN RENUM_PEGAWAI ON RENUM_PEGAWAI.ID_RENUM_PEGAWAI = RENUM_NILAI_PEGAWAI.ID_RENUM_PEGAWAI
				AND TO_CHAR(RENUM_NILAI_PEGAWAI.TGL_NILAI, 'YYYY-MM') = '".date("Y")."-".$_POST["bulan"]."' AND RENUM_PEGAWAI.ID_PENGGUNA = '$id_pengguna'
				order by statustk ");

				foreach($q as $r) {
					$isi .= '
					<tr>
						<td style="padding:5px;">'.$r['KATEGORI'].'</td>
						<td style="padding:5px;">'.$r['STATUSTK'].'</td>
						<td style="padding:5px;" align=center>'.$r['NILAI'].'</td>
					</tr>
					';
				}
				$isi .= '
				</table>';

			$isi .= '
			</td>
			<td valign=top style="padding-left:30px;">
			';
			$periode=$_POST["bulan"].'/'.date('Y');
			$r = getvar("SELECT
	ROUND (x.penyesuai - (y.pengurang * penyesuai / 100)) AS ipk
FROM
	(
		SELECT
			ROUND ((c.nilai_max - (c.nilai_keg * 50)) * ROUND (AVG(nilai)) * 5000 / 100) AS penyesuai,
			statustk,
			a.id_renum_pegawai
		FROM
			renum_nilai_pegawai a, renum_kategori_nilai b, renum_pegawai c
		WHERE
			a.id_renum_pegawai = c.id_renum_pegawai AND a.id_kategori_nilai = b.id_kategori_nilai 
			AND a.id_pengguna = '".$id_pengguna."' AND b.statustk = 'penyesuai' AND periode = '$periode'
		GROUP BY
			b.statustk
	) x,
	(
		SELECT
			SUM (nilai) AS pengurang,
			statustk,
			a.id_renum_pegawai
		FROM
			renum_nilai_pegawai a, renum_kategori_nilai b, renum_pegawai c
		WHERE
			a.id_renum_pegawai = c.id_renum_pegawai AND a.id_kategori_nilai = b.id_kategori_nilai  
			AND a.id_pengguna = '".$id_pengguna."' AND b.statustk = 'pengurang' AND periode = '$periode'
		GROUP BY
			b.statustk
	) y
WHERE
	x.id_renum_pegawai = y.id_renum_pegawai");



			$isi .= '
			<b>Perhitungan Nilai IPK :</b><br><br>Berdasarkan Penilaian dari atasan saudara atas kinerja selama <b>satu bulan</b> maka, menghasilkan perhitungaan <b>Insentif</b> sebesar : <br><br><font size=6>Rp. '.number_format($r['IPK'],0,",",".").',-</font>
			</td>
			</tr>
			</table>
			';
		}
		
$smarty->assign('isi', $isi);		
$smarty->display('renum_lihat_nilai.tpl');
?>