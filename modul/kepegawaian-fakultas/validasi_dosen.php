<?php
//Yudi Sulistya, 10/07/2013

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

$id_pengguna= $user->ID_PENGGUNA;
$id_fak=$user->ID_FAKULTAS;
$smarty->assign('kdfak',$id_fak);

$hasil=getData("select pgg.id_pengguna, nip_dosen, 
case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna, 
case when sts.status_valid = '1' then 1 else 0 end as status_valid
from dosen dsn
left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
left join validasi_pegawai sts on sts.id_pengguna=dsn.id_pengguna
where dsn.id_program_studi in 
(
select id_program_studi from program_studi where id_fakultas = $id_fak
)
and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
order by status_valid
");
$smarty->assign('DOSEN',$hasil);

$smarty->display('validasi_dosen.tpl');
?>