<?php
//Yudi Sulistya, 29/03/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

$id_pengguna=$user->ID_PENGGUNA;
$id_fak=$user->ID_FAKULTAS;

$kd_prodi=getData("select id_unit_kerja, upper(nm_unit_kerja) as nm_program_studi from unit_kerja where id_fakultas=$id_fak order by nm_unit_kerja");
$smarty->assign('KD_PRODI', $kd_prodi);

if ($request_method == 'GET' or $request_method == 'POST')
{
		$id_prodi = get('id','');
		$uk=getData("select upper(nm_unit_kerja)||' ('||count(peg.nip_pegawai)||' karyawan)' as unit_kerja
		from pegawai peg
		left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
		where uk.id_unit_kerja like '".$id_prodi."' and uk.id_fakultas=$id_fak and peg.status_pegawai='KONTRAK'
		and peg.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=22 and status_aktif=1)
		group by uk.nm_unit_kerja");
		$smarty->assign('UK', $uk);

		$count=getvar("select count(*) as cek from
		(select peg.id_pengguna, peg.nip_pegawai as username,
		case when gelar_belakang is null then trim(gelar_depan||' '||upper(nm_pengguna))
		else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		upper(nm_unit_kerja) as nm_unit_kerja
		from pegawai peg
		left join pengguna pgg on peg.id_pengguna=pgg.id_pengguna
		left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
		where uk.id_unit_kerja like '".$id_prodi."' and uk.id_fakultas=$id_fak and peg.status_pegawai='KONTRAK'
		and peg.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=22 and status_aktif=1))");
		$smarty->assign('DATA', $count['CEK']);

		$pegawai=getData("select peg.id_pengguna, peg.nip_pegawai as username,
		case when gelar_belakang is null then trim(gelar_depan||' '||upper(nm_pengguna))
		else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		upper(nm_unit_kerja) as nm_unit_kerja
		from pegawai peg
		left join pengguna pgg on peg.id_pengguna=pgg.id_pengguna
		left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
		where uk.id_unit_kerja like '".$id_prodi."' and uk.id_fakultas=$id_fak and peg.status_pegawai='KONTRAK'
		and peg.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=22 and status_aktif=1)");
		$smarty->assign('PEGAWAI', $pegawai);
}

$smarty->display('kp_karyawan_kontrak.tpl');

?>