<?php
//Yudi Sulistya, 01/09/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');
die('Mohon Maaf Sementara fitur ini di non aktifkan');
if (isset($_POST['action'])=='add')
{
// tabel pengguna
	$username = $_POST['nip'];
	$gelar_depan = $_POST['gelar_dpn'];
	$gelar_belakang = $_POST['gelar_blkg'];
	$nm_pengguna = $_POST['nm_lengkap'];
	$id_kota_lahir = $_POST['id_kota_lahir'];
	$tgl_lahir_pengguna = $_POST['tgl_lahir'];
	$email_pengguna = $_POST['email'];
	$kelamin_pengguna = $_POST['jk'];

// default password
	//$se1 = $user->Encrypt($username);
// new password encrypt class
	$password_hash = sha1($username);
	$password_encrypted = $user->PublicKeyEncrypt($username);

	tambahdata("pengguna", "username, gelar_depan, gelar_belakang, nm_pengguna, id_kota_lahir, tgl_lahir_pengguna, kelamin_pengguna, email_alternate, id_role, join_table, foto_pengguna,
	password_hash, password_encrypted, password_must_change",
	"trim('".$username."'), trim('".$gelar_depan."'), trim('".$gelar_belakang."'), trim(upper('".$nm_pengguna."')), $id_kota_lahir, to_date('".$tgl_lahir_pengguna."','DD-MM-YYYY'), $kelamin_pengguna,
	trim(lower('".$email_pengguna."')), 22, 1, 'http://".$_SERVER['HTTP_HOST']."/foto_pegawai', '".$password_hash."', '".$password_encrypted."', 1");
	
	$get_id = getvar("select id_pengguna from pengguna where username=trim('".$username."')");
	$id_pengguna = $get_id['ID_PENGGUNA'];
	
// tabel pegawai
	$nip_pegawai = $_POST['nip'];
	$id_golongan = $_POST['gol'];
	$id_unit_kerja = $_POST['unit_kerja'];
	$status_pegawai = $_POST['status_peg'];
	$id_status_pengguna = $_POST['aktif'];
	$alamat_rumah_pegawai = $_POST['alamat'];
	$telp_pegawai = $_POST['tlp'];
	
	tambahdata("pegawai","nip_pegawai, id_golongan, id_jabatan_pegawai, id_unit_kerja, id_unit_kerja_sd, status_pegawai, id_status_pengguna, alamat_pegawai, telp_pegawai, id_pengguna, flag_valid",
	"trim('".$nip_pegawai."'), $id_golongan, 26, $id_unit_kerja, $id_unit_kerja, '".$status_pegawai."', $id_status_pengguna, trim(upper('".$alamat_rumah_pegawai."')), trim('".$telp_pegawai."'), $id_pengguna, 1");

	header("location: /modul/sumberdaya/#data_karyawan!karyawan_detail.php?action=detail&id=".$id_pengguna."");
	
} else {

$sts_peg=getData("select distinct status_pegawai from pegawai where status_pegawai is not null order by status_pegawai");
$smarty->assign('STS_PEG', $sts_peg);

$sts_aktif=getData("select id_status_pengguna, upper(nm_status_pengguna) as nm_status_pengguna from status_pengguna where id_role=22 and status_aktif=1 order by nm_status_pengguna");
$smarty->assign('STS_AKTIF', $sts_aktif);

$listunit=getData("select id_unit_kerja, upper(nm_unit_kerja) as unitkerja,
case when type_unit_kerja='REKTORAT' then 0 
when type_unit_kerja='LEMBAGA' then 1 
when type_unit_kerja='FAKULTAS' then 2 end as urut0,
case when id_fakultas is null then 0 else id_fakultas end as urut1
from unit_kerja where type_unit_kerja in ('REKTORAT', 'LEMBAGA', 'FAKULTAS')
order by urut0, urut1, unitkerja");
$smarty->assign('T_UNIT', $listunit);
		
$jk=getData("select distinct kelamin_pengguna, case when kelamin_pengguna='1' then 'LAKI-LAKI' else 'PEREMPUAN' end as nm_kelamin_pengguna from pengguna where kelamin_pengguna in ('1', '2') order by kelamin_pengguna");
$smarty->assign('JK', $jk);

$gol=getData("select id_golongan, upper(nm_golongan) as nm_golongan from golongan where id_golongan!=41 order by id_golongan desc");
$smarty->assign('GOL', $gol);

$data_kota = array();
$provinsi = getData("SELECT * FROM PROVINSI WHERE ID_NEGARA=114 ORDER BY NM_PROVINSI");
foreach ($provinsi as $data) {
	array_push($data_kota, array(
		'nama' => $data['NM_PROVINSI'],
		'kota' => getData("SELECT id_kota, tipe_dati2||' '||nm_kota as kota FROM KOTA WHERE ID_PROVINSI='{$data['ID_PROVINSI']}' ORDER BY TIPE_DATI2, NM_KOTA")
	));
}
$smarty->assign('data_kota', $data_kota);
$smarty->assign('count_provinsi', count($data_kota));

$smarty->display('tambah_karyawan.tpl');

}

?>