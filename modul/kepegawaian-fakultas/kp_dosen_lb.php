<?php
//Yudi Sulistya, 29/03/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

$id_pengguna=$user->ID_PENGGUNA;
$id_fak=$user->ID_FAKULTAS;

if ($id_fak == 1) {

	echo '
		<script>
		location.href="#data_dosen-dosen_lb!kp_dosen_lb_fk.php";
		</script>
	';

} else {

$kd_prodi=getData("select pst.id_program_studi, upper(substr(jjg.nm_jenjang,1,2))||' - '||upper(pst.nm_program_studi) as nm_program_studi
from program_studi pst
left join jenjang jjg on pst.id_jenjang=jjg.id_jenjang
where pst.id_fakultas=$id_fak
order by jjg.nm_jenjang, pst.nm_program_studi");
$smarty->assign('KD_PRODI', $kd_prodi);

if ($request_method == 'GET' or $request_method == 'POST')
{
		$id_prodi = get('id','');
		$fak=getData("select 'PROGRAM STUDI '||upper(substr(jjg.nm_jenjang,1,2))||' - '||upper(pst.nm_program_studi)||' ('||count(dsn.nip_dosen)||' dosen)' as fakultas
		from dosen dsn
		left join program_studi pst on dsn.id_program_studi_sd=pst.id_program_studi
		left join jenjang jjg on jjg.id_jenjang=pst.id_jenjang
		where dsn.id_program_studi_sd like '".$id_prodi."' and pst.id_fakultas=$id_fak and dsn.status_dosen='LB'
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		group by jjg.nm_jenjang,pst.nm_program_studi");
		$smarty->assign('FAK', $fak);

		$count=getvar("select count(*) as cek from
		(select dsn.id_pengguna, dsn.nip_dosen as username, dsn.nidn_dosen, dsn.serdos,
		case when gelar_belakang is null then trim(gelar_depan||' '||upper(nm_pengguna))
		else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		jjg.nm_jenjang||' - ' ||pst.nm_program_studi as nm_program_studi, sts.nm_status_pengguna
		from dosen dsn
		left join pengguna pgg on dsn.id_pengguna=pgg.id_pengguna
		left join status_pengguna sts on sts.id_status_pengguna=dsn.id_status_pengguna
		left join program_studi pst on dsn.id_program_studi_sd=pst.id_program_studi
		left join jenjang jjg on pst.id_jenjang=jjg.id_jenjang
		where dsn.id_program_studi_sd like '".$id_prodi."' and pst.id_fakultas=$id_fak and dsn.status_dosen='LB'
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1))");
		$smarty->assign('DATA', $count['CEK']);

		$dosen=getData("select dsn.id_pengguna, dsn.nip_dosen as username,
		case when (length(dsn.nidn_dosen)<10 or dsn.nidn_dosen is null) then '-' else dsn.nidn_dosen end as nidn_dosen,
		case when (length(dsn.serdos)<10 or dsn.serdos is null) then '-' else dsn.serdos end as serdos,
		case when gelar_belakang is null then trim(gelar_depan||' '||upper(nm_pengguna))
		else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		jjg.nm_jenjang||' - ' ||pst.nm_program_studi as nm_program_studi, sts.nm_status_pengguna
		from dosen dsn
		left join pengguna pgg on dsn.id_pengguna=pgg.id_pengguna
		left join status_pengguna sts on sts.id_status_pengguna=dsn.id_status_pengguna
		left join program_studi pst on dsn.id_program_studi_sd=pst.id_program_studi
		left join jenjang jjg on pst.id_jenjang=jjg.id_jenjang
		where dsn.id_program_studi_sd like '".$id_prodi."' and pst.id_fakultas=$id_fak and dsn.status_dosen='LB'
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)");
		$smarty->assign('DOSEN', $dosen);
}

$smarty->display('kp_dosen_lb.tpl');
}
?>