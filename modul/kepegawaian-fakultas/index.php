<?php
require_once ('config.php');
require_once ('ociFunction.php');

if ($user->Role() == AUCC_ROLE_KEPEGAWAIAN_FAKULTAS)
{
$id_pengguna = $user->ID_PENGGUNA; 
$kdfak = $user->ID_FAKULTAS;

$data_tab = array();
foreach ($xml_menu->tab as $tab)
{
    array_push($data_tab, array(
        'ID'    => $tab->id,
        'TITLE' => $tab->title,
        'MENU'  => $tab->menu,
        'PAGE'  => $tab->page
    ));
}

$nm_fak=getvar("select upper(nm_fakultas) as fak from fakultas where id_fakultas=$kdfak");
$fak=$nm_fak['FAK'];
$smarty->assign('FAKULTAS', $fak);
	
$smarty->assign('user_login', $user->NM_PENGGUNA);
$smarty->assign('tabs', $data_tab);
$smarty->display('index.tpl');

    // data menu dari $user
    //$smarty->assign('modul_set', $user->MODULs);

    //$smarty->display("index.tpl");

}
else
{
    header("location: /logout.php");
    exit();
}
?>