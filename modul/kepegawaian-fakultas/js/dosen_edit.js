function pddsubmit(id_pendidikan){
	$.ajax({
	type: "POST",
	url: "dosen_detail.php",
	data: "action=update_status_pdd&id_pendidikan="+id_pendidikan,
	cache: false,
	success: function(data){
		if (data == 1)
		{
			window.location.reload(true);
		}
		else
		{
			alert('gagal update');
		}
	}
	});
}

function pddhapus(hapus_pendidikan){
	$.ajax({
	type: "POST",
	url: "dosen_detail.php",
	data: "action=delete_pdd&hapus="+hapus_pendidikan,
	cache: false,
	success: function(data){
		if (data == 1)
		{
			window.location.reload(true);
		}
		else
		{
			alert('gagal hapus');
		}
	}
	});
}

function togglepdd(tambahpdd, pddbtn) {
var ele = document.getElementById(tambahpdd);
var imageEle = document.getElementById(pddbtn);
if(ele.style.display == "block") {
		ele.style.display = "none";
		imageEle.innerHTML = '<img src="includes/images/add.png" alt="Tambah" title="Tambah">';
}
else {
		ele.style.display = "block";
		imageEle.innerHTML = '<img src="includes/images/cancel.png" alt="Batal" title="Batal">';
}
}

function golsubmit(id_golongan){
	$.ajax({
	type: "POST",
	url: "dosen_detail.php",
	data: "action=update_status_gol&id_golongan="+id_golongan,
	cache: false,
	success: function(data){
		if (data == 1)
		{
			window.location.reload(true);
		}
		else
		{
			alert('gagal update');
		}
	}
	});
}

function golhapus(hapus_golongan){
	$.ajax({
	type: "POST",
	url: "dosen_detail.php",
	data: "action=delete_gol&hapus="+hapus_golongan,
	cache: false,
	success: function(data){
		if (data == 1)
		{
			window.location.reload(true);
		}
		else
		{
			alert('gagal hapus');
		}
	}
	});
}

function togglegol(tambahgol, golbtn) {
var ele = document.getElementById(tambahgol);
var imageEle = document.getElementById(golbtn);
if(ele.style.display == "block") {
		ele.style.display = "none";
		imageEle.innerHTML = '<img src="includes/images/add.png" alt="Tambah" title="Tambah">';
}
else {
		ele.style.display = "block";
		imageEle.innerHTML = '<img src="includes/images/cancel.png" alt="Batal" title="Batal">';
}
}

function fsgsubmit(id_fungsional){
	$.ajax({
	type: "POST",
	url: "dosen_detail.php",
	data: "action=update_status_fsg&id_fungsional="+id_fungsional,
	cache: false,
	success: function(data){
		if (data == 1)
		{
			window.location.reload(true);
		}
		else
		{
			alert('gagal update');
		}
	}
	});
}

function fsghapus(hapus_fungsional){
	$.ajax({
	type: "POST",
	url: "dosen_detail.php",
	data: "action=delete_fsg&hapus="+hapus_fungsional,
	cache: false,
	success: function(data){
		if (data == 1)
		{
			window.location.reload(true);
		}
		else
		{
			alert('gagal hapus');
		}
	}
	});
}

function togglefsg(tambahfsg, fsgbtn) {
var ele = document.getElementById(tambahfsg);
var imageEle = document.getElementById(fsgbtn);
if(ele.style.display == "block") {
		ele.style.display = "none";
		imageEle.innerHTML = '<img src="includes/images/add.png" alt="Tambah" title="Tambah">';
}
else {
		ele.style.display = "block";
		imageEle.innerHTML = '<img src="includes/images/cancel.png" alt="Batal" title="Batal">';
}
}

function golfilehapus(hapus_file_golongan){
	$.ajax({
	type: "POST",
	url: "proses/delete_file.php",
	data: "action=delete_file&hapus="+hapus_file_golongan,
	cache: false,
	success: function(data){
		if (data == 1)
		{
			window.location.reload(true);
		}
		else
		{
			alert('gagal hapus');
		}
	}
	});
}

function jabfilehapus(hapus_file_jabatan){
	$.ajax({
	type: "POST",
	url: "proses/delete_file.php",
	data: "action=delete_file&hapus="+hapus_file_jabatan,
	cache: false,
	success: function(data){
		if (data == 1)
		{
			window.location.reload(true);
		}
		else
		{
			alert('gagal hapus');
		}
	}
	});
}

function pddfilehapus(hapus_file_pendidikan){
	$.ajax({
	type: "POST",
	url: "proses/delete_file.php",
	data: "action=delete_file&hapus="+hapus_file_pendidikan,
	cache: false,
	success: function(data){
		if (data == 1)
		{
			window.location.reload(true);
		}
		else
		{
			alert('gagal hapus');
		}
	}
	});
}

function phghapus(hapus_penghargaan){
	$.ajax({
	type: "POST",
	url: "dosen_detail.php",
	data: "action=delete_phg&hapus="+hapus_penghargaan,
	cache: false,
	success: function(data){
		if (data == 1)
		{
			window.location.reload(true);
		}
		else
		{
			alert('gagal hapus');
		}
	}
	});
}

function togglephg(tambahphg, phgbtn) {
var ele = document.getElementById(tambahphg);
var imageEle = document.getElementById(phgbtn);
if(ele.style.display == "block") {
		ele.style.display = "none";
		imageEle.innerHTML = '<img src="includes/images/add.png" alt="Tambah" title="Tambah">';
}
else {
		ele.style.display = "block";
		imageEle.innerHTML = '<img src="includes/images/cancel.png" alt="Batal" title="Batal">';
}
}

function orghapus(hapus_org){
	$.ajax({
	type: "POST",
	url: "dosen_detail.php",
	data: "action=delete_org&hapus="+hapus_org,
	cache: false,
	success: function(data){
		if (data == 1)
		{
			window.location.reload(true);
		}
		else
		{
			alert('gagal hapus');
		}
	}
	});
}

function toggleorg(tambahorg, orgbtn) {
var ele = document.getElementById(tambahorg);
var imageEle = document.getElementById(orgbtn);
if(ele.style.display == "block") {
		ele.style.display = "none";
		imageEle.innerHTML = '<img src="includes/images/add.png" alt="Tambah" title="Tambah">';
}
else {
		ele.style.display = "block";
		imageEle.innerHTML = '<img src="includes/images/cancel.png" alt="Batal" title="Batal">';
}
}

function trghapus(hapus_trg){
	$.ajax({
	type: "POST",
	url: "dosen_detail.php",
	data: "action=delete_trg&hapus="+hapus_trg,
	cache: false,
	success: function(data){
		if (data == 1)
		{
			window.location.reload(true);
		}
		else
		{
			alert('gagal hapus');
		}
	}
	});
}

function toggletrg(tambahtrg, trgbtn) {
var ele = document.getElementById(tambahtrg);
var imageEle = document.getElementById(trgbtn);
if(ele.style.display == "block") {
		ele.style.display = "none";
		imageEle.innerHTML = '<img src="includes/images/add.png" alt="Tambah" title="Tambah">';
}
else {
		ele.style.display = "block";
		imageEle.innerHTML = '<img src="includes/images/cancel.png" alt="Batal" title="Batal">';
}
}

function prohapus(hapus_pro){
	$.ajax({
	type: "POST",
	url: "dosen_detail.php",
	data: "action=delete_pro&hapus="+hapus_pro,
	cache: false,
	success: function(data){
		if (data == 1)
		{
			window.location.reload(true);
		}
		else
		{
			alert('gagal hapus');
		}
	}
	});
}

function togglepro(tambahpro, probtn) {
var ele = document.getElementById(tambahpro);
var imageEle = document.getElementById(probtn);
if(ele.style.display == "block") {
		ele.style.display = "none";
		imageEle.innerHTML = '<img src="includes/images/add.png" alt="Tambah" title="Tambah">';
}
else {
		ele.style.display = "block";
		imageEle.innerHTML = '<img src="includes/images/cancel.png" alt="Batal" title="Batal">';
}
}