<?php
//Yudi Sulistya, 20/04/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

$id_pengguna= $user->ID_PENGGUNA;
$id_fak=$user->ID_FAKULTAS;

$smarty->assign('KDFAK', $id_fak);

$opsi = getData("select id_program_studi as opsi, upper(substr(nm_jenjang,1,2))||' - '||upper(nm_program_studi) as nama
				from program_studi
				left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
				where id_fakultas = $id_fak
				order by nm_jenjang, nm_program_studi");
$smarty->assign('KDPRODI', $opsi);

// default view
		$fak=getData("select 'FAKULTAS '||upper(fak.nm_fakultas) as fakultas
		from fakultas fak
		where fak.id_fakultas=$id_fak");
		$smarty->assign('FAK', $fak);

		$ttl=getvar("select sum(S3+S2+S1+SP+PR) as total
						from
						(
						select
						case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
						case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
						case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1,
						case when pda.nama_pendidikan_akhir='Spesialis' then 1 else 0 end as SP,
						case when pda.nama_pendidikan_akhir='Profesi' then 1 else 0 end as PR
						from sejarah_pendidikan pdd
						left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
						left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
						left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from dosen where id_program_studi_sd in
						(select id_program_studi from program_studi where id_fakultas=$id_fak)
						and id_status_pengguna in
						(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('TTL', $ttl['TOTAL']);

		$gol=getData("select id_jabatan_fungsional, jabatan, sum(S3) as S3, sum(S2) as S2, sum(S1) as S1, sum(SP) as SP, sum(PR) as PR, sum(S3+S2+S1+SP+PR) as total
						from
						(
						select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 0 else dsn.id_jabatan_fungsional end as id_jabatan_fungsional,
						case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else jab.nm_jabatan_fungsional end as jabatan,
						case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
						case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
						case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1,
						case when pda.nama_pendidikan_akhir='Spesialis' then 1 else 0 end as SP,
						case when pda.nama_pendidikan_akhir='Profesi' then 1 else 0 end as PR
						from sejarah_pendidikan pdd
						left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
						left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
						left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from dosen where id_program_studi_sd in
						(select id_program_studi from program_studi where id_fakultas=$id_fak)
						and id_status_pengguna in
						(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
						)
						group by id_pengguna
						)
						)
						group by id_jabatan_fungsional, jabatan
						order by id_jabatan_fungsional");
		$smarty->assign('GOL', $gol);

		$jml=getData("select sum(S3) as S3, sum(S2) as S2, sum(S1) as S1, sum(SP) as SP, sum(PR) as PR, sum(S3+S2+S1+SP+PR) as total
						from
						(
						select
						case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
						case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
						case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1,
						case when pda.nama_pendidikan_akhir='Spesialis' then 1 else 0 end as SP,
						case when pda.nama_pendidikan_akhir='Profesi' then 1 else 0 end as PR
						from sejarah_pendidikan pdd
						left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
						left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
						left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from dosen where id_program_studi_sd in
						(select id_program_studi from program_studi where id_fakultas=$id_fak)
						and id_status_pengguna in
						(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('JML', $jml);

		$psn=getData("select case when sum(S3)=0 then 0 else round((sum(S3)/sum(S3+S2+S1)*100),2) end as S3,
						case when sum(S2)=0 then 0 else round((sum(S2)/sum(S3+S2+S1)*100),2) end as S2,
						case when sum(S1)=0 then 0 else round((sum(S1)/sum(S3+S2+S1)*100),2) end as S1,
						round((sum(S3+S2+S1)/sum(S3+S2+S1)*100),2) as persen
						from
						(
						select
						case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
						case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
						case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1,
						case when pda.nama_pendidikan_akhir='Spesialis' then 1 else 0 end as SP,
						case when pda.nama_pendidikan_akhir='Profesi' then 1 else 0 end as PR
						from sejarah_pendidikan pdd
						left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
						left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
						left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from dosen where id_program_studi_sd in
						(select id_program_studi from program_studi where id_fakultas=$id_fak)
						and id_status_pengguna in
						(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('PSN', $psn);

if (get('action') == 'view')
{
		$id = get('id');

	// data fakultas
	if ($id == 0) {
		$fak=getData("select 'FAKULTAS '||upper(fak.nm_fakultas) as fakultas
		from fakultas fak
		where fak.id_fakultas=$id_fak");
		$smarty->assign('FAK', $fak);

		$ttl=getvar("select sum(S3+S2+S1+SP+PR) as total
						from
						(
						select
						case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
						case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
						case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1,
						case when pda.nama_pendidikan_akhir='Spesialis' then 1 else 0 end as SP,
						case when pda.nama_pendidikan_akhir='Profesi' then 1 else 0 end as PR
						from sejarah_pendidikan pdd
						left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
						left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
						left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from dosen where id_program_studi_sd in
						(select id_program_studi from program_studi where id_fakultas=$id_fak)
						and id_status_pengguna in
						(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('TTL', $ttl['TOTAL']);

		$gol=getData("select id_jabatan_fungsional, jabatan, sum(S3) as S3, sum(S2) as S2, sum(S1) as S1, sum(SP) as SP, sum(PR) as PR, sum(S3+S2+S1+SP+PR) as total
						from
						(
						select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 0 else dsn.id_jabatan_fungsional end as id_jabatan_fungsional,
						case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else jab.nm_jabatan_fungsional end as jabatan,
						case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
						case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
						case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1,
						case when pda.nama_pendidikan_akhir='Spesialis' then 1 else 0 end as SP,
						case when pda.nama_pendidikan_akhir='Profesi' then 1 else 0 end as PR
						from sejarah_pendidikan pdd
						left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
						left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
						left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from dosen where id_program_studi_sd in
						(select id_program_studi from program_studi where id_fakultas=$id_fak)
						and id_status_pengguna in
						(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
						)
						group by id_pengguna
						)
						)
						group by id_jabatan_fungsional, jabatan
						order by id_jabatan_fungsional");
		$smarty->assign('GOL', $gol);

		$jml=getData("select sum(S3) as S3, sum(S2) as S2, sum(S1) as S1, sum(SP) as SP, sum(PR) as PR, sum(S3+S2+S1+SP+PR) as total
						from
						(
						select
						case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
						case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
						case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1,
						case when pda.nama_pendidikan_akhir='Spesialis' then 1 else 0 end as SP,
						case when pda.nama_pendidikan_akhir='Profesi' then 1 else 0 end as PR
						from sejarah_pendidikan pdd
						left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
						left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
						left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from dosen where id_program_studi_sd in
						(select id_program_studi from program_studi where id_fakultas=$id_fak)
						and id_status_pengguna in
						(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('JML', $jml);

		$psn=getData("select case when sum(S3)=0 then 0 else round((sum(S3)/sum(S3+S2+S1)*100),2) end as S3,
						case when sum(S2)=0 then 0 else round((sum(S2)/sum(S3+S2+S1)*100),2) end as S2,
						case when sum(S1)=0 then 0 else round((sum(S1)/sum(S3+S2+S1)*100),2) end as S1,
						round((sum(S3+S2+S1)/sum(S3+S2+S1)*100),2) as persen
						from
						(
						select
						case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
						case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
						case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1,
						case when pda.nama_pendidikan_akhir='Spesialis' then 1 else 0 end as SP,
						case when pda.nama_pendidikan_akhir='Profesi' then 1 else 0 end as PR
						from sejarah_pendidikan pdd
						left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
						left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
						left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from dosen where id_program_studi_sd in
						(select id_program_studi from program_studi where id_fakultas=$id_fak)
						and id_status_pengguna in
						(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('PSN', $psn);

	// data prodi
	} else {
		$fak = getData("select upper(substr(jjg.nm_jenjang,1,2))||' - '||upper(pst.nm_program_studi) as fakultas
					from program_studi pst
					left join jenjang jjg on pst.id_jenjang = jjg.id_jenjang
					where pst.id_program_studi = $id");
		$smarty->assign('FAK', $fak);

		$ttl=getvar("select sum(S3+S2+S1+SP+PR) as total
						from
						(
						select
						case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
						case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
						case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1,
						case when pda.nama_pendidikan_akhir='Spesialis' then 1 else 0 end as SP,
						case when pda.nama_pendidikan_akhir='Profesi' then 1 else 0 end as PR
						from sejarah_pendidikan pdd
						left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
						left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
						left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from dosen where id_program_studi = $id
						and id_status_pengguna in
						(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('TTL', $ttl['TOTAL']);

		$gol=getData("select id_jabatan_fungsional, jabatan, sum(S3) as S3, sum(S2) as S2, sum(S1) as S1, sum(SP) as SP, sum(PR) as PR, sum(S3+S2+S1+SP+PR) as total
						from
						(
						select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 0 else dsn.id_jabatan_fungsional end as id_jabatan_fungsional,
						case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else jab.nm_jabatan_fungsional end as jabatan,
						case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
						case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
						case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1,
						case when pda.nama_pendidikan_akhir='Spesialis' then 1 else 0 end as SP,
						case when pda.nama_pendidikan_akhir='Profesi' then 1 else 0 end as PR
						from sejarah_pendidikan pdd
						left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
						left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
						left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from dosen where id_program_studi = $id
						and id_status_pengguna in
						(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
						)
						group by id_pengguna
						)
						)
						group by id_jabatan_fungsional, jabatan
						order by id_jabatan_fungsional");
		$smarty->assign('GOL', $gol);

		$jml=getData("select sum(S3) as S3, sum(S2) as S2, sum(S1) as S1, sum(SP) as SP, sum(PR) as PR, sum(S3+S2+S1+SP+PR) as total
						from
						(
						select
						case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
						case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
						case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1,
						case when pda.nama_pendidikan_akhir='Spesialis' then 1 else 0 end as SP,
						case when pda.nama_pendidikan_akhir='Profesi' then 1 else 0 end as PR
						from sejarah_pendidikan pdd
						left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
						left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
						left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from dosen where id_program_studi = $id
						and id_status_pengguna in
						(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('JML', $jml);

		$psn=getData("select case when sum(S3)=0 then 0 else round((sum(S3)/sum(S3+S2+S1)*100),2) end as S3,
						case when sum(S2)=0 then 0 else round((sum(S2)/sum(S3+S2+S1)*100),2) end as S2,
						case when sum(S1)=0 then 0 else round((sum(S1)/sum(S3+S2+S1)*100),2) end as S1,
						round((sum(S3+S2+S1)/sum(S3+S2+S1)*100),2) as persen
						from
						(
						select
						case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
						case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
						case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1,
						case when pda.nama_pendidikan_akhir='Spesialis' then 1 else 0 end as SP,
						case when pda.nama_pendidikan_akhir='Profesi' then 1 else 0 end as PR
						from sejarah_pendidikan pdd
						left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
						left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
						left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from dosen where id_program_studi = $id
						and id_status_pengguna in
						(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('PSN', $psn);
	}
}

$smarty->display('rekap_dosen_jab_pend.tpl');

?>