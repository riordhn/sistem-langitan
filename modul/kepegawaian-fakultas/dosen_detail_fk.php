<?php
//Yudi Sulistya, 29/03/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');
include ('includes/encrypt.php');

$depan=time();
$belakang=strrev(time());

$id_fak=$user->ID_FAKULTAS;
$fak = getvar("select singkatan_fakultas from fakultas where id_fakultas=$id_fak");
$nm_fak = $fak['SINGKATAN_FAKULTAS'];

if ($request_method == 'GET')
{
if (get('action')=='detail')
{
		$id_pgg = get('id');

		$dosen=getData("select dsn.id_pengguna, dsn.id_dosen, dsn.nip_dosen as username, dsn.nidn_dosen, dsn.serdos, upper(dsn.alamat_rumah_dosen) as alamat_rumah_dosen, dsn.tlp_dosen, dsn.mobile_dosen, dsn.status_dosen,
		upper(pgg.nm_pengguna) as nm_pengguna, pgg.gelar_depan, pgg.gelar_belakang, TO_CHAR(pgg.tgl_lahir_pengguna, 'DD-MM-YYYY') as tgl_lahir_pengguna, pgg.email_pengguna, pgg.email_alternate,
		pgg.kelamin_pengguna, upper(gol.nm_golongan) as nm_golongan, upper(gol.nm_pangkat) as nm_pangkat, upper(fgs.nm_jabatan_fungsional) as nm_jabatan_fungsional, kt.tipe_dati2||' '||nm_kota as tempat_lahir,
		upper(jab.nm_jabatan_pegawai) as nm_jabatan_pegawai, upper(stp.nm_status_pengguna) as nm_status_pengguna, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_fakultas) as nm_fakultas,
		dep.nm_departemen, dsn.npwp, dsn.rekening, dsn.asal_institusi
		from dosen dsn
		left join pengguna pgg on dsn.id_pengguna=pgg.id_pengguna
		left join dosen_departemen dpf on dsn.id_dosen=dpf.id_dosen
		left join departemen dep on dep.id_departemen=dpf.id_departemen
		left join kota kt on kt.id_kota=pgg.id_kota_lahir
		left join program_studi pst on dsn.id_program_studi_sd=pst.id_program_studi
		left join fakultas fak on pst.id_fakultas=fak.id_fakultas
		left join jenjang jjg on pst.id_jenjang=jjg.id_jenjang
		left join golongan gol on gol.id_golongan=dsn.id_golongan
		left join jabatan_fungsional fgs on fgs.id_jabatan_fungsional=dsn.id_jabatan_fungsional
		left join jabatan_pegawai jab on jab.id_jabatan_pegawai=dsn.id_jabatan_pegawai
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where pgg.id_pengguna=$id_pgg");
		$smarty->assign('DOSEN', $dosen);
		
		$sms=getvar("select case when (pgg.gelar_belakang is null or pgg.gelar_belakang = '') then trim(pgg.gelar_depan||' '||upper(pgg.nm_pengguna)) else trim(pgg.gelar_depan||' '||upper(pgg.nm_pengguna)||', '||pgg.gelar_belakang) end as nm_pengguna,
		dsn.mobile_dosen from dosen dsn left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		where dsn.id_pengguna=$id_pgg");
		$to=$sms['NM_PENGGUNA'];
		$send=$sms['MOBILE_DOSEN'];
		$link = 'proses/kirim_sms.php?'.paramEncrypt($depan.'='.$belakang.$depan.'&yth='.$to.'&kirim='.$send.'&fak='.$nm_fak.'&'.$belakang.'='.$depan.$belakang).'';
		$smarty->assign('LINK', $link);

		$foto=getvar("select case when (pgg.gelar_belakang is null or pgg.gelar_belakang = '') then trim(pgg.gelar_depan||' '||upper(pgg.nm_pengguna)) else trim(pgg.gelar_depan||' '||upper(pgg.nm_pengguna)||', '||pgg.gelar_belakang) end as nm_pengguna,
		dsn.nip_dosen from dosen dsn left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		where dsn.id_pengguna=$id_pgg");
		$to=$foto['NM_PENGGUNA'];
		$file=$foto['NIP_DOSEN'];
		$img = 'proses/upload_photo.php?'.paramEncrypt($depan.'='.$belakang.$depan.'&yth='.$to.'&file='.$file.'&'.$belakang.'='.$depan.$belakang).'';
		$smarty->assign('IMG', $img);
		

		$get_photo=getvar("select username as photo from pengguna where id_pengguna=$id_pgg");
		$filename="../../foto_pegawai/".$get_photo['PHOTO'].".JPG";
		if (file_exists($filename)) {
			$photo = $filename;
		} else {
			$photo = 'includes/images/unknown.png';
		}
		$smarty->assign('PHOTO', $photo);
			
		$gol_dosen=getData("
		select * from
		(
		(select sg.id_sejarah_golongan, sg.id_golongan, upper(gol.nm_golongan) as nm_golongan, sg.no_sk_sejarah_golongan, sg.asal_sk_sejarah_golongan,
		sg.keterangan_sk_sejarah_golongan, TO_CHAR(sg.tmt_sejarah_golongan, 'DD-MM-YYYY') as tmt_sejarah_golongan, sg.status_akhir, TO_CHAR(sg.tmt_sejarah_golongan, 'YYYY') as tahun,
		'SK_'||replace(upper(gol.nm_golongan),'/','_') as kategori, TO_CHAR(sg.tgl_sk_sejarah_golongan, 'DD-MM-YYYY') as tgl_sk_sejarah_golongan, sg.ttd_sk_nama_pejabat		from sejarah_golongan sg
		left join pengguna pgg on pgg.id_pengguna=sg.id_pengguna
		left join golongan gol on gol.id_golongan=sg.id_golongan
		where pgg.id_pengguna=$id_pgg) a
		left join
		(select fl.id_upload_file, replace(fl.deskripsi,'GOL_','') as id_sejarah_golongan, 'http://'||sv.ip_upload_server||pd.path_upload_folder||'/'||pd.nama_upload_folder||'/'||fd.nama_upload_folder||'/'||fl.nama_upload_file as files
		from upload_file fl
		join upload_folder fd on fd.id_upload_folder=fl.id_upload_folder
		join upload_folder pd on fd.id_parent_folder=pd.id_upload_folder
		join upload_server sv on sv.id_upload_server=fd.id_upload_server
		where fd.nama_upload_folder='".$id_pgg."' and fl.deskripsi like 'GOL_%') b
		on a.id_sejarah_golongan=b.id_sejarah_golongan
		)
		order by a.status_akhir desc, a.tahun desc
		");
		$smarty->assign('GOL', $gol_dosen);

		$jab_dosen=getData("
		select * from
		(
		(select sf.id_sejarah_jabatan_fungsional, sf.id_jabatan_fungsional, jab.nm_jabatan_fungsional, sf.no_sk_sej_jab_fungsional, sf.asal_sk_sej_jab_fungsional,
		sf.ket_sk_sej_jab_fungsional, TO_CHAR(sf.tmt_sej_jab_fungsional, 'DD-MM-YYYY') as tmt_sej_jab_fungsional, sf.status_akhir, TO_CHAR(sf.tmt_sej_jab_fungsional, 'YYYY') as tahun,
		'SK_'||replace(upper(jab.nm_jabatan_fungsional),' ','_') as kategori, TO_CHAR(sf.tgl_sk_sej_jab_fungsional, 'DD-MM-YYYY') as tgl_sk_sej_jab_fungsional, sf.ttd_sk_sej_jab_fungsional
		from sejarah_jabatan_fungsional sf
		left join pengguna pgg on pgg.id_pengguna=sf.id_pengguna
		left join jabatan_fungsional jab on jab.id_jabatan_fungsional=sf.id_jabatan_fungsional
		where pgg.id_pengguna=$id_pgg) a
		left join
		(select fl.id_upload_file, replace(fl.deskripsi,'JAB_','') as id_sejarah_jabatan_fungsional, 'http://'||sv.ip_upload_server||pd.path_upload_folder||'/'||pd.nama_upload_folder||'/'||fd.nama_upload_folder||'/'||fl.nama_upload_file as files
		from upload_file fl
		join upload_folder fd on fd.id_upload_folder=fl.id_upload_folder
		join upload_folder pd on fd.id_parent_folder=pd.id_upload_folder
		join upload_server sv on sv.id_upload_server=fd.id_upload_server
		where fd.nama_upload_folder='".$id_pgg."' and fl.deskripsi like 'JAB_%') b
		on a.id_sejarah_jabatan_fungsional=b.id_sejarah_jabatan_fungsional
		)
		order by a.status_akhir desc, a.tahun desc");
		$smarty->assign('JAB', $jab_dosen);

		$pdd_dosen=getData("
		select * from
		(
		(select pdd.*, to_date(pdd.tahun_lulus_pendidikan,'YYYY') as tahun_lulus, pda.nama_pendidikan_akhir, 'IJASAH_'||upper(pda.nama_pendidikan_akhir) as kategori
		from sejarah_pendidikan pdd
		left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
		left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
		where pgg.id_pengguna=$id_pgg) a
		left join
		(select fl.id_upload_file, replace(fl.deskripsi,'PDD_','') as id_sejarah_pendidikan, 'http://'||sv.ip_upload_server||pd.path_upload_folder||'/'||pd.nama_upload_folder||'/'||fd.nama_upload_folder||'/'||fl.nama_upload_file as files
		from upload_file fl
		join upload_folder fd on fd.id_upload_folder=fl.id_upload_folder
		join upload_folder pd on fd.id_parent_folder=pd.id_upload_folder
		join upload_server sv on sv.id_upload_server=fd.id_upload_server
		where fd.nama_upload_folder='".$id_pgg."' and fl.deskripsi like 'PDD_%') b
		on a.id_sejarah_pendidikan=b.id_sejarah_pendidikan
		)
		order by a.status_akhir desc, a.tahun_lulus desc");
		$smarty->assign('PEND', $pdd_dosen);

		$pms_dosen=getData("select pms.*
		from dosen_pengmas pms
		left join dosen dsn on dsn.id_dosen=pms.id_dosen
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		where pgg.id_pengguna=$id_pgg order by thn_dosen_pengmas desc");
		$smarty->assign('PEMS', $pms_dosen);

		$phg_dosen=getData("select phg.*, to_date(phg.thn_dosen_penghargaan||'-01-01', 'yyyy-mm-dd') as thn
		from dosen_penghargaan phg
		left join dosen dsn on dsn.id_dosen=phg.id_dosen
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		where pgg.id_pengguna=$id_pgg order by thn_dosen_penghargaan desc");
		$smarty->assign('PEHG', $phg_dosen);

		$pbk_dosen=getData("select pbk.*
		from dosen_publikasi pbk
		left join dosen dsn on dsn.id_dosen=pbk.id_dosen
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		where pgg.id_pengguna=$id_pgg order by thn_dosen_publikasi desc");
		$smarty->assign('PEBK', $pbk_dosen);

		$org_dosen=getData("select org.*
		from dosen_organisasi org
		left join dosen dsn on dsn.id_dosen=org.id_dosen
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		where pgg.id_pengguna=$id_pgg order by mulai_dosen_org desc, selesai_dosen_org desc");
		$smarty->assign('PORG', $org_dosen);

		$trg_dosen=getData("select trg.*, TO_CHAR(trg.mulai_dos_training, 'DD-MM-YYYY') as mulai, TO_CHAR(trg.selesai_dos_training, 'DD-MM-YYYY') as sampai
		from dosen_training trg
		left join dosen dsn on dsn.id_dosen=trg.id_dosen
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		where pgg.id_pengguna=$id_pgg order by thn_dos_training desc");
		$smarty->assign('PTRG', $trg_dosen);

		$pro_dosen=getData("select pro.*, to_date(pro.thn_dos_prof||'-01-01', 'yyyy-mm-dd') as thn
		from dosen_profesional pro
		left join dosen dsn on dsn.id_dosen=pro.id_dosen
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		where pgg.id_pengguna=$id_pgg order by thn_dos_prof desc");
		$smarty->assign('PROF', $pro_dosen);

		$rwt_dosen=getData("select rwt.nm_dos_riwayat, rwt.jenis_dos_riwayat,
		TO_CHAR(rwt.mulai_dos_riwayat, 'DD-MM-YYYY') as mulai_dos_riwayat,
		TO_CHAR(rwt.selesai_dos_riwayat, 'DD-MM-YYYY') as selesai_dos_riwayat
		from dosen_riwayat rwt
		left join dosen dsn on dsn.id_dosen=rwt.id_dosen
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		where pgg.id_pengguna=$id_pgg order by mulai_dos_riwayat desc, selesai_dos_riwayat desc");
		$smarty->assign('RWYT', $rwt_dosen);
		
		$id_gol=getData("select id_golongan, upper(nm_golongan) as nm_golongan from golongan order by nm_golongan desc");
		$smarty->assign('ID_GOL', $id_gol);
		
		$id_fsg=getData("select id_jabatan_fungsional, nm_jabatan_fungsional from jabatan_fungsional order by id_jabatan_fungsional");
		$smarty->assign('ID_FSG', $id_fsg);

		$id_pdd=getData("select urut, id_pendidikan_akhir, nama_pendidikan_akhir
		from (
		select id_pendidikan_akhir, nama_pendidikan_akhir,
		case when id_pendidikan_akhir=2 then 11 
		when id_pendidikan_akhir=1 then 12
		when id_pendidikan_akhir=10 then 13  
		when id_pendidikan_akhir=9 then 10 
		when id_pendidikan_akhir=8 then 9
		when id_pendidikan_akhir=7 then 8   
		when id_pendidikan_akhir=3 then 7  
		when id_pendidikan_akhir=4 then 6 
		when id_pendidikan_akhir=5 then 5 
		when id_pendidikan_akhir=6 then 4 
		when id_pendidikan_akhir=13 then 3 
		when id_pendidikan_akhir=12 then 2 
		when id_pendidikan_akhir=11 then 1 
		else 0 end as urut
		from pendidikan_akhir)
		order by urut desc");
		$smarty->assign('ID_PDD', $id_pdd);
		
		$id_neg=getData("select id_negara, nm_negara from negara order by nm_negara");
		$smarty->assign('ID_NEG', $id_neg);
		
		$tkt=array("", "Lokal", "Nasional", "Internasional");
		$smarty->assign('TKT', $tkt);
		
		$jns=array("", "Ceramah/Orasi Ilmiah", "Diklat", "Diskusi Ilmiah", "Konferensi", "Kongres", "Lokakarya", "Magang", "Pelatihan", "Seminar", "Short Course", "Simposium", "Talk Show", "Temu Ilmiah", "Workshop");
		$smarty->assign('JNS', $jns);
}
}

$smarty->display('dosen_detail_fk.tpl');

?>