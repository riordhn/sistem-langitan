<?php
//Yudi Sulistya, 22/08/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

$id_pengguna= $user->ID_PENGGUNA;
$id_fak=$user->ID_FAKULTAS;
$smarty->assign('kdfak',$id_fak);

if ($request_method == 'GET' or $request_method == 'POST')
{

// Golongan Usia
	if (get('action') == 'empty' && get('mode') == 'gu') {
		$id = get('id','');
		$id_gol = get('gol','');

		$gol=getvar("select case when upper(nm_golongan) = 'HONORER' then 'HONORER' when upper(nm_golongan) = 'DLB' then 'DLB' else 'GOLONGAN'||' '||upper(nm_golongan) end AS nm_golongan from golongan where id_golongan=$id_gol");
		$smarty->assign('GOL',$gol['NM_GOLONGAN']);

		$usia="tanpa tanggal lahir";
		$smarty->assign('USIA',$usia);

		if ($id == 0 || $id == '') {
		$hasil=getData("select pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where dsn.id_golongan=$id_gol and pgg.tgl_lahir_pengguna is null
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and dsn.id_program_studi_sd in (select id_program_studi from program_studi where id_fakultas=$id_fak)");
		} else {
		$hasil=getData("select pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where dsn.id_golongan=$id_gol and pgg.tgl_lahir_pengguna is null
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and dsn.id_program_studi_sd=$id");
		}
		$smarty->assign('DOSEN',$hasil);
	}

	if(get('action') == 'min' && get('mode') == 'gu') {
		$id = get('id','');
		$id_gol = get('gol','');
		$mulai = get('mulai','');
		$sampai = get('sampai','');

		$gol=getvar("select case when upper(nm_golongan) = 'HONORER' then 'HONORER' when upper(nm_golongan) = 'DLB' then 'DLB' else 'GOLONGAN'||' '||upper(nm_golongan) end AS nm_golongan from golongan where id_golongan=$id_gol");
		$smarty->assign('GOL',$gol['NM_GOLONGAN']);

		$kurang=$sampai+1;
		$usia="usia kurang dari ".$kurang."";
		$smarty->assign('USIA',$usia);

		if ($id == 0 || $id == '') {
		$hasil=getData("select pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where dsn.id_golongan=$id_gol and trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between $mulai and $sampai
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and dsn.id_program_studi_sd in (select id_program_studi from program_studi where id_fakultas=$id_fak)");
		} else {
		$hasil=getData("select pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where dsn.id_golongan=$id_gol and trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between $mulai and $sampai
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and dsn.id_program_studi_sd=$id");
		}
		$smarty->assign('DOSEN',$hasil);
	}

	if(get('action') == 'max' && get('mode') == 'gu') {
		$id = get('id','');
		$id_gol = get('gol','');
		$max = get('max','');

		$gol=getvar("select case when upper(nm_golongan) = 'HONORER' then 'HONORER' when upper(nm_golongan) = 'DLB' then 'DLB' else 'GOLONGAN'||' '||upper(nm_golongan) end AS nm_golongan from golongan where id_golongan=$id_gol");
		$smarty->assign('GOL',$gol['NM_GOLONGAN']);

		$usia="usia lebih dari ".$max."";
		$smarty->assign('USIA',$usia);

		if ($id == 0 || $id == '') {
		$hasil=getData("select pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where dsn.id_golongan=$id_gol and trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > $max
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and dsn.id_program_studi_sd in (select id_program_studi from program_studi where id_fakultas=$id_fak)");
		} else {
		$hasil=getData("select pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where dsn.id_golongan=$id_gol and trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > $max
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and dsn.id_program_studi_sd=$id");
		}
		$smarty->assign('DOSEN',$hasil);
	}

	if(get('action') == 'between' && get('mode') == 'gu') {
		$id = get('id','');
		$id_gol = get('gol','');
		$mulai = get('mulai','');
		$sampai = get('sampai','');

		$gol=getvar("select case when upper(nm_golongan) = 'HONORER' then 'HONORER' when upper(nm_golongan) = 'DLB' then 'DLB' else 'GOLONGAN'||' '||upper(nm_golongan) end AS nm_golongan from golongan where id_golongan=$id_gol");
		$smarty->assign('GOL',$gol['NM_GOLONGAN']);

		$usia="usia antara ".$mulai." sampai dengan ".$sampai." tahun";
		$smarty->assign('USIA',$usia);

		if ($id == 0 || $id == '') {
		$hasil=getData("select pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where dsn.id_golongan=$id_gol and trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between $mulai and $sampai
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and dsn.id_program_studi_sd in (select id_program_studi from program_studi where id_fakultas=$id_fak)");
		} else {
		$hasil=getData("select pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where dsn.id_golongan=$id_gol and trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between $mulai and $sampai
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and dsn.id_program_studi_sd=$id");
		}
		$smarty->assign('DOSEN',$hasil);
	}

// Pendidikan Usia
	if (get('action') == 'empty' && get('mode') == 'pu') {
		$id = get('id','');
		$id_pdd = get('pdd','');

		$gol=getvar("select 'PENDIDIKAN TERAKHIR '||upper(nama_pendidikan_akhir) as nama_pendidikan_akhir from pendidikan_akhir where id_pendidikan_akhir=$id_pdd");
		$smarty->assign('GOL',$gol['NAMA_PENDIDIKAN_AKHIR']);

		$usia="tanpa tanggal lahir";
		$smarty->assign('USIA',$usia);

		if ($id == 0 || $id == '') {
		$hasil=getData("select * from
		(
		select pdd.id_pendidikan_akhir, pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join sejarah_pendidikan pdd on dsn.id_pengguna=pdd.id_pengguna
		left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where pgg.tgl_lahir_pengguna is null
		and pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
		(
		select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
		(
		select id_pengguna from dosen where id_program_studi in
		(select id_program_studi from program_studi where id_fakultas=$id_fak)
		and dsn.id_status_pengguna in
		(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		)
		group by id_pengguna
		)
		)
		where id_pendidikan_akhir=$id_pdd");
		} else {
		$hasil=getData("select * from
		(
		select pdd.id_pendidikan_akhir, pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join sejarah_pendidikan pdd on dsn.id_pengguna=pdd.id_pengguna
		left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where pgg.tgl_lahir_pengguna is null
		and pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
		(
		select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
		(
		select id_pengguna from dosen where id_program_studi=$id
		and dsn.id_status_pengguna in
		(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		)
		group by id_pengguna
		)
		)
		where id_pendidikan_akhir=$id_pdd");
		}
		$smarty->assign('DOSEN',$hasil);
	}

	if(get('action') == 'min' && get('mode') == 'pu') {
		$id = get('id','');
		$id_pdd = get('pdd','');
		$mulai = get('mulai','');
		$sampai = get('sampai','');

		$gol=getvar("select 'PENDIDIKAN TERAKHIR '||upper(nama_pendidikan_akhir) as nama_pendidikan_akhir from pendidikan_akhir where id_pendidikan_akhir=$id_pdd");
		$smarty->assign('GOL',$gol['NAMA_PENDIDIKAN_AKHIR']);

		$kurang=$sampai+1;
		$usia="usia kurang dari ".$kurang."";
		$smarty->assign('USIA',$usia);

		if ($id == 0 || $id == '') {
		$hasil=getData("select * from
		(
		select pdd.id_pendidikan_akhir, pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join sejarah_pendidikan pdd on dsn.id_pengguna=pdd.id_pengguna
		left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between $mulai and $sampai
		and pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
		(
		select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
		(
		select id_pengguna from dosen where id_program_studi in
		(select id_program_studi from program_studi where id_fakultas=$id_fak)
		and dsn.id_status_pengguna in
		(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		)
		group by id_pengguna
		)
		)
		where id_pendidikan_akhir=$id_pdd");
		} else {
		$hasil=getData("select * from
		(
		select pdd.id_pendidikan_akhir, pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join sejarah_pendidikan pdd on dsn.id_pengguna=pdd.id_pengguna
		left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between $mulai and $sampai
		and pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
		(
		select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
		(
		select id_pengguna from dosen where id_program_studi=$id
		and dsn.id_status_pengguna in
		(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		)
		group by id_pengguna
		)
		)
		where id_pendidikan_akhir=$id_pdd");
		}
		$smarty->assign('DOSEN',$hasil);
	}

	if(get('action') == 'max' && get('mode') == 'pu') {
		$id = get('id','');
		$id_pdd = get('pdd','');
		$max = get('max','');

		$gol=getvar("select 'PENDIDIKAN TERAKHIR '||upper(nama_pendidikan_akhir) as nama_pendidikan_akhir from pendidikan_akhir where id_pendidikan_akhir=$id_pdd");
		$smarty->assign('GOL',$gol['NAMA_PENDIDIKAN_AKHIR']);

		$usia="usia lebih dari ".$max."";
		$smarty->assign('USIA',$usia);

		if ($id == 0 || $id == '') {
		$hasil=getData("select * from
		(
		select pdd.id_pendidikan_akhir, pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join sejarah_pendidikan pdd on dsn.id_pengguna=pdd.id_pengguna
		left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > $max
		and pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
		(
		select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
		(
		select id_pengguna from dosen where id_program_studi in
		(select id_program_studi from program_studi where id_fakultas=$id_fak)
		and dsn.id_status_pengguna in
		(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		)
		group by id_pengguna
		)
		)
		where id_pendidikan_akhir=$id_pdd");
		} else {
		$hasil=getData("select * from
		(
		select pdd.id_pendidikan_akhir, pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join sejarah_pendidikan pdd on dsn.id_pengguna=pdd.id_pengguna
		left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > $max
		and pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
		(
		select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
		(
		select id_pengguna from dosen where id_program_studi=$id
		and dsn.id_status_pengguna in
		(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		)
		group by id_pengguna
		)
		)
		where id_pendidikan_akhir=$id_pdd");
		}
		$smarty->assign('DOSEN',$hasil);
	}

	if(get('action') == 'between' && get('mode') == 'pu') {
		$id = get('id','');
		$id_pdd = get('pdd','');
		$mulai = get('mulai','');
		$sampai = get('sampai','');

		$gol=getvar("select 'PENDIDIKAN TERAKHIR '||upper(nama_pendidikan_akhir) as nama_pendidikan_akhir from pendidikan_akhir where id_pendidikan_akhir=$id_pdd");
		$smarty->assign('GOL',$gol['NAMA_PENDIDIKAN_AKHIR']);

		$usia="usia antara ".$mulai." sampai dengan ".$sampai." tahun";
		$smarty->assign('USIA',$usia);

		if ($id == 0 || $id == '') {
		$hasil=getData("select * from
		(
		select pdd.id_pendidikan_akhir, pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join sejarah_pendidikan pdd on dsn.id_pengguna=pdd.id_pengguna
		left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between $mulai and $sampai
		and pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
		(
		select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
		(
		select id_pengguna from dosen where id_program_studi in
		(select id_program_studi from program_studi where id_fakultas=$id_fak)
		and dsn.id_status_pengguna in
		(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		)
		group by id_pengguna
		)
		)
		where id_pendidikan_akhir=$id_pdd");
		} else {
		$hasil=getData("select * from
		(
		select pdd.id_pendidikan_akhir, pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join sejarah_pendidikan pdd on dsn.id_pengguna=pdd.id_pengguna
		left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between $mulai and $sampai
		and pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
		(
		select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
		(
		select id_pengguna from dosen where id_program_studi=$id
		and dsn.id_status_pengguna in
		(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		)
		group by id_pengguna
		)
		)
		where id_pendidikan_akhir=$id_pdd");
		}
		$smarty->assign('DOSEN',$hasil);
	}

// Jabatan Usia
	if (get('action') == 'empty' && get('mode') == 'ju') {
		$id = get('id','');
		$id_jab = get('jab','');

		$usia="tanpa tanggal lahir";
		$smarty->assign('USIA',$usia);

		if($id_jab == 0){
		$gol='Belum memiliki jabatan fungsional';
		$smarty->assign('GOL',$gol);

		if ($id == 0 || $id == '') {
		$hasil=getData("select distinct * from
		(
		select pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where pgg.tgl_lahir_pengguna is null
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and dsn.id_program_studi_sd in (select id_program_studi from program_studi where id_fakultas=$id_fak)
		and (id_jabatan_fungsional is null or id_jabatan_fungsional>4)
		)
		");
		} else {
		$hasil=getData("select distinct * from
		(
		select pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where pgg.tgl_lahir_pengguna is null
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and dsn.id_program_studi_sd=$id
		and (id_jabatan_fungsional is null or id_jabatan_fungsional>4)
		)
		");
		}
		$smarty->assign('DOSEN',$hasil);
		} else {
		$gol=getvar("select 'JABATAN FUNGSIONAL '||upper(nm_jabatan_fungsional) as nm_jabatan_fungsional from jabatan_fungsional where id_jabatan_fungsional=$id_jab");
		$smarty->assign('GOL',$gol['NM_JABATAN_FUNGSIONAL']);

		if ($id == 0 || $id == '') {
		$hasil=getData("select distinct * from
		(
		select dsn.id_jabatan_fungsional, pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where pgg.tgl_lahir_pengguna is null
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and dsn.id_program_studi_sd in (select id_program_studi from program_studi where id_fakultas=$id_fak)
		)
		where id_jabatan_fungsional=$id_jab");
		} else {
		$hasil=getData("select distinct * from
		(
		select dsn.id_jabatan_fungsional, pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where pgg.tgl_lahir_pengguna is null
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and dsn.id_program_studi_sd=$id
		)
		where id_jabatan_fungsional=$id_jab");
		}
		$smarty->assign('DOSEN',$hasil);
		}
	}

	if(get('action') == 'min' && get('mode') == 'ju') {
		$id = get('id','');
		$id_jab = get('jab','');
		$mulai = get('mulai','');
		$sampai = get('sampai','');

		$kurang=$sampai+1;
		$usia="usia kurang dari ".$kurang."";
		$smarty->assign('USIA',$usia);

		if($id_jab == 0){
		$gol='Belum memiliki jabatan fungsional';
		$smarty->assign('GOL',$gol);

		if ($id == 0 || $id == '') {
		$hasil=getData("select distinct * from
		(
		select pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between $mulai and $sampai
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and dsn.id_program_studi_sd in (select id_program_studi from program_studi where id_fakultas=$id_fak)
		and (id_jabatan_fungsional is null or id_jabatan_fungsional>4)
		)
		");
		} else {
		$hasil=getData("select distinct * from
		(
		select pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between $mulai and $sampai
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and dsn.id_program_studi_sd=$id
		and (id_jabatan_fungsional is null or id_jabatan_fungsional>4)
		)
		");
		}
		$smarty->assign('DOSEN',$hasil);
		} else {
		$gol=getvar("select 'JABATAN FUNGSIONAL '||upper(nm_jabatan_fungsional) as nm_jabatan_fungsional from jabatan_fungsional where id_jabatan_fungsional=$id_jab");
		$smarty->assign('GOL',$gol['NM_JABATAN_FUNGSIONAL']);

		if ($id == 0 || $id == '') {
		$hasil=getData("select distinct * from
		(
		select dsn.id_jabatan_fungsional, pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between $mulai and $sampai
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and dsn.id_program_studi_sd in (select id_program_studi from program_studi where id_fakultas=$id_fak)
		)
		where id_jabatan_fungsional=$id_jab");
		} else {
		$hasil=getData("select distinct * from
		(
		select dsn.id_jabatan_fungsional, pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between $mulai and $sampai
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and dsn.id_program_studi_sd=$id
		)
		where id_jabatan_fungsional=$id_jab");
		}
		$smarty->assign('DOSEN',$hasil);
		}
	}

	if(get('action') == 'max' && get('mode') == 'ju') {
		$id = get('id','');
		$id_jab = get('jab','');
		$max = get('max','');

		$usia="usia lebih dari ".$max."";
		$smarty->assign('USIA',$usia);

		if($id_jab == 0){
		$gol='Belum memiliki jabatan fungsional';
		$smarty->assign('GOL',$gol);

		if ($id == 0 || $id == '') {
		$hasil=getData("select distinct * from
		(
		select pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > $max
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and dsn.id_program_studi_sd in (select id_program_studi from program_studi where id_fakultas=$id_fak)
		and (id_jabatan_fungsional is null or id_jabatan_fungsional>4)
		)
		");
		} else {
		$hasil=getData("select distinct * from
		(
		select pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > $max
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and dsn.id_program_studi_sd=$id
		and (id_jabatan_fungsional is null or id_jabatan_fungsional>4)
		)
		");
		}
		$smarty->assign('DOSEN',$hasil);
		} else {
		$gol=getvar("select 'JABATAN FUNGSIONAL '||upper(nm_jabatan_fungsional) as nm_jabatan_fungsional from jabatan_fungsional where id_jabatan_fungsional=$id_jab");
		$smarty->assign('GOL',$gol['NM_JABATAN_FUNGSIONAL']);

		if ($id == 0 || $id == '') {
		$hasil=getData("select distinct * from
		(
		select dsn.id_jabatan_fungsional, pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > $max
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and dsn.id_program_studi_sd in (select id_program_studi from program_studi where id_fakultas=$id_fak)
		)
		where id_jabatan_fungsional=$id_jab");
		} else {
		$hasil=getData("select distinct * from
		(
		select dsn.id_jabatan_fungsional, pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > $max
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and dsn.id_program_studi_sd=$id
		)
		where id_jabatan_fungsional=$id_jab");
		}
		$smarty->assign('DOSEN',$hasil);
		}
	}

	if(get('action') == 'between' && get('mode') == 'ju') {
		$id = get('id','');
		$id_jab = get('jab','');
		$mulai = get('mulai','');
		$sampai = get('sampai','');

		$usia="usia antara ".$mulai." sampai dengan ".$sampai." tahun";
		$smarty->assign('USIA',$usia);
		if($id_jab == 0){
		$gol='Belum memiliki jabatan fungsional';
		$smarty->assign('GOL',$gol);

		if ($id == 0 || $id == '') {
		$hasil=getData("select distinct * from
		(
		select pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between $mulai and $sampai
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and dsn.id_program_studi_sd in (select id_program_studi from program_studi where id_fakultas=$id_fak)
		and (id_jabatan_fungsional is null or id_jabatan_fungsional>4)
		)
		");
		} else {
		$hasil=getData("select distinct * from
		(
		select pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between $mulai and $sampai
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and dsn.id_program_studi_sd=$id
		and (id_jabatan_fungsional is null or id_jabatan_fungsional>4)
		)
		");
		}
		$smarty->assign('DOSEN',$hasil);
		} else {
		$gol=getvar("select 'JABATAN FUNGSIONAL '||upper(nm_jabatan_fungsional) as nm_jabatan_fungsional from jabatan_fungsional where id_jabatan_fungsional=$id_jab");
		$smarty->assign('GOL',$gol['NM_JABATAN_FUNGSIONAL']);

		if ($id == 0 || $id == '') {
		$hasil=getData("select distinct * from
		(
		select dsn.id_jabatan_fungsional, pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between $mulai and $sampai
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and dsn.id_program_studi_sd in (select id_program_studi from program_studi where id_fakultas=$id_fak)
		)
		where id_jabatan_fungsional=$id_jab");
		} else {
		$hasil=getData("select distinct * from
		(
		select dsn.id_jabatan_fungsional, pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between $mulai and $sampai
		and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and dsn.id_program_studi_sd=$id
		)
		where id_jabatan_fungsional=$id_jab");
		}
		$smarty->assign('DOSEN',$hasil);
		}
	}

// Jabatan Pendidikan
	if (get('action') == 'list' && get('mode') == 'jp') {
		$id = get('id','');
		$id_jab = get('jab','');
		$id_pdd = get('pdd','');

		$usia=getvar("select 'PENDIDIKAN TERAKHIR '||upper(nama_pendidikan_akhir) as nama_pendidikan_akhir from pendidikan_akhir where id_pendidikan_akhir=$id_pdd");
		$smarty->assign('USIA',$usia['NAMA_PENDIDIKAN_AKHIR']);

		if($id_jab == 0){
		$gol='Belum memiliki jabatan fungsional';
		$smarty->assign('GOL',$gol);

		if ($id == 0 || $id == '') {
		$hasil=getData("select distinct * from
		(
		select dsn.id_jabatan_fungsional, pdd.id_pendidikan_akhir, pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		left join sejarah_pendidikan pdd on dsn.id_pengguna=pdd.id_pengguna
		where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
		(
		select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
		(
		select id_pengguna from dosen where id_program_studi in
		(select id_program_studi from program_studi where id_fakultas=$id_fak)
		and dsn.id_status_pengguna in
		(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		)
		group by id_pengguna
		)
		)
		where (id_jabatan_fungsional is null or id_jabatan_fungsional>4) and id_pendidikan_akhir=$id_pdd
		");
		} else {
		$hasil=getData("select distinct * from
		(
		select dsn.id_jabatan_fungsional, pdd.id_pendidikan_akhir, pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		left join sejarah_pendidikan pdd on dsn.id_pengguna=pdd.id_pengguna
		where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
		(
		select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
		(
		select id_pengguna from dosen where id_program_studi=$id
		and dsn.id_status_pengguna in
		(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		)
		group by id_pengguna
		)
		)
		where (id_jabatan_fungsional is null or id_jabatan_fungsional>4) and id_pendidikan_akhir=$id_pdd
		");
		}
		$smarty->assign('DOSEN',$hasil);
		} else {
		$gol=getvar("select 'JABATAN FUNGSIONAL '||upper(nm_jabatan_fungsional) as nm_jabatan_fungsional from jabatan_fungsional where id_jabatan_fungsional=$id_jab");
		$smarty->assign('GOL',$gol['NM_JABATAN_FUNGSIONAL']);

		if ($id == 0 || $id == '') {
		$hasil=getData("select distinct * from
		(
		select dsn.id_jabatan_fungsional, pdd.id_pendidikan_akhir, pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		left join sejarah_pendidikan pdd on dsn.id_pengguna=pdd.id_pengguna
		where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
		(
		select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
		(
		select id_pengguna from dosen where id_program_studi in
		(select id_program_studi from program_studi where id_fakultas=$id_fak)
		and dsn.id_status_pengguna in
		(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		)
		group by id_pengguna
		)
		)
		where id_jabatan_fungsional=$id_jab and id_pendidikan_akhir=$id_pdd
		");
		} else {
		$hasil=getData("select distinct * from
		(
		select dsn.id_jabatan_fungsional, pdd.id_pendidikan_akhir, pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		left join sejarah_pendidikan pdd on dsn.id_pengguna=pdd.id_pengguna
		where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
		(
		select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
		(
		select id_pengguna from dosen where id_program_studi=$id
		and dsn.id_status_pengguna in
		(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		)
		group by id_pengguna
		)
		)
		where id_jabatan_fungsional=$id_jab and id_pendidikan_akhir=$id_pdd
		");
		}
		$smarty->assign('DOSEN',$hasil);
		}
	}

// Golongan Jabatan
	if (get('action') == 'list' && get('mode') == 'gj') {
		$id = get('id','');
		$id_jab = get('jab','');
		$id_gol = get('gol','');

		$gol=getvar("select case when upper(nm_golongan) = 'HONORER' then 'HONORER' when upper(nm_golongan) = 'DLB' then 'DLB' else 'GOLONGAN'||' '||upper(nm_golongan) end AS nm_golongan from golongan where id_golongan=$id_gol");
		$smarty->assign('GOL',$gol['NM_GOLONGAN']);

		if($id_jab == 0){
		$usia='Belum memiliki jabatan fungsional';
		$smarty->assign('USIA',$usia);

		if ($id == 0 || $id == '') {
		$hasil=getData("select dsn.id_jabatan_fungsional, dsn.id_golongan, pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where dsn.id_program_studi_sd in
		(select id_program_studi from program_studi where id_fakultas=$id_fak)
		and dsn.id_status_pengguna in
		(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and (id_jabatan_fungsional is null or id_jabatan_fungsional>4) and id_golongan=$id_gol
		");
		} else {
		$hasil=getData("select dsn.id_jabatan_fungsional, dsn.id_golongan, pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where dsn.id_program_studi_sd=$id
		and dsn.id_status_pengguna in
		(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and (id_jabatan_fungsional is null or id_jabatan_fungsional>4) and id_golongan=$id_gol
		");
		}
		$smarty->assign('DOSEN',$hasil);
		} else {
		$usia=getvar("select 'JABATAN FUNGSIONAL '||upper(nm_jabatan_fungsional) as nm_jabatan_fungsional from jabatan_fungsional where id_jabatan_fungsional=$id_jab");
		$smarty->assign('USIA',$usia['NM_JABATAN_FUNGSIONAL']);

		if ($id == 0 || $id == '') {
		$hasil=getData("select dsn.id_jabatan_fungsional, dsn.id_golongan, pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where dsn.id_program_studi_sd in
		(select id_program_studi from program_studi where id_fakultas=$id_fak)
		and dsn.id_status_pengguna in
		(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and id_jabatan_fungsional=$id_jab and id_golongan=$id_gol
		");
		} else {
		$hasil=getData("select dsn.id_jabatan_fungsional, dsn.id_golongan, pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where dsn.id_program_studi_sd=$id
		and dsn.id_status_pengguna in
		(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		and id_jabatan_fungsional=$id_jab and id_golongan=$id_gol
		");
		}
		$smarty->assign('DOSEN',$hasil);
		}
	}

// Pendidikan 5 Tahun Terakhir
	if (get('action') == 'list' && get('mode') == 'p5') {
		$id = get('id','');
		$id_pdd = get('pdd','');
		$id_thn = get('thn','');

		$usia=getvar("select 'PENDIDIKAN TERAKHIR '||upper(nama_pendidikan_akhir) as nama_pendidikan_akhir from pendidikan_akhir where id_pendidikan_akhir=$id_pdd");
		$smarty->assign('USIA',$usia['NAMA_PENDIDIKAN_AKHIR']);

		$gol="TAHUN $id_thn";
		$smarty->assign('GOL',$gol);

		if ($id == 0 || $id == '') {
		$hasil=getData("select pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role,
		tahun_lulus_pendidikan
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		left join sejarah_pendidikan pdd on dsn.id_pengguna=pdd.id_pengguna
		where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
		(
		select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
		(
		select id_pengguna from dosen where id_program_studi in
		(select id_program_studi from program_studi where id_fakultas=$id_fak)
		and dsn.id_status_pengguna in
		(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		)
		group by id_pengguna
		)
		and id_pendidikan_akhir=$id_pdd and to_number(tahun_lulus_pendidikan,9999)<=$id_thn
		");
		} else {
		$hasil=getData("select pgg.id_pengguna, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role,
		tahun_lulus_pendidikan
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		left join sejarah_pendidikan pdd on dsn.id_pengguna=pdd.id_pengguna
		where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
		(
		select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
		(
		select id_pengguna from dosen where id_program_studi=$id
		and dsn.id_status_pengguna in
		(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
		)
		group by id_pengguna
		)
		and id_pendidikan_akhir=$id_pdd and to_number(tahun_lulus_pendidikan,9999)<=$id_thn
		");
		}
		$smarty->assign('DOSEN',$hasil);
	}

// Pendidikan Golongan
	if (get('action') == 'list' && get('mode') == 'pg') {
		$id = get('id','');
		$id_pdd = get('pdd','');
		$id_gol = get('gol','');

		$gol=getvar("select case when upper(nm_golongan) = 'HONORER' then 'HONORER' when upper(nm_golongan) = 'DLB' then 'DLB' else 'GOLONGAN'||' '||upper(nm_golongan) end AS nm_golongan from golongan where id_golongan=$id_gol");
		$smarty->assign('GOL',$gol['NM_GOLONGAN']);

		$usia=getvar("select 'PENDIDIKAN TERAKHIR '||upper(nama_pendidikan_akhir) as nama_pendidikan_akhir from pendidikan_akhir where id_pendidikan_akhir=$id_pdd");
		$smarty->assign('USIA',$usia['NAMA_PENDIDIKAN_AKHIR']);

		if ($id == 0 || $id == '') {
		$hasil=getData("select * from
		(
		select pdd.id_pendidikan_akhir, pgg.id_pengguna, dsn.id_golongan, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join sejarah_pendidikan pdd on dsn.id_pengguna=pdd.id_pengguna
		left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
			(
			select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
			(
			select id_pengguna from dosen where id_program_studi in
			(select id_program_studi from program_studi where id_fakultas=$id_fak)
			and dsn.id_status_pengguna in
			(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
			)
			group by id_pengguna
			)
		)
		where id_pendidikan_akhir=$id_pdd and id_golongan=$id_gol");
		} else {
		$hasil=getData("select * from
		(
		select pdd.id_pendidikan_akhir, pgg.id_pengguna, dsn.id_golongan, nip_dosen, upper(stp.nm_status_pengguna) as nm_status_pengguna,
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_role) as nm_role
		from dosen dsn
		left join program_studi ps on ps.id_program_studi=dsn.id_program_studi_sd
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		left join sejarah_pendidikan pdd on dsn.id_pengguna=pdd.id_pengguna
		left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
		left join role rl on rl.id_role=pgg.id_role
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
			(
			select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
			(
			select id_pengguna from dosen where id_program_studi=$id)
			and dsn.id_status_pengguna in
			(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
			)
			group by id_pengguna
			)
		)
		where id_pendidikan_akhir=$id_pdd and id_golongan=$id_gol");
		}
		$smarty->assign('DOSEN',$hasil);
	}
}

$smarty->display('list_dosen.tpl');

?>