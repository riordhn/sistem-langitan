<?php
//Yudi Sulistya, 15/10/2012

error_reporting (E_ALL & ~E_NOTICE);

require ("../../../config.php");
$db = new MyOracle();

$id_pengguna=$user->ID_PENGGUNA;
$id_fak=$user->ID_FAKULTAS;

$fak = "select lower(nm_fakultas) as nm_fakultas from fakultas where id_fakultas=$id_fak";
$result = $db->Query($fak)or die("salah kueri fak ");
while($r = $db->FetchRow()) {
	$nm_fak = $r[0];
}

if ($user->Role() == AUCC_ROLE_KEPEGAWAIAN_FAKULTAS) {

header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=data_dosen_tetap_".$nm_fak.".xls");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data";

echo '
<table width="100%" border="1" cellspacing="0" cellpadding="0">
   <tr>
	<td bgcolor="#939393" align="center"><b>No.</b></td>
	<td bgcolor="#939393" align="center"><b>NIDN</b></td>
	<td bgcolor="#939393" align="center"><b>Serdos</b></td>
	<td bgcolor="#939393" align="center"><b>NIP</b></td>
	<td bgcolor="#939393" align="center"><b>Nama Dosen</b></td>
	<td bgcolor="#939393" align="center"><b>Status</b></td>
	<td bgcolor="#939393" align="center"><b>Prodi</b></td>
	<td bgcolor="#939393" align="center"><b>Departemen</b></td>
   </tr>';

$dosen="
select dsn.id_pengguna, dsn.nip_dosen as username,
case when (length(dsn.nidn_dosen)<10 or dsn.nidn_dosen is null) then '-' else dsn.nidn_dosen end as nidn_dosen,
case when (length(dsn.serdos)<10 or dsn.serdos is null) then '-' else dsn.serdos end as serdos,
case when gelar_belakang is null then trim(gelar_depan||' '||upper(nm_pengguna))
else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
jjg.nm_jenjang||' - ' ||pst.nm_program_studi as nm_program_studi, sts.nm_status_pengguna, dep.nama_departemen_fk
from dosen dsn
left join pengguna pgg on dsn.id_pengguna=pgg.id_pengguna
left join departemen_fk_dosen dpf on dsn.id_dosen=dpf.id_dosen
left join departemen_fk dep on dep.id_departemen_fk=dpf.id_departemen_fk
left join status_pengguna sts on sts.id_status_pengguna=dsn.id_status_pengguna
left join program_studi pst on dsn.id_program_studi_sd=pst.id_program_studi
left join jenjang jjg on pst.id_jenjang=jjg.id_jenjang
where pst.id_fakultas=$id_fak and dsn.status_dosen='PNS'
and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
";
$result = $db->Query($dosen)or die("salah kueri dosen ");
$i=0;$nomor=1;
while($r = $db->FetchRow()) {
$i++;
	if($i%2==0){
	$row="#cccccc";
	}
	else{
	$row="#ffffff";
	}
echo '
   <tr>
	<td bgcolor='.$row.'>&nbsp;'.$nomor.'&nbsp;</td>
	<td bgcolor='.$row.'>&nbsp;'.$r[2].'&nbsp;</td>
	<td bgcolor='.$row.'>&nbsp;'.$r[3].'&nbsp;</td>
	<td bgcolor='.$row.'>&nbsp;'.$r[1].'&nbsp;</td>
	<td bgcolor='.$row.'>&nbsp;'.$r[4].'&nbsp;</td>
	<td bgcolor='.$row.'>&nbsp;'.$r[6].'&nbsp;</td>
	<td bgcolor='.$row.'>&nbsp;'.$r[5].'&nbsp;</td>
	<td bgcolor='.$row.'>&nbsp;'.$r[6].'&nbsp;</td>
   </tr>
';
$nomor++;
}

echo '
</table>
';
}
?>