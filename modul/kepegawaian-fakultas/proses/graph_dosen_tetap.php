<?php
//Yudi Sulistya, 23/08/2012

error_reporting (E_ALL & ~E_NOTICE);

require ("../../../config.php");

$id_pengguna=$user->ID_PENGGUNA;
$id_fak=$user->ID_FAKULTAS;

if ($user->Role() == AUCC_ROLE_KEPEGAWAIAN_FAKULTAS) {
include ("../includes/jpgraph/jpgraph.php");
include ("../includes/jpgraph/jpgraph_bar.php");

$tot = array();
$fak = array();

$nm_fakultas="select upper(nm_fakultas) as fakultas from fakultas where id_fakultas=$id_fak";
$result = $db->Query($nm_fakultas) or die ("salah kueri : graph nm fakultas");
while($r = $db->FetchRow()) {
	$fakultas=$r[0];
}

$data="
select b.id_program_studi, upper(substr(c.nm_jenjang,1,2))||' - '||upper(b.nm_singkat_prodi) as nm_program_studi,
case when a.total is null then 0 else a.total end as total from 
program_studi b
left join jenjang c on c.id_jenjang=b.id_jenjang
left join fakultas d on d.id_fakultas=b.id_fakultas
left join
(
select nm_program_studi, id_program_studi, count(id_program_studi) as total from 
(
select upper(substr(jjg.nm_jenjang,1,2))||' - '||upper(pst.nm_singkat_prodi) as nm_program_studi, dsn.id_program_studi_sd as id_program_studi
from dosen dsn
left join program_studi pst on dsn.id_program_studi_sd=pst.id_program_studi
left join jenjang jjg on jjg.id_jenjang=pst.id_jenjang
left join fakultas fak on fak.id_fakultas=pst.id_fakultas
where dsn.status_dosen='PNS' and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and pst.id_fakultas=$id_fak
)
group by nm_program_studi, id_program_studi
) a
on b.id_program_studi=a.id_program_studi
where d.id_fakultas=$id_fak
order by c.nm_jenjang desc, b.nm_singkat_prodi";
$result = $db->Query($data) or die ("salah kueri : graph data");
while($r = $db->FetchRow()) {
	array_unshift($tot, $r[2]);
    array_unshift($fak, $r[1]);
}

$total="
select count(dsn.id_dosen) as total
from dosen dsn
where dsn.status_dosen='PNS' and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.id_program_studi_sd in (select id_program_studi from program_studi where id_fakultas=$id_fak)";
$result = $db->Query($total) or die ("salah kueri : graph total");
while($r = $db->FetchRow()) {
	$total=$r[0];
}

$graph = new Graph(830,400,"auto");
$graph->SetScale("textlin");
$graph->SetShadow();
$graph->SetMargin(30,20,50,120);
$graph->SetBox();
$graph->ygrid->SetFill(true,'#f5f5f5','#f0f0f0');
$graph->xaxis->SetTickLabels($fak);
$graph->xaxis->SetLabelAngle(90);
$graph->yaxis->scale->SetGrace(25);
$graph->yaxis->SetLabelFormat('%0.0f');
$graph->SetMarginColor('#c6b3cf');
$graph->title->Set("GRAFIK DOSEN TETAP AKTIF FAKULTAS ".$fakultas."");
$graph->footer->center->Set("TOTAL KESELURUHAN: ".$total."");
$graph->title->SetFont(FF_FONT1,FS_BOLD);
$graph->footer->center->SetFont(FF_FONT1,FS_BOLD);
$barplot=new BarPlot($tot);
$barplot->SetFillGradient('green','red',GRAD_HOR);
$barplot->value->show();
$barplot->value->SetFormat("%0.0f");
$barplot->value->SetFont(FF_FONT1,FS_BOLD);
$barplot->value->SetColor('darkgreen','red');
$graph->Add($barplot);
$graph->Stroke();

} else {
    header("location: /logout.php");
    exit();
}
?>