<?php
//Yudi Sulistya, 23/08/2012

error_reporting (E_ALL & ~E_NOTICE);

require ("../../../config.php");

$id_pengguna=$user->ID_PENGGUNA;
$id_fak=$user->ID_FAKULTAS;

if ($user->Role() == AUCC_ROLE_KEPEGAWAIAN_FAKULTAS) {
include ("../includes/jpgraph/jpgraph.php");
include ("../includes/jpgraph/jpgraph_bar.php");

$tot = array();
$fak = array();

$nm_fakultas="select upper(nm_fakultas) as fakultas from fakultas where id_fakultas=$id_fak";
$result = $db->Query($nm_fakultas) or die ("salah kueri : graph nm fakultas");
while($r = $db->FetchRow()) {
	$fakultas=$r[0];
}

$data="
select unitkerja, count(nip_pegawai) as total
from
(
select case when uk.nm_unit_kerja is null then 'NO UNIT KERJA' 
when upper(uk.nm_unit_kerja) like 'FAKULTAS%' then trim('FAK. '||substr(upper(uk.nm_unit_kerja),10,10))
when upper(uk.nm_unit_kerja) like '%MAGISTER%' then trim(substr(upper(uk.nm_unit_kerja),1,2)||' MAG. '||substr(upper(uk.nm_unit_kerja),13,8))
else trim(substr(upper(uk.nm_unit_kerja),1,15)) end as unitkerja, peg.nip_pegawai
from pegawai peg
left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
where (peg.status_pegawai='' or peg.status_pegawai is null) and peg.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=22 and status_aktif=1)
and uk.id_fakultas=$id_fak
)
group by unitkerja
order by unitkerja desc";
$result = $db->Query($data) or die ("salah kueri : graph data");
while($r = $db->FetchRow()) {
	array_unshift($tot, $r[1]);
    array_unshift($fak, $r[0]);
}

$total="
select count(peg.id_pegawai) as total
from pegawai peg
where (peg.status_pegawai='' or peg.status_pegawai is null) and peg.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=22 and status_aktif=1)
and peg.id_unit_kerja_sd in (select id_unit_kerja from unit_kerja where id_fakultas=$id_fak)";
$result = $db->Query($total) or die ("salah kueri : graph total");
while($r = $db->FetchRow()) {
	$total=$r[0];
}

try {
$graph = new Graph(830,400,"auto");
$graph->SetScale("textlin");
$graph->SetShadow();
$graph->SetMargin(30,20,50,120);
$graph->SetBox();
$graph->ygrid->SetFill(true,'#f5f5f5','#f0f0f0');
$graph->xaxis->SetTickLabels($fak);
$graph->xaxis->SetLabelAngle(90);
$graph->yaxis->scale->SetGrace(25);
$graph->yaxis->SetLabelFormat('%0.0f');
$graph->SetMarginColor('#c6b3cf');
$graph->title->Set("GRAFIK KARYAWAN AKTIF TANPA STATUS KEPEGAWAIAN FAKULTAS ".$fakultas."");
$graph->footer->center->Set("TOTAL KESELURUHAN: ".$total."");
$graph->title->SetFont(FF_FONT1,FS_BOLD);
$graph->footer->center->Set("TOTAL KESELURUHAN: ".$total."");
$barplot=new BarPlot($tot);
$barplot->SetFillGradient('red','green',GRAD_HOR);
$barplot->value->show();
$barplot->value->SetFormat("%0.0f");
$barplot->value->SetFont(FF_FONT1,FS_BOLD);
$barplot->value->SetColor('red','darkgreen');
$graph->Add($barplot);
$graph->Stroke();
} catch(JpGraphException $e) {
echo '
<link rel="stylesheet" type="text/css" href="../includes/iframe.css" />
<table align="center">
	<tr>
		<td>SEMUA KARYAWAN <b><u>AKTIF</u></b> SUDAH DI SET STATUS KEPEGAWAIANNYA</td>
	</tr>
</table>';
}

} else {
    header("location: /logout.php");
    exit();
}
?>