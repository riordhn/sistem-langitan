<?php
//Yudi Sulistya, 23/08/2012

error_reporting (E_ALL & ~E_NOTICE);

require ("../../../config.php");

$id_pengguna=$user->ID_PENGGUNA;
$id_fak=$user->ID_FAKULTAS;

if ($user->Role() == AUCC_ROLE_KEPEGAWAIAN_FAKULTAS) {
include ("../includes/jpgraph/jpgraph.php");
include ("../includes/jpgraph/jpgraph_bar.php");

$tot = array();
$fak = array();

$nm_fakultas="select upper(nm_fakultas) as fakultas from fakultas where id_fakultas=$id_fak";
$result = $db->Query($nm_fakultas) or die ("salah kueri : graph nm fakultas");
while($r = $db->FetchRow()) {
	$fakultas=$r[0];
}

$data="
select id_status_pengguna, nm_status_pengguna, count(id_dosen) as jml
from 
(
select a.id_status_pengguna, a.nm_status_pengguna, b.id_dosen
from
(
(select id_status_pengguna, nm_status_pengguna from status_pengguna where id_role=4) union (select 9999 as id_status_pengguna, 'Tanpa Status' as nm_status_pengguna from dual)
) a left join
(
select id_status_pengguna, id_dosen from dosen
where dosen.id_program_studi in (select id_program_studi from program_studi where id_fakultas=$id_fak)
) b on a.id_status_pengguna=b.id_status_pengguna
)
group by id_status_pengguna, nm_status_pengguna
order by id_status_pengguna desc
";
$result = $db->Query($data) or die ("salah kueri : graph data");
while($r = $db->FetchRow()) {
	array_unshift($tot, $r[2]);
    array_unshift($fak, $r[1]);
}

$graph = new Graph(830,400,"auto");
$graph->SetScale("textlin");
$graph->SetShadow();
$graph->SetMargin(30,20,50,120);
$graph->SetBox();
$graph->ygrid->SetFill(true,'#f5f5f5','#f0f0f0');
$graph->xaxis->SetTickLabels($fak);
$graph->xaxis->SetLabelAngle(90);
$graph->yaxis->scale->SetGrace(25);
$graph->yaxis->SetLabelFormat('%0.0f');
$graph->SetMarginColor('#c6b3cf');
$graph->title->Set("GRAFIK STATUS DOSEN FAKULTAS ".$fakultas."");
$graph->title->SetFont(FF_FONT1,FS_BOLD);
$barplot=new BarPlot($tot);
$barplot->SetFillGradient('green','red',GRAD_HOR);
$barplot->value->show();
$barplot->value->SetFormat("%0.0f");
$barplot->value->SetFont(FF_FONT1,FS_BOLD);
$barplot->value->SetColor('darkgreen','red');
$graph->Add($barplot);
$graph->Stroke();

} else {
    header("location: /logout.php");
    exit();
}
?>