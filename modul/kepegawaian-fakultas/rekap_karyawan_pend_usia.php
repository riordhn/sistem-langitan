<?php
//Yudi Sulistya, 23/04/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

$id_pengguna= $user->ID_PENGGUNA; 
$id_fak=$user->ID_FAKULTAS;
$smarty->assign('KDFAK', $id_fak);

$opsi = getData("select id_unit_kerja, upper(nm_unit_kerja) as nm_program_studi from unit_kerja where id_fakultas=$id_fak order by nm_unit_kerja");
$smarty->assign('KD_PRODI', $opsi);

// default view	
		$fak=getData("select 'SEMUA KARYAWAN DI FAKULTAS '||upper(fak.nm_fakultas) as fakultas
		from fakultas fak
		where fak.id_fakultas=$id_fak");
		$smarty->assign('FAK', $fak);
		
		$ttl=getvar("select sum(A+B+C+D+E) as total
						from
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 50 then 1 else 0 end as E
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
						left join sejarah_pendidikan pdd on peg.id_pengguna=pdd.id_pengguna
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from pegawai where
						uk.id_fakultas=$id_fak
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('TTL', $ttl['TOTAL']);

		$gol=getData("select urut, id_pendidikan_akhir, nama_pendidikan_akhir, sum(A) as I, sum(B) as II, sum(C) as III, sum(D) as IV, sum(E) as V, sum(A+B+C+D+E) as total
						from
						(
						select pda.id_pendidikan_akhir,
						case when pda.id_pendidikan_akhir=2 then 1 
						when pda.id_pendidikan_akhir=1 then 2
						when pda.id_pendidikan_akhir=10 then 9  
						when pda.id_pendidikan_akhir=9 then 3 
						when pda.id_pendidikan_akhir=8 then 4
						when pda.id_pendidikan_akhir=7 then 5   
						when pda.id_pendidikan_akhir=3 then 0  
						when pda.id_pendidikan_akhir=4 then -1 
						when pda.id_pendidikan_akhir=5 then -2 
						when pda.id_pendidikan_akhir=6 then -3 
						when pda.id_pendidikan_akhir=13 then -4 
						when pda.id_pendidikan_akhir=12 then -5 
						when pda.id_pendidikan_akhir=11 then -6 
						else 6 end as urut, pda.nama_pendidikan_akhir,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 50 then 1 else 0 end as E
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
						left join sejarah_pendidikan pdd on peg.id_pengguna=pdd.id_pengguna
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from pegawai where
						uk.id_fakultas=$id_fak
						)
						group by id_pengguna
						)
						)
						group by urut, id_pendidikan_akhir, nama_pendidikan_akhir
						order by urut");
		$smarty->assign('GOL', $gol);

		$jml=getData("select sum(A) as I, sum(B) as II, sum(C) as III, sum(D) as IV, sum(E) as V, sum(A+B+C+D+E) as total
						from
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 50 then 1 else 0 end as E
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
						left join sejarah_pendidikan pdd on peg.id_pengguna=pdd.id_pengguna
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from pegawai where
						uk.id_fakultas=$id_fak
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('JML', $jml);

		$psn=getData("select case when sum(A)=0 then 0 else round((sum(A)/sum(A+B+C+D+E)*100),2) end as I,
						case when sum(B)=0 then 0 else round((sum(B)/sum(A+B+C+D+E)*100),2) end as II,
						case when sum(C)=0 then 0 else round((sum(C)/sum(A+B+C+D+E)*100),2) end as III,
						case when sum(D)=0 then 0 else round((sum(D)/sum(A+B+C+D+E)*100),2) end as IV,
						case when sum(E)=0 then 0 else round((sum(E)/sum(A+B+C+D+E)*100),2) end as V,
						round((sum(A+B+C+D+E)/sum(A+B+C+D+E)*100),2) as persen
						from
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 60 then 1 else 0 end as E
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
						left join sejarah_pendidikan pdd on peg.id_pengguna=pdd.id_pengguna
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from pegawai where
						uk.id_fakultas=$id_fak
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('PSN', $psn);

if (get('action') == 'view')
{
		$id = get('id');

	// data fakultas
	if ($id == 0) {
		$fak = getData("select 'UNIT KERJA : '||upper(fak.nm_fakultas) as fakultas
		from fakultas fak
		where fak.id_fakultas = $id_fak");
		$smarty->assign('FAK', $fak);
		
		$ttl=getvar("select sum(A+B+C+D+E) as total
						from
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 50 then 1 else 0 end as E
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
						left join sejarah_pendidikan pdd on peg.id_pengguna=pdd.id_pengguna
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from pegawai where
						uk.id_fakultas=$id_fak
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('TTL', $ttl['TOTAL']);

		$gol=getData("select urut, id_pendidikan_akhir, nama_pendidikan_akhir, sum(A) as I, sum(B) as II, sum(C) as III, sum(D) as IV, sum(E) as V, sum(A+B+C+D+E) as total
						from
						(
						select pda.id_pendidikan_akhir,
						case when pda.id_pendidikan_akhir=2 then 1 
						when pda.id_pendidikan_akhir=1 then 2
						when pda.id_pendidikan_akhir=10 then 9  
						when pda.id_pendidikan_akhir=9 then 3 
						when pda.id_pendidikan_akhir=8 then 4
						when pda.id_pendidikan_akhir=7 then 5   
						when pda.id_pendidikan_akhir=3 then 0  
						when pda.id_pendidikan_akhir=4 then -1 
						when pda.id_pendidikan_akhir=5 then -2 
						when pda.id_pendidikan_akhir=6 then -3 
						when pda.id_pendidikan_akhir=13 then -4 
						when pda.id_pendidikan_akhir=12 then -5 
						when pda.id_pendidikan_akhir=11 then -6 
						else 6 end as urut, pda.nama_pendidikan_akhir,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 50 then 1 else 0 end as E
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
						left join sejarah_pendidikan pdd on peg.id_pengguna=pdd.id_pengguna
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from pegawai where
						uk.id_fakultas=$id_fak
						)
						group by id_pengguna
						)
						)
						group by urut, id_pendidikan_akhir, nama_pendidikan_akhir
						order by urut");
		$smarty->assign('GOL', $gol);

		$jml=getData("select sum(A) as I, sum(B) as II, sum(C) as III, sum(D) as IV, sum(E) as V, sum(A+B+C+D+E) as total
						from
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 50 then 1 else 0 end as E
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
						left join sejarah_pendidikan pdd on peg.id_pengguna=pdd.id_pengguna
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from pegawai where
						uk.id_fakultas=$id_fak
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('JML', $jml);

		$psn=getData("select case when sum(A)=0 then 0 else round((sum(A)/sum(A+B+C+D+E)*100),2) end as I,
						case when sum(B)=0 then 0 else round((sum(B)/sum(A+B+C+D+E)*100),2) end as II,
						case when sum(C)=0 then 0 else round((sum(C)/sum(A+B+C+D+E)*100),2) end as III,
						case when sum(D)=0 then 0 else round((sum(D)/sum(A+B+C+D+E)*100),2) end as IV,
						case when sum(E)=0 then 0 else round((sum(E)/sum(A+B+C+D+E)*100),2) end as V,
						round((sum(A+B+C+D+E)/sum(A+B+C+D+E)*100),2) as persen
						from
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 60 then 1 else 0 end as E
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
						left join sejarah_pendidikan pdd on peg.id_pengguna=pdd.id_pengguna
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from pegawai where
						uk.id_fakultas=$id_fak
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('PSN', $psn);
		
	// data prodi
	} else {
		$fak = getData("select 'UNIT KERJA : '||upper(nm_unit_kerja) as fakultas from unit_kerja where id_unit_kerja=$id");
		$smarty->assign('FAK', $fak);
	
		$ttl=getvar("select sum(A+B+C+D+E) as total
						from
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 50 then 1 else 0 end as E
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
						left join sejarah_pendidikan pdd on peg.id_pengguna=pdd.id_pengguna
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from pegawai where
						uk.id_unit_kerja=$id
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('TTL', $ttl['TOTAL']);

		$gol=getData("select urut, id_pendidikan_akhir, nama_pendidikan_akhir, sum(A) as I, sum(B) as II, sum(C) as III, sum(D) as IV, sum(E) as V, sum(A+B+C+D+E) as total
						from
						(
						select pda.id_pendidikan_akhir,
						case when pda.id_pendidikan_akhir=2 then 1 
						when pda.id_pendidikan_akhir=1 then 2
						when pda.id_pendidikan_akhir=10 then 9  
						when pda.id_pendidikan_akhir=9 then 3 
						when pda.id_pendidikan_akhir=8 then 4
						when pda.id_pendidikan_akhir=7 then 5   
						when pda.id_pendidikan_akhir=3 then 0  
						when pda.id_pendidikan_akhir=4 then -1 
						when pda.id_pendidikan_akhir=5 then -2 
						when pda.id_pendidikan_akhir=6 then -3 
						when pda.id_pendidikan_akhir=13 then -4 
						when pda.id_pendidikan_akhir=12 then -5 
						when pda.id_pendidikan_akhir=11 then -6 
						else 6 end as urut, pda.nama_pendidikan_akhir,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 50 then 1 else 0 end as E
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
						left join sejarah_pendidikan pdd on peg.id_pengguna=pdd.id_pengguna
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from pegawai where
						uk.id_unit_kerja=$id
						)
						group by id_pengguna
						)
						)
						group by urut, id_pendidikan_akhir, nama_pendidikan_akhir
						order by urut");
		$smarty->assign('GOL', $gol);

		$jml=getData("select sum(A) as I, sum(B) as II, sum(C) as III, sum(D) as IV, sum(E) as V, sum(A+B+C+D+E) as total
						from
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 50 then 1 else 0 end as E
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
						left join sejarah_pendidikan pdd on peg.id_pengguna=pdd.id_pengguna
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from pegawai where
						uk.id_unit_kerja=$id
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('JML', $jml);

		$psn=getData("select case when sum(A)=0 then 0 else round((sum(A)/sum(A+B+C+D+E)*100),2) end as I,
						case when sum(B)=0 then 0 else round((sum(B)/sum(A+B+C+D+E)*100),2) end as II,
						case when sum(C)=0 then 0 else round((sum(C)/sum(A+B+C+D+E)*100),2) end as III,
						case when sum(D)=0 then 0 else round((sum(D)/sum(A+B+C+D+E)*100),2) end as IV,
						case when sum(E)=0 then 0 else round((sum(E)/sum(A+B+C+D+E)*100),2) end as V,
						round((sum(A+B+C+D+E)/sum(A+B+C+D+E)*100),2) as persen
						from
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 60 then 1 else 0 end as E
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
						left join sejarah_pendidikan pdd on peg.id_pengguna=pdd.id_pengguna
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from pegawai where
						uk.id_unit_kerja=$id
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('PSN', $psn);
	}
}

$smarty->display('rekap_karyawan_pend_usia.tpl');

?>