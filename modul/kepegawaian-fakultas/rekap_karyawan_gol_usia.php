<?php
//Yudi Sulistya, 23/04/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

$id_pengguna= $user->ID_PENGGUNA; 
$id_fak=$user->ID_FAKULTAS;
$smarty->assign('KDFAK', $id_fak);

$opsi = getData("select id_unit_kerja, upper(nm_unit_kerja) as nm_program_studi from unit_kerja where id_fakultas=$id_fak order by nm_unit_kerja");
$smarty->assign('KD_PRODI', $opsi);

// default view	
		$fak=getData("select 'SEMUA KARYAWAN DI FAKULTAS '||upper(fak.nm_fakultas) as fakultas
		from fakultas fak
		where fak.id_fakultas=$id_fak");
		$smarty->assign('FAK', $fak);
		
		$ttl=getvar("select sum(A+B+C+D+E) as total
						from
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 50 then 1 else 0 end as E
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where uk.id_fakultas=$id_fak
						)");
		$smarty->assign('TTL', $ttl['TOTAL']);

		$gol=getData("select id_golongan, upper(golongan) as golongan, sum(A) as I, sum(B) as II, sum(C) as III, sum(D) as IV, sum(E) as V, sum(A+B+C+D+E) as total
						from
						(
						select peg.id_golongan, case when peg.id_golongan is not null then gol.nm_golongan else '-' end as golongan,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 50 then 1 else 0 end as E
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where uk.id_fakultas=$id_fak
						)
						group by id_golongan, golongan
						order by golongan");
		$smarty->assign('GOL', $gol);

		$jml=getData("select sum(A) as I, sum(B) as II, sum(C) as III, sum(D) as IV, sum(E) as V, sum(A+B+C+D+E) as total
						from
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 50 then 1 else 0 end as E
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where uk.id_fakultas=$id_fak
						)");
		$smarty->assign('JML', $jml);
		
		$psn=getData("select case when sum(A)=0 then 0 else round((sum(A)/sum(A+B+C+D+E)*100),2) end as I,
						case when sum(B)=0 then 0 else round((sum(B)/sum(A+B+C+D+E)*100),2) end as II,
						case when sum(C)=0 then 0 else round((sum(C)/sum(A+B+C+D+E)*100),2) end as III,
						case when sum(D)=0 then 0 else round((sum(D)/sum(A+B+C+D+E)*100),2) end as IV,
						case when sum(E)=0 then 0 else round((sum(E)/sum(A+B+C+D+E)*100),2) end as V,
						round((sum(A+B+C+D+E)/sum(A+B+C+D+E)*100),2) as persen
						from 
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 50 then 1 else 0 end as E
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where uk.id_fakultas=$id_fak
						)");
		$smarty->assign('PSN', $psn);

if (get('action') == 'view')
{
		$id = get('id');

	// data fakultas
	if ($id == 0) {
		$fak = getData("select 'UNIT KERJA : '||upper(fak.nm_fakultas) as fakultas
		from fakultas fak
		where fak.id_fakultas = $id_fak");
		$smarty->assign('FAK', $fak);
		
		$ttl=getvar("select sum(A+B+C+D+E) as total
						from
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 50 then 1 else 0 end as E
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where uk.id_fakultas=$id_fak
						)");
		$smarty->assign('TTL', $ttl['TOTAL']);

		$gol=getData("select id_golongan, upper(golongan) as golongan, sum(A) as I, sum(B) as II, sum(C) as III, sum(D) as IV, sum(E) as V, sum(A+B+C+D+E) as total
						from
						(
						select peg.id_golongan, case when peg.id_golongan is not null then gol.nm_golongan else '-' end as golongan,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 50 then 1 else 0 end as E
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where uk.id_fakultas=$id_fak
						)
						group by id_golongan, golongan
						order by golongan");
		$smarty->assign('GOL', $gol);

		$jml=getData("select sum(A) as I, sum(B) as II, sum(C) as III, sum(D) as IV, sum(E) as V, sum(A+B+C+D+E) as total
						from
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 50 then 1 else 0 end as E
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where uk.id_fakultas=$id_fak
						)");
		$smarty->assign('JML', $jml);
		
		$psn=getData("select case when sum(A)=0 then 0 else round((sum(A)/sum(A+B+C+D+E)*100),2) end as I,
						case when sum(B)=0 then 0 else round((sum(B)/sum(A+B+C+D+E)*100),2) end as II,
						case when sum(C)=0 then 0 else round((sum(C)/sum(A+B+C+D+E)*100),2) end as III,
						case when sum(D)=0 then 0 else round((sum(D)/sum(A+B+C+D+E)*100),2) end as IV,
						case when sum(E)=0 then 0 else round((sum(E)/sum(A+B+C+D+E)*100),2) end as V,
						round((sum(A+B+C+D+E)/sum(A+B+C+D+E)*100),2) as persen
						from 
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 50 then 1 else 0 end as E
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where uk.id_fakultas=$id_fak
						)");
		$smarty->assign('PSN', $psn);
	
	// data prodi
	} else {
		$fak = getData("select 'UNIT KERJA : '||upper(nm_unit_kerja) as fakultas from unit_kerja where id_unit_kerja=$id");
		$smarty->assign('FAK', $fak);
	
		$ttl=getvar("select sum(A+B+C+D+E) as total
					from
					(
					select 
					case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
					case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
					case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
					case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
					case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 50 then 1 else 0 end as E
					from pegawai peg
					left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
					left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
					left join golongan gol on gol.id_golongan=peg.id_golongan
					where uk.id_unit_kerja=$id
					)");
		$smarty->assign('TTL', $ttl['TOTAL']);

		$gol=getData("select id_golongan, upper(golongan) as golongan, sum(A) as I, sum(B) as II, sum(C) as III, sum(D) as IV, sum(E) as V, sum(A+B+C+D+E) as total
						from
						(
						select peg.id_golongan, case when peg.id_golongan is not null then gol.nm_golongan else '-' end as golongan,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 50 then 1 else 0 end as E
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where uk.id_unit_kerja=$id
						)
						group by id_golongan, golongan
						order by golongan");
		$smarty->assign('GOL', $gol);

		$jml=getData("select sum(A) as I, sum(B) as II, sum(C) as III, sum(D) as IV, sum(E) as V, sum(A+B+C+D+E) as total
						from
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 50 then 1 else 0 end as E
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where uk.id_unit_kerja=$id
						)");
		$smarty->assign('JML', $jml);
		
		$psn=getData("select case when sum(A)=0 then 0 else round((sum(A)/sum(A+B+C+D+E)*100),2) end as I,
						case when sum(B)=0 then 0 else round((sum(B)/sum(A+B+C+D+E)*100),2) end as II,
						case when sum(C)=0 then 0 else round((sum(C)/sum(A+B+C+D+E)*100),2) end as III,
						case when sum(D)=0 then 0 else round((sum(D)/sum(A+B+C+D+E)*100),2) end as IV,
						case when sum(E)=0 then 0 else round((sum(E)/sum(A+B+C+D+E)*100),2) end as V,
						round((sum(A+B+C+D+E)/sum(A+B+C+D+E)*100),2) as persen
						from 
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 50 then 1 else 0 end as E
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where uk.id_unit_kerja=$id
						)");
		$smarty->assign('PSN', $psn);
	}
}

$smarty->display('rekap_karyawan_gol_usia.tpl');

?>