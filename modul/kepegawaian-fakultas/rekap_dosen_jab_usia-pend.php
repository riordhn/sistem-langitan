<?php
//Yudi Sulistya, 25/09/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

$id_pengguna= $user->ID_PENGGUNA; 
$id_fak=$user->ID_FAKULTAS;

$smarty->assign('KDFAK', $id_fak);

$opsi = getData("select id_program_studi as opsi, upper(substr(nm_jenjang,1,2))||' - '||upper(nm_program_studi) as nama
				from program_studi
				left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
				where id_fakultas = $id_fak
				order by nm_jenjang, nm_program_studi");
$smarty->assign('KDPRODI', $opsi);

// default view
$fak=getData("select 'FAKULTAS '||upper(fak.nm_fakultas) as fakultas
from fakultas fak
where fak.id_fakultas=$id_fak");
$smarty->assign('FAK', $fak);

$data = getData("
select a.urut, a.jabatan, a.a1, a.a2, a.a3, b.b1, b.b2, b.b3, c.c1, c.c2, c.c3, d.d1, d.d2, d.d3, e.e1, e.e2, e.e3 from 
(
select fsg.urut, fsg.jabatan, a1, a2, a3 from
(
select distinct
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 0 else b.id_jabatan_fungsional end as urut,
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a left join dosen b on a.id_jabatan_fungsional=b.id_jabatan_fungsional
) fsg
left join
(
select jabatan, sum(S3) as a3, sum(S2) as a2, sum(S1) as a1
from
(
select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else jab.nm_jabatan_fungsional end as jabatan,
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) <= 30) and
pdd.id_pengguna in
(
select distinct id_pengguna from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas=$id_fak)
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
) and pdd.status_akhir=1
group by id_pengguna
)
)
group by jabatan
) mat on fsg.jabatan=mat.jabatan
) a
left join 
(
select fsg.urut, fsg.jabatan, b1, b2, b3 from
(
select distinct
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 0 else b.id_jabatan_fungsional end as urut,
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a left join dosen b on a.id_jabatan_fungsional=b.id_jabatan_fungsional
) fsg
left join
(
select jabatan, sum(S3) as b3, sum(S2) as b2, sum(S1) as b1
from
(
select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else jab.nm_jabatan_fungsional end as jabatan,
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40) and
pdd.id_pengguna in
(
select distinct id_pengguna from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas=$id_fak)
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
) and pdd.status_akhir=1
group by id_pengguna
)
)
group by jabatan
) mat on fsg.jabatan=mat.jabatan
) b on a.urut=b.urut
left join 
(
select fsg.urut, fsg.jabatan, c1, c2, c3 from
(
select distinct
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 0 else b.id_jabatan_fungsional end as urut,
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a left join dosen b on a.id_jabatan_fungsional=b.id_jabatan_fungsional
) fsg
left join
(
select jabatan, sum(S3) as c3, sum(S2) as c2, sum(S1) as c1
from
(
select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else jab.nm_jabatan_fungsional end as jabatan,
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50) and
pdd.id_pengguna in
(
select distinct id_pengguna from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas=$id_fak)
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
) and pdd.status_akhir=1
group by id_pengguna
)
)
group by jabatan
) mat on fsg.jabatan=mat.jabatan
) c on a.urut=c.urut
left join 
(
select fsg.urut, fsg.jabatan, d1, d2, d3 from
(
select distinct
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 0 else b.id_jabatan_fungsional end as urut,
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a left join dosen b on a.id_jabatan_fungsional=b.id_jabatan_fungsional
) fsg
left join
(
select jabatan, sum(S1) as d1, sum(S2) as d2, sum(S3) as d3
from
(
select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else jab.nm_jabatan_fungsional end as jabatan,
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 51 and 60) and
pdd.id_pengguna in
(
select distinct id_pengguna from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas=$id_fak)
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
) and pdd.status_akhir=1
group by id_pengguna
)
)
group by jabatan
) mat on fsg.jabatan=mat.jabatan
) d on a.urut=d.urut
left join 
(
select fsg.urut, fsg.jabatan, e1, e2, e3 from
(
select distinct
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 0 else b.id_jabatan_fungsional end as urut,
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a left join dosen b on a.id_jabatan_fungsional=b.id_jabatan_fungsional
) fsg
left join
(
select jabatan, sum(S3) as e3, sum(S2) as e2, sum(S1) as e1
from
(
select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else jab.nm_jabatan_fungsional end as jabatan,
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 60) and
pdd.id_pengguna in
(
select distinct id_pengguna from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas=$id_fak)
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
) and pdd.status_akhir=1
group by id_pengguna
)
)
group by jabatan
) mat on fsg.jabatan=mat.jabatan
) e on a.urut=e.urut
order by a.urut
");

$smarty->assign('DATA', $data);

if (get('action') == 'view')
{
		$id = get('id');

	// data fakultas
	if ($id == 0) {
$fak=getData("select 'FAKULTAS '||upper(fak.nm_fakultas) as fakultas
from fakultas fak
where fak.id_fakultas=$id_fak");
$smarty->assign('FAK', $fak);

$data = getData("
select a.urut, a.jabatan, a.a1, a.a2, a.a3, b.b1, b.b2, b.b3, c.c1, c.c2, c.c3, d.d1, d.d2, d.d3, e.e1, e.e2, e.e3 from 
(
select fsg.urut, fsg.jabatan, a1, a2, a3 from
(
select distinct
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 0 else b.id_jabatan_fungsional end as urut,
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a left join dosen b on a.id_jabatan_fungsional=b.id_jabatan_fungsional
) fsg
left join
(
select jabatan, sum(S3) as a3, sum(S2) as a2, sum(S1) as a1
from
(
select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else jab.nm_jabatan_fungsional end as jabatan,
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) <= 30) and
pdd.id_pengguna in
(
select distinct id_pengguna from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas=$id_fak)
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
) and pdd.status_akhir=1
group by id_pengguna
)
)
group by jabatan
) mat on fsg.jabatan=mat.jabatan
) a
left join 
(
select fsg.urut, fsg.jabatan, b1, b2, b3 from
(
select distinct
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 0 else b.id_jabatan_fungsional end as urut,
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a left join dosen b on a.id_jabatan_fungsional=b.id_jabatan_fungsional
) fsg
left join
(
select jabatan, sum(S3) as b3, sum(S2) as b2, sum(S1) as b1
from
(
select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else jab.nm_jabatan_fungsional end as jabatan,
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40) and
pdd.id_pengguna in
(
select distinct id_pengguna from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas=$id_fak)
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
) and pdd.status_akhir=1
group by id_pengguna
)
)
group by jabatan
) mat on fsg.jabatan=mat.jabatan
) b on a.urut=b.urut
left join 
(
select fsg.urut, fsg.jabatan, c1, c2, c3 from
(
select distinct
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 0 else b.id_jabatan_fungsional end as urut,
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a left join dosen b on a.id_jabatan_fungsional=b.id_jabatan_fungsional
) fsg
left join
(
select jabatan, sum(S3) as c3, sum(S2) as c2, sum(S1) as c1
from
(
select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else jab.nm_jabatan_fungsional end as jabatan,
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50) and
pdd.id_pengguna in
(
select distinct id_pengguna from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas=$id_fak)
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
) and pdd.status_akhir=1
group by id_pengguna
)
)
group by jabatan
) mat on fsg.jabatan=mat.jabatan
) c on a.urut=c.urut
left join 
(
select fsg.urut, fsg.jabatan, d1, d2, d3 from
(
select distinct
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 0 else b.id_jabatan_fungsional end as urut,
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a left join dosen b on a.id_jabatan_fungsional=b.id_jabatan_fungsional
) fsg
left join
(
select jabatan, sum(S1) as d1, sum(S2) as d2, sum(S3) as d3
from
(
select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else jab.nm_jabatan_fungsional end as jabatan,
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 51 and 60) and
pdd.id_pengguna in
(
select distinct id_pengguna from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas=$id_fak)
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
) and pdd.status_akhir=1
group by id_pengguna
)
)
group by jabatan
) mat on fsg.jabatan=mat.jabatan
) d on a.urut=d.urut
left join 
(
select fsg.urut, fsg.jabatan, e1, e2, e3 from
(
select distinct
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 0 else b.id_jabatan_fungsional end as urut,
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a left join dosen b on a.id_jabatan_fungsional=b.id_jabatan_fungsional
) fsg
left join
(
select jabatan, sum(S3) as e3, sum(S2) as e2, sum(S1) as e1
from
(
select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else jab.nm_jabatan_fungsional end as jabatan,
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 60) and
pdd.id_pengguna in
(
select distinct id_pengguna from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas=$id_fak)
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
) and pdd.status_akhir=1
group by id_pengguna
)
)
group by jabatan
) mat on fsg.jabatan=mat.jabatan
) e on a.urut=e.urut
order by a.urut
");

$smarty->assign('DATA', $data);
// data prodi
} else {
$fak = getData("
select upper(substr(jjg.nm_jenjang,1,2))||' - '||upper(pst.nm_program_studi) as fakultas
from program_studi pst
left join jenjang jjg on pst.id_jenjang = jjg.id_jenjang
where pst.id_program_studi = $id");
$smarty->assign('FAK', $fak);

$data = getData("
select a.urut, a.jabatan, a.a1, a.a2, a.a3, b.b1, b.b2, b.b3, c.c1, c.c2, c.c3, d.d1, d.d2, d.d3, e.e1, e.e2, e.e3 from 
(
select fsg.urut, fsg.jabatan, a1, a2, a3 from
(
select distinct
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 0 else b.id_jabatan_fungsional end as urut,
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a left join dosen b on a.id_jabatan_fungsional=b.id_jabatan_fungsional
) fsg
left join
(
select jabatan, sum(S3) as a3, sum(S2) as a2, sum(S1) as a1
from
(
select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else jab.nm_jabatan_fungsional end as jabatan,
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) <= 30) and
pdd.id_pengguna in
(
select distinct id_pengguna from sejarah_pendidikan where id_pengguna in
(select id_pengguna from dosen where id_program_studi=$id
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
) and pdd.status_akhir=1
group by id_pengguna
)
)
group by jabatan
) mat on fsg.jabatan=mat.jabatan
) a
left join 
(
select fsg.urut, fsg.jabatan, b1, b2, b3 from
(
select distinct
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 0 else b.id_jabatan_fungsional end as urut,
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a left join dosen b on a.id_jabatan_fungsional=b.id_jabatan_fungsional
) fsg
left join
(
select jabatan, sum(S3) as b3, sum(S2) as b2, sum(S1) as b1
from
(
select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else jab.nm_jabatan_fungsional end as jabatan,
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40) and
pdd.id_pengguna in
(
select distinct id_pengguna from sejarah_pendidikan where id_pengguna in
(select id_pengguna from dosen where id_program_studi=$id
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
) and pdd.status_akhir=1
group by id_pengguna
)
)
group by jabatan
) mat on fsg.jabatan=mat.jabatan
) b on a.urut=b.urut
left join 
(
select fsg.urut, fsg.jabatan, c1, c2, c3 from
(
select distinct
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 0 else b.id_jabatan_fungsional end as urut,
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a left join dosen b on a.id_jabatan_fungsional=b.id_jabatan_fungsional
) fsg
left join
(
select jabatan, sum(S3) as c3, sum(S2) as c2, sum(S1) as c1
from
(
select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else jab.nm_jabatan_fungsional end as jabatan,
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50) and
pdd.id_pengguna in
(
select distinct id_pengguna from sejarah_pendidikan where id_pengguna in
(select id_pengguna from dosen where id_program_studi=$id
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
) and pdd.status_akhir=1
group by id_pengguna
)
)
group by jabatan
) mat on fsg.jabatan=mat.jabatan
) c on a.urut=c.urut
left join 
(
select fsg.urut, fsg.jabatan, d1, d2, d3 from
(
select distinct
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 0 else b.id_jabatan_fungsional end as urut,
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a left join dosen b on a.id_jabatan_fungsional=b.id_jabatan_fungsional
) fsg
left join
(
select jabatan, sum(S1) as d1, sum(S2) as d2, sum(S3) as d3
from
(
select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else jab.nm_jabatan_fungsional end as jabatan,
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 51 and 60) and
pdd.id_pengguna in
(
select distinct id_pengguna from sejarah_pendidikan where id_pengguna in
(select id_pengguna from dosen where id_program_studi=$id
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
) and pdd.status_akhir=1
group by id_pengguna
)
)
group by jabatan
) mat on fsg.jabatan=mat.jabatan
) d on a.urut=d.urut
left join 
(
select fsg.urut, fsg.jabatan, e1, e2, e3 from
(
select distinct
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 0 else b.id_jabatan_fungsional end as urut,
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a left join dosen b on a.id_jabatan_fungsional=b.id_jabatan_fungsional
) fsg
left join
(
select jabatan, sum(S3) as e3, sum(S2) as e2, sum(S1) as e1
from
(
select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else jab.nm_jabatan_fungsional end as jabatan,
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 60) and
pdd.id_pengguna in
(
select distinct id_pengguna from sejarah_pendidikan where id_pengguna in
(select id_pengguna from dosen where id_program_studi=$id
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
) and pdd.status_akhir=1
group by id_pengguna
)
)
group by jabatan
) mat on fsg.jabatan=mat.jabatan
) e on a.urut=e.urut
order by a.urut
");

$smarty->assign('DATA', $data);
}
}

$smarty->display('rekap_dosen_jab_usia-pend.tpl');
?>