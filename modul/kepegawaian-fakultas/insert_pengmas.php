<?php
include ('common.php');
require_once ('ociFunction.php');

if ($user->Role() == 24) {
    $id = $_GET['id'];

    if (isset($_POST['submit'])) {
        $id_dosen = $_POST['id_dosen'];
        $nama = $_POST['nama'];
        $tempat = $_POST['tempat'];
        $bidang = $_POST['bidang'];
        $peran = $_POST['peran'];
        $tahun = $_POST['tahun'];
        $dana = $_POST['dana'];
        $sumber = $_POST['sumber'];
        $tingkat = $_POST['tingkat'];
        $output = $_POST['hasil'];

        InsertData("insert into dosen_pengmas (id_dosen, nm_dosen_pengmas, tempat_dosen_pengmas, bidang_dosen_pengmas, peran_dosen_pengmas, thn_dosen_pengmas,dana_dosen_pengmas,sumber_dana_dosen_pengmas,tingkat_dosen_pengmas,output_dosen_pengmas) 
                    values ('{$id_dosen}','{$nama}','{$tempat}','{$bidang}','{$peran}','{$tahun}','{$dana}','{$sumber}','{$tingkat}','{$output}')");

        echo '<script>alert("Data berhasil ditambahkan")</script>';
        echo '<script>window.parent.document.location.reload();</script>';
    }
    $smarty->display('insert/insert_pengmas.tpl');
} else {
    header("location: /logout.php");
    exit();
}
?>