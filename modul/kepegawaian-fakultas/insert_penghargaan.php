<?php
include ('common.php');
require_once ('ociFunction.php');

if ($user->Role() == 24) {
    $id = $_GET['id'];

    if (isset($_POST['submit'])) {
        $id_pengguna = $_POST['id_pengguna'];
        $nama = $_POST['nama'];
        $nomor = $_POST['nomor'];
        $bidang = $_POST['bidang'];
        $bentuk = $_POST['bentuk'];
        $tgl_perolehan = $_POST['tgl_perolehan'];
        $negara = $_POST['negara'];
        $pemberi = $_POST['pemberi'];
        $intansi = $_POST['intansi'];
        $jabatan = $_POST['jabatan'];
        $tingkat = $_POST['tingkat'];
        
        InsertData("insert into sejarah_penghargaan (id_pengguna, nama_penghargaan, nomor_penghargaan, tgl_perolehan , id_negara_pemberi, jabatan_pemberi, bidang_penghargaan ,bentuk_penghargaan,instansi_pemberi,pemberi_penghargaan,tingkat_penghargaan,status_valid) 
                    values ('{$id_pengguna}',upper('{$nama}'),'{$nomor}',to_date('{$tgl_perolehan}','DD-MM-YYYY'),'{$negara}','{$jabatan}','{$bidang}','{$bentuk}','{$intansi}','{$pemberi}','{$tingkat}',1)");

        echo '<script>alert("Data berhasil ditambahkan")</script>';
        echo '<script>window.parent.document.location.reload();</script>';
    }
    $negara = getData("SELECT * FROM NEGARA ORDER BY NM_NEGARA");
    $smarty->assign('negara',$negara);
    $smarty->display('insert/insert_penghargaan.tpl');
} else {
    header("location: /logout.php");
    exit();
}
?>