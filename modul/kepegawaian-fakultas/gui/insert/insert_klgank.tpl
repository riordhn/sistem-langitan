<html>
    <head>
        <link rel="stylesheet" type="text/css" href="includes/iframe.css" />
        <script type="text/javascript" src="js/datetimepicker.js"></script>
        <script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
    </head>
    <body>
        <form name="klgdrinsert" action="insert_klgank.php" id="klgdrinsert" method="post" onsubmit="return validate_form();">
            <input type="hidden" name="id_pengguna" value="{$smarty.get.id}">
            <table cellspacing="0" cellpadding="0" border="0">
                <tr class="collapse">
                    <td class="labelrow">Nama Keluarga&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="nama" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Jenis Kelamin&nbsp;:&nbsp;</td>
                    <td class="inputrow">
                        <select name="kelamin">
                            <option value="1">Laki-Laki</option>
                            <option value="2">Perempuan</option>
                        </select>
                    </td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Hubungan Keluarga&nbsp;:&nbsp;</td>
                    <td class="inputrow">
                        <select name="status">
                            <option value="1">Anak Kandung</option>
                            <option value="2">Anak Tiri</option>
                            <option value="3">Anak Angkat</option>
                        </select>
                    </td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Pekerjaan&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="pekerjaan" style="width:600px;" maxlength="50"  /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Jenjang&nbsp;:&nbsp;</td>
                    <td class="inputrow">
                        <select name="pendidikan" id="id_pdd">
                            {foreach item="pdd1" from=$ID_PDD}
                                {html_options values=$pdd1.ID_PENDIDIKAN_AKHIR output=$pdd1.NAMA_PENDIDIKAN_AKHIR}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tanggal Lahir&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tgl_lahir" style="width:120px; text-align:center;" id="tgl_lahir" style="text-align:center;" onclick="javascript:NewCssCal('tgl_lahir', 'ddmmyyyy', '', '', '', '', 'past')" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tempat Lahir &nbsp;:&nbsp;</td>
                    <td class="inputrow">
                        <select id="negara" name="negara">
                            <option value="">Pilih Negara</option>
                            {foreach $negara as $n}
                                <option value="{$n.ID_NEGARA}">{$n.NM_NEGARA}</option>
                            {/foreach}
                        </select>
                        <select id="propinsi" name="propinsi">
                            <option value="">Pilih Propinsi</option>
                        </select>
                        <select id="kota" name="tmpat_lahir">
                            <option value="1">Pilih Kota</option>
                        </select>
                    </td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow"></td>
                    <td class="inputrow"><input type="submit" name="submit" value="Simpan" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('pdd{$d.ID_KELUARGA_DIRI}')" /></td>
                </tr>
            </table>
        </form>
        {literal}
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#negara').change(function() {
                        $.ajax({
                            type: "POST",
                            url: "getNegaraPropinsi.php",
                            data: {id: $('#negara').val()},
                            success: function(data) {
                                $('#propinsi').html(data);
                            }
                        })
                    });
                    $('#propinsi').change(function() {
                        $.ajax({
                            type: "POST",
                            url: "getPropinsiKota.php",
                            data: {id: $('#propinsi').val()},
                            success: function(data) {
                                $('#kota').html(data);
                            }
                        })
                    });
                });
            </script>
        {/literal}
    </body>
</html>