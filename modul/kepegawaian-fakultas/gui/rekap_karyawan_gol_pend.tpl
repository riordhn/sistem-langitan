<div class="center_title_bar">Rekapitulasi Data Karyawan Aktif berdasarkan Golongan dan Pendidikan {foreach item="list" from=$FAK}<li>{$list.FAKULTAS}</li>{/foreach}</div>
<form action="rekap_karyawan_gol_pend.php" method="get">
<input type="hidden" name="action" value="view">
<p>Pilih Unit Kerja : 
	<select name="id" id="id">
	<option value="">----------</option>
	<option value='0' style="font-weight: bold; color: #FFF; background-color: #261831;">LIHAT SEMUA</option>
	{foreach item="prodi" from=$KD_PRODI}
	{html_options values=$prodi.ID_UNIT_KERJA output=$prodi.NM_PROGRAM_STUDI}
	{/foreach}
	</select>
	<input type="submit" name="View" value="View">
</p>
</form>
	<table  width="850" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
        <tr>
			<th rowspan="2" width="130" style="vertical-align: middle;"><center>Golongan</center></th>
			<th colspan="12" width="720"><center>Pendidikan Akhir</center></th>
		</tr>
		<tr>
			<th width="60"><center>S3</center></th>
			<th width="60"><center>S2</center></th>
			<th width="60"><center>S1</center></th>
			<th width="60"><center>D4</center></th>
			<th width="60"><center>D3</center></th>
			<th width="60"><center>D2</center></th>
			<th width="60"><center>D1</center></th>
			<th width="60"><center>SMA</center></th>
			<th width="60"><center>SMP</center></th>
			<th width="60"><center>SD</center></th>
			<th width="60"><center>&Sigma;</center></th>
			<th width="60"><center>&#37;</center></th>
		</tr>
		</thead>
		<tbody>
			{foreach item="gol" from=$GOL}
		<tr>
			<td><center>{$gol.GOLONGAN}</center></td>
			{if $gol.S3 == 0}<td></td>{else}<td class="link" onclick="window.location.href='#rekap_peg-rekap_gol_pddkn_peg!list_karyawan.php?id={$smarty.get.id}&action=list&mode=pg&pdd=10&gol={$gol.ID_GOLONGAN}';" alt="Klik untuk melihat daftar" title="Klik untuk melihat daftar">{$gol.S3}</td>{/if}
			{if $gol.S2 == 0}<td></td>{else}<td class="link" onclick="window.location.href='#rekap_peg-rekap_gol_pddkn_peg!list_karyawan.php?id={$smarty.get.id}&action=list&mode=pg&pdd=1&gol={$gol.ID_GOLONGAN}';" alt="Klik untuk melihat daftar" title="Klik untuk melihat daftar">{$gol.S2}</td>{/if}
			{if $gol.S1 == 0}<td></td>{else}<td class="link" onclick="window.location.href='#rekap_peg-rekap_gol_pddkn_peg!list_karyawan.php?id={$smarty.get.id}&action=list&mode=pg&pdd=2&gol={$gol.ID_GOLONGAN}';" alt="Klik untuk melihat daftar" title="Klik untuk melihat daftar">{$gol.S1}</td>{/if}
			{if $gol.D4 == 0}<td></td>{else}<td class="link" onclick="window.location.href='#rekap_peg-rekap_gol_pddkn_peg!list_karyawan.php?id={$smarty.get.id}&action=list&mode=pg&pdd=3&gol={$gol.ID_GOLONGAN}';" alt="Klik untuk melihat daftar" title="Klik untuk melihat daftar">{$gol.D4}</td>{/if}
			{if $gol.D3 == 0}<td></td>{else}<td class="link" onclick="window.location.href='#rekap_peg-rekap_gol_pddkn_peg!list_karyawan.php?id={$smarty.get.id}&action=list&mode=pg&pdd=4&gol={$gol.ID_GOLONGAN}';" alt="Klik untuk melihat daftar" title="Klik untuk melihat daftar">{$gol.D3}</td>{/if}
			{if $gol.D2 == 0}<td></td>{else}<td class="link" onclick="window.location.href='#rekap_peg-rekap_gol_pddkn_peg!list_karyawan.php?id={$smarty.get.id}&action=list&mode=pg&pdd=5&gol={$gol.ID_GOLONGAN}';" alt="Klik untuk melihat daftar" title="Klik untuk melihat daftar">{$gol.D2}</td>{/if}
			{if $gol.D1 == 0}<td></td>{else}<td class="link" onclick="window.location.href='#rekap_peg-rekap_gol_pddkn_peg!list_karyawan.php?id={$smarty.get.id}&action=list&mode=pg&pdd=6&gol={$gol.ID_GOLONGAN}';" alt="Klik untuk melihat daftar" title="Klik untuk melihat daftar">{$gol.D1}</td>{/if}
			{if $gol.SLTA == 0}<td></td>{else}<td class="link" onclick="window.location.href='#rekap_peg-rekap_gol_pddkn_peg!list_karyawan.php?id={$smarty.get.id}&action=list&mode=pg&pdd=13&gol={$gol.ID_GOLONGAN}';" alt="Klik untuk melihat daftar" title="Klik untuk melihat daftar">{$gol.SLTA}</td>{/if}
			{if $gol.SLTP == 0}<td></td>{else}<td class="link" onclick="window.location.href='#rekap_peg-rekap_gol_pddkn_peg!list_karyawan.php?id={$smarty.get.id}&action=list&mode=pg&pdd=12&gol={$gol.ID_GOLONGAN}';" alt="Klik untuk melihat daftar" title="Klik untuk melihat daftar">{$gol.SLTP}</td>{/if}
			{if $gol.SD == 0}<td></td>{else}<td class="link" onclick="window.location.href='#rekap_peg-rekap_gol_pddkn_peg!list_karyawan.php?id={$smarty.get.id}&action=list&mode=pg&pdd=11&gol={$gol.ID_GOLONGAN}';" alt="Klik untuk melihat daftar" title="Klik untuk melihat daftar">{$gol.SD}</td>{/if}
			<td><center>{$gol.TOTAL}</center></td>
			<td><center>{math equation="((x / y) * 100)" x=$gol.TOTAL y=$TTL format="%.2f"}</center></td>
		</tr>
			{foreachelse}
        <tr><td colspan="12"><em>Data tidak ditemukan</em></td></tr>
			{/foreach}
			{foreach item="jml" from=$JML}
		<tr>
			<th><center>&Sigma;</center></th>
			<th><center>{$jml.S3}</center></th>
			<th><center>{$jml.S2}</center></th>
			<th><center>{$jml.S1}</center></th>
			<th><center>{$jml.D4}</center></th>
			<th><center>{$jml.D3}</center></th>
			<th><center>{$jml.D2}</center></th>
			<th><center>{$jml.D1}</center></th>
			<th><center>{$jml.SLTA}</center></th>
			<th><center>{$jml.SLTP}</center></th>
			<th><center>{$jml.SD}</center></th>
			<th><center>{$jml.TOTAL}</center></th>
			<th><center></center></th>
		</tr>
			{/foreach}
			{foreach item="psn" from=$PSN}
		<tr>
			<th><center>&#37;</center></th>
			<th><center>{$psn.S3|string_format:"%.2f"}</center></th>
			<th><center>{$psn.S2|string_format:"%.2f"}</center></th>
			<th><center>{$psn.S1|string_format:"%.2f"}</center></th>
			<th><center>{$psn.D4|string_format:"%.2f"}</center></th>
			<th><center>{$psn.D3|string_format:"%.2f"}</center></th>
			<th><center>{$psn.D2|string_format:"%.2f"}</center></th>
			<th><center>{$psn.D1|string_format:"%.2f"}</center></th>
			<th><center>{$psn.SLTA|string_format:"%.2f"}</center></th>
			<th><center>{$psn.SLTP|string_format:"%.2f"}</center></th>
			<th><center>{$psn.SD|string_format:"%.2f"}</center></th>
			<th><center></center></th>
			<th><center>{$psn.PERSEN}</center></th>
		</tr>
			{/foreach}
		</tbody>
	</table>
<p><br/>&nbsp;</p>