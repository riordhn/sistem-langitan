{literal}
<script type="text/javascript" src="js/dosen_edit.js"></script>
<script language="javascript" type="text/javascript">
var popupWindow=null;
function popup(mypage,myname,w,h,pos,infocus){
if (pos == "random")
{LeftPosition=(screen.width)?Math.floor(Math.random()*(screen.width-w)):100;TopPosition=(screen.height)?Math.floor(Math.random()*((screen.height-h)-75)):100;}
else
{LeftPosition=(screen.width)?(screen.width-w)/2:100;TopPosition=(screen.height)?(screen.height-h)/2:100;}
settings="width="+ w + ",height="+ h + ",top=" + TopPosition + ",left=" + LeftPosition + ",scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no";popupWindow=window.open("",myname,settings);
if(infocus=="front"){popupWindow.focus();popupWindow.location=mypage;}
if(infocus=="back"){popupWindow.blur();popupWindow.location=mypage;popupWindow.blur();}
}

function edit(itemID){
if ((document.getElementById(itemID).style.display  == 'none'))
{
document.getElementById(itemID).style.display  = '';
document.getElementById('hide'+itemID).style.display  = '';
document.getElementById('row'+itemID).style.display  = 'none';
} else {
document.getElementById(itemID).style.display  = 'none';
document.getElementById('hide'+itemID).style.display  = 'none';
document.getElementById('row'+itemID).style.display  = '';
}
}
</script>
{/literal}

<div class="center_title_bar">Detail Data Dosen</div>
{* biodata dosen *}
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<th width="840"><center><strong>BIODATA</strong></center></th>
		<th width="10"><center><span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/left.png" alt="Kembali" title="Kembali" onclick="javascript:history.go(-1);" /></span></center></th>
	</tr>
</table>
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	{foreach item="dsn" from=$DOSEN}
	<tr>
		<td>NIP/NIK</td>
		<td><center>:</center></td>
		<td>{$dsn.USERNAME}</td>
		<td rowspan="10"><center><img src="{$PHOTO}" border="0" width="160" /><br/><br/><input type="button" name="ganti_photo" value="Ganti Photo" onclick="javascript:popup('{$IMG}','name','600','400','center','front')"></center></td>
	</tr>
	<tr>
		<td>NIDN</td>
		<td><center>:</center></td>
		{if $dsn.NIDN_DOSEN > 0}
		<td>{$dsn.NIDN_DOSEN} <input type="button" name="cek_nidn" value="Cek Data DIKTI" onclick="javascript:popup('http://evaluasi.pdpt.dikti.go.id/epsbed/datadosen/{$dsn.NIDN_DOSEN}','name','960','435','center','front');"></td>
		{else}
		<td>-</td>
		{/if}
	</tr>
	<tr>
		<td>SERDOS</td>
		<td><center>:</center></td>
		{if $dsn.SERDOS > 0}
		<td>{$dsn.SERDOS}</td>
		{else}
		<td>-</td>
		{/if}		
	</tr>
	<tr>
		<td>Departemen</td>
		<td><center>:</center></td>
		<td>{$dsn.NM_DEPARTEMEN}</td>
	</tr>
	<tr>
		<td>Program Studi</td>
		<td><center>:</center></td>
		<td>{$dsn.NM_PROGRAM_STUDI}</td>
	</tr>
	<tr>
		<td>Fakultas</td>
		<td><center>:</center></td>
		<td>{$dsn.NM_FAKULTAS}</td>
	</tr>
	<tr>
		<td>Status Kepegawaian</td>
		<td><center>:</center></td>
		{if $dsn.STATUS_DOSEN == 'PENDIDIK KLINIS'}
		<td>{$dsn.STATUS_DOSEN} ({$dsn.ASAL_INSTITUSI})</td>
		{else}
		<td>{$dsn.STATUS_DOSEN} ( {$dsn.NM_GOLONGAN} - {$dsn.NM_PANGKAT} )</td>
		{/if}
	</tr>
	<tr>
		<td>Jabatan Fungsional</td>
		<td><center>:</center></td>
		{if $dsn.NM_JABATAN_FUNGSIONAL != null}
		<td>{$dsn.NM_JABATAN_FUNGSIONAL}</td>
		{else}
		<td>-</td>
		{/if}	
	</tr>
{*
	<tr>
		<td>Jabatan Struktural</td>
		<td><center>:</center></td>
		{if $dsn.NM_JABATAN_PEGAWAI != null}
		<td>{$dsn.NM_JABATAN_PEGAWAI}</td>
		{else}
		<td>-</td>
		{/if}	
	</tr>
*}
	<tr>
		<td>Status Aktif</td>
		<td><center>:</center></td>
		<td>{$dsn.NM_STATUS_PENGGUNA}</td>
	</tr>
	<tr>
		<td>Gelar Depan</td>
		<td><center>:</center></td>
		<td>{$dsn.GELAR_DEPAN}</td>
	</tr>
	<tr>
		<td>Gelar Belakang</td>
		<td><center>:</center></td>
		<td colspan="2">{$dsn.GELAR_BELAKANG}</td>
	</tr>
	<tr>
		<td>Nama Lengkap</td>
		<td><center>:</center></td>
		<td colspan="2">{$dsn.NM_PENGGUNA}</td>
	</tr>
	<tr>
		<td>Tempat, Tanggal Lahir</td>
		<td><center>:</center></td>
		<td colspan="2">{$dsn.TEMPAT_LAHIR}, {$dsn.TGL_LAHIR_PENGGUNA}</td>
	</tr>
	<tr>
		<td>Jenis Kelamin</td>
		<td><center>:</center></td>
		{if $dsn.KELAMIN_PENGGUNA=='1'}
		<td colspan="2">LAKI-LAKI</td>
		{elseif $dsn.KELAMIN_PENGGUNA=='2'}
		<td colspan="2">PEREMPUAN</td>
		{else}
		<td colspan="2">-</td>
		{/if}
	</tr>
	<tr>
		<td>Alamat</td>
		<td><center>:</center></td>
		<td colspan="2">{$dsn.ALAMAT_RUMAH_DOSEN}</td>
	</tr>
	<tr>
		<td>Telepon/HP</td>
		<td><center>:</center></td>
		{if $dsn.TLP_DOSEN == null}
		<td colspan="2">{$dsn.MOBILE_DOSEN} {*{if $dsn.MOBILE_DOSEN != ''}<span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/sms.png" alt="Kirim SMS" title="Kirim SMS" onclick="javascript:popup('{$LINK}','name','600','400','center','front');" /></span>{else}{/if}*}</td>
		{else if $dsn.MOBILE_DOSEN == null}
		<td colspan="2">{$dsn.TLP_DOSEN}</td>
		{else}
		<td colspan="2">{$dsn.TLP_DOSEN} / {$dsn.MOBILE_DOSEN} {*{if $dsn.MOBILE_DOSEN != ''}<span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/sms.png" alt="Kirim SMS" title="Kirim SMS" onclick="javascript:popup('{$LINK}','name','600','400','center','front');" /></span>{else}{/if}*}</td>
		{/if}
	</tr>
	<tr>
		<td>Email #1</td>
		<td><center>:</center></td>
		<td colspan="2">{$dsn.EMAIL_PENGGUNA}</td>
	</tr>
	<tr>
		<td>Email #2</td>
		<td><center>:</center></td>
		<td colspan="2">{$dsn.EMAIL_ALTERNATE}</td>
	</tr>
	<tr>
		<td>NPWP</td>
		<td><center>:</center></td>
		<td colspan="2">{$dsn.NPWP}</td>
	</tr>
	<tr>
		<td>Rekening Bank</td>
		<td><center>:</center></td>
		<td colspan="2">{$dsn.REKENING}</td>
	</tr>
	{/foreach}
	<tr>
		<td colspan="4" style="text-align: right;width: 850"><span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/edit.png" alt="Edit" title="Edit" onclick="window.location.href='#data_dosen_fk!dosen_edit_biodata_fk.php?action=edit_biodata&id={$dsn.ID_DOSEN}';" /></span></td>
	</tr>
</table>

{* riwayat dosen *}
{*
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="3" width="850"><center><strong>RIWAYAT</strong></center></th></tr>
	<tr>
		<th><center><strong>Nama</strong></center></th>
		<th><center><strong>Jenis</strong></center></th>
		<th><center><strong>Periode</strong></center></th>
	</tr>
	{foreach item="rwt" from=$RWYT}
	<tr>
		<td>{$rwt.NM_DOS_RIWAYAT}</td>
		<td>{$rwt.JENIS_DOS_RIWAYAT}</td>
		<td><center>{$rwt.MULAI_DOS_RIWAYAT} s/d {$rwt.SELESAI_DOS_RIWAYAT}</center></td>
	</tr>
	{foreachelse}
        <tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
</table>
*}

{* riwayat golongan *}
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="8" width="850"><center><strong>RIWAYAT GOLONGAN</strong></center></th></tr>
	<tr>
		<th width="80"><center><strong>Golongan</strong></center></th>
		<th width="160"><center><strong>Surat Keputusan</strong></center></th>
		<th width="50"><center><strong>File</strong></center></th>
		<th width="190"><center><strong>Asal SK/Tanggal SK</strong></center></th>
		<th width="190"><center><strong>Penandatangan</strong></center></th>
		<th width="80"><center><strong>TMT</strong></center></th>
		<th width="50"><center><strong>Status</strong></center></th>
		<th width="50"><center><strong>Aksi</strong></center></th>
	</tr>
	{foreach item="gol" from=$GOL}
	<tr id="rowgol{$gol.ID_SEJARAH_GOLONGAN}">
		<td><center>{$gol.NM_GOLONGAN}</center></td>
		<td>{$gol.NO_SK_SEJARAH_GOLONGAN}</td>
		{if $gol.FILES == ''}
		<td>
			<center>
				<span onMouseOver="this.style.cursor='pointer'">
					<img src="includes/images/export.png" alt="Upload PDF" title="Upload PDF" onclick="javascript:popup('proses/upload_pdf.php?id={$smarty.get.id}&kode={$gol.ID_SEJARAH_GOLONGAN}&tahun={$gol.TAHUN}&jenis={$gol.KATEGORI}&doc=GOL','name','600','400','center','front');" />
				</span>
			</center>
		</td>
		{else}
		<td>
			<center>
				<span onMouseOver="this.style.cursor='pointer'">
					<img src="includes/images/pdf.png" alt="PDF" title="PDF" onclick="javascript:window.open('{$gol.FILES}');" />&nbsp;
					<img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_file_golongan" id="hapus_file_golongan{$gol.ID_UPLOAD_FILE}" onclick="javascript:if(confirm('Proses penghapusan adalah permanen. Lanjutkan?'))golfilehapus({$gol.ID_UPLOAD_FILE});" />
				</span>
			</center>
		</td>	
		{/if}
		<td>{$gol.ASAL_SK_SEJARAH_GOLONGAN}/{$gol.TGL_SK_SEJARAH_GOLONGAN}</td>
		<td>{$gol.TTD_SK_NAMA_PEJABAT}</td>
		<td><center>{$gol.TMT_SEJARAH_GOLONGAN}</center></td>
		{if $gol.STATUS_AKHIR == 1}
		<td><center><img src="includes/images/success.png" alt="Status Terakhir" title="Status Terakhir" /></center></td>
		<td>
			<center>
				<span onMouseOver="this.style.cursor='pointer'">
					<img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('gol{$gol.ID_SEJARAH_GOLONGAN}')" />
				</span>
			</center>
		</td>
		{else}
		<td><center><span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/forbidden.png" alt="Set sebagai Status Terakhir" title="Set sebagai Status Terakhir" name="id_golongan" id="id_golongan{$gol.ID_SEJARAH_GOLONGAN}" onclick="javascript:golsubmit({$gol.ID_SEJARAH_GOLONGAN});" /></span></center></td>
		<td>
			<center>
				<span onMouseOver="this.style.cursor='pointer'">
					<img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('gol{$gol.ID_SEJARAH_GOLONGAN}')" />&nbsp;
					<img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_golongan" id="hapus_golongan{$gol.ID_SEJARAH_GOLONGAN}" onclick="javascript:if(confirm('Proses penghapusan adalah permanen. Lanjutkan?'))golhapus({$gol.ID_SEJARAH_GOLONGAN});" />
				</span>
			</center>
		</td>
		{/if}
	</tr>
	<tr class="edit">
		<td colspan="8" id="hidegol{$gol.ID_SEJARAH_GOLONGAN}" style="display:none;">
	<div id="gol{$gol.ID_SEJARAH_GOLONGAN}" style="display:none;">
	<form action="dosen_detail.php" method="post">
	<table class="collapse">
	<tbody>
	<tr class="collapse">
		<td class="labelrow">Golongan<font color="red">*</font>&nbsp;:&nbsp;</td>
		<td class="inputrow">
			<input type="hidden" name="id_sej" value="{$gol.ID_SEJARAH_GOLONGAN}" />
			<input type="hidden" name="action" value="edit_gol" />
			<select name="id_gol" id="id_gol">
				{foreach item="gol1" from=$ID_GOL}
				{html_options values=$gol1.ID_GOLONGAN output=$gol1.NM_GOLONGAN selected=$gol.ID_GOLONGAN}
				{/foreach}
			</select>
		&nbsp;&nbsp;
		TMT<font color="red">*</font>&nbsp;:&nbsp;<input type="text" name="tmt_sej" style="width:120px; text-align:center;" id="tmt_gol{$gol.ID_SEJARAH_GOLONGAN}" onclick="javascript:NewCssCal('tmt_gol{$gol.ID_SEJARAH_GOLONGAN}','ddmmyyyy','','','','','past')" value="{$gol.TMT_SEJARAH_GOLONGAN}" />
		&nbsp;&nbsp;
		Tanggal SK<font color="red">*</font>&nbsp;:&nbsp;<input type="text" name="tgl_sej" style="width:120px; text-align:center;" id="tgl_gol{$gol.ID_SEJARAH_GOLONGAN}" onclick="javascript:NewCssCal('tgl_gol{$gol.ID_SEJARAH_GOLONGAN}','ddmmyyyy','','','','','past')" value="{$gol.TGL_SK_SEJARAH_GOLONGAN}" /></td>
	</tr>
	<tr class="collapse">
		<td class="labelrow">No. SK<font color="red">*</font>&nbsp;:&nbsp;</td>
		<td class="inputrow"><input type="text" name="no_sk" style="width:600px;" maxlength="50" value="{$gol.NO_SK_SEJARAH_GOLONGAN}" /></td>
	</tr>
	<tr class="collapse">
		<td class="labelrow">Asal SK<font color="red">*</font>&nbsp;:&nbsp;</td>
		<td class="inputrow"">
			<input type="text" name="asal_sk" style="width:250px;" maxlength="50" value="{$gol.ASAL_SK_SEJARAH_GOLONGAN}" />&nbsp;&nbsp;
			Keterangan&nbsp;:&nbsp;<input type="text" name="ket_sk" style="width:259px;" maxlength="50" value="{$gol.KETERANGAN_SK_SEJARAH_GOLONGAN}" />
		</td>
	</tr>
	<tr class="collapse">
		<td class="labelrow">Penandatangan SK<font color="red">*</font>&nbsp;:&nbsp;</td>
		<td class="inputrow"><input type="text" name="ttd_sk" style="width:600px;" maxlength="120" value="{$gol.TTD_SK_NAMA_PEJABAT}" /></td>
	</tr>
	<tr class="collapse">
		<td class="labelrow"></td>
		<td class="inputrow"><input type="submit" name="submit" value="Simpan" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('gol{$gol.ID_SEJARAH_GOLONGAN}')" /></center></td>
	</tr>
	</tbody>
	</table>
	</form>
	</div>
		</td>
	</tr>
	{foreachelse}
        <tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	<tr id="tambahgol_btn">
		<td colspan="8" style="text-align: right;"><a onclick="javascript:togglegol('tambahgol', 'golbtn');" onMouseOver="this.style.cursor='pointer'" id="golbtn"><img src="includes/images/add.png" alt="Tambah" title="Tambah"></a></td>
	</tr>
</table>
<div id="tambahgol" style="display: none;">
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="8" width="850"><center><strong>INPUT SEJARAH GOLONGAN - {$dsn.NM_PENGGUNA}</strong></center></th></tr>
	<tr><td colspan="8" width="850">
	<iframe scrolling="no" src="insert_gol.php?id={$dsn.ID_PENGGUNA}" width="100%" frameborder="0"></iframe>
	</td></tr>
</table>
</div>

{* riwayat jabatan fungsionl *}
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="8" width="850"><center><strong>RIWAYAT JABATAN FUNGSIONAL</strong></center></th></tr>
	<tr>
		<th width="80"><center><strong>Fungsional</strong></center></th>
		<th width="160"><center><strong>Surat Keputusan</strong></center></th>
		<th width="50"><center><strong>File</strong></center></th>
		<th width="190"><center><strong>Asal SK/Tanggal SK</strong></center></th>
		<th width="190"><center><strong>Penandatangan</strong></center></th>
		<th width="80"><center><strong>TMT</strong></center></th>
		<th width="50"><center><strong>Status</strong></center></th>
		<th width="50"><center><strong>Aksi</strong></center></th>
	</tr>
	{foreach item="jab" from=$JAB}
	<tr id="rowfsg{$jab.ID_SEJARAH_JABATAN_FUNGSIONAL}">
		<td>{$jab.NM_JABATAN_FUNGSIONAL}</td>
		<td>{$jab.NO_SK_SEJ_JAB_FUNGSIONAL}</td>
		{if $jab.FILES == '' || $jab.FILES == null}
		<td>
			<center>
				<span onMouseOver="this.style.cursor='pointer'">
					<img src="includes/images/export.png" alt="Upload PDF" title="Upload PDF" onclick="javascript:popup('proses/upload_pdf.php?id={$smarty.get.id}&kode={$jab.ID_SEJARAH_JABATAN_FUNGSIONAL}&tahun={$jab.TAHUN}&jenis={$jab.KATEGORI}&doc=JAB','name','600','400','center','front');" />
				</span>
			</center>
		</td>
		{else}
		<td>
			<center>
				<span onMouseOver="this.style.cursor='pointer'">
					<img src="includes/images/pdf.png" alt="PDF" title="PDF" onclick="javascript:window.open('{$jab.FILES}');" />&nbsp;
					<img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_file_golongan" id="hapus_file_jabatan{$jab.ID_UPLOAD_FILE}" onclick="javascript:if(confirm('Proses penghapusan adalah permanen. Lanjutkan?'))jabfilehapus({$jab.ID_UPLOAD_FILE});" />
				</span>
			</center>
		</td>	
		{/if}
		<td>{$jab.ASAL_SK_SEJ_JAB_FUNGSIONAL}/{$jab.TGL_SK_SEJ_JAB_FUNGSIONAL}</td>
		<td>{$jab.TTD_SK_SEJ_JAB_FUNGSIONAL}</td>
		<td><center>{$jab.TMT_SEJ_JAB_FUNGSIONAL}</center></td>
		{if $jab.STATUS_AKHIR == 1}
		<td><center><img src="includes/images/success.png" alt="Status Terakhir" title="Status Terakhir" /></center></td>
		<td>
			<center>
				<span onMouseOver="this.style.cursor='pointer'">
					<img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('fsg{$jab.ID_SEJARAH_JABATAN_FUNGSIONAL}')" />
				</span>
			</center>
		</td>
		{else}
		<td><center><span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/forbidden.png" alt="Set sebagai Status Terakhir" title="Set sebagai Status Terakhir" name="id_fungsional" id="id_fungsional{$jab.ID_SEJARAH_JABATAN_FUNGSIONAL}" onclick="javascript:fsgsubmit({$jab.ID_SEJARAH_JABATAN_FUNGSIONAL});" /></span></center></td>
		<td>
			<center>
				<span onMouseOver="this.style.cursor='pointer'">
					<img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('fsg{$jab.ID_SEJARAH_JABATAN_FUNGSIONAL}')" />&nbsp;
					<img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_fungsional" id="hapus_fungsional{$jab.ID_SEJARAH_JABATAN_FUNGSIONAL}" onclick="javascript:if(confirm('Proses penghapusan adalah permanen. Lanjutkan?'))fsghapus({$jab.ID_SEJARAH_JABATAN_FUNGSIONAL});" />
				</span>
			</center>
		</td>
		{/if}
	</tr>
	<tr class="edit">
		<td colspan="8" id="hidefsg{$jab.ID_SEJARAH_JABATAN_FUNGSIONAL}" style="display:none;">
	<div id="fsg{$jab.ID_SEJARAH_JABATAN_FUNGSIONAL}" style="display:none;">
	<form action="dosen_detail.php" method="post">
	<table class="collapse">
	<tbody>
	<tr class="collapse">
		<td class="labelrow">Jabatan Fungsional<font color="red">*</font>&nbsp;:&nbsp;</td>
		<td class="inputrow">
			<input type="hidden" name="id_sej" value="{$jab.ID_SEJARAH_JABATAN_FUNGSIONAL}" />
			<input type="hidden" name="action" value="edit_fsg" />
			<select name="id_fsg" id="id_fsg">
				{foreach item="jab1" from=$ID_FSG}
				{html_options values=$jab1.ID_JABATAN_FUNGSIONAL output=$jab1.NM_JABATAN_FUNGSIONAL selected=$jab.ID_JABATAN_FUNGSIONAL}
				{/foreach}
			</select>
			&nbsp;&nbsp;
			TMT<font color="red">*</font>&nbsp;:&nbsp;<input type="text" name="tmt_sej" style="width:120px; text-align:center;" id="tmt_fsg{$jab.ID_SEJARAH_JABATAN_FUNGSIONAL}" onclick="javascript:NewCssCal('tmt_fsg{$jab.ID_SEJARAH_JABATAN_FUNGSIONAL}','ddmmyyyy','','','','','past')" value="{$jab.TMT_SEJ_JAB_FUNGSIONAL}" />
			&nbsp;&nbsp;
			Tanggal SK<font color="red">*</font>&nbsp;:&nbsp;<input type="text" name="tgl_sej" style="width:120px; text-align:center;" id="tgl_fsg{$jab.ID_SEJARAH_JABATAN_FUNGSIONAL}" onclick="javascript:NewCssCal('tgl_fsg{$jab.ID_SEJARAH_JABATAN_FUNGSIONAL}','ddmmyyyy','','','','','past')" value="{$jab.TGL_SK_SEJ_JAB_FUNGSIONAL}" />
		</td>
	</tr>
	<tr class="collapse">
		<td class="labelrow">No. SK<font color="red">*</font>&nbsp;:&nbsp;</td>
		<td class="inputrow"><input type="text" name="no_sk" style="width:600px;" maxlength="50" value="{$jab.NO_SK_SEJ_JAB_FUNGSIONAL}" /></td>
	</tr>
	<tr class="collapse">
		<td class="labelrow">Asal SK<font color="red">*</font>&nbsp;:&nbsp;</td>
		<td class="inputrow"><input type="text" name="asal_sk" style="width:250px;" maxlength="50" value="{$jab.ASAL_SK_SEJ_JAB_FUNGSIONAL}" />&nbsp;&nbsp;
		Keterangan&nbsp;:&nbsp;<input type="text" name="ket_sk" style="width:259px;" maxlength="50" value="{$jab.KET_SK_SEJ_JAB_FUNGSIONAL}" /></td>
	</tr>
	<tr class="collapse">
		<td class="labelrow">Penandatangan SK<font color="red">*</font>&nbsp;:&nbsp;</td>
		<td class="inputrow"><input type="text" name="ttd_sk" style="width:600px;" maxlength="120" value="{$jab.TTD_SK_SEJ_JAB_FUNGSIONAL}" /></td>
	</tr>
	<tr class="collapse">
		<td class="labelrow"></td>
		<td class="inputrow"><input type="submit" name="submit" value="Simpan" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('fsg{$jab.ID_SEJARAH_JABATAN_FUNGSIONAL}')" /></td>
	</tr>
	</tbody>
	</table>
	</form>
	</div>
		</td>
	</tr>
	{foreachelse}
        <tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	<tr id="tambahfsg_btn">
		<td colspan="8" style="text-align: right;"><a onclick="javascript:togglegol('tambahfsg', 'fsgbtn');" onMouseOver="this.style.cursor='pointer'" id="fsgbtn"><img src="includes/images/add.png" alt="Tambah" title="Tambah"></a></td>
	</tr>
</table>
<div id="tambahfsg" style="display: none;">
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="8" width="850"><center><strong>INPUT SEJARAH JABATAN FUNGSIONAL - {$dsn.NM_PENGGUNA}</strong></center></th></tr>
	<tr><td colspan="8" width="850">
	<iframe scrolling="no" src="insert_fsg.php?id={$dsn.ID_PENGGUNA}" width="100%" frameborder="0"></iframe>
	</td></tr>
</table>
</div>

{* riwayat pendidikan *}
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="8" width="850"><center><strong>PENDIDIKAN</strong></center></th></tr>
	<tr>
		<th><center><strong>Jenjang</strong></center></th>
		<th width="50"><center><strong>File</strong></center></th>
		<th><center><strong>Nama Sekolah</strong></center></th>
		<th><center><strong>Jurusan</strong></center></th>
		<th><center><strong>Tempat</strong></center></th>
		<th width="50"><center><strong>Lulus</strong></center></th>
		<th width="50"><center><strong>Status</strong></center></th>
		<th width="50"><center><strong>Aksi</strong></center></th>
	</tr>
	{foreach item="pdd" from=$PEND}
	<tr id="rowpdd{$pdd.ID_SEJARAH_PENDIDIKAN}">
		<td>{$pdd.NAMA_PENDIDIKAN_AKHIR}</td>
		{if $pdd.FILES == ''}
		<td>
			<center>
				<span onMouseOver="this.style.cursor='pointer'">
					<img src="includes/images/export.png" alt="Upload PDF" title="Upload PDF" onclick="javascript:popup('proses/upload_pdf.php?id={$smarty.get.id}&kode={$pdd.ID_SEJARAH_PENDIDIKAN}&tahun={$pdd.TAHUN_LULUS_PENDIDIKAN}&jenis={$pdd.KATEGORI}&doc=PDD','name','600','400','center','front');" />
				</span>
			</center>
		</td>
		{else}
		<td>
			<center>
				<span onMouseOver="this.style.cursor='pointer'">
					<img src="includes/images/pdf.png" alt="PDF" title="PDF" onclick="javascript:window.open('{$pdd.FILES}');" />&nbsp;
					<img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_file_pendidikan" id="hapus_file_pendidikan{$pdd.ID_UPLOAD_FILE}" onclick="javascript:if(confirm('Proses penghapusan adalah permanen. Lanjutkan?'))pddfilehapus({$pdd.ID_UPLOAD_FILE});" />
				</span>
			</center>
		</td>	
		{/if}
		<td>{$pdd.NM_SEKOLAH_PENDIDIKAN}</td>
		<td>{$pdd.NM_JURUSAN_PENDIDIKAN}</td>
		<td>{$pdd.TEMPAT_PENDIDIKAN}</td>
		<td><center>{$pdd.TAHUN_LULUS_PENDIDIKAN}</center></td>
		{if $pdd.STATUS_AKHIR == 1}
		<td><center><img src="includes/images/success.png" alt="Status Terakhir" title="Status Terakhir" /></center></td>
		<td>
			<center>
				<span onMouseOver="this.style.cursor='pointer'">
					<img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('pdd{$pdd.ID_SEJARAH_PENDIDIKAN}')" />
				</span>
			</center>
		</td>
		{else}
		<td><center><span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/forbidden.png" alt="Set sebagai Status Terakhir" title="Set sebagai Status Terakhir" name="id_pendidikan" id="id_pendidikan{$pdd.ID_SEJARAH_PENDIDIKAN}" onclick="javascript:pddsubmit({$pdd.ID_SEJARAH_PENDIDIKAN});" /></span></center></td>
		<td>
			<center>
				<span onMouseOver="this.style.cursor='pointer'">
					<img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('pdd{$pdd.ID_SEJARAH_PENDIDIKAN}')" />&nbsp;
					<img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_pendidikan" id="hapus_pendidikan{$pdd.ID_SEJARAH_PENDIDIKAN}" onclick="javascript:if(confirm('Proses penghapusan adalah permanen. Lanjutkan?'))pddhapus({$pdd.ID_SEJARAH_PENDIDIKAN});" />
				</span>
			</center>
		</td>
		{/if}
	</tr>
	<tr class="edit">
		<td colspan="8" id="hidepdd{$pdd.ID_SEJARAH_PENDIDIKAN}" style="display:none;">
	<div id="pdd{$pdd.ID_SEJARAH_PENDIDIKAN}" style="display:none;">
	<form action="dosen_detail.php" method="post">
	<table class="collapse">
	<tbody>
	<tr class="collapse">
		<td class="labelrow">Jenjang&nbsp;:&nbsp;</td>
		<td class="inputrow" colspan="5">
			<input type="hidden" name="id_sej" value="{$pdd.ID_SEJARAH_PENDIDIKAN}" />
			<input type="hidden" name="action" value="edit_pdd" />
			<select name="id_pdd" id="id_pdd">
				{foreach item="pdd1" from=$ID_PDD}
				{html_options values=$pdd1.ID_PENDIDIKAN_AKHIR output=$pdd1.NAMA_PENDIDIKAN_AKHIR selected=$pdd.ID_PENDIDIKAN_AKHIR}
				{/foreach}
			</select>
		</td>
	</tr>
	<tr class="collapse">
		<td class="labelrow">Nama Sekolah&nbsp;:&nbsp;</td>
		<td class="inputrow" colspan="5"><input type="text" name="nm_skolah" style="width:600px;" maxlength="50" value="{$pdd.NM_SEKOLAH_PENDIDIKAN}" /></td>
	</tr>
	<tr class="collapse">
		<td class="labelrow">Jurusan&nbsp;:&nbsp;</td>
		<td class="inputrow" colspan="5"><input type="text" name="jur_skolah" style="width:600px;" maxlength="50" value="{$pdd.NM_JURUSAN_PENDIDIKAN}" /></td>
	</tr>
	<tr class="collapse">
		<td class="labelrow">Tempat Sekolah&nbsp;:&nbsp;</td>
		<td class="inputrow" colspan="5"><input type="text" name="tpt_skolah" style="width:600px;" maxlength="50" value="{$pdd.TEMPAT_PENDIDIKAN}" /></td>
	</tr>
	<tr class="collapse">
		<td class="labelrow">Tahun Masuk&nbsp;:&nbsp;</td>
		<td class="inputrow">
			{html_select_date prefix='msk' time=$pdd.TAHUN_MASUK start_year='-60' end_year='+0' display_months=false display_days=false reverse_years=true}
		</td>
		<td class="labelrow">Tahun Lulus&nbsp;:&nbsp;</td>
		<td class="inputrow">
			{html_select_date prefix='pdd' time=$pdd.TAHUN_LULUS start_year='-60' end_year='+0' display_months=false display_days=false reverse_years=true}
		</td>
		<td class="labelrow">No. Ijasah&nbsp;:&nbsp;</td>
		<td class="inputrow">
			<input type="text" name="no_ijasah" style="width:160px;" maxlength="50" value="{$pdd.NO_IJASAH}" />
		</td>
	</tr>
	<tr class="collapse">
		<td class="labelrow"></td>
		<td class="inputrow" colspan="5"><input type="submit" name="submit" value="Simpan" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('pdd{$pdd.ID_SEJARAH_PENDIDIKAN}')" /></td>
	</tr>
	</tbody>
	</table>
	</form>
	</div>
		</td>
	</tr>
	{foreachelse}
        <tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	<tr id="tambahpdd_btn">
		<td colspan="8" style="text-align: right;"><a onclick="javascript:togglepdd('tambahpdd', 'pddbtn');" onMouseOver="this.style.cursor='pointer'" id="pddbtn"><img src="includes/images/add.png" alt="Tambah" title="Tambah"></a></td>
	</tr>
</table>
<div id="tambahpdd" style="display: none;">
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="8" width="850"><center><strong>INPUT SEJARAH PENDIDIKAN - {$dsn.NM_PENGGUNA}</strong></center></th></tr>
	<tr><td colspan="8" width="850">
	<iframe scrolling="no" src="insert_pdd.php?id={$dsn.ID_PENGGUNA}" width="100%" frameborder="0"></iframe>
	</td></tr>
</table>
</div>

{* riwayat pengmas *}
{*
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="7" width="850"><center><strong>PENGABDIAN MASYARAKAT</strong></center></th></tr>
	<tr>
		<th><center><strong>Tingkat</strong></center></th>
		<th><center><strong>Nama</strong></center></th>
		<th><center><strong>Bidang</strong></center></th>
		<th><center><strong>Tempat</strong></center></th>
		<th><center><strong>Peran</strong></center></th>
		<th width="50"><center><strong>Tahun</strong></center></th>
		<th width="50"><center><strong>Aksi</strong></center></th>
	</tr>
	{foreach item="pms" from=$PEMS}
	<tr>
		<td>{$pms.TINGKAT_DOSEN_PENGMAS}</td>
		<td>{$pms.NM_DOSEN_PENGMAS}</td>
		<td>{$pms.BIDANG_DOSEN_PENGMAS}</td>
		<td>{$pms.TEMPAT_DOSEN_PENGMAS}</td>
		<td>{$pms.PERAN_DOSEN_PENGMAS}</td>
		<td><center>{$pms.THN_DOSEN_PENGMAS}</center></td>
		<td><center><span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/update.png" alt="Edit" title="Edit" />&nbsp;<img src="includes/images/delete.png" alt="Hapus" title="Hapus" /></span></center></td>
	</tr>
	{foreachelse}
        <tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	<tr>
		<td colspan="7" style="text-align: right;"><span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/add.png" alt="Tambah" title="Tambah" /></span></td>
	</tr>
</table>
*}
{* riwayat penghargaan *}
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="6" width="850"><center><strong>PENGHARGAAN</strong></center></th></tr>
	<tr>
		<th><center><strong>Tingkat</strong></center></th>
		<th><center><strong>Bidang</strong></center></th>
		<th><center><strong>Bentuk</strong></center></th>
		<th><center><strong>Pemberi</strong></center></th>
		<th width="50"><center><strong>Tahun</strong></center></th>
		<th width="50"><center><strong>Aksi</strong></center></th>
	</tr>
	{foreach item="phg" from=$PEHG}
	<tr>
		<td>{$phg.TINGKAT_DOSEN_PENGHARGAAN}</td>
		<td>{$phg.BIDANG_DOSEN_PENGHARGAAN}</td>
		<td>{$phg.BENTUK_DOSEN_PENGHARGAAN}</td>
		<td>{$phg.PEMBERI_DOSEN_PENGHARGAAN}</td>
		<td><center>{$phg.THN_DOSEN_PENGHARGAAN}</center></td>
		<td>
			<center>
				<span onMouseOver="this.style.cursor='pointer'">
					<img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('phg{$phg.ID_DOSEN_PENGHARGAAN}')" />&nbsp;
					<img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_penghargaan" id="hapus_penghargaan{$phg.ID_DOSEN_PENGHARGAAN}" onclick="javascript:if(confirm('Proses penghapusan adalah permanen. Lanjutkan?'))phghapus({$phg.ID_DOSEN_PENGHARGAAN});" />
				</span>
			</center>
		</td>
	</tr>
	<tr class="edit">
		<td colspan="6" id="hidephg{$phg.ID_DOSEN_PENGHARGAAN}" style="display:none;">
	<div id="phg{$phg.ID_DOSEN_PENGHARGAAN}" style="display:none;">
	<form action="dosen_detail.php" method="post">
	<table class="collapse">
	<tbody>
	<tr class="collapse">
		<td class="labelrow">Tingkat&nbsp;:&nbsp;</td>
		<td class="inputrow">
			<input type="hidden" name="id_phg" value="{$phg.ID_DOSEN_PENGHARGAAN}" />
			<input type="hidden" name="action" value="edit_phg" />
			<select name="tingkat" id="tingkat">
				{foreach item="tk" from=$TKT}
				{html_options values=$tk output=$tk selected=$phg.TINGKAT_DOSEN_PENGHARGAAN}
				{/foreach}
            </select>
		&nbsp;&nbsp;&nbsp;Negara&nbsp;:&nbsp;
			<select name="id_neg" id="id_neg">
				{foreach item="neg" from=$ID_NEG}
				{html_options values=$neg.ID_NEGARA output=$neg.NM_NEGARA selected=$phg.ID_NEGARA}
				{/foreach}
			</select>
		</td>
	</tr>
	<tr class="collapse">
		<td class="labelrow">Bidang&nbsp;:&nbsp;</td>
		<td class="inputrow"><input type="text" name="bidang" style="width:600px;" maxlength="250" value="{$phg.BIDANG_DOSEN_PENGHARGAAN}" /></td>
	</tr>
	<tr class="collapse">
		<td class="labelrow">Bentuk&nbsp;:&nbsp;</td>
		<td class="inputrow"><input type="text" name="bentuk" style="width:600px;" maxlength="250" value="{$phg.BENTUK_DOSEN_PENGHARGAAN}" /></td>
	</tr>
	<tr class="collapse">
		<td class="labelrow">Pemberi&nbsp;:&nbsp;</td>
		<td class="inputrow"><input type="text" name="pemberi" style="width:600px;" maxlength="250" value="{$phg.PEMBERI_DOSEN_PENGHARGAAN}" /></td>
	</tr>
	<tr class="collapse">
		<td class="labelrow">Tahun&nbsp;:&nbsp;</td>
		<td class="inputrow">
			{html_select_date prefix='tahun' time=$phg.THN start_year='-60' end_year='+0' display_months=false display_days=false reverse_years=true}
			&nbsp;&nbsp;&nbsp;<input type="submit" name="submit" value="Simpan" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('phg{$phg.ID_DOSEN_PENGHARGAAN}')" />
		</td>
	</tr>
	</tbody>
	</table>
	</form>
	</div>
		</td>
	</tr>
	{foreachelse}
        <tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	<tr>
		<td colspan="6" style="text-align: right;"><a onclick="javascript:togglephg('tambahphg', 'phgbtn');" onMouseOver="this.style.cursor='pointer'" id="phgbtn"><img src="includes/images/add.png" alt="Tambah" title="Tambah"></a></td>
	</tr>
</table>
<div id="tambahphg" style="display: none;">
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="6" width="850"><center><strong>INPUT PENGHARGAAN DOSEN - {$dsn.NM_PENGGUNA}</strong></center></th></tr>
	<tr><td colspan="6" width="850">
	<iframe scrolling="no" src="insert_phg.php?id={$dsn.ID_DOSEN}" width="100%" frameborder="0"></iframe>
	</td></tr>
</table>
</div>

{* riwayat publikasi *}
{*
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="7" width="850"><center><strong>PUBLIKASI</strong></center></th></tr>
	<tr>
		<th><center><strong>Jenis</strong></center></th>
		<th><center><strong>Judul</strong></center></th>
		<th><center><strong>Media</strong></center></th>
		<th><center><strong>Pengarang</strong></center></th>
		<th><center><strong>Penerbit</strong></center></th>
		<th width="50"><center><strong>Tahun</strong></center></th>
		<th width="50"><center><strong>Aksi</strong></center></th>
	</tr>
	{foreach item="pbk" from=$PEBK}
	<tr>
		<td>{$pbk.JENIS_DOSEN_PUBLIKASI}</td>
		<td>{$pbk.JUDUL_DOSEN_PUBLIKASI}</td>
		<td>{$pbk.MEDIA_DOSEN_PUBLIKASI}</td>
		<td>{$pbk.PENGARANG_DOSEN_PUBLIKASI}</td>
		<td>{$pbk.PENERBIT_DOSEN_PUBLIKASI}</td>
		<td><center>{$pbk.THN_DOSEN_PUBLIKASI}</center></td>
		<td><center><span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/update.png" alt="Edit" title="Edit" />&nbsp;<img src="includes/images/delete.png" alt="Hapus" title="Hapus" /></span></center></td>
	</tr>
	{foreachelse}
        <tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	<tr>
		<td colspan="7" style="text-align: right;"><span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/add.png" alt="Tambah" title="Tambah" /></span></td>
	</tr>
</table>
*}
{* riwayat organisasi *}
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="6" width="850"><center><strong>ORGANISASI</strong></center></th></tr>
	<tr>
		<th><center><strong>Tingkat</strong></center></th>
		<th><center><strong>Nama</strong></center></th>
		<th><center><strong>Jabatan</strong></center></th>
		<th><center><strong>Mulai</strong></center></th>
		<th><center><strong>Sampai</strong></center></th>
		<th width="50"><center><strong>Aksi</strong></center></th>
	</tr>
	{foreach item="org" from=$PORG}
	<tr>
		<td>{$org.TINGKAT_DOSEN_ORG}</td>
		<td>{$org.NM_DOSEN_ORG}</td>
		<td>{$org.JABATAN_DOSEN_ORG}</td>
		<td><center>{$org.MULAI_DOSEN_ORG}</center></td>
		<td><center>{$org.SELESAI_DOSEN_ORG}</center></td>
		<td>
			<center>
				<span onMouseOver="this.style.cursor='pointer'">
					<img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('org{$org.ID_DOSEN_ORGANISASI}')" />&nbsp;
					<img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_org" id="hapus_org{$org.ID_DOSEN_ORGANISASI}" onclick="javascript:if(confirm('Proses penghapusan adalah permanen. Lanjutkan?'))orghapus({$org.ID_DOSEN_ORGANISASI});" />
				</span>
			</center>
		</td>
	</tr>
	<tr class="edit">
		<td colspan="6" id="hideorg{$org.ID_DOSEN_ORGANISASI}" style="display:none;">
	<div id="org{$org.ID_DOSEN_ORGANISASI}" style="display:none;">
	<form action="dosen_detail.php" method="post">
	<table class="collapse">
	<tbody>
	<tr class="collapse">
		<td class="labelrow">Tingkat&nbsp;:&nbsp;</td>
		<td class="inputrow">
			<input type="hidden" name="id_org" value="{$org.ID_DOSEN_ORGANISASI}" />
			<input type="hidden" name="action" value="edit_org" />
			<select name="tingkat" id="tingkat">
				{foreach item="tk" from=$TKT}
				{html_options values=$tk output=$tk selected=$org.TINGKAT_DOSEN_ORG}
				{/foreach}
            </select>
			&nbsp;&nbsp;
			Mulai&nbsp;:&nbsp;<input type="text" name="mulai_org" style="width:120px; text-align:center;" id="mulai_org{$org.ID_DOSEN_ORGANISASI}" onclick="javascript:NewCssCal('mulai_org{$org.ID_DOSEN_ORGANISASI}','ddmmyyyy','','','','','past')" value="{$org.MULAI_DOSEN_ORG}" />
			&nbsp;&nbsp;
			Sampai&nbsp;:&nbsp;<input type="text" name="sampai_org" style="width:120px; text-align:center;" id="sampai_org{$org.ID_DOSEN_ORGANISASI}" onclick="javascript:NewCssCal('sampai_org{$org.ID_DOSEN_ORGANISASI}','ddmmyyyy','','','','','')" value="{$org.SELESAI_DOSEN_ORG}" />
		</td>
	</tr>
	<tr class="collapse">
		<td class="labelrow">Nama Organisasi&nbsp;:&nbsp;</td>
		<td class="inputrow"><input type="text" name="org" style="width:600px;" maxlength="250" value="{$org.NM_DOSEN_ORG}" /></td>
	</tr>
	<tr class="collapse">
		<td class="labelrow">Jabatan&nbsp;:&nbsp;</td>
		<td class="inputrow"><input type="text" name="jab" style="width:600px;" maxlength="250" value="{$org.JABATAN_DOSEN_ORG}" /></td>
	</tr>
	<tr class="collapse">
		<td class="labelrow"></td>
		<td class="inputrow"><input type="submit" name="submit" value="Simpan" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('org{$org.ID_DOSEN_ORGANISASI}')" /></td>
	</tr>
	</tbody>
	</table>
	</form>
	</div>
		</td>
	</tr>
	{foreachelse}
        <tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	<tr>
		<td colspan="6" style="text-align: right;"><a onclick="javascript:toggleorg('tambahorg', 'orgbtn');" onMouseOver="this.style.cursor='pointer'" id="orgbtn"><img src="includes/images/add.png" alt="Tambah" title="Tambah"></a></td>
	</tr>
</table>
<div id="tambahorg" style="display: none;">
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="6" width="850"><center><strong>INPUT ORGANISASI DOSEN - {$dsn.NM_PENGGUNA}</strong></center></th></tr>
	<tr><td colspan="6" width="850">
	<iframe scrolling="no" src="insert_org.php?id={$dsn.ID_DOSEN}" width="100%" frameborder="0"></iframe>
	</td></tr>
</table>
</div>

{* riwayat training *}
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="7" width="850"><center><strong>TRAINING/WORKSHOP/SEMINAR</strong></center></th></tr>
	<tr>
		<th><center><strong>Tingkat</strong></center></th>
		<th><center><strong>Jenis</strong></center></th>
		<th><center><strong>Nama</strong></center></th>
		<th><center><strong>Instansi</strong></center></th>
		<th><center><strong>Peran</strong></center></th>
		<th width="50"><center><strong>Tahun</strong></center></th>
		<th width="50"><center><strong>Aksi</strong></center></th>
	</tr>
	{foreach item="trg" from=$PTRG}
	<tr>
		<td>{$trg.TINGKAT_DOS_TRAINING}</td>
		<td>{$trg.JENIS_DOS_TRAINING}</td>
		<td>{$trg.NM_DOS_TRAINING}</td>
		<td>{$trg.INSTANSI_DOS_TRAINING}</td>
		<td>{$trg.PERAN_DOS_TRAINING}</td>
		<td><center>{$trg.THN_DOS_TRAINING}</center></td>
		<td>
			<center>
				<span onMouseOver="this.style.cursor='pointer'">
					<img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('trg{$trg.ID_DOSEN_TRAINING}')" />&nbsp;
					<img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_trg" id="hapus_trg{$trg.ID_DOSEN_TRAINING}" onclick="javascript:if(confirm('Proses penghapusan adalah permanen. Lanjutkan?'))trghapus({$trg.ID_DOSEN_TRAINING});" />
				</span>
			</center>
		</td>
	</tr>
	<tr class="edit">
		<td colspan="7" id="hidetrg{$trg.ID_DOSEN_TRAINING}" style="display:none;">
	<div id="trg{$trg.ID_DOSEN_TRAINING}" style="display:none;">
	<form action="dosen_detail.php" method="post">
	<table class="collapse">
	<tbody>
	<tr class="collapse">
		<td class="labelrow">Mulai&nbsp;:&nbsp;</td>
		<td class="inputrow">
			<input type="text" name="mulai_trg" style="width:120px; text-align:center;" id="mulai_trg{$trg.ID_DOSEN_TRAINING}" value="{$trg.MULAI}" onclick="javascript:NewCssCal('mulai_trg{$trg.ID_DOSEN_TRAINING}','ddMMyyyy','','','','','past')" />
			&nbsp;&nbsp;&nbsp;
			Sampai&nbsp;:&nbsp;<input type="text" name="sampai_trg" style="width:120px; text-align:center;" id="sampai_trg{$trg.ID_DOSEN_TRAINING}" value="{$trg.SAMPAI}" onclick="javascript:NewCssCal('sampai_trg{$trg.ID_DOSEN_TRAINING}','ddMMyyyy','','','','','past')" />
		</td>
	</tr>
	<tr class="collapse">
		<td class="labelrow">Tingkat&nbsp;:&nbsp;</td>
		<td class="inputrow">
			<input type="hidden" name="id_trg" value="{$trg.ID_DOSEN_TRAINING}" />
			<input type="hidden" name="action" value="edit_trg" />
			<select name="tingkat" id="tingkat">
				{foreach item="tk" from=$TKT}
				{html_options values=$tk output=$tk selected=$trg.TINGKAT_DOS_TRAINING}
				{/foreach}
            </select>
			&nbsp;&nbsp;
			Jenis&nbsp;:&nbsp;
			<select name="jenis" id="jenis">
				{foreach item="jn" from=$JNS}
				{html_options values=$jn output=$jn selected=$trg.JENIS_DOS_TRAINING}
				{/foreach}
            </select>
		</td>
	</tr>
	<tr class="collapse">
		<td class="labelrow">Nama Training&nbsp;:&nbsp;</td>
		<td class="inputrow"><input type="text" name="nama" style="width:600px;" maxlength="250" value="{$trg.NM_DOS_TRAINING}" /></td>
	</tr>
	<tr class="collapse">
		<td class="labelrow">Peran&nbsp;:&nbsp;</td>
		<td class="inputrow"><input type="text" name="peran" style="width:600px;" maxlength="250" value="{$trg.PERAN_DOS_TRAINING}" /></td>
	</tr>
	<tr class="collapse">
		<td class="labelrow">Instansi&nbsp;:&nbsp;</td>
		<td class="inputrow"><input type="text" name="instansi" style="width:600px;" maxlength="250" value="{$trg.INSTANSI_DOS_TRAINING}" /></td>
	</tr>
	<tr class="collapse">
		<td class="labelrow"></td>
		<td class="inputrow"><input type="submit" name="submit" value="Simpan" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('trg{$trg.ID_DOSEN_TRAINING}')" /></td>
	</tr>
	</tbody>
	</table>
	</form>
	</div>
		</td>
	</tr>
	{foreachelse}
        <tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	<tr>
		<td colspan="7" style="text-align: right;"><a onclick="javascript:toggletrg('tambahtrg', 'trgbtn');" onMouseOver="this.style.cursor='pointer'" id="trgbtn"><img src="includes/images/add.png" alt="Tambah" title="Tambah"></a></td>
	</tr>
</table>
<div id="tambahtrg" style="display: none;">
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="7" width="850"><center><strong>INPUT TRAINING/WORKSHOP/SEMINAR DOSEN - {$dsn.NM_PENGGUNA}</strong></center></th></tr>
	<tr><td colspan="7" width="850">
	<iframe scrolling="no" src="insert_trg.php?id={$dsn.ID_DOSEN}" width="100%" frameborder="0"></iframe>
	</td></tr>
</table>
</div>

{* riwayat kegiatan profesional *}
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="6" width="850"><center><strong>KEGIATAN PROFESIONAL</strong></center></th></tr>
	<tr>
		<th><center><strong>Tingkat</strong></center></th>
		<th><center><strong>Nama Kegiatan</strong></center></th>
		<th><center><strong>Instansi</strong></center></th>
		<th><center><strong>Peran</strong></center></th>
		<th width="50"><center><strong>Tahun</strong></center></th>
		<th width="50"><center><strong>Aksi</strong></center></th>
	</tr>
	{foreach item="pro" from=$PROF}
	<tr>
		<td>{$pro.TINGKAT_DOS_PROF}</td>
		<td>{$pro.KEGIATAN_DOS_PROF}</td>
		<td>{$pro.INSTANSI_DOS_PROF}</td>
		<td>{$pro.PERAN_DOS_PROF}</td>
		<td><center>{$pro.THN_DOS_PROF}</center></td>
		<td>
			<center>
				<span onMouseOver="this.style.cursor='pointer'">
					<img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('pro{$pro.ID_DOSEN_PROFESIONAL}')" />&nbsp;
					<img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_pro" id="hapus_pro{$pro.ID_DOSEN_PROFESIONAL}" onclick="javascript:if(confirm('Proses penghapusan adalah permanen. Lanjutkan?'))prohapus({$pro.ID_DOSEN_PROFESIONAL});" />
				</span>
			</center>
		</td>
	</tr>
	<tr class="edit">
		<td colspan="7" id="hidepro{$pro.ID_DOSEN_PROFESIONAL}" style="display:none;">
	<div id="pro{$pro.ID_DOSEN_PROFESIONAL}" style="display:none;">
	<form action="dosen_detail.php" method="post">
	<table class="collapse">
	<tbody>
	<tr class="collapse">
		<td class="labelrow">Tingkat&nbsp;:&nbsp;</td>
		<td class="inputrow">
			<input type="hidden" name="id_pro" value="{$pro.ID_DOSEN_PROFESIONAL}" />
			<input type="hidden" name="action" value="edit_pro" />
			<select name="tingkat" id="tingkat">
				{foreach item="tk" from=$TKT}
				{html_options values=$tk output=$tk selected=$pro.TINGKAT_DOS_PROF}
				{/foreach}
            </select>
		</td>
	</tr>
	<tr class="collapse">
		<td class="labelrow">Nama Training&nbsp;:&nbsp;</td>
		<td class="inputrow"><input type="text" name="nama" style="width:600px;" maxlength="250" value="{$pro.KEGIATAN_DOS_PROF}" /></td>
	</tr>
	<tr class="collapse">
		<td class="labelrow">Peran&nbsp;:&nbsp;</td>
		<td class="inputrow"><input type="text" name="peran" style="width:600px;" maxlength="250" value="{$pro.PERAN_DOS_PROF}" /></td>
	</tr>
	<tr class="collapse">
		<td class="labelrow">Instansi&nbsp;:&nbsp;</td>
		<td class="inputrow"><input type="text" name="instansi" style="width:600px;" maxlength="250" value="{$pro.INSTANSI_DOS_PROF}" /></td>
	</tr>
	<tr class="collapse">
		<td class="labelrow">Tahun&nbsp;:&nbsp;</td>
		<td class="inputrow">{html_select_date prefix='thn' time=$pro.THN start_year='-60' end_year='+0' display_months=false display_days=false reverse_years=true}
			&nbsp;&nbsp;&nbsp;<input type="submit" name="submit" value="Simpan" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('pro{$pro.ID_DOSEN_PROFESIONAL}')" />
		</td>
	</tr>
	</tbody>
	</table>
	</form>
	</div>
		</td>
	</tr>
	{foreachelse}
        <tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	<tr>
		<td colspan="6" style="text-align: right;"><a onclick="javascript:toggletrg('tambahpro', 'probtn');" onMouseOver="this.style.cursor='pointer'" id="probtn"><img src="includes/images/add.png" alt="Tambah" title="Tambah"></a></td>
	</tr>
</table>
<div id="tambahpro" style="display: none;">
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="6" width="850"><center><strong>INPUT KEGIATAN PROFESIONAL - {$dsn.NM_PENGGUNA}</strong></center></th></tr>
	<tr><td colspan="6" width="850">
	<iframe scrolling="no" src="insert_pro.php?id={$dsn.ID_DOSEN}" width="100%" frameborder="0"></iframe>
	</td></tr>
</table>
</div>

<p align="center">
<span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/up.png" alt="Kembali ke atas" title="Kembali ke atas" onclick="window.scrollTo(0,0); return false" /></span>
</p>
<p><br/>&nbsp;</p>