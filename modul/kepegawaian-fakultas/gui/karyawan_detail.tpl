{literal}
    <script type="text/javascript" src="js/karyawan_edit.js"></script>
    <script language="javascript" type="text/javascript">

        var popupWindow = null;
        function popup(mypage, myname, w, h, pos, infocus) {
            if (pos == "random")
            {
                LeftPosition = (screen.width) ? Math.floor(Math.random() * (screen.width - w)) : 100;
                TopPosition = (screen.height) ? Math.floor(Math.random() * ((screen.height - h) - 75)) : 100;
            }
            else
            {
                LeftPosition = (screen.width) ? (screen.width - w) / 2 : 100;
                TopPosition = (screen.height) ? (screen.height - h) / 2 : 100;
            }
            settings = "width=" + w + ",height=" + h + ",top=" + TopPosition + ",left=" + LeftPosition + ",scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no";
            popupWindow = window.open("", myname, settings);
            if (infocus == "front") {
                popupWindow.focus();
                popupWindow.location = mypage;
            }
            if (infocus == "back") {
                popupWindow.blur();
                popupWindow.location = mypage;
                popupWindow.blur();
            }
        }
        function toggle(element, button) {
            var ele = document.getElementById(element);
            var imageEle = document.getElementById(button);
            if (ele.style.display == "block") {
                ele.style.display = "none";
                imageEle.innerHTML = '<img src="includes/images/add.png" alt="Tambah" title="Tambah">';
            }
            else {
                ele.style.display = "block";
                imageEle.innerHTML = '<img src="includes/images/cancel.png" alt="Batal" title="Batal">';
            }
        }

        function hapus(mode, hapus) {
            $.ajax({
                type: "POST",
                url: "karyawan_detail.php",
                data: "action=delete_" + mode + "&hapus=" + hapus,
                cache: false,
                success: function(data) {
                    if (data == 1)
                    {
                        window.location.reload(true);
                    }
                    else
                    {
                        alert('gagal hapus');
                    }
                }
            });
        }

        function valid(mode, valid) {
            $.ajax({
                type: "POST",
                url: "karyawan_detail.php",
                data: "action=update_status_" + mode + "&valid=" + valid,
                cache: false,
                success: function(data) {
                    if (data == 1)
                    {
                        window.location.reload(true);
                    }
                    else
                    {
                        alert('gagal update');
                    }
                }
            });
        }

        function edit(itemID) {
            if ((document.getElementById(itemID).style.display == 'none'))
            {
                document.getElementById(itemID).style.display = '';
                document.getElementById('hide' + itemID).style.display = '';
                document.getElementById('row' + itemID).style.display = 'none';
            } else {
                document.getElementById(itemID).style.display = 'none';
                document.getElementById('hide' + itemID).style.display = 'none';
                document.getElementById('row' + itemID).style.display = '';
            }
        }
    </script>
{/literal}

<div class="center_title_bar">Detail Data Karyawan</div>
{* biodata karwayan *}
<table  style="width: 98%"  class="tablesorter" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <th width="840" style="text-align: center"><strong>BIODATA</strong></th>
        <th width="10" style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/left.png" alt="Kembali" title="Kembali" onclick="javascript:history.go(-1);" /></span></th>
    </tr>
</table>
<table style="width: 98%"  class="tablesorter" cellspacing="0" cellpadding="0" border="0">
    {foreach item="peg" from=$PEGAWAI}
        <tr>
            <td>NIP/NIK</td>
            <td style="text-align: center">:</td>
            <td>{$peg.USERNAME}</td>
            <td rowspan="10"  style="text-align: center"><img src="{$PHOTO}" border="0" width="160" /><br/><br/><input type="button" name="ganti_photo" value="Ganti Photo" onclick="javascript:popup('{$IMG}', 'name', '600', '400', 'center', 'front')"></td>
        </tr>
        <tr>
            <td>NIP/NIK LAMA</td>
            <td>:</td>
            {if $peg.NIP_LAMA!=''}
                <td>{$peg.NIP_LAMA}</td>
            {else}
                <td>-</td>
            {/if}		
        </tr>
        <tr>
            <td>Unit Kerja</td>
            <td style="text-align: center">:</td>
            <td>{$peg.NM_UNIT_KERJA}</td>
        </tr>
        <tr>
            <td>Status Kepegawaian</td>
            <td style="text-align: center">:</td>
            <td>{$peg.STATUS_PEGAWAI} ( {$peg.NM_GOLONGAN} - {$peg.NM_PANGKAT} )</td>
        </tr>
        <tr>
            <td>Prajabatan Nomor</td>
            <td>:</td>
            {if $peg.PRAJAB_NOMOR!=''}
                <td>{$peg.PRAJAB_NOMOR}</td>
            {else}
                <td>-</td>
            {/if}		
        </tr>
        <tr>
            <td>Prajabatan Tanggal</td>
            <td>:</td>
            {if $peg.PRAJAB_TANGGAL!=''}
                <td>{$peg.PRAJAB_TANGGAL}</td>
            {else}
                <td>-</td>
            {/if}		
        </tr>
        <tr>
            <td>Tanggal Sumpah PNS</td>
            <td>:</td>
            {if $peg.TGL_SUMPAH_PNS!=''}
                <td>{$peg.TGL_SUMPAH_PNS}</td>
            {else}
                <td>-</td>
            {/if}		
        </tr>
        <tr>
            <td>TMT CPNS</td>
            <td>:</td>
            {if $peg.TMT_CPNS!=''}
                <td>{$peg.TMT_CPNS}</td>
            {else}
                <td>-</td>
            {/if}		
        </tr>
        <tr>
            <td>Nomor Karpeg</td>
            <td>:</td>
            {if $peg.NOMER_KARPEG!=''}
                <td>{$peg.NOMER_KARPEG}</td>
            {else}
                <td>-</td>
            {/if}		
        </tr>
        <tr>
            <td>Nomor NPWP</td>
            <td>:</td>
            {if $peg.NOMOR_NPWP!=''}
                <td>{$peg.NOMOR_NPWP}</td>
            {else}
                <td>-</td>
            {/if}		
        </tr>
        <tr>
            <td>Taspen</td>
            <td>:</td>
            {if $peg.TASPEN!=''}
                <td colspan="2">{if $peg.TASPEN==1} Sudah {else if $peg.TASPEN==0} Belum{/if}</td>
            {else}
                <td colspan="2">-</td>
            {/if}		
        </tr>
        <tr>
            <td>Unit Esselon I</td>
            <td>:</td>
            {if $peg.UNIT_ESSELON_I!=''}
                <td colspan="2">{$peg.UNIT_ESSELON_I}</td>
            {else}
                <td colspan="2">-</td>
            {/if}		
        </tr>
        <tr>
            <td>Unit Esselon II</td>
            <td>:</td>
            {if $peg.UNIT_ESSELON_II!=''}
                <td colspan="2">{$peg.UNIT_ESSELON_II}</td>
            {else}
                <td colspan="2">-</td>
            {/if}		
        </tr>
        <tr>
            <td>Unit Esselon III</td>
            <td>:</td>
            {if $peg.UNIT_ESSELON_III!=''}
                <td colspan="2">{$peg.UNIT_ESSELON_III}</td>
            {else}
                <td colspan="2">-</td>
            {/if}		
        </tr>
        <tr>
            <td>Unit Esselon IV</td>
            <td>:</td>
            {if $peg.UNIT_ESSELON_IV!=''}
                <td colspan="2">{$peg.UNIT_ESSELON_IV}</td>
            {else}
                <td colspan="2">-</td>
            {/if}		
        </tr>
        <tr>
            <td>Unit Esselon V</td>
            <td>:</td>
            {if $peg.UNIT_ESSELON_IV!=''}
                <td colspan="2">{$peg.UNIT_ESSELON_IV}</td>
            {else}
                <td colspan="2">-</td>
            {/if}		
        </tr>
        <tr>
            <td>Pangkat (Gol.) Terakhir / TMT</td>
            <td>:</td>
            <td colspan="2">{$peg.NM_GOLONGAN} - {$peg.NM_PANGKAT}  / {$peg.TMT_GOLONGAN}</td>
        </tr>
        <tr>
            <td>Jabatan Fungsional / TMT </td>
            <td>:</td>
            <td colspan="2">{$peg.NM_JABATAN_FUNGSIONAL}  / {$peg.TMT_JAB_FUNGSIONAL} </td>
        </tr>
        <tr>
            <td>Tugas Tambahan / TMT</td>
            <td>:</td>
            {if $peg.NM_JABATAN_STRUKTURAL != null}
                <td colspan="2">{$peg.NM_JABATAN_STRUKTURAL} ({$peg.TGL_SK_SEJ_JAB_STRUKTURAL}-{$peg.TMT_SEJ_JAB_STRUKTURAL})</td>
            {else}
                <td colspan="2">-</td>
            {/if}	
        </tr>
        <tr>
            <td>Status Aktif</td>
            <td>:</td>
            <td colspan="2">{$peg.NM_STATUS_PENGGUNA}</td>
        </tr>
        <tr>
            <td>Pendidikan Akhir</td>
            <td>:</td>
            <td colspan="2">{if $peg.NAMA_PENDIDIKAN_AKHIR!=''}{$peg.NAMA_PENDIDIKAN_AKHIR} {$peg.NM_SEKOLAH_PENDIDIKAN} {$peg.NM_JURUSAN_PENDIDIKAN} ({$peg.TAHUN_MASUK_PENDIDIKAN}-{$peg.TAHUN_LULUS_PENDIDIKAN}){else}-{/if}</td>
        </tr>
        <tr>
            <td>Gelar Depan</td>
            <td>:</td>
            <td colspan="2">{$peg.GELAR_DEPAN}</td>
        </tr>
        <tr>
            <td>Gelar Belakang</td>
            <td>:</td>
            <td colspan="2">{$peg.GELAR_BELAKANG}</td>
        </tr>
        <tr>
            <td>Nama Lengkap</td>
            <td>:</td>
            <td colspan="2">{$peg.NM_PENGGUNA}</td>
        </tr>
        <tr>
            <td>Tempat, Tanggal Lahir</td>
            <td>:</td>
            <td colspan="2">{$peg.TEMPAT_LAHIR}, {$peg.TGL_LAHIR_PENGGUNA}</td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td>:</td>
            {if $peg.KELAMIN_PENGGUNA=='1'}
                <td colspan="2">LAKI-LAKI</td>
            {elseif $peg.KELAMIN_PENGGUNA=='2'}
                <td colspan="2">PEREMPUAN</td>
            {else}
                <td colspan="2">-</td>
            {/if}
        </tr>
        <tr>
            <td>Agama</td>
            <td>:</td>
            <td colspan="2">{$peg.NM_AGAMA}</td>
        </tr>
        <tr>
            <td>Status Pernikahan</td>
            <td>:</td>
            {if $peg.NM_STATUS_PERNIKAHAN!=''}
                <td colspan="2">{$peg.NM_STATUS_PERNIKAHAN}</td>
            {else}
                <td colspan="2">-</td>
            {/if}		
        </tr>
        <tr>
            <td>Alamat</td>
            <td>:</td>
            <td colspan="2">{$peg.ALAMAT_RUMAH_PEGAWAI}</td>
        </tr>
        <tr>
            <td>Kode Pos</td>
            <td>:</td>
            <td colspan="2">{$peg.KODE_POST}</td>
        </tr>
        <tr>
            <td>Telepon/HP</td>
            <td>:</td>
            {if $peg.TLP_PEGAWAI == null}
                <td colspan="2">{$peg.MOBILE_PEGAWAI}</td>
            {else if $peg.MOBILE_PEGAWAI == null}
                <td colspan="2">{$peg.TLP_PEGAWAI}</td>
            {else}
                <td colspan="2">{$peg.TLP_PEGAWAI} / {$peg.MOBILE_PEGAWAI} {if $peg.MOBILE_PEGAWAI != ''}<span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/sms.png" alt="Kirim SMS" title="Kirim SMS" onclick="javascript:popup('{$LINK}', 'name', '600', '400', 'center', 'front');" /></span>{else}{/if}</td>
                    {/if}
        </tr>
        <tr>
            <td>Email #1</td>
            <td>:</td>
            <td colspan="2">{$peg.EMAIL_PENGGUNA}</td>
        </tr>
        <tr>
            <td>Email #2</td>
            <td>:</td>
            <td colspan="2">{$peg.EMAIL_ALTERNATE}</td>
        </tr>
    {/foreach}
    <tr>
        <td colspan="4" style="text-align: right;width: 850"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/edit.png" alt="Edit" title="Edit" onclick="window.location.href = '#data_karyawan!karyawan_edit_biodata.php?action=edit_biodata&id={$peg.ID_PEGAWAI}';" /></span></td>
    </tr>
</table>


{*KELUARGA DIRI*}

<table style="width: 98%" class="tablesorter" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <th colspan="8" ><strong>KELUARGA DIRI</strong></th>
    </tr>
    <tr>
        <th style="text-align: center"><strong>Nama</strong></th>
        <th width="50" style="text-align: center"><strong>Hubungan</strong></th>
        <th style="text-align: center"><strong>Pekerjaan</strong></th>
        <th style="text-align: center"><strong>TTL</strong></th>
        <th style="text-align: center"><strong>Kondisi</strong></th>
        <th width="50" style="text-align: center"><strong>Status</strong></th>
        <th width="50" style="text-align: center"><strong>Aksi</strong></th>
    </tr>
    {foreach item="d" from=$KLGDR}
        <tr id="rowpdd{$d.ID_KELUARGA_DIRI}">
            <td>{$d.NAMA_KELUARGA} {if $d.KELAMIN_KELUARGA=='1'}Laki Laki{else if $d.KELAMIN_KELUARGA=='2'}Perempuan{/if}</td>
            <td>
                {if $d.HUBUNGAN_KELUARGA==1}
                    Ayah
                {else if $d.HUBUNGAN_KELUARGA==2}
                    Ibu
                {else if $d.HUBUNGAN_KELUARGA==3}
                    Kakak
                {else if $d.HUBUNGAN_KELUARGA==4}
                    Adik
                {/if}
            </td>
            <td>{$d.PEKERJAAN_KELUARGA}</td>
            <td>{$d.KOTA_LAHIR} {$d.TGL_LAHIR_KELUARGA} </td>
            <td>
                {if $d.KONDISI==1}
                    Sudah Meninggal
                {else if $d.KONDISI==2}
                    Masih Hidup
                {/if}
            </td>
            {if $d.STATUS_VALID==1}
                <td style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/success.png" alt="Ubah menjadi belum valid" title="Ubah menjadi belum valid" name="id_klgdr" id="id_klgdr{$d.ID_KELUARGA_DIRI}" onclick="javascript:valid('klgdr',{$d.ID_KELUARGA_DIRI});" /></span></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('klgdr{$d.ID_KELUARGA_DIRI}')" />
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_keluarga_diri" id="hapus_pendidikan{$d.ID_KELUARGA_DIRI}" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    hapus('klgdr',{$d.ID_KELUARGA_DIRI});" />
                    </span>

                </td>
            {else}
                <td style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/forbidden.png" alt="Ubah Menjadi Valid" title="Ubah Menjadi Valid" name="id_klgdr" id="id_klgdr{$d.ID_KELUARGA_DIRI}" onclick="javascript:valid('klgdr',{$d.ID_KELUARGA_DIRI});" /></span></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('klgdr{$d.ID_KELUARGA_DIRI}')" />&nbsp;
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_keluarga_diri" id="hapus_pendidikan{$d.ID_KELUARGA_DIRI}" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    hapus('klgdr',{$d.ID_KELUARGA_DIRI});" />
                    </span>

                </td>
            {/if}
        </tr>
        <tr class="edit">
            <td colspan="8" id="hideklgdr{$d.ID_KELUARGA_DIRI}" style="display:none;">
                <div id="klgdr{$d.ID_KELUARGA_DIRI}" style="display:none;">
                    <form name="klgdredit" id="klgdredit" method="post" action="karyawan_detail.php" onsubmit="return validate_form();">
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tr class="collapse">
                                <td class="labelrow">Nama Keluarga&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="nama" style="width:600px;" maxlength="50" value="{$d.NAMA_KELUARGA}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Hubungan Keluarga&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select name="hubungan">
                                        <option {if $d.HUBUNGAN_KELUARGA==1}selected="true"{/if} value="1">Ayah</option>
                                        <option {if $d.HUBUNGAN_KELUARGA==2}selected="true"{/if}  value="2">Ibu</option>
                                        <option {if $d.HUBUNGAN_KELUARGA==3}selected="true"{/if}  value="3">Kakak</option>
                                        <option {if $d.HUBUNGAN_KELUARGA==4}selected="true"{/if}  value="4">Adik</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Jenis Kelamin&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select name="kelamin">
                                        <option {if $d.KELAMIN_KELUARGA=='1'}selected="true"{/if} value="1">Laki-Laki</option>
                                        <option {if $d.KELAMIN_KELUARGA=='2'}selected="true"{/if}  value="2">Perempuan</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Pekerjaan&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="pekerjaan" style="width:600px;" maxlength="50" value="{$d.PEKERJAAN_KELUARGA}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Tanggal Lahir&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="tgl_lahir" style="width:120px; text-align:center;" id="tgl_lahir" style="text-align:center;" onclick="javascript:NewCssCal('tgl_lahir', 'ddmmyyyy', '', '', '', '', 'past')" value="{$d.TGL_LAHIR_KELUARGA}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Tempat Lahir &nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select class="negara" form-name="klgdr" id="negaraklgdr{$d.ID_KELUARGA_DIRI}" name="negara">
                                        <option value="">Pilih Negara</option>
                                        {foreach $negara as $n}
                                            <option value="{$n.ID_NEGARA}">{$n.NM_NEGARA}</option>
                                        {/foreach}
                                    </select>
                                    <select class="propinsi" form-name="klgdr" id="propinsiklgdr{$d.ID_KELUARGA_DIRI}" name="propinsi">
                                        <option value="">Pilih Propinsi</option>
                                    </select>
                                    <select id="kotaklgdr{$d.ID_KELUARGA_DIRI}" name="tmpat_lahir">
                                        <option value="1">Pilih Kota</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Kondisi Keluarga&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select name="kondisi">
                                        <option {if $d.KONDISI==1}selected="true"{/if} value="1">Sudah Meninggal</option>
                                        <option {if $d.KONDISI==2}selected="true"{/if} value="2">Masih Hidup</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">
                                    <input type="hidden" name="id_klgdr" value="{$d.ID_KELUARGA_DIRI}" />
                                    <input type="hidden" name="action" value="edit_klgdr" />
                                </td>
                                <td class="inputrow"><input type="submit" name="submit" value="Update" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('klgdr{$d.ID_KELUARGA_DIRI}')" /></td>
                            </tr>
                        </table>
                    </form>
                    {literal}
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $('.negara').change(function() {
                                    var form_name = $(this).attr('form-name');
                                    var id = $(this).attr('id').replace("negara" + form_name, "");
                                    $.ajax({
                                        type: "POST",
                                        url: "getNegaraPropinsi.php",
                                        data: {id: $('#negara' + form_name + id).val()},
                                        success: function(data) {
                                            $('#propinsi' + form_name + id).html(data);
                                        }
                                    })
                                });
                                $('.propinsi').change(function() {
                                    var form_name = $(this).attr('form-name');
                                    var id = $(this).attr('id').replace("propinsi" + form_name, "");
                                    $.ajax({
                                        type: "POST",
                                        url: "getPropinsiKota.php",
                                        data: {id: $('#propinsi' + form_name + id).val()},
                                        success: function(data) {
                                            $('#kota' + form_name + id).html(data);
                                        }
                                    })
                                });
                            });
                        </script>
                    {/literal}
                </div>
            </td>
        </tr>
    {foreachelse}
        <tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
    {/foreach}
    <tr id="tambahklgdr_btn">
        <td colspan="8" style="text-align: right;"><a onclick="javascript:toggle('tambahklgdr', 'klgdrbtn');" onMouseOver="this.style.cursor = 'pointer'" id="klgdrbtn"><img src="includes/images/add.png" alt="Tambah" title="Tambah"></a></td>
    </tr>
</table>
<div id="tambahklgdr" style="display: none;">
    <table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
        <tr><th colspan="8" width="850" style="text-align: center"><strong>INPUT DATA KELUARGA - {$peg.NM_PENGGUNA}</strong></th></tr>
        <tr><td colspan="8" width="850">
                <iframe height="300" scrolling="no" src="insert_klgdr.php?id={$peg.ID_PENGGUNA}" width="100%" frameborder="0"></iframe>
            </td></tr>
    </table>
</div>    


{*KELUARGA PASANGAN*}

<table style="width: 98%" class="tablesorter" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <th colspan="8" ><strong>KELUARGA PASANGAN</strong></th>
    </tr>
    <tr>
        <th style="text-align: center"><strong>Nama</strong></th>
        <th width="50" style="text-align: center"><strong>Hubungan</strong></th>
        <th style="text-align: center"><strong>Pekerjaan</strong></th>
        <th style="text-align: center"><strong>TTL</strong></th>
        <th style="text-align: center"><strong>Kondisi</strong></th>
        <th width="50" style="text-align: center"><strong>Status</strong></th>
        <th width="50" style="text-align: center"><strong>Aksi</strong></th>
    </tr>
    {foreach item="d" from=$KLGPSG}
        <tr id="rowpdd{$d.ID_KELUARGA_PASANGAN}">
            <td>{$d.NAMA_PASANGAN} {if $d.KELAMIN_PASANGAN=='1'}Laki Laki{else if $d.KELAMIN_PASANGAN=='2'}Perempuan{/if}</td>
            <td>
                {if $d.HUBUNGAN_PASANGAN==1}
                    Ayah
                {else if $d.HUBUNGAN_PASANGAN==2}
                    Ibu
                {else if $d.HUBUNGAN_PASANGAN==3}
                    Kakak
                {else if $d.HUBUNGAN_PASANGAN==4}
                    Adik
                {/if}
            </td>
            <td>{$d.PEKERJAAN_PASANGAN}</td>
            <td>{$d.KOTA_LAHIR} {$d.TGL_LAHIR_PASANGAN} </td>
            <td>
                {if $d.KONDISI==1}
                    Sudah Meninggal
                {else if $d.KONDISI==2}
                    Masih Hidup
                {/if}
            </td>
            {if $d.STATUS_VALID==1}
                <td style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/success.png" alt="Ubah menjadi belum valid" title="Ubah menjadi belum valid" name="id_klgpsg" id="id_klgpsg{$d.ID_KELUARGA_PASANGAN}" onclick="javascript:valid('klgpsg',{$d.ID_KELUARGA_PASANGAN});" /></span></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('klgpsg{$d.ID_KELUARGA_PASANGAN}')" />
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_keluarga_diri" id="hapus_pendidikan{$d.ID_KELUARGA_PASANGAN}" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    hapus('klgpsg',{$d.ID_KELUARGA_PASANGAN});" />
                    </span>

                </td>
            {else}
                <td style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/forbidden.png" alt="Ubah Menjadi Valid" title="Ubah Menjadi Valid" name="id_klgpsg" id="id_klgdr{$d.ID_KELUARGA_PASANGAN}" onclick="javascript:valid('klgpsg',{$d.ID_KELUARGA_PASANGAN});" /></span></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('klgpsg{$d.ID_KELUARGA_PASANGAN}')" />&nbsp;
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_keluarga_diri" id="hapus_pendidikan{$d.ID_KELUARGA_PASANGAN}" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    hapus('klgpsg',{$d.ID_KELUARGA_PASANGAN});" />
                    </span>

                </td>
            {/if}
        </tr>
        <tr class="edit">
            <td colspan="8" id="hideklgpsg{$d.ID_KELUARGA_PASANGAN}" style="display:none;">
                <div id="klgpsg{$d.ID_KELUARGA_PASANGAN}" style="display:none;">
                    <form name="klgpsgedit" id="klgdredit" method="post" action="karyawan_detail.php" onsubmit="return validate_form();">
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tr class="collapse">
                                <td class="labelrow">Nama Keluarga&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="nama" style="width:600px;" maxlength="50" value="{$d.NAMA_PASANGAN}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Hubungan Keluarga&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select name="hubungan">
                                        <option {if $d.HUBUNGAN_PASANGAN==1}selected="true"{/if} value="1">Ayah</option>
                                        <option {if $d.HUBUNGAN_PASANGAN==2}selected="true"{/if}  value="2">Ibu</option>
                                        <option {if $d.HUBUNGAN_PASANGAN==3}selected="true"{/if}  value="3">Kakak</option>
                                        <option {if $d.HUBUNGAN_PASANGAN==4}selected="true"{/if}  value="4">Adik</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Jenis Kelamin&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select name="kelamin">
                                        <option {if $d.KELAMIN_PASANGAN=='1'}selected="true"{/if} value="1">Laki-Laki</option>
                                        <option {if $d.KELAMIN_PASANGAN=='2'}selected="true"{/if}  value="2">Perempuan</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Pekerjaan&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="pekerjaan" style="width:600px;" maxlength="50" value="{$d.PEKERJAAN_PASANGAN}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Tanggal Lahir&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="tgl_lahir" style="width:120px; text-align:center;" id="tgl_lahir{$d.ID_KELUARGA_PASANGAN}" style="text-align:center;" onclick="javascript:NewCssCal('tgl_lahir{$d.ID_KELUARGA_PASANGAN}', 'ddmmyyyy', '', '', '', '', 'past')" value="{$d.TGL_LAHIR}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Tempat Lahir &nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select class="negara" form-name="klgpsg" id="negaraklgpsg{$d.ID_KELUARGA_PASANGAN}" name="negara">
                                        <option value="">Pilih Negara</option>
                                        {foreach $negara as $n}
                                            <option value="{$n.ID_NEGARA}">{$n.NM_NEGARA}</option>
                                        {/foreach}
                                    </select>
                                    <select class="propinsi" form-name="klgpsg" id="propinsiklgpsg{$d.ID_KELUARGA_PASANGAN}" name="propinsi">
                                        <option value="">Pilih Propinsi</option>
                                    </select>
                                    <select id="kotaklgpsg{$d.ID_KELUARGA_PASANGAN}" name="tmpat_lahir">
                                        <option value="1">Pilih Kota</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Kondisi Keluarga&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select name="kondisi">
                                        <option {if $d.KONDISI==1}selected="true"{/if} value="1">Sudah Meninggal</option>
                                        <option {if $d.KONDISI==2}selected="true"{/if} value="2">Masih Hidup</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">
                                    <input type="hidden" name="id_klgpsg" value="{$d.ID_KELUARGA_PASANGAN}" />
                                    <input type="hidden" name="action" value="edit_klgpsg" />
                                </td>
                                <td class="inputrow"><input type="submit" name="submit" value="Update" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('klgdr{$d.ID_KELUARGA_PASANGAN}')" /></td>
                            </tr>
                        </table>
                    </form>
                    {literal}
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $('.negara').change(function() {
                                    var form_name = $(this).attr('form-name');
                                    var id = $(this).attr('id').replace("negara" + form_name, "");
                                    $.ajax({
                                        type: "POST",
                                        url: "getNegaraPropinsi.php",
                                        data: {id: $('#negara' + form_name + id).val()},
                                        success: function(data) {
                                            $('#propinsi' + form_name + id).html(data);
                                        }
                                    })
                                });
                                $('.propinsi').change(function() {
                                    var form_name = $(this).attr('form-name');
                                    var id = $(this).attr('id').replace("propinsi" + form_name, "");
                                    $.ajax({
                                        type: "POST",
                                        url: "getPropinsiKota.php",
                                        data: {id: $('#propinsi' + form_name + id).val()},
                                        success: function(data) {
                                            $('#kota' + form_name + id).html(data);
                                        }
                                    })
                                });
                            });
                        </script>
                    {/literal}
                </div>
            </td>
        </tr>
    {foreachelse}
        <tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
    {/foreach}
    <tr id="tambahklgpsg_btn">
        <td colspan="8" style="text-align: right;"><a onclick="javascript:toggle('tambahklgpsg', 'klgpsgbtn');" onMouseOver="this.style.cursor = 'pointer'" id="klgpsgbtn"><img src="includes/images/add.png" alt="Tambah" title="Tambah"></a></td>
    </tr>
</table>
<div id="tambahklgpsg" style="display: none;">
    <table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
        <tr><th colspan="8" width="850" style="text-align: center"><strong>INPUT DATA KELUARGA PASANGAN - {$peg.NM_PENGGUNA}</strong></th></tr>
        <tr><td colspan="8" width="850">
                <iframe height="300" scrolling="no" src="insert_klgpsg.php?id={$peg.ID_PENGGUNA}" width="100%" frameborder="0"></iframe>
            </td></tr>
    </table>
</div> 


{*KELUARGA ANAK*}

<table style="width: 98%" class="tablesorter" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <th colspan="8" ><strong>KELUARGA ANAK</strong></th>
    </tr>
    <tr>
        <th style="text-align: center"><strong>Nama</strong></th>
        <th style="text-align: center"><strong>TTL</strong></th>
        <th style="text-align: center"><strong>Pendidikan</strong></th>
        <th width="50" style="text-align: center"><strong>Status Anak</strong></th>
        <th style="text-align: center"><strong>Pekerjaan</strong></th>
        <th width="50" style="text-align: center"><strong>Aksi</strong></th>
    </tr>
    {foreach item="d" from=$KLGANK}
        <tr id="rowpdd{$d.ID_KELUARGA_ANAK}">
            <td>{$d.NAMA_ANAK} {if $d.KELAMIN_ANAK=='1'}Laki Laki{else if $d.KELAMIN_ANAK=='2'}Perempuan{/if}</td>
            <td>{$d.KOTA_LAHIR} {$d.TGL_LAHIR} </td>
            <td>{$d.NAMA_PENDIDIKAN_AKHIR}</td>
            <td>
                {if $d.STATUS_ANAK==1}
                    Anak Kandung
                {else if $d.STATUS_ANAK==2}
                    Anak Tiri
                {else if $d.STATUS_ANAK==3}
                    Anak Angkat
                {/if}
            </td>
            <td>{$d.PEKERJAAN_ANAK}</td>
            <td style="text-align: center">
                <span onMouseOver="this.style.cursor = 'pointer'">
                    <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('klgank{$d.ID_KELUARGA_ANAK}')" />
                    <img src="includes/images/delete.png" alt="Hapus" title="Hapus" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                hapus('klgank',{$d.ID_KELUARGA_ANAK});" />
                </span>

            </td>
        </tr>
        <tr class="edit">
            <td colspan="8" id="hideklgank{$d.ID_KELUARGA_ANAK}" style="display:none;">
                <div id="klgank{$d.ID_KELUARGA_ANAK}" style="display:none;">
                    <form name="klgankedit" id="klgdredit" method="post" action="karyawan_detail.php" onsubmit="return validate_form();">
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tr class="collapse">
                                <td class="labelrow">Nama Keluarga&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="nama" style="width:600px;" maxlength="50" value="{$d.NAMA_ANAK}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Jenis Kelamin&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select name="kelamin">
                                        <option {if $d.KELAMIN_ANAK=='1'}selected="true"{/if} value="1">Laki-Laki</option>
                                        <option {if $d.KELAMIN_ANAK=='2'}selected="true"{/if}  value="2">Perempuan</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Hubungan Keluarga&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select name="status">
                                        <option {if $d.STATUS_ANAK==1}selected="true"{/if} value="1">Anak Kandung</option>
                                        <option {if $d.STATUS_ANAK==2}selected="true"{/if}  value="2">Anak Tiri</option>
                                        <option {if $d.STATUS_ANAK==3}selected="true"{/if}  value="3">Anak Angkat</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Pekerjaan&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="pekerjaan" style="width:600px;" maxlength="50" value="{$d.PEKERJAAN_ANAK}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Jenjang&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select name="pendidikan" id="id_pdd">
                                        {foreach item="pdd1" from=$ID_PDD}
                                            {html_options values=$pdd1.ID_PENDIDIKAN_AKHIR output=$pdd1.NAMA_PENDIDIKAN_AKHIR selected=$d.ID_PENDIDIKAN_AKHIR}
                                        {/foreach}
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Tanggal Lahir&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="tgl_lahir" style="width:120px; text-align:center;" id="tgl_lahir" style="text-align:center;" onclick="javascript:NewCssCal('tgl_lahir', 'ddmmyyyy', '', '', '', '', 'past')" value="{$d.TGL_LAHIR}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Tempat Lahir &nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select class="negara" form-name="klgank" id="negaraklgank{$d.ID_KELUARGA_ANAK}" name="negara">
                                        <option value="">Pilih Negara</option>
                                        {foreach $negara as $n}
                                            <option value="{$n.ID_NEGARA}">{$n.NM_NEGARA}</option>
                                        {/foreach}
                                    </select>
                                    <select class="propinsi" form-name="klgank" id="propinsiklgank{$d.ID_KELUARGA_ANAK}" name="propinsi">
                                        <option value="">Pilih Propinsi</option>
                                    </select>
                                    <select id="kotaklgank{$d.ID_KELUARGA_ANAK}" name="tmpat_lahir">
                                        <option value="1">Pilih Kota</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">
                                    <input type="hidden" name="id_klgank" value="{$d.ID_KELUARGA_ANAK}" />
                                    <input type="hidden" name="action" value="edit_klgank" />
                                </td>
                                <td class="inputrow"><input type="submit" name="submit" value="Update" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('klgank{$d.ID_KELUARGA_ANAK}')" /></td>
                            </tr>
                        </table>
                    </form>
                    {literal}
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $('.negara').change(function() {
                                    var form_name = $(this).attr('form-name');
                                    var id = $(this).attr('id').replace("negara" + form_name, "");
                                    $.ajax({
                                        type: "POST",
                                        url: "getNegaraPropinsi.php",
                                        data: {id: $('#negara' + form_name + id).val()},
                                        success: function(data) {
                                            $('#propinsi' + form_name + id).html(data);
                                        }
                                    })
                                });
                                $('.propinsi').change(function() {
                                    var form_name = $(this).attr('form-name');
                                    var id = $(this).attr('id').replace("propinsi" + form_name, "");
                                    $.ajax({
                                        type: "POST",
                                        url: "getPropinsiKota.php",
                                        data: {id: $('#propinsi' + form_name + id).val()},
                                        success: function(data) {
                                            $('#kota' + form_name + id).html(data);
                                        }
                                    })
                                });
                            });
                        </script>
                    {/literal}
                </div>
            </td>
        </tr>
    {foreachelse}
        <tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
    {/foreach}
    <tr id="tambahklgank_btn">
        <td colspan="8" style="text-align: right;"><a onclick="javascript:toggle('tambahklgank', 'klgankbtn');" onMouseOver="this.style.cursor = 'pointer'" id="klgankbtn"><img src="includes/images/add.png" alt="Tambah" title="Tambah"></a></td>
    </tr>
</table>
<div id="tambahklgank" style="display: none;">
    <table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
        <tr><th colspan="8" width="850" style="text-align: center"><strong>INPUT DATA ANAK - {$peg.NM_PENGGUNA}</strong></th></tr>
        <tr><td colspan="8" width="850">
                <iframe height="300" scrolling="no" src="insert_klgank.php?id={$peg.ID_PENGGUNA}" width="100%" frameborder="0"></iframe>
            </td></tr>
    </table>
</div>    

{* SEJARAH PERNIKAHAN *}
<table style="width: 98%" class="tablesorter" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <th colspan="8" width="850"><strong>RIWAYAT PERNIKAHAN</strong></th>
    </tr>
    <tr>
        <th style="text-align: center"><strong>Nama Pasangan</strong></th>
        <th width="50" style="text-align: center"><strong>Nomor Karsis/Karsu</strong></th>
        <th style="text-align: center"><strong>Tgl Lahir</strong></th>
        <th style="text-align: center"><strong>Tgl Nikah</strong></th>
        <th style="text-align: center"><strong>Pekerjaan</strong></th>
        <th style="text-align: center"><strong>Status</strong></th>
        <th width="50" style="text-align: center"><strong>Aksi</strong></th>
    </tr>
    {foreach item="d" from=$NIKAH}
        <tr id="rowpdd{$d.ID_SEJARAH_PERNIKAHAN}">
            <td>{$d.NAMA_PASANGAN} - {if $d.STATUS_PASANGAN==1}Suami/Istri Saat Ini{else if $d.STATUS_PASANGAN==2}Sudah Meninggal{else}Cerai{/if}</td>
            <td>{$d.NOMOR_KARSISU}  </td>
            <td>{$d.TGL_LAHIR}  </td>
            <td>{$d.TGL_NIKAH}</td>
            <td>{$d.PEKERJAAN_PASANGAN}</td>
            {if $d.STATUS_VALID==1}
                <td style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/success.png" alt="Ubah menjadi belum valid" title="Ubah menjadi belum valid" onclick="javascript:valid('nikah',{$d.ID_SEJARAH_PERNIKAHAN});" /></span></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('nikah{$d.ID_SEJARAH_PERNIKAHAN}')" />
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    hapus('nikah',{$d.ID_SEJARAH_PERNIKAHAN});" />
                    </span>

                </td>
            {else}
                <td style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/forbidden.png" alt="Ubah Menjadi Valid" title="Ubah Menjadi Valid"onclick="javascript:valid('nikah',{$d.ID_SEJARAH_PERNIKAHAN});" /></span></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('nikah{$d.ID_SEJARAH_PERNIKAHAN}')" />&nbsp;
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    hapus('nikah',{$d.ID_SEJARAH_PERNIKAHAN});" />
                    </span>

                </td>
            {/if}
        <tr class="edit">
            <td colspan="8" id="hidenikah{$d.ID_SEJARAH_PERNIKAHAN}" style="display:none;">
                <div id="nikah{$d.ID_SEJARAH_PERNIKAHAN}" style="display:none;">
                    <form name="nikahedit" id="nikahedit" method="post" action="karyawan_detail.php" onsubmit="return validate_form();">
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tr class="collapse">
                                <td class="labelrow">Nama Pasangan&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="nama" style="width:600px;" maxlength="50" value="{$d.NAMA_PASANGAN}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Nomor Karsis/Karsu&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="karsisu" style="width:600px;" maxlength="50" value="{$d.NOMOR_KARSISU}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Tanggal Lahir Pasangan&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="tgl_lhr_psg" style="width:120px; text-align:center;" id="tgl_lhr_psg" style="text-align:center;" onclick="javascript:NewCssCal('tgl_lhr_psg', 'ddmmyyyy', '', '', '', '', 'past')" value="{$d.TGL_LAHIR}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Tanggal Nikah&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="tgl_nikah" style="width:120px; text-align:center;" id="tgl_nikah" style="text-align:center;" onclick="javascript:NewCssCal('tgl_lahir', 'ddmmyyyy', '', '', '', '', 'past')" value="{$d.TGL_NIKAH}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Pendidikan Pasangan&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select name="pendidikan_psg" id="id_pdd">
                                        {foreach item="pdd1" from=$ID_PDD}
                                            {html_options values=$pdd1.ID_PENDIDIKAN_AKHIR output=$pdd1.NAMA_PENDIDIKAN_AKHIR selected=$d.ID_PENDIDIKAN_PASANGAN}
                                        {/foreach}
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Pekerjaan Pasangan&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="pekerjaan_psg" style="width:600px;" maxlength="50" value="{$d.PEKERJAAN_PASANGAN}"  /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Status Pasangan&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select name="status_psg">
                                        <option {if $d.STATUS_PASANGAN==1}selected="true"{/if} value="1">Suami/Istri Saat ini</option>
                                        <option {if $d.STATUS_PASANGAN==2}selected="true"{/if} value="2">Meninggal Duni</option>
                                        <option {if $d.STATUS_PASANGAN==3}selected="true"{/if} value="3">Cerai</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">
                                    <input type="hidden" name="id_nikah" value="{$d.ID_SEJARAH_PERNIKAHAN}" />
                                    <input type="hidden" name="action" value="edit_nikah" />
                                </td>
                                <td class="inputrow"><input type="submit" name="submit" value="Update" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('klgdr{$d.ID_KELUARGA_DIRI}')" /></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </td>
        </tr>
    {foreachelse}
        <tr>
            <td colspan="8">Data tidak ditemukan
            </td>
        </tr>
    {/foreach}
    <tr id="tambahnikah_btn">
        <td colspan="8" style="text-align: right;"><a onclick="javascript:toggle('tambahnikah', 'nikah_btn');" onMouseOver="this.style.cursor = 'pointer'" id="nikah_btn"><img src="includes/images/add.png" alt="Tambah" title="Tambah"></a></td>
    </tr>
</table>
<div id="tambahnikah" style="display: none;">
    <table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
        <tr><th colspan="8" width="850" style="text-align: center"><strong>INPUT DATA PERNIKAHAN - {$peg.NM_PENGGUNA}</strong></th></tr>
        <tr><td colspan="8" width="850">
                <iframe height="300" scrolling="no" src="insert_nikah.php?id={$peg.ID_PENGGUNA}" width="100%" frameborder="0"></iframe>
            </td></tr>
    </table>
</div>   



{* RIWAYAT TUGAS TAMBAHAN *}
<table style="width: 98%" class="tablesorter" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <th colspan="7" width="850"><strong>RIWAYAT TUGAS TAMBAHAN</strong></th>
    </tr>
    <tr>
        <th style="text-align: center"><strong>Nama Jabatan</strong></th>
        <th width="50" style="text-align: center"><strong>SK Jabatan</strong></th>
        <th style="text-align: center"><strong>Asal SK</strong></th>
        <th style="text-align: center"><strong>TMT SK Jabatan</strong></th>
        <th style="text-align: center"><strong>Keterangan</strong></th>
        <th width="50" style="text-align: center"><strong>Status</strong></th>
        <th width="50" style="text-align: center"><strong>Aksi</strong></th>
    </tr>
    {foreach item="d" from=$STRUKTURAL}
        <tr id="rowpdd{$d.ID_SEJARAH_JABATAN_STRUKTURAL}">
            <td>{$d.NM_JABATAN_STRUKTURAL}  </td>
            <td>{$d.NO_SK_SEJ_JAB_STRUKTURAL} {$d.TGL_SK} </td>
            <td>{$d.ASAL_SK_SEJ_JAB_STRUKTURAL}</td>
            <td>{$d.TGL_TMT}  </td>
            <td>{$d.KET_SK_SEJ_JAB_STRUKTURAL}  </td>
            {if $d.VALID_SD==1}
                <td style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/success.png" alt="Ubah menjadi belum valid" title="Ubah menjadi belum valid" onclick="javascript:valid('tambahan',{$d.ID_SEJARAH_JABATAN_STRUKTURAL});" /></span></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('tambahan{$d.ID_SEJARAH_JABATAN_STRUKTURAL}')" />
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    hapus('tambahan',{$d.ID_SEJARAH_JABATAN_STRUKTURAL});" />
                    </span>

                </td>
            {else}
                <td style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/forbidden.png" alt="Ubah Menjadi Valid" title="Ubah Menjadi Valid"onclick="javascript:valid('tambahan',{$d.ID_SEJARAH_JABATAN_STRUKTURAL});" /></span></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('tambahan{$d.ID_SEJARAH_JABATAN_STRUKTURAL}')" />&nbsp;
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    hapus('tambahan',{$d.ID_SEJARAH_JABATAN_STRUKTURAL});" />
                    </span>

                </td>
            {/if}
        <tr class="edit">
            <td colspan="7" id="hidetambahan{$d.ID_SEJARAH_JABATAN_STRUKTURAL}" style="display:none;">
                <div id="tambahan{$d.ID_SEJARAH_JABATAN_STRUKTURAL}" style="display:none;">
                    <form name="nikahedit" id="tambahanedit" method="post" action="karyawan_detail.php" onsubmit="return validate_form();">
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tr class="collapse">
                                <td class="labelrow">Jabatan Struktural&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select name="jabatan" id="id_pdd">
                                        {foreach item="js" from=$ID_STRUK}
                                            {html_options values=$js.ID_JABATAN_STRUKTURAL output=$js.NM_JABATAN_STRUKTURAL selected=$d.ID_JABATAN_STRUKTURAL}
                                        {/foreach}
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">NO SK Jabatan Struktural&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="nomor" style="width:600px;" maxlength="50" value="{$d.NO_SK_SEJ_JAB_STRUKTURAL}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Asal SK Jabatan Struktural&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="asal" style="width:600px;" maxlength="50" value="{$d.ASAL_SK_SEJ_JAB_STRUKTURAL}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Keterangan SK&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <textarea name="keterangan">{$d.KET_SK_SEJ_JAB_STRUKTURAL}</textarea>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Tanggal SK Jabatan Struktural&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="tgl_sk" style="width:120px; text-align:center;" id="tgl_sk" style="text-align:center;" onclick="javascript:NewCssCal('tgl_sk', 'ddmmyyyy', '', '', '', '', 'past')" value="{$d.TGL_SK}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">TMT SK Jabatan Struktural&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="tgl_tmt" style="width:120px; text-align:center;" id="tgl_tmt" style="text-align:center;" onclick="javascript:NewCssCal('tgl_tmt', 'ddmmyyyy', '', '', '', '', 'past')" value="{$d.TGL_TMT}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Pejabatan TTD SK Jabatan Struktural&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="ttd" style="width:600px;" maxlength="50" value="{$d.TTD_SK_SEJ_JAB_STRUKTURAL}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">
                                    <input type="hidden" name="id_tambahan" value="{$d.ID_SEJARAH_JABATAN_STRUKTURAL}" />
                                    <input type="hidden" name="action" value="edit_tambahan" />
                                </td>
                                <td class="inputrow"><input type="submit" name="submit" value="Update" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('tambahan{$d.ID_SEJARAH_JABATAN_STRUKTURAL}')" /></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </td>
        </tr>
    {foreachelse}
        <tr>
            <td colspan="7">Data tidak ditemukan
            </td>
        </tr>
    {/foreach}
    <tr id="tambahtambahan_btn">
        <td colspan="7" style="text-align: right;"><a onclick="javascript:toggle('tambahtambahan', 'tambahan_btn');" onMouseOver="this.style.cursor = 'pointer'" id="tambahan_btn"><img src="includes/images/add.png" alt="Tambah" title="Tambah"></a></td>
    </tr>
</table>
<div id="tambahtambahan" style="display: none;">
    <table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
        <tr><th colspan="7" width="850" style="text-align: center"><strong>INPUT DATA TUGAS TAMBAHAN - {$peg.NM_PENGGUNA}</strong></th></tr>
        <tr><td colspan="7" width="850">
                <iframe height="300" scrolling="no" src="insert_tambahan.php?id={$peg.ID_PENGGUNA}" width="100%" frameborder="0"></iframe>
            </td></tr>
    </table>
</div>   


{* riwayat golongan *}
<table style="width: 98%"  class="tablesorter" cellspacing="0" cellpadding="0" border="0">
    <tr><th colspan="8" width="850" style="text-align: center"><strong>RIWAYAT GOLONGAN</strong></th></tr>
    <tr>
        <th width="80" style="text-align: center"><strong>Golongan</strong></th>
        <th width="160" style="text-align: center"><strong>Surat Keputusan</strong></th>
        <th width="50" style="text-align: center"><strong>File</strong></th>
        <th width="190" style="text-align: center"><strong>Asal SK</strong></th>
        <th width="190" style="text-align: center"><strong>Keterangan</strong></th>
        <th width="80" style="text-align: center"><strong>TMT</strong></th>
        <th width="50" style="text-align: center"><strong>Status</strong></th>
        <th width="50" style="text-align: center"><strong>Aksi</strong></th>
    </tr>
    {foreach item="gol" from=$GOL}
        <tr id="rowgol{$gol.ID_SEJARAH_GOLONGAN}">
            <td style="text-align: center">{$gol.NM_GOLONGAN}</td>
            <td>{$gol.NO_SK_SEJARAH_GOLONGAN}</td>
            {if $gol.FILES == ''}
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/export.png" alt="Upload PDF" title="Upload PDF" onclick="javascript:popup('proses/upload_pdf.php?id={$smarty.get.id}&kode={$gol.ID_SEJARAH_GOLONGAN}&tahun={$gol.TAHUN}&jenis={$gol.KATEGORI}&doc=GOL', 'name', '600', '400', 'center', 'front');" />
                    </span>

                </td>
            {else}
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/pdf.png" alt="PDF" title="PDF" onclick="javascript:window.open('{$gol.FILES}');" />&nbsp;
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_file_golongan" id="hapus_file_golongan{$gol.ID_UPLOAD_FILE}" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    golfilehapus({$gol.ID_UPLOAD_FILE});" />
                    </span>

                </td>	
            {/if}
            <td>{$gol.ASAL_SK_SEJARAH_GOLONGAN}</td>
            <td>{$gol.KETERANGAN_SK_SEJARAH_GOLONGAN}</td>
            <td style="text-align: center">{$gol.TMT_SEJARAH_GOLONGAN}</td>
            {if $gol.STATUS_AKHIR == 1}
                <td style="text-align: center"><img src="includes/images/success.png" /></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('gol{$gol.ID_SEJARAH_GOLONGAN}')" />
                    </span>

                </td>
            {else}
                <td style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/forbidden.png" alt="Set sebagai Status Terakhir" title="Set sebagai Status Terakhir" name="id_golongan" id="id_golongan{$gol.ID_SEJARAH_GOLONGAN}" onclick="javascript:golsubmit({$gol.ID_SEJARAH_GOLONGAN});" /></span></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('gol{$gol.ID_SEJARAH_GOLONGAN}')" />&nbsp;
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_golongan" id="hapus_golongan{$gol.ID_SEJARAH_GOLONGAN}" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    golhapus({$gol.ID_SEJARAH_GOLONGAN});" />
                    </span>

                </td>
            {/if}
        </tr>
        <tr class="edit">
            <td colspan="8" id="hidegol{$gol.ID_SEJARAH_GOLONGAN}" style="display:none;">
                <div id="gol{$gol.ID_SEJARAH_GOLONGAN}" style="display:none;">
                    <form action="karyawan_detail.php" method="post">
                        <table class="collapse">
                            <tbody>
                                <tr class="collapse">
                                    <td class="labelrow">Golongan&nbsp;:&nbsp;</td>
                                    <td class="inputrow">
                                        <input type="hidden" name="id_sej" value="{$gol.ID_SEJARAH_GOLONGAN}" />
                                        <input type="hidden" name="action" value="edit_gol" />
                                        <select name="id_gol" id="id_gol">
                                            {foreach item="gol1" from=$ID_GOL}
                                                {html_options values=$gol1.ID_GOLONGAN output=$gol1.NM_GOLONGAN selected=$gol.ID_GOLONGAN}
                                            {/foreach}
                                        </select>
                                    </td>
                                </tr>
                                <tr class="collapse">
                                    <td class="labelrow">No. SK&nbsp;:&nbsp;</td>
                                    <td class="inputrow"><input type="text" name="no_sk" style="width:600px;" maxlength="50" value="{$gol.NO_SK_SEJARAH_GOLONGAN}" /></td>
                                </tr>
                                <tr class="collapse">
                                    <td class="labelrow">Asal SK&nbsp;:&nbsp;</td>
                                    <td class="inputrow"><input type="text" name="asal_sk" style="width:600px;" maxlength="50" value="{$gol.ASAL_SK_SEJARAH_GOLONGAN}" /></td>
                                </tr>
                                <tr class="collapse">
                                    <td class="labelrow">Keterangan&nbsp;:&nbsp;</td>
                                    <td class="inputrow"><input type="text" name="ket_sk" style="width:600px;" maxlength="50" value="{$gol.KETERANGAN_SK_SEJARAH_GOLONGAN}" /></td>
                                </tr>
                                <tr class="collapse">
                                    <td class="labelrow">TMT&nbsp;:&nbsp;</td>
                                    <td class="inputrow"><input type="text" name="tmt_sej" style="width:120px; text-align:center;" id="tmt_sej" style="text-align:center;" onclick="javascript:NewCssCal('tmt_sej', 'ddmmyyyy', '', '', '', '', 'past')" value="{$gol.TMT_SEJARAH_GOLONGAN}" /></td>
                                </tr>
                                <tr class="collapse">
                                    <td class="labelrow"></td>
                                    <td class="inputrow"><input type="submit" name="submit" value="Simpan" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('gol{$gol.ID_SEJARAH_GOLONGAN}')" /></td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </td>
        </tr>
    {foreachelse}
        <tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
    {/foreach}
    <tr id="tambahgol_btn">
        <td colspan="8" style="text-align: right;"><a onclick="javascript:togglegol('tambahgol', 'golbtn');" onMouseOver="this.style.cursor = 'pointer'" id="golbtn"><img src="includes/images/add.png" alt="Tambah" title="Tambah"></a></td>
    </tr>
</table>
<div id="tambahgol" style="display: none;">
    <table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
        <tr><th colspan="8" width="850" style="text-align: center"><strong>INPUT SEJARAH GOLONGAN - {$peg.NM_PENGGUNA}</strong></th></tr>
        <tr><td colspan="8" width="850">
                <iframe scrolling="no" src="insert_gol.php?id={$peg.ID_PENGGUNA}" width="100%" frameborder="0"></iframe>
            </td></tr>
    </table>
</div>

{* riwayat pendidikan *}
<table style="width: 98%" class="tablesorter" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <th colspan="8" width="850" style="text-align: center"><strong>PENDIDIKAN</strong></th>
    </tr>
    <tr>
        <th style="text-align: center"><strong>Jenjang</strong></th>
        <th width="50" style="text-align: center"><strong>File</strong></th>
        <th style="text-align: center"><strong>Nama Sekolah</strong></th>
        <th style="text-align: center"><strong>Jurusan</strong></th>
        <th style="text-align: center"><strong>Tempat</strong></th>
        <th width="50" style="text-align: center"><strong>Lulus</strong></th>
        <th width="50" style="text-align: center"><strong>Status</strong></th>
        <th width="50" style="text-align: center"><strong>Aksi</strong></th>
    </tr>
    {foreach item="pdd" from=$PEND}
        <tr id="rowpdd{$pdd.ID_SEJARAH_PENDIDIKAN}">
            <td>{$pdd.NAMA_PENDIDIKAN_AKHIR}</td>
            {if $pdd.FILES == ''}
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/export.png" alt="Upload PDF" title="Upload PDF" onclick="javascript:popup('proses/upload_pdf.php?id={$smarty.get.id}&kode={$pdd.ID_SEJARAH_PENDIDIKAN}&tahun={$pdd.TAHUN_LULUS_PENDIDIKAN}&jenis={$pdd.KATEGORI}&doc=PDD', 'name', '600', '400', 'center', 'front');" />
                    </span>

                </td>
            {else}
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/pdf.png" alt="PDF" title="PDF" onclick="javascript:window.open('{$pdd.FILES}');" />&nbsp;
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_file_pendidikan" id="hapus_file_pendidikan{$pdd.ID_UPLOAD_FILE}" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    pddfilehapus({$pdd.ID_UPLOAD_FILE});" />
                    </span>

                </td>	
            {/if}
            <td>{$pdd.NM_SEKOLAH_PENDIDIKAN}</td>
            <td>{$pdd.NM_JURUSAN_PENDIDIKAN}</td>
            <td>{$pdd.TEMPAT_PENDIDIKAN}</td>
            <td style="text-align: center">{$pdd.TAHUN_LULUS_PENDIDIKAN}</td>
            {if $pdd.STATUS_AKHIR == 1}
                <td style="text-align: center"><img src="includes/images/success.png" alt="Status Terakhir" title="Status Terakhir" /></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('pdd{$pdd.ID_SEJARAH_PENDIDIKAN}')" />
                    </span>

                </td>
            {else}
                <td style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/forbidden.png" alt="Set sebagai Status Terakhir" title="Set sebagai Status Terakhir" name="id_pendidikan" id="id_pendidikan{$pdd.ID_SEJARAH_PENDIDIKAN}" onclick="javascript:pddsubmit({$pdd.ID_SEJARAH_PENDIDIKAN});" /></span></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('pdd{$pdd.ID_SEJARAH_PENDIDIKAN}')" />&nbsp;
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_pendidikan" id="hapus_pendidikan{$pdd.ID_SEJARAH_PENDIDIKAN}" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    pddhapus({$pdd.ID_SEJARAH_PENDIDIKAN});" />
                    </span>

                </td>
            {/if}
        </tr>
        <tr class="edit">
            <td colspan="8" id="hidepdd{$pdd.ID_SEJARAH_PENDIDIKAN}" style="display:none;">
                <div id="pdd{$pdd.ID_SEJARAH_PENDIDIKAN}" style="display:none;">
                    <form action="karyawan_detail.php" method="post">
                        <table class="collapse">
                            <tbody>
                                <tr class="collapse">
                                    <td class="labelrow">Jenjang&nbsp;:&nbsp;</td>
                                    <td class="inputrow">
                                        <input type="hidden" name="id_sej" value="{$pdd.ID_SEJARAH_PENDIDIKAN}" />
                                        <input type="hidden" name="action" value="edit_pdd" />
                                        <select name="id_pdd" id="id_pdd">
                                            {foreach item="pdd1" from=$ID_PDD}
                                                {html_options values=$pdd1.ID_PENDIDIKAN_AKHIR output=$pdd1.NAMA_PENDIDIKAN_AKHIR selected=$pdd.ID_PENDIDIKAN_AKHIR}
                                            {/foreach}
                                        </select>
                                    </td>
                                </tr>
                                <tr class="collapse">
                                    <td class="labelrow">Nama Sekolah&nbsp;:&nbsp;</td>
                                    <td class="inputrow"><input type="text" name="nm_skolah" style="width:600px;" maxlength="50" value="{$pdd.NM_SEKOLAH_PENDIDIKAN}" /></td>
                                </tr>
                                <tr class="collapse">
                                    <td class="labelrow">Jurusan&nbsp;:&nbsp;</td>
                                    <td class="inputrow"><input type="text" name="jur_skolah" style="width:600px;" maxlength="50" value="{$pdd.NM_JURUSAN_PENDIDIKAN}" /></td>
                                </tr>
                                <tr class="collapse">
                                    <td class="labelrow">Tempat Sekolah&nbsp;:&nbsp;</td>
                                    <td class="inputrow"><input type="text" name="tpt_skolah" style="width:600px;" maxlength="50" value="{$pdd.TEMPAT_PENDIDIKAN}" /></td>
                                </tr>
                                <tr class="collapse">
                                    <td class="labelrow">Tahun Lulus&nbsp;:&nbsp;</td>
                                    <td class="inputrow">
                                        {html_select_date prefix='pdd' time=$pdd.TAHUN_LULUS start_year='-60' end_year='+0' display_months=false display_days=false reverse_years=true}
                                    </td>
                                </tr>
                                <tr class="collapse">
                                    <td class="labelrow"></td>
                                    <td class="inputrow"><input type="submit" name="submit" value="Simpan" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('pdd{$pdd.ID_SEJARAH_PENDIDIKAN}')" /></td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </td>
        </tr>
    {foreachelse}
        <tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
    {/foreach}
    <tr id="tambahpdd_btn">
        <td colspan="8" style="text-align: right;"><a onclick="javascript:togglepdd('tambahpdd', 'pddbtn');" onMouseOver="this.style.cursor = 'pointer'" id="pddbtn"><img src="includes/images/add.png" alt="Tambah" title="Tambah"></a></td>
    </tr>
</table>
<div id="tambahpdd" style="display: none;">
    <table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
        <tr><th colspan="8" width="850" style="text-align: center"><strong>INPUT SEJARAH PENDIDIKAN - {$peg.NM_PENGGUNA}</strong></th></tr>
        <tr><td colspan="8" width="850">
                <iframe scrolling="no" src="insert_pdd.php?id={$peg.ID_PENGGUNA}" width="100%" frameborder="0"></iframe>
            </td></tr>
    </table>
</div>


{*SEJARAH DIKLAT*}

<table style="width: 98%" class="tablesorter" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <th colspan="8" ><strong>SEJARAH DIKLAT</strong></th>
    </tr>
    <tr>
        <th style="text-align: center"><strong>Nama Diklat</strong></th>
        <th style="text-align: center"><strong>Lokasi</strong></th>
        <th style="text-align: center"><strong>Waktu Diklat</strong></th>
        <th width="50" style="text-align: center"><strong>Penyelenggara</strong></th>
        <th style="text-align: center"><strong>Jenis Diklat</strong></th>
        <th style="text-align: center"><strong>Status</strong></th>
        <th width="50" style="text-align: center"><strong>Aksi</strong></th>
    </tr>
    {foreach item="d" from=$DIKLAT}
        <tr id="rowpdd{$d.ID_SEJARAH_DIKLAT}">
            <td>{$d.NAMA_DIKLAT}</td>
            <td>{$d.LOKASI} {$d.KOTA_DIKLAT} </td>
            <td>{$d.T_MULAI} {$d.T_SELESAI} Total Jam {$d.JUMLAH_JAM}</td>
            <td>{$d.PENYELENGGARA}</td>
            <td>{$d.JENIS_DIKLAT}</td>
            {if $d.STATUS_VALID==1}
                <td style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/success.png" alt="Ubah menjadi belum valid" title="Ubah menjadi belum valid" name="id_klgdr" id="id_klgdr{$d.ID_SEJARAH_DIKLAT}" onclick="javascript:valid('diklat',{$d.ID_SEJARAH_DIKLAT});" /></span></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('diklat{$d.ID_SEJARAH_DIKLAT}')" />
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_keluarga_diri" id="hapus_pendidikan{$d.ID_SEJARAH_DIKLAT}" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    hapus('diklat',{$d.ID_SEJARAH_DIKLAT});" />
                    </span>

                </td>
            {else}
                <td style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/forbidden.png" alt="Ubah Menjadi Valid" title="Ubah Menjadi Valid" name="id_klgdr" id="id_klgdr{$d.ID_SEJARAH_DIKLAT}" onclick="javascript:valid('diklat',{$d.ID_SEJARAH_DIKLAT});" /></span></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('diklat{$d.ID_SEJARAH_DIKLAT}')" />&nbsp;
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_keluarga_diri" id="hapus_pendidikan{$d.ID_SEJARAH_DIKLAT}" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    hapus('diklat',{$d.ID_SEJARAH_DIKLAT});" />
                    </span>

                </td>
            {/if}
        </tr>
        <tr class="edit">
            <td colspan="8" id="hidediklat{$d.ID_SEJARAH_DIKLAT}" style="display:none;">
                <div id="diklat{$d.ID_SEJARAH_DIKLAT}" style="display:none;">
                    <form name="diklatedit" id="klgdredit" method="post" action="karyawan_detail.php" onsubmit="return validate_form();">
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tr class="collapse">
                                <td class="labelrow">Nama Diklat&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="nama" style="width:600px;" maxlength="50" value="{$d.NAMA_DIKLAT}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Status Luar Negeri&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select name="status_luar">
                                        <option {if $d.STATUS_LUAR_NEGERI=='1'}selected="true"{/if} value="1">Luar Negeri</option>
                                        <option {if $d.STATUS_LUAR_NEGERI=='2'}selected="true"{/if}  value="2">Dalam Negeri</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Lokasi&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <textarea name="lokasi" style="width: 90%;height: 120px;resize: none">{$d.LOKASI}</textarea>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Kota Lokasi&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select class="negara" form-name="diklat" id="negaradiklat{$d.ID_SEJARAH_DIKLAT}" name="negara">
                                        <option value="">Pilih Negara</option>
                                        {foreach $negara as $n}
                                            <option value="{$n.ID_NEGARA}">{$n.NM_NEGARA}</option>
                                        {/foreach}
                                    </select>
                                    <select class="propinsi" form-name="diklat" id="propinsidiklat{$d.ID_SEJARAH_DIKLAT}" name="propinsi">
                                        <option value="">Pilih Propinsi</option>
                                    </select>
                                    <select id="kotadiklat{$d.ID_SEJARAH_DIKLAT}" name="kota_lokasi">
                                        <option value="1">Pilih Kota</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Tanggal Mulai&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="tgl_mulai" style="width:120px; text-align:center;" id="tgl_mulai" style="text-align:center;" onclick="javascript:NewCssCal('tgl_mulai', 'ddmmyyyy', '', '', '', '', 'past')" value="{$d.T_MULAI}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Tanggal Selesai&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="tgl_selesai" style="width:120px; text-align:center;" id="tgl_selesai" style="text-align:center;" onclick="javascript:NewCssCal('tgl_selesai', 'ddmmyyyy', '', '', '', '', 'past')" value="{$d.T_SELESAI}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Jumlah Jam&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="jumlah_jam" size="10" maxlength="10" value="{$d.JUMLAH_JAM}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Penyelenggara&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="penyelenggara" style="width:600px;" maxlength="50" value="{$d.PENYELENGGARA}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Jenis Diklat&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select name="jenis">
                                        <option {if $d.JENIS_DIKLAT=='Fungsional'}selected="true"{/if} value="Fungsional">Fungsional</option>
                                        <option {if $d.JENIS_DIKLAT=='Teknis'}selected="true"{/if}  value="Teknis">Teknis</option>
                                        <option {if $d.JENIS_DIKLAT=='Penjenjangan/Struktural'}selected="true"{/if}  value="Penjenjangan/Struktural">Penjenjangan/Struktural</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Tingkat Diklat&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select name="tingkat">
                                        <option value=""></option>
                                        <option {if $d.TINGKAT_DIKLAT=='1'}selected="true"{/if} value="1">Diklatpim TK-I</option>
                                        <option {if $d.TINGKAT_DIKLAT=='2'}selected="true"{/if} value="2">Diklatpim TK-II</option>
                                        <option {if $d.TINGKAT_DIKLAT=='3'}selected="true"{/if} value="3">Diklatpim TK-III</option>
                                        <option {if $d.TINGKAT_DIKLAT=='4'}selected="true"{/if} value="4">Diklatpim TK-IV</option>
                                        <option {if $d.TINGKAT_DIKLAT=='5'}selected="true"{/if} value="5">Diklat Lain yang Setara</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Tahun Angkatan&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="tahun" size="4" maxlength="4" value="{$d.TAHUN_ANGKATAN}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Predikat Dikat&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="predikat" size="20" maxlength="20" value="{$d.PREDIKAT}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">
                                    <input type="hidden" name="id_diklat" value="{$d.ID_SEJARAH_DIKLAT}" />
                                    <input type="hidden" name="action" value="edit_diklat" />
                                </td>
                                <td class="inputrow"><input type="submit" name="submit" value="Update" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('diklat{$d.ID_SEJARAH_DIKLAT}')" /></td>
                            </tr>
                        </table>
                    </form>
                    {literal}
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $('.negara').change(function() {
                                    var form_name = $(this).attr('form-name');
                                    var id = $(this).attr('id').replace("negara" + form_name, "");
                                    $.ajax({
                                        type: "POST",
                                        url: "getNegaraPropinsi.php",
                                        data: {id: $('#negara' + form_name + id).val()},
                                        success: function(data) {
                                            $('#propinsi' + form_name + id).html(data);
                                        }
                                    })
                                });
                                $('.propinsi').change(function() {
                                    var form_name = $(this).attr('form-name');
                                    var id = $(this).attr('id').replace("propinsi" + form_name, "");
                                    $.ajax({
                                        type: "POST",
                                        url: "getPropinsiKota.php",
                                        data: {id: $('#propinsi' + form_name + id).val()},
                                        success: function(data) {
                                            $('#kota' + form_name + id).html(data);
                                        }
                                    })
                                });
                            });
                        </script>
                    {/literal}
                </div>
            </td>
        </tr>
    {foreachelse}
        <tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
    {/foreach}
    <tr id="tambahdiklat_btn">
        <td colspan="8" style="text-align: right;"><a onclick="javascript:toggle('tambahdiklat', 'diklatbtn');" onMouseOver="this.style.cursor = 'pointer'" id="diklatbtn"><img src="includes/images/add.png" alt="Tambah" title="Tambah"></a></td>
    </tr>
</table>
<div id="tambahdiklat" style="display: none;">
    <table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
        <tr><th colspan="8" width="850" style="text-align: center"><strong>INPUT DATA ANAK - {$peg.NM_PENGGUNA}</strong></th></tr>
        <tr><td colspan="8" width="850">
                <iframe height="500" scrolling="no" src="insert_diklat.php?id={$peg.ID_PENGGUNA}" width="100%" frameborder="0"></iframe>
            </td></tr>
    </table>
</div>    



{* RIWAYAT PENGHARGAAN *}
<table class="tablesorter" style="width: 98%"  cellspacing="0" cellpadding="0" border="0">
    <tr><th colspan="7" width="850"><strong>RIWAYAT PENGHARGAAN</strong></th></tr>
    <tr>
        <th><strong>Nama - Bidang</strong></th>
        <th><strong>Nomor</strong></th>
        <th><strong>Pemberi</strong></th>
        <th width="50"><strong>Tingkat</strong></th>
        <th width="50"><strong>Status</strong></th>
        <th width="50"><strong>Aksi</strong></th>
    </tr>
    {foreach item="d" from=$PEHG}
        <tr>
            <td>{$d.NAMA_PENGHARGAAN} - {$d.BIDANG_PENGHARGAAN}</td>
            <td>{$d.NOMOR_PENGHARGAAN} / {$d.TGL_OLEH}</td>
            <td>{$d.PEMBERI_PENGHARGAAN} {$d.INSTANSI_PEMBERI}</td>
            <td>{$d.TINGKAT_PENGHARGAAN}</td>
            {if $d.STATUS_VALID==1}
                <td style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/success.png" alt="Ubah menjadi belum valid" title="Ubah menjadi belum valid" name="id_klgdr" id="id_klgdr{$d.ID_KELUARGA_DIRI}" onclick="javascript:valid('penghar',{$d.ID_SEJARAH_PENGHARGAAN});" /></span></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('penghar{$d.ID_SEJARAH_PENGHARGAAN}')" />
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_keluarga_diri" id="hapus_pendidikan{$d.ID_KELUARGA_DIRI}" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    hapus('penghar',{$d.ID_SEJARAH_PENGHARGAAN});" />
                    </span>

                </td>
            {else}
                <td style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/forbidden.png" alt="Ubah Menjadi Valid" title="Ubah Menjadi Valid" name="id_klgdr" id="id_klgdr{$d.ID_KELUARGA_DIRI}" onclick="javascript:valid('penghar',{$d.ID_SEJARAH_PENGHARGAAN});" /></span></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('penghar{$d.ID_SEJARAH_PENGHARGAAN}')" />&nbsp;
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_keluarga_diri" id="hapus_pendidikan{$d.ID_KELUARGA_DIRI}" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    hapus('penghar',{$d.ID_SEJARAH_PENGHARGAAN});" />
                    </span>

                </td>
            {/if}
        </tr>
        <tr class="edit">
            <td colspan="8" id="hidepenghar{$d.ID_SEJARAH_PENGHARGAAN}" style="display:none;">
                <div id="penghar{$d.ID_SEJARAH_PENGHARGAAN}" style="display:none;">
                    <form name="pengharedit" id="pengharedit" method="post" action="karyawan_detail.php" onsubmit="return validate_form();">
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tr class="collapse">
                                <td class="labelrow">Nama Penghargaan&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="nama" style="width:600px;" maxlength="50" value="{$d.NAMA_PENGHARGAAN}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Nomor Penghargaan&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="nomor" style="width:600px;" maxlength="50" value="{$d.NOMOR_PENGHARGAAN}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Bidang Penghargaan&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="bidang" style="width:600px;" maxlength="50" value="{$d.BIDANG_PENGHARGAAN}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Bentuk Penghargaan&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="bentuk" style="width:600px;" maxlength="50" value="{$d.BENTUK_PENGHARGAAN}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Tanggal Perolehan&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="tgl_perolehan" style="width:120px; text-align:center;" id="tgl_perolehan" style="text-align:center;" onclick="javascript:NewCssCal('tgl_perolehan', 'ddmmyyyy', '', '', '', '', 'past')" value="{$d.TGL_OLEH}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Negara Pemberi&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select name="negara">
                                        <option value="">Pilih Negara</option>
                                        {foreach $negara as $n}
                                            <option value="{$n.ID_NEGARA}">{$n.NM_NEGARA}</option>
                                        {/foreach}
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Nama Pemberi&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="pemberi" style="width:600px;" maxlength="50" value="{$d.PEMBERI_PENGHARGAAN}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Intansi Pemberi&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="intansi" style="width:600px;" maxlength="50" value="{$d.INSTANSI_PEMBERI}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Jabatan Pemberi&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="jabatan" style="width:600px;" maxlength="50" value="{$d.JABATAN_PEMBERI}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Tingkat Penghargaan&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="tingkat" placeholder="Nasional/Internasional/Lokal" style="width:600px;" maxlength="50" value="{$d.TINGKAT_PENGHARGAAN}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">
                                    <input type="hidden" name="id_penghar" value="{$d.ID_SEJARAH_PENGHARGAAN}" />
                                    <input type="hidden" name="action" value="edit_penghar" />
                                </td>
                                <td class="inputrow"><input type="submit" name="submit" value="Update" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('klgdr{$d.ID_KELUARGA_DIRI}')" /></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </td>
        </tr>
    {foreachelse}
        <tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
    {/foreach}
    <tr id="tambahpenghar_btn">
        <td colspan="7" style="text-align: right;"><a onclick="javascript:toggle('tambahpenghar', 'penghar_btn');" onMouseOver="this.style.cursor = 'pointer'" id="penghar_btn"><img src="includes/images/add.png" alt="Tambah" title="Tambah"></a></td>
    </tr>
</table>
<div id="tambahpenghar" style="display: none;">
    <table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
        <tr><th colspan="7" width="850" style="text-align: center"><strong>INPUT DATA RIWAYAT PENGHARGAAN - {$peg.NM_PENGGUNA}</strong></th></tr>
        <tr><td colspan="7" width="850">
                <iframe height="300" scrolling="no" src="insert_penghargaan.php?id={$peg.ID_PENGGUNA}" width="100%" frameborder="0"></iframe>
            </td></tr>
    </table>
</div>   



{* RIWAYAT ORGANISASI *}
<table class="tablesorter" style="width: 98%"  cellspacing="0" cellpadding="0" border="0">
    <tr><th colspan="7" width="850"><strong>ORGANISASI</strong></th></tr>
    <tr>
        <th><strong>Nama</strong></th>
        <th><strong>Jabatan</strong></th>
        <th><strong>Waktu</strong></th>
        <th><strong>SK Organisasi</strong></th>
        <th><strong>Tingkat Organisasi</strong></th>
        <th><strong>Status</strong></th>
        <th width="50"><strong>Aksi</strong></th>
    </tr>
    {foreach item="d" from=$PORG}
        <tr>
            <td>{$d.NAMA_ORGANISASI}</td>
            <td>{$d.KEDUDUKAN_ORGANISASI}</td>
            <td>{$d.T_MULAI} - {$d.T_SELESAI}</td>
            <td>{$d.NO_SK_ORGANISASI} {$d.JABATAN_SK_ORGANISASI}</td>
            <td>{$d.TINGKAT_ORGANISASI}</td>
            {if $d.STATUS_VALID==1}
                <td style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/success.png" alt="Ubah menjadi belum valid" title="Ubah menjadi belum valid" name="id_klgdr" id="id_klgdr{$d.ID_KELUARGA_DIRI}" onclick="javascript:valid('organ',{$d.ID_SEJARAH_ORGANISASI});" /></span></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('organ{$d.ID_SEJARAH_ORGANISASI}')" />
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_keluarga_diri" id="hapus_pendidikan{$d.ID_KELUARGA_DIRI}" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    hapus('organ',{$d.ID_SEJARAH_ORGANISASI});" />
                    </span>

                </td>
            {else}
                <td style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/forbidden.png" alt="Ubah Menjadi Valid" title="Ubah Menjadi Valid" name="id_klgdr" id="id_klgdr{$d.ID_KELUARGA_DIRI}" onclick="javascript:valid('organ',{$d.ID_SEJARAH_ORGANISASI});" /></span></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('organ{$d.ID_SEJARAH_ORGANISASI}')" />&nbsp;
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_keluarga_diri" id="hapus_pendidikan{$d.ID_KELUARGA_DIRI}" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    hapus('organ',{$d.ID_SEJARAH_ORGANISASI});" />
                    </span>

                </td>
            {/if}
        </tr>
        <tr class="edit">
            <td colspan="8" id="hideorgan{$d.ID_SEJARAH_ORGANISASI}" style="display:none;">
                <div id="organ{$d.ID_SEJARAH_ORGANISASI}" style="display:none;">
                    <form name="organedit" id="organedit" method="post" action="karyawan_detail.php" onsubmit="return validate_form();">
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tr class="collapse">
                                <td class="labelrow">Nama Organisasi&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="nama" style="width:600px;" maxlength="50" value="{$d.NAMA_ORGANISASI}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Jabatan Organisasi&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="kedudukan" style="width:600px;" maxlength="50" value="{$d.KEDUDUKAN_ORGANISASI}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Tanggal Mulai&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="tgl_mulai" style="width:120px; text-align:center;" id="tgl_mulai" style="text-align:center;" onclick="javascript:NewCssCal('tgl_mulai', 'ddmmyyyy', '', '', '', '', 'past')" value="{$d.T_MULAI}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Tanggal Selesai&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="tgl_selesai" style="width:120px; text-align:center;" id="tgl_selesai" style="text-align:center;" onclick="javascript:NewCssCal('tgl_selesai', 'ddmmyyyy', '', '', '', '', 'past')" value="{$d.T_SELESAI}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">NO SK Organisasi&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="no_sk" style="width:600px;" maxlength="50" value="{$d.NO_SK_ORGANISASI}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Jabatan Pemberi SK&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="jabatan_pemberi" style="width:600px;" maxlength="50" value="{$d.JABATAN_SK_ORGANISASI}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Tingkat Organisasin&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="tingkat" placeholder="Nasional/Internasional/Lokal" style="width:600px;" maxlength="50" value="{$d.TINGKAT_ORGANISASI}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">
                                    <input type="hidden" name="id_organ" value="{$d.ID_SEJARAH_ORGANISASI}" />
                                    <input type="hidden" name="action" value="edit_organ" />
                                </td>
                                <td class="inputrow"><input type="submit" name="submit" value="Update" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('klgdr{$d.ID_KELUARGA_DIRI}')" /></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </td>
        </tr>
    {foreachelse}
        <tr><td colspan="5"><em>Data tidak ditemukan</em></td></tr>
    {/foreach}
    <tr id="tambahorgan_btn">
        <td colspan="7" style="text-align: right;"><a onclick="javascript:toggle('tambahorgan', 'organ_btn');" onMouseOver="this.style.cursor = 'pointer'" id="organ_btn"><img src="includes/images/add.png" alt="Tambah" title="Tambah"></a></td>
    </tr>
</table>
<div id="tambahorgan" style="display: none;">
    <table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
        <tr><th colspan="7" width="850" style="text-align: center"><strong>INPUT DATA RIWAYAT ORGANISASI - {$peg.NM_PENGGUNA}</strong></th></tr>
        <tr><td colspan="7" width="850">
                <iframe height="300" scrolling="no" src="insert_organisasi.php?id={$peg.ID_PENGGUNA}" width="100%" frameborder="0"></iframe>
            </td></tr>
    </table>
</div>   

{* RIWAYAT KEGIATAN PEGAWAI *}
<table class="tablesorter" style="width: 98%"  cellspacing="0" cellpadding="0" border="0">
    <tr><th colspan="8" width="850"><strong>TRAINING/WORKSHOP/SEMINAR</strong></th></tr>
    <tr>
        <th><strong>Jenis</strong></th>
        <th><strong>Nama</strong></th>
        <th><strong>Lokasi</strong></th>
        <th><strong>Waktu</strong></th>
        <th><strong>Kedudukan</strong></th>
        <th><strong>Tingkat</strong></th>
        <th width="50"><strong>Status</strong></th>
        <th width="50"><strong>Aksi</strong></th>
    </tr>
    {foreach item="d" from=$PTRG}
        <tr>
            <td>{$d.JENIS_KEGIATAN_PEGAWAI}</td>
            <td>{$d.NAMA_KEGIATAN_PEGAWAI}</td>
            <td>{$d.LOKASI_KEGIATAN_PEGAWAI}</td>
            <td>{$d.TGL_MULAI} {$d.TGL_SELESAI}</td>
            <td>{$d.KEDUDUKAN_KEGIATAN_PEGAWAI}</td>
            <td>{if $d.TINGKAT_KEGIATAN_PEGAWAI==1}Nasional{else if $d.TINGKAT_KEGIATAN_PEGAWAI==2}Internasional{/if}</td>
            {if $d.STATUS_VALID==1}
                <td style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/success.png" alt="Ubah menjadi belum valid" title="Ubah menjadi belum valid" name="id_klgdr" id="id_klgdr{$d.ID_SEJARAH_KEGIATAN_PEGAWAI}" onclick="javascript:valid('kegpeg',{$d.ID_SEJARAH_KEGIATAN_PEGAWAI});" /></span></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('kegpeg{$d.ID_SEJARAH_KEGIATAN_PEGAWAI}')" />
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_keluarga_diri" id="hapus_pendidikan{$d.ID_SEJARAH_DIKLAT}" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    hapus('kegpeg',{$d.ID_SEJARAH_KEGIATAN_PEGAWAI});" />
                    </span>

                </td>
            {else}
                <td style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/forbidden.png" alt="Ubah Menjadi Valid" title="Ubah Menjadi Valid" name="id_klgdr" id="id_klgdr{$d.ID_SEJARAH_DIKLAT}" onclick="javascript:valid('kegpeg',{$d.ID_SEJARAH_KEGIATAN_PEGAWAI});" /></span></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('kegpeg{$d.ID_SEJARAH_DIKLAT}')" />&nbsp;
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_keluarga_diri" id="hapus_pendidikan{$d.ID_SEJARAH_DIKLAT}" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    hapus('kegpeg',{$d.ID_SEJARAH_KEGIATAN_PEGAWAI});" />
                    </span>

                </td>
            {/if}
        </tr>
        <tr class="edit">
            <td colspan="8" id="hidekegpeg{$d.ID_SEJARAH_KEGIATAN_PEGAWAI}" style="display:none;">
                <div id="kegpeg{$d.ID_SEJARAH_KEGIATAN_PEGAWAI}" style="display:none;">
                    <form name="kegpegedit" id="klgdredit" method="post" action="karyawan_detail.php" onsubmit="return validate_form();">
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tr class="collapse">
                                <td class="labelrow">Jenis Kegiatan&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select name="jenis">
                                        <option {if $d.JENIS_KEGIATAN_PEGAWAI=='TRAINING'}selected="true"{/if} value="TRAINING">TRAINING</option>
                                        <option {if $d.JENIS_KEGIATAN_PEGAWAI=='WORKSHOP'}selected="true"{/if}  value="WORKSHOP">WORKSHOP</option>
                                        <option {if $d.JENIS_KEGIATAN_PEGAWAI=='LOKAKARYA'}selected="true"{/if}  value="LOKAKARYA">LOKAKARYA</option>
                                        <option {if $d.JENIS_KEGIATAN_PEGAWAI=='SEMINAR'}selected="true"{/if}  value="SEMINAR">SEMINAR</option>
                                        <option {if $d.JENIS_KEGIATAN_PEGAWAI=='SIMPOSIUM'}selected="true"{/if}  value="SIMPOSIUM">SIMPOSIUM</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Nama Kegiatan&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="nama" style="width:600px;" maxlength="50" value="{$d.NAMA_KEGIATAN_PEGAWAI}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Lokasi&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <textarea name="lokasi" style="width: 90%;height: 120px;resize: none">{$d.LOKASI}</textarea>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Kota Lokasi&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select class="negara" form-name="kegpeg" id="negarakegpeg{$d.ID_SEJARAH_KEGIATAN_PEGAWAI}" name="negara">
                                        <option value="">Pilih Negara</option>
                                        {foreach $negara as $n}
                                            <option value="{$n.ID_NEGARA}">{$n.NM_NEGARA}</option>
                                        {/foreach}
                                    </select>
                                    <select class="propinsi" form-name="kegpeg" id="propinsikegpeg{$d.ID_SEJARAH_KEGIATAN_PEGAWAI}" name="propinsi">
                                        <option value="">Pilih Propinsi</option>
                                    </select>
                                    <select id="kotakegpeg{$d.ID_SEJARAH_KEGIATAN_PEGAWAI}" name="kota_lokasi">
                                        <option value="1">Pilih Kota</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Status Luar Negeri&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select name="status_luar">
                                        <option {if $d.STATUS_LUAR_NEGERI=='1'}selected="true"{/if} value="1">Luar Negeri</option>
                                        <option {if $d.STATUS_LUAR_NEGERI=='2'}selected="true"{/if}  value="2">Dalam Negeri</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Tanggal Mulai&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="tgl_mulai" style="width:120px; text-align:center;" id="tgl_mulai" style="text-align:center;" onclick="javascript:NewCssCal('tgl_mulai', 'ddmmyyyy', '', '', '', '', 'past')" value="{$d.T_MULAI}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Tanggal Selesai&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="tgl_selesai" style="width:120px; text-align:center;" id="tgl_selesai" style="text-align:center;" onclick="javascript:NewCssCal('tgl_selesai', 'ddmmyyyy', '', '', '', '', 'past')" value="{$d.T_SELESAI}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Penyelenggara&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="penyelenggara" style="width:600px;" maxlength="50" value="{$d.PENYELENGGARA}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Kedudukan/Peran&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="kedudukan" style="width:600px;" maxlength="50" value="{$d.PENYELENGGARA}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Tingkat Kegiatan&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select name="tingkat">
                                        <option value=""></option>
                                        <option {if $d.TINGKAT_KEGIATAN_PEGAWAI=='1'}selected="true"{/if} value="1">Internasional</option>
                                        <option {if $d.TINGKAT_KEGIATAN_PEGAWAI=='2'}selected="true"{/if} value="2">Nasional</option>
                                        <option {if $d.TINGKAT_KEGIATAN_PEGAWAI=='3'}selected="true"{/if} value="2">Lokal</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">
                                    <input type="hidden" name="id_kegpeg" value="{$d.ID_SEJARAH_KEGIATAN_PEGAWAI}" />
                                    <input type="hidden" name="action" value="edit_kegpeg" />
                                </td>
                                <td class="inputrow"><input type="submit" name="submit" value="Update" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('kegpeg{$d.ID_SEJARAH_KEGIATAN_PEGAWAI}')" /></td>
                            </tr>
                        </table>
                    </form>
                    {literal}
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $('.negara').change(function() {
                                    var form_name = $(this).attr('form-name');
                                    var id = $(this).attr('id').replace("negara" + form_name, "");
                                    $.ajax({
                                        type: "POST",
                                        url: "getNegaraPropinsi.php",
                                        data: {id: $('#negara' + form_name + id).val()},
                                        success: function(data) {
                                            $('#propinsi' + form_name + id).html(data);
                                        }
                                    })
                                });
                                $('.propinsi').change(function() {
                                    var form_name = $(this).attr('form-name');
                                    var id = $(this).attr('id').replace("propinsi" + form_name, "");
                                    $.ajax({
                                        type: "POST",
                                        url: "getPropinsiKota.php",
                                        data: {id: $('#propinsi' + form_name + id).val()},
                                        success: function(data) {
                                            $('#kota' + form_name + id).html(data);
                                        }
                                    })
                                });
                            });
                        </script>
                    {/literal}
                </div>
            </td>
        </tr>
    {foreachelse}
        <tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
    {/foreach}
    <tr id="tambahkegpeg_btn">
        <td colspan="8" style="text-align: right;"><a onclick="javascript:toggle('tambahkegpeg', 'kegpegbtn');" onMouseOver="this.style.cursor = 'pointer'" id="kegpegbtn"><img src="includes/images/add.png" alt="Tambah" title="Tambah"></a></td>
    </tr>
</table>
<div id="tambahkegpeg" style="display: none;">
    <table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
        <tr><th colspan="8" width="850" style="text-align: center"><strong>INPUT DATA KEGIATAN - {$peg.NM_PENGGUNA}</strong></th></tr>
        <tr><td colspan="8" width="850">
                <iframe height="500" scrolling="no" src="insert_kegpeg.php?id={$peg.ID_PENGGUNA}" width="100%" frameborder="0"></iframe>
            </td></tr>
    </table>
</div>    


{* RIWAYAT DP3 *}
<table style="width: 98%" class="tablesorter" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <th colspan="6" width="850"><strong>RIWAYAT DP3</strong></th>
    </tr>
    <tr>
        <th style="text-align: center"><strong>Pejabat Penilai</strong></th>
        <th style="text-align: center"><strong>Atasan Pejabat Penilai</strong></th>
        <th style="text-align: center"><strong>Rata - Rata Nilai</strong></th>
        <th width="50" style="text-align: center"><strong>Status</strong></th>
        <th width="50" style="text-align: center"><strong>Aksi</strong></th>
    </tr>
    {foreach item="d" from=$DP3}
        <tr id="rowpdd{$d.ID_PEGAWAI_DP3}">
            <td>{$d.JABATAN_PJBT_PENILAI} {$d.UNIT_PJBT_PENILAI} </td>
            <td>{$d.JABATAN_PJBT_ATASAN} {$d.UNIT_PJBT_ATASAN} </td>
            <td>{$d.NILAI_RATA}</td>
            {if $d.STATUS_VALID==1}
                <td style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/success.png" alt="Ubah menjadi belum valid" title="Ubah menjadi belum valid" name="id_klgdr" id="id_klgdr{$d.ID_PEGAWAI_DP3}" onclick="javascript:valid('dp3',{$d.ID_PEGAWAI_DP3});" /></span></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('dp3{$d.ID_PEGAWAI_DP3}')" />
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_keluarga_diri" id="hapus_pendidikan{$d.ID_PEGAWAI_DP3}" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    hapus('dp3',{$d.ID_PEGAWAI_DP3});" />
                    </span>

                </td>
            {else}
                <td style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/forbidden.png" alt="Ubah Menjadi Valid" title="Ubah Menjadi Valid" name="id_klgdr" id="id_klgdr{$d.ID_PEGAWAI_DP3}" onclick="javascript:valid('dp3',{$d.ID_PEGAWAI_DP3});" /></span></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('dp3{$d.ID_PEGAWAI_DP3}')" />&nbsp;
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_keluarga_diri" id="hapus_pendidikan{$d.ID_PEGAWAI_DP3}" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    hapus('dp3',{$d.ID_PEGAWAI_DP3});" />
                    </span>

                </td>
            {/if}
        </tr>
        <tr class="edit">
            <td colspan="8" id="hidedp3{$d.ID_PEGAWAI_DP3}" style="display:none;">
                <div id="dp3{$d.ID_PEGAWAI_DP3}" style="display:none;">
                    <form name="dp3edit" id="klgdredit" method="post" action="karyawan_detail.php" onsubmit="return validate_form();">
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tr class="collapse">
                                <td class="labelrow">NIP Pejabat Penilai&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="nip_penilai" style="width:600px;" maxlength="50" value="{$d.NIP_PJBT_PENILAI}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">NIP Lama Pejabat Penilai&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="nip_lama_penilai" style="width:600px;" maxlength="50" value="{$d.NIP_LAMA_PJBT_PENILAI}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Jabatan Pejabat Penilai&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="jabatan_penilai" style="width:600px;" maxlength="50" value="{$d.JABATAN_PJBT_PENILAI}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Unit Pejabat Penilai&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="unit_penilai" style="width:600px;" maxlength="100" value="{$d.UNIT_PJBT_PENILAI}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">NIP Atasan Pejabat Penilai&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="nip_atasan" style="width:600px;" maxlength="50" value="{$d.NIP_PJBT_ATASAN}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Jabatan Atasan Pejabat Penilai&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="jabatan_atasan" style="width:600px;" maxlength="50" value="{$d.JABATAN_PJBT_ATASAN}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Unit Atasan Pejabat Penilai&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="unit_atasan" style="width:600px;" maxlength="100" value="{$d.UNIT_PJBT_ATASAN}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Nilai Kesetiaan&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="kesetiaan" style="width:100px;" maxlength="4" placeholder="1-100" value="{$d.KESETIAAAN}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Nilai Kerjasama&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="kerjasama" style="width:100px;" maxlength="4" placeholder="1-100" value="{$d.KERJASAMA}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Nilai Prestasi Kerja&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="prestasi" style="width:100px;" maxlength="4" placeholder="1-100" value="{$d.PRESTASI_KERJA}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Nilai Prakarsa&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="prakarsa" style="width:100px;" maxlength="4" placeholder="1-100" value="{$d.PRAKARSA}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Nilai Tanggung Jawab&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="tanggung_jawab" style="width:100px;" maxlength="4" placeholder="1-100" value="{$d.TANGGUNG_JAWAB}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Nilai Kepemimpinan&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="kepemimpinan" style="width:100px;" maxlength="4" placeholder="1-100" value="{$d.KEPEMIMPINAN}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Nilai Ketaatan&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="ketaatan" style="width:100px;" maxlength="4" placeholder="1-100" value="{$d.KETAATAN}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Nilai Kerjujuran&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="kejujuran" style="width:100px;" maxlength="4" placeholder="1-100" value="{$d.KEJUJURAN}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">
                                    <input type="hidden" name="id_dp3" value="{$d.ID_PEGAWAI_DP3}" />
                                    <input type="hidden" name="action" value="edit_dp3" />
                                </td>
                                <td class="inputrow"><input type="submit" name="submit" value="Update" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('kegpeg{$d.ID_SEJARAH_KEGIATAN_PEGAWAI}')" /></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </td>
        </tr>
    {foreachelse}
        <tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
    {/foreach}
    <tr id="tambahdp3_btn">
        <td colspan="8" style="text-align: right;"><a onclick="javascript:toggle('tambahdp3', 'dp3btn');" onMouseOver="this.style.cursor = 'pointer'" id="dp3btn"><img src="includes/images/add.png" alt="Tambah" title="Tambah"></a></td>
    </tr>
</table>
<div id="tambahdp3" style="display: none;">
    <table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
        <tr><th colspan="8" width="850" style="text-align: center"><strong>INPUT DATA DP3 - {$peg.NM_PENGGUNA}</strong></th></tr>
        <tr><td colspan="8" width="850">
                <iframe height="500" scrolling="no" src="insert_dp3.php?id={$peg.ID_PENGGUNA}" width="100%" frameborder="0"></iframe>
            </td></tr>
    </table>
</div>    

{* RIWAYAT CUTI *}
<table style="width: 98%" class="tablesorter" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <th colspan="5" width="850" ><strong>RIWAYAT CUTI</strong></th>
    </tr>
    <tr>
        <th style="text-align: center"><strong>Status Cuti</strong></th>
        <th width="50" style="text-align: center"><strong>SK Cuti</strong></th>
        <th width="50" style="text-align: center"><strong>Tanggal SK Cuti</strong></th>
        <th style="text-align: center"><strong>TMT SK Cuti</strong></th>
        <th width="50" style="text-align: center"><strong>Aksi</strong></th>
    </tr>
    {foreach item="d" from=$CUTI}
        <tr id="rowpdd{$d.ID_SEJARAH_CUTI}">
            <td>{$d.NM_STATUS_PENGGUNA}  </td>
            <td>{$d.SK_CUTI}  </td>
            <td>{$d.TGL_SK_CUTI} </td>
            <td>{$d.TMT_SK_CUTI}</td>
            <td>  </td>

        </tr>
    {foreachelse}
        <tr><td colspan="5"><em>Data tidak ditemukan</em></td></tr>
    {/foreach}
    <tr>
        <td colspan="5" style="text-align: right;"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/add.png" alt="Tambah" title="Tambah" /></span></td>
    </tr>
</table>


{* RIWAYAT HUKUMAN *}
<table style="width: 98%" class="tablesorter" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <th colspan="6" width="850"><strong>RIWAYAT HUKUMAN</strong></th>
    </tr>
    <tr>
        <th style="text-align: center"><strong>Kode Hukuman</strong></th>
        <th width="50" style="text-align: center"><strong>SK Hukuman</strong></th>
        <th style="text-align: center"><strong>Pejabat SK Hukuman</strong></th>
        <th style="text-align: center"><strong>Keterangan Hukuman</strong></th>
        <th width="50" style="text-align: center"><strong>Status</strong></th>
        <th width="50" style="text-align: center"><strong>Aksi</strong></th>
    </tr>
    {foreach item="d" from=$HUKUMAN}
        <tr id="rowpdd{$d.ID_SEJARAH_HUKUMAN}">
            <td>{$d.KODE_HUKUMAN} {$d.NAMA_HUKUMAN} {$d.KETERANGAN}</td>
            <td>{$d.NO_SK_HUKUMAN} {$d.TGL_SK_HUKUMAN} {$d.TMT_SK_HUKUMAN} </td>
            <td>{$d.PEJABAT_SK_HUKUMAN}</td>
            <td>{$d.KETERANGAN}  </td>
            {if $d.STATUS_VALID==1}
                <td style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/success.png" alt="Ubah menjadi belum valid" title="Ubah menjadi belum valid" name="id_klgdr" id="id_klgdr{$d.ID_SEJARAH_KEGIATAN_PEGAWAI}" onclick="javascript:valid('hukum',{$d.ID_SEJARAH_HUKUMAN});" /></span></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('hukum{$d.ID_SEJARAH_HUKUMAN}')" />
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_keluarga_diri" id="hapus_pendidikan{$d.ID_SEJARAH_DIKLAT}" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    hapus('hukum',{$d.ID_SEJARAH_HUKUMAN});" />
                    </span>

                </td>
            {else}
                <td style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/forbidden.png" alt="Ubah Menjadi Valid" title="Ubah Menjadi Valid" name="id_klgdr" id="id_klgdr{$d.ID_SEJARAH_DIKLAT}" onclick="javascript:valid('hukum',{$d.ID_SEJARAH_HUKUMAN});" /></span></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('hukum{$d.ID_SEJARAH_HUKUMAN}')" />&nbsp;
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" name="hapus_keluarga_diri" id="hapus_pendidikan{$d.ID_SEJARAH_DIKLAT}" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    hapus('hukum',{$d.ID_SEJARAH_HUKUMAN});" />
                    </span>

                </td>
            {/if}
        </tr>
        <tr class="edit">
            <td colspan="8" id="hidehukum{$d.ID_SEJARAH_HUKUMAN}" style="display:none;">
                <div id="hukum{$d.ID_SEJARAH_HUKUMAN}" style="display:none;">
                    <form name="hukumedit" id="klgdredit" method="post" action="karyawan_detail.php" onsubmit="return validate_form();">
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tr class="collapse">
                                <td class="labelrow">Kode Hukuman&nbsp;:&nbsp;</td>
                                <td class="inputrow">
                                    <select name="kode">
                                        <option value="">Pilih Kode Hukuman</option>
                                        {foreach $kode_hukuman as $kh}
                                            <option value="{$kh.KODE_HUKUMAN}" {if $kh.KODE_HUKUMAN==$d.KODE_HUKUMAN}selected="true"{/if}>{$kh.KODE_HUKUMAN} {$kh.NAMA_HUKUMAN} {$kh.KETERANGAN}</option>
                                        {/foreach}
                                    </select>
                                </td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Nomor SK Hukuman&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="no_sk" style="width:600px;" maxlength="50" value="{$d.NO_SK_HUKUMAN}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Tanggal SK Hukuman&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="tgl_sk" style="width:120px; text-align:center;" id="tgl_sk{$d.ID_SEJARAH_HUKUMAN}" style="text-align:center;" onclick="javascript:NewCssCal('tgl_sk{$d.ID_SEJARAH_HUKUMAN}', 'ddmmyyyy', '', '', '', '', 'past')" value="{$d.T_SK}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Tanggal TMT Hukuman&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="tgl_tmt" style="width:120px; text-align:center;" id="tgl_tmt{$d.ID_SEJARAH_HUKUMAN}" style="text-align:center;" onclick="javascript:NewCssCal('tgl_tmt{$d.ID_SEJARAH_HUKUMAN}', 'ddmmyyyy', '', '', '', '', 'past')" value="{$d.T_TMT}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Pejabat SK Hukuman&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="pejabat" style="width:600px;" maxlength="100" value="{$d.PEJABAT_SK_HUKUMAN}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">
                                    <input type="hidden" name="id_hukum" value="{$d.ID_SEJARAH_HUKUMAN}" />
                                    <input type="hidden" name="action" value="edit_hukum" />
                                </td>
                                <td class="inputrow"><input type="submit" name="submit" value="Update" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('kegpeg{$d.ID_SEJARAH_KEGIATAN_PEGAWAI}')" /></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </td>
        </tr>
    {foreachelse}
        <tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
    {/foreach}
    <tr id="tambahhukum_btn">
        <td colspan="8" style="text-align: right;"><a onclick="javascript:toggle('tambahhukum', 'hukumbtn');" onMouseOver="this.style.cursor = 'pointer'" id="hukumbtn"><img src="includes/images/add.png" alt="Tambah" title="Tambah"></a></td>
    </tr>
</table>
<div id="tambahhukum" style="display: none;">
    <table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
        <tr><th colspan="8" width="850" style="text-align: center"><strong>INPUT DATA HUKUMAN - {$peg.NM_PENGGUNA}</strong></th></tr>
        <tr><td colspan="8" width="850">
                <iframe height="500" scrolling="no" src="insert_hukuman.php?id={$peg.ID_PENGGUNA}" width="100%" frameborder="0"></iframe>
            </td></tr>
    </table>
</div>    

{* RIWAYAT PEKERJAAN/JABATAN *}
<table style="width: 98%" class="tablesorter" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <th colspan="7" width="850"><strong>RIWAYAT  RIWAYAT PEKERJAAN/JABATAN</strong></th>
    </tr>
    <tr>
        <th style="text-align: center"><strong>Nama Jabatan</strong></th>
        <th width="50" style="text-align: center"><strong>Intansi</strong></th>
        <th style="text-align: center"><strong>SK Jabatan</strong></th>
        <th style="text-align: center"><strong>TMT SK Jabatan</strong></th>
        <th style="text-align: center"><strong>Pejabat SK Jabatan</strong></th>
        <th width="50" style="text-align: center"><strong>Status</strong></th>
        <th width="50" style="text-align: center"><strong>Aksi</strong></th>
    </tr>
    {foreach item="d" from=$PEKERJAAN}
        <tr id="rowpdd{$d.ID_SEJARAH_PEKERJAAN}">
            <td>{$d.NAMA_JABATAN}  </td>
            <td>{$d.INSTANSI} </td>
            <td>{$d.NO_SK_JABATAN} {$d.TGL_SK_JABATAN}</td>
            <td>{$d.TGL_MULAI_JABATAN}-{$d.TMT_JABATAN}  </td>
            <td>{$d.PJBT_SK}  </td>
            {if $d.STATUS_VALID==1}
                <td style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/success.png" alt="Ubah menjadi belum valid" title="Ubah menjadi belum valid" onclick="javascript:valid('pekerjaan',{$d.ID_SEJARAH_PEKERJAAN});" /></span></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('pekerjaan{$d.ID_SEJARAH_PEKERJAAN}')" />
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    hapus('pekerjaan',{$d.ID_SEJARAH_PEKERJAAN});" />
                    </span>

                </td>
            {else}
                <td style="text-align: center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/forbidden.png" alt="Ubah Menjadi Valid" title="Ubah Menjadi Valid"onclick="javascript:valid('pekerjaan',{$d.ID_SEJARAH_PEKERJAAN});" /></span></td>
                <td style="text-align: center">
                    <span onMouseOver="this.style.cursor = 'pointer'">
                        <img src="includes/images/update.png" alt="Edit" title="Edit" onclick="javascript:edit('pekerjaan{$d.ID_SEJARAH_PEKERJAAN}')" />&nbsp;
                        <img src="includes/images/delete.png" alt="Hapus" title="Hapus" onclick="javascript:if (confirm('Proses penghapusan adalah permanen. Lanjutkan?'))
                                    hapus('pekerjaan',{$d.ID_SEJARAH_PEKERJAAN});" />
                    </span>

                </td>
            {/if}
        <tr class="edit">
            <td colspan="7" id="hidepekerjaan{$d.ID_SEJARAH_PEKERJAAN}" style="display:none;">
                <div id="pekerjaan{$d.ID_SEJARAH_PEKERJAAN}" style="display:none;">
                    <form name="nikahedit" id="tambahanedit" method="post" action="karyawan_detail.php" onsubmit="return validate_form();">
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tr class="collapse">
                                <td class="labelrow">Nama Jabatan&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="nama" style="width:600px;" maxlength="50" value="{$d.NAMA_JABATAN}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Intansi Pekerjaan&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="intansi" style="width:600px;" maxlength="50" value="{$d.INSTANSI}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">NO SK Jabatan Pekerjaan&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="no_sk" style="width:600px;" maxlength="50" value="{$d.NO_SK_JABATAN}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Tanggal SK Jabatan Struktural&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="tgl_sk" style="width:120px; text-align:center;" id="tgl_sk{$d.ID_SEJARAH_PEKERJAAN}}" style="text-align:center;" onclick="javascript:NewCssCal('tgl_sk{$d.ID_SEJARAH_PEKERJAAN}', 'ddmmyyyy', '', '', '', '', 'past')" value="{$d.T_SK}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">TGl Mulai Jabatan Struktural&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="tgl_mulai" style="width:120px; text-align:center;" id="tgl_mulai{$d.ID_SEJARAH_PEKERJAAN}" style="text-align:center;" onclick="javascript:NewCssCal('tgl_mulai{$d.ID_SEJARAH_PEKERJAAN}', 'ddmmyyyy', '', '', '', '', 'past')" value="{$d.T_MULAI}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">TMT SK Jabatan Struktural&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="tgl_tmt" style="width:120px; text-align:center;" id="tgl_tmt{$d.ID_SEJARAH_PEKERJAAN}" style="text-align:center;" onclick="javascript:NewCssCal('tgl_tmt{$d.ID_SEJARAH_PEKERJAAN}', 'ddmmyyyy', '', '', '', '', 'past')" value="{$d.T_TMT}" /></td>
                            </tr>

                            <tr class="collapse">
                                <td class="labelrow">NIP Pejabat TTD SK Jabatan&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="nip" style="width:600px;" maxlength="50" value="{$d.NIP_PJBT_SK}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">NIP Lama Pejabat TTD SK Jabatan&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="nip_lama" style="width:600px;" maxlength="50" value="{$d.NIP_LAMA_PJBT_SK}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">Pejabat TTD SK Jabatan&nbsp;:&nbsp;</td>
                                <td class="inputrow"><input type="text" name="pejabat" style="width:600px;" maxlength="50" value="{$d.PJBT_SK}" /></td>
                            </tr>
                            <tr class="collapse">
                                <td class="labelrow">
                                    <input type="hidden" name="id_pekerjaan" value="{$d.ID_SEJARAH_PEKERJAAN}" />
                                    <input type="hidden" name="action" value="edit_pekerjaan" />
                                </td>
                                <td class="inputrow"><input type="submit" name="submit" value="Update" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('tambahan{$d.ID_SEJARAH_JABATAN_STRUKTURAL}')" /></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </td>
        </tr>
    {foreachelse}
        <tr>
            <td colspan="7">Data tidak ditemukan
            </td>
        </tr>
    {/foreach}
    <tr id="tambahpekerjaan_btn">
        <td colspan="7" style="text-align: right;"><a onclick="javascript:toggle('tambahpekerjaan', 'pekerjaan_btn');" onMouseOver="this.style.cursor = 'pointer'" id="pekerjaan_btn"><img src="includes/images/add.png" alt="Tambah" title="Tambah"></a></td>
    </tr>
</table>
<div id="tambahpekerjaan" style="display: none;">
    <table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
        <tr><th colspan="7" width="850" style="text-align: center"><strong>INPUT DATA RIWAYAT PEKERJAAN - {$peg.NM_PENGGUNA}</strong></th></tr>
        <tr><td colspan="7" width="850">
                <iframe height="300" scrolling="no" src="insert_pekerjaan.php?id={$peg.ID_PENGGUNA}" width="100%" frameborder="0"></iframe>
            </td></tr>
    </table>
</div>  

<p align="center">
    <span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/up.png" alt="Kembali ke atas" title="Kembali ke atas" onclick="window.scrollTo(0, 0);
            return false" /></span>
</p>
<p><br/>&nbsp;</p>