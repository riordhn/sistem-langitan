<div class="center_title_bar">Rekapitulasi Data Dosen Aktif berdasarkan Pendidikan dalam 5 Tahun Terakhir {foreach item="list" from=$FAK}<li>{$list.FAKULTAS}</li>{/foreach}</div>
<form action="rekap_dosen_pend_5thn_akhir.php" method="get">
<input type="hidden" name="action" value="view">
<p>Pilih Program Studi : 
	<select name="id" id="id">
	<option value="">----------</option>
	<option value='0' style="font-weight: bold; color: #FFF; background-color: #261831;">LIHAT SEMUA</option>
	{foreach item="prodi" from=$KDPRODI}
	{html_options values=$prodi.OPSI output=$prodi.NAMA}
	{/foreach}
	</select>
	<input type="submit" name="View" value="View">
</p>
</form>
	<table  width="850" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
		<tr>
			<th rowspan="2" width="130" style="vertical-align: middle;"><center>Pendidikan</center></th>
			<th width="144" colspan="2"><center>{'Y'|date}</center></th>
			<th width="144" colspan="2"><center>{{'Y'|date}-1}</center></th>
			<th width="144" colspan="2"><center>{{'Y'|date}-2}</center></th>
			<th width="144" colspan="2"><center>{{'Y'|date}-3}</center></th>
			<th width="144" colspan="2"><center>{{'Y'|date}-4}</center></th>
		</tr>
		<tr>
			<th width="72"><center>&Sigma;</center></th>
			<th width="72"><center>&#37;</center></th>
			<th width="72"><center>&Sigma;</center></th>
			<th width="72"><center>&#37;</center></th>
			<th width="72"><center>&Sigma;</center></th>
			<th width="72"><center>&#37;</center></th>
			<th width="72"><center>&Sigma;</center></th>
			<th width="72"><center>&#37;</center></th>
			<th width="72"><center>&Sigma;</center></th>
			<th width="72"><center>&#37;</center></th>
		</tr>
		</thead>
		<tbody>
			{foreach item="jml" from=$JML}
			{foreach item="gol" from=$GOL}
		<tr>
			<td><center>{$gol.NAMA_PENDIDIKAN_AKHIR}</center></td>
			<td class="link" onclick="window.location.href='#rekap_dsn-rekap_dosen_pend_5thn_akhir!list_dosen.php?action=list&id={$smarty.get.id}&mode=p5&thn={'Y'|date}&pdd={$gol.ID_PENDIDIKAN_AKHIR}';" alt="Klik untuk melihat daftar" title="Klik untuk melihat daftar">{$gol.I}</td>
			<td><center>{math equation="((x / y) * 100)" x=$gol.I y=$jml.I format="%.2f"}</center></td>
			<td class="link" onclick="window.location.href='#rekap_dsn-rekap_dosen_pend_5thn_akhir!list_dosen.php?action=list&id={$smarty.get.id}&mode=p5&thn={{'Y'|date}-1}&pdd={$gol.ID_PENDIDIKAN_AKHIR}';" alt="Klik untuk melihat daftar" title="Klik untuk melihat daftar">{$gol.II}</td>
			<td><center>{math equation="((x / y) * 100)" x=$gol.II y=$jml.II format="%.2f"}</center></td>
			<td class="link" onclick="window.location.href='#rekap_dsn-rekap_dosen_pend_5thn_akhir!list_dosen.php?action=list&id={$smarty.get.id}&mode=p5&thn={{'Y'|date}-2}&pdd={$gol.ID_PENDIDIKAN_AKHIR}';" alt="Klik untuk melihat daftar" title="Klik untuk melihat daftar">{$gol.III}</td>
			<td><center>{math equation="((x / y) * 100)" x=$gol.III y=$jml.III format="%.2f"}</center></td>
			<td class="link" onclick="window.location.href='#rekap_dsn-rekap_dosen_pend_5thn_akhir!list_dosen.php?action=list&id={$smarty.get.id}&mode=p5&thn={{'Y'|date}-3}&pdd={$gol.ID_PENDIDIKAN_AKHIR}';" alt="Klik untuk melihat daftar" title="Klik untuk melihat daftar">{$gol.IV}</td>
			<td><center>{math equation="((x / y) * 100)" x=$gol.IV y=$jml.IV format="%.2f"}</center></td>
			<td class="link" onclick="window.location.href='#rekap_dsn-rekap_dosen_pend_5thn_akhir!list_dosen.php?action=list&id={$smarty.get.id}&mode=p5&thn={{'Y'|date}-4}&pdd={$gol.ID_PENDIDIKAN_AKHIR}';" alt="Klik untuk melihat daftar" title="Klik untuk melihat daftar">{$gol.V}</td>
			<td><center>{math equation="((x / y) * 100)" x=$gol.V y=$jml.V format="%.2f"}</center></td>
		</tr>
			{foreachelse}
        <tr><td colspan="11"><em>Data tidak ditemukan</em></td></tr>
			{/foreach}
			{/foreach}
			{foreach item="jml" from=$JML}
		<tr>
			<th><center>&Sigma;</center></th>
			<th colspan="2"><center>{$jml.I}</center></th>
			<th colspan="2"><center>{$jml.II}</center></th>
			<th colspan="2"><center>{$jml.III}</center></th>
			<th colspan="2"><center>{$jml.IV}</center></th>
			<th colspan="2"><center>{$jml.V}</center></th>
		</tr>
			{/foreach}
		</tbody>
	</table>
<p><br/>&nbsp;</p>