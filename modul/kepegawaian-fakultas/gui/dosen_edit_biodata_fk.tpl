{literal}
<script language="javascript">
	var popupWindow=null;
	function popup(mypage,myname,w,h,pos,infocus){
	if (pos == "random")
	{LeftPosition=(screen.width)?Math.floor(Math.random()*(screen.width-w)):100;TopPosition=(screen.height)?Math.floor(Math.random()*((screen.height-h)-75)):100;}
	else
	{LeftPosition=(screen.width)?(screen.width-w)/2:100;TopPosition=(screen.height)?(screen.height-h)/2:100;}
	settings="width="+ w + ",height="+ h + ",top=" + TopPosition + ",left=" + LeftPosition + ",scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no";popupWindow=window.open("",myname,settings);
	if(infocus=="front"){popupWindow.focus();popupWindow.location=mypage;}
	if(infocus=="back"){popupWindow.blur();popupWindow.location=mypage;popupWindow.blur();}
	}
	
$(document).ready(function(){

	$("#nip").blur(function()
	{
		$("#msgbox1").text("Checking...").fadeIn("slow");
		$.post("cek_nip.php",{ nip:$("#nip").val(), id:$("#id_pengguna").val() } ,function(data)
        {
		  if(data=="ada")
		  {
		  	$("#msgbox1").fadeTo(200,0.1,function()
			{ 
			  $(this).html("<font color='red'>NIP/NIK tidak valid, sudah digunakan</font>").fadeTo(900,1);
			});		
          }
		  else
		  {
		  	$("#msgbox1").fadeTo(200,0.1,function()
			{ 
			  $(this).html("").fadeTo(900,1);	
			});
		  }
				
        });
 
	});

	$("#nidn").blur(function()
	{
		$("#msgbox2").text("Checking...").fadeIn("slow");
		$.post("cek_nidn.php",{ nidn:$("#nidn").val(), id:$("#id_dosen").val() } ,function(data)
        {
		  if(data=="ada")
		  {
		  	$("#msgbox2").fadeTo(200,0.1,function()
			{ 
			  $(this).html("<font color='red'>NIDN tidak valid, sudah digunakan</font>").fadeTo(900,1);
			});		
          }
		  else
		  {
		  	$("#msgbox2").fadeTo(200,0.1,function()
			{ 
			  $(this).html("<font color='darkgreen'>NIDN valid, </font><input type='button' name='cek_nidn' value='Cek Data DIKTI' onclick=\"javascript:popup('http://evaluasi.pdpt.dikti.go.id/epsbed/datadosen/"+$("#nidn").val()+"','name','960','435','center','front');\">").fadeTo(900,1);	
			});
		  }
				
        });
 
	});

	$("#serdos").blur(function()
	{
		$("#msgbox3").text("Checking...").fadeIn("slow");
		$.post("cek_serdos.php",{ serdos:$("#serdos").val(), id:$("#id_dosen").val() } ,function(data)
        {
		  if(data=="ada")
		  {
		  	$("#msgbox3").fadeTo(200,0.1,function()
			{ 
			  $(this).html("<font color='red'>Serdos tidak valid, sudah digunakan</font>").fadeTo(900,1);
			});		
          }
		  else
		  {
		  	$("#msgbox3").fadeTo(200,0.1,function()
			{ 
			  $(this).html("").fadeTo(900,1);	
			});
		  }
				
        });
 
	});

});

function submitform()
{
if (!document.update.tlp.value.match( /^[0-9]+$/ )){
	alert ("No. telepon harus angka semua");
	return false;
} else if (!document.update.hp.value.match( /^[0-9]+$/ )){
	alert ("No. hp harus angka semua");
	return false;
} else if (document.update.tlp.value.length < 8){
	alert ("No. telepon terlalu pendek. Masukkan juga kode area.");
	return false;
} else if (document.update.hp.value.length < 9){
	alert ("No. hp terlalu pendek");
	return false;
}
    document.update.submit();
}
</script>
{/literal}

<div class="center_title_bar">Edit Data Dosen</div>
{* biodata dosen *}
<form name="update" id="update" action="dosen_edit_biodata_fk.php" method="post">
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<th width="840"><center><strong>BIODATA</strong></center></th>
		<th width="10"><center><span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/left.png" alt="Kembali" title="Kembali" onclick="javascript:history.go(-1);" /></span></center></th>
	</tr>
</table>
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	{foreach item="dsn" from=$DOSEN}
	<input type="hidden" name="id_dosen" id="id_dosen" value="{$dsn.ID_DOSEN}">
	<input type="hidden" name="id_pengguna" id="id_pengguna" value="{$dsn.ID_PENGGUNA}">
	<input type="hidden" name="action" value="update">
	<tr>
		<td>NIP/NIK</td>
		<td><center>:</center></td>
		{*<td><input type="text" name="nip" id="nip" style="width:200px;" value="{$dsn.USERNAME}"/>&nbsp;&nbsp;&nbsp;<span id="msgbox1" style="display:none"></span></td>*}
		<td>{$dsn.USERNAME}</td>
		<td rowspan="8"><center><img src="{$PHOTO}" border="0" width="160" /></center></td>
	</tr>
	<tr>
		<td>NIDN</td>
		<td><center>:</center></td>
		{if $dsn.NIDN_DOSEN > 0}
		<td><input type="text" name="nidn" id="nidn" style="width:200px;" value="{$dsn.NIDN_DOSEN}"/>&nbsp;&nbsp;&nbsp;<span id="msgbox2" style="display:none"></span></td>
		{else}
		<td><input type="text" name="nidn" id="nidn" style="width:200px;" value=""/></td>&nbsp;&nbsp;&nbsp;<span id="msgbox2" style="display:none"></span>
		{/if}
	</tr>
	<tr>
		<td>SERDOS</td>
		<td><center>:</center></td>
		{if $dsn.SERDOS > 0}
		<td><input type="text" name="serdos" id="serdos" style="width:200px;" value="{$dsn.SERDOS}"/>&nbsp;&nbsp;&nbsp;<span id="msgbox3" style="display:none"></span></td>
		{else}
		<td><input type="text" name="serdos" id="serdos" style="width:200px;" value=""/>&nbsp;&nbsp;&nbsp;<span id="msgbox3" style="display:none"></span></td>
		{/if}
	</tr>
	<tr>
		<td>Departemen</td>
		<td><center>:</center></td>
		<td>
			<select name="dept" id="dept">
				{foreach item="dept" from=$DEPT}
				{html_options values=$dept.ID_DEPARTEMEN output=$dept.NM_DEPARTEMEN selected=$dsn.ID_DEPARTEMEN}
				{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Program Studi</td>
		<td><center>:</center></td>
		<td>
			<select name="prodi" id="prodi">
				{foreach item="pro" from=$PRO}
				{html_options values=$pro.ID_PROGRAM_STUDI output=$pro.NM_PROGRAM_STUDI selected=$dsn.ID_PROGRAM_STUDI_SD}
				{/foreach}
			</select>
		</td>
	</tr>	
	<tr>
		<td>Status Kepegawaian</td>
		<td><center>:</center></td>
		<td>
			<select name="status_dsn" id="status_dsn">
				{foreach item="sts_dsn" from=$STS_DSN}
				{html_options values=$sts_dsn.STATUS_DOSEN output=$sts_dsn.STATUS_DOSEN selected=$dsn.STATUS_DOSEN}
				{/foreach}
			</select>
			<input type="text" name="asal" style="width:400px;" value="{$dsn.ASAL_INSTITUSI}" maxlength="126" />*<br/>
			<font color="gray">*(Diisi asal institusi jika dosen adalah dosen LB atau PENDIDIK KLINIS)</font>
		</td>
	</tr>
	<tr>
		<td>Jabatan Fungsional</td>
		<td><center>:</center></td>
		{if $dsn.NM_JABATAN_FUNGSIONAL != null}
		<td>{$dsn.NM_JABATAN_FUNGSIONAL}</td>
		{else}
		<td>-</td>
		{/if}
	</tr>
{*
	<tr>
		<td>Jabatan Struktural</td>
		<td><center>:</center></td>
		{if $dsn.NM_JABATAN_PEGAWAI != null}
		<td>{$dsn.NM_JABATAN_PEGAWAI}</td>
		{else}
		<td>-</td>
		{/if}
	</tr>
*}
	<tr>
		<td>Status Aktif</td>
		<td><center>:</center></td>
		{if $dsn.ID_STATUS_PENGGUNA != null}
		<td>
			<select name="aktif" id="aktif">
				{foreach item="aktif" from=$STS_AKTIF}
				{html_options values=$aktif.ID_STATUS_PENGGUNA output=$aktif.NM_STATUS_PENGGUNA selected=$dsn.ID_STATUS_PENGGUNA}
				{/foreach}
			</select>
		</td>
		{else}
		<td>
			<select name="aktif" id="aktif">
				<option value=''></option>
				{foreach item="aktif" from=$STS_AKTIF}
				{html_options values=$aktif.ID_STATUS_PENGGUNA output=$aktif.NM_STATUS_PENGGUNA}
				{/foreach}
			</select>
		</td>
		{/if}
	</tr>
	<tr>
		<td>Gelar Depan</td>
		<td><center>:</center></td>
		<td colspan="2"><input type="text" name="gelar_dpn" style="width:200px;" value="{$dsn.GELAR_DEPAN}"/></td>
	</tr>
	<tr>
		<td>Gelar Belakang</td>
		<td><center>:</center></td>
		<td colspan="2"><input type="text" name="gelar_blkg" style="width:200;" value="{$dsn.GELAR_BELAKANG}"/></td>
	</tr>
	<tr>
		<td>Nama Lengkap</td>
		<td><center>:</center></td>
		<td colspan="2"><input type="text" name="nm_lengkap" style="width:450px;" value="{$dsn.NM_PENGGUNA}"/></td>
	</tr>
	<tr>
		<td>Tempat Lahir</td>
		<td><center>:</center></td>
		<td colspan="2">
		{if $dsn.ID_KOTA_LAHIR == null}
                <select name="id_kota_lahir">
					<option value="null" selected="true"></option>
                    {for $i=0 to $count_provinsi}
                    <optgroup label="{$data_kota[$i].nama}">
                        {foreach $data_kota[$i].kota as $data}
                            {html_options values=$data.ID_KOTA output=$data.KOTA}
                        {/foreach}
                    </optgroup>
                    {/for}
                </select>
		{else}
                <select name="id_kota_lahir">
                    {for $i=0 to $count_provinsi}
                    <optgroup label="{$data_kota[$i].nama}">
                        {foreach $data_kota[$i].kota as $data}
                            {html_options values=$data.ID_KOTA output=$data.KOTA selected=$dsn.ID_KOTA_LAHIR}
                        {/foreach}
                    </optgroup>
                    {/for}
                </select>
		{/if}
			&nbsp;, Tanggal Lahir : <input type="text" name="tgl_lahir" id="tgl_lahir" style="text-align:center;" onclick="javascript:NewCssCal('tgl_lahir','ddmmyyyy','','','','','past');" value="{$dsn.TGL_LAHIR_PENGGUNA|date_format:'%d-%m-%Y'}">
		</td>
	</tr>
	<tr>
		<td>Jenis Kelamin</td>
		<td><center>:</center></td>
		<td colspan="2">
			<select name="jk" id="jk">
				{foreach item="jk" from=$JK}
				{html_options values=$jk.KELAMIN_PENGGUNA output=$jk.NM_KELAMIN_PENGGUNA selected=$dsn.KELAMIN_PENGGUNA}
				{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Alamat</td>
		<td><center>:</center></td>
		<td colspan="2"><input type="text" name="alamat" style="width:650px;" value="{$dsn.ALAMAT_RUMAH_DOSEN}" /></td>
	</tr>
	<tr>
		<td>Telepon/HP</td>
		<td><center>:</center></td>
		<td colspan="2"><input type="text" name="tlp" style="text-align:center;" value="{$dsn.TLP_DOSEN}" maxlength="15"/> / <input type="text" name="hp" style="text-align:center;" value="{$dsn.MOBILE_DOSEN}" maxlength="15"/>&nbsp;<font color="gray">(harus angka semua)</font></td>
{*
		{if $dsn.TLP_DOSEN == null}
		<td colspan="2">{$dsn.MOBILE_DOSEN}</td>
		{else if $dsn.MOBILE_DOSEN == null}
		<td colspan="2">{$dsn.TLP_DOSEN}</td>
		{else}
		<td colspan="2">{$dsn.TLP_DOSEN} / {$dsn.MOBILE_DOSEN}</td>
		{/if}
*}
	</tr>
	<tr>
		<td>Email #1</td>
		<td><center>:</center></td>
		<td colspan="2">{$dsn.EMAIL_PENGGUNA}</td>
	</tr>
	<tr>
		<td>Email #2</td>
		<td><center>:</center></td>
		<td colspan="2"><input type="text" name="email" style="width:335px;" value="{$dsn.EMAIL_ALTERNATE}" maxlength="100"/></td>
	</tr>
	<tr>
		<td>NPWP</td>
		<td><center>:</center></td>
		<td colspan="2"><input type="text" name="npwp" style="width:335px;" value="{$dsn.NPWP}" maxlength="100"/></td>
	</tr>
	<tr>
		<td>Rekening Bank</td>
		<td><center>:</center></td>
		<td colspan="2"><input type="text" name="rekening" style="width:335px;" value="{$dsn.REKENING}" maxlength="100"/></td>
	</tr>
	{/foreach}
	<tr>
		<td colspan="4" style="text-align: right;width: 850">
			<span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/save.png" alt="Simpan" title="Simpan" onclick="javascript:submitform();" /></span>
			<span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/cancel.png" alt="Batal" title="Batal" onclick="javascript:history.go(-1);" /></span>
		</td>
	</tr>
</table>
</form>
<p><br/>&nbsp;</p>