<div class="center_title_bar"> Rekapitulasi Dosen </div>
<table width="850" class="tableabout">
  <tr>
    <th width="38">No</th>
    <th width="178">Sub menu</th>
    <th width="634">Uraian</th>
  </tr>
  <tbody>
  <tr>
    <td><center>1</center></td>
    <td>Gol/Usia</td>
    <td>Difungsikan untuk menampilkan rekapitulasi data Dosen Aktif berdasarkan Golongan dan Usia Dosen di Fakultas atau Program Studi</td>
  </tr>
  <tr class="odd">
    <td><center>2</center></td>
    <td>Gol/Pend</td>
    <td>Difungsikan untuk menampilkan rekapitulasi data Dosen Aktif berdasarkan Golongan dan Pendidikan Akhir Dosen di Fakultas atau Program Studi</td>
  </tr>
   <tr>
    <td><center>3</center></td>
    <td>Gol/Jab</td>
    <td>Difungsikan untuk menampilkan rekapitulasi data Dosen Aktif berdasarkan Golongan dan Jabatan Fungsional Dosen di Fakultas atau Program Studi</td>
  </tr>
  <tr class="odd">
    <td><center>4</center></td>
    <td>Jab/Usia</td>
    <td>Difungsikan untuk menampilkan rekapitulasi data Dosen Aktif berdasarkan Jabatan Fungsional dan Usia Dosen di Fakultas atau Program Studi</td>
  </tr>
  <tr>
    <td><center>5</center></td>
    <td>Jab/Pend</td>
    <td>Difungsikan untuk menampilkan rekapitulasi data Dosen Aktif berdasarkan Jabatan Fungsional dan Pendidikan Akhir Dosen di Fakultas atau Program Studi</td>
  </tr>
  <tr class="odd">
    <td><center>6</center></td>
    <td>Jab - Usia/Pend</td>
    <td>Difungsikan untuk menampilkan rekapitulasi data Dosen Aktif berdasarkan Jabatan Fungsional, Usia dan Pendidikan Akhir Dosen di Universitas atau Fakultas atau Program Studi</td>
  </tr>
  <tr>
    <td><center>7</center></td>
    <td>Pend/Usia</td>
    <td>Difungsikan untuk menampilkan rekapitulasi data Dosen Aktif berdasarkan Pendidikan Akhir dan Usia Dosen di Fakultas atau Program Studi</td>
  </tr>
  <tr class="odd">
    <td><center>7</center></td>
    <td>Pend 5 Thn Terakhir</td>
    <td>Difungsikan untuk menampilkan rekapitulasi data Dosen Aktif berdasarkan Pendidikan Akhir Dosen di Fakultas atau Program Studi dalam 5 Tahun Terakhir</td>
  </tr>
  </tbody>
</table>