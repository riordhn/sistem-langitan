{literal}
    <script language="javascript">
        var popupWindow = null;
        function popup(mypage, myname, w, h, pos, infocus) {
            if (pos == "random")
            {
                LeftPosition = (screen.width) ? Math.floor(Math.random() * (screen.width - w)) : 100;
                TopPosition = (screen.height) ? Math.floor(Math.random() * ((screen.height - h) - 75)) : 100;
            }
            else
            {
                LeftPosition = (screen.width) ? (screen.width - w) / 2 : 100;
                TopPosition = (screen.height) ? (screen.height - h) / 2 : 100;
            }
            settings = "width=" + w + ",height=" + h + ",top=" + TopPosition + ",left=" + LeftPosition + ",scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no";
            popupWindow = window.open("", myname, settings);
            if (infocus == "front") {
                popupWindow.focus();
                popupWindow.location = mypage;
            }
            if (infocus == "back") {
                popupWindow.blur();
                popupWindow.location = mypage;
                popupWindow.blur();
            }
        }

        $(document).ready(function() {

            $("#nip").blur(function()
            {
                $("#msgbox1").text("Checking...").fadeIn("slow");
                $.post("cek_nip.php", {nip: $("#nip").val(), id: $("#id_pengguna").val()}, function(data)
                {
                    if (data == "ada")
                    {
                        $("#msgbox1").fadeTo(200, 0.1, function()
                        {
                            $(this).html("<font color='red'>NIP/NIK tidak valid, sudah digunakan</font>").fadeTo(900, 1);
                        });
                    }
                    else
                    {
                        $("#msgbox1").fadeTo(200, 0.1, function()
                        {
                            $(this).html("").fadeTo(900, 1);
                        });
                    }

                });

            });

            $("#nidn").blur(function()
            {
                $("#msgbox2").text("Checking...").fadeIn("slow");
                $.post("cek_nidn.php", {nidn: $("#nidn").val(), id: $("#id_dosen").val()}, function(data)
                {
                    if (data == "ada")
                    {
                        $("#msgbox2").fadeTo(200, 0.1, function()
                        {
                            $(this).html("<font color='red'>NIDN tidak valid, sudah digunakan</font>").fadeTo(900, 1);
                        });
                    }
                    else
                    {
                        $("#msgbox2").fadeTo(200, 0.1, function()
                        {
                            $(this).html("<font color='darkgreen'>NIDN valid, </font><input type='button' name='cek_nidn' value='Cek Data DIKTI' onclick=\"javascript:popup('http://evaluasi.pdpt.dikti.go.id/epsbed/datadosen/" + $("#nidn").val() + "','name','960','435','center','front');\">").fadeTo(900, 1);
                        });
                    }

                });

            });

            $("#serdos").blur(function()
            {
                $("#msgbox3").text("Checking...").fadeIn("slow");
                $.post("cek_serdos.php", {serdos: $("#serdos").val(), id: $("#id_dosen").val()}, function(data)
                {
                    if (data == "ada")
                    {
                        $("#msgbox3").fadeTo(200, 0.1, function()
                        {
                            $(this).html("<font color='red'>Serdos tidak valid, sudah digunakan</font>").fadeTo(900, 1);
                        });
                    }
                    else
                    {
                        $("#msgbox3").fadeTo(200, 0.1, function()
                        {
                            $(this).html("").fadeTo(900, 1);
                        });
                    }

                });

            });

        });

        function ambil(KDFAK) {
            $.ajax({
                type: "POST",
                url: "dosen_edit_biodata.php?action=edit_biodata&id=" + $("#id_dosen").val(),
                data: "kdfak=" + $("#kdfak").val(),
                cache: false,
                success: function(data) {
                    $('#content').html(data);
                }
            });
        }

        function submitform()
        {
            document.update.submit();
        }
    </script>
{/literal}

<div class="center_title_bar">Edit Data Dosen</div>
{* biodata dosen *}
<form name="update" id="update" action="dosen_edit_biodata.php" method="post">
    <table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <th width="840"><center><strong>BIODATA</strong></center></th>
        <th width="10"><center><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/left.png" alt="Kembali" title="Kembali" onclick="javascript:history.go(-1);" /></span></center></th>
        </tr>
    </table>
    <table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
        {foreach item="dsn" from=$DOSEN}
            <input type="hidden" name="id_dosen" id="id_dosen" value="{$dsn.ID_DOSEN}">
            <input type="hidden" name="id_pengguna" id="id_pengguna" value="{$dsn.ID_PENGGUNA}">
            <input type="hidden" name="action" value="update">
            <tr>
                <td>NIP/NIK</td>
                <td style="text-align:center"></td>
                <td><input type="text" name="nip" id="nip" style="width:200px;" value="{$dsn.USERNAME}"/>&nbsp;&nbsp;&nbsp;<span id="msgbox1" style="display:none"></span></td>
                <td rowspan="9"><center><img src="{$PHOTO}" border="0" width="160" /><br><br><input type="button" name="upload" value="Upload Photo" onclick=""></center></td>
            </tr>
            <tr>
                <td>NIP/NIK LAMA</td>
                <td style="text-align:center"></td>
                <td><input  type="text" name="nip_lama" style="width:200px;" value="{$dsn.NIP_LAMA}"/></td>
            </tr>
            <tr>
                <td>NIDN</td>
                <td style="text-align:center"></td>
                {if $dsn.NIDN_DOSEN > 0}
                    <td><input type="text" name="nidn" id="nidn" style="width:200px;" value="{$dsn.NIDN_DOSEN}"/>&nbsp;&nbsp;&nbsp;<span id="msgbox2" style="display:none"></span></td>
                    {else}
                    <td><input type="text"  name="nidn" id="nidn" style="width:200px;" value=""/></td>&nbsp;&nbsp;&nbsp;<span id="msgbox2" style="display:none"></span>
                    {/if}
            </tr>
            <tr>
                <td>SERDOS</td>
                <td style="text-align:center"></td>
                {if $dsn.SERDOS > 0}
                    <td><input type="text" name="serdos" id="serdos" style="width:200px;" value="{$dsn.SERDOS}"/>&nbsp;&nbsp;&nbsp;<span id="msgbox3" style="display:none"></span></td>
                    {else}
                    <td><input type="text" name="serdos" id="serdos" style="width:200px;" value=""/>&nbsp;&nbsp;&nbsp;<span id="msgbox3" style="display:none"></span></td>
                    {/if}
            </tr>
            <tr>
                <td>Fakultas</td>
                <td style="text-align:center"></td>
                <td>
                    <select name="kdfak" id="kdfak" onChange="ambil()">
                        {foreach item="fak" from=$T_FAK}
                            {html_options values=$fak.ID_FAKULTAS output=$fak.NM_FAKULTAS selected=$FAKGET}
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td>Program Studi</td>
                <td style="text-align:center"></td>
                <td>
                    <select  name="prodi" id="prodi">
                        {foreach item="pro" from=$PRO}
                            {html_options values=$pro.ID_PROGRAM_STUDI output=$pro.NM_PROGRAM_STUDI selected=$dsn.ID_PROGRAM_STUDI_SD}
                        {/foreach}
                    </select>
                </td>
            </tr>	
            <tr>
                <td>Status Kepegawaian</td>
                <td style="text-align:center"></td>
                <td>
                    <select name="status_dsn" id="status_dsn">
                        <option value="PNS" {if $dsn.STATUS_DOSEN=='PNS'}selected="true"{/if}>PNS</option>
                        <option value="CPNS" {if $dsn.STATUS_DOSEN=='CPNS'}selected="true"{/if}>CPNS</option>
                        <option value="HONORER" {if $dsn.STATUS_DOSEN=='HONORER'}selected="true"{/if}>HONORER</option>
                        <option value="KONTRAK" {if $dsn.STATUS_DOSEN=='KONTRAK'}selected="true"{/if}>KONTRAK</option>
                        <option value="GB UNAIR" {if $dsn.STATUS_DOSEN=='GB UNAIR'}selected="true"{/if}>GB UNAIR</option>
                        <option value="GB EMIRITUS" {if $dsn.STATUS_DOSEN=='GB EMIRITUS'}selected="true"{/if}>GB EMIRITUS</option>
                        <option value="LB" {if $dsn.STATUS_DOSEN=='LB'}selected="true"{/if}>LB</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Prajabatan Nomor</td>
                <td style="text-align:center"></td>
                <td><input  type="text" name="prajab_nomor" style="width:200px;" value="{$dsn.PRAJAB_NOMOR}"/></td>
            </tr>
            <tr>
                <td>Prajabatan Tanggal</td>
                <td style="text-align:center"></td>
                <td><input  type="text" name="prajab_tanggal" id="prajab_tanggal" style="width:200px;"  onclick="javascript:NewCssCal('prajab_tanggal', 'ddmmyyyy', '', '', '', '', 'past');" value="{$dsn.PRAJAB_TANGGAL|date_format:'%d-%m-%Y'}"/></td>
            </tr>
            <tr>
                <td>TGL Sumpah PNS</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input  type="text" name="tgl_sumpah_pns" id="tgl_sumpah_pns" style="width:200px;"  onclick="javascript:NewCssCal('tgl_sumpah_pns', 'ddmmyyyy', '', '', '', '', 'past');" value="{$dsn.TGL_SUMPAH_PNS|date_format:'%d-%m-%Y'}"/></td>
            </tr>
            <tr>
                <td>TMT CPNS</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input  type="text" name="tmt_cpns" id="tmt_cpns" style="width:200px;"  onclick="javascript:NewCssCal('tmt_cpns', 'ddmmyyyy', '', '', '', '', 'past');" value="{$dsn.tmt_cpns|date_format:'%d-%m-%Y'}"/></td>
            </tr>
            <tr>
                <td>Nomer Karpeg</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input  type="text" name="nomor_karpeg" style="width:500px;" value="{$dsn.NOMER_KARPEG}"/></td>
            </tr>
             <tr>
                <td>Nomer NPWP</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input  type="text" name="nomor_npwp" style="width:500px;" value="{$dsn.NOMOR_NPWP}"/></td>
            </tr>
             <tr>
                <td>Taspen</td>
                <td style="text-align:center"></td>
                <td colspan="2">
                    <select name="taspen">
                        <option value="1" {if $dsn.TASPEN==1}selected="true"{/if}>Sudah</option>
                        <option value="0" {if $dsn.TASPEN==0}selected="true"{/if}>Belum</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Pangkat (Gol.) Terakhir / TMT</td>
                <td style="text-align:center"></td>
                {if $dsn.NM_GOLONGAN != null}
                    <td colspan="2" >{$dsn.NM_GOLONGAN} - {$dsn.NM_PANGKAT}  / {$dsn.TMT_GOLONGAN}</td>
                {else}
                    <td colspan="2" >-</td>
                {/if}
            </tr>
            <tr>
                <td>Unit Esselon I</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input  type="text" name="unit_esselon1" style="width:500px;" value="{$dsn.UNIT_ESSELON_I}"/></td>
            </tr>
            <tr>
                <td>Unit Esselon II</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input  type="text" name="unit_esselon2" style="width:500px;" value="{$dsn.UNIT_ESSELON_II}"/></td>
            </tr>
            <tr>
                <td>Unit Esselon III</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input  type="text" name="unit_esselon3" style="width:500px;" value="{$dsn.UNIT_ESSELON_III}"/></td>
            </tr>
            <tr>
                <td>Unit Esselon IV</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input  type="text" name="unit_esselon4" style="width:500px;" value="{$dsn.UNIT_ESSELON_IV}"/></td>
            </tr>
            <tr>
                <td>Unit Esselon V</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input  type="text" name="unit_esselon5" style="width:500px;" value="{$dsn.UNIT_ESSELON_V}"/></td>
            </tr>
            <tr>
                <td>Jabatan Fungsional / TMT </td>
                <td style="text-align:center"></td>
                {if $dsn.NM_JABATAN_FUNGSIONAL != null}
                    <td colspan="2" >{$dsn.NM_JABATAN_FUNGSIONAL}  / {$dsn.TMT_JAB_FUNGSIONAL} </td>
                {else}
                    <td colspan="2" >-</td>
                {/if}
            </tr>
            <tr>
                <td>Tugas Tambahan</td>
                <td style="text-align:center"></td>
                {if $dsn.NM_JABATAN_PEGAWAI != null}
                    <td colspan="2" >{$dsn.NM_JABATAN_PEGAWAI}</td>
                {else}
                    <td colspan="2" >-</td>
                {/if}
            </tr>
            <tr>
                <td>Pendidikan Akhir</td>
                <td style="text-align:center"></td>
                {if $dsn.NAMA_PENDIDIKAN_AKHIR != null}
                    <td colspan="2" >{$dsn.NAMA_PENDIDIKAN_AKHIR} {$dsn.NM_SEKOLAH_PENDIDIKAN} {$dsn.NM_JURUSAN_PENDIDIKAN} ({$dsn.TAHUN_MASUK_PENDIDIKAN}-{$dsn.TAHUN_LULUS_PENDIDIKAN})</td>
                {else}
                    <td colspan="2" >-</td>
                {/if}
            </tr>
            <tr>
                <td>Status Aktif</td>
                <td style="text-align:center"></td>
                {if $dsn.ID_STATUS_PENGGUNA != null}
                    <td colspan="2" >
                        <select name="aktif" id="aktif">
                            {foreach item="aktif" from=$STS_AKTIF}
                                {html_options values=$aktif.ID_STATUS_PENGGUNA output=$aktif.NM_STATUS_PENGGUNA selected=$dsn.ID_STATUS_PENGGUNA}
                            {/foreach}
                        </select>
                    </td>
                {else}
                    <td colspan="2" >
                        <select name="aktif" id="aktif">
                            <option value=''></option>
                            {foreach item="aktif" from=$STS_AKTIF}
                                {html_options values=$aktif.ID_STATUS_PENGGUNA output=$aktif.NM_STATUS_PENGGUNA}
                            {/foreach}
                        </select>
                    </td>
                {/if}
            </tr>
            <tr>
                <td>Gelar Depan</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input  type="text" name="gelar_dpn" style="width:200px;" value="{$dsn.GELAR_DEPAN}"/></td>
            </tr>
            <tr>
                <td>Gelar Belakang</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input  type="text" name="gelar_blkg" style="width:200;" value="{$dsn.GELAR_BELAKANG}"/></td>
            </tr>
            <tr>
                <td>Nama Lengkap</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input type="text" name="nm_lengkap" style="width:450px;" value="{$dsn.NM_PENGGUNA}"/></td>
            </tr>
            <tr>
                <td>Tempat Lahir</td>
                <td style="text-align:center"></td>
                <td colspan="2">
                    {if $dsn.ID_KOTA_LAHIR == null}
                        <select name="id_kota_lahir">
                            <option value="null" selected="true"></option>
                            {for $i=0 to $count_provinsi}
                                <optgroup label="{$data_kota[$i].nama}">
                                    {foreach $data_kota[$i].kota as $data}
                                        {html_options values=$data.ID_KOTA output=$data.KOTA}
                                    {/foreach}
                                </optgroup>
                            {/for}
                        </select>
                    {else}
                        <select name="id_kota_lahir">
                            {for $i=0 to $count_provinsi}
                                <optgroup label="{$data_kota[$i].nama}">
                                    {foreach $data_kota[$i].kota as $data}
                                        {html_options values=$data.ID_KOTA output=$data.KOTA selected=$dsn.ID_KOTA_LAHIR}
                                    {/foreach}
                                </optgroup>
                            {/for}
                        </select>
                    {/if}
                    &nbsp;, Tanggal Lahir : <input type="text" name="tgl_lahir" id="tgl_lahir" style="text-align:center;" onclick="javascript:NewCssCal('tgl_lahir', 'ddmmyyyy', '', '', '', '', 'past');" value="{$dsn.TGL_LAHIR_PENGGUNA|date_format:'%d-%m-%Y'}">
                </td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                <td style="text-align:center"></td>
                {if $dsn.KELAMIN_PENGGUNA == '1' || $dsn.KELAMIN_PENGGUNA == '2'}
                    <td colspan="2">
                        <select name="jk" id="jk">
                            {foreach item="jk" from=$JK}
                                {html_options values=$jk.KELAMIN_PENGGUNA output=$jk.NM_KELAMIN_PENGGUNA selected=$dsn.KELAMIN_PENGGUNA}
                            {/foreach}
                        </select>
                    </td>
                {else}
                    <td colspan="2">
                        <select name="jk" id="jk">
                            <option value=''></option>
                            {foreach item="jk" from=$JK}
                                {html_options values=$jk.KELAMIN_PENGGUNA output=$jk.NM_KELAMIN_PENGGUNA}
                            {/foreach}
                        </select>
                    </td>
                {/if}
            </tr>
            <tr>
                <td>Status Pernikahan</td>
                <td style="text-align:center"></td>
                {if $dsn.ID_STATUS_PERNIKAHAN != null}
                    <td colspan="2" >
                        <select name="status_nikah" id="agama">
                            {foreach item="sn" from=$SNIKAH}
                                {html_options values=$sn.ID_STATUS_PERNIKAHAN output=$sn.NM_STATUS_PERNIKAHAN selected=$dsn.ID_STATUS_PERNIKAHAN}
                            {/foreach}
                        </select>
                    </td>
                {else}
                    <td colspan="2" >
                        <select name="status_nikah" id="agama">
                            {foreach item="sn" from=$SNIKAH}
                                {html_options values=$sn.ID_STATUS_PERNIKAHAN output=$sn.NM_STATUS_PERNIKAHAN}
                            {/foreach}
                        </select>
                    </td>
                {/if}
            </tr>
            <tr>
                <td>Agama</td>
                <td style="text-align:center"></td>
                {if $dsn.ID_AGAMA != ''}
                    <td colspan="2">
                        <select name="agama" id="jk">
                            {foreach item="ag" from=$AGAMA}
                                {html_options values=$ag.ID_AGAMA output=$ag.NM_AGAMA selected=$dsn.ID_AGAMA}
                            {/foreach}
                        </select>
                    </td>
                {else}
                    <td colspan="2">
                        <select name="agama" id="jk">
                            {foreach item="ag" from=$AGAMA}
                                {html_options values=$ag.ID_AGAMA output=$ag.NM_AGAMA}
                            {/foreach}
                        </select>
                    </td>
                {/if}
            </tr>
            <tr>
                <td>Alamat</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input type="text" name="alamat" style="width:650px;" value="{$dsn.ALAMAT_RUMAH_DOSEN}" /></td>
            </tr>
            <tr>
                <td>Kode Pos</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input type="text" name="kode_pos" style="width:50px;" value="{$dsn.KODE_POS}" /></td>
            </tr>
            <tr>
                <td>Telepon/HP</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input type="text" name="tlp" style="text-align:center;" value="{$dsn.TLP_DOSEN}" maxlength="15"/> / <input type="text" name="hp" style="text-align:center;" value="{$dsn.MOBILE_DOSEN}" maxlength="15"/></td>
            </tr>
            <tr>
                <td>Email #1</td>
                <td style="text-align:center"></td>
                <td colspan="2">{$dsn.EMAIL_PENGGUNA}</td>
            </tr>
            <tr>
                <td>Email #2</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input type="text" name="email" style="width:335px;" value="{$dsn.EMAIL_ALTERNATE}" maxlength="100"/></td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="4" style="text-align: right;width: 850">
                <span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/save.png" alt="Simpan" title="Simpan" onclick="javascript:submitform();" /></span>
                <span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/cancel.png" alt="Batal" title="Batal" onclick="javascript:history.go(-1);" /></span>
            </td>
        </tr>
    </table>
</form>
<p><br/>&nbsp;</p>