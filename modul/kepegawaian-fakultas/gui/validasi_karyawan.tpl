<div class="center_title_bar">Validasi Data Karyawan</div>
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
        <tr>
			<th>NIP/NIK</td>
			<th>Nama Karyawan</td>
			<th>Status</td>
			<th class="noheader">Aksi</td>
		</tr>
		</thead>
		<tbody>
			{foreach name=test item="list" from=$PEGAWAI}
		<tr class="row">
			<td>{$list.NIP_PEGAWAI}</td>
			<td>{$list.NM_PENGGUNA}</td>
			{if $list.STATUS_VALID == '1'}
			<td>Sudah divalidasi</td>
			{else}
			<td><font color="red"><b>Belum divalidasi</b></font></td>
			{/if}
			<td><center><span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/search.png" alt="Detail" title="Detail" onclick="window.location.href='#data_karyawan-cari_karyawan!karyawan_detail.php?action=detail&id={$list.ID_PENGGUNA}';"/></span></center></td>
		</tr>
			{/foreach}
		</tbody>
	</table>
<p align="center">
<span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/up.png" alt="Kembali ke atas" title="Kembali ke atas" onclick="window.scrollTo(0,0); return false" /></span>
</p>
<p><br/>&nbsp;</p>