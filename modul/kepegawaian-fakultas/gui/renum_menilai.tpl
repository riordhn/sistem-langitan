<div class="center_title_bar">RENUM PENILAIAN PEGAWAI</div>

{if isset($smarty.request.edit)}




{literal}    
<script type="text/javascript">



	function getnilai(i){
		
			$.ajax({


				type: "POST",
				url: "getnilai.php",
				data: 'jawaban='+$("#jawaban"+i).val(),
				success: function(data) {	
					$("#nilai"+i).html(data);
				} 
			});
			
	}


</script>
{/literal}

<form method="post" action="renum_menilai.php?edit={$smarty.get.edit}">



<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<th width="840" colspan="3"><center><strong>BIODATA</strong></center></th>
		<th width="10"><center><span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/left.png" alt="Kembali" title="Kembali" onclick="javascript:history.go(-1);" /></span></center></th>
	</tr>
	{foreach item="peg" from=$PEGAWAI}
	<tr>
		<td>NIP/NIK</td>
		<td><center>:</center></td>
		<td>{$peg.USERNAME}</td>
		<td rowspan="10"><center><img src="{$PHOTO}" border="0" width="160" /><br/><br/></center></td>
	</tr>
	<tr>
		<td>Unit Kerja</td>
		<td><center>:</center></td>
		<td>{$peg.NM_UNIT_KERJA}</td>
	</tr>
	<tr>
		<td>Status Kepegawaian</td>
		<td><center>:</center></td>
		<td>{$peg.STATUS_PEGAWAI} ( {$peg.NM_GOLONGAN} - {$peg.NM_PANGKAT} )</td>
	</tr>
	<tr>
		<td>Jabatan Struktural</td>
		<td><center>:</center></td>
		<td>{$peg.NM_JABATAN_PEGAWAI}</td>
	</tr>
	<tr>
		<td>Status Aktif</td>
		<td><center>:</center></td>
		<td>{$peg.NM_STATUS_PENGGUNA}</td>
	</tr>
	<tr>
		<td>Gelar Depan</td>
		<td><center>:</center></td>
		<td>{$peg.GELAR_DEPAN}</td>
	</tr>
	<tr>
		<td>Gelar Belakang</td>
		<td><center>:</center></td>
		<td>{$peg.GELAR_BELAKANG}</td>
	</tr>
	<tr>
		<td>Nama Lengkap</td>
		<td><center>:</center></td>
		<td>{$peg.NM_PENGGUNA}</td>
	</tr>
	<tr>
		<td>Tempat, Tanggal Lahir</td>
		<td><center>:</center></td>
		<td>{$peg.TEMPAT_LAHIR}, {$peg.TGL_LAHIR_PENGGUNA}</td>
	</tr>
	<tr>
		<td>Jenis Kelamin</td>
		<td><center>:</center></td>
		{if $peg.KELAMIN_PENGGUNA=='1'}
		<td>LAKI-LAKI</td>
		{elseif $peg.KELAMIN_PENGGUNA=='2'}
		<td>PEREMPUAN</td>
		{else}
		<td>-</td>
		{/if}
	</tr>
	<tr>
		<td>Alamat</td>
		<td><center>:</center></td>
		<td colspan="2">{$peg.ALAMAT_RUMAH_PEGAWAI}</td>
	</tr>
	<tr>
		<td>No. HP</td>
		<td><center>:</center></td>
		<td colspan="2">{$peg.TELP_PEGAWAI} {*{if $peg.TELP_PEGAWAI != ''}<span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/sms.png" alt="Kirim SMS" title="Kirim SMS" onclick="javascript:popup('{$LINK}','name','600','400','center','front');" /></span>{else}{/if}*}</td>
	</tr>
	<tr>
		<td>Email #1</td>
		<td><center>:</center></td>
		<td colspan="2">{$peg.EMAIL_PENGGUNA}</td>
	</tr>
	<tr>
		<td>Email #2</td>
		<td><center>:</center></td>
		<td colspan="2">{$peg.EMAIL_ALTERNATE}</td>
	</tr>
	{/foreach}
</table>


<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<th width="840" colspan="13"><center><strong>KEDISIPLINAN</strong></center></th>
	</tr>
    <tr>
      <td align=right>1&nbsp;</td>
      <td>Tidak Masuk Kerja:</td>
      <td>a. Tanpa Ijin</td>
      <td>&nbsp;<input name="tanpa_ijin" type="text" size="3" value=""></td>
      <td></td>
      <td>b. Dengan Ijin</td>
      <td>&nbsp;<input name="dengan_ijin" type="text" size="3" value="">&nbsp;hari</td>
    </tr>
    <tr>
      <td align=right>2&nbsp;</td>
      <td>Masuk Kerja Terlambat:</td>
      <td></td>
      <td>&nbsp;<input name="terlambat" type="text" size="3" value=""></td>
      <td></td>
      <td>Pulang Lebih Awal</td>
      <td>&nbsp;<input name="plg_awal" type="text" size="3" value="">&nbsp;hari</td>
    </tr>
    <tr>
      <td align=right>3&nbsp;</td>
      <td>Meninggalkan tugas/pek:</td>
      <td></td>
      <td>&nbsp;<input name="meninggalkan_tugas" type="text" size="3" value=""></td>
      <td></td>
      <td>Berbisnis saat jam kerja</td>
      <td>&nbsp;<input name="berbisnis" type="text" size="3" value="">&nbsp;hari</td>
    </tr>
    <tr>
      <td align=right>4&nbsp;</td>
      <td colspan="5">Menerima/meminta imbalan kepanitian yang tdk sesuai ketentuan:</td>
      <td>&nbsp;<input name="berbisnis" type="text" size="3" value="">&nbsp;Kegiatan</td>
    </tr>

	<tr>
		<th width="840" colspan="13"><center><strong>SIKAP DAN PERILAKU DALAM PEKERJAAN</strong></center></th>
	</tr>
    {$no = 1}
    {foreach $query1 as $q1}
    <tr>
		<td>{$no++}. </td>
        <td colspan="4">{$q1.NM_KATEGORI_DISIPLIN}</td>
        <td>
        	<select id="jawaban{$no}" name="jawaban" onchange="getnilai({$no})">
			<option value="">-</option>';
				{foreach $jawaban as $data}
				 	<option value="{$data.KODE_JAWABAN}">{$data.JAWABAN}</option>
				{/foreach}
			</select>
        </td>
        <td>
        	<select id="nilai{$no}" class="nilai" name="nilai">
                <option value="" selected="selected"></option>
              </select>
        </td>
    </tr>
    {/foreach}    
  <tr>
    <th colspan="12" width="840" ><center><strong>CATATAN</strong></center></th>
  </tr>
  <tr>
    <td width="40" >No</td>
    <td width="800"  colspan="7">Catatan</td>
  </tr>
  <tr>
    <td>*</td>
    <td colspan="7"><textarea name="catatan" cols="90" rows="3"></textarea></td>
  </tr>
  <tr>
    <td colspan="17"><center><input type="submit" value="Simpan" name="simpan" /><input type="button" value="Batal" onclick="javascript:history.go(-1);" /></center></td>
  </tr>
</table>
</form>
<br />
<br />
<br />

    

{else}

{$isi}

{/if}