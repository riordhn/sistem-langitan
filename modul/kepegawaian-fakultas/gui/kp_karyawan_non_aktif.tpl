<div class="center_title_bar">Data Karyawan Non-Aktif {foreach item="list" from=$UK}<li>{$list.UNIT_KERJA}</li>{/foreach}</div>
<form action="kp_karyawan_non_aktif.php" method="get">
<p>Pilih Unit Kerja : 
	<select name="id" id="id">
	<option value="">----------</option>
	<option value='%' style="font-weight: bold; color: #FFF; background-color: #261831;">LIHAT SEMUA</option>
	{foreach item="prodi" from=$KD_PRODI}
	{html_options values=$prodi.ID_UNIT_KERJA output=$prodi.NM_PROGRAM_STUDI}
	{/foreach}
	</select>
	<input type="submit" name="View" value="View">
</p>
</form>
{if $smarty.get.id == ''}
<iframe scrolling="no" src="proses/graph_karyawan_non_aktif.php" width="100%" height="420" frameborder="0"></iframe>
{else}
{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
            3: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
        <tr>
			<th>NIP</th>
			<th>Nama Karyawan</th>
			<th>Unit Kerja</th>
			<th class="noheader">Aksi</th>
		</tr>
		</thead>
		<tbody>
			{foreach item="peg" from=$PEGAWAI}
		<tr class="row">
			<td>{$peg.USERNAME}</td>
			<td>{$peg.NM_PENGGUNA}</td>
			<td>{$peg.NM_UNIT_KERJA}</td>
			<td><center><span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/search.png" alt="Detail" title="Detail" onclick="window.location.href='#data_karyawan-karyawan_non_aktif!karyawan_detail.php?action=detail&id={$peg.ID_PENGGUNA}';"/></span></center></td>
		</tr>
			{foreachelse}
        <tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
			{/foreach}
		</tbody>
	</table>
{/if}
{if $DATA > 10}
<p align="center">
<span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/up.png" alt="Kembali ke atas" title="Kembali ke atas" onclick="window.scrollTo(0,0); return false" /></span>
</p>
{else}
{/if}
<p><br/>&nbsp;</p>