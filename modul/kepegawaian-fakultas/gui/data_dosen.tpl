<div class="center_title_bar"> Data Dosen </div>
<table width="850" class="tableabout">
  <tr>
    <th width="38">No</th>
    <th width="178">Sub menu</th>
    <th width="634">Uraian</th>
  </tr>
  <tbody>
  <tr>
    <td><center>1</center></td>
    <td>Dosen Tetap</td>
    <td>Difungsikan untuk menampilkan data Dosen Tetap di Fakultas</td>
  </tr>
  <tr class="odd">
    <td><center>2</center></td>
    <td>Dosen Kontrak</td>
    <td>Difungsikan untuk menampilkan data Dosen Kontrak di Fakultas</td>
  </tr>
  <tr>
    <td><center>3</center></td>
    <td>Dosen LB</td>
    <td>Difungsikan untuk menampilkan data Dosen Luar Biasa di Fakultas</td>
  </tr>
  <tr class="odd">
    <td><center>4</center></td>
    <td>Dosen No Status</td>
    <td>Difungsikan untuk menampilkan data Dosen yang belum diset status kepegawaiannya di Fakultas</td>
  </tr>
  <tr>
    <td><center>5</center></td>
    <td>Dosen No NIDN</td>
    <td>Difungsikan untuk menampilkan data Dosen yang belum memiliki NIDN di Fakultas</td>
  </tr>
  <tr class="odd">
    <td><center>6</center></td>
    <td>Dosen No Serdos</td>
    <td>Difungsikan untuk menampilkan data Dosen yang belum memiliki Sertifikasi Dosen di Fakultas</td>
  </tr>
  <tr>
    <td><center>7</center></td>
    <td>Dosen Non Aktif</td>
    <td>Difungsikan untuk menampilkan data Dosen non-aktif (Pensiun, Undur Diri, dll) di Fakultas</td>
  </tr>
  <tr class="odd">
    <td><center>8</center></td>
    <td>Cari Dosen</td>
    <td>Difungsikan untuk mencari data Dosen</td>
  </tr>
  </tbody>
</table>