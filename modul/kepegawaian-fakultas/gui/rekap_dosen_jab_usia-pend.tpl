<div class="center_title_bar">Rekapitulasi Data Dosen Aktif berdasarkan Usia, Tingkat Pendidikan dan Jabatan Fungsional {foreach item="list" from=$FAK}<li>{$list.FAKULTAS}</li>{/foreach}</div>
<form action="rekap_dosen_jab_usia-pend.php" method="get">
<input type="hidden" name="action" value="view">
<p>Pilih Program Studi : 
	<select name="id" id="id">
	<option value="">----------</option>
	<option value='0' style="font-weight: bold; color: #FFF; background-color: #261831;">LIHAT SEMUA</option>
	{foreach item="prodi" from=$KDPRODI}
	{html_options values=$prodi.OPSI output=$prodi.NAMA}
	{/foreach}
	</select>
	<input type="submit" name="View" value="View">
</p>
</form>
	<table width="850" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
        <tr>
			<th rowspan="3" width="90" style="vertical-align: middle;"><center>Jabatan<br>Fungsional</center></th>
			<th colspan="15" width="720"><center>Kelompok Usia (tahun)</center></th>
			<th colspan="2" width="40"><center>Jumlah</center></th>
		</tr>
		<tr>
			<th width="90" colspan="3"><center>&#60; 31</center></th>
			<th width="90" colspan="3"><center>31 &#8211; 40</center></th>
			<th width="90" colspan="3"><center>41 &#8211; 50</center></th>
			<th width="90" colspan="3"><center>51 &#8211; 60</center></th>
			<th width="90" colspan="3"><center>&#62; 60</center></th>
			<th width="90" rowspan="2" style="vertical-align: middle;"><center>&Sigma;</center></th>
			<th width="90" rowspan="2" style="vertical-align: middle;"><center>&#37;</center></th>
		</tr>
		<tr>
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
		</tr>
		</thead>
		<tbody>
			{assign var="a1" value=0}
			{foreach item="data" from=$DATA}
			{math equation="x+y" x=$a1 y=$data.A1 assign="a1"}
			{/foreach}

			{assign var="a2" value=0}
			{foreach item="data" from=$DATA}
			{math equation="x+y" x=$a2 y=$data.A2 assign="a2"}
			{/foreach}

			{assign var="a3" value=0}
			{foreach item="data" from=$DATA}
			{math equation="x+y" x=$a3 y=$data.A3 assign="a3"}
			{/foreach}

			{assign var="b1" value=0}
			{foreach item="data" from=$DATA}
			{math equation="x+y" x=$b1 y=$data.B1 assign="b1"}
			{/foreach}

			{assign var="b2" value=0}
			{foreach item="data" from=$DATA}
			{math equation="x+y" x=$b2 y=$data.B2 assign="b2"}
			{/foreach}

			{assign var="b3" value=0}
			{foreach item="data" from=$DATA}
			{math equation="x+y" x=$b3 y=$data.B3 assign="b3"}
			{/foreach}

			{assign var="c1" value=0}
			{foreach item="data" from=$DATA}
			{math equation="x+y" x=$c1 y=$data.C1 assign="c1"}
			{/foreach}

			{assign var="c2" value=0}
			{foreach item="data" from=$DATA}
			{math equation="x+y" x=$c2 y=$data.C2 assign="c2"}
			{/foreach}

			{assign var="c3" value=0}
			{foreach item="data" from=$DATA}
			{math equation="x+y" x=$c3 y=$data.C3 assign="c3"}
			{/foreach}

			{assign var="d1" value=0}
			{foreach item="data" from=$DATA}
			{math equation="x+y" x=$d1 y=$data.D1 assign="d1"}
			{/foreach}

			{assign var="d2" value=0}
			{foreach item="data" from=$DATA}
			{math equation="x+y" x=$d2 y=$data.D2 assign="d2"}
			{/foreach}

			{assign var="d3" value=0}
			{foreach item="data" from=$DATA}
			{math equation="x+y" x=$d3 y=$data.D3 assign="d3"}
			{/foreach}

			{assign var="e1" value=0}
			{foreach item="data" from=$DATA}
			{math equation="x+y" x=$e1 y=$data.E1 assign="e1"}
			{/foreach}

			{assign var="e2" value=0}
			{foreach item="data" from=$DATA}
			{math equation="x+y" x=$e2 y=$data.E2 assign="e2"}
			{/foreach}

			{assign var="e3" value=0}
			{foreach item="data" from=$DATA}
			{math equation="x+y" x=$e3 y=$data.E3 assign="e3"}
			{/foreach}

			{math equation=($a1 + $a2 + $a3 + $b1 + $b2 + $b3 + $c1 + $c2 + $c3 + $d1 + $d2 + $d3 + $e1 + $e2 + $e3) assign="ttl"}
			
			{foreach item="data" from=$DATA}
		<tr>
			<td width="90" style="vertical-align: middle;"><center>{$data.JABATAN}</center></td>
			<td width="30" style="vertical-align: middle;"><center>{if $data.A1 < 1}-{else}{$data.A1}{/if}</center></td>
			<td width="30" style="vertical-align: middle;"><center>{if $data.A2 < 1}-{else}{$data.A2}{/if}</center></td>
			<td width="30" style="vertical-align: middle;"><center>{if $data.A3 < 1}-{else}{$data.A3}{/if}</center></td>
			<td width="30" style="vertical-align: middle;"><center>{if $data.B1 < 1}-{else}{$data.B1}{/if}</center></td>
			<td width="30" style="vertical-align: middle;"><center>{if $data.B2 < 1}-{else}{$data.B2}{/if}</center></td>
			<td width="30" style="vertical-align: middle;"><center>{if $data.B3 < 1}-{else}{$data.B3}{/if}</center></td>
			<td width="30" style="vertical-align: middle;"><center>{if $data.C1 < 1}-{else}{$data.C1}{/if}</center></td>
			<td width="30" style="vertical-align: middle;"><center>{if $data.C2 < 1}-{else}{$data.C2}{/if}</center></td>
			<td width="30" style="vertical-align: middle;"><center>{if $data.C3 < 1}-{else}{$data.C3}{/if}</center></td>
			<td width="30" style="vertical-align: middle;"><center>{if $data.D1 < 1}-{else}{$data.D1}{/if}</center></td>
			<td width="30" style="vertical-align: middle;"><center>{if $data.D2 < 1}-{else}{$data.D2}{/if}</center></td>
			<td width="30" style="vertical-align: middle;"><center>{if $data.D3 < 1}-{else}{$data.D3}{/if}</center></td>
			<td width="30" style="vertical-align: middle;"><center>{if $data.E1 < 1}-{else}{$data.E1}{/if}</center></td>
			<td width="30" style="vertical-align: middle;"><center>{if $data.E2 < 1}-{else}{$data.E2}{/if}</center></td>
			<td width="30" style="vertical-align: middle;"><center>{if $data.E3 < 1}-{else}{$data.E3}{/if}</center></td>
			{math equation=($data.A1 + $data.A2 + $data.A3 + $data.B1 + $data.B2 + $data.B3 + $data.C1 + $data.C2 + $data.C3 + $data.D1 + $data.D2 + $data.D3 + $data.E1 + $data.E2 + $data.E3) assign="jmlh"}
			<td width="90" style="vertical-align: middle;"><center>{$jmlh}</center></td>
			<td width="90" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$jmlh y=$ttl format="%.2f"}</center></td>
		</tr>
			{/foreach}
		<tr>
			<th width="90" style="vertical-align: middle;"><center>&Sigma;</center></th>
			<th width="30" style="vertical-align: middle;"><center>{$a1}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{$a2}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{$a3}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{$b1}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{$b2}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{$b3}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{$c1}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{$c2}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{$c3}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{$d1}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{$d2}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{$d3}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{$e1}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{$e2}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{$e3}</center></th>
			<th width="90" style="vertical-align: middle;"><center>{$ttl}</center></th>
			<th width="90" style="vertical-align: middle;"><center></center></th>
		</tr>
		<tr>
			<th width="90" style="vertical-align: middle;"><center>&#37;</center></th>
			<th width="30" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$a1 y=$ttl format="%.2f"}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$a2 y=$ttl format="%.2f"}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$a3 y=$ttl format="%.2f"}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$b1 y=$ttl format="%.2f"}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$b2 y=$ttl format="%.2f"}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$b3 y=$ttl format="%.2f"}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$c1 y=$ttl format="%.2f"}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$c2 y=$ttl format="%.2f"}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$c3 y=$ttl format="%.2f"}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$d1 y=$ttl format="%.2f"}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$d2 y=$ttl format="%.2f"}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$d3 y=$ttl format="%.2f"}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$e1 y=$ttl format="%.2f"}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$e2 y=$ttl format="%.2f"}</center></th>
			<th width="30" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$e3 y=$ttl format="%.2f"}</center></th>
			<th width="90" style="vertical-align: middle;"><center></center></th>
			<th width="90" style="vertical-align: middle;"><center>100</center></th>
		</tr>
		</tbody>
	</table>
{*<p><font color="blue">* Tanggal lahir belum di isi.</font></p>*}
<p><br/>&nbsp;</p>