<div class="center_title_bar">Pencarian Data Dosen</div>
<form action="cari_dosen_fk.php" method="get">
<p>Nama Dosen :  <input name="cari" type="text" style="width:300px;" /> <input type="submit" name="Cari_Dosen" value="Cari Dosen"></p>
</form>
{if $smarty.get.cari == ''}
{else}
{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[2,0]],
		headers: {
            0: { sorter: false },
            5: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
        <tr>
			<th class="noheader">Photo Dosen</td>
			<th>NIP/NIK</td>
			<th>Nama Dosen</td>
			<th>Fakultas (Program Studi)</td>
			<th>Status</td>
			<th class="noheader">Aksi</td>
		</tr>
		</thead>
		<tbody>
			{foreach name=test item="list" from=$DOSEN}
			{assign var="photo" value="../../foto_pegawai/{$list.PHOTO}"}
		<tr class="row">
			{if file_exists($photo)}
			<td><center><img src="../../foto_pegawai/{$list.PHOTO}" border="0" width="160" /></center></td>
			{else}
			<td><center><img src="includes/images/unknown.png" border="0" width="160" /></center></td>
			{/if}
			<td>{$list.NIP_DOSEN}</td>
			<td>{$list.NM_PENGGUNA}</td>
			<td>{$list.NM_FAKULTAS}<br>({$list.NM_PROGRAM_STUDI})</td>
			<td>{$list.NM_STATUS_PENGGUNA}</td>
			<td><center><span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/search.png" alt="Detail" title="Detail" onclick="window.location.href='#data_dosen_fk-cari_dosen_fk!dosen_detail_fk.php?action=detail&id={$list.ID_PENGGUNA}';"/></span></center></td>
		</tr>
			{/foreach}
		</tbody>
	</table>
{if $DATA > 5}
<p align="center">
<span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/up.png" alt="Kembali ke atas" title="Kembali ke atas" onclick="window.scrollTo(0,0); return false" /></span>
</p>
{else}
{/if}
{/if}
<p><br/>&nbsp;</p>