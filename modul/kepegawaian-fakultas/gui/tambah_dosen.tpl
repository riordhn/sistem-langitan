{literal}
    <script language="javascript">
        $(document).ready(function() {

            $("#nip").blur(function()
            {
                $("#msgbox1").text("Checking...").fadeIn("slow");
                $.post("cek_nip.php", {nip: $("#nip").val(), id: $("#id_pengguna").val()}, function(data)
                {
                    if (data == "ada")
                    {
                        $("#msgbox1").fadeTo(200, 0.1, function()
                        {
                            $(this).html("<font color='red'>NIP/NIK tidak valid, sudah digunakan</font>").fadeTo(900, 1);
                        });
                    }
                    else
                    {
                        $("#msgbox1").fadeTo(200, 0.1, function()
                        {
                            $(this).html("").fadeTo(900, 1);
                        });
                    }

                });

            });

            $("#nidn").blur(function()
            {
                $("#msgbox2").text("Checking...").fadeIn("slow");
                $.post("cek_nidn.php", {nidn: $("#nidn").val(), id: $("#id_dosen").val()}, function(data)
                {
                    if (data == "ada")
                    {
                        $("#msgbox2").fadeTo(200, 0.1, function()
                        {
                            $(this).html("<font color='red'>NIDN tidak valid, sudah digunakan</font>").fadeTo(900, 1);
                        });
                    }
                    else
                    {
                        $("#msgbox2").fadeTo(200, 0.1, function()
                        {
                            $(this).html("").fadeTo(900, 1);
                        });
                    }

                });

            });

            $("#serdos").blur(function()
            {
                $("#msgbox3").text("Checking...").fadeIn("slow");
                $.post("cek_serdos.php", {serdos: $("#serdos").val(), id: $("#id_dosen").val()}, function(data)
                {
                    if (data == "ada")
                    {
                        $("#msgbox3").fadeTo(200, 0.1, function()
                        {
                            $(this).html("<font color='red'>Serdos tidak valid, sudah digunakan</font>").fadeTo(900, 1);
                        });
                    }
                    else
                    {
                        $("#msgbox3").fadeTo(200, 0.1, function()
                        {
                            $(this).html("").fadeTo(900, 1);
                        });
                    }

                });

            });

        });

        function ambil(KDFAK)
        {
            $.ajax({
                type: "POST",
                url: "tambah_dosen.php",
                data: "kdfak=" + $("#kdfak").val(),
                cache: false,
                success: function(data) {
                    $('#content').html(data);
                }
            });
        }

        function submitform()
        {
            document.update.submit();
        }

        function IsNumber(NUMBER)
        {
            if (!document.update.nip.value.match(/^[0-9]+$/)) {
                alert("Format NIP harus angka.");
                return true;
            }
            else if (document.activation.nidn.value.match(/^[0-9]+$/)) {
                alert("Format NIDN harus angka.");
                return true;
            }
            else if (document.activation.serdos.value.match(/^[0-9]+$/)) {
                alert("Format SERDOS harus angka.");
                return true;
            }
            else if (document.activation.tlp.value.match(/^[0-9]+$/)) {
                alert("Format nomor telepon harus angka.");
                return true;
            }
            else if (document.activation.hp.value.match(/^[0-9]+$/)) {
                alert("Format nomor HP harus angka.");
                return true;
            }
            return false;
        }
    </script>
{/literal}

<div class="center_title_bar">Input Data Dosen</div>
{* biodata dosen *}
<form name="update" id="update" action="tambah_dosen.php" method="post">
    <table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
        <tr><th colspan="4" width="850" style="text-align:center"><strong>BIODATA</strong></th></tr>
        <input type="hidden" name="action" value="add">
        <tr>
            <td>Fakultas</td>
            <td style="text-align:center">:</td>
            <td>
                <select name="kdfak" id="kdfak" onChange="ambil()">
                    <option value=''></option>
                    {foreach item="fak" from=$T_FAK}
                        {html_options values=$fak.ID_FAKULTAS output=$fak.NM_FAKULTAS selected=$FAKGET}
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Program Studi</td>
            <td style="text-align:center">:</td>
            <td>
                <select name="prodi" id="prodi">
                    {foreach item="pro" from=$PRO}
                        {html_options values=$pro.ID_PROGRAM_STUDI output=$pro.NM_PROGRAM_STUDI}
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Departemen Dosen</td>
            <td style="text-align:center">:</td>
            <td>
                <select name="dept" id="dept">
                    {foreach item="dept" from=$DEPT}
                        {html_options values=$dept.ID_DEPARTEMEN output=$dept.NM_DEPARTEMEN selected=$dsn.ID_DEPARTEMEN}
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>NIP/NIK</td>
            <td style="text-align:center">:</td>
            <td><input type="text" name="nip" id="nip" style="width:200px;" value="" />&nbsp;&nbsp;&nbsp;<span id="msgbox1" style="display:none"></span></td>
        </tr>
        <tr>
            <td>NIP/NIK LAMA</td>
            <td style="text-align:center"></td>
            <td><input  type="text" name="nip_lama" style="width:200px;" value=""/></td>
        </tr>
        <tr>
            <td>NIDN</td>
            <td style="text-align:center">:</td>
            <td><input type="text" name="nidn" id="nidn" style="width:200px;" value="" />&nbsp;&nbsp;&nbsp;<span id="msgbox2" style="display:none"></span></td>
        </tr>
        <tr>
            <td>SERDOS</td>
            <td style="text-align:center">:</td>
            <td><input type="text" name="serdos" id="serdos" style="width:200px;" value="" />&nbsp;&nbsp;&nbsp;<span id="msgbox3" style="display:none"></span></td>
        </tr>
        <tr>
            <td>Status Kepegawaian</td>
            <td style="text-align:center"></td>
            <td>
                <select name="status_dsn" id="status_dsn">
                    <option value="HONORER">HONORER</option>
                    <option value="KONTRAK">KONTRAK</option>
                    <option value="GB UNAIR">GB UNAIR</option>
                    <option value="GB EMIRITUS" >GB EMIRITUS</option>
                    <option value="LB">LB</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Prajabatan Nomor</td>
            <td style="text-align:center"></td>
            <td><input  type="text" name="prajab_nomor" style="width:200px;" value=""/></td>
        </tr>
        <tr>
            <td>Prajabatan Tanggal</td>
            <td style="text-align:center"></td>
            <td><input  type="text" name="prajab_tanggal" id="prajab_tanggal" style="width:200px;"  onclick="javascript:NewCssCal('prajab_tanggal', 'ddmmyyyy', '', '', '', '', 'past');" value=""/></td>
        </tr>
        <tr>
            <td>TGL Sumpah PNS</td>
            <td style="text-align:center"></td>
            <td colspan="2"><input  type="text" name="tgl_sumpah_pns" id="tgl_sumpah_pns" style="width:200px;"  onclick="javascript:NewCssCal('tgl_sumpah_pns', 'ddmmyyyy', '', '', '', '', 'past');" value=""/></td>
        </tr>
        <tr>
            <td>TMT CPNS</td>
            <td style="text-align:center"></td>
            <td colspan="2"><input  type="text" name="tmt_cpns" id="tmt_cpns" style="width:200px;"  onclick="javascript:NewCssCal('tmt_cpns', 'ddmmyyyy', '', '', '', '', 'past');" value=""/></td>
        </tr>
        <tr>
            <td>Nomer Karpeg</td>
            <td style="text-align:center"></td>
            <td colspan="2"><input  type="text" name="nomor_karpeg" style="width:500px;" value=""/></td>
        </tr>
        <tr>
            <td>Nomer NPWP</td>
            <td style="text-align:center"></td>
            <td colspan="2"><input  type="text" name="nomor_npwp" style="width:500px;" value=""/></td>
        </tr>
        <tr>
            <td>Taspen</td>
            <td style="text-align:center"></td>
            <td colspan="2">
                <select name="taspen">
                    <option value="1" >Sudah</option>
                    <option value="0" >Belum</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Status Aktif</td>
            <td style="text-align:center">:</td>
            <td>
                <select name="aktif" id="aktif">
                    {foreach item="aktif" from=$STS_AKTIF}
                        {html_options values=$aktif.ID_STATUS_PENGGUNA output=$aktif.NM_STATUS_PENGGUNA}
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Unit Esselon I</td>
            <td style="text-align:center"></td>
            <td colspan="2"><input  type="text" name="unit_esselon1" style="width:500px;" value=""/></td>
        </tr>
        <tr>
            <td>Unit Esselon II</td>
            <td style="text-align:center"></td>
            <td colspan="2"><input  type="text" name="unit_esselon2" style="width:500px;" value=""/></td>
        </tr>
        <tr>
            <td>Unit Esselon III</td>
            <td style="text-align:center"></td>
            <td colspan="2"><input  type="text" name="unit_esselon3" style="width:500px;" value=""/></td>
        </tr>
        <tr>
            <td>Unit Esselon IV</td>
            <td style="text-align:center"></td>
            <td colspan="2"><input  type="text" name="unit_esselon4" style="width:500px;" value=""/></td>
        </tr>
        <tr>
            <td>Unit Esselon V</td>
            <td style="text-align:center"></td>
            <td colspan="2"><input  type="text" name="unit_esselon5" style="width:500px;" value=""/></td>
        </tr>
        <tr>
            <td>Gelar Depan</td>
            <td style="text-align:center">:</td>
            <td><input type="text" name="gelar_dpn" style="width:200px;" value="" /></td>
        </tr>
        <tr>
            <td>Gelar Belakang</td>
            <td style="text-align:center">:</td>
            <td><input type="text" name="gelar_blkg" style="width:200;" value="" /></td>
        </tr>
        <tr>
            <td>Nama Lengkap</td>
            <td style="text-align:center">:</td>
            <td><input type="text" name="nm_lengkap" style="width:450px;" value="" /></td>
        </tr>
        <tr>
            <td>Tempat Lahir</td>
            <td style="text-align:center">:</td>
            <td>
                <select name="id_kota_lahir">
                    {for $i=0 to $count_provinsi}
                        <optgroup label="{$data_kota[$i].nama}">
                            {foreach $data_kota[$i].kota as $data}
                                {html_options values=$data.ID_KOTA output=$data.KOTA}
                            {/foreach}
                        </optgroup>
                    {/for}
                </select>
                &nbsp;, Tanggal Lahir : <input type="text" name="tgl_lahir" id="tgl_lahir" style="text-align:center;" onclick="javascript:NewCssCal('tgl_lahir', 'ddmmyyyy', '', '', '', '', 'past');" value="">
            </td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td style="text-align:center">:</td>
            <td>
                <select name="jk" id="jk">
                    {foreach item="jk" from=$JK}
                        {html_options values=$jk.KELAMIN_PENGGUNA output=$jk.NM_KELAMIN_PENGGUNA}
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td style="text-align:center">:</td>
            <td><input type="text" name="alamat" style="width:650px;" value="" /></td>
        </tr>
        <tr>
            <td>Telepon/HP</td>
            <td style="text-align:center">:</td>
            <td><input type="text" name="tlp" style="text-align:center;" value="" maxlength="15" /> / <input type="text" name="hp" style="text-align:center;" value="" maxlength="15" /></td>
        </tr>
        <tr>
            <td>Email</td>
            <td style="text-align:center">:</td>
            <td><input type="text" name="email" style="width:335px;" value="" maxlength="100" /></td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: right;">
                <span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/save.png" alt="Simpan" title="Simpan" onclick="javascript:submitform();" /></span>
                <span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/cancel.png" alt="Batal" title="Batal" onclick="javascript:history.go(-1);" /></span>
            </td>
        </tr>
    </table>
</form>
<p><br/>&nbsp;</p>