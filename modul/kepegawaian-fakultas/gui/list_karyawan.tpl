<div class="center_title_bar">Data Karyawan</div>

{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[2,0]],
		headers: {
            0: { sorter: false },
            6: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}

<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<th width="840"><strong>{$GOL|upper} - {$USIA|upper}</strong></th>
		<th width="10"><center><span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/left.png" alt="Kembali" title="Kembali" onclick="javascript:history.go(-1);" /></span></center></th>
	</tr>
</table>
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
        <tr>
			<th class="noheader">Photo Karyawan</td>
			<th>NIP/NIK</td>
			<th>Nama Karyawan</td>
			<th>Usia (tahun)</td>
			<th>Unit Kerja</td>
			<th>Status</td>
			<th class="noheader">Aksi</td>
		</tr>
		</thead>
		<tbody>
			{foreach name=test item="list" from=$KARYAWAN}
			{assign var="photo" value="../../foto_pegawai/{$list.PHOTO}"}
		<tr class="row">
			{if file_exists($photo)}
			<td><center><img src="../../foto_pegawai/{$list.PHOTO}" border="0" width="160" /></center></td>
			{else}
			<td><center><img src="includes/images/unknown.png" border="0" width="160" /></center></td>
			{/if}
			<td>{$list.NIP_PEGAWAI}</td>
			<td>{$list.NM_PENGGUNA}</td>
			<td><center>{$list.USIA}</center></td>
			<td>{$list.NM_UNIT_KERJA}</td>
			<td>{$list.NM_STATUS_PENGGUNA}</td>
			<td><center><span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/search.png" alt="Detail" title="Detail" onclick="window.location.href='#rekap_peg!karyawan_detail.php?action=detail&id={$list.ID_PENGGUNA}';"/></span></center></td>
		</tr>
			{foreachelse}
		<tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
			{/foreach}
		</tbody>
	</table>
<p align="center">
<span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/up.png" alt="Kembali ke atas" title="Kembali ke atas" onclick="window.scrollTo(0,0); return false" /></span>
</p>
<p><br/>&nbsp;</p>