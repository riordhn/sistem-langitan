<div class="center_title_bar">RENUM SETTING PEGAWAI</div>
{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[3,0]],
		headers: {
            //3: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
<form action="renum_setting_pegawai.php" method="post">
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
        <tr>
        	<th>No</th>
			<th>NIK</th>
			<th>Nama Karyawan</th>
			<th>Status Pegawai</th>
			<th>Point Max</th>
            <th>Jabatan</th>
		</tr>
		</thead>
		<tbody>
        	{$no=1}
			{foreach item="peg" from=$PEGAWAI}
		<tr class="row">
        	<td>{$no++}</td>
			<td>{$peg.USERNAME}</td>
			<td>{$peg.NM_PENGGUNA}</td>
			<td>{$peg.STATUS_PEGAWAI}</td>
			<td><input type="text" value="{$peg.NILAI_MAX}" name="nilai_max{$no}" size="7"/></td>
            <td>
            	<select name="id_renum_jabatan{$no}">
                    <option value="">----------</option>
                    {foreach $JABATAN as $data}
                    	<option value="{$data.ID_RENUM_JABATAN}" {if $data.ID_RENUM_JABATAN ==  $peg.ID_RENUM_JABATAN} selected="selected" {/if} >{$data.NM_RENUM_JABATAN}</option>
                    {/foreach}
                </select>
            </td>
		</tr>
        		<input type="hidden" value="{$peg.ID_PENGGUNA}" name="id_pengguna{$no}" />
        		<input type="hidden" value="{$no}" name="no" />
                <input type="hidden" value="insert" name="mode" />
			{foreachelse}
        <tr><td colspan="14"><em>Data tidak ditemukan</em></td></tr>
			{/foreach}
         
		</tbody>
         <tr><td colspan="14" style="text-align:center"><input type="submit" name="simpan" value="Simpan"></td></tr>
	</table>
</form>
{if $DATA > 10}
<p align="center">
<span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/up.png" alt="Kembali ke atas" title="Kembali ke atas" onclick="window.scrollTo(0,0); return false" /></span>
</p>
{/if}