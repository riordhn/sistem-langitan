<div class="center_title_bar"> RENUM </div>
<table width="850" class="tableabout">
  <tr>
    <th width="38">No</th>
    <th width="178">Sub menu</th>
    <th width="634">Uraian</th>
  </tr>
  <tbody>
  <tr>
    <td><center>1</center></td>
    <td>Rekap Nilai</td>
    <td>Difungsikan untuk laporan penilaian</td>
  </tr>
  <tr class="odd">
    <td><center>2</center></td>
    <td>Rekap IPK</td>
    <td>Difungsikan untuk laporan IPK</td>
  </tr>
  <tr>
    <td><center>3</center></td>
    <td>Lihat Nilai</td>
    <td>Difungsikan untuk melihat nilai</td>
  </tr>
  <tr class="odd">
    <td><center>4</center></td>
    <td>Menilai</td>
    <td>Difungsikan untuk Menilai</td>
  </tr>
  <tr>
    <td><center>5</center></td>
    <td>Setting Pegawai</td>
    <td>Difungsikan untuk setting pegawai</td>
  </tr>
  <tr class="odd">
    <td><center>6</center></td>
    <td>Setting Jabatan</td>
    <td>Difungsikan untuk setting jabatan</td>
  </tr>
  </tbody>
</table>