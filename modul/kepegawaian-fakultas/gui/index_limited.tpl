<html>
<head>
	<title>Cyber Campus - Universitas Airlangga</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" type="text/css" href="includes/sumberdaya.css" />
	<link rel="stylesheet" type="text/css" href="includes/sortable/themes/green/style.css" />
	<link rel="stylesheet" type="text/css" href="includes/jquery-ui.min.css" />
	<link rel="stylesheet" type="text/css" href="../../css/reset.css" />
	<link rel="stylesheet" type="text/css" href="../../css/text.css" />
	<script type="text/javascript">var defaultRel = 'limited'; var defaultPage = 'limited.php';</script>
	<script type="text/javascript" src="../../js/cybercampus.ajax-1.0.js"></script>
	<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script>
	<script type="text/javascript" src="js/jquery-fixed-table-header.js"></script>
	<script type="text/javascript" src="js/sd.js"></script>
	<script type="text/javascript" src="js/tab.js"></script>
	<script type="text/javascript" src="js/datetimepicker.js"></script>
	<script type="text/javascript" src="includes/sortable/jquery.tablesorter.min.js"></script>
</head>
<body oncontextmenu="return false;">
<script type="text/javascript">
$(document).ready(function()
{
$("#header-page").fixedTableHeader();
}
);
</script>
	<table id="header-page" class="clear-margin-bottom">
		<colgroup>
			<col />
			<col class="main-width"/>
			<col />
		</colgroup>
		<thead>
			<tr bgcolor="#261831">
				<th class="header-left"></th>
				<th class="header-center">
					<div align="right">
						<span style="font-size:32px;padding-right:75px;">KEPEGAWAIAN {$FAKULTAS}</span>
						<br/><span style="font-size:14px;padding-right:75px;color:#ecaf09;"><strong>Selamat Datang : {$user_login}</strong></span>
					</div>
				</th>
				<th class="header-right"></th>
			</tr>
			<tr>
				<th class="tab-left"></th>
				<th class="tab-center">
					<ul>
					{foreach $tabs as $tab}
						<li><a rel="{$tab.ID}" href="{$tab.PAGE}">{$tab.TITLE}</a></li>
						<li class="divider"></li>
					{/foreach}
					<li><a class="disable-ajax" href="../../logout.php">Logout</a></li>
					</ul>
				</th>
				<th class="tab-right"></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="body-left">&nbsp;</td>
				<td class="body-center">
					<table class="content-table">
						<colgroup>
							<col />
							<col />
						</colgroup>
						<tr>
							<td colspan="2"></td>
						</tr>
						<tr>
							<td id="menu" class="menu"></td>
							<td id="content" class="content">Content</td>
						</tr>
					</table>
				</td>
				<td class="body-right">&nbsp;</td>
			</tr>
		</tbody>
	</table>
	<div class="footer-bottom">
	<table>
		<tr>
			<td class="foot-left">&nbsp;</td>
			<td class="foot-center">
				<div class="footer-nav">
                    <a href="/" target="_blank">Home</a> &bull; 
                    <a href="/index.php?category=cybercampus&lang=id#!portal-about.php" target="_blank">About</a> &bull; 
                    <a href="/index.php?category=cybercampus&lang=id#!portal-products.php" target="_blank">Sitemap</a> &bull; 
                    <a href="/index.php?category=cybercampus&lang=id#!portal-contacts.php" target="_blank">Contact Us</a>
                </div>
				<div class="footer">Copyright &copy; 2012 &bull; Sistem Langitan<br/>All Rights Reserved</div>
			</td>
			<td class="foot-left">&nbsp;</td>
		</tr>
	</table>
	</div>
</body>
</html>