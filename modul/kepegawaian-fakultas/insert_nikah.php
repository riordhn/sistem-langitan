<?php
include ('common.php');
require_once ('ociFunction.php');

if ($user->Role() == 24) {
    $id = $_GET['id'];

    if (isset($_POST['submit'])) {
        $id_pengguna = $_POST['id_pengguna'];
        $nama_pasangan = $_POST['nama'];
        $nomor = $_POST['karsisu'];
        $tgl_lahir_psg = $_POST['tgl_lhr_psg'];
        $tgl_nikah = $_POST['tgl_nikah'];
        $pendidikan = $_POST['pendidikan_psg'];
        $pekerjaan = $_POST['pekerjaan_psg'];
        $status = $_POST['status_psg'];

        InsertData("insert into sejarah_pernikahan (id_pengguna, nama_pasangan, nomor_karsisu, tgl_lahir_pasangan, tgl_pernikahan, id_pendidikan_pasangan,pekerjaan_pasangan,status_pasangan,status_valid) 
                    values ('{$id_pengguna}',upper('{$nama_pasangan}'),'{$nomor}',to_date('{$tgl_lahir_psg}','DD-MM-YYYY'),to_date('{$tgl_nikah}','DD-MM-YYYY'),'{$pendidikan}','{$pekerjaan}','{$status}',1)");

        echo '<script>alert("Data berhasil ditambahkan")</script>';
        echo '<script>window.parent.document.location.reload();</script>';
    }
    $id_pdd = getData("select urut, id_pendidikan_akhir, nama_pendidikan_akhir
		from (
		select id_pendidikan_akhir, nama_pendidikan_akhir,
		case when id_pendidikan_akhir=2 then 11 
		when id_pendidikan_akhir=1 then 12
		when id_pendidikan_akhir=10 then 13  
		when id_pendidikan_akhir=9 then 10 
		when id_pendidikan_akhir=8 then 9
		when id_pendidikan_akhir=7 then 8   
		when id_pendidikan_akhir=3 then 7  
		when id_pendidikan_akhir=4 then 6 
		when id_pendidikan_akhir=5 then 5 
		when id_pendidikan_akhir=6 then 4 
		when id_pendidikan_akhir=13 then 3 
		when id_pendidikan_akhir=12 then 2 
		when id_pendidikan_akhir=11 then 1 
		else 0 end as urut
		from pendidikan_akhir)
		order by urut desc");
    $smarty->assign('ID_PDD', $id_pdd);
    $smarty->display('insert/insert_nikah.tpl');
} else {
    header("location: /logout.php");
    exit();
}
?>