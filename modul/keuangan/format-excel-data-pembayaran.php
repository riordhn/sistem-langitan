<?php

include 'config.php';
include 'class/excel/OLEwriter.php';
include 'class/excel/BIFFwriter.php';
include 'class/excel/Worksheet.php';
include 'class/excel/Workbook.php';


//create 'header' function. if called, this will tell the browser that the file returned is an excel document
function HeaderingExcel($filename) {
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=$filename");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
    header("Pragma: public");
}

// HTTP headers
HeaderingExcel('format-excel-data-pembayaran.xls'); //call the function above
// Creating a workbook instance
$workbook = new Workbook("-");

// woksheet 1
$worksheet1 = & $workbook->add_worksheet('Tambah Pembayaran');

$worksheet1->set_zoom(100); //75% zoom
$worksheet1->set_portrait();
$worksheet1->set_paper(9); //set A4
$worksheet1->hide_gridlines();  //hide gridlines


$worksheet1->write_string(0, 0, "NIM");
$worksheet1->write_string(0, 1, "NAMA");

// 240717 fth
$worksheet1->write_string(0, 2, "PROGRAM_STUDI");
$worksheet1->write_string(0, 3, "TAHUN");
$worksheet1->write_string(0, 4, "NAMA_SEMESTER");
$worksheet1->write_string(0, 5, "TGL_BAYAR");
$worksheet1->write_string(0, 6, "NAMA_BIAYA");
$worksheet1->write_string(0, 7, "BESAR_BIAYA");
$worksheet1->write_string(0, 8, "BULAN");

/*$worksheet1->write_string(0, 0, "NIM");
$worksheet1->write_string(0, 1, "NAMA");

// 240717 fth
$worksheet1->write_string(0, 2, "PROGRAM_STUDI");
$worksheet1->write_string(0, 3, "KELAS");
$worksheet1->write_string(0, 4, "SEMESTER");
$worksheet1->write_string(0, 5, "DP");
$worksheet1->write_string(0, 6, "HER");
$worksheet1->write_string(0, 7, "IKM");
$worksheet1->write_string(0, 8, "SPP");
$worksheet1->write_string(0, 9, "UTS");
$worksheet1->write_string(0, 10, "UAS");
$worksheet1->write_string(0, 11, "PRAKTEK");
$worksheet1->write_string(0, 12, "JUMLAH");*/

$workbook->close();
?>
