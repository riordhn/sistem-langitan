<?php

include 'config.php';
include 'class/pendaftaran.class.php';

$daftar = new pendaftaran($db);

if (isset($_GET)) {
    $no_ujian = addslashes(get('no_ujian'));
    if ($no_ujian != '') {
        $cek_mahasiswa_lama = $db->QuerySingle("SELECT COUNT(*) FROM CALON_MAHASISWA_BARU WHERE NO_UJIAN='{$no_ujian}' AND ID_MHS_LAMA IS NOT NULL");
        $cek_pindah_prodi = $db->QuerySingle("
                SELECT COUNT(*) 
                FROM CALON_MAHASISWA_BARU CMB
                JOIN MAHASISWA M ON M.ID_MHS = CMB.ID_MHS_LAMA
                WHERE CMB.NO_UJIAN='{$no_ujian}' AND M.ID_PROGRAM_STUDI!=CMB.ID_PROGRAM_STUDI");
        if ($cek_pindah_prodi > 0) {
            $smarty->assign("status", alert_error("Bukan Merupakan Pindah Jalur tapi Pindah Prodi..."));
        } else if ($cek_mahasiswa_lama > 0) {
            // Biodata Mahasiswa
            $db->Query("SELECT CMB.*,M.NIM_MHS,PS.NM_PROGRAM_STUDI,J.NM_JENJANG
                FROM CALON_MAHASISWA_BARU CMB
                JOIN MAHASISWA M ON M.ID_MHS=CMB.ID_MHS_LAMA
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                WHERE CMB.NO_UJIAN='{$no_ujian}'");
            $data_biodata = $db->FetchAssoc();
            // Semester Calon
            $semester_cmhs = $daftar->get_semester_cmhs($data_biodata['ID_C_MHS']);


            if (!empty($_POST)) {
                // Update Jalur Mahasiswa
                if (post('mode') == 'update_jalur') {
                    $db->Query("UPDATE JALUR_MAHASISWA SET ID_JALUR_AKTIF=0 WHERE ID_MHS='{$data_biodata['ID_MHS_LAMA']}'");
                    $db->Query("
                        INSERT INTO JALUR_MAHASISWA 
                            (NIM_MHS,ID_MHS,ID_PROGRAM_STUDI,ID_JALUR,ID_SEMESTER,ID_JALUR_AKTIF)
                        VALUES
                            ('{$data_biodata['NIM_MHS']}','{$data_biodata['ID_MHS_LAMA']}','{$data_biodata['ID_PROGRAM_STUDI']}','{$data_biodata['ID_JALUR']}','{$semester_cmhs}','1')");
                } else if (post('mode') == 'update_biaya') {
                    $biaya_kuliah_cmhs = $daftar->get_biaya_kuliah_cmhs($data_biodata['ID_C_MHS']);
                    $db->Query("UPDATE BIAYA_KULIAH_MHS SET ID_BIAYA_KULIAH='{$biaya_kuliah_cmhs}' WHERE ID_MHS='{$data_biodata['ID_MHS_LAMA']}'");
                    $kelompok_biaya = $db->QuerySingle("SELECT ID_KELOMPOK_BIAYA FROM BIAYA_KULIAH WHERE ID_BIAYA_KULIAH='{$biaya_kuliah_cmhs}'");
                    $db->Query("UPDATE MAHASISWA SET ID_KELOMPOK_BIAYA='{$kelompok_biaya}' WHERE ID_MHS='{$data_biodata['ID_MHS_LAMA']}'");
                    $db->Query("
                        INSERT INTO PEMBAYARAN
                            (ID_MHS,ID_SEMESTER,ID_DETAIL_BIAYA,BESAR_BIAYA,TGL_BAYAR,NO_TRANSAKSI,ID_BANK,ID_BANK_VIA,KETERANGAN,ID_SEMESTER_BAYAR,IS_TAGIH,ID_STATUS_PEMBAYARAN,FLAG_PEMINDAHAN)
                        SELECT '{$data_biodata['ID_MHS_LAMA']}' ID_MHS,ID_SEMESTER,ID_DETAIL_BIAYA,BESAR_BIAYA,TGL_BAYAR,NO_TRANSAKSI,ID_BANK,ID_BANK_VIA,KETERANGAN,ID_SEMESTER_BAYAR,'T' IS_TAGIH,'1' ID_STATUS_PEMBAYARAN,'1' FLAG_PEMINDAHAN 
                        FROM PEMBAYARAN_CMHS
                        WHERE ID_C_MHS='{$data_biodata['ID_C_MHS']}' AND ID_SEMESTER='{$semester_cmhs}'
                        ");
                }
            }
            // History Jalur
            $history_jalur = $db->QueryToArray("
                SELECT JM.*,JAL.NM_JALUR,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,S.NM_SEMESTER,S.TAHUN_AJARAN
                FROM JALUR_MAHASISWA JM
                JOIN JALUR JAL ON JM.ID_JALUR=JAL.ID_JALUR
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=JM.ID_PROGRAM_STUDI
                JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                JOIN SEMESTER S ON S.ID_SEMESTER=JM.ID_SEMESTER
                WHERE JM.ID_MHS='{$data_biodata['ID_MHS_LAMA']}'
                ORDER BY JM.ID_JALUR_MAHASISWA
                ");

            // Data Pembayaran
            $data_pembayaran = $db->QueryToArray("
                SELECT PC.*,B.NM_BANK,BV.NAMA_BANK_VIA,B.NM_BIAYA
                FROM PEMBAYARAN_CMHS PC
                LEFT JOIN BANK B ON B.ID_BANK=PC.ID_BANK
                LEFT JOIN BANK_VIA BV ON BV.ID_BANK_VIA=PC.ID_BANK_VIA
                JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PC.ID_DETAIL_BIAYA
                JOIN BIAYA B ON B.ID_BIAYA=DB.ID_BIAYA
                WHERE PC.ID_C_MHS='{$data_biodata['ID_C_MHS']}' AND PC.ID_SEMESTER='{$semester_cmhs}'");

            $cek_perubahan_jalur = $db->QuerySingle("SELECT COUNT(*) FROM JALUR_MAHASISWA WHERE ID_SEMESTER='{$semester_cmhs}' AND ID_MHS='{$data_biodata['ID_MHS_LAMA']}'");
            $cek_pemindahan_bayar = $db->QuerySingle("SELECT COUNT(*) FROM PEMBAYARAN WHERE ID_MHS='{$data_biodata['ID_MHS_LAMA']}' AND ID_SEMESTER='{$semester_cmhs}'");
            $smarty->assign('semester_cmhs', $semester_cmhs);
            $smarty->assign('cek_jalur', $cek_perubahan_jalur);
            $smarty->assign('cek_bayar', $cek_pemindahan_bayar);
            $smarty->assign('pembayaran', $data_pembayaran);
            $smarty->assign('biodata', $data_biodata);
            $smarty->assign('history', $history_jalur);
        } else {
            $smarty->assign('status', alert_error("Data Mahasiswa Pindah Jalur tidak ditemukan..."));
        }
    }
}

$smarty->display('pendaftaran/pindah-jalur.tpl');
?>
