<?php

include('config.php');
$struktur_menu = array();
// User Full Akses
//$id_pegawai_keuangan = array(2,25411, 25687, 26143, 26144, 56822, 24160, 25240, 47981,175257);

// data menu dari $user
foreach ($user->MODULs as $data) {
    /*if (in_array($user->ID_PENGGUNA, $id_pegawai_keuangan)) { */
        array_push($struktur_menu, array(
            'NM_MODUL' => $data['NM_MODUL'],
            'PAGE' => $data['PAGE'],
            'TITLE' => $data['TITLE'],
            'AKSES' => $data['AKSES'],
            'SUBMENU' => $db->QueryToArray("SELECT * FROM MENU WHERE ID_MODUL={$data['ID_MODUL']} AND AKSES=1 ORDER BY URUTAN")
                )
        );
    /*} else {
        // MENU KHUSUS PEGAWAI VERFIKASI KEUANGAN
        if ($data['ID_MODUL'] == '86' || $data['ID_MODUL'] == '30') {
            array_push($struktur_menu, array(
                'NM_MODUL' => $data['NM_MODUL'],
                'PAGE' => $data['PAGE'],
                'TITLE' => $data['TITLE'],
                'AKSES' => $data['AKSES'],
                'SUBMENU' => $db->QueryToArray("SELECT * FROM MENU WHERE ID_MODUL={$data['ID_MODUL']} AND ID_MENU NOT IN (689,551) ORDER BY URUTAN")
                    )
            );
        }
    }
    */
}

$smarty->assign('modul_set', $user->MODULs);
$smarty->assign('struktur_menu', $struktur_menu);
$smarty->assign('TITLE', 'master-biaya');
$smarty->assign('user', $user->NAMA_PENGGUNA);
$smarty->display("index.tpl");
?>
