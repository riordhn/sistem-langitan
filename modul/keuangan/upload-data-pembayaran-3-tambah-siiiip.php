<?php

// Test CVS
include 'config.php';
include 'class/reader.php';
include 'class/list_data.class.php';
include 'class/upload.class.php';

$list = new list_data($db);
$upload = new upload($db);

// ExcelFile($filename, $encoding);
$data = new Spreadsheet_Excel_Reader();
$status_message = '';

$id_pengguna = $user->ID_PENGGUNA;


if (isset($_POST)) {
    // Saat menyimpan data
    if (post('mode') == 'save_data') {        
        foreach ($_SESSION['data_pembayaran'] as $p) {     
       	       	
	        	$db->Query("INSERT INTO TAGIHAN (ID_MHS,ID_SEMESTER,BESAR_BIAYA,KETERANGAN,ID_BULAN) VALUES
	        		 ('{$p['ID_MHS']}','{$p['ID_SEMESTER']}','{$p['BESAR_BIAYA']}','{$p['NAMA_BIAYA']}','{$p['ID_BULAN']}')");

	            $db->Query("SELECT TAGIHAN_SEQ.nextval AS ID_TAGIHAN from dual");
		                $tagihan = $db->FetchAssoc();
		                $id_tagihan = $tagihan['ID_TAGIHAN'];
	        // 290717 fth
            $db->Query("INSERT INTO PEMBAYARAN (ID_TAGIHAN,TGL_BAYAR,BESAR_PEMBAYARAN,ID_PENGGUNA,KETERANGAN,ID_SEMESTER_BAYAR,IS_TARIK)
            	VALUES('$id_tagihan',to_date('{$p['TGL_BAYAR']}', 'DD/MM/YYYY'),'{$p['BESAR_BIAYA']}','$id_pengguna','{$p['NAMA_BIAYA']}','{$p['ID_SEMESTER']}','0')");
        }            
        	$smarty->assign('sukses_simpan',$hasil ? "sukses disimpan":"gagal disimpan");         
    }
    // Saat menyimpan Excel
    else if (post('mode') == 'save_excel') {
        $filename = date('dmy_H_i_s') . '_' . $_FILES["file"]["name"];
        if ($_FILES["file"]["error"] > 0) {
            $status_message .= "Error : " . $_FILES["file"]["error"] . "<br />";
            $status_upload = '0';
        } else {
            $status_message .= "Nama File : " . $filename . "<br />";
            $status_message .= "Type : " . $_FILES["file"]["type"] . "<br />";
            $status_message .= "Size : " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
            if (file_exists("excel/excel_pembayaran_tambah/" . $filename)) {
                $status_message .= 'Keterangan : ' . $filename . " Sudah Ada ";
                $status_upload = '0';
            } else {
                move_uploaded_file($_FILES["file"]["tmp_name"], "excel/excel_pembayaran_tambah/" . $filename);
                chmod("excel/excel_pembayaran_tambah/" . $filename, 0755);
                $status_upload = '1';
            }
        }

        //Saat upload Berhasil

        if ($status_upload) {
            $data->setOutputEncoding('CP1251');
            $data->read("excel/excel_pembayaran_tambah/{$filename}");
            $data_pembayaran_upload = array();
            for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
                //$status_simpan = 0;
                $nim = trim($data->sheets[0]['cells'][$i][1]);

                $data_mhs = $upload->get_mahasiswa($nim);

                $tahun = trim($data->sheets[0]['cells'][$i][4]);

                $nama_semester = trim($data->sheets[0]['cells'][$i][5]);
                $data_semester = $upload->get_semester($tahun,$nama_semester); 
                $tgl_bayar= trim($data->sheets[0]['cells'][$i][6]);
                                
                $nama_biaya = trim($data->sheets[0]['cells'][$i][7]);
                $besar_biaya = trim($data->sheets[0]['cells'][$i][8]);
                $nama_bulan = trim($data->sheets[0]['cells'][$i][9]);

                $data_bulan = $upload->get_bulan($nama_bulan); 


                array_push($data_pembayaran_upload, array(
                	// get_mahasiswa di file upload.class
                    'ID_MHS' => $data_mhs['ID_MHS'],	// insert ke kolom ID_MHS
                    'NIM_MHS' => $nim,		// mengetahui  NIM_MHS
                    'NAMA' => $data_mhs['NM_PENGGUNA'], // mengetahui nama mahasiswa
                    'PRODI' => $data_mhs['PRODI'],		// mengetahui prodi mahasiswa
                    
                    'ID_SEMESTER' => $data_semester['ID_SEMESTER'],
                    'TAHUN' => $tahun,
                    
                    'NAMA_SEMESTER' => $nama_semester,
                    'TGL_BAYAR' => $tgl_bayar,
                    'NAMA_BIAYA' => $nama_biaya,
                    'BESAR_BIAYA' => $besar_biaya,

                    'NAMA_BULAN' => $nama_bulan,
                    'ID_BULAN' => $data_bulan['ID_BULAN']

                ));
            }

            $_SESSION['data_pembayaran'] = $data_pembayaran_upload;
            $smarty->assign('data_pembayaran_upload', $_SESSION['data_pembayaran']);
        }
        $smarty->assign('status_upload', $status_upload);
        $smarty->assign('status_message', $status_message);
    }
}

$smarty->assign('data_semester', $list->load_list_semester());
$smarty->assign('data_biaya', $list->load_biaya());
$smarty->display('upload-pembayaran/upload-data-pembayaran-3-tambah.tpl');
?>
