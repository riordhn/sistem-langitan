<?php

include 'config.php';
include 'class/list_data.class.php';
include 'class/laporan.class.php';
include('../ppmb/class/Penerimaan.class.php');
include 'class/pendaftaran.class.php';


$list = new list_data($db);
$laporan = new laporan($db);
$penerimaan = new Penerimaan($db);

if (isset($_GET)) {
    if (get('mode') == 'laporan') {
        $id_penerimaan = get('penerimaan');
        $id_fakultas = get('fakultas');
        
        $smarty->assign('penerimaan',$list->get_penerimaan($id_penerimaan));
        $smarty->assign('data_penghasilan', $laporan->load_report_penghasilan_ortu($id_fakultas, $id_penerimaan));
    }
}

$smarty->assign('penerimaan_set', $penerimaan->GetAllForView());
$smarty->assign('data_prodi', $list->load_list_prodi(get('fakultas')));
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->display('pendaftaran/penghasilan-ortu.tpl');
?>
