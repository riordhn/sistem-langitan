<?php

include 'config.php';
include 'class/history.class.php';

$history = new history($db);
$data_tagihan_pembayaran = [];
$id_pt = $id_pt_user;

$smarty->assign('cari', get('cari'));
$smarty->assign('id_pt', $id_pt);
$smarty->assign('data_detail_mahasiswa', $history->get_data_mhs(trim(get('cari')), getenv('ID_PT')));
$smarty->assign('data_pembayaran_va', $history->load_riwayat_pembayaran_va_bank(get('trx_id')));
$smarty->display('history/riwayat-transaksi-va.tpl');
