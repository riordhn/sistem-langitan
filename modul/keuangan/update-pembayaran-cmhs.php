<?php

include 'config.php';
include 'class/utility.class.php';
include 'class/master.class.php';

$utility = new utility($db);
$master = new master($db);


if (isset($_GET)) {
    if (get('no_ujian') != '') {
        if (isset($_POST['mode'])) {
            if (post('mode') == 'tambah-biaya') {
                $db->Query("
                    INSERT INTO PEMBAYARAN_CMHS 
                        (ID_C_MHS,ID_SEMESTER,ID_DETAIL_BIAYA,BESAR_BIAYA,ID_STATUS_PEMBAYARAN,IS_TAGIH)
                    VALUES
                        ('{$_POST['id_c_mhs']}','{$_POST['id_semester']}','{$_POST['biaya']}','{$_POST['besar_biaya']}','{$_POST['status']}','Y')
                    ");
                
            } else if (post('mode') == 'biaya_detail') {
                for ($i = 1; $i < $_POST['jumlah']; $i++) {
                    $biaya = str_replace(',', '', $_POST['besar_biaya' . $i]);
                    $db->Query("
                    UPDATE PEMBAYARAN_CMHS 
                    SET 
                        BESAR_BIAYA='{$biaya}',
                        ID_BANK='{$_POST['bank' . $i]}',
                        ID_BANK_VIA='{$_POST['bank_via' . $i]}',
                        TGL_BAYAR='{$_POST['tgl_bayar' . $i]}',
                        NO_TRANSAKSI='{$_POST['no_transaksi' . $i]}',
                        KETERANGAN='{$_POST['keterangan' . $i]}',
                        IS_TAGIH='{$_POST['is_tagih' . $i]}',
                        ID_STATUS_PEMBAYARAN='{$_POST['status' . $i]}'
                WHERE ID_PEMBAYARAN_CMHS='{$_POST['id' . $i]}' AND ID_C_MHS ='{$_POST['id_c_mhs']}'");
                }
            } else if (post('mode') == 'biaya_all') {
                $db->Query("
                    UPDATE PEMBAYARAN_CMHS SET 
                        ID_BANK='{$_POST['bank']}',
                        ID_BANK_VIA='{$_POST['bank_via']}',
                        TGL_BAYAR='{$_POST['tgl_bayar']}',
                        NO_TRANSAKSI='{$_POST['no_transaksi']}',
                        KETERANGAN='{$_POST['keterangan']}',
                        IS_TAGIH='{$_POST['is_tagih']}',
                        ID_STATUS_PEMBAYARAN='{$_POST['status']}'
                    WHERE ID_C_MHS ='{$_POST['id_c_mhs']}'");
            }
        }
        $smarty->assign('data_status_bayar', $master->load_status_pembayaran());
        $smarty->assign('data_pembayaran', $utility->detail_pembayaran_cmhs(get('no_ujian')));
        $smarty->assign('data_bank', $utility->get_bank());
        $smarty->assign('data_bank_via', $utility->get_via_bank());
        $smarty->assign('cari_no_ujian', 1);
    }
}


$smarty->display('utility/update-pembayaran-cmhs.tpl');
?>
