<?php

include 'config.php';
include 'class/list_data.class.php';
include 'class/piutang.class.php';

$list = new list_data($db);
$piutang = new piutang($db);

if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        $smarty->assign('data_biaya', $db->QueryToArray("SELECT DISTINCT(NM_BIAYA) BIAYA FROM AUCC.RIWAYAT_DETAIL_PIUTANG ORDER BY BIAYA"));
        $smarty->assign('data_piutang', $piutang->load_data_kontrol_piutang_detail(get('fakultas'), get('jenjang'), get('prodi'), get('tgl'), get('status'), get('mhs')));
    }
}

$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->assign('data_cut_off', $list->load_tgl_cut_off_piutang());
$smarty->assign('data_status_piutang', $list->load_status_piutang());
$smarty->display('kontrol/kontrol-piutang-detail.tpl');
?>
