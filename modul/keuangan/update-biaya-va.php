<?php

include 'config.php';
include 'class/master.class.php';
include 'class/history.class.php';

$master = new master($db);
$history = new history($db);
$post_mode = post('mode');
$id_tagihan_mhs = get('id_tagihan_mhs');

$mahasiswa = $history->get_data_mhs(trim(get('cari')), $id_pt);

if (!empty($post_mode)) {
    if ($post_mode == 'update_biaya') {
        $tgl_hari_ini = post('tgl_bayar');
        $id_tagihan_mhs = post('id_tagihan_mhs');
        $jumlah_data = post('jumlah_data');
        $no_transaksi = post('no_transaksi');
        $jumlah_pembayaran_perhari = $db->QuerySingle("SELECT COUNT(DISTINCT(NO_TRANSAKSI)) FROM PEMBAYARAN WHERE TO_CHAR(TGL_BAYAR,'YYYY-MM-DD')='{$tgl_hari_ini}'");
        $no_kuitansi = date('Y') . '/' . $history->getRomawi(date('n')) . '/' . date('d') . '/' . str_pad($jumlah_pembayaran_perhari + 1, 5, "0", STR_PAD_LEFT).'/'.rand(10,99);
        $created_at = $db->QuerySingle("SELECT TO_CHAR(CURRENT_TIMESTAMP,'DD-MON-YYYY HH24.MI.SS') FROM DUAL");
        $tgl_bayar = date('d-M-Y', strtotime(post('tgl_bayar')));
        $periode_bayar = 'Pagi';
        $id_bank = post('bank');
        $id_bank_via = 10;
        $status_pembayaran = 1;
        $keterangan = post('keterangan');
        if (!empty($tgl_bayar)) {
            try {
                for ($i = 1; $i <= $jumlah_data; $i++) {
                    $id_tagihan = post('id_tagihan' . $i);
                    $besar_biaya = post('biaya_dibayar' . $i);
                    $db->Query("
                    INSERT INTO PEMBAYARAN (ID_TAGIHAN, BESAR_PEMBAYARAN, TGL_BAYAR, ID_BANK, ID_BANK_VIA,NO_TRANSAKSI,KETERANGAN,PERIODE_BAYAR,NO_KUITANSI,CREATED_AT,ID_PENGGUNA,ID_STATUS_PEMBAYARAN)
                    VALUES 
                    ('{$id_tagihan}','{$besar_biaya}','{$tgl_bayar}','{$id_bank}','{$id_bank_via}','{$no_transaksi}','{$keterangan}','{$periode_bayar}','{$no_kuitansi}',CURRENT_TIMESTAMP,'{$user->ID_PENGGUNA}','{$status_pembayaran}')
                    ");
                    $last_inserted_id_pembayaran = $db->QuerySingle("SELECT ID_PEMBAYARAN FROM PEMBAYARAN WHERE ID_TAGIHAN='{$id_tagihan}' AND BESAR_PEMBAYARAN='{$besar_biaya}' AND NO_TRANSAKSI='{$no_transaksi}'");
                    // UPDATE BIAYA PERBULAN
                    $data_semester_bulan = $history->load_riwayat_pembayaran_biaya($id_tagihan_mhs);
                    $index_bulan = 1;
                    foreach ($data_semester_bulan as $ds) {
                        $kd_bulan_pembayaran = post('biaya_dibayar_kd_bulan_' . $i . '_' . $index_bulan);
                        $nm_bulan_pembayaran = post('biaya_dibayar_nm_bulan_' . $i . '_' . $index_bulan);
                        $besar_bulan_pembayaran = post('biaya_dibayar_bulan_' . $i . '_' . $index_bulan);
                        if ($besar_bulan_pembayaran > 0) {
                            $db->Query("
                            INSERT INTO PEMBAYARAN_PERBULAN (ID_TAGIHAN, ID_PEMBAYARAN, KODE_BULAN, NM_BULAN, BESAR_PEMBAYARAN,CREATED_ON)
                            VALUES 
                            ('{$id_tagihan}','{$last_inserted_id_pembayaran}','{$kd_bulan_pembayaran}','{$nm_bulan_pembayaran}','{$besar_bulan_pembayaran}',CURRENT_TIMESTAMP)
                            ");
                        }
                        $index_bulan++;
                    }
                    $query = $db->Query("
                    SELECT TAG.ID_TAGIHAN,TAG.BESAR_BIAYA,
                    SUM(CASE WHEN PEM.ID_STATUS_PEMBAYARAN=1 THEN PEM.BESAR_PEMBAYARAN ELSE 0 END) BESAR_PEMBAYARAN,
                    SUM(CASE WHEN PEM.ID_STATUS_PEMBAYARAN=3 THEN PEM.BESAR_PEMBAYARAN ELSE 0 END) BESAR_PENANGGUHAN,
                    SUM(CASE WHEN PEM.ID_STATUS_PEMBAYARAN=4 THEN PEM.BESAR_PEMBAYARAN ELSE 0 END) BESAR_PEMBEBASAN
                    FROM PEMBAYARAN PEM
                    JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN=PEM.ID_TAGIHAN
                    WHERE TAG.ID_TAGIHAN='{$id_tagihan}'
                    GROUP BY TAG.ID_TAGIHAN,TAG.BESAR_BIAYA
                    ");
                    $cek_tagihan = $db->FetchAssoc();
                    if ($cek_tagihan['BESAR_BIAYA'] == $cek_tagihan['BESAR_PEMBAYARAN'] && $cek_tagihan['BESAR_BIAYA'] > 0) {
                        $is_lunas = '1';
                    } else {
                        $is_lunas = '0';
                    }

                    $db->Query("
                    UPDATE TAGIHAN SET
                        IS_LUNAS='{$is_lunas}',
                    WHERE ID_TAGIHAN='{$id_tagihan}'
                    ");
                }
                // $db->Query("UPDATE PEMBAYARAN_VA_BANK SET NOTES='$no_kuitansi' WHERE PAYMENT_NTB='$no_transaksi'");
                echo json_encode([
                    'status' => 1,
                    'message' => 'Update biaya berhasil'
                ]);
            } catch (\Exception $e) {
                echo json_encode([
                    'status' => 0,
                    'message' => $e->getMessage()
                ]);
            }
        } else {
            echo json_encode([
                'status' => 0,
                'message' => 'Tanggal bayar tidak sesuai'
            ]);
        }
        exit();
    }
}

if ($id_tagihan_mhs != '') {
    $data_biaya_tagihan = $history->load_riwayat_pembayaran_biaya($id_tagihan_mhs);
} else {
    $data_biaya_tagihan = [];
}
$data_tagihan_semester = $db->QueryToArray("
SELECT TM.ID_TAGIHAN_MHS,S.NM_SEMESTER,S.TAHUN_AJARAN,PEM.TOTAL_PEMBAYARAN,SUM(T.BESAR_BIAYA) TOTAL_TAGIHAN
FROM TAGIHAN_MHS TM
JOIN SEMESTER S ON S.ID_SEMESTER=TM.ID_SEMESTER
LEFT JOIN TAGIHAN T ON T.ID_TAGIHAN_MHS=TM.ID_TAGIHAN_MHS
LEFT JOIN (
	SELECT TAG.ID_TAGIHAN_MHS,SUM(P.BESAR_PEMBAYARAN) TOTAL_PEMBAYARAN
	FROM PEMBAYARAN P 
	JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN=P.ID_TAGIHAN
	JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS=TAG.ID_TAGIHAN_MHS
	WHERE TM.ID_MHS='{$mahasiswa['ID_MHS']}'
	GROUP BY TAG.ID_TAGIHAN_MHS
) PEM ON PEM.ID_TAGIHAN_MHS=TM.ID_TAGIHAN_MHS
WHERE TM.ID_MHS='{$mahasiswa['ID_MHS']}'
GROUP BY TM.ID_TAGIHAN_MHS,S.NM_SEMESTER,S.TAHUN_AJARAN,PEM.TOTAL_PEMBAYARAN
ORDER BY
    S.TAHUN_AJARAN DESC,
    S.NM_SEMESTER DESC
");
$smarty->assign('cari', get('cari'));
$smarty->assign('id_tagihan_mhs', $id_tagihan_mhs);
$smarty->assign('trx_id', get('trx_id'));
$smarty->assign('pembayaran_va', $history->get_pembayaran_va(get('no_transaksi')));
$smarty->assign('data_tagihan_semester', $data_tagihan_semester);
$smarty->assign('data_detail_biaya_tagihan', $data_biaya_tagihan);
$smarty->display('history/update-biaya-va.tpl');
