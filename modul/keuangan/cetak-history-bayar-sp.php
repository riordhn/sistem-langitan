<?php
include ('config.php');
include ('../../tcpdf/config/lang/ind.php');
include ('../../tcpdf/tcpdf.php');
include ('class/history.class.php');
include ('class/list_data.class.php');

$history = new history($db);
$list = new list_data($db);
$semester = $list->get_semester(get('semester'));

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->AddPage();

$html = <<<EOF
<style>
    .keterangan {font-size: 7pt;font-weight:bold;}
    div { margin-top: 0pt; }
    .header { font-size: 20pt; font-family: times; font-weight: bold; text-align:center;}
    .address { font-size: 13pt; font-family: serif; margin-top: 0px ;text-align:center; }
    td { font-size: 7pt; }
    td.center { font-weight:bold;text-align:center }
    th { background-color:black;color:white;font-size: 8pt;text-align:center; }
</style>
<table width="100%" border="0">
    <tr>
        <td width="10%" align="right"><img src="../../img/maba/logo_unair.gif" width="60px" height="60px"/></td>
        <td width="80%" align="center">
            <span class="header">UNIVERSITAS AIRLANGGA<br/></span><span class="address">DIREKTORAT KEUANGAN</span><br/>BUKTI PEMBAYARAN 
        </td>
    </tr>
</table>
<hr/>
<p></p>
<table width="50%" cellpadding="3" border="0.5">
                <tr>
                    <th colspan="2">Detail Biodata</th>
                </tr>
                <tr>
                    <td>NIM</td>
                    <td>{nim}</td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td>{nama}</td>
                </tr>
                <tr>
                    <td>Fakultas</td>
                    <td>{fakultas}</td>
                </tr>
                <tr>
                    <td>Program Studi</td>
                    <td>({jenjang}) {prodi}</td>
                </tr>
</table>
<p></p>
<table width="100%" border="0">
    <tr>
        <td width="100%" align="center">
            <span class="address">DETAIL PEMBAYARAN SEMESTER PENDEK</span>
        </td>
    </tr>
</table>
<p></p>
<table width="90%" cellpadding="2" border="0.5">
        
        <tr>
            <th width="25px">No</th>
            <th>Besar Biaya</th>
            <th>Semester</th>
            <th width="60px">Tanggal Bayar</th>
            <th>Nama Bank</th>
            <th>Via Bank</th>
            <th>No Transaksi</th>
            <th>Keterangan</th>
            <th>Status</th>
        </tr>
    {data_pembayaran}
        
</table>
<p></p>
<table  width="100%" cellpadding="1" border="0" >
    {ttd}
</table>
<span class="keterangan">Data Ini Di Cetak Pada Tanggal {tanggal}</span>
EOF;
$ttd = '<tr>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td width="70%" colspan="2"></td>
            <td width="30%" colspan="2" align="center">
                Surabaya, ' . strftime('%d %B %Y') . '
                <br/>Mengetahui
                <br/>
                <img src="img/ttd_agus_hafiludin.jpg" width="50" height="50"/>
                <br/>
                Eko Retnowati,SE
                <br/>
                NIP.197303182001122002
            </td>
        </tr>';

$index = 1;
$data_pembayaran = '';


$biodata = $history->get_data_mhs(get('cari'));

$data_pembayaran_sp = $history->load_history_bayar_sp(get('cari'));

foreach($data_pembayaran_sp as $sp){
    $besar_biaya = number_format($sp['BESAR_BIAYA']);
    $data_pembayaran .="
        <tr>
            <td>{$index}</td>
            <td>{$besar_biaya}</td>
            <td>{$sp['NM_SEMESTER']} {$sp['GROUP_SEMESTER']} ({$sp['TAHUN_AJARAN']})</td>
            <td>{$sp['TGL_BAYAR']}</td>
            <td>{$sp['NM_BANK']}</td>
            <td>{$sp['NAMA_BANK_VIA']}</td>
            <td>{$sp['NO_TRANSAKSI']}</td>
            <td>{$sp['KETERANGAN']}</td>
            <td>{$sp['NAMA_STATUS']}</td>
        </tr>";
            $index++;
}
$html = str_replace('{nim}', $biodata['NIM_MHS'], $html);
$html = str_replace('{nama}', $biodata['NM_PENGGUNA'], $html);
$html = str_replace('{fakultas}', $biodata['NM_FAKULTAS'], $html);
$html = str_replace('{prodi}', $biodata['NM_PROGRAM_STUDI'], $html);
$html = str_replace('{jenjang}', $biodata['NM_JENJANG'], $html);
$html = str_replace('{data_pembayaran}', $data_pembayaran, $html);
$html = str_replace('{tanggal}', date('d F Y  H:i:s'), $html);
$html = str_replace('{ttd}', $ttd, $html);

$html = str_replace('{total_pembayaran}', 'Rp.' . number_format($total_pembayaran), $html);

$pdf->writeHTML($html);

$pdf->Output();
?>
