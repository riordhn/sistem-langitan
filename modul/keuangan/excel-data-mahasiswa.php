<?php
include 'config.php';
include 'class/detail.class.php';
include 'class/list_data.class.php';

$list = new list_data($db);
$detail = new detail($db);

$data_excel = $detail->load_data_mahasiswa_unair(get('fakultas'), get('biaya_kuliah'), get('kelompok_biaya'), get('prodi'), get('jalur'), get('angkatan'), get('mode'), get('jenis'));
if (get('fakultas') != '' || get('prodi') != '') {
    $fakultas = $list->get_fakultas(get('fakultas'));
    $prodi = $list->get_prodi(get('prodi'));
}
ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #2b1212;
        text-transform:capitalize;
    }
    td{
        text-align: left;
    }
</style>
<?php
if (get('mode') == 'detail') {
    ?>
    <table border='1' align='left' cellpadding='0' cellspacing='0' class='keuangan'>
        <thead>
            <tr>
                <td colspan="12" class="header_text">
                    <?php
                    echo 'DATA MAHASISWA UNAIR<br/>';
                    if ($fakultas != '') {
                        echo 'Fakultas ' . $fakultas['NM_FAKULTAS'] . '<br/>';
                    }
                    if ($prodi != '') {
                        echo 'Program Studi (' . $prodi['NM_JENJANG'] . ') ' . $prodi['NM_PROGRAM_STUDI'] . '<br/>';
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td class="header_text">NO</td>
                <td class="header_text">NIM</td>
                <td class="header_text">NAMA</td>
                <td class="header_text">JENJANG</td>
                <td class="header_text">PRODI</td>
                <td class="header_text">FAKULTAS</td>
                <td class="header_text">JALUR</td>
                <td class="header_text">STATUS</td>
                <td class="header_text">SOP</td>
                <td class="header_text">SP3</td>
                <td class="header_text">MATRIK</td>
                <td class="header_text">PRAKTIKUM</td>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            foreach ($data_excel as $data) {
                echo "<tr>
				<td>{$no}</td>
				<td>'{$data['NIM_MHS']}</td>
				<td>{$data['NM_PENGGUNA']}</td>
				<td>{$data['NM_JENJANG']}</td>
                                <td>{$data['NM_PROGRAM_STUDI']}</td>
				<td>{$data['NM_FAKULTAS']}</td>
				<td>{$data['NM_JALUR']}</td>
                                <td>{$data['NM_STATUS_PENGGUNA']}</td>
				<td>{$data['SOP']}</td>
				<td>{$data['SP3']}</td>
                                <td>{$data['MATRIK']}</td>
                                <td>{$data['PRAKTIKUM']}</td>
			</tr>";
                $no++;
            }
            ?>
        </tbody>
    </table>

    <?php
} else {
    ?>
    <table border='1' align='left' cellpadding='0' cellspacing='0' class='keuangan'>
        <thead>
            <tr>
                <td colspan="12" class="header_text">
                    <?php
                    echo 'DATA MAHASISWA UNAIR<br/>';
                    if ($fakultas != '') {
                        echo 'Fakultas ' . $fakultas['NM_FAKULTAS'] . '<br/>';
                    }
                    if ($prodi != '') {
                        echo 'Program Studi (' . $prodi['NM_JENJANG'] . ') ' . $prodi['NM_PROGRAM_STUDI'] . '<br/>';
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td class="header_text">NO</td>
                <td class="header_text">FAKULTAS</td>
                <td class="header_text">JENJANG</td>
                <td class="header_text">PROGRAM STUDI</td>
                <td class="header_text">ANGKATAN</td>
                <td class="header_text">JALUR</td>
                <td class="header_text">KELOMPOK BIAYA</td>
                <td class="header_text">JUMLAH MAHASISWA</td>
                <td class="header_text">SOP</td>
                <td class="header_text">SP3</td>
                <td class="header_text">MATRIK</td>
                <td class="header_text">PRAKTIKUM</td>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            foreach ($data_excel as $data) {
                echo "<tr>
				<td>{$no}</td>
				<td>{$data['NM_FAKULTAS']}</td>
				<td>{$data['NM_JENJANG']}</td>
				<td>{$data['NM_PROGRAM_STUDI']}</td>
                                <td>{$data['THN_AKADEMIK_SEMESTER']}</td>
				<td>{$data['NM_JALUR']}</td>
				<td>{$data['NM_KELOMPOK_BIAYA']}</td>
                                <td>{$data['JUMLAH_MHS']}</td>
				<td>{$data['SOP']}</td>
				<td>{$data['SP3']}</td>                                
                                <td>{$data['MATRIK']}</td>
                                <td>{$data['PRAKTIKUM']}</td>
			</tr>";
                $no++;
            }
            ?>
        </tbody>
    </table>
    <?php
}
echo '<b>Data Ini Di Ambil Pada Tanggal ' . date('d F Y  H:i:s') . '</b>';

$nm_file = "data-mahasiswa" . date('d-m-Y');
header("Content-disposition: attachment; filename=$nm_file.xls");
header("Content-type: Application/exe");
header("Content-Transfer-Encoding: binary");
ob_flush();
?>
