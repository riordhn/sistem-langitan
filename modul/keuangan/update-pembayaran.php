<?php

include 'config.php';
include 'class/utility.class.php';

$utility = new utility($db);
if (isset($_POST['cari_nim'])) {
    $nim = str_replace("'", "''", $_POST['cari_nim']);
    $smarty->assign('data_pembayaran', $utility->detail_pembayaran($nim));
    $smarty->assign('data_bank', $utility->get_bank());
    $smarty->assign('data_semester', $utility->get_semester_all());
    $smarty->assign('data_bank_via', $utility->get_via_bank());
    $smarty->assign('cari_nim', 1);
}
if (isset($_GET['mode'])) {
    $nim = str_replace("'", "''", $_POST['cari_nim']);
    if ($_GET['mode'] == 'biaya') {
        for ($i = 1; $i < $_POST['jumlah']; $i++) {
            $biaya=str_replace(',', '', $_POST['besar_biaya' . $i]);
            $db->Query("UPDATE PEMBAYARAN SET BESAR_BIAYA='{$biaya}',DENDA_BIAYA='{$_POST['denda_biaya' . $i]}',
			ID_BANK='{$_POST['bank'. $i]}',
			ID_BANK_VIA='{$_POST['bank_via'. $i]}',TGL_BAYAR='{$_POST['tgl_bayar'. $i]}',NO_TRANSAKSI='{$_POST['no_transaksi'. $i]}'
			,KETERANGAN='{$_POST['keterangan'. $i]}',ID_SEMESTER='{$_POST['semester'.$i]}' WHERE ID_PEMBAYARAN='{$_POST['id' . $i]}'");
        }
    } else if ($_GET['mode'] == 'status') {
        $db->Query("UPDATE PEMBAYARAN SET ID_BANK='{$_POST['bank']}',ID_BANK_VIA='{$_POST['bank_via']}',TGL_BAYAR='{$_POST['tgl_bayar']}',NO_TRANSAKSI='{$_POST['no_transaksi']}',KETERANGAN='{$_POST['keterangan']}',IS_TAGIH='{$_POST['is_tagih']}' WHERE ID_MHS='{$_POST['id']}' AND ID_SEMESTER='{$_POST['semester']}'");
    }
    $smarty->assign('data_pembayaran', $utility->detail_pembayaran($nim));
    $smarty->assign('data_semester', $utility->get_semester_all());
    $smarty->assign('data_bank', $utility->get_bank());
    $smarty->assign('data_bank_via', $utility->get_via_bank());
    $smarty->assign('cari_nim', 1);
}

$smarty->display('utility/update-pembayaran.tpl');
?>
