<?php

include 'config.php';
include 'class/list_data.class.php';
include 'class/piutang.class.php';
include 'class/utility.class.php';

$id_pt = $id_pt_user;
$id_user = $user->ID_PENGGUNA;

$list = new list_data($db);
$piutang = new piutang($db);
$utility = new utility($db);

if (isset($_GET)) {
	if (get('mode') == 'tampil') {
		if (post('mode') == 'generate') {
			for ($i = 1; $i <= post('total_data'); $i++) {
				$process_type = post('process_type' . $i);
				$id_mhs = post('id_mhs' . $i);
				$no_va = post('no_va' . $i);
				$semester = get('semester');
				$jenis_pem = get('jenis_pembayaran');
				$awal_periode = get('awal_periode');
				$akhir_periode = get('akhir_periode');
				$keterangan = get('keterangan');
				$tagih = 0;
				if ($id_mhs != '') {

					if ($process_type == 'generate_invoice') {
						// Ambil data tagihan
						$cek_tagihan = $db->QuerySingle("SELECT COUNT(*) FROM TAGIHAN_MHS WHERE ID_SEMESTER='{$semester}' AND ID_MHS='{$id_mhs}'");
						// Jika belum punya tagihan
						if ($cek_tagihan == 0) {
							// AMBIL JENIS TAGIHAN PERSEMESTER
							//$id_jenis_tagihan_semester=$utility->convert_array_to_string_query_in($utility->id_jsem);
							$id_jenis_detail_biaya = $utility->convert_array_to_string_query_in($jenis_pem);
							$db->Query(
								"SELECT M.*,PS.ID_JENJANG,PS.ID_PROGRAM_STUDI 
							FROM MAHASISWA M
							JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
							WHERE M.ID_MHS='{$id_mhs}'"
							);

							$mhs = $db->FetchAssoc();
							// AMBIL LAST INSERT ID TAGIHAN MHS
							$db->Query(
								"SELECT TAGIHAN_MHS_SEQ.NEXTVAL+1 ID_TAGIHAN_MHS FROM DUAL"
							);

							$tagihan_mhs = $db->FetchAssoc();
							// INSERT TAGIHAN MHS
							$db->Query(
								"
								INSERT INTO TAGIHAN_MHS (ID_MHS,ID_SEMESTER,TOTAL_BESAR_BIAYA,TOTAL_DENDA_BIAYA,TOTAL_TERBAYAR,NO_VA,AWAL_PERIODE,AKHIR_PERIODE,KETERANGAN,CREATED_ON,CREATED_BY)
								SELECT FOO.*,0 TOTAL_DENDA_BIAYA,0 TOTAL_TERBAYAR,'{$no_va}' NO_VA,TO_DATE('{$awal_periode}','yyyy/mm/dd') AWAL_PERIODE,TO_DATE('{$akhir_periode}','yyyy/mm/dd') AKHIR_PERIODE,'{$keterangan}' KETERANGAN, SYSDATE CREATED_ON,{$id_user} CREATED_BY 
								FROM 
								(
									SELECT BKM.ID_MHS, {$semester} AS ID_SEMESTER, SUM(DB.BESAR_BIAYA) TOTAL_BESAR_BIAYA
									FROM BIAYA_KULIAH_MHS BKM 
									JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH
									JOIN MAHASISWA M ON M.ID_MHS = BKM.ID_MHS
									WHERE DB.ID_JENIS_DETAIL_BIAYA IN ($id_jenis_detail_biaya)
									AND M.ID_MHS='{$id_mhs}'
									GROUP BY BKM.ID_MHS,{$semester}
								) FOO"
							);

							// Status Pembayaran = 2 Belum dibayar 
							$db->Query(
								"INSERT INTO TAGIHAN (ID_TAGIHAN_MHS, ID_DETAIL_BIAYA, BESAR_BIAYA, IS_LUNAS,ID_STATUS_PEMBAYARAN)
								SELECT {$tagihan_mhs['ID_TAGIHAN_MHS']} ID_TAGIHAN_MHS,DB.ID_DETAIL_BIAYA, DB.BESAR_BIAYA, '{$tagih}' AS IS_LUNAS, 2 AS ID_STATUS_PEMBAYARAN
								FROM BIAYA_KULIAH_MHS BKM
								JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH
								JOIN MAHASISWA M ON M.ID_MHS = BKM.ID_MHS
								WHERE DB.ID_JENIS_DETAIL_BIAYA IN ($id_jenis_detail_biaya) AND M.ID_MHS='{$id_mhs}'"
							);
						}
					} else if ($process_type == 'generate_va') {
						// Ambil data tagihan
						$tagihan = $db->QuerySingle("SELECT ID_TAGIHAN_MHS FROM TAGIHAN_MHS WHERE ID_SEMESTER='{$semester}' AND ID_MHS='{$id_mhs}'");
						// UPDATE NO VA
						$db->Query("UPDATE TAGIHAN_MHS 
						SET NO_VA='{$no_va}',
							AWAL_PERIODE=TO_DATE('{$awal_periode}','yyyy/mm/dd'),
							AKHIR_PERIODE=TO_DATE('{$akhir_periode}','yyyy/mm/dd') 
						WHERE ID_TAGIHAN_MHS='{$tagihan}'");
					}
				}
			}
		}
		$smarty->assign('data_mahasiswa', $utility->load_data_mahasiswa_virtual_account($id_pt, get('fakultas'), get('prodi'), get('status'), get('semester'), get('angkatan'), get('tagihan'), get('p_sebelum')));
	}
}

$smarty->assign('data_prodi', $list->load_list_prodi($id_pt, get('fakultas')));
$smarty->assign('data_fakultas', $list->load_list_fakultas($id_pt));
$smarty->assign('data_status', $piutang->load_list_status());
$smarty->assign('data_semester', $list->load_list_semester());
$smarty->assign('data_angkatan', $list->load_angkatan_mhs());
$smarty->assign('data_jenis_detail_biaya', $list->load_jenis_detail_biaya());
$smarty->display('utility/generate-va.tpl');
