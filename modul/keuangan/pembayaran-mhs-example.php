<?php
include 'config.php';

$mode           = get('mode', 'fakultas');
$tgl_awal       = get('tgl_awal', '2020-02-11');
$tgl_akhir      = get('tgl_akhir', '2020-02-13');
$id_fakultas    = get('fakultas', '2');
$id_prodi       = get('prodi', '');
$periode_bayar  = get('periode_bayar', '');
$id_bank        = get('bank', '6');

// Contoh ini menggunakan kasus detail pelaporan per Fakultas Ekonomi per Tanggal 11/02/2020 s.d. 13/02/2020

/**
 * Data Kelas / Data Sebelah Kiri
 * -----------------
 * Mengambil semua data kelas yang mau dimunculkan. Munculkan juga ID kelasnya yang nanti digunakan untuk proses
 * penampilan data.
 * Jika terdapat fungsi agregasi (sum/min/max/avg), bisa langsung diikutkan pada query disini.
 * 
 * Ex. : Program Studi, Angkatan, Jalur, Jumlah Mahasiswa (Sum), Total Pembayaran (Sum), Total Denda (Sum)
 */
$class_query = "
    SELECT
        F.ID_FAKULTAS, F.NM_FAKULTAS, PS.ID_PROGRAM_STUDI, J.NM_JENJANG, PS.NM_PROGRAM_STUDI,
        JLR.ID_JALUR, JLR.NM_JALUR, M.THN_ANGKATAN_MHS, 
        COUNT(DISTINCT PEM.NO_TRANSAKSI) AS JUMLAH_MAHASISWA, 
        SUM(PEM.BESAR_PEMBAYARAN) AS TOTAL_PEMBAYARAN
    FROM PEMBAYARAN PEM
    JOIN TAGIHAN T ON T.ID_TAGIHAN = PEM.ID_TAGIHAN
    JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS = T.ID_TAGIHAN_MHS
    JOIN MAHASISWA M ON M.ID_MHS = TM.ID_MHS
    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
    JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
    JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
    LEFT JOIN ADMISI A ON A.ID_MHS = M.ID_MHS AND A.ID_JALUR IS NOT NULL
    LEFT JOIN JALUR JLR ON JLR.ID_JALUR = A.ID_JALUR
    WHERE
        F.ID_FAKULTAS = {$id_fakultas} AND
        TO_CHAR(PEM.TGL_BAYAR, 'YYYY-MM-DD') >= '{$tgl_awal}' AND
        TO_CHAR(PEM.TGL_BAYAR, 'YYYY-MM-DD') <= '{$tgl_akhir}' AND
        PEM.ID_BANK = {$id_bank} AND
        PEM.ID_STATUS_PEMBAYARAN = 1
    GROUP BY 
        F.ID_FAKULTAS, F.NM_FAKULTAS, PS.ID_PROGRAM_STUDI, J.NM_JENJANG, PS.NM_PROGRAM_STUDI,
        JLR.ID_JALUR, JLR.NM_JALUR, M.THN_ANGKATAN_MHS
    ORDER BY PS.NM_PROGRAM_STUDI, J.NM_JENJANG";
        
$class_data_set = $db->QueryToArray($class_query);
$smarty->assign('class_data_set', $class_data_set);

/**
 * Data Kelas Kolom / Data untuk Header
 * ------------------------------------
 * Menampilkan data untuk kebutuhan header. Munculkan juga ID kolomnya untuk kebutuhan proses penampilan data.
 * - Jika tampilan kolom statis (semua master dipakai), maka query-nya tinggal select dari tabel masternya.
 * - Jika tampilan kolomnya dinamis (bergantung terdapat transaksi / tidak), maka querynya bisa mengikuti contoh
 *   dibawah ini.
 * Ex. : HER, IKM, IJAZAH, dst... (Master Nama Biaya yang terbayar)
 */
$column_query = "
    SELECT B.ID_BIAYA, B.NM_BIAYA, SUM(PEM.BESAR_PEMBAYARAN) AS TOTAL_PEMBAYARAN
    FROM PEMBAYARAN PEM
    JOIN TAGIHAN T ON T.ID_TAGIHAN = PEM.ID_TAGIHAN
    JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = T.ID_DETAIL_BIAYA
    JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
    JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS = T.ID_TAGIHAN_MHS
    JOIN MAHASISWA M ON M.ID_MHS = TM.ID_MHS
    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
    JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
    WHERE
        F.ID_FAKULTAS = {$id_fakultas} AND
        TO_CHAR(PEM.TGL_BAYAR, 'YYYY-MM-DD') >= '{$tgl_awal}' AND
        TO_CHAR(PEM.TGL_BAYAR, 'YYYY-MM-DD') <= '{$tgl_akhir}' AND
        PEM.ID_BANK = {$id_bank} AND
        PEM.ID_STATUS_PEMBAYARAN = 1
    GROUP BY 
        B.ID_BIAYA, B.NM_BIAYA
    ORDER BY B.NM_BIAYA";
$column_data_set = $db->QueryToArray($column_query);
$smarty->assign('column_data_set', $column_data_set);

// Data Set
$data_query = "
    SELECT
        F.ID_FAKULTAS, PS.ID_PROGRAM_STUDI,
        JLR.ID_JALUR, M.THN_ANGKATAN_MHS, 
        B.ID_BIAYA,
        SUM(PEM.BESAR_PEMBAYARAN) AS TOTAL_PEMBAYARAN
    FROM PEMBAYARAN PEM
    JOIN TAGIHAN T ON T.ID_TAGIHAN = PEM.ID_TAGIHAN
    JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = T.ID_DETAIL_BIAYA
    JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
    JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS = T.ID_TAGIHAN_MHS
    JOIN MAHASISWA M ON M.ID_MHS = TM.ID_MHS
    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
    JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
    JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
    LEFT JOIN ADMISI A ON A.ID_MHS = M.ID_MHS AND A.ID_JALUR IS NOT NULL
    LEFT JOIN JALUR JLR ON JLR.ID_JALUR = A.ID_JALUR
    WHERE
        F.ID_FAKULTAS = {$id_fakultas} AND
        TO_CHAR(PEM.TGL_BAYAR, 'YYYY-MM-DD') >= '{$tgl_awal}' AND
        TO_CHAR(PEM.TGL_BAYAR, 'YYYY-MM-DD') <= '{$tgl_akhir}' AND
        PEM.ID_BANK = {$id_bank} AND
        PEM.ID_STATUS_PEMBAYARAN = 1
    GROUP BY 
        F.ID_FAKULTAS, PS.ID_PROGRAM_STUDI,
        JLR.ID_JALUR, M.THN_ANGKATAN_MHS,
        B.ID_BIAYA,
        PS.NM_PROGRAM_STUDI, J.NM_JENJANG, B.NM_BIAYA
    ORDER BY PS.NM_PROGRAM_STUDI, J.NM_JENJANG, B.NM_BIAYA";
$data_set = $db->QueryToArray($data_query);
$smarty->assign('data_set', $data_set);
        
$smarty->display('laporan/pembayaran-mhs-example.tpl');
