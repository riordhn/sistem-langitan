<?php

include 'config.php';
include 'class/master.class.php';

$bank_via = new master($db);

$mode = get('mode', 'view');

if ($request_method == 'POST') {
    if (post('mode') == 'add') {
        $bank_via->add_bank_via(post('kode'),post('nama'));
    } else if (post('mode') == 'edit') {
        $bank_via->edit_bank_via(post('kode'),post('nama'), post('id_bank_via'));
    }
}
if ($mode == 'detail' || $mode == 'edit' || $mode == 'upload' || $mode == 'delete') {
    $smarty->assign('data_bank_via', $bank_via->get_bank_via(get('id_bank_via')));
} else {
    $smarty->assign('data_bank_via', $bank_via->load_bank_via());
}


$smarty->display("master/{$mode}-bank-via.tpl");
?>