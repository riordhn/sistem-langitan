<?php

include 'config.php';
include_once 'class/BniEncryption.class.php';


// FROM BNI
$client_id = getenv('BNI_CLIENT_ID');
$secret_key = getenv('BNI_SECRET_KEY');
$url = getenv('BNI_API_URL');

function get_content($url, $post = '')
{
    $header[] = 'Content-Type: application/json';
    $header[] = "Accept-Encoding: gzip, deflate";
    $header[] = "Cache-Control: max-age=0";
    $header[] = "Connection: keep-alive";
    $header[] = "Accept-Language: en-US,en;q=0.8,id;q=0.6";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_VERBOSE, false);
    // curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_ENCODING, true);
    curl_setopt($ch, CURLOPT_AUTOREFERER, true);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36");

    if ($post) {
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    }

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $rs = curl_exec($ch);

    if (empty($rs)) {
        var_dump($rs, curl_error($ch));
        curl_close($ch);
        return false;
    }
    curl_close($ch);
    return $rs;
}

function checkEmailMhs($email)
{
    if (strlen($email) > 4 && strstr($email, '@')) {
        return $email;
    } else {
        return '';
    }
}
// URL utk simulasi pembayaran: http://dev.bni-ecollection.com/

// FROM LANGITAN
$id_tagihan_mhs = post('id_tagihan_mhs');
$no_va = post('no_va');
// LOAD ULANG DATA
$db->Query("
SELECT TM.*,M.NIM_MHS,P.NM_PENGGUNA,P.EMAIL_PENGGUNA,P.EMAIL_ALTERNATE
FROM TAGIHAN_MHS TM
JOIN MAHASISWA M ON M.ID_MHS=TM.ID_MHS
JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
WHERE TM.ID_TAGIHAN_MHS='{$id_tagihan_mhs}'
");
$tagihan_mhs = $db->fetchAssoc();
$db->Query("
SELECT SUM(PEM.BESAR_PEMBAYARAN) TOTAL_TERBAYAR
FROM PEMBAYARAN PEM
JOIN TAGIHAN T ON T.ID_TAGIHAN=PEM.ID_TAGIHAN
JOIN TAGIHAN_MHS TM ON T.ID_TAGIHAN_MHS=TM.ID_TAGIHAN_MHS
WHERE TM.ID_TAGIHAN_MHS='{$id_tagihan_mhs}' AND PEM.ID_STATUS_PEMBAYARAN=1
");
$pembayaran_mhs = $db->fetchAssoc();
$data_asli = array(
    'type' => 'updatebilling',
    'client_id' => $client_id,
    'trx_id' => $tagihan_mhs['TRX_ID'], // fill with Billing ID
    'trx_amount' => post('total_tagihan'),
    'billing_type' => 'i', // paid multiple time as long as paid
    'datetime_expired' => date('c', strtotime("+1 year", strtotime(date('Y-m-d')))), // billing will be expired in 1 year
    'virtual_account' => $tagihan_mhs['NO_VA'],
    'customer_name' => $tagihan_mhs['NM_PENGGUNA'],
    'customer_email' =>  checkEmailMhs($tagihan_mhs['EMAIL_ALTERNATE']),
    'customer_phone' => '',
);
$hashed_string = BniEncryption::encrypt(
    $data_asli,
    $client_id,
    $secret_key
);

$data = array(
    'client_id' => $client_id,
    'data' => $hashed_string,
);

$response = get_content($url, json_encode($data));
$response_json = json_decode($response, true);
$response_json['send_data'] = $data_asli;
if ($response_json['status'] !== '000') {
    // handling jika gagal    
    echo json_encode($response_json);
} else {
    $data_response = BniEncryption::decrypt($response_json['data'], $client_id, $secret_key);
    // $data_response will contains something like this: 
    // array(
    // 	'virtual_account' => 'xxxxx',
    // 	'trx_id' => 'xxx',
    // );
    $response_json['data'] = $data_response;
    echo json_encode($response_json);
}
