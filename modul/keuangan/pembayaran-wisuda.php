<?php

include 'config.php';
include 'class/laporan.class.php';
include 'class/list_data.class.php';
include 'class/paging.class.php';

$laporan = new laporan($db);
$list = new list_data($db);

if (isset($_GET)) {
    if (get('mode') == 'bank') {
        $smarty->assign('data_pew_bank', $laporan->load_data_pembayaran_wisuda_mode_bank(get('tgl_awal'), get('tgl_akhir'), get('periode_wisuda')));
    } else if (get('mode') == 'detail') {
        $smarty->assign('data_bank_one', $list->get_bank(get('bank')));
        $smarty->assign('data_fakultas_one', $list->get_fakultas(get('fakultas')));
        $smarty->assign('data_pew_detail', $laporan->load_data_pembayaran_wisuda_mode_detail(get('tgl_awal'), get('tgl_akhir'), get('periode_wisuda'), get('bank'), get('fakultas')));
    } else if (get('mode') == 'fakultas') {
        $smarty->assign('data_bank_one', $list->get_bank(get('bank')));
        $smarty->assign('data_pew_fakultas', $laporan->load_data_pembayaran_wisuda_mode_fakultas(get('tgl_awal'), get('tgl_akhir'), get('periode_wisuda'), get('bank')));
    }
}

$smarty->assign('tgl_awal', get('tgl_awal'));
$smarty->assign('tgl_akhir', get('tgl_akhir'));
$smarty->assign('bank', get('bank'));
$smarty->assign('data_periode_wisuda', $laporan->load_periode_wisuda());
$smarty->display('laporan/pembayaran-wisuda.tpl');
?>
