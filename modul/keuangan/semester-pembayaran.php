<?php

include 'config.php';
$tahun_sekarang = date('Y');

$id_pt = $id_pt_user;

if(isset($_POST)){
    if(post('mode')=='aktifkan'){
        $semester= post('semester');
        $db->Query("UPDATE SEMESTER SET STATUS_AKTIF_PEMBAYARAN='False' WHERE STATUS_AKTIF_PEMBAYARAN='True' AND ID_PERGURUAN_TINGGI = '{$id_pt}' ");
        $db->Query("UPDATE SEMESTER SET STATUS_AKTIF_PEMBAYARAN='True' WHERE ID_SEMESTER='{$semester}'");
    }
}
$data_semester = $db->QueryToArray("SELECT * FROM SEMESTER WHERE ID_PERGURUAN_TINGGI = '{$id_pt}' AND TAHUN_AJARAN LIKE '%{$tahun_sekarang}%' AND TIPE_SEMESTER IN ('REG','SP') ORDER BY TAHUN_AJARAN DESC,NM_SEMESTER");

$smarty->assign('data_semester',$data_semester);
$smarty->display("master/semester-pembayaran.tpl");
?>