<?php

include 'config.php';
include 'class/laporan.class.php';
include('../ppmb/class/Penerimaan.class.php');
include 'class/pendaftaran.class.php';

$laporan = new laporan($db);
$penerimaan = new Penerimaan($db);

if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        $smarty->assign('data_kelompok',$laporan->load_kelompok_biaya_penerimaan(get('penerimaan')));
        $smarty->assign('data_laporan', $laporan->load_laporan_verifikator(get('penerimaan'), get('tgl_awal'), get('tgl_akhir')));
    }
}

$smarty->assign('penerimaan_set', $penerimaan->GetAllForView());
$smarty->display('pendaftaran/laporan-verifikator.tpl');
?>
