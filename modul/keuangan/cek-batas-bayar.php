<?php

include 'config.php';

$id_pt = $id_pt_user;

if (isset($_GET['cari'])) {
    if (isset($_POST)) {
        if (post('mode') == 'mhs') {
            $db->Query("UPDATE MAHASISWA SET BATAS_BAYAR='{$_POST['batas_bayar']}' WHERE ID_MHS='{$_POST['id_mhs']}'");
        } else if (post('mode') == 'cmhs') {
            $db->Query("UPDATE CALON_MAHASISWA_BARU SET BATAS_BAYAR='{$_POST['batas_bayar']}' WHERE ID_C_MHS='{$_POST['id_c_mhs']}'");
        }
    }
    $db->Query("
        SELECT P.NM_PENGGUNA,M.BATAS_BAYAR,M.ID_MHS,M.NIM_MHS,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,F.NM_FAKULTAS,J.ID_JENJANG,PS.ID_PROGRAM_STUDI
        FROM MAHASISWA M
        JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
        JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
        WHERE M.NIM_MHS='{$_GET['cari']}' AND P.ID_PERGURUAN_TINGGI = {$id_pt} ");
    $data_mhs = $db->FetchAssoc();
    $db->Query("
        SELECT CMB.*,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,F.NM_FAKULTAS
        FROM CALON_MAHASISWA_BARU CMB
        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
        JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
        WHERE CMB.NO_UJIAN='{$_GET['cari']}' AND F.ID_PERGURUAN_TINGGI = {$id_pt} ");
    $data_cmhs = $db->FetchAssoc();
    if ($data_mhs != '') {
        $smarty->assign('data_mhs', $data_mhs);
    } else if ($data_cmhs != '') {
        $smarty->assign('data_cmhs', $data_cmhs);
    } else {
        $smarty->assign('error', alert_error("Data Mahasiswa/Calon Mahasiswa Tidak Ada"));
    }
}
$smarty->display("utility/cek-batas-bayar.tpl");
?>
