<?php

include 'config.php';
include 'class/laporan.class.php';
include 'class/list_data.class.php';
include 'class/paging.class.php';

$id_pt = $id_pt_user;

$laporan = new laporan($db);
$list = new list_data($db);

if (isset($_GET)) {
    if (get('fakultas') != '' || get('prodi') != '') {
        $smarty->assign('data_prodi', $list->load_list_prodi($id_pt, get('fakultas')));
        $smarty->assign('data_fakultas_one', $list->get_fakultas(get('fakultas')));
        $smarty->assign('data_prodi_one', $list->get_prodi(get('prodi')));
    }
    if (get('mode') == 'bank') {
        $smarty->assign('data_report_bank', $laporan->load_report_bank_pembayaran_sp(get('tgl_awal'), get('tgl_akhir'), get('fakultas'), get('prodi')));
    } else if (get('mode') == 'detail') {
        $smarty->assign('data_report_detail', $laporan->load_report_detail_pembayaran_sp(get('tgl_awal'), get('tgl_akhir'), get('fakultas'), get('prodi'), get('bank')));
    }
}

$smarty->assign('tgl_awal', get('tgl_awal'));
$smarty->assign('tgl_akhir', get('tgl_akhir'));
$smarty->assign('fakultas', get('fakultas'));
$smarty->assign('prodi', get('prodi'));
$smarty->assign('bank', get('bank'));
$smarty->assign('jalur', get('jalur'));
$smarty->assign('data_fakultas', $list->load_list_fakultas($id_pt));
$smarty->assign('data_jalur', $list->load_list_jalur());
$smarty->display('laporan/pembayaran-sp.tpl');
?>
