<?php

include 'config.php';
include 'class/list_data.class.php';

$id_pt = $id_pt_user;

$list = new list_data($db);

if (!empty($_GET)) {
    if (get('mode') == 'laporan') {
        $angkatan = get('angkatan');
        $fakultas = get('fakultas');
        $q_fakultas = $fakultas == '' ? "" : " AND PS.ID_FAKULTAS='{$fakultas}'";
        $array_laporan = array();
        $data_jenjang = $list->load_list_jenjang();
        foreach ($data_jenjang as $j) {
            $db->Query("
                SELECT J.ID_JENJANG,J.NM_JENJANG,
                    SUM(
                      CASE 
                      WHEN M.PENGHASILAN_ORTU_MHS BETWEEN 100000 AND 1749000
                      THEN 1
                      ELSE 0
                      END
                    ) RANGE_1,
                    SUM(
                      CASE 
                      WHEN M.PENGHASILAN_ORTU_MHS BETWEEN 1750000 AND 2999000
                      THEN 1
                      ELSE 0
                      END
                    ) RANGE_2,
                    SUM(
                      CASE 
                      WHEN M.PENGHASILAN_ORTU_MHS BETWEEN 3000000 AND 7499000
                      THEN 1
                      ELSE 0
                      END
                    ) RANGE_3,
                    SUM(
                      CASE 
                      WHEN M.PENGHASILAN_ORTU_MHS BETWEEN 7500000 AND 14999000
                      THEN 1
                      ELSE 0
                      END
                    ) RANGE_4,
                    SUM(
                      CASE 
                      WHEN M.PENGHASILAN_ORTU_MHS BETWEEN 15000000 AND 24999000
                      THEN 1
                      ELSE 0
                      END
                    ) RANGE_5,
                    SUM(
                      CASE 
                      WHEN M.PENGHASILAN_ORTU_MHS >=25000000
                      THEN 1
                      ELSE 0
                      END
                    ) RANGE_6,
                    SUM(
                      CASE 
                      WHEN M.PENGHASILAN_ORTU_MHS <100000
                      THEN 1
                      ELSE 0
                      END
                    ) RANGE_7,
                    SUM(
                      CASE 
                      WHEN M.PENGHASILAN_ORTU_MHS IS NULL
                      THEN 1
                      ELSE 0
                      END
                    ) RANGE_NULL
                    FROM AUCC.JENJANG J
                    LEFT JOIN AUCC.PROGRAM_STUDI PS ON J.ID_JENJANG=PS.ID_JENJANG
                    JOIN AUCC.MAHASISWA M  ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI {$q_fakultas}
                    JOIN AUCC.PENGGUNA P ON M.ID_PENGGUNA=P.ID_PENGGUNA AND M.THN_ANGKATAN_MHS='{$angkatan}'
                    WHERE J.ID_JENJANG='{$j['ID_JENJANG']}' 
                    GROUP BY J.ID_JENJANG,J.NM_JENJANG
                ");
            $laporan = $db->FetchAssoc();
            array_push($array_laporan, $laporan);
        }
        $smarty->assign('data_laporan', $array_laporan);
    }
}

$smarty->assign('data_fakultas', $list->load_list_fakultas($id_pt));
$smarty->display("laporan/laporan-penghasilan-ortu-mhs.tpl");
?>