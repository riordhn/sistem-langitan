<?php

include 'config.php';
include 'class/laporan.class.php';
include 'class/list_data.class.php';
include 'class/paging.class.php';

$laporan = new laporan($db);
$list = new list_data($db);
$paging = new paging('pembayaran-mhs.php');

$id_pt = $id_pt_user;

if (isset($_GET['mode'])) {
    if (get('mode') == 'bank') {
        if (get('fakultas') != '' || get('prodi') != '') {
            $smarty->assign('data_prodi', $list->load_list_prodi($id_pt, get('fakultas')));
            $smarty->assign('data_fakultas_one', $list->get_fakultas(get('fakultas')));
            $smarty->assign('data_prodi_one', $list->get_prodi(get('prodi')));
        }
        $smarty->assign('data_report_bank', $laporan->load_report_bank_mhs(get('tgl_awal'), get('tgl_akhir'), get('fakultas'), get('prodi'),get('periode_bayar')));
    } else if (get('mode') == 'detail') {
        if (get('fakultas') != '' || get('prodi') != '') {
            $smarty->assign('data_prodi', $list->load_list_prodi($id_pt, get('fakultas')));
            $smarty->assign('data_fakultas_one', $list->get_fakultas(get('fakultas')));
            $smarty->assign('data_prodi_one', $list->get_prodi(get('prodi')));
        }
        $smarty->assign('data_report_detail_mhs', $laporan->load_report_detail_mhs(get('tgl_awal'), get('tgl_akhir'), get('fakultas'), get('prodi'),get('periode_bayar'), get('bank'), get('jalur'), get('angkatan')));
    } else if (get('mode') == 'fakultas') {
        if (get('fakultas') != '' || get('prodi') != '') {
            $smarty->assign('data_jalur_one', $list->get_jalur(get('jalur')));
            $smarty->assign('data_prodi', $list->load_list_prodi($id_pt, get('fakultas')));
            $smarty->assign('data_fakultas_one', $list->get_fakultas(get('fakultas')));
            $smarty->assign('data_prodi_one', $list->get_prodi(get('prodi')));
        }
        $row=$laporan->load_report_fakultas_mhs_row(get('tgl_awal'), get('tgl_akhir'), get('fakultas'), get('prodi'),get('periode_bayar'), get('bank'), get('jalur'), get('angkatan'));
        $row_value=$laporan->load_report_fakultas_mhs_row_value(get('tgl_awal'), get('tgl_akhir'), get('fakultas'), get('prodi'),get('periode_bayar'), get('bank'), get('jalur'), get('angkatan'));
        $smarty->assign('count_data_biaya', count($list->load_biaya()));
        $smarty->assign('data_biaya', $list->load_biaya());        
        $smarty->assign('data_report_fakultas_row',$row );
        $smarty->assign('data_report_fakultas_row_value', $row_value);
    }
}

$smarty->assign('tgl_awal', get('tgl_awal'));
$smarty->assign('tgl_akhir', get('tgl_akhir'));
$smarty->assign('fakultas', get('fakultas'));
$smarty->assign('prodi', get('prodi'));
$smarty->assign('bank', get('bank'));
$smarty->assign('periode_bayar', get('periode_bayar'));
$smarty->assign('jalur', get('jalur'));
$smarty->assign('data_fakultas', $list->load_list_fakultas($id_pt));
$smarty->assign('data_jalur', $list->load_list_jalur());
$smarty->display('laporan/pembayaran-mhs-new.tpl');
