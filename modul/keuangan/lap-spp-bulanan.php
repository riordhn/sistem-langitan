<?php

include 'config.php';
include 'class/laporan.class.php';
include 'class/list_data.class.php';
include 'class/paging.class.php';

$id_pt = $id_pt_user;

$laporan = new laporan($db);
$list = new list_data($db);
$tahun_ini = date('Y');
$data_fakultas_all = $list->load_list_fakultas($id_pt);
$id_fakultas = get('fakultas');
$bulan = get('bulan');
if (isset($_GET)) {
    if (get('mode') == 'laporan-view') {
        if (!empty($id_fakultas)) {
            $data_row = $list->load_list_prodi($id_pt, get('fakultas'));
        }
        if (get('bulan') == date('m')) {
            $bulan_ini = date('m');
            $bulan_ini_name = strftime('%B');
            $bulan_kemarin = date('m', strtotime('-1 month'));
            $bulan_kemarin_name = strftime('%B', strtotime('-1 month'));
        } else {
            $bulan_ini = date('m', strtotime("{$tahun_ini}-{$bulan}-01"));
            $bulan_ini_name = strftime('%B', strtotime("{$tahun_ini}-{$bulan}-01"));
            $bulan_kemarin = date('m', strtotime('-1 month', strtotime("{$tahun_ini}-{$bulan}-01")));
            $bulan_kemarin_name = strftime('%B', strtotime('-1 month', strtotime("{$tahun_ini}-{$bulan}-01")));
        }
        $report_row_fakultas = $laporan->load_row_pembayaran_bulanan($data_row, $id_fakultas, $bulan_ini, $bulan_kemarin);
        $smarty->assign('report_row_fakultas', $report_row_fakultas);
    }
}

$smarty->assign('tahun_ini', $tahun_ini);
$smarty->assign('bulan_ini', $bulan_ini);
$smarty->assign('bulan_ini_name', $bulan_ini_name);
$smarty->assign('bulan_kemarin', $bulan_kemarin);
$smarty->assign('bulan_kemarin_name', $bulan_kemarin_name);
$smarty->assign('fakultas', $id_fakultas);
$smarty->assign('data_fakultas_all', $data_fakultas_all);
$smarty->assign('data_row', $data_row);
$smarty->display('laporan/lap-spp-bulanan.tpl');
