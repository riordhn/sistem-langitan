<?php

include('config.php');
include('../ppmb/class/Penerimaan.class.php');
include 'class/pendaftaran.class.php';
include 'class/list_data.class.php';

$daftar = new pendaftaran($db);
$list = new list_data($db);

//$db->Query("UPDATE AUCC.CALON_MAHASISWA_BARU SET TGL_REGMABA='' WHERE NO_UJIAN IN ('81130200006','81130201895')");

if (isset($_GET)) {


    $id_penerimaan = get('id_penerimaan', '');

    $penerimaan = new Penerimaan($db);
    $penerimaan_mhs = $list->get_penerimaan($id_penerimaan);
    $smarty->assign('penerimaan_set', $penerimaan->GetAllForView());

    if ($id_penerimaan != '') {
        $semester_generate = $db->QuerySingle("
            SELECT S.ID_SEMESTER 
            FROM PENERIMAAN P
            LEFT JOIN AUCC.SEMESTER S ON S.ID_SEMESTER=P.ID_SEMESTER
            WHERE P.ID_PENERIMAAN='{$id_penerimaan}'");
        if (isset($_POST)) {
            $kelompok_biaya_all = post('kelompok_biaya_all');
            if (post('mode') == 'update_kelompok') {
                $db->Query("UPDATE CALON_MAHASISWA_BARU SET ID_KELOMPOK_BIAYA='{$_POST['kelompok_biaya']}' WHERE ID_C_MHS='{$_POST['id_c_mhs']}'");
            } elseif (post('mode') == 'generate') {
                for ($i = 1; $i <= post('total_data'); $i++) {
                    $id_c_mhs_kelompok = post('id_c_mhs_kelompok' . $i);
                    $id_c_mhs = post('id_c_mhs' . $i);
                    $biaya_kuliah = post('biaya_kuliah' . $i);
                    $sp3_cmhs = post('sp3_cmhs' . $i);
                    /* TAMBAHIN BREAK MEMPERCEPAT PROSES 
                      if ($id_c_mhs == '' && $id_c_mhs_kelompok == '') {
                      die('asda');
                      break;
                      }
                      /* UPDATE KELOMPOK BIAYA ALL */
                    if ($id_c_mhs_kelompok != '') {
                        $db->Query("UPDATE CALON_MAHASISWA_BARU SET ID_KELOMPOK_BIAYA='{$kelompok_biaya_all}' WHERE ID_C_MHS='{$id_c_mhs_kelompok}'");
                    }
                    /* GENERATE BIAYA */
                    /* UNTUK BIAYA UANG KULIAH TUNGGAL JENIS BIAYA DIBEDAKAN DAN BUKAN ALIH JENIS*/
                    if ($penerimaan_mhs['TAHUN'] >= 2013 && $penerimaan_mhs['ID_JENJANG'] == 1 && ($penerimaan_mhs['ID_JALUR'] == 1 || $penerimaan_mhs['ID_JALUR'] == 3)&& $penerimaan_mhs['ID_JALUR'] != 4) {
                        $sp3_master = $db->QuerySingle("SELECT BESAR_BIAYA FROM DETAIL_BIAYA WHERE ID_BIAYA_KULIAH='{$biaya_kuliah}' AND ID_BIAYA=135");
                    } else {
                        $sp3_master = $db->QuerySingle("SELECT BESAR_BIAYA FROM DETAIL_BIAYA WHERE ID_BIAYA_KULIAH='{$biaya_kuliah}' AND ID_BIAYA=81");
                    }
                    $sp3_lebih = $sp3_cmhs - $sp3_master;
                    if ($id_c_mhs != '') {
                        $cek_pembayaran = $db->QuerySingle("SELECT COUNT(*) FROM PEMBAYARAN_CMHS WHERE ID_C_MHS='{$id_c_mhs}'");
                        $cek_sudah_bayar = $db->QuerySingle("SELECT COUNT(*) FROM PEMBAYARAN_CMHS WHERE ID_C_MHS='{$id_c_mhs}' AND TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL");
                        if ($cek_pembayaran > 0) {
                            if ($cek_sudah_bayar == 0) {
                                /* GENERATE ULANG PEMBAYARAN */
                                $daftar->update_biaya_kuliah_cmhs($id_c_mhs, $biaya_kuliah);
                                $db->Query("DELETE FROM PEMBAYARAN_CMHS WHERE ID_C_MHS='{$id_c_mhs}'");
                                $db->Query("
                                INSERT INTO PEMBAYARAN_CMHS 
                                    (ID_C_MHS,ID_SEMESTER,ID_DETAIL_BIAYA,BESAR_BIAYA,IS_TAGIH,ID_SEMESTER_BAYAR,ID_STATUS_PEMBAYARAN)
                                SELECT ID_C_MHS,{$semester_generate} ID_SEMESTER,ID_DETAIL_BIAYA,BESAR_BIAYA,'Y' IS_TAGIH,{$semester_generate} ID_SEMESTER_BAYAR,'2' ID_STATUS_PEMBAYARAN
                                FROM BIAYA_KULIAH_CMHS BKC
                                JOIN DETAIL_BIAYA DB ON BKC.ID_BIAYA_KULIAH=DB.ID_BIAYA_KULIAH
                                WHERE BKC.ID_C_MHS='{$id_c_mhs}'");
                                /* UPDATE BIAYA SUKARELA */
                                if ($sp3_lebih > 0) {
                                    /* KHUSUS BIAYA UANG KULIAH TUNGGAL DAN BUKAN ALIH JENIS */
                                    if ($penerimaan_mhs['TAHUN'] >= 2013 && $penerimaan_mhs['ID_JENJANG'] == 1 && ($penerimaan_mhs['ID_JALUR'] == 1 || $penerimaan_mhs['ID_JALUR'] == 3) && $penerimaan_mhs['ID_JALUR'] != 4) {
                                        $db->Query("
                                        UPDATE PEMBAYARAN_CMHS SET BESAR_BIAYA={$sp3_master}+{$sp3_lebih}
                                        WHERE ID_C_MHS='{$id_c_mhs}' AND ID_DETAIL_BIAYA IN (
                                            SELECT ID_DETAIL_BIAYA
                                            FROM DETAIL_BIAYA 
                                            WHERE ID_BIAYA_KULIAH='{$biaya_kuliah}' AND ID_BIAYA=135)");
                                    } else {
                                        $db->Query("
                                        UPDATE PEMBAYARAN_CMHS SET BESAR_BIAYA='{$sp3_lebih}'
                                        WHERE ID_C_MHS='{$id_c_mhs}' AND ID_DETAIL_BIAYA IN (
                                            SELECT ID_DETAIL_BIAYA
                                            FROM DETAIL_BIAYA 
                                            WHERE ID_BIAYA_KULIAH='{$biaya_kuliah}' AND ID_BIAYA=124)");
                                    }
                                }
                            }
                        } else {
                            /* GENERATE BIAYA CALON MAHASISWA */
                            $daftar->update_biaya_kuliah_cmhs($id_c_mhs, $biaya_kuliah);
                            $db->Query("
                                INSERT INTO PEMBAYARAN_CMHS 
                                    (ID_C_MHS,ID_SEMESTER,ID_DETAIL_BIAYA,BESAR_BIAYA,IS_TAGIH,ID_SEMESTER_BAYAR,ID_STATUS_PEMBAYARAN)
                                SELECT ID_C_MHS,{$semester_generate} ID_SEMESTER,ID_DETAIL_BIAYA,BESAR_BIAYA,'Y' IS_TAGIH,{$semester_generate} ID_SEMESTER_BAYAR,'2' ID_STATUS_PEMBAYARAN
                                FROM BIAYA_KULIAH_CMHS BKC
                                JOIN DETAIL_BIAYA DB ON BKC.ID_BIAYA_KULIAH=DB.ID_BIAYA_KULIAH
                                WHERE BKC.ID_C_MHS='{$id_c_mhs}'");
                            /* UPDATE BIAYA SUKARELA */
                            if ($sp3_lebih > 0) {
                                /* KHUSUS BIAYA UANG KULIAH TUNGGAL DAN BUKAN ALIH JENIS*/
                                if ($penerimaan_mhs['TAHUN'] >= 2013 && $penerimaan_mhs['ID_JENJANG'] == 1 && ($penerimaan_mhs['ID_JALUR'] == 1 || $penerimaan_mhs['ID_JALUR'] == 3)&& $penerimaan_mhs['ID_JALUR'] != 4) {
                                    $db->Query("
                                        UPDATE PEMBAYARAN_CMHS SET BESAR_BIAYA={$sp3_master}+{$sp3_lebih}
                                        WHERE ID_C_MHS='{$id_c_mhs}' AND ID_DETAIL_BIAYA IN (
                                            SELECT ID_DETAIL_BIAYA
                                            FROM DETAIL_BIAYA 
                                            WHERE ID_BIAYA_KULIAH='{$biaya_kuliah}' AND ID_BIAYA=135)");
                                } else {
                                    $db->Query("
                                        UPDATE PEMBAYARAN_CMHS SET BESAR_BIAYA='{$sp3_lebih}'
                                        WHERE ID_C_MHS='{$id_c_mhs}' AND ID_DETAIL_BIAYA IN (
                                            SELECT ID_DETAIL_BIAYA
                                            FROM DETAIL_BIAYA 
                                            WHERE ID_BIAYA_KULIAH='{$biaya_kuliah}' AND ID_BIAYA=124)");
                                }
                            }
                        }
                    }
                }
            }
        }
        /* DATA CALON TIAP PENERIMAAN */
        $cmb_set = $db->QueryToArray("
             SELECT
                CMB.ID_C_MHS, KODE_VOUCHER, NO_UJIAN, NM_C_MHS, NM_JENJANG, NM_PROGRAM_STUDI, NM_PRODI_MINAT, KELAS_PILIHAN,
                CMP.PTN_S1, CMP.PTN_S2,CMB.ID_PROGRAM_STUDI,NVL(CMB.ID_KELOMPOK_BIAYA,KB.ID_KELOMPOK_BIAYA) ID_KELOMPOK_BIAYA,PS.ID_JENJANG,
                P.ID_JALUR,KB.NM_KELOMPOK_BIAYA,KB.KHUSUS_KELOMPOK_BIAYA,BK.ID_BIAYA_KULIAH,CMB.TGL_REGMABA,
                S.ID_SEMESTER, BK.BESAR_BIAYA_KULIAH AS VARIAN, NVL(DB.BESAR_BIAYA,0) AS VARIAN_SP3,
                (CASE
                  WHEN CMB.ID_PROGRAM_STUDI=CMB.ID_PILIHAN_1 AND CMB.SP3_1 IS NOT NULL
                  THEN CMB.SP3_1
                  WHEN CMB.ID_PROGRAM_STUDI=CMB.ID_PILIHAN_2 AND CMB.SP3_2 IS NOT NULL
                  THEN CMB.SP3_2
                  WHEN CMB.ID_PROGRAM_STUDI=CMB.ID_PILIHAN_3 AND CMB.SP3_3 IS NOT NULL
                  THEN CMB.SP3_3
                  WHEN CMB.ID_PROGRAM_STUDI=CMB.ID_PILIHAN_4 AND CMB.SP3_4 IS NOT NULL
                  THEN CMB.SP3_4
                  ELSE DB.BESAR_BIAYA
                END) SP3_CMHS,
                (SELECT COUNT(*) FROM AUCC.PEMBAYARAN_CMHS WHERE CMB.ID_C_MHS=ID_C_MHS AND TGL_BAYAR IS NULL AND ID_BANK IS NULL) TAGIHAN,
                (SELECT COUNT(*) FROM AUCC.PEMBAYARAN_CMHS WHERE CMB.ID_C_MHS=ID_C_MHS) PEMBAYARAN_CMHS,
                (SELECT COUNT(*) FROM AUCC.PEMBAYARAN_CMHS WHERE CMB.ID_C_MHS=ID_C_MHS AND TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL) SUDAH_BAYAR,
                (SELECT SUM(BESAR_BIAYA) FROM AUCC.PEMBAYARAN_CMHS WHERE CMB.ID_C_MHS=ID_C_MHS AND TGL_BAYAR IS NULL AND ID_BANK IS NULL AND IS_TAGIH='Y') JUMLAH_TAGIHAN,
                (SELECT SUM(BESAR_BIAYA) FROM AUCC.PEMBAYARAN_CMHS WHERE CMB.ID_C_MHS=ID_C_MHS AND TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL) JUMLAH_SUDAH_BAYAR
            FROM AUCC.CALON_MAHASISWA_BARU CMB
            JOIN AUCC.CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
            JOIN AUCC.PENERIMAAN P ON P.ID_PENERIMAAN = CMB.ID_PENERIMAAN
            JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CMB.ID_PROGRAM_STUDI
            JOIN AUCC.JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            LEFT JOIN AUCC.KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA= CMB.ID_KELOMPOK_BIAYA OR (KB.ID_KELOMPOK_BIAYA= (
            CASE
                  WHEN CMB.ID_PROGRAM_STUDI=CMB.ID_PILIHAN_1 
                  THEN CMD.ID_KELOMPOK_BIAYA_1
                  WHEN CMB.ID_PROGRAM_STUDI=CMB.ID_PILIHAN_2 
                  THEN CMD.ID_KELOMPOK_BIAYA_2
                  WHEN CMB.ID_PROGRAM_STUDI=CMB.ID_PILIHAN_3 
                  THEN CMD.ID_KELOMPOK_BIAYA_3
                  WHEN CMB.ID_PROGRAM_STUDI=CMB.ID_PILIHAN_4 
                  THEN CMD.ID_KELOMPOK_BIAYA_4
                  ELSE NULL
                END
                ) AND CMB.ID_KELOMPOK_BIAYA IS NULL)
            LEFT JOIN AUCC.SEMESTER S ON  S.ID_SEMESTER=P.ID_SEMESTER
            LEFT JOIN AUCC.BIAYA_KULIAH BK ON (BK.ID_JALUR = P.ID_JALUR AND BK.ID_PROGRAM_STUDI = CMB.ID_PROGRAM_STUDI AND BK.ID_KELOMPOK_BIAYA = KB.ID_KELOMPOK_BIAYA AND BK.ID_SEMESTER = S.ID_SEMESTER)
            LEFT JOIN AUCC.DETAIL_BIAYA DB ON (DB.ID_BIAYA_KULIAH = BK.ID_BIAYA_KULIAH AND DB.ID_BIAYA = 81)
            LEFT JOIN AUCC.CALON_MAHASISWA_PASCA CMP ON CMP.ID_C_MHS = CMB.ID_C_MHS
            LEFT JOIN AUCC.PRODI_MINAT PM ON PM.ID_PRODI_MINAT = CMP.ID_PRODI_MINAT
            WHERE CMB.ID_PENERIMAAN IN ('{$id_penerimaan}') AND TGL_DITERIMA IS NOT NULL
            ORDER BY CMB.NO_UJIAN");
        /* KELOMPOK BIAYA TIAP PENERIMAAN */
        $kb_cmb = $db->QueryToArray("
            SELECT * FROM AUCC.KELOMPOK_BIAYA
            WHERE ID_KELOMPOK_BIAYA IN (
              SELECT BK.ID_KELOMPOK_BIAYA 
              FROM AUCC.BIAYA_KULIAH BK
              JOIN AUCC.SEMESTER S ON S.ID_SEMESTER=BK.ID_SEMESTER
              JOIN AUCC.PENERIMAAN P ON BK.ID_JALUR=P.ID_JALUR AND S.THN_AKADEMIK_SEMESTER = P.TAHUN
              WHERE P.ID_PENERIMAAN='{$id_penerimaan}'
            )");
        $smarty->assign('cmb_set', $cmb_set);
        $smarty->assign('kb_cmb', $kb_cmb);
    }
}

$smarty->display('utility/generate-biaya-maba-pasca.tpl');
?>