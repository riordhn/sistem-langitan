<?php

include 'config.php';


echo '
    <div class="center_title_bar">Cek Tagihan Host To Host Mahasiswa Wisuda</div> 
<form method="post" action="cek-tagihan-wisuda.php">
<table class="ui-widget">
<tr class="ui-widget-header">
    <th colspan="2" class="header-coloumn">INPUT</td>
</tr>
<tr class="ui-widget-content">
    <td>NIM : <input type="text" name="nim" value="' . $_POST["nim"] . '"></td><td><input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" name="ok" value="Lihat"><input type="hidden" name="ok" value="ok"/></td>
</td>
</table>
</form>
';


if ($_POST["nim"]) {
    if ($_POST['tagihan']) {
        $tagihan = str_replace('.', '', $_POST['tagihan']);
        $db->Query("UPDATE PENGAJUAN_WISUDA SET TAGIHAN_WISUDA='{$tagihan}' WHERE ID_MHS IN (SELECT ID_MHS FROM MAHASISWA WHERE NIM_MHS ='{$_POST['nim']}')");
    }
    $query = "SELECT a.id_mhs,b.yudisium,b.tagihan_wisuda from aucc.mahasiswa a, aucc.pengajuan_wisuda b where a.id_mhs=b.id_mhs and a.nim_bank='" . addslashes($_POST["nim"]) . "'";
    $db->Query($query);
    $id_mhs = '0';
    $r = $db->FetchArray();
    if ($r[1] == '1') {
        $id_mhs = $r[0];
        $wisuda_tagihan = $r[2];
    } else {
        $id_mhs = '0';
        $wisuda_tagihan = '0';
        $gagal .= '<br>-<br/><div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;margin-left:10px"> 
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                     status wisuda belum 1, harap mengisi judul skripsi</p>
        </div> ';
    }

    if (strlen($gagal) == 0) {
        $query = "
			SELECT a.id_mhs, e.nm_pengguna,c.nm_fakultas,d.nm_jenjang,b.nm_program_studi,d.id_jenjang
			from aucc.mahasiswa a, aucc.program_studi b, aucc.fakultas c, aucc.jenjang d, aucc.pengguna e
			where a.id_pengguna=e.id_pengguna and a.id_program_studi=b.id_program_studi and b.id_fakultas=c.id_fakultas and b.id_jenjang=d.id_jenjang and a.nim_bank='" . addslashes($_POST["nim"]) . "'
		";
        $db->Query($query);
        $r = $db->FetchArray();
        $id_mhs = '0';
        $nama = '0';
        $fakultas = '0';
        $jenjang = '0';
        $prodi = '0';
        $id_mhs = $r[0];
        $nama = $r[1];
        $fakultas = $r[2];
        $jenjang = $r[3];
        $prodi = $r[4];
        $jenjang_id = $r[5];
        if ($nama == '0') {
            $gagal .= '<br/><div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;margin-left:10px"> 
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                       Data tidak ditemukan</p>
        </div>';
        }

        // cek data periode wisuda
        $sekarang = date("YmdHis");
        $tmp_jum = '0';
        $query = "select count(*) from aucc.periode_wisuda 
		where id_jenjang='" . $jenjang_id . "' and (to_char(TGL_BAYAR_MULAI,'YYYYMMDDHH24MISS')<='" . $sekarang . "' and to_char(TGL_BAYAR_SELESAI,'YYYYMMDDHH24MISS')>='" . $sekarang . "' )";
        $db->Query($query);
        $r = $db->FetchArray();
        $tmp_jum = $r[0];

        if ($tmp_jum == 0) {
            $gagal .= '<br/><div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;margin-left:10px"> 
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                     periode wisuda tidak ada</p>
        </div>  ';
        } else if ($tmp_jum > 1) {
            $gagal .= '<br/><div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;margin-left:10px"> 
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                       periode wisuda salah</p>
        </div>';
        } else if ($tmp_jum == 1) {
            $query = "
				select a.id_periode_wisuda,a.besar_biaya,a.nm_periode_wisuda, b.tahun_ajaran, b.nm_semester
				from aucc.periode_wisuda a, aucc.semester b
				where a.id_semester=b.id_semester and a.id_jenjang='" . $jenjang_id . "' and (to_char(a.TGL_BAYAR_MULAI,'YYYYMMDDHH24MISS')<='" . $sekarang . "' and to_char(a.TGL_BAYAR_SELESAI,'YYYYMMDDHH24MISS')>='" . $sekarang . "' )
			";
            $db->Query($query);
            $r = $db->FetchArray();
            $wisuda_idperiode = $r[0];
            $wisuda_biaya = $r[1];
            $wisuda_nmperiode = $r[2];
            $wisuda_thakad = $r[3];
            $wisuda_semes = $r[4];

            if ($wisuda_tagihan <= 0) {
                $wisuda_tagihan = 0;
                $gagal .= '<br/><div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;margin-left:10px"> 
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                      tagihan kosong</p>
        </div> ';
            } else {
                // cek apakah wisuda sudah dibayar
                $is_sudahbayar = "0";
                $query = "select count(*)
				from aucc.pembayaran_wisuda 
				where id_mhs='" . $id_mhs . "' and id_periode_wisuda='" . $wisuda_idperiode . "'";
                $db->Query($query);
                $r = $db->FetchArray();
                $is_sudahbayar = $r[0];

                if ($is_sudahbayar > 0) {
                    $gagal .= '<br/><div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;margin-left:10px"> 
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                       sudah dibayar</p>
        </div>';
                } else if ($is_sudahbayar == 0) {
                    //$gagal = "SUKSES;".$mhs_nama.";".$mhs_fakul.";".$mhs_prodi.";".$mhs_jenjang.";".$wisuda_tagihan.";".$wisuda_thakad.";".$wisuda_semes;
                    echo '          <form action="cek-tagihan-wisuda.php" method="post">
					<table class="ui-widget">
                                            <tr class="ui-widget-header">
                                                <th colspan="3">Data Tagihan</th>
                                            </tr>
                                            <tr class="ui-widget-content">
                                                    <td>Nim</td>
                                                    <td>:</td>
                                                    <td>' . $_POST["nim"] . '</td>
                                            </tr>
                                            <tr class="ui-widget-content">
                                                    <td>Nama</td>
                                                    <td>:</td>
                                                    <td>' . $nama . '</td>
                                            </tr>
                                            <tr class="ui-widget-content">
                                                    <td>Fakultas</td>
                                                    <td>:</td>
                                                    <td>' . $fakultas . '</td>
                                            </tr>
                                            <tr class="ui-widget-content">
                                                    <td>Prodi</td>
                                                    <td>:</td>
                                                    <td>' . $jenjang . ' - ' . $prodi . '</td>
                                            </tr>
                                            <tr class="ui-widget-content">
                                                    <td>Tagihan Wisuda</td>
                                                    <td>:</td>
                                                    <td>
                                                        <input name="tagihan" type="text" value="' . number_format($wisuda_tagihan, 0, ',', '.') . '"/>
                                                        <input name="nim" type="hidden" value="' . $_POST['nim'] . '"/>
                                                    </td>
                                            </tr>
                                            <tr class="ui-widget-content">
                                                    <td class="center" colspan="3"><input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" name="ok" value="Update"></td>
                                            </tr>
					</table>
                                    </form>
					';
                }
            }
        }
    }
    echo $gagal;
}
?>
