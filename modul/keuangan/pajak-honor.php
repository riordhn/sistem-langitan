<?php

include 'config.php';
include 'class/honor.class.php';
include 'class/list_data.class.php';
$list = new list_data($db);
$honor = new honor($db);
if (isset($_GET['mode'])) {
    if (get('mode') == 'add') {
        $smarty->assign('data_golongan', $list->load_golongan());
        $smarty->display('honor/add-pajak-honor.tpl');
    } else if (get('mode') == 'edit') {
        $smarty->assign('data_pajak_honor', $honor->get_pajak_honor(get('id')));
        $smarty->assign('data_golongan', $list->load_golongan());
        $smarty->display('honor/edit-pajak-honor.tpl');
    }
} else {
    if (isset($_POST)) {
        if (post('mode') == 'add') {
            $honor->add_pajak_honor(post('pajak'), post('pajak_npwp'), post('golongan'));
            $smarty->assign('alert', alert_success('Data Berhasil Dimasukkan'));
        } else if (post('mode') == 'edit') {
            $honor->edit_pajak_honor(post('id'), post('pajak'), post('pajak_npwp'), post('golongan'));
            $smarty->assign('alert', alert_success('Data Berhasil Di Ubah'));
        }
    }
    $smarty->assign('data_pajak_honor', $honor->load_pajak_honor());
    $smarty->assign('data_golongan', $list->load_golongan());
    $smarty->display('honor/pajak-honor.tpl');
}
?>
