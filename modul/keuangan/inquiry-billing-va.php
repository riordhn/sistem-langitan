<?php

include 'config.php';
include_once 'class/BniEncryption.class.php';


// FROM BNI
$client_id = getenv('BNI_CLIENT_ID');
$secret_key = getenv('BNI_SECRET_KEY');
$url = getenv('BNI_API_URL');
$response_status = 0;
$response_message = '';
$response_data = [];

function get_content($url, $post = '')
{
    $header[] = 'Content-Type: application/json';
    $header[] = "Accept-Encoding: gzip, deflate";
    $header[] = "Cache-Control: max-age=0";
    $header[] = "Connection: keep-alive";
    $header[] = "Accept-Language: en-US,en;q=0.8,id;q=0.6";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_VERBOSE, false);
    // curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_ENCODING, true);
    curl_setopt($ch, CURLOPT_AUTOREFERER, true);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36");

    if ($post) {
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    }

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $rs = curl_exec($ch);

    if (empty($rs)) {
        var_dump($rs, curl_error($ch));
        curl_close($ch);
        return false;
    }
    curl_close($ch);
    return $rs;
}

// URL utk simulasi pembayaran: http://dev.bni-ecollection.com/

// FROM LANGITAN
$trx_id = get('trx_id');
$data_asli = array(
    'type' => 'inquirybilling',
    'client_id' => $client_id,
    'trx_id' => $trx_id, // fill with Billing ID
);
$hashed_string = BniEncryption::encrypt(
    $data_asli,
    $client_id,
    $secret_key
);

$data = array(
    'client_id' => $client_id,
    'data' => $hashed_string,
);

$response = get_content($url, json_encode($data));
$response_json = json_decode($response, true);
$response_json['send_data'] = $data_asli;
if ($response_json['status'] !== '000') {
    // handling jika gagal
    // var_dump($response_json['send_data']);
    $response_message = $response_json['message'];
} else {
    $data_response = BniEncryption::decrypt($response_json['data'], $client_id, $secret_key);
    // $data_response will contains something like this: 
    // array(
    // 	'virtual_account' => 'xxxxx',
    // 	'trx_id' => 'xxx',
    // );
    $response_json['data'] = $data_response;
    $response_status = 1;
    $response_message = $response_json['message'];
    $response_data = $response_json['data'];
}


$smarty->assign('status', $response_status);
$smarty->assign('message', $response_message);
$smarty->assign('data', $response_data);
$smarty->display('history/cek-billing-va.tpl');
