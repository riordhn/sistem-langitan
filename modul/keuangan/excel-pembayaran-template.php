<?php

include 'config.php';
include 'class/list_data.class.php';
include 'class/excel/OLEwriter.php';
include 'class/excel/BIFFwriter.php';
include 'class/excel/Worksheet.php';
include 'class/excel/Workbook.php';

$list = new list_data($db);
$data_biaya = $list->load_biaya();

//create 'header' function. if called, this will tell the browser that the file returned is an excel document
function HeaderingExcel($filename) {
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=$filename");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
    header("Pragma: public");
}

// HTTP headers
HeaderingExcel('excel_pembayaran.xls'); //call the function above
// Creating a workbook instance
$workbook = new Workbook("-");

// woksheet 1
$worksheet1 = & $workbook->add_worksheet('Statu Bayar');

$worksheet1->set_zoom(100); //75% zoom
$worksheet1->set_portrait();
$worksheet1->set_paper(9); //set A4
$worksheet1->hide_gridlines();  //hide gridlines


$worksheet1->write_string(0, 0, "NIM");
$worksheet1->write_string(0, 1, "NAMA");
for ($i = 2; $i <= count($list->load_biaya()) + 2; $i++) {
    $worksheet1->write_string(0, $i, $data_biaya[$i-2]['NM_BIAYA']);
}
$workbook->close();
?>
