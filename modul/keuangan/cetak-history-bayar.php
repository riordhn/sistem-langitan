<?php
include('config.php');
include('../../tcpdf/config/lang/ind.php');
include('../../tcpdf/tcpdf.php');
include('class/history.class.php');
include('class/list_data.class.php');
include ('class/utility.class.php');

$history = new history($db);
$list = new list_data($db);
$semester = $list->get_semester(get('semester'));
$utility = new utility($db);

$jabatan_ttd = $utility->find_konfigurasi_keu('KEU_TTD_JABATAN');
$pejabat_ttd = $utility->find_konfigurasi_keu('KEU_TTD_NAMA');

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->AddPage();

$html = <<<EOF
<style>
    .keterangan {font-size: 7pt;font-weight:bold;}
    div { margin-top: 0pt; }
    .header { font-size: 18pt; font-family: times; font-weight: bold; text-align:center;}
    .address { font-size: 12pt; font-family: serif; margin-top: 0px ;text-align:center; }
    .address2 { font-size: 11pt; font-family: serif; margin-top: 0px ;text-align:center; }
    td { font-size: 8pt; }
    td.center { font-weight:bold;text-align:center }
    th { background-color:black;color:white;font-size: 6pt;text-align:center; }
</style>
<table width="100%" border="0">
    <tr>
        <td width="10%" align="right"><img src="../../img/akademik_images/logo-{$nama_singkat}.gif" width="80px" height="80px"/></td>
        <td width="88%" align="center">
            <span class="header">{$nama_pt_kapital}<br/></span>
            <span class="address">{$alamat},{$kota_pt}</span><br/>
            <span class="address2">Telp.{$telp_pt},Website: {$web_pt},email: {$web_email_pt}, pos: {$kd_pos_pt}</span><br/>
            <b style="font-size:13pt">BUKTI PEMBAYARAN </b>
        </td>
    </tr>
</table>
<hr/>
<p></p>
<table width="50%" cellpadding="3" border="0.5">
                <tr>
                    <th colspan="2">DETAIL BIODATA</th>
                </tr>
                {nomer_header}
                <tr>
                    <td>Nama</td>
                    <td>{nama}</td>
                </tr>
                <tr>
                    <td>Fakultas</td>
                    <td>{fakultas}</td>
                </tr>
                <tr>
                    <td>Program Studi</td>
                    <td>({jenjang}) {prodi}</td>
                </tr>
</table>
<p></p>
<table width="100%" border="0">
    <tr>
        <td width="100%" align="center">
            <span class="address">PEMBAYARAN {semester}</span>
        </td>
    </tr>
</table>
<p></p>
<table width="100%" cellpadding="2" border="0.5">
        
        <tr>
            <th width="25px">No</th>
            <th>Semester</th>
            <th>Total Tagihan</th>
            <th>Terbayar</th>
            <th>Belum terbayar</th>
            <th>Lunas</th>
        </tr>
    {data_pembayaran}
        
</table>
<p></p>
<table width="100%" border="0">
    <tr>
        <td width="100%" class="center">
            <span class="address2" style="color:grey">Total belum terbayar : Rp. {total_tagihan}</span><br/>
            <span class="address2" style="color:black">Total telah dibayarkan : Rp. {total_pembayaran}</span>
        </td>
    </tr>
</table>
<table  width="100%" cellpadding="1" border="0" >
    {ttd}
</table>
<span class="keterangan">Data Ini Di Cetak Pada Tanggal {tanggal}</span>
EOF;
$ttd = '<tr>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td width="70%" colspan="2"></td>
            <td width="30%" colspan="2" align="center">
                Surabaya, ' . strftime('%d %B %Y') . '<br/>
                Mengetahui<br/>
                {jabatan_ttd}
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                {pejabat_ttd}
                <br/>
            </td>
        </tr>';

$index = 1;
$data_pembayaran = '';

if (get('mode') == 'all') {
    if (get('jenis') == 'mhs') {
        $biodata = $history->get_data_mhs(get('cari'), $id_pt_user);
        $nomer_header = '<tr><td>NIM</td><td>' . $biodata['NIM_MHS'] . '</td></tr>';
        $data_pembayaran = $history->load_riwayat_pembayaran_mhs(get('cari'));
        $total_pembayaran = 0;
        $total_piutang = 0;
        foreach ($data_pembayaran as $data) {
            if ($data['STATUS_PEMBAYARAN'] == 3) {
                $penangguhan_str = '<br/>
                    <b style="font-size:1.2em;color:orange">Ditangguhkan</b>';
            } else {
                $penangguhan_str = '';
            }
            $is_lunas_str = $data['TOTAL_BESAR_BIAYA'] > $data['TOTAL_TERBAYAR'] ? '<b style="font-size:1.2em;color:red">Belum</b>' : '<b style="font-size:1.2em;color:green">Sudah</b>';
            $data_pembayaran_str .= '
            <tr>
                <td>' . $index++ . '</td>
                <td>' . $data['NM_SEMESTER'] . ' ( ' . $data['TAHUN_AJARAN'] . ' )' . '</td>
                <td style="text-align:center">Rp. ' . number_format($data['TOTAL_BESAR_BIAYA']) . '</td>
                <td style="text-align:center">Rp. ' . number_format($data['TOTAL_TERBAYAR']) . '</td>
                <td style="text-align:center">Rp. ' . number_format($data['TOTAL_BELUM_TERBAYAR']) . '</td>
                <td style="text-align:center">' . $is_lunas_str . $penangguhan_str . '</td>
            </tr>';
            if ($data['TOTAL_TERBAYAR'] > 0) {
                $total_pembayaran += $data['TOTAL_TERBAYAR'];
                $total_piutang += $data['TOTAL_BELUM_TERBAYAR'];
            }
        }
    } else {
        $biodata = $history->get_data_cmhs(get('cari'));
        $nomer_header = '<tr><td>Nomer Ujian</td><td>' . $biodata['NO_UJIAN'] . '</td></tr>';
        $all_header = '<th width="80px">Semester</th>';
        $besar_biaya_header = '<th width="75px">Besar Biaya</th>';
        foreach ($history->load_history_bayar_cmhs(get('cari')) as $data) {
            $data_pembayaran .= '<tr>
            <td>' . $index++ . '</td>
            <td>' . $data['NM_SEMESTER'] . '<br/> ( ' . $data['TAHUN_AJARAN'] . ' )' . '</td>
            <td>Rp. ' . number_format($data['JUMLAH_PEMBAYARAN']) . '</td>
            <td>' . $data['NM_BANK'] . '</td>
            <td>' . $data['NAMA_BANK_VIA'] . '</td>
            <td>' . $data['TGL_BAYAR'] . '</td>
            <td>' . $data['NO_TRANSAKSI'] . '</td>
            <td>' . $data['KETERANGAN'] . '</td> 
            <td>' . $data['NAMA_STATUS'] . '</td> 
    </tr>';
            $total_piutang += $data['TOTAL_BELUM_TERBAYAR'];
            if ($data['NAMA_STATUS'] == 'Sudah Bayar') {
                $total_pembayaran += $data['JUMLAH_PEMBAYARAN'];
            }
        }
    }
} else {
    if (get('jenis') == 'mhs') {
        $biodata = $history->get_data_mhs(get('cari'), $id_pt_user);
        $nomer_header = '<tr><td>NIM</td><td>' . $biodata['NIM_MHS'] . '</td></tr>';
        $all_header = '<th>Nama Biaya</th>';
        $besar_biaya_header = '<th width="75px">Besar Biaya</th><th width="75px">Denda Biaya</th>';
        foreach ($history->get_history_bayar_mhs(get('cari'), get('semester'), get('no_transaksi'), get('tgl_bayar'), get('keterangan'), get('is_tagih')) as $data) {
            $data_pembayaran .= '<tr>
            <td>' . $index++ . '</td>
            <td>' . $data['NM_BIAYA'] . '</td>
            <td>Rp. ' . number_format($data['BESAR_BIAYA']) . '</td>
            <td>' . $data['DENDA_BIAYA'] . '</td>
            <td>' . $data['NM_BANK'] . '</td>
            <td>' . $data['NAMA_BANK_VIA'] . '</td>
            <td>' . $data['TGL_BAYAR'] . '</td>
            <td>' . $data['NO_TRANSAKSI'] . '</td>
            <td>' . $data['KETERANGAN'] . '</td>
            <td>' . $data['NAMA_STATUS'] . '</td>
            <td>' . $data['NM_SEMESTER'] . ' ' . $data['TAHUN_AJARAN'] . '</td>
    </tr>';
            if ($data['NAMA_STATUS'] == 'Sudah Bayar') {
                $total_pembayaran += $data['BESAR_BIAYA'] + $data['DENDA_BIAYA'];
            }
        }
    } else {
        $biodata = $history->get_data_cmhs(get('cari'));
        $nomer_header = '<tr><td>Nomer Ujian</td><td>' . $biodata['NO_UJIAN'] . '</td></tr>';
        $all_header = '<th>Nama Biaya</th>';
        $besar_biaya_header = '<th width="75px">Besar Biaya</th>';
        $total_piutang += $data['TOTAL_BELUM_TERBAYAR'];
        foreach ($history->get_history_bayar_cmhs(get('cari'), get('semester'), get('no_transaksi'), get('tgl_bayar'), get('keterangan')) as $data) {
            $data_pembayaran .= '<tr>
            <td>' . $index++ . '</td>
            <td>' . $data['NM_BIAYA'] . '</td>
            <td>Rp. ' . number_format($data['BESAR_BIAYA']) . '</td>
            <td>' . $data['NM_BANK'] . '</td>
            <td>' . $data['NAMA_BANK_VIA'] . '</td>
            <td>' . $data['TGL_BAYAR'] . '</td>
            <td>' . $data['NO_TRANSAKSI'] . '</td>
            <td>' . $data['KETERANGAN'] . '</td>
            <td>' . $data['NAMA_STATUS'] . '</td> 
    </tr>';
            if ($data['NAMA_STATUS'] == 'Sudah Bayar') {
                $total_pembayaran += $data['BESAR_BIAYA'];
            }
        }
    }
}

$nama = $biodata['NM_PENGGUNA'] != NULL ? $biodata['NM_PENGGUNA'] : $biodata['NM_C_MHS'];
$html = str_replace('{nama}', $nama, $html);
$html = str_replace('{total_tagihan}', number_format($total_piutang), $html);
$html = str_replace('{fakultas}', $biodata['NM_FAKULTAS'], $html);
$html = str_replace('{prodi}', $biodata['NM_PROGRAM_STUDI'], $html);
$html = str_replace('{jenjang}', $biodata['NM_JENJANG'], $html);
$html = str_replace('{nomer_header}', $nomer_header, $html);
$html = str_replace('{besar_biaya_header}', $besar_biaya_header, $html);
$html = str_replace('{all_header}', $all_header, $html);
$html = str_replace('{data_pembayaran}', $data_pembayaran_str, $html);
$html = str_replace('{tanggal}', date('d F Y  H:i:s'), $html);
$html = str_replace('{ttd}', $ttd, $html);
$html = str_replace('{jabatan_ttd}', $jabatan_ttd, $html);
$html = str_replace('{pejabat_ttd}', $pejabat_ttd, $html);
if (get('mode') == '')
    $html = str_replace('{semester}', 'SEMESTER ' . $semester['NM_SEMESTER'] . ' ( ' . $semester['TAHUN_AJARAN'] . ' )', $html);
else
    $html = str_replace('{semester}', '', $html);
$html = str_replace('{total_pembayaran}',  number_format($total_pembayaran), $html);

$pdf->writeHTML($html);

$pdf->Output();
