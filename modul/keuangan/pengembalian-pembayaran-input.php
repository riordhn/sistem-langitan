<?php

include 'config.php';
include 'class/list_data.class.php';
include 'class/utility.class.php';
include 'class/master.class.php';

$list = new list_data($db);
$utility = new utility($db);
$master = new master($db);

if (isset($_POST)) {
    if (post('mode') == 'simpan') {
        for ($i = 1; $i < post('jumlah_data'); $i++) {
            $id_mhs = post('id_mhs' . $i);
            if ($id_mhs != '') {
                $utility->insert_pengembalian_pembayaran(post('nim' . $i), post('semester' . $i), '0', post('tgl_pengembalian' . $i), post('bank' . $i), post('bank_via' . $i)
                        , post('no_transaksi' . $i), post('keterangan' . $i));
                $db->Query("SELECT * FROM PENGEMBALIAN_PEMBAYARAN WHERE ID_MHS='{$id_mhs}' ORDER BY ID_PENGEMBALIAN_PEMBAYARAN DESC");
                $data_pengembalian = $db->FetchAssoc();
                $id_pengembalian = $data_pengembalian['ID_PENGEMBALIAN_PEMBAYARAN'];
                if ($id_pengembalian != '') {
                    for ($j = 1; $j <= post('jumlah_detail' . $i); $j++) {
                        $utility->tambah_detail_pengembalian($id_pengembalian, post('id_biaya' . $i . '-' . $j), post('biaya' . $i . '-' . $j));
                    }
                    $jumlah_input = $i;
                }
                $jumlah_pengembalian = $db->QuerySingle("SELECT SUM(BESAR_BIAYA) FROM PENGEMBALIAN_PEMBAYARAN_DETAIL WHERE ID_PENGEMBALIAN_PEMBAYARAN='{$id_pengembalian}'");
                $db->Query("UPDATE PENGEMBALIAN_PEMBAYARAN SET BESAR_PENGEMBALIAN='{$jumlah_pengembalian}' WHERE ID_PENGEMBALIAN_PEMBAYARAN='{$id_pengembalian}'");
            }
        }
        if ($jumlah_input != 0) {
            $smarty->assign('success', alert_success("{$jumlah_input} data berhasil Di Masukkan"));
        }
    }
}


$smarty->assign('data_semester', $utility->get_semester_all());
$smarty->assign('data_status', $master->load_status_pembayaran());
$smarty->assign('data_bank', $list->load_bank());
$smarty->assign('data_bank_via', $list->load_bank_via());

$smarty->display('utility/pengembalian-pembayaran-input.tpl');
?>
