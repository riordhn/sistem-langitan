<?php

include 'config.php';
include 'class/master.class.php';
include 'class/history.class.php';

$master = new master($db);
$history = new history($db);
$mahasiswa = $history->get_data_mhs(trim(get('cari')), $id_pt);
$mode = get('mode');
$view = 'manage-va-create.tpl';
$virtual_account = [];
$data_bank = $master->load_bank_vendor($mahasiswa['ID_MHS']);

if ($mode == 'generate-va') {
    $id_bank = get('id_bank');
    $bank_vendor = $master->get_bank_vendor($id_bank);
    if ($bank_vendor['KODE_BANK'] == '9') {
        $date_expired = date('Y-m-d', strtotime("+4 year", strtotime(date('Y-m-d'))));
        $date_time_expired = date('c', strtotime("+4 year", strtotime(date('Y-m-d'))));
        $kode_va = getenv('BNI_PREFIX_VA') . getenv('BNI_CLIENT_ID') . str_pad($mahasiswa['ID_MHS'], 8, '0', STR_PAD_LEFT);
        $result = [
            'no_va' => $kode_va,
            'tgl_expired' => $date_expired,
            'time_expired' => $date_time_expired
        ];
        echo json_encode($result);
    }

    exit();
} else if ($mode == 'update') {
    $trx_id = get('trx_id');
    $db->Query("SELECT * FROM MAHASISWA_VA WHERE TRX_ID='{$trx_id}'");
    $virtual_account = $db->FetchAssoc();
    $virtual_account['DATE_EXPIRED'] = date('Y-m-d', strtotime("+4 year", strtotime(date('Y-m-d'))));
    $virtual_account['DATETIME_EXPIRED'] = date('c', strtotime("+4 year", strtotime(date('Y-m-d'))));
    $view = 'manage-va-update.tpl';
}

$smarty->assign('virtual_account', $virtual_account);
$smarty->assign('mahasiswa', $mahasiswa);
$smarty->assign('cari', get('cari'));
$smarty->assign('data_bank',$data_bank);
$smarty->display('history/' . $view);
