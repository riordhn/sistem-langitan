<?php

include 'config.php';
include_once 'class/BniEncryption.class.php';
include 'class/history.class.php';
$history = new history($db);

$mahasiswa = $history->get_data_mhs(trim(post('nim_mhs')), $id_pt);
// FROM BNI
$client_id = getenv('BNI_CLIENT_ID');
$secret_key = getenv('BNI_SECRET_KEY');
$url = getenv('BNI_API_URL');

function get_content($url, $post = '')
{
    $header[] = 'Content-Type: application/json';
    $header[] = "Accept-Encoding: gzip, deflate";
    $header[] = "Cache-Control: max-age=0";
    $header[] = "Connection: keep-alive";
    $header[] = "Accept-Language: en-US,en;q=0.8,id;q=0.6";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_VERBOSE, false);
    // curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_ENCODING, true);
    curl_setopt($ch, CURLOPT_AUTOREFERER, true);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36");

    if ($post) {
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    }

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $rs = curl_exec($ch);

    if (empty($rs)) {
        var_dump($rs, curl_error($ch));
        curl_close($ch);
        return false;
    }
    curl_close($ch);
    return $rs;
}

function checkEmailMhs($email)
{
    if (strlen($email) > 4 && strstr($email, '@')) {
        return $email;
    } else {
        return '';
    }
}
// URL utk simulasi pembayaran: http://dev.bni-ecollection.com/

// FROM LANGITAN
$mode = post('mode');
$no_va = post('no_va');
$id_bank_vendor = post('bank');
$date_expired = post('tgl_expired');
$datetime_expired = post('time_expired');
$trx_id = mt_rand();
// CREATE JSON VA
$data_asli = array(
    'type' => 'createbilling',
    'client_id' => $client_id,
    'trx_id' => $trx_id, // fill with Billing ID
    'trx_amount' => 100000,
    'billing_type' => 'n', // can be paid multiple time as long as greater or equal minimum amount
    'datetime_expired' => $datetime_expired, // billing will be expired in 1 year
    'virtual_account' => $no_va,
    'customer_name' => $mahasiswa['NM_PENGGUNA'],
    'customer_email' =>  checkEmailMhs($mahasiswa['EMAIL_ALTERNATE']),
    'customer_phone' => '',
);
$hashed_string = BniEncryption::encrypt(
    $data_asli,
    $client_id,
    $secret_key
);

$data = array(
    'client_id' => $client_id,
    'data' => $hashed_string,
);

$response = get_content($url, json_encode($data));
$response_json = json_decode($response, true);
$response_json['send_data'] = $data_asli;
if ($response_json['status'] !== '000') {
    // handling jika gagal
    $response_json['query'] = "INSERT INTO MAHASISWA_VA  (ID_MHS, NO_VA, ID_BANK_VENDOR, TRX_ID,TGL_EXPIRED,CREATED_ON ) VALUES ({$mahasiswa['ID_MHS']},'{$no_va}',$id_bank_vendor,$trx_id,TO_DATE('{$date_expired}','YYYY-MM-DD'),CURRENT_TIMESTAMP)";
    echo json_encode($response_json);
} else {
    $data_response = BniEncryption::decrypt($response_json['data'], $client_id, $secret_key);
    $query = "INSERT INTO MAHASISWA_VA  (ID_MHS, NO_VA, ID_BANK_VENDOR, TRX_ID,TGL_EXPIRED,CREATED_ON ) VALUES ({$mahasiswa['ID_MHS']},'{$no_va}',$id_bank_vendor,$trx_id,TO_DATE('{$date_expired}','YYYY-MM-DD'),CURRENT_TIMESTAMP)";
    $db->Query($query);
    $response_json['data'] = $data_response;
    echo json_encode($response_json);
}
