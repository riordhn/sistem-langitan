<?php

include 'config.php';
include 'class/laporan.class.php';
include 'class/pendapatan.class.php';
include 'class/list_data.class.php';
include 'class/paging.class.php';

$laporan = new laporan($db);
$pendapatan = new pendapatan($db);
$list = new list_data($db);

if (isset($_GET)) {
    if (get('mode') == 'fakultas') {
        $smarty->assign('data_penw_fakultas', $pendapatan->load_data_pendapatan_wisuda_mode_fakultas(get('periode_wisuda')));
    } else if (get('mode') == 'prodi') {
        $smarty->assign('data_fakultas_one', $list->get_fakultas(get('fakultas')));
        $smarty->assign('data_penw_prodi', $pendapatan->load_data_pendapatan_wisuda_mode_prodi(get('periode_wisuda'), get('fakultas')));
    } else if (get('mode') == 'detail') {
        $smarty->assign('data_fakultas_one', $list->get_fakultas(get('fakultas')));
        $smarty->assign('data_prodi_one', $list->get_prodi(get('prodi')));
        $smarty->assign('data_penw_detail', $pendapatan->load_data_pendapatan_wisuda_mode_detail(get('periode_wisuda'), get('fakultas'), get('prodi')));
    }
}

$smarty->assign('tgl_awal', get('tgl_awal'));
$smarty->assign('tgl_akhir', get('tgl_akhir'));
$smarty->assign('bank', get('bank'));
$smarty->assign('data_periode_wisuda', $laporan->load_periode_wisuda());
$smarty->display('pendapatan/pendapatan-wisuda.tpl');
?>
