<?php

include 'config.php';
include 'class/laporan.class.php';
include 'class/list_data.class.php';
include 'class/paging.class.php';

$laporan = new laporan($db);
$list = new list_data($db);
$paging = new paging('pembayaran-cmhs.php');

$id_pt = $id_pt_user;

// Update Manual Untuk Status Pembayaran CMHS
$db->Query("UPDATE AUCC.PEMBAYARAN_CMHS SET IS_TAGIH='T',ID_STATUS_PEMBAYARAN=1 WHERE TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL AND IS_TAGIH='Y'");

if (isset($_GET['mode'])) {
    if (get('mode') == 'bank') {
        if (get('fakultas') != '' || get('prodi') != '') {
            $smarty->assign('data_prodi', $list->load_list_prodi($id_pt, get('fakultas')));
            $smarty->assign('data_fakultas_one', $list->get_fakultas(get('fakultas')));
            $smarty->assign('data_prodi_one', $list->get_prodi(get('prodi')));
        }
        $smarty->assign('data_report_bank_bidik_misi', $laporan->load_report_bank_cmhs_bidik_misi(get('tgl_awal'), get('tgl_akhir'), get('fakultas'), get('prodi')));
        $smarty->assign('data_report_bank', $laporan->load_report_bank_cmhs(get('tgl_awal'), get('tgl_akhir'), get('fakultas'), get('prodi'), get('angkatan'), get('jalur')));
    } else if (get('mode') == 'detail') {
        // pendefinisan untuk paging
        $batas = 100;
        $posisi = $paging->cari_posisi($batas);
        $jml_data = count($laporan->load_report_detail_cmhs(get('tgl_awal'), get('tgl_akhir'), get('fakultas'), get('prodi'), get('bank'), get('jalur'), get('angkatan')));
        $jml_halaman = $paging->jumlah_halaman($jml_data, $batas);
        $link_halaman = $paging->nav_halaman(get('page'), $jml_halaman);
        if (get('fakultas') != '' || get('prodi') != '') {
            $smarty->assign('data_prodi', $list->load_list_prodi(get('fakultas')));
            $smarty->assign('data_fakultas_one', $list->get_fakultas(get('fakultas')));
            $smarty->assign('data_prodi_one', $list->get_prodi(get('prodi')));
        }
        $smarty->assign('data_report_detail_cmhs', $laporan->load_report_detail_cmhs_page(get('tgl_awal'), get('tgl_akhir'), get('fakultas'), get('prodi'), get('bank'), get('jalur'), get('angkatan'), $batas, $posisi));
        $smarty->assign('link_halaman', $link_halaman);
    } else if (get('mode') == 'fakultas') {
        if (get('fakultas') != '' || get('prodi') != '') {
            $smarty->assign('data_jalur_one', $list->get_jalur(get('jalur')));
            $smarty->assign('data_prodi', $list->load_list_prodi(get('fakultas')));
            $smarty->assign('data_fakultas_one', $list->get_fakultas(get('fakultas')));
            $smarty->assign('data_prodi_one', $list->get_prodi(get('prodi')));
        }
        $smarty->assign('count_data_biaya', count($list->load_biaya()));
        $smarty->assign('data_biaya', $list->load_biaya());
        $smarty->assign('data_report_fakultas', $laporan->load_report_fakultas_cmhs(get('tgl_awal'), get('tgl_akhir'), get('fakultas'), get('prodi'), get('bank'), get('jalur'), get('angkatan')));
    }
}

$smarty->assign('tgl_awal', get('tgl_awal'));
$smarty->assign('tgl_akhir', get('tgl_akhir'));
$smarty->assign('fakultas', get('fakultas'));
$smarty->assign('prodi', get('prodi'));
$smarty->assign('bank', get('bank'));
$smarty->assign('jalur', get('jalur'));
$smarty->assign('data_angkatan', $list->load_angkatan_mhs());
$smarty->assign('data_jalur', $list->load_list_jalur());
$smarty->assign('data_fakultas', $list->load_list_fakultas($id_pt));
$smarty->display('laporan/pembayaran-cmhs.tpl');
?>
