<?php

include 'config.php';
include 'class/utility.class.php';

$utility = new utility($db);
if (isset($_POST['cari_nim'])) {
    $nim = str_replace("'", "''", $_POST['cari_nim']);
    $smarty->assign('data_pembayaran', $utility->detail_pembayaran($nim));
    $smarty->assign('data_bank', $utility->get_bank());
    $smarty->assign('data_semester', $utility->get_semester_all());
    $smarty->assign('data_bank_via', $utility->get_via_bank());
    $smarty->assign('cari_nim', 1);
}
if (isset($_GET['mode'])) {
    $nim = str_replace("'", "''", $_POST['cari_nim']);
    if ($_GET['mode'] == 'biaya') {
        for ($i = 1; $i < $_POST['jumlah']; $i++) {
            $db->Query("UPDATE PEMBAYARAN SET DENDA_BIAYA='{$_POST['denda_biaya' . $i]}'  WHERE ID_PEMBAYARAN='{$_POST['id' . $i]}'");
        }
    }
    $smarty->assign('data_pembayaran', $utility->detail_pembayaran($nim));
    $smarty->assign('data_semester', $utility->get_semester_all());
    $smarty->assign('data_bank', $utility->get_bank());
    $smarty->assign('data_bank_via', $utility->get_via_bank());
    $smarty->assign('cari_nim', 1);
}

$smarty->display('denda/denda-biaya.tpl');
?>
