<?php

include 'config.php';
include 'class/utility.class.php';
include 'class/history.class.php';

$utility = new utility($db);
$history = new history($db);

$id_pt = $id_pt_user;

if (isset($_GET)) {
    if (post('mode') == 'tambah') {
        $db->BeginTransaction();
        try {
            $jumlah_biaya = post('jumlah_biaya');
            $id_tagihan_mhs = post('id_tagihan_mhs');
            $total_pembayaran = post('total_pembayaran');
            $tgl_pengembalian = post('tgl_pengembalian');
            $no_transaksi = date('YmdHi') . rand(10000, 99999);
            $semester = post('semester');
            $bank = post('bank');
            $bank_via = post('bank_via');
            $keterangan = post('keterangan');
            $total_pengembalian = 0;
            if ($jumlah_biaya > 0) {
                $db->Query("
                INSERT INTO PENGEMBALIAN_PEMBAYARAN (BESAR_PENGEMBALIAN,BESAR_PEMBAYARAN,TGL_PENGEMBALIAN,NO_TRANSAKSI, ID_BANK,ID_BANK_VIA,ID_TAGIHAN_MHS,ID_SEMESTER_PENGEMBALIAN,KETERANGAN,CREATED_ON,CREATED_BY)
                VALUES 
                ('0','{$total_pembayaran}','{$tgl_pengembalian}','{$no_transaksi}','{$bank}','{$bank_via}','{$id_tagihan_mhs}','{$semester}','{$keterangan}',CURRENT_TIMESTAMP,'{$user->ID_PENGGUNA}')
                ");
                $last_inserted_id_pembayaran = $db->QuerySingle("SELECT ID_PENGEMBALIAN_PEMBAYARAN FROM PENGEMBALIAN_PEMBAYARAN WHERE NO_TRANSAKSI='{$no_transaksi}'");

                for ($i = 1; $i <= $jumlah_biaya; $i++) {
                    $pengembalian_biaya = post('besar_pengembalian' . $i);
                    $id_biaya = post('id_biaya' . $i);
                    if ($pengembalian_biaya > 0) {
                        $db->Query("
                        INSERT INTO PENGEMBALIAN_PEMBAYARAN_DETAIL (ID_PENGEMBALIAN_PEMBAYARAN,ID_BIAYA, BESAR_BIAYA)
                        VALUES 
                        ('{$last_inserted_id_pembayaran}','{$id_biaya}','{$pengembalian_biaya}')
                        ");
                        $total_pengembalian += $pengembalian_biaya;
                    }                    
                }
                $db->Query("UPDATE PENGEMBALIAN_PEMBAYARAN SET BESAR_PENGEMBALIAN='{$total_pengembalian}' WHERE ID_PENGEMBALIAN_PEMBAYARAN='{$last_inserted_id_pembayaran}'");
            }
            $db->Commit();
        } catch (\Exception $e) {
            $db->Rollback();
            echo '<div class="alert alert-danger" role="alert">Gagal. Proses '.$e->getMessage().'</div>';
        }
    } else if (post('mode') == 'hapus') {
        $db->BeginTransaction();
        try {
            $id_pengembalian = post('id_pengembalian');
            $db->Query("DELETE FROM PENGEMBALIAN_PEMBAYARAN_DETAIL WHERE ID_PENGEMBALIAN_PEMBAYARAN='{$id_pengembalian}'");
            $db->Query("DELETE FROM PENGEMBALIAN_PEMBAYARAN WHERE ID_PENGEMBALIAN_PEMBAYARAN='{$id_pengembalian}'");
            $db->Commit();
        } catch (\Exception $e) {
            $db->Rollback();
            echo '<div class="alert alert-danger" role="alert">Gagal. Proses '.$e->getMessage().'</div>';
        }
    }
    $smarty->assign('data_detail_mahasiswa', $history->get_data_mhs(trim(get('cari')), $id_pt));
    $smarty->assign('data_history_pengembalian', $utility->load_history_pengembalian_pembayaran(get('cari')));
}
$smarty->assign('data_semester', $utility->get_semester_all());
$smarty->assign('data_bank', $utility->get_bank());
$smarty->assign('data_bank_via', $utility->get_via_bank());
$smarty->display('utility/pengembalian-pembayaran.tpl');
