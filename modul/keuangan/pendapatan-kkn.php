<?php

include 'config.php';
include 'class/laporan.class.php';
include 'class/pendapatan.class.php';
include 'class/list_data.class.php';
include 'class/paging.class.php';

$laporan = new laporan($db);
$pendapatan = new pendapatan($db);
$list = new list_data($db);

if (isset($_GET)) {
    if (get('mode') == 'fakultas') {
        $smarty->assign('data_penw_fakultas', $pendapatan->load_data_pendapatan_kkn_mode_fakultas(get('semester')));
    } else if (get('mode') == 'prodi') {
        $smarty->assign('data_fakultas_one', $list->get_fakultas(get('fakultas')));
        $smarty->assign('data_penw_prodi', $pendapatan->load_data_pendapatan_kkn_mode_prodi(get('semester'), get('fakultas')));
    } else if (get('mode') == 'detail') {
        $smarty->assign('data_fakultas_one', $list->get_fakultas(get('fakultas')));
        $smarty->assign('data_prodi_one', $list->get_prodi(get('prodi')));
        $smarty->assign('data_penw_detail', $pendapatan->load_data_pendapatan_kkn_mode_detail(get('semester'), get('fakultas'), get('prodi')));
    }
}

$smarty->assign('tgl_awal', get('tgl_awal'));
$smarty->assign('tgl_akhir', get('tgl_akhir'));
$smarty->assign('bank', get('bank'));

$smarty->assign('data_semester', $list->load_list_semester());
$smarty->display('pendapatan/pendapatan-kkn.tpl');
?>
