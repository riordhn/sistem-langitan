<?php

include 'config.php';
include 'class/kontrol.class.php';

$k = new kontrol($db);

if (isset($_POST)) {
    if (post('mode') == 'lock') {
        $id = post('id');
        $db->Query("UPDATE REKON_PEMBAYARAN SET STATUS_REKON=1 WHERE ID_REKON_PEMBAYARAN='{$id}'");
    } else if (post('mode') == 'unlock') {
        $id = post('id');
        $db->Query("UPDATE REKON_PEMBAYARAN SET STATUS_REKON=0 WHERE ID_REKON_PEMBAYARAN='{$id}'");
    }
}
if (isset($_GET)) {
    if (get('mode') == '') {
        if (get('thn') != '') {
            $data_rekon = $k->load_rekon(get('thn'));
        } else {
            $data_rekon = $k->load_rekon($date['year']);
        }

        $smarty->assign('data_tahun', $db->QueryToArray("SELECT DISTINCT(TAHUN_REKON) TAHUN FROM REKON_PEMBAYARAN ORDER BY TAHUN"));
        $smarty->assign('data_rekon', $data_rekon);
    } else if (get('mode') == 'detail') {
        $id = get('id');
        if (isset($_POST)) {
            if (post('mode') == 'clear') {
                $id_p = post('id');
                $db->Query("UPDATE REKON_PERUBAHAN SET STATUS_CLEAR=1 WHERE ID_REKON_PERUBAHAN='{$id_p}'");
            }
        }
        $data_perubahan = $db->QueryToArray("
            SELECT RP.*,P.NM_PENGGUNA,M.NIM_MHS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,B.NM_BANK,BV.NAMA_BANK_VIA,S.NM_SEMESTER,S.TAHUN_AJARAN,P2.NM_PENGGUNA PERUBAH
            FROM AUCC.REKON_PERUBAHAN RP
            JOIN AUCC.MAHASISWA M ON M.ID_MHS=RP.ID_MHS
            JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
            JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN AUCC.BANK B ON B.ID_BANK=RP.ID_BANK
            JOIN AUCC.BANK_VIA BV ON BV.ID_BANK_VIA=RP.ID_BANK_VIA
            JOIN AUCC.SEMESTER S ON S.ID_SEMESTER=RP.ID_SEMESTER
            JOIN AUCC.PENGGUNA P2 ON P2.ID_PENGGUNA=RP.ID_PENGGUNA
            WHERE RP.ID_REKON_PEMBAYARAN='{$id}'
            ");
        $smarty->assign('data_perubahan', $data_perubahan);
    }
}

$smarty->display('kontrol/kontrol-rekon.tpl');
?>
