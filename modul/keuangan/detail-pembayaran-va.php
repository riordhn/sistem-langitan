<?php

include 'config.php';
include 'class/history.class.php';

$history = new history($db);
$data_tagihan_pembayaran = [];
$id_pt = $id_pt_user;


if (post('mode') == 'hapus_riwayat') {
    $no_kuitansi = post('no_kuitansi');
    
    $db->Query("
    DELETE FROM PEMBAYARAN_PERBULAN WHERE ID_PEMBAYARAN IN (SELECT ID_PEMBAYARAN FROM PEMBAYARAN WHERE NO_KUITANSI='{$no_kuitansi}')
    ");
    $db->Query("
    DELETE FROM PEMBAYARAN WHERE NO_KUITANSI='{$no_kuitansi}'
    ");
}



$smarty->assign('cari', get('cari'));
$smarty->assign('id_pt', $id_pt);
$smarty->assign('jenis', 'mhs');
$smarty->assign('data_detail_mahasiswa', $history->get_data_mhs(trim(get('cari')), $id_pt));
$smarty->assign('data_pembayaran_va', $history->load_riwayat_pembayaran_va_bank(get('trx_id')));
$smarty->display('history/detail-pembayaran-va.tpl');
