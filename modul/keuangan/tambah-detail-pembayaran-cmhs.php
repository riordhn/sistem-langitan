<?php

include 'config.php';
include 'class/master.class.php';

$master = new master($db);

$id_c_mhs = $_GET['id_cmhs'];

$data_biaya = $db->QueryToArray("
    SELECT DB.ID_DETAIL_BIAYA,B.NM_BIAYA
    FROM AUCC.BIAYA_KULIAH_CMHS BK
    JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BK.ID_BIAYA_KULIAH
    JOIN AUCC.BIAYA B ON B.ID_BIAYA=DB.ID_BIAYA
    WHERE BK.ID_C_MHS='{$id_c_mhs}'");
$db->Query("
    SELECT CMB.*,PEN.SEMESTER,PEN.TAHUN 
    FROM CALON_MAHASISWA_BARU CMB
    JOIN PENERIMAAN PEN ON PEN.ID_PENERIMAAN=CMB.ID_PENERIMAAN
    WHERE ID_C_MHS='{$id_c_mhs}'");

$data_cmhs = $db->FetchAssoc();
$id_semester = $db->QuerySingle("SELECT S.ID_SEMESTER FROM AUCC.SEMESTER S WHERE S.TIPE_SEMESTER = 'REG' AND S.THN_AKADEMIK_SEMESTER ='{$data_cmhs['TAHUN']}' AND  SUBSTR(S.NM_SEMESTER,0,2) LIKE SUBSTR('{$data_cmhs['SEMESTER']}',0,2)");
$smarty->assign('id_semester', $id_semester);
$smarty->assign('data_cmhs', $data_cmhs);
$smarty->assign('data_biaya', $data_biaya);
$smarty->assign('data_status', $master->load_status_pembayaran());

$smarty->display('utility/tambah-detail-pembayaran-cmhs.tpl');
?>
