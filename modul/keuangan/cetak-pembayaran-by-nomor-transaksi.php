<?php
include ('config.php');
include ('../../tcpdf/config/lang/ind.php');
include ('../../tcpdf/tcpdf.php');
include ('class/utility.class.php');
include ('class/history.class.php');
include ('class/list_data.class.php');

$history = new history($db);
$list = new list_data($db);
$utility = new utility($db);

$biodata = $history->get_data_mhs(get('cari'),$id_pt_user);
$data_transaksi = $history->load_pembayaran_by_no_transaksi(get('no_transaksi'));

$jabatan_ttd = $utility->find_konfigurasi_keu('KEU_TTD_JABATAN');
$pejabat_ttd = $utility->find_konfigurasi_keu('KEU_TTD_NAMA');

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->AddPage();

$html = <<<EOF
<style>
    .keterangan {font-size: 7pt;font-weight:bold;}
    div { margin-top: 0pt; }
    .header { font-size: 18pt; font-family: times; font-weight: bold; text-align:center;}
    .address { font-size: 12pt; font-family: serif; margin-top: 0px ;text-align:center; }
    .address2 { font-size: 11pt; font-family: serif; margin-top: 0px ;text-align:center; }
    td { font-size: 8pt; }
    td.center { font-weight:bold;text-align:center }
    th { background-color:black;color:white;font-size: 6pt;text-align:center; }
</style>
<table width="100%" border="0">
    <tr>
        <td width="10%" align="right"><img src="../../img/akademik_images/logo-{$nama_singkat}.gif" width="80px" height="80px"/></td>
        <td width="88%" align="center">
            <span class="header">{$nama_pt_kapital}<br/></span>
            <span class="address">{$alamat},{$kota_pt}</span><br/>
            <span class="address2">Telp.{$telp_pt},Website: {$web_pt},email: {$web_email_pt}, pos: {$kd_pos_pt}</span><br/>
            <b style="font-size:13pt">BUKTI PEMBAYARAN </b>
        </td>
    </tr>
</table>
<hr/>
<p></p>
<table width="50%" cellpadding="3" border="0.5">
        <tr>
            <th colspan="2">DESKRIPSI BIODATA</th>
        </tr>
        <tr>
            <td>Nim</td>
            <td>{nim}</td>
        </tr>
        <tr>
            <td>Nama</td>
            <td>{nama}</td>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>{fakultas}</td>
        </tr>
        <tr>
            <td>Program Studi</td>
            <td>({jenjang}) {prodi}</td>
        </tr>
</table>
<p></p>
<table width="50%" cellpadding="3" border="0.5">
    <tr>
        <th colspan="2">DESKRIPSI TRANSAKSI</th>
    </tr>
    <tr>
        <td>TANGGAL BAYAR</td>
        <td>{tgl_bayar}</td>
    </tr>
    <tr>
        <td>BANK</td>
        <td>{bank}</td>
    </tr>
    <tr>
        <td>BANK VIA</td>
        <td>{bank_via}</td>
    </tr>
    <tr>
        <td>NO TRANSAKSI</td>
        <td>{no_transaksi}</td>
    </tr>
</table>
<p></p>
<h2>BIAYA YANG DIBAYARKAN</h2>
<table width="80%" cellpadding="3" border="0.5">
    <tr>
        <th width="10%">NO</th>
        <th width="50%">NAMA BIAYA</th>
        <th width="40%">BESAR PEMBAYARAN</th>
    </tr>
    {data_pembayaran}
    <tr>
        <td colspan="3" class="total center"><b>TOTAL PEMBAYARAN <br/>{total_pembayaran}</b></td>
    </tr>
</table>
<table  width="100%" cellpadding="1" border="0" >
    {ttd}
</table>
<span class="keterangan">Data Ini Di Cetak Pada Tanggal {tanggal}</span>
EOF;
$ttd = '<tr>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td width="70%" colspan="2"></td>
            <td width="30%" colspan="2" align="center">
                Surabaya, ' . strftime('%d %B %Y') . '<br/>
                Mengetahui<br/>
                {jabatan_ttd}
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                {pejabat_ttd}
                <br/>
            </td>
        </tr>';

$index = 1;
$data_pembayaran_str = '';
$total_pembayaran=0;
foreach($data_transaksi as $d){
    $data_pembayaran_str .='
    <tr>        
        <td style="width:10%">'.$index++.'</td>
        <td>'.$d['NM_BIAYA'].'</td>
        <td align="right">'.number_format($d['BESAR_PEMBAYARAN']).'</td>
    </tr>
    ';
    $total_pembayaran+=$d['BESAR_PEMBAYARAN'];
}

$nama = $biodata['NM_PENGGUNA'] != NULL ? $biodata['NM_PENGGUNA'] : $biodata['NM_C_MHS'];
$html = str_replace('{nama}', $nama, $html);
$html = str_replace('{nim}', $biodata['NIM_MHS'], $html);
$html = str_replace('{fakultas}', $biodata['NM_FAKULTAS'], $html);
$html = str_replace('{prodi}', $biodata['NM_PROGRAM_STUDI'], $html);
$html = str_replace('{jenjang}', $biodata['NM_JENJANG'], $html);
$html = str_replace('{tgl_bayar}', date("d/m/Y",strtotime($data_transaksi[0]['TGL_BAYAR'])), $html);
$html = str_replace('{bank}', $data_transaksi[0]['NM_BANK'], $html);
$html = str_replace('{bank_via}', $data_transaksi[0]['NAMA_BANK_VIA'], $html);
$html = str_replace('{no_transaksi}', $data_transaksi[0]['NO_TRANSAKSI'], $html);
$html = str_replace('{nomer_header}', $nomer_header, $html);
$html = str_replace('{besar_biaya_header}', $besar_biaya_header, $html);
$html = str_replace('{all_header}', $all_header, $html);
$html = str_replace('{data_pembayaran}', $data_pembayaran_str, $html);
$html = str_replace('{tanggal}', date('d F Y  H:i:s'), $html);
$html = str_replace('{ttd}', $ttd, $html);
$html = str_replace('{jabatan_ttd}', $jabatan_ttd, $html);
$html = str_replace('{pejabat_ttd}', $pejabat_ttd, $html);
$html = str_replace('{total_pembayaran}', 'Rp.' . number_format($total_pembayaran), $html);

$pdf->writeHTML($html);

$pdf->Output();
?>
