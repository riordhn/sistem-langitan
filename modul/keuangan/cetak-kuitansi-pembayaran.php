<?php
require 'config.php';

require_once 'class/utility.class.php';
require_once 'class/history.class.php';
require_once 'class/list_data.class.php';

$history = new history($db);
$list = new list_data($db);
$utility = new utility($db);

$biodata = $history->get_data_mhs(get('cari'), $id_pt_user);
$transaksi = $history->load_pembayaran_by_no_kuitansi(get('no_kuitansi'));
$data_transaksi = $history->load_pembayaran_all_biaya_by_no_kuitansi(get('no_kuitansi'));
$data_transaksi_biaya = $history->load_detail_riwayat_transaksi_pembayaran(get('id_tagihan'), get('no_transaksi'), get('no_kuitansi'));

// set document informatio
$var = [];

$tglcetak = $history->tanggalInd(date('Y-m-d'));
$kode_transaksi = $_GET['no_transaksi'];
$nama = $biodata['NM_PENGGUNA'];
$nim = $biodata['NIM_MHS'];
$fakultas = $biodata['NM_FAKULTAS'];
$prodi = $biodata['NM_JENJANG'] . ' ' . $biodata['NM_PROGRAM_STUDI'];
$status = $biodata['NM_STATUS_PENGGUNA'];
$jalur = $biodata['NM_JALUR'];
$tgl_bayar = $history->tanggalInd(date("Y-m-d", strtotime($transaksi[0]['TGL_BAYAR'])));
$no_kuitansi = $transaksi[0]['NO_KUITANSI'];
$periode_bayar = !empty($transaksi[0]['PERIODE_BAYAR']) ? $transaksi[0]['PERIODE_BAYAR'] : '-';
$semester = $transaksi[0]['SEMESTER'];
$total_bayar = 0;

// if ($is_tunai == 1) {
//   $is_tunai = "Tunai";
// } else {
//   $is_tunai = "Transfer Tgl. " . $tgl_transfer;
// }

$id_pt = $id_pt_user;

$nm_pengguna = $user->NM_PENGGUNA;
?>
<style type="text/css">
  table {
    width: 100%;
    font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
    font-size: 12px;
    margin: 0px;
    border-collapse: collapse;
  }


  .tbl-total {
    width: 100%;
    border: 1px solid black
  }

  .tbl-head-border {
    width: 100%;
    border-bottom: 1px solid black;
    border-collapse: collapse;
  }

  .tbl-head-border tr,
  .tbl-head-border td {
    border: none
  }

  .tbl-head-border th {
    border: 1px solid black;
    text-align: center;
  }

  .tbl-total {
    width: 100%;
    border: 1px solid black
  }

  .no-margin {
    margin: 0
  }

  .page_break {
    page-break-before: always;
  }
</style>

<body>
  <?php
  $str_nomor = '';
  if (!empty($no_kuitansi)) {
    $str_nomor = 'NOMOR KUITANSI : ' . $no_kuitansi;
  } else {
    $str_nomor = 'NOMOR TRANSAKSI : ' . $kode_transaksi;
  }

  $html = '
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
	  <td width="15%" align="center" valign="middle"><img src="../../img/akademik_images/logo-' . $nama_singkat . '.gif" width="35" border="0"></td>
    <td width="85%" align="left" valign="middle"><font style="font-size:1.1em"><b>' . strtoupper($nama_pt) . '</b></font><br><font  style="font-size:1.2em">' . strtoupper($alamat) . ' Telp. ' . $telp_pt . '</font></td>
  </tr>
  <tr>
    <td colspan="2" align="center"  style="border-top:1px solid black;border-bottom:1px solid black;"><font  style="font-size:1.1em"><b>KWITANSI PEMBAYARAN</b></font><br><font><b>' . $str_nomor . '<br/>SEMESTER : ' . $semester . '</b></font></td>
  </tr>
</table>
  ';
  $html .= '
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="15%"><font>Nama</font></td>
    <td width="45%"><font> : ' . strtoupper($nama) . '</font></td>
    <td width="15%"><font>Status</font></td>
    <td width="25%"><font> : ' . ucwords(strtolower($status)) . '</font></td>
  </tr>
  <tr>
    <td width="15%"><font>NIM</font></td>
    <td width="35%"><font> : ' . strtoupper($nim) . '</font></td>
    <td width="15%"><font>Jalur</font></td>
    <td width="35%"><font> : ' . $jalur . '</font></td>
  
  </tr>
  <tr>
    <td width="15%"><font>Prodi</font></td>
    <td width="35%"><font> : ' . $prodi . '</font></td>
    <td width="15%"><font>Fakultas</font></td>
    <td width="35%"><font> : ' . ucwords(strtolower($fakultas)) . '</font></td>
  </tr>
  <tr>
    <td width="15%"><font>Tgl.Bayar</font></td>
    <td width="35%"><font> : ' . $tgl_bayar . '</font></td>   
    <td width="15%"><font>Periode Bayar</font></td>
    <td width="35%"><font> : ' . $periode_bayar . '</font></td> 
  </tr>
</table>

<table width="100%" border="1" cellpadding="2">

	<tr>
		 <th align="center" width="5%">No</th>
		 <th  align="center" width="70%">Nama Biaya</th>
		 <th  colspan="2" align="center" width="27%">Jumlah</th> 			 
    </tr>
    ';
  $no = 1;
  foreach ($data_transaksi as $d) {
    if ($d['BESAR_PEMBAYARAN'] > 0) {
      $html .= ' <tr>
    <th align="center" width="5%">' . $no++ . '</th>
    <th align="left" width="70%">' . $d['NM_BIAYA'] . '</th>
    <th align="left" width="6%">Rp</th>
    <th align="right" width="21%">' . number_format($d['BESAR_PEMBAYARAN']) . '</th>			 			 
   </tr>';
      $total_bayar += !empty($d['BESAR_PEMBAYARAN']) ? $d['BESAR_PEMBAYARAN'] : 0;
    }
  }
  $keterangan_perbulan = '';
  $terbilang = $history->getTerbilang($total_bayar);
  foreach ($data_transaksi_biaya as $dtb) {
    $index = 1;
    if (count($dtb['DATA_PEMBAYARAN_PERBULAN']) > 0) {
      $keterangan_perbulan .= "Untuk pembayaran bulan ";
    }
    foreach ($dtb['DATA_PEMBAYARAN_PERBULAN'] as $dtbb) {
      if ($index == count($dtb['DATA_PEMBAYARAN_PERBULAN'])) {
        $keterangan_perbulan .= $dtbb['NM_BULAN'];
      } else {
        $keterangan_perbulan .= $dtbb['NM_BULAN'] . ', ';
      }
      $index++;
    }
  }
  $html .= '
    <tr>
		 <th align="left" colspan="2"><b>Total dibayar</b></th>
		 <th align="left" width="6%">Rp</th>
		 <th align="right" width="21%"><b>' . number_format($total_bayar) . '</b></th>	 			 
    </tr>

    <tr>
		 <th align="left" colspan="4"><b><i>Terbilang :  &nbsp; ' . $terbilang . ' rupiah</i></b></th>	 			 
    </tr>
    <tr>
    <th align="left" colspan="4"><b>Keterangan :  &nbsp;' . $keterangan_perbulan . '</i></th>	 			 
   </tr>
</table>
';

  $html .= '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2"><br></td>
  </tr>
  <tr>
    <td width="15%"><font>Lembar 1</font></td>
	<td width="55%"><font> : Untuk Mahasiswa</font></td>
    <td width="30%" align="center"><font>' . ucwords(strtolower($kota_pt)) . ',&nbsp;' . $tglcetak . '</font></td>
  </tr>
  <tr>
    <td width="15%"><font>Lembar 2</font></td>
	<td width="55%"><font> : Bagian Keuangan</font></td>
    <td width="30%" align="center"><font>Petugas</font></td>
  </tr>
  <tr>
    <td colspan="2"><font><b>Catatan</b></font></td>
  </tr>
  <tr>
    <td colspan="3"><font>*) Periksa Kembali Kwitansi yang sudah diterima</font></td>
  </tr>
  <tr>
    <td colspan="2"><font>*) Uang yang sudah dibayarkan tidak bisa dikembalikan</font></td>
	<td width="30%" align="center">' . $nm_pengguna . '</td>
  </tr>
  <tr>
    <td colspan="2"><br><b><i>Waktu cetak :' . date('d/m/y H:i:s') . '</i><b></td>
  </tr>
</table>
';
  echo $html;
  ?>
</body>
<script>
  window.print();
  setTimeout(function() {
    window.close();
  }, 2000);
</script>