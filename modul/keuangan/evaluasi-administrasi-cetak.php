<?php
require('../../config.php');
$db = new MyOracle();

require_once('../../tcpdf/config/lang/eng.php');
require_once('../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('EVALUASI ADMINISTRASI');
$pdf->SetSubject('EVALUASI ADMINISTRASI');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
$logo = "../../img/akademik_images/logounair.gif";
$logo_size = "25";
$title = "UNIVERSITAS AIRLANGGA";

$semester = $_GET['sem'];
$id_fakultas = $_GET['fak'];

		
		if($id_fakultas <> ''){
			$where  = " AND FAKULTAS.ID_FAKULTAS = '$id_fakultas'";
			$where2  = " AND h.ID_FAKULTAS = '$id_fakultas'";
		}else{
			$where  = "";
			$where2  = "";
		}

		
$thn = date('Y');

$evaluasi = $db->QueryToArray("SELECT MAHASISWA.ID_MHS, NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, NM_FAKULTAS, NM_STATUS_PENGGUNA, NM_JENJANG
				, STATUS_CEKAL, FAKULTAS.ID_FAKULTAS, A.PIUTANG,
				(CASE WHEN B.SUDAH_KRS IS NULL THEN 0 ELSE B.SUDAH_KRS END) AS SUDAH_KRS, STATUS_PENGGUNA.ID_STATUS_PENGGUNA,
				EVALUASI_STUDI.KETERANGAN, NM_STATUS_EVALUASI
				FROM MAHASISWA
				LEFT JOIN PENGGUNA ON MAHASISWA.ID_PENGGUNA = PENGGUNA.ID_PENGGUNA
				LEFT JOIN PROGRAM_STUDI ON MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
				LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
				LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
				LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
				JOIN 
				(
					SELECT ID_MHS, COUNT(*) AS PIUTANG FROM 
						(
							SELECT MAHASISWA.ID_MHS, SEMESTER.ID_SEMESTER
							FROM PEMBAYARAN 
							JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBAYARAN.ID_MHS
							JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
							JOIN SEMESTER ON SEMESTER.ID_SEMESTER = PEMBAYARAN.ID_SEMESTER
							WHERE (ID_STATUS_PEMBAYARAN = 2 OR ID_STATUS_PEMBAYARAN IS NULL) AND STATUS_PENGGUNA.STATUS_AKTIF_BAYAR = 1
							AND STATUS_AKTIF_PEMBAYARAN = 'False'
							GROUP BY MAHASISWA.ID_MHS, SEMESTER.ID_SEMESTER
						) GROUP BY ID_MHS HAVING COUNT(*) > 1
				) A ON A.ID_MHS = MAHASISWA.ID_MHS
				LEFT JOIN (
					SELECT COUNT(DISTINCT PENGAMBILAN_MK.ID_MHS) AS SUDAH_KRS, PENGAMBILAN_MK.ID_MHS 
					FROM PENGAMBILAN_MK
					LEFT JOIN SEMESTER ON SEMESTER.ID_SEMESTER = PENGAMBILAN_MK.ID_SEMESTER
					LEFT JOIN MAHASISWA ON PENGAMBILAN_MK.ID_MHS = MAHASISWA.ID_MHS
					LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
					JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
					WHERE SEMESTER.STATUS_AKTIF_SEMESTER = 'True' $where
					GROUP BY PENGAMBILAN_MK.ID_MHS) B ON B.ID_MHS = MAHASISWA.ID_MHS
				LEFT JOIN (select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, 
				sum(kredit_semester) as sks, id_mhs
				from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
				c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
				row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
				count(*) over(partition by c.nm_mata_kuliah) terulang
					from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
					where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
					and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs
					and h.id_program_studi = g.id_program_studi $where2
				) x
				where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
				group by id_mhs) C ON C.ID_MHS = MAHASISWA.ID_MHS
				LEFT JOIN (select id_mhs, sum(kredit_semester) as jml_sks_d, count(*) as jml_nilai_d from (
				select f.NIM_MHS,a.id_mhs,e.tahun_ajaran,e.nm_semester,c.kd_mata_kuliah,c.nm_mata_kuliah,
				d.kredit_semester,a.nilai_huruf, a.flagnilai,
				 row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
				 count(*) over(partition by c.nm_mata_kuliah) terulang
					from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, mahasiswa f, program_studi h
					where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah 
				and a.id_semester=e.id_semester and a.STATUS_APV_PENGAMBILAN_MK='1'  and f.id_mhs = a.id_mhs
				and h.id_program_studi = f.id_program_studi $where2
					) apem
				where APEM.NILAI_HURUF = 'D' AND APEM.RANGKING = 1
				GROUP BY id_mhs
				) D ON D.ID_MHS = MAHASISWA.ID_MHS
				LEFT JOIN EVALUASI_STUDI ON EVALUASI_STUDI.ID_MHS = MAHASISWA.ID_MHS 
				AND EVALUASI_STUDI.ID_SEMESTER = '$semester' AND IS_AKTIF = 1 AND JENIS_EVALUASI = 3
				LEFT JOIN STATUS_EVALUASI ON STATUS_EVALUASI.ID_STATUS_EVALUASI = REKOMENDASI_STATUS
				WHERE STATUS_AKTIF_BAYAR = 1  $where
				ORDER BY NM_JENJANG, NM_PROGRAM_STUDI, NIM_MHS");


$biomhs="select j.nm_jenjang, f.id_fakultas, f.alamat_fakultas, ps.nm_program_studi, f.nm_fakultas, 
			f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas
			from program_studi ps
			left join fakultas f on ps.id_fakultas=f.id_fakultas
			left join jenjang j on ps.id_jenjang=j.id_jenjang
			where f.id_fakultas='".$id_fakultas."'";
$result1 = $db->Query($biomhs)or die("salah kueri 2 ");
while($r1 = $db->FetchRow()) {
	$nmjenjang = $r1[0];
	$fak = $r1[4];
	$alm_fak = $r1[2];
	$pos_fak = $r1[5];
	$tel_fak = $r1[6];
	$fax_fak = $r1[7];
	$web_fak = $r1[8];
	$eml_fak = $r1[9];
}

$content = "FAKULTAS ".strtoupper($fak)."\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

//set margins
$pdf->SetMargins(10, 40, 10);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default footer
//$pdf->setPrintFooter(false);
//$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 10);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('times', '', 9);

// add a page
$pdf->AddPage('L', 'A4');

$pdf->Image('includes/logo_unair.png', 100, 50, 100, '', '', '', '', false, 72);

// set the starting point for the page content
$pdf->setPageMark();



$html = '
<table width="100%" cellpadding="2" border="0.5">
	<tr bgcolor="green" style="color:#fff;">
		<th style="text-align:center" width="3%">NO</th>
		<th style="text-align:center" width="7%">NIM</th>
		<th style="text-align:center" width="20%">NAMA</th>
		<th style="text-align:center" width="20%">PRODI</th>
		<th style="text-align:center" width="8%">STATUS TERKINI</th>
		<th style="text-align:center" width="6%">STATUS KRS</th>
		<th style="text-align:center" width="6%">PIUTANG</th>
		<th style="text-align:center" width="10%">REKOMENDASI STATUS</th>
		<th style="text-align:center" width="20%">KETERANGAN</th>	
	</tr>';
	$no = 1;
	foreach($evaluasi as $data){
	
	
	if($data['SUDAH_KRS'] == 1){
		$krs = 'Sudah';
	}else{
		$krs = 'Belum';
	}
	
$html .= '<tr>
		<td style="text-align:center">'.$no++.'</td>
		<td>'.$data['NIM_MHS'].'</td>
		<td>'.$data['NM_PENGGUNA'].'</td>
		<td>'.$data['NM_JENJANG'].' - '.$data['NM_PROGRAM_STUDI'].'</td>
		<td>'.$data['NM_STATUS_PENGGUNA'].'</td>
		<td>'.$krs.'</td>
		<td style="text-align:center">'.$data['PIUTANG'].'</td>
		<td>'.$data['NM_STATUS_EVALUASI'].'</td>
		<td>'.$data['KETERANGAN'].'</td>
	</tr>';
	}
$html .= '</table>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('evaluasi-administrasi.pdf', 'I');
?>