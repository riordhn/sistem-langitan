<?php

include 'config.php';
include 'class/list_data.class.php';
include 'class/laporan.class.php';
include('../ppmb/class/Penerimaan.class.php');
include 'class/pendaftaran.class.php';


$list = new list_data($db);
$laporan = new laporan($db);
$penerimaan = new Penerimaan($db);
$daftar = new pendaftaran($db);

if (isset($_GET)) {
    if (get('mode') == 'laporan') {
        $smarty->assign('data_kelompok',$laporan->load_kelompok_biaya_penerimaan(get('penerimaan')));
        $smarty->assign('data_kelompok_verifikasi',$laporan->load_kelompok_biaya_penerimaan_verifikasi(get('penerimaan')));
        $smarty->assign('data_jadwal', $daftar->load_jadwal_verifikasi_keuangan(get('penerimaan')));
        $smarty->assign('data_laporan', $laporan->load_verifikasi_data(get('fakultas'), get('penerimaan')));
    } else if (get("mode") == 'detail') {
        $smarty->assign('data_detail', $laporan->load_detail_verifikasi_data(get('penerimaan'), get('fakultas'), get('status')));
    }
}

$smarty->assign('penerimaan_set', $penerimaan->GetAllForView());
$smarty->assign('data_prodi', $list->load_list_prodi(get('fakultas')));
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->display('pendaftaran/verifikasi-data.tpl');
?>
