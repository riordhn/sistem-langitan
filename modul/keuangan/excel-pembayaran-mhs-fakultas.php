<?php
include 'config.php';
include 'class/laporan.class.php';
include 'class/list_data.class.php';

$list = new list_data($db);
$laporan = new laporan($db);

$data_laporan_row = $laporan->load_report_fakultas_mhs_row(get('tgl_awal'), get('tgl_akhir'), get('fakultas'), get('prodi'), get('periode_bayar'), get('bank'), get('jalur'), get('angkatan'));
$data_laporan_value = $laporan->load_report_fakultas_mhs_row_value(get('tgl_awal'), get('tgl_akhir'), get('fakultas'), get('prodi'), get('periode_bayar'), get('bank'), get('jalur'), get('angkatan'));

$data_biaya = $list->load_biaya();
if (get('fakultas') != '' || get('prodi') != '') {
    $fakultas = $list->get_fakultas(get('fakultas'));
    $prodi = $list->get_prodi(get('prodi'));
    $jalur = $list->get_jalur(get('jalur'));
}
ob_start();
?>
<style>
    .header_text {
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #2b1212;
    }

    td {
        text-align: left;
    }
</style>
<table width='950' border='1' align='left' cellpadding='0' cellspacing='0' class='keuangan'>
    <thead>
        <tr>
            <td colspan="<?php
                            if (get('fakultas') != '') {
                                $colspan = count($data_biaya) + 6;
                            } else {
                                $colspan = count($data_biaya) + 5;
                            }
                            echo $colspan;
                            ?>" class="header_text">
                <?php
                echo 'Rekapitulasi Pembayaran Mahasiswa<br/>';
                if ($fakultas != '') {
                    echo 'Fakultas ' . $fakultas['NM_FAKULTAS'] . '<br/>';
                }
                if ($prodi != '') {
                    echo 'Program Studi (' . $prodi['NM_JENJANG'] . ') ' . $prodi['NM_PROGRAM_STUDI'] . '<br/>';
                    echo 'Jalur  ' . $jalur['NM_JALUR'] . '<br/>';
                }
                ?>
            </td>
        </tr>
        <tr>
            <th>No</th>
            <th>
                <?php
                if (get('prodi') != '') {
                    echo 'Nama';
                } else if (get('fakultas') != '') {
                    echo 'Program Studi ';
                } else {
                    echo 'Fakultas';
                }
                ?>
            </th>
            <?php
            if (get('prodi')) {
            ?>
                <th>NIM</th>
            <?php
            }
            if (get('prodi') != '') {
                echo '<th>Program Studi</th>';
            } else if (get('fakultas') != '') {
                echo '<th>Jalur</th>';
            }
            if (get('prodi') == '') {
                echo '<th>Jumlah Mhs</th>';
            }
            ?>
            <?php
            foreach ($data_biaya as $data) {
                echo "<th>{$data['NM_BIAYA']}</th>";
            }
            ?>
            <th>Total Pembayaran</th>
            <th>Denda Pembayaran</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        $total_pembayaran_bottom = array();
        foreach ($data_laporan_row as $data) {
            echo "<tr>
                        <td>{$no}</td>";
            if (get('prodi') != '') {
                echo "<td> {$data['NM_PENGGUNA']}</td>";
            } else if (get('fakultas') != '') {
                echo "<td>({$data['NM_JENJANG']}) {$data['NM_PROGRAM_STUDI']}</td>";
            } else {
                echo "<td>{$data['NM_FAKULTAS']}</td>";
            }
            if (get('prodi') != '') {
                echo "<td> '{$data['NIM_MHS']}</td><td>{$data['NM_PROGRAM_STUDI']}</td>";
            } else if (get('fakultas') != '')
                echo "<td>{$data['NM_JALUR']}</td><td>{$data['JUMLAH_MHS']}</td>";
            else
                echo "<td>{$data['JUMLAH_MHS']}</td>";
            $total_mhs += $data['JUMLAH_MHS'];
            $total_pembayaran_right = 0;
            $denda_pembayaran_right = 0;
            $no_pembayaran = 0;
            foreach ($data_biaya as $db) {
                $pembayaran = 0;
                foreach ($data_laporan_value as $data_pembayaran) {
                    if (!empty($data['THN_ANGKATAN_MHS'])) {
                        if (
                            $data['ID_PROGRAM_STUDI'] == $data_pembayaran['ID_PROGRAM_STUDI'] &&
                            $data['THN_ANGKATAN_MHS'] == $data_pembayaran['THN_ANGKATAN_MHS'] &&
                            $data['ID_JALUR'] == $data_pembayaran['ID_JALUR'] &&
                            $db['ID_BIAYA'] == $data_pembayaran['ID_BIAYA']
                        ) {
                            $pembayaran = $data_pembayaran['BESAR_BIAYA'];

                            $total_pembayaran_right += $pembayaran;
                            $denda_pembayaran_right += $data_pembayaran['DENDA_BIAYA'];
                        }
                    } else {
                    }
                }

                echo "<td>{$pembayaran}</td>";
                $total_pembayaran_bottom[$no_pembayaran] += $pembayaran;
                $no_pembayaran++;
            }
            $no++;
            echo "<td>{$total_pembayaran_right}</td>
                    <td>{$denda_pembayaran_right}</td>
                    </tr>";
            $total_denda += $denda_pembayaran_right;
        }
        ?>
        <tr>
            <td bgcolor='#CCFF99' colspan="<?php
                                            if (get('prodi') != '')
                                                $colspan = 4;
                                            else if (get('fakultas') != '')
                                                $colspan = 3;
                                            else
                                                $colspan = 2;
                                            echo $colspan
                                            ?>">Jumlah</td>
            <?php if (get('prodi') == '') echo "<td bgcolor='#CCFF99'>" . $total_mhs . "</td>"; ?>
            <?php
            $no_total = 0;
            foreach ($data_biaya as $data) {
                echo "<td bgcolor='#CCFF99'>{$total_pembayaran_bottom[$no_total]}</td>";
                $no_total++;
            }
            ?>
            <td bgcolor='#CCFF99' align='right' colspan="2"><?php echo array_sum($total_pembayaran_bottom) + $total_denda; ?></td>
        </tr>
    </tbody>
</table>

<?php
echo '<b>Data Ini Di Ambil Pada Tanggal ' . date('d F Y  H:i:s') . '</b>';

$nm_file = "detail-pembayaran-mhs-fakultas__" . date('d-m-Y');
header("Content-disposition: attachment; filename=$nm_file.xls");
header("Content-type: Application/exe");
header("Content-Transfer-Encoding: binary");
ob_flush();
?>