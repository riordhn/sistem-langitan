<?php

include 'config.php';
include 'class/master.class.php';
include 'class/history.class.php';

$master = new master($db);
$history = new history($db);
$smarty->assign('cari', get('cari'));
$smarty->assign('is_va', get('is_va'));
$smarty->assign('id_tagihan_mhs', get('id_tagihan_mhs'));
$smarty->assign('data_detail_biaya_tagihan', $history->load_riwayat_pembayaran_biaya(get('id_tagihan_mhs')));
$smarty->display('history/ubah-master-tagihan.tpl');
?>
