<?php
include 'config.php';
include 'class/laporan.class.php';
include 'class/utility.class.php';

$laporan = new laporan($db);
$utility = new utility($db);

$data_excel = $utility->load_data_mahasiswa_virtual_account($id_pt, get('fakultas'), get('prodi'), get('status'), get('semester'), get('angkatan'), get('tagihan'), get('p_sebelum'));
ob_start();
?>
<style>
    .header_text {
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #2b1212;
    }

    td {
        text-align: left;
    }
</style>
<table border='1' align='left' cellpadding='0' cellspacing='0' class='keuangan'>
    <thead>
        <tr>
            <td class="header_text">NO MVA</td>
            <td class="header_text">Key2</td>
            <td class="header_text">Key3</td>
            <td class="header_text">currency</td>
            <td class="header_text">NIM</td>
            <td class="header_text">NAMA</td>
            <td class="header_text">PROG STUDI</td>
            <td class="header_text">ANGKATAN</td>
            <td class="header_text">KETERANGAN</td>
            <td class="header_text">Bill Info 06</td>
            <td class="header_text">Bill Info 07</td>
            <td class="header_text">Bill Info 08</td>
            <td class="header_text">Bill Info 09</td>
            <td class="header_text">Bill Info 10</td>
            <td class="header_text">Bill Info 11</td>
            <td class="header_text">Bill Info 12</td>
            <td class="header_text">Bill Info 13</td>
            <td class="header_text">Bill Info 14</td>
            <td class="header_text">Bill Info 15</td>
            <td class="header_text">Bill Info 16</td>
            <td class="header_text">Bill Info 17</td>
            <td class="header_text">Bill Info 18</td>
            <td class="header_text">Bill Info 19</td>
            <td class="header_text">Bill Info 20</td>
            <td class="header_text">Bill Info 21</td>
            <td class="header_text">Bill Info 22</td>
            <td class="header_text">Bill Info 23</td>
            <td class="header_text">Bill Info 24</td>
            <td class="header_text">Bill Info 25</td>
            <td class="header_text">Periode Open</td>
            <td class="header_text">Periode Close</td>
            <td class="header_text">SubBill 01</td>
            <td class="header_text">SubBill 02</td>
            <td class="header_text">SubBill 03</td>
            <td class="header_text">SubBill 04</td>
            <td class="header_text">SubBill 05</td>
            <td class="header_text">SubBill 06</td>
            <td class="header_text">SubBill 07</td>
            <td class="header_text">SubBill 08</td>
            <td class="header_text">SubBill 09</td>
            <td class="header_text">SubBill 10</td>
            <td class="header_text">SubBill 11</td>
            <td class="header_text">SubBill 12</td>
            <td class="header_text">SubBill 13</td>
            <td class="header_text">SubBill 14</td>
            <td class="header_text">SubBill 15</td>
            <td class="header_text">SubBill 16</td>
            <td class="header_text">SubBill 17</td>
            <td class="header_text">SubBill 18</td>
            <td class="header_text">SubBill 19</td>
            <td class="header_text">SubBill 20</td>
            <td class="header_text">SubBill 21</td>
            <td class="header_text">SubBill 22</td>
            <td class="header_text">SubBill 23</td>
            <td class="header_text">SubBill 24</td>
            <td class="header_text">SubBill 25</td>
            <td class="header_text">end record</td>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach ($data_excel as $data) {
            if ($data['NO_VA']) {
                $awal_periode = date('Ymd', strtotime($data['AWAL_PERIODE']));
                $akhir_periode = date('Ymd', strtotime($data['AKHIR_PERIODE']));
                $total_biaya = "01\TOTAL\TOTAL" . "\\" . $data['TOTAL_BESAR_BIAYA'];
                echo "<tr>
                <td>{$data['NO_VA']}</td>
                <td></td>
                <td></td>
                <td>IDR</td>
                <td>{$data['NM_PENGGUNA']}</td>
                <td>{$data['NIM_MHS']}</td>
                <td>{$data['NM_PROGRAM_STUDI']}</td>
                <td>{$data['THN_ANGKATAN_MHS']}</td>
                <td>{$data['KETERANGAN']}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>" . $awal_periode . "</td>
                <td>" . $akhir_periode . "</td>
                <td>" . $total_biaya . "</td>
                <td>\\\\\\</td>
                <td>\\\\\\</td>
                <td>\\\\\\</td>
                <td>\\\\\\</td>
                <td>\\\\\\</td>
                <td>\\\\\\</td>
                <td>\\\\\\</td>
                <td>\\\\\\</td>
                <td>\\\\\\</td>
                <td>\\\\\\</td>
                <td>\\\\\\</td>
                <td>\\\\\\</td>
                <td>\\\\\\</td>
                <td>\\\\\\</td>
                <td>\\\\\\</td>
                <td>\\\\\\</td>
                <td>\\\\\\</td>
                <td>\\\\\\</td>
                <td>\\\\\\</td>
                <td>\\\\\\</td>
                <td>\\\\\\</td>
                <td>\\\\\\</td>
                <td>\\\\\\</td>
                <td>\\\\\\</td>
                <td>~</td>
			</tr>";
            }
        }
        ?>
    </tbody>
</table>

<?php
$nm_file = "excel-virtual-account-" . date('d-m-Y');
header("Content-disposition: attachment; filename=$nm_file.xls");
header("Content-type: Application/exe");
header("Content-Transfer-Encoding: binary");
ob_flush();
?>