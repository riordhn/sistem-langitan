<?php

include 'config.php';
include 'class/list_data.class.php';
include 'class/laporan.class.php';
include('../ppmb/class/Penerimaan.class.php');
include 'class/pendaftaran.class.php';

$id_pt = $id_pt_user;

$list = new list_data($db);
$laporan = new laporan($db);
$penerimaan = new Penerimaan($db);

if (isset($_GET)) {
    if (get('mode') == 'laporan') {
        $smarty->assign('data_kelompok', $laporan->load_kelompok_biaya_penerimaan_verifikasi(get('penerimaan')));
        $smarty->assign('data_laporan', $laporan->load_laporan_pengelompokan_biaya(get('fakultas'), get('penerimaan')));
    } else if (get('mode') == 'detail') {
        $id_penerimaan = get('penerimaan');
        $id_kelompok = get('kelompok');
        $get_status = get('status');
        
        if ($get_status == 1) {
            $status_cmhs = "Bayar Lebih";
        } else if ($get_status == 2) {
            $status_cmhs = "Belum Bayar";
        } else if ($get_status == 3) {
            $status_cmhs = "Sudah Bayar";
        }
        $smarty->assign('penerimaan', $laporan->get_penerimaan($id_penerimaan));
        $smarty->assign('kelompok', $laporan->get_kelompok_biaya($id_kelompok));
        $smarty->assign('status', $status_cmhs);
        $smarty->assign('data_laporan', $laporan->load_detail_pengelompokan_biaya($id_penerimaan, $id_kelompok, $get_status));
    }
}

$smarty->assign('penerimaan_set', $penerimaan->GetAllForView());
$smarty->assign('data_prodi', $list->load_list_prodi(get('fakultas')));
$smarty->assign('data_fakultas', $list->load_list_fakultas($id_pt));
$smarty->display('pendaftaran/lap-kelompok-biaya.tpl');
?>
