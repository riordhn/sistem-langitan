<?php

include 'config.php';
include 'class/utility.class.php';
include 'class/history.class.php';

$list = new utility($db);
$history = new history($db);
$data_transaksi = $history->load_pembayaran_by_no_transaksi(get('no_transaksi'));
$smarty->assign('cari', get('cari'));
$smarty->assign('data_transaksi', $data_transaksi);
$smarty->display('history/update-upload-transaksi.tpl');
?>
