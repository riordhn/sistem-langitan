<?php

include 'config.php';
include 'class/utility.class.php';

$ut = new utility($db);

if (isset($_GET)) {
    if (get('mode') == 'view') {
        $smarty->assign('data_biaya', $ut->load_detail_pengembalian(get('pengembalian')));
    } 
}


$smarty->display('utility/pengembalian-detail.tpl');
?>
