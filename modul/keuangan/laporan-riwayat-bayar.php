<?php

include 'config.php';
include 'class/list_data.class.php';
include 'class/piutang.class.php';
$list = new list_data($db);
$piutang = new piutang($db);

$id_pt = $id_pt_user;

if (get('submit') != '') {
    $smarty->assign('data_piutang', $piutang->load_data_puitang(get('fakultas'), get('prodi'),get('status')));
    $smarty->assign('data_semester', $piutang->load_list_semester());
}
$smarty->assign('status',get('status'));
$smarty->assign('fakultas',get('fakultas'));
$smarty->assign('prodi',get('prodi'));
$smarty->assign('data_prodi',$list->load_list_prodi($id_pt, get('fakultas')));
$smarty->assign('data_fakultas', $list->load_list_fakultas($id_pt));
$smarty->assign('data_status', $piutang->load_list_status());
$smarty->display('laporan/laporan-riwayat-bayar.tpl');
?>
