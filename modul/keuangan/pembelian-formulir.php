<?php

include 'config.php';
include 'class/laporan.class.php';
include 'class/list_data.class.php';
include 'class/paging.class.php';

$laporan = new laporan($db);
$list = new list_data($db);

if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        $smarty->assign('data_pembelian_formulir', $laporan->load_data_rekap_pembelian_formulir(get('tgl_awal'), get('tgl_akhir')));
    }
}

$smarty->assign('tgl_awal', get('tgl_awal'));
$smarty->assign('tgl_akhir', get('tgl_akhir'));
$smarty->assign('fakultas', get('fakultas'));
$smarty->display('laporan/pembelian-formulir.tpl');
?>
