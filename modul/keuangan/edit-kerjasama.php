<?php

include 'config.php';
include 'class/utility.class.php';
include 'class/history.class.php';
include 'class/master.class.php';

$utility = new utility($db);
$history = new history($db);
$master = new master($db);

$id_pt = $id_pt_user;

if (isset($_GET)) {
    if (post('mode') == 'tambah') {
        $utility->insert_beasiswa_mahasiswa(post('id_mhs'), post('beasiswa'), post('semester_mulai'), post('semester_selesai'), post('keterangan'));
    } else if (post('mode') == 'update') {
        $utility->update_beasiswa_mahasiswa(post('id_sejarah_beasiswa'), post('beasiswa'), post('semester_mulai'), post('semester_selesai'), post('keterangan'), post('aktif'));
    } else if (post('mode') == 'delete') {
        $utility->delete_beasiswa_mahasiswa(post('id_sejarah_beasiswa'));
    }
    $smarty->assign('data_detail_mahasiswa', $history->get_data_mhs(trim(get('cari')),$id_pt));
    $smarty->assign('data_sejarah_beasiswa', $history->load_sejarah_kerjasama(get('cari')),$id_pt);
}
$smarty->assign('data_semester', $utility->get_semester_all());
$smarty->assign('data_kerjasama', $master->load_kerjasama());
$smarty->display('utility/edit-kerjasama.tpl');
?>
