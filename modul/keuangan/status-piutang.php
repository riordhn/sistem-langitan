<?php

include 'config.php';
include 'class/master.class.php';

$status = new master($db);

$mode = get('mode', 'view');

if ($request_method == 'POST') {
    if (post('mode') == 'add') {
        $status->add_status_piutang(post('nama'), post('keterangan'));
    } else if (post('mode') == 'edit') {
        $status->edit_status_piutang(post('id_status'), post('nama'), post('keterangan'));
    }
}
if ($mode == 'detail' || $mode == 'edit' || $mode == 'upload' || $mode == 'delete') {
    $smarty->assign('data_status', $status->get_status_piutang(get('id_status')));
} else {
    $smarty->assign('data_status', $status->load_status_piutang());
}


$smarty->display("master/{$mode}-status-piutang.tpl");
?>