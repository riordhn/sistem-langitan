<?php

include 'config.php';
include 'class/master.class.php';
include 'class/list_data.class.php';

$biaya = new master($db);
$list = new list_data($db);

$id_pt = $id_pt_user;

$mode = get('mode', 'view');

if ($request_method == 'POST') {
    $id_biaya_kuliah = post('id_biaya_kuliah');
    $semester = post('semester');
    $fakultas = post('fakultas');
    $prodi = post('prodi');
    $jenjang = post('jenjang');
    $jalur = post('jalur');
    $kelompok_biaya = post('kelompok_biaya');
    $keterangan = post('keterangan');
    if (post('mode') == 'add') {
        if (count($biaya->load_biaya_kuliah($semester, $fakultas, $prodi, $jenjang, $jalur, $kelompok_biaya)) > 0) {
            $smarty->assign('status', 'error');
        } else {
            $biaya->add_biaya_kuliah($semester, $prodi, $jenjang, $jalur, $kelompok_biaya, $keterangan);
            $biaya_kuliah_added = $biaya->get_biaya_kuliah_added($semester, $prodi, $jalur, $kelompok_biaya, $keterangan);
            $smarty->assign('data_biaya_kuliah_added', $biaya_kuliah_added);
            $smarty->assign('status', 'success');
        }
    } else if (post('mode') == 'edit') {
        for ($i = 1; $i <= post('jumlah'); $i++) {
            if (post('besar_biaya' . $i) != '' && post('id_jenis_detail_biaya' . $i) != '') {
                $cek_detail_biaya = $biaya->cek_detail_biaya($id_biaya_kuliah, post('id_biaya' . $i));
                if ($cek_detail_biaya > 1) {
                    $biaya->delete_detail_biaya($id_biaya_kuliah, post('id_biaya' . $i));
                    $biaya->add_detail_biaya($id_biaya_kuliah, post('id_biaya' . $i), post('besar_biaya' . $i), post('id_jenis_detail_biaya' . $i));
                } else if ($cek_detail_biaya == 0) {
                    $biaya->add_detail_biaya($id_biaya_kuliah, post('id_biaya' . $i), post('besar_biaya' . $i), post('id_jenis_detail_biaya' . $i));
                } else if ($cek_detail_biaya == 1) {
                    $biaya->update_detail_biaya($id_biaya_kuliah, post('id_biaya' . $i), post('besar_biaya' . $i), post('id_jenis_detail_biaya' . $i));
                }
                $biaya->update_biaya_kuliah($id_biaya_kuliah, post('total_biaya'));
            }
        }
    } else if (post('mode') == 'copy') {
        if (count($biaya->load_biaya_kuliah($semester, $fakultas, $prodi, $jenjang, $jalur, $kelompok_biaya)) > 0) {
            $smarty->assign('status_copy', alert_error("Copy Biaya Gagal Master Biaya Sudah Ada"));
        } else {
            $biaya->add_biaya_kuliah($semester, $prodi, $jenjang, $jalur, $kelompok_biaya, $keterangan);
            $biaya_ditambah = $biaya->get_biaya_kuliah_added($semester, $prodi, $jalur, $kelompok_biaya, $keterangan);
            $biaya->copy_master_biaya($id_biaya_kuliah, $biaya_ditambah['ID_BIAYA_KULIAH']);
            $smarty->assign('id_biaya_kuliah_baru', $biaya_ditambah['ID_BIAYA_KULIAH']);
            $smarty->assign('status_copy', alert_success("Copy Biaya Berhasil"));
        }
    } else if (post('mode') == 'edit-master') {
        $biaya->update_master_biaya_kuliah(post('id_biaya_kuliah'), post('jalur'), post('kelompok_biaya'), post('semester'), post('keterangan'));
    } else if (post('mode') == 'delete') {
        $id = post('id_biaya_kuliah');
        $db->Query("DELETE DETAIL_BIAYA WHERE ID_BIAYA_KULIAH = '{$id}'");
        $db->Query("DELETE BIAYA_KULIAH WHERE ID_BIAYA_KULIAH = '{$id}'");
        $smarty->assign('status_delete', alert_success("Master biaya berhasil Di hapus"));
    }
}




$mode_get_biaya = array('edit', 'detail', 'upload', 'delete', 'cop');
if (in_array($mode, $mode_get_biaya)) {
    $smarty->assign('data_jenis_detail_biaya', $biaya->load_jenis_detail_biaya());
    $smarty->assign('data_biaya_kuliah_one', $biaya->get_biaya_kuliah(get('id_biaya_kuliah')));
    $smarty->assign('data_detail_biaya', $biaya->load_detail_biaya(get('id_biaya_kuliah')));
    $smarty->assign('data_jalur', $list->load_list_jalur());
    $smarty->assign('data_semester', $list->load_list_semester());
    $smarty->assign('data_kelompok_biaya', $list->load_list_kelompok_biaya());
} else if ($mode == 'mhs') {
    $id_bk = get('bk');
    $data_mhs = $db->QueryToArray("
        SELECT M.*,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JAL.NM_JALUR,SP.NM_STATUS_PENGGUNA,F.NM_FAKULTAS
        FROM MAHASISWA M
        JOIN PENGGUNA P ON M.ID_PENGGUNA=P.ID_PENGGUNA
        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
        JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAs
        JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
        LEFT JOIN ADMISI JM ON JM.ID_MHS=M.ID_MHS AND JM.ID_JALUR IS NOT NULL
        LEFT JOIN JALUR JAL ON JAL.ID_JALUR=JM.ID_JALUR
        JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA=M.STATUS_AKADEMIK_MHS
        JOIN BIAYA_KULIAH_MHS BKM ON BKM.ID_MHS=M.ID_MHS
        WHERE BKM.ID_BIAYA_KULIAH='{$id_bk}'
        ORDER BY PS.ID_FAKULTAS,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,M.THN_ANGKATAN_MHS,M.NIM_MHS,P.NM_PENGGUNA
       ");
    $smarty->assign('data_mhs', $data_mhs);
} else {
    $smarty->assign('data_jalur', $list->load_list_jalur());
    $smarty->assign('data_jenjang', $list->load_list_jenjang());
    $smarty->assign('data_semester', $list->load_list_semester());
    $smarty->assign('data_kelompok_biaya', $list->load_list_kelompok_biaya());
    $smarty->assign('data_fakultas', $list->load_list_fakultas($id_pt));
    $smarty->assign('data_prodi', $list->load_list_prodi($id_pt, get('fakultas'), get('jenjang')));
    if (get('semester') != '' || get('fakultas') != '' || get('prodi') != '' || get('jenjang') != '' || get('jalur') != '' || get('kelompok_biaya') != '')
        $smarty->assign('data_biaya_kuliah', $biaya->load_biaya_kuliah(get('semester'), get('fakultas'), get('prodi'), get('jenjang'), get('jalur'), get('kelompok_biaya')));
    else if ($_GET['mode'] != NULL) {
        $smarty->assign('overload', true);
    }
}


$smarty->assign('jalur', get('jalur'));
$smarty->assign('jenjang', get('jenjang'));
$smarty->assign('semester', get('semester'));
$smarty->assign('kelompok_biaya', get('kelompok_biaya'));
$smarty->assign('fakultas', get('fakultas'));
$smarty->assign('prodi', get('prodi'));
$smarty->display("master/{$mode}-biaya.tpl");
?>