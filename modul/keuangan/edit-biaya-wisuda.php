<?php

include "config.php";
include 'class/history.class.php';

$history = new history($db);

$id_pt = $id_pt_user;

$periode = $db->QueryToArray("SELECT * FROM TARIF_WISUDA WHERE ID_PERGURUAN_TINGGI = {$id_pt}");
$smarty->assign('periode', $periode);

if (isset($_GET['nim'])) {

    $bank = $db->QueryToArray("SELECT * FROM BANK");
    $smarty->assign('bank', $bank);

    $bank_via = $db->QueryToArray("SELECT * FROM BANK_VIA");
    $smarty->assign('bank_via', $bank_via);

    $periode = $db->QueryToArray("SELECT * FROM TARIF_WISUDA WHERE ID_PERGURUAN_TINGGI = {$id_pt}");
    $smarty->assign('periode', $periode);

    $db->Query("SELECT COUNT(*) AS ADA FROM PENGAJUAN_WISUDA
								JOIN MAHASISWA ON MAHASISWA.ID_MHS = PENGAJUAN_WISUDA.ID_MHS
                                JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
								WHERE NIM_MHS = '$_GET[nim]' AND PENGGUNA.ID_PERGURUAN_TINGGI = {$id_pt}");
    $mhs = $db->FetchArray();
    $db->Query("SELECT COUNT(*) ADA FROM PEMBAYARAN_WISUDA WHERE ID_MHS IN (SELECT ID_MHS FROM MAHASISWA 
                                                                                JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA 
                                                                                WHERE NIM_MHS='{$_GET['nim']}' AND PENGGUNA.ID_PERGURUAN_TINGGI = {$id_pt})");
    $cek_pembayaran = $db->FetchAssoc();
    if ($mhs['ADA'] == 1) {
        if ($cek_pembayaran['ADA']) {
            $status_form = '
            <div class="alert_biaya_kuliah ui-widget" style="margin-left:15px;">
                <div class="ui-state-error ui-corner-all" style="padding: 5px;width:30%;"> 
                    <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                        Data Pembayaran Sudah Ada.</p>
                </div>
            </div>';
            $smarty->assign('status_form', $status_form);
        } else {
            $db->Query("SELECT SUM(PEMBAYARAN.BESAR_BIAYA) AS JML FROM PEMBAYARAN
					JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBAYARAN.ID_MHS
                    JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA AND PENGGUNA.ID_PERGURUAN_TINGGI = {$id_pt}
					JOIN DETAIL_BIAYA ON DETAIL_BIAYA.ID_DETAIL_BIAYA = PEMBAYARAN.ID_DETAIL_BIAYA
					WHERE NIM_MHS = '$_GET[nim]' AND ID_BIAYA = 115");
            $cicilan = $db->FetchArray();

            $db->Query("SELECT BESAR_BIAYA, MAHASISWA.ID_MHS, ID_PERIODE_WISUDA FROM PERIODE_WISUDA
					JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_JENJANG = PERIODE_WISUDA.ID_JENJANG
					JOIN MAHASISWA ON MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
                    JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA AND PENGGUNA.ID_PERGURUAN_TINGGI = {$id_pt}
					WHERE ID_TARIF_WISUDA = '$_GET[periode]' AND NIM_MHS = '$_GET[nim]'");
            $tarif = $db->FetchArray();

            $tagihan = $tarif['BESAR_BIAYA'] - $cicilan['JML'];

            $smarty->assign('tagihan', $tagihan);
            $smarty->assign('tarif', $tarif['BESAR_BIAYA']);
            $smarty->assign('cicilan', $cicilan['JML']);
            $smarty->assign('id_mhs', $tarif['ID_MHS']);
            $smarty->assign('id_periode', $tarif['ID_PERIODE_WISUDA']);
            $smarty->assign('data_detail_mahasiswa', $history->get_data_mhs(trim(get('nim')),$id_pt));
        }
    }
}

if (isset($_POST['id_mhs'])) {

    $db->Query("SELECT ID_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True' AND ID_PERGURUAN_TINGGI = {$id_pt}");
    $semester = $db->FetchArray();
    $besar_tagihan = str_replace(',', '', $_POST['tagihan']);
    if ($_POST['bank'] != '' and $_POST['bank_via'] != '' and $_POST['tgl_bayar']) {
        $bayar = $db->Query("INSERT INTO PEMBAYARAN_WISUDA (ID_MHS, ID_SEMESTER, ID_PERIODE_WISUDA, ID_BANK, ID_BANK_VIA, 
												BESAR_BIAYA, TGL_BAYAR, NO_TRANSAKSI, KETERANGAN, ABSENSI_WISUDA)
				VALUES ('{$_POST['id_mhs']}', '{$semester['ID_SEMESTER']}', '{$_POST['id_periode']}', '{$_POST['bank']}', '{$_POST['bank_via']}',
                                '{$besar_tagihan}', '{$_POST['tgl_bayar']}', '{$_POST['no_transaksi']}', '{$_POST['keterangan']}', '1')") or die("gagal simpan");
        if ($bayar) {
            $status_form = '
        <div class="alert_biaya_kuliah ui-widget" style="margin-left:15px;">
            <div class="ui-state-highlight ui-corner-all" style="padding: 5px;width:30%;"> 
                <p><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> 
                    Data Pembayaran Berhasil Di Tambahkan.</p>
            </div>
        </div>';
            $smarty->assign('status_form', $status_form);
        }
    } else {
        $status_form = '
        <div class="alert_biaya_kuliah ui-widget" style="margin-left:15px;">
                <div class="ui-state-error ui-corner-all" style="padding: 5px;width:30%;"> 
                    <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                        Gagal. Bank, Bank Via dan Tanggal Bayar TIdak Boleh Kosong.</p>
                </div>
            </div>';
        $smarty->assign('status_form', $status_form);
    }
}

$smarty->display('utility/edit-biaya-wisuda.tpl');
?>
