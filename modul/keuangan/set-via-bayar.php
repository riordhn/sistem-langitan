<?php

include 'config.php';

$id_tagihan_mhs=get('id_tagihan_mhs');
$db->Query("
    select tm.*,m.nim_mhs
    from tagihan_mhs tm
    join mahasiswa m on m.id_mhs=tm.id_mhs 
    where tm.id_tagihan_mhs='{$id_tagihan_mhs}'
    ");
$tagihan_mhs = $db->fetchAssoc();
$kode_va=getenv('BNI_PREFIX_VA').getenv('BNI_CLIENT_ID').str_pad($tagihan_mhs['ID_MHS'],8,'0',STR_PAD_LEFT);
$smarty->assign('cari', get('cari'));
$smarty->assign('ditangguhkan', get('ditangguhkan'));
$smarty->assign('kode_va',$kode_va);
$smarty->assign('id_tagihan_mhs', get('id_tagihan_mhs'));
$smarty->assign('tagihan_mhs', $tagihan_mhs);
$smarty->display('history/set-via-bayar.tpl');
?>
