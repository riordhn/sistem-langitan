<?php

include 'config.php';
include 'class/master.class.php';
include 'class/list_data.class.php';

$master = new master($db);
$list = new list_data($db);

if (isset($_GET['mode'])) {
    if (get('mode') == 'add_group') {
        $smarty->display("master/add-group-kerjasama.tpl");
    } elseif (get('mode') == 'edit_group') {
        $smarty->assign('data_group_kerjasama', $master->get_group_kerjasama(get('id')));
        $smarty->display("master/edit-group-kerjasama.tpl");
    } elseif (get('mode') == 'delete_group') {
        $smarty->assign('data_group_kerjasama', $master->get_group_kerjasama(get('id')));
        $smarty->display("master/delete-group-kerjasama.tpl");
    } elseif (get('mode') == 'add_kerjasama') {
        $smarty->assign("data_group_kerjasama", $master->load_group_kerjasama());
        $smarty->display("master/add-kerjasama.tpl");
    } elseif (get('mode') == 'edit_kerjasama') {
        $smarty->assign("data_group_kerjasama", $master->load_group_kerjasama());
        $smarty->assign("data_kerjasama", $master->get_kerjasama(get('id')));
        $smarty->display("master/edit-kerjasama.tpl");
    } elseif (get('mode') == 'delete_kerjasama') {
        $smarty->assign("data_group_kerjasama", $master->load_group_kerjasama());
        $smarty->assign("data_kerjasama", $master->get_kerjasama(get('id')));
        $smarty->display("master/delete-kerjasama.tpl");
    }
} else {
    if (isset($_POST)) {
        if (post('mode') == 'add_group') {
            $master->add_group_kerjasama(post('nama'));
        } else if (post('mode') == 'edit_group') {
            $master->edit_group_kerjasama(post('id'), post('nama'));
        } else if (post('mode') == 'delete_group') {
            $master->delete_group_kerjasama(post('id'), post('nama'));
        } else if (post('mode') == 'add_kerjasama') {
            $master->add_kerjasama(post('nama'), strtoupper(post('penyelenggara')), post('jenis'), post('periode'), post('besar'), post('group'), post('keterangan'));
        } else if (post('mode') == 'edit_kerjasama') {
            $master->edit_kerjasama(post('id'), post('nama'), strtoupper(post('penyelenggara')), post('jenis'), post('periode'), post('besar'), post('group'), post('keterangan'));
        } else if (post('mode') == 'delete_kerjasama') {
            $master->delete_kerjasama(post('id'), post('nama'), strtoupper(post('penyelenggara')), post('jenis'), post('periode'), post('besar'), post('group'), post('keterangan'));
        }
    }
    $smarty->assign("data_group_kerjasama", $master->load_group_kerjasama());
    $smarty->assign('data_kerjasama', $master->load_kerjasama());
    $smarty->display("master/kerja-sama.tpl");
}
?>