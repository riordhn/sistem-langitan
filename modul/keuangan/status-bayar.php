<?php

include 'config.php';
include 'class/utility.class.php';
include 'class/master.class.php';
include 'class/history.class.php';

$utility = new utility($db);
$master = new master($db);
$history = new history($db);

$id_pt = $id_pt_user;

if (isset($_GET['cari'])) {
    if ($_POST['mode'] == 'tambah') {
        $utility->insert_sejarah_status_pembayaran(post('id_mhs'), post('semester'), post('status'), post('tgl_mulai'), post('tgl_tempo'), post('keterangan'), $user->ID_PENGGUNA, 1);
    } else if ($_POST['mode'] == 'aktifkan') {
        $utility->aktifkan_sejarah_status_pembayaran(post('id_sejarah_status'));
    }else if ($_POST['mode'] == 'hapus') {
        $utility->hapus_sejarah_status_pembayaran(post('id_sejarah_status'));
    }
    $nim = str_replace("'", "''", trim($_GET['cari']));
    $smarty->assign('data_sejarah_status', $utility->load_sejarah_status_pembayaran($nim, $id_pt));
    $smarty->assign('data_status', $master->load_status_pembayaran());
    $smarty->assign('data_mhs', $history->get_data_mhs($nim, $id_pt));
    $smarty->assign('data_semester', $utility->get_semester_all());
}


$smarty->display('utility/status-bayar.tpl');
?>
