<?php
include 'config.php';
include 'class/laporan.class.php';
include 'class/list_data.class.php';

$list = new list_data($db);
$laporan = new laporan($db);

$data_excel = $laporan->load_data_sp3_lebih(get('fakultas'), get('prodi'), get('semester'));
ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #2b1212;
        text-transform:capitalize;
    }
    td{
        text-align: left;
    }
</style>
<table border='1' align='left' cellpadding='0' cellspacing='0' class='keuangan'>
    <thead>
        <tr>
            <td colspan="12" class="header_text">
                <?php
                echo 'LAPORAN SP3 LEBIH';
                ?>
            </td>
        </tr>
        <tr>
            <td class="header_text">NO</td>
            <td class="header_text">NO UJIAN</td>
            <td class="header_text">NAMA</td>
            <td class="header_text">PRODI</td>
            <td class="header_text">JALUR</td>
            <td class="header_text">KELOMPOK BIAYA</td>
            <td class="header_text">SEMESTER</td>
            <td class="header_text">BESAR SP3</td>
            <td class="header_text">KELEBIHAN SP3</td>
            <td class="header_text">STATUS PEMBAYARAN</td>
            <td class="header_text">TOTAL PEMBAYARAN</td>
            <td class="header_text">TOTAL BIAYA</td>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach ($data_excel as $data) {
            $sp3_lebih = $data['SP3_BAYAR'] - $data['BESAR_BIAYA'];
            if ($sp3_lebih == 0) {
                $sp3_lebih = $data['SP3_SUKARELA'];
            }
            echo "<tr>
				<td>{$no}</td>
				<td>{$data['NO_UJIAN']}</td>
				<td>{$data['NM_C_MHS']}</td>
				<td>{$data['NM_JENJANG']} {$data['NM_PROGRAM_STUDI']}</td>
                                <td>{$data['NM_JALUR']}</td>
				<td>{$data['NM_KELOMPOK_BIAYA']}</td>
                                <td>{$data['NM_SEMESTER']} {$data['TAHUN_AJARAN']}</td>
				<td>
                                    SP3 Usulan : {$data['SP3_1']}<br/>
                                    SP3 Master : {$data['BESAR_BIAYA']}<br/>
                                    SP3 Pembayaran : {$data['SP3_BAYAR']}
                                </td>
                                <td>{$sp3_lebih}</td>
                                <td>
                                ";
            if (isset($data['STATUS_BAYAR'])) {
                $no_p = 1;
                foreach ($data['STATUS_BAYAR'] as $pembayaran) {
                    echo "Pembayaran Ke- {$no_p}
                    Tgl Bayar : {$pembayaran['TGL_BAYAR']} <br/>
                    Bank : {$pembayaran['NM_BANK']} <br/>
                    Total Bayar : {$pembayaran['TOTAL_BAYAR']}<br/><br/>";
                    $no_p++;
                }
            } else {
                echo 'Belum Bayar';
            }
            echo "  </td>
                    <td>{$data['TOTAL_PEMBAYARAN']}</td>
                    <td>{$data['TOTAL_BIAYA']}</td>
                    </tr>";
            $no++;
        }
        ?>
    </tbody>
</table>

<?php
echo '<b>Data Ini Di Ambil Pada Tanggal ' . date('d F Y  H:i:s') . '</b>';

$nm_file = "sp3-lebih" . date('d-m-Y');
header("Content-disposition: attachment; filename=$nm_file.xls");
header("Content-type: Application/exe");
header("Content-Transfer-Encoding: binary");
ob_flush();
?>
