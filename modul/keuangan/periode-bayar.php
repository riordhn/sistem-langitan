<?php

include 'config.php';
include 'class/master.class.php';
$master = new master($db);

$smarty->assign('data_periode_mhs', $master->load_periode_bayar_mhs());
$smarty->assign('data_periode_cmhs', $master->load_periode_bayar_cmhs());
$smarty->display("master/periode-bayar.tpl");
?>
