<?php

include 'config.php';
include 'class/list_data.class.php';
include 'class/piutang.class.php';
include 'class/utility.class.php';

$id_pt = $id_pt_user;
$id_user = $user->ID_PENGGUNA;

$list = new list_data($db);
$piutang = new piutang($db);
$utility = new utility($db);
$id_fakultas = get('fakultas');
$id_prodi = get('prodi');
$id_semester = get('semester');
$id_status = get('status');
if (isset($_GET)) {
    $semester = $piutang->get_semester($id_semester);
    if (get('mode') == 'tampil') {
        $form_params =  md5(str_replace('tampil', '', $_SERVER['QUERY_STRING']));
        // CEK DATA WH
        $check_wh = $db->QueryToArray("
            SELECT * FROM WH_KEU_TAGIHAN_MHS_REKAP 
            WHERE FORM_PARAMS='{$form_params}'
            ");
        // JIKA KOSONG RELOAD DATA WAREHOUSE
        if (empty($check_wh)) {
            $data_rekap = $piutang->load_rekap_tagihan_mahasiswa($id_fakultas, $id_status, $id_semester);
            $piutang->insert_wh_rekap($data_rekap, $semester, $form_params);
            $smarty->assign('load_wh', false);
            $smarty->assign('data_rekap', $data_rekap);
        } else {
            // CEK EXPIRED DATA WH TAGIHAN (DEFAULT 1 JAM)
            $db->Query("
            select extract( hour from diff ) hour
            from (select systimestamp - to_timestamp( '{$check_wh[0]['UPDATED_AT']}') diff
            from dual)
            ");
            $wh_diff = $db->fetchAssoc();
            // JIKA SUDAH LEBIH DARI 1 JAM
            if ($wh_diff['HOUR'] >= 1) {
                // RELOAD ULANG DATA WH
                $data_rekap = $piutang->load_rekap_tagihan_mahasiswa($id_fakultas, $id_status, $id_semester);
                $piutang->insert_wh_rekap($data_rekap, $semester, $form_params);
                $smarty->assign('load_wh', false);
                $smarty->assign('data_rekap', $data_rekap);
            } else {
                // AMBIL DATA WH TAGIHAN REKAP
                $smarty->assign('load_wh', true);
                $smarty->assign('data_rekap', $piutang->load_rekap_tagihan_mahasiswa_wh($form_params));
            }
        }

        $smarty->assign('form_params',  $form_params);
    } else if (get('mode') == 'detail') {
        $form_params =  get('params');
        if ($id_fakultas != '' || $id_prodi != '') {
            $smarty->assign('data_jalur_one', $list->get_jalur(get('jalur')));
            $smarty->assign('data_prodi', $list->load_list_prodi($id_pt, $id_fakultas));
            $smarty->assign('data_fakultas_one', $list->get_fakultas($id_fakultas));
            $smarty->assign('data_prodi_one', $list->get_prodi($id_prodi));
        }
        $smarty->assign('count_data_biaya', count($list->load_biaya()));
        $smarty->assign('data_biaya', $list->load_biaya());
        $smarty->assign('form_params',  $form_params);
        // AMBIL DATA WH TAGIHAN DETAIL
        $smarty->assign('data_rekap_detail', $piutang->load_detail_tagihan_mhs_wh($id_fakultas, $id_prodi, $id_semester, $form_params));
    }
}
$smarty->assign('page_info', alert_info("Informasi : Data laporan akan <b>direfresh</b> setiap 1 Jam Sekali"));
$smarty->assign('mode', get('mode'));
$smarty->assign('fakultas', $id_fakultas);
$smarty->assign('prodi', $id_prodi);
$smarty->assign('semester', $id_semester);
$smarty->assign('status', $_GET['status']);
$smarty->assign('data_prodi', $list->load_list_prodi($id_pt, $id_fakultas));
$smarty->assign('data_fakultas', $list->load_list_fakultas($id_pt));
$smarty->assign('data_status', $piutang->load_list_status());
$smarty->assign('data_semester', $list->load_list_semester());
$smarty->display('laporan/tagihan-mhs.tpl');
