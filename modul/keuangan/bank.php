<?php

include 'config.php';
include 'class/master.class.php';

$bank = new master($db);

$mode = get('mode', 'view');

if ($request_method == 'POST') {
    if (post('mode') == 'add') {
        $bank->add_bank(post('nama'));
    } else if (post('mode') == 'edit') {
        $bank->edit_bank(post('nama'), post('id_bank'));
    }
}
if ($mode == 'detail' || $mode == 'edit' || $mode == 'upload' || $mode == 'delete') {
    $smarty->assign('data_bank', $bank->get_bank(get('id_bank')));
} else {
    $smarty->assign('data_bank', $bank->load_bank());
}


$smarty->display("master/{$mode}-bank.tpl");
?>