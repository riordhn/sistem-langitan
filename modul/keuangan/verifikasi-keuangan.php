<?php

include 'config.php';
include '../registrasi/class/CalonMahasiswa.class.php';
include '../regmaba/class/CalonMahasiswaNambi.class.php';
include 'class/pendaftaran.class.php';
include '../regmaba/class/Kota.class.php';
include 'funtion-tampil-informasi.php';

$Kota = new Kota($db);
$CalonMahasiswa = new CalonMahasiswa($db);
$CalonMahasiswaNambi = new CalonMahasiswaNambi($db);
$daftar = new pendaftaran($db);
if (isset($_GET)) {
    if (get('no_ujian') != '') {
        $id_c_mhs = $daftar->get_id_cmhs(get('no_ujian'));
        if ($id_c_mhs != '') {
            if (isset($_POST)) {
                if (post('mode') == 'verifikasi') {
                    if (post('biaya_kuliah') != '') {
                        $id_penerimaan = $db->QuerySingle("SELECT ID_PENERIMAAN FROM CALON_MAHASISWA_BARU WHERE ID_C_MHS='{$id_c_mhs}'");
                        $daftar->update_no_invoice_cmhs($id_c_mhs, $daftar->generate_no_invoice_cmhs($id_penerimaan));
                        $daftar->update_biaya_kuliah_cmhs($id_c_mhs, post('biaya_kuliah'));
                        $daftar->update_verifikator_keuangan($id_c_mhs, $user->ID_PENGGUNA);
                        $status_generate = $daftar->generate_biaya_cmhs($id_c_mhs, post('set_ukt_manual'), post('biaya_kuliah'));
                        $smarty->assign('status_generate', alert_success($status_generate));
                    }
                } else if (post('mode') == 'update-data') {
                    $post = $_POST;
                    $range_penghasilan = $CalonMahasiswaNambi->GetRangebyData($post['total_pendapatan_ortu'], 1);
                    // Query Update dari Nambi

                    $query_update_cmo = "
                    update calon_mahasiswa_ortu set
                        pekerjaan_ayah		= '{$post['pekerjaan_ayah']}',
                        penghasilan_ayah	= '{$post['penghasilan_ayah']}', 
                        pekerjaan_ibu		= '{$post['pekerjaan_ibu']}',
                        penghasilan_ibu         = '{$post['penghasilan_ibu']}',
                
                        kediaman_ortu       = '{$post['kediaman_ortu']}',
                        luas_tanah          = '{$post['luas_tanah']}',
                        luas_bangunan       = '{$post['luas_bangunan']}',
                        listrik             = '{$post['listrik']}',
                        kendaraan_r2        = '{$post['kendaraan_r2']}',
                        tahun_kendaraan_r2  = '{$post['tahun_kendaraan_r2']}',
                        merek_kendaraan_r2  = '{$post['merek_kendaraan_r2']}',
                        kendaraan_r4        = '{$post['kendaraan_r4']}',
                        tahun_kendaraan_r4  = '{$post['tahun_kendaraan_r4']}',
                        merek_kendaraan_r4  = '{$post['merek_kendaraan_r4']}',
                        kekayaan_lain       = '{$post['kekayaan_lain']}',
                        info_lain           = '{$post['info_lain']}',

                        gaji_ayah                   = '{$post['gaji_ayah']}',
                        tunjangan_keluarga_ayah     = '{$post['tunjangan_keluarga_ayah']}',
                        tunjangan_jabatan_ayah      = '{$post['tunjangan_jabatan_ayah']}',
                        tunjangan_sertifikasi_ayah  = '{$post['tunjangan_sertifikasi_ayah']}',
                        tunjangan_kehormatan_ayah   = '{$post['tunjangan_kehormatan_ayah']}',
                        renumerasi_ayah             = '{$post['renumerasi_ayah']}',
                        tunjangan_lain_ayah         = '{$post['tunjangan_lain_ayah']}',
                        penghasilan_lain_ayah       = '{$post['penghasilan_lain_ayah']}',

                        gaji_ibu                   = '{$post['gaji_ibu']}',
                        tunjangan_keluarga_ibu     = '{$post['tunjangan_keluarga_ibu']}',
                        tunjangan_jabatan_ibu      = '{$post['tunjangan_jabatan_ibu']}',
                        tunjangan_sertifikasi_ibu  = '{$post['tunjangan_sertifikasi_ibu']}',
                        tunjangan_kehormatan_ibu   = '{$post['tunjangan_kehormatan_ibu']}',
                        renumerasi_ibu             = '{$post['renumerasi_ibu']}',
                        tunjangan_lain_ibu         = '{$post['tunjangan_lain_ibu']}',
                        penghasilan_lain_ibu       = '{$post['penghasilan_lain_ibu']}',

                        total_pendapatan_ortu       = '{$post['total_pendapatan_ortu']}',

                        range_air           = '{$post['range_air']}',
                        range_pbb           = '{$post['range_pbb']}',
                        range_listrik       = '{$post['range_listrik']}',
                        range_njop          = '{$post['range_njop']}',
                        range_penghasilan   = '{$range_penghasilan['ID_RANGE_JUMLAH']}',
                        jumlah_handphone    = '{$post['jumlah_handphone']}',
                        transportasi        = '{$post['transportasi']}'
                    where id_c_mhs = '{$id_c_mhs}'
                    ";
                    $db->Query($query_update_cmo);

                    // Proses Pengelompokan Kelompok Biaya
                    $kelompok_biaya = $CalonMahasiswaNambi->AutoVerifikasiKelompokBiaya($id_c_mhs);
                    $db->Query("UPDATE CALON_MAHASISWA_BARU SET ID_KELOMPOK_BIAYA='{$kelompok_biaya}' WHERE ID_C_MHS='{$id_c_mhs}'");
                }
            }
            $data_cmb = $CalonMahasiswaNambi->GetData($id_c_mhs, true);
            $biaya_kuliah_cmhs = $daftar->get_biaya_kuliah_cmhs($id_c_mhs);
            $ukt_cmhs = $daftar->get_ukt_cmhs($id_c_mhs, $biaya_kuliah_cmhs);


            // Jumlah pilihan
            if ($data_cmb['KODE_JURUSAN'] == '01') {
                $jumlah_pilihan = 2;
                $jurusan_sekolah = 1;
            } //ipa
            if ($data_cmb['KODE_JURUSAN'] == '02') {
                $jumlah_pilihan = 2;
                $jurusan_sekolah = 2;
            } //ips
            if ($data_cmb['KODE_JURUSAN'] == '03') {
                $jumlah_pilihan = 4;
                $jurusan_sekolah = 3;
            } //ipc
            // Ambil Data Skor
            $db->Query("SELECT * FROM CALON_MAHASISWA_ORTU WHERE ID_C_MHS='{$id_c_mhs}'");
            $cmo = $db->FetchAssoc();
            $s_penghasilan = $CalonMahasiswaNambi->GetRangeSkor($cmo['RANGE_PENGHASILAN']);
            $s_pekerjaan_ayah = $CalonMahasiswaNambi->GetSkorPekerjaan($cmo['PEKERJAAN_AYAH']);
            $s_pekerjaan_ibu = $CalonMahasiswaNambi->GetSkorPekerjaan($cmo['PEKERJAAN_IBU']);
            if ($s_pekerjaan_ayah > $s_pekerjaan_ibu) {
                $s_pekerjaan = $s_pekerjaan_ayah;
            } else {
                $s_pekerjaan = $s_pekerjaan_ibu;
            }
            $s_njop = $CalonMahasiswaNambi->GetRangeSkor($cmo['RANGE_NJOP']);
            $s_listrik = $CalonMahasiswaNambi->GetRangeSkor($cmo['RANGE_LISTRIK']);
            $s_air = $CalonMahasiswaNambi->GetRangeSkor($cmo['RANGE_AIR']);
            $s_pbb = $CalonMahasiswaNambi->GetRangeSkor($cmo['RANGE_PBB']);
            $s_asset = $CalonMahasiswaNambi->GetSkorKendaraan($cmo['KENDARAAN_R2'] + $cmo['KENDARAAN_R4']);

            // Ambil data penghasilan Pertama
            $db->Query("
                    SELECT PO1.PENGHASILAN PENG_AYAH,PO2.PENGHASILAN PENG_IBU
                    FROM CALON_MAHASISWA_ORTU CMO
                    LEFT JOIN PENGHASILAN_ORTU PO1 ON PO1.ID_PENGHASILAN_ORTU=CMO.PENGHASILAN_SNMPTN_AYAH
                    LEFT JOIN PENGHASILAN_ORTU PO2 ON PO2.ID_PENGHASILAN_ORTU=CMO.PENGHASILAN_SNMPTN_IBU
                    WHERE CMO.ID_C_MHS='{$id_c_mhs}'");
            $data_penghasilan_pertama = $db->FetchAssoc();


            $status_bayar = $daftar->cek_status_bayar($id_c_mhs);
            $smarty->assign('peng_pertama', $data_penghasilan_pertama);
            $smarty->assign('status_bayar', $status_bayar != 0 ? alert_error("Sudah melakukan pembayaran") : "");
            $smarty->assign('kota_set', $CalonMahasiswa->GetListKota());
            $smarty->assign('negara_set', $Kota->GetListNegara());
            $smarty->assign('negara_indo_set', $Kota->GetListNegaraIndonesia());
            $smarty->assign('negara_asing_set', $Kota->GetListNegaraAsing());
            $smarty->assign('jenis_kelamin_set', $CalonMahasiswa->GetListJenisKelamin());
            $smarty->assign('kewarganegaraan_set', $CalonMahasiswa->GetListKewarganegaraan());
            $smarty->assign('agama_set', $CalonMahasiswa->GetListAgama());
            $smarty->assign('sumber_biaya_set', $CalonMahasiswa->GetListSumberBiaya());
            $smarty->assign('jurusan_sekolah_set', $CalonMahasiswa->GetListJurusanSekolah());
            $smarty->assign('pendidikan_ortu_set', $CalonMahasiswa->GetListPendidikanOrtu());
            $smarty->assign('pekerjaan_set', $CalonMahasiswa->GetListPekerjaan());
            $smarty->assign('penghasilan_ortu_set', $CalonMahasiswa->GetListPenghasilanOrtu());
            $smarty->assign('skala_pekerjaan_set', $CalonMahasiswa->GetListSkalaPekerjaan());
            $smarty->assign('kediaman_ortu_set', $CalonMahasiswa->GetListKediamanOrtu());
            $smarty->assign('luas_tanah_set', $CalonMahasiswa->GetListLuasTanah());
            $smarty->assign('luas_bangunan_set', $CalonMahasiswa->GetListLuasBangunan());
            $smarty->assign('njop_set', $CalonMahasiswa->GetListNJOP());
            $smarty->assign('listrik_set', $CalonMahasiswa->GetListListrik());
            $smarty->assign('kendaraan_r4_set', $CalonMahasiswa->GetListKendaraanR4());
            $smarty->assign('kendaraan_r2_set', $CalonMahasiswa->GetListKendaraanR2());
            $smarty->assign('status_ptn_set', $CalonMahasiswa->GetListStatusPTN());
            $smarty->assign('program_studi_set', $CalonMahasiswa->GetListProgramStudi($id_c_mhs));

            // Tambahan Nambi
            $smarty->assign('s_penghasilan', $s_penghasilan);
            $smarty->assign('s_pekerjaan', $s_pekerjaan);
            $smarty->assign('s_njop', $s_njop);
            $smarty->assign('s_air', $s_air);
            $smarty->assign('s_pbb', $s_pbb);
            $smarty->assign('s_listrik', $s_listrik);
            $smarty->assign('s_asset', $s_asset);
            $smarty->assign('data_pekerjaan', $CalonMahasiswaNambi->LoadPekerjaan());
            $smarty->assign('data_range_listrik', $CalonMahasiswaNambi->LoadRange(5));
            $smarty->assign('data_range_penghasilan', $CalonMahasiswaNambi->LoadRange(1));
            $smarty->assign('data_range_pbb', $CalonMahasiswaNambi->LoadRange(7));
            $smarty->assign('data_range_njop', $CalonMahasiswaNambi->LoadRange(3));
            $smarty->assign('data_range_air', $CalonMahasiswaNambi->LoadRange(6));
            $smarty->assign('biaya_kuliah_recom_cmhs', $daftar->get_biaya_kuliah_recom_cmhs(get('no_ujian')));
            $smarty->assign('biaya_kuliah_cmhs', $biaya_kuliah_cmhs);
            $smarty->assign('ukt_cmhs', $ukt_cmhs);
            $smarty->assign('sp3_master_biaya_cmhs', $daftar->get_sp3_master_biaya_cmhs($biaya_kuliah_cmhs));
            $smarty->assign('kelompok_biaya_pilihan', $daftar->load_pilihan_kelompok_biaya_cmhs(get('no_ujian')));
            $smarty->assign('no_invoice_cmhs', $daftar->get_no_invoice_cmhs($id_c_mhs));
            $smarty->assign('jumlah_pilihan', $jumlah_pilihan);
            $smarty->assign('jurusan_sekolah', $jurusan_sekolah);
            $tgl_regmaba = $db->QuerySingle("SELECT TGL_REGMABA FROM AUCC.CALON_MAHASISWA_BARU WHERE ID_C_MHS='{$id_c_mhs}'");
            $smarty->assign('status_regmaba', $tgl_regmaba == '' ? alert_error("Mahasiswa Belum melakukan regmaba") : '');
            $smarty->assign('cmb', $data_cmb);
        } else {
            $smarty->assign('error', alert_error("Data calon mahasiswa tidak ditemukan..."));
        }
    }
}
if (get('mode') == 'edit') {
    $smarty->display('pendaftaran/edit-data-regmaba.tpl');
} else {
    $smarty->display('pendaftaran/verifikasi-keuangan.tpl');
}
?>
