<?php

include 'config.php';
//include 'config-mysql.php';
include 'class/history.class.php';
include 'class/utility.class.php';
include 'class/master.class.php';
include 'class/rekon.class.php';

$history = new history($db);
$utility = new utility($db);
$master = new master($db);
$rekon = new rekon($db, $user->ID_PENGGUNA);

$id_pt = $id_pt_user;
$id_user = $user->ID_PENGGUNA;
$array_bln = array(1 => "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII");

if (isset($_GET['cari'])) {
    $cari = trim($_GET['cari']);
    $cek_mhs = $db->QuerySingle("
    SELECT COUNT(*) FROM MAHASISWA M 
    JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA 
    WHERE P.ID_PERGURUAN_TINGGI = {$id_pt} AND M.NIM_MHS='{$cari}'
    ");
    $cek_cmhs = $db->QuerySingle("
    SELECT COUNT(*) FROM CALON_MAHASISWA_BARU CMB
    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CMB.ID_PROGRAM_STUDI
    JOIN FAKULTAS F ON PS.ID_FAKULTAS = F.ID_FAKULTAS
    WHERE F.ID_PERGURUAN_TINGGI = {$id_pt} AND CMB.NO_UJIAN='{$cari}'
    ");
    if ($cek_mhs > 0) {
        if (post('mode') == 'proses_bayar') {
            $id_tagihan_mhs = post('id_tagihan_mhs');
            $tgl_hari_ini = date('Y-m-d');
            $jumlah_data = post('jumlah_data');
            $jumlah_pembayaran_perhari = $db->QuerySingle("SELECT COUNT(DISTINCT(NO_TRANSAKSI)) FROM PEMBAYARAN WHERE TO_CHAR(TGL_BAYAR,'YYYY-MM-DD')='{$tgl_hari_ini}'");
            $no_transaksi = date('YmdHi') . rand(10000, 99999);
            $no_kuitansi = date('Y') . '/' . $history->getRomawi(date('n')) . '/' . date('d') . '/' . str_pad($jumlah_pembayaran_perhari + 1, 5, "0", STR_PAD_LEFT) . '/' . rand(10, 99);
            $created_at = $db->QuerySingle("SELECT TO_CHAR(CURRENT_TIMESTAMP,'DD-MON-YYYY HH24.MI.SS') FROM DUAL");
            $tgl_bayar = post('tgl_bayar');
            $periode_bayar = post('periode_bayar');
            $id_bank = post('bank');
            $id_bank_via = post('bank_via');
            $total_cicilan = post('total_cicilan') == '' ? "1" : post('total_cicilan');
            $status_pembayaran = post('status_pembayaran');
            $keterangan = post('keterangan');

            if (!empty($tgl_bayar)) {
                for ($i = 1; $i <= $jumlah_data; $i++) {
                    $id_tagihan = post('tagihan' . $i);
                    $besar_biaya = post('biaya_dibayar' . $i);
                    $besar_biaya_default = post('besar_biaya_default' . $i);
                    $db->Query("
                    INSERT INTO PEMBAYARAN (ID_TAGIHAN, BESAR_PEMBAYARAN, TGL_BAYAR, ID_BANK, ID_BANK_VIA,NO_TRANSAKSI,KETERANGAN,PERIODE_BAYAR,NO_KUITANSI,CREATED_AT,ID_PENGGUNA,ID_STATUS_PEMBAYARAN)
                    VALUES 
                    ('{$id_tagihan}','{$besar_biaya}','{$tgl_bayar}','{$id_bank}','{$id_bank_via}','{$no_transaksi}','{$keterangan}','{$periode_bayar}','{$no_kuitansi}',CURRENT_TIMESTAMP,'{$user->ID_PENGGUNA}','{$status_pembayaran}')
                    ");
                    $last_inserted_id_pembayaran = $db->QuerySingle("SELECT ID_PEMBAYARAN FROM PEMBAYARAN WHERE ID_TAGIHAN='{$id_tagihan}' AND BESAR_PEMBAYARAN='{$besar_biaya}' AND NO_TRANSAKSI='{$no_transaksi}'");
                    // UPDATE BIAYA PERBULAN
                    $data_semester_bulan = $history->load_riwayat_pembayaran_biaya($id_tagihan_mhs);
                    $index_bulan = 1;
                    foreach ($data_semester_bulan as $ds) {
                        $kd_bulan_pembayaran = post('biaya_dibayar_kd_bulan_' . $i . '_' . $index_bulan);
                        $nm_bulan_pembayaran = post('biaya_dibayar_nm_bulan_' . $i . '_' . $index_bulan);
                        $besar_bulan_pembayaran = post('biaya_dibayar_bulan_' . $i . '_' . $index_bulan);
                        if ($besar_bulan_pembayaran > 0 && $status_pembayaran == '1') {
                            $db->Query("
                            INSERT INTO PEMBAYARAN_PERBULAN (ID_TAGIHAN, ID_PEMBAYARAN, KODE_BULAN, NM_BULAN, BESAR_PEMBAYARAN,CREATED_ON)
                            VALUES 
                            ('{$id_tagihan}','{$last_inserted_id_pembayaran}','{$kd_bulan_pembayaran}','{$nm_bulan_pembayaran}','{$besar_bulan_pembayaran}',CURRENT_TIMESTAMP)
                            ");
                        }
                        $index_bulan++;
                    }

                    $query = $db->Query("
                    SELECT TAG.ID_TAGIHAN,TAG.BESAR_BIAYA,
                    SUM(CASE WHEN PEM.ID_STATUS_PEMBAYARAN=1 THEN PEM.BESAR_PEMBAYARAN ELSE 0 END) BESAR_PEMBAYARAN,
                    SUM(CASE WHEN PEM.ID_STATUS_PEMBAYARAN=3 THEN PEM.BESAR_PEMBAYARAN ELSE 0 END) BESAR_PENANGGUHAN,
                    SUM(CASE WHEN PEM.ID_STATUS_PEMBAYARAN=4 THEN PEM.BESAR_PEMBAYARAN ELSE 0 END) BESAR_PEMBEBASAN
                    FROM PEMBAYARAN PEM
                    JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN=PEM.ID_TAGIHAN
                    WHERE TAG.ID_TAGIHAN='{$id_tagihan}'
                    GROUP BY TAG.ID_TAGIHAN,TAG.BESAR_BIAYA
                    ");
                    $cek_tagihan = $db->FetchAssoc();
                    if ($cek_tagihan['BESAR_BIAYA'] == $cek_tagihan['BESAR_PEMBAYARAN'] && $cek_tagihan['BESAR_BIAYA'] > 0) {
                        $is_lunas = '1';
                    } else {
                        $is_lunas = '0';
                    }

                    $db->Query("
                    UPDATE TAGIHAN SET
                        IS_LUNAS='{$is_lunas}',
                        JUMLAH_CICILAN='{$total_cicilan}'
                    WHERE ID_TAGIHAN='{$id_tagihan}'
                    ");
                }
            } else {
                $smarty->assign('error', alert_error("Tanggal bayar tidak boleh kosong", 98));
            }
        } else if (post('mode') == 'ubah_master') {
            $id_tagihan_mhs = post('id_tagihan_mhs');
            $total_tagihan = post('total_tagihan');
            for ($i = 1; $i <= post('jumlah_data'); $i++) {
                $id_tagihan = post('id_tagihan' . $i);
                $besar_biaya = post('besar_biaya' . $i);
                $db->Query("UPDATE TAGIHAN SET BESAR_BIAYA='{$besar_biaya}' WHERE ID_TAGIHAN='{$id_tagihan}'");
                $besar_tagihan = $db->QuerySingle("
                SELECT SUM(TAG.BESAR_BIAYA) BESAR_BIAYA
                FROM TAGIHAN TAG
                WHERE TAG.ID_TAGIHAN_MHS='{$id_tagihan_mhs}'
                GROUP BY TAG.ID_TAGIHAN_MHS
                ");
            }
            $db->Query("UPDATE TAGIHAN_MHS SET TOTAL_BESAR_BIAYA='{$total_tagihan}' WHERE ID_TAGIHAN_MHS='{$id_tagihan_mhs}'");
        } else if (post('mode') == 'tambah') {
            for ($i = 1; $i < post('jumlah_data'); $i++) {
                $semester = post('semester' . $i);
                $tagih = "0";
                $id_mhs = post('id_mhs');
                if ($semester != '') {
                    $id_jenis_tagihan_semester = $utility->convert_array_to_string_query_in($utility->id_jsem);
                    // AMBIL LAST INSERT ID TAGIHAN MHS                                    
                    $id_tagihan_mhs =  $db->QuerySingle(
                        "SELECT TAGIHAN_MHS_SEQ.NEXTVAL ID_TAGIHAN_MHS FROM DUAL"
                    );

                    $db->BeginTransaction();

                    // INSERT TAGIHAN MHS
                    $result1 = $db->Query(
                        "
                        INSERT INTO TAGIHAN_MHS (ID_TAGIHAN_MHS, ID_MHS,ID_SEMESTER,TOTAL_BESAR_BIAYA,TOTAL_DENDA_BIAYA,TOTAL_TERBAYAR,CREATED_ON,CREATED_BY)
                        SELECT {$id_tagihan_mhs}, FOO.*,0 TOTAL_DENDA_BIAYA,0 TOTAL_TERBAYAR, SYSDATE CREATED_ON,{$id_user} CREATED_BY 
                        FROM 
                        (
                            SELECT BKM.ID_MHS, {$semester} AS ID_SEMESTER, SUM(DB.BESAR_BIAYA) TOTAL_BESAR_BIAYA
                            FROM BIAYA_KULIAH_MHS BKM 
                            JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH
                            JOIN MAHASISWA M ON M.ID_MHS = BKM.ID_MHS
                            WHERE DB.ID_JENIS_DETAIL_BIAYA IN ($id_jenis_tagihan_semester) AND M.ID_MHS='{$id_mhs}'
                            GROUP BY BKM.ID_MHS,{$semester}
                        ) FOO"
                    );

                    if (!$result1) {
                        $db->Rollback();
                    }

                    // Status Pembayaran = 2 Belum dibayar 
                    $result2 = $db->Query(
                        "INSERT INTO TAGIHAN (ID_TAGIHAN_MHS, ID_DETAIL_BIAYA, BESAR_BIAYA, IS_LUNAS)
                        SELECT {$id_tagihan_mhs} ID_TAGIHAN_MHS,DB.ID_DETAIL_BIAYA, DB.BESAR_BIAYA, '{$tagih}' AS IS_LUNAS
                        FROM BIAYA_KULIAH_MHS BKM
                        JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH
                        JOIN MAHASISWA M ON M.ID_MHS = BKM.ID_MHS
                        WHERE DB.ID_JENIS_DETAIL_BIAYA IN ($id_jenis_tagihan_semester) AND M.ID_MHS='{$id_mhs}'"
                    );

                    if (!$result2) {
                        $db->Rollback();
                    }

                    // Jika query berhasil semua, di commit
                    if ($result1 && $result2) {
                        $db->Commit();
                    }
                }
            }
        } else if (post('mode') == 'tambah_biaya') {
            $semester = post('semester');
            $besar_biaya = post('besar_biaya');
            $biaya = post('biaya');
            $id_mhs = post('id_mhs');
            $tagih = "0";
            $id_biaya_kuliah = $db->QuerySingle("SELECT ID_BIAYA_KULIAH FROM BIAYA_KULIAH_MHS WHERE ID_MHS='{$id_mhs}'");
            $biaya_cek = $db->QuerySingle("SELECT ID_DETAIL_BIAYA FROM DETAIL_BIAYA WHERE ID_BIAYA_KULIAH='{$id_biaya_kuliah}' AND ID_BIAYA='{$biaya}'");
            if (empty($biaya_cek)) {
                $db->Query("
                INSERT INTO DETAIL_BIAYA (ID_BIAYA_KULIAH,ID_BIAYA,BESAR_BIAYA,STATUS_BIAYA,ID_JENIS_DETAIL_BIAYA)
                VALUES
                ('{$id_biaya_kuliah}','{$biaya}','{$besar_biaya}','1','3')
                ");
            }
            $tagihan_mhs_cek = $db->QuerySingle(
                "SELECT ID_TAGIHAN_MHS FROM TAGIHAN_MHS WHERE ID_SEMESTER='{$semester}' AND ID_MHS='{$id_mhs}'"
            );
            if (empty($tagihan_mhs_cek)) {
                $id_tagihan_mhs = $db->QuerySingle(
                    "SELECT TAGIHAN_MHS_SEQ.NEXTVAL+1 ID_TAGIHAN_MHS FROM DUAL"
                );
                // INSERT TAGIHAN MHS
                $db->Query(
                    "
                INSERT INTO TAGIHAN_MHS (ID_MHS,ID_SEMESTER,TOTAL_BESAR_BIAYA,TOTAL_DENDA_BIAYA,TOTAL_TERBAYAR,CREATED_ON,CREATED_BY)
                SELECT FOO.*,0 TOTAL_DENDA_BIAYA,0 TOTAL_TERBAYAR, SYSDATE CREATED_ON,{$id_user} CREATED_BY 
                    FROM 
                    (
                        SELECT BKM.ID_MHS, {$semester} AS ID_SEMESTER, SUM(DB.BESAR_BIAYA) TOTAL_BESAR_BIAYA
                        FROM BIAYA_KULIAH_MHS BKM 
                        JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH
                        JOIN MAHASISWA M ON M.ID_MHS = BKM.ID_MHS
                        WHERE DB.ID_BIAYA='{$biaya}' AND M.ID_MHS='{$id_mhs}'
                        GROUP BY BKM.ID_MHS,{$semester}
                    ) FOO"
                );
            } else {
                $id_tagihan_mhs = $tagihan_mhs_cek;
            }

            $db->Query(
                "INSERT INTO TAGIHAN (ID_TAGIHAN_MHS, ID_DETAIL_BIAYA, BESAR_BIAYA, IS_LUNAS)
                SELECT {$id_tagihan_mhs} ID_TAGIHAN_MHS,DB.ID_DETAIL_BIAYA, $besar_biaya BESAR_BIAYA, '{$tagih}' AS IS_LUNAS
                FROM BIAYA_KULIAH_MHS BKM
                JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH
                JOIN MAHASISWA M ON M.ID_MHS = BKM.ID_MHS
                WHERE M.ID_MHS='{$id_mhs}' AND DB.ID_BIAYA='{$biaya}'"
            );
            $besar_tagihan = $db->QuerySingle("
            SELECT SUM(TAG.BESAR_BIAYA) BESAR_BIAYA
            FROM TAGIHAN TAG
            WHERE TAG.ID_TAGIHAN_MHS='{$id_tagihan_mhs}'
            GROUP BY TAG.ID_TAGIHAN_MHS
            ");
            $db->Query("UPDATE TAGIHAN_MHS SET TOTAL_BESAR_BIAYA='{$besar_tagihan}' WHERE ID_TAGIHAN_MHS='{$id_tagihan_mhs}'");
        } else if (post('mode') == 'hapus') {
            $id_tagihan_mhs = post('id_tagihan_mhs');
            $db->Query("
                DELETE FROM PENGEMBALIAN_PEMBAYARAN_DETAIL WHERE ID_PENGEMBALIAN_PEMBAYARAN IN (SELECT ID_PENGEMBALIAN_PEMBAYARAN FROM PENGEMBALIAN_PEMBAYARAN WHERE ID_TAGIHAN_MHS='{$id_tagihan_mhs}')
                ");
            $db->Query("
                DELETE FROM PENGEMBALIAN_PEMBAYARAN WHERE ID_TAGIHAN_MHS='{$id_tagihan_mhs}'
                ");
            $db->Query("
                DELETE FROM PEMBAYARAN_PERBULAN WHERE ID_TAGIHAN IN (SELECT ID_TAGIHAN FROM TAGIHAN WHERE ID_TAGIHAN_MHS='{$id_tagihan_mhs}')
                ");
            $db->Query("
            DELETE FROM PEMBAYARAN WHERE ID_TAGIHAN IN (SELECT ID_TAGIHAN FROM TAGIHAN WHERE ID_TAGIHAN_MHS='{$id_tagihan_mhs}')
            ");
            $db->Query("
            DELETE FROM TAGIHAN WHERE ID_TAGIHAN IN (SELECT ID_TAGIHAN FROM TAGIHAN WHERE ID_TAGIHAN_MHS='{$id_tagihan_mhs}')
            ");
            $db->Query("
            DELETE FROM TAGIHAN_MHS WHERE ID_TAGIHAN_MHS IN (SELECT ID_TAGIHAN_MHS FROM TAGIHAN_MHS WHERE ID_TAGIHAN_MHS='{$id_tagihan_mhs}')
            ");
        } else if (post('mode') == 'reset') {
            $id_tagihan_mhs = post('id_tagihan_mhs');
            $db->Query("
                UPDATE TAGIHAN SET IS_LUNAS='0' WHERE ID_TAGIHAN_MHS='{$id_tagihan_mhs}'
                ");
            $db->Query("
                DELETE FROM PEMBAYARAN_PERBULAN WHERE ID_TAGIHAN IN (SELECT ID_TAGIHAN FROM TAGIHAN WHERE ID_TAGIHAN_MHS='{$id_tagihan_mhs}')
                ");
            $db->Query("
                DELETE FROM PEMBAYARAN WHERE ID_TAGIHAN IN (SELECT ID_TAGIHAN FROM TAGIHAN WHERE ID_TAGIHAN_MHS='{$id_tagihan_mhs}')
                ");
        } else if (post('mode') == 'set_via_bayar') {
            $id_tagihan_mhs = post('id_tagihan_mhs');
            $is_va = post('is_va');
            // Ambil data tagihan
            // JIKA BUKA SET VA
            if ($is_va == '0') {
                $db->Query("SELECT * FROM TAGIHAN_MHS WHERE ID_TAGIHAN_MHS='{$id_tagihan_mhs}'");
                $tagihan = $db->fetchAssoc();
                $no_va = post('no_va');
                $awal_periode = post('awal_periode');
                $akhir_periode = post('akhir_periode');
                $db->Query("
                UPDATE TAGIHAN_MHS 
                    SET NO_VA=NULL,
                        TRX_ID=NULL,
                        AWAL_PERIODE=NULL,
                        AKHIR_PERIODE=NULL 
                    WHERE ID_TAGIHAN_MHS='{$id_tagihan_mhs}'
                ");
            }
        }

        $smarty->assign('tampil_pembayaran', array());
        $smarty->assign('jenis', 'mhs');
        $smarty->assign('data_vas', $history->load_data_va(get('cari')));
        $smarty->assign('data_pembayaran_sp', $history->load_history_bayar_sp(get('cari')));
        $smarty->assign('data_detail_biaya', $history->load_detail_biaya(get('cari')));
        $smarty->assign('data_detail_mahasiswa', $history->get_data_mhs(trim(get('cari')), $id_pt));
        $smarty->assign('data_pembayaran', $history->load_riwayat_pembayaran_mhs(trim(get('cari'))));
        $smarty->assign('data_pembayaran_cmhs', $history->load_history_bayar_cmhs(get('cari')));
        $smarty->assign('data_history_pengembalian', $utility->load_history_pengembalian_pembayaran(get('cari')));
        $smarty->assign('data_sejarah_kerjasama', $history->load_sejarah_kerjasama(get('cari'), $id_pt));
    } else if ($cek_cmhs > 0) {
        $smarty->assign('jenis', 'cmhs');
        $smarty->assign('data_detail_calon_mahasiswa', $history->get_data_cmhs(trim(get('cari'))));
        $smarty->assign('data_pembayaran', $history->load_history_bayar_cmhs(trim(get('cari'))));
    }
}
$smarty->assign('id_pt', $id_pt);
$smarty->assign('data_status', $master->load_status_pembayaran());
$smarty->assign('data_semester', $utility->get_semester_all());
$smarty->assign('data_bank', $utility->get_bank());
$smarty->assign('data_bank_via', $utility->get_via_bank());
$smarty->display('history/history-bayar.tpl');
