<?php

$id_pt = $id_pt_user;

class master
{
    // HARCORE UNTUK JENIS BIAYA YANG DITAGIHKAN
    // BIAYA YANG DIBAYARKAN SATU KALI SAJA
    public $id_jsat = [2];
    // BIAYA YANG DIBAYARKAN TIAP SEMESTER
    public $id_jsem = [1];
    // BIAYA YANG DIBAYARKAN TIAP BULAN
    public $id_jbul = [4];
    // BIAYA YANG DIBAYARKAN TANPA IKATAN WAKTU
    public $id_jfree = [3, 5];

    public $db;

    function __construct($db)
    {
        $this->db = $db;
    }

    function convert_array_to_string_query_in($arr)
    {
        if (count($arr) == 1) {
            return "{$arr[0]}";
        } else {
            return implode(",", $arr);
        }
    }

    private function get_condition_biaya_kuliah($semester, $fakultas, $prodi, $jenjang, $jalur, $kelompok_biaya)
    {
        $query = "";
        if ($semester != '') {
            $query .= "AND BK.ID_SEMESTER={$semester}";
        }
        if ($prodi != '') {
            $query .= "AND BK.ID_PROGRAM_STUDI={$prodi}";
        }
        if ($fakultas != '') {
            $query .= "AND PS.ID_FAKULTAS={$fakultas}";
        }
        if ($jenjang != '') {
            $query .= "AND BK.ID_JENJANG={$jenjang}";
        }
        if ($jalur != '') {
            $query .= "AND BK.ID_JALUR={$jalur}";
        }
        if ($kelompok_biaya != '') {
            $query .= "AND BK.ID_KELOMPOK_BIAYA={$kelompok_biaya}";
        }
        return $query;
    }

    function load_biaya_kuliah($semester, $fakultas, $prodi, $jenjang, $jalur, $kelompok_biaya)
    {
        $condition_biaya_kuliah = $this->get_condition_biaya_kuliah($semester, $fakultas, $prodi, $jenjang, $jalur, $kelompok_biaya);
        $query = "
            SELECT BK.ID_BIAYA_KULIAH,
            SUM(CASE WHEN DB.ID_JENIS_DETAIL_BIAYA IN (" . $this->convert_array_to_string_query_in($this->id_jsat) . ") THEN COALESCE(BESAR_BIAYA, 0) ELSE 0 END) GEN_SAT,
            SUM(CASE WHEN DB.ID_JENIS_DETAIL_BIAYA IN (" . $this->convert_array_to_string_query_in($this->id_jsem) . ") THEN COALESCE(BESAR_BIAYA, 0) ELSE 0 END) GEN_SEM,
            SUM(CASE WHEN DB.ID_JENIS_DETAIL_BIAYA IN (" . $this->convert_array_to_string_query_in($this->id_jbul) . ") THEN COALESCE(BESAR_BIAYA, 0) ELSE 0 END) GEN_BULAN,
            SUM(CASE WHEN DB.ID_JENIS_DETAIL_BIAYA IN (" . $this->convert_array_to_string_query_in($this->id_jfree) . ") THEN COALESCE(BESAR_BIAYA, 0) ELSE 0 END) GEN_FREE 
            ,S.NM_SEMESTER,S.TAHUN_AJARAN,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JAL.NM_JALUR,KB.NM_KELOMPOK_BIAYA,KB.KHUSUS_KELOMPOK_BIAYA,BK.KETERANGAN_BIAYA_KULIAH
            ,(SELECT COUNT(ID_MHS) FROM BIAYA_KULIAH_MHS WHERE ID_BIAYA_KULIAH=BK.ID_BIAYA_KULIAH) RELASI_MHS
            FROM BIAYA_KULIAH BK
            LEFT JOIN (
                SELECT ID_BIAYA_KULIAH,BESAR_BIAYA,ID_BIAYA,ID_JENIS_DETAIL_BIAYA
                FROM DETAIL_BIAYA
                GROUP BY ID_BIAYA_KULIAH,BESAR_BIAYA,ID_BIAYA,ID_JENIS_DETAIL_BIAYA
            ) DB ON DB.ID_BIAYA_KULIAH=BK.ID_BIAYA_KULIAH    
            JOIN SEMESTER S ON S.ID_SEMESTER = BK.ID_SEMESTER
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = BK.ID_PROGRAM_STUDI
            JOIN FAKULTAS FAK ON FAK.ID_FAKULTAS=PS.ID_FAKULTAS
            JOIN JENJANG J ON J.ID_JENJANG= BK.ID_JENJANG
            JOIN JALUR JAL ON JAL.ID_JALUR =BK.ID_JALUR
            JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = BK.ID_KELOMPOK_BIAYA
            WHERE BK.ID_BIAYA_KULIAH IS NOT NULL {$condition_biaya_kuliah}
            AND FAK.ID_PERGURUAN_TINGGI = ".getenv('ID_PT')."
            GROUP BY BK.ID_BIAYA_KULIAH,S.NM_SEMESTER,S.TAHUN_AJARAN,PS.NM_PROGRAM_STUDI,J.NM_JENJANG
            ,JAL.NM_JALUR,KB.NM_KELOMPOK_BIAYA,KB.KHUSUS_KELOMPOK_BIAYA,BK.KETERANGAN_BIAYA_KULIAH
        ";
        return $this->db->QueryToArray($query);
    }

    function load_biaya_kuliah_mhs($nim)
    {
        return $this->db->QueryToArray("
        SELECT
        BK.ID_BIAYA_KULIAH,
        SUM( CASE WHEN DB.ID_JENIS_DETAIL_BIAYA IN ( 2 ) THEN COALESCE( BESAR_BIAYA, 0 ) ELSE 0 END ) GEN_SAT,
        SUM( CASE WHEN DB.ID_JENIS_DETAIL_BIAYA IN ( 1 ) THEN COALESCE( BESAR_BIAYA, 0 ) ELSE 0 END ) GEN_SEM,
        SUM( CASE WHEN DB.ID_JENIS_DETAIL_BIAYA IN ( 4 ) THEN COALESCE( BESAR_BIAYA, 0 ) ELSE 0 END ) GEN_BULAN,
        SUM( CASE WHEN DB.ID_JENIS_DETAIL_BIAYA IN ( 3, 5 ) THEN COALESCE( BESAR_BIAYA, 0 ) ELSE 0 END ) GEN_FREE,
        S.NM_SEMESTER,
        S.TAHUN_AJARAN,
        PS.NM_PROGRAM_STUDI,
        J.NM_JENJANG,
        JAL.NM_JALUR,
        KB.NM_KELOMPOK_BIAYA,
        KB.KHUSUS_KELOMPOK_BIAYA,
        BK.KETERANGAN_BIAYA_KULIAH 
    FROM
        BIAYA_KULIAH BK
        JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BK.ID_BIAYA_KULIAH
        JOIN MAHASISWA M ON M.ID_PROGRAM_STUDI = BK.ID_PROGRAM_STUDI
        JOIN SEMESTER S ON S.ID_SEMESTER = BK.ID_SEMESTER
        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = BK.ID_PROGRAM_STUDI
        JOIN JENJANG J ON J.ID_JENJANG = BK.ID_JENJANG
        JOIN JALUR JAL ON JAL.ID_JALUR = BK.ID_JALUR
        JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = BK.ID_KELOMPOK_BIAYA 
    WHERE
        BK.ID_BIAYA_KULIAH IS NOT NULL 
        AND M.NIM_MHS = '{$nim}' 
    GROUP BY
        BK.ID_BIAYA_KULIAH,
        S.NM_SEMESTER,
        S.TAHUN_AJARAN,
        PS.NM_PROGRAM_STUDI,
        J.NM_JENJANG,
        JAL.NM_JALUR,
        KB.NM_KELOMPOK_BIAYA,
        KB.KHUSUS_KELOMPOK_BIAYA,
        BK.KETERANGAN_BIAYA_KULIAH");
    }

    function get_biaya_kuliah_mhs($nim)
    {
        $this->db->Query("
            SELECT BK.*,S.NM_SEMESTER,S.TAHUN_AJARAN,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JAL.NM_JALUR,KB.NM_KELOMPOK_BIAYA,KB.KHUSUS_KELOMPOK_BIAYA
                FROM BIAYA_KULIAH BK
            JOIN BIAYA_KULIAH_MHS BKM ON BKM.ID_BIAYA_KULIAH = BK.ID_BIAYA_KULIAH
            JOIN SEMESTER S ON S.ID_SEMESTER = BK.ID_SEMESTER
            JOIN MAHASISWA M ON BKM.ID_MHS=M.ID_MHS
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = BK.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG= BK.ID_JENJANG
            JOIN JALUR JAL ON JAL.ID_JALUR =BK.ID_JALUR
            JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = BK.ID_KELOMPOK_BIAYA
            WHERE BK.ID_BIAYA_KULIAH IS NOT NULL AND M.NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function update_biaya_kuliah_mhs($nim, $id_biaya_kuliah)
    {
        $this->db->Query("UPDATE BIAYA_KULIAH_MHS SET ID_BIAYA_KULIAH='{$id_biaya_kuliah}' WHERE ID_MHS IN (SELECT ID_MHS FROM MAHASISWA WHERE NIM_MHS='{$nim}')");
        $id_kelompok_biaya = $this->db->QuerySingle("SELECT ID_KELOMPOK_BIAYA FROM BIAYA_KULIAH WHERE ID_BIAYA_KULIAH ='{$id_biaya_kuliah}'");
        $this->db->Query("UPDATE MAHASISWA SET ID_KELOMPOK_BIAYA='{$id_kelompok_biaya}' WHERE NIM_MHS='{$nim}'");
    }

    function insert_biaya_kuliah_mhs($nim, $id_biaya_kuliah)
    {
        $this->db->Query("INSERT INTO BIAYA_KULIAH_MHS (ID_MHS,ID_BIAYA_KULIAH) SELECT ID_MHS,'{$id_biaya_kuliah}' AS ID_BIAYA_KULIAH FROM MAHASISWA WHERE NIM_MHS='{$nim}'");
        $id_kelompok_biaya = $this->db->QuerySingle("SELECT ID_KELOMPOK_BIAYA FROM BIAYA_KULIAH WHERE ID_BIAYA_KULIAH ='{$id_biaya_kuliah}'");
        $this->db->Query("UPDATE MAHASISWA SET ID_KELOMPOK_BIAYA='{$id_kelompok_biaya}' WHERE NIM_MHS='{$nim}'");
    }

    function get_biaya_kuliah_added($semester, $prodi, $jalur, $kelompok_biaya, $keterangan)
    {
        $q_keterangan = !empty($keterangan) ? "AND BK.KETERANGAN_BIAYA_KULIAH='{$keterangan}'" : "AND BK.KETERANGAN_BIAYA_KULIAH IS NULL";
        $this->db->Query("SELECT BK.*,S.NM_SEMESTER,S.TAHUN_AJARAN,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JAL.NM_JALUR,KB.NM_KELOMPOK_BIAYA,KB.KHUSUS_KELOMPOK_BIAYA
            FROM BIAYA_KULIAH BK
            JOIN SEMESTER S ON S.ID_SEMESTER = BK.ID_SEMESTER
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = BK.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG= BK.ID_JENJANG
            JOIN JALUR JAL ON JAL.ID_JALUR =BK.ID_JALUR
            JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = BK.ID_KELOMPOK_BIAYA
            WHERE BK.BESAR_BIAYA_KULIAH IS NULL AND BK.ID_SEMESTER='{$semester}' 
                AND BK.ID_PROGRAM_STUDI='{$prodi}' AND BK.ID_JALUR='{$jalur}' 
                AND BK.ID_KELOMPOK_BIAYA='{$kelompok_biaya}'  {$q_keterangan}");
        return $this->db->FetchAssoc();
    }

    function get_biaya_kuliah($id_biaya_kuliah)
    {
        $this->db->Query("
            SELECT BK.*,S.NM_SEMESTER,S.TAHUN_AJARAN,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JAL.NM_JALUR,KB.NM_KELOMPOK_BIAYA,KB.KHUSUS_KELOMPOK_BIAYA FROM BIAYA_KULIAH BK
            JOIN SEMESTER S ON S.ID_SEMESTER = BK.ID_SEMESTER
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = BK.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG= BK.ID_JENJANG
            JOIN JALUR JAL ON JAL.ID_JALUR =BK.ID_JALUR
            JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = BK.ID_KELOMPOK_BIAYA
            WHERE BK.ID_BIAYA_KULIAH='{$id_biaya_kuliah}'");
        return $this->db->FetchAssoc();
    }

    function get_detail_biaya($id_biaya_kuliah, $id_biaya)
    {
        $this->db->Query("
            SELECT DB.*,JDB.NM_JENIS_DETAIL_BIAYA
            FROM DETAIL_BIAYA DB
            LEFT JOIN JENIS_DETAIL_BIAYA JDB ON JDB.ID_JENIS_DETAIL_BIAYA=DB.ID_JENIS_DETAIL_BIAYA
            WHERE ID_BIAYA_KULIAH='{$id_biaya_kuliah}' 
            AND ID_BIAYA='{$id_biaya}'
        ");
        return $this->db->FetchAssoc();
    }

    function cek_detail_biaya($id_biaya_kuliah, $id_biaya)
    {
        return $this->db->QuerySingle("SELECT COUNT(*) FROM DETAIL_BIAYA WHERE ID_BIAYA_KULIAH='{$id_biaya_kuliah}' AND ID_BIAYA='{$id_biaya}'");
    }

    function load_jenis_detail_biaya()
    {
        return $this->db->QueryToArray("SELECT * FROM JENIS_DETAIL_BIAYA ORDER BY ID_JENIS_DETAIL_BIAYA");
    }

    function load_detail_biaya($id_biaya_kuliah)
    {
        $data_detail_biaya = array();
        foreach ($this->db->QueryToArray("SELECT * FROM BIAYA WHERE ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "' ORDER BY NM_BIAYA") as $data) {
            array_push($data_detail_biaya, array(
                'ID_BIAYA' => $data['ID_BIAYA'],
                'NM_BIAYA' => $data['NM_BIAYA'],
                'JENIS_BIAYA'=>$data['JENIS_BIAYA'],
                'DETAIL_BIAYA' => $this->get_detail_biaya($id_biaya_kuliah, $data['ID_BIAYA'])
            ));
        }
        return $data_detail_biaya;
    }

    function add_detail_biaya($id_biaya_kuliah, $id_biaya, $besar_biaya, $id_jenis_detail_biaya)
    {
        $this->db->Query("
            INSERT INTO DETAIL_BIAYA
                (ID_BIAYA_KULIAH,ID_BIAYA,BESAR_BIAYA,ID_JENIS_DETAIL_BIAYA)
            VALUES
                ('{$id_biaya_kuliah}','{$id_biaya}','{$besar_biaya}','{$id_jenis_detail_biaya}')");
    }

    function update_detail_biaya($id_biaya_kuliah, $id_biaya, $besar_biaya, $id_jenis_detail_biaya)
    {
        $this->db->Query("
            UPDATE DETAIL_BIAYA SET
                BESAR_BIAYA='{$besar_biaya}',
                ID_JENIS_DETAIL_BIAYA='{$id_jenis_detail_biaya}'
            WHERE ID_BIAYA_KULIAH='{$id_biaya_kuliah}' AND ID_BIAYA='{$id_biaya}'");
    }

    function delete_detail_biaya($id_biaya_kuliah, $id_biaya)
    {
        $this->db->Query("DELETE FROM DETAIL_BIAYA WHERE ID_BIAYA_KULIAH='{$id_biaya_kuliah}' AND ID_BIAYA='{$id_biaya}'");
    }

    function update_biaya_kuliah($id_biaya_kuliah, $besar_biaya_kuliah)
    {
        $this->db->Query("
            UPDATE BIAYA_KULIAH SET
                BESAR_BIAYA_KULIAH='{$besar_biaya_kuliah}'
            WHERE ID_BIAYA_KULIAH='{$id_biaya_kuliah}'");
    }

    function update_master_biaya_kuliah($id_biaya_kuliah, $jalur, $kelompok_biaya, $semester, $keterangan)
    {
        $this->db->Query("
            UPDATE BIAYA_KULIAH SET
                ID_JALUR='{$jalur}',
                ID_KELOMPOK_BIAYA='{$kelompok_biaya}',
                ID_SEMESTER='{$semester}',
                KETERANGAN_BIAYA_KULIAH='{$keterangan}'
            WHERE ID_BIAYA_KULIAH='{$id_biaya_kuliah}'");
    }

    function add_biaya_kuliah($semester, $prodi, $jenjang, $jalur, $kelompok_biaya, $keterangan)
    {
        $this->db->Query("
            INSERT INTO BIAYA_KULIAH
                (ID_SEMESTER,ID_PROGRAM_STUDI,ID_JENJANG,ID_JALUR,ID_KELOMPOK_BIAYA,KETERANGAN_BIAYA_KULIAH)
            VALUES
                ('{$semester}','{$prodi}','{$jenjang}','{$jalur}','{$kelompok_biaya}','{$keterangan}')");
    }

    function load_kelompok_biaya()
    {
        return $this->db->QueryToArray("SELECT * FROM KELOMPOK_BIAYA WHERE ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "'");
    }

    function get_kelompok_biaya($id)
    {
        $this->db->Query("SELECT * FROM KELOMPOK_BIAYA WHERE ID_KELOMPOK_BIAYA='{$id}'");
        return $this->db->FetchAssoc();
    }

    function add_kelompok_biaya($nama, $jenis, $keterangan)
    {
        $this->db->Query("
		INSERT INTO KELOMPOK_BIAYA
			(NM_KELOMPOK_BIAYA,KHUSUS_KELOMPOK_BIAYA,KETERANGAN_KELOMPOK_BIAYA,ID_PERGURUAN_TINGGI)
		VALUES
			('{$nama}','{$jenis}','{$keterangan}','" . getenv('ID_PT') . "')");
    }

    function edit_kelompok_biaya($nama, $jenis, $keterangan, $id)
    {
        $this->db->Query("
		UPDATE KELOMPOK_BIAYA SET
			NM_KELOMPOK_BIAYA='{$nama}',
			KHUSUS_KELOMPOK_BIAYA='{$jenis}',
			KETERANGAN_KELOMPOK_BIAYA='{$keterangan}'
		WHERE ID_KELOMPOK_BIAYA={$id}
		");
    }

    function load_biaya_sks($id_fakultas = null)
    {
        if (isset($id_fakultas)) {
            return $this->db->QueryToArray("
            SELECT BS.*,F.NM_FAKULTAS,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,S.NM_SEMESTER,S.TAHUN_AJARAN,S.GROUP_SEMESTER,JAL.NM_JALUR
            FROM BIAYA_SP BS
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = BS.ID_PROGRAM_STUDI
            JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
            JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            JOIN SEMESTER S ON S.ID_SEMESTER = BS.ID_SEMESTER
            LEFT JOIN JALUR JAL ON JAL.ID_JALUR=BS.ID_JALUR
            WHERE F.ID_FAKULTAS='{$id_fakultas}' AND S.TIPE_SEMESTER='SP'
            ORDER BY PS.ID_FAKULTAS,S.ID_SEMESTER,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,JAL.NM_JALUR");
        } else {
            return $this->db->QueryToArray("
            SELECT BS.*,F.NM_FAKULTAS,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,S.NM_SEMESTER,S.TAHUN_AJARAN,S.GROUP_SEMESTER,JAL.NM_JALUR
            FROM BIAYA_SP BS
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = BS.ID_PROGRAM_STUDI
            JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS AND F.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "'
            JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            JOIN SEMESTER S ON S.ID_SEMESTER = BS.ID_SEMESTER
            LEFT JOIN JALUR JAL ON JAL.ID_JALUR=BS.ID_JALUR
            WHERE S.TIPE_SEMESTER='SP'
            ORDER BY PS.ID_FAKULTAS,S.ID_SEMESTER,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,JAL.NM_JALUR");
        }
    }

    function get_biaya_sks($id_biaya_sks)
    {
        $this->db->Query("SELECT * FROM BIAYA_SP WHERE ID_BIAYA_SP='{$id_biaya_sks}'");
        return $this->db->FetchAssoc();
    }

    function add_biaya_sks($semester, $prodi, $besar_biaya, $keterangan, $jalur)
    {
        $this->db->Query("
            INSERT INTO BIAYA_SP
                (ID_SEMESTER,ID_PROGRAM_STUDI,BESAR_BIAYA_SP,KETERANGAN_BIAYA_SP,ID_JALUR)
            VALUES
                ('{$semester}','{$prodi}','{$besar_biaya}','{$keterangan}','{$jalur}')");
    }

    function edit_biaya_sks($semester, $prodi, $besar_biaya, $keterangan, $id_biaya_sks, $jalur)
    {
        $this->db->Query("
            UPDATE BIAYA_SP SET
                ID_SEMESTER='{$semester}',
                ID_PROGRAM_STUDI='{$prodi}',
                BESAR_BIAYA_SP='{$besar_biaya}',
                KETERANGAN_BIAYA_SP='{$keterangan}',
                ID_JALUR='{$jalur}'
            WHERE ID_BIAYA_SP='{$id_biaya_sks}'");
    }

    function load_nama_biaya()
    {
        return $this->db->QueryToArray("
            SELECT B.*,(SELECT COUNT(*) FROM DETAIL_BIAYA WHERE ID_BIAYA=B.ID_BIAYA) RELASI ,JDB.NM_JENIS_DETAIL_BIAYA
            FROM BIAYA B
            LEFT JOIN JENIS_DETAIL_BIAYA JDB ON JDB.ID_JENIS_DETAIL_BIAYA=B.JENIS_BIAYA
            WHERE ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "' ORDER BY ID_BIAYA");
    }

    function get_nama_biaya($id_biaya)
    {
        $this->db->Query("SELECT * FROM BIAYA 
                            WHERE ID_BIAYA='{$id_biaya}'");
        return $this->db->FetchAssoc();
    }

    function add_nama_biaya($nama, $keterangan, $jenis_biaya)
    {
        $this->db->Query("
            INSERT INTO BIAYA
                (NM_BIAYA,KETERANGAN_BIAYA,JENIS_BIAYA,ID_PERGURUAN_TINGGI)
            VALUES
                ('{$nama}','{$keterangan}','{$jenis_biaya}','" . getenv('ID_PT') . "')");
    }

    function edit_nama_biaya($nama, $keterangan, $jenis_biaya, $id_biaya)
    {
        $this->db->Query("
            UPDATE BIAYA SET
                NM_BIAYA='{$nama}',
                KETERANGAN_BIAYA='{$keterangan}',
                JENIS_BIAYA='{$jenis_biaya}'
            WHERE ID_BIAYA='{$id_biaya}'");
    }

    function load_bank_vendor_exclude($id_mhs)
    {
        return $this->db->QueryToArray("SELECT * FROM BANK_VENDOR WHERE ID_BANK_VENDOR NOT IN (SELECT ID_BANK_VENDOR FROM MAHASISWA_VA WHERE ID_MHS='{$id_mhs}') ORDER BY NAMA_BANK");
    }

    function load_bank_vendor()
    {
        return $this->db->QueryToArray("SELECT * FROM BANK_VENDOR ORDER BY NAMA_BANK");
    }

    function get_bank_vendor($id_bank)
    {
        $this->db->Query("SELECT * FROM BANK_VENDOR WHERE ID_BANK_VENDOR='{$id_bank}'");
        return $this->db->FetchAssoc();
    }

    function add_bank_vendor($kode,$nama)
    {
        $this->db->Query("
            INSERT INTO BANK_VENDOR (KODE_BANK,NAMA_BANK) VALUES ('{$kode}','{$nama}')");
    }

    function edit_bank_vendor($kode,$nama, $id_bank)
    {
        $this->db->Query("
            UPDATE BANK_VENDOR SET KODE_BANK='{$kode}',NAMA_BANK='{$nama}' WHERE ID_BANK_VENDOR='{$id_bank}'");
    }

    function load_bank()
    {
        return $this->db->QueryToArray("SELECT * FROM BANK ORDER BY NM_BANK");
    }

    function get_bank($id_bank)
    {
        $this->db->Query("SELECT * FROM BANK WHERE ID_BANK='{$id_bank}'");
        return $this->db->FetchAssoc();
    }

    function add_bank($nama)
    {
        $this->db->Query("
            INSERT INTO BANK (NM_BANK) VALUES ('{$nama}')");
    }

    function edit_bank($nama, $id_bank)
    {
        $this->db->Query("
            UPDATE BANK SET NM_BANK='{$nama}' WHERE ID_BANK='{$id_bank}'");
    }

    function load_bank_via()
    {
        return $this->db->QueryToArray("SELECT * FROM BANK_VIA ORDER BY NAMA_BANK_VIA");
    }

    function get_bank_via($id_bank_via)
    {
        $this->db->Query("SELECT * FROM BANK_VIA WHERE ID_BANK_VIA='{$id_bank_via}'");
        return $this->db->FetchAssoc();
    }

    function add_bank_via($kode, $nama)
    {
        $this->db->Query("SELECT ID_BANK_VIA FROM BANK_VIA ORDER BY ID_BANK_VIA DESC");
        $temp_data = $this->db->FetchAssoc();
        $id_bank_via = $temp_data['ID_BANK_VIA'] + 1;
        $this->db->Query("
            INSERT INTO BANK_VIA (ID_BANK_VIA,KODE_BANK_VIA,NAMA_BANK_VIA) VALUES ('{$id_bank_via}','{$kode}','{$nama}')");
    }

    function edit_bank_via($kode, $nama, $id_bank_via)
    {
        $this->db->Query("
            UPDATE BANK_VIA SET KODE_BANK_VIA='{$kode}',NAMA_BANK_VIA='{$nama}' WHERE ID_BANK_VIA='{$id_bank_via}'");
    }

    //fungsi status pembayaran
    function load_status_pembayaran()
    {
        return $this->db->QueryToArray("SELECT * FROM STATUS_PEMBAYARAN ORDER BY ID_STATUS_PEMBAYARAN");
    }

    function get_status_pembayaran($id_status)
    {
        $this->db->Query("SELECT * FROM STATUS_PEMBAYARAN WHERE ID_STATUS_PEMBAYARAN='{$id_status}'");
        return $this->db->FetchAssoc();
    }

    function add_status_pembayaran($nama, $keterangan)
    {
        $this->db->Query("INSERT INTO STATUS_PEMBAYARAN (NAMA_STATUS,KETERANGAN) VALUES ('{$nama}','{$keterangan}')");
    }

    function edit_status_pembayaran($id_status, $nama, $keterangan)
    {
        $this->db->Query("UPDATE STATUS_PEMBAYARAN SET NAMA_STATUS='{$nama}',KETERANGAN='{$keterangan}' WHERE ID_STATUS_PEMBAYARAN='{$id_status}'");
    }

    //fungsi status piutang
    function load_status_piutang()
    {
        return $this->db->QueryToArray("SELECT * FROM STATUS_PIUTANG ORDER BY ID_STATUS_PIUTANG");
    }

    function get_status_piutang($id_status)
    {
        $this->db->Query("SELECT * FROM STATUS_PIUTANG WHERE ID_STATUS_PIUTANG='{$id_status}'");
        return $this->db->FetchAssoc();
    }

    function add_status_piutang($nama, $keterangan)
    {
        $this->db->Query("INSERT INTO STATUS_PIUTANG (NAMA_STATUS,KETERANGAN) VALUES ('{$nama}','{$keterangan}')");
    }

    function edit_status_piutang($id_status, $nama, $keterangan)
    {
        $this->db->Query("UPDATE STATUS_PIUTANG SET NAMA_STATUS='{$nama}',KETERANGAN='{$keterangan}' WHERE ID_STATUS_PIUTANG='{$id_status}'");
    }

    //fungsi periode bayar
    function load_periode_bayar_mhs()
    {
        return $this->db->QueryToArray("
            SELECT PB.*,S.NM_SEMESTER,S.TAHUN_AJARAN
            FROM PERIODE_BAYAR PB
            JOIN SEMESTER S ON S.ID_SEMESTER = PB.ID_SEMESTER AND S.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "'
            ");
    }

    function load_periode_bayar_cmhs()
    {
        return $this->db->QueryToArray("
            SELECT PB.*,PEN.NM_PENERIMAAN,PEN.GELOMBANG,JAL.NM_JALUR
            FROM PERIODE_BAYAR_CMHS PB
            JOIN PENERIMAAN PEN ON PEN.ID_PENERIMAAN=PB.ID_PENERIMAAN
            JOIN JALUR JAL ON PEN.ID_JALUR=JAL.ID_JALUR");
    }

    //fungsi copy master biaya
    function copy_master_biaya($id_biaya_kuliah_copy, $id_biaya_kuliah_baru)
    {
        $this->db->Query("
            INSERT INTO DETAIL_BIAYA 
                (ID_BIAYA_KULIAH,ID_BIAYA,BESAR_BIAYA,ID_JENIS_DETAIL_BIAYA,STATUS_BIAYA)
            SELECT '{$id_biaya_kuliah_baru}' ID_BIAYA_KULIAH,ID_BIAYA,BESAR_BIAYA,ID_JENIS_DETAIL_BIAYA,STATUS_BIAYA
            FROM DETAIL_BIAYA 
            WHERE ID_BIAYA_KULIAH='{$id_biaya_kuliah_copy}'");
    }

    //fungsi Kerjasama

    function load_group_kerjasama()
    {
        return $this->db->QueryToArray("SELECT * FROM GROUP_BEASISWA WHERE ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "' ORDER BY ID_GROUP_BEASISWA");
    }

    function get_group_kerjasama($id)
    {
        $this->db->Query("SELECT * FROM GROUP_BEASISWA WHERE ID_GROUP_BEASISWA='{$id}'");
        return $this->db->FetchAssoc();
    }

    function add_group_kerjasama($nama)
    {
        $this->db->QUery("INSERT INTO GROUP_BEASISWA (NM_GROUP,ID_PERGURUAN_TINGGI) VALUES ('{$nama}','" . getenv('ID_PT') . "')");
    }

    function edit_group_kerjasama($id, $nama)
    {
        $this->db->Query("UPDATE GROUP_BEASISWA SET NM_GROUP='{$nama}' WHERE ID_GROUP_BEASISWA='{$id}'");
    }

    function delete_group_kerjasama($id)
    {
        $this->db->Query("DELETE GROUP_BEASISWA WHERE ID_GROUP_BEASISWA='{$id}'");
    }

    function load_kerjasama()
    {
        return $this->db->QueryToArray("
            SELECT B.*,GB.NM_GROUP FROM BEASISWA B 
            JOIN GROUP_BEASISWA GB ON GB.ID_GROUP_BEASISWA=B.ID_GROUP_BEASISWA AND GB.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "'
            ORDER BY B.NM_BEASISWA");
    }

    function get_kerjasama($id)
    {
        $this->db->Query("SELECT * FROM BEASISWA WHERE ID_BEASISWA='{$id}'");
        return $this->db->FetchAssoc();
    }

    function add_kerjasama($nama, $penyelenggara, $jenis, $periode, $besar, $group, $keterangan)
    {
        $this->db->Query("
            INSERT INTO BEASISWA 
                (NM_BEASISWA,PENYELENGGARA_BEASISWA,JENIS_BEASISWA,PERIODE_PEMBERIAN_BEASISWA,BESAR_BEASISWA,ID_GROUP_BEASISWA,KETERANGAN)
            VALUES 
                ('{$nama}','{$penyelenggara}','{$jenis}','{$periode}','{$besar}','{$group}','{$keterangan}')");
    }

    function edit_kerjasama($id, $nama, $penyelenggara, $jenis, $periode, $besar, $group, $keterangan)
    {
        $this->db->Query("
            UPDATE BEASISWA
            SET
                NM_BEASISWA='{$nama}',
                PENYELENGGARA_BEASISWA='{$penyelenggara}',
                JENIS_BEASISWA='{$jenis}',
                PERIODE_PEMBERIAN_BEASISWA='{$periode}',
                BESAR_BEASISWA='{$besar}',
                ID_GROUP_BEASISWA='{$group}',
                KETERANGAN='{$keterangan}'
            WHERE ID_BEASISWA='{$id}'");
    }

    function delete_kerjasama($id)
    {
        $this->db->Query("DELETE BEASISWA WHERE ID_BEASISWA='{$id}'");
    }

    function get_bulan_semester($nm_semester)
    {
        return $this->db->QueryToArray("SELECT * FROM SEMESTER_BAYAR_BULAN WHERE NM_SEMESTER='{$nm_semester}' ORDER BY ID_SEMESTER_BAYAR_BULAN");
    }
}
