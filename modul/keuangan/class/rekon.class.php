<?php

class rekon {

    public $db;
    public $id_pengguna;

    function __construct($db, $pengguna) {
        $this->db = $db;
        $this->id_pengguna = $pengguna;
    }

    function cek_rekon($tgl) {
        $str = strftime('%d-%m-%Y', strtotime($tgl));
        $ex = explode("-", $str);
        $month = intval($ex[1]);
        $year = $ex[2];
        $this->db->Query("SELECT * FROM REKON_PEMBAYARAN WHERE BULAN_REKON='{$month}' AND TAHUN_REKON='{$year}' AND STATUS_REKON=1");
        return $this->db->FetchAssoc();
    }

    function save_perubahan($id_rekon, $mhs, $semester, $tgl, $bank, $via, $no_transaksi, $keterangan, $status, $biaya=0) {
        //1= Update
        //2= Update detail
        //3= Tambah
        //4= Hapus
        //5= Update Upload
        $this->db->Query("
            INSERT INTO REKON_PERUBAHAN
                (ID_REKON_PEMBAYARAN,ID_MHS,ID_SEMESTER,TGL_BAYAR,ID_BANK,ID_BANK_VIA,NO_TRANSAKSI,KETERANGAN,STATUS_PERUBAHAN,TGL_PERUBAHAN,ID_PENGGUNA,TOTAL_PEMBAYARAN,STATUS_CLEAR)
            VALUES
                ('{$id_rekon}','{$mhs}','{$semester}','{$tgl}','{$bank}','{$via}','{$no_transaksi}','{$keterangan}','{$status}',SYSDATE,'{$this->id_pengguna}',{$biaya},0)
            ");
    }

}

?>
