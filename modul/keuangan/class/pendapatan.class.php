<?php

class pendapatan {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    //fungsi pendapatan wisuda

    function load_data_pendapatan_wisuda_mode_fakultas($periode_wisuda) {
        return $this->db->QueryToArray("SELECT F.ID_FAKULTAS,F.NM_FAKULTAS,COUNT(DISTINCT(PENGW.ID_MHS)) JUMLAH_MHS,SUM(PERW.BESAR_BIAYA) AS TARIF,
        (
            CASE
            WHEN SUM(PEM.BESAR_BIAYA) IS NOT NULL
            THEN SUM(PEM.BESAR_BIAYA)
            ELSE 0
            END) AS CICILAN,
            (
            CASE
            WHEN SUM(PEMW.BESAR_BIAYA) IS NOT NULL
            THEN SUM(PEMW.BESAR_BIAYA)
            ELSE 0
            END)AS PELUNASAN
        FROM PENGAJUAN_WISUDA PENGW
        JOIN PERIODE_WISUDA PERW ON PENGW.ID_PERIODE_WISUDA = PERW.ID_PERIODE_WISUDA
        JOIN TARIF_WISUDA TW ON TW.ID_TARIF_WISUDA = PERW.ID_TARIF_WISUDA
        LEFT JOIN (
            SELECT * FROM PEMBAYARAN_WISUDA
            WHERE TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL AND ID_BANK_VIA IS NOT NULL
        ) PEMW ON PEMW.ID_MHS = PENGW.ID_MHS
        LEFT JOIN (
            SELECT PEM.ID_MHS,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA
            FROM PEMBAYARAN PEM
            JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
            WHERE ID_STATUS_PEMBAYARAN =1 AND DB.ID_BIAYA =115
            GROUP BY PEM.ID_MHS
        ) PEM
        ON PEM.ID_MHS= PENGW.ID_MHS
        JOIN MAHASISWA M ON M.ID_MHS = PENGW.ID_MHS
        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
        JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        JOIN FAKULTAS F ON PS.ID_FAKULTAS = F.ID_FAKULTAS
        JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
        WHERE TW.ID_TARIF_WISUDA={$periode_wisuda} 
        GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS
        ORDER BY F.ID_FAKULTAS,F.NM_FAKULTAS");
    }

    function load_data_pendapatan_wisuda_mode_prodi($periode_wisuda, $fakultas) {
        return $this->db->QueryToArray("
        SELECT F.ID_FAKULTAS,PS.ID_PROGRAM_STUDI,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,COUNT(DISTINCT(PENGW.ID_MHS)) JUMLAH_MHS,SUM(PERW.BESAR_BIAYA) AS TARIF,
        (
            CASE
            WHEN SUM(PEM.BESAR_BIAYA) IS NOT NULL
            THEN SUM(PEM.BESAR_BIAYA)
            ELSE 0
            END) AS CICILAN,
            (
            CASE
            WHEN SUM(PEMW.BESAR_BIAYA) IS NOT NULL
            THEN SUM(PEMW.BESAR_BIAYA)
            ELSE 0
            END)AS PELUNASAN
        FROM PENGAJUAN_WISUDA PENGW
        JOIN PERIODE_WISUDA PERW ON PENGW.ID_PERIODE_WISUDA = PERW.ID_PERIODE_WISUDA
        JOIN TARIF_WISUDA TW ON TW.ID_TARIF_WISUDA = PERW.ID_TARIF_WISUDA
        LEFT JOIN (
            SELECT * FROM PEMBAYARAN_WISUDA
            WHERE TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL AND ID_BANK_VIA IS NOT NULL
        ) PEMW ON PEMW.ID_MHS = PENGW.ID_MHS
            LEFT JOIN (
            SELECT PEM.ID_MHS,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA
            FROM PEMBAYARAN PEM
            JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
            WHERE ID_STATUS_PEMBAYARAN =1 AND DB.ID_BIAYA =115
            GROUP BY PEM.ID_MHS
        ) PEM
        ON PEM.ID_MHS= PENGW.ID_MHS
        JOIN MAHASISWA M ON M.ID_MHS = PENGW.ID_MHS
        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
        JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        JOIN FAKULTAS F ON PS.ID_FAKULTAS = F.ID_FAKULTAS
        JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
        WHERE TW.ID_TARIF_WISUDA='{$periode_wisuda}' AND F.ID_FAKULTAS='{$fakultas}'
        GROUP BY F.ID_FAKULTAS,PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.NM_JENJANG
        ORDER BY PS.NM_PROGRAM_STUDI,J.NM_JENJANG");
    }

    function load_data_pendapatan_wisuda_mode_detail($periode_wisuda, $fakultas, $prodi) {
        $q_fakultas = $fakultas != '' ? "AND F.ID_FAKULTAS='{$fakultas}'" : "";
        $q_prodi = $prodi != '' ? "AND PS.ID_PROGRAM_STUDI='{$prodi}'" : "";
        return $this->db->QueryToArray("
        SELECT M.ID_MHS,M.NIM_MHS,P.NM_PENGGUNA,M.THN_ANGKATAN_MHS,TW.NM_TARIF_WISUDA,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,F.ID_FAKULTAS,F.NM_FAKULTAS,PERW.BESAR_BIAYA AS TARIF, 
        ( CASE 
            WHEN SUM(PEM.BESAR_BIAYA) IS NOT NULL 
            THEN SUM(PEM.BESAR_BIAYA) 
            ELSE 0 
        END) AS CICILAN, 
        ( CASE 
            WHEN PEMW.BESAR_BIAYA IS NOT NULL 
            THEN PEMW.BESAR_BIAYA 
            ELSE 0 
        END)AS PELUNASAN
        FROM PENGAJUAN_WISUDA PENGW 
        JOIN PERIODE_WISUDA PERW ON PENGW.ID_PERIODE_WISUDA = PERW.ID_PERIODE_WISUDA
        JOIN TARIF_WISUDA TW ON TW.ID_TARIF_WISUDA = PERW.ID_TARIF_WISUDA 
        LEFT JOIN (
            SELECT * FROM PEMBAYARAN_WISUDA
            WHERE TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL AND ID_BANK_VIA IS NOT NULL
        ) PEMW ON PEMW.ID_MHS = PENGW.ID_MHS
        LEFT JOIN (
            SELECT PEM.ID_MHS,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA
            FROM PEMBAYARAN PEM
            JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
            WHERE ID_STATUS_PEMBAYARAN =1 AND DB.ID_BIAYA =115
            GROUP BY PEM.ID_MHS
        ) PEM
        ON PEM.ID_MHS= PENGW.ID_MHS
        JOIN MAHASISWA M ON M.ID_MHS = PENGW.ID_MHS
        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI 
        JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        JOIN FAKULTAS F ON PS.ID_FAKULTAS = F.ID_FAKULTAS 
        JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA 
        WHERE TW.ID_TARIF_WISUDA='{$periode_wisuda}' {$q_fakultas} {$q_prodi}
        GROUP BY F.ID_FAKULTAS,M.ID_MHS,M.NIM_MHS,M.THN_ANGKATAN_MHS,P.NM_PENGGUNA,TW.NM_TARIF_WISUDA,PERW.BESAR_BIAYA
        ,PEMW.BESAR_BIAYA,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,F.NM_FAKULTAS 
        ORDER BY F.ID_FAKULTAS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,M.NIM_MHS");
    }

    //fungsi untuk pendapatan spp

    function load_pendapatan_spp_mala($fakultas, $semester) {
        if ($fakultas != '') {
            $query = "
            SELECT PS.ID_PROGRAM_STUDI,('('||J.NM_JENJANG||') '||PS.NM_PROGRAM_STUDI) NM_PROGRAM_STUDI,PEM.PENDAPATAN_SPP,PEM.PENGEMBALIAN_SPP
            FROM PROGRAM_STUDI PS
            JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            LEFT JOIN (
                SELECT 
                PS.ID_PROGRAM_STUDI,(SUM(PEM.BESAR_BIAYA)+SUM(PEM.DENDA_BIAYA)) PENDAPATAN_SPP,SUM(PP.PENGEMBALIAN) PENGEMBALIAN_SPP
                FROM (
                    SELECT ID_MHS,ID_SEMESTER,SUM(BESAR_BIAYA) AS BESAR_BIAYA,SUM(DENDA_BIAYA) AS DENDA_BIAYA,ID_STATUS_PEMBAYARAN,ID_SEMESTER_BAYAR
                    FROM AUCC.PEMBAYARAN
                    GROUP BY ID_MHS,ID_SEMESTER,ID_STATUS_PEMBAYARAN,ID_SEMESTER_BAYAR
                ) PEM
                JOIN MAHASISWA M ON PEM.ID_MHS= M.ID_MHS
                LEFT JOIN (
                    SELECT ID_MHS,SUM(BESAR_PENGEMBALIAN) PENGEMBALIAN
                    FROM PENGEMBALIAN_PEMBAYARAN
                    WHERE ID_SEMESTER='{$semester}'
                    GROUP BY ID_MHS
                ) PP ON PP.ID_MHS = M.ID_MHS
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                WHERE PEM.ID_SEMESTER='{$semester}' AND PEM.ID_SEMESTER_BAYAR='{$semester}' AND PEM.ID_STATUS_PEMBAYARAN=1 
                GROUP BY PS.ID_PROGRAM_STUDI
                ORDER BY PS.ID_PROGRAM_STUDI
            ) PEM ON PEM.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            WHERE PS.ID_FAKULTAS='{$fakultas}'
            ORDER BY NM_PROGRAM_STUDI
            ";
        } else {
            $query = "
            SELECT F.ID_FAKULTAS,F.NM_FAKULTAS,PEM.PENDAPATAN_SPP,PEM.PENGEMBALIAN_SPP FROM FAKULTAS F
            LEFT JOIN (
                SELECT 
                    PS.ID_FAKULTAS,(SUM(PEM.BESAR_BIAYA)+SUM(PEM.DENDA_BIAYA)) PENDAPATAN_SPP,SUM(PP.PENGEMBALIAN) PENGEMBALIAN_SPP
                FROM (
                    SELECT ID_MHS,ID_SEMESTER,SUM(BESAR_BIAYA) AS BESAR_BIAYA,SUM(DENDA_BIAYA) AS DENDA_BIAYA,ID_STATUS_PEMBAYARAN,ID_SEMESTER_BAYAR
                    FROM AUCC.PEMBAYARAN
                    GROUP BY ID_MHS,ID_SEMESTER,ID_STATUS_PEMBAYARAN,ID_SEMESTER_BAYAR
                ) PEM
                JOIN MAHASISWA M ON PEM.ID_MHS= M.ID_MHS
                LEFT JOIN (
                    SELECT ID_MHS,SUM(BESAR_PENGEMBALIAN) PENGEMBALIAN
                    FROM PENGEMBALIAN_PEMBAYARAN
                    WHERE ID_SEMESTER={$semester}
                    GROUP BY ID_MHS
                ) PP ON PP.ID_MHS = M.ID_MHS
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                WHERE PEM.ID_SEMESTER='{$semester}' AND PEM.ID_SEMESTER_BAYAR='{$semester}' AND PEM.ID_STATUS_PEMBAYARAN=1
                GROUP BY PS.ID_FAKULTAS
                ORDER BY PS.ID_FAKULTAS
            ) PEM ON PEM.ID_FAKULTAS=F.ID_FAKULTAS
            ORDER BY F.ID_FAKULTAS
            ";
        }
        return $this->db->QueryToArray($query);
    }

    function load_detail_pendapatan_spp_mala($fakultas, $prodi, $semester) {
        $data_detail_pendapatan = array();
        if ($prodi != '') {
            $q_arr_mahasiswa = "
                SELECT M.ID_MHS,M.NIM_MHS,P.NM_PENGGUNA,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,SP.NM_STATUS_PENGGUNA
                FROM MAHASISWA M
                JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
                JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
                JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
                WHERE M.ID_MHS IN (
                    SELECT DISTINCT(ID_MHS) ID_MHS FROM PEMBAYARAN
                    WHERE ID_SEMESTER='{$semester}' AND ID_SEMESTER_BAYAR='{$semester}' AND ID_STATUS_PEMBAYARAN=1
                ) AND PS.ID_PROGRAM_STUDI='{$prodi}'
                ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI,M.NIM_MHS";
            foreach ($this->db->QueryToArray($q_arr_mahasiswa) as $data) {
                array_push($data_detail_pendapatan, array(
                    'NM_PENGGUNA' => $data['NM_PENGGUNA'],
                    'NIM_MHS' => $data['NIM_MHS'],
                    'PRODI' => $data['NM_JENJANG'] . ' ' . $data['NM_PROGRAM_STUDI'],
                    'STATUS' => $data['NM_STATUS_PENGGUNA'],
                    'DATA_PEMBAYARAN' => $this->db->QueryToArray("
                        SELECT B.ID_BIAYA,B.NM_BIAYA,PEM.BESAR_BIAYA,PEM.DENDA_BIAYA FROM AUCC.BIAYA B
                        LEFT JOIN (
                            SELECT DB.ID_BIAYA,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA,SUM(PEM.DENDA_BIAYA) DENDA_BIAYA FROM AUCC.PEMBAYARAN PEM
                            JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
                            JOIN AUCC.MAHASISWA M ON PEM.ID_MHS = M.ID_MHS
                            WHERE M.ID_MHS='{$data['ID_MHS']}' AND PEM.ID_SEMESTER='{$semester}' AND PEM.ID_SEMESTER_BAYAR='{$semester}' AND PEM.ID_STATUS_PEMBAYARAN=1 
                            GROUP BY ID_BIAYA
                        ) PEM ON PEM.ID_BIAYA = B.ID_BIAYA
                        ORDER BY B.NM_BIAYA"),
                    'DATA_PENGEMBALIAN' => $this->db->QuerySingle("
                        SELECT SUM(BESAR_PENGEMBALIAN) PENGEMBALIAN FROM PENGEMBALIAN_PEMBAYARAN PP
                        JOIN MAHASISWA M ON PP.ID_MHS=M.ID_MHS
                        WHERE PP.ID_SEMESTER='{$semester}' AND M.MHS='{$data['ID_MHS']}'")
                ));
            }
        } else if ($fakultas != '') {
            $q_arr_prodi = "
                SELECT PS.ID_PROGRAM_STUDI,('('||J.NM_JENJANG||') '||PS.NM_PROGRAM_STUDI) NM_PROGRAM_STUDI 
                FROM PROGRAM_STUDI PS
                JOIN JENJANG J ON J.ID_JENJANG= PS.ID_JENJANG WHERE ID_FAKULTAS='{$fakultas}'";
            foreach ($this->db->QueryToArray($q_arr_prodi) as $data) {
                array_push($data_detail_pendapatan, array(
                    'ID_PROGRAM_STUDI' => $data['ID_PROGRAM_STUDI'],
                    'NM_PROGRAM_STUDI' => $data['NM_PROGRAM_STUDI'],
                    'DATA_PEMBAYARAN' => $this->db->QueryToArray("
                        SELECT B.ID_BIAYA,B.NM_BIAYA,PEM.BESAR_BIAYA,PEM.DENDA_BIAYA,PEM.JUMLAH_MHS FROM BIAYA B
                        LEFT JOIN (
                            SELECT DB.ID_BIAYA,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA,SUM(PEM.DENDA_BIAYA) DENDA_BIAYA,COUNT(DISTINCT(M.ID_MHS)) JUMLAH_MHS FROM PEMBAYARAN PEM
                            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
                            JOIN MAHASISWA M ON PEM.ID_MHS = M.ID_MHS
                            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                            WHERE PS.ID_PROGRAM_STUDI='{$data['ID_PROGRAM_STUDI']}' AND 
                            PEM.ID_SEMESTER='{$semester}' AND PEM.ID_SEMESTER_BAYAR='{$semester}' AND PEM.ID_STATUS_PEMBAYARAN=1 
                        GROUP BY ID_BIAYA
                        ) PEM ON PEM.ID_BIAYA = B.ID_BIAYA
                        ORDER BY B.NM_BIAYA"),
                    'DATA_PENGEMBALIAN' => $this->db->QuerySingle("
                        SELECT SUM(BESAR_PENGEMBALIAN) PENGEMBALIAN FROM PENGEMBALIAN_PEMBAYARAN PP
                        JOIN MAHASISWA M ON PP.ID_MHS=M.ID_MHS
                        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                        WHERE PP.ID_SEMESTER='{$semester}' AND M.ID_PROGRAM_STUDI='{$data['ID_PROGRAM_STUDI']}'")
                ));
            }
        } else {
            $q_arr_fakultas = "SELECT * FROM FAKULTAS";
            foreach ($this->db->QueryToArray($q_arr_fakultas) as $data) {
                array_push($data_detail_pendapatan, array(
                    'ID_FAKULTAS' => $data['ID_FAKULTAS'],
                    'NM_FAKULTAS' => $data['NM_FAKULTAS'],
                    'DATA_PEMBAYARAN' => $this->db->QueryToArray("
                        SELECT B.ID_BIAYA,B.NM_BIAYA,PEM.BESAR_BIAYA,PEM.DENDA_BIAYA,PEM.JUMLAH_MHS FROM BIAYA B
                        LEFT JOIN (
                            SELECT DB.ID_BIAYA,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA,SUM(PEM.DENDA_BIAYA) DENDA_BIAYA,COUNT(DISTINCT(M.ID_MHS)) JUMLAH_MHS FROM PEMBAYARAN PEM
                            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
                            JOIN MAHASISWA M ON PEM.ID_MHS = M.ID_MHS
                            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                            WHERE PS.ID_FAKULTAS='{$data['ID_FAKULTAS']}' AND 
                            PEM.ID_SEMESTER='{$semester}' AND PEM.ID_SEMESTER_BAYAR='{$semester}' AND PEM.ID_STATUS_PEMBAYARAN=1 
                        GROUP BY ID_BIAYA
                        ) PEM ON PEM.ID_BIAYA = B.ID_BIAYA
                        ORDER BY B.NM_BIAYA"),
                    'DATA_PENGEMBALIAN' => $this->db->QuerySingle("
                        SELECT SUM(BESAR_PENGEMBALIAN) PENGEMBALIAN FROM PENGEMBALIAN_PEMBAYARAN PP
                        JOIN MAHASISWA M ON PP.ID_MHS=M.ID_MHS
                        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                        WHERE PP.ID_SEMESTER='{$semester}' AND PS.ID_FAKULTAS='{$data['ID_FAKULTAS']}'")
                ));
            }
        }
        return $data_detail_pendapatan;
    }

    function load_pendapatan_spp_maba($fakultas, $semester) {
        if ($fakultas != '') {
            $query = "
            SELECT PS.ID_PROGRAM_STUDI,('('||J.NM_JENJANG||') '||PS.NM_PROGRAM_STUDI) NM_PROGRAM_STUDI,PEM.PENDAPATAN_SPP
            FROM PROGRAM_STUDI PS
            JOIN JENJANG J ON J.ID_JENJANG= PS.ID_JENJANG
            LEFT JOIN (
                SELECT PS.ID_PROGRAM_STUDI,SUM(PC.BESAR_BIAYA) PENDAPATAN_SPP
                FROM CALON_MAHASISWA_BARU CMB
                JOIN PEMBAYARAN_CMHS PC ON CMB.ID_C_MHS = PC.ID_C_MHS
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI= CMB.ID_PROGRAM_STUDI
                WHERE PC.TGL_BAYAR IS NOT NULL AND PC.ID_BANK IS NOT NULL AND PC.ID_SEMESTER='{$semester}' AND PC.ID_SEMESTER_BAYAR='{$semester}'
                GROUP BY PS.ID_PROGRAM_STUDI
                ORDER BY PS.ID_PROGRAM_STUDI
            ) PEM ON PEM.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            WHERE PS.ID_FAKULTAS='{$fakultas}'
            ORDER BY NM_PROGRAM_STUDI";
        } else {
            $query = "
            SELECT F.ID_FAKULTAS,F.NM_FAKULTAS,PEM.PENDAPATAN_SPP
            FROM FAKULTAS F
            LEFT JOIN (
                SELECT PS.ID_FAKULTAS,SUM(
                PC.BESAR_BIAYA) PENDAPATAN_SPP
                FROM CALON_MAHASISWA_BARU CMB
                JOIN PEMBAYARAN_CMHS PC ON CMB.ID_C_MHS = PC.ID_C_MHS
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI= CMB.ID_PROGRAM_STUDI
                WHERE PC.TGL_BAYAR IS NOT NULL AND PC.ID_BANK IS NOT NULL AND PC.ID_SEMESTER='{$semester}' AND PC.ID_SEMESTER_BAYAR='{$semester}'
                GROUP BY PS.ID_FAKULTAS
                ORDER BY PS.ID_FAKULTAS
            ) PEM ON PEM.ID_FAKULTAS = F.ID_FAKULTAS
            ORDER BY F.ID_FAKULTAS";
        }
        return $this->db->QueryToArray($query);
    }

    function load_detail_pendapatan_spp_maba($fakultas, $prodi, $semester) {
        $data_detail_pendapatan = array();
        if ($prodi != '') {
            $q_arr_mahasiswa = "
                SELECT CMB.ID_C_MHS,CMB.NO_UJIAN,CMB.NM_C_MHS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI
                FROM CALON_MAHASISWA_BARU CMB
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
                JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
                WHERE M.ID_C_MHS IN (
                    SELECT DISTINCT(ID_C_MHS) ID_C_MHS FROM PEMBAYARAN_CMHS
                    WHERE ID_SEMESTER='{$semester}' AND TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL
                ) AND PS.ID_PROGRAM_STUDI='{$prodi}'
                ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI,M.NO_UJIAN";
            foreach ($this->db->QueryToArray($q_arr_mahasiswa) as $data) {
                array_push($data_detail_pendapatan, array(
                    'NM_C_MHS' => $data['NM_C_MHS'],
                    'NO_UJIAN' => $data['NO_UJIAN'],
                    'PRODI' => $data['NM_JENJANG'] . ' ' . $data['NM_PROGRAM_STUDI'],
                    'DATA_PEMBAYARAN' => $this->db->QueryToArray("
                        SELECT B.ID_BIAYA,B.NM_BIAYA,PEM.BESAR_BIAYA FROM AUCC.BIAYA B
                        LEFT JOIN (
                            SELECT DB.ID_BIAYA,SUM(PEM.BESAR_BIAYA+PEM.DENDA_BIAYA) BESAR_BIAYA FROM AUCC.PEMBAYARAN_CMHS PEM
                            JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
                            JOIN AUCC.CALON_MAHASISWA_BARU CMB ON PEM.ID_C_MHS = CMB.ID_C_MHS
                            WHERE CMB.ID_C_MHS='{$data['ID_C_MHS']}' AND PEM.ID_SEMESTER='{$semester}' AND PEM.ID_SEMESTER_BAYAR='{$semester}' AND  PEM.TGL_BAYAR IS NOT NULL AND PEM.ID_BANK IS NOT NULL
                            GROUP BY ID_BIAYA
                        ) PEM ON PEM.ID_BIAYA = B.ID_BIAYA
                        ORDER BY B.NM_BIAYA")
                ));
            }
        } else if ($fakultas != '') {
            $q_arr_prodi = "
                SELECT PS.ID_PROGRAM_STUDI,('('||J.NM_JENJANG||') '||PS.NM_PROGRAM_STUDI) NM_PROGRAM_STUDI FROM PROGRAM_STUDI PS
                JOIN JENJANG J ON J.ID_JENJANG= PS.ID_JENJANG WHERE ID_FAKULTAS='{$fakultas}'";
            foreach ($this->db->QueryToArray($q_arr_prodi) as $data) {
                array_push($data_detail_pendapatan, array(
                    'ID_PROGRAM_STUDI' => $data['ID_PROGRAM_STUDI'],
                    'NM_PROGRAM_STUDI' => $data['NM_PROGRAM_STUDI'],
                    'DATA_PEMBAYARAN' => $this->db->QueryToArray("
                        SELECT B.ID_BIAYA,B.NM_BIAYA,PEM.BESAR_BIAYA FROM BIAYA B
                        LEFT JOIN (
                            SELECT DB.ID_BIAYA,SUM(PEM.BESAR_BIAYA+PEM.DENDA_BIAYA) BESAR_BIAYA FROM AUCC.PEMBAYARAN_CMHS PEM
                            JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
                            JOIN AUCC.CALON_MAHASISWA_BARU CMB ON PEM.ID_C_MHS = CMB.ID_C_MHS
                            WHERE PS.ID_PROGRAM_STUDI='{$data['ID_PROGRAM_STUDI']}' AND 
                            PEM.ID_SEMESTER='{$semester}' AND PEM.ID_SEMESTER_BAYAR='{$semester}' AND  PEM.TGL_BAYAR IS NOT NULL AND PEM.ID_BANK IS NOT NULL
                        GROUP BY ID_BIAYA
                        ) PEM ON PEM.ID_BIAYA = B.ID_BIAYA
                        ORDER BY B.NM_BIAYA")
                ));
            }
        } else {
            $q_arr_fakultas = "SELECT * FROM FAKULTAS";
            foreach ($this->db->QueryToArray($q_arr_fakultas) as $data) {
                array_push($data_detail_pendapatan, array(
                    'ID_FAKULTAS' => $data['ID_FAKULTAS'],
                    'NM_FAKULTAS' => $data['NM_FAKULTAS'],
                    'DATA_PEMBAYARAN' => $this->db->QueryToArray("
                        SELECT B.ID_BIAYA,B.NM_BIAYA,PEM.BESAR_BIAYA FROM BIAYA B
                        LEFT JOIN (
                            SELECT DB.ID_BIAYA,SUM(PEM.BESAR_BIAYA+PEM.DENDA_BIAYA) BESAR_BIAYA FROM AUCC.PEMBAYARAN_CMHS PEM
                            JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
                            JOIN AUCC.CALON_MAHASISWA_BARU CMB ON PEM.ID_C_MHS = CMB.ID_C_MHS
                            WHERE PS.ID_FAKULTAS='{$data['ID_FAKULTAS']}' AND 
                            PEM.ID_SEMESTER='{$semester}' AND PEM.ID_SEMESTER_BAYAR='{$semester}' AND PEM.TGL_BAYAR IS NOT NULL AND PEM.ID_BANK IS NOT NULL
                        GROUP BY ID_BIAYA
                        ) PEM ON PEM.ID_BIAYA = B.ID_BIAYA
                        ORDER BY B.NM_BIAYA")
                ));
            }
        }
        return $data_detail_pendapatan;
    }

    function load_pendapatan_terima_muka_spp_mala($fakultas, $semester) {
        if ($fakultas != '') {
            $query = "
            SELECT PS.ID_PROGRAM_STUDI,('('||J.NM_JENJANG||') '||PS.NM_PROGRAM_STUDI) NM_PROGRAM_STUDI,PEM.PENDAPATAN_TERIMA_MUKA
            FROM PROGRAM_STUDI PS
            JOIN JENJANG J ON J.ID_JENJANG= PS.ID_JENJANG
            LEFT JOIN (
                SELECT PS.ID_PROGRAM_STUDI,SUM(
                CASE 
                    WHEN S.SEMESTER>SB.SEMESTER
                    THEN PEM.BESAR_BIAYA+PEM.DENDA_BIAYA
                    ELSE 0
                END) PENDAPATAN_TERIMA_MUKA FROM MAHASISWA M
                JOIN PEMBAYARAN PEM ON PEM.ID_MHS = M.ID_MHS
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                JOIN (
                    SELECT ID_SEMESTER,(TAHUN_AJARAN||' '||NM_SEMESTER) SEMESTER
                    FROM SEMESTER
                ) S ON S.ID_SEMESTER = PEM.ID_SEMESTER
                JOIN (
                    SELECT ID_SEMESTER,(TAHUN_AJARAN||' '||NM_SEMESTER) SEMESTER
                    FROM SEMESTER
                ) SB ON SB.ID_SEMESTER = PEM.ID_SEMESTER_BAYAR
                WHERE PEM.ID_SEMESTER_BAYAR='{$semester}' AND PEM.ID_STATUS_PEMBAYARAN=1
                GROUP BY PS.ID_PROGRAM_STUDI
                ORDER BY PS.ID_PROGRAM_STUDI
            ) PEM ON PEM.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            WHERE PS.ID_FAKULTAS='{$fakultas}'
            ORDER BY NM_PROGRAM_STUDI";
        } else {
            $query = "
            SELECT F.ID_FAKULTAS,F.NM_FAKULTAS,PEM.PENDAPATAN_TERIMA_MUKA
            FROM FAKULTAS F 
            LEFT JOIN (
                SELECT PS.ID_FAKULTAS,SUM(
                CASE 
                    WHEN S.SEMESTER>SB.SEMESTER
                    THEN PEM.BESAR_BIAYA+PEM.DENDA_BIAYA
                    ELSE 0
                    END) PENDAPATAN_TERIMA_MUKA FROM MAHASISWA M
                JOIN PEMBAYARAN PEM ON PEM.ID_MHS = M.ID_MHS
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                JOIN (
                    SELECT ID_SEMESTER,(TAHUN_AJARAN||' '||NM_SEMESTER) SEMESTER
                    FROM SEMESTER
                ) S ON S.ID_SEMESTER = PEM.ID_SEMESTER
                JOIN (
                    SELECT ID_SEMESTER,(TAHUN_AJARAN||' '||NM_SEMESTER) SEMESTER
                    FROM SEMESTER
                ) SB ON SB.ID_SEMESTER = PEM.ID_SEMESTER_BAYAR
                WHERE PEM.ID_SEMESTER_BAYAR='{$semester}' AND PEM.ID_STATUS_PEMBAYARAN=1
                GROUP BY PS.ID_FAKULTAS
                ORDER BY PS.ID_FAKULTAS
            ) PEM ON PEM.ID_FAKULTAS = F.ID_FAKULTAS
            ORDER BY F.ID_FAKULTAS";
        }
        return $this->db->QueryToArray($query);
    }

    function load_detail_pendapatan_terima_muka_spp_mala($fakultas, $prodi, $semester) {
        $data_detail_pendapatan = array();
        if ($prodi != '') {
            $q_arr_mahasiswa = "
                SELECT M.ID_MHS,M.NIM_MHS,P.NM_PENGGUNA,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,SP.NM_STATUS_PENGGUNA
                FROM MAHASISWA M
                JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
                JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
                JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
                WHERE M.ID_MHS IN (
                    SELECT DISTINCT(ID_MHS) ID_MHS FROM PEMBAYARAN PEM
                    JOIN (
                        SELECT ID_SEMESTER,(TAHUN_AJARAN||' '||NM_SEMESTER) SEMESTER
                        FROM SEMESTER
                    ) S ON S.ID_SEMESTER = PEM.ID_SEMESTER
                    JOIN (
                        SELECT ID_SEMESTER,(TAHUN_AJARAN||' '||NM_SEMESTER) SEMESTER
                        FROM SEMESTER
                    ) SB ON SB.ID_SEMESTER = PEM.ID_SEMESTER_BAYAR
                    WHERE PEM.ID_SEMESTER_BAYAR='{$semester}' AND ID_STATUS_PEMBAYARAN=1
                    AND S.SEMESTER>SB.SEMESTER
                ) AND PS.ID_PROGRAM_STUDI='{$prodi}'
                ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI,M.NIM_MHS";
            foreach ($this->db->QueryToArray($q_arr_mahasiswa) as $data) {
                array_push($data_detail_pendapatan, array(
                    'NM_PENGGUNA' => $data['NM_PENGGUNA'],
                    'NIM_MHS' => $data['NIM_MHS'],
                    'PRODI' => $data['NM_JENJANG'] . ' ' . $data['NM_PROGRAM_STUDI'],
                    'STATUS' => $data['NM_STATUS_PENGGUNA'],
                    'DATA_PEMBAYARAN' => $this->db->QueryToArray("
                        SELECT B.ID_BIAYA,B.NM_BIAYA,PEM.BESAR_BIAYA FROM AUCC.BIAYA B
                        LEFT JOIN (
                            SELECT DB.ID_BIAYA,SUM(PEM.BESAR_BIAYA+PEM.DENDA_BIAYA) BESAR_BIAYA FROM AUCC.PEMBAYARAN PEM
                            JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
                            JOIN AUCC.MAHASISWA M ON PEM.ID_MHS = M.ID_MHS
                            JOIN (
                                SELECT ID_SEMESTER,(TAHUN_AJARAN||' '||NM_SEMESTER) SEMESTER
                                FROM SEMESTER
                            ) S ON S.ID_SEMESTER = PEM.ID_SEMESTER
                            JOIN (
                                SELECT ID_SEMESTER,(TAHUN_AJARAN||' '||NM_SEMESTER) SEMESTER
                                FROM SEMESTER
                            ) SB ON SB.ID_SEMESTER = PEM.ID_SEMESTER_BAYAR
                            WHERE M.ID_MHS='{$data['ID_MHS']}' AND PEM.ID_SEMESTER_BAYAR='{$semester}' AND PEM.ID_STATUS_PEMBAYARAN=1 
                            AND S.SEMESTER>SB.SEMESTER
                            GROUP BY ID_BIAYA
                        ) PEM ON PEM.ID_BIAYA = B.ID_BIAYA
                        ORDER BY B.NM_BIAYA")
                ));
            }
        } else if ($fakultas != '') {
            $q_arr_prodi = "
                SELECT PS.ID_PROGRAM_STUDI,('('||J.NM_JENJANG||') '||PS.NM_PROGRAM_STUDI) NM_PROGRAM_STUDI FROM PROGRAM_STUDI PS
                JOIN JENJANG J ON J.ID_JENJANG= PS.ID_JENJANG WHERE ID_FAKULTAS='{$fakultas}'";
            foreach ($this->db->QueryToArray($q_arr_prodi) as $data) {
                array_push($data_detail_pendapatan, array(
                    'ID_PROGRAM_STUDI' => $data['ID_PROGRAM_STUDI'],
                    'NM_PROGRAM_STUDI' => $data['NM_PROGRAM_STUDI'],
                    'DATA_PEMBAYARAN' => $this->db->QueryToArray("
                        SELECT B.ID_BIAYA,B.NM_BIAYA,PEM.BESAR_BIAYA,PEM.JUMLAH_MHS FROM BIAYA B
                        LEFT JOIN (
                            SELECT DB.ID_BIAYA,SUM(PEM.BESAR_BIAYA+PEM.DENDA_BIAYA) BESAR_BIAYA,COUNT(DISTINCT(M.ID_MHS)) JUMLAH_MHS FROM PEMBAYARAN PEM
                            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
                            JOIN MAHASISWA M ON PEM.ID_MHS = M.ID_MHS
                            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                            JOIN (
                                SELECT ID_SEMESTER,(TAHUN_AJARAN||' '||NM_SEMESTER) SEMESTER
                                FROM SEMESTER
                            ) S ON S.ID_SEMESTER = PEM.ID_SEMESTER
                            JOIN (
                                SELECT ID_SEMESTER,(TAHUN_AJARAN||' '||NM_SEMESTER) SEMESTER
                                FROM SEMESTER
                            ) SB ON SB.ID_SEMESTER = PEM.ID_SEMESTER_BAYAR
                            WHERE PS.ID_PROGRAM_STUDI='{$data['ID_PROGRAM_STUDI']}'  AND PEM.ID_SEMESTER_BAYAR='{$semester}' 
                            AND PEM.ID_STATUS_PEMBAYARAN=1 AND S.SEMESTER>SB.SEMESTER
                        GROUP BY ID_BIAYA
                        ) PEM ON PEM.ID_BIAYA = B.ID_BIAYA
                        ORDER BY B.NM_BIAYA")
                ));
            }
        } else {
            $q_arr_fakultas = "SELECT * FROM FAKULTAS";
            foreach ($this->db->QueryToArray($q_arr_fakultas) as $data) {
                array_push($data_detail_pendapatan, array(
                    'ID_FAKULTAS' => $data['ID_FAKULTAS'],
                    'NM_FAKULTAS' => $data['NM_FAKULTAS'],
                    'DATA_PEMBAYARAN' => $this->db->QueryToArray("
                        SELECT B.ID_BIAYA,B.NM_BIAYA,PEM.BESAR_BIAYA,PEM.JUMLAH_MHS FROM BIAYA B
                        LEFT JOIN (
                            SELECT DB.ID_BIAYA,SUM(PEM.BESAR_BIAYA+PEM.DENDA_BIAYA) BESAR_BIAYA,COUNT(DISTINCT(M.ID_MHS)) JUMLAH_MHS FROM PEMBAYARAN PEM
                            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
                            JOIN MAHASISWA M ON PEM.ID_MHS = M.ID_MHS
                            JOIN (
                                SELECT ID_SEMESTER,(TAHUN_AJARAN||' '||NM_SEMESTER) SEMESTER
                                FROM SEMESTER
                            ) S ON S.ID_SEMESTER = PEM.ID_SEMESTER
                            JOIN (
                                SELECT ID_SEMESTER,(TAHUN_AJARAN||' '||NM_SEMESTER) SEMESTER
                                FROM SEMESTER
                            ) SB ON SB.ID_SEMESTER = PEM.ID_SEMESTER_BAYAR
                            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                            WHERE PS.ID_FAKULTAS='{$data['ID_FAKULTAS']}' AND PEM.ID_SEMESTER_BAYAR='{$semester}' 
                            AND PEM.ID_STATUS_PEMBAYARAN=1 AND S.SEMESTER>SB.SEMESTER                            
                        GROUP BY ID_BIAYA
                        ) PEM ON PEM.ID_BIAYA = B.ID_BIAYA
                        ORDER BY B.NM_BIAYA")
                ));
            }
        }
        return $data_detail_pendapatan;
    }

    function load_pendapatan_terima_muka_spp_maba($fakultas, $semester) {
        if ($fakultas != '') {
            $query = "
            SELECT PS.ID_PROGRAM_STUDI,('('||J.NM_JENJANG||') '||PS.NM_PROGRAM_STUDI) NM_PROGRAM_STUDI,PEM.PENDAPATAN_TERIMA_MUKA
            FROM PROGRAM_STUDI PS
            JOIN JENJANG J ON J.ID_JENJANG= PS.ID_JENJANG
            LEFT JOIN (
                SELECT PS.ID_PROGRAM_STUDI,SUM(
                CASE
                    WHEN S.SEMESTER>SB.SEMESTER
                    THEN PC.BESAR_BIAYA
                    ELSE 0
                END) PENDAPATAN_TERIMA_MUKA
                FROM CALON_MAHASISWA_BARU CMB
                JOIN PEMBAYARAN_CMHS PC ON CMB.ID_C_MHS = PC.ID_C_MHS
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI= CMB.ID_PROGRAM_STUDI
                JOIN (
                    SELECT ID_SEMESTER,(TAHUN_AJARAN||' '||NM_SEMESTER) SEMESTER
                    FROM SEMESTER 
                ) SB ON SB.ID_SEMESTER = PC.ID_SEMESTER_BAYAR
                JOIN (
                    SELECT ID_SEMESTER,(TAHUN_AJARAN||' '||NM_SEMESTER) SEMESTER
                    FROM SEMESTER 
                ) S ON S.ID_SEMESTER = PC.ID_SEMESTER
                WHERE PC.TGL_BAYAR IS NOT NULL AND PC.ID_BANK IS NOT NULL AND PC.ID_SEMESTER_BAYAR='{$semester}' AND PS.ID_FAKULTAS='{$fakultas}'
                GROUP BY PS.ID_PROGRAM_STUDI
                ORDER BY PS.ID_PROGRAM_STUDI
            ) PEM ON PEM.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            WHERE PS.ID_FAKULTAS='{$fakultas}'
            ORDER BY NM_PROGRAM_STUDI";
        } else {
            $query = "
            SELECT F.ID_FAKULTAS,F.NM_FAKULTAS,PEM.PENDAPATAN_TERIMA_MUKA
            FROM FAKULTAS F 
            LEFT JOIN (
                SELECT PS.ID_FAKULTAS,SUM(
                CASE
                    WHEN S.SEMESTER>SB.SEMESTER
                    THEN PC.BESAR_BIAYA
                    ELSE 0
                END) PENDAPATAN_TERIMA_MUKA
                FROM CALON_MAHASISWA_BARU CMB
                JOIN PEMBAYARAN_CMHS PC ON CMB.ID_C_MHS = PC.ID_C_MHS
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI= CMB.ID_PROGRAM_STUDI
                JOIN (
                    SELECT ID_SEMESTER,(TAHUN_AJARAN||' '||NM_SEMESTER) SEMESTER
                    FROM SEMESTER 
                ) SB ON SB.ID_SEMESTER = PC.ID_SEMESTER_BAYAR
                JOIN (
                    SELECT ID_SEMESTER,(TAHUN_AJARAN||' '||NM_SEMESTER) SEMESTER
                    FROM SEMESTER 
                ) S ON S.ID_SEMESTER = PC.ID_SEMESTER
                WHERE PC.TGL_BAYAR IS NOT NULL AND PC.ID_BANK IS NOT NULL AND PC.ID_SEMESTER_BAYAR='{$semester}'
                GROUP BY PS.ID_FAKULTAS
                ORDER BY PS.ID_FAKULTAS
            ) PEM ON PEM.ID_FAKULTAS = F.ID_FAKULTAS
            ORDER BY F.ID_FAKULTAS";
        }
        return $this->db->QueryToArray($query);
    }

    function load_detail_pendapatan_terima_muka_spp_maba($fakultas, $prodi, $semester) {
        $data_detail_pendapatan = array();
        if ($prodi != '') {
            $q_arr_mahasiswa = "
                SELECT CMB.ID_C_MHS,CMB.NO_UJIAN,CMB.NM_C_MHS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI
                FROM CALON_MAHASISWA_BARU CMB
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
                WHERE CMB.ID_C_MHS IN (
                    SELECT DISTINCT(ID_C_MHS) ID_C_MHS
                    FROM PEMBAYARAN_CMHS PEM
                    JOIN (
                        SELECT ID_SEMESTER,(TAHUN_AJARAN||' '||NM_SEMESTER) SEMESTER
                        FROM SEMESTER
                    ) S ON S.ID_SEMESTER = PEM.ID_SEMESTER
                    JOIN (
                        SELECT ID_SEMESTER,(TAHUN_AJARAN||' '||NM_SEMESTER) SEMESTER
                        FROM SEMESTER
                    ) SB ON SB.ID_SEMESTER = PEM.ID_SEMESTER_BAYAR
                    WHERE S.SEMESTER>SB.SEMESTER AND PEM.ID_SEMESTER_BAYAR='{$semester}' 
                    AND PEM.TGL_BAYAR IS NOT NULL AND PEM.ID_BANK IS NOT NULL
                ) AND PS.ID_PROGRAM_STUDI='{$prodi}'
                ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI,CMB.NO_UJIAN";
            foreach ($this->db->QueryToArray($q_arr_mahasiswa) as $data) {
                array_push($data_detail_pendapatan, array(
                    'NM_PENGGUNA' => $data['NM_C_MHS'],
                    'NO_UJIAN' => $data['NO_UJIAN'],
                    'PRODI' => $data['NM_JENJANG'] . ' ' . $data['NM_PROGRAM_STUDI'],
                    'DATA_PEMBAYARAN' => $this->db->QueryToArray("
                        SELECT B.ID_BIAYA,B.NM_BIAYA,PEM.BESAR_BIAYA FROM AUCC.BIAYA B
                        LEFT JOIN (
                            SELECT DB.ID_BIAYA,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA FROM AUCC.PEMBAYARAN_CMHS PEM
                            JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
                            JOIN AUCC.CALON_MAHASISWA_BARU CMB ON PEM.ID_C_MHS = CMB.ID_C_MHS
                            JOIN (
                                SELECT ID_SEMESTER,(TAHUN_AJARAN||' '||NM_SEMESTER) SEMESTER
                                FROM SEMESTER
                            ) S ON S.ID_SEMESTER = PEM.ID_SEMESTER
                            JOIN (
                                SELECT ID_SEMESTER,(TAHUN_AJARAN||' '||NM_SEMESTER) SEMESTER
                                FROM SEMESTER
                            ) SB ON SB.ID_SEMESTER = PEM.ID_SEMESTER_BAYAR                    
                            WHERE CMB.ID_C_MHS='{$data['ID_C_MHS']}' AND PEM.ID_SEMESTER_BAYAR='{$semester}' AND S.SEMESTER>SB.SEMESTER
                             AND PEM.TGL_BAYAR IS NOT NULL AND PEM.ID_BANK IS NOT NULL
                            GROUP BY ID_BIAYA
                        ) PEM ON PEM.ID_BIAYA = B.ID_BIAYA
                        ORDER BY B.NM_BIAYA")
                ));
            }
        } else if ($fakultas != '') {
            $q_arr_prodi = "
                SELECT PS.ID_PROGRAM_STUDI,('('||J.NM_JENJANG||') '||PS.NM_PROGRAM_STUDI) NM_PROGRAM_STUDI FROM PROGRAM_STUDI PS
                JOIN JENJANG J ON J.ID_JENJANG= PS.ID_JENJANG WHERE ID_FAKULTAS='{$fakultas}'";
            foreach ($this->db->QueryToArray($q_arr_prodi) as $data) {
                array_push($data_detail_pendapatan, array(
                    'ID_PROGRAM_STUDI' => $data['ID_PROGRAM_STUDI'],
                    'NM_PROGRAM_STUDI' => $data['NM_PROGRAM_STUDI'],
                    'DATA_PEMBAYARAN' => $this->db->QueryToArray("
                        SELECT B.ID_BIAYA,B.NM_BIAYA,PEM.BESAR_BIAYA,PEM.JUMLAH_MHS FROM BIAYA B
                        LEFT JOIN (
                            SELECT DB.ID_BIAYA,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA,COUNT(DISTINCT(CMB.ID_C_MHS)) JUMLAH_MHS FROM PEMBAYARAN_CMHS PEM
                            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
                            JOIN CALON_MAHASISWA_BARU CMB ON PEM.ID_C_MHS = CMB.ID_C_MHS
                            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CMB.ID_PROGRAM_STUDI
                            JOIN (
                                SELECT ID_SEMESTER,(TAHUN_AJARAN||' '||NM_SEMESTER) SEMESTER
                                FROM SEMESTER
                            ) S ON S.ID_SEMESTER = PEM.ID_SEMESTER
                            JOIN (
                                SELECT ID_SEMESTER,(TAHUN_AJARAN||' '||NM_SEMESTER) SEMESTER
                                FROM SEMESTER
                            ) SB ON SB.ID_SEMESTER = PEM.ID_SEMESTER_BAYAR                    
                            WHERE PS.ID_PROGRAM_STUDI='{$data['ID_PROGRAM_STUDI']}'  AND PEM.ID_SEMESTER_BAYAR='{$semester}' AND S.SEMESTER>SB.SEMESTER
                             AND PEM.TGL_BAYAR IS NOT NULL AND PEM.ID_BANK IS NOT NULL
                        GROUP BY ID_BIAYA
                        ) PEM ON PEM.ID_BIAYA = B.ID_BIAYA
                        ORDER BY B.NM_BIAYA")
                ));
            }
        } else {
            $q_arr_fakultas = "SELECT * FROM FAKULTAS";
            foreach ($this->db->QueryToArray($q_arr_fakultas) as $data) {
                array_push($data_detail_pendapatan, array(
                    'ID_FAKULTAS' => $data['ID_FAKULTAS'],
                    'NM_FAKULTAS' => $data['NM_FAKULTAS'],
                    'DATA_PEMBAYARAN' => $this->db->QueryToArray("
                        SELECT B.ID_BIAYA,B.NM_BIAYA,PEM.BESAR_BIAYA,PEM.JUMLAH_MHS FROM BIAYA B
                        LEFT JOIN (
                            SELECT DB.ID_BIAYA,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA,COUNT(DISTINCT(CMB.ID_C_MHS)) JUMLAH_MHS FROM PEMBAYARAN_CMHS PEM
                            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
                            JOIN CALON_MAHASISWA_BARU CMB ON PEM.ID_C_MHS = CMB.ID_C_MHS
                            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CMB.ID_PROGRAM_STUDI
                            JOIN (
                                SELECT ID_SEMESTER,(TAHUN_AJARAN||' '||NM_SEMESTER) SEMESTER
                                FROM SEMESTER
                            ) S ON S.ID_SEMESTER = PEM.ID_SEMESTER
                            JOIN (
                                SELECT ID_SEMESTER,(TAHUN_AJARAN||' '||NM_SEMESTER) SEMESTER
                                FROM SEMESTER
                            ) SB ON SB.ID_SEMESTER = PEM.ID_SEMESTER_BAYAR                    
                            WHERE PS.ID_FAKULTAS='{$data['ID_FAKULTAS']}' AND PEM.ID_SEMESTER_BAYAR='{$semester}' AND S.SEMESTER>SB.SEMESTER
                             AND PEM.TGL_BAYAR IS NOT NULL AND PEM.ID_BANK IS NOT NULL
                        GROUP BY ID_BIAYA
                        ) PEM ON PEM.ID_BIAYA = B.ID_BIAYA
                        ORDER BY B.NM_BIAYA")
                ));
            }
        }
        return $data_detail_pendapatan;
    }

    //Fungsi Pendapatan KKN
    function load_data_pendapatan_kkn_mode_fakultas($semester) {
        return $this->db->QueryToArray("SELECT F.ID_FAKULTAS,F.NM_FAKULTAS,COUNT(M.ID_MHS) JUMLAH_MHS,(COUNT(M.ID_MHS)*450000) AS TARIF,
        (
            CASE
            WHEN SUM(A.BESAR_BIAYA) IS NOT NULL
            THEN SUM(A.BESAR_BIAYA)
            ELSE 0
            END) AS ANGSURAN
        FROM AUCC.PENGAMBILAN_MK PMK
        JOIN AUCC.MAHASISWA M ON M.ID_MHS=PMK.ID_MHS
        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
        JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        JOIN FAKULTAS F ON PS.ID_FAKULTAS = F.ID_FAKULTAS
        JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
        JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK=PMK.ID_KELAS_MK
        JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK=KMK.ID_KURIKULUM_MK
        LEFT JOIN (
            SELECT PEM.ID_MHS,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA FROM PEMBAYARAN PEM
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PEM.ID_DETAIL_BIAYA
            WHERE DB.ID_BIAYA=114 AND PEM.ID_STATUS_PEMBAYARAN=1
            GROUP BY PEM.ID_MHS
        ) A ON A.ID_MHS = M.ID_MHS
        WHERE  PMK.ID_SEMESTER='{$semester}' AND KUMK.STATUS_MKTA=2
        GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS
        ORDER BY F.ID_FAKULTAS,F.NM_FAKULTAS");
    }

    function load_data_pendapatan_kkn_mode_prodi($semester, $fakultas) {
        return $this->db->QueryToArray("
        SELECT PS.NM_PROGRAM_STUDI,J.NM_JENJANG,COUNT(M.ID_MHS) JUMLAH_MHS,(COUNT(M.ID_MHS)*450000) AS TARIF,
        (
            CASE
            WHEN SUM(A.BESAR_BIAYA) IS NOT NULL
            THEN SUM(A.BESAR_BIAYA)
            ELSE 0
            END) AS ANGSURAN
        FROM AUCC.PENGAMBILAN_MK PMK
        JOIN AUCC.MAHASISWA M ON M.ID_MHS=PMK.ID_MHS
        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
        JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
        JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK=PMK.ID_KELAS_MK
        JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK=KMK.ID_KURIKULUM_MK
        LEFT JOIN (
            SELECT PEM.ID_MHS,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA FROM PEMBAYARAN PEM
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PEM.ID_DETAIL_BIAYA
            WHERE DB.ID_BIAYA=114 AND PEM.ID_STATUS_PEMBAYARAN=1
            GROUP BY PEM.ID_MHS
        ) A ON A.ID_MHS = M.ID_MHS
        WHERE  PMK.ID_SEMESTER='{$semester}' AND KUMK.STATUS_MKTA=2 AND PS.ID_FAKULTAS='{$fakultas}'
        GROUP BY PS.NM_PROGRAM_STUDI,J.NM_JENJANG
        ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI");
    }

    function load_data_pendapatan_kkn_mode_detail($semester, $fakultas, $prodi) {
        $q_fakultas = $fakultas != '' ? "AND F.ID_FAKULTAS='{$fakultas}'" : "";
        $q_prodi = $prodi != '' ? "AND PS.ID_PROGRAM_STUDI='{$prodi}'" : "";
        return $this->db->QueryToArray("
        SELECT M.ID_MHS,M.NIM_MHS,P.NM_PENGGUNA,M.THN_ANGKATAN_MHS,450000 AS TARIF,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,F.ID_FAKULTAS,F.NM_FAKULTAS, A.BESAR_BIAYA ANGSURAN,(450000-A.BESAR_BIAYA) TAGIHAN
        FROM PENGAMBILAN_MK PMK
        JOIN MAHASISWA M ON M.ID_MHS=PMK.ID_MHS
        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
        JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
        JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
        JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK=PMK.ID_KELAS_MK
        JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK=KMK.ID_KURIKULUM_MK
        LEFT JOIN (
            SELECT PEM.ID_MHS,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA FROM PEMBAYARAN PEM
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PEM.ID_DETAIL_BIAYA
            WHERE DB.ID_BIAYA=114 AND PEM.ID_STATUS_PEMBAYARAN=1
            GROUP BY PEM.ID_MHS
        ) A ON A.ID_MHS = M.ID_MHS
        WHERE  PMK.ID_SEMESTER='{$semester}' AND KUMK.STATUS_MKTA=2 {$q_fakultas} {$q_prodi}
        ORDER BY F.ID_FAKULTAS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,M.NIM_MHS");
    }

}

?>
