<?php

$id_pt = $id_pt_user;

class utility
{

    public $db;
    public $id_jsemua = [2, 1, 4, 3, 5];
    // HARCORE UNTUK JENIS BIAYA YANG DITAGIHKAN
    // BIAYA YANG DIBAYARKAN SATU KALI SAJA
    public $id_jsat = [2];
    // BIAYA YANG DIBAYARKAN TIAP SEMESTER
    public $id_jsem = [1, 4];
    // BIAYA YANG DIBAYARKAN TIAP BULAN
    public $id_jbul = [4];
    // BIAYA YANG DIBAYARKAN TANPA IKATAN WAKTU
    public $id_jfree = [3, 5];

    function __construct($db)
    {
        $this->db = $db;
    }

    function convert_array_to_string_query_in($arr)
    {
        if (count($arr) == 1) {
            if (empty($arr[0])) {
                return implode(",", $this->id_jsemua);
            } else {
                return "{$arr[0]}";
            }
        } else {
            return implode(",", $arr);
        }
    }

    function get_hasil_cari_mahasiswa($var, $id_pt)
    {
        $var_trim = trim($var);
        return $this->db->QueryToArray("
            SELECT P.NM_PENGGUNA,M.NIM_MHS,CMB.NO_UJIAN,F.NM_FAKULTAS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI FROM PENGGUNA P
            JOIN MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA
            LEFT JOIN CALON_MAHASISWA_BARU CMB ON M.NIM_MHS = CMB.NIM_MHS
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            JOIN FAKULTAS F ON PS.ID_FAKULTAS = F.ID_FAKULTAS
            WHERE F.ID_PERGURUAN_TINGGI = {$id_pt} AND 
            (M.NIM_MHS LIKE '%{$var_trim}%' OR UPPER(P.NM_PENGGUNA) LIKE UPPER('%{$var}%') OR CMB.NO_UJIAN LIKE '%{$var_trim}%')
            ");
    }

    function detail_pembayaran($nim)
    {
        return $this->db->QueryToArray("
        SELECT * FROM 
            (SELECT M.ID_MHS AS ID1,M.NIM_LAMA,P.NM_PENGGUNA,NM_PROGRAM_STUDI,UPPER(F.NM_FAKULTAS) AS NM_FAKULTAS,M.NIM_MHS,PEM.*,B.NM_BIAYA,J.NM_JENJANG,M.STATUS_CEKAL,JAL.NM_JALUR,S.THN_AKADEMIK_SEMESTER
            ,KB.NM_KELOMPOK_BIAYA,M.NIM_BANK,BNK.NM_BANK,BNK.ID_BANK AS ID_BNK,BNKV.ID_BANK_VIA AS ID_BNKV,BNKV.NAMA_BANK_VIA,S2.NM_SEMESTER,S2.TAHUN_AJARAN
            FROM PENGGUNA P
            LEFT JOIN MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA
            LEFT JOIN PROGRAM_STUDI PR ON M.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI
            LEFT JOIN JENJANG J ON J.ID_JENJANG = PR.ID_JENJANG
            LEFT JOIN (SELECT * FROM JALUR_MAHASISWA WHERE ROWNUM<=1 ORDER BY ID_JALUR_MAHASISWA DESC) JM ON JM.ID_MHS=M.ID_MHS
            LEFT JOIN JALUR JAL ON JAL.ID_JALUR = JM.ID_JALUR
            LEFT JOIN SEMESTER S ON S.ID_SEMESTER = JM.ID_SEMESTER
            LEFT JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = M.ID_KELOMPOK_BIAYA
            LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PR.ID_FAKULTAS
            LEFT JOIN PEMBAYARAN PEM ON M.ID_MHS = PEM.ID_MHS
            LEFT JOIN SEMESTER S2 ON S2.ID_SEMESTER = PEM.ID_SEMESTER
            LEFT JOIN BANK BNK ON BNK.ID_BANK = PEM.ID_BANK
            LEFT JOIN BANK_VIA BNKV ON BNKV.ID_BANK_VIA = PEM.ID_BANK_VIA
            LEFT JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
            LEFT JOIN BIAYA B ON DB.ID_BIAYA = B.ID_BIAYA
            ) X1
        LEFT JOIN
            (SELECT M.ID_MHS AS ID2,SUM(PEM.BESAR_BIAYA) AS TOTAL_SPP_TAGIH,SUM(PEM.DENDA_BIAYA) AS TOTAL_DENDA_BIAYA FROM PEMBAYARAN PEM
            LEFT JOIN MAHASISWA M ON PEM.ID_MHS = M.ID_MHS
            WHERE PEM.TGL_BAYAR IS NULL AND PEM.IS_TAGIH ='Y'
            GROUP BY M.ID_MHS
            ) X2 
        ON X1.ID1 = X2.ID2
        LEFT JOIN
            (SELECT M.ID_MHS AS ID2,SUM(PEM.BESAR_BIAYA) AS TOTAL_SPP,SUM(PEM.DENDA_BIAYA) AS TOTAL_DENDA FROM PEMBAYARAN PEM
            LEFT JOIN MAHASISWA M ON PEM.ID_MHS = M.ID_MHS
            GROUP BY M.ID_MHS
            ) X3 
        ON X1.ID1 = X3.ID2
        WHERE X1.NIM_MHS ='{$nim}'
        ORDER BY X1.NM_SEMESTER DESC,X1.TAHUN_AJARAN DESC,X1.ID_PEMBAYARAN");
    }

    function detail_pembayaran_cmhs($no_ujian)
    {
        $calon_mahasiswa = $this->db->QueryToArray("SELECT * FROM CALON_MAHASISWA WHERE NO_UJIAN ='{$no_ujian}'") == NULL ? "_BARU" : "";
        return $this->db->QueryToArray("
        SELECT * FROM 
            (SELECT CM.ID_C_MHS AS ID1,CM.NM_C_MHS,CM.NO_UJIAN,J.NM_JENJANG,PR.NM_PROGRAM_STUDI,UPPER(F.NM_FAKULTAS) AS NM_FAKULTAS,PEM.*,B.NM_BIAYA,
            BNK.NM_BANK,BNK.ID_BANK AS ID_BNK,BNKV.ID_BANK_VIA AS ID_BNKV,BNKV.NAMA_BANK_VIA
            FROM CALON_MAHASISWA{$calon_mahasiswa} CM
            LEFT JOIN PROGRAM_STUDI PR ON CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI
            LEFT JOIN JENJANG J ON J.ID_JENJANG = PR.ID_JENJANG
            LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PR.ID_FAKULTAS
            LEFT JOIN PEMBAYARAN_CMHS PEM ON CM.ID_C_MHS = PEM.ID_C_MHS
            LEFT JOIN SEMESTER S2 ON S2.ID_SEMESTER = PEM.ID_SEMESTER
            LEFT JOIN BANK BNK ON BNK.ID_BANK = PEM.ID_BANK
            LEFT JOIN BANK_VIA BNKV ON BNKV.ID_BANK_VIA = PEM.ID_BANK_VIA
            LEFT JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
            LEFT JOIN BIAYA B ON DB.ID_BIAYA = B.ID_BIAYA
            ) X1
        LEFT JOIN
            (SELECT CM.ID_C_MHS AS ID2,SUM(PEM.BESAR_BIAYA) AS TOTAL_BIAYA_TAGIH
            FROM PEMBAYARAN_CMHS PEM
            LEFT JOIN CALON_MAHASISWA{$calon_mahasiswa} CM ON PEM.ID_C_MHS = CM.ID_C_MHS
            WHERE PEM.TGL_BAYAR IS NULL
            GROUP BY CM.ID_C_MHS
            ) X2 
        ON X1.ID1 = X2.ID2
        LEFT JOIN
            (SELECT CM.ID_C_MHS AS ID2,SUM(PEM.BESAR_BIAYA) AS TOTAL_BIAYA
            FROM PEMBAYARAN_CMHS PEM
            LEFT JOIN CALON_MAHASISWA{$calon_mahasiswa} CM ON PEM.ID_C_MHS = CM.ID_C_MHS
            GROUP BY CM.ID_C_MHS
            ) X3
        ON X1.ID1 = X3.ID2
        WHERE X1.NO_UJIAN ='{$no_ujian}'
        ORDER BY X1.ID_PEMBAYARAN_CMHS");
    }

    function get_detail_pembayaran_mahasiswa($nim)
    {
        return $this->db->QueryToArray("SELECT B.NM_BIAYA,P.* FROM PEMBAYARAN P
        JOIN DETAIL_BIAYA DB ON P.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
        JOIN BIAYA B ON DB.ID_BIAYA = B.ID_BIAYA
        JOIN MAHASISWA M ON P.ID_MHS = M.ID_MHS
        WHERE NIM_MHS ='{$nim}'
        ");
    }

    function get_detail_pembayaran($id_pembayaran)
    {
        $this->db->Query("SELECT B.NM_BIAYA,P.* FROM PEMBAYARAN P
        JOIN DETAIL_BIAYA DB ON P.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
        JOIN BIAYA B ON DB.ID_BIAYA = B.ID_BIAYA
        JOIN MAHASISWA M ON P.ID_MHS = M.ID_MHS
        WHERE P.ID_PEMBAYARAN ='{$id_pembayaran}'
        ");

        return $this->db->FetchAssoc();
    }

    function get_detail_pengguna($username)
    {
        $this->db->Query("
            SELECT * FROM PENGGUNA P
            LEFT JOIN ROLE R ON R.ID_ROLE = P.ID_ROLE
            WHERE P.USERNAME='{$username}'
        ");

        return $this->db->FetchAssoc();
    }

    function reset_password_pengguna($username, $pass)
    {
        $this->db->Query("UPDATE PENGGUNA SET SE1 = '{$pass}' WHERE USERNAME='{$username}'");
    }

    function cek_data($nim)
    {
        $this->db->Query("
        SELECT M.NIM_MHS,P.NM_PENGGUNA,J.ID_JENJANG,J.NM_JENJANG,S.ID_SEMESTER,S.THN_AKADEMIK_SEMESTER,PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.ID_JALUR,J.NM_JALUR,
        KB.ID_KELOMPOK_BIAYA,KB.NM_KELOMPOK_BIAYA 
            FROM PENGGUNA P
            LEFT JOIN MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA 
            LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            LEFT JOIN JENJANG J ON PS.ID_JENJANG = J.ID_JENJANG
            LEFT JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = M.ID_KELOMPOK_BIAYA
            LEFT JOIN JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS
            LEFT JOIN SEMESTER S ON JM.ID_SEMESTER = S.ID_SEMESTER 
            LEFT JOIN JALUR J ON JM.ID_JALUR = J.ID_JALUR
        WHERE M.NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function cek_pengguna($nim)
    {
        $this->db->Query("SELECT * FROM PENGGUNA WHERE USERNAME='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function cek_mahasiswa($nim)
    {
        $this->db->Query("SELECT * FROM MAHASISWA WHERE NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function cek_jalur($nim)
    {
        $this->db->Query("SELECT * FROM JALUR_MAHASISWA WHERE NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function insert_pengguna_mahasiswa($nama, $nim)
    {
        $this->db->Query("INSERT INTO PENGGUNA 
                (USERNAME,PASSWORD_PENGGUNA,NM_PENGGUNA,ID_ROLE,JOIN_TABLE) 
                VALUES 
                ('{$nim}','{$nim}','{$nama}',3,3)");
    }

    function insert_mahasiswa($id_pengguna, $nim, $program_studi, $kelompok_biaya)
    {
        $nim_bank = substr($nim, 0, 9);
        $this->db->Query("INSERT INTO MAHASISWA 
                (ID_PENGGUNA,NIM_MHS,ID_PROGRAM_STUDI,ID_KELOMPOK_BIAYA,NIM_BANK,STATUS_CEKAL) 
                VALUES 
                ('{$id_pengguna}','{$nim}','{$program_studi}','{$kelompok_biaya}','{$nim_bank}','0')");
    }

    function insert_jalur_mahasiswa($id_mhs, $program_studi, $jalur, $semester, $nim_mhs)
    {
        $this->db->Query("INSERT INTO JALUR_MAHASISWA 
                (ID_MHS,ID_PROGRAM_STUDI,ID_JALUR,ID_SEMESTER,NIM_MHS)
                VALUES
                ('{$id_mhs}','{$program_studi}','{$jalur}','{$semester}','{$nim_mhs}')");
    }

    function update_pengguna($nama, $nim)
    {
        $this->db->Query("UPDATE PENGGUNA SET NM_PENGGUNA ='{$nama}' WHERE USERNAME='{$nim}'");
    }

    function update_mahasiswa($id_kelompok_biaya, $nim, $id_program_studi)
    {
        $nim_bank = substr($nim, 0, 9);
        $this->db->Query("UPDATE MAHASISWA SET ID_KELOMPOK_BIAYA='{$id_kelompok_biaya}',NIM_BANK='{$nim_bank}',ID_PROGRAM_STUDI='{$id_program_studi}',STATUS_CEKAL=0 WHERE NIM_MHS='{$nim}'");
    }

    function update_jalur_mahasiswa($id_mhs, $program_studi, $jalur, $semester, $nim)
    {
        $this->db->Query("UPDATE JALUR_MAHASISWA SET ID_MHS = '{$id_mhs}', ID_PROGRAM_STUDI='{$program_studi}',ID_JALUR='{$jalur}',ID_SEMESTER='{$semester}' WHERE NIM_MHS='{$nim}'");
    }

    function get_jenjang()
    {
        return $this->db->QueryToArray("SELECT * FROM JENJANG ORDER BY NM_JENJANG,ID_JENJANG");
    }

    function get_semester()
    {
        return $this->db->QueryToArray("SELECT * FROM SEMESTER WHERE NM_SEMESTER = 'Ganjil' AND ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "' ORDER BY ID_SEMESTER");
    }

    function get_program_studi()
    {
        return $this->db->QueryToArray("SELECT * FROM PROGRAM_STUDI ORDER BY NM_PROGRAM_STUDI,ID_PROGRAM_STUDI");
    }

    function get_program_studi_by_jenjang($id_jenjang)
    {
        return $this->db->QueryToArray("SELECT * FROM PROGRAM_STUDI WHERE ID_JENJANG={$id_jenjang} ORDER BY NM_PROGRAM_STUDI,ID_PROGRAM_STUDI");
    }

    function get_jalur()
    {
        return $this->db->QueryToArray("SELECT * FROM JALUR ORDER BY NM_JALUR,ID_JALUR");
    }

    function get_kelompok()
    {
        return $this->db->QueryToArray("SELECT * FROM KELOMPOK_BIAYA ORDER BY NM_KELOMPOK_BIAYA,ID_KELOMPOK_BIAYA");
    }

    function get_bank()
    {
        return $this->db->QueryToArray("SELECT * FROM BANK ORDER BY NM_BANK,ID_BANK");
    }

    function get_via_bank()
    {
        return $this->db->QueryToArray("SELECT * FROM BANK_VIA ORDER BY NAMA_BANK_VIA,ID_BANK_VIA");
    }

    function get_biaya_kuliah_mahasiswa()
    {
        return $this->db->QueryToArray("
            SELECT S.THN_AKADEMIK_SEMESTER,S.NM_SEMESTER,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JAL.NM_JALUR,KB.NM_KELOMPOK_BIAYA,BK.BESAR_BIAYA_KULIAH FROM BIAYA_KULIAH BK
            JOIN SEMESTER S ON BK.ID_SEMESTER = S.ID_SEMESTER
            JOIN PROGRAM_STUDI PS ON BK.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG = BK.ID_JENJANG
            JOIN JALUR JAL ON JAL.ID_JALUR = BK.ID_JALUR
            JOIN KELOMPOK_BIAYA KB ON BK.ID_KELOMPOK_BIAYA = KB.ID_KELOMPOK_BIAYA
            ORDER BY PS.NM_PROGRAM_STUDI,JAL.NM_JALUR,S.THN_AKADEMIK_SEMESTER
            ");
    }

    function get_semester_all()
    {
        return $this->db->QueryToArray("SELECT * FROM SEMESTER WHERE (NM_SEMESTER='Ganjil' OR NM_SEMESTER='Genap') AND THN_AKADEMIK_SEMESTER>".(date('Y')-8)." 
            AND ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "' ORDER BY THN_AKADEMIK_SEMESTER DESC,NM_SEMESTER DESC");
    }

    function cek_biaya_kuliah_mhs($nim)
    {
        $this->db->Query("
          SELECT T.NIM_MHS,T.ID_MHS, BK.ID_BIAYA_KULIAH,BK.BESAR_BIAYA_KULIAH FROM BIAYA_KULIAH BK
          RIGHT JOIN (
              SELECT M.NIM_MHS, M.ID_MHS, JM.ID_SEMESTER, M.ID_PROGRAM_STUDI, M.ID_KELOMPOK_BIAYA, JM.ID_JALUR FROM MAHASISWA M
              JOIN JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS
              WHERE JM.ID_SEMESTER IS NOT NULL) T ON T.ID_PROGRAM_STUDI = BK.ID_PROGRAM_STUDI
          WHERE
          BK.ID_KELOMPOK_BIAYA = T.ID_KELOMPOK_BIAYA AND BK.ID_SEMESTER = T.ID_SEMESTER AND BK.ID_JALUR = T.ID_JALUR
          AND T.ID_MHS NOT IN (SELECT ID_MHS FROM BIAYA_KULIAH_MHS) AND T.NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function cek_pembayaran($nim)
    {
        $this->db->Query("
        SELECT M.NIM_MHS,BKM.ID_MHS, 5 AS ID_SEMESTER, DB.ID_DETAIL_BIAYA, DB.BESAR_BIAYA, 'Y' AS IS_TAGIH FROM BIAYA_KULIAH_MHS BKM
            JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH
            LEFT JOIN MAHASISWA M ON M.ID_MHS = BKM.ID_MHS
            WHERE
	M.ID_MHS IN (SELECT ID_MHS FROM BIAYA_KULIAH_MHS) AND
	M.ID_MHS NOT IN (SELECT DISTINCT ID_MHS FROM PEMBAYARAN) AND M.NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function generate_pembayaran($nim)
    {
        $this->db->Query("
        INSERT INTO PEMBAYARAN (ID_MHS, ID_SEMESTER, ID_DETAIL_BIAYA, BESAR_BIAYA, IS_TAGIH)
            SELECT BKM.ID_MHS, 5 AS ID_SEMESTER, DB.ID_DETAIL_BIAYA, DB.BESAR_BIAYA, 'Y' AS IS_TAGIH FROM BIAYA_KULIAH_MHS BKM
            JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH
            LEFT JOIN MAHASISWA M ON M.ID_MHS = BKM.ID_MHS
            WHERE
        M.ID_MHS IN (SELECT ID_MHS FROM BIAYA_KULIAH_MHS) AND
        M.ID_MHS NOT IN (SELECT DISTINCT ID_MHS FROM PEMBAYARAN) AND
        M.NIM_MHS ='{$nim}'");
    }

    function generate_biaya_kuliah($nim)
    {
        $this->db->Query("
        INSERT INTO BIAYA_KULIAH_MHS (ID_MHS, ID_BIAYA_KULIAH)
          SELECT T.ID_MHS, BK.ID_BIAYA_KULIAH FROM BIAYA_KULIAH BK
          RIGHT JOIN (
            SELECT M.NIM_MHS, M.ID_MHS, JM.ID_SEMESTER, M.ID_PROGRAM_STUDI, M.ID_KELOMPOK_BIAYA, JM.ID_JALUR FROM MAHASISWA M
            JOIN JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS
            WHERE JM.ID_SEMESTER IS NOT NULL) T ON T.ID_PROGRAM_STUDI = BK.ID_PROGRAM_STUDI
          WHERE
            BK.ID_KELOMPOK_BIAYA = T.ID_KELOMPOK_BIAYA AND BK.ID_SEMESTER = T.ID_SEMESTER AND BK.ID_JALUR = T.ID_JALUR
            AND T.ID_MHS NOT IN (SELECT ID_MHS FROM BIAYA_KULIAH_MHS) AND
            T.NIM_MHS ='{$nim}'");
    }

    //fungsi sejarah status pembayaran
    function load_sejarah_status_pembayaran($nim, $id_pt)
    {
        return $this->db->QueryToArray("
        SELECT SSP.ID_SEJARAH_STATUS_BAYAR,M.NIM_MHS,P.NM_PENGGUNA NM_MHS,S.NM_SEMESTER,S.TAHUN_AJARAN,SP.NAMA_STATUS,
                SSP.TGL_MULAI_BAYAR,SSP.TGL_JATUH_TEMPO,SSP.KETERANGAN,PU.NM_PENGGUNA NM_PENGUPDATE,SSP.IS_AKTIF
        FROM SEJARAH_STATUS_BAYAR SSP
        JOIN MAHASISWA M ON M.ID_MHS = SSP.ID_MHS
        JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
        JOIN SEMESTER S ON S.ID_SEMESTER = SSP.ID_SEMESTER
        JOIN STATUS_PEMBAYARAN SP ON SP.ID_STATUS_PEMBAYARAN= SSP.ID_STATUS_PEMBAYARAN
        JOIN PENGGUNA PU ON PU.ID_PENGGUNA = SSP.ID_PENGGUNA
        WHERE M.NIM_MHS='{$nim}' AND PU.ID_PERGURUAN_TINGGI = {$id_pt}");
    }

    function insert_sejarah_status_pembayaran($id_mhs, $semester, $status, $tgl_mulai, $tgl_tempo, $keterangan, $id_update, $aktif)
    {
        $this->db->Query("
        INSERT INTO SEJARAH_STATUS_BAYAR
            (ID_MHS,ID_SEMESTER,ID_STATUS_PEMBAYARAN,TGL_MULAI_BAYAR,TGL_JATUH_TEMPO,KETERANGAN,ID_PENGGUNA,IS_AKTIF)
        VALUES
            ('{$id_mhs}','{$semester}','{$status}','{$tgl_mulai}','{$tgl_tempo}','{$keterangan}','{$id_update}','{$aktif}')");
        $this->db->Query("UPDATE PEMBAYARAN SET ID_STATUS_PEMBAYARAN='{$status}',KETERANGAN='{$keterangan}' WHERE ID_SEMESTER='{$semester}' AND ID_MHS='{$id_mhs}'");
    }

    function insert_sejarah_status_pembayaran_sop($id_mhs, $semester, $status, $tgl_mulai, $tgl_tempo, $keterangan, $id_update, $aktif)
    {
        $this->db->Query("
        INSERT INTO SEJARAH_STATUS_BAYAR
            (ID_MHS,ID_SEMESTER,ID_STATUS_PEMBAYARAN,TGL_MULAI_BAYAR,TGL_JATUH_TEMPO,KETERANGAN,ID_PENGGUNA,IS_AKTIF)
        VALUES
            ('{$id_mhs}','{$semester}','{$status}','{$tgl_mulai}','{$tgl_tempo}','{$keterangan}','{$id_update}','{$aktif}')");
        $this->db->Query("
            UPDATE PEMBAYARAN SET ID_STATUS_PEMBAYARAN='{$status}', KETERANGAN='{$keterangan}' 
            WHERE ID_PEMBAYARAN IN (
                SELECT ID_PEMBAYARAN FROM PEMBAYARAN P
                JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=P.ID_DETAIL_BIAYA 
                WHERE P.ID_SEMESTER='{$semester}' AND P.ID_MHS='{$id_mhs}' AND DB.ID_BIAYA=47
            )");
    }

    function aktifkan_sejarah_status_pembayaran($id_sejarah_status)
    {
        $this->db->Query("SELECT * FROM SEJARAH_STATUS_BAYAR WHERE ID_SEJARAH_STATUS_BAYAR='{$id_sejarah_status}'");
        $data = $this->db->FetchAssoc();
        $aktif = $data['IS_AKTIF'] == '1' ? '0' : '1';
        $this->db->Query("UPDATE SEJARAH_STATUS_BAYAR SET IS_AKTIF='{$aktif}' WHERE ID_SEJARAH_STATUS_BAYAR='{$id_sejarah_status}'");
    }

    function non_aktifkan_sejarah_status_bayar($id_mhs)
    {
        $this->db->Query("UPDATE SEJARAH_STATUS_BAYAR SET IS_AKTIF=0 WHERE ID_MHS='{$id_mhs}'");
    }

    function hapus_sejarah_status_pembayaran($id_status)
    {
        $this->db->Query("DELETE FROM SEJARAH_STATUS_BAYAR WHERE ID_SEJARAH_STATUS_BAYAR='{$id_status}'");
    }

    //fungsi pengembalian pembayaran

    function load_history_pengembalian_pembayaran($nim)
    {
        return $this->db->QueryToArray("
            SELECT PP.*,SB.NM_SEMESTER NM_SEMESTER_BAYAR,SB.TAHUN_AJARAN 
            TAHUN_AJARAN_BAYAR,SP.NM_SEMESTER,SP.TAHUN_AJARAN,B.NM_BANK,BV.NAMA_BANK_VIA,
            (SELECT SUM(BESAR_BIAYA) FROM PENGEMBALIAN_PEMBAYARAN_DETAIL 
                WHERE ID_PENGEMBALIAN_PEMBAYARAN=PP.ID_PENGEMBALIAN_PEMBAYARAN
            ) TOTAL_PENGEMBALIAN
            FROM PENGEMBALIAN_PEMBAYARAN PP
            JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS=PP.ID_TAGIHAN_MHS
            JOIN SEMESTER SB ON SB.ID_SEMESTER=TM.ID_SEMESTER
            JOIN SEMESTER SP ON SP.ID_SEMESTER=PP.ID_SEMESTER_PENGEMBALIAN
            JOIN BANK B ON B.ID_BANK = PP.ID_BANK
            JOIN BANK_VIA BV ON BV.ID_BANK_VIA = PP.ID_BANK_VIA
            JOIN MAHASISWA M ON M.ID_MHS = TM.ID_MHS
            WHERE M.NIM_MHS='{$nim}'
            ORDER BY SP.NM_SEMESTER DESC,SP.TAHUN_AJARAN DESC,SB.NM_SEMESTER DESC,SB.TAHUN_AJARAN DESC");
    }

    function insert_pengembalian_pembayaran($nim, $semester, $besar_pengembalian, $tgl_pengembalian, $bank, $bank_via, $no_transaksi, $keterangan)
    {
        $this->db->Query("
        INSERT INTO PENGEMBALIAN_PEMBAYARAN 
            (ID_MHS,ID_SEMESTER,BESAR_PEMBAYARAN,BESAR_PENGEMBALIAN,TGL_PENGEMBALIAN,ID_BANK,ID_BANK_VIA,NO_TRANSAKSI,KETERANGAN,ID_SEMESTER_PENGEMBALIAN)
        SELECT PEM.ID_MHS,PEM.ID_SEMESTER,SUM(PEM.BESAR_BIAYA) BESAR_PEMBAYARAN,'{$besar_pengembalian}' BESAR_PENGEMBALIAN,'{$tgl_pengembalian}' TGL_PENGEMBALIAN, 
        '{$bank}' ID_BANK,'{$bank_via}' ID_BANK_VIA,'{$no_transaksi}' NO_TRANSAKSI,'{$keterangan}'
        KETERANGAN, (SELECT ID_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True') ID_SEMESTER_PENGEMBALIAN
        FROM PEMBAYARAN PEM
        JOIN MAHASISWA M ON M.ID_MHS = PEM.ID_MHS
        WHERE M.NIM_MHS='{$nim}' AND PEM.ID_SEMESTER='{$semester}'
        GROUP BY PEM.ID_MHS,PEM.ID_SEMESTER");
    }

    function update_pengembalian_pembayaran($id_pengembalian, $semester, $besar_pembayaran, $besar_pengembalian, $tgl_pengembalian, $bank, $bank_via, $no_transaksi, $keterangan)
    {
        $this->db->Query("
            UPDATE PENGEMBALIAN_PEMBAYARAN SET
                ID_SEMESTER='{$semester}',
                BESAR_PENGEMBALIAN='{$besar_pengembalian}',
                BESAR_PEMBAYARAN='{$besar_pembayaran}',
                TGL_PENGEMBALIAN='{$tgl_pengembalian}',
                ID_BANK='{$bank}',
                ID_BANK_VIA='{$bank_via}',
                NO_TRANSAKSI='{$no_transaksi}',
                KETERANGAN='{$keterangan}'
            WHERE ID_PENGEMBALIAN_PEMBAYARAN='{$id_pengembalian}'");
    }

    function load_biaya_pengembalian_pembayaran($id_mhs, $semester)
    {
        return $this->db->QueryToArray("
            SELECT B.ID_BIAYA,B.NM_BIAYA
            FROM PEMBAYARAN PEM
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PEM.ID_DETAIL_BIAYA
            JOIN BIAYA B ON DB.ID_BIAYA=B.ID_BIAYA
            WHERE PEM.ID_SEMESTER='{$semester}' AND PEM.ID_MHS='{$id_mhs}' AND PEM.BESAR_BIAYA!=0
            GROUP BY B.ID_BIAYA,B.NM_BIAYA");
    }

    function load_detail_pengembalian($id_pengembalian)
    {
        return $this->db->QueryToArray("
            SELECT PENG.ID_PENGEMBALIAN_DETAIL,B.NM_BIAYA,PENG.BESAR_BIAYA
            FROM PENGEMBALIAN_PEMBAYARAN_DETAIL PENG
            JOIN BIAYA B ON PENG.ID_BIAYA=B.ID_BIAYA
            WHERE PENG.ID_PENGEMBALIAN_PEMBAYARAN='{$id_pengembalian}'");
    }

    function tambah_detail_pengembalian($id_pengembalian, $id_biaya, $besar_biaya)
    {
        $this->db->Query("
            INSERT INTO PENGEMBALIAN_PEMBAYARAN_DETAIL
                (ID_PENGEMBALIAN_PEMBAYARAN,ID_BIAYA,BESAR_BIAYA)
            VALUES
                ('{$id_pengembalian}','{$id_biaya}','{$besar_biaya}')");
    }

    function edit_detail_pengembalian($id_detail, $besar_biaya)
    {
        $this->db->Query("
            UPDATE PENGEMBALIAN_PEMBAYARAN_DETAIL 
            SET
                BESAR_BIAYA='{$besar_biaya}'
            WHERE ID_PENGEMBALIAN_DETAIL='{$id_detail}'");
    }

    //fungsi generate biaya

    function get_semester_kemarin($semester)
    {
        $this->db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER='{$semester}'");
        $semester_aktif = $this->db->FetchAssoc();
        if ($semester_aktif['NM_SEMESTER'] == 'Genap') {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER!='Genap' AND THN_AKADEMIK_SEMESTER='{$semester_aktif['THN_AKADEMIK_SEMESTER']}' AND NM_SEMESTER='Ganjil'");
            $semester_kemarin = $this->db->FetchAssoc();
        } else {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER='Genap' AND THN_AKADEMIK_SEMESTER=('{$semester_aktif['THN_AKADEMIK_SEMESTER']}'-1)");
            $semester_kemarin = $this->db->FetchAssoc();
        }
        return $semester_kemarin['ID_SEMESTER'];
    }

    function load_data_mahasiswa_generate($pt, $fakultas, $prodi, $status, $semester_gen, $angkatan, $tagihan, $p_sebelum)
    {
        $q_prodi = $prodi != '' ? "AND PS.ID_PROGRAM_STUDI ='{$prodi}'" : "";
        $q_angkatan = $angkatan != '' ? "AND M.THN_ANGKATAN_MHS='{$angkatan}'" : "";
        if ($p_sebelum != '') {
            if ($p_sebelum == 1) {
                $q_p_sebelum = " AND PEM_K.PEMBAYARAN_KEMARIN IS NOT NULL AND (PEM_K.STATUS_KEMARIN='Sudah Bayar' OR PEM_K.STATUS_KEMARIN='Penangguhan')";
            } else if ($p_sebelum == 2) {
                $q_p_sebelum = " AND PEM_K.PEMBAYARAN_KEMARIN IS NOT NULL AND (PEM_K.STATUS_KEMARIN='Belum Bayar' OR PEM_K.STATUS_KEMARIN='Pembebasan'";
            } else if ($p_sebelum == 3) {
                $q_p_sebelum = " AND PEM_K.PEMBAYARAN_KEMARIN IS NULL";
            }
        } else {
            $q_p_sebelum = "";
        }
        $semester_kemarin = $this->get_semester_kemarin($semester_gen);
        if ($tagihan == '') {
            $q_tagihan = "";
        } else {
            if ($tagihan != 3) {
                $q_tagihan = "HAVING COUNT(TAG.ID_SEMESTER)={$tagihan}";
            } else {
                $q_tagihan = "HAVING COUNT(TAG.ID_SEMESTER)>2";
            }
        }
        $c_status = count($status);
        if ($c_status > 1) {
            $arr_s = '';
            for ($i = 0; $i < $c_status; $i++) {
                if ($i == ($c_status - 1)) {
                    $arr_s .= $status[$i];
                } else {
                    $arr_s .= $status[$i] . ',';
                }
            }
            $q_status = "AND SP.ID_STATUS_PENGGUNA IN ({$arr_s})";
        } else {
            $q_status = $status[0] != '' ? "AND SP.ID_STATUS_PENGGUNA ='{$status[0]}'" : "";
        }
        $query =
            "SELECT M.ID_MHS,M.NIM_MHS,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,SP.NM_STATUS_PENGGUNA,
                PEM_K.PEMBAYARAN_KEMARIN,PEM_K.STATUS_KEMARIN,JB.GEN_SAT,JB.GEN_SEM,JB.GEN_BULAN,JB.GEN_FREE,PEM_S.PEMBAYARAN_SEKARANG
                ,PEM_S.STATUS_SEKARANG, COUNT(TAG.ID_SEMESTER) TAGIHAN
            FROM MAHASISWA M
            JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
            LEFT JOIN (
                SELECT ID_MHS,SUM(BESAR_BIAYA) PEMBAYARAN_SEKARANG,SP.NAMA_STATUS STATUS_SEKARANG
                FROM TAGIHAN_MHS TM
                JOIN TAGIHAN T ON T.ID_TAGIHAN_MHS=TM.ID_TAGIHAN_MHS
                JOIN STATUS_PEMBAYARAN SP ON SP.ID_STATUS_PEMBAYARAN = T.ID_STATUS_PEMBAYARAN
                WHERE TM.ID_SEMESTER='{$semester_gen}'
                GROUP BY TM.ID_MHS,TM.ID_SEMESTER,SP.NAMA_STATUS
            ) PEM_S ON PEM_S.ID_MHS= M.ID_MHS
            LEFT JOIN (
                SELECT
                    BKM.ID_MHS,
                    SUM(CASE WHEN DB.ID_JENIS_DETAIL_BIAYA IN (2) THEN COALESCE(BESAR_BIAYA, 0) ELSE 0 END) GEN_SAT,
                    SUM(CASE WHEN DB.ID_JENIS_DETAIL_BIAYA IN (1) THEN COALESCE(BESAR_BIAYA, 0) ELSE 0 END) GEN_SEM,
                    SUM(CASE WHEN DB.ID_JENIS_DETAIL_BIAYA IN (4) THEN COALESCE(BESAR_BIAYA, 0) ELSE 0 END) GEN_BULAN,
                    SUM(CASE WHEN DB.ID_JENIS_DETAIL_BIAYA IN (3,5) THEN COALESCE(BESAR_BIAYA, 0) ELSE 0 END) GEN_FREE 
                FROM
                    BIAYA_KULIAH_MHS BKM
                    JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH 
                GROUP BY
                    BKM.ID_MHS
            ) JB ON JB.ID_MHS = M.ID_MHS
            LEFT JOIN (
                SELECT ID_MHS, SUM(T.BESAR_BIAYA) PEMBAYARAN_KEMARIN, SP.NAMA_STATUS STATUS_KEMARIN
                FROM TAGIHAN_MHS TM
                JOIN TAGIHAN T ON T.ID_TAGIHAN_MHS=TM.ID_TAGIHAN_MHS
                JOIN STATUS_PEMBAYARAN SP ON SP.ID_STATUS_PEMBAYARAN = T.ID_STATUS_PEMBAYARAN
                WHERE TM.ID_SEMESTER='{$semester_kemarin}'
                GROUP BY TM.ID_MHS,TM.ID_SEMESTER,SP.NAMA_STATUS
            ) PEM_K ON PEM_K.ID_MHS = M.ID_MHS
            LEFT JOIN (
                SELECT TM.ID_MHS, TM.ID_SEMESTER 
                FROM TAGIHAN_MHS TM
                JOIN TAGIHAN T ON T.ID_TAGIHAN_MHS=TM.ID_TAGIHAN_MHS
                WHERE T.ID_STATUS_PEMBAYARAN = 2 AND TM.ID_SEMESTER <> '{$semester_gen}'
                GROUP BY TM.ID_MHS,TM.ID_SEMESTER
            ) TAG ON TAG.ID_MHS=M.ID_MHS
            WHERE PS.ID_FAKULTAS='{$fakultas}' {$q_prodi} {$q_status} {$q_angkatan} {$q_p_sebelum}
            GROUP BY
                M.ID_MHS,M.NIM_MHS,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,SP.NM_STATUS_PENGGUNA, 
                PEM_K.PEMBAYARAN_KEMARIN,PEM_K.STATUS_KEMARIN,JB.GEN_SAT,JB.GEN_SEM,JB.GEN_BULAN,JB.GEN_FREE,PEM_S.PEMBAYARAN_SEKARANG ,PEM_S.STATUS_SEKARANG
            {$q_tagihan}
            ORDER BY M.NIM_MHS";
        return $this->db->QueryToArray($query);
    }

    function load_data_mahasiswa_virtual_account($pt, $fakultas, $prodi, $status, $semester_gen, $angkatan, $tagihan, $p_sebelum)
    {
        $q_prodi = $prodi != '' ? "AND PS.ID_PROGRAM_STUDI ='{$prodi}'" : "";
        $q_angkatan = $angkatan != '' ? "AND M.THN_ANGKATAN_MHS='{$angkatan}'" : "";
        if ($p_sebelum != '') {
            if ($p_sebelum == 1) {
                $q_p_sebelum = " AND PEM_K.PEMBAYARAN_KEMARIN IS NOT NULL AND (PEM_K.STATUS_KEMARIN='Sudah Bayar' OR PEM_K.STATUS_KEMARIN='Penangguhan')";
            } else if ($p_sebelum == 2) {
                $q_p_sebelum = " AND PEM_K.PEMBAYARAN_KEMARIN IS NOT NULL AND (PEM_K.STATUS_KEMARIN='Belum Bayar' OR PEM_K.STATUS_KEMARIN='Pembebasan'";
            } else if ($p_sebelum == 3) {
                $q_p_sebelum = " AND PEM_K.PEMBAYARAN_KEMARIN IS NULL";
            }
        } else {
            $q_p_sebelum = "";
        }
        $semester_kemarin = $this->get_semester_kemarin($semester_gen);
        if ($tagihan == '') {
            $q_tagihan = "";
        } else {
            if ($tagihan != 3) {
                $q_tagihan = "HAVING COUNT(TAG.ID_SEMESTER)={$tagihan}";
            } else {
                $q_tagihan = "HAVING COUNT(TAG.ID_SEMESTER)>2";
            }
        }
        $c_status = count($status);
        if ($c_status > 1) {
            $arr_s = '';
            for ($i = 0; $i < $c_status; $i++) {
                if ($i == ($c_status - 1)) {
                    $arr_s .= $status[$i];
                } else {
                    $arr_s .= $status[$i] . ',';
                }
            }
            $q_status = "AND SP.ID_STATUS_PENGGUNA IN ({$arr_s})";
        } else {
            $q_status = $status[0] != '' ? "AND SP.ID_STATUS_PENGGUNA ='{$status[0]}'" : "";
        }
        $query =
            "SELECT M.ID_MHS,M.NIM_MHS,M.THN_ANGKATAN_MHS,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,SP.NM_STATUS_PENGGUNA
                ,PEM_K.PEMBAYARAN_KEMARIN,PEM_K.STATUS_KEMARIN,JB.GEN_SAT,JB.GEN_SEM,JB.GEN_BULAN,JB.GEN_FREE,PEM_S.PEMBAYARAN_SEKARANG
                ,PEM_S.STATUS_SEKARANG,PEM_S.NO_VA,PEM_S.AWAL_PERIODE,PEM_S.AKHIR_PERIODE,PEM_S.KETERANGAN,PEM_S.TOTAL_BESAR_BIAYA
                , COUNT(TAG.ID_SEMESTER) TAGIHAN
            FROM MAHASISWA M
            JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
            LEFT JOIN (
                SELECT ID_MHS,SUM(BESAR_BIAYA) PEMBAYARAN_SEKARANG,SP.NAMA_STATUS STATUS_SEKARANG
                ,TM.NO_VA,TM.AWAL_PERIODE,TM.AKHIR_PERIODE,TM.KETERANGAN,TM.TOTAL_BESAR_BIAYA
                FROM TAGIHAN_MHS TM
                JOIN TAGIHAN T ON T.ID_TAGIHAN_MHS=TM.ID_TAGIHAN_MHS
                JOIN STATUS_PEMBAYARAN SP ON SP.ID_STATUS_PEMBAYARAN = T.ID_STATUS_PEMBAYARAN
                WHERE TM.ID_SEMESTER='{$semester_gen}'
                GROUP BY TM.ID_MHS,TM.ID_SEMESTER,SP.NAMA_STATUS
                ,TM.NO_VA,TM.AWAL_PERIODE,TM.AKHIR_PERIODE,TM.KETERANGAN,TM.TOTAL_BESAR_BIAYA
            ) PEM_S ON PEM_S.ID_MHS= M.ID_MHS
            LEFT JOIN (
                SELECT
                    BKM.ID_MHS,
                    SUM(CASE WHEN DB.ID_JENIS_DETAIL_BIAYA IN (2) THEN COALESCE(BESAR_BIAYA, 0) ELSE 0 END) GEN_SAT,
                    SUM(CASE WHEN DB.ID_JENIS_DETAIL_BIAYA IN (1) THEN COALESCE(BESAR_BIAYA, 0) ELSE 0 END) GEN_SEM,
                    SUM(CASE WHEN DB.ID_JENIS_DETAIL_BIAYA IN (4) THEN COALESCE(BESAR_BIAYA, 0) ELSE 0 END) GEN_BULAN,
                    SUM(CASE WHEN DB.ID_JENIS_DETAIL_BIAYA IN (3,5) THEN COALESCE(BESAR_BIAYA, 0) ELSE 0 END) GEN_FREE 
                FROM
                    BIAYA_KULIAH_MHS BKM
                    JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH 
                GROUP BY
                    BKM.ID_MHS
            ) JB ON JB.ID_MHS = M.ID_MHS
            LEFT JOIN (
                SELECT ID_MHS, SUM(T.BESAR_BIAYA) PEMBAYARAN_KEMARIN, SP.NAMA_STATUS STATUS_KEMARIN
                FROM TAGIHAN_MHS TM
                JOIN TAGIHAN T ON T.ID_TAGIHAN_MHS=TM.ID_TAGIHAN_MHS
                JOIN STATUS_PEMBAYARAN SP ON SP.ID_STATUS_PEMBAYARAN = T.ID_STATUS_PEMBAYARAN
                WHERE TM.ID_SEMESTER='{$semester_kemarin}'
                GROUP BY TM.ID_MHS,TM.ID_SEMESTER,SP.NAMA_STATUS
            ) PEM_K ON PEM_K.ID_MHS = M.ID_MHS
            LEFT JOIN (
                SELECT TM.ID_MHS, TM.ID_SEMESTER 
                FROM TAGIHAN_MHS TM
                JOIN TAGIHAN T ON T.ID_TAGIHAN_MHS=TM.ID_TAGIHAN_MHS
                WHERE T.ID_STATUS_PEMBAYARAN = 2 AND TM.ID_SEMESTER <> '{$semester_gen}'
                GROUP BY TM.ID_MHS,TM.ID_SEMESTER
            ) TAG ON TAG.ID_MHS=M.ID_MHS
            WHERE PS.ID_FAKULTAS='{$fakultas}' {$q_prodi} {$q_status} {$q_angkatan} {$q_p_sebelum}
            GROUP BY
                M.ID_MHS,M.NIM_MHS,M.THN_ANGKATAN_MHS,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,SP.NM_STATUS_PENGGUNA, 
                PEM_K.PEMBAYARAN_KEMARIN,PEM_K.STATUS_KEMARIN,JB.GEN_SAT,JB.GEN_SEM,JB.GEN_BULAN,JB.GEN_FREE,PEM_S.PEMBAYARAN_SEKARANG ,PEM_S.STATUS_SEKARANG,               
                PEM_S.NO_VA,
                PEM_S.AWAL_PERIODE,
                PEM_S.AKHIR_PERIODE,
                PEM_S.TOTAL_BESAR_BIAYA,
                PEM_S.KETERANGAN
            {$q_tagihan}
            ORDER BY M.NIM_MHS";
        return $this->db->QueryToArray($query);
    }

    function load_data_mahasiswa_pembayaran_spp($pt, $fakultas, $prodi)
    {
        $q_prodi = $prodi != '' ? "AND PS.ID_PROGRAM_STUDI ='{$prodi}'" : "";

        $query =
            "SELECT
               M.ID_MHS,
                M.NIM_MHS,
                PENGG.NM_PENGGUNA,
                S.NM_SEMESTER,
                S.THN_AKADEMIK_SEMESTER,
                PS.NM_PROGRAM_STUDI,
                J.NM_JENJANG,TAG.ID_TAGIHAN,
                PEM.NO_KUITANSI,
                PEM.ID_PEMBAYARAN,
                PEM.TGL_BAYAR,
                TAG.BESAR_BIAYA,
                ( TAG.BESAR_BIAYA / 6 ) SPP_PERBULAN,
                SUM( PEM.BESAR_PEMBAYARAN ) PEMBAYARAN_SPP,
                COALESCE(SUM( PP.BESAR_PEMBAYARAN ),0) DIPROSES_SPP 
            FROM
                PEMBAYARAN PEM
                JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN = PEM.ID_TAGIHAN
                JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS = TAG.ID_TAGIHAN_MHS
                JOIN SEMESTER S ON S.ID_SEMESTER = TM.ID_SEMESTER
                JOIN MAHASISWA M ON M.ID_MHS = TM.ID_MHS
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
                JOIN PENGGUNA PENGG ON PENGG.ID_PENGGUNA = M.ID_PENGGUNA
                JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = TAG.ID_DETAIL_BIAYA
                LEFT JOIN (
                    SELECT ID_PEMBAYARAN,ID_TAGIHAN,SUM(BESAR_PEMBAYARAN) BESAR_PEMBAYARAN
                    FROM PEMBAYARAN_PERBULAN
                    GROUP BY ID_PEMBAYARAN,ID_TAGIHAN
                ) PP ON PP.ID_TAGIHAN=TAG.ID_TAGIHAN
                AND PP.ID_PEMBAYARAN=PEM.ID_PEMBAYARAN
            WHERE
                DB.ID_BIAYA = 185 
                AND PEM.ID_STATUS_PEMBAYARAN = 1 
                AND PENGG.ID_PERGURUAN_TINGGI = '{$pt}'
                AND PS.ID_FAKULTAS='{$fakultas}' {$q_prodi} 
                AND TO_CHAR(PEM.TGL_BAYAR,'YYYY')='2020'
                AND ( TAG.BESAR_BIAYA / 6 ) >250000 AND ( TAG.BESAR_BIAYA / 6 ) <500000
                AND MOD( TAG.BESAR_BIAYA / 6 ,100)=0
            GROUP BY
            M.ID_MHS,
            M.NIM_MHS,
            PENGG.NM_PENGGUNA,
            S.NM_SEMESTER,
            S.THN_AKADEMIK_SEMESTER,
            PS.NM_PROGRAM_STUDI,
            J.NM_JENJANG,TAG.ID_TAGIHAN,
            PEM.NO_KUITANSI,
            PEM.ID_PEMBAYARAN,
            PEM.TGL_BAYAR,
            TAG.BESAR_BIAYA
            HAVING
                SUM( PEM.BESAR_PEMBAYARAN ) > ( TAG.BESAR_BIAYA / 6 )
            ORDER BY
                PEM.TGL_BAYAR DESC,M.NIM_MHS,PENGG.NM_PENGGUNA DESC,NM_SEMESTER,THN_AKADEMIK_SEMESTER ASC";
        return $this->db->QueryToArray($query);
    }


    //fungsi Edit Kerjasama

    function insert_beasiswa_mahasiswa($id_mhs, $beasiswa, $semester_mulai, $semester_selesai, $keterangan)
    {
        $this->db->Query("UPDATE SEJARAH_BEASISWA SET BEASISWA_AKTIF=0 WHERE ID_MHS='{$id_mhs}'");
        $this->db->Query("
            INSERT INTO SEJARAH_BEASISWA
                (ID_MHS,ID_BEASISWA,ID_SEMESTER_MULAI,ID_SEMESTER_SELESAI,KETERANGAN,BEASISWA_AKTIF)
            VALUES
                ('{$id_mhs}','{$beasiswa}','{$semester_mulai}','{$semester_selesai}','{$keterangan}',1)");
    }

    function update_beasiswa_mahasiswa($id_sejarah_beasiswa, $beasiswa, $semester_mulai, $semester_selesai, $keterangan, $aktif)
    {
        $this->db->Query("
            UPDATE SEJARAH_BEASISWA
            SET
                ID_BEASISWA='{$beasiswa}',
                ID_SEMESTER_MULAI='{$semester_mulai}',
                ID_SEMESTER_SELESAI='{$semester_selesai}',
                KETERANGAN='{$keterangan}',
                BEASISWA_AKTIF='{$aktif}'
            WHERE ID_SEJARAH_BEASISWA='{$id_sejarah_beasiswa}'");
    }

    function delete_beasiswa_mahasiswa($id_sejarah_beasiswa)
    {
        $this->db->Query("DELETE FROM SEJARAH_BEASISWA WHERE ID_SEJARAH_BEASISWA='{$id_sejarah_beasiswa}'");
    }

    // Fungsi Konfig

    function get_konfigurasi_keu()
    {
        $query = "
        SELECT C.DESKRIPSI,CP.* FROM CONFIG_PT CP
        JOIN CONFIG C ON C.KD_CONFIG=CP.KD_CONFIG
        WHERE CP.KD_CONFIG LIKE '%KEU%'
        AND CP.ID_PERGURUAN_TINGGI=" . getenv('ID_PT') . "
        ";
        return $this->db->QueryToArray($query);
    }

    function find_konfigurasi_keu($kode)
    {
        $query = "
        SELECT C.DESKRIPSI,CP.* FROM CONFIG_PT CP
        JOIN CONFIG C ON C.KD_CONFIG=CP.KD_CONFIG
        WHERE CP.KD_CONFIG='{$kode}'
        AND CP.ID_PERGURUAN_TINGGI=" . getenv('ID_PT') . "
        ";
        $this->db->Query($query);
        $result = $this->db->FetchAssoc();
        return $result['CONFIG_VALUE'];
    }
}
