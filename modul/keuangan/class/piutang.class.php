<?php

$id_pt = $id_pt_user;

class piutang
{

    public $db;

    function __construct($db)
    {
        $this->db = $db;
    }

    function get_semester($id_semester)
    {
        $this->db->Query("SELECT * FROM AUCC.SEMESTER WHERE ID_SEMESTER='{$id_semester}'");
        return $this->db->FetchAssoc();
    }

    function get_semester_aktif()
    {
        $this->db->Query("SELECT * FROM AUCC.SEMESTER WHERE STATUS_AKTIF_SEMESTER='True'");
        return $this->db->FetchAssoc();
    }

    function get_semester_setelahnya()
    {
        $hasil_semester = '';
        $semester_aktif = $this->get_semester_aktif();
        if ($semester_aktif['NM_SEMESTER'] == 'Ganjil') {
            $semester_setelah = $this->db->QueryToArray("SELECT ID_SEMESTER FROM AUCC.SEMESTER WHERE (NM_SEMESTER='Genap' OR NM_SEMESTER='Ganjil') AND (THN_AKADEMIK_SEMESTER>'{$semester_aktif['THN_AKADEMIK_SEMESTER']}' OR (NM_SEMESTER='Genap' AND THN_AKADEMIK_SEMESTER={$semester_aktif['THN_AKADEMIK_SEMESTER']}))");
        } else {
            $semester_setelah = $this->db->QueryToArray("SELECT ID_SEMESTER FROM SEMESTER WHERE (NM_SEMESTER='Genap' OR NM_SEMESTER='Ganjil') AND THN_AKADEMIK_SEMESTER>{$semester_aktif['THN_AKADEMIK_SEMESTER']}");
        }
        $no = 1;
        foreach ($semester_setelah as $s) {
            if ($no == count($semester_setelah)) {
                $hasil_semester .= $s['ID_SEMESTER'];
            } else {
                $hasil_semester .= $s['ID_SEMESTER'] . ",";
            }
            $no++;
        }
        return $hasil_semester;
    }

    private function get_condition_akademik($id_fakultas, $id_prodi)
    {
        if ($id_fakultas != "" && $id_prodi != "") {
            $query = " AND PS.ID_FAKULTAS = '{$id_fakultas}' AND PS.ID_PROGRAM_STUDI ='{$id_prodi}'";
        } else if ($id_fakultas != "") {
            $query = " AND PS.ID_FAKULTAS = '{$id_fakultas}' ";
        } else {
            $query = " ";
        }
        return $query;
    }

    function load_prodi_piutang()
    {
        return $this->db->QueryToArray("
            SELECT (JENJANG||' '||PRODI) PRODI,(JENJANG||'\/'||PRODI) VALUE FROM AUCC.RIWAYAT_PIUTANG RP
            JOIN MAHASISWA M ON M.NIM_MHS = RP.NIM
            JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA ON P.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "'
            GROUP BY JENJANG,PRODI
            ORDER BY PRODI
            ");
    }

    function load_list_semester()
    {
        return $this->db->QueryToArray("
            SELECT * FROM SEMESTER 
            WHERE  (NM_SEMESTER = 'Ganjil' or NM_SEMESTER = 'Genap') AND THN_AKADEMIK_SEMESTER BETWEEN '2000' 
            AND (SELECT THN_AKADEMIK_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True')
            ORDER BY TAHUN_AJARAN,NM_SEMESTER");
    }

    function load_list_status()
    {
        return $this->db->QueryToArray("SELECT * FROM STATUS_PENGGUNA WHERE ID_ROLE=3");
    }

    function load_mahasiswa_piutang($id_fakultas, $id_program_studi, $status)
    {
        if ($id_program_studi != '') {
            $query = " AND PS.ID_PROGRAM_STUDI = '{$id_program_studi}'";
        } else if ($id_fakultas != '') {
            $query = "AND PS.ID_FAKULTAS = '{$id_fakultas}'";
        } else {
            $query = "";
        }
        if ($status != '') {
            $query_status = "M.STATUS_AKADEMIK_MHS ='{$status}'";
        } else {
            $query_status = "M.STATUS_AKADEMIK_MHS IS NOT NULL";
        }
        return $this->db->QueryToArray("
            SELECT M.ID_MHS, M.NIM_MHS, P.NM_PENGGUNA, SP.NM_STATUS_PENGGUNA, F.NM_FAKULTAS,PS. NM_PROGRAM_STUDI, J.NM_JENJANG,M.STATUS_AKADEMIK_MHS
            FROM MAHASISWA M
            LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
            LEFT JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
            LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
            LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            WHERE {$query_status}
            {$query}
            ");
    }

    function load_mahasiswa_piutang_excel($id_fakultas, $id_program_studi, $status)
    {
        if ($id_program_studi != '') {
            $query = " AND PS.ID_PROGRAM_STUDI = '{$id_program_studi}'";
        } else if ($id_fakultas != '') {
            $query = "AND PS.ID_FAKULTAS = '{$id_fakultas}'";
        } else {
            $query = "";
        }
        if ($status != '') {
            $query_status = "M.STATUS_AKADEMIK_MHS ='{$status}'";
        } else {
            $query_status = "M.STATUS_AKADEMIK_MHS IS NOT NULL";
        }
        return $this->db->QueryToArray("
            SELECT M.ID_MHS, M.NIM_MHS, P.NM_PENGGUNA, SP.NM_STATUS_PENGGUNA, F.NM_FAKULTAS,PS. NM_PROGRAM_STUDI, J.NM_JENJANG,M.STATUS_AKADEMIK_MHS
            FROM MAHASISWA M
            LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
            LEFT JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
            LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
            LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            WHERE {$query_status}
            {$query}");
    }

    function get_data_piutang($nim)
    {
        return $this->db->QueryToArray("SELECT S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN,PEM.ID_MHS,SUM(PEM.TOTAL_BIAYA) TOTAL_BIAYA FROM 
        (SELECT ID_SEMESTER,NM_SEMESTER,TAHUN_AJARAN FROM SEMESTER
        WHERE  (NM_SEMESTER = 'Ganjil' or NM_SEMESTER = 'Genap') AND THN_AKADEMIK_SEMESTER BETWEEN '2000' 
        AND (SELECT THN_AKADEMIK_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True')) S
        LEFT JOIN 
        (SELECT PEM.ID_SEMESTER,PEM.ID_MHS,(CASE
            WHEN (PEM.ID_STATUS_PEMBAYARAN!=2) THEN SUM(PEM.BESAR_BIAYA)
            WHEN (PEM.ID_STATUS_PEMBAYARAN=2) THEN SUM(PEM.BESAR_BIAYA)*-1
        END) 
        TOTAL_BIAYA FROM PEMBAYARAN PEM
        JOIN MAHASISWA M ON M.ID_MHS=PEM.ID_MHS
        WHERE M.NIM_MHS='{$nim}'
        GROUP BY PEM.ID_SEMESTER,PEM.ID_MHS,PEM.TGL_BAYAR,PEM.ID_STATUS_PEMBAYARAN) PEM
        ON S.ID_SEMESTER= PEM.ID_SEMESTER 
        GROUP BY S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN,PEM.ID_MHS
        ORDER BY S.TAHUN_AJARAN,S.NM_SEMESTER");
    }

    function load_list_fakultas()
    {
        return $this->db->QueryToArray("SELECT ID_FAKULTAS,NM_FAKULTAS FROM FAKULTAS");
    }

    function load_list_prodi($id_fakultas, $id_jenjang = null)
    {
        if ($id_jenjang != '') {
            $q_jenjang = "PS.ID_JENJANG = '{$id_jenjang}'";
        } else {
            $q_jenjang = "";
        }
        if ($id_fakultas != '') {
            $q_fakultas = "PS.ID_FAKULTAS = '{$id_fakultas}'";
        } else {
            $q_fakultas = "";
        }
        if ($id_jenjang != '' && $id_fakultas != '') {
            $q_operator = "AND";
        } else {
            $q_operator = "";
        }

        return $this->db->QueryToArray("
            SELECT PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.NM_JENJANG
            FROM PROGRAM_STUDI PS
            JOIN JENJANG J ON PS.ID_JENJANG=J.ID_JENJANG
            WHERE {$q_fakultas} {$q_operator} {$q_jenjang}");
    }

    function load_data_puitang($id_fakultas, $id_program_studi, $status, $excel = NULL)
    {
        $data_piutang = array();
        if (empty($excel)) {
            foreach ($this->load_mahasiswa_piutang($id_fakultas, $id_program_studi, $status) as $data) {
                array_push($data_piutang, array(
                    'ID_MHS' => $data['ID_MHS'],
                    'NIM_MHS' => $data['NIM_MHS'],
                    'PRODI' => '( ' . $data['NM_JENJANG'] . ' ) ' . $data['NM_PROGRAM_STUDI'],
                    'NM_PENGGUNA' => $data['NM_PENGGUNA'],
                    'ID_STATUS' => $data['STATUS_AKADEMIK_MHS'],
                    'STATUS' => $data['NM_STATUS_PENGGUNA'],
                    'DATA_PIUTANG' => $this->get_data_piutang($data['NIM_MHS'])
                ));
            }
        } else {
            foreach ($this->load_mahasiswa_piutang_excel($id_fakultas, $id_program_studi, $status) as $data) {
                array_push($data_piutang, array(
                    'ID_MHS' => $data['ID_MHS'],
                    'NIM_MHS' => $data['NIM_MHS'],
                    'PRODI' => '( ' . $data['NM_JENJANG'] . ' ) ' . $data['NM_PROGRAM_STUDI'],
                    'NM_PENGGUNA' => $data['NM_PENGGUNA'],
                    'ID_STATUS' => $data['STATUS_AKADEMIK_MHS'],
                    'STATUS' => $data['NM_STATUS_PENGGUNA'],
                    'DATA_PIUTANG' => $this->get_data_piutang($data['NIM_MHS'])
                ));
            }
        }
        return $data_piutang;
    }

    // fungsi laporan tagihan / piutang

    function load_rekap_tagihan_mahasiswa($fakultas, $status, $semester)
    {
        $c_status = count($status);
        if ($c_status > 1) {
            $arr_s = '';
            for ($i = 0; $i < $c_status; $i++) {
                if ($i == ($c_status - 1)) {
                    $arr_s .= $status[$i];
                } else {
                    $arr_s .= $status[$i] . ',';
                }
            }
            $q_status = "AND SP.ID_STATUS_PENGGUNA IN ({$arr_s})";
        } else {
            $q_status = $status[0] != '' ? "AND SP.ID_STATUS_PENGGUNA ='{$status[0]}'" : "";
        }

        $query =
            "
                SELECT
                    PS.ID_FAKULTAS,
                    F.NM_FAKULTAS,
                    PS.ID_PROGRAM_STUDI ID_PRODI,
                    ('('||J.NM_JENJANG||') '||PS.NM_PROGRAM_STUDI) PRODI,
                    COUNT( TAG.ID_MHS ) JUMLAH_MHS,
                    SUM( TAG.TOTAL_TAGIHAN ) TAGIHAN,
                    SUM(TAG.TOTAL_TERBAYAR  ) TERBAYAR
                FROM
                    PROGRAM_STUDI PS
                    JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                    JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
                    LEFT JOIN (
                    SELECT
                        PS.ID_PROGRAM_STUDI,
                        TM.ID_SEMESTER,
                        M.ID_MHS,
                        SUM(CASE WHEN T.BESAR_BIAYA IS NOT NULL THEN T.BESAR_BIAYA ELSE 0 END) TOTAL_TAGIHAN,
                        SUM(CASE WHEN PEM.BESAR_PEMBAYARAN IS NOT NULL THEN PEM.BESAR_PEMBAYARAN ELSE 0 END) TOTAL_TERBAYAR 
                    FROM
                        TAGIHAN_MHS TM
                        JOIN TAGIHAN T ON T.ID_TAGIHAN_MHS = TM.ID_TAGIHAN_MHS
                        LEFT JOIN PEMBAYARAN PEM ON PEM.ID_TAGIHAN = T.ID_TAGIHAN
                        JOIN MAHASISWA M ON M.ID_MHS = TM.ID_MHS
                        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI 
                        JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
                    WHERE
                        TM.ID_SEMESTER = '{$semester}' 
                        {$q_status}
                    GROUP BY
                        PS.ID_PROGRAM_STUDI,
                        TM.ID_SEMESTER,
                        M.ID_MHS 
                    HAVING 
                        SUM(
                            COALESCE( T.BESAR_BIAYA, 0 )) >
                        SUM(
                            COALESCE( PEM.BESAR_PEMBAYARAN, 0 ))
                    ) TAG ON TAG.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
                WHERE
                    F.ID_FAKULTAS='{$fakultas}' AND
                    F.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "' 
                GROUP BY  
                    PS.ID_FAKULTAS,      
                    F.NM_FAKULTAS,            
                    PS.ID_PROGRAM_STUDI,
                    J.NM_JENJANG,
                    PS.NM_PROGRAM_STUDI
                ";
        // die($query);
        return $this->db->QueryToArray($query);
    }

    function load_rekap_tagihan_mahasiswa_wh($form_params)
    {
        $query = "
        SELECT * FROM WH_KEU_TAGIHAN_MHS_REKAP
        WHERE FORM_PARAMS='{$form_params}'
        ";
        return $this->db->QueryToArray($query);
    }

    function load_detail_tagihan_mhs_row($fakultas, $prodi, $status, $semester)
    {
        $condition_akademik = $this->get_condition_akademik($fakultas, $prodi);
        $c_status = count($status);
        if ($c_status > 1) {
            $arr_s = '';
            for ($i = 0; $i < $c_status; $i++) {
                if ($i == ($c_status - 1)) {
                    $arr_s .= $status[$i];
                } else {
                    $arr_s .= $status[$i] . ',';
                }
            }
            $q_status = "AND SP.ID_STATUS_PENGGUNA IN ({$arr_s})";
        } else {
            $q_status = $status[0] != '' ? "AND SP.ID_STATUS_PENGGUNA ='{$status[0]}'" : "";
        }
        $query = "
        SELECT PS.ID_MHS,PS.ID_PROGRAM_STUDI,PS.THN_ANGKATAN_MHS,PS.ID_FAKULTAS,PS.ID_JENJANG,PS.NIM_MHS,PS.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,PS.NM_JALUR,PS.NM_JENJANG,PB.TAGIHAN
        FROM
        (
            SELECT M.ID_MHS,PS.ID_PROGRAM_STUDI,PS.ID_FAKULTAS,J.ID_JENJANG,M.NIM_MHS,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,JAL.NM_JALUR,J.NM_JENJANG,M.THN_ANGKATAN_MHS 
            FROM PENGGUNA P
            JOIN MAHASISWA M ON P.ID_PENGGUNA  = M.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            JOIN FAKULTAS FAK ON FAK.ID_FAKULTAS=PS.ID_FAKULTAS
            LEFT JOIN ADMISI JM ON JM.ID_MHS = M.ID_MHS AND JM.ID_JALUR IS NOT NULL
            LEFT JOIN JALUR JAL ON JAL.ID_JALUR=JM.ID_JALUR 
		    JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
            WHERE FAK.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "'             
            {$q_status}
        ) PS
		  ,
		  (SELECT TM.ID_MHS,( SUM(CASE WHEN TAG.BESAR_BIAYA IS NOT NULL THEN TAG.BESAR_BIAYA ELSE 0 END) -
            SUM(CASE WHEN PEM.BESAR_PEMBAYARAN IS NOT NULL THEN PEM.BESAR_PEMBAYARAN ELSE 0 END) ) TAGIHAN
          FROM TAGIHAN TAG 
          LEFT JOIN PEMBAYARAN PEM ON TAG.ID_TAGIHAN=PEM.ID_TAGIHAN
          JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS=TAG.ID_TAGIHAN_MHS
		  WHERE TM.ID_SEMESTER='{$semester}'
          GROUP BY TM.ID_MHS
          HAVING 
            SUM(
                COALESCE( TAG.BESAR_BIAYA, 0 )) >
            SUM(
                COALESCE( PEM.BESAR_PEMBAYARAN, 0 ))
        ) PB
        WHERE PS.ID_MHS = PB.ID_MHS {$condition_akademik}
        GROUP BY PS.ID_MHS,
            PS.ID_PROGRAM_STUDI,
            PS.THN_ANGKATAN_MHS,
            PS.ID_FAKULTAS,
            PS.ID_JENJANG,
            PS.NIM_MHS,
            PS.NM_PENGGUNA,
            PS.NM_PROGRAM_STUDI,
            PS.NM_JALUR,
            PS.NM_JENJANG,
            PB.TAGIHAN 
        ORDER BY PS.NM_PENGGUNA";
        // die($query);
        return $this->db->QueryToArray($query);
    }

    function load_detail_tagihan_mhs($fakultas, $prodi, $status, $semester)
    {
        $data_report_fakultas = array();
        //saat report fakultas tertentu
        $rows = $this->load_detail_tagihan_mhs_row($fakultas, $prodi, $status, $semester);
        foreach ($rows as $data) {
            $query = "
                SELECT B.ID_BIAYA,B.NM_BIAYA,
                ( 
                    SUM(BESAR_BIAYA) -
                    SUM(BESAR_PEMBAYARAN)
                 )
                BESAR_BIAYA
                    FROM BIAYA B
                LEFT JOIN (
                      SELECT B.ID_BIAYA,
                      SUM(CASE WHEN TAG.BESAR_BIAYA IS NOT NULL THEN TAG.BESAR_BIAYA ELSE 0 END) BESAR_BIAYA,
                      SUM(CASE WHEN PEM.BESAR_PEMBAYARAN IS NOT NULL THEN PEM.BESAR_PEMBAYARAN ELSE 0 END) BESAR_PEMBAYARAN
                      FROM MAHASISWA M
                      JOIN TAGIHAN_MHS TM ON TM.ID_MHS=M.ID_MHS
                      JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN_MHS=TM.ID_TAGIHAN_MHS
                      LEFT JOIN PEMBAYARAN PEM ON PEM.ID_TAGIHAN = TAG.ID_TAGIHAN
                      JOIN DETAIL_BIAYA DB ON TAG.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
                      JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
                      JOIN PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                      JOIN FAKULTAS FAK ON FAK.ID_FAKULTAS=PS.ID_FAKULTAS
                      WHERE M.ID_MHS='{$data['ID_MHS']}' AND TM.ID_SEMESTER='{$semester}'
                      AND FAK.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "'
                      GROUP BY B.ID_BIAYA
                      ) PEM ON B.ID_BIAYA = PEM.ID_BIAYA
                WHERE B.ID_PERGURUAN_TINGGI='" . getenv('ID_PT') . "'
                GROUP BY B.ID_BIAYA,B.NM_BIAYA
                ORDER BY B.NM_BIAYA";
            array_push($data_report_fakultas, array(
                'NM_PENGGUNA' => $data['NM_PENGGUNA'],
                'NIM_MHS' => $data['NIM_MHS'],
                'ANGKATAN' => $data['THN_ANGKATAN_MHS'],
                'JALUR' => $data['NM_JALUR'],
                'NM_PROGRAM_STUDI' => '( ' . $data['NM_JENJANG'] . ' ) ' . $data['NM_PROGRAM_STUDI'],
                'TOTAL_TAGIHAN' => $this->db->QueryToArray($query)
            ));
        }
        // die(var_dump($data_report_fakultas));
        return $data_report_fakultas;
    }

    function load_detail_tagihan_mhs_wh($fakultas, $prodi, $semester, $form_params)
    {
        $query = "
        SELECT * FROM WH_KEU_TAGIHAN_MHS
        WHERE FORM_PARAMS='{$form_params}'
        AND ID_FAKULTAS='{$fakultas}'
        AND ID_PRODI='{$prodi}'
        AND ID_SEMESTER='{$semester}'
        ";
        return $this->db->QueryToArray($query);
    }

    function insert_wh_rekap($data_rekap, $semester, $form_params)
    {
        $str_semester = $semester['NM_SEMESTER'] . ' ' . $semester['TAHUN_AJARAN'];
        $this->db->Query("DELETE FROM WH_KEU_TAGIHAN_MHS_REKAP WHERE FORM_PARAMS='{$form_params}'");
        foreach ($data_rekap as $d) {
            $jumlah_mhs = $d['JUMLAH_MHS'] != '' ? $d['JUMLAH_MHS'] : 0;
            $tagihan = $d['TAGIHAN'] != '' ? $d['TAGIHAN'] : 0;
            $terbayar = $d['TERBAYAR'] != '' ? $d['TERBAYAR'] : 0;
            $piutang = $tagihan - $terbayar;
            $this->db->Query("
                    INSERT INTO WH_KEU_TAGIHAN_MHS_REKAP
                        (ID_FAKULTAS,
                        NM_FAKULTAS,
                        ID_PRODI,
                        PRODI,
                        ID_SEMESTER,
                        SEMESTER,
                        JUMLAH_MHS,
                        TAGIHAN,
                        TERBAYAR,
                        PIUTANG,
                        UPDATED_AT,
                        FORM_PARAMS)
                    VALUES
                        ('{$d['ID_FAKULTAS']}',
                        '{$d['NM_FAKULTAS']}',
                        '{$d['ID_PRODI']}',
                        '{$d['PRODI']}',
                        '{$semester['ID_SEMESTER']}',
                        '{$str_semester}',
                        {$jumlah_mhs},
                        {$tagihan},
                        {$terbayar},
                        {$piutang},
                        CURRENT_TIMESTAMP,
                        '{$form_params}')
                ");
        }
    }

    // fungsi daftar mahasiswa piutang

    function load_daftar_mahasiswa_piutang($fakultas, $prodi, $status, $tagihan)
    {
        $data_hasil = array();
        $q_fakultas = $fakultas != '' ? "AND F.ID_FAKULTAS='{$fakultas}'" : "";
        $q_prodi = $prodi != '' ? "AND M.ID_PROGRAM_STUDI ='{$prodi}'" : "";
        $q_semester = "AND PEM.ID_SEMESTER NOT IN (" . $this->get_semester_setelahnya() . ")";
        $c_status = count($status);
        if ($c_status > 1) {
            $arr_s = '';
            for ($i = 0; $i < $c_status; $i++) {
                if ($i == ($c_status - 1)) {
                    $arr_s .= $status[$i];
                } else {
                    $arr_s .= $status[$i] . ',';
                }
            }
            $q_status = "AND SP.ID_STATUS_PENGGUNA IN ({$arr_s})";
        } else {
            $q_status = $status != '' ? "AND SP.ID_STATUS_PENGGUNA ='{$status[0]}'" : "";
        }

        if ($tagihan == '') {
            $q_tagihan = "";
        } else {
            if ($tagihan != 3) {
                $q_tagihan = "HAVING COUNT(DISTINCT(ID_SEMESTER))={$tagihan}";
            } else {
                $q_tagihan = "HAVING COUNT(DISTINCT(ID_SEMESTER))>2";
            }
        }

        $result = $this->db->QueryToArray("SELECT * FROM (
            SELECT M.ID_MHS,M.NIM_MHS,P.NM_PENGGUNA,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,F.ID_FAKULTAS,F.NM_FAKULTAS,SP.NM_STATUS_PENGGUNA
            FROM AUCC.MAHASISWA M
            JOIN AUCC.STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
            JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
            JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI= M.ID_PROGRAM_STUDI
            JOIN AUCC.JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            JOIN AUCC.FAKULTAS F ON PS.ID_FAKULTAS = F.ID_FAKULTAS
            WHERE M.STATUS_AKADEMIK_MHS IS NOT NULL {$q_fakultas} {$q_prodi} {$q_status}
        ) M
        JOIN 
        (SELECT PEM.ID_MHS,COUNT(DISTINCT(ID_SEMESTER)) JUMLAH_PIUTANG_SEMESTER,SUM(PEM.BESAR_BIAYA) PIUTANG
            FROM AUCC.PEMBAYARAN PEM
            WHERE ID_STATUS_PEMBAYARAN=2 {$q_semester}
            GROUP BY PEM.ID_MHS
            {$q_tagihan}
        ) PEM ON M.ID_MHS= PEM.ID_MHS
        ORDER BY M.NM_FAKULTAS,M.NM_JENJANG,M.NM_PROGRAM_STUDI,M.NIM_MHS");
        foreach ($result as $r) {
            $perubahan_s = $this->GetPerubahanStatusMhs($r['ID_MHS']);
            array_push($data_hasil, array(
                'ID_MHS' => $r['ID_MHS'],
                'NIM' => $r['NIM_MHS'],
                'NAMA' => $r['NM_PENGGUNA'],
                'JENJANG' => $r['NM_JENJANG'],
                'PRODI' => $r['NM_PROGRAM_STUDI'],
                'FAKULTAS' => $r['NM_FAKULTAS'],
                'STATUS' => $r['NM_STATUS_PENGGUNA'],
                'BANYAK_TAGIHAN' => $r['JUMLAH_PIUTANG_SEMESTER'],
                'BESAR_PIUTANG' => $r['PIUTANG'],
                'PERUBAHAN_STATUS' => $perubahan_s,
                'DATA_PIUTANG' => $this->db->QueryToArray("SELECT * FROM RIWAYAT_PIUTANG WHERE NIM='{$r['NIM_MHS']}'"),
                'KLASIFIKASI' => $this->NaiveBayesClassification($perubahan_s, $r['NM_JENJANG'], $r['JUMLAH_PIUTANG_SEMESTER'])
            ));
        }
        return $data_hasil;
    }

    function GetPerubahanStatusMhs($id_mhs)
    {
        /*
         * 1.Tetap Aktif
         * 2.Berubah Aktif
         * 3.Berubah Tidak Aktif
         */
        $query1 = "SELECT * FROM ADMISI WHERE ID_MHS='{$id_mhs}' ORDER BY TGL_APV DESC,TGL_USULAN DESC";
        $status_mhs = $this->db->QuerySingle("SELECT STATUS_AKADEMIK_MHS FROM MAHASISWA WHERE ID_MHS='{$id_mhs}'");
        // Ambil Status Terakhir
        $data_history = $this->db->QueryToArray($query1);
        if (count($data_history) > 1) {
            $status = $this->db->QuerySingle("SELECT STATUS_AKTIF FROM STATUS_PENGGUNA WHERE ID_STATUS_PENGGUNA='{$status_mhs}')");
            if ($status == '1') {
                $status_ubah = 2;
            } else {
                $status_ubah = 3;
            }
        } else {
            $status_ubah = 1;
        }
        return $status_ubah;
    }

    function insert_data_piutang($id_mhs, $nama, $nim, $jenjang, $prodi, $fakultas, $banyak, $tgl, $perubahan, $status_piutang, $status_akademik)
    {
        $nama = str_replace("'", "''", $nama);
        $this->db->Query("
            INSERT INTO AUCC.RIWAYAT_PIUTANG 
                (NAMA,NIM,JENJANG,PRODI,FAKULTAS,BANYAK_TAGIHAN,TGL_CUT_OFF,PERUBAHAN_STATUS,STATUS_PIUTANG,STATUS)
            VALUES
                ('{$nama}','{$nim}','{$jenjang}','{$prodi}','{$fakultas}','{$banyak}','{$tgl}','{$perubahan}','{$status_piutang}','{$status_akademik}')
            ");
        $id_riwayat = $this->db->QuerySingle("SELECT ID_RIWAYAT_PIUTANG FROM RIWAYAT_PIUTANG WHERE NIM='{$nim}' AND TGL_CUT_OFF='{$tgl}'");
        $this->db->Query("
            INSERT INTO RIWAYAT_DETAIL_PIUTANG
                (ID_RIWAYAT_PIUTANG,NM_BIAYA,BESAR_BIAYA)
            SELECT '{$id_riwayat}' ID_RIWAYAT_PIUTANG,B.NM_BIAYA,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA
            FROM PEMBAYARAN PEM
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PEM.ID_DETAIL_BIAYA
            JOIN BIAYA B ON B.ID_BIAYA=DB.ID_BIAYA
            WHERE PEM.ID_STATUS_PEMBAYARAN=2 AND PEM.ID_MHS='{$id_mhs}' AND PEM.BESAR_BIAYA!=0
            GROUP BY B.NM_BIAYA
            ");
    }

    // fungsi Klasifikasi Naive Bayes

    function NaiveBayesClassification($status, $jenjang, $tagihan)
    {

        // Hitung Probabilitas Prior Kelas
        $this->db->Query("
            SELECT 
                SUM(CASE WHEN STATUS_PIUTANG=1 THEN 1 ELSE 0 END) YA,
                SUM(CASE WHEN STATUS_PIUTANG=0 THEN 1 ELSE 0 END) TIDAK
            FROM RIWAYAT_PIUTANG
            ");
        $pri = $this->db->FetchAssoc();
        $pri_y = $pri['YA'] / ($pri['YA'] + $pri['TIDAK']);
        $pri_t = $pri['TIDAK'] / ($pri['YA'] + $pri['TIDAK']);

        // Probabilitas Prosterior
        // Untuk Atribut Kategorikal
        // Atribut Perubahan Status
        $var_1 = $this->db->QueryToArray("
            SELECT 
                PERUBAHAN_STATUS,
                SUM(CASE WHEN STATUS_PIUTANG=1 THEN 1 ELSE 0 END) YA,
                SUM(CASE WHEN STATUS_PIUTANG=0 THEN 1 ELSE 0 END) TIDAK
            FROM RIWAYAT_PIUTANG
            GROUP BY PERUBAHAN_STATUS
            ");
        foreach ($var_1 as $v1) {
            if ($v1['PERUBAHAN_STATUS'] == $status) {
                $pos_status_y = $v1['YA'] / $pri['YA'];
                $pos_status_t = $v1['TIDAK'] / $pri['TIDAK'];
            }
        }
        // Atribut Jenjang
        $var_2 = $this->db->QueryToArray("
            SELECT 
                JENJANG,
                SUM(CASE WHEN STATUS_PIUTANG=1 THEN 1 ELSE 0 END) YA,
                SUM(CASE WHEN STATUS_PIUTANG=0 THEN 1 ELSE 0 END) TIDAK
            FROM RIWAYAT_PIUTANG
            GROUP BY JENJANG
            ");
        foreach ($var_2 as $v2) {
            if ($v2['JENJANG'] == $jenjang) {
                $pos_jenjang_y = $v2['YA'] / $pri['YA'];
                $pos_jenjang_t = $v2['TIDAK'] / $pri['TIDAK'];
            }
        }

        // Atribut Banyak Tagihan
        $var_3 = $this->db->QueryToArray("
            SELECT 
                BANYAK_TAGIHAN,
                SUM(CASE WHEN STATUS_PIUTANG=1 THEN 1 ELSE 0 END) YA,
                SUM(CASE WHEN STATUS_PIUTANG=0 THEN 1 ELSE 0 END) TIDAK
            FROM RIWAYAT_PIUTANG
            GROUP BY BANYAK_TAGIHAN
            ");
        foreach ($var_3 as $v3) {
            if ($v3['BANYAK_TAGIHAN'] == $tagihan) {
                $pos_tagihan_y = $v3['YA'] / $pri['YA'];
                $pos_tagihan_t = $v3['TIDAK'] / $pri['TIDAK'];
            }
        }

        // Hitunga Probabilitas Prosterior 
        //        echo $pro_jenjang_y ." ". $pro_status_y ." ". $pro_tagihan_y."<br/>";
        //        echo $pro_jenjang_t ." ". $pro_status_t ." ". $pro_tagihan_t;
        $pos_y = $pos_jenjang_y * $pos_status_y * $pos_tagihan_y;
        $pos_t = $pos_jenjang_t * $pos_status_t * $pos_tagihan_t;
        // Hasil Probabilitas Naive Bayes
        $nb_y = $pos_y * $pri_y;
        $nb_t = $pos_t * $pri_t;

        return array('pri_y' => $pri_y, 'pri_t' => $pri_t, 'pos_j_t' => $pos_jenjang_t, 'pos_j_y' => $pos_jenjang_y, 'pos_s_t' => $pos_status_t, 'pos_s_y' => $pos_status_y, 'pos_t_t' => $pos_tagihan_t, 'pos_t_y' => $pos_tagihan_y, 'y' => $nb_y, 't' => $nb_t);
    }

    //fungsi kontrol piutang

    function load_data_kontrol_piutang($fakultas, $prodi, $status, $tgl)
    {
        $q_fakultas = $fakultas == '' ? "" : "AND RP.FAKULTAS='{$fakultas}'";
        if ($prodi != '') {
            $p = explode('\/', $prodi);
        }
        $q_prodi = $prodi == '' ? "" : "AND RP.JENJANG='{$p[0]}' AND RP.PRODI='{$p[1]}'";
        return $this->db->QueryToArray("
            SELECT RP.ID_RIWAYAT_PIUTANG,NAMA,NIM,PRODI,JENJANG,FAKULTAS,
                BANYAK_TAGIHAN,PERUBAHAN_STATUS,TGL_CUT_OFF,STATUS_PIUTANG,STATUS,SUM(RDP.BESAR_BIAYA) BESAR_PIUTANG
            FROM RIWAYAT_PIUTANG RP
            JOIN MAHASISWA M ON M.NIM_MHS = RP.NIM
            JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA ON P.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "'
            JOIN RIWAYAT_DETAIL_PIUTANG RDP ON RP.ID_RIWAYAT_PIUTANG=RDP.ID_RIWAYAT_PIUTANG
            WHERE RP.TGL_CUT_OFF='{$tgl}' {$q_fakultas} {$q_prodi} AND RP.STATUS_PIUTANG='{$status}'
            GROUP BY RP.ID_RIWAYAT_PIUTANG,NAMA,NIM,PRODI,JENJANG,FAKULTAS,BANYAK_TAGIHAN,PERUBAHAN_STATUS,TGL_CUT_OFF,STATUS_PIUTANG,STATUS
            ORDER BY FAKULTAS,JENJANG,PRODI,NIM");
    }

    function load_data_kontrol_piutang_detail($fakultas, $jenjang, $prodi, $tgl, $status, $mhs)
    {
        $data_hasil = array();
        if ($prodi != '' || $mhs != '') {
            if ($mhs != '') {
                $r_mhs = $this->db->QueryToArray("
                SELECT * FROM RIWAYAT_PIUTANG 
                WHERE TGL_CUT_OFF='{$tgl}' AND FAKULTAS='{$fakultas}' AND STATUS_PIUTANG='{$status}'
                ORDER BY FAKULTAS,JENJANG,PRODI,NIM");
            } else {
                $r_mhs = $this->db->QueryToArray("
                SELECT * FROM RIWAYAT_PIUTANG 
                WHERE TGL_CUT_OFF='{$tgl}' AND PRODI='{$prodi}' AND JENJANG='{$jenjang}' AND STATUS_PIUTANG='{$status}'
                ORDER BY FAKULTAS,JENJANG,PRODI,NIM");
            }
            foreach ($r_mhs as $r) {
                $r_biaya = $this->db->QueryToArray("
                SELECT B.BIAYA,SUM(D.BESAR_BIAYA) BESAR_BIAYA
                FROM 
                (
                  SELECT DISTINCT(NM_BIAYA) BIAYA FROM AUCC.RIWAYAT_DETAIL_PIUTANG
                ) B
                LEFT JOIN
                (
                  SELECT RDP.NM_BIAYA,RDP.BESAR_BIAYA
                  FROM AUCC.RIWAYAT_PIUTANG RP
                  JOIN AUCC.RIWAYAT_DETAIL_PIUTANG RDP ON RP.ID_RIWAYAT_PIUTANG=RDP.ID_RIWAYAT_PIUTANG
                  WHERE RP.TGL_CUT_OFF='{$tgl}' AND RP.STATUS_PIUTANG='{$status}' AND RP.NIM='{$r['NIM']}'
                ) D ON D.NM_BIAYA=B.BIAYA
                GROUP BY B.BIAYA
                ORDER BY B.BIAYA");
                array_push($data_hasil, array_merge($r, array('PIUTANG' => $r_biaya)));
            }
        } else if ($fakultas != '') {
            $r_prodi = $this->db->QueryToArray("
                SELECT J.NM_JENJANG,PS.NM_PROGRAM_STUDI,(SELECT COUNT(DISTINCT(NIM)) FROM AUCC.RIWAYAT_PIUTANG WHERE TGL_CUT_OFF='{$tgl}' AND STATUS_PIUTANG='{$status}' AND JENJANG=J.NM_JENJANG AND PRODI=PS.NM_PROGRAM_STUDI) JUMLAH_MHS 
                FROM PROGRAM_STUDI PS
                JOIN JENJANG J ON PS.ID_JENJANG=J.ID_JENJANG
                JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
                WHERE F.NM_FAKULTAS='{$fakultas}'
                ");
            foreach ($r_prodi as $r) {
                $r_biaya = $this->db->QueryToArray("
                SELECT B.BIAYA,SUM(D.BESAR_BIAYA) BESAR_BIAYA
                FROM 
                (
                  SELECT DISTINCT(NM_BIAYA) BIAYA FROM AUCC.RIWAYAT_DETAIL_PIUTANG
                ) B
                LEFT JOIN
                (
                  SELECT RDP.NM_BIAYA,RDP.BESAR_BIAYA
                  FROM AUCC.RIWAYAT_PIUTANG RP
                  JOIN AUCC.RIWAYAT_DETAIL_PIUTANG RDP ON RP.ID_RIWAYAT_PIUTANG=RDP.ID_RIWAYAT_PIUTANG
                  WHERE RP.TGL_CUT_OFF='{$tgl}' AND RP.STATUS_PIUTANG='{$status}' AND RP.JENJANG='{$r['NM_JENJANG']}' AND RP.PRODI='{$r['NM_PROGRAM_STUDI']}'
                ) D ON D.NM_BIAYA=B.BIAYA
                GROUP BY B.BIAYA
                ORDER BY B.BIAYA");
                array_push($data_hasil, array_merge($r, array('PIUTANG' => $r_biaya)));
            }
        } else {
            $r_fakultas = $this->db->QueryToArray("
                SELECT F.*,(SELECT COUNT(DISTINCT(NIM)) FROM AUCC.RIWAYAT_PIUTANG WHERE TGL_CUT_OFF='{$tgl}' AND STATUS_PIUTANG='{$status}' AND FAKULTAS=F.NM_FAKULTAS) JUMLAH_MHS 
                FROM AUCC.FAKULTAS F 
                ORDER BY ID_FAKULTAS");
            foreach ($r_fakultas as $r) {
                $r_biaya = $this->db->QueryToArray("
                SELECT B.BIAYA,SUM(D.BESAR_BIAYA) BESAR_BIAYA
                FROM 
                (
                  SELECT DISTINCT(NM_BIAYA) BIAYA FROM AUCC.RIWAYAT_DETAIL_PIUTANG
                ) B
                LEFT JOIN
                (
                  SELECT RDP.NM_BIAYA,RDP.BESAR_BIAYA
                  FROM AUCC.RIWAYAT_PIUTANG RP
                  JOIN AUCC.RIWAYAT_DETAIL_PIUTANG RDP ON RP.ID_RIWAYAT_PIUTANG=RDP.ID_RIWAYAT_PIUTANG
                  WHERE RP.TGL_CUT_OFF='{$tgl}' AND RP.STATUS_PIUTANG='{$status}' AND RP.FAKULTAS='{$r['NM_FAKULTAS']}'
                ) D ON D.NM_BIAYA=B.BIAYA
                GROUP BY B.BIAYA
                ORDER BY B.BIAYA");
                array_push($data_hasil, array_merge($r, array('PIUTANG' => $r_biaya)));
            }
        }
        return $data_hasil;
    }

    //fungsi pelunasan piutang


    function load_data_pelunasan_piutang($fakultas, $prodi, $status, $semester_pelunasan)
    {
        $q_fakultas = $fakultas != '' ? "AND PS.ID_FAKULTAS='{$fakultas}'" : "";
        $q_prodi = $prodi != '' ? "AND M.ID_PROGRAM_STUDI ='{$prodi}'" : "";
        $q_status = $status != '' ? "AND SP.ID_STATUS_PENGGUNA ='{$status}'" : "";
        return $this->db->QueryToArray("
        SELECT M.NIM_MHS,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,CONCAT(SB.NM_SEMESTER||' ',SB.TAHUN_AJARAN) SEMESTER_BAYAR,
            CONCAT(S.NM_SEMESTER||' ',S.TAHUN_AJARAN) SEMESTER_PIUTANG,SP.NM_STATUS_PENGGUNA,
            PEM.ID_SEMESTER,PEM.ID_SEMESTER_BAYAR,PEM.ID_MHS,PEM.ID_BANK,PEM.ID_BANK_VIA,PEM.TGL_BAYAR,PEM.NO_TRANSAKSI,PEM.KETERANGAN
            ,SUM(PEM.BESAR_BIAYA) PEMBAYARAN,PEM.ID_STATUS_PEMBAYARAN
        FROM AUCC.PEMBAYARAN PEM
        JOIN AUCC.MAHASISWA M ON PEM.ID_MHS=M.ID_MHS
        JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
        JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
        JOIN AUCC.JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        JOIN AUCC.STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA= M.STATUS_AKADEMIK_MHS
        JOIN AUCC.SEMESTER S ON S.ID_SEMESTER = PEM.ID_SEMESTER
        JOIN AUCC.SEMESTER SB ON SB.ID_SEMESTER = PEM.ID_SEMESTER_BAYAR
        WHERE PEM.ID_SEMESTER != PEM.ID_SEMESTER_BAYAR AND PEM.ID_STATUS_PEMBAYARAN =1 AND
            CONCAT(SB.THN_AKADEMIK_SEMESTER,SB.NM_SEMESTER) > CONCAT(S.THN_AKADEMIK_SEMESTER,S.NM_SEMESTER)
            AND SB.ID_SEMESTER='{$semester_pelunasan}' {$q_fakultas} {$q_prodi} {$q_status}
        GROUP BY M.NIM_MHS,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,S.NM_SEMESTER,S.TAHUN_AJARAN,SB.NM_SEMESTER,SB.TAHUN_AJARAN,SP.NM_STATUS_PENGGUNA,
            PEM.ID_SEMESTER,PEM.ID_MHS,PEM.ID_BANK,PEM.ID_BANK_VIA,PEM.TGL_BAYAR,PEM.NO_TRANSAKSI,PEM.KETERANGAN
            ,PEM.ID_STATUS_PEMBAYARAN,PEM.ID_SEMESTER_BAYAR
        ORDER BY PEM.ID_MHS,PEM.ID_SEMESTER,PEM.ID_SEMESTER_BAYAR");
    }
}
