<?php

$id_pt = $id_pt_user;

class list_data {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    function load_list_fakultas($id_pt) {
        return $this->db->QueryToArray("SELECT ID_FAKULTAS,NM_FAKULTAS FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = {$id_pt}");
    }

    function load_list_fakultas_one($id_pt,$id_fakultas) {
    return $this->db->QueryToArray("SELECT ID_FAKULTAS,NM_FAKULTAS FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = {$id_pt} AND ID_FAKULTAS={$id_fakultas}");
    }

    function load_list_prodi($id_pt, $id_fakultas= null, $id_jenjang = null) {
        if ($id_jenjang != '') {
            $q_jenjang = "PS.ID_JENJANG = '{$id_jenjang}' AND";
        } else {
            $q_jenjang = "";
        }
        if ($id_fakultas != '') {
            $q_fakultas = "PS.ID_FAKULTAS = '{$id_fakultas}' AND";
        } else {
            $q_fakultas = "";
        }
        /*if ($id_jenjang != '' && $id_fakultas != '') {
            $q_operator = "AND";
        } else {
            $q_operator = "";
        }
        */

        return $this->db->QueryToArray("
            SELECT PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,PS.GELAR_PENDEK GELAR
            FROM PROGRAM_STUDI PS
            JOIN JENJANG J ON PS.ID_JENJANG=J.ID_JENJANG
            JOIN FAKULTAS F ON PS.ID_FAKULTAS=F.ID_FAKULTAS
            WHERE {$q_fakultas} {$q_jenjang} STATUS_AKTIF_PRODI = 1 AND F.ID_PERGURUAN_TINGGI = {$id_pt}
            ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI");
    }

    function load_list_prodi_all($id_pt) {
        return $this->db->QueryToArray("
            SELECT PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.NM_JENJANG
            FROM PROGRAM_STUDI PS
            JOIN JENJANG J ON PS.ID_JENJANG=J.ID_JENJANG
            JOIN FAKULTAS F ON PS.ID_FAKULTAS=F.ID_FAKULTAS
			WHERE STATUS_AKTIF_PRODI = 1 AND F.ID_PERGURUAN_TINGGI = {$id_pt}");
    }

    function load_list_jalur() {
        return $this->db->QueryToArray("
            SELECT * FROM JALUR WHERE ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."' ORDER BY NM_JALUR");
    }

    function load_list_jenjang() {
        return $this->db->QueryToArray("
            SELECT * FROM JENJANG ORDER BY NM_JENJANG");
    }

    function load_list_semester() {
        return $this->db->QueryToArray("
            SELECT * FROM SEMESTER WHERE (NM_SEMESTER='Ganjil' OR NM_SEMESTER='Genap') AND ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."' ORDER BY TAHUN_AJARAN DESC,NM_SEMESTER DESC");
    }
    
    function load_list_semester_sp(){
        return $this->db->QueryToArray("
            SELECT * FROM SEMESTER WHERE TIPE_SEMESTER='SP' AND ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."' ORDER BY TAHUN_AJARAN DESC,GROUP_SEMESTER DESC");
    }

    function load_list_kelompok_biaya() {
        return $this->db->QueryToArray("
            SELECT * FROM KELOMPOK_BIAYA WHERE ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."' ORDER BY NM_KELOMPOK_BIAYA");
    }

    function load_bank() {
        return $this->db->QueryToArray("
            SELECT * FROM BANK ORDER BY NM_BANK");
    }

    function load_bank_via() {
        return $this->db->QueryToArray("
            SELECT * FROM BANK_VIA ORDER BY NAMA_BANK_VIA");
    }

    function load_jenis_detail_biaya() {
        return $this->db->QueryToArray("SELECT * FROM JENIS_DETAIL_BIAYA ORDER BY NM_JENIS_DETAIL_BIAYA");
    }

    function load_biaya() {
        return $this->db->QueryToArray("SELECT * FROM BIAYA WHERE ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."' ORDER BY NM_BIAYA");
    }

    function load_status_piutang() {
        return $this->db->QueryToArray("SELECT * FROM STATUS_PIUTANG");
    }

    function load_angkatan_mhs() {
        return $this->db->QueryToArray("SELECT DISTINCT(THN_ANGKATAN_MHS) ANGKATAN FROM MAHASISWA 
        WHERE THN_ANGKATAN_MHS BETWEEN (TO_CHAR(SYSDATE,'YYYY')-10) AND TO_CHAR(SYSDATE,'YYYY')
        ORDER BY ANGKATAN DESC");
    }

    function load_golongan() {
        return $this->db->QueryToArray("
            SELECT * FROM GOLONGAN ORDER BY NM_GOLONGAN,NM_PANGKAT");
    }
    
    function load_tgl_cut_off_piutang(){
        return $this->db->QueryToArray("
            SELECT DISTINCT(TGL_CUT_OFF) CUT_OFF FROM RIWAYAT_PIUTANG");
    }

    function load_pendidikan_akhir() {
        return $this->db->QueryToArray("
            SELECT * FROM PENDIDIKAN_AKHIR");
    }

    function get_bank($id_bank) {
        $this->db->Query("SELECT * FROM BANK WHERE ID_BANK='{$id_bank}'");
        return $this->db->FetchAssoc();
    }

    function get_fakultas($id_fakultas) {
        if ($id_fakultas != '') {
            $this->db->Query("SELECT * FROM FAKULTAS WHERE ID_FAKULTAS={$id_fakultas}");
            return $this->db->FetchAssoc();
        }
        
    }

    function get_semester($id_semester) {
        $this->db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER={$id_semester}");
        return $this->db->FetchAssoc();
    }

    function get_prodi($id_prodi) {
        if ($id_prodi != '') {
            $this->db->Query("SELECT * FROM PROGRAM_STUDI PS JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG WHERE ID_PROGRAM_STUDI={$id_prodi}");
            return $this->db->FetchAssoc();
        }
    }

    function get_jalur($id_jalur) {
        $this->db->Query("SELECT * FROM JALUR WHERE ID_JALUR ='{$id_jalur}'");
        return $this->db->FetchAssoc();
    }

    function get_status($id_status) {
        $this->db->Query("SELECT * FROM STATUS_PEMBAYARAN WHERE ID_STATUS_PEMBAYARAN ='{$id_status}'");
        return $this->db->FetchAssoc();
    }
    
    function get_penerimaan($id){
        $this->db->Query("
            SELECT PEN.*,JAL.NM_JALUR
            FROM PENERIMAAN PEN
            JOIN JALUR JAL ON PEN.ID_JALUR=PEN.ID_JALUR
            WHERE PEN.ID_PENERIMAAN='{$id}'
            ");
        return $this->db->FetchAssoc();
    }

}

?>
