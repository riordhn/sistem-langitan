<?php

class pendaftaran {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    //Jadwal Verifikasi Keuangan

    function load_jadwal_verifikasi_keuangan($penerimaan = null) {
        if (empty($penerimaan))
            return $this->db->QueryToArray("
            SELECT JVK.*,PEN.GELOMBANG,PEN.SEMESTER,PEN.NM_PENERIMAAN,PEN.TAHUN,
            (
                SELECT COUNT(*)
                FROM CALON_MAHASISWA_BARU CMB
                JOIN CALON_MAHASISWA_DATA CMD ON CMB.ID_C_MHS=CMD.ID_C_MHS
                WHERE CMB.ID_PENERIMAAN=JVK.ID_PENERIMAAN 
                AND CMD.ID_JADWAL_VERIFIKASI_KEUANGAN=JVK.ID_JADWAL
            ) JUMLAH_MHS,
            (
                SELECT COUNT(*)
                FROM CALON_MAHASISWA_BARU CMB
                JOIN CALON_MAHASISWA_DATA CMD ON CMB.ID_C_MHS=CMD.ID_C_MHS
                WHERE CMB.ID_PENERIMAAN=JVK.ID_PENERIMAAN 
                AND CMD.ID_JADWAL_VERIFIKASI_KEUANGAN=JVK.ID_JADWAL
                AND TGL_VERIFIKASI_KEUANGAN IS NOT NULL
            ) SUDAH_VERIFIKASI,
            (
                SELECT COUNT(*)
                FROM CALON_MAHASISWA_BARU CMB
                JOIN CALON_MAHASISWA_DATA CMD ON CMB.ID_C_MHS=CMD.ID_C_MHS
                WHERE CMB.ID_PENERIMAAN=JVK.ID_PENERIMAAN 
                AND CMD.ID_JADWAL_VERIFIKASI_KEUANGAN=JVK.ID_JADWAL
                AND TGL_VERIFIKASI_KEUANGAN IS NULL
            ) BELUM_VERIFIKASI
            FROM JADWAL_VERIFIKASI_KEUANGAN JVK
            JOIN PENERIMAAN PEN ON PEN.ID_PENERIMAAN=JVK.ID_PENERIMAAN
            ORDER BY JVK.TGL_JADWAL
            ");
        else {
            return $this->db->QueryToArray("
            SELECT JVK.*,PEN.GELOMBANG,PEN.SEMESTER,PEN.NM_PENERIMAAN,PEN.TAHUN
            FROM JADWAL_VERIFIKASI_KEUANGAN JVK
            JOIN PENERIMAAN PEN ON PEN.ID_PENERIMAAN=JVK.ID_PENERIMAAN
            WHERE PEN.ID_PENERIMAAN='{$penerimaan}'
            ORDER BY JVK.TGL_JADWAL
            ");
        }
    }

    function get_jadwal_verifikasi_keuangan($id) {
        $this->db->Query("
            SELECT JVK.*,PEN.GELOMBANG,PEN.SEMESTER,PEN.NM_PENERIMAAN,PEN.TAHUN
            FROM JADWAL_VERIFIKASI_KEUANGAN JVK
            JOIN PENERIMAAN PEN ON PEN.ID_PENERIMAAN=JVK.ID_PENERIMAAN
            WHERE JVK.ID_JADWAL='{$id}'");
        return $this->db->FetchAssoc();
    }

    function tambah_jadwal_verifikasi_keuangan($penerimaan, $tanggal, $status, $quota) {
        $this->db->Query("
            INSERT INTO JADWAL_VERIFIKASI_KEUANGAN 
                (ID_PENERIMAAN,TGL_JADWAL,STATUS_MABA,KUOTA)
            VALUES
                ('{$penerimaan}','{$tanggal}','{$status}','{$quota}')");
    }

    function update_jadwal_verifikasi_keuangan($id, $penerimaan, $tanggal, $status, $quota) {
        $this->db->Query("
            UPDATE JADWAL_VERIFIKASI_KEUANGAN
            SET 
                ID_PENERIMAAN='{$penerimaan}',
                TGL_JADWAL='{$tanggal}',
                STATUS_MABA='{$status}',
                KUOTA='{$quota}'
            WHERE ID_JADWAL='{$id}'");
    }

    //verifikasi keuangan
    function get_id_cmhs($no_ujian) {
        return $this->db->QuerySingle("SELECT ID_C_MHS FROM CALON_MAHASISWA_BARU WHERE NO_UJIAN='{$no_ujian}'");
    }

    function get_semester_cmhs($id_c_mhs) {
        return $this->db->QuerySingle("
            SELECT S.ID_SEMESTER
            FROM CALON_MAHASISWA_BARU CMB
            JOIN PENERIMAAN PEN ON PEN.ID_PENERIMAAN=CMB.ID_PENERIMAAN
            JOIN SEMESTER S ON (S.TIPE_SEMESTER = 'REG' AND S.THN_AKADEMIK_SEMESTER = PEN.TAHUN 
              AND SUBSTR(S.NM_SEMESTER,0,2) LIKE SUBSTR(PEN.SEMESTER,0,2))
            WHERE CMB.ID_C_MHS='{$id_c_mhs}'
            ");
    }

    function get_biaya_kuliah_recom_cmhs($no_ujian) {
        $this->db->Query("
            SELECT BK.ID_BIAYA_KULIAH,KB.NM_KELOMPOK_BIAYA,SUM(DB.BESAR_BIAYA) BESAR_BIAYA_KULIAH
            FROM CALON_MAHASISWA_BARU CMB
            JOIN PENERIMAAN PEN ON PEN.ID_PENERIMAAN=CMB.ID_PENERIMAAN
            JOIN SEMESTER S ON (S.TIPE_SEMESTER = 'REG' AND S.THN_AKADEMIK_SEMESTER = PEN.TAHUN 
              AND SUBSTR(S.NM_SEMESTER,0,2) LIKE SUBSTR(PEN.SEMESTER,0,2))
            JOIN BIAYA_KULIAH BK ON 
                BK.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI AND
                BK.ID_JALUR=CMB.ID_JALUR AND 
                S.ID_SEMESTER=BK.ID_SEMESTER AND
                BK.ID_KELOMPOK_BIAYA=CMB.ID_KELOMPOK_BIAYA
            JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH=BK.ID_BIAYA_KULIAH    
            JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA=BK.ID_KELOMPOK_BIAYA
            WHERE CMB.NO_UJIAN='{$no_ujian}'
            GROUP BY BK.ID_BIAYA_KULIAH,KB.NM_KELOMPOK_BIAYA");
        return $this->db->FetchAssoc();
    }

    function get_biaya_kuliah_cmhs($id_c_mhs) {
        return $this->db->QuerySingle("
            SELECT ID_BIAYA_KULIAH FROM
            BIAYA_KULIAH_CMHS WHERE ID_C_MHS='{$id_c_mhs}'");
    }

    function get_sp3_master_biaya_cmhs($id_biaya_kuliah) {
        return $this->db->QuerySingle("
        SELECT BESAR_BIAYA FROM DETAIL_BIAYA WHERE ID_BIAYA_KULIAH='{$id_biaya_kuliah}' AND ID_BIAYA='81'");
    }

    function load_pilihan_kelompok_biaya_cmhs($no_ujian) {
        return $this->db->QueryToArray("
            SELECT BK.ID_BIAYA_KULIAH,KB.NM_KELOMPOK_BIAYA 
            FROM CALON_MAHASISWA_BARU CMB
            JOIN PENERIMAAN PEN ON PEN.ID_PENERIMAAN=CMB.ID_PENERIMAAN
            JOIN SEMESTER S ON (S.TIPE_SEMESTER = 'REG' AND S.THN_AKADEMIK_SEMESTER = PEN.TAHUN AND SUBSTR(S.NM_SEMESTER,0,2) LIKE SUBSTR(PEN.SEMESTER,0,2))
            JOIN BIAYA_KULIAH BK ON 
                BK.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI AND
                BK.ID_JALUR=CMB.ID_JALUR AND 
                S.ID_SEMESTER=BK.ID_SEMESTER
            JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA=BK.ID_KELOMPOK_BIAYA
            WHERE CMB.NO_UJIAN='{$no_ujian}'");
    }

    function update_biaya_kuliah_cmhs($id_c_mhs, $id_biaya_kuliah) {
        // $this->db->Query("SELECT * FROM BIAYA_KULIAH WHERE ID_BIAYA_KULIAH='{$id_biaya_kuliah}'");
        //$data_biaya_kuliah = $this->db->FetchAssoc();
        $biaya_kuliah_cmhs = $this->db->QuerySingle("SELECT COUNT(*) FROM BIAYA_KULIAH_CMHS WHERE ID_C_MHS={$id_c_mhs}");
        if ($biaya_kuliah_cmhs == 0) {
            // $this->db->Query("UPDATE CALON_MAHASISWA_BARU SET ID_KELOMPOK_BIAYA ='{$data_biaya_kuliah['ID_KELOMPOK_BIAYA']}' WHERE ID_C_MHS='{$id_c_mhs}'");
            $this->db->Query("INSERT INTO BIAYA_KULIAH_CMHS (ID_BIAYA_KULIAH,ID_C_MHS) VALUES ('{$id_biaya_kuliah}','{$id_c_mhs}')");
        } else {
            //$this->db->Query("UPDATE CALON_MAHASISWA_BARU SET ID_KELOMPOK_BIAYA ='{$data_biaya_kuliah['ID_KELOMPOK_BIAYA']}' WHERE ID_C_MHS='{$id_c_mhs}'");
            $this->db->Query("UPDATE BIAYA_KULIAH_CMHS SET ID_BIAYA_KULIAH='{$id_biaya_kuliah}' WHERE ID_C_MHS='{$id_c_mhs}'");
        }
    }

    function update_sp3_cmhs($id_c_mhs, $program_studi, $pilihan1, $pilihan2, $pilihan3, $besar_sp3) {
        if ($program_studi == $pilihan1) {
            $this->db->Query("UPDATE CALON_MAHASISWA_BARU SET SP3_1='{$besar_sp3}' WHERE ID_C_MHS='{$id_c_mhs}'");
        } else if ($program_studi == $pilihan2) {
            $this->db->Query("UPDATE CALON_MAHASISWA_BARU SET SP3_2='{$besar_sp3}' WHERE ID_C_MHS='{$id_c_mhs}'");
        } else if ($program_studi == $pilihan3) {
            $this->db->Query("UPDATE CALON_MAHASISWA_BARU SET SP3_3='{$besar_sp3}' WHERE ID_C_MHS='{$id_c_mhs}'");
        } else {
            $this->db->Query("UPDATE CALON_MAHASISWA_BARU SET SP3_4='{$besar_sp3}' WHERE ID_C_MHS='{$id_c_mhs}'");
        }
    }

    function get_sp3_cmhs($id_c_mhs, $program_studi, $pilihan1, $pilihan2, $pilihan3) {
        if ($program_studi == $pilihan1) {
            $sp3 = $this->db->QuerySingle("SELECT SP3_1 FROM CALON_MAHASISWA_BARU WHERE ID_C_MHS='{$id_c_mhs}'");
        } else if ($program_studi == $pilihan2) {
            $sp3 = $this->db->QuerySingle("SELECT SP3_2 FROM CALON_MAHASISWA_BARU WHERE ID_C_MHS='{$id_c_mhs}'");
        } else if ($program_studi == $pilihan3) {
            $sp3 = $this->db->QuerySingle("SELECT SP3_3 FROM CALON_MAHASISWA_BARU WHERE ID_C_MHS='{$id_c_mhs}'");
        } else {
            $sp3 = $this->db->QuerySingle("SELECT SP3_4 FROM CALON_MAHASISWA_BARU WHERE ID_C_MHS='{$id_c_mhs}'");
        }
        return $sp3;
    }

    function generate_biaya_cmhs($id_c_mhs, $ukt, $id_biaya_kuliah) {
        $semester_cmhs = $this->get_semester_cmhs($id_c_mhs);

        $pembayaran = $this->db->QuerySingle("SELECT COUNT(*) FROM PEMBAYARAN_CMHS WHERE ID_C_MHS={$id_c_mhs}");
        if ($pembayaran == 0) {
            // generate biaya
            $this->db->Query("
                    INSERT INTO PEMBAYARAN_CMHS 
                        (ID_C_MHS,ID_SEMESTER,ID_DETAIL_BIAYA,BESAR_BIAYA,IS_TAGIH,ID_SEMESTER_BAYAR,ID_STATUS_PEMBAYARAN)
                    SELECT ID_C_MHS,'{$semester_cmhs}' ID_SEMESTER,ID_DETAIL_BIAYA,BESAR_BIAYA,'Y' IS_TAGIH,'{$semester_cmhs}' ID_SEMESTER_BAYAR,'2' ID_STATUS_PEMBAYARAN
                    FROM BIAYA_KULIAH_CMHS BKC
                    JOIN DETAIL_BIAYA DB ON BKC.ID_BIAYA_KULIAH=DB.ID_BIAYA_KULIAH
                    WHERE BKC.ID_C_MHS='{$id_c_mhs}'");
            // update biaya UKT
            if ($ukt != '-') {
                $this->db->Query("
                    UPDATE PEMBAYARAN_CMHS SET BESAR_BIAYA='{$ukt}'
                    WHERE ID_C_MHS='{$id_c_mhs}' AND ID_DETAIL_BIAYA IN (
                        SELECT ID_DETAIL_BIAYA
                        FROM DETAIL_BIAYA 
                        WHERE ID_BIAYA_KULIAH='{$id_biaya_kuliah}' AND ID_BIAYA=134
                    )");
            }
            $status = "Biaya Kuliah Berhasil di generate...";
        } else {
            $this->db->Query("DELETE FROM PEMBAYARAN_CMHS WHERE ID_C_MHS='{$id_c_mhs}'");
            $this->db->Query("
                    INSERT INTO PEMBAYARAN_CMHS 
                        (ID_C_MHS,ID_SEMESTER,ID_DETAIL_BIAYA,BESAR_BIAYA,IS_TAGIH,ID_SEMESTER_BAYAR,ID_STATUS_PEMBAYARAN)
                    SELECT ID_C_MHS,'{$semester_cmhs}' ID_SEMESTER,ID_DETAIL_BIAYA,BESAR_BIAYA,'Y' IS_TAGIH,'{$semester_cmhs}' ID_SEMESTER_BAYAR,'2' ID_STATUS_PEMBAYARAN
                    FROM BIAYA_KULIAH_CMHS BKC
                    JOIN DETAIL_BIAYA DB ON BKC.ID_BIAYA_KULIAH=DB.ID_BIAYA_KULIAH
                    WHERE BKC.ID_C_MHS='{$id_c_mhs}'");
            // update biaya UKT
            if ($ukt != '-') {
                $this->db->Query("
                    UPDATE PEMBAYARAN_CMHS SET BESAR_BIAYA='{$ukt}'
                    WHERE ID_C_MHS='{$id_c_mhs}' AND ID_DETAIL_BIAYA IN (
                        SELECT ID_DETAIL_BIAYA
                        FROM DETAIL_BIAYA 
                        WHERE ID_BIAYA_KULIAH='{$id_biaya_kuliah}' AND ID_BIAYA=134
                    )");
            }
            $status = "Biaya Kuliah Berhasil di generate ulang...";
        }
        return $status;
    }

    function get_no_invoice_cmhs($id_c_mhs) {
        return $this->db->QuerySingle("SELECT NO_INVOICE FROM CALON_MAHASISWA_BARU WHERE ID_C_MHS='{$id_c_mhs}'");
    }

    function generate_no_invoice_cmhs($id_penerimaan) {
        $tahun_invoice = date('Y');
        $no_urut_invoice = $this->db->QuerySingle("SELECT COUNT(*) URUTAN FROM CALON_MAHASISWA_BARU WHERE ID_PENERIMAAN='{$id_penerimaan}' AND TGL_VERIFIKASI_KEUANGAN IS NOT NULL");
        return $tahun_invoice . str_pad($no_urut_invoice + 1, 5, "0", STR_PAD_LEFT);
    }

    function update_no_invoice_cmhs($id_c_mhs, $no_invoice) {
        $cek_no_invoice = $this->db->QuerySingle("SELECT NO_INVOICE FROM CALON_MAHASISWA_BARU WHERE ID_C_MHS='{$id_c_mhs}'");
        if ($cek_no_invoice == '') {
            $this->db->Query("UPDATE CALON_MAHASISWA_BARU SET NO_INVOICE='{$no_invoice}' WHERE ID_C_MHS='{$id_c_mhs}'");
        }
    }

    function update_verifikator_keuangan($id_c_mhs, $pengguna) {
        $this->db->Query("UPDATE CALON_MAHASISWA_BARU SET ID_VERIFIKATOR_KEUANGAN ='{$pengguna}',TGL_VERIFIKASI_KEUANGAN=CURRENT_TIMESTAMP WHERE ID_C_MHS='{$id_c_mhs}'");
    }

    // Cek status Pembayaran 

    function cek_status_bayar($id_c_mhs) {
        return $this->db->QuerySingle("
            SELECT COUNT(*)
            FROM PEMBAYARAN_CMHS
            WHERE ID_C_MHS='{$id_c_mhs}'
            AND TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL");
    }

    // Potongan SP3

    function get_biaya_kuliah_verifikasi($id_c_mhs) {
        return $this->db->QuerySingle("
            SELECT SUM(BESAR_BIAYA) BIAYA
            FROM PEMBAYARAN_CMHS WHERE ID_C_MHS='{$id_c_mhs}'
            AND TGL_BAYAR IS NULL AND ID_BANK IS NULL");
    }

    function get_biaya_sp3_verifikasi($id_c_mhs) {
        return $this->db->QuerySingle("
            SELECT PC.BESAR_BIAYA
            FROM PEMBAYARAN_CMHS PC
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PC.ID_DETAIL_BIAYA
            WHERE PC.ID_C_MHS='{$id_c_mhs}'
            AND TGL_BAYAR IS NULL AND ID_BANK IS NULL 
            AND DB.ID_BIAYA=81");
    }

    function get_kelompok_biaya_verifikasi($id_c_mhs) {
        return $this->db->QuerySingle("
            SELECT NM_KELOMPOK_BIAYA
            FROM CALON_MAHASISWA_BARU CMB
            JOIN KELOMPOK_BIAYA KB ON CMB.ID_KELOMPOK_BIAYA=KB.ID_KELOMPOK_BIAYA
            WHERE CMB.ID_C_MHS='{$id_c_mhs}'");
    }

    function update_sp3_potongan($id_c_mhs, $sp3_potongan) {
        $this->db->Query("
            UPDATE PEMBAYARAN_CMHS
                SET BESAR_BIAYA='{$sp3_potongan}'
            WHERE ID_C_MHS='{$id_c_mhs}'
            AND ID_PEMBAYARAN_CMHS IN (
                SELECT ID_PEMBAYARAN_CMHS FROM AUCC.PEMBAYARAN_CMHS PC
                JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PC.ID_DETAIL_BIAYA
                WHERE DB.ID_BIAYA=81 AND PC.ID_C_MHS='{$id_c_mhs}'
            )");
    }

    // Pindah Profesi

    function load_pindah_profesi($prodi, $semester) {
        return $this->db->QueryToArray("
            SELECT PB.NM_PENGGUNA NAMA_B,MB.ID_MHS ID_B,MB.NIM_MHS NIM_B,JB.NM_JENJANG JENJANG_B,PSB.NM_PROGRAM_STUDI PRODI_B,
          ML.ID_MHS ID_L,ML.NIM_MHS NIM_L,JL.NM_JENJANG JENJANG_L,PSL.NM_PROGRAM_STUDI PRODI_L,
          A2.ADA ADA_B,SB2.SUDAH_BAYAR SBB,A1.ADA ADA_L,SB1.SUDAH_BAYAR SBL
            FROM AUCC.MAHASISWA MB
            JOIN AUCC.PENGGUNA PB ON PB.ID_PENGGUNA=MB.ID_PENGGUNA
            JOIN AUCC.PROGRAM_STUDI PSB ON PSB.ID_PROGRAM_STUDI=MB.ID_PROGRAM_STUDI
            JOIN AUCC.JENJANG JB ON JB.ID_JENJANG=PSB.ID_JENJANG
            JOIN AUCC.MAHASISWA ML ON ML.NIM_MHS=MB.NIM_LAMA
            JOIN AUCC.PENGGUNA PL ON PL.ID_PENGGUNA=ML.ID_PENGGUNA
            JOIN AUCC.PROGRAM_STUDI PSL ON PSL.ID_PROGRAM_STUDI=ML.ID_PROGRAM_STUDI
            JOIN AUCC.JENJANG JL ON JL.ID_JENJANG=PSL.ID_JENJANG
            LEFT JOIN (
                SELECT ID_MHS,COUNT(ID_PEMBAYARAN) ADA
                FROM AUCC.PEMBAYARAN
                WHERE ID_SEMESTER='{$semester}'
                GROUP BY ID_MHS
            ) A1 ON A1.ID_MHS=ML.ID_MHS
            LEFT JOIN (
                SELECT ID_MHS,COUNT(ID_PEMBAYARAN) SUDAH_BAYAR
                FROM AUCC.PEMBAYARAN
                WHERE ID_SEMESTER='{$semester}' AND ((TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL) OR ID_STATUS_PEMBAYARAN=1)
                GROUP BY ID_MHS
            ) SB1 ON SB1.ID_MHS=ML.ID_MHS
            LEFT JOIN (
                SELECT ID_MHS,COUNT(ID_PEMBAYARAN) ADA
                FROM AUCC.PEMBAYARAN
                WHERE ID_SEMESTER='{$semester}'
                GROUP BY ID_MHS
            ) A2 ON A2.ID_MHS=MB.ID_MHS
            LEFT JOIN (
                SELECT ID_MHS,COUNT(ID_PEMBAYARAN) SUDAH_BAYAR
                FROM AUCC.PEMBAYARAN
                WHERE ID_SEMESTER='{$semester}' AND ((TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL) OR ID_STATUS_PEMBAYARAN=1)
                GROUP BY ID_MHS
            ) SB2 ON SB2.ID_MHS=MB.ID_MHS
            WHERE MB.ID_PROGRAM_STUDI='{$prodi}' AND MB.ID_SEMESTER_MASUK='{$semester}'
            ");
    }

    // Get UKT Calon Mahasiswa
    function get_ukt_cmhs($id_c_mhs, $id_biaya_kuliah) {
        return $this->db->QuerySingle("
            SELECT BESAR_BIAYA FROM PEMBAYARAN_CMHS
            WHERE ID_C_MHS='{$id_c_mhs}' AND ID_DETAIL_BIAYA IN (
                SELECT ID_DETAIL_BIAYA
                FROM DETAIL_BIAYA 
                WHERE ID_BIAYA_KULIAH='{$id_biaya_kuliah}' AND ID_BIAYA=134
            )
            ");
    }

}

?>
