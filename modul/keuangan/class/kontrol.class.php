<?php

class kontrol {

    public $db;

    function __construct($db) {
        $this->db = $db;
        // AUTO INSERT RUMAH REKON
        $date = getdate();
        for ($y = intval($date['year']); $y >= (intval($date['year']) - 2); $y--) {
            for ($m = 1; $m <= 12; $m++) {
                $cek_rekon = $db->QuerySingle("SELECT COUNT(*) FROM REKON_PEMBAYARAN WHERE BULAN_REKON='{$m}' AND TAHUN_REKON='{$y}'");
                if ($cek_rekon == 0) {
                    $this->insert_rekon($m, $y);
                }
            }
        }
    }

    function insert_rekon($mon, $year) {
        $this->db->Query("
            INSERT INTO REKON_PEMBAYARAN
                (BULAN_REKON,TAHUN_REKON,STATUS_REKON)
            VALUES
                ('{$mon}','{$year}','0')
            ");
    }

    function load_rekon($year) {
        return $this->db->QueryToArray("
            SELECT RP.*,(SELECT COUNT(*) FROM REKON_PERUBAHAN WHERE ID_REKON_PEMBAYARAN=RP.ID_REKON_PEMBAYARAN) PERUBAHAN FROM REKON_PEMBAYARAN RP
            WHERE TAHUN_REKON='{$year}'
            ORDER BY TAHUN_REKON,BULAN_REKON
            ");
    }

}

?>
