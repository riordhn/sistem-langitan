<?php
$id_pt= $id_pt_user;

class upload {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    function get_mahasiswa($nim) {
        $this->db->Query("SELECT P.NM_PENGGUNA,J.NM_JENJANG || ' - ' || PS.NM_PROGRAM_STUDI AS PRODI, M.* 
            FROM MAHASISWA M 
            JOIN PENGGUNA P ON P.ID_PENGGUNA= M.ID_PENGGUNA 
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            WHERE NIM_MHS='{$nim}' AND P.ID_PERGURUAN_TINGGI='".getenv('ID_PT')."' ");
        return $this->db->FetchAssoc();
    }

    // 21072017 fth
    function get_semester($tahun,$nama_semester){
        $this->db->Query("SELECT ID_SEMESTER FROM SEMESTER  WHERE THN_AKADEMIK_SEMESTER = '{$tahun}' 
                AND UPPER(NM_SEMESTER) = UPPER('{$nama_semester}') AND ID_PERGURUAN_TINGGI='".getenv('ID_PT')."' AND TIPE_SEMESTER ='REG' ");
        return $this->db->FetchAssoc();
    }

    function get_bulan($nama_bulan){
        $this->db->Query("SELECT ID_BULAN FROM BULAN  WHERE UPPER(NM_BULAN) = UPPER('{$nama_bulan}') ");
        return $this->db->FetchAssoc();
    }

   /* function get_id_tagihan_pembayaran($id_tagihan){
        $this->db->Query("SELECT ID_TAGIHAN FROM TAGIHAN TGH JOIN PEMBAYARAN PEMB.ID_TAGIHAN = TAGIHAN.ID_TAGIHAN 
            WHERE TAGIHAN.ID_TAGIHAN = '{$id_tagihan}'");

        $this->db->Query("SELECT * FROM PEMBAYARAN WHERE ID_TAGIHAN IN (SELECT MAX(ID_TAGIHAN) FROM PEMBAYARAN");

    }*/

   /* function get_detail_biaya($nama_biaya,$besar_biaya){
        this->db->Query("");
        return $this->db->FetchAssoc();
    }*/





    function cek_mahasiswa($nim) {
        return $this->db->QueryToArray("SELECT * FROM MAHASISWA WHERE NIM_MHS='{$nim}'");
    }

    function cek_sudah_bayar_status($id_mhs, $id_semester) {
        $sudah = $this->db->QuerySingle("SELECT COUNT(*) FROM PEMBAYARAN WHERE ID_SEMESTER='{$id_semester}' AND ID_MHS='{$id_mhs}' AND ID_STATUS_PEMBAYARAN=1");
        return $sudah > 0 ? true : false;
    }

    function cek_detail_biaya($nim_mhs, $semester, $id_biaya, $tgl_bayar, $id_bank) {
        $sudah_dimasukkan = $this->db->QueryToArray("
            SELECT M.ID_MHS FROM MAHASISWA M
            JOIN PEMBAYARAN PEM ON M.ID_MHS = PEM.ID_MHS
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
            WHERE PEM.ID_SEMESTER = '{$semester}' AND M.NIM_MHS='{$nim_mhs}' AND DB.ID_BIAYA ='{$id_biaya}'
            AND PEM.TGL_BAYAR =UPPER('{$tgl_bayar}') AND PEM.ID_BANK ='{$id_bank}'");
        $sudah_bayar = $this->db->QueryToArray("
            SELECT M.ID_MHS FROM MAHASISWA M
            JOIN PEMBAYARAN PEM ON M.ID_MHS = PEM.ID_MHS
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
            WHERE PEM.ID_SEMESTER = '{$semester}' AND M.NIM_MHS='{$nim_mhs}' AND DB.ID_BIAYA ='{$id_biaya}'
            AND PEM.TGL_BAYAR IS NOT NULL AND PEM.ID_BANK IS NOT NULL");
        $diloloskan = $this->db->QueryToArray("SELECT M.ID_MHS FROM MAHASISWA M
            JOIN PEMBAYARAN PEM ON M.ID_MHS = PEM.ID_MHS
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
            WHERE PEM.ID_SEMESTER = '{$semester}' AND M.NIM_MHS='{$nim_mhs}' AND DB.ID_BIAYA ='{$id_biaya}'
            AND (PEM.TGL_BAYAR IS NOT NULL OR PEM.TGL_BAYAR IS NULL) AND PEM.ID_BANK IS NULL");
        if ($sudah_dimasukkan != null) {
            $status_bayar = 4;
        } else if ($sudah_bayar != null && $diloloskan == null && $sudah_dimasukkan == null) {
            $status_bayar = 1;
        } else if ($sudah_bayar == null && $diloloskan != null) {
            $status_bayar = 2;
        } else {
            $status_bayar = 0;
        }
        return $status_bayar;
    }

    function get_id_detail_biaya($nim, $id_biaya) {
        $this->db->Query("SELECT DB.ID_DETAIL_BIAYA FROM MAHASISWA M
            JOIN PEMBAYARAN PEM ON M.ID_MHS = PEM.ID_MHS
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
            WHERE M.NIM_MHS='{$nim}' AND DB.ID_BIAYA ='{$id_biaya}'
            ");
        $data_biaya = $this->db->FetchAssoc();
        if (empty($data_biaya)) {
            $this->db->Query("SELECT DB.ID_DETAIL_BIAYA FROM DETAIL_BIAYA DB
            WHERE DB.ID_BIAYA ='{$id_biaya}'
            ");
            $data_biaya = $this->db->FetchAssoc();
        }
        return $data_biaya['ID_DETAIL_BIAYA'];
    }

    function get_id_semester_aktif() {
        return $this->db->QuerySingle("SELECT ID_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_PEMBAYARAN='True'");
    }

    function insert($id_mhs, $semester, $id_detail_biaya, $id_bank, $id_bank_via, $tgl_bayar, $keterangan, $besar_biaya) {
        $id_semester = $this->get_id_semester_aktif();
//        echo "INSERT INTO PEMBAYARAN
//                (ID_MHS,ID_SEMESTER,ID_DETAIL_BIAYA,ID_BANK,ID_BANK_VIA,TGL_BAYAR,KETERANGAN,BESAR_BIAYA,IS_TAGIH,ID_STATUS_PEMBAYARAN,ID_SEMESTER_BAYAR)
//            VALUES
//                ('{$id_mhs}','{$semester}','{$id_detail_biaya}','{$id_bank}','{$id_bank_via}','{$tgl_bayar}','{$keterangan}','{$besar_biaya}','T',1,'{$id_semester}')"."<br/>";
        $this->db->Query("
            INSERT INTO PEMBAYARAN
                (ID_MHS,ID_SEMESTER,ID_DETAIL_BIAYA,ID_BANK,ID_BANK_VIA,TGL_BAYAR,KETERANGAN,BESAR_BIAYA,IS_TAGIH,ID_STATUS_PEMBAYARAN,ID_SEMESTER_BAYAR,FLAG_PEMINDAHAN)
            VALUES
                ('{$id_mhs}','{$semester}','{$id_detail_biaya}','{$id_bank}','{$id_bank_via}','{$tgl_bayar}','{$keterangan}','{$besar_biaya}','T',1,'{$id_semester}','')");
    }

    function update($id_bank, $id_bank_via, $tgl_bayar, $keterangan, $besar_biaya, $nim_mhs, $semester, $id_biaya) {
        $id_semester = $this->get_id_semester_aktif();
        $char_not_allowed = array(',', '.', '-');
        $besar_biaya = str_replace($char_not_allowed, '', $besar_biaya);
        //update biaya yang di masukkan
        $this->db->Query("
            UPDATE PEMBAYARAN SET
                ID_BANK='{$id_bank}',
                ID_BANK_VIA='{$id_bank_via}',
                TGL_BAYAR='{$tgl_bayar}',
                BESAR_BIAYA='{$besar_biaya}',
                KETERANGAN='{$keterangan}',
                IS_TAGIH='T',
                FLAG_PEMINDAHAN='',
                ID_STATUS_PEMBAYARAN=1,
                ID_SEMESTER_BAYAR='{$id_semester}'
            WHERE ID_PEMBAYARAN IN (
                SELECT PEM.ID_PEMBAYARAN FROM MAHASISWA M
                JOIN PEMBAYARAN PEM ON M.ID_MHS = PEM.ID_MHS
                JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
                WHERE PEM.ID_SEMESTER = '{$semester}' AND M.NIM_MHS='{$nim_mhs}' AND DB.ID_BIAYA ='{$id_biaya}'
                AND (PEM.TGL_BAYAR IS NOT NULL OR PEM.TGL_BAYAR IS NULL) AND PEM.ID_BANK IS NULL AND PEM.ID_BANK_VIA IS NULL
            )");
    }

    function update_pembayaran_not_in_excel($id_bank, $id_bank_via, $tgl_bayar, $keterangan, $nim_mhs, $semester, $id_biaya) {
        $id_semester = $this->get_id_semester_aktif();
        $this->db->Query("
            UPDATE PEMBAYARAN SET
                ID_BANK='{$id_bank}',
                ID_BANK_VIA='{$id_bank_via}',
                TGL_BAYAR='{$tgl_bayar}',
                BESAR_BIAYA='0',
                KETERANGAN='{$keterangan}',
                IS_TAGIH='T',
                ID_STATUS_PEMBAYARAN=1,
                ID_SEMESTER_BAYAR='{$id_semester}'
            WHERE ID_PEMBAYARAN IN (
                SELECT PEM.ID_PEMBAYARAN FROM MAHASISWA M
                JOIN PEMBAYARAN PEM ON M.ID_MHS = PEM.ID_MHS
                JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
                WHERE PEM.ID_SEMESTER = '{$semester}' AND M.NIM_MHS='{$nim_mhs}' AND DB.ID_BIAYA NOT IN ({$id_biaya})
                AND (PEM.TGL_BAYAR IS NOT NULL OR PEM.TGL_BAYAR IS NULL) AND PEM.ID_BANK IS NULL AND PEM.ID_BANK_VIA IS NULL
            )");
    }

    //fungsi upload mahasiswa kerjasama

    function cek_mahasiswa_kerjasama($nim) {
        $this->db->Query("SELECT B.*,M.NIM_MHS,M.ID_MHS FROM SEJARAH_BEASISWA SB
            JOIN BEASISWA B ON SB.ID_BEASISWA=B.ID_BEASISWA
            JOIN MAHASISWA M ON M.ID_MHS=SB.ID_MHS
            WHERE M.NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function non_aktif_mahasiswa_kerjasama($id_mhs) {
        $this->db->Query("UPDATE SEJARAH_BEASISWA SET BEASISWA_AKTIF=0 WHERE ID_MHS='{$id_mhs}'");
    }

    function insert_mahasiswa_kerjasama($id_mhs, $id_beasiswa, $semester_mulai, $semester_selesai, $keterangan) {
        $this->db->Query("
                INSERT INTO SEJARAH_BEASISWA 
                    (ID_MHS,ID_BEASISWA,ID_SEMESTER_MULAI,ID_SEMESTER_SELESAI,KETERANGAN,BEASISWA_AKTIF) 
                VALUES 
                    ('{$id_mhs}','{$id_beasiswa}','{$semester_mulai}','{$semester_selesai}','{$keterangan}',1)");
    }

    // fungsi tambah pembayaran mahasiswa uploer

    function cek_biaya_tambah_pembayaran($id_mhs, $id_biaya, $semester) {
        return $this->db->QuerySingle("
            SELECT COUNT(*)
            FROM PEMBAYARAN PEM
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PEM.ID_DETAIL_BIAYA
            WHERE PEM.ID_MHS='{$id_mhs}' AND DB.ID_BIAYA='{$id_biaya}' AND PEM.ID_SEMESTER='{$semester}'");
    }

    function cek_biaya_tambah_pembayaran_sudah_bayar($id_mhs, $id_biaya, $semester) {
        return $this->db->QuerySingle("
            SELECT COUNT(*)
            FROM PEMBAYARAN PEM
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PEM.ID_DETAIL_BIAYA
            WHERE PEM.ID_MHS='{$id_mhs}' AND DB.ID_BIAYA='{$id_biaya}' AND PEM.ID_SEMESTER='{$semester}'
            AND PEM.TGL_BAYAR IS NOT NULL AND PEM.ID_BANK IS NOT NULL");
    }

    function cek_biaya_tambah_pembayaran_belum_bayar($id_mhs, $id_biaya, $semester) {
        return $this->db->QuerySingle("
            SELECT COUNT(*)
            FROM PEMBAYARAN PEM
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PEM.ID_DETAIL_BIAYA
            WHERE PEM.ID_MHS='{$id_mhs}' AND DB.ID_BIAYA='{$id_biaya}' AND PEM.ID_SEMESTER='{$semester}'
            AND PEM.TGL_BAYAR IS NULL AND PEM.ID_BANK IS NULL");
    }

    function get_biaya_tambah_pembayaran_sudah_bayar($id_mhs, $id_biaya, $semester) {
        return $this->db->QuerySingle("
            SELECT SUM(PEM.BESAR_BIAYA)
            FROM PEMBAYARAN PEM
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PEM.ID_DETAIL_BIAYA
            WHERE PEM.ID_MHS='{$id_mhs}' AND DB.ID_BIAYA='{$id_biaya}' AND PEM.ID_SEMESTER='{$semester}'
            AND PEM.TGL_BAYAR IS NOT NULL AND PEM.ID_BANK IS NOT NULL");
    }

    function get_biaya_tambah_pembayaran_belum_bayar($id_mhs, $id_biaya, $semester) {
        return $this->db->QuerySingle("
            SELECT SUM(PEM.BESAR_BIAYA)
            FROM PEMBAYARAN PEM
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PEM.ID_DETAIL_BIAYA
            WHERE PEM.ID_MHS='{$id_mhs}' AND DB.ID_BIAYA='{$id_biaya}' AND PEM.ID_SEMESTER='{$semester}'
            AND PEM.TGL_BAYAR IS NULL AND PEM.ID_BANK IS NULL");
    }

    function cek_biaya_master($id_mhs, $id_biaya) {
        return $this->db->QuerySingle("
            SELECT COUNT(*)
            FROM MAHASISWA M
            JOIN BIAYA_KULIAH_MHS BKM ON BKM.ID_MHS=M.ID_MHS
            JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH=BKM.ID_BIAYA_KULIAH
            WHERE M.ID_MHS='{$id_mhs}' AND DB.ID_BIAYA='{$id_biaya}'");
    }

    function get_biaya_master($id_mhs, $id_biaya) {
        return $this->db->QuerySingle("
            SELECT DB.BESAR_BIAYA
            FROM MAHASISWA M
            JOIN BIAYA_KULIAH_MHS BKM ON BKM.ID_MHS=M.ID_MHS
            JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH=BKM.ID_BIAYA_KULIAH
            WHERE M.ID_MHS='{$id_mhs}' AND DB.ID_BIAYA='{$id_biaya}'");
    }

}

?>
