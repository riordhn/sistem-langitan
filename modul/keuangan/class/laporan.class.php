<?php

class laporan
{

    public $db;
    public $id_pt;

    function __construct($db)
    {
        $this->db = $db;
        $this->id_pt = $GLOBALS['id_pt'];
    }

    private function get_condition_akademik($id_fakultas, $id_prodi)
    {
        if ($id_fakultas != "" && $id_prodi != "") {
            $query = " AND PS.ID_FAKULTAS = '{$id_fakultas}' AND PS.ID_PROGRAM_STUDI ='{$id_prodi}'";
        } else if ($id_fakultas != "") {
            $query = " AND PS.ID_FAKULTAS = '{$id_fakultas}' ";
        } else {
            $query = " ";
        }
        return $query;
    }

    private function get_condition_bank($id_bank)
    {
        if (empty($id_bank)) {
            $query = "AND PEM.ID_BANK IS NOT NULL";
        } else {
            if ($id_bank == 'NULL') {
                $query = "AND PEM.ID_BANK IS NULL";
            } else {
                $query = "AND PEM.ID_BANK={$id_bank}";
            }
        }
        return $query;
    }

    private function get_condition_jalur($id_jalur)
    {
        if ($id_jalur == '') {
            $query = "";
        } else {
            $query = "AND JAL.ID_JALUR='{$id_jalur}'";
        }
        return $query;
    }

    private function get_condition_angkatan($angkatan)
    {
        if (empty($angkatan)) {
            $query = "";
        } else {
            $query = "AND M.THN_ANGKATAN_MHS='{$angkatan}'";
        }
        return $query;
    }

    private function get_condition_angkatan_cmhs($angkatan)
    {
        if (empty($angkatan)) {
            $query = "";
        } else {
            $query = "AND PEN.TAHUN='{$angkatan}'";
        }
        return $query;
    }

    private function get_row_report_fakultas_mhs($tgl_awal, $tgl_akhir, $id_bank, $periode_bayar, $id_fakultas)
    {
        $c_bank = $this->get_condition_bank($id_bank);
        $condition_periode_bayar = !empty($periode_bayar) ? "AND PEM.PERIODE_BAYAR='{$periode_bayar}'" : "";
        if ($id_fakultas != '') {
            $query = "
            SELECT F.ID_FAKULTAS,UPPER(F.NM_FAKULTAS) NM_FAKULTAS,PS.ID_PROGRAM_STUDI,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,JAL.ID_JALUR,UPPER(JAL.NM_JALUR) NM_JALUR,M.THN_ANGKATAN_MHS,
            COUNT(DISTINCT(M.ID_MHS)) JUMLAH_MHS
            FROM MAHASISWA M 
            JOIN PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
            JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG 
            LEFT JOIN ADMISI JM ON JM.ID_MHS = M.ID_MHS AND JM.ID_JALUR IS NOT NULL
            LEFT JOIN JALUR JAL ON JAL.ID_JALUR = JM.ID_JALUR
            JOIN TAGIHAN_MHS TM ON TM.ID_MHS=M.ID_MHS
            JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN_MHS=TM.ID_TAGIHAN_MHS
            JOIN PEMBAYARAN PEM ON PEM.ID_TAGIHAN = TAG.ID_TAGIHAN
            WHERE F.ID_FAKULTAS='{$id_fakultas}' {$c_bank} {$condition_periode_bayar}
                AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' 
                AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}'
                AND F.ID_PERGURUAN_TINGGI = " . getenv('ID_PT') . "
                AND PEM.ID_STATUS_PEMBAYARAN='1'
            GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS,PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JAL.ID_JALUR,JAL.NM_JALUR,M.THN_ANGKATAN_MHS
            ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI,JAL.NM_JALUR,M.THN_ANGKATAN_MHS";
        } else {
            $query = "
            SELECT F.ID_FAKULTAS,UPPER(F.NM_FAKULTAS) NM_FAKULTAS,
            COUNT(DISTINCT(M.ID_MHS)) JUMLAH_MHS
            FROM MAHASISWA M 
            LEFT JOIN PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
            JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            JOIN TAGIHAN_MHS TM ON TM.ID_MHS=M.ID_MHS
            JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN_MHS=TM.ID_TAGIHAN_MHS
            JOIN PEMBAYARAN PEM ON PEM.ID_TAGIHAN = TAG.ID_TAGIHAN
            WHERE PEM.TGL_BAYAR IS NOT NULL AND PEM.ID_BANK IS NOT NULL
            {$c_bank} {$condition_periode_bayar}
            AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' 
            AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}'
            AND F.ID_PERGURUAN_TINGGI = " . getenv('ID_PT') . "
            AND PEM.ID_STATUS_PEMBAYARAN='1'
            GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS
            ORDER BY F.ID_FAKULTAS";
        }
        return $this->db->QueryToArray($query);
    }

    private function get_jumlah_mhs($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $periode_bayar, $id_bank, $id_jalur, $angkatan)
    {
        $condition_akademik = $this->get_condition_akademik($id_fakultas, $id_prodi);
        $condition_periode_bayar = !empty($periode_bayar) ? "AND PEM.PERIODE_BAYAR='{$periode_bayar}'" : "";
        $condition_bank = $this->get_condition_bank($id_bank);
        $condition_jalur = $this->get_condition_jalur($id_jalur);
        $condition_angkatan = $angkatan != '' ? "AND M.THN_ANGKATAN_MHS='{$angkatan}'" : "";
        $this->db->Query("
        SELECT
                COUNT(
                COUNT( TM.ID_MHS )) JUMLAH_MHS 
            FROM
                MAHASISWA M
                JOIN TAGIHAN_MHS TM ON TM.ID_MHS=M.ID_MHS
                JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN_MHS=TM.ID_TAGIHAN_MHS
                JOIN PEMBAYARAN PEM ON PEM.ID_TAGIHAN= TAG.ID_TAGIHAN
                LEFT JOIN JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS 
                AND JM.IS_JALUR_AKTIF = 1
                LEFT JOIN JALUR JAL ON JAL.ID_JALUR = JM.ID_JALUR
                JOIN PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI 
                JOIN FAKULTAS FAK ON FAK.ID_FAKULTAS=PS.ID_FAKULTAS
            WHERE TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' and TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' 
            {$condition_akademik} {$condition_bank} {$condition_periode_bayar} {$condition_jalur} AND PEM.ID_BANK IS NOT NULL {$condition_angkatan}
            AND FAK.ID_PERGURUAN_TINGGI=" . getenv('ID_PT') .
            "GROUP BY 
                PEM.NO_TRANSAKSI,
                PEM.TGL_BAYAR,
                TM.ID_MHS
            ");
        $data_jumlah_mhs = $this->db->FetchAssoc();
        $jumlah_mhs = $data_jumlah_mhs['JUMLAH_MHS'] == '' ? 0 : $data_jumlah_mhs['JUMLAH_MHS'];
        return $jumlah_mhs;
    }

    function load_report_pembayaran_va($tgl_awal, $tgl_akhir)
    {
        $query = "
        SELECT PVB.*,M.NIM_MHS,
        COALESCE((SELECT SUM(BESAR_PEMBAYARAN) FROM PEMBAYARAN WHERE NO_TRANSAKSI =PVB.PAYMENT_NTB ),0) PEMBAYARAN_DIPROSES
        FROM PEMBAYARAN_VA_BANK PVB
        LEFT JOIN MAHASISWA_VA MV ON MV.NO_VA=PVB.VIRTUAL_ACCOUNT AND PVB.TRX_ID=MV.TRX_ID
        LEFT JOIN MAHASISWA M ON M.ID_MHS=MV.ID_MHS
        WHERE TO_CHAR(PAYMENT_TIME,'YYYY-MM-DD') >= '{$tgl_awal}' and TO_CHAR(PAYMENT_TIME,'YYYY-MM-DD') <= '{$tgl_akhir}' 
        ORDER BY PVB.PAYMENT_TIME DESC
        ";
        return $this->db->QueryToArray($query);
    }

    function load_report_bank_mhs($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $periode_bayar)
    {
        $condition_akademik = $this->get_condition_akademik($id_fakultas, $id_prodi);
        $condition_periode_bayar = !empty($periode_bayar) ? "AND PEM.PERIODE_BAYAR='{$periode_bayar}'" : "";
        $query = "
		SELECT B.ID_BANK,B.NM_BANK,
		  SUM(CASE WHEN PEM.ID_MHS IS NULL
				THEN 0
			  ELSE
				PEM.ID_MHS
			  END) JUMLAH_MHS,
		  (CASE WHEN SUM(PEM.BESAR_BIAYA) IS NULL 
			  THEN 0
			ELSE 
			   SUM(PEM.BESAR_BIAYA)
			END) BESAR_BIAYA
		FROM BANK B
		LEFT JOIN (
            SELECT COUNT(DISTINCT(TM.ID_MHS)) ID_MHS,PEM.ID_BANK,SUM(PEM.BESAR_PEMBAYARAN) BESAR_BIAYA,0 DENDA_BIAYA 
            FROM MAHASISWA M
            LEFT JOIN ADMISI JM ON JM.ID_MHS = M.ID_MHS AND JM.ID_JALUR IS NOT NULL
            LEFT JOIN JALUR JAL ON JAL.ID_JALUR=JM.ID_JALUR 
            JOIN TAGIHAN_MHS TM ON TM.ID_MHS=M.ID_MHS
            JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN_MHS=TM.ID_TAGIHAN_MHS
            JOIN PEMBAYARAN PEM ON PEM.ID_TAGIHAN=TAG.ID_TAGIHAN
            JOIN PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
            JOIN FAKULTAS FAK ON FAK.ID_FAKULTAS=PS.ID_FAKULTAS
            WHERE PEM.ID_STATUS_PEMBAYARAN='1' AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' 
            AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}'
            {$condition_akademik} 
            {$condition_periode_bayar}
            AND FAK.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "' 
		     GROUP BY PEM.ID_BANK
		  ) PEM ON PEM.ID_BANK = B.ID_BANK
		GROUP BY B.ID_BANK,B.NM_BANK
        ORDER BY B.NM_BANK";
        return $this->db->QueryToArray($query);
    }

    function load_report_detail_mhs($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $periode_bayar, $id_bank, $id_jalur, $angkatan)
    {
        $condition_akademik = $this->get_condition_akademik($id_fakultas, $id_prodi);
        $condition_periode_bayar = !empty($periode_bayar) ? "AND PEM.PERIODE_BAYAR='{$periode_bayar}'" : "";
        $condition_bank = $this->get_condition_bank($id_bank);
        $condition_jalur = $this->get_condition_jalur($id_jalur);
        $condition_angkatan = $angkatan != '' ? " AND PS.THN_ANGKATAN_MHS='{$angkatan}'" : "";
        $query = "
        SELECT PS.ID_MHS,PS.ID_PROGRAM_STUDI,PS.THN_ANGKATAN_MHS,PS.ID_FAKULTAS,PS.ID_JENJANG,PS.NIM_MHS,PS.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,PS.NM_JALUR,PS.NM_JENJANG,PB.TGL_BAYAR,SUM(PB.BIAYA) BIAYA 
        FROM
        (
            SELECT M.ID_MHS,PS.ID_PROGRAM_STUDI,PS.ID_FAKULTAS,J.ID_JENJANG,M.NIM_MHS,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,JAL.NM_JALUR,J.NM_JENJANG,M.THN_ANGKATAN_MHS 
            FROM PENGGUNA P
            JOIN MAHASISWA M ON P.ID_PENGGUNA  = M.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            JOIN FAKULTAS FAK ON FAK.ID_FAKULTAS=PS.ID_FAKULTAS
            LEFT JOIN ADMISI JM ON JM.ID_MHS = M.ID_MHS AND JM.ID_JALUR IS NOT NULL
            LEFT JOIN JALUR JAL ON JAL.ID_JALUR=JM.ID_JALUR 
		    JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            WHERE PS.ID_PROGRAM_STUDI IS NOT NULL {$condition_jalur} AND FAK.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "' ) PS
		  ,
		  (SELECT TM.ID_MHS,TGL_BAYAR,SUM(BESAR_PEMBAYARAN) BIAYA,SUM(DENDA_BIAYA) DENDA
          FROM PEMBAYARAN PEM
          JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN=PEM.ID_TAGIHAN
          JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS=TAG.ID_TAGIHAN_MHS
		  WHERE PEM.ID_STATUS_PEMBAYARAN='1' AND TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' and TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' {$condition_bank} {$condition_periode_bayar}
            AND ID_BANK IS NOT NULL
		  GROUP BY NO_TRANSAKSI,TM.ID_MHS,TGL_BAYAR ) PB
        WHERE PS.ID_MHS = PB.ID_MHS {$condition_akademik} {$condition_angkatan}
        GROUP BY PS.ID_MHS,
            PS.ID_PROGRAM_STUDI,
            PS.THN_ANGKATAN_MHS,
            PS.ID_FAKULTAS,
            PS.ID_JENJANG,
            PS.NIM_MHS,
            PS.NM_PENGGUNA,
            PS.NM_PROGRAM_STUDI,
            PS.NM_JALUR,
            PS.NM_JENJANG,
            PB.TGL_BAYAR
        ORDER BY PS.NM_PENGGUNA";
        return $this->db->QueryToArray($query);
    }

    function load_report_detail_mhs_page($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $periode_bayar, $id_bank, $id_jalur, $batas, $posisi)
    {
        $condition_akademik = $this->get_condition_akademik($id_fakultas, $id_prodi);
        $condition_periode_bayar = !empty($periode_bayar) ? "AND PEM.PERIODE_BAYAR='{$periode_bayar}'" : "";
        $condition_bank = $this->get_condition_bank($id_bank);
        $condition_jalur = $this->get_condition_jalur($id_jalur);
        return $this->db->QueryToArray("
		SELECT * FROM ( SELECT PAGE.*, ROWNUM rnum FROM
		(SELECT PS.ID_MHS,PS.ID_PROGRAM_STUDI,PS.ID_FAKULTAS,PS.ID_JENJANG,PS.NIM_MHS,PS.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,PS.NM_JALUR,PS.NM_JENJANG,PB.TGL_BAYAR,PB.BIAYA,PB.DENDA FROM
			  (SELECT M.ID_MHS,PS.ID_PROGRAM_STUDI,PS.ID_FAKULTAS,J.ID_JENJANG,M.NIM_MHS,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,JAL.NM_JALUR,J.NM_JENJANG 
			  FROM PENGGUNA P
			  JOIN MAHASISWA M ON P.ID_PENGGUNA  = M.ID_PENGGUNA
                LEFT JOIN JALUR_MA  HASISWA JM ON JM.ID_MHS = M.ID_MHS AND JM.ID_JALUR_AKTIF=1
                LEFT JOIN JALUR JAL ON JAL.ID_JALUR=JM.ID_JALUR     
              JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
              JOIN FAKULTAS FAK ON FAK.ID_FAKULTAS=PS.ID_FAKULTAS
			  JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            WHERE PS.ID_PROGRAM_STUDI IS NOT NULL {$condition_jalur} AND FAK.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "' ) PS
			  ,
			  (SELECT ID_MHS,TGL_BAYAR,SUM(BESAR_BIAYA) BIAYA,SUM(DENDA_BIAYA) DENDA
			  FROM PEMBAYARAN
			  WHERE TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' and TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' {$condition_bank} {$condition_periode_bayar} 
                          AND ID_BANK IS NOT NULL  AND FLAG_PEMINDAHAN IS NULL
			  GROUP BY NO_TRANSAKSI,ID_MHS,TGL_BAYAR ) PB
			WHERE PS.ID_MHS = PB.ID_MHS {$condition_akademik}
		ORDER BY PS.NM_PENGGUNA) PAGE
		WHERE ROWNUM <= ({$batas}+{$posisi}))
		WHERE rnum >= ({$posisi}+1)");
    }

    function load_report_fakultas_mhs($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $periode_bayar, $id_bank, $id_jalur, $angkatan)
    {
        $condition_bank = $this->get_condition_bank($id_bank);
        $condition_periode_bayar = !empty($periode_bayar) ? "AND PEM.PERIODE_BAYAR='{$periode_bayar}'" : "";
        $data_report_fakultas = array();
        //saat report fakultas tertentu
        if ($id_prodi != '') {
            foreach ($this->load_report_detail_mhs($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $periode_bayar, $id_bank, $id_jalur, $angkatan) as $data) {
                $query = "
                SELECT B.ID_BIAYA,B.NM_BIAYA,
                    (CASE WHEN SUM(PEM.BESAR_BIAYA) IS NULL 
                        THEN 0
                    ELSE 
                        SUM(PEM.BESAR_BIAYA)
                    END) BESAR_BIAYA,
                    (CASE WHEN SUM(PEM.DENDA_BIAYA) IS NULL 
                        THEN 0
                    ELSE 
                        SUM(PEM.DENDA_BIAYA)
                    END) DENDA_BIAYA
                    FROM BIAYA B
                LEFT JOIN (
                      SELECT B.ID_BIAYA,SUM(PEM.BESAR_PEMBAYARAN) BESAR_BIAYA,0 DENDA_BIAYA 
                      FROM MAHASISWA M
                      JOIN TAGIHAN_MHS TM ON TM.ID_MHS=M.ID_MHS
                      JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN_MHS=TM.ID_TAGIHAN_MHS
                      JOIN PEMBAYARAN PEM ON PEM.ID_TAGIHAN=TAG.ID_TAGIHAN
                      JOIN DETAIL_BIAYA DB ON TAG.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
                      JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
                      JOIN PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                      JOIN FAKULTAS FAK ON FAK.ID_FAKULTAS=PS.ID_FAKULTAS
                      WHERE PEM.ID_STATUS_PEMBAYARAN='1' AND TO_CHAR(PEM.TGL_BAYAR,'DD-MON-YY') = '{$data['TGL_BAYAR']}' AND
                      M.ID_MHS='{$data['ID_MHS']}' {$condition_bank} {$condition_periode_bayar} AND PEM.ID_BANK IS NOT NULL
                      AND FAK.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "'
                      GROUP BY B.ID_BIAYA
                      ) PEM ON B.ID_BIAYA = PEM.ID_BIAYA
                WHERE B.ID_PERGURUAN_TINGGI='{$this->id_pt}'
                GROUP BY B.ID_BIAYA,B.NM_BIAYA
                ORDER BY B.NM_BIAYA";
                array_push($data_report_fakultas, array(
                    'NM_PENGGUNA' => $data['NM_PENGGUNA'],
                    'NIM_MHS' => $data['NIM_MHS'],
                    'NM_PROGRAM_STUDI' => '( ' . $data['NM_JENJANG'] . ' ) ' . $data['NM_PROGRAM_STUDI'],
                    'TOTAL_PEMBAYARAN' => $this->db->QueryToArray($query)
                ));
            }
        } else if ($id_fakultas != '') {
            foreach ($this->get_row_report_fakultas_mhs($tgl_awal, $tgl_akhir, $id_bank, $periode_bayar, $id_fakultas) as $data) {
                $q_jalur = $data['ID_JALUR'] != '' ? "AND JAL.ID_JALUR='{$data['ID_JALUR']}'" : "";
                $query = "
                SELECT B.ID_BIAYA,B.NM_BIAYA,
                    (CASE WHEN SUM(PEM.BESAR_BIAYA) IS NULL 
                        THEN 0
                    ELSE 
                        SUM(PEM.BESAR_BIAYA)
                    END) BESAR_BIAYA,
                    (CASE WHEN SUM(PEM.DENDA_BIAYA) IS NULL 
                        THEN 0
                    ELSE 
                        SUM(PEM.DENDA_BIAYA)
                    END) DENDA_BIAYA
                    FROM BIAYA B
                LEFT JOIN (
                      SELECT B.ID_BIAYA,SUM(PEM.BESAR_PEMBAYARAN) BESAR_BIAYA,0 DENDA_BIAYA 
                      FROM MAHASISWA M
                      LEFT JOIN ADMISI JM ON JM.ID_MHS = M.ID_MHS AND JM.ID_JALUR IS NOT NULL
                      LEFT JOIN JALUR JAL ON JAL.ID_JALUR=JM.ID_JALUR
                      JOIN TAGIHAN_MHS TM ON TM.ID_MHS=M.ID_MHS
                      JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN_MHS=TM.ID_TAGIHAN_MHS
                      JOIN PEMBAYARAN PEM ON PEM.ID_TAGIHAN=TAG.ID_TAGIHAN
                      JOIN DETAIL_BIAYA DB ON TAG.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
                      JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
                      JOIN PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                      JOIN FAKULTAS FAK ON FAK.ID_FAKULTAS=PS.ID_FAKULTAS
                      WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' AND
                      PS.ID_PROGRAM_STUDI='{$data['ID_PROGRAM_STUDI']}' AND M.THN_ANGKATAN_MHS='{$data['THN_ANGKATAN_MHS']}' {$condition_bank} {$condition_periode_bayar} {$q_jalur} AND PEM.ID_BANK IS NOT NULL
                      AND FAK.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "'
                      GROUP BY B.ID_BIAYA
                      ) PEM ON B.ID_BIAYA = PEM.ID_BIAYA
                WHERE B.ID_PERGURUAN_TINGGI='{$this->id_pt}'
                GROUP BY B.ID_BIAYA,B.NM_BIAYA
                ORDER BY B.NM_BIAYA";
                array_push($data_report_fakultas, array(
                    'ID_PROGRAM_STUDI' => $data['ID_PROGRAM_STUDI'],
                    'NM_PROGRAM_STUDI' => $data['NM_PROGRAM_STUDI'],
                    'NM_JENJANG' => $data['NM_JENJANG'],
                    'ID_JALUR' => $data['ID_JALUR'],
                    'NM_JALUR' => $data['NM_JALUR'],
                    'ANGKATAN' => $data['THN_ANGKATAN_MHS'],
                    'JUMLAH_MHS' => $this->get_jumlah_mhs($tgl_awal, $tgl_akhir, $data['ID_FAKULTAS'], $data['ID_PROGRAM_STUDI'], $periode_bayar, $id_bank, $data['ID_JALUR'], $data['THN_ANGKATAN_MHS']),
                    'ID_FAKULTAS' => $data['ID_FAKULTAS'],
                    'TOTAL_PEMBAYARAN' => $this->db->QueryToArray($query)
                ));
            }
        }
        // saat report semua fakultas
        else {
            foreach ($this->get_row_report_fakultas_mhs($tgl_awal, $tgl_akhir, $id_bank, $periode_bayar, $id_fakultas) as $data) {
                $query = "
                SELECT B.ID_BIAYA,B.NM_BIAYA,
                    (CASE WHEN SUM(PEM.BESAR_BIAYA) IS NULL 
                        THEN 0
                    ELSE 
                        SUM(PEM.BESAR_BIAYA)
                    END) BESAR_BIAYA,
                    (CASE WHEN SUM(PEM.DENDA_BIAYA) IS NULL 
                        THEN 0
                    ELSE 
                        SUM(PEM.DENDA_BIAYA)
                    END) DENDA_BIAYA
                    FROM BIAYA B
                LEFT JOIN (
                    SELECT B.ID_BIAYA,SUM(PEM.BESAR_PEMBAYARAN) BESAR_BIAYA,0 DENDA_BIAYA 
                    FROM MAHASISWA M
                    JOIN TAGIHAN_MHS TM ON TM.ID_MHS=M.ID_MHS
                    JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN_MHS=TM.ID_TAGIHAN_MHS
                    JOIN PEMBAYARAN PEM ON PEM.ID_TAGIHAN=TAG.ID_TAGIHAN
                    JOIN DETAIL_BIAYA DB ON TAG.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
                    JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
                    JOIN PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                    JOIN FAKULTAS FAK ON FAK.ID_FAKULTAS=PS.ID_FAKULTAS
                    WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' 
                    AND PS.ID_FAKULTAS='{$data['ID_FAKULTAS']}' {$condition_bank} {$condition_periode_bayar} AND PEM.ID_BANK IS NOT NULL
                    AND FAK.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "'
                    GROUP BY B.ID_BIAYA
                    ) PEM ON B.ID_BIAYA = PEM.ID_BIAYA
                WHERE B.ID_PERGURUAN_TINGGI='{$this->id_pt}'
                GROUP BY B.ID_BIAYA,B.NM_BIAYA
                ORDER BY B.NM_BIAYA";
                array_push($data_report_fakultas, array(
                    'ID_FAKULTAS' => $data['ID_FAKULTAS'],
                    'NM_FAKULTAS' => $data['NM_FAKULTAS'],
                    'JUMLAH_MHS' => $this->get_jumlah_mhs($tgl_awal, $tgl_akhir, $data['ID_FAKULTAS'], '', $periode_bayar, $id_bank, '', ''),
                    'TOTAL_PEMBAYARAN' => $this->db->QueryToArray($query)
                ));
            }
        }
        return $data_report_fakultas;
    }

    public function load_report_fakultas_mhs_row($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $periode_bayar, $id_bank, $id_jalur, $angkatan)
    {
        if ($id_prodi != '') {
            return $this->load_report_detail_mhs($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $periode_bayar, $id_bank, $id_jalur, $angkatan);
        } else if ($id_fakultas != '') {
            return $this->get_row_report_fakultas_mhs($tgl_awal, $tgl_akhir, $id_bank, $periode_bayar, $id_fakultas);
        } else {
            return $this->get_row_report_fakultas_mhs($tgl_awal, $tgl_akhir, $id_bank, $periode_bayar, $id_fakultas);
        }
    }

    public function load_report_fakultas_mhs_row_value($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $periode_bayar, $id_bank, $id_jalur, $angkatan)
    {
        $condition_bank = $this->get_condition_bank($id_bank);
        $condition_periode_bayar = !empty($periode_bayar) ? "AND PEM.PERIODE_BAYAR='{$periode_bayar}'" : "";

        if ($id_prodi != '') {
            $query = "
                SELECT B.ID_BIAYA,B.NM_BIAYA,
                    (CASE WHEN SUM(PEM.BESAR_BIAYA) IS NULL 
                        THEN 0
                    ELSE 
                        SUM(PEM.BESAR_BIAYA)
                    END) BESAR_BIAYA,
                    (CASE WHEN SUM(PEM.DENDA_BIAYA) IS NULL 
                        THEN 0
                    ELSE 
                        SUM(PEM.DENDA_BIAYA)
                    END) DENDA_BIAYA
                    FROM BIAYA B
                LEFT JOIN (
                      SELECT B.ID_BIAYA,SUM(PEM.BESAR_PEMBAYARAN) BESAR_BIAYA,0 DENDA_BIAYA 
                      FROM MAHASISWA M
                      JOIN TAGIHAN_MHS TM ON TM.ID_MHS=M.ID_MHS
                      JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN_MHS=TM.ID_TAGIHAN_MHS
                      JOIN PEMBAYARAN PEM ON PEM.ID_TAGIHAN=TAG.ID_TAGIHAN
                      JOIN DETAIL_BIAYA DB ON TAG.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
                      JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
                      JOIN PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                      JOIN FAKULTAS FAK ON FAK.ID_FAKULTAS=PS.ID_FAKULTAS
                      WHERE PEM.ID_STATUS_PEMBAYARAN='1' AND TO_CHAR(PEM.TGL_BAYAR,'DD-MON-YY') AND
                       {$condition_bank} {$condition_periode_bayar} AND PEM.ID_BANK IS NOT NULL
                      AND FAK.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "'
                      AND PEM.ID_STATUS_PEMBAYARAN='1'
                      GROUP BY B.ID_BIAYA
                      ) PEM ON B.ID_BIAYA = PEM.ID_BIAYA
                WHERE B.ID_PERGURUAN_TINGGI='{$this->id_pt}'
                GROUP BY B.ID_BIAYA,B.NM_BIAYA
                ORDER BY B.NM_BIAYA";
        } else if ($id_fakultas != '') {

            $query = "
                SELECT PEM.ID_FAKULTAS, PEM.ID_PROGRAM_STUDI,
                    PEM.ID_JALUR, PEM.THN_ANGKATAN_MHS,B.ID_BIAYA,B.NM_BIAYA,
                    (CASE WHEN SUM(PEM.BESAR_BIAYA) IS NULL 
                        THEN 0
                    ELSE 
                        SUM(PEM.BESAR_BIAYA)
                    END) BESAR_BIAYA,
                    (CASE WHEN SUM(PEM.DENDA_BIAYA) IS NULL 
                        THEN 0
                    ELSE 
                        SUM(PEM.DENDA_BIAYA)
                    END) DENDA_BIAYA
                    FROM BIAYA B
                LEFT JOIN (
                      SELECT FAK.ID_FAKULTAS, PS.ID_PROGRAM_STUDI,
                      JAL.ID_JALUR, M.THN_ANGKATAN_MHS,B.ID_BIAYA,SUM(PEM.BESAR_PEMBAYARAN) BESAR_BIAYA,0 DENDA_BIAYA 
                      FROM MAHASISWA M
                      LEFT JOIN ADMISI JM ON JM.ID_MHS = M.ID_MHS AND JM.ID_JALUR IS NOT NULL
                      LEFT JOIN JALUR JAL ON JAL.ID_JALUR=JM.ID_JALUR
                      JOIN TAGIHAN_MHS TM ON TM.ID_MHS=M.ID_MHS
                      JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN_MHS=TM.ID_TAGIHAN_MHS
                      JOIN PEMBAYARAN PEM ON PEM.ID_TAGIHAN=TAG.ID_TAGIHAN
                      JOIN DETAIL_BIAYA DB ON TAG.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
                      JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
                      JOIN PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                      JOIN FAKULTAS FAK ON FAK.ID_FAKULTAS=PS.ID_FAKULTAS
                      WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}'
                       {$condition_bank} {$condition_periode_bayar} AND PEM.ID_BANK IS NOT NULL
                      AND FAK.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "'
                      AND PEM.ID_STATUS_PEMBAYARAN='1'
                      GROUP BY FAK.ID_FAKULTAS, PS.ID_PROGRAM_STUDI,
                      JAL.ID_JALUR, M.THN_ANGKATAN_MHS,B.ID_BIAYA
                      ) PEM ON B.ID_BIAYA = PEM.ID_BIAYA
                WHERE B.ID_PERGURUAN_TINGGI='{$this->id_pt}'
                GROUP BY PEM.ID_FAKULTAS, PEM.ID_PROGRAM_STUDI,
                PEM.ID_JALUR, PEM.THN_ANGKATAN_MHS,B.ID_BIAYA,B.NM_BIAYA
                ORDER BY B.NM_BIAYA";
        } else {
            $query = "
                SELECT PEM.ID_FAKULTAS,B.ID_BIAYA,B.NM_BIAYA,
                    (CASE WHEN SUM(PEM.BESAR_BIAYA) IS NULL 
                        THEN 0
                    ELSE 
                        SUM(PEM.BESAR_BIAYA)
                    END) BESAR_BIAYA,
                    (CASE WHEN SUM(PEM.DENDA_BIAYA) IS NULL 
                        THEN 0
                    ELSE 
                        SUM(PEM.DENDA_BIAYA)
                    END) DENDA_BIAYA
                    FROM BIAYA B
                LEFT JOIN (
                      SELECT FAK.ID_FAKULTAS,B.ID_BIAYA,SUM(PEM.BESAR_PEMBAYARAN) BESAR_BIAYA,0 DENDA_BIAYA 
                      FROM MAHASISWA M
                      LEFT JOIN ADMISI JM ON JM.ID_MHS = M.ID_MHS AND JM.ID_JALUR IS NOT NULL
                      LEFT JOIN JALUR JAL ON JAL.ID_JALUR=JM.ID_JALUR
                      JOIN TAGIHAN_MHS TM ON TM.ID_MHS=M.ID_MHS
                      JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN_MHS=TM.ID_TAGIHAN_MHS
                      JOIN PEMBAYARAN PEM ON PEM.ID_TAGIHAN=TAG.ID_TAGIHAN
                      JOIN DETAIL_BIAYA DB ON TAG.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
                      JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
                      JOIN PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                      JOIN FAKULTAS FAK ON FAK.ID_FAKULTAS=PS.ID_FAKULTAS
                      WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}'
                       {$condition_bank} {$condition_periode_bayar} AND PEM.ID_BANK IS NOT NULL
                      AND FAK.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "'
                      AND PEM.ID_STATUS_PEMBAYARAN='1'
                      GROUP BY FAK.ID_FAKULTAS, PS.ID_PROGRAM_STUDI,
                      JAL.ID_JALUR, M.THN_ANGKATAN_MHS,B.ID_BIAYA
                      ) PEM ON B.ID_BIAYA = PEM.ID_BIAYA
                WHERE B.ID_PERGURUAN_TINGGI='{$this->id_pt}'
                GROUP BY PEM.ID_FAKULTAS,B.ID_BIAYA,B.NM_BIAYA
                ORDER BY B.NM_BIAYA";
        }

        return $this->db->QueryToArray($query);
    }

    private function get_jumlah_cmhs($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $id_bank, $id_jalur, $angkatan)
    {
        $condition_akademik = $this->get_condition_akademik($id_fakultas, $id_prodi);
        $condition_bank = $this->get_condition_bank($id_bank);
        $condition_jalur = $this->get_condition_jalur($id_jalur);
        $condition_angkatan = $this->get_condition_angkatan_cmhs($angkatan);
        $this->db->Query("
            SELECT COUNT(COUNT(PEM.ID_C_MHS)) JUMLAH_MHS FROM CALON_MAHASISWA_BARU CMB
            LEFT JOIN PEMBAYARAN_CMHS PEM ON PEM.ID_C_MHS=CMB.ID_C_MHS
            JOIN PROGRAM_STUDI PS ON CMB.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            JOIN PENERIMAAN PEN ON PEN.ID_PENERIMAAN=CMB.ID_PENERIMAAN
            JOIN JALUR JAL ON JAL.ID_JALUR = CMB.ID_JALUR
            WHERE TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' and TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' 
            {$condition_akademik} 
            {$condition_bank} 
            {$condition_jalur} 
            {$condition_angkatan} 
            GROUP BY PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_C_MHS");
        $data_jumlah_mhs = $this->db->FetchAssoc();
        $jumlah_mhs = $data_jumlah_mhs['JUMLAH_MHS'] == '' ? 0 : $data_jumlah_mhs['JUMLAH_MHS'];
        return $jumlah_mhs;
    }

    private function get_row_report_fakultas_cmhs($id_fakultas, $jalur, $angkatan)
    {
        $condition_jalur = $this->get_condition_jalur($jalur);
        $condition_angkatan = $this->get_condition_angkatan_cmhs($angkatan);
        if ($id_fakultas != '') {
            return $this->db->QueryToArray("
                SELECT F.ID_FAKULTAS,UPPER(F.NM_FAKULTAS) NM_FAKULTAS,PS.ID_PROGRAM_STUDI,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,JAL.ID_JALUR,UPPER(JAL.NM_JALUR) NM_JALUR,PEN.TAHUN
                FROM CALON_MAHASISWA_BARU CMB 
                LEFT JOIN PROGRAM_STUDI PS ON CMB.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
                JOIN PENERIMAAN PEN ON PEN.ID_PENERIMAAN=CMB.ID_PENERIMAAN
                JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
                JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
                JOIN JALUR JAL ON JAL.ID_JALUR = CMB.ID_JALUR
                JOIN PEMBAYARAN_CMHS PEM ON PEM.ID_C_MHS = CMB.ID_C_MHS
                WHERE PEM.TGL_BAYAR IS NOT NULL AND F.ID_FAKULTAS='{$id_fakultas}' AND ID_BANK IS NOT NULL
                {$condition_jalur}
                {$condition_angkatan}
                GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS,PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JAL.ID_JALUR,JAL.NM_JALUR,PEN.TAHUN
                ORDER BY F.ID_FAKULTAS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,PEN.TAHUN,JAL.ID_JALUR,JAL.NM_JALUR");
        } else {
            return $this->db->QueryToArray("
                SELECT F.ID_FAKULTAS,UPPER(F.NM_FAKULTAS) NM_FAKULTAS
                FROM CALON_MAHASISWA_BARU CMB 
                LEFT JOIN PROGRAM_STUDI PS ON CMB.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
                JOIN PENERIMAAN PEN ON PEN.ID_PENERIMAAN=CMB.ID_PENERIMAAN
                JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
                JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
                JOIN PEMBAYARAN_CMHS PEM ON PEM.ID_C_MHS = CMB.ID_C_MHS
                WHERE PEM.TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL
                GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS
                ORDER BY F.ID_FAKULTAS");
        }
    }

    function load_report_bank_cmhs($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $angkatan, $jalur)
    {
        $condition_akademik = $this->get_condition_akademik($id_fakultas, $id_prodi);
        $condition_angkatan = $this->get_condition_angkatan_cmhs($angkatan);
        $condition_jalur = $this->get_condition_jalur($jalur);
        return $this->db->QueryToArray("
		SELECT B.ID_BANK,B.NM_BANK,
		  (CASE WHEN COUNT(PEM.ID_C_MHS) IS NULL
				THEN 0
			  ELSE
				COUNT(PEM.ID_C_MHS)
			  END) JUMLAH_MHS,
		  (CASE WHEN SUM(PEM.BESAR_BIAYA) IS NULL 
			  THEN 0
			ELSE 
			   SUM(PEM.BESAR_BIAYA)
			END) BESAR_BIAYA
		FROM BANK B
		LEFT JOIN (
		  SELECT PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_C_MHS,PEM.ID_BANK,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA,SUM(PEM.DENDA_BIAYA) DENDA_BIAYA 
                  FROM CALON_MAHASISWA_BARU CMB
		  JOIN PEMBAYARAN_CMHS PEM ON PEM.ID_C_MHS=CMB.ID_C_MHS
                  JOIN PENERIMAAN PEN ON CMB.ID_PENERIMAAN=PEN.ID_PENERIMAAN
                  JOIN JALUR JAL ON JAL.ID_JALUR=CMB.ID_JALUR
		  LEFT JOIN PROGRAM_STUDI PS ON CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
		  WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') >= UPPER('{$tgl_awal}') AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}'
		  {$condition_akademik}
                  {$condition_angkatan}
                  {$condition_jalur}
		  GROUP BY PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_C_MHS,PEM.ID_BANK
		  ) PEM ON PEM.ID_BANK = B.ID_BANK
		GROUP BY B.ID_BANK,B.NM_BANK
		ORDER BY B.NM_BANK");
    }

    function load_report_bank_cmhs_bidik_misi($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi)
    {
        $condition_akademik = $this->get_condition_akademik($id_fakultas, $id_prodi);
        $this->db->Query("SELECT B.ID_BANK,B.NM_BANK,
		  (CASE WHEN COUNT(PEM.ID_C_MHS) IS NULL
				THEN 0
			  ELSE
				COUNT(PEM.ID_C_MHS)
			  END) JUMLAH_MHS,
		  (CASE WHEN SUM(PEM.BESAR_BIAYA) IS NULL 
			  THEN 0
			ELSE 
			   SUM(PEM.BESAR_BIAYA)
			END) BESAR_BIAYA
		FROM BANK B
		RIGHT JOIN (
		  SELECT PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_C_MHS,PEM.ID_BANK,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA,SUM(PEM.DENDA_BIAYA) DENDA_BIAYA 
                    FROM CALON_MAHASISWA_BARU CMB
		  JOIN PEMBAYARAN_CMHS PEM ON PEM.ID_C_MHS=CMB.ID_C_MHS
		  LEFT JOIN PROGRAM_STUDI PS ON CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
		  WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') >= UPPER('{$tgl_awal}') AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}'
		  {$condition_akademik}
		  GROUP BY PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_C_MHS,PEM.ID_BANK
		  ) PEM ON PEM.ID_BANK = B.ID_BANK
                WHERE B.ID_BANK IS NULL  
		GROUP BY B.ID_BANK,B.NM_BANK
		ORDER BY B.NM_BANK");
        return $this->db->FetchAssoc();
    }

    function load_report_detail_cmhs($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $id_bank, $id_jalur, $angkatan)
    {
        $condition_akademik = $this->get_condition_akademik($id_fakultas, $id_prodi);
        $condition_bank = $this->get_condition_bank($id_bank);
        $condition_jalur = $this->get_condition_jalur($id_jalur);
        $condition_angkatan = $this->get_condition_angkatan_cmhs($angkatan);
        return $this->db->QueryToArray("
		SELECT PS.ID_C_MHS,PS.ID_PROGRAM_STUDI,PS.ID_FAKULTAS,PS.ID_JENJANG,PS.NO_UJIAN,PS.NM_C_MHS,PS.NM_PROGRAM_STUDI,PS.NM_JALUR,PS.NM_JENJANG,PB.TGL_BAYAR,PB.BIAYA,PB.DENDA,PS.NIM_MHS FROM
		  (SELECT CMB.ID_C_MHS,PS.ID_PROGRAM_STUDI,PS.ID_FAKULTAS,J.ID_JENJANG,CMB.NO_UJIAN,CMB.NM_C_MHS,PS.NM_PROGRAM_STUDI,JAL.NM_JALUR,J.NM_JENJANG,CMB.NIM_MHS 
		  FROM CALON_MAHASISWA_BARU CMB
		  LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CMB.ID_PROGRAM_STUDI
		  JOIN JALUR JAL ON JAL.ID_JALUR = CMB.ID_JALUR
		  JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
                  JOIN PENERIMAAN PEN ON PEN.ID_PENERIMAAN=CMB.ID_PENERIMAAN
                  {$condition_jalur}
                  {$condition_angkatan}
                  ) PS
		  ,
		  (SELECT ID_C_MHS,TGL_BAYAR,SUM(BESAR_BIAYA) BIAYA,SUM(DENDA_BIAYA) DENDA
		  FROM PEMBAYARAN_CMHS PEM
		  WHERE TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') >= UPPER('{$tgl_awal}') and TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' {$condition_bank}
		  GROUP BY NO_TRANSAKSI,ID_C_MHS,TGL_BAYAR ) PB
		WHERE PS.ID_C_MHS = PB.ID_C_MHS {$condition_akademik}
		ORDER BY PS.NM_C_MHS");
    }

    function load_report_detail_cmhs_page($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $id_bank, $id_jalur, $angkatan, $batas, $posisi)
    {
        $condition_akademik = $this->get_condition_akademik($id_fakultas, $id_prodi);
        $condition_bank = $this->get_condition_bank($id_bank);
        $condition_jalur = $this->get_condition_jalur($id_jalur);
        $condition_angkatan = $this->get_condition_angkatan_cmhs($angkatan);

        return $this->db->QueryToArray("
		SELECT * FROM ( SELECT PAGE.*, ROWNUM rnum FROM
		(SELECT PS.ID_C_MHS,PS.ID_PROGRAM_STUDI,PS.ID_FAKULTAS,PS.ID_JENJANG,PS.NO_UJIAN,PS.NM_C_MHS,PS.NM_PROGRAM_STUDI,PS.NM_JALUR,PS.NM_JENJANG,PB.TGL_BAYAR,PB.BIAYA,PB.DENDA,PS.NIM_MHS FROM
		  (SELECT CMB.ID_C_MHS,PS.ID_PROGRAM_STUDI,PS.ID_FAKULTAS,J.ID_JENJANG,CMB.NO_UJIAN,CMB.NM_C_MHS,PS.NM_PROGRAM_STUDI,JAL.NM_JALUR,J.NM_JENJANG,CMB.NIM_MHS 
		  FROM CALON_MAHASISWA_BARU CMB
		  JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CMB.ID_PROGRAM_STUDI
		  JOIN JALUR JAL ON JAL.ID_JALUR = CMB.ID_JALUR
		  JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
                  JOIN PENERIMAAN PEN ON PEN.ID_PENERIMAAN=CMB.ID_PENERIMAAN
                  {$condition_jalur}
                  {$condition_angkatan}
                  ) PS
		  ,
		  (SELECT ID_C_MHS,TGL_BAYAR,SUM(BESAR_BIAYA) BIAYA,SUM(DENDA_BIAYA) DENDA
		  FROM PEMBAYARAN_CMHS PEM
		  WHERE TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') >= UPPER('{$tgl_awal}') and TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' {$condition_bank}
		  GROUP BY NO_TRANSAKSI,ID_C_MHS,TGL_BAYAR ) PB
		WHERE PS.ID_C_MHS = PB.ID_C_MHS {$condition_akademik}
		ORDER BY PS.NM_C_MHS) PAGE
		WHERE ROWNUM <= ({$batas}+{$posisi}))
		WHERE rnum >= ({$posisi}+1)");
    }

    function load_report_fakultas_cmhs($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $id_bank, $id_jalur, $angkatan)
    {
        $condition_bank = $this->get_condition_bank($id_bank);
        $condition_jalur = $this->get_condition_jalur($id_jalur);
        $condition_angkatan = $this->get_condition_angkatan_cmhs($angkatan);
        $data_report_fakultas = array();
        if ($id_prodi != '') {
            foreach ($this->load_report_detail_cmhs($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $id_bank, $id_jalur, $angkatan) as $data) {
                array_push($data_report_fakultas, array(
                    'NM_C_MHS' => $data['NM_C_MHS'],
                    'NO_UJIAN' => $data['NO_UJIAN'],
                    'NM_PROGRAM_STUDI' => '( ' . $data['NM_JENJANG'] . ' ) ' . $data['NM_PROGRAM_STUDI'],
                    'TOTAL_PEMBAYARAN' => $this->db->QueryToArray("
                        SELECT B.ID_BIAYA,B.NM_BIAYA,
                            (CASE WHEN SUM(PEM.BESAR_BIAYA) IS NULL 
                                THEN 0
                            ELSE 
                                SUM(PEM.BESAR_BIAYA)
                            END) BESAR_BIAYA
                            FROM BIAYA B
                        LEFT JOIN (
                              SELECT B.ID_BIAYA,PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_C_MHS,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA,SUM(PEM.DENDA_BIAYA) DENDA_BIAYA 
                              FROM CALON_MAHASISWA_BARU CMB
                              JOIN JALUR JAL ON JAL.ID_JALUR = CMB.ID_JALUR
                              JOIN PEMBAYARAN_CMHS PEM ON PEM.ID_C_MHS=CMB.ID_C_MHS
                              JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
                              JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
                              LEFT JOIN PROGRAM_STUDI PS ON CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                              WHERE TO_CHAR(PEM.TGL_BAYAR,'DD-MON-YY') = UPPER('{$data['TGL_BAYAR']}') {$condition_bank} AND CMB.ID_C_MHS='{$data['ID_C_MHS']}'
                              GROUP BY PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_C_MHS,B.ID_BIAYA
                              ) PEM ON B.ID_BIAYA = PEM.ID_BIAYA
                        GROUP BY B.ID_BIAYA,B.NM_BIAYA
                        ORDER BY B.NM_BIAYA")
                ));
            }
        }
        //saat report fakultas tertentu
        else if ($id_fakultas != '') {
            foreach ($this->get_row_report_fakultas_cmhs($id_fakultas, $id_jalur, $angkatan) as $data) {
                array_push($data_report_fakultas, array(
                    'ID_PROGRAM_STUDI' => $data['ID_PROGRAM_STUDI'],
                    'NM_PROGRAM_STUDI' => $data['NM_PROGRAM_STUDI'],
                    'NM_JENJANG' => $data['NM_JENJANG'],
                    'NM_JALUR' => $data['NM_JALUR'],
                    'ID_JALUR' => $data['ID_JALUR'],
                    'TAHUN' => $data['TAHUN'],
                    'JUMLAH_MHS' => $this->get_jumlah_cmhs($tgl_awal, $tgl_akhir, $data['ID_FAKULTAS'], $data['ID_PROGRAM_STUDI'], $id_bank, $data['ID_JALUR'], $data['TAHUN']),
                    'ID_FAKULTAS' => $data['ID_FAKULTAS'],
                    'TOTAL_PEMBAYARAN' => $this->db->QueryToArray("
                        SELECT B.ID_BIAYA,B.NM_BIAYA,
                            (CASE WHEN SUM(PEM.BESAR_BIAYA) IS NULL 
                                THEN 0
                            ELSE 
                                SUM(PEM.BESAR_BIAYA)
                            END) BESAR_BIAYA
                            FROM BIAYA B
                        LEFT JOIN (
                              SELECT B.ID_BIAYA,PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_C_MHS,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA,SUM(PEM.DENDA_BIAYA) DENDA_BIAYA 
                              FROM CALON_MAHASISWA_BARU CMB
                              JOIN JALUR JAL ON JAL.ID_JALUR = CMB.ID_JALUR
                              JOIN PENERIMAAN PEN ON PEN.ID_PENERIMAAN=CMB.ID_PENERIMAAN
                              JOIN PEMBAYARAN_CMHS PEM ON PEM.ID_C_MHS=CMB.ID_C_MHS
                              JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
                              JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
                              LEFT JOIN PROGRAM_STUDI PS ON CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                              WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') >= UPPER('{$tgl_awal}') AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' 
                              AND PS.ID_PROGRAM_STUDI='{$data['ID_PROGRAM_STUDI']}' {$condition_bank} AND JAL.ID_JALUR='{$data['ID_JALUR']}' AND PEN.TAHUN='{$data['TAHUN']}'
                              GROUP BY PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_C_MHS,B.ID_BIAYA
                              ) PEM ON B.ID_BIAYA = PEM.ID_BIAYA
                        GROUP BY B.ID_BIAYA,B.NM_BIAYA
                        ORDER BY B.NM_BIAYA")
                ));
            }
        }
        // saat report semua fakultas
        else {
            foreach ($this->get_row_report_fakultas_cmhs($id_fakultas, $id_jalur, $angkatan) as $data) {
                array_push($data_report_fakultas, array(
                    'ID_FAKULTAS' => $data['ID_FAKULTAS'],
                    'NM_FAKULTAS' => $data['NM_FAKULTAS'],
                    'NM_JALUR' => $data['NM_JALUR'],
                    'JUMLAH_MHS' => $this->get_jumlah_cmhs($tgl_awal, $tgl_akhir, $data['ID_FAKULTAS'], '', $id_bank, $id_jalur, $angkatan),
                    'TOTAL_PEMBAYARAN' => $this->db->QueryToArray("
                        SELECT B.ID_BIAYA,B.NM_BIAYA,
                            (CASE WHEN SUM(PEM.BESAR_BIAYA) IS NULL 
                                THEN 0
                            ELSE 
                                SUM(PEM.BESAR_BIAYA)
                            END) BESAR_BIAYA
                            FROM BIAYA B
                        LEFT JOIN (
                              SELECT B.ID_BIAYA,PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_C_MHS,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA,SUM(PEM.DENDA_BIAYA) DENDA_BIAYA 
                              FROM CALON_MAHASISWA_BARU CMB
                              JOIN PEMBAYARAN_CMHS PEM ON PEM.ID_C_MHS=CMB.ID_C_MHS
                              JOIN PENERIMAAN PEN ON PEN.ID_PENERIMAAN=CMB.ID_PENERIMAAN
                              JOIN JALUR JAL ON JAL.ID_JALUR=CMB.ID_JALUR
                              JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
                              JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
                              LEFT JOIN PROGRAM_STUDI PS ON CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                              WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') >= UPPER('{$tgl_awal}') AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' 
                              AND PS.ID_FAKULTAS='{$data['ID_FAKULTAS']}' {$condition_bank} {$condition_jalur} {$condition_angkatan}
                              GROUP BY PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_C_MHS,B.ID_BIAYA
                              ) PEM ON B.ID_BIAYA = PEM.ID_BIAYA
                        GROUP BY B.ID_BIAYA,B.NM_BIAYA
                        ORDER BY B.NM_BIAYA")
                ));
            }
        }
        return $data_report_fakultas;
    }

    function load_data_status_pembayaran($fakultas, $prodi, $semester, $status, $jalur, $angkatan, $jenjang)
    {
        $q_fakultas = $fakultas != '' ? "AND PS.ID_FAKULTAS='{$fakultas}'" : "";
        $q_prodi = $prodi != '' ? "AND PS.ID_PROGRAM_STUDI='{$prodi}'" : "";
        $q_jalur = $jalur != '' ? "AND JM.ID_JALUR='{$jalur}'" : "";
        $q_angkatan = $angkatan != '' ? "AND M.THN_ANGKATAN_MHS='{$angkatan}'" : "";
        $q_jenjang = $jenjang != '' ? "AND PS.ID_JENJANG='{$jenjang}'" : "";
        return $this->db->QueryToArray("
        SELECT M.NIM_MHS,P.NM_PENGGUNA,SUM(PEM.BESAR_BIAYA) TOTAL_BIAYA,NM_JENJANG,PS.NM_PROGRAM_STUDI,SP.NAMA_STATUS
        ,PEM.KETERANGAN KETERANGAN_PEMBAYARAN,SSP.KETERANGAN KETERANGAN_STATUS,SSP.TGL_JATUH_TEMPO,JAL.NM_JALUR,SPENG.NM_STATUS_PENGGUNA
        FROM MAHASISWA M
        LEFT JOIN STATUS_PENGGUNA SPENG ON SPENG.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
        LEFT JOIN JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS AND JM.ID_JALUR_AKTIF=1
        LEFT JOIN JALUR JAL ON JM.ID_JALUR = JAL.ID_JALUR
        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
        JOIN FAKULTAS FAK ON FAK.ID_FAKULTAS=PS.ID_FAKULTAS
        JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
        JOIN PEMBAYARAN PEM ON PEM.ID_MHS= M.ID_MHS
        JOIN SEMESTER S ON S.ID_SEMESTER= PEM.ID_SEMESTER
        LEFT JOIN STATUS_PEMBAYARAN SP ON SP.ID_STATUS_PEMBAYARAN=PEM.ID_STATUS_PEMBAYARAN
        LEFT JOIN SEJARAH_STATUS_BAYAR SSP ON SSP.ID_MHS=PEM.ID_MHS AND SSP.ID_SEMESTER = PEM.ID_SEMESTER AND SSP.IS_AKTIF=1 
        WHERE SP.ID_STATUS_PEMBAYARAN='{$status}'  AND S.ID_SEMESTER='{$semester}' {$q_fakultas} {$q_prodi} {$q_jalur} {$q_angkatan} {$q_jenjang}
        AND FAK.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "'
        GROUP BY M.NIM_MHS,P.NM_PENGGUNA,SP.NAMA_STATUS,PEM.KETERANGAN,SSP.KETERANGAN,SSP.TGL_JATUH_TEMPO,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,JAL.NM_JALUR,SPENG.NM_STATUS_PENGGUNA
        ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI,M.NIM_MHS");
    }

    function load_rekap_pembayaran($id_fakultas, $id_semester)
    {
        $kolom_status = "";
        foreach ($this->db->QueryToArray("SELECT * FROM STATUS_PEMBAYARAN ORDER BY ID_STATUS_PEMBAYARAN") as $data_status) {
            $nama_status = strtoupper(str_replace(' ', '_', $data_status['NAMA_STATUS']));
            $kolom_status .= "
            ,SUM(
                CASE WHEN ID_STATUS_PEMBAYARAN={$data_status['ID_STATUS_PEMBAYARAN']}
                    THEN 1
                    ELSE 0
                END
            ) {$nama_status}";
        }
        if ($id_fakultas != '') {
            return $this->db->QueryToArray("
            SELECT J.NM_JENJANG,PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI{$kolom_status}
            FROM (SELECT ID_MHS,TGL_BAYAR,NO_TRANSAKSI,ID_BANK,ID_BANK_VIA,KETERANGAN,ID_STATUS_PEMBAYARAN
            FROM PEMBAYARAN WHERE ID_SEMESTER='{$id_semester}'
            GROUP BY ID_MHS,TGL_BAYAR,NO_TRANSAKSI,ID_BANK,ID_BANK_VIA,KETERANGAN,ID_STATUS_PEMBAYARAN
            ORDER BY ID_MHS) PEM
            JOIN MAHASISWA M ON M.ID_MHS= PEM.ID_MHS
            LEFT JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI AND PS.ID_FAKULTAS='{$id_fakultas}'
            JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            GROUP BY J.NM_JENJANG,PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI
            ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI");
        } else {
            return $this->db->QueryToArray("
            SELECT F.ID_FAKULTAS,F.NM_FAKULTAS{$kolom_status}
            FROM (SELECT ID_MHS,TGL_BAYAR,NO_TRANSAKSI,ID_BANK,ID_BANK_VIA,KETERANGAN,ID_STATUS_PEMBAYARAN
            FROM PEMBAYARAN WHERE ID_SEMESTER='{$id_semester}'
            GROUP BY ID_MHS,TGL_BAYAR,NO_TRANSAKSI,ID_BANK,ID_BANK_VIA,KETERANGAN,ID_STATUS_PEMBAYARAN
            ORDER BY ID_MHS) PEM
            JOIN MAHASISWA M ON M.ID_MHS= PEM.ID_MHS
            LEFT JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
            GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS
            ORDER BY F.ID_FAKULTAS");
        }
    }

    //fungsi pembayaran wisuda

    function load_data_pembayaran_wisuda_mode_fakultas($tgl_awal, $tgl_akhir, $periode_wisuda, $bank)
    {
        $periode_wisuda = $periode_wisuda != '' ? "AND ID_TARIF_WISUDA='" . $periode_wisuda . "'" : "";
        return $this->db->QueryToArray("
            SELECT F.ID_FAKULTAS,UPPER(F.NM_FAKULTAS) NM_FAKULTAS,PEW.JUMLAH_MHS,PEW.TOTAL_BIAYA FROM 
                (SELECT * FROM FAKULTAS) F
            LEFT JOIN 
                (
                    SELECT PS.ID_FAKULTAS,COUNT(M.ID_MHS) JUMLAH_MHS,SUM(PEW.BESAR_BIAYA) TOTAL_BIAYA 
                    FROM PEMBAYARAN_WISUDA PEW
                    JOIN PERIODE_WISUDA PW ON PW.ID_PERIODE_WISUDA = PEW.ID_PERIODE_WISUDA
                    JOIN MAHASISWA M ON PEW.ID_MHS = M.ID_MHS
                    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                    WHERE TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' AND TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' 
                    {$periode_wisuda} AND PEW.ID_BANK ='{$bank}'
                    GROUP BY PS.ID_FAKULTAS
                ) PEW 
            ON PEW.ID_FAKULTAS = F.ID_FAKULTAS");
    }

    function load_data_pembayaran_wisuda_mode_bank($tgl_awal, $tgl_akhir, $periode_wisuda)
    {
        $periode_wisuda = $periode_wisuda != '' ? "AND ID_TARIF_WISUDA='" . $periode_wisuda . "'" : "";
        return $this->db->QueryToArray("
            SELECT B.ID_BANK,B.NM_BANK,PW.JUMLAH_MHS,PW.TOTAL_BIAYA FROM 
                (SELECT * FROM BANK) B
            LEFT JOIN   
                (
                    SELECT PEW.ID_BANK,COUNT(PEW.ID_PEMBAYARAN_WISUDA) JUMLAH_MHS,SUM(PEW.BESAR_BIAYA) TOTAL_BIAYA FROM PEMBAYARAN_WISUDA PEW
                    JOIN PERIODE_WISUDA PW ON PW.ID_PERIODE_WISUDA = PEW.ID_PERIODE_WISUDA
                    WHERE TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' AND TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' {$periode_wisuda}
                    GROUP BY PEW.ID_BANK
                ) PW ON PW.ID_BANK = B.ID_BANK");
    }

    function load_data_pembayaran_wisuda_mode_detail($tgl_awal, $tgl_akhir, $periode_wisuda, $bank, $fakultas)
    {
        $periode_wisuda = $periode_wisuda != '' ? "AND ID_TARIF_WISUDA='" . $periode_wisuda . "'" : "";
        $q_fakultas = $fakultas != '' ? "AND F.ID_FAKULTAS='{$fakultas}'" : "";
        return $this->db->QueryToArray("
            SELECT M.NIM_MHS,M.THN_ANGKATAN_MHS,P.NM_PENGGUNA,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,F.ID_FAKULTAS,UPPER(F.NM_FAKULTAS) NM_FAKULTAS,PEW.TGL_BAYAR,PEW.BESAR_BIAYA 
                FROM PEMBAYARAN_WISUDA PEW
                JOIN MAHASISWA M ON M.ID_MHS = PEW.ID_MHS
                JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
                JOIN JENJANG J ON J.ID_JENJANG  = PS.ID_JENJANG
                JOIN PERIODE_WISUDA PW ON PW.ID_PERIODE_WISUDA = PEW.ID_PERIODE_WISUDA
            WHERE TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' AND TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' 
            {$periode_wisuda} AND PEW.ID_BANK ='{$bank}' {$q_fakultas}
            ORDER BY F.ID_FAKULTAS,M.NIM_MHS");
    }

    function load_periode_wisuda()
    {
        return $this->db->QueryToArray("
            SELECT * FROM TARIF_WISUDA");
    }

    //fungsi pengembalian pembayaran

    function load_pengembalian_pembayaran($fakultas, $prodi, $semester)
    {
        $q_fakultas = $fakultas != "" ? "AND PS.ID_FAKULTAS='{$fakultas}'" : "";
        $q_prodi = $prodi != "" ? "AND PS.ID_PROGRAM_STUDI='{$prodi}'" : "";
        return $this->db->QueryToArray("
            SELECT PP.*,SB.NM_SEMESTER NM_SEMESTER_BAYAR,SB.TAHUN_AJARAN TAHUN_AJARAN_BAYAR,SP.NM_SEMESTER,SP.TAHUN_AJARAN,B.NM_BANK,BV.NAMA_BANK_VIA
            ,M.NIM_MHS,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,STA.NM_STATUS_PENGGUNA
                FROM PENGEMBALIAN_PEMBAYARAN PP
            JOIN SEMESTER SB ON SB.ID_SEMESTER=PP.ID_SEMESTER
            JOIN SEMESTER SP ON SP.ID_SEMESTER=PP.ID_SEMESTER_PENGEMBALIAN
            JOIN BANK B ON B.ID_BANK = PP.ID_BANK
            JOIN BANK_VIA BV ON BV.ID_BANK_VIA = PP.ID_BANK_VIA
            JOIN MAHASISWA M ON M.ID_MHS = PP.ID_MHS
            JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            LEFT JOIN STATUS_PENGGUNA STA ON STA.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
            WHERE PP.ID_SEMESTER='{$semester}' {$q_fakultas} {$q_prodi}
            ORDER BY PS.NM_PROGRAM_STUDI,M.NIM_MHS");
    }

    function load_report_bank_pengembalian($tgl_awal, $tgl_akhir, $fakultas, $prodi)
    {
        $condition_akademik = $this->get_condition_akademik($fakultas, $prodi);
        return $this->db->QueryToArray("
		SELECT B.ID_BANK,B.NM_BANK,SUM(PP.PENGEMBALIAN) PENGEMBALIAN,COUNT(PP.ID_MHS) JUMLAH_MHS FROM AUCC.BANK B
                LEFT JOIN
                (
                    SELECT PP.ID_BANK,SUM(PPD.BESAR_BIAYA) PENGEMBALIAN,M.ID_MHS
                    FROM AUCC.PENGEMBALIAN_PEMBAYARAN PP
                    JOIN AUCC.PENGEMBALIAN_PEMBAYARAN_DETAIL PPD ON PPD.ID_PENGEMBALIAN_PEMBAYARAN = PP.ID_PENGEMBALIAN_PEMBAYARAN
                    JOIN AUCC.MAHASISWA M ON M.ID_MHS=PP.ID_MHS
                    JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
                    WHERE 
                        TO_CHAR(PP.TGL_PENGEMBALIAN,'YYYY-MM-DD') >= '{$tgl_awal}' AND 
                        TO_CHAR(PP.TGL_PENGEMBALIAN,'YYYY-MM-DD') <= '{$tgl_akhir}'
                        {$condition_akademik}
                    GROUP BY PP.ID_BANK,M.ID_MHS
                ) PP ON PP.ID_BANK=B.ID_BANK
                GROUP BY B.ID_BANK,B.NM_BANK
                ORDER BY B.NM_BANK");
    }

    function load_report_detail_pengembalian($tgl_awal, $tgl_akhir, $fakultas, $prodi, $bank)
    {
        $q_bank = $bank != '' ? "AND B.ID_BANK='{$bank}'" : "";
        $condition_akademik = $this->get_condition_akademik($fakultas, $prodi);
        return $this->db->QueryToArray("
            SELECT PP.ID_MHS,PP.ID_PENGEMBALIAN_PEMBAYARAN,P.NM_PENGGUNA,M.NIM_MHS,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,SB.NM_SEMESTER NM_SEMESTER_BAYAR,
                SB.TAHUN_AJARAN TAHUN_AJARAN_BAYAR,SP.NM_SEMESTER,SP.TAHUN_AJARAN,B.NM_BANK,BV.NAMA_BANK_VIA,STA.NM_STATUS_PENGGUNA,PP.TGL_PENGEMBALIAN
                ,SUM(PPD.BESAR_BIAYA) PENGEMBALIAN,PP.BESAR_PEMBAYARAN
            FROM AUCC.PENGEMBALIAN_PEMBAYARAN PP
            JOIN AUCC.PENGEMBALIAN_PEMBAYARAN_DETAIL PPD ON PPD.ID_PENGEMBALIAN_PEMBAYARAN=PP.ID_PENGEMBALIAN_PEMBAYARAN
            JOIN AUCC.SEMESTER SB ON SB.ID_SEMESTER=PP.ID_SEMESTER
            JOIN AUCC.SEMESTER SP ON SP.ID_SEMESTER=PP.ID_SEMESTER_PENGEMBALIAN
            JOIN AUCC.BANK B ON B.ID_BANK = PP.ID_BANK
            JOIN AUCC.BANK_VIA BV ON BV.ID_BANK_VIA = PP.ID_BANK_VIA
            JOIN AUCC.MAHASISWA M ON M.ID_MHS = PP.ID_MHS
            JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
            JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            JOIN AUCC.JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            LEFT JOIN AUCC.STATUS_PENGGUNA STA ON STA.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
            WHERE TO_CHAR(PP.TGL_PENGEMBALIAN,'YYYY-MM-DD') >= '{$tgl_awal}' AND TO_CHAR(PP.TGL_PENGEMBALIAN,'YYYY-MM-DD') <= '{$tgl_akhir}'
            {$condition_akademik} {$q_bank}
            GROUP BY PP.ID_MHS,PP.ID_PENGEMBALIAN_PEMBAYARAN,P.NM_PENGGUNA,M.NIM_MHS,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,SB.NM_SEMESTER,
            SB.TAHUN_AJARAN,SP.NM_SEMESTER,SP.TAHUN_AJARAN,B.NM_BANK,BV.NAMA_BANK_VIA,STA.NM_STATUS_PENGGUNA,PP.TGL_PENGEMBALIAN,PP.BESAR_PEMBAYARAN
            ORDER BY PS.NM_PROGRAM_STUDI,M.NIM_MHS");
    }

    function load_report_fakultas_pengembalian($tgl_awal, $tgl_akhir, $fakultas, $prodi, $bank)
    {
        $f_bank = $bank != '' ? "AND PP.ID_BANK='{$bank}'" : "";
        $data_report_fakultas = array();
        //saat report fakultas tertentu
        if ($prodi != '') {
            foreach ($this->load_report_detail_pengembalian($tgl_awal, $tgl_akhir, $fakultas, $prodi, $bank) as $data) {
                array_push($data_report_fakultas, array(
                    'NM_PENGGUNA' => $data['NM_PENGGUNA'],
                    'NIM_MHS' => $data['NIM_MHS'],
                    'NM_PROGRAM_STUDI' => '( ' . $data['NM_JENJANG'] . ' ) ' . $data['NM_PROGRAM_STUDI'],
                    'DATA_PENGEMBALIAN' => $this->db->QueryToArray("
                        SELECT B.ID_BIAYA,B.NM_BIAYA,SUM(PP.PENGEMBALIAN) PENGEMBALIAN
                        FROM AUCC.BIAYA B
                        LEFT JOIN (
                                SELECT PPD.ID_BIAYA,PPD.BESAR_BIAYA PENGEMBALIAN
                                FROM AUCC.PENGEMBALIAN_PEMBAYARAN PP 
                                JOIN AUCC.PENGEMBALIAN_PEMBAYARAN_DETAIL PPD ON PP.ID_PENGEMBALIAN_PEMBAYARAN=PPD.ID_PENGEMBALIAN_PEMBAYARAN
                                JOIN AUCC.MAHASISWA M ON M.ID_MHS=PP.ID_MHS
                                JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
                                WHERE TO_CHAR(PP.TGL_PENGEMBALIAN,'YYYY-MM-DD') >= '{$tgl_awal}' AND TO_CHAR(PP.TGL_PENGEMBALIAN,'YYYY-MM-DD') <= '{$tgl_akhir}'
                                AND M.ID_MHS='{$data['ID_MHS']}' {$f_bank}
                                ) PP ON B.ID_BIAYA = PP.ID_BIAYA
                        GROUP BY B.ID_BIAYA,B.NM_BIAYA
                        ORDER BY B.NM_BIAYA")
                ));
            }
        } else if ($fakultas != '') {
            $q_prodi = "
                SELECT PS.NM_PROGRAM_STUDI,PS.ID_PROGRAM_STUDI,J.NM_JENJANG,COUNT(PP.ID_MHS) JUMLAH_MHS 
                FROM AUCC.PROGRAM_STUDI PS
                JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                LEFT JOIN
                (
                    SELECT M.ID_PROGRAM_STUDI,M.ID_MHS
                    FROM AUCC.MAHASISWA M
                    JOIN AUCC.PENGEMBALIAN_PEMBAYARAN PP ON PP.ID_MHS=M.ID_MHS
                    JOIN AUCC.PENGEMBALIAN_PEMBAYARAN_DETAIL PPD ON PPD.ID_PENGEMBALIAN_PEMBAYARAN=PP.ID_PENGEMBALIAN_PEMBAYARAN
                    WHERE TO_CHAR(PP.TGL_PENGEMBALIAN,'YYYY-MM-DD')>='{$tgl_awal}' 
                    AND TO_CHAR(PP.TGL_PENGEMBALIAN,'YYYY-MM-DD') <= '{$tgl_akhir}'
                    {$f_bank}
                    GROUP BY M.ID_PROGRAM_STUDI,M.ID_MHS
                ) PP ON PP.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                WHERE PS.ID_FAKULTAS='{$fakultas}'
                GROUP BY PS.NM_PROGRAM_STUDI,PS.ID_PROGRAM_STUDI,J.NM_JENJANG
                ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI";
            foreach ($this->db->QueryToArray($q_prodi) as $data) {
                array_push($data_report_fakultas, array(
                    'ID_PROGRAM_STUDI' => $data['ID_PROGRAM_STUDI'],
                    'NM_PROGRAM_STUDI' => $data['NM_PROGRAM_STUDI'],
                    'NM_JENJANG' => $data['NM_JENJANG'],
                    'JUMLAH_MHS' => $data['JUMLAH_MHS'],
                    'ID_FAKULTAS' => $data['ID_FAKULTAS'],
                    'DATA_PENGEMBALIAN' => $this->db->QueryToArray("
                        SELECT B.ID_BIAYA,B.NM_BIAYA,SUM(PP.PENGEMBALIAN) PENGEMBALIAN
                        FROM AUCC.BIAYA B
                        LEFT JOIN (
                            SELECT PPD.ID_BIAYA,PPD.BESAR_BIAYA PENGEMBALIAN
                            FROM AUCC.PENGEMBALIAN_PEMBAYARAN PP 
                            JOIN AUCC.PENGEMBALIAN_PEMBAYARAN_DETAIL PPD ON PP.ID_PENGEMBALIAN_PEMBAYARAN=PPD.ID_PENGEMBALIAN_PEMBAYARAN
                            JOIN AUCC.MAHASISWA M ON M.ID_MHS=PP.ID_MHS
                            JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
                            WHERE TO_CHAR(PP.TGL_PENGEMBALIAN,'YYYY-MM-DD') >= '{$tgl_awal}' AND TO_CHAR(PP.TGL_PENGEMBALIAN,'YYYY-MM-DD') <= '{$tgl_akhir}'
                            AND PS.ID_PROGRAM_STUDI='{$data['ID_PROGRAM_STUDI']}' {$f_bank} 
                            ) PP ON B.ID_BIAYA = PP.ID_BIAYA
                        GROUP BY B.ID_BIAYA,B.NM_BIAYA
                        ORDER BY B.NM_BIAYA")
                ));
            }
        }
        // saat report semua fakultas
        else {
            $q_fakultas = "
                SELECT F.ID_FAKULTAS,F.NM_FAKULTAS,COUNT(PP.ID_MHS) JUMLAH_MHS 
                FROM AUCC.FAKULTAS F 
                JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_FAKULTAS=F.ID_FAKULTAS 
                LEFT JOIN
                (
                    SELECT M.ID_PROGRAM_STUDI,M.ID_MHS
                    FROM AUCC.MAHASISWA M
                    JOIN AUCC.PENGEMBALIAN_PEMBAYARAN PP ON PP.ID_MHS=M.ID_MHS
                    JOIN AUCC.PENGEMBALIAN_PEMBAYARAN_DETAIL PPD ON PPD.ID_PENGEMBALIAN_PEMBAYARAN=PP.ID_PENGEMBALIAN_PEMBAYARAN
                    WHERE TO_CHAR(PP.TGL_PENGEMBALIAN,'YYYY-MM-DD')>='{$tgl_awal}' 
                    AND TO_CHAR(PP.TGL_PENGEMBALIAN,'YYYY-MM-DD') <= '{$tgl_akhir}'
                    {$f_bank}
                    GROUP BY M.ID_PROGRAM_STUDI,M.ID_MHS
                ) PP ON PP.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS 
                ORDER BY F.ID_FAKULTAS";
            foreach ($this->db->QueryToArray($q_fakultas) as $data) {
                array_push($data_report_fakultas, array(
                    'ID_FAKULTAS' => $data['ID_FAKULTAS'],
                    'NM_FAKULTAS' => $data['NM_FAKULTAS'],
                    'JUMLAH_MHS' => $data['JUMLAH_MHS'],
                    'DATA_PENGEMBALIAN' => $this->db->QueryToArray("
                        SELECT B.ID_BIAYA,B.NM_BIAYA,SUM(PP.PENGEMBALIAN) PENGEMBALIAN
                        FROM AUCC.BIAYA B
                        LEFT JOIN (
                            SELECT PPD.ID_BIAYA,PPD.BESAR_BIAYA PENGEMBALIAN
                            FROM AUCC.PENGEMBALIAN_PEMBAYARAN PP 
                            JOIN AUCC.PENGEMBALIAN_PEMBAYARAN_DETAIL PPD ON PP.ID_PENGEMBALIAN_PEMBAYARAN=PPD.ID_PENGEMBALIAN_PEMBAYARAN
                            JOIN AUCC.MAHASISWA M ON M.ID_MHS=PP.ID_MHS
                            JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
                            WHERE TO_CHAR(PP.TGL_PENGEMBALIAN,'YYYY-MM-DD') >= '{$tgl_awal}' AND TO_CHAR(PP.TGL_PENGEMBALIAN,'YYYY-MM-DD') <= '{$tgl_akhir}'
                            AND PS.ID_FAKULTAS='{$data['ID_FAKULTAS']}' {$f_bank} 
                            ) PP ON B.ID_BIAYA = PP.ID_BIAYA
                        GROUP BY B.ID_BIAYA,B.NM_BIAYA
                        ORDER BY B.NM_BIAYA")
                ));
            }
        }
        return $data_report_fakultas;
    }

    //fungsi kontrol kerjasama

    function load_data_mahasiswa_kerjasama($fakultas, $prodi, $beasiswa, $semester)
    {
        $data_rekap = array();
        $q_fakultas = $fakultas != "" ? "AND PS.ID_FAKULTAS='{$fakultas}'" : "";
        $q_prodi = $prodi != "" ? "AND PS.ID_PROGRAM_STUDI='{$prodi}'" : "";
        $data_query = $this->db->QueryToArray("
            SELECT M.ID_MHS,M.NIM_MHS,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,J.NM_JALUR,SB.*,B.NM_BEASISWA,B.PENYELENGGARA_BEASISWA,B.PERIODE_PEMBERIAN_BEASISWA
                ,GB.NM_GROUP,(SM.NM_SEMESTER||' '||SM.TAHUN_AJARAN) SEMESTER_MULAI,(SS.NM_SEMESTER||' '||SS.TAHUN_AJARAN) SEMESTER_SELESAI
            FROM SEJARAH_BEASISWA SB
            JOIN BEASISWA B ON SB.ID_BEASISWA=B.ID_BEASISWA AND SB.BEASISWA_AKTIF=1
            JOIN GROUP_BEASISWA GB ON GB.ID_GROUP_BEASISWA=B.ID_GROUP_BEASISWA
            JOIN MAHASISWA M ON SB.ID_MHS=M.ID_MHS
            JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
            LEFT JOIN SEMESTER SM ON SM.ID_SEMESTER=SB.ID_SEMESTER_MULAI
            LEFT JOIN SEMESTER SS ON SS.ID_SEMESTER=SB.ID_SEMESTER_SELESAI
            LEFT JOIN JALUR_MAHASISWA JM ON JM.ID_MHS=M.ID_MHS AND JM.ID_JALUR_AKTIF=1
            LEFT JOIN JALUR J ON J.ID_JALUR=JM.ID_JALUR
            WHERE SB.ID_BEASISWA='{$beasiswa}' {$q_fakultas} {$q_prodi}");
        foreach ($data_query as $data) {
            $data_bayar = array('DATA_PEMBAYARAN' => $this->db->QueryToArray("
                SELECT ID_MHS,ID_SEMESTER,TGL_BAYAR,B.NM_BANK,SP.NAMA_STATUS,SUM(PEM.BESAR_BIAYA) AS TOTAL_BAYAR 
                FROM PEMBAYARAN PEM
                JOIN BANK B ON PEM.ID_BANK =B.ID_BANK
                JOIN STATUS_PEMBAYARAN SP ON PEM.ID_STATUS_PEMBAYARAN=SP.ID_STATUS_PEMBAYARAN
                WHERE ID_SEMESTER='{$semester}' AND ID_MHS='{$data['ID_MHS']}'
                GROUP BY ID_MHS,ID_SEMESTER,TGL_BAYAR,B.NM_BANK,SP.NAMA_STATUS"));
            array_push($data_rekap, array_merge($data, $data_bayar));
        }
        return $data_rekap;
    }

    //fungsi menu Pembayaran Sp3 Lebih

    function load_data_sp3_lebih($fakultas, $prodi, $semester)
    {
        $data_sp3_lebih = array();
        $q_fakultas = $fakultas != "" ? "AND PS.ID_FAKULTAS='{$fakultas}'" : "";
        $q_prodi = $prodi != "" ? "AND PS.ID_PROGRAM_STUDI='{$prodi}'" : "";
        $q_semester = $semester != "" ? "AND S.ID_SEMESTER='{$semester}'" : "";
        $data_query = $this->db->QueryToArray("
            SELECT CMB.ID_C_MHS,CMB.NO_UJIAN,CMB.GELAR,CMB.NM_C_MHS,CMB.SP3_1,DB.BESAR_BIAYA,DB1.SP3_BAYAR
                ,JAL.NM_JALUR,KB.NM_KELOMPOK_BIAYA,PC.TOTAL_PEMBAYARAN,DB2.TOTAL_BIAYA,S.NM_SEMESTER,S.TAHUN_AJARAN,
                F.ID_FAKULTAS,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,F.NM_FAKULTAS,DB3.SP3_SUKARELA
            FROM CALON_MAHASISWA_BARU CMB
            JOIN BIAYA_KULIAH_CMHS BKC ON BKC.ID_C_MHS = CMB.ID_C_MHS
            JOIN (
                SELECT ID_C_MHS,ID_SEMESTER,SUM(BESAR_BIAYA) TOTAL_PEMBAYARAN
                FROM PEMBAYARAN_CMHS
                GROUP BY ID_C_MHS,ID_SEMESTER
            ) PC ON PC.ID_C_MHS = CMB.ID_C_MHS
            JOIN BIAYA_KULIAH BK ON BK.ID_BIAYA_KULIAH = BKC.ID_BIAYA_KULIAH
            JOIN (
                SELECT * 
                FROM DETAIL_BIAYA
                WHERE ID_BIAYA=81
            ) DB ON DB.ID_BIAYA_KULIAH = BK.ID_BIAYA_KULIAH
            JOIN (
                SELECT ID_C_MHS,PC.BESAR_BIAYA SP3_BAYAR 
                FROM PEMBAYARAN_CMHS PC 
                JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PC.ID_DETAIL_BIAYA
                WHERE DB.ID_BIAYA=81
            ) DB1 ON DB1.ID_C_MHS = CMB.ID_C_MHS
            JOIN (
                SELECT ID_BIAYA_KULIAH,SUM(BESAR_BIAYA) TOTAL_BIAYA
                FROM DETAIL_BIAYA
                GROUP BY ID_BIAYA_KULIAH
            ) DB2 ON DB2.ID_BIAYA_KULIAH=BK.ID_BIAYA_KULIAH
            LEFT JOIN (
                SELECT ID_C_MHS,PC.BESAR_BIAYA SP3_SUKARELA 
                FROM PEMBAYARAN_CMHS PC 
                JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PC.ID_DETAIL_BIAYA
                WHERE DB.ID_BIAYA=124
            ) DB3 ON DB3.ID_C_MHS = CMB.ID_C_MHS
            JOIN JALUR JAL ON JAL.ID_JALUR=CMB.ID_JALUR
            JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = BK.ID_KELOMPOK_BIAYA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CMB.ID_PROGRAM_STUDI
            JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
            JOIN JENJANG J ON J.ID_JENJANG =PS.ID_JENJANG
            JOIN SEMESTER S ON S.ID_SEMESTER=PC.ID_SEMESTER
            WHERE (DB1.SP3_BAYAR>DB.BESAR_BIAYA OR DB3.SP3_SUKARELA >0)   {$q_fakultas} {$q_prodi} {$q_semester}
            ORDER BY F.ID_FAKULTAS,PS.NM_PROGRAM_STUDI,CMB.NO_UJIAN");
        foreach ($data_query as $data) {
            $status_bayar = array('STATUS_BAYAR' => $this->db->QueryToArray("
                SELECT PC.ID_C_MHS,PC.ID_SEMESTER,PC.TGL_BAYAR,B.NM_BANK,SUM(BESAR_BIAYA) TOTAL_BAYAR 
                FROM PEMBAYARAN_CMHS PC
                LEFT JOIN BANK B ON PC.ID_BANK=B.ID_BANK
                WHERE PC.ID_C_MHS='{$data['ID_C_MHS']}' AND PC.ID_SEMESTER='{$semester}' AND TGL_BAYAR IS NOT NULL
                GROUP BY PC.ID_C_MHS,PC.ID_SEMESTER,PC.TGL_BAYAR,B.NM_BANK"));
            array_push($data_sp3_lebih, array_merge($data, $status_bayar));
        }
        return $data_sp3_lebih;
    }

    /// Fungsi Menu Pengembalian Formulir

    function load_data_rekap_pembelian_formulir($tgl_awal, $tgl_akhir)
    {
        $data_rekap = array();
        //Array Kode Jurusan IPA,IPS,IPC
        $arr_kode_jurusan = array('01', '02', '03');
        foreach ($arr_kode_jurusan as $kode) {
            array_push($data_rekap, array(
                'KODE' => $kode,
                'DATA_REKAP' => $this->db->QueryToArray("
                SELECT ('Gelombang '||PEN.GELOMBANG) GELOMBANG,PEN.NM_PENERIMAAN,ID_VOUCHER_TARIF,TARIF,
                    CASE KODE_JURUSAN WHEN '01' THEN 'IPA/Reguler' WHEN '02' THEN 'IPS' WHEN '03' THEN 'IPC' END AS KODE_JURUSAN,J.NM_JENJANG,
                    (SELECT COUNT(*) FROM VOUCHER V WHERE V.ID_PENERIMAAN = VT.ID_PENERIMAAN AND IS_AKTIF = 1) AKTIF,
                    (SELECT COUNT(*) FROM VOUCHER V WHERE V.ID_PENERIMAAN = VT.ID_PENERIMAAN AND KODE_JURUSAN='{$kode}' AND TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') BETWEEN '{$tgl_awal}' AND '{$tgl_akhir}') PEMBELIAN,
                    (SELECT SUM(BESAR_BIAYA) FROM VOUCHER V WHERE V.ID_PENERIMAAN = VT.ID_PENERIMAAN AND KODE_JURUSAN='{$kode}' AND TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') BETWEEN '{$tgl_awal}' AND '{$tgl_akhir}') PENERIMAAN
                FROM VOUCHER_TARIF VT
                JOIN PENERIMAAN PEN ON PEN.ID_PENERIMAAN=VT.ID_PENERIMAAN
                LEFT JOIN JENJANG J ON J.ID_JENJANG = PEN.ID_JENJANG
                WHERE VT.KODE_JURUSAN='{$kode}'
                ORDER BY GELOMBANG,NM_PENERIMAAN
                ")
            ));
        }
        return $data_rekap;
    }

    /// FUngsi menu pendaftaran

    function load_verifikasi_per_tanggal($penerimaan, $tipe)
    {
        $count_per_tanggal = "";
        $query_tanggal = "SELECT TGL_JADWAL FROM AUCC.JADWAL_VERIFIKASI_KEUANGAN WHERE ID_PENERIMAAN='{$penerimaan}' ORDER BY TGL_JADWAL";
        $index = 1;
        if ($tipe == 1) {
            foreach ($this->db->QueryToArray($query_tanggal) as $tgl) {
                $count_per_tanggal .= "
                ,(
                    SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                    WHERE ID_PENERIMAAN='{$penerimaan}' AND 
                    CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI AND 
                    TO_DATE(TGL_VERIFIKASI_KEUANGAN,'DD-MM-YY')='{$tgl['TGL_JADWAL']}'
                ) TGL_{$index}
                ";
                $index++;
            }
        } else {
            foreach ($this->db->QueryToArray($query_tanggal) as $tgl) {
                $count_per_tanggal .= "
                ,(
                    SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI                        
                    WHERE ID_PENERIMAAN='{$penerimaan}' AND 
                    PS.ID_FAKULTAS=F.ID_FAKULTAS AND 
                    TO_DATE(TGL_VERIFIKASI_KEUANGAN,'DD-MM-YY')='{$tgl['TGL_JADWAL']}'
                ) TGL_{$index}
                ";
                $index++;
            }
        }
        return $count_per_tanggal;
    }

    function load_verifikasi_data($fakultas, $penerimaan)
    {
        $query_kelompok = '';
        // Data Prediksi Pengelompokan
        $data_kel_biaya = $this->load_kelompok_biaya_penerimaan($penerimaan);
        $data_kel_biaya_verifikasi = $this->load_kelompok_biaya_penerimaan_verifikasi($penerimaan);

        if ($fakultas != '') {
            foreach ($data_kel_biaya as $d) {
                $query_kelompok .=
                    "(
                    SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                    WHERE ID_PENERIMAAN='{$penerimaan}' 
                    AND CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI 
                    AND CMB.TGL_REGMABA IS NOT NULL 
                    AND CMB.ID_KELOMPOK_BIAYA ='{$d['ID_KELOMPOK_BIAYA']}'
                    ) " . str_replace(" ", "_", $d['NM_KELOMPOK_BIAYA']) . ", ";
            }
            foreach ($data_kel_biaya_verifikasi as $d) {
                $query_kelompok_verifikasi .=
                    "(
                    SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                    JOIN AUCC.BIAYA_KULIAH_CMHS BKC ON BKC.ID_C_MHS=CMB.ID_C_MHS
                    JOIN AUCC.BIAYA_KULIAH BK ON BK.ID_BIAYA_KULIAH=BKC.ID_BIAYA_KULIAH
                    WHERE ID_PENERIMAAN='{$penerimaan}' 
                    AND CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI 
                    AND CMB.TGL_REGMABA IS NOT NULL 
                    AND BK.ID_KELOMPOK_BIAYA ='{$d['ID_KELOMPOK_BIAYA']}'
                    ) " . str_replace(" ", "_", $d['NM_KELOMPOK_BIAYA']) . "_VER" . ", ";
            }
            $query = "
                SELECT ID_PROGRAM_STUDI,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,
                    {$query_kelompok}
                    {$query_kelompok_verifikasi}
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI AND CMB.TGL_DITERIMA IS NOT NULL
                    ) DITERIMA,
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI AND CMB.TGL_DITERIMA IS NOT NULL AND CMB.TGL_REGMABA IS NOT NULL
                    ) REGMABA,
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI AND CMB.TGL_DITERIMA IS NOT NULL AND CMB.TGL_REGMABA IS NULL
                    ) BELUM_REGMABA,
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI AND CMB.TGL_DITERIMA IS NOT NULL AND CMB.TGL_VERIFIKASI_KEUANGAN IS NULL AND CMB.TGL_REGMABA IS NOT NULL
                    ) BELUM_VERIFIKASI,
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        JOIN AUCC.CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI AND CMB.TGL_DITERIMA IS NOT NULL AND CMB.TGL_VERIFIKASI_KEUANGAN IS NOT NULL AND CMB.TGL_REGMABA IS NULL
                    ) SUDAH_VER_BELUM_REG,
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        JOIN AUCC.CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI AND CMB.TGL_DITERIMA IS NOT NULL AND CMB.TGL_VERIFIKASI_KEUANGAN IS NOT NULL AND CMD.STATUS_BIDIK_MISI=1 AND CMB.TGL_REGMABA IS NOT NULL
                    ) SUDAH_VER_BIDIK_MISI,
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        JOIN AUCC.CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI AND CMB.TGL_DITERIMA IS NOT NULL AND CMB.TGL_VERIFIKASI_KEUANGAN IS NOT NULL AND CMD.STATUS_BIDIK_MISI=0 AND CMB.TGL_REGMABA IS NOT NULL
                    ) SUDAH_VER_BUKAN_BIDIK_MISI,
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI AND CMB.TGL_DITERIMA IS NOT NULL AND CMB.ID_C_MHS IN 
                        ( SELECT ID_C_MHS FROM AUCC.PEMBAYARAN_CMHS WHERE TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL )
                    ) SUDAH_BAYAR
                FROM AUCC.PROGRAM_STUDI PS 
                JOIN AUCC.JENJANG J ON PS.ID_JENJANG=J.ID_JENJANG AND J.ID_JENJANG IN (1,5)
                WHERE PS.ID_FAKULTAS='{$fakultas}'
                GROUP BY ID_PROGRAM_STUDI,J.NM_JENJANG,PS.NM_PROGRAM_STUDI";
            return $this->db->QueryToArray($query);
        } else {
            $query_pertanggal = $this->load_verifikasi_per_tanggal($penerimaan, 2);
            foreach ($data_kel_biaya as $d) {
                $query_kelompok .=
                    "(
                    SELECT COUNT(*) 
                    FROM AUCC.CALON_MAHASISWA_BARU CMB
                    JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                    WHERE ID_PENERIMAAN='{$penerimaan}' 
                    AND CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                    AND CMB.TGL_REGMABA IS NOT NULL 
                    AND PS.ID_FAKULTAS=F.ID_FAKULTAS
                    AND CMB.ID_KELOMPOK_BIAYA ='{$d['ID_KELOMPOK_BIAYA']}'
                    ) " . str_replace(" ", "_", $d['NM_KELOMPOK_BIAYA']) . ", ";
            }
            foreach ($data_kel_biaya_verifikasi as $d) {
                $query_kelompok_verifikasi .=
                    "(
                    SELECT COUNT(*) 
                    FROM AUCC.CALON_MAHASISWA_BARU CMB
                    JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                    JOIN AUCC.BIAYA_KULIAH_CMHS BKC ON BKC.ID_C_MHS=CMB.ID_C_MHS
                    JOIN AUCC.BIAYA_KULIAH BK ON BK.ID_BIAYA_KULIAH=BKC.ID_BIAYA_KULIAH
                    WHERE ID_PENERIMAAN='{$penerimaan}' 
                    AND CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                    AND CMB.TGL_REGMABA IS NOT NULL 
                    AND PS.ID_FAKULTAS=F.ID_FAKULTAS
                    AND BK.ID_KELOMPOK_BIAYA ='{$d['ID_KELOMPOK_BIAYA']}'
                    ) " . str_replace(" ", "_", $d['NM_KELOMPOK_BIAYA']) . "_VER" . ", ";
            }
            $query = "
                SELECT F.ID_FAKULTAS,F.NM_FAKULTAS,
                    {$query_kelompok}
                    {$query_kelompok_verifikasi}
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND PS.ID_FAKULTAS=F.ID_FAKULTAS AND CMB.TGL_DITERIMA IS NOT NULL
                    ) DITERIMA,
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND PS.ID_FAKULTAS=F.ID_FAKULTAS AND CMB.TGL_DITERIMA IS NOT NULL AND CMB.TGL_REGMABA IS NOT NULL 
                    ) REGMABA,
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND PS.ID_FAKULTAS=F.ID_FAKULTAS AND CMB.TGL_DITERIMA IS NOT NULL AND CMB.TGL_REGMABA IS NULL
                    ) BELUM_REGMABA,
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        JOIN AUCC.CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                        JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND PS.ID_FAKULTAS=F.ID_FAKULTAS AND CMB.TGL_DITERIMA IS NOT NULL AND CMB.TGL_VERIFIKASI_KEUANGAN IS NULL AND CMB.TGL_REGMABA IS NOT NULL
                    ) BELUM_VERIFIKASI,
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        JOIN AUCC.CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                        JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND PS.ID_FAKULTAS=F.ID_FAKULTAS AND CMB.TGL_DITERIMA IS NOT NULL AND CMB.TGL_VERIFIKASI_KEUANGAN IS NOT NULL AND CMD.STATUS_BIDIK_MISI=1 AND CMB.TGL_REGMABA IS NOT NULL
                    ) SUDAH_VER_BIDIK_MISI,
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        JOIN AUCC.CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                        JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND PS.ID_FAKULTAS=F.ID_FAKULTAS AND CMB.TGL_DITERIMA IS NOT NULL AND CMB.TGL_VERIFIKASI_KEUANGAN IS NOT NULL AND CMD.STATUS_BIDIK_MISI=0 AND CMB.TGL_REGMABA IS NOT NULL
                    ) SUDAH_VER_BUKAN_BIDIK_MISI,
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        JOIN AUCC.CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                        JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND PS.ID_FAKULTAS=F.ID_FAKULTAS AND CMB.TGL_DITERIMA IS NOT NULL AND CMB.TGL_VERIFIKASI_KEUANGAN IS NOT NULL AND CMB.TGL_REGMABA IS NULL
                    ) SUDAH_VER_BELUM_REG,
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND PS.ID_FAKULTAS=F.ID_FAKULTAS AND CMB.TGL_DITERIMA IS NOT NULL AND CMB.ID_C_MHS IN 
                        ( SELECT ID_C_MHS FROM AUCC.PEMBAYARAN_CMHS WHERE TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL )
                    ) SUDAH_BAYAR
                FROM AUCC.FAKULTAS F
                GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS
                ORDER BY F.ID_FAKULTAS";
            return $this->db->QueryToArray($query);
        }
    }

    function load_detail_verifikasi_data($penerimaan, $fakultas, $status)
    {
        if ($fakultas != '') {
        } else {
            switch ($status) {
                case 1:
                    $query = "
                    SELECT CMB.*,CMD.*,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,F.NM_FAKULTAS
                    FROM CALON_MAHASISWA_BARU CMB
                    JOIN CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                    JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
                    JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                    WHERE CMB.ID_PENERIMAAN='{$penerimaan}'
                    ORDER BY PS.ID_FAKULTAS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI
                    ";
                    break;
                case 2:
                    $query = "
                    SELECT CMB.*,CMD.*,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,F.NM_FAKULTAS
                    FROM CALON_MAHASISWA_BARU CMB
                    JOIN CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                    JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
                    JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                    WHERE CMB.ID_PENERIMAAN='{$penerimaan}' AND CMB.TGL_REGMABA IS NOT NULL 
                    ORDER BY PS.ID_FAKULTAS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI
                    ";
                    break;
                case 3:
                    $query = "
                    SELECT CMB.*,CMD.*,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,F.NM_FAKULTAS
                    FROM CALON_MAHASISWA_BARU CMB
                    JOIN CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                    JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
                    JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                    WHERE CMB.ID_PENERIMAAN='{$penerimaan}' AND CMB.TGL_REGMABA IS NULL
                    ORDER BY PS.ID_FAKULTAS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI
                    ";
                    break;
                case 4:
                    $query = "
                    SELECT CMB.*,CMD.*,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,F.NM_FAKULTAS
                    FROM CALON_MAHASISWA_BARU CMB
                    JOIN CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                    JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
                    JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                    WHERE CMB.ID_PENERIMAAN='{$penerimaan}' AND CMB.TGL_VERIFIKASI_KEUANGAN IS NULL AND CMB.TGL_REGMABA IS NOT NULL
                    ORDER BY PS.ID_FAKULTAS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI
                    ";
                    break;
                case 5:
                    $query = "
                    SELECT CMB.*,CMD.*,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,F.NM_FAKULTAS
                    FROM CALON_MAHASISWA_BARU CMB
                    JOIN CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                    JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
                    JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                    WHERE CMB.ID_PENERIMAAN='{$penerimaan}' AND CMB.TGL_VERIFIKASI_KEUANGAN IS NOT NULL AND CMD.STATUS_BIDIK_MISI=1 AND CMB.TGL_REGMABA IS NOT NULL
                    ORDER BY PS.ID_FAKULTAS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI
                    ";
                    break;
                case 6:
                    $query = "
                    SELECT CMB.*,CMD.*,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,F.NM_FAKULTAS
                    FROM CALON_MAHASISWA_BARU CMB
                    JOIN CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                    JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
                    JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                    WHERE CMB.ID_PENERIMAAN='{$penerimaan}' AND CMB.TGL_VERIFIKASI_KEUANGAN IS NOT NULL AND CMD.STATUS_BIDIK_MISI=0 AND CMB.TGL_REGMABA IS NOT NULL
                    ORDER BY PS.ID_FAKULTAS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI
                    ";
                    break;
                case 7:
                    $query = "
                    SELECT CMB.*,CMD.*,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,F.NM_FAKULTAS
                    FROM CALON_MAHASISWA_BARU CMB
                    JOIN CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                    JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
                    JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                    WHERE CMB.ID_PENERIMAAN='{$penerimaan}' AND CMB.TGL_VERIFIKASI_KEUANGAN IS NOT NULL AND CMB.TGL_REGMABA IS NULL
                    ORDER BY PS.ID_FAKULTAS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI
                    ";
                    break;
                case 8:
                    $query = "
                    SELECT CMB.*,CMD.*,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,F.NM_FAKULTAS
                    FROM CALON_MAHASISWA_BARU CMB
                    JOIN CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                    JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
                    JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                    WHERE CMB.ID_PENERIMAAN='{$penerimaan}' AND CMB.ID_C_MHS IN 
                        ( SELECT ID_C_MHS FROM AUCC.PEMBAYARAN_CMHS WHERE TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL )
                    ORDER BY PS.ID_FAKULTAS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI
                    ";
                    break;
            }
        }
        return $this->db->QueryToArray($query);
    }

    function load_kelompok_biaya_penerimaan($penerimaan)
    {
        return $this->db->QueryToArray("
            SELECT KB.NM_KELOMPOK_BIAYA,KB.ID_KELOMPOK_BIAYA
            FROM KELOMPOK_BIAYA KB
            JOIN
            (
                SELECT DISTINCT(ID_KELOMPOK_BIAYA) ID_KELOMPOK_BIAYA
                FROM CALON_MAHASISWA_BARU CMB
                WHERE ID_PENERIMAAN='{$penerimaan}'
            ) CKB ON CKB.ID_KELOMPOK_BIAYA=KB.ID_KELOMPOK_BIAYA
            ORDER BY NM_KELOMPOK_BIAYA");
    }

    function load_kelompok_biaya_penerimaan_verifikasi($penerimaan)
    {
        return $this->db->QueryToArray("
            SELECT KB.NM_KELOMPOK_BIAYA,KB.ID_KELOMPOK_BIAYA
            FROM AUCC.KELOMPOK_BIAYA KB
            JOIN
            (
                SELECT DISTINCT(BK.ID_KELOMPOK_BIAYA) ID_KELOMPOK_BIAYA
                FROM AUCC.CALON_MAHASISWA_BARU CMB
                JOIN AUCC.BIAYA_KULIAH_CMHS BKC ON CMB.ID_C_MHS=BKC.ID_C_MHS
                JOIN AUCC.BIAYA_KULIAH BK ON BK.ID_BIAYA_KULIAH =BKC.ID_BIAYA_KULIAH
                WHERE ID_PENERIMAAN='{$penerimaan}'
            ) CKB ON CKB.ID_KELOMPOK_BIAYA=KB.ID_KELOMPOK_BIAYA
            ORDER BY NM_KELOMPOK_BIAYA");
    }

    function load_laporan_pengelompokan_biaya($fakultas, $penerimaan)
    {
        $data_laporan = array();
        if ($fakultas != '') {
            $row_prodi = $this->db->QueryToArray("
                SELECT J.NM_JENJANG,PS.NM_PROGRAM_STUDI,PS.ID_PROGRAM_STUDI,CM.JUMLAH_MHS
                FROM PROGRAM_STUDI PS
                JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                JOIN
                (
                    SELECT PS.ID_PROGRAM_STUDI,COUNT(*) JUMLAH_MHS
                    FROM CALON_MAHASISWA_BARU CMB
                    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                    WHERE TGL_DITERIMA IS NOT NULL AND ID_PENERIMAAN='{$penerimaan}'
                    GROUP BY PS.ID_PROGRAM_STUDI
                ) CM ON CM.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                WHERE PS.ID_FAKULTAS='{$fakultas}'
                ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI");
            foreach ($row_prodi as $row) {
                $data_pembayaran = array();
                foreach ($this->load_kelompok_biaya_penerimaan_verifikasi($penerimaan) as $kel) {
                    $this->db->Query("
                    SELECT BK.ID_KELOMPOK_BIAYA,
                        SUM(
                            CASE 
                            WHEN SK.SUMBANGAN>0
                            THEN 1
                            ELSE 0
                            END
                        ) BAYAR_LEBIH,
                        SUM(
                            CASE 
                            WHEN PEM.TOTAL_BAYAR IS NULL
                            THEN 1
                            ELSE 0
                            END
                        ) BELUM_BAYAR,
                        SUM(
                            CASE 
                            WHEN SPP.SPP IS NOT NULL OR PEN.PENDAFTARAN IS NOT NULL
                            THEN 1
                            ELSE 0
                            END
                        ) SUDAH_BAYAR,
                        SUM(
                            CASE 
                            WHEN (SK.SUMBANGAN=0 OR SK.SUMBANGAN IS NULL) AND PEN.PENDAFTARAN IS NOT NULL
                            THEN 1
                            ELSE 0
                            END
                        ) BAYAR_NORMAL
                        ,SUM(PEN.PENDAFTARAN) PENDAFTARAN,SUM(SPP.SPP) SPP,SUM(SP3.SP3) SP3,SUM(SK.SUMBANGAN) SUMBANGAN
                        FROM AUCC.CALON_MAHASISWA_BARU CMB
                        JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                        JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                        LEFT JOIN AUCC.BIAYA_KULIAH_CMHS BKC ON BKC.ID_C_MHS=CMB.ID_C_MHS
                        LEFT JOIN AUCC.BIAYA_KULIAH BK ON BK.ID_BIAYA_KULIAH=BKC.ID_BIAYA_KULIAH
                        LEFT JOIN 
                        (
                            SELECT ID_C_MHS,SUM(BESAR_BIAYA) TOTAL_BAYAR
                            FROM AUCC.PEMBAYARAN_CMHS
                            WHERE TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL
                            GROUP BY ID_C_MHS
                        ) PEM ON PEM.ID_C_MHS=CMB.ID_C_MHS
                        LEFT JOIN 
                        (
                            SELECT ID_C_MHS,SUM(PC.BESAR_BIAYA) SUMBANGAN
                            FROM AUCC.PEMBAYARAN_CMHS PC
                            JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PC.ID_DETAIL_BIAYA
                            WHERE TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL AND DB.ID_BIAYA=124
                            GROUP BY ID_C_MHS
                        ) SK ON SK.ID_C_MHS=CMB.ID_C_MHS
                        LEFT JOIN 
                        (
                            SELECT ID_C_MHS,SUM(PC.BESAR_BIAYA) SPP
                            FROM AUCC.PEMBAYARAN_CMHS PC
                            JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PC.ID_DETAIL_BIAYA
                            WHERE TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL AND (DB.ID_BIAYA=47 OR DB.ID_BIAYA=134)
                            GROUP BY ID_C_MHS
                        ) SPP ON SPP.ID_C_MHS=CMB.ID_C_MHS
                        LEFT JOIN 
                        (
                            SELECT ID_C_MHS,SUM(PC.BESAR_BIAYA) SP3
                            FROM AUCC.PEMBAYARAN_CMHS PC
                            JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PC.ID_DETAIL_BIAYA
                            WHERE TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL AND (DB.ID_BIAYA=81 OR DB.ID_BIAYA=135)
                            GROUP BY ID_C_MHS
                        ) SP3 ON SP3.ID_C_MHS=CMB.ID_C_MHS
                        LEFT JOIN 
                        (
                            SELECT ID_C_MHS,SUM(PC.BESAR_BIAYA) PENDAFTARAN
                            FROM AUCC.PEMBAYARAN_CMHS PC
                            JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PC.ID_DETAIL_BIAYA
                            WHERE TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL AND DB.ID_BIAYA NOT IN (124,47,81,134,135)
                            GROUP BY ID_C_MHS
                        ) PEN ON PEN.ID_C_MHS=CMB.ID_C_MHS
                        WHERE CMB.ID_PENERIMAAN='{$penerimaan}' AND BK.ID_KELOMPOK_BIAYA='{$kel['ID_KELOMPOK_BIAYA']}' AND PS.ID_PROGRAM_STUDI='{$row['ID_PROGRAM_STUDI']}'
                        GROUP BY BK.ID_KELOMPOK_BIAYA");
                    array_push($data_pembayaran, $this->db->FetchAssoc());
                }
                array_push($data_laporan, array_merge($row, array('DATA_KOLOM' => $data_pembayaran)));
            }
        } else {
            $row_fakultas = $this->db->QueryToArray("
                SELECT NM_FAKULTAS,CM.*
                FROM FAKULTAS F
                LEFT JOIN
                (
                    SELECT PS.ID_FAKULTAS,COUNT(*) JUMLAH_MHS
                    FROM CALON_MAHASISWA_BARU CMB
                    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                    WHERE TGL_DITERIMA IS NOT NULL AND ID_PENERIMAAN='{$penerimaan}'
                    GROUP BY PS.ID_FAKULTAS
                ) CM ON CM.ID_FAKULTAS=F.ID_FAKULTAS
                ORDER BY F.ID_FAKULTAS");
            foreach ($row_fakultas as $row) {
                $data_pembayaran = array();
                foreach ($this->load_kelompok_biaya_penerimaan_verifikasi($penerimaan) as $kel) {
                    $this->db->Query("
                    SELECT BK.ID_KELOMPOK_BIAYA,
                        SUM(
                            CASE 
                            WHEN SK.SUMBANGAN>0
                            THEN 1
                            ELSE 0
                            END
                        ) BAYAR_LEBIH,
                        SUM(
                            CASE 
                            WHEN PEM.TOTAL_BAYAR IS NULL
                            THEN 1
                            ELSE 0
                            END
                        ) BELUM_BAYAR,
                        SUM(
                            CASE 
                            WHEN SPP.SPP IS NOT NULL OR PEN.PENDAFTARAN IS NOT NULL
                            THEN 1
                            ELSE 0
                            END
                        ) SUDAH_BAYAR,
                        SUM(
                            CASE 
                            WHEN (SK.SUMBANGAN=0 OR SK.SUMBANGAN IS NULL) AND PEN.PENDAFTARAN IS NOT NULL
                            THEN 1
                            ELSE 0
                            END
                        ) BAYAR_NORMAL
                        ,SUM(PEN.PENDAFTARAN) PENDAFTARAN,SUM(SPP.SPP) SPP,SUM(SP3.SP3) SP3,SUM(SK.SUMBANGAN) SUMBANGAN
                        FROM AUCC.CALON_MAHASISWA_BARU CMB
                        JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                        JOIN AUCC.FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
                        JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                        LEFT JOIN AUCC.BIAYA_KULIAH_CMHS BKC ON BKC.ID_C_MHS=CMB.ID_C_MHS
                        LEFT JOIN AUCC.BIAYA_KULIAH BK ON BK.ID_BIAYA_KULIAH=BKC.ID_BIAYA_KULIAH
                        LEFT JOIN 
                        (
                            SELECT ID_C_MHS,SUM(BESAR_BIAYA) TOTAL_BAYAR
                            FROM AUCC.PEMBAYARAN_CMHS
                            WHERE TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL
                            GROUP BY ID_C_MHS
                        ) PEM ON PEM.ID_C_MHS=CMB.ID_C_MHS
                        LEFT JOIN 
                        (
                            SELECT ID_C_MHS,SUM(PC.BESAR_BIAYA) SUMBANGAN
                            FROM AUCC.PEMBAYARAN_CMHS PC
                            JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PC.ID_DETAIL_BIAYA
                            WHERE TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL AND DB.ID_BIAYA=124
                            GROUP BY ID_C_MHS
                        ) SK ON SK.ID_C_MHS=CMB.ID_C_MHS
                        LEFT JOIN 
                        (
                            SELECT ID_C_MHS,SUM(PC.BESAR_BIAYA) SPP
                            FROM AUCC.PEMBAYARAN_CMHS PC
                            JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PC.ID_DETAIL_BIAYA
                            WHERE TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL AND (DB.ID_BIAYA=47 OR DB.ID_BIAYA=134)
                            GROUP BY ID_C_MHS
                        ) SPP ON SPP.ID_C_MHS=CMB.ID_C_MHS
                        LEFT JOIN 
                        (
                            SELECT ID_C_MHS,SUM(PC.BESAR_BIAYA) SP3
                            FROM AUCC.PEMBAYARAN_CMHS PC
                            JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PC.ID_DETAIL_BIAYA
                            WHERE TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL AND (DB.ID_BIAYA=81 OR DB.ID_BIAYA=135)
                            GROUP BY ID_C_MHS
                        ) SP3 ON SP3.ID_C_MHS=CMB.ID_C_MHS
                        LEFT JOIN 
                        (
                            SELECT ID_C_MHS,SUM(PC.BESAR_BIAYA) PENDAFTARAN
                            FROM AUCC.PEMBAYARAN_CMHS PC
                            JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PC.ID_DETAIL_BIAYA
                            WHERE TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL AND DB.ID_BIAYA NOT IN (124,47,81,134,135)
                            GROUP BY ID_C_MHS
                        ) PEN ON PEN.ID_C_MHS=CMB.ID_C_MHS
                        WHERE CMB.ID_PENERIMAAN='{$penerimaan}' AND BK.ID_KELOMPOK_BIAYA='{$kel['ID_KELOMPOK_BIAYA']}' AND F.ID_FAKULTAS='{$row['ID_FAKULTAS']}'
                        GROUP BY BK.ID_KELOMPOK_BIAYA");
                    array_push($data_pembayaran, $this->db->FetchAssoc());
                }
                array_push($data_laporan, array_merge($row, array('DATA_KOLOM' => $data_pembayaran)));
            }
        }
        return $data_laporan;
    }

    function get_kelompok_biaya($id_kelompok_biaya)
    {
        $this->db->Query("SELECT * FROM KELOMPOK_BIAYA WHERE ID_KELOMPOK_BIAYA='{$id_kelompok_biaya}'");
        return $this->db->FetchAssoc();
    }

    function get_penerimaan($id_penerimaan)
    {
        $this->db->Query("
            SELECT PEN.*,JAL.NM_JALUR,J.NM_JENJANG
            FROM PENERIMAAN PEN
            JOIN JALUR JAL ON JAL.ID_JALUR=PEN.ID_JALUR
            JOIN JENJANG J ON J.ID_JENJANG=PEN.ID_JENJANG
            WHERE PEN.ID_PENERIMAAN='{$id_penerimaan}'
            ");
        return $this->db->FetchAssoc();
    }

    function load_detail_pengelompokan_biaya($penerimaan, $kelompok, $status)
    {
        switch ($status) {
            case 1:
                $f_status = "AND SK.SUMBANGAN>0"; // Bayar Lebih
                break;
            case 2:
                $f_status = "AND CMB.ID_C_MHS IN (SELECT ID_C_MHS FROM PEMBAYARAN_CMHS WHERE TGL_BAYAR IS NULL AND ID_BANK IS NULL) AND SPP.SPP IS NULL"; // Belum Bayar
                break;
            case 3:
                $f_status = "AND CMB.ID_C_MHS IN (SELECT ID_C_MHS FROM PEMBAYARAN_CMHS WHERE TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL)"; // Sudah Bayar
                break;
        }
        $data = $this->db->QueryToArray("
                SELECT CMB.ID_C_MHS,CMB.NO_UJIAN,CMB.NM_C_MHS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,F.NM_FAKULTAS,PEN.PENDAFTARAN,SPP.SPP,SP3.SP3,SK.SUMBANGAN
                        FROM AUCC.CALON_MAHASISWA_BARU CMB
                        JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                        JOIN AUCC.FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
                        JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                        JOIN AUCC.BIAYA_KULIAH_CMHS BKC ON BKC.ID_C_MHS=CMB.ID_C_MHS
                        JOIN AUCC.BIAYA_KULIAH BK ON BK.ID_BIAYA_KULIAH=BKC.ID_BIAYA_KULIAH
                        LEFT JOIN 
                        (
                            SELECT ID_C_MHS,SUM(BESAR_BIAYA) TOTAL_BAYAR
                            FROM AUCC.PEMBAYARAN_CMHS
                            WHERE TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL
                            GROUP BY ID_C_MHS
                        ) PEM ON PEM.ID_C_MHS=CMB.ID_C_MHS
                        LEFT JOIN 
                        (
                            SELECT ID_C_MHS,SUM(PC.BESAR_BIAYA) SUMBANGAN
                            FROM AUCC.PEMBAYARAN_CMHS PC
                            JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PC.ID_DETAIL_BIAYA
                            WHERE TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL AND DB.ID_BIAYA=124
                            GROUP BY ID_C_MHS
                        ) SK ON SK.ID_C_MHS=CMB.ID_C_MHS
                        LEFT JOIN 
                        (
                            SELECT ID_C_MHS,SUM(PC.BESAR_BIAYA) SPP
                            FROM AUCC.PEMBAYARAN_CMHS PC
                            JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PC.ID_DETAIL_BIAYA
                            WHERE TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL AND DB.ID_BIAYA=47
                            GROUP BY ID_C_MHS
                        ) SPP ON SPP.ID_C_MHS=CMB.ID_C_MHS
                        LEFT JOIN 
                        (
                            SELECT ID_C_MHS,SUM(PC.BESAR_BIAYA) SP3
                            FROM AUCC.PEMBAYARAN_CMHS PC
                            JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PC.ID_DETAIL_BIAYA
                            WHERE TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL AND DB.ID_BIAYA=81
                            GROUP BY ID_C_MHS
                        ) SP3 ON SP3.ID_C_MHS=CMB.ID_C_MHS
                        LEFT JOIN 
                        (
                            SELECT ID_C_MHS,SUM(PC.BESAR_BIAYA) PENDAFTARAN
                            FROM AUCC.PEMBAYARAN_CMHS PC
                            JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PC.ID_DETAIL_BIAYA
                            WHERE TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL AND DB.ID_BIAYA NOT IN (124,47,81)
                            GROUP BY ID_C_MHS
                        ) PEN ON PEN.ID_C_MHS=CMB.ID_C_MHS
                        WHERE CMB.ID_PENERIMAAN='{$penerimaan}' AND BK.ID_KELOMPOK_BIAYA='{$kelompok}' {$f_status}
                        ORDER BY F.ID_FAKULTAS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,CMB.NO_UJIAN,CMB.NM_C_MHS
            ");
        return $data;
    }

    // Menu Rekap Verifikator

    function load_laporan_verifikator($penerimaan, $tgl_awal, $tgl_akhir)
    {
        foreach ($this->load_kelompok_biaya_penerimaan($penerimaan) as $kel) {
            $col_name = str_replace(" ", "_", $kel['NM_KELOMPOK_BIAYA']);
            $col_name_bidik = str_replace(" ", "_", $kel['NM_KELOMPOK_BIAYA'] . "_B");
            $q_per_kelompok .= "
                ,SUM(
                    CASE
                        WHEN BK.ID_KELOMPOK_BIAYA='{$kel['ID_KELOMPOK_BIAYA']}' AND CMD.STATUS_BIDIK_MISI!=1
                        THEN 1
                        ELSE 0
                    END
                ) {$col_name},
                SUM(
                    CASE
                        WHEN BK.ID_KELOMPOK_BIAYA='{$kel['ID_KELOMPOK_BIAYA']}' AND CMD.STATUS_BIDIK_MISI=1
                        THEN 1
                        ELSE 0
                    END
                ) {$col_name_bidik}
                ";
        }
        return $this->db->QueryToArray("
            SELECT * FROM
            (
                SELECT CMB.ID_VERIFIKATOR_KEUANGAN,P.NM_PENGGUNA
                FROM AUCC.CALON_MAHASISWA_BARU CMB
                JOIN AUCC.PENGGUNA P ON CMB.ID_VERIFIKATOR_KEUANGAN=P.ID_PENGGUNA
                GROUP BY CMB.ID_VERIFIKATOR_KEUANGAN,P.NM_PENGGUNA
            ) V
            LEFT JOIN 
            (
                SELECT CMB.ID_VERIFIKATOR_KEUANGAN,COUNT(ID_C_MHS) JUMLAH_MHS
                {$q_per_kelompok}
                FROM AUCC.CALON_MAHASISWA_BARU CMB
                JOIN CALON_MAHASISWA_DATA CMD ON CMB.ID_C_MHS=CMD.ID_C_MHS
                JOIN BIAYA_KULIAH_CMHS BKC ON BKC.ID_C_MHS=CMB.ID_C_MHS
                JOIN BIAYA_KULIAH BK ON BK.ID_BIAYA_KULIAH = BKC.ID_BIAYA_KULIAH
                WHERE TO_CHAR(TGL_VERIFIKASI_KEUANGAN,'YYYY-MM-DD') BETWEEN '{$tgl_awal}' AND '{$tgl_akhir}' AND ID_PENERIMAAN='{$penerimaan}'
                GROUP BY ID_VERIFIKATOR_KEUANGAN
            ) CM ON CM.ID_VERIFIKATOR_KEUANGAN=V.ID_VERIFIKATOR_KEUANGAN
            WHERE NM_PENGGUNA!='NAMBI SEMBILU'
            ORDER BY NM_PENGGUNA");
    }

    // Menu Pembayaran SP


    function load_report_bank_pembayaran_sp($tgl_awal, $tgl_akhir, $fakultas, $prodi)
    {
        $q_fakultas = $fakultas != '' ? "AND PR.ID_FAKULTAS='{$fakultas}'" : "";
        $q_prodi = $prodi != '' ? "AND PR.ID_PROGRAM_STUDI='{$prodi}'" : "";
        return $this->db->QueryToArray("
		SELECT B.ID_BANK,B.NM_BANK,SUM(PS.PEMBAYARAN) PEMBAYARAN,COUNT(PS.ID_MHS) JUMLAH_MHS FROM AUCC.BANK B
                LEFT JOIN
                (
                    SELECT PS.ID_BANK,SUM(PS.BESAR_BIAYA) PEMBAYARAN,M.ID_MHS
                    FROM AUCC.PEMBAYARAN_SP PS
                    JOIN AUCC.MAHASISWA M ON M.ID_MHS=PS.ID_MHS
                    JOIN AUCC.PROGRAM_STUDI PR ON PR.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
                    WHERE 
                        TO_CHAR(PS.TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' AND 
                        TO_CHAR(PS.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}'
                        {$q_fakultas} {$q_prodi}
                    GROUP BY PS.ID_BANK,M.ID_MHS
                ) PS ON PS.ID_BANK=B.ID_BANK
                GROUP BY B.ID_BANK,B.NM_BANK
                ORDER BY B.NM_BANK");
    }

    function load_report_detail_pembayaran_sp($tgl_awal, $tgl_akhir, $fakultas, $prodi, $bank)
    {
        $q_bank = $bank != '' ? "AND B.ID_BANK='{$bank}'" : "";
        $q_fakultas = $fakultas != '' ? "AND PR.ID_FAKULTAS='{$fakultas}'" : "";
        $q_prodi = $prodi != '' ? "AND PR.ID_PROGRAM_STUDI='{$prodi}'" : "";
        return $this->db->QueryToArray("
            SELECT PS.*,P.NM_PENGGUNA,S.NM_SEMESTER,S.GROUP_SEMESTER,S.TAHUN_AJARAN,M.NIM_MHS,B.NM_BANK,BV.NAMA_BANK_VIA,
                PR.NM_PROGRAM_STUDI,J.NM_JENJANG,STA.NM_STATUS_PENGGUNA,JAL.NM_JALUR
            FROM AUCC.PEMBAYARAN_SP PS
            JOIN AUCC.SEMESTER S ON S.ID_SEMESTER=PS.ID_SEMESTER
            JOIN AUCC.BANK B ON B.ID_BANK = PS.ID_BANK
            JOIN AUCC.BANK_VIA BV ON BV.ID_BANK_VIA = PS.ID_BANK_VIA
            JOIN AUCC.MAHASISWA M ON M.ID_MHS = PS.ID_MHS
            JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
            JOIN AUCC.PROGRAM_STUDI PR ON PR.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            JOIN AUCC.JENJANG J ON J.ID_JENJANG = PR.ID_JENJANG
            LEFT JOIN AUCC.JALUR_MAHASISWA JM ON JM.ID_MHS=M.ID_MHS AND JM.ID_JALUR_AKTIF=1
            LEFT JOIN AUCC.JALUR JAL ON JAL.ID_JALUR=JM.ID_JALUR
            LEFT JOIN AUCC.STATUS_PENGGUNA STA ON STA.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
            WHERE TO_CHAR(PS.TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' AND TO_CHAR(PS.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}'
            {$q_bank} {$q_fakultas} {$q_prodi}
            ORDER BY M.NIM_MHS,J.NM_JENJANG,PR.NM_PROGRAM_STUDI");
    }

    // Laporan Penghasilan Ortu

    function load_report_penghasilan_ortu($fakultas, $penerimaan)
    {
        $result = array();
        $q_fakultas = $fakultas != '' ? "AND PS.ID_FAKULTAS='{$fakultas}'" : "";
        $data_laporan = $this->db->QueryToArray("
            SELECT CMB.ID_C_MHS,CMB.NO_UJIAN,CMB.NM_C_MHS,CMB.NIM_MHS,PS.NM_PROGRAM_STUDI,JAL.NM_JALUR,J.NM_JENJANG,CMO.*,KB.NM_KELOMPOK_BIAYA,CMD.STATUS_BIDIK_MISI
            FROM AUCC.CALON_MAHASISWA_BARU CMB
            JOIN AUCC.CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
            JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
            JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN AUCC.JALUR JAL ON JAL.ID_JALUR=CMB.ID_JALUR
            JOIN AUCC.CALON_MAHASISWA_ORTU CMO ON CMO.ID_C_MHS=CMB.ID_C_MHS
            LEFT JOIN BIAYA_KULIAH_CMHS BKC ON BKC.ID_C_MHS=CMB.ID_C_MHS
            LEFT JOIN AUCC.BIAYA_KULIAH BK ON BK.ID_BIAYA_KULIAH=BKC.ID_BIAYA_KULIAH
            LEFT JOIN AUCC.KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA=BK.ID_KELOMPOK_BIAYA
            WHERE CMB.ID_PENERIMAAN='{$penerimaan}' {$q_fakultas}
            ORDER BY PS.ID_FAKULTAS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,CMB.NO_UJIAN,CMB.NM_C_MHS
            ");
        foreach ($data_laporan as $d) {
            // Ambil Data Skor
            $this->db->Query("SELECT * FROM CALON_MAHASISWA_ORTU WHERE ID_C_MHS='{$d['ID_C_MHS']}'");
            $cmo = $this->db->FetchAssoc();
            $s_penghasilan = $this->GetRangeSkor($cmo['RANGE_PENGHASILAN']);
            $s_pekerjaan_ayah = $this->GetSkorPekerjaan($cmo['PEKERJAAN_AYAH']);
            $s_pekerjaan_ibu = $this->GetSkorPekerjaan($cmo['PEKERJAAN_IBU']);
            if ($s_pekerjaan_ayah > $s_pekerjaan_ibu) {
                $s_pekerjaan = $s_pekerjaan_ayah;
            } else {
                $s_pekerjaan = $s_pekerjaan_ibu;
            }
            $s_njop = $this->GetRangeSkor($cmo['RANGE_NJOP']);
            $s_listrik = $this->GetRangeSkor($cmo['RANGE_LISTRIK']);
            $s_air = $this->GetRangeSkor($cmo['RANGE_AIR']);
            $s_pbb = $this->GetRangeSkor($cmo['RANGE_PBB']);
            $s_asset = $this->GetSkorKendaraan($cmo['KENDARAAN_R2'] + $cmo['KENDARAAN_R4']);
            $n_penghasilan = 0.3 * $s_penghasilan;
            $n_pekerjaan = 0.3 * $s_pekerjaan;
            $n_njop = 0.075 * $s_njop;
            $n_listrik = 0.05 * $s_listrik;
            $n_air = 0.05 * $s_air;
            $n_pbb = 0.075 * $s_pbb;
            $n_asset = 0.15 * $s_asset;
            $total_skor = $n_penghasilan + $n_pekerjaan + $n_njop + $n_listrik + $n_air + $n_pbb + $n_asset;
            array_push($result, array_merge($d, array('SKOR_VERIFIKASI' => $total_skor)));
        }
        return $result;
    }

    // Kontrol Status Penundaan Pembayaran

    function load_report_penundaan_pembayaran($fakultas, $penerimaan)
    {
        $q_fakultas = $fakultas != '' ? "AND PS.ID_FAKULTAS='{$fakultas}'" : "";
        return $this->db->QueryToArray("
            SELECT M.ID_MHS,M.NIM_MHS,CMB.ID_C_MHS,CMB.NO_UJIAN,CMB.NM_C_MHS,PS.NM_PROGRAM_STUDI,JAL.NM_JALUR,J.NM_JENJANG,PC.TOTAL_TUNDA,PC.KETERANGAN,(SELECT COUNT(*) FROM PEMBAYARAN WHERE ID_MHS=M.ID_MHS AND ID_STATUS_PEMBAYARAN=1) ADA
            FROM AUCC.CALON_MAHASISWA_BARU CMB
            JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
            JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN AUCC.JALUR JAL ON JAL.ID_JALUR=CMB.ID_JALUR
            JOIN (
                SELECT ID_C_MHS,KETERANGAN,SUM(BESAR_BIAYA) TOTAL_TUNDA
                FROM AUCC.PEMBAYARAN_CMHS
                WHERE ID_STATUS_PEMBAYARAN=3
                GROUP BY ID_C_MHS,KETERANGAN
            ) PC ON PC.ID_C_MHS=CMB.ID_C_MHS
            LEFT JOIN MAHASISWA M ON M.NIM_MHS=CMB.NIM_MHS
            WHERE CMB.ID_PENERIMAAN='{$penerimaan}' {$q_fakultas}
            ORDER BY PS.ID_FAKULTAS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,CMB.NO_UJIAN,CMB.NM_C_MHS
            ");
    }

    // Laporan Tahunan Per Prodi

    function load_row_tahunan_prodi($fakultas)
    {
        return $this->db->QueryToArray("
            SELECT PS.ID_PROGRAM_STUDI,J.NM_JENJANG,PS.NM_PROGRAM_STUDI 
            FROM AUCC.PROGRAM_STUDI PS
            JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            WHERE PS.ID_FAKULTAS='{$fakultas}'
            ");
    }

    function load_row_tahunan_fakultas()
    {
        $query = "SELECT * FROM FAKULTAS ORDER BY ID_FAKULTAS";
        return $this->db->QueryToArray($query);
    }

    function load_laporan_tahunan_perfakultas($tgl_awal, $tgl_akhir, $semester)
    {
        $array_hasil = array();
        foreach ($this->load_row_tahunan_fakultas() as $p) {
            $arr_bayar = array();
            foreach ($semester as $s) {
                $query_laporan = "
                SELECT P.*,PC.*,F.NM_FAKULTAS FROM AUCC.FAKULTAS F
                    LEFT JOIN
                    (
                      SELECT PS.ID_FAKULTAS,COUNT(DISTINCT(PEM.ID_MHS)) JUMLAH_MHS,
                      SUM(CASE
                        WHEN D.ID_BIAYA=47
                        THEN PEM.BESAR_BIAYA
                        ELSE 0
                      END) SOP,
                      SUM(CASE
                        WHEN D.ID_BIAYA=61
                        THEN PEM.BESAR_BIAYA
                        ELSE 0
                      END) PRAKTIKUM,
                      SUM(CASE
                        WHEN D.ID_BIAYA=81
                        THEN PEM.BESAR_BIAYA
                        ELSE 0
                      END) SP3,
                      SUM(CASE
                        WHEN D.ID_BIAYA=124
                        THEN PEM.BESAR_BIAYA
                        ELSE 0
                      END) SUKARELA
                      FROM AUCC.PEMBAYARAN PEM
                      JOIN AUCC.DETAIL_BIAYA D ON D.ID_DETAIL_BIAYA=PEM.ID_DETAIL_BIAYA
                      JOIN AUCC.MAHASISWA M ON M.ID_MHS=PEM.ID_MHS
                      JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
                      WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') BETWEEN '{$tgl_awal}' AND '{$tgl_akhir}' 
                      AND PEM.FLAG_PEMINDAHAN IS NULL AND PEM.ID_BANK IS NOT NULL AND PEM.ID_SEMESTER {$s}
                      GROUP BY PS.ID_FAKULTAS
                    ) P ON P.ID_FAKULTAS=F.ID_FAKULTAS
                    LEFT JOIN
                    (
                      SELECT PS.ID_FAKULTAS,COUNT(DISTINCT(PEM.ID_C_MHS)) JUMLAH_CMHS,
                      SUM(CASE
                        WHEN D.ID_BIAYA=47
                        THEN PEM.BESAR_BIAYA
                        ELSE 0
                      END) SOP_MABA,
                      SUM(CASE
                        WHEN D.ID_BIAYA=61
                        THEN PEM.BESAR_BIAYA
                        ELSE 0
                      END) PRAKTIKUM_MABA,
                      SUM(CASE
                        WHEN D.ID_BIAYA=81
                        THEN PEM.BESAR_BIAYA
                        ELSE 0
                      END) SP3_MABA,
                      SUM(CASE
                        WHEN D.ID_BIAYA=124
                        THEN PEM.BESAR_BIAYA
                        ELSE 0
                      END) SUKARELA_MABA
                      FROM AUCC.PEMBAYARAN_CMHS PEM
                      JOIN AUCC.DETAIL_BIAYA D ON D.ID_DETAIL_BIAYA=PEM.ID_DETAIL_BIAYA
                      JOIN AUCC.CALON_MAHASISWA_BARU CMB ON CMB.ID_C_MHS=PEM.ID_C_MHS
                      JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                      WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') BETWEEN '{$tgl_awal}' AND '{$tgl_akhir}' 
                      AND PEM.ID_BANK IS NOT NULL
                      AND PEM.ID_SEMESTER {$s}
                      GROUP BY PS.ID_FAKULTAS
                    ) PC ON PC.ID_FAKULTAS=F.ID_FAKULTAS
                    WHERE F.ID_FAKULTAS ='{$p['ID_FAKULTAS']}'
                ";
                $this->db->Query($query_laporan);
                array_push($arr_bayar, $this->db->FetchAssoc());
            }
            array_push($array_hasil, array_merge($p, array('BAYAR' => $arr_bayar)));
        }

        return $array_hasil;
    }

    function load_laporan_tahunan_perprodi($fakultas, $tgl_awal, $tgl_akhir, $semester)
    {
        $array_hasil = array();
        foreach ($this->load_row_tahunan_prodi($fakultas) as $p) {
            $arr_bayar = array();
            foreach ($semester as $s) {
                $query_laporan = "
                SELECT P.*,PC.* FROM AUCC.PROGRAM_STUDI PS
                    JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                    LEFT JOIN
                    (
                      SELECT M.ID_PROGRAM_STUDI,COUNT(DISTINCT(PEM.ID_MHS)) JUMLAH_MHS,
                      SUM(CASE
                        WHEN D.ID_BIAYA=47
                        THEN PEM.BESAR_BIAYA
                        ELSE 0
                      END) SOP,
                      SUM(CASE
                        WHEN D.ID_BIAYA=61
                        THEN PEM.BESAR_BIAYA
                        ELSE 0
                      END) PRAKTIKUM,
                      SUM(CASE
                        WHEN D.ID_BIAYA=81
                        THEN PEM.BESAR_BIAYA
                        ELSE 0
                      END) SP3,
                      SUM(CASE
                        WHEN D.ID_BIAYA=124
                        THEN PEM.BESAR_BIAYA
                        ELSE 0
                      END) SUKARELA
                      FROM AUCC.PEMBAYARAN PEM
                      JOIN AUCC.DETAIL_BIAYA D ON D.ID_DETAIL_BIAYA=PEM.ID_DETAIL_BIAYA
                      JOIN AUCC.MAHASISWA M ON M.ID_MHS=PEM.ID_MHS
                      WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') BETWEEN '{$tgl_awal}' AND '{$tgl_akhir}' 
                      AND PEM.FLAG_PEMINDAHAN IS NULL AND PEM.ID_BANK IS NOT NULL AND PEM.ID_SEMESTER {$s}
                      GROUP BY M.ID_PROGRAM_STUDI
                    ) P ON P.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                    LEFT JOIN
                    (
                      SELECT CMB.ID_PROGRAM_STUDI,COUNT(DISTINCT(PEM.ID_C_MHS)) JUMLAH_CMHS,
                      SUM(CASE
                        WHEN D.ID_BIAYA=47
                        THEN PEM.BESAR_BIAYA
                        ELSE 0
                      END) SOP_MABA,
                      SUM(CASE
                        WHEN D.ID_BIAYA=61
                        THEN PEM.BESAR_BIAYA
                        ELSE 0
                      END) PRAKTIKUM_MABA,
                      SUM(CASE
                        WHEN D.ID_BIAYA=81
                        THEN PEM.BESAR_BIAYA
                        ELSE 0
                      END) SP3_MABA,
                      SUM(CASE
                        WHEN D.ID_BIAYA=124
                        THEN PEM.BESAR_BIAYA
                        ELSE 0
                      END) SUKARELA_MABA
                      FROM AUCC.PEMBAYARAN_CMHS PEM
                      JOIN AUCC.DETAIL_BIAYA D ON D.ID_DETAIL_BIAYA=PEM.ID_DETAIL_BIAYA
                      JOIN AUCC.CALON_MAHASISWA_BARU CMB ON CMB.ID_C_MHS=PEM.ID_C_MHS
                      WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') BETWEEN '{$tgl_awal}' AND '{$tgl_akhir}' 
                      AND PEM.ID_BANK IS NOT NULL
                      AND PEM.ID_SEMESTER {$s}
                      GROUP BY CMB.ID_PROGRAM_STUDI
                    ) PC ON PC.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                    WHERE PS.ID_PROGRAM_STUDI ='{$p['ID_PROGRAM_STUDI']}'
                ";
                $this->db->Query($query_laporan);
                array_push($arr_bayar, $this->db->FetchAssoc());
            }
            array_push($array_hasil, array_merge($p, array('BAYAR' => $arr_bayar)));
        }

        return $array_hasil;
    }

    function load_row_tahunan_prodi_detail($fakultas)
    {
        return $this->db->QueryToArray("
            SELECT PS.ID_PROGRAM_STUDI,JAL.ID_JALUR,KB.ID_KELOMPOK_BIAYA,KB.NM_KELOMPOK_BIAYA,M.THN_ANGKATAN_MHS,JAL.NM_JALUR,
                J.NM_JENJANG,PS.NM_PROGRAM_STUDI 
            FROM AUCC.PROGRAM_STUDI PS
            JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN AUCC.MAHASISWA M ON M.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
            JOIN AUCC.JALUR_MAHASISWA JM ON JM.ID_MHS=M.ID_MHS AND JM.ID_JALUR_AKTIF=1
            JOIN AUCC.JALUR JAL ON JAL.ID_JALUR=JM.ID_JALUR
            JOIN AUCC.KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA=M.ID_KELOMPOK_BIAYA
            WHERE PS.ID_FAKULTAS='{$fakultas}'
            GROUP BY PS.ID_PROGRAM_STUDI,JAL.ID_JALUR,KB.ID_KELOMPOK_BIAYA,KB.NM_KELOMPOK_BIAYA,M.THN_ANGKATAN_MHS,JAL.NM_JALUR,
            J.NM_JENJANG,PS.NM_PROGRAM_STUDI 
            ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI,JAL.NM_JALUR,KB.NM_KELOMPOK_BIAYA,M.THN_ANGKATAN_MHS
            ");
    }

    function load_laporan_tahunan_perprodi_perjalur($fakultas, $tgl_awal, $tgl_akhir, $semester)
    {
        $array_hasil = array();
        foreach ($this->load_row_tahunan_prodi_detail($fakultas) as $p) {
            $arr_bayar = array();
            foreach ($semester as $s) {
                $query_laporan = "
                SELECT P.*,PC.JUMLAH_CMHS,PC.SOP_MABA,PC.PRAKTIKUM_MABA,PC.SP3_MABA,PC.SUKARELA_MABA
                    FROM AUCC.PROGRAM_STUDI PS
                    JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                    LEFT JOIN
                    (
                      SELECT M.ID_PROGRAM_STUDI,COUNT(DISTINCT(PEM.ID_MHS)) JUMLAH_MHS,
                      SUM(CASE
                        WHEN D.ID_BIAYA=47
                        THEN PEM.BESAR_BIAYA
                        ELSE 0
                      END) SOP,
                      SUM(CASE
                        WHEN D.ID_BIAYA=61
                        THEN PEM.BESAR_BIAYA
                        ELSE 0
                      END) PRAKTIKUM,
                      SUM(CASE
                        WHEN D.ID_BIAYA=81
                        THEN PEM.BESAR_BIAYA
                        ELSE 0
                      END) SP3,
                      SUM(CASE
                        WHEN D.ID_BIAYA=124
                        THEN PEM.BESAR_BIAYA
                        ELSE 0
                      END) SUKARELA
                      FROM AUCC.PEMBAYARAN PEM
                      JOIN AUCC.DETAIL_BIAYA D ON D.ID_DETAIL_BIAYA=PEM.ID_DETAIL_BIAYA
                      JOIN AUCC.MAHASISWA M ON M.ID_MHS=PEM.ID_MHS
                      JOIN AUCC.JALUR_MAHASISWA JM ON JM.ID_MHS=M.ID_MHS AND JM.ID_JALUR_AKTIF=1
                      JOIN AUCC.KELOMPOK_BIAYA KB ON M.ID_KELOMPOK_BIAYA=KB.ID_KELOMPOK_BIAYA
                      WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') BETWEEN '{$tgl_awal}' AND '{$tgl_akhir}' 
                      AND PEM.FLAG_PEMINDAHAN IS NULL 
                      AND PEM.ID_BANK IS NOT NULL 
                      AND PEM.ID_SEMESTER {$s} 
                      AND JM.ID_JALUR='{$p['ID_JALUR']}'
                      AND KB.ID_KELOMPOK_BIAYA='{$p['ID_KELOMPOK_BIAYA']}' 
                      AND M.THN_ANGKATAN_MHS='{$p['THN_ANGKATAN_MHS']}'
                      GROUP BY M.ID_PROGRAM_STUDI
                    ) P ON P.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                    LEFT JOIN
                    (
                      SELECT CMB.ID_PROGRAM_STUDI,COUNT(DISTINCT(PEM.ID_C_MHS)) JUMLAH_CMHS,
                      SUM(CASE
                        WHEN D.ID_BIAYA=47
                        THEN PEM.BESAR_BIAYA
                        ELSE 0
                      END) SOP_MABA,
                      SUM(CASE
                        WHEN D.ID_BIAYA=61
                        THEN PEM.BESAR_BIAYA
                        ELSE 0
                      END) PRAKTIKUM_MABA,
                      SUM(CASE
                        WHEN D.ID_BIAYA=81
                        THEN PEM.BESAR_BIAYA
                        ELSE 0
                      END) SP3_MABA,
                      SUM(CASE
                        WHEN D.ID_BIAYA=124
                        THEN PEM.BESAR_BIAYA
                        ELSE 0
                      END) SUKARELA_MABA
                      FROM AUCC.PEMBAYARAN_CMHS PEM
                      JOIN AUCC.CALON_MAHASISWA_BARU CMB ON CMB.ID_C_MHS=PEM.ID_C_MHS
                      JOIN AUCC.MAHASISWA M ON M.NIM_MHS=CMB.NIM_MHS
                      JOIN AUCC.KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA=M.ID_KELOMPOK_BIAYA
                      JOIN AUCC.DETAIL_BIAYA D ON D.ID_DETAIL_BIAYA=PEM.ID_DETAIL_BIAYA
                      WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') BETWEEN '{$tgl_awal}' AND '{$tgl_akhir}' 
                      AND PEM.ID_BANK IS NOT NULL
                      AND PEM.ID_SEMESTER {$s}
                      AND CMB.ID_JALUR='{$p['ID_JALUR']}'
                      AND KB.ID_KELOMPOK_BIAYA='{$p['ID_KELOMPOK_BIAYA']}' 
                      AND M.THN_ANGKATAN_MHS='{$p['THN_ANGKATAN_MHS']}'
                      GROUP BY CMB.ID_PROGRAM_STUDI
                    ) PC ON PC.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                    WHERE PS.ID_PROGRAM_STUDI ='{$p['ID_PROGRAM_STUDI']}'
                ";
                $this->db->Query($query_laporan);
                array_push($arr_bayar, $this->db->FetchAssoc());
            }
            array_push($array_hasil, array_merge($p, array('BAYAR' => $arr_bayar)));
        }

        return $array_hasil;
    }

    function get_semester_aktif()
    {
        $this->db->Query("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True'");
        return $this->db->FetchAssoc();
    }

    function get_semester_sebelum_aktif()
    {
        $semester_aktif = $this->get_semester_aktif();
        if ($semester_aktif['NM_SEMESTER'] == 'Genap') {
            $query = "SELECT * FROM SEMESTER WHERE THN_AKADEMIK_SEMESTER={$semester_aktif['THN_AKADEMIK_SEMESTER']} AND NM_SEMESTER!='{$semester_aktif['NM_SEMESTER']}' AND TIPE_SEMESTER='REG'";
        } else {
            $query = "SELECT * FROM SEMESTER WHERE THN_AKADEMIK_SEMESTER={$semester_aktif['THN_AKADEMIK_SEMESTER']}-1 AND NM_SEMESTER!='{$semester_aktif['NM_SEMESTER']}'  AND TIPE_SEMESTER='REG'";
        }
        $this->db->Query($query);
        return $this->db->FetchAssoc();
    }

    function get_semester_sebelumnya()
    {
        $hasil_semester = '';
        $semester_aktif = $this->get_semester_sebelum_aktif();
        if ($semester_aktif['NM_SEMESTER'] == 'Genap') {
            $semester_sebelum = $this->db->QueryToArray("SELECT ID_SEMESTER FROM AUCC.SEMESTER WHERE (NM_SEMESTER='Genap' OR NM_SEMESTER='Ganjil') AND (THN_AKADEMIK_SEMESTER<'{$semester_aktif['THN_AKADEMIK_SEMESTER']}' OR (NM_SEMESTER='Ganjil' AND THN_AKADEMIK_SEMESTER={$semester_aktif['THN_AKADEMIK_SEMESTER']}))");
        } else {
            $semester_sebelum = $this->db->QueryToArray("SELECT ID_SEMESTER FROM SEMESTER WHERE (NM_SEMESTER='Genap' OR NM_SEMESTER='Ganjil') AND THN_AKADEMIK_SEMESTER<'{$semester_aktif['THN_AKADEMIK_SEMESTER']}'");
        }
        $no = 1;
        foreach ($semester_sebelum as $s) {
            if ($no == count($semester_sebelum)) {
                $hasil_semester .= $s['ID_SEMESTER'];
            } else {
                $hasil_semester .= $s['ID_SEMESTER'] . ",";
            }
            $no++;
        }
        return $hasil_semester;
    }

    function get_semester_setelahnya()
    {
        $hasil_semester = '';
        $semester_aktif = $this->get_semester_aktif();
        if ($semester_aktif['NM_SEMESTER'] == 'Ganjil') {
            $semester_setelah = $this->db->QueryToArray("SELECT ID_SEMESTER FROM AUCC.SEMESTER WHERE (NM_SEMESTER='Genap' OR NM_SEMESTER='Ganjil') AND (THN_AKADEMIK_SEMESTER>'{$semester_aktif['THN_AKADEMIK_SEMESTER']}' OR (NM_SEMESTER='Genap' AND THN_AKADEMIK_SEMESTER={$semester_aktif['THN_AKADEMIK_SEMESTER']}))");
        } else {
            $semester_setelah = $this->db->QueryToArray("SELECT ID_SEMESTER FROM SEMESTER WHERE (NM_SEMESTER='Genap' OR NM_SEMESTER='Ganjil') AND THN_AKADEMIK_SEMESTER>{$semester_aktif['THN_AKADEMIK_SEMESTER']}");
        }
        $no = 1;
        foreach ($semester_setelah as $s) {
            if ($no == count($semester_setelah)) {
                $hasil_semester .= $s['ID_SEMESTER'];
            } else {
                $hasil_semester .= $s['ID_SEMESTER'] . ",";
            }
            $no++;
        }
        return $hasil_semester;
    }

    // Laporan Bayar per Biaya

    function load_laporan_bayar_perbiaya_bank($tgl_awal, $tgl_akhir, $biaya, $fakultas, $prodi)
    {
        $q_fakultas = $fakultas != '' ? "AND PS.ID_FAKULTAS='{$fakultas}'" : "";
        $q_prodi = $prodi != '' ? "AND PS.ID_PROGRAM_STUDI='{$prodi}'" : "";
        return $this->db->QueryToArray("
            SELECT B.ID_BANK,B.NM_BANK,PEM.JUMLAH_MHS,PEM.TOTAL_BIAYA FROM 
                (SELECT * FROM BANK) B
            LEFT JOIN   
            (
                SELECT PEM.ID_BANK,COUNT(DISTINCT(TM.ID_MHS)) JUMLAH_MHS,SUM(PEM.BESAR_PEMBAYARAN) TOTAL_BIAYA 
                FROM PEMBAYARAN PEM
                JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN=PEM.ID_TAGIHAN
                JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS=TAG.ID_TAGIHAN_MHS
                JOIN MAHASISWA M ON M.ID_MHS=TM.ID_MHS
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
                JOIN DETAIL_BIAYA DB ON TAG.ID_DETAIL_BIAYA=DB.ID_DETAIL_BIAYA
                WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' 
                    AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' 
                    AND DB.ID_BIAYA='{$biaya}'
                    AND PEM.BESAR_PEMBAYARAN!=0 AND PEM.ID_STATUS_PEMBAYARAN=1
                    {$q_fakultas}
                    {$q_prodi}
                GROUP BY PEM.ID_BANK
            ) PEM ON PEM.ID_BANK = B.ID_BANK");
    }

    function load_laporan_bayar_perbiaya_detail($tgl_awal, $tgl_akhir, $biaya, $bank, $fakultas, $prodi)
    {
        $q_fakultas = $fakultas != '' ? "AND F.ID_FAKULTAS='{$fakultas}'" : "";
        $q_prodi = $prodi != '' ? "AND PS.ID_PROGRAM_STUDI='{$prodi}'" : "";
        $q_bank = $bank == '' ? "" : "AND PEM.ID_BANK ='{$bank}'";
        return $this->db->QueryToArray("
            SELECT M.ID_MHS,M.NIM_MHS,M.THN_ANGKATAN_MHS,P.NM_PENGGUNA,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,F.ID_FAKULTAS,UPPER(F.NM_FAKULTAS) NM_FAKULTAS,PEM.TGL_BAYAR,SUM(PEM.BESAR_PEMBAYARAN) BESAR_BIAYA ,
            SP.NM_STATUS_PENGGUNA STATUS,SP.STATUS_AKTIF AKTIF
                FROM PEMBAYARAN PEM
                JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN=PEM.ID_TAGIHAN
                JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS=TAG.ID_TAGIHAN_MHS
                JOIN DETAIL_BIAYA DB ON TAG.ID_DETAIL_BIAYA=DB.ID_DETAIL_BIAYA
                JOIN MAHASISWA M ON M.ID_MHS = TM.ID_MHS
                JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                LEFT JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
                JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
                JOIN JENJANG J ON J.ID_JENJANG  = PS.ID_JENJANG
            WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' 
            AND DB.ID_BIAYA='{$biaya}' AND PEM.BESAR_PEMBAYARAN!=0 
            {$q_bank} 
            {$q_fakultas} 
            {$q_prodi} 
            AND PEM.ID_STATUS_PEMBAYARAN=1
            GROUP BY M.ID_MHS,M.NIM_MHS,M.THN_ANGKATAN_MHS,P.NM_PENGGUNA,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,F.ID_FAKULTAS,F.NM_FAKULTAS,PEM.TGL_BAYAR,SP.NM_STATUS_PENGGUNA,SP.STATUS_AKTIF
            ORDER BY F.ID_FAKULTAS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,M.NIM_MHS");
    }

    function load_laporan_bayar_perbiaya_fakultas($tgl_awal, $tgl_akhir, $biaya, $bank)
    {
        $q_bank = $bank == '' ? "" : "AND PEM.ID_BANK ='{$bank}'";
        return $this->db->QueryToArray("
            SELECT F.ID_FAKULTAS,UPPER(F.NM_FAKULTAS) NM_FAKULTAS,PEM.JUMLAH_MHS,PEM.TOTAL_BIAYA FROM 
                (SELECT * FROM FAKULTAS) F
            LEFT JOIN 
                (
                    SELECT PS.ID_FAKULTAS,COUNT(DISTINCT(TM.ID_MHS)) JUMLAH_MHS,SUM(PEM.BESAR_PEMBAYARAN) TOTAL_BIAYA 
                    FROM PEMBAYARAN PEM
                    JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN=PEM.ID_TAGIHAN
                    JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS=TAG.ID_TAGIHAN_MHS
                    JOIN DETAIL_BIAYA DB ON TAG.ID_DETAIL_BIAYA=DB.ID_DETAIL_BIAYA
                    JOIN MAHASISWA M ON M.ID_MHS = TM.ID_MHS
                    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                    WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' AND DB.ID_BIAYA='{$biaya}'
                    {$q_bank} AND PEM.BESAR_PEMBAYARAN!=0 AND PEM.ID_STATUS_PEMBAYARAN=1
                    GROUP BY PS.ID_FAKULTAS
                ) PEM 
            ON PEM.ID_FAKULTAS = F.ID_FAKULTAS
            WHERE F.ID_PERGURUAN_TINGGI='{$this->id_pt}'");
    }

    function load_row_pembayaran_bulanan($data_row, $fakultas, $bulan_ini, $bulan_kemarin)
    {
        $tahun_ini = date('Y');
        $tahun_sebelum = $tahun_ini - 1;
        $result = [];
        $kode_bulan_sekarang = date('m');
        $tahun_sekarang = date('Y');
        // Semester Bayar Bulan
        $this->db->Query("SELECT * FROM SEMESTER_BAYAR_BULAN WHERE KODE_BULAN='{$kode_bulan_sekarang}'");
        $semester_bayar_bulan = $this->db->FetchAssoc();
        // Semester Bayar
        $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER='{$semester_bayar_bulan['NM_SEMESTER']}' AND THN_AKADEMIK_SEMESTER='{$tahun_sekarang}' AND ID_PERGURUAN_TINGGI='{$this->id_pt}'");
        $semester_bayar = $this->db->FetchAssoc();

        if (!empty($fakultas)) {
            foreach ($data_row as $dr) {
                $query_row = "
                SELECT PS.NM_PROGRAM_STUDI,PS.ID_PROGRAM_STUDI,
                J.NM_JENJANG,
                M.THN_ANGKATAN_MHS,
                COUNT(DISTINCT(M.ID_MHS)) JUMLAH_MHS
                FROM MAHASISWA M
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
                JOIN JENJANG J ON J.ID_JENJANG =PS.ID_JENJANG
                JOIN BIAYA_KULIAH_MHS BKM ON BKM.ID_MHS = M.ID_MHS
                JOIN BIAYA_KULIAH BK ON BK.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH
                JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BK.ID_BIAYA_KULIAH 
                AND DB.ID_BIAYA = 185
                WHERE PS.ID_FAKULTAS='{$fakultas}' 
                AND PS.ID_PROGRAM_STUDI='{$dr['ID_PROGRAM_STUDI']}' 
                AND M.STATUS_AKADEMIK_MHS=1
                GROUP BY PS.NM_PROGRAM_STUDI,PS.ID_PROGRAM_STUDI,J.NM_JENJANG,M.THN_ANGKATAN_MHS 
                ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI,PS.ID_PROGRAM_STUDI,M.THN_ANGKATAN_MHS
                ";
                $row = $this->db->QueryToArray($query_row);
                $query_value = "
                SELECT
                    PS.NM_PROGRAM_STUDI,PS.ID_PROGRAM_STUDI,
                    J.NM_JENJANG,
                    M.THN_ANGKATAN_MHS,
                    SUM(COALESCE( (CASE WHEN DB.BESAR_BIAYA < 700000 THEN DB.BESAR_BIAYA ELSE DB.BESAR_BIAYA / 6 END ), 0 )) TARGET,
                    SUM(COALESCE(PEM_SKRG.BESAR_PEMBAYARAN,0)) TERBAYAR_SEKARANG,
                    SUM(COALESCE(PEM_SBLM.BESAR_PEMBAYARAN,0)) TERBAYAR_KEMARIN,
                    SUM(COALESCE(TAG_THN_SBLM.BESAR_BIAYA,0)) TAGIHAN_THN_KEMARIN,
                    SUM(COALESCE(PEM_THN_SBLM.BESAR_PEMBAYARAN,0)) TERBAYAR_THN_KEMARIN
                FROM
                    MAHASISWA M
                    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                    JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                    JOIN BIAYA_KULIAH_MHS BKM ON BKM.ID_MHS = M.ID_MHS
                    JOIN BIAYA_KULIAH BK ON BK.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH
                    JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BK.ID_BIAYA_KULIAH AND DB.ID_BIAYA = 185 
                    LEFT JOIN (
                        SELECT TM.ID_MHS,SUM(PP.BESAR_PEMBAYARAN) BESAR_PEMBAYARAN
                        FROM PEMBAYARAN PEM
                        JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN = PEM.ID_TAGIHAN
                        JOIN PEMBAYARAN_PERBULAN PP ON PP.ID_PEMBAYARAN=PEM.ID_PEMBAYARAN AND PP.ID_TAGIHAN=PEM.ID_TAGIHAN
                        JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA =TAG.ID_DETAIL_BIAYA AND DB.ID_BIAYA =185
                        JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS = TAG.ID_TAGIHAN_MHS
                        JOIN SEMESTER SEM ON SEM.ID_SEMESTER=TM.ID_SEMESTER
                        WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM')='{$tahun_ini}-{$bulan_ini}'
                        AND PP.KODE_BULAN='{$bulan_ini}' AND TM.ID_SEMESTER='{$semester_bayar['ID_SEMESTER']}'
                        AND PEM.ID_STATUS_PEMBAYARAN='1' AND PEM.BESAR_PEMBAYARAN>0
                        GROUP BY TM.ID_MHS,PEM.BESAR_PEMBAYARAN ,PEM.TGL_BAYAR 
                    ) PEM_SKRG ON PEM_SKRG.ID_MHS=M.ID_MHS
                    LEFT JOIN (
                        SELECT TM.ID_MHS,SUM(PP.BESAR_PEMBAYARAN) BESAR_PEMBAYARAN
                        FROM PEMBAYARAN PEM
                        JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN = PEM.ID_TAGIHAN
                        JOIN PEMBAYARAN_PERBULAN PP ON PP.ID_PEMBAYARAN=PEM.ID_PEMBAYARAN AND PP.ID_TAGIHAN=PEM.ID_TAGIHAN
                        JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA =TAG.ID_DETAIL_BIAYA AND DB.ID_BIAYA =185
                        JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS = TAG.ID_TAGIHAN_MHS
                        JOIN SEMESTER SEM ON SEM.ID_SEMESTER=TM.ID_SEMESTER
                        WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM')='{$tahun_ini}-{$bulan_ini}'
                        AND PP.KODE_BULAN<'{$bulan_ini}' AND TM.ID_SEMESTER='{$semester_bayar['ID_SEMESTER']}'
                        AND PEM.ID_STATUS_PEMBAYARAN='1' AND PEM.BESAR_PEMBAYARAN>0
                        GROUP BY TM.ID_MHS,PEM.BESAR_PEMBAYARAN ,PEM.TGL_BAYAR 
                    ) PEM_SBLM ON PEM_SBLM.ID_MHS=M.ID_MHS
                    LEFT JOIN (
                        SELECT TM.ID_MHS,TAG.BESAR_BIAYA
                        FROM TAGIHAN TAG
                        JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS = TAG.ID_TAGIHAN_MHS
                        JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=TAG.ID_DETAIL_BIAYA
                        JOIN SEMESTER SEM ON SEM.ID_SEMESTER=TM.ID_SEMESTER
                        WHERE SEM.THN_AKADEMIK_SEMESTER = '{$tahun_sebelum}' AND SEM.NM_SEMESTER='Ganjil'
                        AND DB.ID_BIAYA=185
                        GROUP BY TM.ID_MHS,TAG.BESAR_BIAYA
                    ) TAG_THN_SBLM ON TAG_THN_SBLM.ID_MHS=M.ID_MHS
                    LEFT JOIN (
                        SELECT TM.ID_MHS,SUM(PP.BESAR_PEMBAYARAN) BESAR_PEMBAYARAN
                        FROM PEMBAYARAN PEM
                        JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN = PEM.ID_TAGIHAN
                        JOIN PEMBAYARAN_PERBULAN PP ON PP.ID_PEMBAYARAN=PEM.ID_PEMBAYARAN AND PP.ID_TAGIHAN=PEM.ID_TAGIHAN
                        JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA =TAG.ID_DETAIL_BIAYA AND DB.ID_BIAYA =185
                        JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS = TAG.ID_TAGIHAN_MHS
                        JOIN SEMESTER SEM ON SEM.ID_SEMESTER=TM.ID_SEMESTER
                        WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM')='{$tahun_ini}-{$bulan_ini}'
                        AND TM.ID_SEMESTER!='{$semester_bayar['ID_SEMESTER']}'
                        AND PEM.ID_STATUS_PEMBAYARAN='1' AND PEM.BESAR_PEMBAYARAN>0
                        GROUP BY TM.ID_MHS,PEM.BESAR_PEMBAYARAN ,PEM.TGL_BAYAR 
                    ) PEM_THN_SBLM ON PEM_THN_SBLM.ID_MHS=M.ID_MHS
                    WHERE
                        PS.ID_FAKULTAS = '{$fakultas}'
                        AND PS.ID_PROGRAM_STUDI='{$dr['ID_PROGRAM_STUDI']}' 
                        AND M.ID_MHS IN (
                        SELECT ID_MHS FROM V_MHS_AKTIF_PEM
                        )
                    GROUP BY
                        PS.NM_PROGRAM_STUDI,PS.ID_PROGRAM_STUDI,
                        J.NM_JENJANG,
                        M.THN_ANGKATAN_MHS 
                    ORDER BY
                        PS.NM_PROGRAM_STUDI,PS.ID_PROGRAM_STUDI,
                        J.NM_JENJANG,
                        M.THN_ANGKATAN_MHS
                ";
                $value = $this->db->QueryToArray($query_value);
                array_push($result, ['row' => $row, 'value' => $value]);
            }

            return $result;
        } else {
            foreach ($data_row as $f) {
                $query_row = "
                    SELECT M.THN_ANGKATAN_MHS,COUNT(DISTINCT(M.ID_MHS)) JUMLAH_MHS
                    FROM MAHASISWA M
                    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
                    JOIN BIAYA_KULIAH_MHS BKM ON BKM.ID_MHS = M.ID_MHS
                    JOIN BIAYA_KULIAH BK ON BK.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH
                    JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BK.ID_BIAYA_KULIAH 
                    AND DB.ID_BIAYA = 185
                    WHERE PS.ID_FAKULTAS='{$f['ID_FAKULTAS']}' AND M.STATUS_AKADEMIK_MHS=1
                    GROUP BY M.THN_ANGKATAN_MHS 
                    ORDER BY M.THN_ANGKATAN_MHS
                ";
                $row = $this->db->QueryToArray($query_row);
                $query_value = "
                SELECT
                    M.THN_ANGKATAN_MHS,
                    SUM(COALESCE( (CASE WHEN DB.BESAR_BIAYA < 700000 THEN DB.BESAR_BIAYA ELSE DB.BESAR_BIAYA / 6 END ), 0 )) TARGET,
                    SUM(COALESCE(PEM_SKRG.BESAR_PEMBAYARAN,0)) TERBAYAR_SEKARANG,
                    SUM(COALESCE(PEM_SBLM.BESAR_PEMBAYARAN,0)) TERBAYAR_KEMARIN,
                    SUM(COALESCE(TAG_THN_SBLM.BESAR_BIAYA,0)) TAGIHAN_THN_KEMARIN,
                    SUM(COALESCE(PEM_THN_SBLM.BESAR_PEMBAYARAN,0)) TERBAYAR_THN_KEMARIN
                FROM
                    MAHASISWA M
                    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                    JOIN BIAYA_KULIAH_MHS BKM ON BKM.ID_MHS = M.ID_MHS
                    JOIN BIAYA_KULIAH BK ON BK.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH
                    JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BK.ID_BIAYA_KULIAH AND DB.ID_BIAYA = 185 
                    LEFT JOIN (
                        SELECT TM.ID_MHS,PEM.BESAR_PEMBAYARAN
                        FROM PEMBAYARAN PEM
                        JOIN PEMBAYARAN_PERBULAN PP ON PP.ID_PEMBAYARAN=PEM.ID_PEMBAYARAN
                        JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN = PEM.ID_TAGIHAN
                        JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS = TAG.ID_TAGIHAN_MHS
                        JOIN SEMESTER SEM ON SEM.ID_SEMESTER=TM.ID_SEMESTER
                        WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM')='{$tahun_ini}-{$bulan_ini}'
                        AND PEM.ID_STATUS_PEMBAYARAN='1' AND PEM.BESAR_PEMBAYARAN>0
                    ) PEM_SKRG ON PEM_SKRG.ID_MHS=M.ID_MHS
                    LEFT JOIN (
                        SELECT TM.ID_MHS,PEM.BESAR_PEMBAYARAN
                        FROM PEMBAYARAN PEM
                        JOIN PEMBAYARAN_PERBULAN PP ON PP.ID_PEMBAYARAN=PEM.ID_PEMBAYARAN
                        JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN = PEM.ID_TAGIHAN
                        JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS = TAG.ID_TAGIHAN_MHS
                        JOIN SEMESTER SEM ON SEM.ID_SEMESTER=TM.ID_SEMESTER
                        WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM') = '{$tahun_ini}-{$bulan_kemarin}'
                        AND PEM.ID_STATUS_PEMBAYARAN='1' AND PEM.BESAR_PEMBAYARAN>0
                    ) PEM_SBLM ON PEM_SBLM.ID_MHS=M.ID_MHS
                    LEFT JOIN (
                        SELECT TM.ID_MHS,TAG.BESAR_BIAYA
                        FROM TAGIHAN TAG
                        JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS = TAG.ID_TAGIHAN_MHS
                        JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=TAG.ID_DETAIL_BIAYA
                        JOIN SEMESTER SEM ON SEM.ID_SEMESTER=TM.ID_SEMESTER
                        WHERE SEM.THN_AKADEMIK_SEMESTER = '{$tahun_sebelum}' AND SEM.NM_SEMESTER='Ganjil'
                        AND DB.ID_BIAYA=185
                        GROUP BY TM.ID_MHS,TAG.BESAR_BIAYA
                    ) TAG_THN_SBLM ON TAG_THN_SBLM.ID_MHS=M.ID_MHS
                    LEFT JOIN (
                        SELECT TM.ID_MHS,SUM(PEM.BESAR_PEMBAYARAN) BESAR_PEMBAYARAN
                        FROM PEMBAYARAN PEM
                        JOIN PEMBAYARAN_PERBULAN PP ON PP.ID_PEMBAYARAN=PEM.ID_PEMBAYARAN
                        JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN = PEM.ID_TAGIHAN
                        JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS = TAG.ID_TAGIHAN_MHS
                        JOIN SEMESTER SEM ON SEM.ID_SEMESTER=TM.ID_SEMESTER
                        WHERE SEM.THN_AKADEMIK_SEMESTER = '{$tahun_sebelum}'
                        AND PEM.ID_STATUS_PEMBAYARAN='1' AND PEM.BESAR_PEMBAYARAN>0
                        GROUP BY TM.ID_MHS
                    ) PEM_THN_SBLM ON PEM_THN_SBLM.ID_MHS=M.ID_MHS
                    WHERE
                        PS.ID_FAKULTAS = '{$f['ID_FAKULTAS']}'
                        AND M.ID_MHS IN (
                        SELECT ID_MHS FROM V_MHS_AKTIF_PEM
                        )
                    GROUP BY
                        M.THN_ANGKATAN_MHS 
                    ORDER BY
                        M.THN_ANGKATAN_MHS
                ";
                $value = $this->db->QueryToArray($query_value);
                array_push($result, ['row' => $row, 'value' => $value]);
            }
            return $result;
        }
    }

    // TAMBAHAN UNTUK PERHITUNGAN SKOR CALON MAHASISWA 
    function GetRangeSkor($id)
    {
        return $this->db->QuerySingle("SELECT NILAI_SKOR FROM RANGE_JUMLAH WHERE ID_RANGE_JUMLAH='{$id}'");
    }

    function GetSkorPekerjaan($id)
    {
        return $this->db->QuerySingle("SELECT NILAI_SKOR FROM PEKERJAAN WHERE ID_PEKERJAAN='{$id}'");
    }

    function GetSkorKendaraan($jumlah)
    {
        $nilai_skor = '';
        if ($jumlah == 1) {
            $nilai_skor = 1;
        } else if ($jumlah == 2) {
            $nilai_skor = 2;
        } else if ($jumlah >= 3) {
            $nilai_skor = 3;
        }
        return $nilai_skor;
    }
}
