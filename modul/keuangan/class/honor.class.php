<?php

class honor {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    //fungsi pajak honor

    function load_pajak_honor() {
        return $this->db->QueryToArray("
            SELECT PH.*,G.NM_GOLONGAN,G.NM_PANGKAT FROM PAJAK_HONOR PH
            JOIN GOLONGAN G ON PH.ID_GOLONGAN=G.ID_GOLONGAN
            ORDER BY G.NM_GOLONGAN,G.NM_PANGKAT");
    }

    function get_pajak_honor($id) {
        $this->db->Query("
            SELECT PH.*,G.NM_GOLONGAN,G.NM_PANGKAT FROM PAJAK_HONOR PH
            JOIN GOLONGAN G ON PH.ID_GOLONGAN=G.ID_GOLONGAN
            WHERE PH.ID_PAJAK_HONOR='{$id}'
            ORDER BY G.NM_GOLONGAN,G.NM_PANGKAT");
        return $this->db->FetchAssoc();
    }

    function add_pajak_honor($pajak, $npwp, $golongan) {
        $this->db->Query("
            INSERT INTO PAJAK_HONOR
                (BESAR_PAJAK,BESAR_PAJAK_NPWP,ID_GOLONGAN)
            VALUES
                ('{$pajak}','{$npwp}','{$golongan}')");
    }

    function edit_pajak_honor($id, $pajak, $npwp, $golongan) {
        $this->db->Query("
            UPDATE PAJAK_HONOR
            SET
                BESAR_PAJAK='{$pajak}',
                BESAR_PAJAK_NPWP='{$npwp}',
                ID_GOLONGAN='{$golongan}'
            WHERE ID_PAJAK_HONOR='{$id}'");
    }

    //fungsi Jenis Honor
    function load_jenis_honor() {
        return $this->db->QueryToArray("SELECT * FROM JENIS_HONOR ORDER BY TIPE_HONOR,NM_JENIS_HONOR");
    }

    function get_jenis_honor($id) {
        $this->db->Query("
            SELECT * FROM JENIS_HONOR
            WHERE ID_JENIS_HONOR='{$id}'");
        return $this->db->FetchAssoc();
    }

    function add_jenis_honor($nama, $tipe) {
        $this->db->Query("
            INSERT INTO JENIS_HONOR
            (NM_JENIS_HONOR,TIPE_HONOR)
                VALUES
            ('{$nama}','{$tipe}')");
    }

    function edit_jenis_honor($id, $nama, $tipe) {
        $this->db->Query("
            UPDATE JENIS_HONOR
            SET
                NM_JENIS_HONOR='{$nama}',
                TIPE_HONOR='{$tipe}'
            WHERE ID_JENIS_HONOR='{$id}'");
    }

}

?>
