<?php

$id_pt = $id_pt_user;

class history
{

    public $db;
    public $id_pt;

    function __construct($db)
    {
        $this->db = $db;
        $this->id_pt = $GLOBALS['id_pt'];
    }

    function get_data_mhs($nim, $id_pt)
    {
        /*if (is_numeric($nim) && strlen($nim) < 9) {
            $query = "M.ID_MHS='{$nim}'";
        } else {*/
        $query = "M.NIM_MHS='{$nim}'";
        /*}*/
        $this->db->Query("
            SELECT M.ID_MHS,M.NIM_MHS,M.NIM_LAMA,M.NIM_BANK,M.THN_ANGKATAN_MHS,M.STATUS_CEKAL,P.NM_PENGGUNA,PR.NM_PROGRAM_STUDI
                ,J.NM_JENJANG,JAL.NM_JALUR,KB.NM_KELOMPOK_BIAYA,F.NM_FAKULTAS,SP.NM_STATUS_PENGGUNA,CMB.NO_UJIAN,AD.TGL_KELUAR,AD.NO_IJASAH,
                M.ALAMAT_MHS,M.MOBILE_MHS,M.NM_AYAH_MHS,M.NM_IBU_MHS,P.TGL_LAHIR_PENGGUNA,P.EMAIL_ALTERNATE,
                NK.NAMA_KELAS
            FROM MAHASISWA M
            JOIN PENGGUNA P ON M.ID_PENGGUNA = P.ID_PENGGUNA
            JOIN PROGRAM_STUDI PR ON M.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG = PR.ID_JENJANG
            LEFT JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
            LEFT JOIN JALUR_MAHASISWA JM ON M.ID_MHS = JM.ID_MHS AND JM.IS_JALUR_AKTIF=1
            LEFT JOIN JALUR JAL ON JAL.ID_JALUR = JM.ID_JALUR
            LEFT JOIN SEMESTER S ON S.ID_SEMESTER = JM.ID_SEMESTER
            LEFT JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = M.ID_KELOMPOK_BIAYA
            LEFT JOIN CALON_MAHASISWA_BARU CMB ON CMB.NIM_MHS = M.NIM_MHS
            LEFT JOIN ADMISI AD ON AD.ID_MHS=M.ID_MHS AND AD.STATUS_APV=1
            LEFT JOIN MAHASISWA_KELAS MK ON MK.ID_MHS = M.ID_MHS
            LEFT JOIN NAMA_KELAS NK ON NK.ID_NAMA_KELAS = MK.ID_NAMA_KELAS
            JOIN FAKULTAS F ON F.ID_FAKULTAS = PR.ID_FAKULTAS
            WHERE {$query} AND F.ID_PERGURUAN_TINGGI = {$id_pt}");
        return $this->db->FetchAssoc();
    }

    function get_data_cmhs($no_ujian)
    {
        if (is_numeric($no_ujian)) {
            $query = "CMB.NO_UJIAN ='{$no_ujian}' OR CMB.ID_C_MHS='{$no_ujian}'";
        } else {
            $query = "CMB.NO_UJIAN ='{$no_ujian}'";
        }
        $this->db->Query("
            SELECT CMB.NM_C_MHS,CMB.NO_UJIAN,CMB.NIM_MHS,PS.NM_PROGRAM_STUDI,F.NM_FAKULTAS,J.NM_JENJANG,JAL.NM_JALUR,KB.NM_KELOMPOK_BIAYA 
            FROM CALON_MAHASISWA_BARU CMB
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CMB.ID_PROGRAM_STUDI
            JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
            JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            JOIN JALUR JAL ON JAL.ID_JALUR = CMB.ID_JALUR
            LEFT JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = CMB.ID_KELOMPOK_BIAYA
            WHERE {$query}
            ");
        return $this->db->FetchAssoc();
    }

    function get_history_bayar_mhs($nim, $semester, $no_transaksi, $tgl_bayar, $keterangan, $is_tagih)
    {
        if (is_numeric($nim) && strlen($nim) < 9) {
            $query = "M.ID_MHS='{$nim}'";
        } else {
            $query = "M.NIM_MHS='{$nim}'";
        }
        $q_no_transaksi = $no_transaksi != '' ? "AND PEM.NO_TRANSAKSI='{$no_transaksi}'" : "AND PEM.NO_TRANSAKSI IS NULL";
        $q_tgl_bayar = $tgl_bayar != '' ? "AND TO_DATE(PEM.TGL_BAYAR)='{$tgl_bayar}'" : "AND PEM.TGL_BAYAR IS NULL";
        $q_keterangan = $keterangan != '' ? "AND CONVERT(PEM.KETERANGAN, 'US7ASCII', 'WE8ISO8859P1') ='{$keterangan}'" : "AND PEM.KETERANGAN IS NULL";
        return $this->db->QueryToArray("
        SELECT PEM.ID_PEMBAYARAN,B.NM_BIAYA,PEM.BESAR_BIAYA,PEM.DENDA_BIAYA,PEM.TGL_BAYAR,BNK.NM_BANK,BV.NAMA_BANK_VIA,
                PEM.NO_TRANSAKSI,CONVERT(PEM.KETERANGAN, 'US7ASCII', 'WE8ISO8859P1') KETERANGAN,PEM.IS_TAGIH,
                PEM.ID_STATUS_PEMBAYARAN,PEM.ID_BANK,PEM.ID_BANK_VIA,SP.NAMA_STATUS,SB.NM_SEMESTER,SB.TAHUN_AJARAN
        FROM PEMBAYARAN PEM
        JOIN MAHASISWA M ON PEM.ID_MHS=M.ID_MHS
        LEFT JOIN STATUS_PEMBAYARAN SP ON SP.ID_STATUS_PEMBAYARAN = PEM.ID_STATUS_PEMBAYARAN
        JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
        JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
        LEFT JOIN SEMESTER SB ON PEM.ID_SEMESTER_BAYAR=SB.ID_SEMESTER
        LEFT JOIN BANK BNK ON BNK.ID_BANK=PEM.ID_BANK
        LEFT JOIN BANK_VIA BV ON BV.ID_BANK_VIA = PEM.ID_BANK_VIA
        WHERE {$query} AND PEM.ID_SEMESTER='{$semester}' {$q_tgl_bayar} {$q_no_transaksi} {$q_keterangan} AND PEM.IS_TAGIH='{$is_tagih}'
        ORDER BY PEM.ID_PEMBAYARAN");
    }

    function get_history_bayar_cmhs($id_cmhs, $semester, $no_transaksi, $tgl_bayar, $is_tagih = 'Y')
    {
        if ($no_transaksi != '' && $tgl_bayar != '') {
            $query = "AND (PEM.NO_TRANSAKSI='{$no_transaksi}' OR PEM.TGL_BAYAR ='{$tgl_bayar}')";
        } else {
            if ($no_transaksi != '') {
                $query = "AND PEM.NO_TRANSAKSI='{$no_transaksi}' AND PEM.TGL_BAYAR IS NULL";
            } else if ($tgl_bayar != '') {
                $query = "AND PEM.TGL_BAYAR ='{$tgl_bayar}' AND PEM.NO_TRANSAKSI IS NULL";
            } else {
                $query = "AND PEM.NO_TRANSAKSI IS NULL AND PEM.TGL_BAYAR IS NULL";
            }
        }
        return $this->db->QueryToArray("
        SELECT B.NM_BIAYA,PEM.BESAR_BIAYA,PEM.DENDA_BIAYA,PEM.TGL_BAYAR,BNK.NM_BANK,BV.NAMA_BANK_VIA,PEM.NO_TRANSAKSI,PEM.KETERANGAN 
        FROM PEMBAYARAN_CMHS PEM
        JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
        JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
        LEFT JOIN BANK BNK ON BNK.ID_BANK=PEM.ID_BANK
        LEFT JOIN BANK_VIA BV ON BV.ID_BANK_VIA = PEM.ID_BANK_VIA
        WHERE PEM.ID_C_MHS='{$id_cmhs}' AND PEM.ID_SEMESTER='{$semester}' AND PEM.IS_TAGIH='{$is_tagih}' {$query}
        ORDER BY PEM.ID_PEMBAYARAN_CMHS");
    }

    function load_pembayaran_by_no_transaksi($no_transaksi)
    {
        $result = [];
        $data_pembayaran = $this->db->QueryToArray("
        SELECT
            PEM.NO_KUITANSI,
            ( S.NM_SEMESTER || ' ' || S.THN_AKADEMIK_SEMESTER ) SEMESTER 
        FROM
            PEMBAYARAN PEM
            JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN = PEM.ID_TAGIHAN
            JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS = TAG.ID_TAGIHAN_MHS
            JOIN SEMESTER S ON S.ID_SEMESTER = TM.ID_SEMESTER
        WHERE
            PEM.NO_TRANSAKSI = '{$no_transaksi}' 
        GROUP BY
            PEM.NO_KUITANSI,S.NM_SEMESTER,S.THN_AKADEMIK_SEMESTER
        ORDER BY PEM.NO_KUITANSI ASC
        ");
        foreach ($data_pembayaran as $data) {
            array_push(
                $result,
                array_merge(
                    $data,
                    array('DATA_DETAIL_PEMBAYARAN' => $this->load_pembayaran_by_no_kuitansi($data['NO_KUITANSI']))
                )
            );
        }
        return $result;
    }

    function load_pembayaran_by_no_kuitansi($no_kuitansi)
    {
        return $this->db->QueryToArray("
        SELECT
            PEM.*,
            BNK.NM_BANK,
            BV.NAMA_BANK_VIA,
            B.NM_BIAYA,
            ( S.NM_SEMESTER || ' ' || S.THN_AKADEMIK_SEMESTER ) SEMESTER 
        FROM
            PEMBAYARAN PEM
            JOIN BANK BNK ON BNK.ID_BANK = PEM.ID_BANK
            JOIN BANK_VIA BV ON BV.ID_BANK_VIA = PEM.ID_BANK_VIA
            JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN = PEM.ID_TAGIHAN
            JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS = TAG.ID_TAGIHAN_MHS
            JOIN SEMESTER S ON S.ID_SEMESTER = TM.ID_SEMESTER
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = TAG.ID_DETAIL_BIAYA
            JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA 
        WHERE
            PEM.NO_KUITANSI = '{$no_kuitansi}' 
        ORDER BY
            B.NM_BIAYA
        ");
    }

    function load_pembayaran_all_biaya_by_no_transaksi($no_transaksi)
    {
        return $this->db->QueryToArray("
        SELECT
            B.NM_BIAYA,PEM.* 
        FROM
            BIAYA B
            LEFT JOIN (
        SELECT
            B.ID_BIAYA,
            PEM.BESAR_PEMBAYARAN,
            PEM.TGL_BAYAR,
            BNK.NM_BANK,
            BV.NAMA_BANK_VIA,
            SEM.NM_SEMESTER,
            SEM.TAHUN_AJARAN
        FROM
            PEMBAYARAN PEM
            JOIN BANK BNK ON BNK.ID_BANK = PEM.ID_BANK
            JOIN BANK_VIA BV ON BV.ID_BANK_VIA = PEM.ID_BANK_VIA
            JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN = PEM.ID_TAGIHAN
            JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS=TAG.ID_TAGIHAN_MHS
            JOIN SEMESTER SEM ON SEM.ID_SEMESTER=TM.ID_SEMESTER
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = TAG.ID_DETAIL_BIAYA
            JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA 
        WHERE
            PEM.NO_TRANSAKSI = '{$no_transaksi}' 
        ORDER BY
            B.NM_BIAYA 
            ) PEM ON PEM.ID_BIAYA = B.ID_BIAYA
            WHERE B.ID_PERGURUAN_TINGGI='" . getenv('ID_PT') . "'
            ORDER BY B.NM_BIAYA
        ");
    }

    function load_pembayaran_all_biaya_by_no_kuitansi($no_kuitansi)
    {
        return $this->db->QueryToArray("
        SELECT
            B.NM_BIAYA,PEM.* 
        FROM
            BIAYA B
            LEFT JOIN (
        SELECT
            B.ID_BIAYA,
            PEM.BESAR_PEMBAYARAN,
            PEM.TGL_BAYAR,
            BNK.NM_BANK,
            BV.NAMA_BANK_VIA,
            SEM.NM_SEMESTER,
            SEM.TAHUN_AJARAN
        FROM
            PEMBAYARAN PEM
            JOIN BANK BNK ON BNK.ID_BANK = PEM.ID_BANK
            JOIN BANK_VIA BV ON BV.ID_BANK_VIA = PEM.ID_BANK_VIA
            JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN = PEM.ID_TAGIHAN
            JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS=TAG.ID_TAGIHAN_MHS
            JOIN SEMESTER SEM ON SEM.ID_SEMESTER=TM.ID_SEMESTER
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = TAG.ID_DETAIL_BIAYA
            JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA 
        WHERE
            PEM.NO_KUITANSI = '{$no_kuitansi}' 
        ORDER BY
            B.NM_BIAYA 
            ) PEM ON PEM.ID_BIAYA = B.ID_BIAYA
            WHERE B.ID_PERGURUAN_TINGGI='" . getenv('ID_PT') . "'
            ORDER BY B.NM_BIAYA
        ");
    }

    function load_history_bayar_cmhs($cari)
    {
        $data_history_bayar = array();
        $query = "(CMB.NO_UJIAN ='{$cari}' OR CMB.NIM_MHS='{$cari}')";
        foreach ($this->db->QueryToArray("
            SELECT PEM.IS_TAGIH,SUM(PEM.BESAR_BIAYA) JUMLAH_PEMBAYARAN,PEM.ID_C_MHS,S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN,S.THN_AKADEMIK_SEMESTER,B.NM_BANK,BV.NAMA_BANK_VIA,PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.KETERANGAN
            FROM PEMBAYARAN_CMHS PEM
            JOIN CALON_MAHASISWA_BARU CMB ON PEM.ID_C_MHS = CMB.ID_C_MHS
            JOIN SEMESTER S ON S.ID_SEMESTER = PEM.ID_SEMESTER
            LEFT JOIN BANK B ON B.ID_BANK=PEM.ID_BANK
            LEFT JOIN BANK_VIA BV ON BV.ID_BANK_VIA=PEM.ID_BANK_VIA
            WHERE {$query}
            GROUP BY PEM.IS_TAGIH,PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,B.NM_BANK,BV.NAMA_BANK_VIA,PEM.KETERANGAN,S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN,S.THN_AKADEMIK_SEMESTER,PEM.ID_C_MHS
            ORDER BY S.TAHUN_AJARAN DESC,S.NM_SEMESTER DESC") as $data) {
            array_push($data_history_bayar, array(
                'ID_C_MHS' => $data['ID_C_MHS'],
                'ID_SEMESTER' => $data['ID_SEMESTER'],
                'NM_SEMESTER' => $data['NM_SEMESTER'],
                'TAHUN_AJARAN' => $data['TAHUN_AJARAN'],
                'JUMLAH_PEMBAYARAN' => $data['JUMLAH_PEMBAYARAN'],
                'NO_TRANSAKSI' => $data['NO_TRANSAKSI'],
                'TGL_BAYAR' => $data['TGL_BAYAR'],
                'NM_BANK' => $data['NM_BANK'],
                'NAMA_BANK_VIA' => $data['NAMA_BANK_VIA'],
                'KETERANGAN' => $data['KETERANGAN'],
                'IS_TAGIH' => $data['IS_TAGIH'],
                'DATA_PEMBAYARAN' => $this->get_history_bayar_cmhs($data['ID_C_MHS'], $data['ID_SEMESTER'], $data['NO_TRANSAKSI'], $data['TGL_BAYAR'], $data['IS_TAGIH'])
            ));
        }
        return $data_history_bayar;
    }

    function load_riwayat_transaksi_pembayaran($id_tagihan_mhs)
    {
        $results = array();
        $query = "
        SELECT T.ID_TAGIHAN_MHS,PEM.TGL_BAYAR,PEM.NO_TRANSAKSI,SP.NAMA_STATUS,SUM(PEM.BESAR_PEMBAYARAN) TOTAL_BAYAR,B.NM_BANK,BV.NAMA_BANK_VIA,PEM.KETERANGAN,PEM.PERIODE_BAYAR,PEM.NO_KUITANSI
        FROM
            PEMBAYARAN PEM
            JOIN TAGIHAN T ON T.ID_TAGIHAN = PEM.ID_TAGIHAN
            LEFT JOIN STATUS_PEMBAYARAN SP ON SP.ID_STATUS_PEMBAYARAN=PEM.ID_STATUS_PEMBAYARAN
            LEFT JOIN BANK B ON B.ID_BANK=PEM.ID_BANK
            LEFT JOIN BANK_VIA BV ON BV.ID_BANK_VIA=PEM.ID_BANK_VIA
            WHERE T.ID_TAGIHAN_MHS = '{$id_tagihan_mhs}' 
        GROUP BY T.ID_TAGIHAN_MHS,PEM.TGL_BAYAR,PEM.NO_TRANSAKSI,SP.NAMA_STATUS,B.NM_BANK,BV.NAMA_BANK_VIA,PEM.KETERANGAN,PEM.PERIODE_BAYAR,PEM.NO_KUITANSI
        ORDER BY PEM.TGL_BAYAR ASC
        ";
        $arr_data = $this->db->QueryToArray($query);
        foreach ($arr_data as $d) {
            array_push(
                $results,
                array_merge($d, [
                    'DATA_BIAYA_PEMBAYARAN' => $this->load_detail_riwayat_transaksi_pembayaran($d['ID_TAGIHAN_MHS'], $d['NO_TRANSAKSI'], $d['NO_KUITANSI'])
                ])
            );
        }
        return $results;
    }

    function load_detail_riwayat_transaksi_pembayaran($id_tagihan_mhs, $no_transaksi, $no_kuitansi)
    {
        $results = array();
        $query = "
        SELECT PEM.ID_PEMBAYARAN,T.ID_TAGIHAN,B.NM_BIAYA,PEM.BESAR_PEMBAYARAN
        FROM PEMBAYARAN PEM
        JOIN TAGIHAN T ON T.ID_TAGIHAN = PEM.ID_TAGIHAN
        JOIN DETAIL_BIAYA DB ON T.ID_DETAIL_BIAYA =DB.ID_DETAIL_BIAYA
        JOIN BIAYA B ON B.ID_BIAYA=DB.ID_BIAYA
        WHERE T.ID_TAGIHAN_MHS = '{$id_tagihan_mhs}' AND PEM.NO_TRANSAKSI='{$no_transaksi}'
        AND PEM.NO_KUITANSI='{$no_kuitansi}'
        ORDER BY B.NM_BIAYA
        ";
        $arr_data = $this->db->QueryToArray($query);
        foreach ($arr_data as $d) {
            array_push(
                $results,
                array_merge($d, [
                    'DATA_PEMBAYARAN_PERBULAN' => $this->get_bulan_semester_pembayaran($d['ID_PEMBAYARAN'], $d['ID_TAGIHAN'])
                ])
            );
        }
        return $results;
    }

    function load_riwayat_pembayaran_biaya($id_tagihan_mhs)
    {
        $result = [];
        $query = "
        SELECT
            T.ID_TAGIHAN_MHS,
            T.ID_TAGIHAN,
            T.BESAR_BIAYA,
            T.IS_LUNAS,
            B.NM_BIAYA,
            S.TAHUN_AJARAN,
            S.NM_SEMESTER,
            DB.ID_JENIS_DETAIL_BIAYA,
            AVG(PEM.ID_STATUS_PEMBAYARAN) STATUS_PEMBAYARAN,
            COALESCE(SUM(PEM.BESAR_PEMBAYARAN ),0) TOTAL_TERBAYAR,
            SUM(CASE WHEN PEM.BESAR_PEMBAYARAN IS NOT NULL THEN 1 ELSE 0 END) SUDAH_ADA_PEMBAYARAN
        FROM
            TAGIHAN T
            LEFT JOIN PEMBAYARAN PEM ON PEM.ID_TAGIHAN = T.ID_TAGIHAN
            JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS = T.ID_TAGIHAN_MHS
            JOIN SEMESTER S ON S.ID_SEMESTER = TM.ID_SEMESTER
            JOIN DETAIL_BIAYA DB ON T.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
            JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
            LEFT JOIN STATUS_PEMBAYARAN SP ON SP.ID_STATUS_PEMBAYARAN = PEM.ID_STATUS_PEMBAYARAN 
        WHERE
            T.ID_TAGIHAN_MHS = '{$id_tagihan_mhs}' 
        GROUP BY
            T.ID_TAGIHAN_MHS,
            T.ID_TAGIHAN,
            T.IS_LUNAS,
            T.BESAR_BIAYA,
            B.NM_BIAYA,
            S.TAHUN_AJARAN,
            DB.ID_JENIS_DETAIL_BIAYA,
            S.NM_SEMESTER 
        ORDER BY
            B.NM_BIAYA
        ";
        $pembayaran_biaya = $this->db->QueryToArray($query);
        foreach ($pembayaran_biaya as $pb) {
            if ($pb['ID_JENIS_DETAIL_BIAYA'] == 4) {
                array_push($result, array_merge(
                    $pb,
                    [
                        'DATA_BAYAR_BULAN' => $this->get_bulan_semester_bayar($pb['NM_SEMESTER'], $pb['ID_TAGIHAN'])
                    ]
                ));
            } else {
                array_push($result, array_merge(
                    $pb,
                    [
                        'DATA_BAYAR_BULAN' => []
                    ]
                ));
            }
        }
        return $result;
    }

    function get_bulan_semester_bayar($nm_semester, $id_tagihan)
    {
        return $this->db->QueryToArray("
            SELECT SBB.ID_SEMESTER_BAYAR_BULAN,SBB.KODE_BULAN,SBB.NM_BULAN,COALESCE(SUM(PP.BESAR_PEMBAYARAN),0) PERBULAN_TERBAYAR
            FROM SEMESTER_BAYAR_BULAN SBB 
            LEFT JOIN PEMBAYARAN_PERBULAN PP ON PP.KODE_BULAN=SBB.KODE_BULAN AND PP.ID_TAGIHAN='{$id_tagihan}'
            WHERE SBB.KODE_BULAN NOT IN (
                SELECT PP.KODE_BULAN
                FROM PEMBAYARAN_PERBULAN PP
                JOIN TAGIHAN T ON T.ID_TAGIHAN=PP.ID_TAGIHAN
                WHERE PP.ID_TAGIHAN='{$id_tagihan}'
                GROUP BY PP.KODE_BULAN,T.ID_TAGIHAN,T.BESAR_BIAYA
                HAVING (T.BESAR_BIAYA/6)=SUM(PP.BESAR_PEMBAYARAN)
            )
            AND SBB.NM_SEMESTER='{$nm_semester}' 
            GROUP BY SBB.ID_SEMESTER_BAYAR_BULAN,SBB.KODE_BULAN,SBB.NM_BULAN
            ORDER BY SBB.ID_SEMESTER_BAYAR_BULAN");
    }

    function get_bulan_semester_pembayaran($id_pembayaran, $id_tagihan)
    {
        return $this->db->QueryToArray("
            SELECT * FROM PEMBAYARAN_PERBULAN
            WHERE ID_TAGIHAN='{$id_tagihan}' AND ID_PEMBAYARAN='{$id_pembayaran}'
            ");
    }

    function get_pembayaran_va($no_transaksi)
    {
        $query = "
        SELECT PVB.*,BNK.NM_BANK,
        COALESCE((SELECT SUM(BESAR_PEMBAYARAN) FROM PEMBAYARAN WHERE NO_TRANSAKSI =PVB.PAYMENT_NTB ),0) PEMBAYARAN_DIPROSES
        FROM PEMBAYARAN_VA_BANK PVB
        JOIN BANK BNK ON BNK.ID_BANK=PVB.ID_BANK
        WHERE PVB.PAYMENT_NTB='{$no_transaksi}'
        ORDER BY PVB.PAYMENT_TIME DESC
        ";
        $this->db->Query($query);
        return $this->db->FetchAssoc();
    }

    function load_riwayat_tagihan_biaya($id_tagihan_mhs)
    {
        $query = "
        SELECT
            T.ID_TAGIHAN_MHS,
            T.ID_TAGIHAN,
            T.BESAR_BIAYA,
            T.IS_LUNAS,
            B.NM_BIAYA,
            B.IS_CETAK,
            S.TAHUN_AJARAN,
            S.NM_SEMESTER,
            AVG(PEM.ID_STATUS_PEMBAYARAN) STATUS_PEMBAYARAN,
            SUM(PEM.BESAR_PEMBAYARAN ) TOTAL_TERBAYAR 
        FROM
            TAGIHAN T
            JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS = T.ID_TAGIHAN_MHS
            LEFT JOIN PEMBAYARAN PEM ON PEM.ID_TAGIHAN = T.ID_TAGIHAN
            JOIN SEMESTER S ON S.ID_SEMESTER = TM.ID_SEMESTER
            JOIN DETAIL_BIAYA DB ON T.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
            JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
            LEFT JOIN STATUS_PEMBAYARAN SP ON SP.ID_STATUS_PEMBAYARAN = PEM.ID_STATUS_PEMBAYARAN 
        WHERE
            T.ID_TAGIHAN_MHS = '{$id_tagihan_mhs}' 
        GROUP BY
            T.ID_TAGIHAN_MHS,
            T.ID_TAGIHAN,
            T.IS_LUNAS,
            T.BESAR_BIAYA,
            B.NM_BIAYA,
            S.TAHUN_AJARAN,
            S.NM_SEMESTER 
        ORDER BY
            B.NM_BIAYA
        ";
        return $this->db->QueryToArray($query);
    }

    function load_riwayat_pembayaran_biaya_cetak($id_tagihan_mhs)
    {
        $query = "
        SELECT
            T.ID_TAGIHAN_MHS,
            T.ID_TAGIHAN,
            T.BESAR_BIAYA,
            T.IS_LUNAS,
            B.NM_BIAYA,
            S.TAHUN_AJARAN,
            S.NM_SEMESTER,
            AVG(PEM.ID_STATUS_PEMBAYARAN) STATUS_PEMBAYARAN,
            SUM(PEM.BESAR_PEMBAYARAN ) TOTAL_TERBAYAR 
        FROM
            TAGIHAN T
            JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS = T.ID_TAGIHAN_MHS
            JOIN SEMESTER S ON S.ID_SEMESTER = TM.ID_SEMESTER
            JOIN DETAIL_BIAYA DB ON T.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
            JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
            LEFT JOIN PEMBAYARAN PEM ON PEM.ID_TAGIHAN = T.ID_TAGIHAN
            LEFT JOIN STATUS_PEMBAYARAN SP ON SP.ID_STATUS_PEMBAYARAN = PEM.ID_STATUS_PEMBAYARAN 
        WHERE
            T.ID_TAGIHAN_MHS = '{$id_tagihan_mhs}' 
            AND B.IS_CETAK = 1
            AND T.IS_LUNAS = 1
        GROUP BY
            T.ID_TAGIHAN_MHS,
            T.ID_TAGIHAN,
            T.IS_LUNAS,
            T.BESAR_BIAYA,
            B.NM_BIAYA,
            S.TAHUN_AJARAN,
            S.NM_SEMESTER 
        ORDER BY
            B.NM_BIAYA
        ";
        return $this->db->QueryToArray($query);
    }


    function load_riwayat_pembayaran_va_bank($trx_id)
    {
        $result = [];
        $query = "
        SELECT PVB.*,BNK.NM_BANK,
        COALESCE((SELECT SUM(BESAR_PEMBAYARAN) FROM PEMBAYARAN WHERE NO_TRANSAKSI =PVB.PAYMENT_NTB ),0) PEMBAYARAN_DIPROSES
        FROM PEMBAYARAN_VA_BANK PVB
        JOIN BANK BNK ON BNK.ID_BANK=PVB.ID_BANK
        WHERE PVB.TRX_ID='{$trx_id}'
        ORDER BY PVB.PAYMENT_TIME ASC
        ";
        $arr_data = $this->db->QueryToArray($query);
        foreach ($arr_data as $data) {
            array_push(
                $result,
                array_merge(
                    $data,
                    array('DATA_PEMBAYARAN' => $this->load_pembayaran_by_no_transaksi($data['PAYMENT_NTB']))
                )
            );
        }
        return $result;
    }

    function load_riwayat_pembayaran_mhs($nim)
    {
        $data_history_bayar = array();
        $query = "
        SELECT
            TM.ID_TAGIHAN_MHS,
            TM.ID_MHS,
            M.NIM_MHS,
            TM.NO_VA,
            TM.TRX_ID,
            TM.AWAL_PERIODE,
            TM.AKHIR_PERIODE,
            TM.KETERANGAN,
            TM.ID_SEMESTER,
            S.NM_SEMESTER,
            S.TAHUN_AJARAN,
            TM.TOTAL_BESAR_BIAYA,
            COALESCE(SUM(CASE WHEN PEM.ID_STATUS_PEMBAYARAN=1 THEN PEM.BESAR_PEMBAYARAN ELSE 0 END),0) TOTAL_TERBAYAR,
            COALESCE(SUM(CASE WHEN PEM.ID_STATUS_PEMBAYARAN=3 THEN PEM.BESAR_PEMBAYARAN ELSE 0 END),0) TOTAL_TERTANGGUH,
            COALESCE(SUM(CASE WHEN PEM.ID_STATUS_PEMBAYARAN=4 THEN PEM.BESAR_PEMBAYARAN ELSE 0 END),0) TOTAL_TERBEBAS,
            (SELECT SUM(PAYMENT_AMOUNT) FROM PEMBAYARAN_VA_BANK WHERE TRX_ID=TM.TRX_ID) TOTAL_TERBAYAR_VA,
            AVG(PEM.ID_STATUS_PEMBAYARAN) STATUS_PEMBAYARAN
        FROM
            TAGIHAN_MHS TM
            JOIN MAHASISWA M ON M.ID_MHS = TM.ID_MHS
            LEFT JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN_MHS = TM.ID_TAGIHAN_MHS
            LEFT JOIN PEMBAYARAN PEM ON PEM.ID_TAGIHAN = TAG.ID_TAGIHAN AND PEM.TGL_BAYAR IS NOT NULL
            JOIN SEMESTER S ON S.ID_SEMESTER = TM.ID_SEMESTER 
        WHERE
            M.NIM_MHS = '{$nim}' 
        GROUP BY
            TM.ID_TAGIHAN_MHS,
            TM.ID_MHS,
            M.NIM_MHS,
            TM.NO_VA,
            TM.TRX_ID,
            TM.AWAL_PERIODE,
            TM.AKHIR_PERIODE,
            TM.KETERANGAN,
            TM.ID_SEMESTER,
            S.NM_SEMESTER,
            S.TAHUN_AJARAN,
            TM.TOTAL_BESAR_BIAYA
        ORDER BY
            S.TAHUN_AJARAN DESC,
            S.NM_SEMESTER DESC";
        $arr_data = $this->db->QueryToArray($query);
        foreach ($arr_data as $data) {
            array_push($data_history_bayar, array(
                'ID_TAGIHAN_MHS' => $data['ID_TAGIHAN_MHS'],
                'ID_MHS' => $data['ID_MHS'],
                'NIM_MHS' => $data['NIM_MHS'],
                'ID_SEMESTER' => $data['ID_SEMESTER'],
                'NO_VA' => $data['NO_VA'],
                'TRX_ID' => $data['TRX_ID'],
                'AWAL_PERIODE' => $data['AWAL_PERIODE'],
                'AKHIR_PERIODE' => $data['AKHIR_PERIODE'],
                'KETERANGAN' => $data['KETERANGAN'],
                'NM_SEMESTER' => $data['NM_SEMESTER'],
                'TAHUN_AJARAN' => $data['TAHUN_AJARAN'],
                'TOTAL_BESAR_BIAYA' => $data['TOTAL_BESAR_BIAYA'],
                'TOTAL_TERBAYAR' => $data['TOTAL_TERBAYAR'],
                'TOTAL_TERTANGGUH' => $data['TOTAL_TERTANGGUH'],
                'TOTAL_TERBEBAS' => $data['TOTAL_TERBEBAS'],
                'TOTAL_TERBAYAR_VA' => !empty($data['TOTAL_TERBAYAR_VA']) ? $data['TOTAL_TERBAYAR_VA'] : 0,
                'TOTAL_BELUM_TERBAYAR' => $data['TOTAL_BESAR_BIAYA'] - $data['TOTAL_TERBAYAR'] - $data['TOTAL_TERTANGGUH'] - $data['TOTAL_TERBEBAS'],
                'STATUS_PEMBAYARAN' => $data['STATUS_PEMBAYARAN'],
                'DETAIL_BIAYA' => $this->load_riwayat_pembayaran_biaya($data['ID_TAGIHAN_MHS']),
                'DETAIL_BIAYA_CETAK' => $this->load_riwayat_pembayaran_biaya_cetak($data['ID_TAGIHAN_MHS'])
            ));
        }
        return $data_history_bayar;
    }

    function load_history_bayar_mhs($nim)
    {
        $data_history_bayar = array();
        $filter = "M.NIM_MHS='{$nim}'";
        $query = "
        SELECT
            TAG.ID_TAGIHAN_MHS,
            SUM( TAG.BESAR_BIAYA + TAG.DENDA_BIAYA ) JUMLAH_PEMBAYARAN,
            TM.ID_MHS,
            S.ID_SEMESTER,
            S.NM_SEMESTER,
            S.TAHUN_AJARAN,
            S.THN_AKADEMIK_SEMESTER,
            B.ID_BANK,
            B.NM_BANK,
            BV.ID_BANK_VIA,
            BV.NAMA_BANK_VIA,
            PEM.NO_TRANSAKSI,
            TO_CHAR( PEM.TGL_BAYAR, 'DD-MON-YY' ) TGL_BAYAR,
            CONVERT( PEM.KETERANGAN, 'US7ASCII', 'WE8ISO8859P1' ) KETERANGAN,
            TAG.IS_LUNAS,
            SP.NAMA_STATUS,
            SP.ID_STATUS_PEMBAYARAN,
            SB.ID_SEMESTER ID_SEMESTER_BAYAR,
            SB.NM_SEMESTER NM_SEMESTER_BAYAR,
            SB.TAHUN_AJARAN TAHUN_AJARAN_BAYAR
        FROM
            TAGIHAN_MHS TM
            JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN_MHS=TM.ID_TAGIHAN_MHS
            LEFT JOIN PEMBAYARAN PEM ON PEM.ID_TAGIHAN=TAG.ID_TAGIHAN
            JOIN MAHASISWA M ON M.ID_MHS = TM.ID_MHS
            JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA 
            AND P.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "'
            JOIN SEMESTER S ON S.ID_SEMESTER = TM.ID_SEMESTER
            LEFT JOIN BANK B ON B.ID_BANK = PEM.ID_BANK
            LEFT JOIN SEMESTER SB ON SB.ID_SEMESTER = PEM.ID_SEMESTER_BAYAR
            LEFT JOIN BANK_VIA BV ON BV.ID_BANK_VIA = PEM.ID_BANK_VIA
            LEFT JOIN STATUS_PEMBAYARAN SP ON SP.ID_STATUS_PEMBAYARAN = PEM.ID_STATUS_PEMBAYARAN 
        WHERE
            {$filter} 
        GROUP BY
            TAG.ID_TAGIHAN_MHS,
            PEM.NO_TRANSAKSI,
            B.ID_BANK,
            B.NM_BANK,
            BV.ID_BANK_VIA,
            BV.NAMA_BANK_VIA,
            PEM.TGL_BAYAR,
            PEM.KETERANGAN,
            S.ID_SEMESTER,
            S.NM_SEMESTER,
            S.TAHUN_AJARAN,
            S.THN_AKADEMIK_SEMESTER,
            SP.ID_STATUS_PEMBAYARAN,
            TM.ID_MHS,
            TAG.IS_LUNAS,
            SP.NAMA_STATUS,
            SB.ID_SEMESTER,
            SB.NM_SEMESTER,
            SB.TAHUN_AJARAN
        ORDER BY
            S.TAHUN_AJARAN DESC,
            S.NM_SEMESTER DESC";
        $arr_data = $this->db->QueryToArray($query);
        foreach ($arr_data as $data) {
            array_push($data_history_bayar, array(
                'ID_MHS' => $data['ID_MHS'],
                'ID_SEMESTER' => $data['ID_SEMESTER'],
                'NM_SEMESTER' => $data['NM_SEMESTER'],
                'TAHUN_AJARAN' => $data['TAHUN_AJARAN'],
                'JUMLAH_PEMBAYARAN' => $data['JUMLAH_PEMBAYARAN'],
                'NO_TRANSAKSI' => $data['NO_TRANSAKSI'],
                'TGL_BAYAR' => $data['TGL_BAYAR'],
                'NM_BANK' => $data['NM_BANK'],
                'NAMA_BANK_VIA' => $data['NAMA_BANK_VIA'],
                'KETERANGAN' => $data['KETERANGAN'],
                'ID_BANK' => $data['ID_BANK'],
                'ID_BANK_VIA' => $data['ID_BANK_VIA'],
                'IS_TAGIH' => $data['IS_TAGIH'],
                'NAMA_STATUS' => $data['NAMA_STATUS'],
                'ID_STATUS_PEMBAYARAN' => $data['ID_STATUS_PEMBAYARAN'],
                'ID_SEMESTER_BAYAR' => $data['ID_SEMESTER_BAYAR'],
                'NM_SEMESTER_BAYAR' => $data['NM_SEMESTER_BAYAR'],
                'TAHUN_AJARAN_BAYAR' => $data['TAHUN_AJARAN_BAYAR'],
                'FLAG_PEMINDAHAN' => $data['FLAG_PEMINDAHAN'],
                //'DATA_PEMBAYARAN' => $this->get_history_bayar_mhs($data['ID_MHS'], $data['ID_SEMESTER'], $data['NO_TRANSAKSI'], $data['TGL_BAYAR'], $data['KETERANGAN'], $data['IS_TAGIH'])
            ));
        }
        return $data_history_bayar;
    }

    function load_history_old()
    {
        $nim = $_GET['cari'] != '' ? $_GET['cari'] : $_POST['cari'];
        //Riwayat Pembayaran Lama
        $biomhs01 = "SELECT Mhs_ua.nama, Mhs_ua.nim, Mhs_ua.id_mhs,Mhs_ua.id_prod FROM kd_fakul Kd_fakul, kd_prodi Kd_prodi, mhs_ua Mhs_ua 
			WHERE Kd_fakul.kd_fakulta = Kd_prodi.kd_fakulta   AND Kd_prodi.id_prod = Mhs_ua.id_prod AND Mhs_ua.nim = '$nim'";
        $biomhs1 = mysql_fetch_assoc(mysql_query($biomhs01));
        if ($biomhs1 != '') {
            $idmhs = $biomhs1[id_mhs];

            $datatrans01 = "SELECT * FROM transspp where id_mhs='$idmhs' order by (9999-id_thn)";
            $datatrans1 = mysql_query($datatrans01);

            while ($datatrans = mysql_fetch_array($datatrans1)) {
                $amb_sp3 = number_format($datatrans[sp3]);
                $amb_pkl = number_format($datatrans[pkl]);
                $amb_asu = number_format($datatrans[asuransi]);
                $amb_reg = number_format($datatrans[regis]);
                $amb_spp = number_format($datatrans[spp]);
                $amb_pra = number_format($datatrans[praktikum]);
                $amb_iko = number_format($datatrans[denda]);
                $amb_piu = number_format($datatrans[piutang]);
                $amb_matrikulasi = number_format($datatrans[matrikulasi]);
                $amb_kkn = number_format($datatrans[kkn]);
                $amb_wisuda = number_format($datatrans[wisuda]);
                $amb_pembinaan = number_format($datatrans[pembinaan]);
                $amb_ikoma = number_format($datatrans[ikoma]);

                $amb_tgl = $datatrans[tgl_bayar];
                $amb_ban = $datatrans[id_bank];
                $amb_sem = $datatrans[id_thn];
                $amb_sta = $datatrans[stat_spp];
                if ($datatrans["dibebaskan"] == "Y") {
                    $amb_sudahbayar = "Y";
                } else if ($datatrans["dibebaskan"] == "T") {
                    if ($datatrans["tgl_bayar"] == '0000-00-00') {
                        $amb_sudahbayar = "T";
                    } else {
                        $amb_sudahbayar = "Y";
                    }
                }


                $biothnsem01 = "SELECT * FROM thn_sem WHERE id_thn = '$amb_sem'";
                $biothnsem1 = mysql_query($biothnsem01);
                $biothnsem = mysql_fetch_array($biothnsem1);
                $amb_semes = $biothnsem[nama_semes];
                $thn_ring = $biothnsem[thn_akad];
                $sem_ring = $biothnsem[sem_akad];

                $databaru01 = "SELECT * FROM tbl_bank where id_bank='$amb_ban'";
                $databaru1 = mysql_query($databaru01);
                $databank = mysql_fetch_array($databaru1);
                $amb_bank = $databank[nm_bank];

                list($thn9, $bln9, $tgl9) = explode("-", $amb_tgl);

                $tampil_pembayaran .=
                    "<tr class='ui-widget-content'>
			<td>
				<P><FONT face=Arial color=#000099 size=1><STRONG>$amb_semes</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_sudahbayar</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_bank</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$tgl9-$bln9-$thn9</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_spp</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_pra</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_asu</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_sp3</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_pkl</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_reg</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_iko</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_piu</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_matrikulasi</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_kkn</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_wisuda</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_pembinaan</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_ikoma</STRONG></FONT></P></td>
		</tr>";
            }
        }
        return $tampil_pembayaran;
    }

    function load_detail_biaya($nim)
    {
        return $this->db->QueryToArray("
            SELECT DB.ID_DETAIL_BIAYA,B.NM_BIAYA FROM BIAYA_KULIAH_MHS BKM
            JOIN BIAYA_KULIAH BK ON BK.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH
            JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BK.ID_BIAYA_KULIAH
            JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
            JOIN MAHASISWA M ON M.ID_MHS = BKM.ID_MHS
            WHERE M.NIM_MHS='{$nim}'");
    }

    function load_detail_biaya_all()
    {
        return $this->db->QueryToArray("
            SELECT * FROM BIAYA WHERE ID_PERGURUAN_TINGGI='{$this->id_pt}' ORDER BY NM_BIAYA");
    }

    function insert_detail_biaya($cari, $id_detail_biaya, $id_semester)
    {
        $this->db->Query("SELECT ID_MHS FROM MAHASISWA WHERE NIM_MHS='{$cari}'");
        $mhs = $this->db->FetchAssoc();

        $this->db->Query("INSERT INTO PEMBAYARAN (ID_MHS,ID_SEMESTER,ID_DETAIL_BIAYA,BESAR_BIAYA,IS_TAGIH,ID_STATUS_PEMBAYARAN) VALUES ('{$mhs['ID_MHS']}','{$id_semester}','$id_detail_biaya','0','Y',2)");
    }

    function update_pembayaran_mhs($cari, $semester, $semester_asli, $tgl_bayar, $tgl_bayar_asli, $bank, $bank_asli, $bank_via, $bank_via_asli, $no_transaksi, $no_transaksi_asli, $keterangan, $keterangan_asli, $is_tagih, $is_tagih_asli, $status, $semester_bayar, $flag_pindah)
    {
        if (is_numeric($nim) && strlen($nim) < 9) {
            $query = "AND ID_MHS='{$cari}'";
        } else {
            $this->db->Query("SELECT ID_MHS FROM MAHASISWA WHERE NIM_MHS='{$cari}'");
            $mhs = $this->db->FetchAssoc();
            $query = "AND ID_MHS='{$mhs['ID_MHS']}'";
        }
        $q_tgl_bayar = $tgl_bayar_asli == '' ? 'TGL_BAYAR IS NULL' : "TO_DATE(TGL_BAYAR)='{$tgl_bayar_asli}' ";
        $q_bank = $bank_asli == '' ? "ID_BANK IS NULL" : "ID_BANK='{$bank_asli}'";
        $q_bank_via = $bank_via_asli == '' ? "ID_BANK_VIA IS NULL" : "ID_BANK_VIA='{$bank_via_asli}'";
        $q_no_transaksi = $no_transaksi_asli == '' ? "NO_TRANSAKSI IS NULL" : "NO_TRANSAKSI ='{$no_transaksi_asli}'";
        $q_keterangan = $keterangan_asli == '' ? "KETERANGAN IS NULL" : "CONVERT(KETERANGAN, 'US7ASCII', 'WE8ISO8859P1')='{$keterangan_asli}'";
        $this->db->Query("
          UPDATE PEMBAYARAN SET 
                ID_SEMESTER='{$semester}',
                IS_TAGIH='{$is_tagih}',
                TGL_BAYAR='{$tgl_bayar}',
                ID_BANK='{$bank}',
                ID_BANK_VIA='{$bank_via}',
                NO_TRANSAKSI='{$no_transaksi}',
                KETERANGAN='{$keterangan}',
                ID_STATUS_PEMBAYARAN='{$status}',
                ID_SEMESTER_BAYAR='{$semester_bayar}',
                FLAG_PEMINDAHAN='{$flag_pindah}'
            WHERE ID_SEMESTER='{$semester_asli}' 
                AND {$q_tgl_bayar}
                AND {$q_bank} 
                AND {$q_bank_via}
                AND {$q_no_transaksi}
                AND {$q_keterangan}
                AND IS_TAGIH='{$is_tagih_asli}'
                {$query}");
    }

    function hapus_pembayaran_mhs($cari, $id_semester, $id_bank, $id_bank_via, $tgl_bayar, $no_transaksi, $keterangan, $is_tagih, $id_pengguna)
    {
        $this->backup_pembayaran_dihapus($cari, $id_semester, $id_bank, $id_bank_via, $tgl_bayar, $no_transaksi, $keterangan, $is_tagih, $id_pengguna);
        if (is_numeric($nim) && strlen($nim) < 9) {
            $query = "AND ID_MHS='{$cari}'";
        } else {
            $this->db->Query("SELECT ID_MHS FROM MAHASISWA WHERE NIM_MHS='{$cari}'");
            $mhs = $this->db->FetchAssoc();
            $query = "AND ID_MHS='{$mhs['ID_MHS']}'";
        }
        $q_tgl_bayar = $tgl_bayar == '' ? 'TGL_BAYAR IS NULL' : "TO_DATE(TGL_BAYAR)='{$tgl_bayar}' ";
        $q_bank = $id_bank == '' ? "ID_BANK IS NULL" : "ID_BANK='{$id_bank}'";
        $q_bank_via = $id_bank_via == '' ? "ID_BANK_VIA IS NULL" : "ID_BANK_VIA='{$id_bank_via}'";
        $q_no_transaksi = $no_transaksi == '' ? "NO_TRANSAKSI IS NULL" : "NO_TRANSAKSI ='{$no_transaksi}'";
        $q_keterangan = $keterangan == '' ? "KETERANGAN IS NULL" : "CONVERT(KETERANGAN, 'US7ASCII', 'WE8ISO8859P1')='{$keterangan}'";

        $this->db->Query("
          DELETE FROM PEMBAYARAN
            WHERE ID_SEMESTER='{$id_semester}' 
                AND {$q_tgl_bayar}
                AND {$q_bank} 
                AND {$q_bank_via}
                AND {$q_no_transaksi}
                AND {$q_keterangan}
                AND IS_TAGIH='{$is_tagih}'
                {$query}");
    }

    function backup_pembayaran_dihapus($cari, $id_semester, $id_bank, $id_bank_via, $tgl_bayar, $no_transaksi, $keterangan, $is_tagih, $id_pengguna)
    {
        if (is_numeric($nim) && strlen($nim) < 9) {
            $query = "AND ID_MHS='{$cari}'";
        } else {
            $this->db->Query("SELECT ID_MHS FROM MAHASISWA WHERE NIM_MHS='{$cari}'");
            $mhs = $this->db->FetchAssoc();
            $query = "AND ID_MHS='{$mhs['ID_MHS']}'";
        }
        $q_tgl_bayar = $tgl_bayar == '' ? 'TGL_BAYAR IS NULL' : "TO_DATE(TGL_BAYAR)='{$tgl_bayar}' ";
        $q_bank = $id_bank == '' ? "ID_BANK IS NULL" : "ID_BANK='{$id_bank}'";
        $q_bank_via = $id_bank_via == '' ? "ID_BANK_VIA IS NULL" : "ID_BANK_VIA='{$id_bank_via}'";
        $q_no_transaksi = $no_transaksi == '' ? "NO_TRANSAKSI IS NULL" : "NO_TRANSAKSI ='{$no_transaksi}'";
        $q_keterangan = $keterangan == '' ? "KETERANGAN IS NULL" : "KETERANGAN='{$keterangan}'";
        $this->db->Query("
          INSERT INTO PEMBAYARAN_DIHAPUS (ID_MHS,ID_SEMESTER,ID_BANK,ID_BANK_VIA,TGL_BAYAR,NO_TRANSAKSI,KETERANGAN,BESAR_BIAYA,ID_PENGHAPUS)
            (SELECT ID_MHS,ID_SEMESTER,ID_BANK,ID_BANK_VIA,TGL_BAYAR,NO_TRANSAKSI,KETERANGAN,BESAR_BIAYA, '{$id_pengguna}' AS ID_PENGHAPUS FROM PEMBAYARAN
            WHERE ID_SEMESTER='{$id_semester}' 
                AND {$q_tgl_bayar}
                AND {$q_bank} 
                AND {$q_bank_via}
                AND {$q_no_transaksi}
                AND {$q_keterangan}
                AND IS_TAGIH='{$is_tagih}'
                {$query})");
    }

    function update_detail_pembayaran($id_pembayaran, $besar_biaya, $denda_biaya, $tagih, $bank, $bank_via, $tgl_bayar, $no_transaksi, $keterangan, $status)
    {
        $this->db->Query("
                UPDATE PEMBAYARAN SET 
                    BESAR_BIAYA='{$besar_biaya}',
                    DENDA_BIAYA='{$denda_biaya}',
                    IS_TAGIH='{$tagih}',
                    ID_BANK='{$bank}',
                    ID_BANK_VIA='{$bank_via}',
                    TGL_BAYAR='{$tgl_bayar}',
                    NO_TRANSAKSI='{$no_transaksi}',
                    KETERANGAN='{$keterangan}',
                    ID_STATUS_PEMBAYARAN='{$status}'
                WHERE ID_PEMBAYARAN='{$id_pembayaran}'");
    }

    function get_biaya_rincian($cari)
    {
        return $this->db->QueryToArray("SELECT B.ID_BIAYA, B.NM_BIAYA FROM AUCC.PEMBAYARAN PEM
        JOIN AUCC.MAHASISWA M ON M.ID_MHS = PEM.ID_MHS
        JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA AND P.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "'
        JOIN AUCC.DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
        JOIN AUCC.BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
        WHERE M.NIM_MHS='{$cari}'
        GROUP BY B.ID_BIAYA, B.NM_BIAYA
        ORDER BY B.NM_BIAYA");
    }

    function load_biaya_rincian($cari, $semester, $no_transaksi, $tgl_bayar, $keterangan, $is_tagih)
    {
        $q_no_transaksi = $no_transaksi != '' ? "AND PEM.NO_TRANSAKSI='{$no_transaksi}'" : "AND PEM.NO_TRANSAKSI IS NULL";
        $q_tgl_bayar = $tgl_bayar != '' ? "AND TO_DATE(PEM.TGL_BAYAR)='{$tgl_bayar}'" : "AND PEM.TGL_BAYAR IS NULL";
        $q_keterangan = $keterangan != '' ? "AND PEM.KETERANGAN ='{$keterangan}'" : "AND PEM.KETERANGAN IS NULL";
        return $this->db->QueryToArray("
        SELECT B.NM_BIAYA,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA FROM 
        (SELECT B.ID_BIAYA, B.NM_BIAYA FROM AUCC.PEMBAYARAN PEM
                JOIN AUCC.MAHASISWA M ON M.ID_MHS = PEM.ID_MHS
                JOIN AUCC.DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
                JOIN AUCC.BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
                WHERE M.NIM_MHS='{$cari}'
                GROUP BY B.ID_BIAYA, B.NM_BIAYA
                ORDER BY B.NM_BIAYA) B
        LEFT JOIN 
        (SELECT PEM.BESAR_BIAYA,DB.ID_BIAYA 
                FROM AUCC.PEMBAYARAN PEM
                JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
                JOIN AUCC.MAHASISWA M ON PEM.ID_MHS = M.ID_MHS 
                WHERE PEM.ID_SEMESTER='{$semester}' AND M.NIM_MHS='{$cari}' {$q_tgl_bayar} {$q_no_transaksi} {$q_keterangan} AND PEM.IS_TAGIH='{$is_tagih}') 
        PEM ON PEM.ID_BIAYA = B.ID_BIAYA
        GROUP BY B.NM_BIAYA
        ORDER BY B.NM_BIAYA
        ");
    }

    function load_semester_rincian($cari)
    {
        return $this->db->QueryToArray("
            SELECT S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN
                ,PEM.TGL_BAYAR,BNK.NM_BANK,BV.NAMA_BANK_VIA,PEM.NO_TRANSAKSI,PEM.KETERANGAN,SP.NAMA_STATUS,PEM.IS_TAGIH,PEM.ID_STATUS_PEMBAYARAN
                ,SB.NM_SEMESTER NM_SEMESTER_BAYAR,SB.TAHUN_AJARAN TAHUN_AJARAN_BAYAR
            FROM AUCC.PEMBAYARAN PEM
            LEFT JOIN AUCC.BANK BNK ON BNK.ID_BANK = PEM.ID_BANK
            LEFT JOIN AUCC.BANK_VIA BV ON BV.ID_BANK_VIA=PEM.ID_BANK_VIA
            LEFT JOIN AUCC.STATUS_PEMBAYARAN SP ON SP.ID_STATUS_PEMBAYARAN= PEM.ID_STATUS_PEMBAYARAN
            LEFT JOIN SEMESTER SB ON SB.ID_SEMESTER = PEM.ID_SEMESTER_BAYAR
            JOIN AUCC.SEMESTER S ON S.ID_SEMESTER = PEM.ID_SEMESTER
            JOIN AUCC.MAHASISWA M ON M.ID_MHS = PEM.ID_MHS
            JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA AND P.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "'
            WHERE M.NIM_MHS ='{$cari}'
            GROUP BY S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN
                ,PEM.TGL_BAYAR,BNK.NM_BANK,BV.NAMA_BANK_VIA,PEM.NO_TRANSAKSI,PEM.KETERANGAN,SP.NAMA_STATUS,PEM.IS_TAGIH,PEM.ID_STATUS_PEMBAYARAN,
                SB.NM_SEMESTER,SB.TAHUN_AJARAN
            ORDER BY S.TAHUN_AJARAN DESC,S.NM_SEMESTER DESC");
    }

    function load_pembayran_rincian_mhs($cari)
    {
        $data_pembayaran = array();
        foreach ($this->load_semester_rincian($cari) as $semester) {
            array_push($data_pembayaran, array(
                'NM_SEMESTER' => $semester['NM_SEMESTER'],
                'TAHUN_AJARAN' => $semester['TAHUN_AJARAN'],
                'TGL_BAYAR' => $semester['TGL_BAYAR'],
                'NO_TRANSAKSI' => $semester['NO_TRANSAKSI'],
                'NM_BANK' => $semester['NM_BANK'],
                'NAMA_BANK_VIA' => $semester['NAMA_BANK_VIA'],
                'KETERANGAN' => $semester['KETERANGAN'],
                'NAMA_STATUS' => $semester['NAMA_STATUS'],
                'IS_TAGIH' => $semester['IS_TAGIH'],
                'NM_SEMESTER_BAYAR' => $semester['NM_SEMESTER_BAYAR'],
                'TAHUN_AJARAN_BAYAR' => $semester['TAHUN_AJARAN_BAYAR'],
                'RINCIAN_PEMBAYARAN' => $this->load_biaya_rincian(
                    $cari,
                    $semester['ID_SEMESTER'],
                    $semester['NO_TRANSAKSI'],
                    $semester['TGL_BAYAR'],
                    $semester['KETERANGAN'],
                    $semester['IS_TAGIH']
                )
            ));
        }
        return $data_pembayaran;
    }

    /*
    function load_sejarah_kerjasama($nim,$id_pt) {
        return $this->db->QueryToArray("
            SELECT M.ID_MHS,SB.*,B.NM_BEASISWA,B.PENYELENGGARA_BEASISWA,B.PERIODE_PEMBERIAN_BEASISWA
                ,GB.NM_GROUP,(SM.NM_SEMESTER||' '||SM.TAHUN_AJARAN) SEMESTER_MULAI,(SS.NM_SEMESTER||' '||SS.TAHUN_AJARAN) SEMESTER_SELESAI
            FROM SEJARAH_BEASISWA SB
            JOIN BEASISWA B ON SB.ID_BEASISWA=B.ID_BEASISWA
            JOIN GROUP_BEASISWA GB ON GB.ID_GROUP_BEASISWA=B.ID_GROUP_BEASISWA
            JOIN MAHASISWA M ON SB.ID_MHS=M.ID_MHS
            JOIN PENGGUNA P ON M.ID_PENGGUNA=P.ID_PENGGUNA
            LEFT JOIN SEMESTER SM ON SM.ID_SEMESTER=SB.ID_SEMESTER_MULAI
            LEFT JOIN SEMESTER SS ON SS.ID_SEMESTER=SB.ID_SEMESTER_SELESAI
            WHERE M.NIM_MHS='{$nim}' AND P.ID_PERGURUAN_TINGGI = {$id_pt}
            ORDER BY SB.ID_SEJARAH_BEASISWA DESC");
    }
*/
    function load_sejarah_kerjasama($nim, $id_pt)
    {
        return $this->db->QueryToArray("
            SELECT M.ID_MHS,SB.*,B.NM_BEASISWA,B.PENYELENGGARA_BEASISWA,GB.NM_GROUP
            FROM SEJARAH_BEASISWA SB
            JOIN BEASISWA B ON SB.ID_BEASISWA=B.ID_BEASISWA
            JOIN GROUP_BEASISWA GB ON GB.ID_GROUP_BEASISWA=B.ID_GROUP_BEASISWA
            JOIN MAHASISWA M ON SB.ID_MHS=M.ID_MHS
            JOIN PENGGUNA P ON M.ID_PENGGUNA=P.ID_PENGGUNA
            WHERE M.NIM_MHS='{$nim}' AND P.ID_PERGURUAN_TINGGI = {$id_pt}
            ORDER BY SB.ID_SEJARAH_BEASISWA DESC");
    }

    function load_history_bayar_sp($nim)
    {
        return $this->db->QueryToArray("
            SELECT PS.*,B.NM_BANK,BV.NAMA_BANK_VIA,S.NM_SEMESTER,S.GROUP_SEMESTER,S.TAHUN_AJARAN,SP.NAMA_STATUS
            FROM PEMBAYARAN_SP PS
            JOIN BANK B ON PS.ID_BANK=B.ID_BANK
            JOIN BANK_VIA BV ON BV.ID_BANK_VIA=PS.ID_BANK_VIA
            JOIN SEMESTER S ON S.ID_SEMESTER=PS.ID_SEMESTER
            JOIN MAHASISWA M ON M.ID_MHS=PS.ID_MHS
            LEFT JOIN STATUS_PEMBAYARAN SP ON SP.ID_STATUS_PEMBAYARAN=PS.ID_STATUS_PEMBAYARAN
            WHERE M.NIM_MHS='{$nim}'");
    }

    function get_va_mhs($id_mhs)
    {
        return $this->db->QueryToArray("
            SELECT MVA.*,BV.NAMA_BANK
            FROM MAHASISWA_VA MVA
            JOIN BANK_VENDOR BV ON BV.ID_BANK_VENDOR=MVA.ID_BANK_VENDOR
            JOIN MAHASISWA M ON M.ID_MHS = MVA.ID_MHS
            WHERE M.ID_MHS='{$id_mhs}'");
    }

    function load_data_va($nim)
    {
        return $this->db->QueryToArray("
            SELECT M.NIM_MHS,BV.NAMA_BANK,MV.NO_VA,MV.TRX_ID ,MV.TGL_EXPIRED,
            SUM(PV.PAYMENT_AMOUNT) TOTAL_BAYAR,
            SUM(PV.TOTAL_DIPROSES) TOTAL_DIPROSES
            FROM MAHASISWA_VA MV
            JOIN BANK_VENDOR BV ON BV.ID_BANK_VENDOR=MV.ID_BANK_VENDOR
            JOIN MAHASISWA M ON M.ID_MHS=MV.ID_MHS
            LEFT JOIN (
                SELECT PVA.TRX_ID,PVA.PAYMENT_AMOUNT,COALESCE(SUM(PEM.BESAR_PEMBAYARAN),0) TOTAL_DIPROSES
                FROM PEMBAYARAN_VA_BANK PVA           	
                LEFT JOIN PEMBAYARAN PEM ON PVA.PAYMENT_NTB=PEM.NO_TRANSAKSI
                GROUP BY PVA.TRX_ID,PVA.PAYMENT_AMOUNT
            ) PV ON PV.TRX_ID=MV.TRX_ID
            WHERE M.NIM_MHS='{$nim}'
            GROUP BY M.NIM_MHS,BV.NAMA_BANK,MV.TRX_ID ,MV.NO_VA,MV.TGL_EXPIRED
            ");
    }

    function getTerbilang($x)
    {
        $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        if ($x < 12) {
            return " " . $abil[$x];
        } elseif ($x < 20) {
            return $this->getTerbilang($x - 10) . "belas";
        } elseif ($x < 100) {
            return $this->getTerbilang($x / 10) . " puluh" . $this->getTerbilang($x % 10);
        } elseif ($x < 200) {
            return " seratus" . $this->getTerbilang($x - 100);
        } elseif ($x < 1000) {
            return $this->getTerbilang($x / 100) . " ratus" . $this->getTerbilang($x % 100);
        } elseif ($x < 2000) {
            return " seribu" . $this->getTerbilang($x - 1000);
        } elseif ($x < 1000000) {
            return $this->getTerbilang($x / 1000) . " ribu" . $this->getTerbilang($x % 1000);
        } elseif ($x < 1000000000) {
            return $this->getTerbilang($x / 1000000) . " juta" . $this->getTerbilang($x % 1000000);
        }
    }

    function tanggalInd($date)
    {
        $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

        $tahun = substr($date, 0, 4);
        $bulan = substr($date, 5, 2);
        $tgl   = substr($date, 8, 2);

        $result = $tgl . " " . $BulanIndo[(int) $bulan - 1] . " " . $tahun;
        return ($result);
    }

    function getRomawi($bln)
    {
        switch ($bln) {
            case 1:
                return "I";
                break;
            case 2:
                return "II";
                break;
            case 3:
                return "III";
                break;
            case 4:
                return "IV";
                break;
            case 5:
                return "V";
                break;
            case 6:
                return "VI";
                break;
            case 7:
                return "VII";
                break;
            case 8:
                return "VIII";
                break;
            case 9:
                return "IX";
                break;
            case 10:
                return "X";
                break;
            case 11:
                return "XI";
                break;
            case 12:
                return "XII";
                break;
        }
    }
}
