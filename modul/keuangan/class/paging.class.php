<?php

class paging {

    public $page;

    function __construct($page) {
        $this->page = $page;
    }

    // Fungsi untuk mencek halaman dan posisi data
    function cari_posisi($batas) {
        if (empty($_GET['page'])) {
            $posisi = 0;
            $_GET['page'] = 1;
        } else {
            $posisi = ($_GET['page'] - 1) * $batas;
        }
        return $posisi;
    }

    // Fungsi untuk menghitung total halaman
    function jumlah_halaman($jmldata, $batas) {
        $jmlhalaman = ceil($jmldata / $batas);
        return $jmlhalaman;
    }

    // Fungsi untuk link halaman 1,2,3 ... Next, Prev, First, Last
    function nav_halaman($halaman_aktif, $jmlhalaman) {
        $link_halaman = "";

        // Link First dan Previous
        if ($halaman_aktif > 1) {
            $link_halaman .= $this->tag_halaman(1, 'First');
        }

        if (($halaman_aktif - 1) > 0) {
            $previous = $halaman_aktif - 1;
            $link_halaman .= $this->tag_halaman($previous, 'Previous');
        }

        // Link halaman 1,2,3, ...
        for ($i = 1; $i <= $jmlhalaman; $i++) {
            if ($i == $halaman_aktif) {
                $link_halaman .= "<b class='ui-state-highlight' style='margin:1px;padding:3px;'>$i</b>  ";
            } else {
                $link_halaman .= $this->tag_halaman($i, $i);
            }
            $link_halaman .= " ";
        }

        // Link Next dan Last
        if ($halaman_aktif < $jmlhalaman) {
            $next = $halaman_aktif + 1;
            $link_halaman .= $this->tag_halaman($next, 'Next');
        }

        if (($halaman_aktif != $jmlhalaman) && ($jmlhalaman != 0)) {
            $link_halaman .= $this->tag_halaman($jmlhalaman, 'Last');
        }
        return $link_halaman;
    }

    function tag_halaman($link, $nama) {
        $tgl_awal = $_GET['tgl_awal'];
        $tgl_akhir = $_GET['tgl_akhir'];
        $fakultas = $_GET['fakultas'];
        $prodi = $_GET['prodi'];
        $bank = $_GET['bank'];
        $jalur = $_GET['jalur'];
        $angkatan = $_GET['angkatan'];
        return "<a class='ui-button ui-state-hover' style='margin:1px;padding:3px;cursor:pointer;' href='{$this->page}?mode=detail&tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$fakultas}&prodi={$prodi}&bank={$bank}&jalur={$jalur}&angkatan={$angkatan}&page={$link}'>{$nama}</a>";
    }

}

?>
