<?php
include 'config.php';
$id_pengguna= $user->ID_PENGGUNA; 
include "../pendidikan/class/aucc.class.php";
include '../pendidikan/class/evaluasi.class.php';
include 'class/list_data.class.php';

$id_pt = $id_pt_user;

$aucc = new AUCC_Pendidikan($db);
$eval = new evaluasi($db);

$data = new list_data($db);

$smarty->assign('get_fakultas', $aucc->get_fakultas($id_pt));
$status = $db->QueryToArray("SELECT * FROM STATUS_PENGGUNA 
								WHERE ID_STATUS_PENGGUNA = 5 OR ID_STATUS_PENGGUNA = 3 OR ID_STATUS_PENGGUNA = 16 OR ID_STATUS_PENGGUNA = 17");
$smarty->assign('status', $status);
$status_penetapan = $db->QueryToArray("SELECT * FROM STATUS_PENGGUNA 
								WHERE ID_STATUS_PENGGUNA = 5 OR ID_STATUS_PENGGUNA = 3 OR ID_STATUS_PENGGUNA = 6 OR ID_STATUS_PENGGUNA = 4");
$smarty->assign('status_penetapan', $status_penetapan);
$db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER = '$_POST[semester]'");
	$semester_aktif = $db->FetchAssoc();
	$smarty->assign('semester_aktif', $semester_aktif['ID_SEMESTER']);

$semester = $db->QueryToArray("SELECT * FROM SEMESTER WHERE ID_PERGURUAN_TINGGI = '{$id_pt}' AND GROUP_SEMESTER = 'Genap' AND TIPE_SEMESTER IN ('REG','SP') 
								ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC");
$smarty->assign('semester', $semester);

$fak = $_POST['fakultas'];


if($_POST['mode'] == 'insert'){
	$no = $_POST['no'];
	$tgl = date('d-m-Y');
	for($i=2; $i<=$no; $i++){
		$id_mhs = $_POST['id_mhs'.$i];
		$rekomendasi = $_POST['rekomendasi'.$i];
		$penetapan = $_POST['penetapan'.$i];
		$tgl_rekomendasi = $_POST['tgl_rekomendasi'.$i];
		$tgl_penetapan = $_POST['tgl_penetapan'.$i];
		$keterangan = $_POST['keterangan'.$i];
		$sks = $_POST['sks'.$i];
		$ipk = $_POST['ipk'.$i];
		$status_terkini = $_POST['status_terkini'.$i];
		
		if(($tgl_penetapan != '' and $penetapan != '') or ($tgl_rekomendasi != '' and $rekomendasi != '')){
		

		$db->Query("UPDATE EVALUASI_STUDI SET IS_AKTIF = 0 WHERE ID_MHS = '$id_mhs' AND ID_SEMESTER = '$semester_aktif[ID_SEMESTER]' AND JENIS_EVALUASI = 3");
		
		$db->Query("INSERT INTO EVALUASI_STUDI (ID_MHS, ID_SEMESTER, REKOMENDASI_STATUS, PENETAPAN_STATUS, TGL_REKOMENDASI_STATUS, 
					  TGL_PENETAPAN_STATUS, KETERANGAN, JENIS_EVALUASI, SKS_DIPEROLEH, IPK_EVALUASI, STATUS_TERKINI, ID_PENGGUNA, WAKTU_UBAH, IS_AKTIF)
					  VALUES 
					 ('$id_mhs', '$semester_aktif[ID_SEMESTER]', '$rekomendasi', '$penetapan', to_date('$tgl_rekomendasi', 'DD-MM-YYYY'), 
					  to_date('$tgl_penetapan', 'DD-MM-YYYY'), '$keterangan', 3, '$sks', '$ipk', '$status_terkini', '$id_pengguna',
					  to_date('$tgl', 'DD-MM-YYYY'), 1)");
		
		$db->Query("SELECT COUNT(*) AS CEK FROM CEKAL_PEMBAYARAN 
							WHERE STATUS_CEKAL = 1 AND ID_SEMESTER = ".$semester_aktif['ID_SEMESTER']." AND ID_MHS = '$id_mhs' AND JENIS_CEKAL = 3");
				$cek = $db->FetchAssoc();
				
				if($cek['CEK'] < 1){
				$db->Query("INSERT INTO CEKAL_PEMBAYARAN (ID_MHS, ID_PENGGUNA, ID_SEMESTER, STATUS_CEKAL, TGL_UBAH, KETERANGAN, STATUS_CEKAL_FAKULTAS, JENIS_CEKAL)
					VALUES ('$id_mhs','$id_pengguna','".$semester_aktif['ID_SEMESTER']."','1',to_date('$tgl', 'DD-MM-YYYY'), '$keterangan', 1, 3)");
        		
				$db->Query("UPDATE MAHASISWA SET STATUS_CEKAL = 1 WHERE ID_MHS = '$id_mhs'");
				}
		
		}

	}

}


if(isset($_POST['fakultas'])){

		if($_POST['fakultas'] <> ''){
			$where  = " AND FAKULTAS.ID_FAKULTAS = '$_POST[fakultas]'";
			$where2  = " AND h.ID_FAKULTAS = '$_POST[fakultas]'";
		}else{
			$where  = "";
			$where2  = "";
		}

/*

$mhs = $db->QueryToArray("SELECT MAHASISWA.ID_MHS, NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, NM_FAKULTAS, NM_STATUS_PENGGUNA, NM_JENJANG
				, STATUS_CEKAL, FAKULTAS.ID_FAKULTAS, A.PIUTANG,
				(CASE WHEN B.SUDAH_KRS IS NULL THEN 0 ELSE B.SUDAH_KRS END) AS SUDAH_KRS, STATUS_PENGGUNA.ID_STATUS_PENGGUNA,
				IPK, SKS, JML_NILAI_D, JML_SKS_D, ROUND(JML_SKS_D / SKS * 100, 2) AS PERSEN_NILAI_D, 
				EVALUASI_STUDI.KETERANGAN, REKOMENDASI_STATUS, PENETAPAN_STATUS,
				to_char(TGL_REKOMENDASI_STATUS, 'dd-mm-yyyy') as TGL_REKOMENDASI_STATUS, 
				to_char(TGL_PENETAPAN_STATUS, 'dd-mm-yyyy') as TGL_PENETAPAN_STATUS
				FROM MAHASISWA
				LEFT JOIN PENGGUNA ON MAHASISWA.ID_PENGGUNA = PENGGUNA.ID_PENGGUNA
				LEFT JOIN PROGRAM_STUDI ON MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
				LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
				LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
				LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
				JOIN 
				(
					SELECT ID_MHS, COUNT(*) AS PIUTANG FROM 
						(
							SELECT MAHASISWA.ID_MHS, SEMESTER.ID_SEMESTER
							FROM PEMBAYARAN 
							JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBAYARAN.ID_MHS
							JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
							JOIN SEMESTER ON SEMESTER.ID_SEMESTER = PEMBAYARAN.ID_SEMESTER
							WHERE (ID_STATUS_PEMBAYARAN = 2 OR ID_STATUS_PEMBAYARAN IS NULL) AND STATUS_PENGGUNA.STATUS_AKTIF_BAYAR = 1
							AND STATUS_AKTIF_PEMBAYARAN = 'False'
							GROUP BY MAHASISWA.ID_MHS, SEMESTER.ID_SEMESTER
						) GROUP BY ID_MHS HAVING COUNT(*) > 1
				) A ON A.ID_MHS = MAHASISWA.ID_MHS
				LEFT JOIN (
					SELECT COUNT(DISTINCT PENGAMBILAN_MK.ID_MHS) AS SUDAH_KRS, PENGAMBILAN_MK.ID_MHS 
					FROM PENGAMBILAN_MK
					LEFT JOIN SEMESTER ON SEMESTER.ID_SEMESTER = PENGAMBILAN_MK.ID_SEMESTER
					LEFT JOIN MAHASISWA ON PENGAMBILAN_MK.ID_MHS = MAHASISWA.ID_MHS
					LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
					JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
					WHERE SEMESTER.STATUS_AKTIF_SEMESTER = 'True' $where
					GROUP BY PENGAMBILAN_MK.ID_MHS) B ON B.ID_MHS = MAHASISWA.ID_MHS
				LEFT JOIN (select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
				from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
				c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
				row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
				count(*) over(partition by c.nm_mata_kuliah) terulang
					from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
					where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
					and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs
					and h.id_program_studi = g.id_program_studi $where2
				) x
				where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
				group by id_mhs) C ON C.ID_MHS = MAHASISWA.ID_MHS
				LEFT JOIN (select id_mhs, sum(kredit_semester) as jml_sks_d, count(*) as jml_nilai_d from (
				select f.NIM_MHS,a.id_mhs,e.tahun_ajaran,e.nm_semester,c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai,
				 row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
				 count(*) over(partition by c.nm_mata_kuliah) terulang
					from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, mahasiswa f, program_studi h
					where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah 
				and a.id_semester=e.id_semester and a.STATUS_APV_PENGAMBILAN_MK='1'  and f.id_mhs = a.id_mhs
				and h.id_program_studi = f.id_program_studi $where2
					) apem
				where APEM.NILAI_HURUF = 'D' AND APEM.RANGKING = 1
				GROUP BY id_mhs
				) D ON D.ID_MHS = MAHASISWA.ID_MHS
				LEFT JOIN EVALUASI_STUDI ON EVALUASI_STUDI.ID_MHS = MAHASISWA.ID_MHS AND EVALUASI_STUDI.ID_SEMESTER = '$semester_aktif[ID_SEMESTER]' AND IS_AKTIF = 1 AND JENIS_EVALUASI = 3
				WHERE STATUS_AKTIF_BAYAR = 1  $where
				ORDER BY NM_JENJANG, NM_PROGRAM_STUDI, NIM_MHS");
				
$smarty->assign('mhs', $mhs);*/
	
	
	$smarty->assign('mhs', $eval->evaluasi_administrasi($_REQUEST['fakultas'], $_REQUEST['jenjang'], $_REQUEST['prodi'], $_REQUEST['semester']));
}

$smarty->assign('id_semester', $semester_aktif['ID_SEMESTER']);
$smarty->assign('id_fakultas', $_POST['fakultas']);
$smarty->assign('id_jenjang', $_POST['jenjang']);
$smarty->assign('data_jenjang',$data->load_list_jenjang());
$smarty->display("evaluasi/evaluasi-administrasi.tpl");
?>