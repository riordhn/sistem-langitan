<?php
include 'config.php';
include 'class/piutang.class.php';
include 'class/list_data.class.php';

$list = new list_data($db);
$piutang = new piutang($db);

$data_laporan = $piutang->load_detail_tagihan_mhs_wh(get('fakultas'), get('prodi'), get('semester'),get('params'));
$data_biaya = $list->load_biaya();
if (get('fakultas') != '' || get('prodi') != '') {
    $fakultas = $list->get_fakultas(get('fakultas'));
    $prodi = $list->get_prodi(get('prodi'));
}
ob_start();
?>
<style>
    .header_text {
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #2b1212;
    }

    td {
        text-align: left;
    }
</style>
<table width='950' border='1' align='left' cellpadding='0' cellspacing='0' class='keuangan'>
    <thead>
        <tr>
            <td colspan="<?php
                            $colspan = count($data_biaya) + 7;
                            echo $colspan;
                            ?>" class="header_text">
                <?php
                echo 'Rekapitulasi Tahihan Mahasiswa<br/>';
                if ($fakultas != '') {
                    echo 'Fakultas ' . $fakultas['NM_FAKULTAS'] . '<br/>';
                }
                if ($prodi != '') {
                    echo 'Program Studi (' . $prodi['NM_JENJANG'] . ') ' . $prodi['NM_PROGRAM_STUDI'] . '<br/>';
                    echo 'Per Tanggal  ' . date('d-m-y') . '<br/>';
                }
                ?>
            </td>
        </tr>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>NIM</th>
            <th>Prodi</th>
            <th>Angkatan</th>
            <th>Jalur</th>
            <?php
            foreach ($data_biaya as $data) {
                echo "<th>{$data['NM_BIAYA']}</th>";
            }
            ?>
            <th>Total Pembayaran</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        $total_pembayaran_bottom = array();
        foreach ($data_laporan as $data) {
            echo "<tr>
                        <td>{$no}</td>";
            echo "<td> {$data['NAMA']}</td>";
            echo "<td> {$data['NIM']}</td>";
            echo "<td> {$data['PRODI']}</td>";
            echo "<td> {$data['ANGKATAN']}</td>";
            echo "<td>{$data['JALUR']}</td>";
            $no_pembayaran = 0;
            foreach ($data_biaya as $b) {
                echo "<td>".($data['BIAYA_'.($no_pembayaran+1)])."</td>";
                $total_pembayaran_bottom[$no_pembayaran] += $data['BIAYA_'.($no_pembayaran+1)];
                $no_pembayaran++;
            }
            $no++;
            echo "<td>{$data['TOTAL']}</td>
                    </tr>";
        }
        ?>
        <tr>
            <td bgcolor='#CCFF99' colspan="6">Jumlah</td>
            <?php
            $no_total = 0;
            foreach ($data_biaya as $data) {
                echo "<td bgcolor='#CCFF99'>{$total_pembayaran_bottom[$no_total]}</td>";
                $no_total++;
            }
            ?>
            <td bgcolor='#CCFF99' align='right' ><?php echo array_sum($total_pembayaran_bottom) ; ?></td>
        </tr>
    </tbody>
</table>

<?php
echo '<b>Data Ini Di Ambil Pada Tanggal ' . date('d F Y  H:i:s') . '</b>';

$nm_file = "detail-tagihan-mhs-fakultas-" . date('d-m-Y');
header("Content-disposition: attachment; filename=$nm_file.xls");
header("Content-type: Application/exe");
header("Content-Transfer-Encoding: binary");
ob_flush();
?>