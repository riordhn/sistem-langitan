<?php
include 'config.php';
include 'class/laporan.class.php';
include 'class/list_data.class.php';

$list = new list_data($db);
$laporan = new laporan($db);

$data_excel = $laporan->load_data_status_pembayaran(get('fakultas'), get('prodi'), get('semester'), get('status'), get('jalur'), get('angkatan'), get('jenjang'));
if (get('fakultas') != '' || get('prodi') != '') {
    $fakultas = $list->get_fakultas(get('fakultas'));
    $prodi = $list->get_prodi(get('prodi'));
}
ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #2b1212;
        text-transform:capitalize;
    }
    td{
        text-align: left;
    }
</style>
<table border='1' align='left' cellpadding='0' cellspacing='0' class='keuangan'>
    <thead>
        <tr>
            <td colspan="10" class="header_text">
                <?php
                echo 'Kontrol Status Pembayaran Mahasiswa<br/>';
                if ($fakultas != '') {
                    echo 'Fakultas ' . $fakultas['NM_FAKULTAS'] . '<br/>';
                }
                if ($prodi != '') {
                    echo 'Program Studi (' . $prodi['NM_JENJANG'] . ') ' . $prodi['NM_PROGRAM_STUDI'] . '<br/>';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td class="header_text">NO</td>
            <td class="header_text">NIM</td>
            <td class="header_text">NAMA</td>
            <td class="header_text">PRODI</td>
            <td class="header_text">JALUR</td>
            <td class="header_text">STATUS MHS</td>
            <td class="header_text">BIAYA</td>
            <td class="header_text">STATUS BAYAR</td>
            <td class="header_text">KET. PEMBAYARAN</td>
            <td class="header_text">KET. STATUS</td>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach ($data_excel as $data) {
            echo "<tr>
				<td>{$no}</td>
				<td>'{$data['NIM_MHS']}</td>
				<td>{$data['NM_PENGGUNA']}</td>
				<td>{$data['NM_JENJANG']} {$data['NM_PROGRAM_STUDI']}</td>
                                <td>{$data['NM_JALUR']}</td>
				<td>{$data['NM_STATUS_PENGGUNA']}</td>
				<td>{$data['TOTAL_BIAYA']}</td>
                                <td>{$data['NM_STATUS']}</td>
				<td>{$data['KETERANGAN_PEMBAYARAN']}</td>
				<td>{$data['KETERANGAN_STATUS']}</td>
			</tr>";
            $no++;
        }
        ?>
    </tbody>
</table>

<?php
echo '<b>Data Ini Di Ambil Pada Tanggal ' . date('d F Y  H:i:s') . '</b>';

$nm_file = "kontrol-status-" . date('d-m-Y');
header("Content-disposition: attachment; filename=$nm_file.xls");
header("Content-type: Application/exe");
header("Content-Transfer-Encoding: binary");
ob_flush();
?>
