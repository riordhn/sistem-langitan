<?php
include 'config.php';
include 'class/laporan.class.php';
include 'class/list_data.class.php';

$list = new list_data($db);
$laporan = new laporan($db);

$data_excel= $laporan->load_report_bank_mhs(get('tgl_awal'),get('tgl_akhir'),get('fakultas'),get('prodi'));
if(get('fakultas')!=''||get('prodi')!=''){
	$fakultas = $list->get_fakultas(get('fakultas'));
	$prodi = $list->get_prodi(get('prodi'));
}
ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #2b1212;
		text-transform:uppercase;
    }
    td{
        text-align: left;
    }
</style>
<table width='950' border='1' align='left' cellpadding='0' cellspacing='0' class='keuangan'>
	<thead>
		<tr>
			<td colspan="4" class="header_text">
				<?php 
					echo 'Rekapitulasi Pembayaran Mahasiswa<br/>';
					if($fakultas!=''){
						echo 'Fakultas '.$fakultas['NM_FAKULTAS'].'<br/>';
					}
					if($prodi!=''){
						echo 'Program Studi ('.$prodi['NM_JENJANG'].') '.$prodi['NM_PROGRAM_STUDI'].'<br/>';
					}
				?>
			</td>
		</tr>
		<tr>
			<td class="header_text">NO</td>
			<td class="header_text">NAMA BANK</td>
			<td class="header_text">JUMLAH MAHASISWA</td>
			<td class="header_text">JUMLAH PEMBAYARAN</td>
		</tr>
	  </thead>
	  <tbody>
        <?php
		$no=1;
        foreach ($data_excel as $data) {
            echo "<tr>
				<td>{$no}</td>
				<td>{$data['NM_BANK']}</td>
				<td>{$data['JUMLAH_MHS']}</td>
				<td>{$data['BESAR_BIAYA']}</td>
			</tr>";
			$no++;
			$total_pembayaran = $total_pembayaran + $data['BESAR_BIAYA'];
        }
        ?>
		<tr>
			<td bgcolor='#CCFF99' align='right' colspan='3'><b>JUMLAH</b></td>
			<td bgcolor='#CCFF99' align='right'><?php echo $total_pembayaran; ?></td>
		</tr>
	</tbody>
</table>

<?php
echo '<b>Data Ini Di Ambil Pada Tanggal '.date('d F Y  H:i:s').'</b>'; 

$nm_file = "detail-pembayaran-mhs-bank__".date('d-m-Y');
header("Content-disposition: attachment; filename=$nm_file.xls");
header("Content-type: Application/exe");
header("Content-Transfer-Encoding: binary");
ob_flush();
?>
