<?php

// Test CVS
include 'config.php';
include 'class/reader.php';
include 'class/list_data.class.php';
include 'class/upload.class.php';

$list = new list_data($db);
$upload = new upload($db);

// ExcelFile($filename, $encoding);
$data = new Spreadsheet_Excel_Reader();
$status_message = '';

if (isset($_POST)) {
    // Saat menyimpan data
    if (post('mode') == 'save_data') {
        $berhasil = 0;
        $gagal = 0;
        foreach ($_SESSION['data_pembayaran'] as $p) {
            if ($p['STATUS_MAHASISWA'] == 'Data Mahasiswa Ada') {
                if ($p['STATUS_SIMPAN'] == "1") {
                    if ($_SESSION['mode_input'] == '0') {
                        $db->Query("
                        INSERT INTO AUCC.PEMBAYARAN (ID_MHS,ID_DETAIL_BIAYA,BESAR_BIAYA,ID_SEMESTER,IS_TAGIH,ID_STATUS_PEMBAYARAN,KETERANGAN)
                        SELECT M.ID_MHS,DB.ID_DETAIL_BIAYA,DB.BESAR_BIAYA,'{$_SESSION['semester']}' ID_SEMESTER,'{$_SESSION['is_tagih']}' IS_TAGIH,
                            2 ID_STATUS_PEMBAYARAN,'{$_SESSION['keterangan']}' KETERANGAN
                        FROM AUCC.MAHASISWA M
                        JOIN AUCC.BIAYA_KULIAH_MHS BKM ON BKM.ID_MHS=M.ID_MHS
                        JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH=BKM.ID_BIAYA_KULIAH
                        WHERE DB.ID_BIAYA='{$_SESSION['biaya']}' AND M.ID_MHS='{$p['ID_MHS']}'
                        ");
                    } else if($_SESSION['mode_input'] == '1') {
                        $db->Query("
                        INSERT INTO AUCC.PEMBAYARAN (ID_MHS,ID_DETAIL_BIAYA,BESAR_BIAYA,ID_SEMESTER,IS_TAGIH,ID_STATUS_PEMBAYARAN,KETERANGAN)
                        SELECT M.ID_MHS,DB.ID_DETAIL_BIAYA,'{$_SESSION['besar_biaya']}' BESAR_BIAYA,'{$_SESSION['semester']}' ID_SEMESTER,'{$_SESSION['is_tagih']}' IS_TAGIH,
                            2 ID_STATUS_PEMBAYARAN,'{$_SESSION['keterangan']}' KETERANGAN
                        FROM AUCC.MAHASISWA M
                        JOIN AUCC.BIAYA_KULIAH_MHS BKM ON BKM.ID_MHS=M.ID_MHS
                        JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH=BKM.ID_BIAYA_KULIAH
                        WHERE DB.ID_BIAYA='{$_SESSION['biaya']}' AND M.ID_MHS='{$p['ID_MHS']}'
                        ");
                    }else{
                        $id_detail_luar_master = $db->QuerySingle("SELECT ID_DETAIL_BIAYA FROM DETAIL_BIAYA WHERE ID_BIAYA='{$_SESSION['biaya']}'");
                        $db->Query("
                        INSERT INTO AUCC.PEMBAYARAN (ID_MHS,ID_DETAIL_BIAYA,BESAR_BIAYA,ID_SEMESTER,IS_TAGIH,ID_STATUS_PEMBAYARAN,KETERANGAN)
                        SELECT M.ID_MHS,'{$id_detail_luar_master}' ID_DETAIL_BIAYA,'{$_SESSION['besar_biaya']}' BESAR_BIAYA,'{$_SESSION['semester']}' ID_SEMESTER,'{$_SESSION['is_tagih']}' IS_TAGIH,
                            2 ID_STATUS_PEMBAYARAN,'{$_SESSION['keterangan']}' KETERANGAN
                        FROM AUCC.MAHASISWA M
                        WHERE M.ID_MHS='{$p['ID_MHS']}'
                        ");
                    }
                    // Update Batas Bayar
                    $db->Query("UPDATE MAHASISWA SET BATAS_BAYAR='{$_SESSION['batas_bayar']}' WHERE ID_MHS='{$p['ID_MHS']}'");
                    $berhasil+=1;
                } else if ($p['STATUS_SIMPAN'] == "2") {
                    if ($_SESSION['mode_input'] == '0') {
                        $master_biaya = $upload->get_biaya_master($p['ID_MHS'], $_SESSION['biaya']);
                        $db->Query("
                        UPDATE AUCC.PEMBAYARAN SET 
                            BESAR_BIAYA='{$master_biaya}',
                            TGL_BAYAR=NULL,
                            ID_BANK=NULL,
                            ID_BANK_VIA=NULL,
                            NO_TRANSAKSI=NULL,
                            ID_STATUS_PEMBAYARAN=2,
                            KETERANGAN='{$_SESSION['keterangan']}',
                            IS_TAGIH='{$_SESSION['is_tagih']}',
                            ID_SEMESTER_BAYAR=NULL
                        WHERE ID_PEMBAYARAN IN(
                        SELECT PEM.ID_PEMBAYARAN 
                        FROM AUCC.PEMBAYARAN PEM
                        JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PEM.ID_DETAIL_BIAYA
                        WHERE DB.ID_BIAYA='{$_SESSION['biaya']}' AND PEM.ID_MHS='{$p['ID_MHS']}'
                        )
                        ");
                    } else {
                        $db->Query("
                        UPDATE AUCC.PEMBAYARAN SET 
                            BESAR_BIAYA='{$_SESSION['besar_biaya']}',
                            TGL_BAYAR=NULL,
                            ID_BANK=NULL,
                            ID_BANK_VIA=NULL,
                            NO_TRANSAKSI=NULL,
                            ID_STATUS_PEMBAYARAN=2,
                            KETERANGAN='{$_SESSION['keterangan']}',
                            IS_TAGIH='{$_SESSION['is_tagih']}',
                            ID_SEMESTER_BAYAR=NULL
                        WHERE ID_PEMBAYARAN IN(
                        SELECT PEM.ID_PEMBAYARAN 
                        FROM AUCC.PEMBAYARAN PEM
                        JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PEM.ID_DETAIL_BIAYA
                        WHERE DB.ID_BIAYA='{$_SESSION['biaya']}' AND PEM.ID_MHS='{$p['ID_MHS']}'
                        )
                        ");
                    }
                    // Update Batas Bayar
                    $db->Query("UPDATE MAHASISWA SET BATAS_BAYAR='{$_SESSION['batas_bayar']}' WHERE ID_MHS='{$p['ID_MHS']}'");
                    $berhasil+=1;
                } else if ($p['STATUS_SIMPAN'] == "0") {
                    $gagal+=1;
                }
            }
        }
        $smarty->assign('berhasil', $berhasil);
        $smarty->assign('gagal', $gagal);
        $smarty->assign('sukses_simpan', '1');
    }
    // Saat menyimpan Excel
    else if (post('mode') == 'save_excel') {
        $filename = date('dmy_H_i_s') . '_' . $_FILES["file"]["name"];
        if ($_FILES["file"]["error"] > 0) {
            $status_message .= "Error : " . $_FILES["file"]["error"] . "<br />";
            $status_upload = '0';
        } else {
            $status_message .= "Nama File : " . $filename . "<br />";
            $status_message .= "Type : " . $_FILES["file"]["type"] . "<br />";
            $status_message .= "Size : " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
            if (file_exists("excel/excel_pembayaran_tambah/" . $filename)) {
                $status_message .= 'Keterangan : ' . $filename . " Sudah Ada ";
                $status_upload = '0';
            } else {
                move_uploaded_file($_FILES["file"]["tmp_name"], "excel/excel_pembayaran_tambah/" . $filename);
                chmod("excel/excel_pembayaran_tambah/" . $filename, 0755);
                $status_upload = '1';
            }
        }
        //Saat upload Berhasil

        if ($status_upload) {
            $data->setOutputEncoding('CP1251');
            $data->read("excel/excel-data-pembayaran-2/{$filename}"); // 170717 fth
            $data_pembayaran_upload = array();
            for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
                $status_simpan = 0;
                $nim = trim($data->sheets[0]['cells'][$i][1]);
                if (count($upload->cek_mahasiswa($nim)) > 0) {
                    $status_mahasiswa = 'Data Mahasiswa Ada';
                    $data_mhs = $upload->get_mahasiswa($nim);
                    if ($upload->cek_biaya_tambah_pembayaran($data_mhs['ID_MHS'], post('biaya'), post('semester')) > 0) {
                        if ($upload->cek_biaya_tambah_pembayaran_belum_bayar($data_mhs['ID_MHS'], post('biaya'), post('semester')) > 0) {
                            $status_biaya = "Biaya Sudah Ada, belum terbayar";
                            $status_biaya .= "<br/>Besar Biaya : " . $upload->get_biaya_tambah_pembayaran_belum_bayar($data_mhs['ID_MHS'], post('biaya'), post('semester'));
                            if ($upload->cek_biaya_master($data_mhs['ID_MHS'], post('biaya')) > 0) {
                                $status_biaya .= "<br/>Biaya Master : " . $upload->get_biaya_master($data_mhs['ID_MHS'], post('biaya'));
                                $status_biaya .= "<br/>Status Simpan : <span style='color:green'>Update</span>";
                                $status_simpan = 2;
                            } else {
                                $status_biaya .= "<br/><span style='color:red'>Master Biaya : Tidak benar/Biaya tidak ada di master</span>";
                            }
                        } else {
                            $status_biaya = "Biaya Sudah Ada, sudah terbayar";
                            $besar_biaya_bayar = $upload->get_biaya_tambah_pembayaran_sudah_bayar($data_mhs['ID_MHS'], post('biaya'), post('semester'));
                            $status_biaya .= "<br/>Besar Biaya : " . $besar_biaya_bayar;
                            if ($upload->cek_biaya_master($data_mhs['ID_MHS'], post('biaya')) > 0) {
                                $besar_biaya_master = $upload->get_biaya_master($data_mhs['ID_MHS'], post('biaya'));
                                $status_biaya .= "<br/>Biaya Master : " . $besar_biaya_master;
                                if ($besar_biaya_bayar == "0") {
                                    if ($besar_biaya_master == "0") {
                                        $status_biaya .= "<br/>Status Simpan : <span style='color:red'>Tidak ada</span>";
                                    } else {
                                        $status_biaya .= "<br/>Status Simpan :<span style='color:green'> Update</span>";
                                        $status_simpan = 2;
                                    }
                                } else {
                                    $status_biaya .= "<br/>Status Simpan : <span style='color:red'>Tidak ada</span>";
                                }
                            } else {
                                $status_biaya .= "<br/><span style='color:red'>Master Biaya : Tidak benar/Biaya tidak ada di master</span>";
                            }
                        }
                    } else {
                        $status_biaya = "Biaya Belum Ada";
                        if ($upload->cek_biaya_master($data_mhs['ID_MHS'], post('biaya')) > 0) {
                            $status_biaya .= "<br/>Biaya Master : " . $upload->get_biaya_master($data_mhs['ID_MHS'], post('biaya'));
                            $status_biaya .= "<br/>Status Simpan : <span style='color:green'>Insert</span>";
                            $status_simpan = 1;
                        } else if (post('mode_input') != 2) {
                            $status_biaya .= "<br/><span style='color:red'>Master Biaya : Tidak benar/Biaya tidak ada di master</span>";
                        } else {
                            $status_simpan = 1;
                            $status_biaya .= "<br/><span style='color:red'>Master Biaya : Biaya Di Tambahkan di luar master</span>";
                        }
                    }
                } else {
                    $status_mahasiswa = 'Data Mahasiswa Tidak Ada';
                    $status_biaya = "Kosong";
                }
                array_push($data_pembayaran_upload, array(
                    'ID_MHS' => $data_mhs['ID_MHS'],
                    'NIM_MHS' => $nim,
                    'NAMA' => $data_mhs['NM_PENGGUNA'],
                    'STATUS_BIAYA' => $status_biaya,
                    'STATUS_MAHASISWA' => $status_mahasiswa,
                    'STATUS_SIMPAN' => $status_simpan
                ));
            }

            $_SESSION['data_pembayaran'] = $data_pembayaran_upload;
            $_SESSION['semester'] = post('semester');
            $_SESSION['biaya'] = post('biaya');
            $_SESSION['mode_input'] = post('mode_input');
            $_SESSION['besar_biaya'] = post('besar_biaya');
            $_SESSION['keterangan'] = post('keterangan');
            $_SESSION['is_tagih'] = post('is_tagih');
            $_SESSION['batas_bayar'] = post('batas_bayar');
            $smarty->assign('data_pembayaran_upload', $_SESSION['data_pembayaran']);
        }
        $smarty->assign('status_upload', $status_upload);
        $smarty->assign('status_message', $status_message);
    }
}

$smarty->assign('data_semester', $list->load_list_semester());
$smarty->assign('data_biaya', $list->load_biaya());
$smarty->display('upload-pembayaran/upload-data-pembayaran-2.tpl');
?>
