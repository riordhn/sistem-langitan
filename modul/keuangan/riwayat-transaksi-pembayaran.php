<?php

include 'config.php';
include 'class/history.class.php';

$history = new history($db);
$data_tagihan_pembayaran = [];
$id_pt = $id_pt_user;


if (post('mode') == 'hapus_riwayat') {
    $no_transaksi = post('no_transaksi');
    $db->Query("
        DELETE FROM PEMBAYARAN_PERBULAN WHERE ID_PEMBAYARAN IN (SELECT ID_PEMBAYARAN FROM PEMBAYARAN WHERE NO_TRANSAKSI='{$no_transaksi}')
        ");
    $db->Query("
    DELETE FROM PEMBAYARAN WHERE NO_TRANSAKSI='{$no_transaksi}'
    ");
    $db->Query("UPDATE PEMBAYARAN_VA_BANK SET NOTES=NULL WHERE PAYMENT_NTB='{$no_transaksi}'");
}

$data_tagihan_biaya = $history->load_riwayat_pembayaran_biaya(get('id_tagihan_mhs'));
foreach ($data_tagihan_biaya as $d) {
    $arr = [
        'SEMESTER' => $d['NM_SEMESTER'] . ' ' . $d['TAHUN_AJARAN'],
        'NAMA_BIAYA' => $d['NM_BIAYA'],
        'BESAR_TAGIHAN' => $d['BESAR_BIAYA'],
        'BESAR_TERBAYAR' => $d['TOTAL_TERBAYAR'],
        'IS_LUNAS' =>  $d['BESAR_BIAYA'] == $d['TOTAL_TERBAYAR'] && $d['TOTAL_TERBAYAR'] > 0 ? 1 : 0,
    ];
    $data_tagihan_pembayaran[] = $arr;
}
$smarty->assign('cari', get('cari'));
$smarty->assign('id_pt', $id_pt);
$smarty->assign('jenis', 'mhs');
$smarty->assign('data_detail_mahasiswa', $history->get_data_mhs(trim(get('cari')), getenv('ID_PT')));
$smarty->assign('data_riwayat_pembayaran', $history->load_riwayat_transaksi_pembayaran(get('id_tagihan_mhs')));
$smarty->assign('data_tagihan_pembayaran', $data_tagihan_pembayaran);
$smarty->display('history/riwayat-transaksi-pembayaran.tpl');
