<?php

include 'config.php';
include 'class/master.class.php';
include 'class/history.class.php';

$biaya_mhs = new master($db);
$history = new history($db);

$id_pt = $id_pt_user;

if (get('cari') != '') {
    if (post('biaya_kuliah') != '') {
        if ($biaya_mhs->get_biaya_kuliah_mhs(trim(get('cari'))) != '')
            $biaya_mhs->update_biaya_kuliah_mhs(get('cari'), post('biaya_kuliah'));
        else
            $biaya_mhs->insert_biaya_kuliah_mhs(get('cari'), post('biaya_kuliah'));
    }
    $smarty->assign('biaya_kuliah_mhs', $biaya_mhs->get_biaya_kuliah_mhs(trim(get('cari'))));
    $smarty->assign('data_biaya_kuliah', $biaya_mhs->load_biaya_kuliah_mhs(trim(get('cari'))));
    $smarty->assign('data_detail_mahasiswa', $history->get_data_mhs(trim(get('cari')),$id_pt));
}

$smarty->display("master/biaya-mahasiswa.tpl");
?>
