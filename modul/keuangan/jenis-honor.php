<?php

include 'config.php';
include '../keuangan/class/honor.class.php';
include '../keuangan/class/list_data.class.php';
$list = new list_data($db);
$honor = new honor($db);
if (isset($_GET['mode'])) {
    if (get('mode') == 'add') {
        $smarty->display('honor/add-jenis-honor.tpl');
    } else if (get('mode') == 'edit') {
        $smarty->assign('data_jenis_honor', $honor->get_jenis_honor(get('id')));
        $smarty->display('honor/edit-jenis-honor.tpl');
    }
} else {
    if (isset($_POST)) {
        if (post('mode') == 'add') {
            $honor->add_jenis_honor(post('nama'), post('tipe'));
            $smarty->assign('alert', alert_success('Data Berhasil Dimasukkan'));
        } else if (post('mode') == 'edit') {
            $honor->edit_jenis_honor(post('id'), post('nama'), post('tipe'));
            $smarty->assign('alert', alert_success('Data Berhasil Di Ubah'));
        }
    }
    $smarty->assign('data_jenis_honor', $honor->load_jenis_honor());
    $smarty->display('honor/jenis-honor.tpl');
}
?>
