<?php
include 'config.php';
include 'class/history.class.php';

$history = new history($db);

$id_pt = $id_pt_user;

$nim = trim(get('cari'));
$mode = get('mode', '');

if (get('cari') != '') {

	$db->Query("SELECT m.ID_MHS 
					FROM MAHASISWA m 
					JOIN PENGGUNA p ON p.ID_PENGGUNA = m.ID_PENGGUNA
					WHERE m.NIM_MHS = '{$nim}' AND p.ID_PERGURUAN_TINGGI = '{$id_pt}'");
	$mhs = $db->FetchAssoc();

	$id_mhs = $mhs['ID_MHS'];

	$semester_set = $db->QueryToArray("SELECT DISTINCT ID_SEMESTER, NM_SEMESTER || ' (' || TAHUN_AJARAN || ')' AS SEMESTER , NM_SEMESTER, THN_AKADEMIK_SEMESTER 
										FROM SEMESTER
										WHERE ID_PERGURUAN_TINGGI = {$id_pt} 
										AND THN_AKADEMIK_SEMESTER IN (select to_char(add_months(trunc(sysdate, 'yyyy'), -12*(level -1)), 'yyyy') yr
											from   dual
											connect by level <= 7)
										AND NM_SEMESTER IN ('Ganjil','Genap')
										ORDER BY THN_AKADEMIK_SEMESTER, NM_SEMESTER");
	$smarty->assign('semester_set', $semester_set);

	$smarty->assign('data_detail_mahasiswa', $history->get_data_mhs(trim(get('cari')),$id_pt));

	$smarty->assign('status_cetak', 100);

	$id_semester 	= (int)post('id_semester', '');

	if($id_semester!=''){
		$db->Query("SELECT IS_BOLEH_CETAK 
									FROM KEUANGAN_KHS 
									WHERE ID_SEMESTER='{$id_semester}' 
									AND ID_MHS = (SELECT m.ID_MHS FROM MAHASISWA m 
													JOIN PENGGUNA p ON p.ID_PENGGUNA = m.ID_PENGGUNA
													WHERE m.NIM_MHS = '{$nim}' AND p.ID_PERGURUAN_TINGGI = '{$id_pt}')");
		$is_boleh_cetak = $db->FetchAssoc();

		if($is_boleh_cetak['IS_BOLEH_CETAK'] == ''){
			$smarty->assign('status_cetak', 10);
			$smarty->assign('id_semester', $id_semester);
		}
		else{
			$smarty->assign('status_cetak', $is_boleh_cetak['IS_BOLEH_CETAK']);
			$smarty->assign('id_semester', $id_semester);
		}
	}

	if (post('mode') == 'set_cetak') {
		$id_semester = (int)post('id_semester', '');
		$status_cetak =  (int)post('status_cetak', '');

		if($status_cetak == 10){
			$db->Query("INSERT INTO KEUANGAN_KHS (ID_MHS, ID_SEMESTER, IS_BOLEH_CETAK) 
								VALUES ('{$id_mhs}','{$id_semester}','1')");

			 header('Location: '.$_SERVER['REQUEST_URI']);
		}
		elseif($status_cetak == 0){
			$db->Query("UPDATE KEUANGAN_KHS SET IS_BOLEH_CETAK = 1, TANGGAL_INPUT = sysdate 
						WHERE ID_MHS = '{$id_mhs}' AND ID_SEMESTER = '{$id_semester}'");

			 header('Location: '.$_SERVER['REQUEST_URI']);
		}
	}

	if (post('mode') == 'hapus_set_cetak') {
		$db->Query("UPDATE KEUANGAN_KHS SET IS_BOLEH_CETAK = 0, TANGGAL_INPUT = sysdate 
						WHERE ID_MHS = '{$id_mhs}' AND ID_SEMESTER = '{$id_semester}'");

		 header('Location: '.$_SERVER['REQUEST_URI']);
	}

}


$smarty->display("set-cetak/set-cetak-khs.tpl");