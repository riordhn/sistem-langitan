<?php

include 'config.php';
include 'class/list_data.class.php';
include 'class/history.class.php';
include 'class/utility.class.php';

$id_pt = $id_pt_user;
$id_user = $user->ID_PENGGUNA;

$list = new list_data($db);
$utility = new utility($db);
$history = new history($db);

if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        if (post('mode') == 'generate') {
            for ($i = 1; $i <= post('total_data'); $i++) {
                $id_pembayaran = post('id_pembayaran' . $i);
                $id_tagihan = post('id_tagihan' . $i);
                $spp_perbulan = post('spp_perbulan' . $i);
                $total_terbayar = post('total_terbayar' . $i);
                $nm_semester = post('nm_semester' . $i);
                $total_terbayar_loop = $total_terbayar;
                if (!empty($id_pembayaran)) {
                    $semester_bulan = $history->get_bulan_semester_bayar($nm_semester, $id_tagihan);
                    foreach ($semester_bulan as $sb) {
                        if ($total_terbayar_loop >= $spp_perbulan) {
                            $db->Query("
                            INSERT INTO PEMBAYARAN_PERBULAN (ID_TAGIHAN, ID_PEMBAYARAN, KODE_BULAN, NM_BULAN, BESAR_PEMBAYARAN,CREATED_ON)
                            VALUES 
                            ('{$id_tagihan}','{$id_pembayaran}','{$sb['KODE_BULAN']}','{$sb['NM_BULAN']}','{$spp_perbulan}',CURRENT_TIMESTAMP)
                            ");
                            $total_terbayar_loop = $total_terbayar_loop - $spp_perbulan;
                        }
                    }
                }
            }
        }

        $smarty->assign('data_mahasiswa', $utility->load_data_mahasiswa_pembayaran_spp($id_pt, get('fakultas'), get('prodi')));
    }
}

$smarty->assign('data_prodi', $list->load_list_prodi($id_pt, get('fakultas')));
$smarty->assign('data_fakultas', $list->load_list_fakultas($id_pt));
$smarty->display('utility/update-pembayaran-perbulan-manual.tpl');
