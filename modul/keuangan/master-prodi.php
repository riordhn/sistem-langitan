<?php

include 'config.php';

$data_prodi = array();

$id_pt = $id_pt_user;

$fakultas = $db->QueryToArray("SELECT * FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = {$id_pt} ORDER BY ID_FAKULTAS");
foreach ($fakultas as $f) {
    $prodi = $db->QueryToArray("
        SELECT PS.*,J.NM_JENJANG
        FROM PROGRAM_STUDI PS
        JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
        WHERE PS.ID_FAKULTAS='{$f['ID_FAKULTAS']}'");
    array_push($data_prodi, array_merge($f, array('DATA_PRODI' => $prodi)));
}
$smarty->assign('data_prodi', $data_prodi);
$smarty->display("master/prodi.tpl");
?>
