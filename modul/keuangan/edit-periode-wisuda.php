<?php

include "config.php";

$id_pt = $id_pt_user;

if (isset($_GET)) {
    $nim = get('nim');
    if ($nim != '') {
        if (isset($_POST)) {
            if (post('mode') == 'update') {
                $db->Query("UPDATE PEMBAYARAN_WISUDA SET ID_PERIODE_WISUDA='{$_POST['periode']}' WHERE ID_PEMBAYARAN_WISUDA='{$_POST['id_pem']}'");
            }
        }
        $db->Query("
            SELECT M.NIM_MHS,P.NM_PENGGUNA,J.ID_JENJANG,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,PW.*,B.NM_BANK,BV.NAMA_BANK_VIA
            FROM MAHASISWA M
            JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN PEMBAYARAN_WISUDA PW ON PW.ID_MHS=M.ID_MHS
            LEFT JOIN BANK B ON B.ID_BANK=PW.ID_BANK
            LEFT JOIN BANK_VIA BV ON BV.ID_BANK_VIA=PW.ID_BANK_VIA
            WHERE M.NIM_MHS='{$nim}' AND P.ID_PERGURUAN_TINGGI = {$id_pt}
            ");
        $data_hasil = $db->FetchAssoc();
        $data_periode = $db->QueryToArray("
            SELECT PEW.*,J.NM_JENJANG,S.NM_SEMESTER,S.THN_AKADEMIK_SEMESTER,TW.NM_TARIF_WISUDA 
            FROM AUCC.PERIODE_WISUDA PEW
            JOIN AUCC.JENJANG J ON J.ID_JENJANG=PEW.ID_JENJANG
            JOIN AUCC.SEMESTER S ON S.ID_SEMESTER=PEW.ID_SEMESTER
            JOIN AUCC.TARIF_WISUDA TW ON TW.ID_TARIF_WISUDA=PEW.ID_TARIF_WISUDA
            WHERE PEW.ID_JENJANG='{$data_hasil['ID_JENJANG']}'
            ORDER BY PEW.TGL_BAYAR_MULAI
            ");
        if ($data_hasil != '') {
            $smarty->assign('data_periode', $data_periode);
            $smarty->assign('data', $data_hasil);
        } else {
            $smarty->assign('error', alert_error("Data Mahasiswa/Data Pembayaran Wisuda tidak ditemukan", 40));
        }
    }
}
$smarty->display('utility/edit-periode-wisuda.tpl');
?>
