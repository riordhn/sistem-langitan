<div class="center_title_bar">Laporan Penghasilan Orang Tua Mahasiswa Per Jenjang</div>
<form method="get" id="report_form" action="laporan-penghasilan-ortu-mhs.php">
    <table class="ui-widget" style="width:80%;">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td width="15%">Fakultas</td>
            <td width="25%">
                <select name="fakultas" id="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
            <td>Tahun Angkatan</td>
            <td>
                <select name="angkatan">
                    {$tahun_sekarang=date('Y')}
                    <option value="">Pilih Angkatan</option>
                    {for $t=$tahun_sekarang-4 to $tahun_sekarang}
                        <option value="{$t}" {if $t == $smarty.get.angkatan}selected="selected"{/if}>{$t}</option>
                    {/for}
                </select>
            </td>
        </tr>
        <tr class="center ui-widget-content">
            <td colspan="6">
                <input type="hidden" name="mode" value="laporan" />
                <input type="submit" name="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if isset($data_laporan)}
    <table class="ui-widget-content" style="width: 98%">
        <tr class="ui-widget-header">
            <th colspan="11" class="header-coloumn">
                LAPORAN PENGHASILAN ORTU MAHASISWA PERJENJANG, ANGKATAN
            </th>
        </tr>
        <tr class="ui-widget-header">
            <th rowspan="2">NO</th>
            <th rowspan="2">JENJANG</th>
            <th rowspan="2">JUMLAH</th>
            <th colspan="9">RANGE PENGHASILAN</th>
        </tr>
        <tr class="ui-widget-header">
            <th>DATA PENGHASILAN KOSONG</th>
            <th><100.000</th>
            <th>100.000 - 1.749.000</th>
            <th>1.750.000 - 2.999.000</th>
            <th>3.000.000 - 7.499.000</th>
            <th>7.500.000 - 14.999.000</th>
            <th>15.000.000 - 24.999.000</th>
            <th>>=25.000.000</th>
        </tr>
        {foreach $data_laporan as $d}
            {if $d.NM_JENJANG!=''}
                <tr>
                    <td>{$d@index+1}</td>
                    <td>{$d.NM_JENJANG}</td>
                    <td>{$d.RANGE_1+$d.RANGE_2+$d.RANGE_3+$d.RANGE_4+$d.RANGE_5+$d.RANGE_6+$d.RANGE_7+$d.RANGE_NULL}</td>
                    <td>{$d.RANGE_NULL}</td>
                    <td>{$d.RANGE_7}</td>
                    <td>{$d.RANGE_1}</td>
                    <td>{$d.RANGE_2}</td>
                    <td>{$d.RANGE_3}</td>
                    <td>{$d.RANGE_4}</td>
                    <td>{$d.RANGE_5}</td>
                    <td>{$d.RANGE_6}</td>
                </tr>
            {/if}
        {/foreach}
    </table>
{/if}