<table class="ui-widget" width="96%">
    <tr class="ui-widget-header">
        <th colspan="11">HISTORY BAYAR MAHASISWA {$data_detail_mahasiswa.NIM_MHS}</th>
    </tr>
    <tr class="ui-widget-header">
        <th>No</th>
        <th>Semester</th>
        <th>Lunas</th>
        <th>Total Biaya</th>
        <th>Nama Bank</th>
        <th>Via Bank</th>
        <th>Tanggal Bayar</th>
        <th>No Transaksi</th>
        <th>Keterangan</th>
        <th>Status Bayar</th>
        <th>Semester Bayar</th>
    </tr>
    {foreach $data_pembayaran as $data}
        <tr {if $data.ID_STATUS_PEMBAYARAN==2} style="background-color: #eab8b8"{else} style="background-color: #c9f797"{/if}>
            <td>{$data@index+1}</td>
            <td>{$data.NM_SEMESTER}<br/> ({$data.TAHUN_AJARAN})</td>
            <td>
                {if $data.IS_LUNAS=='1'}
                    Ya
                {else}
                    Belum
                {/if}
            </td>
            <td class="center">{number_format($data.JUMLAH_PEMBAYARAN)}</td>
            <td>{$data.NM_BANK}</td>
            <td>{$data.NAMA_BANK_VIA}</td>
            <td>{$data.TGL_BAYAR}</td>
            <td>{$data.NO_TRANSAKSI}</td>
            <td>{$data.KETERANGAN}</td>
            <td>{$data.NAMA_STATUS}</td>
            <td>{if $data.NM_SEMESTER_BAYAR!=''}{$data.NM_SEMESTER_BAYAR}<br/> ({$data.TAHUN_AJARAN_BAYAR}){/if}</td>
        </tr>
    {/foreach}
</table>