{if $smarty.get.mode==''}
    <div class="center_title_bar">Laporan Pembayaran SPP Tiap Bulan</div>
    <a href="lap-spp-bulanan.php?mode=laporan" class="disable-ajax ui-button ui-corner-all ui-state-hover" target="_blank" style="padding:5px;margin: 3px;cursor:pointer;">Buka Halaman</a>
{else}
    {literal}
        <link rel="stylesheet" type="text/css" href="../../css/jquery-ui-custom-keu.css" />
        <link rel="stylesheet" type="text/css" href="../../css/keuangan-style.css" />
        <script language="javascript" src="../../js/jquery-1.5.1.min.js"></script>
        <script language="javascript" src="../../js/jquery.validate.js"></script>
        <script language="javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
        <style>           
            .ui-widget-content a{
                text-decoration: none;
            }
            .ui-widget-content a:hover{
                color: #f09a14;
            }
        </style>
    {/literal}
    {if $smarty.get.mode=='laporan'||$smarty.get.mode=='laporan-view'}
        <form method="get" action="lap-spp-bulanan.php">
            <table class="ui-widget" style="width:95%;">
                <tr class="ui-widget-header">
                    <th colspan="6" class="header-coloumn">Parameter</th>
                </tr>
                <tr class="ui-widget-content">
                    <td>Fakultas</td>
                    <td>
                        <select name="fakultas" id="fakultas">
                            {foreach $data_fakultas_all as $data}
                                <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                            {/foreach}
                        </select>
                    </td>
                    <td >Bulan</td>
                    <td >
                        <select name="bulan">
                            <option value="{date('m')}" {if $bulan_ini==date('m')} selected="true"{/if}>Bulan Ini</option>
                            <option value="{date('m',strtotime("-1 month"))}" {if $bulan_ini==date('m',strtotime("-1 month"))} selected="true"{/if}>Sebelumnya</option>
                        </select>
                    </td>
                </tr>
                <tr class="center ui-widget-content">
                    <td colspan="6">
                        <input type="hidden" name="mode" value="laporan-view" />
                        <input type="submit" name="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
                    </td>
                </tr>

            </table>
        </form>
    {/if}
    {if $smarty.get.mode=='laporan-view'}
        <table class="ui-widget ui-widget-content" style="width:100%;">
            <tr class="ui-widget-header">
                <th colspan="10" class="header-coloumn">Laporan Pembayaran SPP Bulanan <br/>BULAN {$bulan_ini_name}  <br/>TAHUN AKADEMIK {$tahun_ini}</th>
            </tr>
            <tr class="ui-widget-header">
                <th rowspan="2">Fakultas / Angkatan</th>
                <th rowspan="2">Jumlah Mahasiswa</th>
                <th colspan="3">Pembayaran SPP Bulan Ini</th>
                <th colspan="2">Tunggakan Bulan Lalu</th>
                <th rowspan="2">Jumlah Sudah Masuk</th>
                <th rowspan="2">Jumlah Belum Masuk</th>
                <th>Tunggakan Tahun Lalu </th>
            </tr>
            <tr class="ui-widget-header">
                <th>Target 100%</th>
                <th>Masuk</th>
                <th>Belum Masuk</th>
                <th>Masuk</th>
                <th>Belum Masuk</th>
                <th>Masuk</th>
            </tr>
            {foreach $data_row as $f}
                <tr style="background-color:#d1ebff">
                    <td colspan="10" ><b>{$f['NM_JENJANG']} {$f['NM_PROGRAM_STUDI']}</b></td>
                </tr>
                {$total_mhs=0}
                {$total_skrg_target=0}
                {$total_skrg_masuk=0}
                {$total_skrg_belum=0}
                {$total_lalu_masuk=0}
                {$total_lalu_belum=0}
                {$total_masuk=0}
                {$total_belum=0}
                {$total_tahun_lalu_masuk=0}
                {$total_tahun_lalu_belum=0}
                {$index_row=0}
                    {foreach $report_row_fakultas[$f@index]['row'] as $r}
                        {$belum_skrg=$report_row_fakultas[$f@index]['value'][$index_row]['TARGET']-$report_row_fakultas[$f@index]['value'][$index_row]['TERBAYAR_SEKARANG']}
                        {$belum_lalu=$report_row_fakultas[$f@index]['value'][$index_row]['TARGET']-$report_row_fakultas[$f@index]['value'][$index_row]['TERBAYAR_KEMARIN']}
                        {$total_masuk_row=$report_row_fakultas[$f@index]['value'][$index_row]['TERBAYAR_SEKARANG']+$report_row_fakultas[$f@index]['value'][$index_row]['TERBAYAR_KEMARIN']}
                        {$total_belum_row=$belum_skrg+$belum_lalu}
                        {$tahun_lalu_masuk=$report_row_fakultas[$f@index]['value'][$index_row]['TERBAYAR_THN_KEMARIN']}
                        {$tahun_lalu_belum=$report_row_fakultas[$f@index]['value'][$index_row]['TAGIHAN_THN_KEMARIN']-$report_row_fakultas[$f@index]['value'][$index_row]['TERBAYAR_THN_KEMARIN']}
                        <tr>
                            <td>{$r['THN_ANGKATAN_MHS']}</td>
                            <td style="text-align:center">{$r['JUMLAH_MHS']}</td>
                            <td style="text-align:right">{number_format($report_row_fakultas[$f@index]['value'][$index_row]['TARGET'])}</td>
                            <td style="text-align:right">{number_format($report_row_fakultas[$f@index]['value'][$index_row]['TERBAYAR_SEKARANG'])}</td>
                            <td style="text-align:right">{number_format($belum_skrg)}</td>
                            <td style="text-align:right">{number_format($report_row_fakultas[$f@index]['value'][$index_row]['TERBAYAR_KEMARIN'])}</td>
                            <td style="text-align:right">{if $belum_lalu<0} <font style="color:red">{number_format($belum_lalu)}</font> {else} {number_format($belum_lalu)}{/if}</td>
                            <td style="text-align:right">{number_format($total_masuk_row)}</td>
                            <td style="text-align:right">{number_format($total_belum_row)}</td>
                            <td style="text-align:right">{number_format($tahun_lalu_masuk)}</td>
                        </tr>
                        {$total_mhs=$total_mhs+$r['JUMLAH_MHS']}
                        {$total_skrg_target=$total_skrg_target+$report_row_fakultas[$f@index]['value'][$index_row]['TARGET']}
                        {$total_skrg_masuk=$total_skrg_masuk+$report_row_fakultas[$f@index]['value'][$index_row]['TERBAYAR_SEKARANG']}
                        {$total_skrg_belum=$total_skrg_belum+$belum_skrg}
                        {$total_lalu_masuk=$total_lalu_masuk+$report_row_fakultas[$f@index]['value'][$index_row]['TERBAYAR_KEMARIN']}
                        {$total_lalu_belum=$total_lalu_belum+$belum_lalu}
                        {$total_masuk=$total_masuk+$total_masuk_row}
                        {$total_belum=$total_belum+$total_belum_row}
                        {$total_tahun_lalu_masuk=$total_tahun_lalu_masuk+$tahun_lalu_masuk}
                        {$total_tahun_lalu_belum=$total_tahun_lalu_belum+$tahun_lalu_belum}
                        {$index_row=$index_row+1}
                    {/foreach}
                <tr class="total">
                    <td ><b>Subtotal</b></td>
                    <td style="text-align:center">{number_format($total_mhs)}</td>
                    <td style="text-align:right">{number_format($total_skrg_target)}</td>
                    <td style="text-align:right">{number_format($total_skrg_masuk)}</td>
                    <td style="text-align:right">{number_format($total_skrg_belum)}</td>
                    <td style="text-align:right">{number_format($total_lalu_masuk)}</td>
                    <td style="text-align:right">{number_format($total_lalu_belum)}</td>
                    <td style="text-align:right">{number_format($total_masuk)}</td>
                    <td style="text-align:right">{number_format($total_belum)}</td>
                    <td style="text-align:right">{number_format($total_tahun_lalu_masuk)}</td>
                </tr>
            {/foreach}
        </table>
   {/if}
{/if}

