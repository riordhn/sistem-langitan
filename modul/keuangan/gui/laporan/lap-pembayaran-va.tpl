<div class="center_title_bar">Laporan Pembayaran Mahasiswa</div> 
<div id="alert" class="ui-widget" style="display:none;margin-left:15px;">
    <div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;"> 
        <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
            Tanggal Harus Diisi.</p>
    </div>
</div>
<form method="get" id="report_form" action="lap-pembayaran-va.php">
    <table class="ui-widget" style="width:98%;margin:5px">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td width="15%">Tanggal Awal</td>
            <td width="15%"><input type="text" id="tgl_awal" class="datepicker"  name="tgl_awal" {if isset($tgl_awal)} value="{$tgl_awal}"{/if} /></td>
            <td width="15%">Tanggal Akhir</td>
            <td width="15%"><input type="text" id="tgl_akhir" class="datepicker" class="required" name="tgl_akhir" {if isset($tgl_awal)} value="{$tgl_akhir}"{/if}/></td>
        </tr>
        <tr class="center ui-widget-content">
            <td colspan="4">
                <input type="hidden" name="mode" value="bank" />
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if isset($data_report)}
    <table id="tablePage" class="ui-widget" style="width:98%">
        <thead>
            <tr class="ui-widget-header">
                <th colspan="9">

                    Rekapitulasi Pembayaran Virtual Account
                    Pada Tanggal {$tgl_awal} sampai {$tgl_akhir}
                </th>
            </tr>
            <tr class="ui-widget-header">
                <th>No</th>
                <th>VIRTUAL ACCOUNT</th>
                <th>NAMA</th>
                <th>JUMLAH TAGIHAN</th>
                <th>JUMLAH PEMBAYARAN</th>
                <th>KODE TRANSAKSI</th>
                <th>WAKTU BAYAR</th>
                <th>AKSI</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_report as $data}
                <tr class="center ui-widget-content">
                    <td>{($data@index+1)}</td>
                    <td>{$data.VIRTUAL_ACCOUNT}</td>
                    <td>
                        {if $data.NIM_MHS!=''} ({$data.NIM_MHS}) <br/>{/if}
                        {$data.CUSTOMER_NAME}
                    </td>
                    <td>{number_format($data.TRX_AMOUNT)}</td>
                    <td>{number_format($data.PAYMENT_AMOUNT)}</td>
                    <td>{$data.PAYMENT_NTB}</td>
                    <td width="70px">{$data.PAYMENT_TIME}</td>
                    <td>
                        {if $data.PEMBAYARAN_DIPROSES<$data.PAYMENT_AMOUNT}
                            <a target="_blank" href="detail-pembayaran-va.php?cari={$data.NIM_MHS}&no_va={$data.VIRTUAL_ACCOUNT}&trx_id={$data.TRX_ID}" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;">Proses</a>
                        {else}
                            <b style="color:green">SUDAH DI PROSES</b>
                        {/if}
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
{/if}

{literal}
    <script>
        $('#report_form').submit(function() {
            if ($('#tgl_awal').val() == '' || $('#tgl_akhir').val() == '') {
                $('#alert').fadeIn();
                return false;
            }
        });
        $('#report_form').validate();
        $(".datepicker").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true,
            changeYear: true});
    </script>
{/literal}