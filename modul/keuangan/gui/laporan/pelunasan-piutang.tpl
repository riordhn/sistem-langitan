<div class="center_title_bar">Laporan Daftar Pelunasan Piutang</div> 
<form method="get" id="report_form" action="pelunasan-piutang.php">
    <table class="ui-widget" style="width:90%;">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td width="15%">Fakultas</td>
            <td width="25%">
                <select name="fakultas" id="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
            <td  width="15%">Program Studi</td>
            <td width="45%">
                <select name="prodi" id="prodi">
                    <option value="">Semua</option>
                    {foreach $data_prodi as $data}
                        <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$smarty.get.prodi}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                </select>
            </td>

        </tr>
        <tr class="ui-widget-content">
            <td>Status Mahasiswa</td>
            <td>
                <select name="status">
                    <option value="">Semua</option>
                    {foreach $data_status as $data}
                        <option value="{$data.ID_STATUS_PENGGUNA}" {if $data.ID_STATUS_PENGGUNA==$smarty.get.status}selected="true"{/if}>{$data.NM_STATUS_PENGGUNA}</option>
                    {/foreach}
                </select>
            </td>
            <td>Semester Pelunasan</td>
            <td>
                <select name="semester_pelunasan">
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER==$smarty.get.semester_pelunasan}selected="true"{/if}>{$data.NM_SEMESTER} ( {$data.TAHUN_AJARAN} )</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="center ui-widget-content">
            <td colspan="6">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" name="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{$informasi_data_input}
{if isset($data_pelunasan_piutang)}
    <form method="post" action="pelunasan-piutang.php?{$smarty.server.QUERY_STRING}">
        <table class="ui-widget-content" style="width: 90%">
            <tr class="ui-widget-header">
                <th colspan="9" class="header-coloumn">Daftar Pelunasan Piutang</th>
            </tr>
            <tr class="ui-widget-header">
                <th>No</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Progrm Studi</th>
                <th>Status</th>
                <th>Semster Piutang</th>
                <th>SemesterBayar</th>
                <th>Besar Piutang</th>
                <th>Detail</th>
            </tr>
            {foreach $data_pelunasan_piutang as $data}
                <tr>
                    <td>{$data@index+1}</td>
                    <td>{$data.NIM_MHS}</td>
                    <td>{$data.NM_PENGGUNA}</td>
                    <td>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</td>
                    <td>{$data.NM_STATUS_PENGGUNA}</td>
                    <td>{$data.SEMESTER_PIUTANG}</td>
                    <td>{$data.SEMESTER_BAYAR}</td>
                    <td>{number_format($data.PEMBAYARAN)}</td>
                    <td class="center">
                        <span class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="$('#history-bayar-piutang').dialog('open').load('load-history-bayar-piutang.php?cari='+'{$data.NIM_MHS}');return false;">Detail</span>
                    </td>
                </tr>
            {foreachelse}
                <tr class="ui-widget-content">
                    <td colspan="9" class="center"><span style="color: red">Data Kosong</span></td>
                </tr>                
            {/foreach}
        </table>
    </form>
{/if}
<div id="history-bayar-piutang" title="History Bayar Piutang"></div>
{literal}
    <script>
            
            $('#fakultas').change(function(){
                    $.ajax({
                            type:'post',
                            url:'getProdi.php',
                            data:'id_fakultas='+$(this).val(),
                            success:function(data){
                                    $('#prodi').html(data);
                            }                    
                    })
            });
            $( "#history-bayar-piutang" ).dialog({
                width:'900',
                modal: true,
                resizable:false,
                autoOpen:false
            });
            $('#check_all').click(function(){
                if($(this).is(':checked')){
                    $('.pilih_mhs_piutang').attr('checked', true);
                }else{
                    $('.pilih_mhs_piutang').attr('checked', false);
                }
            })
    </script>
{/literal}