<div class="center_title_bar">Laporan Pembelian Formulir</div> 
<div id="alert" class="ui-widget" style="display:none;margin-left:15px;">
    <div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;"> 
        <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
            Tanggal Harus Diisi.</p>
    </div>
</div>
<form method="get" id="report_form" action="pembelian-formulir.php">
    <table class="ui-widget" style="width:90%">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Tanggal Awal</td>
            <td><input type="text" id="tgl_awal" class="datepicker"  name="tgl_awal" {if isset($tgl_awal)} value="{$tgl_awal}"{/if} /></td>
            <td>Tanggal Akhir</td>
            <td><input type="text" id="tgl_akhir" class="datepicker" class="required" name="tgl_akhir" {if isset($tgl_awal)} value="{$tgl_akhir}"{/if}/></td>
        </tr>
        <tr class="center ui-widget-content">
            <td colspan="4">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if isset($data_pembelian_formulir)}
    {foreach $data_pembelian_formulir as $data}
        {$total_tersedia=0}
        {$total_beli=0}
        {$total_bayar=0}
        <table class="ui-widget-content" style="width: 70%">
            <caption>KODE JURUSAN : 
                {if $data.KODE=='01'}
                    IPA/Reguler
                {elseif $data.KODE=='02'}
                    IPS
                {else}
                    IPC
                {/if}
            </caption>
            <tr class="ui-widget-header">
                <th class="header-coloumn" colspan="8">LAPORAN PEMBELIAN FORMULIR</th>
            </tr>
            <tr class="ui-widget-header">
                <th>NO</th>
                <th>Gelombang</th>
                <th>Jenjang</th>
                <th>Penerimaan</th>
                <th>Tarif</th>
                <th>Formulir Tersedia</th>
                <th>Formulir Di Beli</th>
                <th>Total Pembayaran</th>
            </tr>
            {foreach $data.DATA_REKAP as $rekap}
                {if $rekap.PEMBELIAN!=0}
                    {$total_tersedia=$total_tersedia+$rekap.AKTIF}
                {/if}
                {$total_beli=$total_beli+$rekap.PEMBELIAN}
                {$total_bayar=$total_bayar+$rekap.PENERIMAAN}
                <tr>
                    <td>{$rekap@index+1}</td>
                    <td>{$rekap.GELOMBANG}</td>
                    <th>{$rekap.NM_JENJANG}</th>
                    <td>{$rekap.NM_PENERIMAAN}</td>
                    <td>{number_format($rekap.TARIF)}</td>
                    <td>{$rekap.AKTIF}</td>
                    <td>{$rekap.PEMBELIAN}</td>
                    <td>{number_format($rekap.PENERIMAAN)}</td>
                </tr>
            {/foreach}
            <tr class="total">
                <td colspan="5"></td>
                <td>{$total_tersedia}</td>
                <td>{$total_beli}</td>
                <td>{number_format($total_bayar)}</td>
            </tr>
        </table>
        <p></p>
    {/foreach}
{/if}
{literal}
    <script>
            $('#report_form').submit(function() {
                    if($('#tgl_awal').val()==''||$('#tgl_akhir').val()==''){
                            $('#alert').fadeIn();
                    return false;
                    }  
            });
            $('#report_form').validate();
            $( ".datepicker" ).datepicker({dateFormat:'yy-mm-dd',changeMonth: true,
                        changeYear: true});
            $('#fakultas').change(function(){
                    $.ajax({
                            type:'post',
                            url:'getProdi.php',
                            data:'id_fakultas='+$(this).val(),
                            success:function(data){
                                    $('#prodi').html(data);
                            }                    
                    })
            });
    </script>
{/literal}