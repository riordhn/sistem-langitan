<table border="1" style="width: 200%">
    <thead>
        <tr>
            <th>Program Studi</th>
            <th>Angkatan</th>
            <th>Jalur</th>
            <th>Jumlah Mahasiswa (Kuitansi)</th>
            {foreach $column_data_set as $column_data}
            <th>{$column_data.NM_BIAYA}</th>
            {/foreach}
            <th>Total Pembayaran</th>
        </tr>
    </thead>
    <tbody>
        {$total_jumlah_mahasiswa = 0}
        {$total_pembayaran = 0}
        {foreach $class_data_set as $class_data}
            <tr>
                <td>{$class_data.NM_JENJANG} {$class_data.NM_PROGRAM_STUDI}</td>
                <td>{$class_data.THN_ANGKATAN_MHS}</td>
                <td>{$class_data.NM_JALUR}</td>
                <td>{$class_data.JUMLAH_MAHASISWA}</td>
                {foreach $column_data_set as $column_data}
                    <td>
                        {foreach $data_set as $data}
                            {if $data.ID_PROGRAM_STUDI == $class_data.ID_PROGRAM_STUDI and
                                $data.THN_ANGKATAN_MHS == $class_data.THN_ANGKATAN_MHS and
                                $data.ID_JALUR == $class_data.ID_JALUR and
                                $data.ID_BIAYA == $column_data.ID_BIAYA}
                                <!-- Hasil Match Class + Column ditampilkan -->
                                {$data.TOTAL_PEMBAYARAN}
                            {/if}
                        {/foreach}
                    </th>
                {/foreach}
                <td>{$class_data.TOTAL_PEMBAYARAN}</td>
            </tr>
            {$total_jumlah_mahasiswa = $total_jumlah_mahasiswa + $class_data.JUMLAH_MAHASISWA}
            {$total_pembayaran = $total_pembayaran + $class_data.TOTAL_PEMBAYARAN}
        {/foreach}
    </tbody>
    <tfoot>
        <tr>
            <td colspan="3"></td>
            <td>{$total_jumlah_mahasiswa}</td>
            {foreach $column_data_set as $column_data}
                <td>{$column_data.TOTAL_PEMBAYARAN}</td>
            {/foreach}
            <td>{$total_pembayaran}</td>
        </tr>
    </tfoot>
</table>