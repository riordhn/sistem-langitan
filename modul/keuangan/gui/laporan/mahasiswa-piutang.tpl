<div class="center_title_bar">Laporan Daftar Mahasiswa Piutang</div> 
<form method="get" id="report_form" action="mahasiswa-piutang.php">
    <table class="ui-widget" style="width:90%;">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td width="15%">Fakultas</td>
            <td width="25%">
                <select name="fakultas" id="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
            <td  width="15%">Program Studi</td>
            <td width="45%">
                <select name="prodi" id="prodi">
                    <option value="">Semua</option>
                    {foreach $data_prodi as $data}
                        <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$smarty.get.prodi}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                </select>
            </td>

        </tr>
        <tr class="ui-widget-content">
            <td>Status Mahasiswa</td>
            <td>
                <select multiple="true" size="8"  name="status[]">
                    {foreach $data_status as $data}
                        <option value="{$data.ID_STATUS_PENGGUNA}" {if $data.ID_STATUS_PENGGUNA==$smarty.get.status}selected="true"{/if}>{$data.NM_STATUS_PENGGUNA}</option>
                    {/foreach}
                </select>
            </td>
            <td>Jumlah Tagihan</td>
            <td>
                <select name="tagihan">
                    <option value="" {if $smarty.get.tagihan==''}selected="true"{/if}>Semua</option>
                    <option value="1" {if $smarty.get.tagihan==1}selected="true"{/if}>1</option>
                    <option value="2" {if $smarty.get.tagihan==2}selected="true"{/if}>2</option>
                    <option value="3" {if $smarty.get.tagihan==3}selected="true"{/if}>Lebih Dari 2</option>
                </select>
            </td>
        </tr>
        <tr class="center ui-widget-content">
            <td colspan="6">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" name="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{$informasi_data_input}
{if isset($data_mhs_piutang)}
    <form method="post" id="form_cut_off" action="mahasiswa-piutang.php?{$smarty.server.QUERY_STRING}">
        <table class="ui-widget-content" style="width: 90%">
            <tr class="ui-widget-header">
                <th colspan="12" class="header-coloumn">Daftar Mahasiswa Piutang</th>
            </tr>
            <tr class="ui-widget-header">
                <th>No</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Progrm Studi</th>
                <th>Fakultas</th>
                <th>Status</th>
                <th>Perubahan Status</th>
                <th>Jumlah Piutang Semester</th>
                <th>Besar Piutang</th>
                <th>Prediksi</th>
                <th>
                    Status Piutang Semua
                    <br/><br/>
                    <select style="font-size: 0.9em" id="select_all">
                        <option value="1">Ya</option>
                        <option value="0">Bukan</option>
                    </select>
                </th>
                <th>Detail</th>
            </tr>
            {foreach $data_mhs_piutang as $data}
                <tr>
                    <td>{$data@index+1}</td>
                    <td>{$data.NIM}</td>
                    <td>{$data.NAMA}</td>
                    <td>({$data.JENJANG}) {$data.PRODI}</td>
                    <td>{$data.FAKULTAS|upper}</td>
                    <td>{$data.STATUS}</td>
                    <td>
                        {if $data.PERUBAHAN_STATUS==1}
                            Tetap
                        {else if $data.PERUBAHAN_STATUS==2}
                            Berubah Aktif
                        {else if $data.PERUBAHAN_STATUS==3}
                            Berubah Tidak Aktif
                        {/if}
                    </td>
                    <td class="center">{$data.BANYAK_TAGIHAN}</td>
                    <td>{number_format($data.BESAR_PIUTANG)}</td>
                    <td class="center">
                        {if $data.KLASIFIKASI.y > $data.KLASIFIKASI.t}
                            Ya
                        {else}
                            Tidak
                        {/if}<br/>
                        <span class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="$('#detail-klasifikasi').dialog('open').load('detail-klasifikasi.php?cari={$data.NIM}&s={$data.PERUBAHAN_STATUS}&j={$data.JENJANG}&t={$data.BANYAK_TAGIHAN}');return false;">
                            Detail
                        </span>
                    </td>
                    <td class="center">
                        {if count($data.DATA_PIUTANG)>0}
                            <span style="font-size: 0.8em;font-weight: bold">Sudah Ada<br/>
                                {foreach $data.DATA_PIUTANG as $dp}
                                    {$dp@index+1}. {$dp.TGL_CUT_OFF} Status 
                                    {if $dp.STATUS_PIUTANG==0}
                                        Bukan
                                    {else}
                                        Ya
                                    {/if}<br/>
                                {/foreach}
                            </span>
                        {/if}
                        <select class="status_piutang" name="status{$data@index+1}">
                            {if count($data.DATA_PIUTANG)>0}
                                <option value="x">Tidak Disimpan</option>
                            {/if}
                            <option value="1">Ya</option>
                            <option value="0">Bukan</option>
                        </select>
                    </td>
                    <td class="center">
                        <span class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="$('#history-bayar-piutang').dialog('open').load('load-history-bayar-piutang.php?cari='+'{$data.NIM}');return false;">
                            Detail
                        </span>
                        <input type="hidden" name="id_mhs{$data@index+1}" value="{$data.ID_MHS}"/>
                        <input type="hidden" name="nama{$data@index+1}" value="{$data.NAMA}"/>
                        <input type="hidden" name="nim{$data@index+1}" value="{$data.NIM}"/>
                        <input type="hidden" name="prodi{$data@index+1}" value="{$data.PRODI}"/>
                        <input type="hidden" name="jenjang{$data@index+1}" value="{$data.JENJANG}"/>
                        <input type="hidden" name="fakultas{$data@index+1}" value="{$data.FAKULTAS}"/>
                        <input type="hidden" name="banyak{$data@index+1}" value="{$data.BANYAK_TAGIHAN}"/>
                        <input type="hidden" name="status_a{$data@index+1}" value="{$data.STATUS}"/>
                        <input type="hidden" name="besar{$data@index+1}" value="{$data.BESAR_PIUTANG}"/>
                        <input type="hidden" name="perubahan{$data@index+1}" value="{$data.PERUBAHAN_STATUS}"/>
                    </td>
                </tr>
            {foreachelse}
                <tr class="ui-widget-content">
                    <td colspan="12" class="center"><span style="color: red">Data Kosong</span></td>
                </tr>                
            {/foreach}
            <tr class="ui-widget-content">
                <td colspan="12" class="center">
                    <b>TANGGAL CUT OFF</b>: <input type="text" class="date_pick required" name="tgl_cut_off" size="10" />
                    <br/>
                    <br/>
                    <input type="submit" value="Masukkan Data Piutang" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;"/>
                    <input type="hidden" name="total_data" value="{$data@index+1}"/>
                    <input type="hidden" name="mode" value="input_data"/>
                </td>
            </tr>
        </table>
    </form>
{/if}
<div id="history-bayar-piutang" title="History Bayar Piutang"></div>
<div id="detail-klasifikasi" title="Detail Klasifikasi"></div>
{literal}
    <script>
            
            $('#fakultas').change(function(){
                $.ajax({
                    type:'post',
                    url:'getProdi.php',
                    data:'id_fakultas='+$(this).val(),
                    success:function(data){
                            $('#prodi').html(data);
                    }                    
                })
            });
            $( ".date_pick" ).datepicker({
                dateFormat:'dd-M-y',changeMonth: true,
                changeYear: true
            });
            $( "#history-bayar-piutang, #detail-klasifikasi" ).dialog({
                width:'900',
                modal: true,
                resizable:false,
                autoOpen:false
            });
            $('#select_all').change(function(){
                if($(this).val()==1){
                    $('.status_piutang').attr('value', 1);
                }else{
                    $('.status_piutang').attr('value', 0);
                }
            });
            $('#form_cut_off').validate();
    </script>
{/literal}