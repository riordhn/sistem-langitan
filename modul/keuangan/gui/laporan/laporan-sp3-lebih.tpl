<div class="center_title_bar">Laporan SP3 Lebih Mahasiswa Baru</div> 
<form method="get" id="report_form" action="laporan-sp3-lebih.php">
    <table class="ui-widget" style="width:90%">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td width="15%">Fakultas</td>
            <td width="25%">
                <select name="fakultas" id="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
            <td width="15%">Program Studi</td>
            <td>
                <select name="prodi" id="prodi">
                    <option value="">Semua</option>
                    {foreach $data_prodi as $data}
                        <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$smarty.get.prodi}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Semester</td>
            <td>
                <select name="semester">
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER==$smarty.get.semester}selected="true"{/if}>{$data.NM_SEMESTER} ( {$data.TAHUN_AJARAN} )</option>
                    {/foreach}
                </select>
            </td>
            <td></td>
            <td>
            </td>
        </tr>
        <tr class="center ui-widget-content">
            <td colspan="4">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if isset($data_sp3_lebih)}
    <table class="ui-widget-content" style="width: 95%;">
        <tr class="ui-widget-header">
            <th colspan="12" class="header-coloumn">LAPORAN PEMBAYARAN SP3 Lebih</th>
        </tr>
        <tr class="ui-widget-header">
            <th>NO</th>
            <th>NO UJIAN</th>
            <th>NAMA</th>
            <th>PRODI</th>
            <th>JALUR</th>
            <th>KELOMPOK BIAYA</th>
            <th>SEMESTER</th>
            <th>BESAR SP3</th>
            <th>KELEBIHAN SP3</th>
            <th>STATUS PEMBAYARAN</th>
            <th>TOTAL BIAYA</th>
            <th>DETAIL</th>
        </tr>
        {$total_sudah_bayar=0}
        {$total_belum_bayar=0}
        {foreach $data_sp3_lebih as $data}
            {$sp3_lebih=$data.SP3_BAYAR-$data.BESAR_BIAYA}
            {if $sp3_lebih==0}
                {$sp3_lebih=$data.SP3_SUKARELA}
            {/if}
            <tr>
                <td>{$data@index+1}</td>
                <td>{$data.NO_UJIAN}</td>
                <td>{$data.NM_C_MHS}</td>
                <td>
                    ({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI} <br/> Fakultas {$data.NM_FAKULTAS|upper}
                </td>
                <td>{$data.NM_JALUR}</td>
                <td>{$data.NM_KELOMPOK_BIAYA}</td>
                <td>{$data.NM_SEMESTER} {$data.TAHUN_AJARAN}</td>
                <td class="center" style="width: 120px">
                    sp3 Usulan : <br/>{number_format($data.SP3_1)}<br/>
                    sp3 Master : <br/>{number_format($data.BESAR_BIAYA)}<br/>
                    sp3 Pembayaran : <br/>{number_format($data.SP3_BAYAR)}
                </td>
                <td>
                    {number_format($sp3_lebih)}
                </td>
                <td class="center" style="width: 100px">
                    {foreach $data.STATUS_BAYAR as $pembayaran}
                        {$total_sudah_bayar=$total_sudah_bayar+$sp3_lebih}
                        Pembayaran Ke-{$pembayaran@index+1}<br/>
                        Tgl Bayar : {$pembayaran.TGL_BAYAR}<br/>
                        Bank : {$pembayaran.NM_BANK}<br/>
                        Total Bayar : {number_format($pembayaran.TOTAL_BAYAR)}
                        <br/><br/>
                    {foreachelse}
                        {$total_belum_bayar=$total_belum_bayar+$sp3_lebih}
                        Belum Bayar
                    {/foreach}
                </td>
                <td class="center" style="width: 120px">
                    Total Bayar : <br/>{number_format($data.TOTAL_PEMBAYARAN)}<br/>
                    Total Biaya : <br/>{number_format($data.TOTAL_BIAYA)}
                </td>
                <td>
                    <a href="history-bayar.php?cari={$data.NO_UJIAN}" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;">Detail</a>
                </td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="12" class="total center">
                Kelebihan SP3 Sudah Bayar : {number_format($total_sudah_bayar)}<br/>
                Kelebihan SP3 Belum Bayar : {number_format($total_belum_bayar)}
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td class="link center" colspan="12">
                <span class="link_button ui-corner-all" onclick="window.location.href='excel-sp3-lebih.php?{$smarty.server.QUERY_STRING}'" style="padding:5px;cursor:pointer;">Excel</span>
            </td>	
        </tr>
    </table>
{/if}
{literal}
    <script>
            $('#fakultas').change(function(){
                    $.ajax({
                            type:'post',
                            url:'getProdi.php',
                            data:'id_fakultas='+$(this).val(),
                            success:function(data){
                                    $('#prodi').html(data);
                            }                    
                    })
            });
    </script>
{/literal}