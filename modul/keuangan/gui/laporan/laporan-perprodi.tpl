{if $smarty.get.mode==''}
    <div class="center_title_bar">Laporan Pembayaran Per Prodi Dalam 1 tahun</div>
    <a href="laporan-perprodi.php?mode=form" class="disable-ajax ui-button ui-corner-all ui-state-hover" target="_blank" style="padding:5px;margin: 3px;cursor:pointer;">Buka Halaman</a>
{else}
    {literal}
        <link rel="stylesheet" type="text/css" href="../../css/jquery-ui-custom-keu.css" />
        <link rel="stylesheet" type="text/css" href="../../css/keuangan-style.css" />
        <script language="javascript" src="../../js/jquery-1.5.1.min.js"></script>
        <script language="javascript" src="../../js/jquery.validate.js"></script>
        <script language="javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
        <style>           
            .ui-widget-content a{
                text-decoration: none;
            }
            .ui-widget-content a:hover{
                color: #f09a14;
            }
        </style>
    {/literal}
    <form method="get" id="report_form" action="laporan-perprodi.php">
        <table class="ui-widget" style="width:90%;">
            <tr class="ui-widget-header">
                <th colspan="8" class="header-coloumn">Parameter</th>
            </tr>
            <tr class="ui-widget-content">
                <td width="15%">Fakultas</td>
                <td width="25%">
                    <select name="fakultas" id="fakultas">
                        <option value="">Semua</option>
                        {foreach $data_fakultas as $data}
                            <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                        {/foreach}
                    </select>
                </td>
                <td>Tanggal Awal</td>
                <td>
                    <input type="text" id="tgl_awal" class="datepicker" class="required"  name="tgl_awal" {if isset($tgl_awal)} value="{$tgl_awal}"{/if} />
                </td>
                <td>Tanggal Akhir</td>
                <td>
                    <input type="text" id="tgl_akhir" class="datepicker" class="required" name="tgl_akhir" {if isset($tgl_awal)} value="{$tgl_akhir}"{/if}/>
                </td>
                <td>Jenis Tampilan</td>
                <td>
                    <select name="tampil">
                        <option value="0" {if $smarty.get.tampil==0}selected="true"{/if}>Fakultas</option>
                        <option value="1" {if $smarty.get.tampil==1}selected="true"{/if}>Prodi</option>
                        <option value="2" {if $smarty.get.tampil==2}selected="true"{/if}>Detail Lengkap</option>
                    </select>
                </td>
            </tr>
            <tr class="center ui-widget-content">
                <td colspan="8">
                    <input type="hidden" name="mode" value="laporan" />
                    <input type="submit" name="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
                </td>
            </tr>
        </table>
    </form>
    {literal}
        <script>
            $('fomr').validate();
            $(".datepicker").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true
            });

        </script>
    {/literal}
    {if $smarty.get.mode=='laporan'}
        {if isset($data_laporan)}
            <table class="ui-widget">
                <tr class="ui-widget-header">
                    <th colspan="
                        {if $smarty.get.tampil==2}
                            {count($semester)*10+5+5}
                        {else}
                            {count($semester)*10+2+5}
                        {/if}
                        " class="header-coloumn">Laporan Pengelompokan Biaya Calon Mahasiswa</th>
                </tr>
                <tr class="ui-widget-header">
                    {if $smarty.get.tampil=='0'}
                        <th rowspan="4">NO</th>
                        <th rowspan="4">FAKULTAS </th>
                    {else if $smarty.get.tampil=='1'}
                        <th rowspan="4">NO</th>
                        <th rowspan="4">PRODI </th>
                    {else if $smarty.get.tampil=='2'}
                        <th rowspan="4">ANGKATAN</th>
                        <th rowspan="4">JALUR</th>
                        <th rowspan="4">KELOMPOK_BIAYA</th>
                    {/if}
                </tr>
                <tr class="ui-widget-header">
                    <th colspan="10">Pembayaran Sebelum {$semester_sebelum_aktif.NM_SEMESTER} {$semester_sebelum_aktif.TAHUN_AJARAN}</th>
                    <th colspan="10">Pembayaran {$semester_sebelum_aktif.NM_SEMESTER} {$semester_sebelum_aktif.TAHUN_AJARAN}</th>
                    <th colspan="10">Pembayaran {$semester_aktif.NM_SEMESTER} {$semester_aktif.TAHUN_AJARAN}</th>
                    <th colspan="10">Pembayaran Setelah {$semester_aktif.NM_SEMESTER} {$semester_aktif.TAHUN_AJARAN}</th>
                    <th colspan="5" rowspan="2">TOTAL</th>
                </tr>
                <tr class="ui-widget-header">
                    {foreach $semester as $s}
                        <th colspan="5">Mala</th>
                        <th colspan="5">Maba</th>
                    {/foreach}
                </tr>
                <tr class="ui-widget-header">
                    {foreach $semester as $s}
                        <th>Jumlah Mhs</th>
                        <th>SOP</th>
                        <th>Praktikum</th>
                        <th>SP3</th>
                        <th>Sukarela</th>
                        <th>Jumlah Mhs</th>
                        <th>SOP</th>
                        <th>Praktikum</th>
                        <th>SP3</th>
                        <th>Sukarela</th>
                    {/foreach}
                    <th>Jumlah Mhs</th>
                    <th>SOP</th>
                    <th>Praktikum</th>
                    <th>SP3</th>
                    <th>Sukarela</th>
                </tr>
                {foreach $data_laporan as $d}
                    {$total_mhs=0}
                    {$total_sop=0}
                    {$total_praktikum=0}
                    {$total_sp3=0}
                    {$total_sukarela=0}
                    <tr class="ui-widget-content">
                        <td>{$d@index+1}</td>
                        {if $smarty.get.tampil=='0'}
                            <td>{$d.NM_FAKULTAS} </td>
                        {else}
                            <td>{$d.NM_JENJANG} {$d.NM_PROGRAM_STUDI}  </td>
                        {/if}
                        {if $smarty.get.tampil==2}
                            <td> {$d.THN_ANGKATAN_MHS} </td>
                            <td> {$d.NM_JALUR} </td>
                            <td> {$d.NM_KELOMPOK_BIAYA}</td>
                        {/if}
                        {foreach $d.BAYAR as $l}
                            {if empty($l.JUMLAH_MHS)}
                                {$jumlah_mhs=0}
                            {else}
                                {$jumlah_mhs=$l.JUMLAH_MHS}
                            {/if}
                            {if empty($l.SOP)}
                                {$sop=0}
                            {else}
                                {$sop=$l.SOP}
                            {/if}
                            {if empty($l.PRAKTIKUM)}
                                {$praktikum=0}
                            {else}
                                {$praktikum=$l.PRAKTIKUM}
                            {/if}
                            {if empty($l.SP3)}
                                {$sp3=0}
                            {else}
                                {$sp3=$l.SP3}
                            {/if}
                            {if empty($l.SUKARELA)}
                                {$sukarela=0}
                            {else}
                                {$sukarela=$l.SUKARELA}
                            {/if}
                            {if empty($l.JUMLAH_CMHS)}
                                {$jumlah_cmhs=0}
                            {else}
                                {$jumlah_cmhs=$l.JUMLAH_CMHS}
                            {/if}
                            {if empty($l.SOP_MABA)}
                                {$sop_maba=0}
                            {else}
                                {$sop_maba=$l.SOP_MABA}
                            {/if}
                            {if empty($l.PRAKTIKUM_MABA)}
                                {$praktikum_maba=0}
                            {else}
                                {$praktikum_maba=$l.PRAKTIKUM_MABA}
                            {/if}
                            {if empty($l.SP3_MABA)}
                                {$sp3_maba=0}
                            {else}
                                {$sp3_maba=$l.SP3_MABA}
                            {/if}
                            {if empty($l.SUKARELA_MABA)}
                                {$sukarela_maba=0}
                            {else}
                                {$sukarela_maba=$l.SUKARELA_MABA}
                            {/if}

                            <td>{number_format($jumlah_mhs)}</td>
                            <td>{number_format($sop)}</td>
                            <td>{number_format($praktikum)}</td>
                            <td>{number_format($sp3)}</td>
                            <td>{number_format($sukarela)}</td>
                            <td>{number_format($jumlah_cmhs)}</td>
                            <td>{number_format($sop_maba)}</td>
                            <td>{number_format($praktikum_maba)}</td>
                            <td>{number_format($sp3_maba)}</td>
                            <td>{number_format($sukarela_maba)}</td>
                            {$total_mhs=$total_mhs+$jumlah_mhs+$jumlah_cmhs}
                            {$total_sop=$total_sop+$sop+$sop_maba}
                            {$total_praktikum=$total_praktikum+$praktikum+$praktikum_maba}
                            {$total_sp3=$total_sp3+$sp3+$sp3_maba}
                            {$total_sukarela=$total_sukarela+$sukarela+$sukarela_maba}
                        {/foreach}
                        <td>{number_format($total_mhs)}</td>
                        <td>{number_format($total_sop)}</td>
                        <td>{number_format($total_praktikum)}</td>
                        <td>{number_format($total_sp3)}</td>
                        <td>{number_format($total_sukarela)}</td>
                    </tr>
                {/foreach}
            </table>
        {/if}
    {else if $smarty.get.mode=='detail'}
        <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="history.back(-1)">Kembali</a>
        <table style="width: 90%" class="ui-widget">
            <tr class="ui-widget-header">
                <th>NO</th>
                <th>NO UJIAN</th>
                <th>NAMA</th>
                <th>PROGRAM STUDI</th>
                <th>PENDAFTARAN</th>
                <th>SPP</th>
                <th>SP3</th>
                <th>SUMBANGAN</th>
            </tr>
            {foreach $data_laporan as $l}
                <tr class="ui-widget-content">
                    <td>{$l@index+1}</td>
                    <td>{$l.NO_UJIAN}</td>
                    <td>{$l.NM_C_MHS}</td>
                    <td>{$l.NM_JENJANG} {$l.NM_PROGRAM_STUDI}</td>
                    <td>{number_format($l.PENDAFTARAN)}</td>
                    <td>{number_format($l.SPP)}</td>
                    <td>{number_format($l.SP3)}</td>
                    <td>{number_format($l.SUMBANGAN)}</td>
                </tr>
            {foreachelse}
                <tr class="ui-widget-content">
                    <td colspan="8" class="data-kosong">Data Kosong</td>
                </tr>
            {/foreach}
        </table>
    {/if}
{/if}

