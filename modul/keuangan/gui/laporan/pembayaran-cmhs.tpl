<script type="text/javascript" src="addons/pager/jquery.tablesorter.pager.js"></script>
{literal}
    <script type="text/javascript">
        $(function() {
            $(".sort").tablesorter({sortList: [[0, 0]], widgets: ['zebra']});
            $("#tablePage").tablesorter({widthFixed: true, widgets: ['zebra']}).tablesorterPager({container: $("#pager")});
        });

    </script>
{/literal}
{if empty($data_report_fakultas)}
    <div class="center_title_bar">Laporan Pembayaran Mahasiswa Baru</div> 
    <div id="alert" class="ui-widget" style="display:none;margin-left:15px;">
        <div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;"> 
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                Tanggal Harus Diisi.</p>
        </div>
    </div>
    <form method="get" id="report_form" action="pembayaran-cmhs.php">
        <table class="ui-widget" style="width:90%">
            <tr class="ui-widget-header">
                <th colspan="4" class="header-coloumn">Parameter</th>
            </tr>
            <tr class="ui-widget-content">
                <td width="15%">Tanggal Awal</td>
                <td width="15%"><input type="text" id="tgl_awal" class="datepicker"  name="tgl_awal" {if isset($tgl_awal)} value="{$tgl_awal}"{/if} /></td>
                <td width="15%">Fakultas</td>
                <td width="55%">
                    <select name="fakultas" id="fakultas">
                        <option value="">Semua</option>
                        {foreach $data_fakultas as $data}
                            <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Tanggal Akhir</td>
                <td><input type="text" id="tgl_akhir" class="datepicker" class="required" name="tgl_akhir" {if isset($tgl_awal)} value="{$tgl_akhir}"{/if}/></td>
                <td>Program Studi</td>
                <td>
                    <select name="prodi" id="prodi">
                        <option value="">Semua</option>
                        {foreach $data_prodi as $data}
                            <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$prodi}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Angkatan</td>
                <td>
                    <select name="angkatan">
                        <option value="">Semua</option>
                        {foreach $data_angkatan as $d}
                            <option value="{$d.ANGKATAN}" {if $d.ANGKATAN==$smarty.get.angkatan}selected="true"{/if}>{$d.ANGKATAN}</option>
                        {/foreach}
                    </select>
                </td>
                <td>Jalur</td>
                <td>
                    <select name="jalur">
                        <option value="">Semua</option>
                        {foreach $data_jalur as $d}
                            <option value="{$d.ID_JALUR}" {if $d.ID_JALUR==$smarty.get.jalur}selected="true"{/if}>{$d.NM_JALUR}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr class="center ui-widget-content">
                <td colspan="4">
                    <input type="hidden" name="mode" value="bank" />
                    <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
                </td>
            </tr>

        </table>
    </form>
{/if}
{if isset($data_report_bank)}
    <table class="sort tablesorter ui-widget" style="width:90%">
        <thead>
            <tr class="ui-widget-header">
                <th colspan="5">
                    Rekapitulasi Pembayaran Mahasiswa Baru
                    {if $fakultas!=''}
                        <br/>
                        Fakultas {$data_fakultas_one.NM_FAKULTAS}
                    {/if}
                    {if $prodi!=''}
                        <br/>
                        Program Studi ({$data_prodi_one.NM_JENJANG}) {$data_prodi_one.NM_PROGRAM_STUDI}
                    {/if}
                    <br/>
                    Pada Tanggal {$tgl_awal} sampai {$tgl_akhir}
                </th>
            </tr>
            <tr class="ui-widget-header">
                <th>No</th>
                <th>NAMA BANK</th>
                <th>JUMLAH MAHASISWA</th>
                <th>JUMLAH PEMBAYARAN</th>
                <th>OPERASI</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_report_bank as $data}
                <tr class="center ui-widget-content">
                    <td>{$data@index+1}</td>
                    <td>{$data.NM_BANK}</td>
                    <td>{$data.JUMLAH_MHS}</td>
                    <td>{number_format($data.BESAR_BIAYA)}</td>
                    <td>
                        {$total_cmhs=$total_cmhs+$data.JUMLAH_MHS}
                        {$total_pembayaran=$total_pembayaran+$data.BESAR_BIAYA}
                        <a class="ui-button ui-corner-all ui-state-hover" href="pembayaran-cmhs.php?mode=detail&tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$fakultas}&prodi={$prodi}&bank={$data.ID_BANK}&jalur={$smarty.get.jalur}&angkatan={$smarty.get.angkatan}" style="padding:5px;cursor:pointer;">Detail</a>
                        {if $prodi==''}
                            <span class="ui-button ui-corner-all ui-state-hover" onclick="window.open('pembayaran-cmhs.php?mode=fakultas&tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$fakultas}&prodi={$prodi}&bank={$data.ID_BANK}&jalur={$smarty.get.jalur}&angkatan={$smarty.get.angkatan}', '_blank')"  style="padding:5px;cursor:pointer;">Fakultas</span>
                        {/if}
                    </td>
                </tr>
            {/foreach}
        </tbody>
        <tfoot>
            <tr class="center ui-widget-content">
                <td colspan="2">Bidik Misi</td>
                <td>{$data_report_bank_bidik_misi.JUMLAH_MHS}</td>
                <td>{number_format($data_report_bank_bidik_misi.BESAR_BIAYA)}</td>
                <td>
                    <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="pembayaran-cmhs.php?mode=detail&tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$fakultas}&prodi={$prodi}&bank=NULL">Detail</a>
                </td>
            </tr>
            <tr class="center ui-widget-content">
                <td class="total" colspan="2">Jumlah</td>
                <td class="total" >{$total_cmhs}</td>
                <td class="total" >{number_format($total_pembayaran)}</td>
                <td class="total" >
                    <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="pembayaran-cmhs.php?mode=detail&tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$fakultas}&prodi={$prodi}&bank=&jalur={$smarty.get.jalur}&angkatan={$smarty.get.angkatan}">Detail</a>
                    {if $prodi==''}
                        <span class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="window.open('pembayaran-cmhs.php?mode=fakultas&tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$fakultas}&prodi={$prodi}&bank=&jalur={$smarty.get.jalur}&angkatan={$smarty.get.angkatan}', '_blank')">Fakultas</span>
                    {/if}
                </td>
            </tr>
            <tr class="center ui-widget-content">
                <td class="link center" colspan="9">
                    <span class="link_button ui-corner-all" onclick="window.location.href = 'excel-pembayaran-cmhs-bank.php?tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$fakultas}&prodi={$prodi}&jalur={$smarty.get.jalur}&angkatan={$smarty.get.angkatan}'" style="padding:5px;cursor:pointer;">Excel</span>
                </td>	
            </tr>
        </tfoot>
    </table>
{elseif isset($data_report_detail_cmhs)}
    <table class="ui-widget" style="width:90%">
        <tr class="ui-widget-header">
            <th colspan="10">
        <p>
            Rekapitulasi Pembayaran Mahasiswa Baru
            {if $fakultas!=''}
                <br/>
                Fakultas {$data_fakultas_one.NM_FAKULTAS}
            {/if}
            {if $prodi!=''}
                <br/>
                Program Studi ({$data_prodi_one.NM_JENJANG}) {$data_prodi_one.NM_PROGRAM_STUDI}
            {/if}
            <br/>
            Pada Tanggal {$tgl_awal} sampai {$tgl_akhir}
        </p>
    </th>
</tr>
<tr class="ui-widget-header">
    <th>No</th>
    <th>NO UJIAN</th>
    <th>NIM</th>
    <th>NAMA</th>
    <th>PRODI</th>
    <th>JENJANG</th>
    <th>JALUR</th>
    <th>TGL BAYAR</th>
    <th>BESAR BIAYA</th>
    <th>RINCIAN</th>
</tr>
{foreach $data_report_detail_cmhs as $data}
    <tr class="center ui-widget-content">
        <td>{($data@index+1)+({$smarty.get.page-1}*100)}</td>
        <td>{$data.NO_UJIAN}</td>
        <td>{$data.NIM_MHS}</td>
        <td>{$data.NM_C_MHS}</td>
        <td>{$data.NM_PROGRAM_STUDI}</td>
        <td>{$data.NM_JENJANG}</td>
        <td>{$data.NM_JALUR}</td>
        <td width="70px">{$data.TGL_BAYAR}</td>
        <td>{number_format($data.BIAYA+$data.DENDA)}</td>
        <td>
            <a href="history-bayar.php?jenis=cmhs&cari={$data.NO_UJIAN}" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;">Rincian</a>
        </td>
    </tr>
{/foreach}
<tr class="center ui-widget-content">
    <td class="total" colspan="10">
        {$link_halaman}<br/>
        <span class="ui-button ui-corner-all ui-state-hover" style="margin-top:10px;padding:5px;cursor:pointer;" onclick="history.back(-1)">Kembali</span>
    </td>	
</tr>
<tr class="center ui-widget-content">
    <td class="link center" colspan="10">
        <span class="link_button ui-corner-all" onclick="window.location.href = 'excel-pembayaran-cmhs-detail.php?tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$fakultas}&prodi={$prodi}&bank={$bank}'" style="padding:5px;cursor:pointer;">Excel</span>
    </td>	
</tr>
</table>
{elseif isset($data_report_fakultas)}
    {literal}
        <link rel="stylesheet" type="text/css" href="../../css/jquery-ui-custom-keu.css" />
        <link rel="stylesheet" type="text/css" href="../../css/keuangan-style.css" />
        <style>           
            .ui-widget-content a{
                text-decoration: none;
            }
            .ui-widget-content a:hover{
                color: #f09a14;
            }
        </style>
    {/literal}
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="{if $fakultas!=''}{$count_data_biaya+6}{else}{$count_data_biaya+5}{/if}">
        <p>
            Rekapitulasi Pembayaran Mahasiswa Baru
            {if $fakultas!=''}
                <br/>
                Fakultas {$data_fakultas_one.NM_FAKULTAS}
            {/if}
            {if $prodi!=''}
                <br/>
                Program Studi ({$data_prodi_one.NM_JENJANG}) {$data_prodi_one.NM_PROGRAM_STUDI}
                <br/>
                Jalur {$data_jalur_one.NM_JALUR}
            {/if}
            <br/>
            Pada Tanggal {$tgl_awal} sampai {$tgl_akhir}
        </p>
    </th>
</tr>
<tr class="ui-widget-header">
    <th>No</th>
    <th>
        {if $prodi!=''}
            Nama
        {else if $fakultas!=''}
            Program Studi
        {else}
            Fakultas
        {/if}
    </th>
    {if $prodi!=''}
        <th>NO_UJIAN</th>
        {else if $fakultas !=''}
        <th>Tahun</th>
        <th>Jalur</th>
        {/if}
    <th>
        {if $prodi!=''}
            Program Studi
        {else}
            Jumlah MHS
        {/if}
    </th>
    {foreach $data_biaya as $data}
        <th>{$data.NM_BIAYA}</th>
        {/foreach}
    <th>Total Pembayaran</th>
</tr>
{foreach $data_report_fakultas as $data}
    <tr class="ui-widget-content">
        <td>{$data@index+1}</td>
        <td>
            {if $prodi!=''}
                {$data.NM_C_MHS}
            {else if $fakultas!=''}
                <a href="pembayaran-cmhs.php?mode=fakultas&tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$data.ID_FAKULTAS}&prodi={$data.ID_PROGRAM_STUDI}&bank={$bank}&jalur={$data.ID_JALUR}&angkatan={$smarty.get.angkatan}">
                    ({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}
                </a>
            {else}
                <a href="pembayaran-cmhs.php?mode=fakultas&tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$data.ID_FAKULTAS}&prodi={$prodi}&bank={$bank}&jalur={$smarty.get.jalur}&angkatan={$smarty.get.angkatan}">
                    {$data.NM_FAKULTAS}
                </a>
            {/if}    
        </td>
        {if $prodi!=''}
            <td>{$data.NO_UJIAN}</td>
        {else if $fakultas !=''}
            <td>{$data.TAHUN} </td>
            <td>{$data.NM_JALUR}</td>
        {/if}
        <td class="center">
            {if $prodi!=''}
                {$data.NM_PROGRAM_STUDI}
            {else}    
                {$data.JUMLAH_MHS}
                {$total_cmhs=$total_cmhs+$data.JUMLAH_MHS}
            {/if}
        </td>
        {$total_pembayaran_right=0}
        {foreach $data.TOTAL_PEMBAYARAN as $data_pembayaran}
            <td class="center">
                {number_format($data_pembayaran.BESAR_BIAYA)}
                {$total_pembayaran_right=$total_pembayaran_right+$data_pembayaran.BESAR_BIAYA}
                {$total_pembayaran_bottom[$data_pembayaran@index]=$total_pembayaran_bottom[$data_pembayaran@index]+$data_pembayaran.BESAR_BIAYA}
            </td>
        {/foreach}
        <td class="center total">{number_format($total_pembayaran_right)}</td>
    </tr>
{/foreach}
<tr class="center ui-widget-content">
    <td class="total" colspan="{if $prodi!=''}4{else if $fakultas !=''}4{else}2{/if}">Jumlah</td>
    {if $prodi==''}
        <td class="total">{$total_cmhs}</td>
    {/if}
    {foreach $data.TOTAL_PEMBAYARAN as $data_pembayaran}
        <td class="total">
            {number_format($total_pembayaran_bottom[$data_pembayaran@index])}
            {$total=$total+$total_pembayaran_bottom[$data_pembayaran@index]}
        </td>
    {/foreach}
    <td class="total">{number_format($total)}</td>
</tr>
<tr class="center ui-widget-content">
    <td class="total" colspan="{if $fakultas!=''}{$count_data_biaya+6}{else}{$count_data_biaya+5}{/if}">
        <span class="ui-button ui-corner-all ui-state-hover" style="margin-top:10px;padding:5px;cursor:pointer;" onclick="history.back(-1)">Kembali</span>
        <span class="ui-button ui-corner-all ui-state-hover" style="margin-top:10px;padding:5px;cursor:pointer;" onclick="window.close()">Close</span>
    </td>	
</tr>
<tr class="center ui-widget-content">
    <td class="link center" colspan="{if $fakultas!=''}{$count_data_biaya+6}{else}{$count_data_biaya+5}{/if}">
        <span class="link_button ui-corner-all" onclick="window.location.href = 'excel-pembayaran-cmhs-fakultas.php?tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$fakultas}&prodi={$prodi}&bank={$bank}&jalur={$jalur}'" style="padding:5px;cursor:pointer;">Excel</span>
    </td>	
</tr>
</table>
{/if}

{literal}
    <script>
        $('#report_form').submit(function() {
            if ($('#tgl_awal').val() == '' || $('#tgl_akhir').val() == '') {
                $('#alert').fadeIn();
                return false;
            }
        });
        $('#report_form').validate();
        $(".datepicker").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true,
            changeYear: true});
        $('#fakultas').change(function() {
            $.ajax({
                type: 'post',
                url: 'getProdi.php',
                data: 'id_fakultas=' + $(this).val(),
                success: function(data) {
                    $('#prodi').html(data);
                }
            })
        });
    </script>
{/literal}