<script type="text/javascript" src="addons/pager/jquery.tablesorter.pager.js"></script>
{literal}
    <script type="text/javascript">
        $(function() {
            $(".sort").tablesorter({sortList: [[0, 0]], widgets: ['zebra']});
            $("#tablePage").tablesorter({sortList: [[0, 0]], widgets: ['zebra']}).tablesorterPager({container: $("#pager")});
        });

    </script>
{/literal}
{if empty($data_report_fakultas)&&$smarty.get.mode!='fakultas'}
    <div class="center_title_bar">Laporan Pembayaran Mahasiswa</div> 
    <div id="alert" class="ui-widget" style="display:none;margin-left:15px;">
        <div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;"> 
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                Tanggal Harus Diisi.</p>
        </div>
    </div>
    <form method="get" id="report_form" action="pembayaran-mhs.php">
        <table class="ui-widget" style="width:90%">
            <tr class="ui-widget-header">
                <th colspan="4" class="header-coloumn">Parameter</th>
            </tr>
            <tr class="ui-widget-content">
                <td width="15%">Tanggal Awal</td>
                <td width="15%"><input type="text" id="tgl_awal" class="datepicker"  name="tgl_awal" {if isset($tgl_awal)} value="{$tgl_awal}"{/if} /></td>
                <td width="15%">Fakultas</td>
                <td width="55%">
                    <select name="fakultas" id="fakultas">
                        <option value="">Semua</option>
                        {foreach $data_fakultas as $data}
                            <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Tanggal Akhir</td>
                <td><input type="text" id="tgl_akhir" class="datepicker" class="required" name="tgl_akhir" {if isset($tgl_awal)} value="{$tgl_akhir}"{/if}/></td>
                <td>Program Studi</td>
                <td>
                    <select name="prodi" id="prodi">
                        <option value="">Semua</option>
                        {foreach $data_prodi as $data}
                            <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$prodi}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Periode Bayar</td>
                <td colspan="3">
                    <select name="periode_bayar" >
                        <option value="">Semua</option>
                        <option value="Pagi" {if $periode_bayar=='Pagi'}selected="true"{/if}>Pagi</option>
                        <option value="Siang" {if $periode_bayar=='Siang'}selected="true"{/if}>Siang</option>
                        <option value="Malam" {if $periode_bayar=='Malam'}selected="true"{/if}>Malam</option>
                    </select>
                </td>
            </tr>
            <tr class="center ui-widget-content">
                <td colspan="4">
                    <input type="hidden" name="mode" value="bank" />
                    <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
                </td>
            </tr>

        </table>
    </form>
{/if}
{if isset($data_report_bank)}
    <table class="sort tablesorter ui-widget" style="width:90%">
        <thead>
            <tr class="ui-widget-header">
                <th colspan="5">

                    Rekapitulasi Pembayaran Mahasiswa
                    {if $fakultas!=''}
                        <br/>
                        Fakultas {$data_fakultas_one.NM_FAKULTAS}
                    {/if}
                    {if $prodi!=''}
                        <br/>
                        Program Studi ({$data_prodi_one.NM_JENJANG}) {$data_prodi_one.NM_PROGRAM_STUDI}
                    {/if}
                    <br/>
                    Pada Tanggal {$tgl_awal} sampai {$tgl_akhir}
                </th>
            </tr>
            <tr class="ui-widget-header">
                <th>No</th>
                <th>NAMA BANK</th>
                <th>JUMLAH MAHASISWA</th>
                <th>JUMLAH PEMBAYARAN</th>
                <th>OPERASI</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_report_bank as $data}
                <tr class="center ui-widget-content">
                    <td>{$data@index+1}</td>
                    <td>{$data.NM_BANK}</td>
                    <td>{$data.JUMLAH_MHS}</td>
                    <td>{number_format($data.BESAR_BIAYA)}</td>
                    <td>
                        {$total_mhs=$total_mhs+$data.JUMLAH_MHS}
                        {$total_pembayaran=$total_pembayaran+$data.BESAR_BIAYA}
                        <a class="ui-button ui-corner-all ui-state-hover" href="pembayaran-mhs.php?mode=detail&tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$fakultas}&prodi={$prodi}&periode_bayar={$periode_bayar}&bank={$data.ID_BANK}" style="padding:5px;cursor:pointer;">Detail per Mahasiswa</a>
                        {if $prodi==''}
                            <span class="ui-button ui-corner-all ui-state-hover" onclick="window.open('pembayaran-mhs.php?mode=fakultas&tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$fakultas}&prodi={$prodi}&periode_bayar={$periode_bayar}&bank={$data.ID_BANK}', '_blank')"  style="padding:5px;cursor:pointer;">Detail per Biaya</span>
                        {/if}
                    </td>
                </tr>
            {/foreach}
        </tbody>
        <tfoot>
            <tr class="center ui-widget-content">
                <td class="total" colspan="2">Jumlah</td>
                <td class="total" >{$total_mhs}</td>
                <td class="total" >{number_format($total_pembayaran)}</td>
                <td class="total" >
                    <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="pembayaran-mhs.php?mode=detail&tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$fakultas}&prodi={$prodi}&periode_bayar={$periode_bayar}">Per Mahasiswa</a>
                    {if $prodi==''}
                        <span class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="window.open('pembayaran-mhs.php?mode=fakultas&tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$fakultas}&prodi={$prodi}&periode_bayar={$periode_bayar}', '_blank')">Per Fakultas</span>
                        <span class="ui-button ui-corner-all ui-state-hover" onclick="window.open('pembayaran-mhs.php?mode=fakultas&tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$fakultas}&prodi={$prodi}&periode_bayar={$periode_bayar}&bank=', '_blank')"  style="padding:5px;cursor:pointer;">Per Biaya</span>
                    {/if}
                </td>
            </tr>
            {* 
            <tr class="center ui-widget-content">
                <td class="link center" colspan="9">
                    <span class="link_button ui-corner-all" onclick="window.location.href = 'excel-pembayaran-mhs-bank.php?tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$fakultas}&prodi={$prodi}'" style="padding:5px;cursor:pointer;">Excel</span>
                </td>	
            </tr> 
            *}
        </tfoot>
    </table>
{elseif isset($data_report_detail_mhs)}
    <table id="tablePage" class="tablesorter ui-widget" style="width:90%">
        <thead>
            <tr class="ui-widget-header">
                <th colspan="9">

                    Rekapitulasi Pembayaran Mahasiswa
                    {if $fakultas!=''}
                        <br/>
                        Fakultas {$data_fakultas_one.NM_FAKULTAS}
                    {/if}
                    {if $prodi!=''}
                        <br/>
                        Program Studi ({$data_prodi_one.NM_JENJANG}) {$data_prodi_one.NM_PROGRAM_STUDI}
                    {/if}
                    <br/>
                    Pada Tanggal {$tgl_awal} sampai {$tgl_akhir}
                </th>
            </tr>
            <tr class="ui-widget-header">
                <th>No</th>
                <th>NIM</th>
                <th>NAMA</th>
                <th>PRODI</th>
                <th>JENJANG</th>
                <th>JALUR</th>
                <th>TGL BAYAR</th>
                <th>BESAR BIAYA</th>
                <th>RINCIAN</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_report_detail_mhs as $data}
                <tr class="center ui-widget-content">
                    <td>{($data@index+1)}</td>
                    <td>{$data.NIM_MHS}</td>
                    <td>{$data.NM_PENGGUNA}</td>
                    <td>{$data.NM_PROGRAM_STUDI}</td>
                    <td>{$data.NM_JENJANG}</td>
                    <td>{$data.NM_JALUR}</td>
                    <td width="70px">{$data.TGL_BAYAR}</td>
                    <td>{number_format($data.BIAYA+$data.DENDA)}</td>
                    <td>
                        <a target="_blank" href="#pembayaran-mhs!history-bayar.php?cari={$data.NIM_MHS}" class="disable-ajax ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;">Rincian</a>
                    </td>
                </tr>
            {/foreach}
        </tbody>
        <tfoot>
            <tr class="center ui-widget-content">
                <td class="link center" colspan="9">
                    <span class="link_button ui-corner-all" onclick="window.location.href = 'excel-pembayaran-mhs-detail.php?{$smarty.SERVER.QUERY_STRING}'" style="padding:5px;cursor:pointer;">Excel</span>
                </td>	
            </tr>
        </tfoot>
        <span class="ui-button ui-corner-all ui-state-hover" style="margin-top:10px;padding:5px;cursor:pointer;" onclick="history.back(-1)">Kembali</span>
    </table>
    <div id="pager" class="pager" style="margin: 20px">
        <form>
            <img src="addons/pager/first.png" height="16px" width="16px" class="first"/>
            <img src="addons/pager/prev.png" height="16px" width="16px" class="prev"/>
            <input type="text" class="pagedisplay"/>
            <img src="addons/pager/next.png" height="16px" width="16px" class="next"/>
            <img src="addons/pager/last.png" height="16px" width="16px" class="last"/>
            <select class="pagesize">
                <option selected="selected"  value="10">10</option>
                <option value="20">20</option>
                <option value="30">30</option>
                <option  value="40">40</option>
                <option  value="50">50</option>
                <option  value="100">100</option>
            </select>
        </form>
    </div>
{elseif isset($data_report_fakultas_row)}
    {literal}
        <link rel="stylesheet" type="text/css" href="../../css/jquery-ui-custom-keu.css" />
        <link rel="stylesheet" type="text/css" href="../../css/keuangan-style.css" />
        <style>           
            .ui-widget-content a{
                text-decoration: none;
            }
            .ui-widget-content a:hover{
                color: #f09a14;
            }
        </style>
    {/literal}
    <table class="ui-widget" style="width:250%">
        <tr class="ui-widget-header">
            <th colspan="{if $fakultas !=''}{$count_data_biaya+7}{else} {$count_data_biaya+5}{/if}">
                <span>Rekapitulasi Pembayaran Mahasiswa
                    {if $fakultas!=''}
                        <br/>
                        Fakultas {$data_fakultas_one.NM_FAKULTAS}
                    {/if}
                    {if $prodi!=''}
                        <br/>
                        Program Studi ({$data_prodi_one.NM_JENJANG}) {$data_prodi_one.NM_PROGRAM_STUDI}
                        <br/>
                        Jalur {$data_jalur_one.NM_JALUR}
                    {/if}
                    <br/>
                    Pada Tanggal {$tgl_awal} sampai {$tgl_akhir}
                </span>
            </th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>
                {if $prodi!=''}
                    Nama
                {else if $fakultas!=''}
                    Program Studi
                {else}
                    Fakultas
                {/if}
            </th>
            {if $prodi!=''}
                <th>NIM</th>
                {else if $fakultas !=''}
                <th>Angkatan</th>
                <th>Jalur</th>
                {/if}
            <th>
                {if $prodi!=''}
                    Program Studi
                {else}
                    Jumlah MHS
                {/if}
                {foreach $data_biaya as $data}
                <th>{$data.NM_BIAYA}</th>
                {/foreach}
            <th>Total Pembayaran</th>
            <th>Denda Pembayaran</th>
        </tr>
        {foreach $data_report_fakultas_row as $data}
            <tr class="ui-widget-content">
                <td>{$data@index+1}</td>
                <td>
                    {if $prodi!=''}
                        {$data.NM_PENGGUNA}
                    {else if $fakultas!=''}
                        ({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}
                    {else}
                        <a href="pembayaran-mhs.php?mode=fakultas&tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$data.ID_FAKULTAS}&prodi={$prodi}&periode_bayar={$periode_bayar}&bank={$bank}">
                            {$data.NM_FAKULTAS}
                        </a>
                    {/if}    
                </td>
                {if $prodi!=''}
                    <td>{$data.NIM_MHS}</td>
                {else if $fakultas !=''}
                    <td>{$data.THN_ANGKATAN_MHS}</td>
                    <td>{$data.NM_JALUR}</td>
                {/if}
                <td class="center">
                    {if $prodi!=''}
                        {$data.NM_PROGRAM_STUDI}
                    {else}    
                        {$data.JUMLAH_MHS}
                        {$total_mhs=$total_mhs+$data.JUMLAH_MHS}
                    {/if}
                </td>
                {$total_pembayaran_right=0}
                {$total_denda_right=0}
                {foreach $data_biaya as $column_biaya}
                     <td class="center">
                     {$pembayaran=0}
                    {foreach $data_report_fakultas_row_value as $data_pembayaran}                       
                            {if !empty($data.THN_ANGKATAN_MHS)}
                                {if $data.ID_PROGRAM_STUDI == $data_pembayaran.ID_PROGRAM_STUDI and
                                $data.THN_ANGKATAN_MHS == $data_pembayaran.THN_ANGKATAN_MHS and
                                $data.ID_JALUR == $data_pembayaran.ID_JALUR and
                                $column_biaya.ID_BIAYA == $data_pembayaran.ID_BIAYA}
                                        
                                {$pembayaran=$data_pembayaran.BESAR_BIAYA}
                                {$total_pembayaran_right=$total_pembayaran_right+$data_pembayaran.BESAR_BIAYA}
                                {$total_denda_right=$total_denda_right+$data_pembayaran.DENDA_BIAYA}
                                {/if}
                            {else}
                                {if $data.ID_FAKULTAS == $data_pembayaran.ID_FAKULTAS and
                                $column_biaya.ID_BIAYA == $data_pembayaran.ID_BIAYA}
                                        
                                {$pembayaran=$data_pembayaran.BESAR_BIAYA}
                                {$total_pembayaran_right=$total_pembayaran_right+$data_pembayaran.BESAR_BIAYA}
                                {$total_denda_right=$total_denda_right+$data_pembayaran.DENDA_BIAYA}
                                {/if}
                            {/if}
                    {/foreach}
                    {number_format($pembayaran)}                 
                    {$total_pembayaran_bottom[$column_biaya@index]=$total_pembayaran_bottom[$column_biaya@index]+$pembayaran}                    
                    </td>
                {/foreach}                
                <td class="center total">{number_format($total_pembayaran_right)}</td>
                <td class="center total">{number_format($total_denda_right)}</td>
                {$total_denda=$total_denda+$total_denda_right}
            </tr>
        {/foreach}
        <tr class="center ui-widget-content">
            <td class="total" colspan="{if $prodi!=''}4{else if $fakultas !=''}4{else}2{/if}">Jumlah</td>
            {if $prodi==''}
                <td class="total">{$total_mhs}</td>
            {/if}
            {foreach $data_biaya as $column_biaya}
                <td class="total">
                    {number_format($total_pembayaran_bottom[$column_biaya@index])}
                    {$total=$total+$total_pembayaran_bottom[$column_biaya@index]}
                </td>
            {/foreach}
            <td class="total" colspan="2">{number_format($total+$total_denda)}</td>
        </tr>
        <tr class="center ui-widget-content">
            <td class="total" colspan="{if $fakultas !=''}{$count_data_biaya+7}{else} {$count_data_biaya+5}{/if}">
                <span class="ui-button ui-corner-all ui-state-hover" style="margin-top:10px;padding:5px;cursor:pointer;" onclick="history.back(-1)">Kembali</span>
                <span class="ui-button ui-corner-all ui-state-hover" style="margin-top:10px;padding:5px;cursor:pointer;" onclick="window.close()">Close</span>
            </td>	
        </tr>
        <tr class="center ui-widget-content">
            <td class="link center" colspan="{if $fakultas !=''}{$count_data_biaya+7}{else} {$count_data_biaya+5}{/if}">
                <span class="link_button ui-corner-all" onclick="window.location.href = 'excel-pembayaran-mhs-fakultas.php?tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$fakultas}&prodi={$prodi}&periode_bayar={$periode_bayar}&bank={$bank}&jalur={$jalur}'" style="padding:5px;cursor:pointer;">Excel</span>
            </td>	
        </tr>
    </table>
{/if}

{literal}
    <script>
        $('#report_form').submit(function() {
            if ($('#tgl_awal').val() == '' || $('#tgl_akhir').val() == '') {
                $('#alert').fadeIn();
                return false;
            }
        });
        $('#report_form').validate();
        $(".datepicker").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true,
            changeYear: true});
        $('#fakultas').change(function() {
            $.ajax({
                type: 'post',
                url: 'getProdi.php',
                data: 'id_fakultas=' + $(this).val(),
                success: function(data) {
                    $('#prodi').html(data);
                }
            })
        });
    </script>
{/literal}