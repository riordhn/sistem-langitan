{if empty($mode)||$mode=='tampil'}
<div class="center_title_bar">Tagihan Mahasiswa Mahasiswa</div> 
{$page_info}
<form method="get" id="report_form" action="tagihan-mhs.php">
    <table class="ui-widget" style="width:90%;">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td width="15%">Fakultas</td>
            <td width="35%">
                <select name="fakultas" id="fakultas" class="select2" style="width:90%">
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
            <td>Semester</td>
            <td>
                <select name="semester" class="select2" style="width:90%">
                    {foreach $data_semester as $semester}
                        <option value="{$semester['ID_SEMESTER']}" {if $semester.ID_SEMESTER==$smarty.get.semester} selected="true" {/if}>{$semester['NM_SEMESTER']}({$semester['TAHUN_AJARAN']})</option>
                    {/foreach}
                </select>
            </td>

        </tr>
        <tr class="ui-widget-content">
            <td>Status Mahasiswa</td>
            <td>
                <select multiple="true" size="8" name="status[]" id="status" style="width:90%">
                    {foreach $data_status as $data}
                        <option value="{$data.ID_STATUS_PENGGUNA}" {if $data.ID_STATUS_PENGGUNA==$smarty.get.status}selected="true"{/if}>{$data.NM_STATUS_PENGGUNA} ({if $data.STATUS_AKTIF==1}Aktif{else}Tidak{/if})</option>
                    {/foreach}
                </select>
            </td>
            <td colspan="2"></td>
        </tr>
        <tr class="center ui-widget-content">
            <td colspan="4">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" name="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{literal}
<script>
$(document).ready(function() {
    var statuses = {/literal}{json_encode($smarty.get.status)}{literal};   
    $('.select2').select2();
    $('#status').select2().val(statuses).trigger("change");
});
</script>
{/literal}
{/if}
{if !empty($data_rekap)}
    <table class="ui-widget-content" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="7" class="header-coloumn">Rekap Tagihan</th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>{if $fakultas!=''} Program Studi {else} Fakultas {/if}</th>
            <th>Jumlah Mahasiswa (Dengan Tagihan)</th>
            <th>Tagihan</th>
            <th>Terbayar</th>
            <th>Piutang</th>
            <th>Detail</th>
        </tr>
        {$total_data=0}
        {foreach $data_rekap as $data}
            <tr class=" ui-widget-content">
                <td>{$data@index+1}</td>
                <td>{$data.PRODI}</td>
                <td style="text-align:center;font-weight:bold">{number_format($data.JUMLAH_MHS)}</td>
                <td style="text-align:right;font-weight:bold">{number_format($data.TAGIHAN)}</td>
                <td style="text-align:right;font-weight:bold">{number_format($data.TERBAYAR)}</td>
                <td style="text-align:right;font-weight:bold">{number_format($data.TAGIHAN-$data.TERBAYAR)}</td>
                <td class="center" >
                    <span style="display:none;color:red" id="loading-detail-{$data.ID_PRODI}">loading data</span>                        
                    <span style="{if !$load_wh}display:none;{/if}padding:5px;cursor:pointer;" id="btn-detail-{$data.ID_PRODI}" class="ui-button ui-corner-all ui-state-hover hide" onclick="open_window_detail('tagihan-mhs.php?{$smarty.server.QUERY_STRING|replace:'tampil':'detail'}&prodi={$data.ID_PRODI}&params={$form_params}')" >Detail</span>                    
                </td>
            </tr>
        {foreachelse}
            <tr class="ui-widget-content">
                <td colspan="10" class="center"><span style="color: red">Data Kosong</span></td>
            </tr>                
        {/foreach}
    </table>
    {literal}
    <script>
    var data_loaded=0;
    $(document).ready(function() {
    {/literal} 
        {if !$load_wh}
        {foreach $data_rekap as $data}
        $.ajax({
            'url':'/modul/cron/keuangan/tagihan-mhs.php?{$smarty.server.QUERY_STRING}&prodi={$data.ID_PRODI}&params={$form_params}',
            'method':'GET',
            'beforeSend': function() {
                $('#loading-detail-{$data.ID_PRODI}').show();
            },
            'success': function() {
                $('#loading-detail-{$data.ID_PRODI}').hide();
                $('#btn-detail-{$data.ID_PRODI}').show();
                data_loaded++;
            }
        });
        {/foreach}
        {else}
            data_loaded={count($data_rekap)};
        {/if}
    {literal}        
    });
    function open_window_detail(url){
        if(data_loaded=={/literal}{count($data_rekap)}{literal}){
            window.open(url, '_blank')
        }else{
            alert("Mohon tunggu sampai semua data berhasil diambil.")
        }
    }
    </script>
    {/literal}
{/if}
{if !empty($data_rekap_detail)}
    {literal}
        <link rel="stylesheet" type="text/css" href="../../css/jquery-ui-custom-keu.css" />
        <link rel="stylesheet" type="text/css" href="../../css/keuangan-style.css" />
        <style>           
            .ui-widget-content a{
                text-decoration: none;
            }
            .ui-widget-content a:hover{
                color: #f09a14;
            }
        </style>
    {/literal}
    <table class="ui-widget" style="width:250%">
        <tr class="ui-widget-header" >
            <th colspan="{if $fakultas !=''}{$count_data_biaya+7}{else} {$count_data_biaya+5}{/if}" style="text-align:left">
                <span>Rekapitulasi Tagihan Mahasiswa
                    {if $fakultas!=''}
                        <br/>
                        Fakultas {$data_fakultas_one.NM_FAKULTAS}
                    {/if}
                    {if $prodi!=''}
                        <br/>
                        Program Studi ({$data_prodi_one.NM_JENJANG}) {$data_prodi_one.NM_PROGRAM_STUDI}
                    {/if}
                    <br/>
                    Per Tanggal {date('d-m-Y')}
                </span>
            </th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>Nama</th>
            <th>NIM</th>            
            <th>Prodi</th>       
            <th>Angkatan</th>
            <th>Jalur</th>
            {foreach $data_biaya as $data}
                <th>{$data.NM_BIAYA}</th>
            {/foreach}
            <th>Total Pembayaran</th>
        </tr>
        {foreach $data_rekap_detail as $data}
            <tr class="ui-widget-content">
                <td>{$data@index+1}</td>
                <td>{$data.NAMA}</td>
                <td>{$data.NIM}</td>
                <td class="center">{$data.PRODI}</td>
                <td>{$data.ANGKATAN}</td>
                <td>{$data.JALUR}</td>
                {$total_pembayaran_right=0}
                {$total_denda_right=0}
                {$index=1}
                {foreach $data_biaya as $b}
                    <td class="center">
                        {number_format($data['BIAYA_'|cat:$index])}
                        {$total_pembayaran_right=$total_pembayaran_right+$data.TOTAL}
                        {$total_pembayaran_bottom[$index-1]=$total_pembayaran_bottom[$index-1]+$data['BIAYA_'|cat:$index]}
                        {$index=$index+1}
                    </td>
                {/foreach}
                <td class="center total">{number_format($data.TOTAL)}</td>
            </tr>
        {/foreach}
        <tr class="center ui-widget-content">
            <td class="total" colspan="6">Jumlah</td>  
            {$index=1}          
            {foreach $data_biaya as $b}
                <td class="total">
                    {number_format($total_pembayaran_bottom[$index-1])}
                    {$total=$total+$total_pembayaran_bottom[$index-1]}
                    {$index=$index+1}
                </td>
            {/foreach}
            <td class="total">{number_format($total)}</td>
        </tr>
        <tr class="center ui-widget-content">
            <td class="total" colspan="{if $fakultas !=''}{$count_data_biaya+7}{else} {$count_data_biaya+5}{/if}">
                <span class="ui-button ui-corner-all ui-state-hover" style="margin-top:10px;padding:5px;cursor:pointer;" onclick="history.back(-1)">Kembali</span>
                <span class="ui-button ui-corner-all ui-state-hover" style="margin-top:10px;padding:5px;cursor:pointer;" onclick="window.close()">Close</span>
            </td>	
        </tr>
        <tr class="center ui-widget-content">
            <td class="link center" colspan="{if $fakultas !=''}{$count_data_biaya+7}{else} {$count_data_biaya+5}{/if}">
                <span class="link_button ui-corner-all" onclick="window.location.href = 'excel-tagihan-mhs.php?{$smarty.server.QUERY_STRING|replace:'tampil':'detail'}'" style="padding:5px;cursor:pointer;">Excel</span>
            </td>	
        </tr>
    </table>
{/if}

