<table class="ui-widget" width="96%">
    <tr class="ui-widget-header">
        <th colspan="2">DETAIL KLASIFIKASI PIUTANG METODE NAIVE BAYES {$data_detail_mahasiswa.NIM_MHS}</th>
    </tr>
    <tr>
        <td>Probabilitas Prior Kelas</td>
        <td> Ya = {$detail.pri_y} , Tidak = {$detail.pri_t}</td>
    </tr>
    <tr>
        <td>Probabilitas Posterior Untuk Atribut Perubahan Status <br/>
            P(Perubahan Status=
            {if $smarty.get.s==1}
                Tetap
            {else if $smarty.get.s==2}
                Berubah Aktif
            {else if $smarty.get.s==3}
                Berubah Tidak Aktif
            {/if})
        </td>
        <td> Piutang = Ya = {$detail.pos_s_y} , Piutang = Tidak = {$detail.pos_s_t}</td>
    </tr>
    <tr>
        <td>Probabilitas Posterior Untuk Atribut Jenjang
             <br/> P(Jenjang={$smarty.get.j})
        </td>
        <td> Piutang = Ya = {$detail.pos_j_y} , Piutang = Tidak = {$detail.pos_j_t}</td>
    </tr>
    <tr>
        <td>Probabilitas Posterior Untuk Atribut Banyak Tagihan
            <br/> P(Banyak Tagihan={$smarty.get.t})
        </td>
        <td>Piutang =  Ya = {$detail.pos_t_y} , Piutang = Tidak = {$detail.pos_t_t}</td>
    </tr>
    <tr>
        <td rowspan="2">Perhitungan</td>
        <td>P(X|Piutang=Ya) x P(Piutang=Ya) = ({round($detail.pos_s_y,2)} x {round($detail.pos_j_y,2)} x {round($detail.pos_t_y,2)}) x {round($detail.pri_y,2)} <br/> = {round($detail.y,5)} </td>
    </tr>
    <tr>
        <td>P(X|Piutang=Tidak) x P(Piutang=Tidak) = ({round($detail.pos_s_t,2)} x {round($detail.pos_j_t,2)} x {round($detail.pos_t_t,2)}) x {round($detail.pri_t,2)} <br/> = {round($detail.t,5)}</td>
    </tr>
    <tr>
        <td colspan="2" class="center">

            Karena 
            <b>
                {if $detail.y>$detail.t}
                    P(X|Piutang=Ya) x P(Piutang=Ya)
                {else} 
                    P(X|Piutang=Tidak) x P(Piutang=Tidak) 
                {/if} 
            </b>
            Lebih Besar Dari
            <b>
                {if $detail.y>$detail.t}
                    P(X|Piutang=Tidak) x P(Piutang=Tidak)
                {else} 
                    P(X|Piutang=Ya) x P(Piutang=Ya) 
                {/if} 
            </b>
            Maka Data Tagihan Mahasiswa Ini Di Prediksi Termasuk Ke dalam 
            <b>
                {if $detail.y>$detail.t}
                    Piutang=Ya
                {else} 
                    Piutang=Tidak
                {/if}
            </b>
        </td>        
    </tr>
</table>