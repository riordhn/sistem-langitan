<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="../../css/bootstrap-4.5.2-custom-keu.min.css" />
        <link rel="stylesheet" type="text/css" href="css/themes/blue/style.css" media="print, projection, screen" />
        <link rel="stylesheet" type="text/css" href="../../css/jquery-ui-custom-keu.css" />
        <link rel="stylesheet" type="text/css" href="../../css/keuangan-style.css" />
        <link rel="stylesheet" type="text/css" href="../../css/keuangan-nav-style.css" />
        <link rel="stylesheet" type="text/css" href="../../css/select2.min.css" />
        <link rel="stylesheet" type="text/css" href="../../css/sweetalert/sweetalert2.min.css" />
        

        <script type="text/javascript" src="../../js/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
        <script type="text/javascript" src="js/jquery.tablesorter.js"></script>
        <script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="../regmaba/js/additional-methods.min.js"></script>
        <script type="text/javascript" src="../../js/jquery-input-format.js"></script>
        <script type="text/javascript" src="../../js/keuangan-nav.js"></script>
        <script type="text/javascript" src="../../js/select2.min.js"></script>
        <script type="text/javascript" src="../../js/sweetalert/sweetalert2.min.js"></script>
        <script type="text/javascript">
            var defaultRel = 'history-bayar';
            var defaultPage = 'history-bayar.php';
            id_role = 10;
        </script>
        <script language="javascript" src="../../js/cybercampus.ajax-1.0.js"></script>
        <title>Keuangan - {$nama_pt}</title>
    </head>
    <body>
        <div id="main_container">
            <div id="header">
                <div id="judul_header" style="background-image: url('../../img/header/keuangan-{$nama_singkat}.png')">
                </div>
                <div id="menu_tab">
                    <ul class="topnav">
                        {foreach $struktur_menu as $m}
                            {if $m.AKSES == 1}
                                <li><a href="#{$m.NM_MODUL}!{$m.PAGE}">{$m.TITLE}</a>
                                    {if $m.SUBMENU!=NULL}
                                        <ul class="subnav">
                                            {foreach $m.SUBMENU as $sm}
                                                <li><a href="#{$sm.NM_MENU}!{$sm.PAGE}">{$sm.TITLE}</a></li>
                                                {/foreach}
                                        </ul>
                                    {/if}
                                </li>
                            {/if}
                        {/foreach}
                        <li>
                            <label style="cursor: pointer" onclick="window.location.href = '../../logout.php'">Log Out</label>
                        </li>
                        <li>
                            <label style="color: goldenrod;font-size: 13px">User: {$user|strtolower|capitalize}</label>
                        </li>
                    </ul>
                </div>
            </div>
            <div id="center">
                <div id="center_content"></div>
            </div>
            <div id="footer">
                <div id="left_footer">
                    <img src="../../../img/keuangan/footer_logo.jpg" width="170" height="37" />
                </div>
                <div id="center_footer">Copyright &copy; 2015 - {$nama_pt} <br />Sistem Langitan NU</div>
                <div id="right_footer">
                    <a href="#">home</a>
                    <a href="#">about</a>
                    <a href="#">sitemap</a>
                    <a href="#">rss</a>
                    <a href="#">contact us</a>
                </div>
            </div>
        </div>
    </body>
</html>
