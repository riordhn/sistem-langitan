<form id="form_edit" method="post" action="jenis-honor.php">
    <table class="ui-widget" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="text-align: center;">Edit Jenis Honor</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Nama Jenis Honor</td>
            <td>
                <textarea name="nama">{$data_jenis_honor.NM_JENIS_HONOR}</textarea>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Tipe Jenis Honor</td>
            <td>
                <select name="tipe">
                    <option value="1" {if $data_jenis_honor.TIPE_HONOR=='1'}selected="true" {/if}>Dosen</option>
                    <option value="2" {if $data_jenis_honor.TIPE_HONOR=='2'}selected="true" {/if}>Karyawan</option>
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" class="center">
                <input type="hidden" name="mode" value="edit"/>
                <input type="hidden" name="id" value="{$data_jenis_honor.ID_JENIS_HONOR}"/>
                <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Edit"/>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
            $('#form_edit').validate();            
            $('#form_edit').submit(function(){
                if($('#form_edit').valid()){
                    $('#dialog-edit').dialog('close');
                }
                else{
                    return false;
                }
            });
    </script>
{/literal}