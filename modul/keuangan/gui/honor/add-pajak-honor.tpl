<form id="form_add" method="post" action="pajak-honor.php">
    <table class="ui-widget" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="text-align: center;">Tambah Pajak Honor</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Besar Pajak</td>
            <td>
                <input type="text" name="pajak" class="required number" size="3" maxlength="3" />%
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Besar Pajak NPWP</td>
            <td>
                <input type="text" name="pajak_npwp" class="required number" size="3" maxlength="3" />%
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Golongan dan Pangkat</td>
            <td>
                <select name="golongan">
                    {foreach $data_golongan as $data}
                        <option value="{$data.ID_GOLONGAN}">{$data.NM_GOLONGAN} {$data.NM_PANGKAT}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" class="center">
                <input type="hidden" name="mode" value="add"/>
                <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Tambah"/>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
            $('#form_add').validate();            
            $('#form_add').submit(function(){
                if($('#form_add').valid()){
                    $('#dialog-add').dialog('close');
                }
                else{
                    return false;
                }
            });
    </script>
{/literal}