<div class="center_title_bar">Master Pajak Honor</div>
{$alert}
<table class="ui-widget" style="width: 40%">
    <tr class="ui-widget-header">
        <th colspan="5" class="header-coloumn" style="text-align: center;">Master Pajak Honor</th>
    </tr>
    <tr class="ui-widget-header">
        <th>No</th>
        <th>Besar Pajak</th>
        <th>Besar Pajak NPWP</th>
        <th>Golongan dan Pangkat</th>
        <th>Operasi</th>
    </tr>
    {foreach $data_pajak_honor as $data}
        <tr class="ui-widget-content">
            <td class="center">{$data@index+1}</td>
            <td class="center">{$data.BESAR_PAJAK} %</td>
            <td class="center">{$data.BESAR_PAJAK_NPWP} %</td>
            <td>{$data.NM_GOLONGAN} {$data.NM_PANGKAT}</td>
            <td>
                <span class="ui-widget ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;margin-left:15px;" 
                      onclick="$('#dialog-edit').dialog('open').load('pajak-honor.php?mode=edit&id={$data.ID_PAJAK_HONOR}')">Edit</span>
            </td>
        </tr>
    {foreachelse}
        <tr class="ui-widget-content">
            <th colspan="5" class="data-kosong" style="text-align: center;">Data Kosong</th>
        </tr>
    {/foreach}
    <tr class="ui-widget-content">
        <td colspan="5" class="center">
            <span class="ui-widget ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;margin-left:15px;margin-top:5px;" 
                  onclick="$('#dialog-add').dialog('open').load('pajak-honor.php?mode=add')">Tambah</span>
        </td>
    </tr>
</table>	
<div id="dialog-add" title="Tambah Master Pajak Honor"></div>
<div id="dialog-edit" title="Edit Master Pajak Honor"></div>
{literal}
    <script type="text/javascript">
        $('#dialog-add').dialog({
            width:'40%',
            modal: true,
            resizable:false,
            autoOpen:false
        });
        $('#dialog-edit').dialog({
            width:'40%',
            modal: true,
            resizable:false,
            autoOpen:false
        });
    </script>
{/literal}
