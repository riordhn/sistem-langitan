<form id="form_edit" method="post" action="pajak-honor.php">
    <table class="ui-widget" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="text-align: center;">Edit Pajak Honor</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Besar Pajak</td>
            <td>
                <input type="text" name="pajak" class="required number" value="{$data_pajak_honor.BESAR_PAJAK}" size="3" maxlength="3" />%
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Besar Pajak NPWP</td>
            <td>
                <input type="text" name="pajak_npwp" class="required number" value="{$data_pajak_honor.BESAR_PAJAK_NPWP}"size="3" maxlength="3" />%
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Golongan dan Pangkat</td>
            <td>
                <select name="golongan">
                    {foreach $data_golongan as $data}
                        <option value="{$data.ID_GOLONGAN}" {if $data.ID_GOLONGAN==$data_pajak_honor.ID_GOLONGAN}selected="true"{/if}>{$data.NM_GOLONGAN} {$data.NM_PANGKAT}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" class="center">
                <input type="hidden" name="mode" value="edit"/>
                <input type="hidden" name="id" value="{$data_pajak_honor.ID_PAJAK_HONOR}"/>
                <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Edit"/>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
            $('#form_edit').validate();            
            $('#form_edit').submit(function(){
                if($('#form_edit').valid()){
                    $('#dialog-edit').dialog('close');
                }
                else{
                    return false;
                }
            });
    </script>
{/literal}