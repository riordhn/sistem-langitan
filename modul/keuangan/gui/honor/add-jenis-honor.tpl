<form id="form_add" method="post" action="jenis-honor.php">
    <table class="ui-widget" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="text-align: center;">Tambah Jenis Honor</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Nama Jenis Honor</td>
            <td>
                <textarea name="nama" class="required"></textarea>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Tipe Jenis Honor</td>
            <td>
                <select name="tipe">
                    <option value="1" >Dosen</option>
                    <option value="2" >Karyawan</option>
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" class="center">
                <input type="hidden" name="mode" value="add"/>
                <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Tambah"/>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
            $('#form_add').validate();            
            $('#form_add').submit(function(){
                if($('#form_add').valid()){
                    $('#dialog-add').dialog('close');
                }
                else{
                    return false;
                }
            });
    </script>
{/literal}