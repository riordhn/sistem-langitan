<div class="center_title_bar">Denda Pembayaran Mahasiswa</div> 
<form method="post" id="form" action="denda-biaya.php">
    <div id="alert" class="ui-widget" style="display:none;margin-left:15px;">
        <div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;"> 
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                Data Harus Diisi.</p>
        </div>
    </div>
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="3" class="header-coloumn">Input Form</th>
        </tr>
        <tr class="ui-widget-content">
            <td>NIM Mahasiswa</td>
            <td><input id="nim" type="text" name="cari_nim"/></td>
            <td>
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Cari"/>
            </td>
        </tr>
    </table>
</form>
{if isset($cari_nim)}
    <form action="denda-biaya.php?mode=biaya" method="post">
        <table class="ui-widget">
            <tr class="ui-widget-header">
                <th colspan="10" class="header-coloumn" style="text-align: center;">Biaya Mahasiswa Detail</th>
            </tr>
            <tr class="ui-widget-header">
                <th>Nama Biaya</th>
                <th>Besar Biaya</th>
                <th>Denda Biaya</th>
                <th>Tagihkan</th>
                <th>Tanggal Bayar</th>
                <th>Nama Bank</th>
                <th>Via Bank</th>
                <th>No Transaksi</th>
                <th>Keterangan</th>
                <th>Semester Pembayaran</th>
            </tr>
            {$index=1}
            {foreach $data_pembayaran as $data}
                {if $data['NM_BIAYA']}
                    <tr class="ui-widget-content">
                        <td>
                            <input type="hidden" name="id{$data@index+1}" value="{$data['ID_PEMBAYARAN']}"/>
                            {$data['NM_BIAYA']}
                        </td>
                        <td>
                            {number_format($data['BESAR_BIAYA'])}
                        </td>
                        <td>
                            <input type="text" size="8" name="denda_biaya{$data@index+1}" value="{number_format($data['DENDA_BIAYA'])}"/>
                        </td>
                        <td>
                            {if $data['IS_TAGIH']=='Y'}
								Ya
                            {else}
								Tidak
                            {/if}
                        </td>
                        <td>
                            {$data.NM_BANK}
                        </td>
                        <td>
                            {$data.NM_BANK_VIA}
                        </td>
                        <td>
                            {$data['TGL_BAYAR']}
                        </td>
                        <td>
                            {$data['NO_TRANSAKSI']}
                        </td>
                        <td>
                            {$data['KETERANGAN']}
                        </td>
                        <td>    
                            {$data['NM_SEMESTER']} ({$data['TAHUN_AJARAN']}) 
                        </td>
                    </tr>
                {else}
                    <tr class="ui-widget-content">
                        <td colspan="10" style="text-align: center;">Data Pembayaran Kosong</td>
                    </tr>
                {/if}
                {$index=$data@index+1}
            {/foreach}
            <tr class="ui-widget-content">
                <td colspan="10">
                    <input type="hidden" name="cari_nim" value="{$data_pembayaran[0]['NIM_MHS']}"/>
                    <input type="hidden" name="jumlah" value="{$index}"/>
                    <input style="float: right;" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Set Denda"/>
                </td>
            </tr>
        </table>
    </form>
{/if}