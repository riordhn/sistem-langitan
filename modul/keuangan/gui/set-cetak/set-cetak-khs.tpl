<div class="center_title_bar">Set Cetak KHS Mahasiswa</div>
<form method="get" id="form" action="set-cetak-khs.php">
	<div id="alert" class="ui-widget" style="display:none;margin-left:15px;">
		<div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;"> 
			<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
				Data Harus Diisi.</p>
		</div>
	</div>
	<table class="ui-widget">
		<tr class="ui-widget-header">
			<th colspan="4" class="header-coloumn">Input Form</th>
		</tr>
		<tr class="ui-widget-content">
			<td>NIM</td>
			<td><input id="cari" type="search" name="cari" value="{if isset($smarty.get.cari)}{$smarty.get.cari}{/if}"/></td>
			<td><input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;" value="Tampilkan"/></td>
		</tr>
	</table>
</form>
{if isset($smarty.get.cari)}
<div>
	<table style="float: left;" class="ui-widget">
		<tr class="ui-widget-header">
			<th colspan="2" class="header-coloumn" style="text-align: center">Detail Biodata Mahasiswa</th>
		</tr>
		<tr class="ui-widget-content">
			<td>NIM</td>
			<td width="200px">{$data_detail_mahasiswa.NIM_MHS}</td>
		</tr>
		<tr class="ui-widget-content">
			<td>Nama</td>
			<td>{$data_detail_mahasiswa.NM_PENGGUNA}</td>
		</tr>
		<tr class="ui-widget-content">
			<td>Fakultas</td>
			<td>{$data_detail_mahasiswa.NM_FAKULTAS}</td>
		</tr>
		<tr class="ui-widget-content">
			<td>Program Studi</td>
			<td>{$data_detail_mahasiswa.NM_JENJANG} - {$data_detail_mahasiswa.NM_PROGRAM_STUDI}</td>
		</tr>
		<tr class="ui-widget-content">
			<td>Jalur - Angkatan</td>
			<td>{$data_detail_mahasiswa.NM_JALUR} - {$data_detail_mahasiswa.THN_ANGKATAN_MHS}</td>
		</tr>
		<tr class="ui-widget-content">
			<td>Kelas</td>
			<td>{$data_detail_mahasiswa.NAMA_KELAS}</td>
		</tr>
	</table>
	<p></p>

	<form action="set-cetak-khs.php?cari={$smarty.get.cari}" method="post">
            <table style="display: inline-block;" class="ui-widget">
                <tbody>
                	<tr class="ui-widget-header">
						<th colspan="3" class="header-coloumn" style="text-align: center">Pilih Semester</th>
					</tr>
                    <tr class="ui-widget-content">
                        <td>
                            <label><strong>Semester</strong></label>
                        </td>
                        <td>
                        	<label><strong> : </strong></label>
                        </td>
                        <td>
                            <select name="id_semester" onchange="javascript: $(this).submit();">
                                <option value="">-- Pilih Semester --</option>
                                {foreach $semester_set as $sem}
                                    <option value="{$sem.ID_SEMESTER}" {if isset($smarty.post.id_semester)}{if $smarty.post.id_semester == $sem.ID_SEMESTER}selected{/if}{/if}>{$sem.SEMESTER}</option>
                                {/foreach}
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
    </form>
</div>
    {if in_array($status_cetak,[0,10])}
    	<form action="set-cetak-khs.php?cari={$smarty.get.cari}" method="post">

			<table class="ui-widget">

				<tr class="ui-widget-content">
					<td colspan="5">Mahasiswa Pada Semester Ini <strong>Belum Ter-Set</strong> Bisa Mencetak KHS</td>
				</tr>

				<tr class="ui-widget-content">
					<td colspan="5" class="center total">
					<input type="hidden" name="id_semester" value="{$id_semester}" />
					<input type="hidden" name="status_cetak" value="{$status_cetak}" />
					<input type="hidden" name="mode" value="set_cetak" />
					<input type="submit" value="Set Cetak KHS" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" /></td>
				</tr>

			</table>
		</form>
    {elseif $status_cetak == 1}
    	<form action="set-cetak-khs.php?cari={$smarty.get.cari}" method="post">

			<table class="ui-widget">

				<tr class="ui-widget-content">
					<td colspan="5">Mahasiswa Pada Semester Ini <strong>Sudah Ter-Set</strong> Bisa Mencetak KHS</td>
				</tr>

				<tr class="ui-widget-content">
					<td colspan="5" class="center total">
					<input type="hidden" name="id_semester" value="{$id_semester}" />
					<input type="hidden" name="status_cetak" value="{$status_cetak}" />
					<input type="hidden" name="mode" value="hapus_set_cetak" />
					<input type="submit" value="Hapus Set Cetak KHS" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" /></td>
				</tr>

			</table>
		</form>
    {/if}


	<!-- <form action="set-cetak-khs.php?cari={$smarty.get.cari}" method="post">

		<table class="ui-widget">
			<tr class="ui-widget-content">
				<td>Semester</td>
				<td colspan="3">
					<input type="hidden" name="mode" value="simpan" />
					<select name="semester[]" data-id="1">
						<option>--</option>
						{foreach $semester_set as $s}
							<option value="{$s.ID_SEMESTER}">{$s.SEMESTER}</option>
						{/foreach}
					</select>
				</td>
			</tr>


			<tr class="ui-widget-content">
				<td colspan="5" class="center total"><input type="submit" value="Submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" /></td>
			</tr>

		</table>
	</form> -->

	<!-- <span class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" onclick="history.back(-1)">Kembali</span> -->
{/if}
{literal}
	<script>
		
		$(document).ready(function() {

			$('#form').submit(function() {
				if ($('#cari').val() == '') {
					$('#alert').fadeIn();
					return false;
				}
			});

			$( ".date_pick" ).datepicker({dateFormat:'dd/mm/yy'});

			/* Semester OnChange */
			$('[name="semester[]"]').change(function(){
				var id_semester = $(this).val();
				var nim = $('[name="cari"]').val();
				var index = $(this).data('id');

				/* Ambil detail tagihan */
				$.ajax({
					url: 'set-cetak-khs.php?mode=get-detail-tagihan&id_semester='+id_semester+'&nim='+nim,
					dataType: 'json',
					success: function(data) {
						console.log(data);
					}
				});
			});
		});
	</script>
{/literal}