<div class="center_title_bar">Kontrol Rekonsiliasi</div>

{if $smarty.get.mode==''}
    <form method="get" id="rekon" action="kontrol-rekon.php">
        <table class="ui-widget" style="width:45%;">
            <tr class="ui-widget-header">
                <th colspan="2" class="header-coloumn">Parameter</th>
            </tr>
            <tr class="ui-widget-content">
                <td style="width: 50%;" class="center">Tahun</td>
                <td style="width: 50%;" class="center">
                    <select name="thn" onchange="$('#rekon').submit()">
                        <option value="">Pilih Tahun</option>
                        {foreach $data_tahun as $t}
                            <option value="{$t.TAHUN}" {if $t.TAHUN==$smarty.get.thn}selected="true"{/if}>{$t.TAHUN}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
        </table>
    </form>
    <table class="ui-widget-content ui-widget" style="width: 70%">
        <tr class="ui-widget-header">
            <th colspan="5"class="header-coloumn">KONTROL REKONSILIASI PEMBAYARAN</th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>Bulan Rekon</th>
            <th>Tahun Rekon</th>
            <th>Status Rekon</th>
            <th>Detail Perubahan</th>
        </tr>
        {foreach $data_rekon as $data}
            <tr>
                <td>{$data@index+1}</td>
                <td>
                    {if $data.BULAN_REKON==1}
                        Januari
                    {else if $data.BULAN_REKON==2}
                        Februari
                    {else if $data.BULAN_REKON==3}
                        Maret
                    {else if $data.BULAN_REKON==4}
                        April
                    {else if $data.BULAN_REKON==5}
                        Mei
                    {else if $data.BULAN_REKON==6}
                        Juni
                    {else if $data.BULAN_REKON==7}
                        Juli
                    {else if $data.BULAN_REKON==8}
                        Agustus
                    {else if $data.BULAN_REKON==9}
                        September
                    {else if $data.BULAN_REKON==10}
                        Oktober
                    {else if $data.BULAN_REKON==11}
                        November
                    {else if $data.BULAN_REKON==12}
                        Desember
                    {/if}
                </td>
                <td class="center">{$data.TAHUN_REKON}</td>
                <td class="center">
                    {if $data.STATUS_REKON==0}
                        <form method="post" action="kontrol-rekon.php?{$smarty.server.QUERY_STRING}">
                            <input type="hidden" name="mode" value="lock"/>
                            <input type="hidden" name="id" value="{$data.ID_REKON_PEMBAYARAN}"/>
                            <input type="submit" name="submit" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;" value="Kunci Rekon"/>
                        </form>
                    {else}                        
                        <form method="post" action="kontrol-rekon.php?{$smarty.server.QUERY_STRING}">
                            <input type="hidden" name="mode" value="unlock"/>
                            <input type="hidden" name="id" value="{$data.ID_REKON_PEMBAYARAN}"/>
                            <input type="submit" name="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Buka Rekon"/>
                        </form>
                    {/if}
                </td>
                <td class="center">
                    <a href="kontrol-rekon.php?mode=detail&id={$data.ID_REKON_PEMBAYARAN}"class="ui-button ui-corner-all ui-state-hover" style="padding:6px;cursor:pointer;margin:2px;">View</a>
                    {if $data.STATUS_REKON==1}
                        <br/>
                        <span
                            {if $data.PERUBAHAN>0}
                                style="color: red"
                            {else}
                                style="color: green"
                            {/if}
                            >Terdapat {$data.PERUBAHAN} Perubahan data 
                        </span>
                    {/if}
                </td>
            </tr>
        {foreachelse}
            <tr class="ui-widget-content">
                <td colspan="5" class="center"><span style="color: red">Data Kosong</span></td>
            </tr>
        {/foreach}
    </table>
{else if $smarty.get.mode=='detail'}
    <a class="disable-ajax ui-button ui-corner-all ui-state-hover" style="padding:6px;cursor:pointer;margin:5px 15px;" onclick="history.back(-1)">Back</a>
    <table class="ui-widget-content ui-widget" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="6"class="header-coloumn">PERUBAHAN REKONSILIASI</th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>Log Perubahan</th>
            <th>Status Perubahan</th>
            <th>Waktu Perubahan</th>
            <th>Perubah</th>
            <th>Konfirmasi</th>
        </tr>
        {foreach $data_perubahan as $d}
            <tr>
                <td>{$d@index+1}</td>
                <td>
                    <p>NAMA : {$d.NM_PENGGUNA} , NIM : {$d.NIM_MHS}, PRODI : {$d.NM_JENJANG} {$d.NM_PROGRAM_STUDI}</p>
                    <p>
                        TGL BAYAR : {$d.TGL_BAYAR}, BANK : {$d.NM_BANK} via {$d.NAMA_BANK_VIA}, NO TRANSAKSI : {$d.NO_TRANSAKSI}<br/>
                        KETERANGAN : {$d.KETERANGAN}
                    </p>
                </td>
                <td class="center">
                    {if $d.STATUS_PERUBAHAN==1}
                        Update Group dari History Bayar
                    {else if $d.STATUS_PERUBAHAN==2}
                        Update Detail dari History Bayar
                    {else if $d.STATUS_PERUBAHAN==3}
                        Tambah Dari Upload Pembayaran
                    {else if $d.STATUS_PERUBAHAN==4}
                        Hapus dari History Bayar
                    {else if $d.STATUS_PERUBAHAN==5}
                        Update dari Upload Pembayaran
                    {/if}
                </td>
                <td class="center">{$d.TGL_PERUBAHAN}</td>
                <td class="center">{$d.PERUBAH}</td>
                <td class="center">
                    {if $d.STATUS_CLEAR==0}
                        <form method="post" action="kontrol-rekon.php?{$smarty.server.QUERY_STRING}">
                            <input type="hidden" name="mode" value="clear"/>
                            <input type="hidden" name="id" value="{$d.ID_REKON_PERUBAHAN}"/>
                            <input type="submit" value="Clear" style="padding:6px;cursor:pointer;" class="ui-button ui-corner-all ui-state-hover"/>
                        </form>
                    {else}
                        Sudah Clear
                    {/if}
                </td>
            </tr>
        {foreachelse}
            <tr class="ui-widget-content">
                <td colspan="6" class="center"><span style="color: red">Data Kosong</span></td>
            </tr>
        {/foreach}
    </table>
{/if}