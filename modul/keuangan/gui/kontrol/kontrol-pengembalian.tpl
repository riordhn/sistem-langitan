<div class="center_title_bar">Laporan Pengembalian Pembayaran</div> 
<form method="get" id="report_form" action="kontrol-pengembalian.php">
    <table class="ui-widget" style="width:90%;">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td width="15%">Fakultas</td>
            <td width="25%">
                <select name="fakultas" id="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
            <td  width="15%">Program Studi</td>
            <td width="45%">
                <select name="prodi" id="prodi">
                    <option value="">Semua</option>
                    {foreach $data_prodi as $data}
                        <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$smarty.get.prodi}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                </select>
            </td>

        </tr>
        <tr class="ui-widget-content">
            <td>Pengembalian Pembayaran Pada Semester</td>
            <td>
                <select name="semester">
                    {foreach $data_semester as $semester}
                        <option value="{$semester['ID_SEMESTER']}" {if $semester.ID_SEMESTER==$smarty.get.semester} selected="true" {/if}>{$semester['NM_SEMESTER']}({$semester['TAHUN_AJARAN']})</option>
                    {/foreach}
                </select>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr class="center ui-widget-content">
            <td colspan="6">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" name="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if isset($data_pengembalian)}
    <table class="ui-widget-content ui-widget" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="13"class="header-coloumn">KONTROL PENGEMBALIAN PEMBAYARAN</th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>NIM</th>
            <th>Nama</th>
            <th>Prodi</th>
            <th>Status</th>
            <th>Semester Pembayaran</th>
            <th>Besar Pembayaran</th>
            <th>Semester Pengembalian</th>
            <th>Besar Pengembalian</th>
            <th>Tgl Pengembalian</th>
            <th>Bank</th>
            <th>Keterangan</th>
            <th>Detail</th>
        </tr>
        {$index=1}
        {foreach $data_pengembalian as $data}

            <tr>
                <td>{$index}</td>
                <td>{$data.NIM_MHS}</td>
                <td>{$data.NM_PENGGUNA}</td>
                <td>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</td>
                <td>{$data.NM_STATUS_PENGGUNA}</td>
                <td>{$data.NM_SEMESTER_BAYAR} ({$data.TAHUN_AJARAN_BAYAR})</td>
                <td>{number_format($data.BESAR_PEMBAYARAN)}</td>
                <td>{$data.NM_SEMESTER} ({$data.TAHUN_AJARAN})</td>
                <td>{number_format($data.BESAR_PENGEMBALIAN)}</td>
                <td>{$data.TGL_PENGEMBALIAN}</td>
                <td>{$data.NM_BANK}</td>
                <td>{$data.KETERANGAN}</td>
                <td class="center">
                    <a href="history-bayar.php?cari={$data.NIM_MHS}"class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;">Detail</a>
                </td>
            </tr>
            {$index=$index+1}
        {foreachelse}
            <tr class="ui-widget-content">
                <td colspan="13" class="center"><span style="color: red">Data Kosong</span></td>
            </tr>
        {/foreach}
    </table>
{/if}
{literal}
    <script>
            $('#fakultas').change(function(){
                    $.ajax({
                            type:'post',
                            url:'getProdi.php',
                            data:'id_fakultas='+$(this).val(),
                            success:function(data){
                                    $('#prodi').html(data);
                            }                    
                    })
            });
    </script>
{/literal}