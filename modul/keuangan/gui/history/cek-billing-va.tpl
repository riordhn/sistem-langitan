
{if $status==1}
<p style="text-align:center;color:green"><b>{$message}</b></p>
<table class="ui-widget" style="width:98%">
    <tr class="ui-widget-content">
        <td>Nama</td>
        <td>:</td>
        <td class="left">
            <b>{$data.customer_name}</b>
        </td>
    </tr>
    <tr class="ui-widget-content">
        <td>No VA</td>
        <td>:</td>
        <td class="left">
            <b>{$data.virtual_account}            
        </td>
    </tr>
    <tr class="ui-widget-content">
        <td>ID Transaksi</td>
        <td>:</td>
        <td class="left">            
            <b>{$data.trx_id}</b>
        </td>
    </tr>
    <tr class="ui-widget-content">
        <td>Jumlah Tagihan</td>
        <td>:</td>
        <td class="left">
            <b>{number_format($data.trx_amount-$data.payment_amount)}</b>
        </td>
    </tr>
</table>
{else}
    <h3 style="text-align:center;color:red">GAGAL</h3>
    <p style="text-align:center;color:red"><b>{$message}</b></p>
{/if}