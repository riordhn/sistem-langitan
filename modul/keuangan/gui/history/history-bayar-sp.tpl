<table class="ui-widget-content ui-widget" style="width: 97%">
  <caption>
    Semester Pendek
  </caption>
  <tr class="ui-widget-header">
    <th colspan="9" class="header-coloumn">
      RIWAYAT PEMBAYARAN SEMESTER PENDEK
    </th>
  </tr>
  <tr class="ui-widget-header">
    <th>No</th>
    <th>Semester</th>
    <th>Besar Pembayaran</th>
    <th>Tgl Pengembalian</th>
    <th>Bank</th>
    <th>Via Bank</th>
    <th>No Transaksi</th>
    <th>Keterangan</th>
    <th>Status</th>
  </tr>
  {foreach $data_pembayaran_sp as $sp}
  <tr>
    <td>{$sp@index+1}</td>
    <td>{$sp.NM_SEMESTER} {$sp.GROUP_SEMESTER} ({$sp.TAHUN_AJARAN})</td>
    <td>{number_format($sp.BESAR_BIAYA)}</td>
    <td>{$sp.TGL_BAYAR}</td>
    <td>{$sp.NM_BANK}</td>
    <td>{$sp.NAMA_BANK_VIA}</td>
    <td>{$sp.NO_TRANSAKSI}</td>
    <td>{$sp.KETERANGAN}</td>
    <td>{$sp.NAMA_STATUS}</td>
  </tr>
  {foreachelse}
  <tr class="ui-widget-content">
    <td colspan="9" class="center">
      <span style="color: red">Data Kosong</span>
    </td>
  </tr>
  {/foreach}
  <tr>
    <td class="center" colspan="9">
      <a
        class="disable-ajax ui-button ui-corner-all ui-state-hover"
        target="_blank"
        style="padding:4px;cursor:pointer;margin:2px;"
        href="cetak-history-bayar-sp.php?cari={$data_detail_mahasiswa.NIM_MHS}"
      >
        Cetak
      </a>
    </td>
  </tr>
</table>
