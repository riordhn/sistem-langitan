<div class="center_title_bar">History Pembayaran Mahasiswa</div>
<form method="get" id="form" action="history-bayar.php">
  {if !empty($error)}
    {$error}
  {/if}
  <div id="alert" class="ui-widget" style="display:none;margin-left:15px;">
    <div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;">
      <p>
        <span
          class="ui-icon ui-icon-alert"
          style="float: left; margin-right: .3em;"
        ></span>
        Data Harus Diisi.
      </p>
    </div>
  </div>
  <table class="ui-widget">
    <tr class="ui-widget-header">
      <th colspan="4" class="header-coloumn">Input Form</th>
    </tr>
    <tr class="ui-widget-content">
      <td>NIM/NO Ujian Mahasiswa</td>
      <td>
        <input
          id="cari"
          type="search"
          name="cari"
          value="{if isset($smarty.get.cari)}{$smarty.get.cari}{/if}"
        />
      </td>
      <td>
        <input
          type="submit"
          class="btn btn-warning"
          style="margin:2px;"
          value="Tamplikan"
        />
      </td>
    </tr>
  </table>
</form>
