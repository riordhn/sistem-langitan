<form id="form_ubah_master_tagihan" action="history-bayar.php?cari={$cari}" method="post" >
    <div class="alert alert-primary" role="alert">
    <b>Syarat perubahan jumlah tagihan </b><br/>
    1. Jumlah tagihan <b>lebih dari atau sama dengan</b> jumlah yang tagihan yang terproses<br/>
    2. Jika ada pembayaran status <b>Penangguhan</b>/<b>Pembebasan</b> silahkan dihapus dulu
    </div>
    <table class="ui-widget" style="width:98%">
        <tr class="ui-widget-header">
            <th>NO</th>
            <th>Nama Biaya</th>
            <th>Jumlah Tagihan</th>
        </tr>
        {$total_tagihan=0}
        {foreach $data_detail_biaya_tagihan as $d}
        <tr class="ui-widget-content">
            <td style="width:10%">{$d@index+1}</td>
            <td style="width:50%">{$d['NM_BIAYA']}</td>
            <td style="width:40%" class="text-right">
                <b>Sudah di proses bayar : {number_format($d['TOTAL_TERBAYAR'])}</b>
                <br/>
                <input type="hidden"  name="id_tagihan{$d@index+1}" value="{$d['ID_TAGIHAN']}"/>
                <input style="text-align:right" type="number" min="{$d['TOTAL_TERBAYAR']}" class="biaya_dibayar_tagihan" id="biaya_dibayar_tagihan{$d@index+1}" data-id="{$d@index+1}" name="besar_biaya{$d@index+1}" value="{$d['BESAR_BIAYA']}"/>
            </td>
        </tr>
        {$total_tagihan=$total_tagihan+$d['BESAR_BIAYA']}
        {/foreach}
        <tr class="ui-widget-content bg-info">
            <td colspan="2">TOTAL TAGIHAN</td>
            <td class="text-right">
                <input style="text-align:right" type="number" readonly="true" name="total_tagihan" id="total_tagihan" value="{$total_tagihan}"/>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="center total">
                <input type="hidden" id="is_va_ubah_tagihan" name="is_va" value="{$is_va}"/>
                <input type="hidden" name="id_tagihan_mhs" value="{$id_tagihan_mhs}"/>
                <input type="hidden" name="jumlah_data" value="{count($data_detail_biaya_tagihan)}"/>
                <input type="hidden" name="mode" value="ubah_master"/>
                <input type="submit" id="btn-submit-master" value="SIMPAN" class="ui-button ui-corner-all ui-state-hover"/>
            </td>
        </tr>
    </table>
</form>
<script>
    $('.biaya_dibayar_tagihan').keyup(function(){
        var total_tagihan =0;
        $('.biaya_dibayar_tagihan').each(function(){
            var id=$(this).attr('data-id');
            var value=$('#biaya_dibayar_tagihan'+id).val();
            total_tagihan+=parseInt(value);
        });
        console.log(total_tagihan);
        $("#total_tagihan").val(total_tagihan);
    })
    $("#btn-submit-master").click(function(e) {
        e.preventDefault();
        var form_ubah_master_tagihan=$('#form_ubah_master_tagihan');
        console.log($('#is_va_ubah_tagihan').val());
        if(form_ubah_master_tagihan.valid()){                
            form_ubah_master_tagihan.submit();
            $("#ubah_master_tagihan").dialog("close");
        }
        
    });
</script>