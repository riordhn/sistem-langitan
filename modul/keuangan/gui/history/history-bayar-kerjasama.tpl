<table class="ui-widget-content ui-widget" style="width: 97%">
    <caption>Kerjasama / Beasiswa</caption>
    <tr class="ui-widget-header">
        <th colspan="6"class="header-coloumn">RIWAYAT KERJASAMA MAHASISWA</th>
    </tr>
    <tr class="ui-widget-header">
        <th>No</th>
        <th>Jenis Beasiswa</th>
        <th>Semester Mulai</th>
        <th>Semester Selesai</th>
        <th>Keterangan</th>
        <th>Beasiswa Aktif</th>
    </tr>
    {$index=1}
    {foreach $data_sejarah_kerjasama as $data}                
        <tr>
            <td>{$index}</td>
            <td class="center">{$data.NM_BEASISWA} Dari {$data.PENYELENGGARA_BEASISWA} Tahun {$data.PERIODE_PEMBERIAN_BEASISWA}</td>
            <td class="center">{$data.SEMESTER_MULAI}</td>
            <td class="center">{$data.SEMESTER_SELESAI}</td>
            <td class="center">{$data.KETERANGAN}</td>
            <td class="center">{if $data.BEASISWA_AKTIF==1} Aktif{else}Tidak Aktif{/if}</td>
        </tr>
        {$index=$index+1}
    {foreachelse}
        <tr class="ui-widget-content">
            <td colspan="6" class="center"><span style="color: red">Data Kosong</span></td>
        </tr>
    {/foreach}
</table>