<div class="center_title_bar">Riwayat Pembayaran Virtual Account</div>
<span class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:5px 15px;font-weight:bold" onclick="history.back(-1)">KEMBALI</span>
<!--DETAIL BIODATA MAHASISWA-->
{include file='history/history-bayar-biodata.tpl'}
<p></p>
<table class="ui-widget" style="width:70%;font-size:1.2em">
    <caption>
    INFORMASI TAGIHAN <br/>
    SEMESTER {$data_tagihan_pembayaran[0]['SEMESTER']}
    <hr>
    
    </caption>
    <tr class="ui-widget-header">
        <th>NO</th>
        <th>Nama Biaya</th>
        <th>Jumlah Tagihan</th>
        <th>Jumlah Terbayar</th>
        <th>Lunas</th>
    </tr>
        {$total_tagihan=0}
        {$total_terbayar=0}    
        {foreach $data_tagihan_pembayaran as $d}
        <tr class="ui-widget-content">
            <td>{$d@index+1}</td>
            <td>{$d['NAMA_BIAYA']}</td>
            <td style="text-align:right">{number_format($d['BESAR_TAGIHAN'])}</td>
            <td style="text-align:right">{number_format($d['BESAR_TERBAYAR'])}</td>
            <td class="center">{if $d['IS_LUNAS']=='1'&&count($data_riwayat_pembayaran)>0} <b style='font-size:1.2em;color:green'>Sudah</b> {else} <b style='font-size:1.2em;color:red'>Belum</b>{/if}</td>        
        </tr>
        {$total_tagihan=$total_tagihan+$d['BESAR_TAGIHAN']}
        {$total_terbayar=$total_terbayar+$d['BESAR_TERBAYAR']}
        {/foreach}
    <tr class="total" >
        <td colspan="2">TOTAL</td>
        <td style="text-align:right">{number_format($total_tagihan)}</td>
        <td style="text-align:right">{number_format($total_terbayar)}</td>
        <td></td>
    </tr>
</table>
<table class="ui-widget" style="width:70%;font-size:1.2em">
    <caption>
    RIWAYAT PEMBAYARAN VA BANK
    <hr/>
    </caption>
    {foreach $data_pembayaran_va as $d}
    <tr class="ui-widget-content">
        <td colspan=5>
        <p style="font-size:1.1em">
            <b style="color:#c42700"><u>PEMBAYARAN KE-{$d@index+1} </u></b> <br/>
            <b>WAKTU BAYAR</b> : {date("d/m/Y H:i:s",strtotime($d.PAYMENT_TIME))} <br/>
            <b>NO.TRANSAKSI</b> : {$d.PAYMENT_NTB} <br/>
            <b>DIBAYARKAN KE</b> : {$d.NM_BANK} <br/>
            <b>TOTAL TAGIHAN</b> : {number_format($d.TRX_AMOUNT)} <br/>
            <b>TOTAL TERBAYAR</b> : {number_format($d.CUMULATIVE_PAYMENT_AMOUNT)} <br/>
            <b>DIBAYAR</b> : {number_format($d.PAYMENT_AMOUNT)} <br/>
            <b>KETERANGAN</b> : {$d.KETERANGAN} <br/>
            <hr/>
            {if $d.NOTES!=''}
              <b style="color:green">SUDAH DI PROSES</b>
            {else}
            <span onclick="ajax_get_page_dialog('update-biaya-va.php?id_tagihan_mhs={$id_tagihan_mhs}&cari={$data.NIM_MHS}&no_transaksi={$d.PAYMENT_NTB}','#update_biaya')" class="ui-button ui-corner-all ui-state-active" style="padding:4px;cursor:pointer;margin:2px;color:blue">Update Biaya</span>           
            {/if}
            
        </p>
        </td>
    </tr>
    {foreachelse}
    <tr>
        <td colspan=5><label style="color:red">Tidak ada transaksi pembayaran</label></td>
    </tr>
    {/foreach}
</table>
<!--EDIT MASTER TAGIHAN-->
<div id="update_biaya" title="Update Biaya Pembayaran VA"></div>
{literal}
<script>
  var base_url = window.location.protocol+'//'+window.location.hostname;
  var htmlLoading ='<div style="width: 90%;height:auto;padding:15px" align="center"><img src="'+base_url+'/img/spinner.gif" /></div>'; 
  $("#update_biaya").dialog({
     width: "50%",
      height: "auto",
      modal: true,
      resizable: false,
      autoOpen: false
  });
  function ajax_get_page_dialog(url,element){
    $.ajax({
        url: url,
        type: "get",
        beforeSend: function() {          
          $(element).dialog('open')
          $(element).html(
            htmlLoading
          );
        },
        success: function(data) {
          $(element).html(data);
        }
      });
  }
  function confirm_delete(no_transaksi,nim,id_tagihan_mhs) {    
    var query_string=$('#query_string').val();
    var base_url = window.location.protocol+'//'+window.location.hostname;
    var status = confirm("Apakah anda yakin menghapus pembayaran ini?");
    if (status == true) {
      $.ajax({
        url: 'proses-pembayaran-va.php?'+query_string,
        type: "post",
        data: "mode=hapus_riwayat&no_transaksi=" +no_transaksi,
        beforeSend: function() {
          $("#center_content").html(
            '<div style="width: 100%;" align="center"><img src="'+base_url+'/js/loading.gif" /></div>'
          );
        },
        success: function(data) {
          $("#center_content").html(data);
        }
      });
    }
  }
</script>
{/literal}
