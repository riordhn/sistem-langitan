<table class="ui-widget" style="width: 97%;font-size: 0.9em">
    <caption>Pembayaran</caption>
    <tr class="ui-widget-header">
        <th colspan="10"> RIWAYAT PEMBAYARAN {if $jenis=='cmhs'} CALON {/if} MAHASISWA</th>
    </tr>
    <tr class="ui-widget-header">
        <th>No</th>
        <th>Semester</th>
        <th>Total Tagihan Biaya</th>
        <th>Total Terbayar</th>
        <th>Total Ditangguhkan</th>
        <th>Total Dibebaskan</th>
        <th>Total Belum Terbayar</th>
        <th>Lunas</th>
        <th>Detail Biaya</th>
        <th>Operasi</th>
    </tr>
    {foreach $data_pembayaran as $data}
        <tr {if $data.NM_SEMESTER=='Ganjil'}style="background-color: #c9f797"{else}style="background-color: beige"{/if}>
           <td>{$data@index+1}</td>
            <td>{$data.NM_SEMESTER}<br/> ({$data.TAHUN_AJARAN})</td>
            <td class="center">{number_format($data.TOTAL_BESAR_BIAYA)}</td>
            <td class="center">{number_format($data.TOTAL_TERBAYAR)}</td>
            <td class="center">{number_format($data.TOTAL_TERTANGGUH)}</td>
            <td class="center">{number_format($data.TOTAL_TERBEBAS)}</td>
            <td class="center">{number_format($data.TOTAL_BELUM_TERBAYAR)}</td>
            <td class="center">
                {if $data.TOTAL_BELUM_TERBAYAR>0||$data.TOTAL_TERTANGGUH>0} 
                    <b style="font-size:1.2em;color:red">Belum</b>
                {else} 
                    <b style="font-size:1.2em;color:green">Sudah</b>
                {/if}
                {if $data.TOTAL_TERTANGGUH>0}
                    <br/>
                    <b style="font-size:1.2em;color:orange">Ditangguhkan </b><br/>
                    {number_format($data.TOTAL_TERTANGGUH)}
                {/if}
            </td>
            <td>
                {if count($data.DETAIL_BIAYA)>0}
                    <ul>
                        {$total_tagihan=0}
                        {foreach $data.DETAIL_BIAYA as $db}
                            {if $db['BESAR_BIAYA']>0}
                                <li style="font-size:1.2em;font-size:bold">{$db['NM_BIAYA']} <span style="float:right">{number_format($db['BESAR_BIAYA'])}</span></li>
                            {/if}
                            {$total_tagihan=$total_tagihan+$db['BESAR_BIAYA']}
                        {/foreach}
                        <hr/>
                        <li style="font-size:1.2em;font-size:bold">TOTAL <span style="float:right">{number_format($total_tagihan)}</span></li>
                    </ul>
                    {if $data.TOTAL_TERBAYAR<$total_tagihan}
                        <span onclick="ajax_get_page_dialog('ubah-master-tagihan.php?id_tagihan_mhs={$data.ID_TAGIHAN_MHS}&cari={$data.NIM_MHS}&is_va=0','#ubah_master_tagihan')" class="btn btn-sm btn-warning " style="margin:2px;color:white;float:right" >Ubah Tagihan</span><br/>
                    {/if}
                {/if}
            </td>
            <td class="center">
                {if $data.TOTAL_BELUM_TERBAYAR>0||$data.TOTAL_TERTANGGUH>0} 
                    <span onclick="ajax_get_page_dialog('proses-pembayaran.php?id_tagihan_mhs={$data.ID_TAGIHAN_MHS}&cari={$data.NIM_MHS}','#proses_pembayaran')" class="btn btn-sm btn-primary" style="margin:2px;" >Proses</span><br/>
                {/if}
                <a href="riwayat-transaksi-pembayaran.php?cari={$smarty.get.cari}&id_tagihan_mhs={$data.ID_TAGIHAN_MHS}" class="btn btn-sm btn-success" style="margin:2px;color:white">Riwayat</a><br/>
                {if $data.TOTAL_TERBAYAR>0}                    
                    {* <span class="btn btn-sm btn-secondary" style="margin:2px;" 
                    onclick="window.open('cetak-history-bayar-rinci.php?id_tagihan_mhs={$data.ID_TAGIHAN_MHS}&cari={$smarty.get.cari}','_blank')">Cetak
                    </span> 
                    <br/> *}
                {/if}
                {if $data.TOTAL_TERBAYAR>0||$data.TOTAL_TERTANGGUH>0||$data.TOTAL_TERBEBAS>0}
                    <span onclick="confirm_reset('{$data.NIM_MHS}','{$data.ID_TAGIHAN_MHS}')" class="btn btn-sm btn-danger" style="margin:2px;">
                    Reset
                    </span>
                    <br/> 
                {/if}
                {if $data.TOTAL_TERBAYAR==0&&$data.TOTAL_TERTANGGUH==0&&$data.TOTAL_TERBEBAS==0}
                    <span onclick="confirm_delete('{$data.NIM_MHS}','{$data.ID_TAGIHAN_MHS}')" class="btn btn-sm btn-danger" style="margin:2px;">
                    Hapus
                    </span>
                    <br/> 
                {/if}
            </td>
        </tr>
    {foreachelse}
        <tr class="total ui-widget-content">
            <td class="center" colspan="9">
                <span style="color:red">
                    Data Pembayaran Kosong
                </span>
            </td>
        </tr>
    {/foreach}
    <tr class="ui-widget-content">
        <td class="center" colspan="10">
            <span class="btn btn-sm btn-secondary" style="margin:2px;color:white" 
                  onclick="window.open('cetak-history-bayar.php?cari={$data_detail_mahasiswa.NIM_MHS}&mode=all&jenis={$jenis}','_blank')"
                  >Cetak Riwayat Pembayaran
            </span>
            <br/>
            <hr style="color: maroon"/>
            {if $jenis=='mhs'}
                <!--
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" href="status-bayar.php?cari={$data_detail_mahasiswa.NIM_MHS}">
                    Riwayat Status Pembayaran
                </a>                
                -->
                <a class="btn btn-sm btn-secondary" style="margin:2px;color:white" href="biaya-mahasiswa.php?cari={$data_detail_mahasiswa.NIM_MHS}">
                    Master Biaya
                </a>
                <a class="btn btn-sm btn-secondary" style="margin:2px;color:white" href="pengembalian-pembayaran.php?cari={$data_detail_mahasiswa.NIM_MHS}">
                    Pengembalian Pembayaran
                </a>
            {/if}
            <br/>
            <hr style="color: maroon"/>
            <span onclick="ajax_get_page_dialog('tambah-pembayaran.php?cari={$smarty.get.cari}','#tambah_pembayaran')" class="btn btn-sm btn-info" style="margin:2px;color:white">Tambah Tagihan / Semester</span>
            <span onclick="ajax_get_page_dialog('tambah-detail-pembayaran.php?cari={$smarty.get.cari}','#tambah_detail_pembayaran')" class="btn btn-sm btn-info" style="margin:2px;color:white">Tambah Tagihan / Biaya</span>
        </td>
    </tr>
</table>
<!--RIWAYAT TRANSAKSI MAHASISWA-->
<div id="riwayat_transaksi_pembayaran" title="Riwayat Transaksi Pembayaran"></div>
<!--TAMBAH DETAIL PEMBAYARAN MAHASISWA-->
<div id="proses_pembayaran" title="Proses Pembayaran"></div>
<!--TAMBAH PEMBAYARAN MAHASISWA-->
<div id="tambah_pembayaran" title="Tambah Pembayaran"></div>
<!-- UPDATE DAN UPLOAD BUKTI TRANSAKSI-->
<div id="update_upload_transaksi" title="Update dan Upload Transaksi Pembayaran"></div>
<!--EDIT MASTER TAGIHAN-->
<div id="ubah_master_tagihan" title="Ubah Master Tagihan"></div>
<!--TAMBAH DETAIL PEMBAYARAN MAHASISWA-->
<div id="tambah_detail_pembayaran" title="Tambah Detail Pembayaran"></div>
<!--SET VIA BAYAR-->
<div id="set_via_bayar" title="Set Via Bayar"></div>
<!--CEK BILLING VA-->
<div id="cek_billing_va" title="Cek Billing Va"></div>