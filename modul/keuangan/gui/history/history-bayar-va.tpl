<table class="ui-widget-content ui-widget" style="width: 97%">
    <caption>Virtual Account</caption>
    <tr class="ui-widget-header">
        <th colspan="8"class="header-coloumn">DAFTAR VA MAHASISWA</th>
    </tr>
    <tr class="ui-widget-header">
        <th>No</th>
        <th>Nama Bank</th>
        <th>Nomor VA</th>
        <th>Trx ID</th>
        <th>Expired</th>
        <th>Total Pembayaran</th>
        <th>Belum di proses</th>
        <th>Aksi</th>
    </tr>
    {$index=1}
    {foreach $data_vas as $data}                
        <tr>
            <td>{$index}</td>
            <td class="center">{$data.NAMA_BANK}</td>
            <td class="center">
                {$data.NO_VA}
                
                {if date('Y-m-d')>date('Y-m-d',strtotime($data.TGL_EXPIRED))}
                    <br/>
                    <span  class="ui-button ui-corner-all" style="background-color:red;color:white;padding:4px;cursor:pointer;margin:2px;">Tidak Aktif</span>
                {else}
                    <br/>
                    <span  class="ui-button ui-corner-all" style="background-color:green;color:white;padding:4px;cursor:pointer;margin:2px;">Aktif</span>
                {/if}
            </td>
            <td class="center">{$data.TRX_ID}</td>
            <td class="center">
                {$data.TGL_EXPIRED}
            </td>
            <td class="center">{number_format($data.TOTAL_BAYAR)}</td>
            <td class="center">
                {if $data.TOTAL_DIPROSES<$data.TOTAL_BAYAR}
                    <b style="color:red">{number_format($data.TOTAL_BAYAR-$data.TOTAL_DIPROSES)}</b>
                {else}
                    <b style="color:green">{number_format($data.TOTAL_BAYAR-$data.TOTAL_DIPROSES)}</b>
                {/if}
            </td>
            <td class="center">
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" href="detail-pembayaran-va.php?cari={$data_detail_mahasiswa.NIM_MHS}&no_va={$data.NO_VA}&trx_id={$data.TRX_ID}">Detail</a>
            </td>
        </tr>
        {$index=$index+1}
    {foreachelse}
        <tr class="ui-widget-content">
            <td colspan="8" class="center"><span style="color: red">Data Kosong</span></td>
        </tr>
    {/foreach}
    <tr class="ui-widget-content">
        <td colspan="8" class="center">            
            <span onclick="$('#manage_va').dialog('open').load('manage-va.php?mode=tambah&cari={$smarty.get.cari}')" class="btn btn-sm btn-warning" style="margin:2px;">Buat</span>
        </td>
    </tr>
</table>
<!--FORM ADD NEW VA-->
<div id="manage_va" title="Virtual Account"></div>