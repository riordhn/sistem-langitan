<div class="center_title_bar">Pembayaran Virtual Account</div>
<a class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:5px 15px;font-weight:bold" href="history-bayar.php?cari={$data_detail_mahasiswa.NIM_MHS}">KEMBALI</a>
<!--DETAIL BIODATA MAHASISWA-->
{include file='history/history-bayar-biodata.tpl'}
<p></p>
<table class="ui-widget" style="width:70%;font-size:1.2em">
    <caption>
    RIWAYAT PEMBAYARAN VA BANK
    <hr/>
    </caption>
    {foreach $data_pembayaran_va as $d}
    <tr class="ui-widget-content">
        <td colspan=5>
        <p style="font-size:1.1em">
            <b style="color:#c42700"><u>PEMBAYARAN KE-{$d@index+1} </u></b> <br/>
            <b>WAKTU BAYAR</b> : {date("d/m/Y H:i:s",strtotime($d.PAYMENT_TIME))} <br/>
            <b>NO.TRANSAKSI</b> : {$d.PAYMENT_NTB} <br/>
            <b>DIBAYARKAN KE</b> : {$d.NM_BANK} <br/>
            <b>TOTAL TAGIHAN</b> : {number_format($d.TRX_AMOUNT)} <br/>
            <b>TOTAL TERBAYAR</b> : {number_format($d.CUMULATIVE_PAYMENT_AMOUNT)} <br/>
            <b>DIBAYAR</b> : {number_format($d.PAYMENT_AMOUNT)} <br/>
            <b>KETERANGAN</b> : {$d.NOTES}
            {if $d.PEMBAYARAN_DIPROSES<$d.PAYMENT_AMOUNT}
                <br/>
                <hr/>
                <span onclick="ajax_get_page_dialog('update-biaya-va.php?cari={$data_detail_mahasiswa.NIM_MHS}&no_transaksi={$d.PAYMENT_NTB}','#update_biaya')" class="btn btn-sm btn-primary" style="margin:2px;">Proses Pembayaran</span>           
            {/if}
            
        </p>
        </td>
    </tr>
    {if count($d.DATA_PEMBAYARAN)>0}
        {foreach $d.DATA_PEMBAYARAN as $d2}
        <tr>
            <td colspan="5">di proses ke tagihan semester :  <b style="color:black">{$d2.SEMESTER}</b> 
            <span class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;float:right" onclick="window.open('cetak-kuitansi-pembayaran.php?cari={$cari}&no_transaksi={$d.PAYMENT_NTB}&no_kuitansi={$d2.NO_KUITANSI}','_blank')">Cetak Bukti Bayar</span>
            <span onclick="confirm_delete('{$d2.NO_KUITANSI}','{$smarty.get.cari}')" class="ui-button ui-corner-all ui-state-error" style="padding:4px;cursor:pointer;margin:2px;color:white;float:right">Reset</span>
            </td>
        </tr>
        <tr>
            <td colspan="5" style="padding:0px">
                <table style="width:100%;margin:0px;">
                    {$total_bayar=0}
                    {$index=1}
                    <tr class="ui-widget-header">
                        <th colspan="3">RINCIAN</th>
                    </tr>
                    {foreach $d2.DATA_DETAIL_PEMBAYARAN as $db}
                        <tr class="ui-widget-content">
                            <td>{$index++}</td>
                            <td>{$db['NM_BIAYA']}</td>
                            <td style="text-align:right">{number_format($db['BESAR_PEMBAYARAN'])}</td>
                        </tr>
                        {$total_bayar=$total_bayar+$db['BESAR_PEMBAYARAN']} 
                    {/foreach}
                    <tr class="total">
                        <td colspan="2">TOTAL</td>
                        <td style="text-align:right">{number_format($total_bayar)}</td>
                    </tr>
                    <tr >
                        <td colspan="3"><hr/></td>
                    </tr>
                </table>
            </td>
        </tr>
        {/foreach}
    {/if}
    {foreachelse}
    <tr>
        <td colspan=5><label style="color:red">Tidak ada transaksi pembayaran</label></td>
    </tr>
    {/foreach}
</table>
<input type="hidden" id="query_string" value="{$smarty.SERVER.QUERY_STRING}"/>
<!--EDIT MASTER TAGIHAN-->
<div id="update_biaya" title="Update Biaya Pembayaran VA"></div>
{literal}
<script>
  var base_url = window.location.protocol+'//'+window.location.hostname;
  var htmlLoading ='<div style="width: 90%;height:auto;padding:15px" align="center"><img src="'+base_url+'/img/spinner.gif" /></div>'; 
  $("#update_biaya").dialog({
     width: "50%",
      height: "auto",
      modal: true,
      resizable: false,
      autoOpen: false
  });
  function ajax_get_page_dialog(url,element){
    $.ajax({
        url: url,
        type: "get",
        beforeSend: function() {          
          $(element).dialog('open')
          $(element).html(
            htmlLoading
          );
        },
        success: function(data) {
          $(element).html(data);
        }
      });
  }
  function confirm_delete(no_kuitansi,nim) {    
    var query_string=$('#query_string').val();
    var base_url = window.location.protocol+'//'+window.location.hostname;
    var status = confirm("Apakah anda yakin menghapus pembayaran ini?");
    if (status == true) {
      $.ajax({
        url: 'detail-pembayaran-va.php?'+query_string,
        type: "post",
        data: "mode=hapus_riwayat&no_kuitansi=" +no_kuitansi,
        beforeSend: function() {
          $("#center_content").html(
            '<div style="width: 100%;" align="center"><img src="'+base_url+'/js/loading.gif" /></div>'
          );
        },
        success: function(data) {
          $("#center_content").html(data);
        }
      });
    }
  }
</script>
{/literal}
