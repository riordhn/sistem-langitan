{include file='history/history-bayar-head-form.tpl'} 
{if isset($data_detail_mahasiswa)||isset($data_detail_calon_mahasiswa)} 
    {if $data_detail_mahasiswa!=''||$data_detail_calon_mahasiswa!=''}
    <!--DETAIL BIODATA MAHASISWA-->
    {include file='history/history-bayar-biodata.tpl'}
    <p></p>
    <!--DATA VA-->
    {include file='history/history-bayar-va.tpl'}
    <p></p>
    <!-- PENGEMBALIAN PEMBAYARAN-->
    {* {include file='history/history-bayar-pengembalian.tpl'} *}
    <p></p>
    <!-- SEJARAH KERJASAMA-->
    {* {include file='history/history-bayar-kerjasama.tpl'} *}
    <p></p>
    <!-- SEMESTER PENDEK-->
    {* {include file='history/history-bayar-sp.tpl'} *}
    <p></p>
    <!-- PEMBAYARAN MAHASISWA/CALON MAHASISWA-->
    {include file='history/history-bayar-new-main.tpl'}
    <p></p>
    <!--PEMBAYARAN MAHASISWA BARU-->
    {* {include file='history/history-bayar-cmhs.tpl'}  *}
    {else}
    <div id="alert" class="ui-widget" style="margin-left:15px;">
    <div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;">
        <p>
        <span
            class="ui-icon ui-icon-alert"
            style="float: left; margin-right: .3em;"
        ></span>
        Data Tidak Ditemukan.
        </p>
    </div>
    <span
        class="btn btn-warning"
        style=margin:2px;"
        onclick="history.back(-1)"
        >KEMBALI</span>
    </div>
    {/if} 
{/if} 
{literal}
<script>
  var base_url = window.location.protocol+'//'+window.location.hostname;
  var htmlLoading ='<div style="width: 100%;height:auto;padding:15px" align="center"><img src="'+base_url+'/img/spinner.gif" /></div>';
  function show_detail(index, tag) {
    $(tag + index).fadeToggle();
  }
  function hide_detail(index, tag) {
    $(tag + index).fadeOut();
  }
  $("#riwayat_transaksi_pembayaran").dialog({
    width: "80%",
    height: "550",
    modal: true,
    resizable: false,
    autoOpen: false
  });
  $("#ubah_master_tagihan").dialog({
     width: "50%",
    height: "auto",
    modal: true,
    resizable: false,
    autoOpen: false
  });
  $("#proses_pembayaran").dialog({
    width: "50%",
    height: "500",
    modal: true,
    resizable: false,
    autoOpen: false
  });
  $("#update_upload_transaksi").dialog({
    width: "60%",
    height: "500",
    modal: true,
    resizable: false,
    autoOpen: false
  });
  $("#edit_pembayaran").dialog({
    width: "50%",
    height: "400",
    modal: true,
    resizable: false,
    autoOpen: false
  });
  $("#tambah_pembayaran").dialog({
    width: "50%",
    height: "600",
    modal: true,
    resizable: false,
    autoOpen: false
  });
  $("#tambah_detail_pembayaran").dialog({
    width: "50%",
    height: "auto",
    modal: true,
    resizable: false,
    autoOpen: false
  });
  
  $("#set_via_bayar").dialog({
    width: "50%",
    height: "auto",
    modal: true,
    resizable: false,
    autoOpen: false
  });
  $("#cek_billing_va").dialog({
    width: "50%",
    height: "auto",
    modal: true,
    resizable: false,
    autoOpen: false
  });
  $("#manage_va").dialog({
    width: "60%",
    height: "auto",
    modal: true,
    resizable: false,
    autoOpen: false
  });
  $("#form_tambah_pembayaran").submit(function() {
    $("#tambah_pembayaran").dialog("close");
  });
  $("#form_tambah_detail_pembayaran").submit(function() {
    $("#tambah_detail_pembayaran").dialog("close");
  });
  function confirm_delete(nim,id_tagihan_mhs) {
    var status = confirm("Apakah anda yakin menghapus tagihan ini?");
    if (status == true) {
      $.ajax({
        url: "history-bayar.php?cari=" + nim + "",
        type: "post",
        data: "mode=hapus&id_tagihan_mhs=" +id_tagihan_mhs,
        beforeSend: function() {
          $("#center_content").html(
            htmlLoading
          );
        },
        success: function(data) {
          $("#center_content").html(data);
        }
      });
    }
  }
  function confirm_reset(nim,id_tagihan_mhs) {
    var status = confirm("Apakah anda yakin menghapus semua data pembayaran ini?");
    if (status == true) {
      $.ajax({
        url: "history-bayar.php?cari=" + nim + "",
        type: "post",
        data: "mode=reset&id_tagihan_mhs=" +id_tagihan_mhs,
        beforeSend: function() {
          $("#center_content").html(
            htmlLoading
          );
        },
        success: function(data) {
          $("#center_content").html(data);
        }
      });
    }
  }

  function ajax_get_page_dialog(url,element){
    $.ajax({
        url: url,
        type: "get",
        beforeSend: function() {          
          $(element).dialog('open')
          $(element).html(
            htmlLoading
          );
        },
        success: function(data) {
          $(element).html(data);
        }
      });
  }


  $("#form").submit(function() {
    if ($("#cari").val() == "") {
      $("#alert").fadeIn();
      return false;
    }
  });
  $(".date").datepicker({
    dateFormat: "dd-M-yy",
    changeMonth: true,
    changeYear: true
  });
</script>
{/literal}
