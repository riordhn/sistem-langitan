{if !empty($data_pembayaran_cmhs)}
<table class="ui-widget" style="width: 97%">
  <caption>
    History Pembayaran Mahasiswa Baru
  </caption>
  <tr class="ui-widget-header">
    <th colspan="11">DETAIL PEMBAYARAN MAHASISWA BARU</th>
  </tr>
  <tr class="ui-widget-header">
    <th>No</th>
    <th>Semester</th>
    <th>Tagihkan</th>
    <th>Total Biaya</th>
    <th>Nama Bank</th>
    <th>Via Bank</th>
    <th>Tanggal Bayar</th>
    <th>No Transaksi</th>
    <th>Keterangan</th>
    <th>Status Bayar</th>
    <th>Operasi</th>
  </tr>
  {foreach $data_pembayaran_cmhs as $data}
  <tr class="total ui-widget-content">
    <td>{$data@index+1}</td>
    <td>
      {$data.NM_SEMESTER}<br />
      ({$data.TAHUN_AJARAN})
    </td>
    <td>
      {if $data.IS_TAGIH=='Y'} Ya {else} Tidak {/if}
    </td>
    <td class="center">{number_format($data.JUMLAH_PEMBAYARAN)}</td>
    <td>{$data.NM_BANK}</td>
    <td>{$data.NAMA_BANK_VIA}</td>
    <td>{$data.TGL_BAYAR}</td>
    <td>{$data.NO_TRANSAKSI}</td>
    <td>{$data.KETERANGAN}</td>
    <td>{$data.NAMA_STATUS}</td>
    <td>
      <span
        onclick="show_detail('{$data@index+1}','#detail_cmhs')"
        class="ui-button ui-corner-all ui-state-hover"
        style="padding:4px;cursor:pointer;margin:2px;"
        >Detail</span
      ><br />
    </td>
  </tr>
  <tr
    id="detail_cmhs{$data@index+1}"
    style="display: none"
    class="ui-widget-content"
  >
    <td colspan="11">
      <table class="ui-widget" width="95%">
        <tr class="ui-widget-header">
          <th colspan="8" class="header-coloumn" style="text-align: center;">
            Detail Pembayaran Mahasiswa Baru
          </th>
        </tr>
        <tr class="ui-widget-header">
          <th>Nama Biaya</th>
          <th>Besar Biaya</th>
          <th>Tagihkan</th>
          <th>Nama Bank</th>
          <th>Via Bank</th>
          <th>Tanggal Bayar</th>
          <th>No Transaksi</th>
          <th>Keterangan</th>
        </tr>
        {foreach $data.DATA_PEMBAYARAN as $data_pembayaran} {if
        $data_pembayaran.TGL_BAYAR==NULL}
        {$total_tagihan=$total_tagihan+$data_pembayaran.BESAR_BIAYA} {else}
        {$total_pembayaran=$total_pembayaran+$data_pembayaran.BESAR_BIAYA} {/if}
        <tr class="ui-widget-content">
          <td>
            {$data_pembayaran['NM_BIAYA']}
          </td>
          <td>
            {number_format($data_pembayaran['BESAR_BIAYA'])}
          </td>
          <td>
            {if $data_pembayaran['IS_TAGIH']=='Y'} Y {else} Tidak {/if}
          </td>
          <td>
            {$data_pembayaran['NM_BANK']}
          </td>
          <td>
            {$data_pembayaran['NAMA_BANK_VIA']}
          </td>
          <td>
            {$data_pembayaran['TGL_BAYAR']}
          </td>
          <td>
            {$data_pembayaran['NO_TRANSAKSI']}
          </td>
          <td>
            {$data_pembayaran['KETERANGAN']}
          </td>
        </tr>
        {/foreach}

        <tr class="ui-widget-content">
          <td class="center" colspan="{if $jenis=='cmhs'}8{else}9{/if}">
            <span
              onclick="hide_detail('{$data@index+1}','#detail_cmhs')"
              class="ui-button ui-corner-all ui-state-hover"
              style="padding:4px;cursor:pointer;margin:2px;"
              >Tutup</span
            >
          </td>
        </tr>
      </table>
    </td>
  </tr>
  {foreachelse}
  <tr class="total ui-widget-content">
    <td class="center" colspan="11">
      <span style="color:red">
        Data Pembayaran Kosong
      </span>
    </td>
  </tr>
  {/foreach}
</table>

<span
  class="ui-button ui-corner-all ui-state-hover"
  style="padding:4px;cursor:pointer;margin:2px;"
  onclick="history.back(-1)"
  >Kembali</span>
{/if} 