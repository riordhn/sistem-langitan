{if $ditangguhkan=='1'}
    <p style="text-align:center">
        <b style="color:red;">Pembayaran VIA Virtual Account tidak bisa dilakukan karena ada penangguhan. 
            Silahkan hapus terlebih dahulu pada halaman <i>Riwayat Bayar</i>
        </b>
        <br/>
        <br/>
        <button style="padding:3px 7px" class="ui-button ui-corner-all ui-state-hover" onclick="$('#set_via_bayar').dialog('close')">OKE</button>
    </p>
{else}
<form id="form_proses_pembayaran" action="history-bayar.php?cari={$cari}" method="post" >
    <input type="hidden" name="mode" value="set_via_bayar"/>
    <input type="hidden" id="id_tagihan_mhs" name="id_tagihan_mhs" value="{$id_tagihan_mhs}"/>
    <table class="ui-widget" style="width: 96%;">
        <tr class="ui-widget-header">
            <th>SET VIA BAYAR</th>
        </tr>
        <tr class="ui-widget-content">
            <td>
                {if $tagihan_mhs.NO_VA!=''}
                <input type="hidden" id="is_va_set_bayar" name="is_va" value="0"/>
                <p>Tagihan akan diproses dengan pembayaran manual?</p>
                {else}
                <input type="hidden" id="is_va_set_bayar" name="is_va" value="1"/>
                <ul>
                    <li style="font-size:1.1em;font-weight:bolder;width:100%;height:30px">
                        <span style="float:left">NOMOR VA</span> 
                        <input style="float:right;text-align:center" type="text" name="no_va" value="{$kode_va}" readonly/>
                    </li>
                </ul>
                <p>Tagihan akan diproses dengan pembayaran dengan Virtual Account dan tidak bisa di ubah manual?</p>
                {/if}
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td class="center total">
                <input type="submit" value="PROSES" class="ui-button ui-corner-all ui-state-hover"/>
            </td>
        </tr>
    </table>
</form>
<style>
    ul{
        margin:0px;
    }
</style>
<script>

    $(".date").datepicker({
        dateFormat: 'yy-mm-dd', 
        changeMonth: true,
        changeYear: true
    });
    
    $("#form_proses_pembayaran").submit(function() {
        console.log($('#is_va_set_bayar').val());
        if($('#is_va_set_bayar').val()=='1'){
            $.ajax({
                type:'post',
                url:'kirim-billing-va.php',
                data:$(this).serialize(),
                beforeSend: function() {          
                    $('#set_via_bayar').html(htmlLoading);
                },
                success:function(data){                    
                    $("#set_via_bayar").dialog("close");
                    var result =JSON.parse(data);
                    console.log(result);
                    if(result['status']!='000'){
                        Swal.fire({
                            title: 'Error!',
                            html:
                            'Gagal melakukan update ke Sistem VA<br/><i style="font-size:0.8em"> ' +
                            result['message']+
                            '</i>',
                            icon: 'error',
                            confirmButtonText: 'OK'
                        })
                    }else{
                        Swal.fire({
                            title: 'Sukses!',
                            text: 'Berhasil melakukan update ke Sistem BNI',
                            icon: 'success',
                            confirmButtonText: 'OK'
                        })
                    }
                    
                }                    
            });
        }else{
            $("#set_via_bayar").dialog("close");
        }
    });
</script>
{/if}
