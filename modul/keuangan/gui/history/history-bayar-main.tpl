<table class="ui-widget" style="width: 97%;font-size: 0.9em">
    <caption>History Pembayaran</caption>
    <tr class="ui-widget-header">
        <th colspan="13"> PEMBAYARAN {if $jenis=='cmhs'} CALON {/if} MAHASISWA</th>
    </tr>
    <tr class="ui-widget-header">
        <th>No</th>
        <th>Semester</th>
        <th>Tagihkan</th>
        <th>Total Biaya</th>
        <th>Nama Bank</th>
        <th>Via Bank</th>
        <th>Tanggal Bayar</th>
        <th>No Transaksi</th>
        <th>Keterangan</th>
        <th>Status Bayar</th>
        <th>Semester Bayar</th>
        <th>Flag Pindah</th>
        <th>Operasi</th>
    </tr>
    {foreach $data_pembayaran as $data}
        <tr {if $data.NM_SEMESTER=='Ganjil'}style="background-color: #c9f797"{else}style="background-color: beige"{/if}>
            {if $smarty.get.id_semester==$data.ID_SEMESTER&&$smarty.get.tgl_bayar==$data.TGL_BAYAR&&$smarty.get.id_bank==$data.ID_BANK&&$smarty.get.id_bank_via==$data.ID_BANK_VIA&&$smarty.get.no_transaksi==$data.NO_TRANSAKSI&&$smarty.get.is_tagih==$data.IS_TAGIH&&$data.KETERANGAN|base64_encode==$smarty.get.keterangan}
                <td colspan="13">
                    <form method="post" action="history-bayar.php?cari={$smarty.get.cari}">
                        <table style="width: 97%;margin: 0px" class="ui-widget-content">
                            <tr class="ui-widget-header">
                                <th>No</th>
                                <th>Semester</th>
                                <th>Tagihkan</th>
                                <th>Total Biaya</th>
                                <th>Nama Bank</th>
                                <th>Via Bank</th>
                                <th>Tanggal Bayar</th>
                                <th>No Transaksi</th>
                                <th>Keterangan</th>
                                <th>Status Bayar</th>
                                <th>Semester Bayar</th>
                                <th>Flag Pindah</th>
                                <th>Operasi</th>
                            </tr>
                            <tr>
                                <td>{$data@index+1}</td>
                                <td>    
                                    <select name="semester">
                                        {foreach $data_semester as $semester}
                                            <option value="{$semester['ID_SEMESTER']}" {if $data['ID_SEMESTER']==$semester['ID_SEMESTER']}selected="true"{/if}>{$semester['NM_SEMESTER']}({$semester['TAHUN_AJARAN']})</option>
                                        {/foreach}
                                    </select>
                                    <input type="hidden" name="semester_asli" value="{$data.ID_SEMESTER}"/>
                                </td>
                                <td>
                                    <select name="is_tagih">
                                        <option {if $data.IS_TAGIH=='Y'}selected="true" {/if} value="Y">Ya</option>
                                        <option {if $data.IS_TAGIH=='T'}selected="true" {/if} value="T">Tidak</option>
                                    </select>
                                    <input type="hidden" name="is_tagih_asli" value="{$data.IS_TAGIH}"/>
                                </td>
                                <td class="center">{number_format($data.JUMLAH_PEMBAYARAN)}</td>
                                <td>
                                    <select name="bank">
                                        <option value=""></option>
                                        {foreach $data_bank as $data_b}
                                            <option value="{$data_b['ID_BANK']}" {if $data_b['ID_BANK']==$data['ID_BANK']}selected="true"{/if}>{$data_b['NM_BANK']}</option>
                                        {/foreach}
                                    </select>
                                    <input type="hidden" name="bank_asli" value="{$data.ID_BANK}"/>
                                </td>
                                <td>
                                    <select name="bank_via">
                                        <option value=""></option>
                                        {foreach $data_bank_via as $data_bv}
                                            <option value="{$data_bv['ID_BANK_VIA']}" {if $data_bv['ID_BANK_VIA']==$data['ID_BANK_VIA']}selected="true"{/if}>{$data_bv['NAMA_BANK_VIA']}</option>
                                        {/foreach}
                                    </select>
                                    <input type="hidden" name="bank_via_asli" value="{$data.ID_BANK_VIA}"/>
                                </td>
                                <td>
                                    <input type="text" style="text-transform: uppercase" class="date" name="tgl_bayar" size="7" value="{$data['TGL_BAYAR']}" />
                                    <input type="hidden" name="tgl_bayar_asli" value="{$data.TGL_BAYAR}"/>
                                </td>
                                <td>
                                    <input type="text" name="no_transaksi" size="10" value="{$data['NO_TRANSAKSI']}" />
                                    <input type="hidden" name="no_transaksi_asli" value="{$data.NO_TRANSAKSI}"/>
                                </td>
                                <td>
                                    <textarea name="keterangan" cols="15">{$data['KETERANGAN']}</textarea>
                                    <input type="hidden" name="keterangan_asli" value="{$data.KETERANGAN}"/>
                                </td>
                                <td>
                                    <select name="status">
                                        <option value=""></option>
                                        {foreach $data_status as $data_s}
                                            <option value="{$data_s.ID_STATUS_PEMBAYARAN}" {if $data_s.ID_STATUS_PEMBAYARAN==$data.ID_STATUS_PEMBAYARAN}selected="true" {/if}>{$data_s.NAMA_STATUS}</option>
                                        {/foreach}
                                    </select>
                                </td>
                                <td>    
                                    <select name="semester_bayar">
                                        <option value=""></option>
                                        {foreach $data_semester as $semester}
                                            <option value="{$semester['ID_SEMESTER']}" {if $data['ID_SEMESTER_BAYAR']==$semester['ID_SEMESTER']}selected="true"{/if}>{$semester['NM_SEMESTER']}({$semester['TAHUN_AJARAN']})</option>
                                        {/foreach}
                                    </select>
                                    <input type="hidden" name="semester_asli" value="{$data.ID_SEMESTER}"/>
                                </td>
                                <td>
                                    <select name="flag_pindah">
                                        <option {if $data.FLAG_PEMINDAHAN==''}selected="true" {/if} value="">Tidak</option>
                                        <option {if $data.FLAG_PEMINDAHAN=='1'}selected="true" {/if} value="1">Ya</option>
                                    </select>
                                    <input type="hidden" name="is_tagih_asli" value="{$data.IS_TAGIH}"/>
                                </td>
                                <td>
                                    <input type="hidden" name="mode" value="save"/>
                                    <span onclick="show_detail('{$data@index+1}','#detail')" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;">Detail</span><br/>
                                    <input name="save" type="submit" value="Save Group" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" /><br/>
                                    <a class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" href="history-bayar.php?cari={$smarty.get.cari}">Batal</a>
                                </td>
                            </tr>
                        </table>
                    </form>
                </td>
            {else}
                <td>{$data@index+1}</td>
                <td>{$data.NM_SEMESTER}<br/> ({$data.TAHUN_AJARAN})</td>
                <td>
                    {if $data.IS_TAGIH=='Y'}
                        Ya
                    {else}
                        Tidak
                    {/if}
                </td>
                <td class="center">{number_format($data.JUMLAH_PEMBAYARAN)}</td>
                <td>{$data.NM_BANK}</td>
                <td>{$data.NAMA_BANK_VIA}</td>
                <td>{$data.TGL_BAYAR}</td>
                <td>{$data.NO_TRANSAKSI}</td>
                <td>{$data.KETERANGAN}</td>
                <td>{$data.NAMA_STATUS}</td>
                <td>{if $data.NM_SEMESTER_BAYAR!=''}{$data.NM_SEMESTER_BAYAR}<br/> ({$data.TAHUN_AJARAN_BAYAR}){/if}</td>
                <td>{if $data.FLAG_PEMINDAHAN==1}Ya{else}Tidak{/if}</td>
                <td>
                    <span onclick="show_detail('{$data@index+1}','#detail')" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;">Detail</span><br/>
                    {if $smarty.get.mode!='edit'&&$jenis=='mhs'}
                        <span onclick="confirm_delete('{$smarty.get.cari}','{$data.ID_SEMESTER}','{$data.TGL_BAYAR}','{$data.ID_BANK}','{$data.ID_BANK_VIA}','{$data.NO_TRANSAKSI}','{$data.KETERANGAN}','{$data.IS_TAGIH}')" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;">Delete</span><br/>
                    {/if}
                    {if $jenis=='mhs'}
                        <a class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" href="history-bayar.php?mode=edit&id_semester={$data.ID_SEMESTER}&tgl_bayar={$data.TGL_BAYAR}&id_bank={$data.ID_BANK}&id_bank_via={$data.ID_BANK_VIA}&no_transaksi={$data.NO_TRANSAKSI}&keterangan={$data.KETERANGAN|base64_encode}&cari={$smarty.get.cari}&is_tagih={$data.IS_TAGIH}">Edit</a>
                    {/if}
                </td>
            {/if}
        </tr>
        <tr id="detail{$data@index+1}" style="display: none;" class="ui-widget-content">
            <td colspan="13">
                <form method="post" action="history-bayar.php?cari={$smarty.get.cari}">
                    <table class="ui-widget" style="font-size: 0.8em;width: 97%">
                        <tr class="ui-widget-header">
                            <th colspan="{if $jenis=='cmhs'}8{else}10{/if}" class="header-coloumn" style="text-align: center;">Detail Pembayaran {if $jenis=='cmhs'} Calon {/if} Mahasiswa </th>
                        </tr>
                        <tr class="ui-widget-header" style="font-size: 0.8em">
                            {if $jenis=='mhs'}
                                <th>Nama Biaya</th>
                                <th>Besar Biaya</th>
                                <th>Denda Biaya</th>
                                <th>Tagihkan</th>
                                <th>Nama Bank</th>
                                <th>Via Bank</th>
                                <th>Tanggal Bayar</th>
                                <th>No Transaksi</th>
                                <th>Keterangan</th>
                                <th>Status Bayar</th>
                            {else}
                                <th>Nama Biaya</th>
                                <th>Besar Biaya</th>
                                <th>Tagihkan</th>
                                <th>Nama Bank</th>
                                <th>Via Bank</th>
                                <th>Tanggal Bayar</th>
                                <th>No Transaksi</th>
                                <th>Keterangan</th>
                            {/if}
                        </tr>
                        {foreach $data.DATA_PEMBAYARAN as $data_pembayaran}
                            {if $jenis=='cmhs'}
                                {if $data_pembayaran.TGL_BAYAR==NULL}
                                    {$total_tagihan=$total_tagihan+$data_pembayaran.BESAR_BIAYA}
                                {else}
                                    {$total_pembayaran=$total_pembayaran+$data_pembayaran.BESAR_BIAYA}
                                {/if}
                            {else}
                                {if $data_pembayaran.TGL_BAYAR==NULL}
                                    {$total_tagihan=$total_tagihan+$data_pembayaran.BESAR_BIAYA+$data_pembayaran.DENDA_BIAYA}
                                {else}
                                    {$total_pembayaran=$total_pembayaran+$data_pembayaran.BESAR_BIAYA+$data_pembayaran.DENDA_BIAYA}
                                {/if}
                            {/if}
                            <tr class="ui-widget-content">
                                <td>
                                    {$data_pembayaran['NM_BIAYA']}
                                </td>
                                <td>
                                    {if $smarty.get.id_semester==$data.ID_SEMESTER&&$smarty.get.tgl_bayar==$data.TGL_BAYAR&&$smarty.get.id_bank==$data.ID_BANK&&$smarty.get.id_bank_via==$data.ID_BANK_VIA&&$smarty.get.no_transaksi==$data.NO_TRANSAKSI&&$data.KETERANGAN|base64_encode==$smarty.get.keterangan}
                                        <input type="text" name="besar_biaya{$data_pembayaran@index+1}" value="{number_format($data_pembayaran['BESAR_BIAYA'])}"/>
                                        {$total_detail=$data_pembayaran@index+1}
                                    {else}
                                        {number_format($data_pembayaran['BESAR_BIAYA'])}
                                    {/if}                                            
                                </td>

                                {if $jenis=='mhs'}
                                    <td>
                                        {if $smarty.get.id_semester==$data.ID_SEMESTER&&$smarty.get.tgl_bayar==$data.TGL_BAYAR&&$smarty.get.id_bank==$data.ID_BANK&&$smarty.get.id_bank_via==$data.ID_BANK_VIA&&$smarty.get.no_transaksi==$data.NO_TRANSAKSI&&$data.KETERANGAN|base64_encode==$smarty.get.keterangan}
                                            <input type="text" name="denda_biaya{$data_pembayaran@index+1}" value="{number_format($data_pembayaran['DENDA_BIAYA'])}"/>
                                            <input type="hidden" name="id_pembayaran{$data_pembayaran@index+1}" value="{$data_pembayaran.ID_PEMBAYARAN}"/>
                                            {$total_detail=$data_pembayaran@index+1}
                                        {else}
                                            {number_format($data_pembayaran['DENDA_BIAYA'])}
                                        {/if}
                                    </td>
                                {/if}

                                <td>
                                    {if $smarty.get.id_semester==$data.ID_SEMESTER&&$smarty.get.tgl_bayar==$data.TGL_BAYAR&&$smarty.get.id_bank==$data.ID_BANK&&$smarty.get.id_bank_via==$data.ID_BANK_VIA&&$smarty.get.no_transaksi==$data.NO_TRANSAKSI&&$data.KETERANGAN|base64_encode==$smarty.get.keterangan}
                                        <select name="tagih{$data_pembayaran@index+1}">
                                            <option {if $data_pembayaran.IS_TAGIH=='Y'}selected="true" {/if} value="Y">Ya</option>
                                            <option {if $data_pembayaran.IS_TAGIH=='T'}selected="true" {/if} value="T">Tidak</option>
                                        </select>
                                    {else}
                                        {if $data_pembayaran.IS_TAGIH=='Y'}
                                            Y
                                        {else}
                                            Tidak
                                        {/if}
                                    {/if}
                                </td>
                                <td>
                                    {if $smarty.get.id_semester==$data.ID_SEMESTER&&$smarty.get.tgl_bayar==$data.TGL_BAYAR&&$smarty.get.id_bank==$data.ID_BANK&&$smarty.get.id_bank_via==$data.ID_BANK_VIA&&$smarty.get.no_transaksi==$data.NO_TRANSAKSI&&$data.KETERANGAN|base64_encode==$smarty.get.keterangan}
                                        <select name="bank{$data_pembayaran@index+1}">
                                            <option value=""></option>
                                            {foreach $data_bank as $data_b}
                                                <option value="{$data_b['ID_BANK']}" {if $data_b['ID_BANK']==$data_pembayaran['ID_BANK']}selected="true"{/if}>{$data_b['NM_BANK']}</option>
                                            {/foreach}
                                        </select>
                                    {else}
                                        {$data_pembayaran['NM_BANK']}
                                    {/if}
                                </td>
                                <td>
                                    {if $smarty.get.id_semester==$data.ID_SEMESTER&&$smarty.get.tgl_bayar==$data.TGL_BAYAR&&$smarty.get.id_bank==$data.ID_BANK&&$smarty.get.id_bank_via==$data.ID_BANK_VIA&&$smarty.get.no_transaksi==$data.NO_TRANSAKSI&&$data.KETERANGAN|base64_encode==$smarty.get.keterangan}
                                        <select name="bank_via{$data_pembayaran@index+1}">
                                            <option value=""></option>
                                            {foreach $data_bank_via as $data_bv}
                                                <option value="{$data_bv['ID_BANK_VIA']}" {if $data_bv['ID_BANK_VIA']==$data_pembayaran['ID_BANK_VIA']}selected="true"{/if}>{$data_bv['NAMA_BANK_VIA']}</option>
                                            {/foreach}
                                        </select>
                                    {else}
                                        {$data_pembayaran['NAMA_BANK_VIA']}
                                    {/if}
                                </td>
                                <td>
                                    {if $smarty.get.id_semester==$data.ID_SEMESTER&&$smarty.get.tgl_bayar==$data.TGL_BAYAR&&$smarty.get.id_bank==$data.ID_BANK&&$smarty.get.id_bank_via==$data.ID_BANK_VIA&&$smarty.get.no_transaksi==$data.NO_TRANSAKSI&&$data.KETERANGAN|base64_encode==$smarty.get.keterangan}
                                        <input type="text" style="text-transform: uppercase" class="date" name="tgl_bayar{$data_pembayaran@index+1}" size="7" value="{$data_pembayaran['TGL_BAYAR']}" />
                                    {else}
                                        {$data_pembayaran['TGL_BAYAR']}
                                    {/if}
                                </td>
                                <td>
                                    {if $smarty.get.id_semester==$data.ID_SEMESTER&&$smarty.get.tgl_bayar==$data.TGL_BAYAR&&$smarty.get.id_bank==$data.ID_BANK&&$smarty.get.id_bank_via==$data.ID_BANK_VIA&&$smarty.get.no_transaksi==$data.NO_TRANSAKSI&&$data.KETERANGAN|base64_encode==$smarty.get.keterangan}
                                        <input type="text" name="no_transaksi{$data_pembayaran@index+1}" size="10" value="{$data_pembayaran['NO_TRANSAKSI']}" />
                                    {else}
                                        {$data_pembayaran['NO_TRANSAKSI']}
                                    {/if}
                                </td>
                                <td>
                                    {if $smarty.get.id_semester==$data.ID_SEMESTER&&$smarty.get.tgl_bayar==$data.TGL_BAYAR&&$smarty.get.id_bank==$data.ID_BANK&&$smarty.get.id_bank_via==$data.ID_BANK_VIA&&$smarty.get.no_transaksi==$data.NO_TRANSAKSI&&$data.KETERANGAN|base64_encode==$smarty.get.keterangan}
                                        <textarea name="keterangan{$data_pembayaran@index+1}" cols="10">{$data_pembayaran['KETERANGAN']}</textarea>
                                    {else}
                                        {$data_pembayaran['KETERANGAN']}
                                    {/if}
                                </td>
                                {if $jenis=='mhs'}
                                    <td>
                                        {if $smarty.get.id_semester==$data.ID_SEMESTER&&$smarty.get.tgl_bayar==$data.TGL_BAYAR&&$smarty.get.id_bank==$data.ID_BANK&&$smarty.get.id_bank_via==$data.ID_BANK_VIA&&$smarty.get.no_transaksi==$data.NO_TRANSAKSI&&$data.KETERANGAN|base64_encode==$smarty.get.keterangan}
                                            <select name="status{$data_pembayaran@index+1}">
                                                <option value=""></option>
                                                {foreach $data_status as $data_s}
                                                    <option value="{$data_s.ID_STATUS_PEMBAYARAN}" {if $data_s.ID_STATUS_PEMBAYARAN==$data_pembayaran.ID_STATUS_PEMBAYARAN}selected="true" {/if}>{$data_s.NAMA_STATUS}</option>
                                                {/foreach}
                                            </select>
                                        {else}
                                            {$data_pembayaran.NAMA_STATUS}
                                        {/if}
                                    </td>
                                {/if}
                            </tr>
                        {/foreach}

                        <tr class="ui-widget-content">
                            <td class="center" colspan="{if $jenis=='cmhs'}8{else}10{/if}">
                                <input type="hidden" name="total_detail" value="{$total_detail}"/>
                                <input type="hidden" name="mode" value="save-detail"/>
                                {if $smarty.get.id_semester==$data.ID_SEMESTER&&$smarty.get.tgl_bayar==$data.TGL_BAYAR&&$smarty.get.id_bank==$data.ID_BANK&&$smarty.get.id_bank_via==$data.ID_BANK_VIA&&$smarty.get.no_transaksi==$data.NO_TRANSAKSI&&$data.KETERANGAN|base64_encode==$smarty.get.keterangan}
                                    <input name="save" type="submit" value="Save Detail" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" />
                                {/if}
                                <span class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" 
                                      {if $jenis=='mhs'}
                                          onclick="window.open('cetak-history-bayar.php?cari={$data.ID_MHS}&semester={$data.ID_SEMESTER}&jenis={$jenis}&no_transaksi={$data.NO_TRANSAKSI}&tgl_bayar={$data.TGL_BAYAR}&keterangan={$data.KETERANGAN}&is_tagih={$data.IS_TAGIH}','_blank')"
                                      {else}
                                          onclick="window.open('cetak-history-bayar.php?cari={$data.ID_C_MHS}&semester={$data.ID_SEMESTER}&jenis={$jenis}&no_transaksi={$data.NO_TRANSAKSI}&tgl_bayar={$data.TGL_BAYAR}&keterangan={$data.KETERANGAN}','_blank')"
                                      {/if}
                                      >Cetak History Bayar
                                </span>
                                <span onclick="hide_detail('{$data@index+1}','#detail')" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;">Tutup</span>
                            </td>
                        </tr>
                    </table>
                </form>
            </td>
        </tr>
    {foreachelse}
        <tr class="total ui-widget-content">
            <td class="center" colspan="13">
                <span style="color:red">
                    Data Pembayaran Kosong
                </span>
            </td>
        </tr>
    {/foreach}
    <tr class="total ui-widget-content">
        <td class="center" colspan="13">
            <span class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" 
                  {if $jenis=='mhs'}
                      onclick="window.open('cetak-history-bayar.php?cari={$data_detail_mahasiswa.NIM_MHS}&mode=all&jenis={$jenis}','_blank')"
                  {else}
                      onclick="window.open('cetak-history-bayar.php?cari={$data_detail_calon_mahasiswa.NO_UJIAN}&mode=all&jenis={$jenis}','_blank')"
                  {/if}
                  >Cetak History Bayar All
            </span>
            {if $jenis=='mhs'}
                <span class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" 
                      onclick="window.open('cetak-history-bayar-rinci.php?cari={$smarty.get.cari}','_blank')"
                      >Cetak History Bayar Rinci
                </span>
            {/if}
            <br/>
            <hr style="color: maroon"/>
            {if $jenis=='mhs'}
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" href="status-bayar.php?cari={$data_detail_mahasiswa.NIM_MHS}">
                    Riwayat Status Pembayaran
                </a>
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" href="biaya-mahasiswa.php?cari={$data_detail_mahasiswa.NIM_MHS}">
                    Master Biaya
                </a>
            {/if}
            <br/>
            <hr style="color: maroon"/>
            <span onclick="$('#tambah_pembayaran').dialog('open').load('tambah-pembayaran.php?cari={$smarty.get.cari}')" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;">Tambah Pembayaran</span>
            <span onclick="$('#tambah_detail_pembayaran').dialog('open').load('tambah-detail-pembayaran.php?cari={$smarty.get.cari}')" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;">Tambah Detail Pembayaran</span>
        </td>
    </tr>
</table>