{literal}
    <script type="text/javascript">
        $(function() {
            $("table").tablesorter({sortList: [[0, 0]], widgets: ['zebra']});
        });

    </script>
{/literal}
<div class="center_title_bar">History Bayar Semester</div>
<form method="get" id="form" action="history-bayar-semester.php">
    <div id="alert" class="ui-widget" style="display:none;margin-left:15px;">
        <div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;"> 
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                Data Harus Diisi.</p>
        </div>
    </div>
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Input Form</th>
        </tr>
        <tr class="ui-widget-content">
            <td>NIM</td>
            <td><input id="cari" type="search" name="cari" value="{if isset($smarty.get.cari)}{$smarty.get.cari}{/if}"/></td>
            <td><input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;" value="Tampilkan"/></td>
        </tr>
    </table>
</form>
{if isset($smarty.get.cari)}
    <table style="width: 400px;" class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="text-align: center">Detail Biodata Mahasiswa</th>
        </tr>
        <tr class="ui-widget-content">
            <td>NIM</td>
            <td width="200px">{$data_detail_mahasiswa.NIM_MHS}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Nama</td>
            <td>{$data_detail_mahasiswa.NM_PENGGUNA}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Fakultas</td>
            <td>{$data_detail_mahasiswa.NM_FAKULTAS}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Program Studi</td>
            <td>{$data_detail_mahasiswa.NM_JENJANG} - {$data_detail_mahasiswa.NM_PROGRAM_STUDI}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Jalur - Angkatan</td>
            <td>{$data_detail_mahasiswa.NM_JALUR} - {$data_detail_mahasiswa.THN_ANGKATAN_MHS}</td>
        </tr>
    </table>
    <p></p>

    
    {foreach $semester as $sem}
    <table class="tablesorter ui-widget" >
        <!-- <thead> -->
                <tr class="ui-widget-header">
                    <th colspan="4">Semester {$sem.SEMESTER}</th>
                </tr>
            <tr class="ui-widget-header">
                <th>No</th>
                <th>Nama Biaya</th>
                <th>Tagihan</th>
                <th>Pembayaran</th>
            </tr>
        <!-- </thead>
        <tbody> -->
                {$index=1}
                {$besar_tagihan=0}
                {$besar_bayar=0}
                {$piutang=0}
                {foreach $tagihan_mahasiswa as $tagihan}
                {if $sem.ID_SEMESTER == $tagihan.ID_SEMESTER}
                        <tr class="ui-widget-content">
                            <td>{$index}</td>
                            <td>{$tagihan.NM_BIAYA} {$tagihan.NM_BULAN}</td>
                            <td>{$tagihan.BESAR_BIAYA}</td>
                            <td>{$tagihan.BESAR_PEMBAYARAN}</td>
                        </tr>
                        {$index=$index+1}
                        {$besar_tagihan=$besar_tagihan+$tagihan.BESAR_BIAYA}
                        {$piutang=$piutang+$tagihan.BESAR_BIAYA}
                        {$besar_bayar=$besar_bayar+$tagihan.BESAR_PEMBAYARAN}
                        {$piutang=$piutang-$tagihan.BESAR_PEMBAYARAN}
                {/if}
                {/foreach}
                <tr class="ui-widget-header">
                    <th colspan="2">Total</th>
                    <th>{$besar_tagihan}</th>
                    <th>{$besar_bayar}</th>
                </tr>
                <tr class="ui-widget-header">
                    <th colspan="3">Piutang</th>
                    <th>{$piutang}</th>
                </tr>
        <!-- </tbody> -->
    </table>
    <p></p>
    {/foreach}

    <!-- <form action="input-keuangan.php?cari={$smarty.get.cari}" method="post">
        <table class="tablesorter ui-widget" >
            <thead>
                <tr class="ui-widget-header">
                    <th colspan="11">Input Biaya Kuliah</th>
                </tr>
                <tr class="ui-widget-header">
                    <th>No</th>
                    <th width="120px;">Semester</th>
                    <th>SPP Bulan 1</th>
                    <th>SPP Bulan 2</th>
                    <th>SPP Bulan 3</th>
                    <th>SPP Bulan 4</th>
                    <th>SPP Bulan 5</th>
                    <th>SPP Bulan 6</th>
                    <th>UTS</th>
                    <th>UAS</th>
                    <th>HER</th>
                    <th>IKM</th>
                    <th>KOP 1</th>
                    <th>KOP 2</th>
                    <th>KOP 3</th>
                    <th>KOP 4</th>
                    <th>KOP 5</th>
                    <th>KOP 6</th>
                    <th>PLTH 1</th>
                    <th>PLTH 2</th>
                    <th>PLTH 3</th>
                    <th>PLTH 4</th>
                    <th>PLTH 5</th>
                    <th>PLTH 6</th>
                    <th>Praktikum 1</th>
                    <th>Praktikum 2</th>
                    <th>Praktikum 3</th>
                    <th>Praktikum 4</th>
                    <th>Praktikum 5</th>
                    <th>Praktikum 6</th>
                </tr>
            </thead>
            <tbody>
                {foreach $data_biaya_kuliah as $data}
                    <tr class="ui-widget-content">
                        <td>{$data@index+1}</td>
                        <td>{$data.NM_PROGRAM_STUDI}</td>
                        <td>{$data.NM_JENJANG}</td>
                        <td>{$data.NM_JALUR}</td>
                        <td>{$data.NM_SEMESTER} ( {$data.TAHUN_AJARAN} )</td>
                        <td>{$data.NM_KELOMPOK_BIAYA} ( {if $data.KHUSUS_KELOMPOK_BIAYA==1}Khusus{else}Reguler{/if} )</td>
                        <td>{number_format($data.BESAR_BIAYA_PENDAFTARAN)}</td>
                        <td>{number_format($data.BESAR_BIAYA_AKTIF)}</td>
                        <td>{$data.KETERANGAN_BIAYA_KULIAH}</td>
                        <td class="center">
                            <input type="radio" name="biaya_kuliah" {if $data.ID_BIAYA_KULIAH==$biaya_kuliah_mhs.ID_BIAYA_KULIAH}checked="true"{/if} value="{$data.ID_BIAYA_KULIAH}"/>
                        </td>
                        <td>
                            <a class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" href="biaya.php?mode=detail&id_biaya_kuliah={$data.ID_BIAYA_KULIAH}">
                                Detail
                            </a>
                        </td>
                    </tr>
                {/foreach}
            </tbody>
            <tfoot>
                <tr class="ui-widget-content">
                    <td colspan="11" class="center total"><input type="submit" value="Pilih" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" /></td>
                </tr>
            </tfoot>
        </table>
    </form> -->
    <span class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" onclick="history.back(-1)">Kembali</span>
{/if}
{literal}
    <script>
        $('#form').submit(function() {
            if ($('#cari').val() == '') {
                $('#alert').fadeIn();
                return false;
            }
        });
    </script>
{/literal}