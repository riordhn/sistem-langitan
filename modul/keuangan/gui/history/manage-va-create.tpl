<form id="form_create_va" class="form_create_va" action="history-bayar.php?cari={$cari}" method="post" >
    <input type="hidden" name="mode" value="create"/>
    <table class="ui-widget" style="width: 96%;">
        <tr class="ui-widget-header">
            <th>BUAT BARU</th>
        </tr>
        <tr class="ui-widget-content">
            <td>
                <ul>                    
                    <li style="font-size:1.1em;font-weight:bolder;width:100%;height:30px">
                        <span style="float:left">BANK</span> 
                        <select name="bank" id="bank_vendor" style="float:right;">
                            <option value="">---PILIH BANK---</option>
                            {$jumlah_bank_vendor=0}
                            {foreach $data_bank as $d}
                                {if $d.KODE_BANK=='9'}
                                    <option value="{$d.ID_BANK_VENDOR}">{$d.NAMA_BANK}</option>
                                    {$jumlah_bank_vendor=$jumlah_bank_vendor+1}
                                {/if}
                            {/foreach}
                        </select>
                    </li>                    
                    <li style="font-size:1.1em;font-weight:bolder;width:100%;height:30px">
                        <span style="float:left">NOMOR VA</span> 
                        <input style="float:right;text-align:center" type="text" id="manage_no_va" name="no_va" size="20" value="" readonly="true"/>
                    </li>
                    <li style="font-size:1.1em;font-weight:bolder;width:100%;height:30px">
                        <span style="float:left">TANGGAL EXPIRED</span> 
                        <input class="date" style="float:right;text-align:center" id="manage_tgl_va" type="text" name="tgl_expired" value="" readonly="true"/>
                        <input id="manage_time_va" type="hidden" id="manage_time_va"  name="time_expired" value=""/>
                    </li>
                </ul>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td class="center total">
                <input type="hidden" id="nim_mhs" name="nim_mhs" value="{$mahasiswa.NIM_MHS}"/>
                <input type="submit" id="manage_submit_va" style="display:none" value="SIMPAN" class="ui-button ui-corner-all ui-state-hover"/>
                
            </td>
        </tr>
    </table>
</form>
<style>
    ul{
        margin:0px;
    }
</style>
<script>
    $('#bank_vendor').change(function(){
        $.ajax({
            url: "manage-va.php?cari=" + $('#nim_mhs').val() + "&id_bank="+$(this).val()+"&mode=generate-va",
            type: "get",
            success: function(data) {
                result = JSON.parse(data);
                $("#manage_no_va").val(result.no_va);
                $("#manage_tgl_va").val(result.tgl_expired);
                $("#manage_time_va").val(result.time_expired);
                $('#manage_submit_va').show();
            }
      });
    });
    $("form.form_create_va").submit(function(e) {
        $.ajax({
            type:'post',
            url:'create-billing-va.php',
            data:$(this).serialize(),
            beforeSend: function() {          
                $('#manage_va').html(htmlLoading);
            },
            success:function(data){                    
                $("#manage_va").dialog("close");
                var result =JSON.parse(data);
                console.log(result);
                if(result['status']!='000'){
                    Swal.fire({
                        title: 'Error!',
                        html:
                        'Gagal melakukan update ke Sistem VA<br/><i style="font-size:0.8em"> ' +
                        result['message']+
                        '</i>',
                        icon: 'error',
                        confirmButtonText: 'OK',
                        onClose:() => {
                            window.location.reload(true);
                        }
                    })
                }else{
                    Swal.fire({
                        title: 'Sukses!',
                        text: 'Berhasil melakukan update ke Sistem BNI',
                        icon: 'success',
                        confirmButtonText: 'OK',
                        onClose:() => {
                            window.location.reload(true);
                        }
                    })
                }
                
            }                    
        });
        e.preventDefault();
        return false;
    });
</script>
