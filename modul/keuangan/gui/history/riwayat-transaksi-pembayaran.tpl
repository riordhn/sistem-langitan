<div class="center_title_bar">Detail Riwayat Pembayaran Mahasiswa</div>
<span class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:5px 15px;font-weight:bold" onclick="history.back(-1)">KEMBALI</span>
<!--DETAIL BIODATA MAHASISWA-->
{include file='history/history-bayar-biodata.tpl'}
<p></p>
<table class="ui-widget" style="width:70%;font-size:1.2em">
    <caption>
    INFORMASI TAGIHAN <br/>
    SEMESTER {$data_tagihan_pembayaran[0]['SEMESTER']}
    <hr>
    
    </caption>
    <tr class="ui-widget-header">
        <th>NO</th>
        <th>Nama Biaya</th>
        <th>Jumlah Tagihan</th>
        <th>Jumlah Terbayar</th>
        <th>Lunas</th>
    </tr>
        {$total_tagihan=0}
        {$total_terbayar=0}    
        {foreach $data_tagihan_pembayaran as $d}
        <tr class="ui-widget-content">
            <td>{$d@index+1}</td>
            <td>{$d['NAMA_BIAYA']}</td>
            <td style="text-align:right">{number_format($d['BESAR_TAGIHAN'])}</td>
            <td style="text-align:right">{number_format($d['BESAR_TERBAYAR'])}</td>
            <td class="center">{if ($d['IS_LUNAS']=='1'&&count($data_riwayat_pembayaran)>0)||($d['BESAR_TAGIHAN']==0&&count($data_riwayat_pembayaran)>0)} <b style='font-size:1.2em;color:green'>Sudah</b> {else} <b style='font-size:1.2em;color:red'>Belum</b>{/if}</td>        
        </tr>
        {$total_tagihan=$total_tagihan+$d['BESAR_TAGIHAN']}
        {$total_terbayar=$total_terbayar+$d['BESAR_TERBAYAR']}
        {/foreach}
    <tr class="total" >
        <td colspan="2">TOTAL</td>
        <td style="text-align:right">{number_format($total_tagihan)}</td>
        <td style="text-align:right">{number_format($total_terbayar)}</td>
        <td></td>
    </tr>
</table>
<table class="ui-widget" style="width:70%;font-size:1.2em">
    <caption>
    RIWAYAT PEMBAYARAN
    <hr/>
    </caption>
    {foreach $data_riwayat_pembayaran as $d}
    <tr class="ui-widget-content">
        <td colspan=5>
        <p style="font-size:1.1em">
            <b style="color:#c42700"><u>PEMBAYARAN KE-{$d@index+1} </u></b> <br/>
            <b>NO.KUITANSI</b> : {$d.NO_KUITANSI} <br/>
            <b>PERIODE_BAYAR</b> : {$d.PERIODE_BAYAR} <br/>
            <b>STATUS</b> : {$d.NAMA_STATUS} <br/>
            <b>TANGGAL BAYAR</b> : {date("d/m/Y",strtotime($d.TGL_BAYAR))} <br/>
            <b>NO.TRANSAKSI</b> : {$d.NO_TRANSAKSI} <br/>
            <b>DIBAYARKAN KE</b> : {$d.NM_BANK} <br/>
            <b>CARA BAYAR</b> : {$d.NAMA_BANK_VIA} <br/>
            <b>TOTAL TERBAYAR</b> : {number_format($d.TOTAL_BAYAR)} <br/>
            <b>KETERANGAN</b> : {$d.KETERANGAN} <br/>
            <hr/>
            <span onclick="confirm_delete('{$d.NO_TRANSAKSI}','{$smarty.get.cari}','{$smarty.get.id_tagihan_mhs}')" class="ui-button ui-corner-all ui-state-error" style="padding:4px;cursor:pointer;margin:2px;color:white">Hapus</span>
            {if $id_pt==1}
              <span class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" onclick="window.open('cetak-kuitansi-pembayaran.php?cari={$cari}&id_tagihan={$d.ID_TAGIHAN_MHS}&no_transaksi={$d.NO_TRANSAKSI}&no_kuitansi={$d.NO_KUITANSI}','_blank')">Cetak Bukti Bayar</span>
            {else}
            <span class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" onclick="window.open('cetak-pembayaran-by-nomor-transaksi.php?cari={$cari}&no_transaksi={$d.NO_TRANSAKSI}','_blank')">Cetak Bukti Bayar</span>
            {/if}
            
        </p>
        </td>
    </tr>
    <tr class="ui-widget-header">
        <th>NO</th>
        <th>Nama Biaya</th>
        <th>Jumlah Pembayaran</th>
    </tr>
        {$total_bayar=0}    
        {foreach $d.DATA_BIAYA_PEMBAYARAN as $db}
        <tr class="ui-widget-content">
            <td>{$db@index+1}</td>
            <td>
              {$db['NM_BIAYA']}
              {if count($db.DATA_PEMBAYARAN_PERBULAN)>0}
                  <hr/>
                  <ul>
                   {foreach $db.DATA_PEMBAYARAN_PERBULAN as $dpb}
                   <li>
                   <span style="width:50%;float:left;">{$dpb.NM_BULAN}</span>
                   <b style="width:50%;float:right;text-align:right">{number_format($dpb.BESAR_PEMBAYARAN)}</b>
                   </li>
                   {/foreach}
                   </ul>
              {/if}
            </td>
            <td style="text-align:right">{number_format($db['BESAR_PEMBAYARAN'])}</td>
        </tr>
        {$total_bayar=$total_bayar+$db['BESAR_PEMBAYARAN']}            
        {/foreach}
        <tr class="total" >
            <td colspan="2">TOTAL</td>
            <td style="text-align:right">{number_format($total_bayar)}</td>
        </tr>
    {foreachelse}
    <tr>
        <td colspan=5><label style="color:red">Tidak ada transaksi pembayaran</label></td>
    </tr>
    {/foreach}
</table>
{literal}
<script>  
  function confirm_delete(no_transaksi,nim,id_tagihan_mhs) {
      var base_url = window.location.protocol+'//'+window.location.hostname;
    var status = confirm("Apakah anda yakin menghapus pembayaran ini?");
    if (status == true) {
      $.ajax({
        url: "riwayat-transaksi-pembayaran.php?cari="+nim+"&id_tagihan_mhs="+id_tagihan_mhs,
        type: "post",
        data: "mode=hapus_riwayat&no_transaksi=" +no_transaksi,
        beforeSend: function() {
          $("#center_content").html(
            '<div style="width: 100%;" align="center"><img src="'+base_url+'/js/loading.gif" /></div>'
          );
        },
        success: function(data) {
          $("#center_content").html(data);
        }
      });
    }
  }
</script>
{/literal}
