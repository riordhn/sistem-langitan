<form id="form_update_biaya_va" action="update-biaya-va.php?cari={$cari}" method="post" >
    <input type="hidden" name="url" id="current_url" value="update-biaya-va.php?{$smarty.SERVER.QUERY_STRING}"/>                    
    <table class="ui-widget" style="width: 96%;">
        <tr class="ui-widget-content">
            <td>
                <ul>                    
                    <li style="font-size:1.1em;font-weight:bolder;width:100%;height:30px">
                        <span style="float:left">TAGIHAN SEMESTER</span> 
                        <select name="id_tagihan_mhs" id="select_tagihan" style="float:right;">
                            <option value="">---PILIH---</option>
                            {foreach $data_tagihan_semester as $d}
                                {if $d.TOTAL_TERBAYAR<$d.TOTAL_TAGIHAN}
                                    <option value="{$d.ID_TAGIHAN_MHS}" {if $d.ID_TAGIHAN_MHS==$id_tagihan_mhs} selected="true" {/if}>{$d.NM_SEMESTER} {$d.TAHUN_AJARAN} ({number_format($d.TOTAL_TAGIHAN)})</option>
                                {/if}
                            {/foreach}
                        </select>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
    {if count($data_detail_biaya_tagihan)>0}
        <table class="ui-widget" style="width:98%">
            <tr class="ui-widget-header">
                <th>NO</th>
                <th>Nama Biaya</th>
                <th>Jumlah Tagihan</th>
                <th>Terbayar</th>
                <th>Dibayar</th>
            </tr>
            {$total_tagihan=0}
            {$total_terbayar=0}
            {foreach $data_detail_biaya_tagihan as $d}
            <tr class="ui-widget-content">
                <td>{$d@index+1}</td>
                <td>{$d['NM_BIAYA']}</td>            
                <td style="text-align:right" >{number_format($d['BESAR_BIAYA'])}</td>
                <td style="text-align:right" >{number_format($d['TOTAL_TERBAYAR'])}</td>
                <td class="center">
                    <input type="hidden" name="id_tagihan{$d@index+1}" value="{$d['ID_TAGIHAN']}"/>
                    {if $d['BESAR_BIAYA']==$d['TOTAL_TERBAYAR']}
                        <input style="text-align:right" type="number" class="biaya_dibayar" id="biaya_dibayar{$d@index+1}" data-id="{$d@index+1}" name="biaya_dibayar{$d@index+1}" value="0" readonly="true"/>
                    {else}
                        <input style="text-align:right" type="number" class="biaya_dibayar" id="biaya_dibayar{$d@index+1}" data-id="{$d@index+1}" name="biaya_dibayar{$d@index+1}" value="0"/>                        
                        {if $d['ID_JENIS_DETAIL_BIAYA']=='4'&&!empty($d['DATA_BAYAR_BULAN'])}
                            <li style="list-style:none">
                                <hr/>
                                <ul>
                                {foreach $d['DATA_BAYAR_BULAN'] as $bs}
                                   <span style="text-align:right">{$bs['NM_BULAN']}</span><br/>                                                 
                                    <input type="hidden" name="biaya_dibayar_nm_bulan_{$d@index+1}_{$bs@index+1}" value="{$bs['NM_BULAN']}"/>
                                    <input type="hidden" name="biaya_dibayar_kd_bulan_{$d@index+1}_{$bs@index+1}" value="{$bs['KODE_BULAN']}"/>
                                    <input class="biaya_dibayar_bulan"  id="biaya_dibayar_bulan{$bs@index+1}" data-id="{$bs@index+1}" data-biaya-id="{$d@index+1}" style="text-align:right" type="text" name="biaya_dibayar_bulan_{$d@index+1}_{$bs@index+1}" value="{($d['BESAR_BIAYA']/6)-$bs['PERBULAN_TERBAYAR']}"/>
                                    <br/>
                                {/foreach}
                                </ul>
                                <hr/>
                            </li>
                        {/if}
                    {/if}
                    
                </td>
            </tr>
            {$total_tagihan=$total_tagihan+$d['BESAR_BIAYA']}
            {$total_terbayar=$total_terbayar+$d['TOTAL_TERBAYAR']}
            {/foreach}
            <tr class="ui-widget-content">
                <td colspan="2"><b>TOTAL</b></td>
                <td style="text-align:right">
                    <b>{number_format($total_tagihan)}</b>
                </td>
                <td style="text-align:right">
                    <b>{number_format($total_terbayar)}</b>
                </td>
                <td class="center">
                    <input style="text-align:right" type="number" name="total_dibayar" id="total_dibayar" readonly value="0"/>
                </td>
            </tr>
            <tr>
                <td colspan="5" class="center total">
                    <p>JUMLAH PEMBAYARAN VA : {number_format($pembayaran_va.PAYMENT_AMOUNT)}</p>
                    <hr/>
                    <p>PEMBAYARAN TERPROSES : {number_format($pembayaran_va.PEMBAYARAN_DIPROSES)}</p>
                    <hr/>
                    <p>PEMBAYARAN SISA : {number_format($pembayaran_va.PAYMENT_AMOUNT-$pembayaran_va.PEMBAYARAN_DIPROSES)}</p>
                    <hr/>
                    <input type="hidden" id="dibayar_va" value="{$pembayaran_va.PAYMENT_AMOUNT}"/>
                    <input type="hidden" id="diproses_va" value="{$pembayaran_va.PEMBAYARAN_DIPROSES}"/>
                    <input type="hidden" name="bank" value="{$pembayaran_va.ID_BANK}"/>
                    <input type="hidden" name="tgl_bayar" value="{date("Y-m-d",strtotime($pembayaran_va.PAYMENT_TIME))}"/>
                    <input type="hidden" name="no_transaksi" value="{$pembayaran_va.PAYMENT_NTB}"/>
                    <input type="hidden" name="id_tagihan_mhs" value="{$id_tagihan_mhs}"/>
                    <input type="hidden" name="jumlah_data" value="{count($data_detail_biaya_tagihan)}"/>
                    <input type="hidden" name="mode" value="update_biaya"/>
                    <input type="submit" id="btn-submit-master" value="SIMPAN" class="ui-button ui-corner-all ui-state-hover"/>
                </td>
            </tr>
        </table>
    {/if}
</form>
<script>
    $('.biaya_dibayar').keyup(function(){
        var total_dibayar =0;
        $('.biaya_dibayar').each(function(){
            var id=$(this).attr('data-id');
            var value=$('#biaya_dibayar'+id).val();
            total_dibayar+=parseInt(value);
        });
        console.log(total_dibayar);
        $("#total_dibayar").val(total_dibayar);
    });
    $('.biaya_dibayar_bulan').keyup(function(){
        var total_bulanan =0;
        var id_biaya=$(this).attr('data-biaya-id');
            
        $('.biaya_dibayar_bulan').each(function(){
            var id=$(this).attr('data-id');
            var value=$('#biaya_dibayar_bulan'+id).val();
            console.log(value);
            total_bulanan+=parseInt(value);
            console.log(total_bulanan);
        });
        $("#biaya_dibayar"+id_biaya).val(total_bulanan);

        var total_tagihan =0;
        $('.biaya_dibayar').each(function(){
            var id=$(this).attr('data-id');
            var value=$('#biaya_dibayar'+id).val();
            console.log(value);
            total_tagihan+=parseInt(value);
            console.log(total_tagihan);
        });
        $("#total_dibayar").val(total_tagihan);
    });
    $('#select_tagihan').change(function(){
        $.ajax({
            url: $('#current_url').val()+'&id_tagihan_mhs='+$(this).val(),
            type: "get",
            beforeSend: function() {          
                $('#update_biaya').html(
                    htmlLoading
                );
            },
            success: function(data) {
                $('#update_biaya').html(data);
            }
        });
    });
    $("#btn-submit-master").click(function(e) {
        e.preventDefault();
        var total_dibayar=$("#total_dibayar").val();
        var dibayar_va =$("#dibayar_va").val();
        var diproses_va =$("#diproses_va").val();
        if(total_dibayar<=(dibayar_va-diproses_va)){
            $.ajax({
                type:'post',
                url:'update-biaya-va.php',
                data:$('#form_update_biaya_va').serialize(),
                beforeSend: function() {          
                    $('#update_biaya').html(htmlLoading);
                },
                success:function(data){                    
                    $("#update_biaya").dialog("close");
                    var result =JSON.parse(data);
                    console.log(result);
                    if(result['status']!='1'){
                        Swal.fire({
                            title: 'Error!',
                            html:
                            'Gagal <br/><i style="font-size:0.8em"> ' +
                            result['message']+
                            '</i>',
                            icon: 'error',
                            confirmButtonText: 'OK',
                            onClose:() => {
                                window.location.reload(true);
                            }
                        });
                    }else{
                        Swal.fire({
                            title: 'Sukses!',
                            text: result['message'],
                            icon: 'success',                            
                            confirmButtonText: 'OK',
                            onClose:() => {
                                window.location.reload(true);
                            }
                        });                        
                        
                    }
                }                    
            });
        }
        else{
            Swal.fire({
                title: 'Error!',
                html: 'Jumlah yang di bayar tidak sesuai pembayaran<br/>',
                icon: 'error',
                confirmButtonText: 'OK'
            })
        }
        
    });
</script>