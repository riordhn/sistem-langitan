<h2>DETIL TRANSAKSI</h2>
<table class="ui-widget" style="margin:5px 0px">
    <tr class="ui-widget-header">
        <th colspan="2">DESKRIPSI</th>
    </tr>
    <tr class="ui-widget-content">
        <td>TANGGAL BAYAR</td>
        <td>{$data_transaksi[0].TGL_BAYAR}</td>
    </tr>
    <tr class="ui-widget-content">
        <td>BANK</td>
        <td>{$data_transaksi[0].NM_BANK}</td>
    </tr>
    <tr class="ui-widget-content">
        <td>BANK VIA</td>
        <td>{$data_transaksi[0].NAMA_BANK_VIA}</td>
    </tr>
    <tr class="ui-widget-content">
        <td>NO TRANSAKSI</td>
        <td>{$data_transaksi[0].NO_TRANSAKSI}</td>
    </tr>
</table>
<h2>BIAYA YANG DIBAYARKAN</h2>
<table class="ui-widget" style="margin:5px 0px">
    <tr class="ui-widget-header">
        <th>NO</th>
        <th>NAMA BIAYA</th>
        <th>BESAR PEMBAYARAN</th>
    </tr>
    {$total=0}
    {foreach $data_transaksi as $d}
        <tr class="ui-widget-content">
            <td>{$d@index+1}</td>
            <td>{$d.NM_BIAYA}</td>
            <td class="center">{number_format($d.BESAR_PEMBAYARAN)}</td>
        </tr>
    {$total=$total+$d.BESAR_PEMBAYARAN}
    {/foreach}
    <tr>
        <td colspan="3" class="total center"><b>TOTAL PEMBAYARAN <br/>Rp. {number_format($total)}</b></td>
    </tr>
</table>
<span class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" onclick="window.open('cetak-pembayaran-by-nomor-transaksi.php?cari={$cari}&no_transaksi={$data_transaksi[0].NO_TRANSAKSI}','_blank')">Cetak Bukti Bayar</span>