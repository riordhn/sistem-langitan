<form id="form_proses_pembayaran" action="history-bayar.php?cari={$cari}" method="post" >
    <input type="hidden" name="mode" value="proses_bayar"/>
    <table class="ui-widget" style="width: 96%;">
        <tr class="ui-widget-header">
            <th>DETAIL BIAYA TAGIHAN</th>
        </tr>
        
        <tr class="ui-widget-content">
            <td class="center" style="font-size:1.3em">
                <input type="radio" name="cara_bayar" checked="" id="manual"/>Manual
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>
                {if count($data_detail_biaya_tagihan)>0}
                        <ul>
                            {$total_tagihan=0}
                            {foreach $data_detail_biaya_tagihan as $db}
                                {if ($db['BESAR_BIAYA']>$db['TOTAL_TERBAYAR'])||($db['BESAR_BIAYA']==0&&$db['SUDAH_ADA_PEMBAYARAN']==0)}
                                    <input type="hidden" name="tagihan{$db@index+1}" value="{$db['ID_TAGIHAN']}"/>
                                    <input type="hidden" class="besar_biaya_default" id="besar_biaya_default{$db@index+1}" data-id="{$db@index+1}" value="{$db['BESAR_BIAYA']}" name="besar_biaya_default{$db@index+1}"/>
                                    <li style="font-size:1.1em;font-size:bold;width:100%;height:30px">
                                        <span style="float:left">{$db['NM_BIAYA']}</span> 
                                        <input class="biaya_dibayar"  id="biaya_dibayar{$db@index+1}" data-id="{$db@index+1}" style="float:right;text-align:right" type="text" name="biaya_dibayar{$db@index+1}" value="{$db['BESAR_BIAYA']-$db['TOTAL_TERBAYAR']}"/>
                                    </li>
                                    {if $db['ID_JENIS_DETAIL_BIAYA']=='4'&&!empty($db['DATA_BAYAR_BULAN'])}
                                        <li style="list-style:none">
                                            <hr/>
                                            <ul>
                                            {foreach $db['DATA_BAYAR_BULAN'] as $bs}
                                                <li style="font-size:1.1em;font-size:bold;width:100%;height:30px"><span style="float:left;margin-left:15px">{$bs['NM_BULAN']}</span>                                                     
                                                    <input type="hidden" name="biaya_dibayar_nm_bulan_{$db@index+1}_{$bs@index+1}" value="{$bs['NM_BULAN']}"/>
                                                    <input type="hidden" name="biaya_dibayar_kd_bulan_{$db@index+1}_{$bs@index+1}" value="{$bs['KODE_BULAN']}"/>
                                                    <input class="biaya_dibayar_bulan"  id="biaya_dibayar_bulan{$bs@index+1}" data-id="{$bs@index+1}" data-biaya-id="{$db@index+1}" style="float:right;text-align:right" type="text" name="biaya_dibayar_bulan_{$db@index+1}_{$bs@index+1}" value="{($db['BESAR_BIAYA']/6)-$bs['PERBULAN_TERBAYAR']}"/>
                                                </li>
                                            {/foreach}
                                            </ul>
                                            <hr/>
                                        </li>
                                    {/if}
                                    {$total_tagihan=$total_tagihan+($db['BESAR_BIAYA']-$db['TOTAL_TERBAYAR'])}
                                {/if}
                            {/foreach}
                            <hr/>
                            <li style="font-size:1.1em;font-weight:bolder"><span style="float:left">TOTAL AKAN DIBAYAR</span> <input style="float:right;text-align:right" type="text" id="total_dibayar" readonly="true" name="total_dibayar" value="{$total_tagihan}"/></li>
                        </ul>
                    {/if}
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>
                <ul>
                    <li style="font-size:1.1em;font-weight:bolder;width:100%;height:30px">
                        <span style="float:left">TANGGAL BAYAR</span> 
                        <input class="date" style="float:right;text-align:center" type="text" name="tgl_bayar" value="" required/>
                    </li>
                    <li style="font-size:1.1em;font-weight:bolder;width:100%;height:50px">
                        <span style="float:left">KETERANGAN</span> <textarea name="keterangan" style="float:right;width:200px;height:30px;resize:none" >Bulan</textarea>
                    </li>
                     <li style="font-size:1.1em;font-weight:bolder;width:100%;height:30px">
                        <span style="float:left">PERIODE BAYAR</span> 
                        <select name="periode_bayar" style="float:right;">
                            <option value="">---PILIH PERIODE BAYAR---</option>
                            <option value="Pagi">Pagi</option>
                            <option value="Siang">Siang</option>
                            <option value="Malam">Malam</option>
                        </select>
                    </li>
                    <li style="font-size:1.1em;font-weight:bolder;width:100%;height:30px">
                        <span style="float:left">BANK</span> 
                        <select name="bank" style="float:right;">
                            <option value="">---PILIH NAMA BANK---</option>
                            {foreach $data_bank as $d}
                                <option value="{$d.ID_BANK}" {if $d.ID_BANK==6} selected {/if}>{$d.NM_BANK}</option>
                            {/foreach}
                        </select>
                    </li>
                    <li style="font-size:1.1em;font-weight:bolder;width:100%;height:30px">
                        <span style="float:left">VIA</span>
                        <select name="bank_via" style="float:right;">
                            <option value="">---PILIH VIA PEMBAYARAN---</option>
                            {foreach $data_bank_via as $d}
                                <option value="{$d.ID_BANK_VIA}" {if $d.ID_BANK_VIA==9} selected {/if}>{$d.NAMA_BANK_VIA}</option>
                            {/foreach}
                        </select>
                    </li>
                    <li style="font-size:1.1em;font-weight:bolder;width:100%;height:30px">
                        <span style="float:left">STATUS</span>
                        <select name="status_pembayaran" style="float:right;">
                            <option value="">---PILIH STATUS PEMBAYARAN---</option>
                            {foreach $data_status_pembayaran as $d}
                                <option value="{$d.ID_STATUS_PEMBAYARAN}" {if $d.ID_STATUS_PEMBAYARAN==1} selected {/if}>{$d.NAMA_STATUS}</option>
                            {/foreach}
                        </select>
                    </li>
                </ul>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td class="center total">
                <input type="hidden" name="id_tagihan_mhs" value="{$id_tagihan_mhs}"/>
                <input type="hidden" name="nm_semester" value="{$data_detail_biaya_tagihan[0]['NM_SEMESTER']}"/>
                <input type="hidden" name="jumlah_data" value="{count($data_detail_biaya_tagihan)}"/>
                <input type="submit" value="SIMPAN" class="ui-button ui-corner-all ui-state-hover"/>
            </td>
        </tr>
    </table>
</form>
<style>
    ul{
        margin:0px;
    }
</style>
<script>
    $('#cicilan_dibayar').keyup(function(){
        var total_cicilan = $('#total_cicilan').val();
        var jumlah_cicilan = $('#cicilan_dibayar').val();
        var total_tagihan =0;
        $('.biaya_dibayar').each(function(){
            var id=$(this).attr('data-id');
            var value=$('#besar_biaya_default'+id).val();
            var besar_biaya_cicilan=value/total_cicilan*jumlah_cicilan;
            $(this).val(besar_biaya_cicilan);
            total_tagihan+=parseInt(besar_biaya_cicilan);
        });
        $("#total_dibayar").val(total_tagihan);
    });
    $('.biaya_dibayar').keyup(function(){
        var total_tagihan =0;
        $('.biaya_dibayar').each(function(){
            var id=$(this).attr('data-id');
            var value=$('#biaya_dibayar'+id).val();
            console.log(value);
            total_tagihan+=parseInt(value);
            console.log(total_tagihan);
        });
        $("#total_dibayar").val(total_tagihan);
    });
     $('.biaya_dibayar_bulan').keyup(function(){
        var total_bulanan =0;
        var id_biaya=$(this).attr('data-biaya-id');
            
        $('.biaya_dibayar_bulan').each(function(){
            var id=$(this).attr('data-id');
            var value=$('#biaya_dibayar_bulan'+id).val();
            console.log(value);
            total_bulanan+=parseInt(value);
            console.log(total_bulanan);
        });
        $("#biaya_dibayar"+id_biaya).val(total_bulanan);

        var total_tagihan =0;
        $('.biaya_dibayar').each(function(){
            var id=$(this).attr('data-id');
            var value=$('#biaya_dibayar'+id).val();
            console.log(value);
            total_tagihan+=parseInt(value);
            console.log(total_tagihan);
        });
        $("#total_dibayar").val(total_tagihan);
    });
    $("#form_proses_pembayaran").submit(function() {
        $("#proses_pembayaran").dialog("close");
    });
    
    $(".date").datepicker({
        dateFormat: "dd-M-yy",
        changeMonth: true,
        changeYear: true
    });
    $('#cicilan').click(function(){
        $('#detail_cicilan').show();
    });
    $('#manual').click(function(){
        $('#detail_cicilan').hide();
    });
</script>
