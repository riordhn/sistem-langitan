<form id="form_create_va" class="form_create_va" action="history-bayar.php?cari={$cari}" method="post" >
    <input type="hidden" name="mode" value="update"/>
    <table class="ui-widget" style="width: 96%;">
        <tr class="ui-widget-header">
            <th>AKTIFKAN</th>
        </tr>
        <tr class="ui-widget-content">
            <td>
                <ul>                    
                    <li style="font-size:1.1em;font-weight:bolder;width:100%;height:30px">
                        <span style="float:left">BANK</span> 
                        <select name="bank" id="bank_vendor" style="float:right;">
                            {foreach $data_bank as $d}
                                {if $d.ID_BANK_VENDOR==$virtual_account.ID_BANK_VENDOR}
                                    <option value="{$d.ID_BANK_VENDOR}">{$d.NAMA_BANK}</option>
                                {/if}
                            {/foreach}
                        </select>
                    </li>                    
                    <li style="font-size:1.1em;font-weight:bolder;width:100%;height:30px">
                        <span style="float:left">NOMOR VA</span> 
                        <input style="float:right;text-align:center" type="text" id="manage_no_va" name="no_va" size="20" value="{$virtual_account.NO_VA}" readonly="true"/>
                    </li>
                    <li style="font-size:1.1em;font-weight:bolder;width:100%;height:30px">
                        <span style="float:left">TANGGAL EXPIRED</span> 
                        <input class="date" style="float:right;text-align:center" id="manage_tgl_va" type="text" name="tgl_expired" value="{$virtual_account.DATE_EXPIRED}" readonly="true"/>
                        <input id="manage_time_va" type="hidden" id="manage_time_va"  name="time_expired" value="{$virtual_account.DATETIME_EXPIRED}"/>
                    </li>
                </ul>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td class="center total">
                <input type="hidden" id="nim_mhs" name="nim_mhs" value="{$mahasiswa.NIM_MHS}"/>
                <input type="hidden" name="trx_id" value="{$virtual_account.TRX_ID}"/>
                <input type="submit" id="manage_submit_va" value="SIMPAN" class="ui-button ui-corner-all ui-state-hover"/>
            </td>
        </tr>
    </table>
</form>
<style>
    ul{
        margin:0px;
    }
</style>
<script>
    $("form.form_create_va").submit(function(e) {
        $.ajax({
            type:'post',
            url:'create-billing-va.php',
            data:$(this).serialize(),
            beforeSend: function() {          
                $('#manage_va').html(htmlLoading);
            },
            success:function(data){                    
                $("#manage_va").dialog("close");
                var result =JSON.parse(data);
                console.log(result);
                if(result['status']!='000'){
                    Swal.fire({
                        title: 'Error!',
                        html:
                        'Gagal melakukan update ke Sistem VA<br/><i style="font-size:0.8em"> ' +
                        result['message']+
                        '</i>',
                        icon: 'error',
                        confirmButtonText: 'OK',
                        onClose:() => {
                            window.location.reload(true);
                        }
                    })
                }else{
                    Swal.fire({
                        title: 'Sukses!',
                        text: 'Berhasil melakukan update ke Sistem BNI',
                        icon: 'success',
                        confirmButtonText: 'OK',
                        onClose:() => {
                            window.location.reload(true);
                        }
                    })
                }
                
            }                    
        });
        e.preventDefault();
        return false;
    });
</script>
