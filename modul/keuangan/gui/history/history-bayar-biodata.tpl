
<table style="width: 50%;" class="ui-widget">
  <tr class="ui-widget-header">
    <th colspan="2" class="header-coloumn" style="text-align: center">
      Informasi Biodata {if $jenis=='cmhs'} Calon {/if}Mahasiswa
    </th>
  </tr>
  {if $jenis=='mhs'}
  <tr class="ui-widget-content">
    <td>NIM</td>
    <td width="60%">{$data_detail_mahasiswa.NIM_MHS}</td>
  </tr>
  {* 
  <tr class="ui-widget-content">
    <td>NIM BANK</td>
    <td>{$data_detail_mahasiswa.NIM_BANK}</td>
  </tr>
  <tr class="ui-widget-content">
    <td>NIM LAMA</td>
    <td>{$data_detail_mahasiswa.NIM_LAMA}</td>
  </tr> 
  *}
  <tr class="ui-widget-content">
    <td>No Ujian</td>
    <td>{$data_detail_mahasiswa.NO_UJIAN}</td>
  </tr>
  <tr class="ui-widget-content">
    <td>Nama</td>
    <td>{$data_detail_mahasiswa.NM_PENGGUNA}</td>
  </tr>
  <tr class="ui-widget-content">
    <td>Fakultas</td>
    <td>{$data_detail_mahasiswa.NM_FAKULTAS}</td>
  </tr>
  <tr class="ui-widget-content">
    <td>Program Studi</td>
    <td>{$data_detail_mahasiswa.NM_PROGRAM_STUDI}</td>
  </tr>
  <tr class="ui-widget-content">
    <td>Jenjang</td>
    <td>{$data_detail_mahasiswa.NM_JENJANG}</td>
  </tr>
  <tr class="ui-widget-content">
    <td>Jalur / Kelas</td>
    <td>{$data_detail_mahasiswa.NAMA_KELAS}</td>
  </tr>
  <tr class="ui-widget-content">
    <td>Angkatan Akademik Mahasiswa</td>
    <td>{$data_detail_mahasiswa.THN_ANGKATAN_MHS}</td>
  </tr>
  <tr class="ui-widget-content">
    <td>Kelompok Biaya</td>
    <td>{$data_detail_mahasiswa.NM_KELOMPOK_BIAYA}</td>
  </tr>
  <tr class="ui-widget-content">
    <td>Tanggal Lahir</td>
    <td>{$data_detail_mahasiswa.TGL_LAHIR_PENGGUNA}</td>
  </tr>
  <tr class="ui-widget-content">
    <td>Alamat</td>
    <td>{$data_detail_mahasiswa.ALAMAT_MHS}</td>
  </tr>
  <tr class="ui-widget-content">
    <td>Orang Tua</td>
    <td>
      Ayah : {$data_detail_mahasiswa.NM_AYAH_MHS} , Ibu
      :{$data_detail_mahasiswa.NM_IBU_MHS}
    </td>
  </tr>
  <tr class="ui-widget-content">
    <td>Contact</td>
    <td>{$data_detail_mahasiswa.MOBILE_MHS}</td>
  </tr>
  <tr class="ui-widget-content">
    <td>Status Cekal</td>
    <td>{$data_detail_mahasiswa.STATUS_CEKAL}</td>
  </tr>
  <tr class="ui-widget-content">
    <td>Status Akademik</td>
    <td>{$data_detail_mahasiswa.NM_STATUS_PENGGUNA}</td>
  </tr>
  {* <tr class="ui-widget-content">
    <td>Tanggal Lulus</td>
    <td>{$data_detail_mahasiswa.TGL_LULUS}</td>
  </tr>
  <tr class="ui-widget-content">
    <td>Nomer Ijazah</td>
    <td>{$data_detail_mahasiswa.NO_IJASAH}</td>
  </tr> *}
  {else}
  <tr class="ui-widget-content">
    <td>No Ujian</td>
    <td width="200px">{$data_detail_calon_mahasiswa.NO_UJIAN}</td>
  </tr>
  <tr class="ui-widget-content">
    <td>NIM</td>
    <td width="200px">{$data_detail_calon_mahasiswa.NIM_MHS}</td>
  </tr>
  <tr class="ui-widget-content">
    <td>Nama</td>
    <td>{$data_detail_calon_mahasiswa.NM_C_MHS}</td>
  </tr>
  <tr class="ui-widget-content">
    <td>Fakultas</td>
    <td>{$data_detail_calon_mahasiswa.NM_FAKULTAS}</td>
  </tr>
  <tr class="ui-widget-content">
    <td>Program Studi</td>
    <td>{$data_detail_calon_mahasiswa.NM_PROGRAM_STUDI}</td>
  </tr>
  <tr class="ui-widget-content">
    <td>Jenjang</td>
    <td>{$data_detail_calon_mahasiswa.NM_JENJANG}</td>
  </tr>
  <tr class="ui-widget-content">
    <td>Jalur</td>
    <td>{$data_detail_calon_mahasiswa.NM_JALUR}</td>
  </tr>
  <tr class="ui-widget-content">
    <td>Kelompok Biaya</td>
    <td>{$data_detail_calon_mahasiswa.NM_KELOMPOK_BIAYA}</td>
  </tr>
  {/if}
</table>
