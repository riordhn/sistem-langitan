<table class="ui-widget-content ui-widget" style="width: 97%">
    <caption>Pengembalian Pembayaran</caption>
    <tr class="ui-widget-header">
        <th colspan="10"class="header-coloumn">RIWAYAT PENGEMBALIAN MAHASISWA</th>
    </tr>
    <tr class="ui-widget-header">
        <th>No</th>
        <th>Semester Pembayaran</th>
        <th>Besar Pembayaran</th>
        <th>Semester Pengembalian</th>
        <th>Besar Pengembalian</th>
        <th>Tgl Pengembalian</th>
        <th>Bank</th>
        <th>Via Bank</th>
        <th>No Transaksi</th>
        <th>Keterangan</th>
    </tr>
    {$index=1}
    {foreach $data_history_pengembalian as $data}                
        <tr>
            <td>{$index}</td>
            <td>{$data.NM_SEMESTER_BAYAR} ({$data.TAHUN_AJARAN_BAYAR})</td>
            <td>{number_format($data.BESAR_PEMBAYARAN)}</td>
            <td>{$data.NM_SEMESTER} ({$data.TAHUN_AJARAN})</td>
            <td>{number_format($data.BESAR_PENGEMBALIAN)}</td>
            <td>{$data.TGL_PENGEMBALIAN}</td>
            <td>{$data.NM_BANK}</td>
            <td>{$data.NAMA_BANK_VIA}</td>
            <td>{$data.NO_TRANSAKSI}</td>
            <td>{$data.KETERANGAN}</td>
        </tr>
        {$index=$index+1}
    {foreachelse}
        <tr class="ui-widget-content">
            <td colspan="10" class="center"><span style="color: red">Data Kosong</span></td>
        </tr>
    {/foreach}
    <tr class="ui-widget-content">
        <td colspan="10" class="center">
            <a class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" href="pengembalian-pembayaran.php?cari={$data_detail_mahasiswa.NIM_MHS}">
                History Pengembalian
            </a>
        </td>
    </tr>
</table>