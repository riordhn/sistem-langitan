<link rel="stylesheet" type="text/css" href="../../css/jquery-ui-custom-keu.css" />
<link rel="stylesheet" type="text/css" href="../../css/keuangan-style.css" />
<script language="javascript" src="../../js/jquery-1.5.1.min.js"></script>
<script language="javascript" src="../../js/jquery.validate.js"></script>
<script language="javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
{literal}
    <style>
        label.error {
            color: red;
            font-size:11px;
        }
    </style>
{/literal}
<form id="form_upload" action="upload-kerjasama.php" method="post" enctype="multipart/form-data">
    <table class="ui-widget" width="700px" style="margin: 20px auto;">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn">Upload Mahasiswa Kerjasama</th>
        </tr>        
        <tr class="ui-widget-content">
            <td>Jenis Beasiswa</td>
            <td>
                <select name="beasiswa">
                    {foreach $data_beasiswa as $data}
                        <option value="{$data['ID_BEASISWA']}" {if $data['ID_BEASISWA']==$smarty.post.beasiswa}selected="true"{/if}>{$data['NM_BEASISWA']} {$data['PENYELENGGARA_BEASISWA']} {$data['NM_GROUP_BEASISWA']}</option>
                    {/foreach}
                </select>
            </td>
        </tr>

        <tr class="ui-widget-content">
            <td>Semester Mulai</td>
            <td>
                <select name="semester_mulai">
                    <option value="">Kosongi</option>
                    {foreach $data_semester as $data}
                        <option value="{$data['ID_SEMESTER']}" {if $data['ID_SEMESTER']==$smarty.post.semester}selected="true"{/if}>{$data['NM_SEMESTER']}({$data['TAHUN_AJARAN']})</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Semester Selesai</td>
            <td>
                <select name="semester_selesai">
                    <option value="">Kosongi</option>
                    {foreach $data_semester as $data}
                        <option value="{$data['ID_SEMESTER']}" {if $data['ID_SEMESTER']==$smarty.post.semester}selected="true"{/if}>{$data['NM_SEMESTER']}({$data['TAHUN_AJARAN']})</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Keterangan</td>
            <td><textarea name="keterangan" class="required"   rows="5" cols="40">{if isset($smarty.post.keterangan)}{$smarty.post.keterangan} {/if}</textarea> </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Upload File</td>
            <td><input type="file" name="file" id="file" class="required" /> </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" class="center">
                <input type="submit" name="excel" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Submit" />
                <input type="reset" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Reset" />
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="excel-kerjasama-template.php">Download Template</a>
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="upload-kerjasama.php">Reload</a>
            </td>
        </tr>
    </table>
</form>
<center>
    {$status_upload}
    {$sukses_upload}
</center>
{if isset($data_kerjasama)}
    <table class="ui-widget" width="700px" style="margin: 20px auto;">
        <tr class="ui-widget-header">
            <th colspan="5" class="header-coloumn">Status Upload Data</th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>NIM</th>
            <th>Nama</th>
            <th>Status Mahasiswa</th>
            <th>Status Beasiswa Mahasiswa</th>
        </tr>
        {foreach $data_kerjasama as $data}
            <tr  {if $data.STATUS_MAHASISWA=='Data Mahasiswa Tidak Ada'}style="background-color: #eab8b8"{else}class="ui-widget-content"{/if}>
                <td>{$data@index+1}</td>
                <td>{$data.NIM_MHS}</td>
                <td>{$data.NAMA}</td>
                <td>{$data.STATUS_MAHASISWA}</td>
                <td>{if $data.STATUS_BEASISWA!='Data Kosong'}Sudah Terdaftar {$data.STATUS_BEASISWA['NM_BEASISWA']} {$data.STATUS_BEASISWA['PENYELENGGARA_BEASISWA']} ({$data.STATUS_BEASISWA['PERIODE_PEMBERIAN_BEASISWA']}){else}{$data.STATUS_BEASISWA}{/if}</td>
            </tr>
        {/foreach}
        <tr class="ui-widget-content total">
            <td colspan="5" class="center">
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="confirm_save_data('upload-kerjasama.php?mode=save_excel')">Save Data</a>
            </td>
        </tr>
    </table>
{/if}
{literal}
    <script>
            function confirm_save_data(url){
                var c=confirm("Silahkan Periksa Kembali Parameternya. Apakah anda yakin menyimpan data ini? ");
                if(c==true){
                    window.location.href=url;
                }
            }
            $(function() {
                    $( ".date_pick" ).datepicker({dateFormat:'dd-M-yy',changeMonth: true,
                            changeYear: true});
            });
            $('form').validate();
    </script>
{/literal}