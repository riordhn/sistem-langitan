<link rel="stylesheet" type="text/css" href="../../css/jquery-ui-custom-keu.css" />
<link rel="stylesheet" type="text/css" href="../../css/keuangan-style.css" />
<script language="javascript" src="../../js/jquery-1.5.1.min.js"></script>
<script language="javascript" src="../../js/jquery.validate.js"></script>
<script language="javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
{literal}
    <style>
        label.error {
            color: red;
            font-size:11px;
        }
    </style>
{/literal}
<form method="get" id="form_data">
    <table style="width: 35%;margin: 20px auto;"class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn">Input Form</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Banyak Data Yang Ingin Dimasukkan</td>
            <td>
                <select name="data" onchange="$('#form_data').submit()">
                    <option>Pilih</option>
                    <option value="5" {if $smarty.get.data==5}selected="true"{/if}>5</option>
                    <option value="10" {if $smarty.get.data==10}selected="true"{/if}>10</option>
                    <option value="15" {if $smarty.get.data==15}selected="true"{/if}>15</option>
                    <option value="50" {if $smarty.get.data==50}selected="true"{/if}>50</option>
                    <option value="100" {if $smarty.get.data==100}selected="true"{/if}>100</option>
                </select>
            </td>
        </tr>
    </table>
</form>
<center>
    {$success}
</center>

{if $smarty.get.data!=''}
    <form action="pengembalian-pembayaran-input.php" id="form_tambah" method="post">
        <table class="ui-widget-content" style="width: 98%;margin: 20px auto;">
            <tr class="ui-widget-header">
                <th colspan="11" class="header-coloumn">Tambah Pengembalian Mahasiswa</th>
            </tr>
            <tr class="ui-widget-header">
                <th>NO</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Semester Pembayaran</th>
                <th>Besar Pembayaran</th>
                <th style="width: 160px">Besar Pengembalian</th>
                <th>Tanggal Pengembalian</th>
                <th>Nama Bank</th>
                <th>Via Bank</th>
                <th>No Transaksi</th>
                <th>Keterangan</th>
            </tr>
            {section name=data start=1 loop=$smarty.get.data+1 step=1}
                {$jumlah_data=$smarty.section.data.index}
                <tr style="vertical-align: middle;">
                    <td>{$smarty.section.data.index}</td>
                    <td>
                        <input type="text" class="nim" id="nim{$smarty.section.data.index}" name="nim{$smarty.section.data.index}" size="12" />
                        <input type="hidden" id="id_mhs{$smarty.section.data.index}" name="id_mhs{$smarty.section.data.index}" />
                    </td>
                    <td>
                        <div id="nama{$smarty.section.data.index}">Nama Mahasiswa</div>
                    </td>
                    <td>
                        <select class="semester" id="semester{$smarty.section.data.index}" name="semester{$smarty.section.data.index}">
                            {foreach $data_semester as $data}
                                <option value="{$data['ID_SEMESTER']}">{$data['NM_SEMESTER']}({$data['TAHUN_AJARAN']})</option>
                            {/foreach}
                        </select>
                    </td>
                    <td>
                        <input type="text" size="9" readonly="true" id="besar_pembayaran{$smarty.section.data.index}"  name="besar_pembayaran{$smarty.section.data.index}" class="required"/>
                    </td>
                    <td>
                        <div id="detail_pengembalian{$smarty.section.data.index}">
                        </div>
                    </td>
                    <td>
                        <input type="text" style="text-transform: uppercase;" class="date_pick required" name="tgl_pengembalian{$smarty.section.data.index}" size="10"  value="" />
                    </td>

                    <td>
                        <select name="bank{$smarty.section.data.index}">
                            {foreach $data_bank as $data}
                                <option value="{$data['ID_BANK']}">{$data['NM_BANK']}</option>
                            {/foreach}
                        </select>
                    </td>
                    <td>
                        <select name="bank_via{$smarty.section.data.index}">
                            {foreach $data_bank_via as $data}
                                <option value="{$data['ID_BANK_VIA']}" >{$data['NAMA_BANK_VIA']}</option>
                            {/foreach}
                        </select>
                    </td>
                    <td>
                        <input type="text" name="no_transaksi{$smarty.section.data.index}" size="10" value="" />
                    </td>
                    <td>
                        <textarea name="keterangan{$smarty.section.data.index}" class="required" cols="10" rows="5"></textarea>
                    </td>
                </tr>
            {/section}
            <tr>
                <td colspan="11" class="center">
                    <input type="hidden" name="mode" value="simpan"/>
                    <input type="hidden" name="jumlah_data" value="{$jumlah_data}"/>
                    <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" value="Masukkan"/>
                </td>
            </tr>
        </table>
    </form>
{/if}
{literal}
    <script>
            function confirm_save_data(url){
                var c=confirm("Silahkan Periksa Kembali Parameternya. Apakah anda yakin menyimpan data ini? ");
                if(c==true){
                    window.location.href=url;
                }
            }
            $('.nim').focusout(function(){
                id=$(this).attr('id').replace('nim','');
                $.ajax({
                    url:'cari-nim.php',
                    type:'post',
                    data:'mode=nama&nim='+$(this).val(),
                    success:function(data){
                        $('#nama'+id).html(data)
                    }
                });
                $.ajax({
                    url:'cari-nim.php',
                    type:'post',
                    data:'mode=id_mhs&nim='+$(this).val(),
                    success:function(data){
                        $('#id_mhs'+id).val(data)
                    }
                });
            });
            $('.semester').change(function(){
                id=$(this).attr('id').replace('semester','');
                $.ajax({
                    type:'post',
                    url:'get-detail-biaya.php',
                    data:'semester='+$(this).val()+'&id_mhs='+$('#id_mhs'+id).val()+'&id='+id,
                    success:function(data){
                        $('#detail_pengembalian'+id).html(data);
                    }                    
                });
                $.ajax({
                    type:'post',
                    url:'get-besar-pembayaran.php',
                    data:'id_semester='+$(this).val()+'&id_mhs='+$('#id_mhs'+id).val(),
                    success:function(data){
                        $('#besar_pembayaran'+id).val(data);
                    }                    
                })
            });
            $(function() {
                    $( ".date_pick" ).datepicker({dateFormat:'dd-M-yy',changeMonth: true,
                            changeYear: true});
            });
            $('form').validate();
    </script>
{/literal}