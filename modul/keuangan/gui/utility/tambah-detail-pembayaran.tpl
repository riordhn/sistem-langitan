<form id="form_tambah_detail_pembayaran" action="history-bayar.php?cari={$smarty.get.cari}" method="post">
    <table class="ui-widget" style="width: 40%">
        <tr class="ui-widget-header">
            <th>Semester Pembayaran</th>
            <th>Nama Biaya</th>
            <th>Besar Biaya</th>
        </tr>
        <tr class="ui-widget-content">
            <td>
                <input type="hidden" name="id_mhs" value="{$id_mhs}"/>
                <input type="hidden" name="mode" value="tambah_biaya"/>
                <input type="hidden" name="cari" value="{$smarty.get.cari}"/>
                <select name="semester">
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}">{$data['NM_SEMESTER']}({$data['TAHUN_AJARAN']})</option>
                    {/foreach}
                </select>
            </td>
            <td>
                <select name="biaya">
                    {foreach $data_biaya as $data}
                        <option value="{$data.ID_BIAYA}">{$data.NM_BIAYA}</option>
                    {/foreach}
                </select>
            </td>
            <td>
                <input type="number" name="besar_biaya" value="0"/>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="center">
                <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Tambah"/>
            </td>
        </tr>
    </table>
</form>

{literal}
    <script>
        $('#form_tambah_pembayaran').submit(function(){
            $( "#tambah_pembayaran" ).dialog('close');
        })
        $('#form_tambah_detail_pembayaran').submit(function(){
            $( "#tambah_detail_pembayaran" ).dialog('close');
        })
    </script>
{/literal}