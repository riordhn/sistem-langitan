{if !empty($data_detail_biaya)}
    <form id="form_tambah_pembayaran" action="history-bayar.php?cari={$smarty.get.cari}" method="post">
        <table class="ui-widget" style="width: 90%">
            <tr class="ui-widget-header">
                <th>Semester Pembayaran</th>
            </tr>
            {foreach $data_semester as $data}
                <tr class="ui-widget-content">
                    <td class="center">
                        <input type="checkbox" name="semester{$data@index+1}" value="{$data.ID_SEMESTER}"/>
                        <label for="semester{$data@index+1}" >{$data['NM_SEMESTER']}({$data['TAHUN_AJARAN']})</label>
                    </td>
                </tr>
                {$jumlah_data=$data@index+1}
            {/foreach}
            <tr class="ui-widget-content">
                <td colspan="2" class="center">
                    <input type="hidden" name="jumlah_data" value="{$jumlah_data}"/>
                    <input type="hidden" name="mode" value="tambah"/>
                    <input type="hidden" name="id_mhs" value="{$id_mhs}"/>                    
                    <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Tambah"/>
                </td>
            </tr>
        </table>
    </form>
{else}
    <p style="color: red;text-align: center;">Master Biaya Kuliah Mahasiswa belum ada Klik Di
        <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="biaya-mahasiswa.php?cari={$smarty.get.cari}" onclick="$('#tambah_pembayaran').dialog('close');">Sini</a>
        Untuk setting Master Kuliah Biaya Mahasiswa
    </p>
{/if}
{literal}
    <script>
        $('#form_tambah_pembayaran').submit(function(){
            $( "#tambah_pembayaran" ).dialog('close');
        })
        $('#form_tambah_detail_pembayaran').submit(function(){
            $( "#tambah_detail_pembayaran" ).dialog('close');
        })
    </script>
{/literal}