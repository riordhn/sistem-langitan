
{if count($data_pembayaran)>0}
{$jumlah_biaya=0}
{$total_pembayaran=0}
{foreach $data_pembayaran as $d}
    {if $d['TOTAL_TERBAYAR']>0}
    <div class="form-group col-md-4">
        <label for="besar_pengembalian{$jumlah_biaya+1}">Jumlah pengembalian : <b>{$d['NM_BIAYA']}</b></label>
        <input type="hidden" name="id_biaya{$jumlah_biaya+1}" value="{$d['ID_BIAYA']}"/>
        <input type="number" name="besar_pengembalian{$jumlah_biaya+1}" class="form-control" value="{$d['TOTAL_TERBAYAR']-$d['TOTAL_DIKEMBALIKAN']}" max="{$d['TOTAL_TERBAYAR']-$d['TOTAL_DIKEMBALIKAN']}">        
        <input type="hidden" name="besar_pembayaran{$jumlah_biaya+1}" class="form-control" value="{$d['TOTAL_TERBAYAR']}" />
    </div>
    {$total_pembayaran=$total_pembayaran+$d['TOTAL_TERBAYAR']}
    {$jumlah_biaya=$jumlah_biaya+1}
    {/if}
{/foreach}
<input type="hidden" name="id_tagihan_mhs" value="{$data_pembayaran[0]['ID_TAGIHAN_MHS']}"/>
<input type="hidden" name="jumlah_biaya" value="{$jumlah_biaya}"/>
<input type="hidden" name="total_pembayaran" value="{$total_pembayaran}"/>
{else}
<div class="alert alert-danger" role="alert">Data Pembayaran tidak ditemukan</div>
{/if}