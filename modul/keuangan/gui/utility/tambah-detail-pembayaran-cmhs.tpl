{if $data_biaya!=''}
    <form id="form_tambah_detail_pembayaran" action="update-pembayaran-cmhs.php?no_ujian={$data_cmhs.NO_UJIAN}" method="post">
        <table class="ui-widget" style="width: 60%">
            <tr class="ui-widget-header">
                <th>Nama Biaya</th>
                <th>Besar Biaya</th>
                <th>Status Pembayaran</th>
            </tr>
            <tr class="ui-widget-content">
                <td>
                    <input type="hidden" name="mode" value="tambah-biaya"/>
                    <input type="hidden" name="id_c_mhs" value="{$smarty.get.id_cmhs}"/>
                    <input type="hidden" name="id_semester" value="{$id_semester}"/>
                    <select name="biaya">
                        {foreach $data_biaya as $b}
                            <option value="{$b.ID_DETAIL_BIAYA}">{$b.NM_BIAYA}</option>
                        {/foreach}
                    </select>
                </td>
                <td>
                    <input type="text" name="besar_biaya" class="required" size="10" maxlength="10"/>
                </td>
                <td>
                    <select name="status">
                        {foreach $data_status as $ds}
                            <option value="{$ds.ID_STATUS_PEMBAYARAN}">{$ds.NAMA_STATUS}</option>
                        {/foreach}
                    </select>
                </td>

            </tr>
            <tr>
                <td colspan="3" class="center">
                    <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Tambah"/>
                </td>
            </tr>
        </table>
    </form>
{else}
    <p style="color: red;text-align: center">Master Biaya Kuliah Calon Mahasiswa belum
    </p>
{/if}
{literal}
    <script>
        $('#form_tambah_detail_pembayaran').submit(function(){
            $( "#dialog-tambah" ).dialog('close');
        });
        $('#form_tambah_detail_pembayaran').validate();
    </script>
{/literal}