<div class="center_title_bar">Generate Biaya Mahasiswa</div> 
<form method="get" id="report_form" action="update-pembayaran-perbulan-manual.php">
    <table class="ui-widget" style="width:90%;">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td width="15%">Fakultas</td>
            <td width="35%">
                <select name="fakultas" id="fakultas" class="select2" style="width:90%">
                    <option value="">-- Pilih Fakultas --</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
            <td  width="15%">Program Studi</td>
            <td width="35%">
                <select name="prodi" id="prodi" class="select2" style="width:90%">
                    <option value="">Semua</option>
                    {foreach $data_prodi as $data}
                        <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$smarty.get.prodi}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                </select>
            </td>

        </tr>
         <tr class="center ui-widget-content">
            <td colspan="6">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" name="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if isset($data_mahasiswa)}
    <form method="post" action="update-pembayaran-perbulan-manual.php?{$smarty.server.QUERY_STRING}">
        <table class="ui-widget-content" style="width: 90%">
            <tr class="ui-widget-header">
                <th colspan="11" class="header-coloumn">Daftar Mahasiswa</th>
            </tr>
            <tr class="ui-widget-header">
                <th>No</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Program Studi</th>
                <th>Semester</th>
                <th>No Kuitansi</th>
                <th>Tgl Bayar</th>
                <th>Besar Biaya</th>
                <th>SPP Perbulan</th>
                <th>Total Terbayar</th>
                <th>
                    Check All<br/>
                    <input type="checkbox" id="check_all"/>
                </th>
            </tr>
            {$total_data=0}
            {foreach $data_mahasiswa as $data}
                <tr>
                    <td>{$data@index+1}</td>
                    <td><a href="history-bayar.php?cari={$data.NIM_MHS}">{$data.NIM_MHS}</a></td>
                    <td>{$data.NM_PENGGUNA}</td>
                    <td>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</td>
                    <td>{$data.NM_SEMESTER} {$data.THN_AKADEMIK_SEMESTER}</td>
                    <td class="center">{$data.NO_KUITANSI}</td>
                    <td class="center">{$data.TGL_BAYAR}</td>
                    <td class="center">{number_format($data.BESAR_BIAYA)}</td>
                    <td class="center">{number_format($data.SPP_PERBULAN)}</td>
                    <td class="center">{number_format($data.PEMBAYARAN_SPP)}</td>
                    <td class="center">
                        <input type="hidden" name="nm_semester{$data@index+1}" value="{$data.NM_SEMESTER}"/>
                        <input type="hidden" name="total_terbayar{$data@index+1}" value="{$data.PEMBAYARAN_SPP}"/>
                        <input type="hidden" name="id_tagihan{$data@index+1}" value="{$data.ID_TAGIHAN}"/>
                        <input type="hidden" name="spp_perbulan{$data@index+1}" value="{$data.SPP_PERBULAN}"/>
                        {if $data.DIPROSES_SPP==0}                            
                            <input type="checkbox" class="generate_biaya" name="id_pembayaran{$data@index+1}" value="{$data.ID_PEMBAYARAN}" /><br/><br/>
                        {else}
                            {number_format($data.DIPROSES_SPP)}
                        {/if}
                    </td>
                </tr>
                {$total_data=$total_data+1}
            {foreachelse}
                <tr class="ui-widget-content">
                    <td colspan="11" class="center"><span style="color: red">Data Kosong</span></td>
                </tr>                
            {/foreach}
            <tr class="ui-widget-content">
                <td colspan="11" class="center">
                    <input type="submit" value="Insert Bayar SPP Bulanan" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;"/>
                    <input type="hidden" name="total_data" value="{$total_data}"/>
                    <input type="hidden" name="mode" value="generate"/>
                </td>
            </tr>
        </table>
    </form>
{/if}
{literal}
<script>
$(document).ready(function() {
    var statuses = {/literal}{json_encode($smarty.get.status)}{literal};
    var jenis_pembayarans = {/literal}{json_encode($smarty.get.jenis_pembayaran)}{literal};
    $('#fakultas').change(function(){
        $.ajax({
            type:'post',
            url:'getProdi.php',
            data:'id_fakultas='+$(this).val(),
            success:function(data){
                    $('#prodi').html(data);
            }                    
        })
    });
    $('#check_all').click(function(){
        if($(this).is(':checked')){
            $('.generate_biaya').attr('checked', true);
        }else{
            $('.generate_biaya').attr('checked', false);
        }
    });
    $('#select_all').change(function(){
        if($(this).val()=='Y'){
            $('.tagihkan').attr('value', 'Y');
        }else{
            $('.tagihkan').attr('value', 'T');
        }
    });
    $('.select2').select2();
});
</script>
{/literal}