<div class="center_title_bar">Konfigurasi Sistem</div>
<form id="form_ubah_master_tagihan" action="konfig-sistem.php" method="post" >
    <table class="ui-widget" style="width:70%">
        <tr class="ui-widget-header">
            <th>NO</th>
            <th>Nama Konfigurasi</th>
            <th>Deskripsi</th>
            <th>Nilai</th>
        </tr>
        {foreach $data_konfig as $d}
            <tr>
                <td>{$d@index+1}</td>
                <td>{$d['KD_CONFIG']}</td>
                <td>{$d['DESKRIPSI']}</td>
                <td style="width:30%" class="center">
                    <input type='hidden' name="konfig_{$d@index}" value="{$d['KD_CONFIG']}"/>
                    <input type='text' name="val_{$d@index}" value="{$d['CONFIG_VALUE']}" size="50"/>
                </td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="4" class="center total">
                <input type="hidden" name="jumlah_data" value="{count($data_konfig)}"/>
                <input type="hidden" name="mode" value="update"/>
                <input type="submit" value="SIMPAN" class="ui-button ui-corner-all ui-state-hover"/>
            </td>
        </tr>
    </table>
</form>