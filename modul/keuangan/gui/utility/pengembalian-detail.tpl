<table class="table table-striped bg-light" style="width: 90%">
    <thead class="thead-dark">
        <tr>
            <th class="center">NO</th>
            <th class="center">NAMA BIAYA</th>
            <th class="center">BESAR BIAYA</th>
        </tr>
    </thead>
    <tbody>
        {foreach $data_biaya as $biaya}
            <tr>
                <td>{$biaya@index+1}</td>
                <td>{$biaya.NM_BIAYA}</td>
                <td class="text-right">
                    {number_format($biaya.BESAR_BIAYA)}
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="3" class="data-kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="3" class="center">
                <input type="button" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tutup" onclick="$('#dialog-view').dialog('close')" />
            </td>
        </tr>    
    </tbody>
</table>