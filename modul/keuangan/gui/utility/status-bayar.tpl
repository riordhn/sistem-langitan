<div class="center_title_bar">Update Status Pembayaran Mahasiswa</div> 
<form method="get" id="form" action="status-bayar.php">
    <div id="alert" class="ui-widget" style="display:none;margin-left:15px;">
        <div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;"> 
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                Data Harus Diisi.</p>
        </div>
    </div>
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="3" class="header-coloumn">Input Form</th>
        </tr>
        <tr class="ui-widget-content">
            <td>NIM Mahasiswa</td>
            <td><input id="nim" type="text" {if isset($smarty.get.cari)}value="{$smarty.get.cari}"{/if} name="cari"/></td>
            <td>
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Cari"/>
                <span class="ui-button ui-corner-all ui-state-hover" onclick="window.open('upload-status-bayar.php','_blank')"  style="padding:5px;cursor:pointer;">Upload</span>
            </td>
        </tr>
    </table>
</form>
<p></p>
{if isset($smarty.get.cari)}
    <form action="status-bayar.php" method="post">
        <table class="ui-widget" style="width: 90%">
            <tr class="ui-widget-header">
                <th colspan=11"" class="header-coloumn">Riwayat Status Pembayaran Mahasiswa</th>
            </tr>
            <tr class="ui-widget-header">
                <th>No</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Semester</th>
                <th>Status Pembayaran</th>
                <th>Tanggal Mulai</th>
                <th>Tanggal Jatuh Tempo</th>
                <th>Keterangan</th>
                <th>Pengupdate</th>
                <th>Hapus</th>
                <th>Aktif</th>
            </tr>
            {foreach $data_sejarah_status as $data}
                <tr class="ui-widget-content">
                    <td>{$data@index+1}</td>
                    <td>{$data.NIM_MHS}</td>
                    <td>{$data.NM_MHS}</td>
                    <td>{$data.NM_SEMESTER} ({$data.TAHUN_AJARAN})</td>
                    <td>{$data.NAMA_STATUS}</td>
                    <td>{$data.TGL_MULAI_BAYAR}</td>
                    <td>{$data.TGL_JATUH_TEMPO}</td>
                    <td>{$data.KETERANGAN}</td>
                    <td>{$data.NM_PENGUPDATE}</td>
                    <td><input class="hapus" name="hapus" type="checkbox" value="{$data.ID_SEJARAH_STATUS_BAYAR}" /></td>
                    <td><input class="aktifkan" name="aktif" type="checkbox" value="{$data.ID_SEJARAH_STATUS_BAYAR}" {if $data.IS_AKTIF=='1'} checked="true" {/if}/></td>                    
                </tr>
            {foreachelse}
                <tr class="ui-widget-content">
                    <td colspan="11" class="center"><span style="color: red">Data Kosong</span></td>
                </tr>
            {/foreach}
        </table>
    </form>
    <p></p>
    <form method="post" id="form_status" action="status-bayar.php?cari={$smarty.get.cari}">
        <table class="ui-widget" style="width: 50%">
            <tr class="ui-widget-header">
                <th class="header-coloumn" colspan="2">Tambah Sejarah Status Pembayaran</th>
            </tr>
            <tr class="ui-widget-content">
                <td>Nim</td>
                <td>{$data_mhs.NIM_MHS}</td>
            </tr>
            <tr class="ui-widget-content">
                <td>Nama</td>
                <td>{$data_mhs.NM_PENGGUNA}</td>
            </tr>
            <tr class="ui-widget-content">
                <td>Program Studi</td>
                <td>({$data_mhs.NM_JENJANG}) {$data_mhs.NM_PROGRAM_STUDI} </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Status Mahasiswa</td>
                <td>{$data_mhs.NM_STATUS_PENGGUNA} </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Semester</td>
                <td>
                    <select name="semester">
                        {foreach $data_semester as $data}
                            <option value="{$data['ID_SEMESTER']}">{$data['NM_SEMESTER']}({$data['TAHUN_AJARAN']})</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Status</td>
                <td>
                    <select name="status">
                        {foreach $data_status as $data}
                            <option value="{$data.ID_STATUS_PEMBAYARAN}">{$data.NAMA_STATUS}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Tanggal Mulai</td>
                <td><input type="text" class="datepick" name="tgl_mulai" /></td>
            </tr>
            <tr class="ui-widget-content">
                <td>Tanggal Jatuh Tempo</td>
                <td><input type="text" class="datepick" name="tgl_tempo"/></td>
            </tr>
            <tr class="ui-widget-content">
                <td>Keterangan</td>
                <td><textarea name="keterangan" class="required"  rows="5" cols="40"></textarea></td>
            </tr>
            <tr class="ui-widget-content">
                <td colspan="2" class="center">
                    <input type="hidden" name="mode" value="tambah"/>
                    <input type="hidden" name="id_mhs" value="{$data_mhs.ID_MHS}" />
                    <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tambah" />
                </td>
            </tr>
        </table>
    </form>
    <span class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" onclick="history.back(-1)">Kembali</span>
{/if}
{literal}
    <script>
            $('#form_status').validate();
            function getUrlVars()
            {
                var vars = [], hash;
                var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                for(var i = 0; i < hashes.length; i++)
                {
                    hash = hashes[i].split('=');
                    vars.push(hash[0]);
                    vars[hash[0]] = hash[1];
                }
                return vars;
            }
            $(function() {
                $( ".datepick" ).datepicker({dateFormat:'dd-M-yy',changeMonth: true,
                            changeYear: true});
            });
            $('#form').submit(function() {
                    if($('#nim').val()==''){
                            $('#alert').fadeIn();
                    return false;
                }                
            });
            $('.aktifkan').click(function(){
                $.ajax({
                    type:'post',
                    url:'status-bayar.php?cari='+getUrlVars()["cari"],
                    data:'mode=aktifkan&id_sejarah_status='+$(this).val(),
                    success:function(data){
                        $('#center_content').html(data);
                    }    
                });
            });
            $('.hapus').click(function(){
                var c =confirm("Apakah anda yakin menghapus data ini?");
                if(c==true){    
                $.ajax({
                    type:'post',
                    url:'status-bayar.php?cari='+getUrlVars()["cari"],
                    data:'mode=hapus&id_sejarah_status='+$(this).val(),
                    success:function(data){
                        $('#center_content').html(data);
                    }    
                });}
            });
    </script>
{/literal}