<div class="center_title_bar">Pengembalian Pembayaran</div> 
<form method="get" id="form" action="pengembalian-pembayaran.php">
    <div id="alert" class="ui-widget" style="display:none;margin-left:15px;">
        <div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;"> 
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                Data Harus Diisi.</p>
        </div>
    </div>
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="3" class="header-coloumn">Input Form</th>
        </tr>
        <tr class="ui-widget-content">
            <td>NIM Mahasiswa</td>
            <td><input id="nim" type="text" value="{$smarty.get.cari}" name="cari"/></td>
            <td>
                <input class="btn btn-warning" style="cursor:pointer;" type="submit" value="Cari"/>
                {* <span class="ui-widget ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;margin-top:5px;"  onclick="window.open('pengembalian-pembayaran-input.php' , '_blank')">Input</span> *}
                           
            </td>
        </tr>
    </table>
</form>
{if isset($smarty.get.cari)}
    <table style="width: 400px;" class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="text-align: center">Detail Biodata Mahasiswa</th>
        </tr>
        <tr class="ui-widget-content">
            <td>NIM</td>
            <td width="200px">{$data_detail_mahasiswa.NIM_MHS}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>No Ujian</td>
            <td>{$data_detail_mahasiswa.NO_UJIAN}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Nama</td>
            <td>{$data_detail_mahasiswa.NM_PENGGUNA}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Fakultas</td>
            <td>{$data_detail_mahasiswa.NM_FAKULTAS}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Program Studi</td>
            <td>{$data_detail_mahasiswa.NM_PROGRAM_STUDI}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Jenjang</td>
            <td>{$data_detail_mahasiswa.NM_JENJANG}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Jalur</td>
            <td>{$data_detail_mahasiswa.NM_JALUR}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Angkatan Akademik Mahasiswa</td>
            <td>{$data_detail_mahasiswa.THN_ANGKATAN_MHS}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Kelompok Biaya</td>
            <td>{$data_detail_mahasiswa.NM_KELOMPOK_BIAYA}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Status Cekal</td>
            <td>{$data_detail_mahasiswa.STATUS_CEKAL}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Status Akademik</td>
            <td>{$data_detail_mahasiswa.NM_STATUS_PENGGUNA}</td>
        </tr>
    </table>
    <table class="ui-widget-content ui-widget" style="width: 98%">
        <tr class="ui-widget-header">
            <th colspan="11"class="header-coloumn">HISTORY PENGEMBALIAN MAHASISWA</th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>Semester Pembayaran</th>
            <th>Besar Pembayaran</th>
            <th>Besar Pengembalian</th>
            <th>Tgl Pengembalian</th>
            <th>Bank</th>
            <th>Via Bank</th>
            <th>No Transaksi</th>
            <th>Keterangan</th>
            <th>Operasi</th>
        </tr>
        {$index=1}
        {foreach $data_history_pengembalian as $data}
            <tr>
                <td>{$index}</td>
                <td>{$data.NM_SEMESTER_BAYAR} ({$data.TAHUN_AJARAN_BAYAR})</td>
                <td>{number_format($data.BESAR_PEMBAYARAN)}</td>
                <td>{number_format($data.TOTAL_PENGEMBALIAN)}</td>
                <td>{$data.TGL_PENGEMBALIAN}</td>
                <td>{$data.NM_BANK}</td>
                <td>{$data.NAMA_BANK_VIA}</td>
                <td>{$data.NO_TRANSAKSI}</td>
                <td>{$data.KETERANGAN}</td>
                <td class="center">
                    <form action="pengembalian-pembayaran.php?cari={$smarty.get.cari}" id="form_edit" method="post">
                        <input type="hidden" name="id_pengembalian" value="{$data['ID_PENGEMBALIAN_PEMBAYARAN']}"/>                   
                        <input type="hidden" name="mode" value="hapus"/>
                        <input type="submit" class="btn btn-danger btn-sm" value="Hapus"/>   
                    </form>
                     <button class="btn btn-sm btn-info" onclick="ajax_get_page_dialog('pengembalian-detail.php?mode=view&cari={$smarty.get.cari}&pengembalian={$data.ID_PENGEMBALIAN_PEMBAYARAN}','#dialog-view')">Lihat Detail</button>
                </td>
            </tr>
        {foreachelse}
            <tr class="ui-widget-content">
                <td colspan="11" class="center"><span style="color: red">Data Kosong</span></td>
            </tr>
        {/foreach}
    </table>        
    <div class="row m-3 ">
        <h5>Form Tambah Pengembalian</h5>
        <div class="col-sm-12 p-3 border border-warning bg-light">
            <form action="pengembalian-pembayaran.php?cari={$smarty.get.cari}" id="form_tambah" method="post">                            
                <div class="form-row">                    
                    <div class="form-group col-sm-3">
                    <label for="semester">Semester</label>
                    <select id="pilih_semester" name="semester" class="form-control" required="true">
                        <option value="">--- Pilih ----</option>
                        {foreach $data_semester as $data}
                            <option value="{$data['ID_SEMESTER']}">{$data['NM_SEMESTER']}({$data['TAHUN_AJARAN']})</option>
                        {/foreach}
                    </select>
                    </div>
                    <div class="form-group col-sm-3">
                        <label for="tgl_pengembalian">Tgl Pengembalian</label>
                        <input name="tgl_pengembalian"  class="form-control date_pick" required="true" value=""/>
                    </div>  
                    <div class="form-group col-sm-3">
                    <label for="bank">Sumber Pengembalian</label>
                    <select name="bank" class="form-control" required="true">
                        <option value="">--- Pilih ----</option>
                        {foreach $data_bank as $data}
                            <option {if $data['ID_BANK']==6} selected {/if} value="{$data['ID_BANK']}">{$data['NM_BANK']}</option>
                        {/foreach}
                    </select>
                    </div>
                    <div class="form-group col-sm-3">
                    <label for="bank_via">Metode </label>
                    <select name="bank_via" class="form-control" required="true">
                        <option value="">--- Pilih ----</option>
                        {foreach $data_bank_via as $data}
                            <option {if $data['ID_BANK_VIA']==9} selected {/if} value="{$data['ID_BANK_VIA']}" >{$data['NAMA_BANK_VIA']}</option>
                        {/foreach}
                    </select>
                    </div>
                </div>
                <div id="pengembalian-biaya" class="form-group">
                    
                </div> 
                <div class="form-group">
                    <label for="keterangan">Keterangan</label>
                    <input name="keterangan" class="form-control" required="true" value=""/>
                </div>
                <input type="hidden" name="mode" value="tambah"/>
                <input type="hidden" id="id_mhs" name="id_mhs" value="{$data_detail_mahasiswa.ID_MHS}"/> 
                <button type="submit" id="btn-save-pengembalian" class="btn btn-success">Simpan</button>
            </form>
        </div>
    </div>
{/if}
<div id="dialog-add" title="Tambah Detail Pengembalian Pembayaran"></div>
<div id="dialog-edit" title="Edit Detail Pengembalian Pembayaran"></div>
<div id="dialog-view" title="Detail Pengembalian Pembayaran"></div>

{literal}
    <script>
        var base_url = window.location.protocol+'//'+window.location.hostname;
        var htmlLoading ='<div style="width: 100%;height:auto;padding:15px" align="center"><img src="'+base_url+'/img/spinner.gif" /></div>';
        $('#form').submit(function() {
            if($('#nim').val()==''){
                    $('#alert').fadeIn();
            return false;
            }  
        });
        $( ".date_pick" ).datepicker({dateFormat:'dd-M-yy',changeMonth: true, changeYear: true});
        
        $('#form_tambah').submit(function(e){
            e.preventDefault();
            if($('#form_tambah').valid()){
                $('#form_tambah').submit();
            }
        });
        $('#pilih_semester').change(function(){
            $.ajax({
                type:'post',
                url:'pengembalian-pembayaran-ajax-info.php',
                data:'id_semester='+$(this).val()+'&id_mhs='+$('#id_mhs').val(),
                beforeSend:function(){
                    $('#pengembalian-biaya').html('<div class="spinner-border spinner-border-sm" role="status"><span class="sr-only">Loading...</span></div>');
                },
                success:function(data){
                    $('#pengembalian-biaya').html(data);
                }                    
            })
        });
        $('#dialog-add,#dialog-edit,#dialog-view').dialog({
            width:'40%',
            modal: true,
            resizable:false,
            autoOpen:false
        });
        function ajax_get_page_dialog(url,element){
            $.ajax({
                url: url,
                type: "get",
                beforeSend: function() {          
                $(element).dialog('open')
                $(element).html(
                    htmlLoading
                );
                },
                success: function(data) {
                $(element).html(data);
                }
            });
        }
    </script>
{/literal}