<div class="center_title_bar">Cek Batas Pembayaran Mahasiswa/ Calon Mahasiswa (Di luar Periode Bayar)</div> 
<form method="get" id="form" action="cek-batas-bayar.php">
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="3" class="header-coloumn">Input Form</th>
        </tr>
        <tr class="ui-widget-content">
            <td>NIM / NO Ujian</td>
            <td><input id="nim" type="text" {if isset($smarty.get.cari)}value="{$smarty.get.cari}"{/if} name="cari"/></td>
            <td>
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Cari"/>
            </td>
        </tr>
    </table>
</form>
{if $data_mhs!=''}
    <form action="cek-batas-bayar.php?cari={$smarty.get.cari}" method="post">
        <table class="ui-widget-content" style="width: 30%">
            <tr class="ui-widget-header">
                <th colspan="2">DATA MAHASISWA</th>
            </tr>
            <tr>
                <td>NIM</td>
                <td>{$data_mhs.NIM_MHS}</td>
            </tr>
            <tr>
                <td>NAMA</td>
                <td>{$data_mhs.NM_PENGGUNA}</td>
            </tr>
            <tr>
                <td>PRODI</td>
                <td>{$data_mhs.NM_JENJANG} {$data_mhs.NM_PROGRAM_STUDI}</td>
            </tr>
            <tr>
                <td>FAKULTAS</td>
                <td>{$data_mhs.NM_FAKULTAS}</td>
            </tr>
            <tr>
                <td>BATAS BAYAR</td>
                <td>
                    <input type="text" class="date_pick" value="{$data_mhs.BATAS_BAYAR}" name="batas_bayar">
                </td>
            </tr>
            <tr>
                <td colspan="2" class="center">
                    <input type="hidden" name="id_mhs" value="{$data_mhs.ID_MHS}"/>
                    <input type="hidden" name="mode" value="mhs"/>
                    <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Update"/>
                </td>
            </tr>
        </table>
    </form>
{else if $data_cmhs!=''}
    <form action="cek-batas-bayar.php?cari={$smarty.get.cari}" method="post">
        <table class="ui-widget-content" style="width: 30%">
            <tr class="ui-widget-header">
                <th colspan="2">DATA CALON MAHASISWA</th>
            </tr>
            <tr>
                <td>NIM</td>
                <td>{$data_cmhs.NO_UJIAN}</td>
            </tr>
            <tr>
                <td>NAMA</td>
                <td>{$data_cmhs.NM_C_MHS}</td>
            </tr>
            <tr>
                <td>PRODI</td>
                <td>{$data_cmhs.NM_JENJANG} {$data_cmhs.NM_PROGRAM_STUDI}</td>
            </tr>
            <tr>
                <td>FAKULTAS</td>
                <td>{$data_cmhs.NM_FAKULTAS}</td>
            </tr>
            <tr>
                <td>BATAS BAYAR</td>
                <td>
                    <input type="text" class="date_pick" value="{$data_cmhs.BATAS_BAYAR}" name="batas_bayar">
                </td>
            </tr>
            <tr>
                <td colspan="2" class="center">
                    <input type="hidden" name="id_c_mhs" value="{$data_cmhs.ID_C_MHS}"/>
                    <input type="hidden" name="mode" value="cmhs"/>
                    <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Update"/>
                </td>
            </tr>
        </table>
    </form>
{else}
    {$error}
{/if}
{literal}
    <script>
            $(function() {
                    $( ".date_pick" ).datepicker({dateFormat:'dd-M-y',changeMonth: true,
                            changeYear: true});
            });
    </script>
{/literal}