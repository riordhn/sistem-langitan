<div class="center_title_bar">Edit Pembayaran Wisuda (Manual Posting)</div>  
<form method="get" action="edit-biaya-wisuda.php">
    <table class="ui-widget-content">
        <tr class="ui-widget-header">
            <th colspan="5" class="header-coloumn">Input Form</th>
        </tr>
        <tr>
            <td>Periode Wisuda</td>
            <td>
                <select name="periode">
                    {foreach $periode as $data}
                        <option value="{$data.ID_TARIF_WISUDA}" {if $id_periode == $data.ID_TARIF_WISUDA} selected="selected"{/if}>{$data.NM_TARIF_WISUDA}</option>
                    {/foreach}
                </select>
            </td>
            <td>NIM</td>
            <td><input type="text" name="nim" /></td>
            <td><input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Cari" /></td>
        </tr>
    </table>
</form>
{if isset($status_form)}
    {$status_form}
{/if}
{if isset($tagihan)}
    <form action="edit-biaya-wisuda.php" method="post">
        <table class="ui-widget-content" style="width: 30%">
            <tr class="ui-widget-header">
                <th colspan="2" class="header-coloumn">Detail Biaya Wisuda</th>
            </tr>
            <tr class="ui-widget-content">
                <td>NIM</td>
                <td width="200px">{$data_detail_mahasiswa.NIM_MHS}</td>
            </tr>
            <tr class="ui-widget-content">
                <td>Nama</td>
                <td>{$data_detail_mahasiswa.NM_PENGGUNA}</td>
            </tr>
            <tr class="ui-widget-content">
                <td>Fakultas</td>
                <td>{$data_detail_mahasiswa.NM_FAKULTAS}</td>
            </tr>
            <tr class="ui-widget-content">
                <td>Program Studi</td>
                <td>{$data_detail_mahasiswa.NM_PROGRAM_STUDI}</td>
            </tr>
            <tr class="ui-widget-content">
                <td>Jenjang</td>
                <td>{$data_detail_mahasiswa.NM_JENJANG}</td>
            </tr>
            <tr>
                <td>Tarif</td>
                <td>{number_format($tarif)}</td>
            </tr>
            <tr>
                <td>Cicilan</td>
                <td>{number_format($cicilan)}</td>
            </tr>
            <tr>
                <td>Tagihan</td>
                <td><input type="text" name="tagihan" value="{number_format($tagihan)}"  /></td>
            </tr>
            <tr>
                <td>Bank</td>
                <td>
                    <select name="bank">
                        <option value="">Pilih</option>
                        {foreach $bank as $data}
                            <option value="{$data.ID_BANK}">{$data.NM_BANK}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td>Bank Via</td>
                <td>
                    <select name="bank_via">
                        <option value="">Pilih</option>
                        {foreach $bank_via as $data}
                            <option value="{$data.ID_BANK_VIA}">{$data.NAMA_BANK_VIA}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td>Tanggal Bayar</td>
                <td><input type="text" name="tgl_bayar" id="tgl_bayar" value="" class="date_pick" /></td>
            </tr>

            <tr>
                <td>No Transaksi</td>
                <td><input type="text" name="no_transaksi" /></td>
            </tr>
            <tr>
                <td>Keterangan</td>
                <td><textarea name="keterangan"></textarea></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center">
                    <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Simpan" />
                    <input type="hidden" name="id_mhs" value="{$id_mhs}" />
                    <input type="hidden" name="id_periode" value="{$id_periode}" />
                </td>
            </tr>
        </table>
    </form>
{/if}
{literal}
    <script>
            $(function() {
                    $( ".date_pick" ).datepicker({dateFormat:'dd-M-y',changeMonth: true,
                            changeYear: true});
            });
    </script>
{/literal}