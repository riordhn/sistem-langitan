<form id="form-delete" method="post" action="edit-kerjasama.php?cari={$smarty.get.cari}">
    <p class="center">
        Apakah anda yakin menghapus data ini?<br/>
        <input type="submit" value="Ya" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;"/>
        <span onclick="$('#dialog-delete').dialog('close')" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;">Tidak</span>
    </p>
    <input type="hidden" name="id_sejarah_beasiswa" value="{$smarty.get.id_sejarah_beasiswa}"/>
    <input type="hidden" name="mode" value="delete"/>
</form>
{literal}
    <script>
            $('#form-delete').submit(function() {
                $('#dialog-delete').dialog('close')
            });
    </script>
{/literal}