<div class="center_title_bar">Edit Kerjasama</div> 
<form method="get" id="form" action="edit-kerjasama.php">
    <div id="alert" class="ui-widget" style="display:none;margin-left:15px;">
        <div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;"> 
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                Data Harus Diisi.</p>
        </div>
    </div>
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="3" class="header-coloumn">Input Form</th>
        </tr>
        <tr class="ui-widget-content">
            <td>NIM Mahasiswa</td>
            <td><input id="nim" type="text" value="{$smarty.get.cari}" name="cari"/></td>
            <td><input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Cari"/></td>
        </tr>
    </table>
</form>
{if isset($smarty.get.cari)}
    <table style="width: 400px;" class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="text-align: center">Detail Biodata Mahasiswa</th>
        </tr>
        <tr class="ui-widget-content">
            <td>NIM</td>
            <td width="200px">{$data_detail_mahasiswa.NIM_MHS}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>No Ujian</td>
            <td>{$data_detail_mahasiswa.NO_UJIAN}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Nama</td>
            <td>{$data_detail_mahasiswa.NM_PENGGUNA}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Fakultas</td>
            <td>{$data_detail_mahasiswa.NM_FAKULTAS}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Program Studi</td>
            <td>{$data_detail_mahasiswa.NM_PROGRAM_STUDI}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Jenjang</td>
            <td>{$data_detail_mahasiswa.NM_JENJANG}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Jalur</td>
            <td>{$data_detail_mahasiswa.NM_JALUR}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Angkatan Akademik Mahasiswa</td>
            <td>{$data_detail_mahasiswa.THN_ANGKATAN_MHS}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Kelompok Biaya</td>
            <td>{$data_detail_mahasiswa.NM_KELOMPOK_BIAYA}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Status Cekal</td>
            <td>{$data_detail_mahasiswa.STATUS_CEKAL}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Status Akademik</td>
            <td>{$data_detail_mahasiswa.NM_STATUS_PENGGUNA}</td>
        </tr>
    </table>
    <form action="edit-kerjasama.php?cari={$smarty.get.cari}" id="form_edit" method="post">
        <table class="ui-widget-content ui-widget" style="width: 90%">
            <tr class="ui-widget-header">
                <th colspan="7"class="header-coloumn">SEJARAH KERJASAMA MAHASISWA</th>
            </tr>
            <tr class="ui-widget-header">
                <th>No</th>
                <th>Jenis Beasiswa</th>
                <th>Semester Mulai</th>
                <th>Semester Selesai</th>
                <th>Keterangan</th>
                <th>Beasiswa AKtif</th>
                <th>Operasi</th>
            </tr>
            {$index=1}
            {foreach $data_sejarah_beasiswa as $data}
                {if $smarty.get.mode=='edit'&&$data.ID_SEJARAH_BEASISWA==$smarty.get.id_sejarah_beasiswa}
                    <tr>
                        <td>{$index}</td>
                        <td>
                            <select name="beasiswa">
                                {foreach $data_kerjasama as $beasiswa}
                                    <option value="{$beasiswa['ID_BEASISWA']}" {if $beasiswa['ID_BEASISWA']==$data.ID_BEASISWA}selected="true"{/if}>{$beasiswa['NM_BEASISWA']} {$beasiswa['PENYELENGGARA_BEASISWA']} {$beasiswa['NM_GROUP_BEASISWA']}</option>
                                {/foreach}
                            </select>
                        </td>
                        <td>
                            <select name="semester_mulai">
                                <option value="">kosongi</option>
                                {foreach $data_semester as $semester}
                                    <option value="{$semester['ID_SEMESTER']}" {if $semester.ID_SEMESTER==$data.ID_SEMESTER_MULAI} selected="true" {/if}>{$semester['NM_SEMESTER']}({$semester['TAHUN_AJARAN']})</option>
                                {/foreach}
                            </select>
                        </td>
                        <td>
                            <select name="semester_selesai">
                                <option value="">kosongi</option>
                                {foreach $data_semester as $semester}
                                    <option value="{$semester['ID_SEMESTER']}" {if $semester.ID_SEMESTER==$data.ID_SEMESTER_SELESAI} selected="true" {/if}>{$semester['NM_SEMESTER']}({$semester['TAHUN_AJARAN']})</option>
                                {/foreach}
                            </select>
                        </td>
                        <td>
                            <textarea name="keterangan" class="required" cols="10" rows="5">{$data.KETERANGAN} </textarea>
                        </td>
                        <td>
                            <select name="aktif">
                                <option value="1" {if $data.BEASISWA_AKTIF==1}selected="true"{/if}>Aktif</option>
                                <option value="0" {if $data.BEASISWA_AKTIF==0}selected="true"{/if}>Tidak Aktif</option>
                            </select>
                        </td>
                        <td class="center">
                            <input type="hidden" id="id_mhs" name="id_mhs" value="{$data_detail_mahasiswa.ID_MHS}"/>
                            <input type="hidden" name="id_sejarah_beasiswa" value="{$smarty.get.id_sejarah_beasiswa}"/>
                            <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" value="Update"/>
                            <a href="edit-kerjasama.php?cari={$smarty.get.cari}"class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;">Batal</a>
                            <input type="hidden" name="mode" value="update"/>
                        </td>
                    </tr>
                {else}
                    <tr>
                        <td>{$index}</td>
                        <td class="center">{$data.NM_BEASISWA} Dari {$data.PENYELENGGARA_BEASISWA}</td>
                        <td class="center">{$data.SEMESTER_MULAI}</td>
                        <td class="center">{$data.SEMESTER_SELESAI}</td>
                        <td class="center">{$data.KETERANGAN}</td>
                        <td class="center">{if $data.BEASISWA_AKTIF==1} Aktif{else}Tidak Aktif{/if}</td>
                        <td class="center">
                            <a href="edit-kerjasama.php?{$smarty.server.QUERY_STRING}&mode=edit&id_sejarah_beasiswa={$data.ID_SEJARAH_BEASISWA}"class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;">Edit</a>
                            <span class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" onclick="$('#dialog-delete').dialog('open').load('delete-kerjasama.php?id_sejarah_beasiswa={$data.ID_SEJARAH_BEASISWA}&cari={$smarty.get.cari}')">Delete</span>
                        </td>
                    </tr>
                {/if}
                {$index=$index+1}
            {foreachelse}
                <tr class="ui-widget-content">
                    <td colspan="11" class="center"><span style="color: red">Data Kosong</span></td>
                </tr>
            {/foreach}
        </table>
    </form>
    <form action="edit-kerjasama.php?{$smarty.server.QUERY_STRING}" id="form_tambah" method="post">
        <table class="ui-widget" style="width: 90%">
            <tr class="ui-widget-header">
                <th colspan="5" class="header-coloumn">Tambah Beasiswa Mahasiswa</th>
            </tr>
            <tr class="ui-widget-header">
                <th>Jenis Beasiswa</th>
                <th>Semester Mulai</th>
                <th>Semester Selesai</th>
                <th>Keterangan</th>
                <th>Operasi</th>
            </tr>
            <tr  class="ui-widget-content" style="vertical-align: middle;">
                <td>
                    <select name="beasiswa">
                        {foreach $data_kerjasama as $beasiswa}
                            <option value="{$beasiswa['ID_BEASISWA']}" {if $beasiswa['ID_BEASISWA']==$data.ID_BEASISWA}selected="true"{/if}>{$beasiswa['NM_BEASISWA']} {$beasiswa['PENYELENGGARA_BEASISWA']} {$beasiswa['NM_GROUP_BEASISWA']}</option>
                        {/foreach}
                    </select>
                </td>
                <td>
                    <select name="semester_mulai">
                        <option value="">kosongi</option>
                        {foreach $data_semester as $data}
                            <option value="{$data['ID_SEMESTER']}">{$data['NM_SEMESTER']}({$data['TAHUN_AJARAN']})</option>
                        {/foreach}
                    </select>
                </td>
                <td>
                    <select name="semester_selesai">
                        <option value="">kosongi</option>
                        {foreach $data_semester as $data}
                            <option value="{$data['ID_SEMESTER']}">{$data['NM_SEMESTER']}({$data['TAHUN_AJARAN']})</option>
                        {/foreach}
                    </select>
                </td>
                <td>
                    <textarea name="keterangan" class="required" ></textarea>
                </td>
                <td>
                    <input type="hidden" id="id_mhs" name="id_mhs" value="{$data_detail_mahasiswa.ID_MHS}"/>
                    <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" value="Tambah"/>
                    <input type="hidden" name="mode" value="tambah"/>
                </td>
            </tr>
        </table>
    </form>
    <span class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" onclick="history.back(-1)">Kembali</span>
{/if}
<div id="dialog-delete" title="Delete Sejarah Kerjasama"></div>
{literal}
    <script>
            $('#form').submit(function() {
                    if($('#nim').val()==''){
                            $('#alert').fadeIn();
                    return false;
                    }  
            });
            $('#dialog-delete').dialog({
                width:'40%',
                modal: true,
                resizable:false,
                autoOpen:false
            });
            $(function() {
                    $( ".date_pick" ).datepicker({dateFormat:'dd-M-y',changeMonth: true,
                            changeYear: true});
            });
            $('#form_tambah,#form_edit').validate();
    </script>
{/literal}