{literal}
    <style type="text/css">
        .center { text-align: center; }
        .right { text-align: right; }
        .row2 { background-color: #eee; }
        .prodi { font-size: 10px; }
    </style>
{/literal}
<div class="center_title_bar">Generate Biaya Mahasiswa Baru</div>

<form action="generate-biaya-maba-pasca.php" method="get" id="filter">
    <table class="ui-widget-content">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn">
                Parameter
            </th>
        </tr>
        <tr>
            <td>Penerimaan</td>
            <td>
                <select name="id_penerimaan" onchange="$('#filter').submit()">
                    <option value="">Pilih Penerimaan</option>
                    {foreach $penerimaan_set as $p}
                        <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                            {foreach $p.p_set as $p2}
                                <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
            </td>
        </tr>
    </table>
</form>
{if isset($cmb_set)}
    <form method="post" action="generate-biaya-maba-pasca.php?{$smarty.server.QUERY_STRING}">
        <table class="ui-widget-content" style="width: 98%">
            <thead>
                <tr class="ui-widget-header">
                    <th colspan="8">
                        UBAH KELOMPOK BIAYA SEMUA<br/>
                        {if count($kb_cmb)>0}
                            <select name="kelompok_biaya_all">
                                <option value="">Pilih Kelompok Biaya</option>
                                {foreach $kb_cmb as $d}
                                    <option value="{$d.ID_KELOMPOK_BIAYA}">{$d.NM_KELOMPOK_BIAYA}</option>
                                {/foreach}
                            </select>

                        {else}
                            <span>Data Kelompok Biaya tidak ditemukan</span>
                        {/if}
                        <br/>
                        <br/>
                        <input type="submit" value="Update Kelompok Biaya / Generate Biaya" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;"/>
                    </th>
                </tr>
                <tr class="ui-widget-header">
                    <th>No</th>
                    <th>No Ujian</th>
                    <th>Nama</th>
                    <th>Kelompok Biaya</th>
                    <th>Varian</th>
                    <th>Status Tagihan</th>
                    <th class="center">
                        Generate<br/>
                        <input type="checkbox" id="check_all_generate"/>
                    </th>
                    <th class="center">
                        Kelompok<br/>
                        <input type="checkbox" id="check_all_kelompok"/>
                    </th>
                </tr>
            </thead>
            <tbody>
                {foreach $cmb_set as $cmb}
                    <tr {if $cmb@index is div by 2}class="ui-widget-content row2"{/if}{if $cmb.NM_KELOMPOK_BIAYA==''}style="background: #ffcccc"{/if}>
                        <td class="center">{$cmb@index+1}</td>
                        <td>No Ujian : {$cmb.NO_UJIAN}<br/> Voucher : {$cmb.KODE_VOUCHER}</td>
                        <td>{$cmb.NM_C_MHS}<br/>
                            <span class="prodi">
                                {$cmb.NM_JENJANG} {$cmb.NM_PROGRAM_STUDI} {$cmb.KELAS_PILIHAN}
                                {if $cmb.NM_PRODI_MINAT != ''}
                                    <br/>{$cmb.NM_PRODI_MINAT}
                                {/if}
                            </span>
                            <span class="prodi">
                                <br/>PT S1:{$cmb.PTN_S1}<br/>PT S2:{$cmb.PTN_S2}
                            </span>
                        </td>
                        <td class="center">
                            {if $cmb.NM_KELOMPOK_BIAYA!=''}
                                <b>   Kelompok Biaya Maba : {$cmb.NM_KELOMPOK_BIAYA} ({if $cmb.KHUSUS_KELOMPOK_BIAYA==1}Khusus{else}Reguler{/if})</b>
                            {else}
                                <b> Kelompok Biaya Maba : Tidak Ada</b>
                            {/if}
                            <br/>
                            {if $cmb.SUDAH_BAYAR == 0}
                                <br/>
                                <span class="ui-button ui-corner-all ui-state-hover" onclick="$('#dialog-kelompok').dialog('open').load('set-kelompok-biaya-cmhs.php?id_c_mhs={$cmb.ID_C_MHS}&semester={$cmb.ID_SEMESTER}')" style="padding:5px;cursor:pointer;">Set Kelompok Biaya</span>
                                <br/>
                            {/if}
                            <br/>
                            <input type="hidden" name="biaya_kuliah{$cmb@index+1}" value="{$cmb.ID_BIAYA_KULIAH}"/>
                            <span class="prodi" {if $cmb.SP3_CMHS > $cmb.VARIAN_SP3&&$cmb.VARIAN_SP3!=0}style="background-color: yellow;font-weight: bold"{/if}>
                                SP3 Maba : {number_format($cmb.SP3_CMHS,0)}<br/>
                                {if $cmb.VARIAN_SP3!=0}
                                    SP3 Lebih : {number_format($cmb.SP3_CMHS-$cmb.VARIAN_SP3)}<br/>
                                    {if $cmb.SP3_CMHS > $cmb.VARIAN_SP3}
                                        Status SP3 : Berlebih
                                    {else}
                                        Status SP3 : Sesuai dengan Master
                                    {/if}
                                {/if}
                                <input type="hidden" name="sp3_cmhs{$cmb@index+1}" value="{$cmb.SP3_CMHS}"/>
                            </span>
                        </td>
                        <td class="center">
                            Total Master Biaya : {number_format($cmb.VARIAN, 0, ",", ".")}<br/>
                            <span class="prodi">
                                SP3 Master :{number_format($cmb.VARIAN_SP3,0)}
                            </span>
                        </td>
                        <td class="center">
                            {if $cmb.PEMBAYARAN_CMHS > 0}
                                {if $cmb.SUDAH_BAYAR > 0}
                                    SUDAH BAYAR<br/>
                                    <br/>Pembayaran : {number_format($cmb.JUMLAH_SUDAH_BAYAR, 0, ",", ".")}
                                {else}
                                    <span style="color: #f00;">BELUM BAYAR</span><br/>
                                    Tagihan : {number_format($cmb.JUMLAH_TAGIHAN, 0, ",", ".")}<br/>
                                    {if $cmb.ID_JENJANG==1}
                                        <a class="disable-ajax ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" target="_blank" href="cetak-verifikasi-keuangan.php?id_c_mhs={$cmb.ID_C_MHS}">Cetak Invoice</a>
                                    {/if}
                                {/if}
                            {else}
                                Pembayaran Masih Kosong/ Belum Generate
                            {/if}
                        <td class="center">
                            {if $cmb.PEMBAYARAN_CMHS == 0}
                                {if $cmb.ID_KELOMPOK_BIAYA!=''}
                                    {if $cmb.ID_JENJANG==1&&$cmb.TGL_REGMABA==''}
                                        <span class="data-kosong">Belum Regmaba</span>
                                    {else}
                                        <input type="checkbox" class="generate_biaya" name="id_c_mhs{$cmb@index+1}" value="{$cmb.ID_C_MHS}"/>
                                    {/if}
                                {else}
                                    <span class="data-kosong">Kelompok Biaya Kosong</span>
                                {/if}
                            {else}
                                {if $cmb.SUDAH_BAYAR>0} 
                                    Sudah Terbayar
                                {else}
                                    Generate Ulang<br/>
                                    <input type="checkbox" class="generate_biaya"  name="id_c_mhs{$cmb@index+1}" value="{$cmb.ID_C_MHS}"/>
                                {/if}
                            {/if}
                        </td>
                        <td class="center">
                            <input type="checkbox" class="kelompok_biaya"  name="id_c_mhs_kelompok{$cmb@index+1}" value="{$cmb.ID_C_MHS}"/>
                        </td>
                    </tr>
                {foreachelse}
                    <tr class="ui-widget-content">
                        <td colspan="8" class="center"><span style="color: red">Data Kosong</span></td>
                    </tr>  
                {/foreach}
            </tbody>
            <tfoot>
                {if count($cmb_set)>0}
                    <tr>
                        <td colspan="8" class="center">
                            <input type="hidden" name="total_data" value="{count($cmb_set)}"/>
                            <input type="hidden" name="mode" value="generate"/>
                        </td>
                    </tr>
                {/if}
            </tfoot>
        </table>
    </form>
    <div id="dialog-kelompok" title="Set Kelompok Biaya"></div>
{/if}
{literal}
    <script>
                    $('#dialog-kelompok').dialog({
                        width: '30%',
                        modal: true,
                        resizable: false,
                        autoOpen: false
                    });
                    $('#check_all_generate').click(function() {
                        if ($(this).is(':checked')) {
                            $('.generate_biaya').attr('checked', true);
                        } else {
                            $('.generate_biaya').attr('checked', false);
                        }
                    });
                    $('#check_all_kelompok').click(function() {
                        if ($(this).is(':checked')) {
                            $('.kelompok_biaya').attr('checked', true);
                        } else {
                            $('.kelompok_biaya').attr('checked', false);
                        }
                    });
    </script>
{/literal}