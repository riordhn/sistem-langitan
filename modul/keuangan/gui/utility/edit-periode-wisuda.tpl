<div class="center_title_bar">Edit Periode Pembayaran Wisuda</div>  
<form method="get" action="edit-periode-wisuda.php">
    <table class="ui-widget-content">
        <tr class="ui-widget-header">
            <th colspan="3" class="header-coloumn">Input Form</th>
        </tr>
        <tr>
            <td>NIM</td>
            <td><input type="text" name="nim" value="{$smarty.get.nim}" /></td>
            <td><input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Cari" /></td>
        </tr>
    </table>
</form>
{if $error==''&&$data!=''}
    <form action="edit-periode-wisuda.php?{$smarty.server.QUERY_STRING}" method="post">
        <table style="width: 50%" class="ui-widget-content">
            <tr class="ui-widget-header">
                <th class="header-coloumn" colspan="2">Data Mahasiswa dan Pembayaran</th>
            </tr>
            <tr>
                <td>NAMA</td>
                <td>{$data.NM_PENGGUNA}</td>
            </tr>
            <tr>
                <td>NIM</td>
                <td>{$data.NIM_MHS}</td>
            </tr>
            <tr>
                <td>PRODI</td>
                <td>{$data.NM_JENJANG} {$data.NM_PROGRAM_STUDI}</td>
            </tr>
            <tr>
                <td>TANGGAL BAYAR WISUDA</td>
                <td>{$data.TGL_BAYAR}</td>
            </tr>
            <tr>
                <td>BANK</td>
                <td>{$data.NM_BANK} {$data.NAMA_BANK_VIA}</td>
            </tr>
            <tr>
                <td>PERIODE WISUDA</td>
                <td>
                    <select name="periode">
                        {foreach $data_periode as $p}
                            <option value="{$p.ID_PERIODE_WISUDA}" {if $data.ID_PERIODE_WISUDA==$p.ID_PERIODE_WISUDA}selected="true"{/if}>{$p.NM_JENJANG} {$p.NM_TARIF_WISUDA} {$p.NM_SEMESTER} {$p.THN_AKADEMIK_SEMESTER}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="center">
                    <input type="hidden" name="mode" value="update"/>
                    <input type="hidden" name="id_pem" value="{$data.ID_PEMBAYARAN_WISUDA}"/>
                    <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Update" />
                </td>
            </tr>
        </table>
    </form>
{else}
    {$error}
{/if}