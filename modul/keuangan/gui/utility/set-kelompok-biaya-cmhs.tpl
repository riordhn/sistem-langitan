<form id="form-kelompok" action="generate-biaya-maba-pasca.php?id_penerimaan={$cmb.ID_PENERIMAAN}" method="post">
    <table class="ui-widget-content" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn">Pilih Kelompok Biaya Cmhs</th>
        </tr>
        <tr>
            <td>Kelompok Biaya</td>
            <td>
                <select class="required" name="kelompok_biaya">
                    {foreach $data_kb as $kb}
                        <option value="{$kb.ID_KELOMPOK_BIAYA}">{$kb.NM_KELOMPOK_BIAYA} ({if $kb.KHUSUS==1}Khusus{else}Reguler{/if})</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <input type="submit" value="Update" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;"/>
            </td>
        </tr>
    </table>
    <input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}"/>
    <input type="hidden" name="mode" value="update_kelompok"/>
</form>
{literal}
    <script type="text/javascript">
        $('#form-kelompok').validate();            
        $('#form-kelompok').submit(function(){
            if($('#form-kelompok').valid()){
                $('#dialog-kelompok').dialog('close');
            }
            else
                return false;
        });
    </script>
{/literal}