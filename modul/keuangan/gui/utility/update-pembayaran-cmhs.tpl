<div class="center_title_bar">Update Pembayaran Mahasiswa Baru</div> 
<form method="get" id="form" action="update-pembayaran-cmhs.php">
    <div id="alert" class="ui-widget" style="display:none;margin-left:15px;">
        <div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;"> 
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                Data Harus Diisi.</p>
        </div>
    </div>
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="3" class="header-coloumn">Input Form</th>
        </tr>
        <tr class="ui-widget-content">
            <td>No Ujian</td>
            <td><input id="no_ujian" type="search" name="no_ujian" value="{$smarty.get.no_ujian}"/></td>
            <td><input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Cari"/></td>
        </tr>
    </table>
</form>
{if isset($cari_no_ujian)}
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="2" style="text-align: center;">Biodata</th>
        </tr>
        <tr  class="ui-widget-content">
            <td>No Ujian</td>
            <td>{$data_pembayaran[0]['NO_UJIAN']}</td>            
        </tr>
        <tr  class="ui-widget-content">
            <td>Nama</td>
            <td>{$data_pembayaran[0]['NM_C_MHS']}</td>            
        </tr>
        <tr  class="ui-widget-content">
            <td>Program Studi</td>
            <td>{$data_pembayaran[0]['NM_PROGRAM_STUDI']}</td>            
        </tr>
        <tr  class="ui-widget-content">
            <td>Fakultas</td>
            <td>{$data_pembayaran[0]['NM_FAKULTAS']}</td>            
        </tr>
    </table>
    <p></p>
    <form action="update-pembayaran-cmhs.php?no_ujian={$smarty.get.no_ujian}" method="post">
        <table class="ui-widget" style="width: 90%">
            <tr class="ui-widget-header">
                <th colspan="9">Update Biaya Mahasiswa Baru All</th>
            </tr>
            <tr class="ui-widget-header">
                <th>Tagihkan</th>
                <th>Nama Bank</th>
                <th>Via Bank</th>
                <th>Tanggal Bayar</th>
                <th>No Transaksi</th>
                <th>Keterangan</th>
                <th>Status Pembayaran</th>
                <th>Proses</th>
            </tr>
            <tr  class="ui-widget-content">
                <td>
                    <select name="is_tagih">
                        <option {if $data_pembayaran[0].IS_TAGIH=='Y'}selected="true" {/if} value="Y">Ya</option>
                        <option {if $data_pembayaran[0].IS_TAGIH=='T'}selected="true" {/if} value="T">Tidak</option>
                    </select>
                </td>
                <td>
                    <select name="bank">
                        <option value=""></option>
                        {foreach $data_bank as $data}
                            <option value="{$data['ID_BANK']}" {if $data['ID_BANK']==$data_pembayaran[0]['ID_BNK']}selected="true"{/if}>{$data['NM_BANK']}</option>
                        {/foreach}
                    </select>
                </td>
                <td>
                    <select name="bank_via">
                        <option value=""></option>
                        {foreach $data_bank_via as $data}
                            <option value="{$data['ID_BANK_VIA']}" {if $data['ID_BANK_VIA']==$data_pembayaran[0]['ID_BNKV']}selected="true"{/if}>{$data['NAMA_BANK_VIA']}</option>
                        {/foreach}
                    </select>
                </td>
                <td>
                    <input type="text" class="date" name="tgl_bayar" size="7" value="{$data_pembayaran[0]['TGL_BAYAR']}" />
                </td>
                <td>
                    <input type="text" name="no_transaksi" size="15" value="{$data_pembayaran[0]['NO_TRANSAKSI']}" />
                </td>
                <td>
                    <textarea name="keterangan">{$data_pembayaran[0]['KETERANGAN']} </textarea>
                </td>
                <td>
                    <select name="status">
                        <option value="">Pilih Status</option>
                        {foreach $data_status_bayar as $sb}
                            <option value="{$sb.ID_STATUS_PEMBAYARAN}" {if $data_pembayaran[0]['ID_STATUS_PEMBAYARAN']==$sb.ID_STATUS_PEMBAYARAN}selected="true"{/if}>{$sb.NAMA_STATUS}</option>
                        {/foreach}
                    </select>
                </td>
                <td class="center">
                    <input type="hidden" name="no_ujian" value="{$data_pembayaran[0]['NO_UJIAN']}"/>                    
                    <input type="hidden" name="id_c_mhs" value="{$data_pembayaran[0]['ID_C_MHS']}"/>
                    <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Update"/>
                    <input type="hidden" name="mode" value="biaya_all"/>
                </td>
            </tr>
        </table>
    </form>
    <form action="update-pembayaran-cmhs.php?no_ujian={$smarty.get.no_ujian}" style="width: 100%" method="post">
        <table class="ui-widget">
            <tr class="ui-widget-header">
                <th colspan="11" style="text-align: center;">Update Biaya Mahasiswa Baru Detail</th>
            </tr>
            <tr class="ui-widget-header">
                <th>Nama Biaya</th>
                <th>Besar Biaya</th>
                <th>Tagihkan</th>
                <th>Nama Bank</th>
                <th>Via Bank</th>
                <th>Tanggal Bayar</th>
                <th>No Transaksi</th>
                <th>Keterangan</th>
                <th>Status Pembayaran</th>
            </tr>
            {$index=1}
            {foreach $data_pembayaran as $data}
                {if $data['NM_BIAYA']}
                    <tr  class="ui-widget-content">
                        <td>
                            <input type="hidden" name="id{$index}" value="{$data['ID_PEMBAYARAN_CMHS']}"/>
                            {$data['NM_BIAYA']}
                        </td>
                        <td>
                            <input type="text" size="8" name="besar_biaya{$index}" value="{number_format($data['BESAR_BIAYA'])}" />
                        </td>
                        <td>
                            <select name="is_tagih{$index}">
                                <option {if $data.IS_TAGIH=='Y'}selected="true" {/if} value="Y">Ya</option>
                                <option {if $data.IS_TAGIH=='T'}selected="true" {/if} value="T">Tidak</option>
                            </select>
                        </td>
                        <td>
                            <select name="bank{$index}">
                                <option value=""></option>
                                {foreach $data_bank as $bank}
                                    <option value="{$bank['ID_BANK']}" {if $bank['ID_BANK']==$data['ID_BANK']}selected="true"{/if}>{$bank['NM_BANK']}</option>
                                {/foreach}
                            </select>
                        </td>
                        <td>
                            <select name="bank_via{$index}">
                                <option value=""></option>
                                {foreach $data_bank_via as $bank_via}
                                    <option value="{$bank_via['ID_BANK_VIA']}" {if $bank_via['ID_BANK_VIA']==$data['ID_BANK_VIA']}selected="true"{/if}>{$bank_via['NAMA_BANK_VIA']}</option>
                                {/foreach}
                            </select>
                        </td>
                        <td>
                            <input type="text" class="date" name="tgl_bayar{$index}" size="7" value="{$data['TGL_BAYAR']}" />
                        </td>
                        <td>
                            <input type="text" name="no_transaksi{$index}" size="10" value="{$data['NO_TRANSAKSI']}" />
                        </td>
                        <td>
                            <textarea name="keterangan{$index}">{$data['KETERANGAN']}</textarea>
                        </td>
                        <td>
                            <select name="status{$index++}">
                                <option value="">Pilih Status</option>  
                                {foreach $data_status_bayar as $sb}
                                    <option value="{$sb.ID_STATUS_PEMBAYARAN}" {if $data['ID_STATUS_PEMBAYARAN']==$sb.ID_STATUS_PEMBAYARAN}selected="true"{/if}>{$sb.NAMA_STATUS}</option>
                                {/foreach}
                            </select>
                        </td>
                    </tr>
                {else}
                    <tr class="ui-widget-content">
                        <td colspan="11" style="text-align: center;">Data Pembayaran Kosong</td>
                    </tr>
                {/if}
            {/foreach}
            <tr  class="ui-widget-content">
                <td colspan="11" class="center">
                    <input type="hidden" name="no_ujian" value="{$data_pembayaran[0]['NO_UJIAN']}"/>
                    <input type="hidden" name="id_c_mhs" value="{$data_pembayaran[0]['ID_C_MHS']}"/>
                    <input type="hidden" name="jumlah" value="{$index}"/>
                    <span class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="$('#dialog-tambah').dialog('open').load('tambah-detail-pembayaran-cmhs.php?id_cmhs={$data_pembayaran[0]['ID_C_MHS']}')">Tambah Detail Pembayaran</span>
                    <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Update"/>
                </td>
            </tr>
        </table>
        <input type="hidden" name="mode" value="biaya_detail"/>
    </form>
{/if}
<div id="dialog-tambah" title="Tambah Pembayaran Detail"></div>
{literal}
    <script>
            $(function() {
                    $( ".date" ).datepicker({dateFormat:'dd-M-yy'});
            });
                        $('#form').submit(function() {
                    if($('#no_ujian').val()==''){
                            $('#alert').fadeIn();
                    return false;
                    }  
            });
            $('#dialog-tambah').dialog({
                width:'40%',
                modal: true,
                resizable:false,
                autoOpen:false
            });
    </script>
{/literal}