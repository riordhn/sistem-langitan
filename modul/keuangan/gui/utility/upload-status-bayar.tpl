<link rel="stylesheet" type="text/css" href="../../css/jquery-ui-custom-keu.css" />
<link rel="stylesheet" type="text/css" href="../../css/keuangan-style.css" />
<script language="javascript" src="../../js/jquery-1.5.1.min.js"></script>
<script language="javascript" src="../../js/jquery.validate.js"></script>
<script language="javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
{literal}
    <style>
        label.error {
            color: red;
            font-size:11px;
        }
    </style>
{/literal}
{if isset($sukses_upload)}
    <div class="ui-state-highlight ui-corner-all" style="margin: 10px auto;padding: 10px;width:20%"> 
        <span class="ui-icon ui-icon-info" style="float: left;margin:0px 5px;">  </span>
        Data Pembayaran Berhasil Di Simpan.
    </div>
{/if}
<form id="form_upload" action="upload-status-bayar.php" method="post" enctype="multipart/form-data">
    <table class="ui-widget" width="700px" style="margin: 20px auto;">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn">Upload Status Pembayaran Mahasiswa</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Semester</td>
            <td>
                <select name="semester">
                    {foreach $data_semester as $data}
                        <option value="{$data['ID_SEMESTER']}" {if $data['ID_SEMESTER']==$smarty.post.semester}selected="true"{/if}>{$data['NM_SEMESTER']}({$data['TAHUN_AJARAN']})</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Biaya yang Di ubah Statusnya</td>
            <td>
                <select name="biaya">
                    <option value="0" {if $smarty.post.biaya==0}selected="true"{/if}>Semua</option>
                    <option value="1" {if $smarty.post.biaya==1}selected="true"{/if}>SOP Saja</option>
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Status Pembayaran</td>
            <td>
                <select name="status">
                    {foreach $data_status as $data}
                        <option value="{$data['ID_STATUS_PEMBAYARAN']}" {if $data['ID_STATUS_PEMBAYARAN']==$smarty.post.status}selected="true"{/if}>{$data['NAMA_STATUS']}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Tanggal Mulai</td>
            <td><input type="text" name="tgl_mulai" id="tgl_mulai" value="{if isset($smarty.post.tgl_mulai)}{$smarty.post.tgl_mulai} {/if}" class="date_pick" /></td>
        </tr>
        <tr class="ui-widget-content">
            <td>Tanggal Jatuh Tempo</td>
            <td><input type="text" name="tgl_tempo" id="tgl_tempo" value="{if isset($smarty.post.tgl_tempo)}{$smarty.post.tgl_tempo} {/if}" class="date_pick" /></td>
        </tr>
        <tr class="ui-widget-content">
            <td>Keterangan</td>
            <td><textarea name="keterangan" class="required"   rows="5" cols="40">{if isset($smarty.post.keterangan)}{$smarty.post.keterangan} {/if}</textarea> </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Upload File</td>
            <td><input type="file" name="file" id="file" class="required" /> </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" class="center">
                <input type="submit" name="excel" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Submit" />
                <input type="reset" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Reset" />
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="excel-status-bayar.php">Download Template</a>
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="upload-status-bayar.php">Reload</a>
            </td>
        </tr>
    </table>
</form>
<div id="hasil_ubah_password" {if $status_upload==''}style="display:none"{/if}>
    {if $status_upload==1}
        <div class="ui-state-highlight ui-corner-all" style="margin: 10px auto;padding: 10px;width:30%"> 
            <span class="ui-icon ui-icon-info" style="float: left;margin:0px 5px;">  </span>
            Berhasil Upload File <br/>{$status_message}
        </div>
    {else if $status_upload==0}
        <div class="ui-state-error ui-corner-all" style="margin: 10px auto;padding: 10px;width:30%"> 
            <span class="ui-icon ui-icon-alert" style="float: left;margin:0px 5px;">  </span> 
            Gagal Upload file <br/>{$status_message}
        </div>
    {/if}
</div>
{if isset($data_status_bayar)}
    <table class="ui-widget" width="700px" style="margin: 20px auto;">
        <tr class="ui-widget-header">
            <th colspan="5" class="header-coloumn">Status Upload Data</th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>NIM</th>
            <th>Nama</th>
            <th>Status Data Mahasiswa</th>
            <th>Status Pembayaran</th>
        </tr>
        {foreach $data_status_bayar as $data}
            <tr  {if $data.STATUS_MAHASISWA=='Data Mahasiswa Tidak Ada'}style="background-color: #eab8b8"{else}class="ui-widget-content"{/if}>
                <td>{$data@index+1}</td>
                <td>{$data.NIM_MHS}</td>
                <td>{$data.NAMA}</td>
                <td>{$data.STATUS_MAHASISWA}</td>
                <td>{if $data.STATUS_PEMBAYARAN}Pembayaran Sudah Terbayar{else}Belum Ada Pembayaran{/if}</td>
            </tr>
        {/foreach}
        <tr class="ui-widget-content total">
            <td colspan="5" class="center">
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="confirm_save_data('upload-status-bayar.php?mode=save_excel')">Save Data</a>
            </td>
        </tr>
    </table>
{/if}
{literal}
    <script>
            function confirm_save_data(url){
                var c=confirm("Silahkan Periksa Kembali Parameternya. Apakah anda yakin menyimpan data ini? ");
                if(c==true){
                    window.location.href=url;
                }
            }
            $(function() {
                    $( ".date_pick" ).datepicker({dateFormat:'dd-M-yy',changeMonth: true,
                            changeYear: true});
            });
            $('form').validate();
    </script>
{/literal}