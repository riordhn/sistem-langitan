<div class="center_title_bar">Generate Virtual Account</div> 
<form method="get" id="report_form" action="generate-va.php">
    <table class="ui-widget" style="width:95%;">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td width="15%">Fakultas</td>
            <td width="35%">
                <select name="fakultas" id="fakultas" class="select2" style="width:90%">
                    <option value="">-- Pilih Fakultas --</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
            <td  width="15%">Program Studi</td>
            <td width="35%">
                <select name="prodi" id="prodi" class="select2" style="width:90%">
                    <option value="">Semua</option>
                    {foreach $data_prodi as $data}
                        <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$smarty.get.prodi}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                </select>
            </td>

        </tr>
        <tr class="ui-widget-content">
            <td>Status Mahasiswa</td>
            <td>
                <select multiple="true" size="8" name="status[]" id="status" style="width:90%">
                    <option value="">Semua</option>
                    {foreach $data_status as $data}
                        <option value="{$data.ID_STATUS_PENGGUNA}" {if $data.ID_STATUS_PENGGUNA==$smarty.get.status}selected="true"{/if}>{$data.NM_STATUS_PENGGUNA} ({if $data.STATUS_AKTIF==1}Aktif{else}Tidak{/if})</option>
                    {/foreach}
                </select>
            </td>
            <td>Semester Di Generate</td>
            <td>
                <select name="semester" class="select2" style="width:90%">
                    {foreach $data_semester as $semester}
                        <option value="{$semester['ID_SEMESTER']}" {if $semester.ID_SEMESTER==$smarty.get.semester} selected="true" {/if}>{$semester['NM_SEMESTER']}({$semester['TAHUN_AJARAN']})</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Angkatan Mahasiswa</td>
            <td>
                <select name="angkatan" class="select2" style="width:90%">
                    <option value="">Semua</option>
                    {foreach $data_angkatan as $data}
                        <option value="{$data.ANGKATAN}" {if $data.ANGKATAN==$smarty.get.angkatan}selected="true"{/if}>{$data.ANGKATAN}</option>
                    {/foreach}
                </select>
            </td>
            <td>Jumlah Tagihan</td>
            <td>
                <select name="tagihan">
                    <option value="" {if $smarty.get.tagihan==''}selected="true"{/if}>Semua</option>
                    <option value="0" {if $smarty.get.tagihan==0&&$smarty.get.tagihan!=''}selected="true"{/if}>Tidak Ada</option>
                    <option value="1" {if $smarty.get.tagihan==1}selected="true"{/if}>1</option>
                    <option value="2" {if $smarty.get.tagihan==2}selected="true"{/if}>2</option>
                    <option value="3" {if $smarty.get.tagihan==3}selected="true"{/if}>Lebih Dari 2</option>
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Pembayaran Sebelum</td>
            <td>
                <select name="p_sebelum">
                    <option value="" {if $smarty.get.p_sebelum==''}selected="true"{/if}>Semua</option>
                    <option value="1" {if $smarty.get.p_sebelum=='1'}selected="true"{/if}>Ada, Sudah Bayar/ Penangguhan</option>
                    <option value="2" {if $smarty.get.p_sebelum=='2'}selected="true"{/if}>Ada, Status Belum Bayar / Pembebasan</option>
                    <option value="3" {if $smarty.get.p_sebelum=='3'}selected="true"{/if}>Tidak Ada</option>
                </select>
            </td>
            <td>Jenis Pembayaran Biaya</td>
            <td>
                <select multiple="true"  name="jenis_pembayaran[]" id="jenis_pembayaran" style="width:90%">
                    <option value="">Semua</option>
                    {foreach $data_jenis_detail_biaya as $data}
                        <option value="{$data.ID_JENIS_DETAIL_BIAYA}" {if $data.ID_JENIS_DETAIL_BIAYA==$smarty.get.jenis_pembayaran}selected="true"{/if}>{$data.NM_JENIS_DETAIL_BIAYA}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Tanggal Awal</td>
            <td><input type="text" id="tgl_awal" class="datepicker"  name="awal_periode"  value="{if !empty($smarty.get.awal_periode)}{$smarty.get.awal_periode}{else}{date('Y-m-d')}{/if}" /></td>
            <td>Tanggal Akhir</td>
            <td><input type="text" id="tgl_akhir" class="datepicker"  name="akhir_periode" value="{if !empty($smarty.get.akhir_periode)}{$smarty.get.akhir_periode}{else}{date('Y-m-d')}{/if}"/></td>                
        </tr>
        <tr class="ui-widget-content">
            <td>
                Keterangan
            </td>
            <td colspan="3">
                <textarea name="keterangan" style="width: 90%; height: 50px;">{$smarty.get.keterangan}</textarea>
            </td>
        </tr>
        <tr class="center ui-widget-content">
            <td colspan="6">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" name="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if isset($data_mahasiswa)}
    <form method="post" action="generate-va.php?{$smarty.server.QUERY_STRING}">
        <table class="ui-widget-content" style="width: 95%">
            <tr class="ui-widget-header">
                <th colspan="11" class="header-coloumn">Daftar Mahasiswa</th>
            </tr>
            <tr class="ui-widget-header">
                <th>No</th>
                <th>MVA</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Program Studi</th>
                <th>Angkatan</th>
                <th>Status</th>
                <th>Jumlah Tagihan</th>
                <th>Keterangan</th>
                <th>
                    Check All<br/>
                    <input type="checkbox" id="check_all"/>
                </th>
            </tr>
            {$total_data=0}
            {foreach $data_mahasiswa as $data}
                <tr>
                    <td>{$data@index+1}</td>
                    <td>
                        {if $data.NO_VA!=''}
                            <i style="color:green">Nomor VA sudah di set</i><br/>
                            <b>{$data.NO_VA}</b>
                        {else}
                            <i style="color:red">Nomor VA belum di set</i>
                            <input type="number"  name="no_va{$data@index+1}" value="{'89181'|cat:$data.NIM_MHS}" {if $data.PEMBAYARAN_SEKARANG} readonly="true" {/if}/>
                        {/if}
                    </td>
                    <td><a href="history-bayar.php?cari={$data.NIM_MHS}">{$data.NIM_MHS}</a></td>
                    <td>{$data.NM_PENGGUNA}</td>
                    <td>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</td>
                    <td>{$data.THN_ANGKATAN_MHS}</td>
                    <td>{$data.NM_STATUS_PENGGUNA}</td>
                    <td class="center">
                        {if $data.GEN_SEM!=''}
                            Semester<br/>
                            <b>{number_format($data.GEN_SEM)}</b><br/>
                            Pendaftaran<br/>
                            <b>{number_format($data.GEN_SAT)}</b><br/>
                            Bulanan<br/>
                            <b>{number_format($data.GEN_BULAN)}</b><br/>
                            Kegiatan/Lainya<br/>
                            <b>{number_format($data.GEN_FREE)}</b><br/>
                        {else}
                            Master Biaya Kosong<br/>
                            <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="biaya-mahasiswa.php?cari={$data.NIM_MHS}" >Set Master</a>
                        {/if}
                    </td>
                    <td>{$smarty.get.keterangan}</td>
                    <td class="center">
                        {if $data.PEMBAYARAN_SEKARANG!=''}
                            {if $data.NO_VA!=''}
                                <b style="color:green">Tagihan & VA sudah di set</b><br/>                                
                            {else}
                                <b style="color:green">Tagihan sudah di set</b><br/>
                                <b style="color:orange">Tapi Nomor VA belum</b>  <br/>                              
                                <input type="hidden" name="process_type{$data@index+1}" value="generate_va" />                                
                                <input type="checkbox" class="generate_biaya" name="id_mhs{$data@index+1}" value="{$data.ID_MHS}" /><br/><br/>
                                {$total_data=$data@index+1}
                            {/if}
                        {elseif $data.GEN_SEM==''}
                            <b style="color:orange">Master Biaya Kosong</b>
                        {else}
                            {if $data.GEN_SEM==0}
                                <b style="color:orange">Total tagihan tidak sesuai atau sama dengan 0</b>
                            {else}                                
                                <b style="color:red">Tagihan & VA belum di set</b><br/>
                                <input type="hidden" name="process_type{$data@index+1}" value="generate_invoice" /><br/>
                                <input type="checkbox" class="generate_biaya" name="id_mhs{$data@index+1}" value="{$data.ID_MHS}" /><br/><br/>
                                {$total_data=$data@index+1}
                            {/if}
                        {/if}
                    </td>
                </tr>
            {foreachelse}
                <tr class="ui-widget-content">
                    <td colspan="11" class="center"><span style="color: red">Data Kosong</span></td>
                </tr>                
            {/foreach}
            <tr class="ui-widget-content">
                <td colspan="11" class="center">
                    <input type="submit" value="Proses" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;"/>
                    <input type="hidden" name="total_data" value="{$total_data}"/>
                    <input type="hidden" name="mode" value="generate"/>                    
                    <span class="link_button ui-corner-all" onclick="window.location.href = 'excel-generate-va.php?{$smarty.server.QUERY_STRING}'" style="padding:5px;cursor:pointer;">Excel</span>
                </td>
            </tr>
        </table>
    </form>
{/if}
{literal}
<script>
$(document).ready(function() {
    var statuses = 
    {/literal}
        {if !empty($smarty.get.status)}
            {json_encode($smarty.get.status)}
        {else}
            {json_encode([])}
        {/if}
    {literal};
    var jenis_pembayarans = 
    {/literal}
        {if !empty($smarty.get.jenis_pembayaran)}
            {json_encode($smarty.get.jenis_pembayaran)}
        {else}
            {json_encode([])}
        {/if}
    {literal};
    $(".datepicker").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true,
            changeYear: true});
    $('#fakultas').change(function(){
        $.ajax({
            type:'post',
            url:'getProdi.php',
            data:'id_fakultas='+$(this).val(),
            success:function(data){
                    $('#prodi').html(data);
            }                    
        })
    });
    $('#check_all').click(function(){
        if($(this).is(':checked')){
            $('.generate_biaya').attr('checked', true);
        }else{
            $('.generate_biaya').attr('checked', false);
        }
    });
    $('#select_all').change(function(){
        if($(this).val()=='Y'){
            $('.tagihkan').attr('value', 'Y');
        }else{
            $('.tagihkan').attr('value', 'T');
        }
    });
    $('.select2').select2();
    $('#status').select2().val(statuses).trigger("change");
    $('#jenis_pembayaran').select2().val(jenis_pembayarans).trigger("change");
});
</script>
{/literal}