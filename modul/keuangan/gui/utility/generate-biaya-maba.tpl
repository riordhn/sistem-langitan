{literal}
    <style type="text/css">
        .center { text-align: center; }
        .right { text-align: right; }
        .row2 { background-color: #eee; }
        .prodi { font-size: 10px; }
    </style>
{/literal}
<div class="center_title_bar">Generate Biaya Maba</div>

<form action="generate-biaya-maba.php" method="get" id="filter">
    <table class="ui-widget-content">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">
                Parameter
            </th>
        </tr>
		<tr>
			<td>Fakultas</td>
			<td>
				<select name="fakultas">
					<option value="">- SEMUA -</option>
					{foreach $fakultas as $data}
						<option value="{$data.ID_FAKULTAS}" {if $id_fakultas==$data.ID_FAKULTAS}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
					{/foreach}
				</select>
			</td>
			<td>Jenjang</td>
			<td>
				<select name="jenjang">
					<option value="">- SEMUA -</option>
					{foreach $jenjang as $data}
						<option value="{$data.ID_JENJANG}" {if $id_jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG}</option>
					{/foreach}
				</select>
			</td>
		</tr>
		<tr>
			<td>Jalur</td>
			<td>
				<select name="jalur">
					<option value="">- SEMUA -</option>
					{foreach $jalur as $data}
						<option value="{$data.ID_JALUR}" {if $id_jalur==$data.ID_JALUR}selected="true"{/if}>{$data.NM_JALUR}</option>
					{/foreach}
				</select>
			</td>
			<td>Gelombang</td>
			<td>
				<select id="gelombang" name="gelombang">
                    <option value="">- SEMUA -</option>
                    {section name=foo loop=3} 
                    	<option value="{$smarty.section.foo.iteration}" {if $id_gelombang==$smarty.section.foo.iteration}selected="true"{/if}>{$smarty.section.foo.iteration}</option>
					{/section}
                </select>
			</td>
		</tr>
		<tr>
			<td>Tahun Angaktan</td>
			<td>
				<select name="thn" id="thn">
               	<option value="">- SEMUA -</option>
                    {foreach $data_thn as $data}
                        <option value="{$data.TAHUN}" {if $id_thn==$data.TAHUN}selected="true"{/if}>{$data.TAHUN|upper}</option>
                    {/foreach}
               </select>
			</td>
			<td>Semester</td>
			<td>
				<select name="semester">
                	<option value="">- SEMUA -</option>
                    <option value="Gasal" {if $id_semester == 'Gasal'} selected="selected"{/if}>Ganjil</option>
                    <option value="Genap" {if $id_semester == 'Genap'} selected="selected"{/if}>Genap</option>	
                </select>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="text-align:center">
				<input type="hidden" value="tampil" name="mode" />
				<input type="submit" value="Tampil" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" />
			</td>
		</tr>
    </table>
</form>

{if isset($penerimaan)}
<form action="generate-biaya-maba.php" method="post">
	<table class="ui-widget-content">
		<tr class="ui-widget-header">
			<th>NO</th>
			<th>NO UJIAN</th>
			<th>NAMA</th>
			<th>FAKULTAS</th>
			<th>PRODI</th>
			<th>JALUR</th>
			<th>KELOMPOK BIAYA</th>
			<th>RINCIAN BIAYA</th>
			<th>TOTAL BIAYA</th>
			<th><input type="checkbox" id="cek_all" name="cek_all" /></th>
		</tr>
		{$no = 1}
		{foreach $penerimaan as $data}
			<tr>
				<td>{$no++}</td>
				<td>{$data.NO_UJIAN}</td>
				<td>{$data.NM_C_MHS}</td>
				<td>{$data.NM_FAKULTAS}</td>
				<td>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</td>
				<td>{$data.NM_JALUR}</td>
				<td>{$data.NM_KELOMPOK_BIAYA}</td>
				<td style="text-align:right">
					Biaya : {$data.BIAYA|number_format:0:",":"."}<br />
					SP3 : {$data.SP33|number_format:0:",":"."}		
				</td>
				<td style="text-align:right">{$data.TOTAL_BIAYA|number_format:0:",":"."}</td>
				<td>
					{if $data.SUDAH_GENERATE > 0}
						Sudah Generate
					{else}
					<input type="checkbox" id="cek" class="cek" name="cek{$no}" value="1" />
					<input type="hidden" name="id_c_mhs{$no}" value="{$data.ID_C_MHS}" />
					<input type="hidden" name="id_biaya_kuliah{$no}" value="{$data.ID_BIAYA_KULIAH}" />
					<input type="hidden" name="id_semester{$no}" value="{$data.ID_SEMESTER}" />
					<input type="hidden" name="sp3{$no}" value="{$data.SP33}" />
					{/if}
				</td>
			</tr>
		{/foreach}
		<tr>
			<td colspan="20" style="text-align:center">
				<input type="hidden" value="{$no}" name="no" />
				<input type="hidden" value="generate" name="mode" />
				<input type="submit" value="Generate" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" />
			</td>
		</tr>
	</table>
</form>
{/if}


{literal}
    <script type="text/javascript">		
		$("#cek_all").click(function(){
				var checked_status = this.checked;
				$(".cek").each(function()
				{
					this.checked = checked_status;
				});
		});
		
    </script>
{/literal}