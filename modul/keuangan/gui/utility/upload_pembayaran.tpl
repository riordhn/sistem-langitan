<link rel="stylesheet" type="text/css" href="../../css/jquery-ui-custom-keu.css" />
<link rel="stylesheet" type="text/css" href="../../css/keuangan-style.css" />
<script language="javascript" src="../../js/jquery-1.5.1.min.js"></script>
<script language="javascript" src="../../js/jquery.validate.js"></script>
<script language="javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
{literal}
    <style>
        label.error {
            color: red;
            font-size:11px;
        }
    </style>
{/literal}
{if isset($sukses_upload)}
    <div class="ui-state-highlight ui-corner-all" style="margin: 10px auto;padding: 10px;width:20%"> 
        <span class="ui-icon ui-icon-info" style="float: left;margin:0px 5px;">  </span>
        Data Pembayaran Berhasil Di Simpan.
    </div>
{/if}
<form id="form_upload" action="upload_pembayaran.php" method="post" enctype="multipart/form-data">
    <table class="ui-widget" width="700px" style="margin: 20px auto;">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn">Upload Pembayaran Mahasiswa / Mahasiswa Baru</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Tanggal Bayar</td>
            <td><input type="text" name="tgl_bayar" style="text-transform: uppercase" id="tgl_bayar" value="{if isset($smarty.post.tgl_bayar)}{$smarty.post.tgl_bayar} {/if}" class="date_pick" /></td>
        </tr>
        <tr class="ui-widget-content">
            <td>Semester</td>
            <td>
                <select name="semester">
                    {foreach $data_semester as $data}
                        <option value="{$data['ID_SEMESTER']}" {if $data['ID_SEMESTER']==$smarty.post.semester}selected="true"{/if}>{$data['NM_SEMESTER']}({$data['TAHUN_AJARAN']})</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Bank</td>
            <td>
                <select name="bank">
                    {foreach $data_bank as $data}
                        <option value="{$data['ID_BANK']}" {if $data['ID_BANK']==$smarty.post.bank}selected="true"{/if}>{$data['NM_BANK']}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Bank Via</td>
            <td>
                <select name="bank_via">
                    {foreach $data_bank_via as $data}
                        <option value="{$data['ID_BANK_VIA']}" {if $data['ID_BANK_VIA']==$smarty.post.bank_via}selected="true"{/if}>{$data['NAMA_BANK_VIA']}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Keterangan</td>
            <td><textarea name="keterangan" class="required" cols="30">{if isset($smarty.post.keterangan)}{$smarty.post.keterangan} {/if}</textarea> </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Upload File</td>
            <td><input type="file" name="file" id="file" class="required" /> </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" class="center">
                <input type="submit" name="excel" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Submit" />
                <input type="reset" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Reset" />
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="excel-pembayaran-template.php">Download Template</a>
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="upload_pembayaran.php">Reload</a>
            </td>
        </tr>
    </table>
</form>
<div id="hasil_ubah_password" {if $status_upload==''}style="display:none"{/if}>
    {if $status_upload==1}
        <div class="ui-state-highlight ui-corner-all" style="margin: 10px auto;padding: 10px;width:30%"> 
            <span class="ui-icon ui-icon-info" style="float: left;margin:0px 5px;">  </span>
            Berhasil Upload File <br/>{$status_message}
        </div>
    {else if $status_upload==0}
        <div class="ui-state-error ui-corner-all" style="margin: 10px auto;padding: 10px;width:30%"> 
            <span class="ui-icon ui-icon-alert" style="float: left;margin:0px 5px;">  </span> 
            Gagal Upload file <br/>{$status_message}
        </div>
    {/if}
</div>
{if isset($data_pembayaran_upload)}
    <table class="ui-widget" width="700px" style="margin: 20px auto;">
        <tr class="ui-widget-header">
            <th colspan="5" class="header-coloumn">Status Upload Data</th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>NIM</th>
            <th>Nama</th>
            <th>Data Biaya</th>
            <th>Status Mahasiswa</th>
        </tr>
        {$total_pembayaran_sukses=0}
        {$total_pembayaran_gagal=0}
        {foreach $data_pembayaran_upload as $data}
            <tr class="ui-widget-content" {if $data.STATUS_MAHASISWA =='Data Mahasiswa Tidak Ada'}style="background-color: #eab8b8 "{/if}>
                <td>{$data@index+1}</td>
                <td>{$data.NIM_MHS}</td>
                <td>{$data.NAMA}</td>
                <td>
                    <ol>
                        {foreach $data.DATA_BIAYA as $data_biaya}
                            <li>{$data_biaya.NM_BIAYA} : {$data_biaya.BESAR_BIAYA},Status Bayar : <b{if $data_biaya.STATUS_BAYAR=='Tetap'} style="color: crimson"{/if}>{$data_biaya.STATUS_BAYAR}</b></li>
                            {if $data.STATUS_MAHASISWA =='Data Mahasiswa Tidak Ada'}
                                {$total_pembayaran_gagal = $total_pembayaran_gagal +$data_biaya.BESAR_BIAYA}
                            {else}
                                {$total_pembayaran_sukses = $total_pembayaran_sukses +$data_biaya.BESAR_BIAYA}
                            {/if}

                        {/foreach}
                    </ol>
                </td>
                <td>{$data.STATUS_MAHASISWA}</td>
            </tr>
        {/foreach}
        <tr class="ui-widget-content total">
            <td colspan="3" class="center">
                <b>TOTAL PEMBAYARAN</b>
            </td>
            <td colspan="2" class="center">
                <b> Data Yang Akan Berhasil Masuk : {number_format($total_pembayaran_sukses)}</b><br/>
                <b> Data Yang Akan Gagal Masuk : {number_format($total_pembayaran_gagal)}</b>
            </td>
        </tr>
        <tr class="ui-widget-content total">
            <td colspan="5" class="center">
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="upload_pembayaran.php?mode=save_excel">Save Data</a>
            </td>
        </tr>
    </table>
{/if}
{literal}
    <script>
            $('#form_upload').validate({
                rules: {
                 tgl_bayar: {
                   required: true
                 }
                }
            });
            $(function() {
                    $( ".date_pick" ).datepicker({dateFormat:'dd-M-y'});
            });
    </script>
{/literal}