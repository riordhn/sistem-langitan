<div class="center_title_bar">Input Keuangan Mahasiswa</div>
<form method="get" id="form" action="input-keuangan.php">
	<div id="alert" class="ui-widget" style="display:none;margin-left:15px;">
		<div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;"> 
			<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
				Data Harus Diisi.</p>
		</div>
	</div>
	<table class="ui-widget">
		<tr class="ui-widget-header">
			<th colspan="4" class="header-coloumn">Input Form</th>
		</tr>
		<tr class="ui-widget-content">
			<td>NIM</td>
			<td><input id="cari" type="search" name="cari" value="{if isset($smarty.get.cari)}{$smarty.get.cari}{/if}"/></td>
			<td><input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;" value="Tamplikan"/></td>
		</tr>
	</table>
</form>
{if isset($smarty.get.cari)}
	<table style="width: 400px;" class="ui-widget">
		<tr class="ui-widget-header">
			<th colspan="2" class="header-coloumn" style="text-align: center">Detail Biodata Mahasiswa</th>
		</tr>
		<tr class="ui-widget-content">
			<td>NIM</td>
			<td width="200px">{$data_detail_mahasiswa.NIM_MHS}</td>
		</tr>
		<tr class="ui-widget-content">
			<td>Nama</td>
			<td>{$data_detail_mahasiswa.NM_PENGGUNA}</td>
		</tr>
		<tr class="ui-widget-content">
			<td>Fakultas</td>
			<td>{$data_detail_mahasiswa.NM_FAKULTAS}</td>
		</tr>
		<tr class="ui-widget-content">
			<td>Program Studi</td>
			<td>{$data_detail_mahasiswa.NM_JENJANG} - {$data_detail_mahasiswa.NM_PROGRAM_STUDI}</td>
		</tr>
		<tr class="ui-widget-content">
			<td>Jalur - Angkatan</td>
			<td>{$data_detail_mahasiswa.NM_JALUR} - {$data_detail_mahasiswa.THN_ANGKATAN_MHS}</td>
		</tr>
	</table>
	<p></p>

	<form action="input-keuangan.php?cari={$smarty.get.cari}" method="post">
		
		<table style="width: 400px;" class="ui-widget">
			<tr class="ui-widget-content">
				<td>Tanggal Pembayaran</td>
				<td colspan="3">
					<input type="text" style="text-transform: uppercase" name="tgl_bayar" value="" class="date_pick" />
					<input type="hidden" name="mode" value="simpan" />
				</td>
			</tr>
		</table>

		<table class="ui-widget">
			<tr class="ui-widget-content">
				<td>Semester</td>
				<td colspan="3">
					<select name="semester[]" data-id="1">
						<option>--</option>
						{foreach $semester_set as $s}
							<option value="{$s.ID_SEMESTER}">{$s.SEMESTER}</option>
						{/foreach}
					</select>
				</td>
			</tr>

			<tr class="ui-widget-content">
				<td colspan="2">
					<select name="tagihan[1][]"></select>
				</td>
				<td>Tagihan : <input class="form-control" id="" type="text" name="" value="{number_format($db.BESAR_BIAYA)}" disabled="" /></td>
				<td>Pembayaran : <input class="form-control" id="" type="text" name="" value="" /></td>
			</tr>

		</table>

		<table style="width: 570px;" class="ui-widget">
			<tr class="ui-widget-content">
				<td colspan="5" class="center total"><input type="submit" value="Submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" /></td>
			</tr>
		</table>
	</form>

	<span class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" onclick="history.back(-1)">Kembali</span>
{/if}
{literal}
	<script>
		
		$(document).ready(function() {

			$('#form').submit(function() {
				if ($('#cari').val() == '') {
					$('#alert').fadeIn();
					return false;
				}
			});

			$( ".date_pick" ).datepicker({dateFormat:'dd/mm/yy'});

			/* Semester OnChange */
			$('[name="semester[]"]').change(function(){
				var id_semester = $(this).val();
				var nim = $('[name="cari"]').val();
				var index = $(this).data('id');

				/* Ambil detail tagihan */
				$.ajax({
					url: 'input-keuangan.php?mode=get-detail-tagihan&id_semester='+id_semester+'&nim='+nim,
					dataType: 'json',
					success: function(data) {
						console.log(data);
					}
				});
			});
		});
	</script>
{/literal}