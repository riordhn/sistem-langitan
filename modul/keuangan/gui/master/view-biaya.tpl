<link rel="stylesheet" type="text/css" href="css/themes/blue/style.css" media="print, projection, screen" />
<script type="text/javascript" src="js/jquery.tablesorter.js"></script>
{literal}
    <script type="text/javascript">
        $(function() {
            $("table").tablesorter({widgets: ['zebra']});
        });

    </script>
{/literal}
<div class="center_title_bar">Master Biaya Kuliah</div>
{if $overload==true}
    <div id="alert" class="ui-widget" style="margin-left:15px;">
        <div class="ui-state-error ui-corner-all" style="padding: 5px;width:40%;"> 
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                Overload Data Silahkan Tambahkan Filter pada Parameter.</p>
        </div>
    </div>
{/if}
<form method="get" action="biaya.php">
    <table class="ui-widget" style="width: 95%">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn" style="text-align: center;">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Jenjang</td>
            <td width="40%">
                <select name="jenjang" id="jenjang">
                    <option value="">Semua</option>
                    {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $data.ID_JENJANG==$jenjang}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}
                </select>
            </td>
            <td>Jalur</td>
            <td>
                <select name="jalur" id="jalur">
                    <option value="">Semua</option>
                    {foreach $data_jalur as $data}
                        <option value="{$data.ID_JALUR}" {if $data.ID_JALUR==$jalur}selected="true"{/if}>{$data.NM_JALUR}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Fakultas</td>
            <td>
                <select name="fakultas" id="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
            <td>Angkatan/Semester Masuk </td>
            <td>
                <select name="semester" id="semester">
                    <option value="">Semua</option>
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER==$semester}selected="true"{/if}>{$data.NM_SEMESTER} ( {$data.TAHUN_AJARAN} )</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Program Studi</td>
            <td>
                <select name="prodi" id="prodi">
                    <option value="">Semua</option>
                    {foreach $data_prodi as $data}
                        <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$prodi}selected="true"{/if}>( {$data.NM_JENJANG} ) {$data.NM_PROGRAM_STUDI} ({$data.GELAR})</option>
                    {/foreach}
                </select>
            </td>
            <td>Kelompok Biaya</td>
            <td>
                <select name="kelompok_biaya" id="kelompok_biaya">
                    <option value="">Semua</option>
                    {foreach $data_kelompok_biaya as $data}
                        <option value="{$data.ID_KELOMPOK_BIAYA}" {if $data.ID_KELOMPOK_BIAYA==$kelompok_biaya}selected="true"{/if}>{$data.NM_KELOMPOK_BIAYA} ( {if $data.KHUSUS_KELOMPOK_BIAYA==1}Khusus{else}Reguler{/if} )</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td class="center" colspan="4">
                <input type="hidden" name="mode" value="view" />
                <input type="submit" class="ui-button ui-corner-all ui-state-hover"  value="Tampilkan"/>
                <a class="ui-button ui-corner-all ui-state-hover"  href="biaya.php?mode=add">Tambah</a>
            </td>
        </tr>
    </table>
</form>
{if !empty($status_copy)||!empty($status_delete)}
    {$status_copy}
    {$status_delete} 
    <br/>
    <span class="ui-button ui-corner-all ui-state-hover" style="margin: 10px;padding:5px;cursor:pointer;" onclick="history.back(-1)">Kembali</span>
{/if}
{if $data_biaya_kuliah|@count >0}               
    <table class="tablesorter ui-widget" style="width: 98%">
        <thead>
            <tr class="ui-widget-header">
                <th colspan="12" class="header-coloumn" style="text-align: center;">Master Biaya Kuliah</th>
            </tr>
            <tr class="ui-widget-header">
                <th>No</th>
                <th>Program Studi</th>
                <th>Jenjang</th>
                <th>Jalur</th>
                <th width="120px;">Semester</th>
                <th>Kelompok Biaya</th>
                <th>Total Biaya Pendaftaran</th>
                <th>Total Biaya Semester</th>
                <th>Total Biaya Bulanan</th>
                <th>Total Biaya Lainya</th>
                <th>Keterangan</th>
                <th class="center" width="250px;">Operasi</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_biaya_kuliah as $data}
                <tr class="ui-widget-content">
                    <td>{$data@index+1}</td>
                    <td>{$data.NM_PROGRAM_STUDI}</td>
                    <td>{$data.NM_JENJANG}</td>
                    <td>{$data.NM_JALUR}</td>
                    <td>{$data.NM_SEMESTER} ( {$data.TAHUN_AJARAN} )</td>
                    <td>{$data.NM_KELOMPOK_BIAYA} <br/>( {if $data.KHUSUS_KELOMPOK_BIAYA==1}Khusus{else}Reguler{/if} )</td>
                    <td>{number_format($data.GEN_SAT)}</td>
                    <td>{number_format($data.GEN_SEM)}</td>
                    <td>{number_format($data.GEN_BULAN)}</td>
                    <td>{number_format($data.GEN_FREE)}</td>
                    <td>{$data.KETERANGAN_BIAYA_KULIAH}</td>
                    <td class="center" style="width: 140px">
                        <a class="ui-button ui-corner-all ui-state-hover"  href="biaya.php?mode=edit&id_biaya_kuliah={$data.ID_BIAYA_KULIAH}">Edit</a>
                        <a class="ui-button ui-corner-all ui-state-hover"  href="biaya.php?mode=detail&id_biaya_kuliah={$data.ID_BIAYA_KULIAH}">Detail</a>
                        <span class="ui-button ui-corner-all ui-state-hover"  onclick="$('#dialog-copy-master-biaya').load('copy-master-biaya.php?id_biaya_kuliah={$data.ID_BIAYA_KULIAH}').dialog('open')">Copy</span>
                        {if $data.RELASI_MHS>0}
                            <a href="biaya.php?mode=mhs&bk={$data.ID_BIAYA_KULIAH}" class="ui-button ui-corner-all ui-state-active" style="padding:6px;font-size: 12px;font-weight: bold;color: red">{$data.RELASI_MHS} MHS</a>
                        {else}
                            <span class="ui-button ui-corner-all ui-state-hover"  onclick="$('#dialog-delete').load('biaya.php?mode=delete&id_biaya_kuliah={$data.ID_BIAYA_KULIAH}').dialog('open')">Delete</span>
                        {/if}
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
{/if}
<div id="dialog-delete" title="Delete Master Biaya"></div>
<div id="dialog-copy-master-biaya" title="Copy Master Biaya"></div>
{literal}
    <script type="text/javascript">
        $('#dialog-copy-master-biaya,#dialog-delete').dialog({
            width: '90%',
            modal: true,
            resizable: false,
            autoOpen: false
        });
        $('#fakultas,#jenjang').change(function() {
            $.ajax({
                type: 'post',
                url: 'getProdi.php',
                data: 'id_fakultas=' + $('#fakultas').val() + '&id_jenjang=' + $('#jenjang').val(),
                success: function(data) {
                    $('#prodi').html(data);
                }
            })
        });
    </script>
{/literal}
