<div class="center_title_bar">Master Nama Biaya</div>
<form id="form_add" method="post" action="nama-biaya.php">
    <table class="ui-widget" style="width: 40%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="text-align: center;">Hapus Nama Biaya</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Nama Biaya</td>
            <td>
                {$data_nama_biaya.NM_BIAYA}
            </td>
        </tr>
        <!-- <tr class="ui-widget-content">
            <td>Bulan</td>
            <td>
                {$data_nama_biaya.NM_BULAN}
            </td>
        </tr> -->
        <tr class="ui-widget-content">
            <td>Keterangan Nama Biaya</td>
            <td>
                {$data_nama_biaya.KETERANGAN_BIAYA}
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" class="center">
                <p>Apakah anda yakin menghapus data ini?</p>
                <input type="hidden" name="mode" value="delete"/>
                <input type="hidden" name="id_biaya" value="{$data_nama_biaya.ID_BIAYA}" />
                <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Ya"/>
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="nama-biaya.php">Cancel</a>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
            $('#form_add').validate();
    </script>
{/literal}