{literal}
    <script type="text/javascript">
        $(function() {
            $("table").tablesorter({sortList: [[0, 0]], widgets: ['zebra']});
        });

    </script>
{/literal}
<div class="center_title_bar">Biaya Kuliah Mahasiswa</div>
<form method="get" id="form" action="biaya-mahasiswa.php">
    <div id="alert" class="ui-widget" style="display:none;margin-left:15px;">
        <div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;"> 
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                Data Harus Diisi.</p>
        </div>
    </div>
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Input Form</th>
        </tr>
        <tr class="ui-widget-content">
            <td>NIM</td>
            <td><input id="cari" type="search" name="cari" value="{if isset($smarty.get.cari)}{$smarty.get.cari}{/if}"/></td>
            <td><input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;" value="Tamplikan"/></td>
        </tr>
    </table>
</form>
{if isset($smarty.get.cari)}
    <table style="width: 400px;" class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="text-align: center">Detail Biodata Mahasiswa</th>
        </tr>
        <tr class="ui-widget-content">
            <td>NIM</td>
            <td width="200px">{$data_detail_mahasiswa.NIM_MHS}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>NIM BANK</td>
            <td>{$data_detail_mahasiswa.NIM_BANK}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>NIM LAMA</td>
            <td>{$data_detail_mahasiswa.NIM_LAMA}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Nama</td>
            <td>{$data_detail_mahasiswa.NM_PENGGUNA}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Fakultas</td>
            <td>{$data_detail_mahasiswa.NM_FAKULTAS}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Program Studi</td>
            <td>{$data_detail_mahasiswa.NM_PROGRAM_STUDI}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Jenjang</td>
            <td>{$data_detail_mahasiswa.NM_JENJANG}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Jalur</td>
            <td>{$data_detail_mahasiswa.NM_JALUR}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Angkatan Akademik Mahasiswa</td>
            <td>{$data_detail_mahasiswa.THN_ANGKATAN_MHS}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Kelompok Biaya</td>
            <td>{$data_detail_mahasiswa.NM_KELOMPOK_BIAYA}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Status Cekal</td>
            <td>{$data_detail_mahasiswa.STATUS_CEKAL}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Status Akademik</td>
            <td>{$data_detail_mahasiswa.NM_STATUS_PENGGUNA}</td>
        </tr>
    </table>
    <p></p>
    <table class="tablesorter ui-widget" >
        <thead>
            <tr class="ui-widget-header">
                <th colspan="8">Biaya Kuliah Mahasiswa</th>
            </tr>
            <tr class="ui-widget-header">
                <th>No</th>
                <th>Program Studi</th>
                <th>Jenjang</th>
                <th>Jalur</th>
                <th width="120px;">Semester</th>
                <th>Kelompok Biaya</th>
                <th>Total Biaya</th>
                <th>Keterangan</th>
            </tr>
        </thead>
        <tbody>
            {if !empty($biaya_kuliah_mhs)}
                <tr class="ui-widget-content">
                    <td>1</td>
                    <td>{$biaya_kuliah_mhs.NM_PROGRAM_STUDI}</td>
                    <td>{$biaya_kuliah_mhs.NM_JENJANG}</td>
                    <td>{$biaya_kuliah_mhs.NM_JALUR}</td>
                    <td>{$biaya_kuliah_mhs.NM_SEMESTER} ( {$biaya_kuliah_mhs.TAHUN_AJARAN} )</td>
                    <td>{$biaya_kuliah_mhs.NM_KELOMPOK_BIAYA} ( {if $biaya_kuliah_mhs.KHUSUS_KELOMPOK_BIAYA==1}Khusus{else}Reguler{/if} )</td>
                    <td>{number_format($biaya_kuliah_mhs.BESAR_BIAYA_KULIAH)}</td>
                    <td>{$biaya_kuliah_mhs.KETERANGAN_BIAYA_KULIAH}</td>
                </tr>
            {else}
                <tr class="ui-widget-content">
                    <td style="color: red;text-align: center" colspan="8">Master Biaya Kuliah Mahasiswa Kosong</td>
                </tr>
            {/if}
        </tbody>
    </table>
    <p></p>
    <form action="biaya-mahasiswa.php?cari={$smarty.get.cari}" method="post">
        <table class="tablesorter ui-widget" >
            <thead>
                <tr class="ui-widget-header">
                    <th colspan="13">Ubah Data Biaya Kuliah</th>
                </tr>
                <tr class="ui-widget-header">
                    <th>No</th>
                    <th>Program Studi</th>
                    <th>Jenjang</th>
                    <th>Jalur</th>
                    <th width="120px;">Semester</th>
                    <th>Kelompok Biaya</th>
                    <th>Total Biaya Pendaftaran</th>
                    <th>Total Biaya Semester</th>
                    <th>Total Biaya Bulanan</th>
                    <th>Total Biaya Lainya</th>
                    <th>Keterangan</th>
                    <th width="100px;">Operasi</th>
                    <th>Detail</th>
                </tr>
            </thead>
            <tbody>
                {foreach $data_biaya_kuliah as $data}
                    <tr class="ui-widget-content">
                        <td>{$data@index+1}</td>
                        <td>{$data.NM_PROGRAM_STUDI}</td>
                        <td>{$data.NM_JENJANG}</td>
                        <td>{$data.NM_JALUR}</td>
                        <td>{$data.NM_SEMESTER} ( {$data.TAHUN_AJARAN} )</td>
                        <td>{$data.NM_KELOMPOK_BIAYA} ( {if $data.KHUSUS_KELOMPOK_BIAYA==1}Khusus{else}Reguler{/if} )</td>
                        <td>{number_format($data.GEN_SAT)}</td>
                        <td>{number_format($data.GEN_SEM)}</td>
                        <td>{number_format($data.GEN_BULAN)}</td>
                        <td>{number_format($data.GEN_FREE)}</td>
                        <td>{$data.KETERANGAN_BIAYA_KULIAH}</td>
                        <td class="center">
                            <input type="radio" name="biaya_kuliah" {if $data.ID_BIAYA_KULIAH==$biaya_kuliah_mhs.ID_BIAYA_KULIAH}checked="true"{/if} value="{$data.ID_BIAYA_KULIAH}"/>
                        </td>
                        <td>
                            <a class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" href="biaya.php?mode=detail&id_biaya_kuliah={$data.ID_BIAYA_KULIAH}">
                                Detail
                            </a>
                        </td>
                    </tr>
                {/foreach}
            </tbody>
            <tfoot>
                <tr class="ui-widget-content">
                    <td colspan="13" class="center total"><input type="submit" value="Pilih" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" /></td>
                </tr>
            </tfoot>
        </table>
    </form>
    <span class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" onclick="history.back(-1)">Kembali</span>
{/if}
{literal}
    <script>
        $('#form').submit(function() {
            if ($('#cari').val() == '') {
                $('#alert').fadeIn();
                return false;
            }
        });
    </script>
{/literal}