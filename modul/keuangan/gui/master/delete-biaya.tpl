<form id="form_delete" action="biaya.php" method="post">
    <table class="ui-widget" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn" style="text-align: center;">Detail Biaya Kuliah</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Jenjang</td>
            <td width="40%">{$data_biaya_kuliah_one.NM_JENJANG}</td>
            <td>Jalur</td>
            <td>{$data_biaya_kuliah_one.NM_JALUR}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Program Studi</td>
            <td>{$data_biaya_kuliah_one.NM_PROGRAM_STUDI}</td>
            <td>Kelompok Biaya</td>
            <td>{$data_biaya_kuliah_one.NM_KELOMPOK_BIAYA} ( {if $data_biaya_kuliah_one.KHUSUS_KELOMPOK_BIAYA==1}Khusus{else}Reguler{/if} )</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Semester</td>
            <td>{$data_biaya_kuliah_one.NM_SEMESTER} ( {$data_biaya_kuliah_one.TAHUN_AJARAN} )</td>
            <td>Total Biaya</td>
            <td>{number_format($data_biaya_kuliah_one.BESAR_BIAYA_KULIAH)}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Keterangan</td>
            <td colspan="3">{$data_biaya_kuliah_one.KETERANGAN_BIAYA_KULIAH}</td>
        </tr>
        <tr>
            <td class="center ui-widget-content" colspan="4">
                <p>Apakah anda yakin menghapus data ini ?</p>
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Ya"/>
                <input type="hidden" name="mode" value="delete"/>
                <input type="hidden" name="id_biaya_kuliah" value="{$data_biaya_kuliah_one.ID_BIAYA_KULIAH}" />
                <span class="ui-button ui-corner-all ui-state-hover" style="padding:6px;cursor:pointer;" onclick="$('#dialog-delete').dialog('close')">Batal</span>
            </td>
        </tr>
    </table>
</form>
<script>
    $('#form_delete').submit(function(){
        $('#dialog-delete').dialog('close');
    });
</script>