<form action="kerja-sama.php" method="post" id="edit_group">
    <table class="ui-widget-content" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn">Edit Group Kerjasama / Beasiswa</th>
        </tr>
        <tr>
            <td>Nama Grup</td>
            <td><input class="required" type="text" name="nama" value="{$data_group_kerjasama.NM_GROUP}" disabled /></td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <input type="hidden" name="mode" value="delete_group"/>
                <input type="hidden" name="id" value="{$data_group_kerjasama.ID_GROUP_BEASISWA}"/>
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" onclick="" value="Hapus" style="padding:5px;cursor:pointer;"/>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('#edit_group').submit(function(){
            if($('#edit_group').validate())
                $('#dialog-group').dialog('close');
        });
    </script>
{/literal}