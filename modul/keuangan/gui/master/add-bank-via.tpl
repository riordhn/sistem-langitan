<div class="center_title_bar">Master Bank Via</div>
<form id="form_add" method="post" action="bank-via.php">
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="text-align: center;">Tambah Bank Via</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Kode Bank Via</td>
            <td>
                <input type="text" name="kode" class="required number" size="30" />
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Nama Bank Via</td>
            <td>
                <input type="text" name="nama" class="required" size="30" />
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" class="center">
                <input type="hidden" name="mode" value="add"/>
                <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Tambah"/>
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="bank-via.php">Cancel</a>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
            $('#form_add').validate();
    </script>
{/literal}