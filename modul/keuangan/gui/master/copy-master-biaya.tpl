<div id="alert" class="ui-widget" style="display:none;margin-left:15px;">
    <div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;"> 
        <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
            Data Masih Belum Lengkap.</p>
    </div>
</div>
<p class="center">
    Copy Master Biaya Dari Master<br/>
    Prodi ({$data_biaya_kuliah.NM_JENJANG}) {$data_biaya_kuliah.NM_PROGRAM_STUDI} Jalur {$data_biaya_kuliah.NM_JALUR} <br/>
    Kelompok Biaya {$data_biaya_kuliah.NM_KELOMPOK_BIAYA} ( {if $data_biaya_kuliah.KHUSUS_KELOMPOK_BIAYA==1}Khusus{else}Reguler{/if} )<br/>
    Semester {$data_biaya_kuliah.NM_SEMESTER} ({$data_biaya_kuliah.TAHUN_AJARAN}) 
</p>
<form id="copy_form" method="post" action="biaya.php">
    <table class="ui-widget" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn" style="text-align: center;">Copy Biaya Kuliah Ke Master Baru</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Jenjang</td>
            <td width="40%">
                <select name="jenjang" id="jenjang_copy">
                    <option value="">Pilih</option>
                    {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $data.ID_JENJANG==$jenjang}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}
                </select>
            </td>
            <td>Jalur</td>
            <td>
                <select name="jalur" id="jalur_copy">
                    <option value="">Pilih</option>
                    {foreach $data_jalur as $data}
                        <option value="{$data.ID_JALUR}" {if $data.ID_JALUR==$jalur}selected="true"{/if}>{$data.NM_JALUR}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Fakultas</td>
            <td>
                <select name="fakultas" id="fakultas_copy">
                    <option value="">Pilih</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
            <td>Semester</td>
            <td>
                <select name="semester" id="semester_copy">
                    <option value="">Pilih</option>
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER==$semester}selected="true"{/if}>{$data.NM_SEMESTER} ( {$data.TAHUN_AJARAN} )</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Program Studi</td>
            <td>
                <select name="prodi" id="prodi_copy">
                    <option value="">Pilih</option>
                    {foreach $data_prodi as $data}
                        <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$prodi}selected="true"{/if}>( {$data.NM_JENJANG} ) {$data.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                </select>
            </td>
            <td>Kelompok Biaya</td>
            <td>
                <select name="kelompok_biaya" id="kelompok_biaya_copy">
                    <option value="">Pilih</option>
                    {foreach $data_kelompok_biaya as $data}
                        <option value="{$data.ID_KELOMPOK_BIAYA}" {if $data.ID_KELOMPOK_BIAYA==$kelompok_biaya}selected="true"{/if}>{$data.NM_KELOMPOK_BIAYA} ( {if $data.KHUSUS_KELOMPOK_BIAYA==1}Khusus{else}Reguler{/if} )</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Keterangan</td>
            <td colspan="3">
                <textarea name="keterangan" style="width: 378px; height: 50px;"></textarea>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td class="center" colspan="4">
                <input type="hidden" name="mode" value="copy" />
                <input type="hidden" name="id_biaya_kuliah" value="{$smarty.get.id_biaya_kuliah}" />
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Copy" onclick="$('#dialog-copy-master-biaya').dialog('close')"/>
                <span class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="$('#dialog-copy-master-biaya').dialog('close')">Close</span>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('#add_form').submit(function() {
                    $('.alert_biaya_kuliah').hide();    
                    if($('#semester_copy').val()==''||$('#prodi_copy').val()==''||$('#jenjang_copy').val()==''||$('#jalur_copy').val()==''||$('#kelompok_biaya_copy').val()==''){
                            $('#alert').fadeIn();
                    return false;
                    }  
            });
        $('#fakultas_copy,#jenjang_copy').change(function(){
            $.ajax({
                    type:'post',
                    url:'getProdi.php',
                    data:'id_fakultas='+$('#fakultas_copy').val()+'&id_jenjang='+$('#jenjang_copy').val(),
                    success:function(data){
                            $('#prodi_copy').html(data);
                    }                    
            })
        });
    </script>
{/literal}