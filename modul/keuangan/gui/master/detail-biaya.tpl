<div class="center_title_bar">Master Biaya Kuliah - Detail</div>
<table class="ui-widget" style="width: 90%">
    <tr class="ui-widget-header">
        <th colspan="4" class="header-coloumn" style="text-align: center;">Detail Biaya Kuliah</th>
    </tr>
    <tr class="ui-widget-content">
        <td>Jenjang</td>
        <td width="40%">{$data_biaya_kuliah_one.NM_JENJANG}</td>
        <td>Jalur</td>
        <td>{$data_biaya_kuliah_one.NM_JALUR}</td>
    </tr>
    <tr class="ui-widget-content">
        <td>Program Studi</td>
        <td>{$data_biaya_kuliah_one.NM_PROGRAM_STUDI}</td>
        <td>Kelompok Biaya</td>
        <td>{$data_biaya_kuliah_one.NM_KELOMPOK_BIAYA} ( {if $data_biaya_kuliah_one.KHUSUS_KELOMPOK_BIAYA==1}Khusus{else}Reguler{/if} )</td>
    </tr>
    <tr class="ui-widget-content">
        <td>Semester</td>
        <td>{$data_biaya_kuliah_one.NM_SEMESTER} ( {$data_biaya_kuliah_one.TAHUN_AJARAN} )</td>
        <td>Total Biaya</td>
        <td>{number_format($data_biaya_kuliah_one.BESAR_BIAYA_KULIAH)}</td>
    </tr>
    <tr class="ui-widget-content">
        <td>Keterangan</td>
        <td colspan="3">
            {$data_biaya_kuliah_one.KETERANGAN_BIAYA_KULIAH}
        </td>
    </tr>
    <tr>
        <td class="center ui-widget-content" colspan="4">
            <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="biaya.php?mode=edit&id_biaya_kuliah={$data_biaya_kuliah_one.ID_BIAYA_KULIAH}">Edit</a>
            <span class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="history.back(-1)">Kembali</span>
        </td>
    </tr>
</table>
<table class="ui-widget" style="width: 60%">
    <tr class="ui-widget-header">
        <th colspan="4" class="header-coloumn">Detail Biaya Kuliah</th>
    </tr>
    <tr class="ui-widget-header">
        <th>No</th>
        <th>Nama Biaya</th>
        <th>Tarif</th>
        <th>Jenis Pembayaran Biaya</th>
    </tr>
    {foreach $data_detail_biaya as $data}
        <tr class="ui-widget-content">
            <td>{$data@index+1}</td>
            <td>{$data.NM_BIAYA}</td>
            <td style="text-align:right">
            {if $data.DETAIL_BIAYA!=''}{number_format($data.DETAIL_BIAYA['BESAR_BIAYA'])}{/if}
        </td>
        <td style="text-align:center">
            {if $data.DETAIL_BIAYA['NM_JENIS_DETAIL_BIAYA']}
                {$data.DETAIL_BIAYA['NM_JENIS_DETAIL_BIAYA']}
            {else}
                Belum ada
            {/if}                 
        </td>
    </tr>
    {$jumlah=$data@index+1}
    {$total_biaya = $total_biaya+$data.DETAIL_BIAYA['BESAR_BIAYA']}
{/foreach}
<tr class="ui-widget-content">
    <td class="total" colspan="2">Total Biaya</td>
    <td class="total"  style="text-align:right">
        {number_format($total_biaya)}
    </td>
    <td class="total">
    </td>
</tr>
<tr class="ui-widget-content">
    <td class="total center" colspan="4">
        <span class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="history.back(-1)">Kembali</span>
    </td>
</tr>
</table>