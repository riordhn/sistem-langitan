<form action="kerja-sama.php" method="post" id="add_group">
    <table class="ui-widget-content" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn">Tambah Group Kerjasama / Beasiswa</th>
        </tr>
        <tr>
            <td>Nama Grup</td>
            <td><input class="required" id="nama" type="text" name="nama"/></td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <input type="hidden" name="mode" value="add_group"/>
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" value="Tambah" style="padding:5px;cursor:pointer;"/>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('#add_group').validate();            
        $('#add_group').submit(function(){
            if($('#add_group').valid()){
                $('#dialog-group').dialog('close');
            }
            else{
                return false;
            }
        });
    </script>
{/literal}