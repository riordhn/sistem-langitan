<link rel="stylesheet" type="text/css" href="css/themes/blue/style.css" media="print, projection, screen" />
<script type="text/javascript" src="js/jquery.tablesorter.js"></script>
{literal}
    <script type="text/javascript">
        $(function() {
            $("table").tablesorter({sortList: [[0, 0], [2, 1]], widgets: ['zebra']});
        });

    </script>
{/literal}
<div class="center_title_bar">Master Nama Biaya</div>
<a class="ui-widget ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;margin-left:15px;" href="nama-biaya.php?mode=add">Tambah</a>
<table class="tablesorter ui-widget" style="width: 80%">
    <thead>
        <tr class="ui-widget-header">
            <th colspan="5" class="header-coloumn" style="text-align: center;">Master Nama Biaya</th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>Nama Biaya</th>
            <th>Jenis Biaya</th>
            <th>Keterangan Biaya</th>
            <th>Operasi</th>
        </tr>
    </thead>
    <tbody>
        {foreach $data_nama_biaya as $data}
            <tr class="ui-widget-content">
                <td>{$data@index+1}</td>
                <td>{$data.NM_BIAYA}</td>
                <td>{$data.NM_JENIS_DETAIL_BIAYA}</td>
                <td>{$data.KETERANGAN_BIAYA}</td>
                <td class="center">
                    <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="nama-biaya.php?mode=edit&id_biaya={$data.ID_BIAYA}">Edit</a>
                    {if $data.RELASI==0}
                        <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="nama-biaya.php?mode=delete&id_biaya={$data.ID_BIAYA}">Delete</a>
                    {/if}
                </td>
            </tr>
        {/foreach}
    </tbody>
</table>	

