<div class="center_title_bar">Master Bank Via</div>
<form id="form_add" method="post" action="bank-via.php">
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="text-align: center;">Edit Bank Via</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Kode Bank Via</td>
            <td>
                <input type="text" name="kode" value="{$data_bank_via.KODE_BANK_VIA}" class="required number" size="30" />
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Nama Bank Via</td>
            <td>
                <input type="text" name="nama" value="{$data_bank_via.NAMA_BANK_VIA}" class="required" size="30" />
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" class="center">
                <input type="hidden" name="mode" value="edit"/>
                <input  type="hidden" name="id_bank_via" value="{$data_bank_via.ID_BANK_VIA}"/>
                <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Update"/>
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="bank-via.php">Cancel</a>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
            $('#form_add').validate();
    </script>
{/literal}