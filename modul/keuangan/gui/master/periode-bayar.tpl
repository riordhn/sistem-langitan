{literal}
    <script type="text/javascript">
        $(function() {
            $("table").tablesorter({sortList: [[0, 0]], widgets: ['zebra']});
        });

    </script>
{/literal}
<div class="center_title_bar">Periode Bayar Pembayaran</div>
<table class="tablesorter ui-widget">
    <thead>
        <tr class="ui-widget-header">
            <th colspan="6">PERIODE BAYAR MAHASISWA</th>
        </tr>
        <tr class="ui-widget-header">
            <th>NO</th>
            <th>SEMESTER</th>
            <th>JENJANG</th>
            <th>TANGGAL AWAL</th>
            <th>TANGGAL AKHIR</th>
            <th>KETERANGAN</th>
        </tr>
    </thead>
    <tbody>
        {foreach $data_periode_mhs as $data}
            <tr class="ui-widget-content">
                <td>{$data@index+1}</td>
                <td>{$data.NM_SEMESTER} ({$data.TAHUN_AJARAN})</td>
                <td>Semua</td>
                <td>{$data.TGL_AWAL_PERIODE_BAYAR}</td>
                <td>{$data.TGL_AKHIR_PERIODE_BAYAR}</td>
                <td>{$data.KET_PERIODE_BAYAR}</td>
            </tr>
        {/foreach}
    </tbody>
</table>
<p></p>
<table class="tablesorter ui-widget">
    <thead>
        <tr class="ui-widget-header">
            <th colspan="5">PERIODE BAYAR MAHASISWA BARU</th>
        </tr>
        <tr class="ui-widget-header">
            <th>NO</th>
            <th>PENERIMAAN</th>
            <th>TANGGAL AWAL</th>
            <th>TANGGAL AKHIR</th>
            <th>KETERANGAN</th>
        </tr>
    </thead>
    <tbody>
        {foreach $data_periode_cmhs as $data}
            <tr class="ui-widget-content">
                <td>{$data@index+1}</td>
                <td>{$data.NM_PENERIMAAN} {$data.GELOMBANG} {if $data.NM_JALUR!=''}({$data.NM_JALUR}){/if}</td>
                <td>{$data.TGL_AWAL_PERIODE_BAYAR}</td>
                <td>{$data.TGL_AKHIR_PERIODE_BAYAR}</td>
                <td>{$data.KET_PERIODE_BAYAR}</td>
            </tr>
        {/foreach}
    </tbody>
</table>