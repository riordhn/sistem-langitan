<div class="center_title_bar">Master Biaya Kuliah</div>
<table style="width: 90%">
    <thead class="ui-widget-header">
        <tr>
            <th>NO</th>
            <th>NAMA</th>
            <th>NIM</th>
            <th>ANGKATAN</th>
            <th>PRODI</th>
            <th>FAKULTAS</th>
            <th>JALUR</th>
            <th>STATUS</th>
            <th>DETAIL</th>
        </tr>
    </thead>
    <tbody class="ui-widget-content">
        {foreach $data_mhs as $d}
            <tr>
                <td>{$d@index+1}</td>
                <td>{$d.NM_PENGGUNA}</td>
                <td>{$d.NIM_MHS}</td>
                <td>{$d.THN_ANGKATAN_MHS}</td>
                <td>{$d.NM_JENJANG} {$d.NM_PROGRAM_STUDI}</td>
                <td>{$d.NM_FAKULTAS|upper}</td>
                <td>{$d.NM_JALUR}</td>
                <td>{$d.NM_STATUS_PENGGUNA}</td>
                <td class="center">
                    <a href="history-bayar.php?cari={$d.NIM_MHS}" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;">Lihat History</a>
                </td>
            </tr>
        {/foreach}
    </tbody>
</table>
<span class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="history.back(-1)">Kembali</span>