<form action="kerja-sama.php" method="post" id="add_kerjasama">
    <table class="ui-widget-content" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="8" class="header-coloumn">Tambah Kerjasama / Beasiswa</th>
        </tr>
        <tr>
            <td>Nama Kerjasama / Beasiswa</td>
            <td><input class="required" type="text" name="nama"/></td>
            <td>Penyelenggara</td>
            <td><input type="text" name="penyelenggara"/></td>
            <td>Periode</td>
            <td>
                <input type="text" name="periode"/>
            </td>
            <td>Jenis</td>
            <td>
                <select name="jenis">
                    <option value="Pemerintah">Pemerintah</option>
                    <option value="Swasta">Swasta</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Besar Nominal</td>
            <td>
                <input type="text" name="besar"/>
            </td>
            <td>Group</td>
            <td>
                <select name="group">
                    {foreach $data_group_kerjasama as $data}
                        <option value="{$data.ID_GROUP_BEASISWA}">{$data.NM_GROUP}</option>
                    {/foreach}
                </select>
            </td>
            <td>Keterangan</td>
            <td><textarea name="keterangan"></textarea></td>
        </tr>
        <tr>
            <td class="center" colspan="8">
                <input type="hidden" name="mode" value="add_kerjasama"/>
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" value="Tambah" style="padding:5px;cursor:pointer;"/>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('#add_kerjasama').validate();            
        $('#add_kerjasama').submit(function(){
            if($('#add_kerjasama').valid()){
                $('#dialog-kerjasama').dialog('close');
            }
            else
                return false;
        });
    </script>
{/literal}