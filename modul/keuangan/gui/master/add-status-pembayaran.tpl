<div class="center_title_bar">Master Status Pembayaran</div>
<form id="form_add" method="post" action="status-pembayaran.php">
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="text-align: center;">Tambah Status Pembayaran</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Nama Status</td>
            <td>
                <input type="text" name="nama" class="required" size="30" />
            </td>

        </tr>
        <tr class="ui-widget-content">
            <td>Keterangan</td>
            <td>
                <textarea name="keterangan"></textarea>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" class="center">
                <input type="hidden" name="mode" value="add"/>
                <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Tambah"/>
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="status-pembayaran.php">Cancel</a>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
            $('#form_add').validate();
    </script>
{/literal}