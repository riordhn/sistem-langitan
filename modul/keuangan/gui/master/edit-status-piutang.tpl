<div class="center_title_bar">Master Status Piutang</div>
<form id="form_add" method="post" action="status-piutang.php">
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="text-align: center;">Edit Bank</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Nama Status</td>
            <td>
                <input type="text" name="nama" value="{$data_status.NAMA_STATUS}" class="required" size="30" />
            </td>

        </tr>
        <tr class="ui-widget-content">
            <td>Keterangan</td>
            <td>
                <textarea name="keterangan">{$data_status.KETERANGAN}</textarea>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" class="center">
                <input type="hidden" name="mode" value="edit"/>
                <input  type="hidden" name="id_status" value="{$data_status.ID_STATUS_PIUTANG}"/>
                <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Update"/>
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="status-piutang.php">Cancel</a>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
            $('#form_add').validate();
    </script>
{/literal}