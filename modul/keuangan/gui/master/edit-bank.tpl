<div class="center_title_bar">Master Bank</div>
<form id="form_add" method="post" action="bank.php">
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="text-align: center;">Edit Bank</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Nama Bank</td>
            <td>
                <input type="text" name="nama" value="{$data_bank.NM_BANK}" class="required" size="30" />
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" class="center">
                <input type="hidden" name="mode" value="edit"/>
                <input  type="hidden" name="id_bank" value="{$data_bank.ID_BANK}"/>
                <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Update"/>
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="bank.php">Cancel</a>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
            $('#form_add').validate();
    </script>
{/literal}