<div class="center_title_bar">Master Biaya Kuliah - Edit</div>
<form action="biaya.php?mode=edit&id_biaya_kuliah={$data_biaya_kuliah_one.ID_BIAYA_KULIAH}" method="post">
    <table class="ui-widget" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn" style="text-align: center;">Detail Biaya Kuliah</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Jenjang</td>
            <td width="40%">{$data_biaya_kuliah_one.NM_JENJANG}</td>
            <td>Jalur</td>
            <td>
                <select name="jalur" id="jalur">
                    {foreach $data_jalur as $data}
                        <option value="{$data.ID_JALUR}" {if $data.ID_JALUR==$data_biaya_kuliah_one.ID_JALUR}selected="true"{/if}>{$data.NM_JALUR}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Program Studi</td>
            <td>{$data_biaya_kuliah_one.NM_PROGRAM_STUDI}</td>
            <td>Kelompok Biaya</td>
            <td>
                <select name="kelompok_biaya" id="kelompok_biaya">
                    {foreach $data_kelompok_biaya as $data}
                        <option value="{$data.ID_KELOMPOK_BIAYA}" {if $data.ID_KELOMPOK_BIAYA==$data_biaya_kuliah_one.ID_KELOMPOK_BIAYA}selected="true"{/if}>{$data.NM_KELOMPOK_BIAYA} ( {if $data.KHUSUS_KELOMPOK_BIAYA==1}Khusus{else}Reguler{/if} )</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Angkatan/Semester Masuk </td>
            <td>
                <select name="semester" id="semester">
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER==$data_biaya_kuliah_one.ID_SEMESTER}selected="true"{/if}>{$data.NM_SEMESTER} ( {$data.TAHUN_AJARAN} )</option>
                    {/foreach}
                </select>
            </td>
            <td>Total Biaya</td>
            <td>{number_format($data_biaya_kuliah_one.BESAR_BIAYA_KULIAH)}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Keterangan</td>
            <td colspan="3">
                <textarea name="keterangan" cols="40">{$data_biaya_kuliah_one.KETERANGAN_BIAYA_KULIAH}</textarea>
            </td>
        </tr>
        <tr>
            <td class="center ui-widget-content" colspan="4">
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Update"/>
                <input type="hidden" name="mode" value="edit-master"/>
                <input type="hidden" name="id_biaya_kuliah" value="{$data_biaya_kuliah_one.ID_BIAYA_KULIAH}" />
                <span class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="history.back(-1)">Kembali</span>
            </td>
        </tr>
    </table>
</form>
<form id="edit_form" action="biaya.php?mode=edit&id_biaya_kuliah={$data_biaya_kuliah_one.ID_BIAYA_KULIAH}" method="post">
    <table class="ui-widget" style="width: 60%">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Detail Biaya Kuliah</th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>Nama Biaya</th>
            <th>Tarif</th>
            <th>Jenis Pembayaran Biaya</th>
        </tr>
        {foreach $data_detail_biaya as $data}
            <tr class="ui-widget-content">
                <td>{$data@index+1}</td>
                <td>{$data.NM_BIAYA}</td>
                <td style="text-align:center">
                    <input type="hidden" name="id_biaya{$data@index+1}" value="{$data.ID_BIAYA}" />
                    <input class="besar_biaya"  style="text-align:right" index="{$data@index+1}" id="besar_biaya{$data@index+1}" type="text" name="besar_biaya{$data@index+1}" value="{if $data.DETAIL_BIAYA!=''}{$data.DETAIL_BIAYA['BESAR_BIAYA']}{/if}"/>
                </td>
                <td style="text-align:center">
                    <select name="id_jenis_detail_biaya{$data@index+1}">
                        {foreach $data_jenis_detail_biaya as $db}
                            {if $data.DETAIL_BIAYA['ID_JENIS_DETAIL_BIAYA']!=''}
                            {$default_jenis_biaya=$data.DETAIL_BIAYA['ID_JENIS_DETAIL_BIAYA']}
                            {else}
                            {$default_jenis_biaya=$data['JENIS_BIAYA']}                            
                            {/if}
                            <option value="{$db.ID_JENIS_DETAIL_BIAYA}" {if $db.ID_JENIS_DETAIL_BIAYA == $default_jenis_biaya}selected="selected"{/if}>{$db.NM_JENIS_DETAIL_BIAYA}</option>
                        {/foreach}
                    </select>               
                </td>
            </tr>
            {$jumlah=$data@index+1}
            {$total_biaya = $total_biaya+$data.DETAIL_BIAYA['BESAR_BIAYA']}
        {/foreach}
        <tr class="ui-widget-content">
            <td class="total" colspan="2">Total Biaya</td>
            <td class="total"  style="text-align:center">
                <input type="text"  style="text-align:right" name="total_biaya" id="total_biaya" readonly="true" value="{$total_biaya}"/>
            </td>
            <td class="total">
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td class="total center" colspan="4">
                <input type="hidden" name="mode" value="edit" />
                <input type="hidden" name="jumlah" value="{$jumlah}" />
                <input type="hidden" name="id_biaya_kuliah" value="{$data_biaya_kuliah_one.ID_BIAYA_KULIAH}" />
                <span class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="history.back(-1)">Kembali</span>
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Update"/>
                <input type="reset" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Reset"/>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $(".besar_biaya").each(function() {

                        $(this).change(function(){
                                calculateSum();
                        });
                });


        function calculateSum() {
                var sum = 0;
                $(".besar_biaya").each(function() {

                        if(!isNaN(this.value) && this.value.length!=0) {
                                sum += parseFloat(this.value);
                        }

                });
                $("#total_biaya").val(sum);
        }
        $('.besar_biaya').change(function(){
            $('#pendaftaran'+$(this).attr('index')).attr('checked','true');
        })    
    </script>
{/literal}