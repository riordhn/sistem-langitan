<form action="kerja-sama.php" method="post" id="edit_kerjasama">
    <table class="ui-widget-content" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="8" class="header-coloumn">Edit Kerjasama / Beasiswa</th>
        </tr>
        <tr>
            <td>Nama Kerjasama / Beasiswa</td>
            <td><input class="required" type="text" name="nama" value="{$data_kerjasama.NM_BEASISWA}"/></td>
            <td>Penyelenggara</td>
            <td><input type="text" name="penyelenggara" value="{$data_kerjasama.PENYELENGGARA_BEASISWA}"/></td>
            <td>Periode</td>
            <td>
                <input type="text" name="periode" value="{$data_kerjasama.PERIODE_PEMBERIAN_BEASISWA}"/>
            </td>
            <td>Jenis</td>
            <td>
                <select name="jenis">
                    <option value="Pemerintah" {if $data_kerjasama.JENIS_BEASISWA=='Pemerintah'}selected="true"{/if}>Pemerintah</option>
                    <option value="Swasta" {if $data_kerjasama.JENIS_BEASISWA=='Swasta'}selected="true"{/if}>Swasta</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Besar Nominal</td>
            <td>
                <input type="text" name="besar" value="{$data_kerjasama.BESAR_BEASISWA}"/>
            </td>
            <td>Group</td>
            <td>
                <select name="group">
                    {foreach $data_group_kerjasama as $data}
                        <option value="{$data.ID_GROUP_BEASISWA}" {if $data.ID_GROUP_BEASISWA==$data_kerjasama.ID_GROUP_BEASISWA}selected="true"{/if}>{$data.NM_GROUP}</option>
                    {/foreach}
                </select>
            </td>
            <td>Keterangan</td>
            <td><textarea name="keterangan">{$data_kerjasama.KETERANGAN}</textarea></td>
        </tr>
        <tr>
            <td class="center" colspan="8">
                <input type="hidden" name="id" value="{$data_kerjasama.ID_BEASISWA}"/>
                <input type="hidden" name="mode" value="edit_kerjasama"/>
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" value="Update" style="padding:5px;cursor:pointer;"/>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('#edit_kerjasama').validate();            
        $('#edit_kerjasama').submit(function(){
            if($('#edit_kerjasama').valid()){
                $('#dialog-kerjasama').dialog('close');
            }
            else
                return false;
        });
    </script>
{/literal}