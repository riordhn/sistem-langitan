{literal}
    <script type="text/javascript">
        $(function() {
            $("table").tablesorter({sortList: [[0, 0]], widgets: ['zebra']});
        });

    </script>
{/literal}
<div class="center_title_bar">Master Program Studi</div>
{foreach $data_prodi as $dp}
    <table class="tablesorter ui-widget" style="width: 60%">
        <caption>FAKULTAS : {$dp.NM_FAKULTAS}</caption>
        <thead>
            <tr class="ui-widget-header">
                <th colspan="4">Detail Program Studi</th>
            </tr>
            <tr class="ui-widget-header">
                <th>NO</th>
                <th>NAMA PRODI</th>
                <th>KODE PRODI</th>
                <th>PRODI AKTIF</th>
            </tr>
        </thead>
        <tbody>
            {foreach $dp.DATA_PRODI as $p}
                <tr class="ui-widget-content">
                    <td>{$p@index+1}</td>
                    <td>({$p.NM_JENJANG}) {$p.NM_PROGRAM_STUDI} </td>
                    <td>{$p.ID_PROGRAM_STUDI}</td>
                    <td>{if $p.STATUS_AKTIF_PRODI==1}Aktif{else}Tidak Aktif{/if}</td>
                </tr>
            {/foreach}
        </tbody>
    </table>
{/foreach}