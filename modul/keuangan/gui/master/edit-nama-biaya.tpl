<div class="center_title_bar">Master Nama Biaya</div>
<form id="form_add" method="post" action="nama-biaya.php">
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="text-align: center;">Edit Nama Biaya</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Nama Biaya</td>
            <td>
                <input type="text" value="{$data_nama_biaya.NM_BIAYA}" name="nama" class="required" size="30" />
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Jenis Biaya/Pembayaran</td>
            <td>
                <select name="jenis_biaya">
                {foreach $data_jenis_pembayaran as $d}
                    <option value="{$d.ID_JENIS_DETAIL_BIAYA}" {if $d.ID_JENIS_DETAIL_BIAYA==$data_nama_biaya.JENIS_BIAYA}selected="true"{/if}>{$d.NM_JENIS_DETAIL_BIAYA}</option>
                {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Keterangan Nama Biaya</td>
            <td>
                <textarea name="keterangan" cols="30">{$data_nama_biaya.KETERANGAN_BIAYA}</textarea>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" class="center">
                <input type="hidden" name="mode" value="edit"/>
                <input type="hidden" name="id_biaya" value="{$data_nama_biaya.ID_BIAYA}" />
                <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Update"/>
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="nama-biaya.php">Cancel</a>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
            $('#form_add').validate();
    </script>
{/literal}