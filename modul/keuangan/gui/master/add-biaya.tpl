<div class="center_title_bar">Master Biaya Kuliah - Tambah</div>
<div id="alert" class="ui-widget" style="display:none;margin-left:15px;">
    <div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;"> 
        <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
            Data Masih Belum Lengkap.</p>
    </div>
</div>
<form id="add_form" method="post" action="biaya.php?mode=add">
    <table class="ui-widget" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn" style="text-align: center;">Tambah Biaya Kuliah</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Jenjang</td>
            <td width="40%">
                <select name="jenjang" id="jenjang">
                    <option value="">Pilih</option>
                    {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $data.ID_JENJANG==$jenjang}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}
                </select>
            </td>
            <td>Jalur</td>
            <td>
                <select name="jalur" id="jalur">
                    <option value="">Pilih</option>
                    {foreach $data_jalur as $data}
                        <option value="{$data.ID_JALUR}" {if $data.ID_JALUR==$jalur}selected="true"{/if}>{$data.NM_JALUR}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Fakultas</td>
            <td>
                <select name="fakultas" id="fakultas">
                    <option value="">Pilih</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
            <td>Angkatan/Semester Masuk </td>
            <td>
                <select name="semester" id="semester">
                    <option value="">Pilih</option>
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER==$semester}selected="true"{/if}>{$data.NM_SEMESTER} ( {$data.TAHUN_AJARAN} )</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Program Studi</td>
            <td>
                <select name="prodi" id="prodi">
                    <option value="">Pilih</option>
                    {foreach $data_prodi as $data}
                        <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$prodi}selected="true"{/if}>( {$data.NM_JENJANG} ) {$data.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                </select>
            </td>
            <td>Kelompok Biaya</td>
            <td>
                <select name="kelompok_biaya" id="kelompok_biaya">
                    <option value="">Pilih</option>
                    {foreach $data_kelompok_biaya as $data}
                        <option value="{$data.ID_KELOMPOK_BIAYA}" {if $data.ID_KELOMPOK_BIAYA==$kelompok_biaya}selected="true"{/if}>{$data.NM_KELOMPOK_BIAYA} ( {if $data.KHUSUS_KELOMPOK_BIAYA==1}Khusus{else}Reguler{/if} )</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Keterangan</td>
            <td colspan="3">
                <textarea name="keterangan" style="width: 378px; height: 50px;"></textarea>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td class="center" colspan="4">
                <input type="hidden" name="mode" value="add" />
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tambah"/>
                <span class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="history.back(-1)">Kembali</span>
            </td>
        </tr>
    </table>
</form>
{if $status=='error'}
    <div class="alert_biaya_kuliah ui-widget" style="margin-left:15px;">
        <div class="ui-state-error ui-corner-all" style="padding: 5px;width:30%;"> 
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                Master Biaya Kuliah Sudah Ada.</p>
        </div>
    </div>
{else if $status=='success'}
    <div class="alert_biaya_kuliah ui-widget" style="margin-left:15px;">
        <div class="ui-state-highlight ui-corner-all" style="padding: 5px;width:30%;"> 
            <p><span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span> 
                Master Biaya Kuliah Berhasil Di Tambahkan.</p>
        </div>
    </div>
    <table class="ui-widget" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="9" class="header-coloumn" style="text-align: center;">Master Biaya Kuliah</th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>Program Studi</th>
            <th>Jenjang</th>
            <th>Jalur</th>
            <th width="120px;">Semester</th>
            <th>Kelompok Biaya</th>
            <th>Total Biaya</th>
            <th>Keterangan</th>
            <th width="100px;">Operasi</th>
        </tr>
        <tr class="ui-widget-content">
            <td>{$data_biaya_kuliah_added@index+1}</td>
            <td>{$data_biaya_kuliah_added.NM_PROGRAM_STUDI}</td>
            <td>{$data_biaya_kuliah_added.NM_JENJANG}</td>
            <td>{$data_biaya_kuliah_added.NM_JALUR}</td>
            <td>{$data_biaya_kuliah_added.NM_SEMESTER} ( {$data_biaya_kuliah_added.TAHUN_AJARAN} )</td>
            <td>{$data_biaya_kuliah_added.NM_KELOMPOK_BIAYA} ( {if $data_biaya_kuliah_added.KHUSUS_KELOMPOK_BIAYA==1}Khusus{else}Reguler{/if} )</td>
            <td>{number_format($data_biaya_kuliah_added.BESAR_BIAYA_KULIAH)}</td>
            <td>{$data_biaya_kuliah_added.KETERANGAN_BIAYA_KULIAH}</td>
            <td>
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="biaya.php?mode=edit&id_biaya_kuliah={$data_biaya_kuliah_added.ID_BIAYA_KULIAH}">Edit</a>
            </td>
        </tr>
    </table>
{/if}
{literal}
    <script type="text/javascript">
        $('#add_form').submit(function() {
                    $('.alert_biaya_kuliah').hide();    
                    if($('#semester').val()==''||$('#prodi').val()==''||$('#jenjang').val()==''||$('#jalur').val()==''||$('#kelompok_biaya').val()==''){
                            $('#alert').fadeIn();
                    return false;
                    }  
            });
        $('#fakultas,#jenjang').change(function(){
            $.ajax({
                    type:'post',
                    url:'getProdi.php',
                    data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                    success:function(data){
                            $('#prodi').html(data);
                    }                    
            })
        });
    </script>
{/literal}
