<div class="center_title_bar">Master Kelompok Biaya</div>
<form id="form_add" method="post" action="kelompok-biaya.php">
	<table class="ui-widget">
		<tr class="ui-widget-header">
			<th colspan="2" class="header-coloumn" style="text-align: center;">Tambah Kelompok Biaya</th>
		</tr>
		<tr class="ui-widget-content">
			<td>Nama Kelompok Biaya</td>
			<td>
				<input type="text" name="nama" class="required" size="30" />
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td>Jenis Kelompok Biaya</td>
			<td>
				<input type="radio" name="jenis" value="1" class="required" />Khusus
				<input type="radio" name="jenis" value="0" />Reguler
				<br/>
				<label for="jenis" class="error" style="display:none">Pilih Salah Satu</label>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td>Keterangan Kelompok Biaya</td>
			<td>
				<textarea name="keterangan" cols="30"></textarea>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td colspan="2" class="center">
				<input type="hidden" name="mode" value="add"/>
				<input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Tambah"/>
				<a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="kelompok-biaya.php">Cancel</a>
			</td>
		</tr>
	</table>
</form>
{literal}
	<script type="text/javascript">
		$('#form_add').validate();
	</script>
{/literal}