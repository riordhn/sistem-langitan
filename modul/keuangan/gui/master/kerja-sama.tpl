<div class="center_title_bar">Master Kerjasama / Beasiswa</div>
<table class="ui-widget-content" style="width: 40%">
    <tr class="ui-widget-header">
        <th class="header-coloumn" colspan="3">Grup Kerjasama / Beasiswa</th>
    </tr>
    <tr class="ui-widget-header">
        <th>NO</th>
        <th>NAMA GROUP</th>
        <th>Operasi</th>
    </tr>
    {foreach $data_group_kerjasama as $data}
        <tr>
            <td>{$data@index+1}</td>
            <td>{$data.NM_GROUP}</td>
            <td class="center">
                <span class="ui-button ui-corner-all ui-state-hover" onclick="$('#dialog-group').dialog('open').load('kerja-sama.php?mode=edit_group&id={$data.ID_GROUP_BEASISWA}')" style="padding:5px;cursor:pointer;">Edit</span>
                <span class="ui-button ui-corner-all ui-state-hover" onclick="$('#dialog-group').dialog('open').load('kerja-sama.php?mode=delete_group&id={$data.ID_GROUP_BEASISWA}')" style="padding:5px;cursor:pointer;">Hapus</span>
            </td>
        </tr>
    {foreachelse}
        <tr>
            <td colspan="3" class="data-kosong">Data Masih Kosong</td>
        </tr>
    {/foreach}
    <tr>
        <td colspan="3" class="center">
            <span class="ui-button ui-corner-all ui-state-hover" onclick="$('#dialog-group').dialog('open').load('kerja-sama.php?mode=add_group')" style="padding:5px;cursor:pointer;">Tambah</span>
        </td>
    </tr>
</table>
<p></p>
<table class="ui-widget-content" style="width: 90%">
    <tr class="ui-widget-header">
        <th class="header-coloumn" colspan="9">Kerjasama / Beasiswa</th>
    </tr>
    <tr class="ui-widget-header">
        <th>NO</th>
        <th>Nama Kerjasama / Beasiswa</th>
        <th>Penyelenggara</th>
        <th>Jenis</th>
        <th>Periode</th>
        <th>Besar Beasiswa</th>
        <th>Group Beasiswa</th>
        <th>Keterangan</th>
        <th>Operasi</th>
    </tr>
    {foreach $data_kerjasama as $data}
        <tr>
            <td>{$data@index+1}</td>
            <td>{$data.NM_BEASISWA}</td>
            <td>{$data.PENYELENGGARA_BEASISWA}</td>
            <td>{$data.JENIS_BEASISWA}</td>
            <td>{$data.PERIODE_PEMBERIAN_BEASISWA}</td>
            <td>{if $data.BESAR_BEASISWA!=''}{number_format($data.BESAR_BEASISWA)}{/if}</td>
            <td>{$data.NM_GROUP}</td>
            <td>{$data.KETERANGAN}</td>
            <td>
                <span class="ui-button ui-corner-all ui-state-hover" onclick="$('#dialog-kerjasama').dialog('open').load('kerja-sama.php?mode=edit_kerjasama&id={$data.ID_BEASISWA}')" style="padding:5px;cursor:pointer;">Edit</span>
                <span class="ui-button ui-corner-all ui-state-hover" onclick="$('#dialog-kerjasama').dialog('open').load('kerja-sama.php?mode=delete_kerjasama&id={$data.ID_BEASISWA}')" style="padding:5px;cursor:pointer;">Hapus</span>
            </td>
        </tr>
    {foreachelse}
        <tr>
            <td colspan="9" class="data-kosong">Data Masih Kosong</td>
        </tr>
    {/foreach}
    <tr>
        <td colspan="9" class="center">
            <span class="ui-button ui-corner-all ui-state-hover" onclick="$('#dialog-kerjasama').dialog('open').load('kerja-sama.php?mode=add_kerjasama')" style="padding:5px;cursor:pointer;">Tambah</span>
        </td>
    </tr>
</table>
<div id="dialog-group" title="Master Group Kerjasama"></div>
<div id="dialog-kerjasama" title="Master Kerjasama"></div>
{literal}
    <script type="text/javascript">
        $('#dialog-group').dialog({
            width:'30%',
            modal: true,
            resizable:false,
            autoOpen:false
        });
        $('#dialog-kerjasama').dialog({
            width:'90%',
            modal: true,
            resizable:false,
            autoOpen:false
        });
    </script>
{/literal}

