<div class="center_title_bar">Laporan Pendapatan KKN Mahasiswa</div> 
<form method="get" id="report_form" action="pendapatan-kkn.php">
    <table class="ui-widget" style="width: 30%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Semester</td>
            <td>
                <select name="semester">
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER==$smarty.get.semester}selected="true"{/if}>{$data.NM_SEMESTER} {$data.TAHUN_AJARAN}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        </tr>
        <tr class="center ui-widget-content">
            <td colspan="2">
                <input type="hidden" name="mode" value="fakultas" />
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if isset($data_penw_fakultas)}
    <table class="ui-widget" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="7" class="header-coloumn">
                Laporan Pendapatan KKN Mode Fakultas
            </th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>Nama Fakultas</th>
            <th>Jumlah Mahasiswa</th>
            <th>Jumlah Tarif</th>
            <th>Jumlah Angsuran</th>
            <th>Jumlah Tagihan</th>
            <th>Detail</th>
        </tr>
        {foreach $data_penw_fakultas as $data}
            {$tagihan=$data.TARIF-$data.CICILAN-$data.PELUNASAN}
            <tr class="ui-widget-content">
                <td>{$data@index+1}</td>
                <td class="center">
                    {$data.NM_FAKULTAS}
                </td>
                <td class="center">
                    {$data.JUMLAH_MHS}
                </td>
                <td class="center">
                    {number_format($data.TARIF)}
                </td>
                <td class="center">
                    {number_format($data.ANGSURAN)}
                </td>
                <td class="center">
                    {number_format($data.TARIF-$data.ANGSURAN)}
                </td>
                <td class="center">
                    <a class="ui-button ui-corner-all ui-state-hover" href="pendapatan-kkn.php?mode=prodi&semester={$smarty.get.semester}&fakultas={$data.ID_FAKULTAS}" style="padding:5px;cursor:pointer;">Prodi</a>
                    <a class="ui-button ui-corner-all ui-state-hover" href="pendapatan-kkn.php?mode=detail&semester={$smarty.get.semester}&fakultas={$data.ID_FAKULTAS}" style="padding:5px;cursor:pointer;">Detail</a>
                </td>
            </tr>
            {$total_mhs=$total_mhs+$data.JUMLAH_MHS}
            {$total_tarif=$total_tarif+$data.TARIF}
            {$total_angsuran=$total_angsuran+$data.ANGSURAN}
            {$total_tagihan=$total_tagihan+($data.TARIF-$data.ANGSURAN)}
        {/foreach}
        <tr class="total">
            <td class="center" colspan="2">TOTAL</td>
            <td class="center">{$total_mhs}</td>
            <td class="center">{number_format($total_tarif)}</td>
            <td class="center">{number_format($total_angsuran)}</td>
            <td class="center">{number_format($total_tagihan)}</td>
            <td class="center"><a class="ui-button ui-corner-all ui-state-hover" href="pendapatan-kkn.php?mode=detail&semester={$smarty.get.semester}" style="padding:5px;cursor:pointer;">Detail</a></td>
        </tr>
    </table>
{else if isset($data_penw_prodi)}
    <table class="ui-widget" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="7" class="header-coloumn">
                Laporan Pendapatan KKN Mode Program Studi
                {if isset($smarty.get.fakultas)}
                    </br>FAKULTAS {$data_fakultas_one.NM_FAKULTAS}
                {/if}
            </th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>Nama Program Studi</th>
            <th>Jumlah Mahasiswa</th>
            <th>Jumlah Tarif</th>
            <th>Jumlah Angsuran</th>
            <th>Jumlah Tagihan</th>
            <th>Detail</th>
        </tr>
        {foreach $data_penw_prodi as $data}
            {$tagihan=$data.TARIF-$data.ANGSURAN}
            <tr class="ui-widget-content">
                <td>{$data@index+1}</td>
                <td class="center">
                    ({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}
                </td>
                <td class="center">
                    {$data.JUMLAH_MHS}
                </td>
                <td class="center">
                    {number_format($data.TARIF)}
                </td>
                <td class="center">
                    {number_format($data.ANGSURAN)}
                </td>
                <td class="center">
                    {number_format($tagihan)}
                </td>
                <td class="center">
                    <a class="ui-button ui-corner-all ui-state-hover" href="pendapatan-kkn.php?mode=detail&semester={$smarty.get.semester}&fakultas={$data.ID_FAKULTAS}&prodi={$data.ID_PROGRAM_STUDI}" style="padding:5px;cursor:pointer;">Detail</a>
                </td>
            </tr>
            {$total_mhs=$total_mhs+$data.JUMLAH_MHS}
            {$total_tarif=$total_tarif+$data.TARIF}
            {$total_angsuran=$total_angsuran+$data.ANGSURAN}
            {$total_tagihan=$total_tagihan+$tagihan}
        {/foreach}
        <tr class="total">
            <td class="center" colspan="2">TOTAL</td>
            <td class="center">{$total_mhs}</td>
            <td class="center">{number_format($total_tarif)}</td>
            <td class="center">{number_format($total_angsuran)}</td>
            <td class="center">{number_format($total_tagihan)}</td>
            <td class="center"><a class="ui-button ui-corner-all ui-state-hover" href="pendapatan-kkn.php?mode=detail&semester={$smarty.get.semester}&fakultas={$data.ID_FAKULTAS}" style="padding:5px;cursor:pointer;">Detail</a></td>
        </tr>
    </table>
{elseif isset($data_penw_detail)}
    <table class="ui-widget" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="10" class="header-coloumn">
                Laporan Pendapatan KKN Mode Detail<br/>
                {if isset($smarty.get.fakultas)}
                    FAKULTAS {$data_fakultas_one.NM_FAKULTAS}</br>
                {/if}
                {if isset($smarty.get.prodi)}
                    PROGRAM STUDI ({$data_prodi_one.NM_JENJANG}) {$data_prodi_one.NM_PROGRAM_STUDI}</br>
                {/if}
            </th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>NIM</th>
            <th>NAMA</th>
            <th>Angkatan</th>
            <th>Program Studi</th>
            <th>Fakultas</th>
            <th>Tarif</th>
            <th>Angsuran</th>
            <th>Tagihan</th>
            <th>Status</th>
        </tr>
        {$total_belum_bayar=0}
        {$total_lunas=0}
        {$total_tarif_kosong=0}
        {foreach $data_penw_detail as $data}
            {$tagihan=$data.TARIF-$data.ANGSURAN}
            <tr class="ui-widget-content" {if $tagihan>0&&$data.TARIF!=0} style="background-color: #ffcccc"{elseif $data.TARIF!=0} style="background-color: #c9f797"{else} style="background-color: #ffff99"{/if}>
                <td>{$data@index+1}</td>
                <td>{$data.NIM_MHS}</td>
                <td>{$data.NM_PENGGUNA}</td>
                <td>{$data.THN_ANGKATAN_MHS}</td>
                <td>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</td>
                <td>{$data.NM_FAKULTAS}</td>
                <td class="center">{number_format($data.TARIF)}</td>
                <td class="center">{number_format($data.ANGSURAN)}</td>
                <td class="center">{number_format($tagihan)}</td>
                <td class="center">
                    {if $tagihan>0&&$data.TARIF!=0}
                        {$total_belum_bayar=$total_belum_bayar+1}
                        Belum Lunas
                    {elseif $data.TARIF!=0}
                        {$total_lunas=$total_lunas+1}
                        Lunas
                     {/if}
                </td>
            </tr>
            {$total_mhs=$data@index+1}
            {$total_tarif=$total_tarif+$data.TARIF}
            {$total_angsuran=$total_angsuran+$data.ANGSURAN}
            {$total_tagihan=$total_tagihan+$tagihan}
        {foreachelse}
            <tr class="ui-widget-content">
                <td colspan="10" class="center" style="color: red">Data Kosong</td>
            </tr>
        {/foreach}
        <tr style="font-weight: bold" class="ui-widget-content">
            <td colspan="6" class="center">TOTAL</td>
            <td>{number_format($total_tarif)}</td>
            <td>{number_format($total_angsuran)}</td>
            <td>{number_format($total_tagihan)}</td>
            <td></td>
        </tr>
        <tr style="font-weight: bold" class="ui-widget-content">
            <td colspan="10" class="link center">
                Total Mahasiswa Lunas Pembayaran : <span style="color: darkgreen">{$total_lunas}</span><br/>
                Total Mahasiswa Belum Bayar/Lunas : <span style="color: red">{$total_belum_bayar}</span><br/>
            </td>
        </tr>
    </table>
{/if}
{if $smarty.get.mode!='fakultas'&&isset($smarty.get.mode)}
    <span class="ui-button ui-corner-all ui-state-hover" style="margin-top:10px;padding:5px;cursor:pointer;" onclick="history.back(-1)">Kembali</span>
{/if}