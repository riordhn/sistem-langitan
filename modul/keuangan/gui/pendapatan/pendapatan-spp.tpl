{if $smarty.get.mode==''||$smarty.get.mode=='tampil'}
    <div class="center_title_bar">Laporan Pendapatan SPP</div> 
    <form method="get" id="report_form" action="pendapatan-spp.php">
        <table class="ui-widget" style="width:90%;">
            <tr class="ui-widget-header">
                <th colspan="4" class="header-coloumn">Parameter</th>
            </tr>
            <tr class="ui-widget-content">
                <td width="15%">Fakultas</td>
                <td width="25%">
                    <select name="fakultas" id="fakultas">
                        <option value="">Semua</option>
                        {foreach $data_fakultas as $data}
                            <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                        {/foreach}
                    </select>
                </td>
                <td>Pendapatan Pada Semester</td>
                <td>
                    <select name="semester">
                        {foreach $data_semester as $semester}
                            <option value="{$semester['ID_SEMESTER']}" {if $semester.ID_SEMESTER==$smarty.get.semester} selected="true" {/if}>{$semester['NM_SEMESTER']}({$semester['TAHUN_AJARAN']})</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr class="center ui-widget-content">
                <td colspan="6">
                    <input type="hidden" name="mode" value="tampil" />
                    <input type="submit" name="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
                </td>
            </tr>

        </table>
    </form>
{/if}
{if $smarty.get.mode=='tampil'}
    <table border="0" style="width: 100%">
        <caption>Pendapatan SPP Mahasiswa</caption>
        <tbody valign="top" align="left" >
            <tr>
                <td style="border: none;width: 50%">
                    <table class="ui-widget-content ui-widget" style="width: 90%">
                        <tr class="ui-widget-header">
                            <th colspan="5"class="header-coloumn">LAPORAN PENDAPATAN SPP MAHASISWA</th>
                        </tr>
                        <tr class="ui-widget-header">
                            <th>No</th>
                            <th>
                                {if $smarty.get.fakultas!=''}
                                    PROGRAM STUDI
                                {else}
                                    FAKULTAS
                                {/if}
                            </th>
                            <th>Jumlah Pendapatan</th>
                            <th>Jumlah Pengembalian</th>
                            <th>Detail</th>
                        </tr>
                        {$total_pendapatan=0}
                        {$total_pengembalian=0}
                        {foreach $data_pendapatan_mala as $data}
                            {$total_pendapatan=$total_pendapatan+$data.PENDAPATAN_SPP}
                            {$total_pengembalian=$total_pengembalian+$data.PENGEMBALIAN_SPP}
                            <tr>
                                <td>{$data@index+1}</td>
                                <td>
                                    {if $smarty.get.fakultas!=''}
                                        {$data.NM_PROGRAM_STUDI}
                                    {else}
                                        {$data.NM_FAKULTAS}
                                    {/if}
                                </td>
                                <td class="center">{number_format($data.PENDAPATAN_SPP)}</td>
                                <td class="center">
                                    <a href="kontrol-pengembalian.php?semester={$smarty.get.semester}&fakultas={$data.ID_FAKULTAS}&prodi={$data.ID_PROGRAM_STUDI}&mode=tampil">{number_format($data.PENGEMBALIAN_SPP)}</a>
                                </td>
                                <td>
                                    <span class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="window.open('pendapatan-spp.php?mode=detail&semester={$smarty.get.semester}&fakultas={$data.ID_FAKULTAS}&prodi={$data.ID_PROGRAM_STUDI}','_blank')">
                                        Detail
                                    </span>
                                </td>
                            </tr>
                        {foreachelse}
                            <tr class="ui-widget-content">
                                <td colspan="5" class="center"><span style="color: red">Data Kosong</span></td>
                            </tr>
                        {/foreach}
                        <tr class="total">
                            <td class="center" colspan="2">TOTAL</td>
                            <td class="center">{number_format($total_pendapatan)}</td>
                            <td class="center">
                                {number_format($total_pengembalian)}
                            </td>
                            <td>
                                <span class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="window.open('pendapatan-spp.php?{$smarty.server.QUERY_STRING|replace:'tampil':'detail'}','_blank')">
                                    Detail
                                </span>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="border: none;width: 50%">

                </td>
            </tr>

        </tbody>
    </table>
    <table border="0" style="width: 100%">
        <caption>Pendapatan Diterima Di Muka</caption>
        <tbody valign="top" align="left" >
            <tr>
                <td style="border: none;width: 50%">
                    <table class="ui-widget-content ui-widget" style="width: 90%">
                        <tr class="ui-widget-header">
                            <th colspan="4"class="header-coloumn">LAPORAN PENDAPATAN TERIMA DI MUKA SPP MALA</th>
                        </tr>
                        <tr class="ui-widget-header">
                            <th>No</th>
                            <th>
                                {if $smarty.get.fakultas!=''}
                                    PROGRAM STUDI
                                {else}
                                    FAKULTAS
                                {/if}
                            </th>
                            <th>Jumlah Pendapatan Terima Di Muka</th>
                            <th>Detail</th>
                        </tr>
                        {$total_pendapatan_terima_muka=0}
                        {foreach $data_pendapatan_terima_muka_mala as $data}
                            {$total_pendapatan_terima_muka=$total_pendapatan_terima_muka+$data.PENDAPATAN_TERIMA_MUKA}
                            <tr>
                                <td>{$data@index+1}</td>
                                <td>
                                    {if $smarty.get.fakultas!=''}
                                        {$data.NM_PROGRAM_STUDI}
                                    {else}
                                        {$data.NM_FAKULTAS}
                                    {/if}
                                </td>
                                <td class="center">{number_format($data.PENDAPATAN_TERIMA_MUKA)}</td>
                                <td>
                                    <span class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="window.open('pendapatan-spp.php?mode=detail_terima_muka&semester={$smarty.get.semester}&fakultas={$data.ID_FAKULTAS}&prodi={$data.ID_PROGRAM_STUDI}&mhs=lama','_blank')">
                                        Detail
                                    </span>
                                </td>
                            </tr>
                        {foreachelse}
                            <tr class="ui-widget-content">
                                <td colspan="4" class="center"><span style="color: red">Data Kosong</span></td>
                            </tr>
                        {/foreach}
                        <tr class="total">
                            <td class="center" colspan="2">TOTAL</td>
                            <td class="center">{number_format($total_pendapatan_terima_muka)}</td>
                            <td>
                                <span class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="window.open('pendapatan-spp.php?{$smarty.server.QUERY_STRING|replace:'tampil':'detail_terima_muka'}&mhs=lama','_blank')">
                                    Detail
                                </span>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="border: none;width: 50%">
                    <table class="ui-widget-content ui-widget" style="width: 90%">
                        <tr class="ui-widget-header">
                            <th colspan="5"class="header-coloumn">LAPORAN PENDAPATAN TERIMA DI MUKA SPP MABA</th>
                        </tr>
                        <tr class="ui-widget-header">
                            <th>No</th>
                            <th>
                                {if $smarty.get.fakultas!=''}
                                    PROGRAM STUDI
                                {else}
                                    FAKULTAS
                                {/if}
                            </th>
                            <th>Jumlah Pendapatan Terim Di Muka</th>
                            <th>Detail</th>
                        </tr>
                        {$total_pendapatan_terima_muka=0}
                        {foreach $data_pendapatan_terima_muka_maba as $data}
                            {$total_pendapatan_terima_muka=$total_pendapatan_terima_muka+$data.PENDAPATAN_TERIMA_MUKA}
                            <tr>
                                <td>{$data@index+1}</td>
                                <td>
                                    {if $smarty.get.fakultas!=''}
                                        {$data.NM_PROGRAM_STUDI}
                                    {else}
                                        {$data.NM_FAKULTAS}
                                    {/if}
                                </td>
                                <td class="center">{number_format($data.PENDAPATAN_TERIMA_MUKA)}</td>
                                <td>
                                    <span class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="window.open('pendapatan-spp.php?mode=detail_terima_muka&semester={$smarty.get.semester}&fakultas={$data.ID_FAKULTAS}&prodi={$data.ID_PROGRAM_STUDI}&mhs=maba','_blank')">
                                        Detail
                                    </span>
                                </td>
                            </tr>
                        {foreachelse}
                            <tr class="ui-widget-content">
                                <td colspan="5" class="center"><span style="color: red">Data Kosong</span></td>
                            </tr>
                        {/foreach}
                        <tr class="total">
                            <td colspan="2" class="center">TOTAL</td>
                            <td class="center">{number_format($total_pendapatan_terima_muka)}</td>
                            <td>
                                <span class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="window.open('pendapatan-spp.php?{$smarty.server.QUERY_STRING|replace:'tampil':'detail_terima_muka'}&mhs=maba','_blank')">
                                    Detail
                                </span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
{elseif $smarty.get.mode=='detail'}
    {literal}
        <link rel="stylesheet" type="text/css" href="../../css/jquery-ui-custom-keu.css" />
        <link rel="stylesheet" type="text/css" href="../../css/keuangan-style.css" />
        <style>           
            .ui-widget-content a{
                text-decoration: none;
            }
            .ui-widget-content a:hover{
                color: #f09a14;
            }
        </style>
    {/literal}
    <table class="ui-widget" style="width:250%">
        <tr class="ui-widget-header">
            <th colspan="{if $smarty.get.fakultas !=''}{count($data_biaya)+8}{else} {count($data_biaya)+7}{/if}">
                Rekapitulasi Pendapatan Mahasiswa Lama
                <br/>
                SEMESTER {$data_semester_one.NM_SEMESTER} {$data_semester_one.TAHUN_AJARAN} 
                {if $smarty.get.fakultas!=''}
                    <br/>
                    Fakultas {$data_fakultas_one.NM_FAKULTAS}
                {/if}
                {if $smarty.get.prodi!=''}
                    <br/>
                    Program Studi ({$data_prodi_one.NM_JENJANG}) {$data_prodi_one.NM_PROGRAM_STUDI}
                    <br/>
                {/if}
            </th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>
                {if $smarty.get.prodi!=''}
                    Nama
                {else if $smarty.get.fakultas!=''}
                    Program Studi
                {else}
                    Fakultas
                {/if}
            </th>
            {if $smarty.get.prodi!=''}
                <th>NIM</th>            
                <th>Program Studi</th>
            {else}
                <th>Jumlah Mhs</th>
            {/if}
            {foreach $data_biaya as $data}
                <th>{$data.NM_BIAYA}</th>
            {/foreach}
            <th>Pembayaran</th>
            <th>Denda</th>
            <th>Pendapatan Pembayaran</th>
            <th>Pengembalian Pembayaran</th>
        </tr>
        {foreach $data_pendapatan_spp_mala as $data}
            <tr class="ui-widget-content">
                <td>{$data@index+1}</td>
                <td>
                    {if $smarty.get.prodi!=''}
                        {$data.NM_PENGGUNA}
                    {else if $smarty.get.fakultas!=''}
                        <a href="pendapatan-spp.php?mode=detail&semester={$smarty.get.semester}&fakultas={$smarty.get.fakultas}&prodi={$data.ID_PROGRAM_STUDI}">
                            {$data.NM_PROGRAM_STUDI}
                        </a>
                    {else}
                        <a href="pendapatan-spp.php?mode=detail&semester={$smarty.get.semester}&fakultas={$data.ID_FAKULTAS}&prodi={$smarty.get.prodi}">
                            {$data.NM_FAKULTAS}
                        </a>
                    {/if}    
                </td>
                {if $smarty.get.prodi!=''}
                    <td>{$data.NIM_MHS}</td>
                    <td class="center"> {$data.NM_PROGRAM_STUDI}</td>
                {else}
                    <td>{if isset($data.DATA_PEMBAYARAN[22].JUMLAH_MHS)}{$data.DATA_PEMBAYARAN[22].JUMLAH_MHS}{else}0{/if}</td>
                {/if}
                {$total_pembayaran_right=0}
                {$total_denda_right=0}
                {$total_pendapatan_right=0}
                {foreach $data.DATA_PEMBAYARAN as $data_pembayaran}
                    <td class="center">
                        {number_format($data_pembayaran.BESAR_BIAYA)}
                        {$total_pembayaran_right=$total_pembayaran_right+$data_pembayaran.BESAR_BIAYA}
                        {$total_denda_right=$total_denda_right+$data_pembayaran.DENDA_BIAYA}
                        {$total_pendapatan_right=$total_pembayaran_right+$total_denda_right}
                        {$total_pembayaran_bottom[$data_pembayaran@index]=$total_pembayaran_bottom[$data_pembayaran@index]+$data_pembayaran.BESAR_BIAYA}
                    </td>
                {/foreach}
                <td class="center total">{number_format($total_pembayaran_right)}</td>
                <td class="center total">{number_format($total_denda_right)}</td>
                <td class="center total">{number_format($total_pendapatan_right)}</td>
                <td class="center total">{number_format($data.DATA_PENGEMBALIAN)}</td>
                {$total_pengembalian=$total_pengembalian+$data.DATA_PENGEMBALIAN}
                {$total_pendapatan=$total_pendapatan+$total_pendapatan_right}
                {$total_mhs=$total_mhs+$data.DATA_PEMBAYARAN[22].JUMLAH_MHS}
            </tr>
        {/foreach}
        <tr class="center ui-widget-content">
            <td class="total" colspan="{if $smarty.get.prodi!=''}4{else}2{/if}">Jumlah</td>
            {if $smarty.get.prodi==''}
                <td class="total">{$total_mhs}</td>
            {/if}
            {foreach $data.DATA_PEMBAYARAN as $data_pembayaran}
                <td class="total">
                    {number_format($total_pembayaran_bottom[$data_pembayaran@index])}
                </td>
            {/foreach}
            <td class="total" colspan="2">TOTAL PENDAPATAN :</td>
            <td class="total">{number_format($total_pendapatan)}</td>
            <td class="total" >{number_format($total_pengembalian)}</td>
        </tr>
        <tr class="center ui-widget-content">
            <td class="total" colspan="{if $smarty.get.fakultas !=''}{count($data_biaya)+8}{else} {count($data_biaya)+7}{/if}">
                <span class="ui-button ui-corner-all ui-state-hover" style="margin-top:10px;padding:5px;cursor:pointer;" onclick="history.back(-1)">Kembali</span>
                <span class="ui-button ui-corner-all ui-state-hover" style="margin-top:10px;padding:5px;cursor:pointer;" onclick="window.close()">Close</span>
            </td>	
        </tr>
        <tr class="center ui-widget-content">
            <td class="link center" colspan="{if $smarty.get.fakultas !=''}{count($data_biaya)+8}{else} {count($data_biaya)+7}{/if}">
<!--                <span class="link_button ui-corner-all" onclick="window.location.href='excel-pendapatan-spp-fakultas.php?tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$smarty.get.fakultas}&prodi={$smarty.get.prodi}&bank={$bank}&jalur={$jalur}'" style="padding:5px;cursor:pointer;">Excel</span>-->
            </td>	
        </tr>
    </table>
{elseif $smarty.get.mode=='detail_terima_muka'}
    {literal}
        <link rel="stylesheet" type="text/css" href="../../css/jquery-ui-custom-keu.css" />
        <link rel="stylesheet" type="text/css" href="../../css/keuangan-style.css" />
        <style>           
            .ui-widget-content a{
                text-decoration: none;
            }
            .ui-widget-content a:hover{
                color: #f09a14;
            }
        </style>
    {/literal}
    <table class="ui-widget" style="width:250%">
        <tr class="ui-widget-header">
            <th colspan="{if $smarty.get.fakultas !=''}{count($data_biaya)+6}{else} {count($data_biaya)+5}{/if}">
                Rekapitulasi Pendapatan Diterima Di Muka Mahasiswa {if $smarty.get.mhs=='lama'}Lama{else}Baru{/if}
                <br/>
                SEMESTER {$data_semester_one.NM_SEMESTER} {$data_semester_one.TAHUN_AJARAN} 
                {if $smarty.get.fakultas!=''}
                    <br/>
                    Fakultas {$data_fakultas_one.NM_FAKULTAS}
                {/if}
                {if $smarty.get.prodi!=''}
                    <br/>
                    Program Studi ({$data_prodi_one.NM_JENJANG}) {$data_prodi_one.NM_PROGRAM_STUDI}
                    <br/>
                {/if}
            </th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>
                {if $smarty.get.prodi!=''}
                    Nama
                {else if $smarty.get.fakultas!=''}
                    Program Studi
                {else}
                    Fakultas
                {/if}
            </th>
            {if $smarty.get.prodi!=''}
                <th>
                    {if $smarty.get.mhs=='lama'}
                        NIM
                    {else}
                        NO Ujian
                    {/if}
                </th>            
                <th>Program Studi</th>
            {else}
                <th>Jumlah Mhs</th>
            {/if}
            {foreach $data_biaya as $data}
                <th>{$data.NM_BIAYA}</th>
            {/foreach}
            <th>Total Pembayaran</th>
        </tr>
        {foreach $data_pendapatan_terima_muka as $data}
            <tr class="ui-widget-content">
                <td>{$data@index+1}</td>
                <td>
                    {if $smarty.get.prodi!=''}
                        {$data.NM_PENGGUNA}
                    {else if $smarty.get.fakultas!=''}
                        <a href="pendapatan-spp.php?mode=detail_terima_muka&semester={$smarty.get.semester}&fakultas={$smarty.get.fakultas}&prodi={$data.ID_PROGRAM_STUDI}&mhs={$smarty.get.mhs}">
                            {$data.NM_PROGRAM_STUDI}
                        </a>
                    {else}
                        <a href="pendapatan-spp.php?mode=detail_terima_muka&semester={$smarty.get.semester}&fakultas={$data.ID_FAKULTAS}&prodi={$smarty.get.prodi}&mhs={$smarty.get.mhs}">
                            {$data.NM_FAKULTAS}
                        </a>
                    {/if}    
                </td>
                {if $smarty.get.prodi!=''}
                    <td>
                        {if $smarty.get.mhs=='lama'}
                            {$data.NIM_MHS}
                        {else}
                            {$data.NO_UJIAN}
                        {/if}
                    </td>
                    <td class="center"> {$data.NM_PROGRAM_STUDI}</td>
                {else}
                    <td>{if isset($data.DATA_PEMBAYARAN[22].JUMLAH_MHS)}{$data.DATA_PEMBAYARAN[22].JUMLAH_MHS}{else}0{/if}</td>
                {/if}
                {$total_pembayaran_right=0}
                {foreach $data.DATA_PEMBAYARAN as $data_pembayaran}
                    <td class="center">
                        {number_format($data_pembayaran.BESAR_BIAYA)}
                        {$total_pembayaran_right=$total_pembayaran_right+$data_pembayaran.BESAR_BIAYA}
                        {$total_pembayaran_bottom[$data_pembayaran@index]=$total_pembayaran_bottom[$data_pembayaran@index]+$data_pembayaran.BESAR_BIAYA}
                    </td>
                {/foreach}
                <td class="center total">{number_format($total_pembayaran_right)}</td>
                {$total_pendapatan=$total_pendapatan+$total_pembayaran_right}
                {$total_mhs=$total_mhs+$data.DATA_PEMBAYARAN[22].JUMLAH_MHS}
            </tr>
        {/foreach}
        <tr class="center ui-widget-content">
            <td class="total" colspan="{if $smarty.get.prodi!=''}4{else}2{/if}">Jumlah</td>
            {if $smarty.get.prodi==''}
                <td class="total">{$total_mhs}</td>
            {/if}
            {foreach $data.DATA_PEMBAYARAN as $data_pembayaran}
                <td class="total">
                    {number_format($total_pembayaran_bottom[$data_pembayaran@index])}
                </td>
            {/foreach}
            <td class="total">{number_format($total_pendapatan)}</td>
            </tr>
        <tr class="center ui-widget-content">
            <td class="total" colspan="{if $smarty.get.fakultas !=''}{count($data_biaya)+6}{else} {count($data_biaya)+5}{/if}">
                <span class="ui-button ui-corner-all ui-state-hover" style="margin-top:10px;padding:5px;cursor:pointer;" onclick="history.back(-1)">Kembali</span>
                <span class="ui-button ui-corner-all ui-state-hover" style="margin-top:10px;padding:5px;cursor:pointer;" onclick="window.close()">Close</span>
            </td>	
        </tr>
        <tr class="center ui-widget-content">
            <td class="link center" colspan="{if $smarty.get.fakultas !=''}{count($data_biaya)+6}{else} {count($data_biaya)+5}{/if}">
<!--                <span class="link_button ui-corner-all" onclick="window.location.href='excel-pendapatan-spp-fakultas.php?tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$smarty.get.fakultas}&prodi={$smarty.get.prodi}&bank={$bank}&jalur={$jalur}'" style="padding:5px;cursor:pointer;">Excel</span>-->
            </td>	
        </tr>
    </table>
{/if}
{literal}
    <script>
            
    </script>
{/literal}