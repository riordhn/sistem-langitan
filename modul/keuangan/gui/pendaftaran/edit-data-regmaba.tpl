<div class="center_title_bar">Verifikasi Keuangan</div> 
<form method="get" action="verifikasi-keuangan.php">
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn">INPUT</td>
        </tr>
        <tr class="ui-widget-content">
            <td>
                NO UJIAN : <input type="text" name="no_ujian" value="{$smarty.get.no_ujian}">
            </td>
            <td>
                <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" name="ok" value="Cari">
            </td>
    </table>
</form>
{if isset($smarty.get.no_ujian)}
    {if $cmb!=''}
        {$status_generate}
        {$status_bayar}
        {$status_regmaba}
        <form method="post" id="form_verifikasi_keuangan" action="verifikasi-keuangan.php?no_ujian={$smarty.get.no_ujian}" style="font-size: 14px">
            <table id="table-formulir" class="ui-widget-content" style="width: 90%">
                <tr class="ui-widget-header">
                    <th colspan="2" class="header-coloumn">Verifikasi Keuangan</th>
                </tr>
                <tr class="ui-widget-header">
                    <th colspan="2">Data Diri</th>
                </tr>
                <tr>
                    <td>Fakultas</td>
                    <td>{$cmb.NM_FAKULTAS}</td>
                </tr>
                <tr>
                    <td>Program Studi</td>
                    <td>{$cmb.NM_PROGRAM_STUDI}</td>
                </tr>
                <tr>
                    <td>No Ujian</td>
                    <td>{$cmb.NO_UJIAN}</td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td>{$cmb.NM_C_MHS}</td>
                </tr>
                <tr>
                    <td style="width: 35%">Tanggal Lahir</td>
                    <td style="width: 65%">
                        {$cmb.TGL_LAHIR|date_format:'%d %B %Y'}
                    </td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>
                        {$cmb.ALAMAT}
                    </td>
                </tr>
                <tr>
                    <td>Kota</td>
                    <td>

                        {$cmb.NM_KOTA}
                    </td>
                </tr>
                <tr>
                    <td>No Handphone</td>
                    <td>{$cmb.TELP}</td>
                </tr>
                <tr>
                    <td>No Telp Utama <br/><i style="font-size: 0.9em;color: green;font-family: Trebuchet MS">Yang Bisa Di Hubungi</i></td>
                    <td>{$cmb.TELP_UTAMA}</td>
                </tr>
                <tr>
                    <td>PIN Blackberry</td>
                    <td>
                        {$cmb.PIN_BB}
                    </td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>
                        {$cmb.EMAIL}
                    </td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>
                        {foreach $jenis_kelamin_set as $jk}
                            {if $cmb.JENIS_KELAMIN == $jk.JENIS_KELAMIN}
                                {$jk.NM_JENIS_KELAMIN}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr>
                    <td>Kewarganegaraan</td>
                    <td>
                        {foreach $kewarganegaraan_set as $kw}
                            {if $cmb.KEWARGANEGARAAN == $kw.ID_KEWARGANEGARAAN}
                                {$kw.NM_KEWARGANEGARAAN}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr id="wna-hide">
                    <td>Asal Negara</td>
                    <td>
                        {if $cmb.ID_KEBANGSAAN == 114}
                            {foreach $negara_indo_set as $n}
                                {if $n.ID_NEGARA == $cmb.ID_KEBANGSAAN}
                                    {$n.NM_NEGARA}
                                {/if}
                            {/foreach} 
                        {else}
                            {foreach $negara_asing_set as $n}
                                {if $n.ID_NEGARA == $cmb.ID_KEBANGSAAN}
                                    {$n.NM_NEGARA}
                                {/if}
                            {/foreach}
                        {/if}
                    </td>
                </tr>
                <tr>
                    <td>Agama</td>
                    <td>
                        {foreach $agama_set as $a}
                            {if $cmb.ID_AGAMA == $a.ID_AGAMA}
                                {$a.NM_AGAMA}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr>
                    <td>Sumber Biaya</td>
                    <td>
                        {foreach $sumber_biaya_set as $sb}
                            {if $cmb.SUMBER_BIAYA == $sb.ID_SUMBER_BIAYA}
                                {$sb.NM_SUMBER_BIAYA}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr>
                    <th colspan="2"  class="ui-widget-header">Data Pendidikan</th>
                </tr>
                <tr>
                    <td>Asal SMTA / MA</td>
                    <td>
                        {$cmb.NM_SEKOLAH_ASAL}
                    </td>
                </tr>
                <tr>
                    <td>Jurusan SMTA / MA</td>
                    <td>
                        {foreach $jurusan_sekolah_set as $js}
                            {if $cmb.JURUSAN_SEKOLAH == $js.ID_JURUSAN_SEKOLAH}
                                {$js.NM_JURUSAN_SEKOLAH}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr>
                    <td>No &amp; Tgl Ijazah SMTA</td>
                    <td> No Ijazah:
                        {$cmb.NO_IJAZAH}
                        <br/>
                        Tgl Ijazah: {$cmb.TGL_IJAZAH}
                    </td>
                </tr>
                <tr>
                    <td>Ijazah</td>
                    <td> Tahun :{$cmb.TAHUN_LULUS}
                        <br/>
                        Jumlah Mata Pelajaran :{$cmb.JUMLAH_PELAJARAN_IJAZAH}
                        <br/>
                        Jumlah Nilai Akhir : {$cmb.NILAI_IJAZAH}
                        * Bukan rata-rata
                    </td>
                </tr>
                <tr>
                    <td>UAN</td>
                    <td> Tahun : {$cmb.TAHUN_UAN}
                        <br/>
                        Jumlah Mata Pelajaran : {$cmb.JUMLAH_PELAJARAN_UAN}
                        <br/>
                        Jumlah Nilai UAN : {$cmb.NILAI_UAN}
                        * Bukan rata-rata</td>
                </tr>
                <tr class="ui-widget-header">
                    <th colspan="2" >Data Keluarga - Ayah</td>
                </tr>
                <tr>
                    <td>Nama Ayah</td>
                    <td>{$cmb.NAMA_AYAH}</td>
                </tr>
                <tr>
                    <td>Alamat Ayah</td>
                    <td>{$cmb.ALAMAT_AYAH}</td>
                </tr>
                <tr>
                    <td>Kota</td>
                    <td>
                        {$cmb.NM_KOTA_AYAH}
                    </td>
                </tr>
                <tr>
                    <td>Telp</td>
                    <td>{$cmb.TELP_AYAH}</td>
                </tr>
                <tr>
                    <td>Pendidikan Ayah</td>
                    <td>
                        {foreach $pendidikan_ortu_set as $po}
                            {if $cmb.PENDIDIKAN_AYAH == $po.ID_PENDIDIKAN_ORTU}
                                {$po.NM_PENDIDIKAN_ORTU}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr>
                    <td>Jenis Pekerjaan Ayah</td>
                    <td>
                        <select name="pekerjaan_ayah">
                            <option value=""></option>
                            {foreach $data_pekerjaan as $p}
                                <option value="{$p.ID_PEKERJAAN}" {if $cmb.PEKERJAAN_AYAH == $p.ID_PEKERJAAN}selected="selected"{/if}>{$p.NM_PEKERJAAN}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Instansi / Perusahaan Tempat Kerja</td>
                    <td>{$cmb.INSTANSI_AYAH}</td>
                </tr>
                <tr>
                    <td>Masa Kerja (jika ada)</td>
                    <td>{$cmb.MASA_KERJA_AYAH}</td>
                </tr>
                <tr class="ui-widget-header">
                    <th colspan="2" >Data Keluarga - Ibu</td>
                </tr>
                <tr>
                    <td>Nama Ibu </td>
                    <td>{$cmb.NAMA_IBU}</td>
                </tr>
                <tr>
                    <td>Alamat Ibu</td>
                    <td>{$cmb.ALAMAT_IBU}</td>
                </tr>
                <tr>
                    <td>Kota</td>
                    <td>
                        {$cmb.NM_KOTA_IBU}
                    </td>
                </tr>
                <tr>
                    <td>Telp</td>
                    <td>{$cmb.TELP_IBU}</td>
                </tr>
                <tr>
                    <td>Pendidikan Ibu</td>
                    <td>
                        {foreach $pendidikan_ortu_set as $po}
                            {if $cmb.PENDIDIKAN_IBU == $po.ID_PENDIDIKAN_ORTU}
                                {$po.NM_PENDIDIKAN_ORTU}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr>
                    <td>Jenis Pekerjaan Ibu</td>
                    <td>
                        <select name="pekerjaan_ibu">
                            <option value=""></option>
                            {foreach $data_pekerjaan as $p}
                                <option value="{$p.ID_PEKERJAAN}" {if $cmb.PEKERJAAN_IBU == $p.ID_PEKERJAAN}selected="selected"{/if}>{$p.NM_PEKERJAAN}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Instansi / Perusahaan Tempat Kerja</td>
                    <td>{$cmb.INSTANSI_IBU}</td>
                </tr>
                <tr>
                    <td>Masa Kerja (jika ada)</td>
                    <td>{$cmb.MASA_KERJA_IBU}</td>
                </tr>
                <tr class="ui-widget-header">
                    <th colspan="2" >Data Keluarga - Lain-Lain</td>
                </tr>	
                <tr>
                    <td>Skala Usaha/Pekerjaan</td>
                    <td>
                        {foreach $skala_pekerjaan_set as $sp}
                            {if $cmb.SKALA_PEKERJAAN_ORTU == $sp.SKALA_PEKERJAAN}
                                {$sp.NM_SKALA_PEKERJAAN}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr>
                    <td>Jumlah Kakak</td>
                    <td>{$cmb.JUMLAH_KAKAK}</td>
                </tr>
                <tr>
                    <td>Jumlah Adik</td>
                    <td>{$cmb.JUMLAH_ADIK}</td>
                </tr>
                <tr class="ui-widget-header">
                    <th colspan="2">Kediaman dan Kendaraan Keluarga</th>
                </tr>
                <tr>
                    <td>Kediaman Orang Tua</td>
                    <td>
                        <select name="kediaman_ortu">
                            {foreach $kediaman_ortu_set as $ko}
                                <option value="{$ko.KEDIAMAN_ORTU}" {if $cmb.KEDIAMAN_ORTU == $ko.KEDIAMAN_ORTU}selected="selected"{/if}>{$ko.NM_KEDIAMAN_ORTU}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Luas Tanah Kediaman</td>
                    <td>
                        <select name="luas_tanah">
                            {foreach $luas_tanah_set as $lt}
                                <option value="{$lt.LUAS_TANAH}" {if $cmb.LUAS_TANAH == $lt.LUAS_TANAH}selected="selected"{/if}>{$lt.NM_LUAS_TANAH}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Luas Bangunan Kediaman</td>
                    <td>
                        <select name="luas_bangunan">
                            {foreach $luas_bangunan_set as $lb}
                                <option value="{$lb.LUAS_BANGUNAN}" {if $cmb.LUAS_BANGUNAN == $lb.LUAS_BANGUNAN}selected="selected"{/if}>{$lb.NM_LUAS_BANGUNAN}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Nilai NJOP</td>
                    <td>
                        <select name="range_njop">
                            <option value=""></option>
                            {foreach $data_range_njop as $d}
                                {if $d.BATAS_BAWAH==''&&$d.BATAS_ATAS!=''}
                                    <option value="{$d.ID_RANGE_JUMLAH}" {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_NJOP}selected="true" {/if}> kurang dari/sama dengan {number_format($d.BATAS_ATAS)}</option>
                                {else if $d.BATAS_BAWAH!=''&&$d.BATAS_ATAS==''}
                                    <option value="{$d.ID_RANGE_JUMLAH}" {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_NJOP}selected="true" {/if}> lebih dari {number_format($d.BATAS_BAWAH)}</option>
                                {else}
                                    <option value="{$d.ID_RANGE_JUMLAH}" {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_NJOP}selected="true" {/if}>{number_format($d.BATAS_BAWAH)} - {number_format($d.BATAS_ATAS)}</option>
                                {/if}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Tagihan PBB</td>
                    <td>
                        <select name="range_pbb">
                            <option value=""></option>
                            {foreach $data_range_pbb as $d}
                                {if $d.BATAS_BAWAH==''&&$d.BATAS_ATAS!=''}
                                    <option value="{$d.ID_RANGE_JUMLAH}" {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_PBBB}selected="true" {/if}> kurang dari/sama dengan {number_format($d.BATAS_ATAS)}</option>
                                {else if $d.BATAS_BAWAH!=''&&$d.BATAS_ATAS==''}
                                    <option value="{$d.ID_RANGE_JUMLAH}" {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_PBB}selected="true" {/if}> lebih dari {number_format($d.BATAS_BAWAH)}</option>
                                {else}
                                    <option value="{$d.ID_RANGE_JUMLAH}" {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_PBB}selected="true" {/if}>{number_format($d.BATAS_BAWAH)} - {number_format($d.BATAS_ATAS)}</option>
                                {/if}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Daya Listrik</td>
                    <td>
                        <select name="listrik">
                            {foreach $listrik_set as $l}
                                <option value="{$l.LISTRIK}" {if $cmb.LISTRIK == $l.LISTRIK}selected="selected"{/if}>{$l.NM_LISTRIK}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Tagihan Listrik <br/><i style="font-size: 0.9em;color: green;font-family: Trebuchet MS">Tiap Bulan</i></td>
                    <td>
                        <select name="range_listrik">
                            <option value=""></option>
                            {foreach $data_range_listrik as $d}
                                {if $d.BATAS_BAWAH==''&&$d.BATAS_ATAS!=''}
                                    <option value="{$d.ID_RANGE_JUMLAH}" {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_LISTRIK}selected="true" {/if}> kurang dari/sama dengan {number_format($d.BATAS_ATAS)}</option>
                                {else if $d.BATAS_BAWAH!=''&&$d.BATAS_ATAS==''}
                                    <option value="{$d.ID_RANGE_JUMLAH}" {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_LISTRIK}selected="true" {/if}> lebih dari {number_format($d.BATAS_BAWAH)}</option>
                                {else}
                                    <option value="{$d.ID_RANGE_JUMLAH}" {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_LISTRIK}selected="true" {/if}>{number_format($d.BATAS_BAWAH)} - {number_format($d.BATAS_ATAS)}</option>
                                {/if}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Tagihan Air
                    <td>
                        <select name="range_air">
                            <option value=""></option>
                            {foreach $data_range_air as $d}
                                {if $d.BATAS_BAWAH==''&&$d.BATAS_ATAS!=''}
                                    <option value="{$d.ID_RANGE_JUMLAH}" {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_AIR}selected="true" {/if}> kurang dari/sama dengan {number_format($d.BATAS_ATAS)}</option>
                                {else if $d.BATAS_BAWAH!=''&&$d.BATAS_ATAS==''}
                                    <option value="{$d.ID_RANGE_JUMLAH}" {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_AIR}selected="true" {/if}> lebih dari {number_format($d.BATAS_BAWAH)}</option>
                                {else}
                                    <option value="{$d.ID_RANGE_JUMLAH}" {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_AIR}selected="true" {/if}>{number_format($d.BATAS_BAWAH)} - {number_format($d.BATAS_ATAS)}</option>
                                {/if}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Jumlah Kendaraan Bermotor R4</td>
                    <td>
                        <input type="text" name="kendaraan_r4" size="4" maxlength="4" value="{$cmb.KENDARAAN_R4}" />
                    </td>
                </tr>
                <tr>
                    <td>Sebutkan Nama/Merek Kendaraan R4 Sesuai Jumlah</td>
                    <td>
                        <input type="text" name="merek_kendaraan_r4" size="50" maxlength="50" value="{$cmb.MEREK_KENDARAAN_R4}" />
                    </td>
                </tr>
                <tr>
                    <td>Tahun Kendaraan R4<br/><i style="font-size: 0.9em;color: green;font-family: Trebuchet MS">Paling Terbaru</i></td>
                    <td><input type="text" name="tahun_kendaraan_r4" size="4" maxlength="4" value="{$cmb.TAHUN_KENDARAAN_R4}" /></td>
                </tr>
                <tr>
                    <td>Jumlah Kendaraan bermotor R2</td>
                    <td>
                        <input type="text" name="kendaraan_r2" size="4" maxlength="4" value="{$cmb.KENDARAAN_R2}" />
                    </td>
                </tr>
                <tr>
                    <td>Sebutkan Nama/Merek Kendaraan R2 Sesuai Jumlah</td>
                    <td>
                        <input type="text" name="merek_kendaraan_r2" size="50" maxlength="50" value="{$cmb.MEREK_KENDARAAN_R2}" />
                    </td>
                </tr>
                <tr>
                    <td>Tahun Kendaraan R2<br/><i style="font-size: 0.9em;color: green;font-family: Trebuchet MS">Paling Terbaru</i></td>
                    <td><input type="text" name="tahun_kendaraan_r2" size="4" maxlength="4" value="{$cmb.TAHUN_KENDARAAN_R2}" /></td>
                </tr>
                <tr>
                    <td>Jumlah Handphone</td>
                    <td><input type="text" name="jumlah_handphone" size="4" maxlength="4" value="{$cmb.JUMLAH_HANDPHONE}" /></td>
                </tr>
                <tr>
                    <td>Kepemilikan Kekayaan Lainya :</td>
                    <td>
                        <textarea name="kekayaan_lain" cols="75" rows="2" maxlength="256">{$cmb.KEKAYAAN_LAIN}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Jenis Transportasi Ke Kampus</td>
                    <td>
                        <select name="transportasi">
                            <option value=""></option>
                            <option value="Sepeda" {if $cmb.TRANSPORTASI=='Sepedah'||$cmb.TRANSPORTASI=='Sepeda'}selected="true"{/if}>Sepeda</option>
                            <option value="Motor" {if $cmb.TRANSPORTASI=='Motor'}selected="true"{/if}>Motor</option>
                            <option value="Mobil" {if $cmb.TRANSPORTASI=='Mobil'}selected="true"{/if}>Mobil</option>
                            <option value="Angkutan Umum" {if $cmb.TRANSPORTASI=='Angkutan Umum'}selected="true"{/if}>Angkutan Umum</option>
                            <option value="Jalan Kaki" {if $cmb.TRANSPORTASI=='Jalan Kaki'}selected="true"{/if}>Jalan Kaki</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Informasi lainnya, sebutkan :</td>
                    <td>
                        <textarea name="info_lain" cols="75" rows="2" maxlength="256">{$cmb.INFO_LAIN}</textarea>
                    </td>
                </tr>
                <tr>
                    <th colspan="2" class="ui-widget-header">Data Penghasilan Ayah</th>
                </tr>
                <tr>
                    <td>Gaji</td>
                    <td><input type="text" name="gaji_ayah" id="pendapatan-ayah-1" size="20" maxlength="20" value="{$cmb.GAJI_AYAH}" style="text-align:right" class="" /></td>
                </tr>
                <tr>
                    <td>Tunjangan Keluarga</td>
                    <td><input type="text" name="tunjangan_keluarga_ayah" id="pendapatan-ayah-2" size="20" maxlength="20" value="{$cmb.TUNJANGAN_KELUARGA_AYAH}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Tunjangan Jabatan / Golongan</td>
                    <td><input type="text" name="tunjangan_jabatan_ayah" id="pendapatan-ayah-3" size="20" maxlength="20" value="{$cmb.TUNJANGAN_JABATAN_AYAH}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Tunjangan Sertifikasi Guru / Dosen</td>
                    <td><input type="text" name="tunjangan_sertifikasi_ayah" id="pendapatan-ayah-4" size="20" maxlength="20" value="{$cmb.TUNJANGAN_SERTIFIKASI_AYAH}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Tunjangan Kehormatan</td>
                    <td><input type="text" name="tunjangan_kehormatan_ayah" id="pendapatan-ayah-5" size="20" maxlength="20" value="{$cmb.TUNJANGAN_KEHORMATAN_AYAH}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Remunerasi</td>
                    <td><input type="text" name="renumerasi_ayah" id="pendapatan-ayah-6" size="20" maxlength="20" value="{$cmb.RENUMERASI_AYAH}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Tunjangan lain-lain</td>
                    <td><input type="text" name="tunjangan_lain_ayah" id="pendapatan-ayah-7" size="20" maxlength="20" value="{$cmb.TUNJANGAN_LAIN_AYAH}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Penghasilan lain-lain</td>
                    <td><input type="text" name="penghasilan_lain_ayah" id="pendapatan-ayah-8" size="20" maxlength="20" value="{$cmb.PENGHASILAN_LAIN_AYAH}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <th colspan="2" class="ui-widget-header">Data Penghasilan Ibu </th>
                </tr>
                <tr>
                    <td>Gaji</td>
                    <td><input type="text" name="gaji_ibu" id="pendapatan-ibu-1" size="20" maxlength="20" value="{$cmb.GAJI_IBU}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Tunjangan Keluarga</td>
                    <td><input type="text" name="tunjangan_keluarga_ibu" id="pendapatan-ibu-2" size="20" maxlength="20" value="{$cmb.TUNJANGAN_KELUARGA_IBU}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Tunjangan Jabatan / Golongan</td>
                    <td><input type="text" name="tunjangan_jabatan_ibu" id="pendapatan-ibu-3" size="20" maxlength="20" value="{$cmb.TUNJANGAN_JABATAN_IBU}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Tunjangan Sertifikasi Guru / Dosen</td>
                    <td><input type="text" name="tunjangan_sertifikasi_ibu" id="pendapatan-ibu-4" size="20" maxlength="20" value="{$cmb.TUNJANGAN_SERTIFIKASI_IBU}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Tunjangan Kehormatan Guru Besar</td>
                    <td><input type="text" name="tunjangan_kehormatan_ibu" id="pendapatan-ibu-5" size="20" maxlength="20" value="{$cmb.TUNJANGAN_KEHORMATAN_IBU}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Remunerasi</td>
                    <td><input type="text" name="renumerasi_ibu" size="20" id="pendapatan-ibu-6" maxlength="20" value="{$cmb.RENUMERASI_IBU}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Tunjangan lain-lain</td>
                    <td><input type="text" name="tunjangan_lain_ibu" size="20" id="pendapatan-ibu-7" maxlength="20" value="{$cmb.TUNJANGAN_LAIN_IBU}" style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Penghasilan lain-lain</td>
                    <td><input type="text" name="penghasilan_lain_ibu" id="pendapatan-ibu-8" size="20" maxlength="20" value="{$cmb.PENGHASILAN_LAIN_IBU}"  style="text-align:right" class=""/></td>
                </tr>
                <tr class="ui-widget-header">
                    <th colspan="2">Total Penghasilan Orang Tua</th>
                </tr>
                <tr>
                    <td>Total Penghasilan Orang Tua</td>
                    <td>
                        <input type="text" id="total-pendapatan-ortu" class="right-align" value="Rp {number_format($cmb.TOTAL_PENDAPATAN_ORTU,0,',','.')}" readonly="readonly" style="background-color: #dedede"/>
                        <input type="hidden" name="total_pendapatan_ortu" value="{$cmb.TOTAL_PENDAPATAN_ORTU}" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="center">
                        <a class="ui-button ui-corner-all ui-state-hover" href="verifikasi-keuangan.php?no_ujian={$smarty.get.no_ujian}" style="padding:5px;cursor:pointer;" >Kembali</a>
                        <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:6px;cursor:pointer;" value="Simpan" />
                        <input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}"/>
                        <input type="hidden" name="mode" value="update-data"/>
                    </td>
                </tr>
            </table>
        </form>
    {else}
        {$error}
    {/if}
{/if}
{literal}
    <script type="text/javascript">
                                $.getScript('gui/pendaftaran/regmaba.js');
                                $('#biaya_kuliah').change(function() {
                                    $.ajax({
                                        type: 'post',
                                        url: 'get-master-biaya.php',
                                        data: 'biaya_kuliah=' + $(this).val(),
                                        success: function(data) {
                                            $('#master').text(data);
                                            $('#master_hidden').val(data);
                                        }
                                    });
                                });
                                $('#form_verifikasi_keuangan').validate({
                                    rules: {
                                        biaya_kuliah: {
                                            required: true
                                        },
                                        sp3_cmhs: {
                                            required: true
                                        }
                                    }
                                });
    </script>
{/literal}