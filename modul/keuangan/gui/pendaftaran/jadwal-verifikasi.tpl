<div class="center_title_bar">Jadwal Verifikasi Keuangan</div>
<table class="ui-widget-content" style="width: 60%">
    <tr class="ui-widget-header">
        <th class="header-coloumn" colspan="10">Jadwal Verifikasi </th>
    </tr>
    <tr class="ui-widget-header">
        <th>NO</th>
        <th>Nama Penerimaan</th>
        <th>Tanggal Verifikasi</th>
        <th>Status Maba</th>
        <th>Quota</th>
        <th>Terisi</th>
        <th>Sudah Verifikasi</th>
        <th>Belum Verifikasi</th>
        <th>Jam</th>
        <th>Operasi</th>
    </tr>
    {foreach $data_jadwal as $data}
        <tr>
            <td>{$data@index+1}</td>
            <td>{$data.NM_PENERIMAAN} Gelombang {$data.GELOMBANG} Semester {$data.SEMESTER} {$data.TAHUN}</td>
            <td>{$data.TGL_JADWAL}</td>
            <td>
                {if $data.STATUS_MABA==1}
                    Reguler
                {else}
                    Bidik Misi
                {/if}
            </td>
            <td>{$data.KUOTA}</td>
            <td>{$data.JUMLAH_MHS}</td>
            <td>{$data.SUDAH_VERIFIKASI}</td>
            <td>{$data.BELUM_VERIFIKASI}</td>
            <td>08.00 - 15.00</td>
            <td class="center">
                <span class="ui-button ui-corner-all ui-state-hover" onclick="$('#dialog-jadwal').dialog('open').load('jadwal-verifikasi.php?mode=edit&id={$data.ID_JADWAL}')" style="padding:5px;cursor:pointer;">Edit</span>
            </td>
        </tr>
    {foreachelse}
        <tr>
            <td colspan="10" class="data-kosong">Data Masih Kosong</td>
        </tr>
    {/foreach}
    <tr>
        <td colspan="10" class="center">
            <span class="ui-button ui-corner-all ui-state-hover" onclick="$('#dialog-jadwal').dialog('open').load('jadwal-verifikasi.php?mode=add')" style="padding:5px;cursor:pointer;">Tambah</span>
        </td>
    </tr>
</table>
<div id="dialog-jadwal" title="Jadwal Verifikasi"></div>
{literal}
    <script type="text/javascript">
        $('#dialog-jadwal').dialog({
            width:'65%',
            modal: true,
            resizable:false,
            autoOpen:false
        });
    </script>
{/literal}