var ajax_loading_html = '<div style="text-align: center"><img src="http://pendaftaran.unair.ac.id/img/ajax-loading.gif" /></div>';


function GetTotalPendapatanOrtu()
{
    var total_pendapatan = 0;

    $('input[id^="pendapatan-"]').each(function(index, item) {
        total_pendapatan += parseInt($(item).val());
    });

    if (isNaN(total_pendapatan))
        return 0;
    else
        return total_pendapatan;
}

$(document).ready(function() {



    /* --------------------------------------------------------------- PENGISIAN PENDAPATAN ORTU */

    $('input[id^="pendapatan-"]').each(function(index, item) {

        if ($(item).val() == '') {
            $(item).val(0);
        }

        $(item).keyup(function() {
            var total_pendapatan_ortu = GetTotalPendapatanOrtu();
            $('input[name="total_pendapatan_ortu"]').val(total_pendapatan_ortu);
            $('input[name="total_pendapatan_ortu"]').formatCurrency('#total-pendapatan-ortu', {symbol: 'Rp ', digitGroupSymbol: '.', roundToDecimalPlace: 0});
        });
    })
    /* --------------------------------------------------------------- */


});