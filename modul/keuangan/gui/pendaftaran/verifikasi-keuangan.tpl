<div class="center_title_bar">Verifikasi Keuangan</div> 
<form method="get" action="verifikasi-keuangan.php">
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn">INPUT</td>
        </tr>
        <tr class="ui-widget-content">
            <td>
                NO UJIAN : <input type="text" name="no_ujian" value="{$smarty.get.no_ujian}">
            </td>
            <td>
                <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" name="ok" value="Cari">
            </td>
    </table>
</form>
{if isset($smarty.get.no_ujian)}
    {if $cmb!=''}
        {$status_generate}
        {$status_bayar}
        {$status_regmaba}
        <form method="post" id="form_verifikasi_keuangan" action="verifikasi-keuangan.php?no_ujian={$smarty.get.no_ujian}" style="font-size: 14px">
            <table id="table-formulir" class="ui-widget-content" style="width: 90%">
                <tr class="ui-widget-header">
                    <th colspan="2" class="header-coloumn">Verifikasi Keuangan</th>
                </tr>
                <tr class="ui-widget-header">
                    <th colspan="2">Data Diri</th>
                </tr>
                <tr>
                    <td>Fakultas</td>
                    <td>{$cmb.NM_FAKULTAS}</td>
                </tr>
                <tr>
                    <td>Program Studi</td>
                    <td>{$cmb.NM_PROGRAM_STUDI}</td>
                </tr>
                <tr>
                    <td>No Ujian</td>
                    <td>{$cmb.NO_UJIAN}</td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td>{$cmb.NM_C_MHS}</td>
                </tr>
                <tr>
                    <td style="width: 35%">Tanggal Lahir</td>
                    <td style="width: 65%">
                        {$cmb.TGL_LAHIR|date_format:'%d %B %Y'}
                    </td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>
                        {$cmb.ALAMAT}
                    </td>
                </tr>
                <tr>
                    <td>Kota</td>
                    <td>

                        {$cmb.NM_KOTA}
                    </td>
                </tr>
                <tr>
                    <td>No Handphone</td>
                    <td>{$cmb.TELP}</td>
                </tr>
                <tr>
                    <td>No Telp Utama <br/><i style="font-size: 0.9em;color: green;font-family: Trebuchet MS">Yang Bisa Di Hubungi</i></td>
                    <td>{$cmb.TELP_UTAMA}</td>
                </tr>
                <tr>
                    <td>PIN Blackberry</td>
                    <td>
                        {$cmb.PIN_BB}
                    </td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>
                        {$cmb.EMAIL}
                    </td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>
                        {foreach $jenis_kelamin_set as $jk}
                            {if $cmb.JENIS_KELAMIN == $jk.JENIS_KELAMIN}
                                {$jk.NM_JENIS_KELAMIN}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr>
                    <td>Kewarganegaraan</td>
                    <td>
                        {foreach $kewarganegaraan_set as $kw}
                            {if $cmb.KEWARGANEGARAAN == $kw.ID_KEWARGANEGARAAN}
                                {$kw.NM_KEWARGANEGARAAN}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr id="wna-hide">
                    <td>Asal Negara</td>
                    <td>
                        {if $cmb.ID_KEBANGSAAN == 114}
                            {foreach $negara_indo_set as $n}
                                {if $n.ID_NEGARA == $cmb.ID_KEBANGSAAN}
                                    {$n.NM_NEGARA}
                                {/if}
                            {/foreach} 
                        {else}
                            {foreach $negara_asing_set as $n}
                                {if $n.ID_NEGARA == $cmb.ID_KEBANGSAAN}
                                    {$n.NM_NEGARA}
                                {/if}
                            {/foreach}
                        {/if}
                    </td>
                </tr>
                <tr>
                    <td>Agama</td>
                    <td>
                        {foreach $agama_set as $a}
                            {if $cmb.ID_AGAMA == $a.ID_AGAMA}
                                {$a.NM_AGAMA}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr>
                    <td>Sumber Biaya</td>
                    <td>
                        {foreach $sumber_biaya_set as $sb}
                            {if $cmb.SUMBER_BIAYA == $sb.ID_SUMBER_BIAYA}
                                {$sb.NM_SUMBER_BIAYA}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr>
                    <th colspan="2"  class="ui-widget-header">Data Pendidikan</th>
                </tr>
                <tr>
                    <td>Asal SMTA / MA</td>
                    <td>
                        {$cmb.NM_SEKOLAH_ASAL}
                    </td>
                </tr>
                <tr>
                    <td>Jurusan SMTA / MA</td>
                    <td>
                        {foreach $jurusan_sekolah_set as $js}
                            {if $cmb.JURUSAN_SEKOLAH == $js.ID_JURUSAN_SEKOLAH}
                                {$js.NM_JURUSAN_SEKOLAH}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr>
                    <td>No &amp; Tgl Ijazah SMTA</td>
                    <td> No Ijazah:
                        {$cmb.NO_IJAZAH}
                        <br/>
                        Tgl Ijazah: {$cmb.TGL_IJAZAH}
                    </td>
                </tr>
                <tr>
                    <td>Ijazah</td>
                    <td> Tahun :{$cmb.TAHUN_LULUS}
                        <br/>
                        Jumlah Mata Pelajaran :{$cmb.JUMLAH_PELAJARAN_IJAZAH}
                        <br/>
                        Jumlah Nilai Akhir : {$cmb.NILAI_IJAZAH}
                        * Bukan rata-rata
                    </td>
                </tr>
                <tr>
                    <td>UAN</td>
                    <td> Tahun : {$cmb.TAHUN_UAN}
                        <br/>
                        Jumlah Mata Pelajaran : {$cmb.JUMLAH_PELAJARAN_UAN}
                        <br/>
                        Jumlah Nilai UAN : {$cmb.NILAI_UAN}
                        * Bukan rata-rata</td>
                </tr>
                <tr class="ui-widget-header">
                    <th colspan="2" >Data Keluarga - Ayah</td>
                </tr>
                <tr>
                    <td>Nama Ayah</td>
                    <td>{$cmb.NAMA_AYAH}</td>
                </tr>
                <tr>
                    <td>Alamat Ayah</td>
                    <td>{$cmb.ALAMAT_AYAH}</td>
                </tr>
                <tr>
                    <td>Kota</td>
                    <td>
                        {$cmb.NM_KOTA_AYAH}
                    </td>
                </tr>
                <tr>
                    <td>Telp</td>
                    <td>{$cmb.TELP_AYAH}</td>
                </tr>
                <tr>
                    <td>Pendidikan Ayah</td>
                    <td>
                        {foreach $pendidikan_ortu_set as $po}
                            {if $cmb.PENDIDIKAN_AYAH == $po.ID_PENDIDIKAN_ORTU}
                                {$po.NM_PENDIDIKAN_ORTU}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr>
                    <td>Jenis Pekerjaan Ayah</td>
                    <td>
                        {foreach $data_pekerjaan as $p}
                            {if $cmb.PEKERJAAN_AYAH == $p.ID_PEKERJAAN}
                                {$p.NM_PEKERJAAN}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr>
                    <td>Instansi / Perusahaan Tempat Kerja</td>
                    <td>{$cmb.INSTANSI_AYAH}</td>
                </tr>
                <tr>
                    <td>Masa Kerja (jika ada)</td>
                    <td>{$cmb.MASA_KERJA_AYAH}</td>
                </tr>
                <tr class="ui-widget-header">
                    <th colspan="2" >Data Keluarga - Ibu</td>
                </tr>
                <tr>
                    <td>Nama Ibu </td>
                    <td>{$cmb.NAMA_IBU}</td>
                </tr>
                <tr>
                    <td>Alamat Ibu</td>
                    <td>{$cmb.ALAMAT_IBU}</td>
                </tr>
                <tr>
                    <td>Kota</td>
                    <td>
                        {$cmb.NM_KOTA_IBU}
                    </td>
                </tr>
                <tr>
                    <td>Telp</td>
                    <td>{$cmb.TELP_IBU}</td>
                </tr>
                <tr>
                    <td>Pendidikan Ibu</td>
                    <td>
                        {foreach $pendidikan_ortu_set as $po}
                            {if $cmb.PENDIDIKAN_IBU == $po.ID_PENDIDIKAN_ORTU}
                                {$po.NM_PENDIDIKAN_ORTU}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr>
                    <td>Jenis Pekerjaan Ibu</td>
                    <td>
                        {foreach $data_pekerjaan as $p}
                            {if $cmb.PEKERJAAN_IBU == $p.ID_PEKERJAAN}
                                {$p.NM_PEKERJAAN}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr>
                    <td>Instansi / Perusahaan Tempat Kerja</td>
                    <td>{$cmb.INSTANSI_IBU}</td>
                </tr>
                <tr>
                    <td>Masa Kerja (jika ada)</td>
                    <td>{$cmb.MASA_KERJA_IBU}</td>
                </tr>
                <tr class="ui-widget-header">
                    <th colspan="2" >Data Keluarga - Lain-Lain</td>
                </tr>	
                <tr>
                    <td>Skala Usaha/Pekerjaan</td>
                    <td>
                        {foreach $skala_pekerjaan_set as $sp}
                            {if $cmb.SKALA_PEKERJAAN_ORTU == $sp.SKALA_PEKERJAAN}
                                {$sp.NM_SKALA_PEKERJAAN}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr>
                    <td>Jumlah Kakak</td>
                    <td>{$cmb.JUMLAH_KAKAK}</td>
                </tr>
                <tr>
                    <td>Jumlah Adik</td>
                    <td>{$cmb.JUMLAH_ADIK}</td>
                </tr>
                <tr class="ui-widget-header">
                    <th colspan="2">Kediaman dan Kendaraan Keluarga</th>
                </tr>
                <tr>
                    <td>Kediaman Orang Tua</td>
                    <td>
                        {foreach $kediaman_ortu_set as $ko}
                            {if $cmb.KEDIAMAN_ORTU == $ko.KEDIAMAN_ORTU}
                                {$ko.NM_KEDIAMAN_ORTU}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr>
                    <td>Luas Tanah Kediaman</td>
                    <td>
                        {foreach $luas_tanah_set as $lt}
                            {if $cmb.LUAS_TANAH == $lt.LUAS_TANAH}
                                {$lt.NM_LUAS_TANAH}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr>
                    <td>Luas Bangunan Kediaman</td>
                    <td>
                        {foreach $luas_bangunan_set as $lb}
                            {if $cmb.LUAS_BANGUNAN == $lb.LUAS_BANGUNAN}
                                {$lb.NM_LUAS_BANGUNAN}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr>
                    <td>Nilai NJOP</td>
                    <td>
                        {foreach $data_range_njop as $d}
                            {if $d.BATAS_BAWAH==''&&$d.BATAS_ATAS!=''}
                                {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_NJOP}
                                    kurang dari/sama dengan {number_format($d.BATAS_ATAS)}
                                {/if}
                            {else if $d.BATAS_BAWAH!=''&&$d.BATAS_ATAS==''}
                                {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_NJOP}
                                    lebih dari {number_format($d.BATAS_BAWAH)} 
                                {/if}
                            {else}
                                {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_NJOP}
                                    {number_format($d.BATAS_BAWAH)} - {number_format($d.BATAS_ATAS)} 
                                {/if}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr>
                    <td>Tagihan PBB</td>
                    <td>
                        {foreach $data_range_pbb as $d}
                            {if $d.BATAS_BAWAH==''&&$d.BATAS_ATAS!=''}
                                {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_PBB}
                                    kurang dari/sama dengan {number_format($d.BATAS_ATAS)}
                                {/if}
                            {else if $d.BATAS_BAWAH!=''&&$d.BATAS_ATAS==''}
                                {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_PBB}
                                    lebih dari {number_format($d.BATAS_BAWAH)} 
                                {/if}
                            {else}
                                {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_PBB}
                                    {number_format($d.BATAS_BAWAH)} - {number_format($d.BATAS_ATAS)} 
                                {/if}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr>
                    <td>Daya Listrik</td>
                    <td>
                        {foreach $listrik_set as $l}
                            {if $cmb.LISTRIK == $l.LISTRIK}
                                {$l.NM_LISTRIK}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr>
                    <td>Tagihan Listrik <br/><i style="font-size: 0.9em;color: green;font-family: Trebuchet MS">Tiap Bulan</i></td>
                    <td>
                        {foreach $data_range_listrik as $d}
                            {if $d.BATAS_BAWAH==''&&$d.BATAS_ATAS!=''}
                                {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_LISTRIK}
                                    kurang dari/sama dengan {number_format($d.BATAS_ATAS)}
                                {/if}
                            {else if $d.BATAS_BAWAH!=''&&$d.BATAS_ATAS==''}
                                {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_LISTRIK}
                                    lebih dari {number_format($d.BATAS_BAWAH)} 
                                {/if}
                            {else}
                                {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_LISTRIK}
                                    {number_format($d.BATAS_BAWAH)} - {number_format($d.BATAS_ATAS)} 
                                {/if}
                            {/if}
                        {/foreach}    
                    </td>
                </tr>
                <tr>
                    <td>Tagihan Air<br/><i style="font-size: 0.9em;color: green;font-family: Trebuchet MS">Tiap Bulan</i></td>
                    <td>
                        {foreach $data_range_air as $d}
                            {if $d.BATAS_BAWAH==''&&$d.BATAS_ATAS!=''}
                                {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_AIR}
                                    kurang dari/sama dengan {number_format($d.BATAS_ATAS)}
                                {/if}
                            {else if $d.BATAS_BAWAH!=''&&$d.BATAS_ATAS==''}
                                {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_AIR}
                                    lebih dari {number_format($d.BATAS_BAWAH)} 
                                {/if}
                            {else}
                                {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_AIR}
                                    {number_format($d.BATAS_BAWAH)} - {number_format($d.BATAS_ATAS)} 
                                {/if}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr>
                    <td>Jumlah Kendaraan Bermotor R4</td>
                    <td>
                        {$cmb.KENDARAAN_R4}
                    </td>
                </tr>
                <tr>
                    <td>Sebutkan Nama/Merek Kendaraan R4 Sesuai Jumlah</td>
                    <td>
                        {$cmb.MEREK_KENDARAAN_R4}
                    </td>
                </tr>
                <tr>
                    <td>Tahun Kendaraan R4<br/><i style="font-size: 0.9em;color: green;font-family: Trebuchet MS">Paling Terbaru</i></td>
                    <td>{$cmb.TAHUN_KENDARAAN_R4}</td>
                </tr>
                <tr>
                    <td>Jumlah Kendaraan bermotor R2</td>
                    <td>
                        {$cmb.KENDARAAN_R2}
                    </td>
                </tr>
                <tr>
                    <td>Sebutkan Nama/Merek Kendaraan R2 Sesuai Jumlah</td>
                    <td>
                        {$cmb.MEREK_KENDARAAN_R2}
                    </td>
                </tr>
                <tr>
                    <td>Tahun Kendaraan R2<br/><i style="font-size: 0.9em;color: green;font-family: Trebuchet MS">Paling Terbaru</i></td>
                    <td>{$cmb.TAHUN_KENDARAAN_R2}</td>
                </tr>
                <tr>
                    <td>Jumlah Handphone</td>
                    <td>{$cmb.JUMLAH_HANDPHONE}</td>
                </tr>
                <tr>
                    <td>Kepemilikan Kekayaan Lainya :</td>
                    <td>
                        {$cmb.KEKAYAAN_LAIN}
                    </td>
                </tr>
                <tr>
                    <td>Jenis Transportasi Ke Kampus</td>
                    <td>
                        {if $cmb.TRANSPORTASI=='Sepedah'||$cmb.TRANSPORTASI=='Sepeda'}
                            Sepeda
                        {else if $cmb.TRANSPORTASI=='Motor'}
                            Motor
                        {else if $cmb.TRANSPORTASI=='Mobil'}
                            Mobil
                        {else if $cmb.TRANSPORTASI=='Angkutan Umum'}
                            Angkutan Umum
                        {else if $cmb.TRANSPORTASI=='Jalan Kaki'}
                            Jalan Kaki
                        {/if}
                    </td>
                </tr>
                <tr>
                    <td>Informasi lainnya, sebutkan :</td>
                    <td>
                        {$cmb.INFO_LAIN}
                    </td>
                </tr>
                <tr class="ui-widget-header">
                    <th colspan="2">Data Penghasilan Ayah</th>
                </tr>
                <tr>
                    <td>Gaji</td>
                    <td>{number_format($cmb.GAJI_AYAH,0,',','.')}</td>
                </tr>
                <tr>
                    <td>Tunjangan Keluarga</td>
                    <td>{number_format($cmb.TUNJANGAN_KELUARGA_AYAH,0,',','.')}</td>
                </tr>
                <tr>
                    <td>Tunjangan Jabatan / Golongan</td>
                    <td>{number_format($cmb.TUNJANGAN_JABATAN_AYAH,0,',','.')}</td>
                </tr>
                <tr>
                    <td>Tunjangan Sertifikasi Guru / Dosen</td>
                    <td>{number_format($cmb.TUNJANGAN_SERTIFIKASI_AYAH,0,',','.')}</td>
                </tr>
                <tr>
                    <td>Tunjangan Kehormatan</td>
                    <td>{number_format($cmb.TUNJANGAN_KEHORMATAN_AYAH,0,',','.')}</td>
                </tr>
                <tr>
                    <td>Remunerasi</td>
                    <td>{number_format($cmb.RENUMERASI_AYAH,0,',','.')}</td>
                </tr>
                <tr>
                    <td>Tunjangan lain-lain</td>
                    <td>{number_format($cmb.TUNJANGAN_LAIN_AYAH,0,',','.')}</td>
                </tr>
                <tr>
                    <td>Penghasilan lain-lain</td>
                    <td>{number_format($cmb.PENGHASILAN_LAIN_AYAH,0,',','.')}</td>
                </tr>
                <tr class="ui-widget-header">
                    <th colspan="2">Data Penghasilan Ibu </th>
                </tr>
                <tr>
                    <td>Gaji</td>
                    <td>{number_format($cmb.GAJI_IBU,0,',','.')}</td>
                </tr>
                <tr>
                    <td>Tunjangan Keluarga</td>
                    <td>{number_format($cmb.TUNJANGAN_KELUARGA_IBU,0,',','.')}</td>
                </tr>
                <tr>
                    <td>Tunjangan Jabatan / Golongan</td>
                    <td>{number_format($cmb.TUNJANGAN_JABATAN_IBU,0,',','.')}</td>
                </tr>
                <tr>
                    <td>Tunjangan Sertifikasi Guru / Dosen</td>
                    <td>{number_format($cmb.TUNJANGAN_SERTIFIKASI_IBU,0,',','.')}</td>
                </tr>
                <tr>
                    <td>Tunjangan Kehormatan Guru Besar</td>
                    <td>{number_format($cmb.TUNJANGAN_KEHORMATAN_IBU,0,',','.')}</td>
                </tr>
                <tr>
                    <td>Remunerasi</td>
                    <td>{number_format($cmb.RENUMERASI_IBU,0,',','.')}</td>
                </tr>
                <tr>
                    <td>Tunjangan lain-lain</td>
                    <td>{number_format($cmb.TUNJANGAN_LAIN_IBU,0,',','.')}</td>
                </tr>
                <tr>
                    <td>Penghasilan lain-lain</td>
                    <td>{number_format($cmb.PENGHASILAN_LAIN_IBU,0,',','.')}</td>
                </tr>
                <tr class="ui-widget-header">
                    <th colspan="2">Total Penghasilan Orang Tua</th>
                </tr>
                <tr>
                    <td>Total Penghasilan Orang Tua</td>
                    <td>
                        {number_format($cmb.TOTAL_PENDAPATAN_ORTU,0,',','.')}
                    </td>
                </tr>
                <tr>
                    <td>Range Penghasilan Ortu</td>
                    <td>
                        {foreach $data_range_penghasilan as $d}
                            {if $d.BATAS_BAWAH==''&&$d.BATAS_ATAS!=''}
                                {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_PENGHASILAN}
                                    kurang dari/sama dengan {number_format($d.BATAS_ATAS,0,',','.')}
                                {/if}
                            {else if $d.BATAS_BAWAH!=''&&$d.BATAS_ATAS==''}
                                {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_PENGHASILAN}
                                    lebih dari {number_format($d.BATAS_BAWAH,0,',','.')} 
                                {/if}
                            {else}
                                {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_PENGHASILAN}
                                    {number_format($d.BATAS_BAWAH,0,',','.')} - {number_format($d.BATAS_ATAS,0,',','.')} 
                                {/if}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                <tr class="ui-widget-header">
                    <th colspan="2">VERIFIKASI KEUANGAN</th>
                </tr>

                <tr>
                    <td>Status Bidik Misi</td>
                    <td>
                        {if $cmb.STATUS_BIDIK_MISI==1||$cmb.STATUS_BIDIK_MISI_BARU==1}
                            Bidik Misi
                        {else}
                            Bukan
                        {/if}
                    </td>
                </tr>
                <tr>
                    <td>Perhitungan Skor</td>
                    <td>
                        {$n_penghasilan=0.3*$s_penghasilan}
                        {$n_pekerjaan=0.3*$s_pekerjaan}
                        {$n_njop=0.075*$s_njop}
                        {$n_listrik=0.05*$s_listrik}
                        {$n_air=0.05*$s_air}
                        {$n_pbb=0.075*$s_pbb}
                        {$n_asset=0.15*$s_asset}
                        <i>Skor Penghasilan (30%) = {$s_penghasilan} > {$n_penghasilan}</i><br/>
                        <i>Skor Pekerjaan (30%) = {$s_pekerjaan} > {$n_pekerjaan}</i><br/>
                        <i>Skor Nilai NJOP (7.5%) = {$s_njop} > {$n_njop}</i><br/>
                        <i>Skor Tagihan Listrik (5%) = {$s_listrik} > {$n_listrik}</i><br/>
                        <i>Skor Tagihan Air (5%) = {$s_air} > {$n_air}</i><br/>
                        <i>Skor Tagihan PBB (7.5%) = {$s_pbb} > {$n_pbb}</i><br/>
                        <i>Skor Aset Kendaraan (15%) = {$s_asset} > {$n_asset}</i><br/>
                        <strong style="color: green">Total Skor = {$n_penghasilan+$n_pekerjaan+$n_njop+$n_listrik+$n_air+$n_pbb+$n_asset}</strong>
                    </td>
                </tr>
                 <tr>
                    <td>Pengelompokan berdasarkan Skor</td>
                    <td>
                        <i><strong>UKT I</strong> Total Skor antara 0 - 1</i><br/>
                        <i><strong>UKT II</strong> Total Skor antara 1 - 2</i><br/>
                        <i><strong>UKT III</strong> Total Skor antara 2 - 3</i><br/>
                        <i><strong>UKT IV</strong> Total Skor antara 3 - 4</i><br/>
                        <i><strong>UKT V</strong> Total Skor antara 4 - 5</i><br/>
                        <i><strong>UKT VI</strong> Total Skor lebih besar 5</i><br/>
                    </td>
                </tr>
                <tr>
                    <td>Kelompok Biaya Rekomendasi</td>
                    <td>{$biaya_kuliah_recom_cmhs.NM_KELOMPOK_BIAYA}</td>
                </tr>
                <tr>
                    <td>Kelompok Biaya Hasil Verifikasi</td>
                    <td>
                        {foreach $kelompok_biaya_pilihan as $biaya}
                            {if $biaya.ID_BIAYA_KULIAH==$biaya_kuliah_cmhs}
                                {$biaya.NM_KELOMPOK_BIAYA}
                            {/if}
                        {/foreach}
                    </td>
                </tr>
                {if ($status_bayar==''&&$status_regmaba=='')||($cmb.STATUS_BIDIK_MISI==1)}
                    <tr>
                        <td>Besar Biaya Biaya Rekomendasi</td>
                        <td>{number_format($biaya_kuliah_recom_cmhs.BESAR_BIAYA_KULIAH,0,',','.')}</td>
                    </tr>
                    <tr>
                        <td>Kelompok Biaya Hasil Verifikasi</td>
                        <td>
                            <select id="biaya_kuliah" name="biaya_kuliah">
                                <option value="">Pilih</option>
                                {foreach $kelompok_biaya_pilihan as $biaya}
                                    <option value="{$biaya.ID_BIAYA_KULIAH}" {if $biaya.ID_BIAYA_KULIAH==$biaya_kuliah_cmhs}selected="true"{/if}>{$biaya.NM_KELOMPOK_BIAYA}</option>
                                {/foreach}
                            </select>
                            <label class="error" for="biaya_kuliah" style="display: none"> Pilih Salah Satu Kelompok Biaya...</label>
                        </td>
                    </tr>
                    <tr>
                        <td>Besar Biaya Master Biaya Kuliah</td>
                        <td>
                            <span id="master" style="font-size: 14px;font-weight: bold">{number_format($ukt_cmhs)}</span>
                            <input type="hidden" id="master_hidden" name="master_hidden" value="{$ukt_cmhs}"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Ubah Besar Biaya Kuliah</td>
                        <td>
                            <input type="text" value="-" name="set_ukt_manual" />
                        </td>
                    </tr>
                    {*
                    <tr>
                    <td>Kesanggupan Membayar SP3</td>
                    <td>
                    <input type="text" id="sp3_cmhs" name="sp3_cmhs" value="{$sp3_cmhs}" />
                    <label class="error" id="error_cp3_cmhs" for="sp3_cmhs" style="display: none"> Harus Lebih Besar atau Sama dengan Biaya SP3 Master Biaya Kuliah...</label>
                    </td>
                    </tr>
                    *}
                    {if $no_invoice_cmhs!=''}
                        <tr>
                            <td>Nomer Invoice</td>
                            <td>{$no_invoice_cmhs}</td>
                        </tr>
                    {/if}
                    <tr>
                        <td colspan="2" class="center">
                            <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:6px;cursor:pointer;" value="Verifikasi" />
                            <input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}"/>
                            <input type="hidden" name="mode" value="verifikasi"/>
                            <a class="ui-button ui-corner-all ui-state-hover" href="verifikasi-keuangan.php?{$smarty.server.QUERY_STRING}&mode=edit" style="padding:5px;cursor:pointer;" >Edit Data</a>
                            <a class="disable-ajax ui-button ui-corner-all ui-state-hover" target="_blank" href="cetak-regmaba.php?cmhs={$cmb.ID_C_MHS}" style="padding:5px;cursor:pointer;" >Cetak Form Regmaba</a>
                            <a class="disable-ajax ui-button ui-corner-all ui-state-hover" target="_blank" href="list-berkas-regmaba.php?cmhs={$cmb.ID_C_MHS}" style="padding:5px;cursor:pointer;" >Lihat Semua Berkas</a>
                            {if $no_invoice_cmhs!= ''}
                                {* PERTANDA SUDAH NGISI *}
                                <span class="disable-ajax ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="window.open('cetak-verifikasi-keuangan.php?id_c_mhs={$cmb.ID_C_MHS}', '_blank');">Cetak Invoice Maba</span>
                                <span class="disable-ajax ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="window.open('cetak-verifikasi-keuangan-semua.php?id_c_mhs={$cmb.ID_C_MHS}', '_blank');">Cetak Invoice Semua</span>
                            {/if}
                        </td>
                    </tr>
                {else}
                    {if $status_bayar!=''}
                        <tr style="color: red">
                            <td>Status</td>
                            <td>Sudah Melakukan Pembayaran <br/><span class="disable-ajax ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="window.open('cetak-verifikasi-keuangan.php?id_c_mhs={$cmb.ID_C_MHS}', '_blank');">Cetak Invoice</span></td>
                        </tr>
                    {else if $status_regmaba!=''}
                        <tr style="color: red">
                            <td>Status</td>
                            <td>Mahasiswa Belum Melakukan Regmaba</td>
                        </tr>
                    {/if}
                {/if}
            </table>
        </form>
    {else}
        {$error}
    {/if}
{/if}
{literal}
    <script type="text/javascript">
                                    $('#biaya_kuliah').change(function() {
                                        $.ajax({
                                            type: 'post',
                                            url: 'get-master-biaya.php',
                                            data: 'biaya_kuliah=' + $(this).val(),
                                            success: function(data) {
                                                $('#master').text(data);
                                                $('#master_hidden').val(data);
                                            }
                                        });
                                    });
                                    $('#form_verifikasi_keuangan').validate({
                                        rules: {
                                            biaya_kuliah: {
                                                required: true
                                            },
                                            sp3_cmhs: {
                                                required: true
                                            }
                                        }
                                    });
    </script>
{/literal}