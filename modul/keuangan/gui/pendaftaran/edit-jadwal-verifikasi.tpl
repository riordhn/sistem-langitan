<form id="form_edit" method="post" action="jadwal-verifikasi.php">
    <table class="ui-widget-content" style="width: 60%">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn" style="text-align: center;">Edit Jadwal Verifikasi</th>
        </tr>
        <tr>
            <td style="width: 20%">Penerimaan</td>
            <td>
                <select name="penerimaan">
                    {foreach $penerimaan_set as $p}
                        <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                            {foreach $p.p_set as $p2}
                                <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $data_jadwal_one.ID_PENERIMAAN}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
            </td>
            <td>Tanggal Jadwal</td>
            <td><input type="text" class="date_pick" name="tanggal" value="{$data_jadwal_one.TGL_JADWAL}"/></td>

        </tr>
        <tr>
            <td>Status Maba</td>
            <td>
                <select name="status">
                    <option value="1" {if $data_jadwal_one.STATUS_MABA==1}selected="true"{/if}>Reguler</option>
                    <option value="2" {if $data_jadwal_one.STATUS_MABA==2}selected="true"{/if}>Bidik Misi</option>
                </select>
            </td>
            <td>Quota Peserta</td>
            <td>
                <input name="quota" type="text" value="{$data_jadwal_one.KUOTA}" class="required number"/>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="4" class="center">
                <input type="hidden" name="mode" value="edit"/>
                <input type="hidden" name="id_jadwal" value="{$data_jadwal_one.ID_JADWAL}"/>
                <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Update"/>
                <span class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="$('#dialog-jadwal').dialog('close')">Cancel</span>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('#form_edit').validate();
        $('.date_pick' ).datepicker({dateFormat:'dd-M-y',changeMonth: true,
                    changeYear: true});
        $('.date_pick').change(function(){
            $(this).val($(this).val().toUpperCase());
        });
        $('#form_edit').submit(function(){
            if($('#form_edit').valid()){
                $('#dialog-jadwal').dialog('close');
            }
            else
                return false;
        });
    </script>
{/literal}