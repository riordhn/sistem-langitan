<div class="center_title_bar">Laporan Penundaan Bayar Mahasiswa Baru</div>
<form method="get" id="report_form" action="penundaan-bayar.php">
    <table class="ui-widget" style="width:80%;">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td width="15%">Fakultas</td>
            <td width="25%">
                <select name="fakultas" id="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
            <td>Penerimaan</td>
            <td>
                <select name="penerimaan">
                    <option value="">Pilih Penerimaan</option>
                    {foreach $penerimaan_set as $p}
                        <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                            {foreach $p.p_set as $p2}
                                <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="center ui-widget-content">
            <td colspan="6">
                <input type="hidden" name="mode" value="laporan" />
                <input type="submit" name="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if isset($data_penundaan)}
    <table class="ui-widget-content" style="width: 98%">
        <tr class="ui-widget-header">
            <th colspan="9" class="header-coloumn">Laporan Penundaan Mahasiswa Baru</th>
        </tr>
        <tr class="ui-widget-header">
            <th>NO</th>
            <th>NO UJIAN</th>
            <th>NIM</th>
            <th>NAMA</th>
            <th>PRODI</th>
            <th>JALUR</th>
            <th>BESAR PENUNDAAN</th>
            <th>KETERANGAN</th>
            <th>GENERATE ULANG</th>
        </tr>
        {foreach $data_penundaan as $d}
            <tr>
                <td>{$d@index+1}</td>
                <td><a href="history-bayar.php?cari={$d.NO_UJIAN}">{$d.NO_UJIAN}</a></td>
                <td><a href="history-bayar.php?cari={$d.NIM_MHS}">{$d.NIM_MHS}</a></td>
                <td>{$d.NM_C_MHS}</td>
                <td>{$d.NM_JENJANG} {$d.NM_PROGRAM_STUDI}</td>
                <td>{$d.NM_JALUR}</td>
                <td>{number_format($d.TOTAL_TUNDA)}</td>
                <td>{$d.KETERANGAN}</td>
                <td class="center">
                    {if $d.ADA>0}
                        Sudah Ada Pembayaran
                    {else}
                        <form action="penundaan-bayar.php?{$smarty.server.QUERY_STRING}" method="post">
                            <input type="hidden" name="id_mhs" value="{$d.ID_MHS}"/>
                            <input type="hidden" name="id_c_mhs" value="{$d.ID_C_MHS}"/>
                            <input type="hidden" name="mode" value="gen_ulang"/>
                            <input type="submit" name="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Generate"/>
                        </form>
                    {/if}
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="9" class="data-kosong">Data Kosong</td>
            </tr>
        {/foreach}
    </table>
{/if}