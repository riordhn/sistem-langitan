<div class="center_title_bar">Pindah Biaya Profesi</div> 
<form method="get" id="report_form" action="pindah-biaya-profesi.php">
    <table class="ui-widget" style="width:80%">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Program Studi</td>
            <td>
                <select name="prodi" id="prodi">
                    {foreach $data_prodi as $data}
                        <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$smarty.get.prodi}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                </select>
            </td>
            <td>Semester</td>
            <td>
                <select name="semester">
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER==$smarty.get.semester}selected="true"{/if}>{$data.NM_SEMESTER} ( {$data.TAHUN_AJARAN} )</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="center ui-widget-content">
            <td colspan="4">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>

{if $data_mhs!=''}
    <form action="pindah-biaya-profesi.php?{$smarty.server.QUERY_STRING}" method="post">
        <table class="ui-widget-content" style="width: 98%">
            <tr class="ui-widget-header">
                <th colspan="9">DATA MAHASISWA PINDAH PROFESI</th>
            </tr>
            <tr class="ui-widget-header">
                <th>NO</th>
                <th>NAMA</th>
                <th>NIM LAMA</th>
                <th>PRODI LAMA</th>
                <th>NIM BARU</th>
                <th>PRODI BARU</th>
                <th>STATUS PEMBAYARAN LAMA</th>
                <th>STATUS PEMBAYARAN BARU</th>
                <th>OPERASI<br/><input type="checkbox" id="check_all"/></th>
            </tr>
            {foreach $data_mhs as $d}
                <tr
                    {if $d.ADA_L>0&&$d.SBL>0&&$d.SBB==0}
                        style="background-color: #eab8b8"
                    {/if}
                    >
                    <td>{$d@index+1}</td>
                    <td>{$d.NAMA_B}</td>
                    <td>{$d.NIM_L}</td>
                    <td>{$d.JENJANG_L} {$d.PRODI_L}</td>
                    <td>{$d.NIM_B}</td>
                    <td>{$d.JENJANG_B} {$d.PRODI_B}</td>
                    <td class="center">
                        {if $d.ADA_L>0}
                            {if $d.SBL>0}
                                Sudah Di Bayar
                            {else}
                                Belum Di Bayar
                            {/if}
                        {else}
                            Belum Ada Pembayaran
                        {/if}
                        <br/>
                        <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="history-bayar.php?cari={$d.NIM_L}">Detail</a>
                    </td>
                    <td class="center">
                        {if $d.ADA_B>0}
                            {if $d.SBB>0}
                                Sudah Di Bayar
                            {else}
                                Belum Di Bayar
                            {/if}
                        {else}
                            Belum Ada Pembayaran
                        {/if}
                        <br/>
                        <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="history-bayar.php?cari={$d.NIM_B}">Detail</a>
                    </td>
                    <td class="center">
                        {if $d.ADA_L>0&&$d.SBL>0}
                            {if $d.SBB>0}
                                Sudah Di Pindah
                            {else}
                                <input class="id_mhs" type="checkbox" name="id_mhs_l_{$d@index+1}" value="{$d.ID_L}"/>
                                <input type="hidden" name="id_mhs_b_{$d@index+1}" value="{$d.ID_B}"/>
                            {/if}
                        {else}
                            Pembayaran Kosong
                        {/if}
                    </td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="9" class="data-kosong">Data Masih Kosong</td>
                </tr>
            {/foreach}
            <tr>
                <td colspan="9" class="center">
                    <input type="hidden" name="mode" value="pindah"/>
                    <input type="hidden" name="jumlah_data" value="{count($data_mhs)}"/>
                    <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Pindah Semua"/>
                </td>
            </tr>
        </table>
    </form>
{/if}
{literal}
    <script>
        $('#check_all').click(function(){
            if($(this).is(':checked')){
                $('.id_mhs').attr('checked', true);
            }else{
                $('.id_mhs').attr('checked', false);
            }
        })
    </script>
{/literal}