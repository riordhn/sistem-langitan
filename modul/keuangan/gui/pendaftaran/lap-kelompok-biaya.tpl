{if $smarty.get.mode==''}
    <div class="center_title_bar">Laporan Verifikasi Data</div>
    <a href="lap-kelompok-biaya.php?mode=form" class="disable-ajax ui-button ui-corner-all ui-state-hover" target="_blank" style="padding:5px;margin: 3px;cursor:pointer;">Buka Halaman</a>
{else}
    {literal}
        <link rel="stylesheet" type="text/css" href="../../css/jquery-ui-custom-keu.css" />
        <link rel="stylesheet" type="text/css" href="../../css/keuangan-style.css" />
        <style>           
            .ui-widget-content a{
                text-decoration: none;
            }
            .ui-widget-content a:hover{
                color: #f09a14;
            }
        </style>
    {/literal}
    <form method="get" id="report_form" action="lap-kelompok-biaya.php">
        <table class="ui-widget" style="width:90%;">
            <tr class="ui-widget-header">
                <th colspan="4" class="header-coloumn">Parameter</th>
            </tr>
            <tr class="ui-widget-content">
                <td width="15%">Fakultas</td>
                <td width="25%">
                    <select name="fakultas" id="fakultas">
                        <option value="">Semua</option>
                        {foreach $data_fakultas as $data}
                            <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                        {/foreach}
                    </select>
                </td>
                <td>Penerimaan</td>
                <td>
                    <select name="penerimaan">
                        <option value="">Pilih Penerimaan</option>
                        {foreach $penerimaan_set as $p}
                            <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                                {foreach $p.p_set as $p2}
                                    <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                                {/foreach}
                            </optgroup>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr class="center ui-widget-content">
                <td colspan="6">
                    <input type="hidden" name="mode" value="laporan" />
                    <input type="submit" name="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
                </td>
            </tr>
        </table>
    </form>
    {if $smarty.get.mode=='laporan'}
        {if isset($data_laporan)}
            {if count($data_kelompok)!=0}
                <table class="ui-widget">
                    <tr class="ui-widget-header">
                        <th colspan="{3+(count($data_kelompok)*9)}" class="header-coloumn">Laporan Pengelompokan Biaya Calon Mahasiswa</th>
                    </tr>
                    <tr class="ui-widget-header">
                        <th rowspan="2">NO</th>
                        <th rowspan="2">FAKULTAS / PRODI</th>
                        <th rowspan="2">JUMLAH DITERIMA</th>
                            {foreach $data_kelompok as $kel}
                            <th colspan="9">{$kel.NM_KELOMPOK_BIAYA}</th>
                            {/foreach}
                    </tr>
                    <tr class="ui-widget-header">
                        {foreach $data_kelompok as $kel}
                            <th>BELUM BAYAR (SUDAH VERIFKASI)</th>
                            <th>BAYAR NORMAL</th>
                            <th>BAYAR LEBIH</th>
                            <th>SUDAH BAYAR</th>
                            <th>JUMLAH PENDAFTARAN</th>
                            <th>JUMLAH SOP</th>
                            <th>JUMLAH SP3</th>
                            <th>JUMLAH SUKARELA</th>
                            <th>PROSENTASE</th>
                            {/foreach}
                    </tr>
                    {foreach $data_laporan as $lap}
                        <tr  class="ui-widget-content">
                            <td>{$lap@index+1}</td>
                            <td>
                                {if $smarty.get.fakultas!=''}
                                    {$lap.NM_JENJANG} {$lap.NM_PROGRAM_STUDI}
                                {else}
                                    {$lap.NM_FAKULTAS}
                                {/if}
                            </td>
                            <td>{number_format($lap.JUMLAH_MHS)}</td>
                            {$total_terima=$total_terima+$lap.JUMLAH_MHS}
                            {foreach $lap.DATA_KOLOM as $kol}
                                <td>{number_format($kol.BELUM_BAYAR)}</td>
                                <td>{number_format($kol.BAYAR_NORMAL)}</td>
                                <td>{number_format($kol.BAYAR_LEBIH)}</td>
                                <td>{number_format($kol.SUDAH_BAYAR)}</td>
                                <td>{number_format($kol.PENDAFTARAN)}</td>
                                <td>{number_format($kol.SPP)}</td>
                                <td>{number_format($kol.SP3)}</td>
                                <td>{number_format($kol.SUMBANGAN)}</td>
                                <td>{round(((($kol.BELUM_BAYAR+$kol.SUDAH_BAYAR)/$lap.JUMLAH_MHS)*100),2)} %</td>
                                {$total_belum[$kol@index]=$total_belum[$kol@index]+$kol.BELUM_BAYAR}
                                {$total_normal[$kol@index]=$total_normal[$kol@index]+$kol.BAYAR_NORMAL}
                                {$total_lebih[$kol@index]=$total_lebih[$kol@index]+$kol.BAYAR_LEBIH}
                                {$total_sudah[$kol@index]=$total_sudah[$kol@index]+$kol.SUDAH_BAYAR}
                                {$total_pen[$kol@index]=$total_pen[$kol@index]+$kol.PENDAFTARAN}
                                {$total_sop[$kol@index]=$total_sop[$kol@index]+$kol.SPP}
                                {$total_sp3[$kol@index]=$total_sp3[$kol@index]+$kol.SP3}
                                {$total_sukarela[$kol@index]=$total_sukarela[$kol@index]+$kol.SUMBANGAN}
                            {/foreach}
                        </tr>
                    {/foreach}
                    <tr class="total">
                        <td rowspan="2" colspan="2" class="center">TOTAL</td>
                        <td rowspan="2">{$total_terima}</td>
                        {foreach $data_kelompok as $kel}
                            <td><a href="lap-kelompok-biaya.php?mode=detail&penerimaan={$smarty.get.penerimaan}&fakultas={$lap.ID_FAKULTAS}&kelompok={$kel.ID_KELOMPOK_BIAYA}&status=2">{$total_belum[$kel@index]}</a></td>
                            <td>{$total_normal[$kel@index]}</td>
                            <td><a href="lap-kelompok-biaya.php?mode=detail&penerimaan={$smarty.get.penerimaan}&fakultas={$lap.ID_FAKULTAS}&kelompok={$kel.ID_KELOMPOK_BIAYA}&status=1">{$total_lebih[$kel@index]}</a></td>
                            <td><a href="lap-kelompok-biaya.php?mode=detail&penerimaan={$smarty.get.penerimaan}&fakultas={$lap.ID_FAKULTAS}&kelompok={$kel.ID_KELOMPOK_BIAYA}&status=3">{$total_sudah[$kel@index]}</a></td>
                            <td>{number_format($total_pen[$kel@index])}</td>
                            <td>{number_format($total_sop[$kel@index])}</td>
                            <td>{number_format($total_sp3[$kel@index])}</td>
                            <td>{number_format($total_sukarela[$kel@index])}</td>
                            <td>{round((($total_belum[$kel@index]+$total_sudah[$kel@index])/$total_terima)*100)} %</td>
                        {/foreach}
                    </tr>
                    <tr class="total">
                        <td class="center" colspan="{count($data_kelompok)*9}">
                            TOTAL BELUM BAYAR (Sudah Verifikasi) :{array_sum($total_belum)}<br/>
                            TOTAL BAYAR NORMAL :{array_sum($total_normal)}<br/>
                            TOTAL BAYAR LEBIH :{array_sum($total_lebih)}<br/>
                            TOTAL SUDAH BAYAR  :{array_sum($total_sudah)}<br/>
                            TOTAL PENDAFTARAN :{number_format(array_sum($total_pen))}<br/>
                            TOTAL SOP :{number_format(array_sum($total_sop))}<br/>
                            TOTAL SP3 :{number_format(array_sum($total_sp3))}<br/>
                            TOTAL SUKARELA :{number_format(array_sum($total_sukarela))}
                        </td>
                    </tr>
                </table>
            {else}
                <div id='alert' class='ui-widget' style='margin:15px'>
                    <div class='ui-state-error ui-corner-all' style='padding: 5px;width:40%;'> 
                        <p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: .3em;'></span> 
                            Data Masih Belum Lengkap / Data masih Belum ada</p>
                    </div>
                </div>
            {/if}
        {/if}
        {literal}
            <script>
                $('#fakultas').change(function() {
                    $.ajax({
                        type: 'post',
                        url: 'getProdi.php',
                        data: 'id_fakultas=' + $(this).val(),
                        success: function(data) {
                            $('#prodi').html(data);
                        }
                    });
                });
            </script>
        {/literal}
    {else if $smarty.get.mode=='detail'}
        <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="history.back(-1)">Kembali</a>
        <table style="width: 90%" class="ui-widget">
            <tr class="ui-widget-header">
                <th colspan="9">
                    DETAIL DATA MAHASISWA PENERIMAAN {$penerimaan.NM_PENERIMAAN} {$penerimaan.TAHUN} <br/>
                    JALUR {$penerimaan.NM_JALUR} <br/>
                    JENJANG {$penerimaan.NM_JENJANG} <br/>
                    KELOMPOK BIAYA {$kelompok.NM_KELOMPOK_BIAYA}<br/>
                    STATUS {$status}
                </th>
            </tr>
            <tr class="ui-widget-header">
                <th>NO</th>
                <th>NO UJIAN</th>
                <th>NAMA</th>
                <th>PROGRAM STUDI</th>
                <th>FAKULTAS</th>
                <th>PENDAFTARAN</th>
                <th>SPP</th>
                <th>SP3</th>
                <th>SUMBANGAN</th>
            </tr>
            {foreach $data_laporan as $l}
                <tr class="ui-widget-content">
                    <td>{$l@index+1}</td>
                    <td>{$l.NO_UJIAN}</td>
                    <td>{$l.NM_C_MHS}</td>
                    <td>{$l.NM_JENJANG} {$l.NM_PROGRAM_STUDI}</td>
                    <td>{$l.NM_FAKULTAS}</td>
                    <td>{number_format($l.PENDAFTARAN)}</td>
                    <td>{number_format($l.SPP)}</td>
                    <td>{number_format($l.SP3)}</td>
                    <td>{number_format($l.SUMBANGAN)}</td>
                </tr>
            {foreachelse}
                <tr class="ui-widget-content">
                    <td colspan="8" class="data-kosong">Data Kosong</td>
                </tr>
            {/foreach}
        </table>
    {/if}
{/if}
