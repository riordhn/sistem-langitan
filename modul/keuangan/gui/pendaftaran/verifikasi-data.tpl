<div class="center_title_bar">Laporan Verifikasi Data</div>
<form method="get" id="report_form" action="verifikasi-data.php">
    <table class="ui-widget" style="width:80%;">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td width="15%">Fakultas</td>
            <td width="25%">
                <select name="fakultas" id="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
            <td>Penerimaan</td>
            <td>
                <select name="penerimaan">
                    <option value="">Pilih Penerimaan</option>
                    {foreach $penerimaan_set as $p}
                        <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                            {foreach $p.p_set as $p2}
                                {if ($p2.ID_JENJANG==1||$p2.ID_JENJANG==5)}
                                    <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                                {/if}
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="center ui-widget-content">
            <td colspan="6">
                <input type="hidden" name="mode" value="laporan" />
                <input type="submit" name="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if isset($data_laporan)}
    <table class="ui-widget-content" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="{count($data_kelompok)+count($data_kelompok_verifikasi)+11}" class="header-coloumn">Laporan Lapor Diri Calon Mahasiswa</th>
        </tr>
        <tr class="ui-widget-header">
            <th rowspan="2">NO</th>
            <th rowspan="2">
                {if $smarty.get.fakultas!=''}
                    PROGRAM STUDI
                {else}
                    FAKULTAS
                {/if}
            </th>
            <th rowspan="2">JUMLAH DITERIMA</th>
            <th rowspan="2">JUMLAH ISI REGMABA</th>
            <th colspan="{count($data_kelompok)}">PREDIKSI KELOMPOK BIAYA</th>
            <th colspan="{count($data_kelompok_verifikasi)}"> KELOMPOK BIAYA VERIFIKASI</th>
            <th rowspan="2">JUMLAH BELUM REGMABA</th>
            <th rowspan="2">JUMLAH BELUM VERIFIKASI (Sudah Regmaba)</th>
            <th rowspan="2">JUMLAH SUDAH VERIFIKASI (Bidik Misi)</th>
            <th rowspan="2">JUMLAH SUDAH VERIFIKASI (Bukan Bidik Misi)</th>
            <th rowspan="2">JUMLAH SUDAH VERIFIKASI (Belum Regmaba)</th>
            <th rowspan="2">JUMLAH SUDAH BAYAR</th>
        </tr>
        <tr class="ui-widget-header">
            {foreach $data_kelompok as $dk}
                <th>
                    {$dk.NM_KELOMPOK_BIAYA}
                </th>
            {foreachelse}
                <th>
                    Kosong
                </th>
            {/foreach}
            {foreach $data_kelompok_verifikasi as $dk}
                <th>
                    {$dk.NM_KELOMPOK_BIAYA}
                </th>

            {foreachelse}
                <th>
                    Kosong
                </th>
            {/foreach}
        </tr>
        {$diterima=0}
        {$regmaba=0}
        {$belum_regmaba=0}
        {$sudah_ver_belum_reg}
        {$sudah_ver_bidik=0}
        {$belum_verifikasi=0}
        {$sudah_ver_belum_reg}
        {$sudah_bayar=0}
        {foreach $data_laporan as $lap}
            <tr style="font-size: 13px">
                <td>{$lap@index+1}</td>
                <td>
                    {if $smarty.get.fakultas!=''}
                        {$lap.NM_JENJANG} {$lap.NM_PROGRAM_STUDI}
                    {else}
                        {$lap.NM_FAKULTAS}
                    {/if}
                </td>
                <td>{$lap.DITERIMA}</td>
                <td>{$lap.REGMABA}</td>
                {if $smarty.get.fakultas!=''}
                    {$index_key=3}
                {else}
                    {$index_key=2}
                {/if}
                {foreach $data_kelompok as $d}
                    {$kel_var=array_keys($lap)}
                    <td>
                        {$lap[$kel_var[$index_key]]}
                        {$jumlah[$index_key-1]=$jumlah[$index_key-1]+$lap[$kel_var[$index_key]]}
                        {$index_key=$index_key+1}
                    </td>
                {foreachelse}
                    <td>
                        -
                    </td>
                {/foreach}

                {if $smarty.get.fakultas!=''}
                    {$index_key_ver=3+count($data_kelompok)}
                {else}
                    {$index_key_ver=2+count($data_kelompok)}
                {/if}
                {foreach $data_kelompok_verifikasi as $d}
                    {$kel_var_ver=array_keys($lap)}
                    <td>
                        {$lap[$kel_var_ver[$index_key_ver]]}
                        {$jumlah_ver[$index_key_ver-1]=$jumlah_ver[$index_key_ver-1]+$lap[$kel_var_ver[$index_key_ver]]}
                        {$index_key_ver=$index_key_ver+1}
                    </td>
                {foreachelse}
                    <td>
                        -
                    </td>
                {/foreach}
                <td>{$lap.BELUM_REGMABA}</td>
                <td>{$lap.BELUM_VERIFIKASI}</td>
                <td>{$lap.SUDAH_VER_BIDIK_MISI}</td>
                <td>{$lap.SUDAH_VER_BUKAN_BIDIK_MISI}</td>
                <td>{$lap.SUDAH_VER_BELUM_REG}</td>
                <td>{$lap.SUDAH_BAYAR}</td>
            </tr>
            {$diterima=$diterima+$lap.DITERIMA}
            {$regmaba=$regmaba+$lap.REGMABA}
            {$belum_regmaba=$belum_regmaba+$lap.BELUM_REGMABA}
            {$sudah_ver_belum_reg=$sudah_ver_belum_reg+$lap.SUDAH_VER_BELUM_REG}
            {$belum_verifikasi=$belum_verifikasi+$lap.BELUM_VERIFIKASI}
            {$sudah_ver_bidik=$sudah_ver_bidik+$lap.SUDAH_VER_BIDIK_MISI}
            {$sudah_ver_bukan_bidik=$sudah_ver_bukan_bidik+$lap.SUDAH_VER_BUKAN_BIDIK_MISI}
            {$sudah_bayar=$sudah_bayar+$lap.SUDAH_BAYAR}
        {foreachelse}
            <tr>
                <td colspan="7" class="data-kosong">Data Kosong</td>
            </tr>
        {/foreach}
        <tr class="total">
            <td class="center" colspan="2">TOTAL</td>
            <td><a href="verifikasi-data.php?mode=detail&penerimaan={$smarty.get.penerimaan}&status=1">{$diterima}</a></td>
            <td>
                <a href="verifikasi-data.php?mode=detail&penerimaan={$smarty.get.penerimaan}&status=2">{$regmaba}</a>
            </td>
            {foreach $jumlah as $j}
                <td>
                    {$j}
                </td>
            {foreachelse}
                <td>
                    -
                </td>
            {/foreach}
            
            {foreach $jumlah_ver as $j}
                <td>
                    {$j}
                </td>
            {foreachelse}
                <td>
                    -
                </td>
            {/foreach}
            <td><a href="verifikasi-data.php?mode=detail&penerimaan={$smarty.get.penerimaan}&status=3">{$belum_regmaba}</a></td>
            <td><a href="verifikasi-data.php?mode=detail&penerimaan={$smarty.get.penerimaan}&status=4">{$belum_verifikasi}</a></td>
            <td><a href="verifikasi-data.php?mode=detail&penerimaan={$smarty.get.penerimaan}&status=5">{$sudah_ver_bidik}</td>
            <td><a href="verifikasi-data.php?mode=detail&penerimaan={$smarty.get.penerimaan}&status=6">{$sudah_ver_bukan_bidik}</a></td>
            <td><a href="verifikasi-data.php?mode=detail&penerimaan={$smarty.get.penerimaan}&status=7">{$sudah_ver_belum_reg}</a></td>
            <td><a href="verifikasi-data.php?mode=detail&penerimaan={$smarty.get.penerimaan}&status=8">{$sudah_bayar}</a></td>
        </tr>
    </table>
{else if isset($data_detail)}
    <table class="ui-widget-content" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="9">
                Data Detail Mahasiswa<br/>
                {if $smarty.get.status==1}
                    DITERIMA
                {else if $smarty.get.status==2}
                    SUDAH REGMABA
                {else if $smarty.get.status==3}
                    BELUM REGMABA
                {else if $smarty.get.status==4}
                    BELUM VERIFIKASI KEUANGAN SUDAH REGMABA
                {else if $smarty.get.status==5}
                    SUDAH VERIFIKASI (BIDIK MISI)
                {else if $smarty.get.status==6}
                    SUDAH VERIFIKASI (BUKAN BIDIK MISI)
                {else if $smarty.get.status==7}
                {else if $smarty.get.status==8}
                {/if}
            </th>
        </tr>
        <tr class="ui-widget-header">
            <th>NO</th>
            <th>NO UJIAN</th>
            <th>NAMA</th>
            <th>PROGRAM STUDI</th>
            <th>FAKULTAS</th>
            <th>TELP</th>
            <th>TANGGAL REGMABA</th>
            <th>TANGGAL VERIFIKASI KEUANGAN</th>
            <th>TANGGAL VERIFIKASI PENDIDIKAN</th>
        </tr>
        {foreach $data_detail as $dd}
            <tr>
                <td>{$dd@index+1}</td>
                <td>{$dd.NO_UJIAN}</td>
                <td>{$dd.NM_C_MHS}</td>
                <td>{$dd.NM_JENJANG} {$dd.NM_PROGRAM_STUDI}</td>
                <td>{$dd.NM_FAKULTAS}</td>
                <td>Utama :{$dd.TELP_UTAMA}<br/>Telp Lain : {$dd.TELP}</td>
                <td>{$dd.TGL_REGMABA}</td>
                <td>{$dd.TGL_VERIFIKASI_KEUANGAN}</td>
                <td>{$dd.TGL_VERIFIKASI_PENDIDIKAN}</td>
            </tr>
        {/foreach}
    </table>
    <a onclick="window.history.back()" class="disable-ajax ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;">Kembali</a>
{/if}
{literal}
    <script>
        $('#fakultas').change(function() {
            $.ajax({
                type: 'post',
                url: 'getProdi.php',
                data: 'id_fakultas=' + $(this).val(),
                success: function(data) {
                    $('#prodi').html(data);
                }
            })
        });
    </script>
{/literal}