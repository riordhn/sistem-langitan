<div class="center_title_bar">Setting Mahasiswa Pindah Jalur</div>
<form method="get" action="pindah-jalur.php">
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn">INPUT</td>
        </tr>
        <tr class="ui-widget-content">
            <td>
                NO UJIAN : <input type="text" name="no_ujian" value="{$smarty.get.no_ujian}">
            </td>
            <td>
                <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" name="ok" value="Cari">
            </td>
    </table>
</form>
{if isset($smarty.get.no_ujian)}
    {if $status==''}
        <table class="ui-widget-content">
            <caption>Data Biodata</caption>
            <tr class="ui-widget-header">
                <th>Kolom</th>
                <th>Data</th>
            </tr>
            <tr>
                <td>NAMA</td>
                <td>{$biodata.NM_C_MHS}</td>
            </tr>
            <tr>
                <td>NIM MAHASISWA LAMA</td>
                <td>{$biodata.NIM_MHS}</td>
            </tr>
            <tr>
                <td>NO UJIAN</td>
                <td>{$biodata.NO_UJIAN}</td>
            </tr>
            <tr>
                <td>PROGRAM STUDI BARU</td>
                <td>{$biodata.NM_JENJANG} {$biodata.NM_PROGRAM_STUDI}</td>
            </tr>
        </table>
        <table class="ui-widget-content" style="width: 60%">
            <caption>History Jalur</caption>
            <tr class="ui-widget-header">
                <th>NO</th>
                <th>JALUR</th>
                <th>PROGRAM STUDI</th>
                <th>SEMESTER</th>
                <th>STATUS AKTIF</th>
            </tr>
            {foreach $history as $h}
                <tr>
                    <td>{$h@index+1}</td>
                    <td>{$h.NM_JALUR}</td>
                    <td>{$h.NM_JENJANG} {$h.NM_PROGRAM_STUDI}</td>
                    <td>{$h.NM_SEMESTER} {$h.TAHUN_AJARAN}</td>
                    <td>
                        {if $h.ID_JALUR_AKTIF==1}
                            Aktif
                        {else}
                            Tidak Aktif
                        {/if}
                    </td>
                </tr>
            {/foreach}
            <tr>
                <td colspan="5" class="center total">
                    {if $cek_jalur>0}
                        <span class="ui-button ui-corner-all ui-state-active" style="padding:5px;font-size: 12px;font-weight: bold;color: red">Jalur Mahasiswa Sudah di Proses...</span>
                    {else}
                        <form action="pindah-jalur.php?{$smarty.server.QUERY_STRING}" method="post">
                            <input type="hidden" name="mode" value="update_jalur"/>
                            <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Update Jalur"/>
                        </form>
                    {/if}
                </td>
            </tr>
        </table>
        <table class="ui-widget-content" style="width: 60%">
            <caption>Data Pembayaran</caption>
            <tr class="ui-widget-header">
                <th>NO</th>
                <th>BIAYA</th>
                <th>BESAR BIAYA</th>
                <th>TANGGAL BAYAR</th>
                <th>NO TRANSAKSI</th>
                <th>BANK</th>
                <th>KETERANGAN</th>
            </tr>
            {$total_biaya=0}
            {foreach $pembayaran as $p}
                <tr>
                    <td>{$p@index+1}</td>
                    <td>{$p.NM_BIAYA}</td>
                    <td>{number_format($p.BESAR_BIAYA)}</td>
                    <td>{$p.TGL_BAYAR}</td>
                    <td>{$p.NO_TRANSAKSI}</td>
                    <td>{$p.NM_BANK} ({$p.NAMA_BANK_VIA})</td>
                    <td>{$p.KETERANGAN}</td>
                </tr>
                {$total_biaya=$total_biaya+$p.BESAR_BIAYA}
            {/foreach}
            <tr>
                <td colspan="7" class="center total">
                    TOTAL BIAYA : {number_format($total_biaya)}<br/><br/>
                    {if $cek_bayar>0}
                        <span class="ui-button ui-corner-all ui-state-active" style="padding:5px;font-size: 12px;font-weight: bold;color: red">Pembayaran Sudah Di pindahkan, mohon cek terlebih dahulu pembayaran yang ada...</span>
                    {else}
                        <form action="pindah-jalur.php?{$smarty.server.QUERY_STRING}" method="post">
                            <input type="hidden" name="mode" value="update_biaya"/>
                            <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Pindah Biaya"/>
                        </form>
                    {/if}
                </td>
            </tr>
        </table>
    {else}
        {$status}
    {/if}
{/if}