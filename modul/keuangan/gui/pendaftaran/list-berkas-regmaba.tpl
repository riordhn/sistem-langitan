<link rel="stylesheet" type="text/css" href="../../css/jquery-ui-custom-keu.css" />
<link rel="stylesheet" type="text/css" href="../../css/keuangan-style.css" />
<table class="ui-widget" width="968" style="margin: 0px auto;">
    <tr class="ui-widget-header">
        <th class="header-coloumn">LIST BERKAS MAHASISWA BARU</th>
    </tr>
    <tr class="ui-widget-header">
        <th class="header-coloumn">File Ijazah / Surat Keterangan Lulus</th>
    </tr>
    <tr>
        <td>
            <iframe src="view-berkas-regmaba.php?{$smarty.server.QUERY_STRING}&type=ijazah" width="968" height="500px"></iframe>
        </td>
    </tr>
    <tr class="ui-widget-header">
        <th class="header-coloumn">File SKHUN/ SKHUN Sementara</th>
    </tr>
    <tr>
        <td>
            <iframe src="view-berkas-regmaba.php?{$smarty.server.QUERY_STRING}&type=skhun" width="968" height="500px"></iframe>
        </td>
    </tr>
    <tr class="ui-widget-header">
        <th class="header-coloumn">File Akte Kelahiran</th>
    </tr>
    <tr>
        <td>
            <iframe src="view-berkas-regmaba.php?{$smarty.server.QUERY_STRING}&type=akte" width="968" height="500px"></iframe>
        </td>
    </tr>
    <tr class="ui-widget-header">
        <th class="header-coloumn">File KK</th>
    </tr>
    <tr>
        <td>
            <iframe src="view-berkas-regmaba.php?{$smarty.server.QUERY_STRING}&type=kk" width="968" height="500px"></iframe>
        </td>
    </tr>
    <tr class="ui-widget-header">
        <th class="header-coloumn">File Keterangan penghasilan bruto dan pekerjaan serta jabatan ortu</th>
    </tr>
    <tr>
        <td>
            <iframe src="view-berkas-regmaba.php?{$smarty.server.QUERY_STRING}&type=penghasilan" width="968" height="500px"></iframe>
        </td>
    </tr>
    <tr class="ui-widget-header">
        <th class="header-coloumn">File Akta Pendirian / SIUP bagi Wiraswasta/ Pengusaha</th>
    </tr>
    <tr>
        <td>
            <iframe src="view-berkas-regmaba.php?{$smarty.server.QUERY_STRING}&type=siup" width="968" height="500px"></iframe>
        </td>
    </tr>
    <tr class="ui-widget-header">
        <th class="header-coloumn">File Surat keterangan luas lahan petani</th>
    </tr>
    <tr>
        <td>
            <iframe src="view-berkas-regmaba.php?{$smarty.server.QUERY_STRING}&type=petani" width="968" height="500px"></iframe>
        </td>
    </tr>
    <tr class="ui-widget-header">
        <th class="header-coloumn">File SPPT PBB (NJOP) tahun terakhir</th>
    </tr>
    <tr>
        <td>
            <iframe src="view-berkas-regmaba.php?{$smarty.server.QUERY_STRING}&type=sppt_pbb" width="968" height="500px"></iframe>
        </td>
    </tr>
    <tr class="ui-widget-header">
        <th class="header-coloumn">File rekening listrik</th>
    </tr>
    <tr>
        <td>
            <iframe src="view-berkas-regmaba.php?{$smarty.server.QUERY_STRING}&type=listrik" width="968" height="500px"></iframe>
        </td>
    </tr>
    <tr class="ui-widget-header">
        <th class="header-coloumn">File rekening air</th>
    </tr>
    <tr>
        <td>
            <iframe src="view-berkas-regmaba.php?{$smarty.server.QUERY_STRING}&type=air" width="968" height="500px"></iframe>
        </td>
    </tr>
    <tr class="ui-widget-header">
        <th class="header-coloumn">File STNK Motor</th>
    </tr>
    <tr>
        <td>
            <iframe src="view-berkas-regmaba.php?{$smarty.server.QUERY_STRING}&type=stnk_motor" width="968" height="500px"></iframe>
        </td>
    </tr>
    <tr class="ui-widget-header">
        <th class="header-coloumn">File STNK Mobil</th>
    </tr>
    <tr>
        <td>
            <iframe src="view-berkas-regmaba.php?{$smarty.server.QUERY_STRING}&type=stnk_mobil" width="968" height="500px"></iframe>
        </td>
    </tr>
</table>