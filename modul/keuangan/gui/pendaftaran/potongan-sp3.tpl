<div class="center_title_bar">Verifikasi Keuangan (Potongan SP3)</div> 
<form method="get" action="potongan-sp3.php">
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn">INPUT</td>
        </tr>
        <tr class="ui-widget-content">
            <td>
                NO UJIAN : <input type="text" name="no_ujian" value="{$smarty.get.no_ujian}">
            </td>
            <td>
                <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" name="ok" value="Cari">
            </td>
    </table>
</form>
{if isset($smarty.get.no_ujian)}
    {if $cmb!=''}
        {$status_bayar}
        {$status_regmaba}
        <form method="post" id="form_potongan_sp3" action="potongan-sp3.php?no_ujian={$smarty.get.no_ujian}" style="font-size: 14px">
            <table  class="ui-widget-content" style="width: 90%">
                <tr class="ui-widget-header">
                    <th colspan="2" class="header-coloumn">Potongan Sp3</th>
                </tr>
                <tr class="ui-widget-header">
                    <th colspan="2">Data Diri</th>
                </tr>
                <tr>
                    <td style="width: 30%">Nama</td>
                    <td style="width: 70%">
                        {$cmb.NM_C_MHS}
                    </td>
                </tr>
                <tr>
                    <td>No Ujian</td>
                    <td>
                        {$smarty.get.no_ujian}
                    </td>
                </tr>
                <tr>
                    <td>Program Studi</td>
                    <td>
                        {$cmb.NM_JENJANG} {$cmb.NM_PROGRAM_STUDI}
                    </td>
                </tr>
                <tr>
                    <td>Tempat Lahir</td>
                    <td>
                        {$cmb.NM_KOTA_LAHIR}
                    </td>
                </tr>
                <tr>
                    <td>Tanggal Lahir</td>
                    <td>{$cmb.TGL_LAHIR|date_format:"%e-%B-%Y"|upper}</td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>
                        {$cmb.ALAMAT}
                    </td>
                </tr>
                <tr>
                    <td>Status Bidik Misi</td>
                    <td>
                        {if $cmb.STATUS_BIDIK_MISI==1||$cmb.STATUS_BIDIK_MISI_BARU==1}
                            Bidik Misi
                        {else}
                            Bukan
                        {/if}
                    </td>
                </tr>
                <tr>
                    <td>Kelompok Biaya Calon Mahasiswa</td>
                    <td>{$kelompok_biaya_cmhs}</td>
                </tr>
                {if $status_bayar==''&&$status_regmaba==''}
                    <tr>
                        <td>Besar Biaya Kuliah yang Akan Di Bayar</td>
                        <td>{number_format($biaya_kuliah_verifikasi)}</td>
                    </tr>
                    <tr>
                        <td rowspan="3">Besar SP3</td>
                        <td>                        
                            SP3 Sekarang : <span id="sp3_sekarang">{$sp3_sekarang}</span> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Potongan : <input type="text" class="required" id="potongan" size="4" maxlength="2" name="potongan" value="" /> %  
                        </td>
                    </tr>
                    <tr>
                        <td>
                            SP3 Hasil Potongan : <input type="text" name="sp3_potongan" id="sp3_potongan"/> <span class="data-kosong">(Boleh Langsung di isi Manual)</span>
                        </td>
                    </tr>
                    <tr>
                        <td>Nomer Invoice</td>
                        <td>{$no_invoice_cmhs}</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="center">
                            <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Simpan" />
                            <input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}"/>

                            <span class="disable-ajax ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="window.open('cetak-verifikasi-keuangan.php?id_c_mhs={$cmb.ID_C_MHS}','_blank')">Cetak</span>
                        </td>
                    </tr>
                {else}
                    {if $status_bayar!=''}
                        <tr style="color: red">
                            <td>Status</td>
                            <td>Sudah Melakukan Pembayaran</td>
                        </tr>
                    {else if $status_regmaba!=''}
                        <tr style="color: red">
                            <td>Status</td>
                            <td>Mahasiswa Belum Melakukan Regmaba</td>
                        </tr>
                    {/if}
                {/if}
            </table>
        </form>
    {else}
        {$error}
    {/if}
{/if}
{literal}
    <script type="text/javascript">
        $('#potongan').keyup(function(){
            sp3_sekarang=$('#sp3_sekarang').text().replace(',','');
            $('#sp3_potongan').val(sp3_sekarang-sp3_sekarang*($(this).val()/100));
        });
        $('#form_potongan_sp3').validate();
    </script>
{/literal}