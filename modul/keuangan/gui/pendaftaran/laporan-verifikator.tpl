<div class="center_title_bar">Rekapitulasi Verifikator Keuangan</div> 
<form method="get" id="report_form" action="laporan-verifikator.php">
    <table class="ui-widget" style="width:70%">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td width="15%">Tanggal Awal</td>
            <td width="15%"><input type="text" id="tgl_awal" class="datepicker"  name="tgl_awal" value="{$smarty.get.tgl_awal}" /></td>
            <td width="15%">Penerimaan</td>
            <td width="55%">
                <select name="penerimaan">
                    <option value="">Pilih Penerimaan</option>
                    {foreach $penerimaan_set as $p}
                        <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                            {foreach $p.p_set as $p2}
                                {if ($p2.ID_JENJANG==1)}
                                    <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                                {/if}
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Tanggal Akhir</td>
            <td><input type="text" id="tgl_akhir" class="datepicker" class="required" name="tgl_akhir"  value="{$smarty.get.tgl_akhir}"/></td>
            <td colspan="2"></td>
        </tr>
        </tr>
        <tr class="center ui-widget-content">
            <td colspan="4">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if isset($data_laporan)}
    <table style="width: 70%" class="ui-widget-content">
        <tr class="ui-widget-header">
            <th colspan="{count($data_kelompok)*2+4}">REKAPUTILASI VERIFIKATOR KEUANGAN</th>
        </tr>
        <tr class="ui-widget-header">
            <th style="width: 5%" rowspan="3">NO</th>
            <th rowspan="3">VERIFIKATOR KEUANGAN</th>
            <th colspan="{count($data_kelompok)*2+1}">JUMLAH MAHASISWA</th>
        </tr>
        <tr class="ui-widget-header">
            {foreach $data_kelompok as $k}
                <th colspan="2">{$k.NM_KELOMPOK_BIAYA}</th>
                {/foreach}
            <th rowspan="2">TOTAL</th>
        </tr>
        <tr class="ui-widget-header">
            {foreach $data_kelompok as $k}
                <th>BUKAN</th>
                <th>BIDIK MISI</th>
                {/foreach}
        </tr>
        {$total_cmhs=0}
        {foreach $data_laporan as $lap}
            <tr>
                <td>{$lap@index+1}</td>
                <td>{$lap.NM_PENGGUNA}</td>
                {$index_jumlah=1}
                {foreach $data_kelompok as $k}
                    {$col_name=$k.NM_KELOMPOK_BIAYA|replace:' ':'_'}
                    {$col_name_b=$col_name|cat:'_B'}
                    <td>{number_format($lap.$col_name)}</td>
                    <td>{number_format($lap.$col_name_b)}</td>
                    {$jumlah[$index_jumlah]=$jumlah[$index_jumlah]+$lap.$col_name}
                    {$jumlah_b[$index_jumlah]=$jumlah_b[$index_jumlah]+$lap.$col_name_b}
                    {$index_jumlah=$index_jumlah+1}
                {/foreach}
                <td class="center">{number_format($lap.JUMLAH_MHS)}</td>
            </tr>
            {$total_cmhs=$total_cmhs+$lap.JUMLAH_MHS}
        {/foreach}
        <tr class="total">
            <td colspan="2" class="center">TOTAL</td>
            {$index=1}
            {foreach $data_kelompok as $k}
                <td>{number_format($jumlah[$index])}</td>
                <td>{number_format($jumlah_b[$index])}</td>
                {$index=$index+1}
            {/foreach}
            <td class="center">{$total_cmhs}</td>
        </tr>
    </table>
{/if}

{literal}
    <script>
        $('#report_form').submit(function() {
            if ($('#tgl_awal').val() == '' || $('#tgl_akhir').val() == '') {
                $('#alert').fadeIn();
                return false;
            }
        });
        $('#report_form').validate();
        $(".datepicker").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true,
            changeYear: true});
    </script>
{/literal}