<link rel="stylesheet" type="text/css" href="../../css/jquery-ui-custom-keu.css" />
<link rel="stylesheet" type="text/css" href="../../css/keuangan-style.css" />
<script language="javascript" src="../../js/jquery-1.5.1.min.js"></script>
<script language="javascript" src="../../js/jquery.validate.js"></script>
<script language="javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
{literal}
    <style>
        label.error {
            color: red;
            font-size:11px;
        }
    </style>
{/literal}
<div id="upload_section">
    {if isset($sukses_simpan)}
        <div class="ui-state-highlight ui-corner-all" style="margin: 10px auto;padding: 10px;width:40%"> 
            <span class="ui-icon ui-icon-info" style="float: left;margin:0px 5px;">  </span>
            <p>Data Pembayaran Berhasil Di Simpan.<br/><br/>
                {$berhasil} <span style="color: green">Data Berhasil di rubah</span><br/>
                {$gagal} <span style="color: red">Data tidak di rubah</span></p>
        </div>
    {/if}
    <form id="form_upload" action="upload-data-pembayaran-2.php" method="post" enctype="multipart/form-data">
        <table class="ui-widget" width="70%" style="margin: 20px auto;">
            <tr class="ui-widget-header">
                <th colspan="2" class="header-coloumn">Upload Data Pembayaran Mahasiswa ( File Excel )</th>
            </tr>
            <tr class="ui-widget-content">

            <!--  150717 fth
                <td>Jenis Biaya yang ditambah</td>
                <td>
                    <select name="biaya">
                        {foreach $data_biaya as $data}
                            <option value="{$data['ID_BIAYA']}" {if $data['ID_BIAYA']==$smarty.post.biaya}selected="true"{/if}>{$data['NM_BIAYA']}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Semester</td>
                <td>
                    <select name="semester">
                        {foreach $data_semester as $data}
                            <option value="{$data['ID_SEMESTER']}" {if $data['ID_SEMESTER']==$smarty.post.semester}selected="true"{/if}>{$data['NM_SEMESTER']}({$data['TAHUN_AJARAN']})</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Mode Input</td>
                <td>
                    <input type="radio" value="0" name="mode_input" {if $smarty.post.mode_input==0}checked="true"{/if} onfocus="$('#hidden_input').hide()" />Tambah berdasarkan Master
                    <input type="radio" value="1" name="mode_input" {if $smarty.post.mode_input==1}checked="true"{/if} onfocus="$('#hidden_input').show()" />Tambah dengan nilai tertentu<br/>
                    <input type="radio" value="2" name="mode_input" {if $smarty.post.mode_input==2}checked="true"{/if} onfocus="$('#hidden_input').show()" />Tambah Di Luar Master<br/>
                    <div id="hidden_input" {if $smarty.post.mode_input==''||$smarty.post.mode_input=='0'}style="display: none"{/if}>
                        Besar Biaya : <input type="text" class="required" value="{$smarty.post.besar_biaya}" name="besar_biaya" />
                    </div>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Keterangan</td>
                <td><textarea name="keterangan" class="required" cols="30">{if isset($smarty.post.keterangan)}{$smarty.post.keterangan} {/if}</textarea> </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Tagihkan</td>
                <td>
                    <select name="is_tagih">
                        <option value="Y" {if $smarty.post.is_tagih=='Y'}selected="true"{/if}>Ya</option>
                        <option value="T" {if $smarty.post.is_tagih=='T'}selected="true"{/if}>Tidak</option>
                    </select>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Batas Pembayaran</td>
                <td>
                    <input type="text" style="text-transform: uppercase" name="batas_bayar" value="{if isset($smarty.post.batas_bayar)}{$smarty.post.batas_bayar} {/if}" class="date_pick" />
                </td>
            </tr>

            -->
            <tr class="ui-widget-content">
                <td>Upload File</td>
                <td><input type="file" name="file" id="file" class="required" /> </td>
            </tr>
            <tr class="ui-widget-content">
                <td colspan="2" class="center">
                    <input type="submit" name="excel" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Submit" />
                    <input type="hidden" name="mode" value="save_excel"/>
                    
                    <!-- 150717 fth -->
                    <input type="reset" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Reset" />
                    <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="excel-tambah-pembayaran-template.php">Download Template</a>
                    <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="upload_pembayaran_tambah.php">Reload</a>
                    

                </td>
            </tr>
        </table>
    </form>
    <div id="hasil_upload" {if $status_upload==''}style="display:none"{/if}>
        {if $status_upload==1}
            <div class="ui-state-highlight ui-corner-all" style="margin: 10px auto;padding: 10px;width:40%"> 
                <span class="ui-icon ui-icon-info" style="float: left;margin:0px 5px;">  </span>
                <p>Berhasil Upload File <br/><br/>{$status_message}</p>
            </div>
        {else if $status_upload==0}
            <div class="ui-state-error ui-corner-all" style="margin: 10px auto;padding: 10px;width:40%"> 
                <span class="ui-icon ui-icon-alert" style="float: left;margin:0px 5px;">  </span> 
                <p>Gagal Upload file <br/><br/>{$status_message}></p>
            </div>
        {/if}
    </div>
    {if isset($data_pembayaran_upload)}
        <table class="ui-widget" width="70%" style="margin: 20px auto;">
            <tr class="ui-widget-header">
                <th colspan="5" class="header-coloumn">Status Upload Data</th>
            </tr>
            <tr class="ui-widget-header">
                <th>No</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Status Biaya</th>
                <th>Status Mahasiswa</th>
            </tr>
            {$total_pembayaran_sukses=0}
            {$total_pembayaran_gagal=0}
            {foreach $data_pembayaran_upload as $data}
                <tr class="ui-widget-content" {if $data.STATUS_MAHASISWA =='Data Mahasiswa Tidak Ada'}style="background-color: #eab8b8 "{/if}>
                    <td>{$data@index+1}</td>
                    <td>{$data.NIM_MHS}</td>
                    <td>{if $data.STATUS_MAHASISWA !='Data Mahasiswa Tidak Ada'}{$data.NAMA}{else}Kosong{/if}</td>
                    <td style="font-size: 0.9em">
                        {$data.STATUS_BIAYA}
                    </td>
                    <td>{$data.STATUS_MAHASISWA}</td>
                </tr>
            {/foreach}
            <tr class="ui-widget-content total">
                <td colspan="5" class="center">
                    <span class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="confirm_save_data('upload_pembayaran_tambah.php')" >Save Data</span>
                </td>
            </tr>
        </table>
    {/if}
</div>
{literal}
    <script>
            $('#form_upload').validate({
                rules: {
                 tgl_bayar: {
                   required: true
                 }
                }
            });
            function confirm_save_data(url){
                var c=confirm("Silahkan Periksa Kembali Parameternya. Apakah anda yakin menyimpan data ini? ");
                if(c==true){
                    $.ajax({
                        url:url,
                        type:'post',
                        data:'mode=save_data',
                        success:function(data){
                            $('#upload_section').html(data);
                        }
                    })
                }
            }
            $(function() {
                    $( ".date_pick" ).datepicker({dateFormat:'dd-M-y'});
            });
    </script>
{/literal}