<div class="center_title_bar">Upload Data Pembayaran</div>
<table>
        <tr>
            <th colspan="2">Upload File Excel</th>
        </tr>
        <tr>
            <td>
				<iframe src="upload-data-pembayaran-iframe.php"></iframe>
            </td>
            <td>
                <label><a class="disable-ajax" href="includes/Template_Excel_Upload_Data_Pembayaran.xls">Download Template Excel / Format Data pembayaran</a></label>
				
				<p>Format susunan file excel :</p>
				<ul>
					<li>NIM : Nomer Induk Mahasiswa</li>
					<li>NAMA : Nama Mahasiswa</li>
					<li>PROGRAM_STUDI : Program Studi Mahasiswa</li>
					<li>TAHUN : Tahun Pembayaran</li>
					<li>NAMA_SEMESTER : Semester sekarang ( Ganjil / Genap</li>
					<li>TGL_BAYAR : Tanggal Pembayaran</li>
					<li>NAMA_BIAYA: Nama Pembayaran</li>
					<li>BESAR_BIAYA : Jumlah Pembayaran</li>
					
					
				</ul>
            </td>
			<td>
				
			</td>
        </tr>
</table>