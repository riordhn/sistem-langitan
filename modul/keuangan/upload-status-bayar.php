<?php

// Test CVS
include 'config.php';
include 'class/reader.php';
include 'class/list_data.class.php';
include 'class/upload.class.php';
include 'class/utility.class.php';
include 'class/master.class.php';

$list = new list_data($db);
$upload = new upload($db);
$utility = new utility($db);
$master = new master($db);

// ExcelFile($filename, $encoding);
$data = new Spreadsheet_Excel_Reader();
$status_message = '';

if (get('mode') == 'save_excel') {
    foreach ($_SESSION['data_status_bayar'] as $data) {
        if (count($upload->cek_mahasiswa($data['NIM_MHS'])) > 0) {
            if (!$data['STATUS_PEMBAYARAN']) {
                if (count($utility->load_sejarah_status_pembayaran($data['NIM_MHS'])) > 0) {
                    $utility->non_aktifkan_sejarah_status_bayar($data['ID_MHS']);
                }
                if ($_SESSION['biaya'] == '0') {
                    $utility->insert_sejarah_status_pembayaran($data['ID_MHS'], $_SESSION['semester'], $_SESSION['status'], $_SESSION['tgl_mulai'], $_SESSION['tgl_tempo']
                            , $_SESSION['keterangan'], $user->ID_PENGGUNA, 1);
                } else if ($_SESSION['biaya'] == '1') {
                    $utility->insert_sejarah_status_pembayaran_sop($data['ID_MHS'], $_SESSION['semester'], $_SESSION['status'], $_SESSION['tgl_mulai'], $_SESSION['tgl_tempo']
                            , $_SESSION['keterangan'], $user->ID_PENGGUNA, 1);
                }
            }
        }
    }
    $smarty->assign('sukses_upload', '1');
}

// Set output Encoding.
if (isset($_POST['excel'])) {
    $filename = $user->ID_PENGGUNA . '_' . date('dmy_H:i:s') . '_' . $_FILES["file"]["name"];
    if ($_FILES["file"]["error"] > 0) {
        $status_message .= "Error : " . $_FILES["file"]["error"] . "<br />";
        $status_upload = '0';
    } else {
        $status_message .= "Nama File : " . $filename . "<br />";
        $status_message .= "Type : " . $_FILES["file"]["type"] . "<br />";
        $status_message .= "Size : " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
        if (file_exists("excel/excel_status_pembayaran/" . $filename)) {
            $status_message .= 'Keterangan : ' . $filename . " Sudah Ada ";
            $status_upload = '0';
        } else {
            move_uploaded_file($_FILES["file"]["tmp_name"], "excel/excel_status_pembayaran/" . $filename);
            chmod("excel/excel_status_pembayaran/" . $filename, 0755);
            $status_upload = '1';
        }
    }
    if ($status_upload) {
        $data->setOutputEncoding('CP1251');
        $data->read("excel/excel_status_pembayaran/{$filename}");
        $data_status_bayar_upload = array();
        for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
            $nim = str_replace("'", "", trim($data->sheets[0]['cells'][$i][1]));
            $data_mhs = $upload->get_mahasiswa($nim);
            if (count($upload->cek_mahasiswa($nim)) > 0) {
                $status_mahasiswa = 'Data Mahasiswa Ada';
            } else {
                $status_mahasiswa = 'Data Mahasiswa Tidak Ada';
            }

            array_push($data_status_bayar_upload, array(
                'ID_MHS' => $data_mhs['ID_MHS'],
                'NIM_MHS' => $nim,
                'NAMA' => $data_mhs['NM_PENGGUNA'],
                'STATUS_MAHASISWA' => $status_mahasiswa,
                'STATUS_PEMBAYARAN' => $upload->cek_sudah_bayar_status($data_mhs['ID_MHS'], post('semester'))
            ));
        }
        $_SESSION['data_status_bayar'] = $data_status_bayar_upload;
        $_SESSION['semester'] = post('semester');
        $_SESSION['status'] = post('status');
        $_SESSION['tgl_mulai'] = post('tgl_mulai');
        $_SESSION['tgl_tempo'] = post('tgl_tempo');
        $_SESSION['keterangan'] = post('keterangan');
        $_SESSION['biaya'] = post('biaya');
        $smarty->assign('data_status_bayar', $_SESSION['data_status_bayar']);
    }
    $smarty->assign('status_upload', $status_upload);
    $smarty->assign('status_message', $status_message);
}

$smarty->assign('data_semester', $utility->get_semester_all());
$smarty->assign('data_status', $master->load_status_pembayaran());
$smarty->display('utility/upload-status-bayar.tpl');
?>
