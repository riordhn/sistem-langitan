<?php

// Test CVS
include 'config.php';
include 'class/reader.php';
include 'class/list_data.class.php';
include 'class/upload.class.php';
include 'class/rekon.class.php';


$list = new list_data($db);
$upload = new upload($db);
$rekon = new rekon($db, $user->ID_PENGGUNA);

// ExcelFile($filename, $encoding);
$data = new Spreadsheet_Excel_Reader();
$status_message = '';

if (get('mode') == 'save_excel') {
    foreach ($_SESSION['data_pembayaran'] as $data) {
        if ($data['STATUS_MAHASISWA'] == 'Data Mahasiswa Ada') {
            foreach ($data['DATA_BIAYA'] as $data_biaya) {
                if ($data_biaya['STATUS_BAYAR'] != 'Tetap') {
                    if ($data_biaya['STATUS_BAYAR'] == 'Insert') {
                        $upload->insert($data['ID_MHS'], $_SESSION['semester'], $upload->get_id_detail_biaya($data['NIM_MHS'], $data_biaya['ID_BIAYA'])
                                , $_SESSION['bank'], $_SESSION['bank_via'], $_SESSION['tgl_bayar'], $_SESSION['keterangan'], $data_biaya['BESAR_BIAYA']);
                        $cek_rekon = $rekon->cek_rekon($_SESSION['tgl_bayar']);
                        if ($cek_rekon != '') {
                            $id_mhs = $data['ID_MHS'];
                            $rekon->save_perubahan($cek_rekon['ID_REKON_PEMBAYARAN'], $id_mhs, $_SESSION['semester'], $_SESSION['tgl_bayar'], $_SESSION['bank']
                                    , $_SESSION['bank_via'], '', $_SESSION['keterangan'], 3);
                        }
                    } else {
                        $upload->update($_SESSION['bank'], $_SESSION['bank_via'], $_SESSION['tgl_bayar'], $_SESSION['keterangan'], $data_biaya['BESAR_BIAYA']
                                , $data['NIM_MHS'], $_SESSION['semester'], $data_biaya['ID_BIAYA']);
                        $cek_rekon = $rekon->cek_rekon($_SESSION['tgl_bayar']);
                        if ($cek_rekon != '') {
                            $id_mhs = $data['ID_MHS'];
                            $rekon->save_perubahan($cek_rekon['ID_REKON_PEMBAYARAN'], $id_mhs, $_SESSION['semester'], $_SESSION['tgl_bayar'], $_SESSION['bank']
                                    , $_SESSION['bank_via'], '', $_SESSION['keterangan'], 5, $data_biaya['BESAR_BIAYA']);
                        }
                    }
                }
            }
            //Update pembayara yang tidak ada di excel
            $upload->update_pembayaran_not_in_excel($_SESSION['bank'], $_SESSION['bank_via'], $_SESSION['tgl_bayar'], $_SESSION['keterangan']
                    , $data['NIM_MHS'], $_SESSION['semester'], $data['DATA_ID_BIAYA']);
        }
    }
    $smarty->assign('sukses_upload', '1');
}

// Set output Encoding.
if (isset($_POST['excel'])) {
    $filename = date('dmy_H_i_s') . '_' . $_FILES["file"]["name"];
    if ($_FILES["file"]["error"] > 0) {
        $status_message .= "Error : " . $_FILES["file"]["error"] . "<br />";
        $status_upload = '0';
    } else {
        $status_message .= "Nama File : " . $filename . "<br />";
        $status_message .= "Type : " . $_FILES["file"]["type"] . "<br />";
        $status_message .= "Size : " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
        if (file_exists("excel/excel_pembayaran/" . $filename)) {
            $status_message .= 'Keterangan : ' . $filename . " Sudah Ada ";
            $status_upload = '0';
        } else {
            move_uploaded_file($_FILES["file"]["tmp_name"], "excel/excel_pembayaran/" . $filename);
            chmod("excel/excel_pembayaran/" . $filename, 0755);
            $status_upload = '1';
        }
    }
    if ($status_upload) {
        $data->setOutputEncoding('CP1251');
        $data->read("excel/excel_pembayaran/{$filename}");
        $data_biaya = $list->load_biaya();
        $data_pembayaran_upload = array();
        for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
            $data_detail_biaya = array();
            $data_id_biaya = '';
            $nim = trim($data->sheets[0]['cells'][$i][1]);
            if (count($upload->cek_mahasiswa($nim)) > 0) {
                $status_mahasiswa = 'Data Mahasiswa Ada';
                $data_mhs = $upload->get_mahasiswa($nim);
            } else {
                $status_mahasiswa = 'Data Mahasiswa Tidak Ada';
            }
            for ($j = 3; $j < count($data_biaya) + 3; $j++) {
                if (isset($data->sheets[0]['cells'][$i][$j])) {
                    $status_bayar = $upload->cek_detail_biaya($nim, post('semester'), $data_biaya[$j - 3]['ID_BIAYA'], post('tgl_bayar'), post('bank'));
                    if ($status_bayar == 1 || $status_bayar == 0) {
                        $status_bayar_detail = 'Insert';
                    } else if ($status_bayar == 2) {
                        $status_bayar_detail = 'Upate';
                    } else {
                        $status_bayar_detail = 'Tetap';
                    }

                    array_push($data_detail_biaya, array(
                        'ID_BIAYA' => $data_biaya[$j - 3]['ID_BIAYA'],
                        'NM_BIAYA' => $data_biaya[$j - 3]['NM_BIAYA'],
                        'BESAR_BIAYA' => $data->sheets[0]['cells'][$i][$j],
                        'STATUS_BAYAR' => $status_bayar_detail
                    ));
                    $data_id_biaya.=$data_biaya[$j - 3]['ID_BIAYA'] . ',';
                }
            }
            array_push($data_pembayaran_upload, array(
                'ID_MHS' => $data_mhs['ID_MHS'],
                'NIM_MHS' => $nim,
                'NAMA' => $data->sheets[0]['cells'][$i][2],
                'DATA_BIAYA' => $data_detail_biaya,
                'DATA_ID_BIAYA' => substr_replace($data_id_biaya, '', -1),
                'STATUS_MAHASISWA' => $status_mahasiswa
            ));
        }
        $_SESSION['data_pembayaran'] = $data_pembayaran_upload;
        $_SESSION['semester'] = post('semester');
        $_SESSION['bank'] = post('bank');
        $_SESSION['bank_via'] = post('bank_via');
        $_SESSION['tgl_bayar'] = post('tgl_bayar');
        $_SESSION['keterangan'] = post('keterangan');
        $smarty->assign('data_pembayaran_upload', $_SESSION['data_pembayaran']);
    }
    $smarty->assign('status_upload', $status_upload);
    $smarty->assign('status_message', $status_message);
}

$smarty->assign('data_semester', $list->load_list_semester());
$smarty->assign('data_bank', $list->load_bank());
$smarty->assign('data_bank_via', $list->load_bank_via());
$smarty->display('utility/upload_pembayaran.tpl');
?>
