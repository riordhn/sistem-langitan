<?php
include('config.php');
include('../../tcpdf/config/lang/ind.php');
include('../../tcpdf/tcpdf.php');
include('class/history.class.php');
include('class/list_data.class.php');
include('class/utility.class.php');

$history = new history($db);
$list = new list_data($db);
$utility = new utility($db);

$id_tagihan_mhs = get('id_tagihan_mhs');
$id_semester = $db->QuerySingle("SELECT ID_SEMESTER FROM TAGIHAN_MHS WHERE ID_TAGIHAN_MHS='{$id_tagihan_mhs}'");
$semester = $list->get_semester($id_semester);
$biodata = $history->get_data_mhs(get('cari'), $id_pt_user);

$jabatan_ttd = $utility->find_konfigurasi_keu('KEU_TTD_JABATAN');
$pejabat_ttd = $utility->find_konfigurasi_keu('KEU_TTD_NAMA');

/*$biaya_tampil = '';
foreach ($history->get_biaya_rincian(get('cari')) as $biaya) {
    $biaya_tampil .="<th width='30px'>{$biaya['NM_BIAYA']}</th>";
}*/

$pembayaran_tampil = '';
$index = 1;

$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->AddPage();

$html = <<<EOF
<style>
    .keterangan {font-size: 7pt;font-weight:bold;}
    hr{
        margin:100px 0px;
    }
    table.data {border-collapse:collapse;}
    .data td{
        border: 1px solid black;
        vertical-align: baseline;
    }
    div { margin-top: 0pt; }
    .header { font-size: 18pt; font-family: times; font-weight: bold; text-align:center;}
    .address { font-size: 12pt; font-family: serif; margin-top: 0px ;text-align:center; }
    .address2 { font-size: 11pt; font-family: serif; margin-top: 0px ;text-align:center; }
    td { font-size: 8pt;}
    td.center { font-weight:bold;text-align:center }
    th { background-color:black;color:white;font-size: 7pt;text-align:center; }
    
</style>
<table width="100%" border="0">
    <tr>
        <td width="10%" align="right"><img src="../../img/akademik_images/logo-{$nama_singkat}.gif" width="80px" height="80px"/></td>
        <td width="88%" align="center">
            <span class="header">{$nama_pt_kapital}<br/></span>
            <span class="address">{$alamat},{$kota_pt}</span><br/>
            <span class="address2">Telp.{$telp_pt},Website: {$web_pt},email: {$web_email_pt}, pos: {$kd_pos_pt}</span><br/>
            <b style="font-size:13pt">BUKTI PEMBAYARAN </b>
        </td>
    </tr>
</table>
<hr/>
<table width="100%" border="0">
    <tr>
        <td width="100%" align="center">
            <h4></h4>
        </td>
    </tr>
</table>
<table class="data" width="50%" cellpadding="3">
                <tr>
                    <th colspan="2">DETAIL BIODATA</th>
                </tr>
                <tr>
                    <td>NIM</td> 
                    <td>{nim}</td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td>{nama}</td>
                </tr>
                <tr>
                    <td>Fakultas</td>
                    <td>{fakultas}</td>
                </tr>
                <tr>
                    <td>Program Studi</td>
                    <td>({jenjang}) {prodi}</td>
                </tr>
</table>
<table width="98%" border="0">
    <tr>
        <td width="98%" align="center">
            <h3><b>PEMBAYARAN {semester}</b> </h3>
        </td>
    </tr>
</table>

<table class="data" width="98%" cellpadding="3" >    
        <tr>
            <th style="text-align:center;" width="10%">No</th>
            <th style="text-align:left;" width="35%">Nama Biaya</th>
            <th style="text-align:center;" width="15%">Jumlah Tagihan</th>
            <th style="text-align:center;" width="15%">Jumlah Terbayar</th>
            <th style="text-align:center;" width="15%">Lunas</th>
        </tr>
    {data_pembayaran}
    <tr class="total">
        <td colspan="2">TOTAL > <b>Kekurangan : </b>{total_piutang}</td>
        <td style="text-align:right;">{total_tagihan}</td>
        <td style="text-align:right;">{total_terbayar}</td>
        <td style="text-align:right;">{penangguhan}</td>
    </tr>
</table>
    {ttd}
<span class="keterangan">Data Ini Di Cetak Pada Tanggal {tanggal}</span>

EOF;
$ttd = '
        <tr>
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr>
            <td width="70%" colspan="2"></td>
            <td width="30%" colspan="3" align="center">
                Surabaya, ' . strftime('%d %B %Y') . '<br/>
                Mengetahui<br/>
                {jabatan_ttd}
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                {pejabat_ttd}
                <br/>
            </td>
        </tr>';
$total_terbayar = 0;
$total_tagihan = 0;
$total_piutang = 0;
$data_tagihan_biaya = $history->load_riwayat_pembayaran_biaya(get('id_tagihan_mhs'));
foreach ($data_tagihan_biaya as $d) {
    $arr = [
        'STATUS_PEMBAYARAN'=>$d['STATUS_PEMBAYARAN'],
        'NAMA_BIAYA' => $d['NM_BIAYA'],
        'BESAR_TAGIHAN' => $d['BESAR_BIAYA'],
        'BESAR_TERBAYAR' => $d['TOTAL_TERBAYAR'],
        'IS_LUNAS' => $d['IS_LUNAS'],
    ];
    $data_riwayat_transaksi_pembayaran[] = $arr;
}
$index = 1;
foreach ($data_riwayat_transaksi_pembayaran as $data) {
    if ($data['STATUS_PEMBAYARAN'] == 3) {
        $penangguhan_str = '<br/>
            <b style="font-size:1.2em;color:orange">Ditangguhkan</b>';
    } else {
        $penangguhan_str = '';
    }
    $data_pembayaran_str .= '
    <tr>
        <td style="text-align:center;">' . $index++ . '</td>
        <td style="text-align:center;">' . $data['NAMA_BIAYA'] . '</td>
        <td style="text-align:right;">' . number_format($data['BESAR_TAGIHAN']) . '</td>
        <td style="text-align:right;">' . number_format($data['BESAR_TERBAYAR']) . '</td>
        <td style="text-align:center;">' . ($data['IS_LUNAS'] == '1' ? '<b style="font-size:1.2em;color:green">Sudah</b>' : '<b style="font-size:1.2em;color:red">Belum</b>') . '</td>
    </tr>';
    $total_tagihan += $data['BESAR_TAGIHAN'];
    $total_pembayaran += $data['BESAR_TERBAYAR'];
}
$total_piutang = $total_tagihan - $total_pembayaran;
$html = str_replace('{nim}', $biodata['NIM_MHS'], $html);
$html = str_replace('{nama}', $biodata['NM_PENGGUNA'], $html);
$html = str_replace('{fakultas}', strtoupper($biodata['NM_FAKULTAS']), $html);
$html = str_replace('{prodi}', $biodata['NM_PROGRAM_STUDI'], $html);
$html = str_replace('{jenjang}', $biodata['NM_JENJANG'], $html);
$html = str_replace('{data_biaya}', $biaya_tampil, $html);
$html = str_replace('{data_pembayaran}', $data_pembayaran_str, $html);
$html = str_replace('{tanggal}', date('d F Y  H:i:s'), $html);
$html = str_replace('{semester}', 'SEMESTER ' . $semester['NM_SEMESTER'] . ' ( ' . $semester['TAHUN_AJARAN'] . ' )', $html);
$html = str_replace('{total_tagihan}', 'Rp.' . number_format($total_tagihan), $html);
$html = str_replace('{total_terbayar}', 'Rp.' . number_format($total_pembayaran), $html);
$html = str_replace('{total_piutang}', 'Rp.' . number_format($total_piutang), $html);
$html = str_replace('{penangguhan}',  $penangguhan_str, $html);
$html = str_replace('{ttd}', $ttd, $html);
$html = str_replace('{jabatan_ttd}', $jabatan_ttd, $html);
$html = str_replace('{pejabat_ttd}', $pejabat_ttd, $html);

$pdf->writeHTML($html);

$pdf->Output();
