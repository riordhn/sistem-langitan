<?php

//ini_set('display_errors', 1);

//* CREATED BY : FIKRIE ~~ *//

include 'config.php';
include 'class/history.class.php';


$history = new history($db);


$id_pt = $id_pt_user;

$nim = get('cari');

if (get('cari') != '') {

	//mengambil data semester yg sudah ada pada tagihan
	$semester = $db->QueryToArray("SELECT DISTINCT S.ID_SEMESTER, S.NM_SEMESTER || ' (' || S.TAHUN_AJARAN || ')' AS SEMESTER , S.NM_SEMESTER, S.THN_AKADEMIK_SEMESTER 
										FROM SEMESTER S
										JOIN TAGIHAN T ON T.ID_SEMESTER = S.ID_SEMESTER
										JOIN MAHASISWA M ON M.ID_MHS = T.ID_MHS AND M.NIM_MHS = '{$nim}'
										JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA AND P.ID_PERGURUAN_TINGGI = {$id_pt} 
										WHERE S.ID_PERGURUAN_TINGGI = {$id_pt}
										ORDER BY S.THN_AKADEMIK_SEMESTER, S.NM_SEMESTER");
	$smarty->assign('semester', $semester);

	//mengambil data detail biaya pada semua semester berdasarkan nim mahasiswa
	$tagihan_mahasiswa = $db->QueryToArray("SELECT T.ID_TAGIHAN, T.ID_SEMESTER, S.NM_SEMESTER || ' (' || S.TAHUN_AJARAN || ')' AS SEMESTER , B.ID_BIAYA, B.NM_BIAYA, 
												BUL.NM_BULAN, T.BESAR_BIAYA, (CASE WHEN (SELECT SUM(BESAR_PEMBAYARAN) FROM PEMBAYARAN WHERE ID_TAGIHAN = T.ID_TAGIHAN) IS NULL
							                      THEN 0
							                      ELSE (SELECT SUM(BESAR_PEMBAYARAN) FROM PEMBAYARAN WHERE ID_TAGIHAN = T.ID_TAGIHAN) 
							                  END) AS BESAR_PEMBAYARAN
												FROM TAGIHAN T
												JOIN SEMESTER S ON S.ID_SEMESTER = T.ID_SEMESTER AND S.ID_PERGURUAN_TINGGI = {$id_pt}
												JOIN MAHASISWA M ON M.ID_MHS = T.ID_MHS AND M.NIM_MHS = '{$nim}'
												JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA AND P.ID_PERGURUAN_TINGGI = {$id_pt}
												JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = T.ID_DETAIL_BIAYA
												JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
												LEFT JOIN BULAN BUL ON BUL.ID_BULAN = T.ID_BULAN
												LEFT JOIN PEMBAYARAN PEMB ON PEMB.ID_TAGIHAN = T.ID_TAGIHAN	
											ORDER BY B.ID_BIAYA");

	$smarty->assign('tagihan_mahasiswa', $tagihan_mahasiswa);

    $smarty->assign('data_detail_mahasiswa', $history->get_data_mhs(trim(get('cari')),$id_pt));
}



$smarty->display("history/history-bayar-semester.tpl");

?>
