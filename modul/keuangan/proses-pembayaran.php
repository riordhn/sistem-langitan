<?php

include 'config.php';
include 'class/master.class.php';
include 'class/history.class.php';

$master = new master($db);
$history = new history($db);
$smarty->assign('cari', get('cari'));
$smarty->assign('id_tagihan_mhs',get('id_tagihan_mhs'));
$smarty->assign('data_bank',$master->load_bank());
$smarty->assign('data_bank_via',$master->load_bank_via());
$smarty->assign('data_status_pembayaran',$master->load_status_pembayaran());
$smarty->assign('data_detail_biaya_tagihan', $history->load_riwayat_pembayaran_biaya(get('id_tagihan_mhs')));
$smarty->display('history/proses-pembayaran.tpl');
