<?php

include 'config.php';
include 'class/laporan.class.php';

$laporan = new laporan($db);

$id_pt = $id_pt_user;
if($id_pt!=1){
    echo "Fitur Khusus Universitas Tertentu";
    exit;
}
if (isset($_GET['mode'])) {
    $smarty->assign('data_report', $laporan->load_report_pembayaran_va(get('tgl_awal'), get('tgl_akhir')));
}

$smarty->assign('tgl_awal', get('tgl_awal'));
$smarty->assign('tgl_akhir', get('tgl_akhir'));
$smarty->display('laporan/lap-pembayaran-va.tpl');
