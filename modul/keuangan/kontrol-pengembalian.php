<?php

include 'config.php';
include 'class/list_data.class.php';
include 'class/laporan.class.php';

$id_pt = $id_pt_user;

$list = new list_data($db);
$laporan = new laporan($db);
if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        $smarty->assign('data_pengembalian', $laporan->load_pengembalian_pembayaran(get('fakultas'), get('prodi'), get('semester')));
    }
}
$smarty->assign('data_prodi', $list->load_list_prodi($id_pt, get('fakultas')));
$smarty->assign('data_fakultas', $list->load_list_fakultas($id_pt));
$smarty->assign('data_semester', $list->load_list_semester());
$smarty->assign('data_status_piutang', $list->load_status_piutang());
$smarty->display('kontrol/kontrol-pengembalian.tpl');
?>
