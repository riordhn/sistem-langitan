<?php

include 'config.php';
include 'class/master.class.php';
include 'class/list_data.class.php';

$nama_biaya = new master($db);
$list = new list_data($db);

$mode = get('mode', 'view');
if ($request_method == 'POST') {
    if (post('mode') == 'add') {
        $nama_biaya->add_nama_biaya(post('nama'), post('keterangan'),post('jenis_biaya'));
    } else if (post('mode') == 'edit') {
        $nama_biaya->edit_nama_biaya(post('nama'), post('keterangan'),post('jenis_biaya'), post('id_biaya'));
    }else if (post('mode') == 'delete') {
        $id= post('id_biaya');
        $db->Query("DELETE FROM BIAYA WHERE ID_BIAYA='{$id}'");
    }
}
if ($mode == 'detail' || $mode == 'edit' || $mode == 'upload' || $mode == 'delete') {
    $smarty->assign('data_nama_biaya', $nama_biaya->get_nama_biaya(get('id_biaya')));
} else {
    $smarty->assign('data_nama_biaya', $nama_biaya->load_nama_biaya());
}

$smarty->assign('data_jenis_pembayaran', $list->load_jenis_detail_biaya());
$smarty->display("master/{$mode}-nama-biaya.tpl");
?>