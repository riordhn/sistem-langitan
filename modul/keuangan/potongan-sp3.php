<?php

include 'config.php';
include '../registrasi/class/CalonMahasiswa.class.php';
include 'class/pendaftaran.class.php';
include 'funtion-tampil-informasi.php';


$cmb = new CalonMahasiswa($db);
$daftar = new pendaftaran($db);
if (isset($_GET)) {
    if (get('no_ujian') != '') {
        $id_c_mhs = $daftar->get_id_cmhs(get('no_ujian'));
        if ($id_c_mhs != '') {
            $data_cmb = $cmb->GetData($id_c_mhs, true);
            if (isset($_POST)) {
                $daftar->update_sp3_potongan(post('id_c_mhs'), post('sp3_potongan'));
            }
            $status_bayar = $daftar->cek_status_bayar($id_c_mhs);
            $smarty->assign('status_bayar', $status_bayar != 0 ? alert_error("Sudah melakukan pembayaran") : "");
            $smarty->assign('biaya_kuliah_verifikasi', $daftar->get_biaya_kuliah_verifikasi($id_c_mhs));
            $smarty->assign('kelompok_biaya_cmhs', $daftar->get_kelompok_biaya_verifikasi($id_c_mhs));
            $smarty->assign('sp3_sekarang', $daftar->get_biaya_sp3_verifikasi($id_c_mhs));
            $smarty->assign('no_invoice_cmhs', $daftar->get_no_invoice_cmhs($id_c_mhs));
            $tgl_regmaba = $db->QuerySingle("SELECT TGL_REGMABA FROM AUCC.CALON_MAHASISWA_BARU WHERE ID_C_MHS='{$id_c_mhs}'");
            $smarty->assign('status_regmaba', $tgl_regmaba == '' ? alert_error("Mahasiswa Belum melakukan regmaba") : '');
            $smarty->assign('cmb', $data_cmb);
        } else {
            $smarty->assign('error', alert_error("Data calon mahasiswa tidak ditemukan..."));
        }
    }
}
$smarty->display('pendaftaran/potongan-sp3.tpl');
?>
