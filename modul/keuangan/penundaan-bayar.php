<?php

include 'config.php';
include 'class/list_data.class.php';
include 'class/laporan.class.php';
include('../ppmb/class/Penerimaan.class.php');


$list = new list_data($db);
$laporan = new laporan($db);
$penerimaan = new Penerimaan($db);

if (isset($_GET)) {
    if (get('mode') == 'laporan') {
        if (isset($_POST)) {
            if (post('mode') == 'gen_ulang') {
                $db->Query("DELETE FROM PEMBAYARAN WHERE ID_MHS='{$_POST['id_mhs']}' AND TGL_BAYAR IS NULL AND ID_BANK IS NULL");
                $db->Query("
                INSERT INTO AUCC.PEMBAYARAN
                    (ID_MHS,ID_SEMESTER,ID_DETAIL_BIAYA,ID_BANK,ID_BANK_VIA,BESAR_BIAYA,DENDA_BIAYA,TGL_BAYAR
                    ,IS_TAGIH,NO_TRANSAKSI,KETERANGAN,ID_SEMESTER_BAYAR,ID_STATUS_PEMBAYARAN)
                SELECT '{$_POST['id_mhs']}' ID_MHS,ID_SEMESTER,ID_DETAIL_BIAYA,ID_BANK,ID_BANK_VIA,BESAR_BIAYA,DENDA_BIAYA,TGL_BAYAR,
                IS_TAGIH,NO_TRANSAKSI,KETERANGAN,ID_SEMESTER_BAYAR,ID_STATUS_PEMBAYARAN 
                FROM AUCC.PEMBAYARAN_CMHS
                WHERE ID_C_MHS='{$_POST['id_c_mhs']}'");
            }
        }
        $smarty->assign('data_penundaan', $laporan->load_report_penundaan_pembayaran(get('fakultas'), get('penerimaan')));
    }
}

$smarty->assign('penerimaan_set', $penerimaan->GetAllForView());
$smarty->assign('data_prodi', $list->load_list_prodi(get('fakultas')));
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->display('pendaftaran/penundaan-bayar.tpl');
?>
