<?php

include 'config.php';
include 'class/list_data.class.php';
include 'class/detail.class.php';

$list = new list_data($db);
$detail = new detail($db);

$id_pt = $id_pt_user;

if (isset($_GET)) {
    if (get('mode') == 'tampil' || get('mode') == 'detail') {
        $smarty->assign('data_mahasiswa', $detail->load_data_mahasiswa_langitan($id_pt,get('fakultas'), get('biaya_kuliah'), get('kelompok_biaya'), get('prodi'), get('jalur'), get('angkatan'), get('mode'), get('jenis')));
    }
}

$smarty->assign('data_prodi', $list->load_list_prodi($id_pt,get('fakultas')));
$smarty->assign('data_fakultas', $list->load_list_fakultas($id_pt));
$smarty->assign('data_angkatan', $list->load_angkatan_mhs());
$smarty->assign('data_fakultas_one', $list->get_fakultas(get('fakultas')));
$smarty->assign('data_prodi_one', $list->get_prodi(get('prodi')));
$smarty->display('detail/data-mahasiswa.tpl');
?>
