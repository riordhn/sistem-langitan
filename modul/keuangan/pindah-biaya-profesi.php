<?php

include 'config.php';
include 'class/list_data.class.php';
include 'class/pendaftaran.class.php';

$list = new list_data($db);
$pen = new pendaftaran($db);

$prodi_profesi = $db->QueryToArray("
    SELECT PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.NM_JENJANG
    FROM PROGRAM_STUDI PS
    JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
    WHERE J.KODE_JENJANG='9'
    ");
if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        if (isset($_POST)) {
            if (post('mode') == 'pindah') {
                for ($i = 1; $i <= post('jumlah_data'); $i++) {
                    $id_mhs_lama = post('id_mhs_l_' . $i);
                    if ($id_mhs_lama != '') {
                        $id_mhs_baru = post('id_mhs_b_' . $i);
                        $semester = get('semester');
                        $db->Query("DELETE FROM PEMBAYARAN WHERE ID_MHS='{$id_mhs_baru}' AND ID_SEMESTER='{$semester}'");
                        $db->Query("
                           INSERT INTO PEMBAYARAN
                                (ID_MHS,ID_SEMESTER,ID_DETAIL_BIAYA,ID_BANK,ID_BANK_VIA,BESAR_BIAYA,DENDA_BIAYA,TGL_BAYAR,IS_TAGIH,NO_TRANSAKSI,KETERANGAN,ID_STATUS_PEMBAYARAN,ID_SEMESTER_BAYAR,FLAG_PEMINDAHAN)
                            SELECT '{$id_mhs_baru}' ID_MHS,ID_SEMESTER,ID_DETAIL_BIAYA,ID_BANK,ID_BANK_VIA,BESAR_BIAYA,DENDA_BIAYA,TGL_BAYAR,IS_TAGIH,NO_TRANSAKSI,KETERANGAN,ID_STATUS_PEMBAYARAN,ID_SEMESTER_BAYAR,'1' FLAG_PEMINDAHAN
                            FROM PEMBAYARAN 
                            WHERE ID_SEMESTER='{$semester}' AND ID_MHS='{$id_mhs_lama}'
                           ");
                    }
                }
            }
        }
        $smarty->assign('data_mhs', $pen->load_pindah_profesi(get('prodi'), get('semester')));
    }
}
$smarty->assign('data_prodi', $prodi_profesi);
$smarty->assign('data_semester', $list->load_list_semester());
$smarty->display('pendaftaran/pindah-biaya-profesi.tpl');
?>
