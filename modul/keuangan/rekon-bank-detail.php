<?php

$oci_user = 'aucc_akademik'; // username
$oci_pass = 'akadem1k'; // password
//$oci_host = '210.57.212.67'; // hostname
$oci_host = '10.0.110.100'; // hostname
$oci_port = '1521'; // listener port
$oci_sidc = 'aucc.localdomain'; // sid name

$oci_tnsc = '
  (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = ' . $oci_host . ')(PORT = ' . $oci_port . '))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = ' . $oci_sidc . ')
    )
  )';


@$conn = oci_connect($oci_user, $oci_pass, $oci_tnsc);
if (!$conn) {
    $e = oci_error(); // For oci_connect errors do not pass a handle
    die('database tidak konek');
}

if ($_GET["bank"] and $_GET["tgl"]) {
    switch ($_GET["bank"]) {
        case "1" :
            $nmbank = "BRI";
            break;
        case "2" :
            $nmbank = "BNI 6257";
            break;
        case "3" :
            $nmbank = "BTN";
            break;
        case "4" :
            $nmbank = "Mandiri";
            break;
        case "41" :
            $nmbank = "BNI Syariah";
            break;
        default :
            $nmbank = "-";
            break;
    }
    $tanggal = substr($_GET["tgl"], 6, 2) . "-" . substr($_GET["tgl"], 4, 2) . "-" . substr($_GET["tgl"], 0, 4);

    $query4 = "select selisih,usulan,file_lampiran,kontak,jumlah_mhs,nominal from aucc.bank_laporan where id_bank='" . $_GET["bank"] . "' and tanggal_laporan=TO_DATE('" . addslashes($_GET["tgl"]) . "', 'YYYYMMDD')";
    //echo $query;
    $stmt4 = oci_parse($conn, $query4);
    oci_execute($stmt4);
    $selisih = '0';
    $usulan = '0';
    $lampiran = '0';
    while ($r4 = oci_fetch_array($stmt4, OCI_BOTH)) {
        $selisih = $r4[0]->load();
        $usulan = $r4[1]->load();
        $lampiran = $r4[2];
        if (strlen($r4[3]) > 0) {
            $kontak = $r4[3];
        } else {
            $kontak = '0';
        }
        if (strlen($r4[4]) > 0) {
            $bank_jum = $r4[4];
        } else {
            $bank_jum = '0';
        }
        if (strlen($r4[5]) > 0) {
            $bank_nominal = $r4[5];
        } else {
            $bank_nominal = '0';
        }
    }

    // LAMA
    $query3 = "select count(distinct a.id_mhs)
			from aucc.mahasiswa a, aucc.pembayaran b
			where a.id_mhs=b.id_mhs and to_char(b.TGL_BAYAR,'YYYYMMDD')='" . $_GET["tgl"] . "' and b.id_bank='" . addslashes($_GET["bank"]) . "'
			";
    $stmt3 = oci_parse($conn, $query3);
    oci_execute($stmt3);
    $unair_mhslama_jum = '0';
    while ($r3 = oci_fetch_array($stmt3, OCI_BOTH)) {
        $unair_mhslama_jum = $r3[0];
    }

    $query3 = "select sum(b.besar_biaya+b.denda_biaya)
			from aucc.mahasiswa a, aucc.pembayaran b
			where a.id_mhs=b.id_mhs and to_char(b.TGL_BAYAR,'YYYYMMDD')='" . $_GET["tgl"] . "' and b.id_bank='" . addslashes($_GET["bank"]) . "'
			";
    $stmt3 = oci_parse($conn, $query3);
    oci_execute($stmt3);
    $unair_mhslama_nominal = '0';
    while ($r3 = oci_fetch_array($stmt3, OCI_BOTH)) {
        $unair_mhslama_nominal = $r3[0];
    }

    // BARU
    $query2 = "select sum(b.besar_biaya)
			from AUCC.calon_mahasiswa_baru a, AUCC.pembayaran_cmhs b
			where a.id_c_mhs=b.id_c_mhs
			and (to_char(b.TGL_BAYAR,'YYYYMMDD')>='" . $awal . "' and to_char(b.TGL_BAYAR,'YYYYMMDD')<='" . $akhir . "')
			and b.id_bank='" . addslashes($_GET["bank"]) . "'
			";
    $stmt2 = oci_parse($conn, $query2);
    oci_execute($stmt2);
    $unair_mhsbaru_nominal = '0';
    while ($r2 = oci_fetch_array($stmt2, OCI_BOTH)) {
        $unair_mhsbaru_nominal = $r2[0];
    }

    // jum mhs
    $query2 = "select count(distinct a.id_c_mhs)
			from AUCC.calon_mahasiswa_baru a, AUCC.pembayaran_cmhs b
			where a.id_c_mhs=b.id_c_mhs
			and (to_char(b.TGL_BAYAR,'YYYYMMDD')>='" . $awal . "' and to_char(b.TGL_BAYAR,'YYYYMMDD')<='" . $akhir . "')
			and b.id_bank='" . addslashes($_GET["bank"]) . "'
			";
    $stmt2 = oci_parse($conn, $query2);
    oci_execute($stmt2);
    $unair_mhsbaru_jum = '0';
    while ($r2 = oci_fetch_array($stmt2, OCI_BOTH)) {
        $unair_mhsbaru_jum = $r2[0];
    }

    $mhstot_jum = $unair_mhsbaru_jum + $unair_mhslama_jum;
    $mhstot_nominal = $unair_mhsbaru_nominal + $unair_mhslama_nominal;
    $lampiran = substr($lampiran, 2)!=''?'<span class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;" onclick=' . 'window.location.href="'.$base_url.'"modul/mhs/includes/bank/' . substr($lampiran, 2) . '">' . substr($lampiran, 2) . '</span>':'Tidak Ada';
    echo '
		<div class="center_title_bar">Laporan Rekonsiliasi Bank</div>
		<table class="ui-widget" style="width:40%">
		<tr class="ui-widget-header">
			<th>Bank</th>
			<th>' . $nmbank . '</th>
		</tr>
		<tr class="ui-widget-content">
			<td>Tanggal Transaksi</td>
			<td>' . $tanggal . '</td>
		</tr>
		<tr class="ui-widget-content">
			<td>Kontak</td>
			<td>' . $kontak . '</td>
		</tr>
		</table>
		<br>
		<table cellpadding=5 cellspacing=0 border=1 class="ui-widget" style="width:40%">
		<tr class="ui-widget-header">
			<th rowspan=2 align=center><b>Pihak</b></th>
			<th colspan=2 align=center><b>Penerimaan</b></th>
		</tr>
		<tr class="ui-widget-header">
			<th align=center><b>Jum Mhs bayar</b></th>
			<th align=center><b>Nominal</b></th>
		</tr>
		<tr class="ui-widget-content">
			<td>DSI</td>
			<td align=center>' . $mhstot_jum . '</td>
			<td align=right>' . number_format($mhstot_nominal, 0, ',', '.') . '</td>
		</tr>
		<tr class="ui-widget-content">
			<td>Bank</td>
			<td align=center>' . $bank_jum . '</td>
			<td align=right>' . number_format($bank_nominal, 0, ',', '.') . '</td>
		</tr>
		<tr class="ui-widget-content">
			<td>Selisih</td>
			<td align=center>' . ($mhstot_jum - $bank_jum) . '</td>
			<td align=right>' . number_format(($mhstot_nominal - $bank_nominal), 0, ',', '.') . '</td>
		</tr>
		</table>
		<br>
                <div style="margin-left:15px">
		<p>
		<b>Keterangan Selisih</b> :<br>
		<div style="padding-left:10px;">' . nl2br($selisih) . '</div>
		</p>
		<p>
		<b>Usulan</b> :<br>
		<div style="padding-left:10px;">' . nl2br($usulan) . '</div>
		</p>
		<p><b>File Lampiran</b> :' . $lampiran . ' </p>
		<br/><span class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;" onclick="history.back(-1)">Kembali</span></div>';
}
?>