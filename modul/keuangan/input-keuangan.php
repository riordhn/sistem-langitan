<?php
include 'config.php';
include 'class/history.class.php';

$history = new history($db);

$id_pt = $id_pt_user;

$nim = get('cari');
$mode = get('mode', '');

if (get('cari') != '') {
	/*$db->Query("SELECT bkm.ID_BIAYA_KULIAH FROM BIAYA_KULIAH_MHS bkm, MAHASISWA m WHERE bkm.ID_MHS = m.ID_MHS AND m.NIM_MHS = '{$nim}'");
	$bkm = $db->FetchAssoc();

	$db->Query("SELECT * FROM SEMESTER WHERE ID_PERGURUAN_TINGGI = {$id_pt} AND TIPE_SEMESTER = 'REG' ORDER BY THN_AKADEMIK_SEMESTER");
	$sem = $db->FetchAssoc();

	$db->Query("SELECT * FROM DETAIL_BIAYA WHERE ID_BIAYA IN (SELECT ID_BIAYA FROM BIAYA WHERE STATUS_BIAYA = '1') 
													AND ID_BIAYA_KULIAH = {$bkm['ID_BIAYA_KULIAH']} ORDER BY ID_BIAYA");
	$detail_biaya = $db->FetchAssoc();

	$spp1 = $bkm['ID_BIAYA_KULIAH'];
	echo "hahahaha ".$detail_biaya['BESAR_BIAYA'];

	$db->Query("SELECT * FROM DETAIL_BIAYA WHERE ID_BIAYA = '138' 
													AND ID_BIAYA_KULIAH = '{$bkm['ID_BIAYA_KULIAH']}' ORDER BY ID_BIAYA");
	$spp1 = $db->FetchAssoc();*/

	/*if (post('biaya_kuliah') != '') {
		if ($biaya_mhs->get_biaya_kuliah_mhs(trim(get('cari'))) != '')
			$biaya_mhs->update_biaya_kuliah_mhs(get('cari'), post('biaya_kuliah'));
		else
			$biaya_mhs->insert_biaya_kuliah_mhs(get('cari'), post('biaya_kuliah'));
	}
	$smarty->assign('biaya_kuliah_mhs', $biaya_mhs->get_biaya_kuliah_mhs(trim(get('cari'))));
	$smarty->assign('data_biaya_kuliah', $biaya_mhs->load_biaya_kuliah_mhs(trim(get('cari'))));*/

	//mengambil data semester yg sudah ada pada tagihan
	$semester_set = $db->QueryToArray("SELECT DISTINCT S.ID_SEMESTER, S.NM_SEMESTER || ' (' || S.TAHUN_AJARAN || ')' AS SEMESTER , S.NM_SEMESTER, S.THN_AKADEMIK_SEMESTER 
										FROM SEMESTER S
										JOIN TAGIHAN T ON T.ID_SEMESTER = S.ID_SEMESTER
										JOIN MAHASISWA M ON M.ID_MHS = T.ID_MHS AND M.NIM_MHS = '{$nim}'
										JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA AND P.ID_PERGURUAN_TINGGI = {$id_pt} 
										WHERE S.ID_PERGURUAN_TINGGI = {$id_pt}
										ORDER BY S.THN_AKADEMIK_SEMESTER, S.NM_SEMESTER");
	$smarty->assign('semester_set', $semester_set);

	echo "<!-- \$semester_set: ".print_r($semester_set,true)." -->";

	$id_semester = (int)get('id_semester', '');
	$id_detail_biaya = (int)get('id_detail_biaya', '');

	//mengambil nama setiap detail biaya utk keperluan combo box
	$detail_biaya = $db->QueryToArray("SELECT T.ID_DETAIL_BIAYA, T.BESAR_BIAYA, B.NM_BIAYA, BUL.NM_BULAN FROM TAGIHAN T
										JOIN SEMESTER S ON S.ID_SEMESTER = T.ID_SEMESTER AND S.ID_PERGURUAN_TINGGI = {$id_pt}
										JOIN MAHASISWA M ON M.ID_MHS = T.ID_MHS AND M.NIM_MHS = '{$nim}'
										JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA AND P.ID_PERGURUAN_TINGGI = {$id_pt}
										JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = T.ID_DETAIL_BIAYA
										JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
										LEFT JOIN BULAN BUL ON BUL.ID_BULAN = B.ID_BULAN
										WHERE T.ID_SEMESTER = {$id_semester}
										");
	$smarty->assign('detail_biaya', $detail_biaya);

	//mengambil besar biaya tiap tagihan
	$besar_biaya = $db->QueryToArray("SELECT BESAR_BIAYA FROM DETAIL_BIAYA WHERE ID_DETAIL_BIAYA = {$id_detail_biaya}");
	$smarty->assign('besar_biaya', $besar_biaya);

	//mengambil data detail biaya berdasarkan nim mahasiswa dan semester (di tpl diberi if besar_pembayaran > 0)
	$tagihan_mahasiswa = $db->QueryToArray("SELECT T.ID_TAGIHAN, T.ID_SEMESTER, S.NM_SEMESTER || ' (' || S.TAHUN_AJARAN || ')' AS SEMESTER , B.NM_BIAYA, 
												T.BESAR_BIAYA, (CASE WHEN (SELECT SUM(BESAR_PEMBAYARAN) FROM PEMBAYARAN WHERE ID_TAGIHAN = T.ID_TAGIHAN) IS NULL
												  THEN 0
												  ELSE (SELECT SUM(BESAR_PEMBAYARAN) FROM PEMBAYARAN WHERE ID_TAGIHAN = T.ID_TAGIHAN) 
											  END) AS BESAR_PEMBAYARAN
												FROM TAGIHAN T
												JOIN SEMESTER S ON S.ID_SEMESTER = T.ID_SEMESTER AND S.ID_PERGURUAN_TINGGI = {$id_pt}
												JOIN MAHASISWA M ON M.ID_MHS = T.ID_MHS AND M.NIM_MHS = '{$nim}'
												JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA AND P.ID_PERGURUAN_TINGGI = {$id_pt}
												JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = T.ID_DETAIL_BIAYA
												JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
												LEFT JOIN PEMBAYARAN PEMB ON PEMB.ID_TAGIHAN = T.ID_TAGIHAN	
												WHERE T.ID_SEMESTER = {$id_semester}
											ORDER BY B.NM_BIAYA");

	$smarty->assign('tagihan_mahasiswa', $tagihan_mahasiswa);

	$smarty->assign('data_detail_mahasiswa', $history->get_data_mhs(trim(get('cari')),$id_pt));

	if (post('mode') == 'simpan') {
		// action post ketika submit
		$semester = array();
		$semester = post('semester');

		$tagihan = array();
		$besar_pembayaran = array();
		$keterangan = array();

		$tgl_bayar = post('tgl_bayar');

		$id_pengguna = $user->ID_PENGGUNA;

		foreach ($semester as $sem) {
			$tagihan = post('tag_'.$sem.'');
			$besar_pembayaran = post('bp_'.$sem.'');
			$keterangan = post('ket_'.$sem.'');

			for ($i=0; $i < count($tagihan); $i++) { 
				# code...
				$db->Query("INSERT INTO PEMBAYARAN (ID_TAGIHAN, TGL_BAYAR, BESAR_PEMBAYARAN, ID_PENGGUNA, KETERANGAN) 
								VALUES ('{$tagihan[$i]}','{$tgl_bayar}','{$besar_pembayaran[$i]}','{$id_pengguna}','{$keterangan[$i]}')");

			}
			

			
		}

				

	}



}

if ($mode == 'get-detail-tagihan')
{
	$id_semester	= get('id_semester');
	$nim 			= get('nim');

	$detail_biaya_set = $db->QueryToArray(
		"SELECT t.id_tagihan, t.id_detail_biaya, t.besar_biaya, b.nm_biaya, bln.nm_bulan
		FROM tagihan t
		JOIN semester s ON s.id_semester = t.id_semester AND s.id_perguruan_tinggi = {$id_pt}
		JOIN mahasiswa m ON m.id_mhs = t.id_mhs AND m.nim_mhs = '{$nim}'
		JOIN pengguna p ON p.id_pengguna = m.id_pengguna AND p.id_perguruan_tinggi = {$id_pt}
		JOIN detail_biaya db ON db.id_detail_biaya = t.id_detail_biaya
		JOIN biaya b ON b.id_biaya = db.id_biaya
		LEFT JOIN bulan bln ON bln.id_bulan = t.id_bulan
		WHERE t.id_semester = '{$id_semester}'
		ORDER BY nm_biaya, bln.id_bulan");

	echo json_encode($detail_biaya_set);

	exit();
}

$smarty->display("input/input-keuangan.tpl");