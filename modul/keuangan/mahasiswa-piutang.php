<?php

include 'config.php';
include 'class/list_data.class.php';
include 'class/piutang.class.php';
$list = new list_data($db);
$piutang = new piutang($db);

$id_pt = $id_pt_user;

//$db->Query("DELETE FROM AUCC.RIWAYAT_DETAIL_PIUTANG WHERE ID_RIWAYAT_PIUTANG IN (4868,4869)");
//$db->Query("DELETE FROM AUCC.RIWAYAT_PIUTANG WHERE TGL_CUT_OFF='31-JAN-12'");

if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        if (post('mode') == 'input_data') {
            for ($i = 1; $i <= post('total_data'); $i++) {
                if (post('id_mhs' . $i) != '' && post('status' . $i) != 'x') {
                    $nim = post('nim' . $i);
                    $nama = post('nama' . $i);
                    $status = post('status' . $i);
                    $tgl_cut = post('tgl_cut_off');
                    $dp = $db->QueryToArray("SELECT * FROM RIWAYAT_PIUTANG WHERE NIM='{$nim}' AND NAMA='{$nama}' AND TGL_CUT_OFF='{$tgl_cut}'");
                    if (count($dp) > 0) {
                        $db->Query("UPDATE RIWAYAT_PIUTANG SET STATUS_PIUTANG='{$status}' WHERE NIM='{$nim}' AND NAMA='{$nama}' AND TGL_CUT_OFF='{$tgl_cut}'");
                    } else {
                        $piutang->insert_data_piutang(post('id_mhs' . $i), $nama, $nim, post('jenjang' . $i), post('prodi' . $i), post('fakultas' . $i), post('banyak' . $i), $tgl_cut, post('perubahan' . $i), $status, post('status_a' . $i));
                    }
                }
            }
            $smarty->assign('informasi_data_input', alert_success(' Data Berhasil Dimasukkan'));
        }
//        echo "<pre>";
//        var_dump($piutang->load_daftar_mahasiswa_piutang(get('fakultas'), get('prodi'), get('status'), get('tagihan')));
//        echo "</pre>";
        $smarty->assign('data_mhs_piutang', $piutang->load_daftar_mahasiswa_piutang(get('fakultas'), get('prodi'), get('status'), get('tagihan')));
    }
}

$smarty->assign('data_prodi', $list->load_list_prodi($id_pt, get('fakultas')));
$smarty->assign('data_fakultas', $list->load_list_fakultas($id_pt));
$smarty->assign('data_status', $piutang->load_list_status());
$smarty->assign('data_semester', $list->load_list_semester());
$smarty->display('laporan/mahasiswa-piutang.tpl');
?>
