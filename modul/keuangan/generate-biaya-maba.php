<?php
include('config.php');
//die('-');

$id_pt = $id_pt_user;

$fakultas = $db->QueryToArray("select * from fakultas where id_perguruan_tinggi = {$id_pt} order by id_fakultas");
$smarty->assign('fakultas', $fakultas);
$jalur = $db->QueryToArray("select * from jalur order by nm_jalur");
$smarty->assign('jalur', $jalur);
$jenjang = $db->QueryToArray("select * from jenjang order by nm_jenjang");
$smarty->assign('jenjang', $jenjang);
$data_thn = $db->QueryToArray("SELECT TAHUN FROM PENERIMAAN GROUP BY TAHUN ORDER BY TAHUN DESC");
$smarty->assign('data_thn', $data_thn);

$fakultas = $_REQUEST['fakultas'];
$jenjang = $_REQUEST['jenjang'];
$jalur = $_REQUEST['jalur'];
$gelombang = $_REQUEST['gelombang'];
$thn = $_REQUEST['thn'];
$semester = $_REQUEST['semester'];

if($_POST['mode'] == 'generate' and isset($_POST['mode'])){
	
	 $db->BeginTransaction();
	 
	$no = $_POST['no'];
	for($i=2;$i<=$no;$i++){
		$cek = $_POST['cek'.$i];
		$id_biaya_kuliah = $_POST['id_biaya_kuliah'.$i];
		$id_c_mhs = $_POST['id_c_mhs'.$i];
		$id_semester = $_POST['id_semester'.$i];
		$sp3 = $_POST['sp3'.$i];
		
		if($cek == 1){
			 // insert biaya kuliah cmhs
			$result = $db->Query("insert into biaya_kuliah_cmhs (id_c_mhs, id_biaya_kuliah) 
									values ({$id_c_mhs}, {$id_biaya_kuliah})") or die ('mati');
		
			// insert pembayaran calon mahasiswa tanpa SP3
			$result = $db->Query("
				insert into pembayaran_cmhs (id_c_mhs, id_semester, id_detail_biaya, besar_biaya, is_tagih)
					select bkc.id_c_mhs, {$id_semester} as id_semester, db.id_detail_biaya, db.besar_biaya, 'Y' as is_tagih 
					from biaya_kuliah_cmhs bkc
					left join detail_biaya db on db.id_biaya_kuliah = bkc.id_biaya_kuliah
					where bkc.id_c_mhs = {$id_c_mhs} and id_biaya <> 81") or die ('mati');
			
			// insert pembayaran calon mahasiswa SP3
			$result = $db->Query("
				insert into pembayaran_cmhs (id_c_mhs, id_semester, id_detail_biaya, besar_biaya, is_tagih)
				select bkc.id_c_mhs, {$id_semester} as id_semester, db.id_detail_biaya, {$sp3} as besar_biaya, 'Y' as is_tagih 
					from biaya_kuliah_cmhs bkc
					left join detail_biaya db on db.id_biaya_kuliah = bkc.id_biaya_kuliah
					where bkc.id_c_mhs = {$id_c_mhs} and id_biaya = 81") or die ('mati');
		}
	}
			
	$result = $db->Commit();
	
	if ($result) { echo "Sudah Tergenerate"; }
}


if(isset($_REQUEST['mode']) and $_GET['mode'] == 'tampil'){


		if($fakultas <> '' and $jenjang <> '' and $jalur <> '' and $gelombang <> '' and $thn <> '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur <> '' and $gelombang <> '' and $thn <> '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' and p.tahun = '$thn'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur <> '' and $gelombang <> '' and $thn == '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur <> '' and $gelombang == '' and $thn <> '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur == '' and $gelombang <> '' and $thn <> '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.gelombang = '$gelombang' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur <> '' and $gelombang <> '' and $thn <> '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur <> '' and $gelombang <> '' and $thn <> '' and $semester <> ''){
			$where = "and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur <> '' and $gelombang <> '' and $thn == '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur <> '' and $gelombang == '' and $thn <> '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.tahun = '$thn'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur == '' and $gelombang <> '' and $thn <> '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang'= and p.gelombang = '$gelombang' and p.tahun = '$thn'=";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur <> '' and $gelombang <> '' and $thn <> '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' and p.tahun = '$thn'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur <> '' and $gelombang <> '' and $thn <> '' and $semester == ''){
			$where = "and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' and p.tahun = '$thn'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur <> '' and $gelombang == '' and $thn == '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur == '' and $gelombang <> '' and $thn == '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.gelombang = '$gelombang' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur <> '' and $gelombang <> '' and $thn == '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur <> '' and $gelombang <> '' and $thn == '' and $semester <> ''){
			$where = "and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur == '' and $gelombang == '' and $thn <> '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur <> '' and $gelombang == '' and $thn <> '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jalur = '$jalur' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur <> '' and $gelombang == '' and $thn <> '' and $semester <> ''){
			$where = "and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur == '' and $gelombang <> '' and $thn <> '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.gelombang = '$gelombang' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur == '' and $gelombang <> '' and $thn <> '' and $semester <> ''){
			$where = "and p.id_jenjang = '$jenjang' and p.gelombang = '$gelombang' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur <> '' and $gelombang <> '' and $thn <> '' and $semester <> ''){
			$where = "and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur <> '' and $gelombang == '' and $thn == '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur == '' and $gelombang <> '' and $thn == '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.gelombang = '$gelombang'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur <> '' and $gelombang <> '' and $thn == '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur <> '' and $gelombang <> '' and $thn == '' and $semester == ''){
			$where = "and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur == '' and $gelombang == '' and $thn <> '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.tahun = '$thn'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur <> '' and $gelombang == '' and $thn <> '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jalur = '$jalur' and p.tahun = '$thn'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur <> '' and $gelombang == '' and $thn <> '' and $semester == ''){
			$where = "and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.tahun = '$thn'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur == '' and $gelombang <> '' and $thn <> '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.gelombang = '$gelombang' and p.tahun = '$thn'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur == '' and $gelombang <> '' and $thn <> '' and $semester == ''){
			$where = "and p.id_jenjang = '$jenjang' and p.gelombang = '$gelombang' and p.tahun = '$thn'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur <> '' and $gelombang <> '' and $thn <> '' and $semester == ''){
			$where = "and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' and p.tahun = '$thn'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur == '' and $gelombang == '' and $thn == '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur <> '' and $gelombang == '' and $thn == '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jalur = '$jalur' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur <> '' and $gelombang == '' and $thn == '' and $semester <> ''){
			$where = "and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur == '' and $gelombang == '' and $thn <> '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur == '' and $gelombang == '' and $thn <> '' and $semester <> ''){
			$where = "and p.id_jenjang = '$jenjang' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur <> '' and $gelombang == '' and $thn <> '' and $semester <> ''){
			$where = "and p.id_jalur = '$jalur' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur == '' and $gelombang <> '' and $thn <> '' and $semester <> ''){
			$where = "and p.gelombang = '$gelombang' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur == '' and $gelombang == '' and $thn == '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur <> '' and $gelombang == '' and $thn == '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jalur = '$jalur'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur == '' and $gelombang <> '' and $thn == '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.gelombang = '$gelombang'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur == '' and $gelombang == '' and $thn <> '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.tahun = '$thn'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur == '' and $gelombang == '' and $thn == '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur <> '' and $gelombang == '' and $thn == '' and $semester == ''){
			$where = "and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur == '' and $gelombang <> '' and $thn == '' and $semester == ''){
			$where = "and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur == '' and $gelombang == '' and $thn <> '' and $semester == ''){
			$where = "and p.id_jenjang = '$jenjang' and p.tahun = '$thn'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur == '' and $gelombang == '' and $thn == '' and $semester <> ''){
			$where = "and p.id_jenjang = '$jenjang' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur <> '' and $gelombang <> '' and $thn == '' and $semester == ''){
			$where = "and p.id_jalur = '$jalur' and p.gelombang = '$gelombang'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur <> '' and $gelombang == '' and $thn <> '' and $semester == ''){
			$where = "and p.id_jalur = '$jalur' and p.tahun = '$thn'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur <> '' and $gelombang == '' and $thn == '' and $semester <> ''){
			$where = "and p.id_jalur = '$jalur' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur == '' and $gelombang <> '' and $thn <> '' and $semester == ''){
			$where = "and p.gelombang = '$gelombang' and p.tahun = '$thn'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur == '' and $gelombang <> '' and $thn == '' and $semester <> ''){
			$where = "and p.gelombang = '$gelombang' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur == '' and $gelombang == '' and $thn <> '' and $semester <> ''){
			$where = "and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur == '' and $gelombang == '' and $thn == '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur == '' and $gelombang == '' and $thn == '' and $semester == ''){
			$where = "and p.id_jenjang = '$jenjang'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur <> '' and $gelombang == '' and $thn == '' and $semester == ''){
			$where = "and p.id_jalur = '$jalur'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur == '' and $gelombang <> '' and $thn == '' and $semester == ''){
			$where = "and p.gelombang = '$gelombang'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur == '' and $gelombang == '' and $thn <> '' and $semester == ''){
			$where = "and p.tahun = '$thn'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur == '' and $gelombang == '' and $thn == '' and $semester <> ''){
			$where = "and p.semester = '$semester'";
		}
		
		
		if($semester == 'Gasal'){
			$semester = 'Ganjil';
		}
		

$penerimaan = $db->QueryToArray("select f.id_fakultas, no_ujian, nim_mhs, nm_c_mhs, nm_fakultas, nm_program_studi, nm_jenjang, 
		nm_kelompok_biaya, nm_jalur, besar_biaya_kuliah, sp33, cmb.id_c_mhs, biaya_kuliah.id_biaya_kuliah, semester.id_semester,
		sum(detail_biaya.besar_biaya) as biaya, sudah_generate,
		(sum(detail_biaya.besar_biaya) + sp33) as total_biaya
        from calon_mahasiswa_baru cmb
		join (select (case when ID_PROGRAM_STUDI = ID_PILIHAN_1 THEN SP3_1 
																			when ID_PROGRAM_STUDI = ID_PILIHAN_2 THEN SP3_2
																			when ID_PROGRAM_STUDI = ID_PILIHAN_3 THEN SP3_3
																			when ID_PROGRAM_STUDI = ID_PILIHAN_4 THEN SP3_4
																			END) as sp33,
						ID_C_MHS
					from CALON_MAHASISWA_BARU WHERE ID_PROGRAM_STUDI IS NOT NULL) W ON W.ID_C_MHS = cmb.ID_C_MHS
		join program_studi ps on ps.id_program_studi = cmb.id_program_studi
		join fakultas f on f.id_fakultas = ps.id_fakultas
		join jenjang j on j.id_jenjang = ps.id_jenjang
		join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
		join jalur on jalur.id_jalur = p.id_jalur
		join semester on semester.thn_akademik_semester = p.tahun and semester.nm_semester = '$semester'
		join kelompok_biaya on kelompok_biaya.id_kelompok_biaya = cmb.id_kelompok_biaya
		join biaya_kuliah on biaya_kuliah.id_kelompok_biaya = kelompok_biaya.id_kelompok_biaya 
			and biaya_kuliah.id_program_studi = ps.id_program_studi 
		and biaya_kuliah.id_jalur = jalur.id_jalur and biaya_kuliah.id_semester = semester.id_semester
		left join detail_biaya on detail_biaya.id_biaya_kuliah = biaya_kuliah.id_biaya_kuliah and id_biaya <> 81
		left join (select id_c_mhs, count(*) as sudah_generate from pembayaran_cmhs group by id_c_mhs) a on a.id_c_mhs = cmb.id_c_mhs
        where cmb.id_program_studi is not null
		$where
		group by f.id_fakultas, no_ujian, nim_mhs, nm_c_mhs, nm_fakultas, nm_program_studi, nm_jenjang, cmb.id_c_mhs, biaya_kuliah.id_biaya_kuliah,
		nm_kelompok_biaya, nm_jalur, besar_biaya_kuliah, sp33, semester.id_semester, sudah_generate
        order by f.id_fakultas, nm_program_studi, nm_jenjang, no_ujian");
		
$smarty->assign('penerimaan', $penerimaan);
	
}



$smarty->assign('id_fakultas', $_REQUEST['fakultas']);
$smarty->assign('id_jenjang', $_REQUEST['jenjang']);
$smarty->assign('id_jalur', $_REQUEST['jalur']);
$smarty->assign('id_gelombang', $_REQUEST['gelombang']);
$smarty->assign('id_thn', $_REQUEST['thn']);
$smarty->assign('id_semester', $_REQUEST['semester']);
$smarty->display("utility/generate-biaya-maba.tpl");
?>
