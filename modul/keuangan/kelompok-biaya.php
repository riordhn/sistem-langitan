<?php
include 'config.php';
include 'class/master.class.php';

$kelompok_biaya = new master($db);

$mode = get('mode','view');

if($request_method=='POST'){
	if (post('mode') == 'add')
	{
		$kelompok_biaya->add_kelompok_biaya(post('nama'),post('jenis'),post('keterangan'));
	}
	else if(post('mode')=='edit')
	{	
		$kelompok_biaya->edit_kelompok_biaya(post('nama'),post('jenis'),post('keterangan'),post('id_kelompok_biaya'));
	}
}
if($mode=='detail'||$mode=='edit'||$mode=='upload'||$mode=='delete'){
	$smarty->assign('data_kelompok_biaya',$kelompok_biaya->get_kelompok_biaya(get('id')));
}else{
	$smarty->assign('data_kelompok_biaya', $kelompok_biaya->load_kelompok_biaya());
}


$smarty->display("master/{$mode}-kelompok-biaya.tpl");
?>