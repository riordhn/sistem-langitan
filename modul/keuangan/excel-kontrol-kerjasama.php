<?php
include 'config.php';
include 'class/laporan.class.php';
include 'class/list_data.class.php';

$list = new list_data($db);
$laporan = new laporan($db);

$data_excel = $laporan->load_data_mahasiswa_kerjasama(get('fakultas'), get('prodi'), get('beasiswa'), get('semester'));
ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #2b1212;
        text-transform:capitalize;
    }
    td{
        text-align: left;
    }
</style>
<table border='1' align='left' cellpadding='0' cellspacing='0' class='keuangan'>
    <thead>
        <tr>
            <td colspan="8" class="header_text">
                <?php
                echo 'LAPORAN KONTROL KERJASAMA';
                ?>
            </td>
        </tr>
        <tr>
            <td class="header_text">NO</td>
            <td class="header_text">NIM</td>
            <td class="header_text">NAMA</td>
            <td class="header_text">PRODI</td>
            <td class="header_text">JALUR</td>
            <td class="header_text">JENIS BEASISWA</td>
            <td class="header_text">STATUS BAYAR</td>
            <td class="header_text">KETERANGAN</td>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach ($data_excel as $data) {
            $sp3_lebih = $data['SP3_BAYAR'] - $data['BESAR_BIAYA'];
            if ($sp3_lebih == 0) {
                $sp3_lebih = $data['SP3_SUKARELA'];
            }
            echo "<tr>
				<td>{$no}</td>
				<td>'{$data['NIM_MHS']}</td>
				<td>{$data['NM_PENGGUNA']}</td>
				<td>{$data['NM_JENJANG']} {$data['NM_PROGRAM_STUDI']}</td>
                                <td>{$data['NM_JALUR']}</td>
				<td>{$data['NM_BEASISWA']} {$data['PENYELENGGARA_BEASISWA']}</td>
				<td>
                                ";
            if (isset($data['DATA_PEMBAYARAN'])) {
                $no_p = 1;
                foreach ($data['DATA_PEMBAYARAN'] as $pembayaran) {
                    echo "NO: {$no_p} {$pembayaran['NAMA_STATUS']}<br/>
                    Tgl Bayar : {$pembayaran['TGL_BAYAR']} <br/>
                    Bank : {$pembayaran['NM_BANK']} <br/>
                    Total Bayar : {$pembayaran['TOTAL_BAYAR']}<br/><br/>";
                    $no_p++;
                }
            } else {
                echo 'Belum Bayar';
            }
            echo "  </td>
                    <td>{$data['KETERANGAN']}</td>
                    </tr>";
            $no++;
        }
        ?>
    </tbody>
</table>

<?php
echo '<b>Data Ini Di Ambil Pada Tanggal ' . date('d F Y  H:i:s') . '</b>';

$nm_file = "kontrol-kerjasama" . date('d-m-Y');
header("Content-disposition: attachment; filename=$nm_file.xls");
header("Content-type: Application/exe");
header("Content-Transfer-Encoding: binary");
ob_flush();
?>
