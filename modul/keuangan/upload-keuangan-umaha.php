<?php
include '../../config.php';

$nim	= $_GET['nim'];
$tgl	= $_GET['tgl'];
$jenis	= trim($_GET['jenis']);
$bulan	= $_GET['bulan'];
$tahun	= $_GET['tahun'];
$biaya	= $_GET['biaya'];
$baris	= $_GET['baris'];


$db->Query("SELECT id_mhs FROM mahasiswa JOIN pengguna ON pengguna.id_pengguna = mahasiswa.id_pengguna WHERE nim_mhs = '{$nim}' AND id_perguruan_tinggi = 1");
$row = $db->FetchRow();
$id_mhs = $row[0];
//echo "id_mhs={$id_mhs}\r\n";

$db->Query("SELECT id_biaya FROM biaya WHERE id_perguruan_tinggi = 1 AND upper(nm_biaya) = upper('{$jenis}')");
$row = $db->FetchRow();
$id_biaya = $row[0];
//echo "id_biaya={$id_biaya}\r\n";

$db->Query("SELECT id_bulan, koefisien, nm_semester FROM bulan WHERE upper(nm_bulan) = upper('{$bulan}')");
$row = $db->FetchRow();
$id_bulan = $row[0];
$koefisien = $row[1];
$nm_semester = $row[2];
if (!$id_bulan) { $id_bulan = 'NULL'; }
//echo "id_bulan={$id_bulan}\r\n";

$db->Query(
	"SELECT id_semester FROM semester WHERE 
	id_perguruan_tinggi = 1 AND 
	thn_akademik_semester = {$tahun} - {$koefisien} AND
	upper(nm_semester) = upper('{$nm_semester}')");
$row = $db->FetchRow();
$id_semester = $row[0];
//echo "id_semester={$id_semester}\r\n";

$sql = "INSERT INTO tagihan_temp (id_tagihan, id_mhs, id_semester, besar_biaya, denda_biaya, is_tagih, keterangan, id_status_pembayaran, id_bulan, id_biaya)
VALUES ({$baris}, {$id_mhs},{$id_semester},{$biaya},0,0,'Data Lama',1,{$id_bulan},{$id_biaya})\r\n";

$result = $db->Query($sql);
if ($result)
	echo "Baris: {$baris} Berhasil\r\n";
else
	echo "Baris: {$baris} Gagal --&gt;&gt; {$sql}\r\n";