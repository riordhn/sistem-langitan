<?php

// Test CVS
include 'config.php';
include 'class/reader.php';
include 'class/list_data.class.php';
include 'class/upload.class.php';
include 'class/utility.class.php';
include 'class/master.class.php';

$list = new list_data($db);
$upload = new upload($db);
$utility = new utility($db);
$master = new master($db);

// ExcelFile($filename, $encoding);
$data = new Spreadsheet_Excel_Reader();
$status_message = '';

if (get('mode') == 'save_excel') {
    foreach ($_SESSION['data_kerjasama'] as $data) {
        if (count($upload->cek_mahasiswa($data['NIM_MHS'])) > 0) {
            $upload->non_aktif_mahasiswa_kerjasama($data['ID_MHS']);
            $upload->insert_mahasiswa_kerjasama($data['ID_MHS'], $_SESSION['beasiswa'], $_SESSION['semester_mulai'], $_SESSION['semester_selesai'], $_SESSION['keterangan']);
        }
    }
    $smarty->assign('sukses_upload', alert_success('Data Berhasil Dimasukkan'));
}

// Set output Encoding.
if (isset($_POST['excel'])) {
    $filename = $user->ID_PENGGUNA . '_' . date('dmy_H:i:s') . '_' . $_FILES["file"]["name"];
    if ($_FILES["file"]["error"] > 0) {
        $status_message .= "Error : " . $_FILES["file"]["error"] . "<br />";
        $status_upload = alert_error('Upload File Gagal <br/>' . $status_message);
    } else {
        $status_message .= "Nama File : " . $filename . "<br />";
        $status_message .= "Type : " . $_FILES["file"]["type"] . "<br />";
        $status_message .= "Size : " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
        if (file_exists("excel/excel_kerjasama/" . $filename)) {
            $status_message .= 'Keterangan : ' . $filename . " Sudah Ada ";
            $status_upload = alert_error('Upload File Gagal <br/>' . $status_message);
        } else {
            move_uploaded_file($_FILES["file"]["tmp_name"], "excel/excel_kerjasama/" . $filename);
            chmod("excel/excel_kerjasama/" . $filename, 0755);
            $status_upload = alert_success('Berhasil Melakukan Upload File <br/>' . $status_message);
        }
    }
    if ($status_upload) {
        $data->setOutputEncoding('CP1251');
        $data->read("excel/excel_kerjasama/{$filename}");
        $data_kerjasama_upload = array();
        for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
            $nim = str_replace("'", "", trim($data->sheets[0]['cells'][$i][1]));
            $data_mhs = $upload->get_mahasiswa($nim);
            if (count($upload->cek_mahasiswa($nim)) > 0) {
                if ($upload->cek_mahasiswa_kerjasama($nim) != '') {
                    $status_beasiswa = $upload->cek_mahasiswa_kerjasama($nim);
                } else {
                    $status_beasiswa = 'Data Kosong';
                }
                $status_mahasiswa = 'Data Mahasiswa Ada';
            } else {
                $status_mahasiswa = 'Data Mahasiswa Tidak Ada';
                $status_beasiswa = 'Data Kosong';
            }
            array_push($data_kerjasama_upload, array(
                'ID_MHS' => $data_mhs['ID_MHS'],
                'NIM_MHS' => $nim,
                'NAMA' => $data_mhs['NM_PENGGUNA'],
                'STATUS_MAHASISWA' => $status_mahasiswa,
                'STATUS_BEASISWA' => $status_beasiswa
            ));
        }
        $_SESSION['data_kerjasama'] = $data_kerjasama_upload;
        $_SESSION['semester_mulai'] = post('semester_mulai');
        $_SESSION['semester_selesai'] = post('semester_selesai');
        $_SESSION['beasiswa'] = post('beasiswa');
        $_SESSION['keterangan'] = post('keterangan');
        $smarty->assign('data_kerjasama', $_SESSION['data_kerjasama']);
    }
    $smarty->assign('status_upload', $status_upload);
}

$smarty->assign('data_semester', $utility->get_semester_all());
$smarty->assign('data_beasiswa', $master->load_kerjasama());
$smarty->display('utility/upload-kerjasama.tpl');
?>
