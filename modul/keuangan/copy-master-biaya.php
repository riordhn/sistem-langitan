<?php

include 'config.php';
include 'class/master.class.php';
include 'class/list_data.class.php';

$biaya = new master($db);
$list = new list_data($db);

$id_pt = $id_pt_user;

$smarty->assign('data_biaya_kuliah', $biaya->get_biaya_kuliah(get('id_biaya_kuliah')));
$smarty->assign('data_jalur', $list->load_list_jalur());
$smarty->assign('data_jenjang', $list->load_list_jenjang());
$smarty->assign('data_semester', $list->load_list_semester());
$smarty->assign('data_kelompok_biaya', $list->load_list_kelompok_biaya());
$smarty->assign('data_fakultas', $list->load_list_fakultas($id_pt));
$smarty->display("master/copy-master-biaya.tpl");
?>
