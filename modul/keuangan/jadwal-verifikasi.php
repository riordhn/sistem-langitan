<?php

include 'config.php';
include 'class/pendaftaran.class.php';
include('../ppmb/class/Penerimaan.class.php');

$penerimaan = new Penerimaan($db);

$pendaftaran = new pendaftaran($db);

if (isset($_GET['mode'])) {
    if (get('mode') == 'add') {
        $smarty->assign('penerimaan_set', $penerimaan->GetAllForView());
        $smarty->display("pendaftaran/tambah-jadwal-verifikasi.tpl");
    } elseif (get('mode') == 'edit') {
        $smarty->assign('penerimaan_set', $penerimaan->GetAllForView());
        $smarty->assign('data_jadwal_one', $pendaftaran->get_jadwal_verifikasi_keuangan(get('id')));
        $smarty->display("pendaftaran/edit-jadwal-verifikasi.tpl");
    }
} else {
    if (isset($_POST)) {
        if (post('mode') == 'add') {
            $pendaftaran->tambah_jadwal_verifikasi_keuangan(post('penerimaan'), post('tanggal'), post('status'), post('quota'));
        } else if (post('mode') == 'edit') {
            $pendaftaran->update_jadwal_verifikasi_keuangan(post('id_jadwal'), post('penerimaan'), post('tanggal'), post('status'), post('quota'));
        }
    }
    $smarty->assign('data_jadwal', $pendaftaran->load_jadwal_verifikasi_keuangan());
    $smarty->display("pendaftaran/jadwal-verifikasi.tpl");
}
?>
