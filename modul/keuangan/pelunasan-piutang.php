<?php

include 'config.php';
include 'class/list_data.class.php';
include 'class/piutang.class.php';
$list = new list_data($db);
$piutang = new piutang($db);

$id_pt = $id_pt_user;

if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        $smarty->assign('data_pelunasan_piutang', $piutang->load_data_pelunasan_piutang(get('fakultas'), get('prodi'), get('status'), get('semester_pelunasan')));
    }
}

$smarty->assign('data_prodi', $list->load_list_prodi($id_pt, get('fakultas')));
$smarty->assign('data_fakultas', $list->load_list_fakultas($id_pt));
$smarty->assign('data_status', $piutang->load_list_status());
$smarty->assign('data_semester', $list->load_list_semester());
$smarty->display('laporan/pelunasan-piutang.tpl');
?>
