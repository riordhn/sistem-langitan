<?php

include 'config.php';
include 'class/laporan.class.php';
include 'class/list_data.class.php';
include 'class/master.class.php';
include 'class/paging.class.php';

$id_pt = $id_pt_user;

$laporan = new laporan($db);
$list = new list_data($db);
$master = new master($db);

if (isset($_GET['mode'])) {
    $smarty->assign('data_status_bayar', $laporan->load_data_status_pembayaran(get('fakultas'), get('prodi'), get('semester'), get('status'), get('jalur'), get('angkatan'), get('jenjang')));
    $smarty->assign('data_fakultas_one', $list->get_fakultas(get('fakultas')));
    $smarty->assign('data_prodi_one', $list->get_prodi(get('prodi')));
    $smarty->assign('data_prodi', $list->load_list_prodi($id_pt, get('fakultas')));
}

$smarty->assign('data_angkatan', $list->load_angkatan_mhs());
$smarty->assign('data_fakultas', $list->load_list_fakultas($id_pt));
$smarty->assign('data_status', $master->load_status_pembayaran());
$smarty->assign('data_semester', $list->load_list_semester());
$smarty->assign('data_jalur', $list->load_list_jalur());
$smarty->assign('data_jenjang', $list->load_list_jenjang());
$smarty->display('kontrol/kontrol-status.tpl');
?>
