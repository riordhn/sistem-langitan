<?php
require_once('config.php');

$location = explode("-", $_GET['location']);

foreach ($xml_menu->tab as $tab)
{
    // Mencari tab yang dipilih
    if ($tab->id == $location[0])
    {
    	$data = array();
        
    	foreach ($tab->menu as $menu)
        {
        	array_push($data, array(
        		'PAGE'  => $menu->page,
                'TITLE' => $menu->title,
                'TAB'   => $tab->id,
                'MENU'	=> $menu->id
        	));
        }
        
        $smarty->assign('menus', $data);
        
        break;
    }
}

$smarty->display('menu.tpl');
?>