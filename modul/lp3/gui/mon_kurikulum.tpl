{literal}
<script type="text/javascript">

function ambil(ID_FAKULTAS){
		$.ajax({
		type: "POST",
        url: "mon_kurikulum.php",
        data: "kdfak="+$("#id_fakultas").val(),
        cache: false,
        success: function(data){
			$('#content').html(data);
        }
        });
	}
	

</script>
{/literal}

<div class="center_title_bar">Daftar Kurikulum </div>
<form action="mon_kurikulum.php" method="post">
<table class="tb_frame" width="80%" border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td>Fakultas :
	   <select name="kdfak" id="id_fakultas" onChange="ambil()">
	   <option value='ALLF'>-- Semua Fakultas --</option>
	   {foreach item="list" from=$T_FAK}
	   {html_options  values=$list.ID_FAKULTAS output=$list.NM_FAKULTAS selected=$DFAK}
	   {/foreach}
	   </select>
	
	Prodi :
	   <select name="kdprodi">
	   <option value='ALLP'>-- SEMUA PRODI --</option>
	   {foreach item="list" from=$T_PRODI}
	   {html_options  values=$list.ID_PROGRAM_STUDI output=$list.PRODI selected=$DPRODI}
	   {/foreach}
	   </select>
	   <input type="submit" name="View" value="View">
	</td>
   </tr>
  

{if $cek=="cek"}
{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[4,0],[2,0]],
		}
	);
}
);
</script>
{/literal}

<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr><th colspan="7"><em>{$prodi}</em></th></tr>
	<tr>
		<th>Kurikulum</th>
		<th>Kode</th>
		<th width="35%">Nama Mata Ajar</th>
		<th>SKS</th>
		<th>SMT</th>
		<th>Status LP3</th>
		<th>Prasyarat</th>
	</tr>
	</thead>
	<tbody>
	{foreach name=test item="list" from=$T_MK}
	<tr>
		<td>{$list.TAHUN_KURIKULUM}</td>
		<td>{$list.KD_MATA_KULIAH}</td>
		<td>{$list.NM_MATA_KULIAH}</td>
		<td><center>{$list.KREDIT_SEMESTER}</center></td>
		<td><center>{$list.TINGKAT_SEMESTER}</center></td>
		<td>{$list.STATUS_LP3}</td>
		<td>{$list.MKSYARAT}</td>
	</tr>
	{foreachelse}
	<tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>

{else}
{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[0,0],[1,0],[3,0]],
		}
	);
}
);
</script>
{/literal}
</table>
</form>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
		<th>Fakultas</th>
		<th>Prodi</th>
		<th>Nama kurikulum</th>
		<th>Tahun</th>
		<th>Status</th>
		<th>NO SK</th>
		<th>Berlaku</th>
	</tr>
	</thead>
	<tbody>
  {foreach item="list" from=$T_KUR}
    {if $list.STATUS == "Aktif"}
		<tr bgcolor="yellow">
	{else}
		<tr>
	{/if}
	<td>{$list.NM_FAKULTAS}</td>
    <td>{$list.NM_PROGRAM_STUDI}</td>
    <td><a href="mon_kurikulum.php?action=cek&id_kurikulum={$list.ID_KURIKULUM_PRODI}&prodi={$list.NM_PROGRAM_STUDI}">{$list.NM_KURIKULUM}</a></td>
    <td>{$list.TAHUN_KURIKULUM}</td>
    <td>{$list.STATUS}</td>
    <td>{$list.NOMOR_SK_KURIKULUM}</td>
    <td>{$list.MASA}</td>
	</tr>
  {foreachelse}
	<tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
  {/foreach}
  </tbody>
</table>
{/if}

