{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[0,0],[0,1]],
		}
	);
}
);

</script>
{/literal}

<div class="center_title_bar">CEK / MONITORING KODE </div>
<form action="cek_kode.php" method="post">
<table class="tb_frame" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td>
			 	Masukkan KODE / Nama Mata Ajar :
                <input type="text" name="key" id="key" size="40" value="All" />
			   <input type="submit" name="View" value="View">
		     </td>
           </tr>
</table>
</form>

<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
		<th>Kode</th>
		<th width="35%">Nama Mata Ajar</th>
		<th>SKS</th>
		<th>Pengguna</th>
	</tr>
	</thead>
	<tbody>
	{foreach name=test item="list" from=$kode_mk}
	<tr>
		<td>{$list.KD_MATA_KULIAH}</td>
		<td>{$list.NM_MATA_KULIAH}</td>
		<td><center>{$list.KREDIT_SEMESTER}</center></td>
		<td>{$list.PRODI_PENGGUNA}</td>
	</tr>
	{foreachelse}
	<tr><td colspan="5"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
