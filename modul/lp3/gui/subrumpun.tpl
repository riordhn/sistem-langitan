
  
<div class="center_title_bar">Master Sub Rumpun Ilmu</div>
{literal}
  <style>
	input, select{
	height:25px;}
  </style>
  <script>
		$('.chosen-select').chosen();
  </script>
{/literal}

	<div id="tabs">
		<div id="tab1" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
		<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Input</div>
		<div id="tab3" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Update</div>
   	</div>
    <div class="tab_bdr"></div>
	
<div class="panel" id="panel1" style="display: {$disp1}">    
   <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="left_menu">
    <td width="4%" bgcolor="#333333"><font color="#FFFFFF">No</font></td>
	<td width="25%" bgcolor="#333333" nowrap><font color="#FFFFFF">Rumpun Ilmu</font></td>
	<td width="6%" bgcolor="#333333" nowrap><font color="#FFFFFF">Kode Sub Rumpun</font></td>
    <td width="25%" bgcolor="#333333" nowrap><font color="#FFFFFF">Nama Sub Rumpun</font></td>
	<td width="20%" bgcolor="#333333"><font color="#FFFFFF">Keterangan</font></td>
	<td width="5%" bgcolor="#333333" nowrap><font color="#FFFFFF">Aksi</font></td>
  </tr>
  {foreach name=test item="list" from=$T_SUBRUMPUN}
  <tr>
    <td >{$smarty.foreach.test.iteration}</td>
	<td >{$list.NM_RUMPUN_ILMU}</td>
    <td nowrap>{$list.KD_SUB_RUMPUN_ILMU}</td>
	<td nowrap>{$list.NM_SUB_RUMPUN_ILMU}</td>
	<td >{$list.KETERANGAN}</td>
    <td nowrap><a href="subrumpun.php?action=update&id_sub_rumpun={$list.ID_SUB_RUMPUN_ILMU}">Update</a> | <a href="subrumpun.php?action=del&id_sub_rumpun={$list.ID_SUB_RUMPUN_ILMU}">Delete</a></td>
  </tr>
     {foreachelse}
        <tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
</table>
<br>
</div>
		
    <div class="panel" id="panel2" style="display:{$disp2} ">
	  			 <form action="subrumpun.php" method="post" >
				  <input type="hidden" name="action" value="add" >
				  <table class="tb_frame" width="80%" border="0" cellspacing="0" cellpadding="0">
					<tr>
					  <td width="20%">Kode Rumpun Ilmu</td>
					  <td width="2%">:</td>
					  <td width="78%">
					  
							<select class="chosen-select" multiple style="width:98%;" tabindex="4" name="rumpun_ilmu" data-placeholder="Pilih Rumpun Ilmu..." >
								{foreach item="rumpun_ilmu" from=$T_RUMPUN}
								{html_options values=$rumpun_ilmu.ID_RUMPUN_ILMU output=$rumpun_ilmu.DETAIL}
							{/foreach}
							</select>   
					  </td>
					</tr>
                    <tr>
                      <td>Nama Sub Rumpun Ilmu</td>
                      <td>:</td>
                      <td><input type="text" name="nm_sub_rumpun" id="nm_sub_rumpun" size="40" /></td>
                    </tr>
					<tr>
                      <td>Kode Sub Rumpun Ilmu</td>
                      <td>:</td>
                      <td><input type="text" name="kd_sub_rumpun" id="kd_sub_rumpun" size="40" /></td>
                    </tr>
                    <tr>
                      <td>Keterangan</td>
                      <td>:</td>
                      <td><input type="text" name="keterangan" id="keterangan" size="80" /></td>
                    </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td><input type="submit" name="ttambah" value="Simpan" id="ttambah"></td>
					</tr>
		  		  </table>
			</form>	
</div>
	      <div class="panel" id="panel3" style="display: {$disp3} ">
		  <form action="subrumpun.php" method="post" >
		  		  <input type="hidden" name="action" value="proses" >
				  <table class="tb_frame" width="80%" border="0" cellspacing="0" cellpadding="0">
				{foreach item="datarumpun" from=$T_SUBRUMPUN1}
					<input type="hidden" name="id_sub_rumpun_ilmu" value="{$datarumpun.ID_SUB_RUMPUN_ILMU}" >
					<tr>
					  <td width="20%">Kode Rumpun Ilmu</td>
					  <td width="2%">:</td>
					  <td width="78%">
					  
							<select class="chosen-select" multiple style="width:98%;" tabindex="4" name="rumpun_ilmu" data-placeholder="Pilih Rumpun Ilmu..." >
								{foreach item="rumpun_ilmu" from=$T_RUMPUN}
								{html_options values=$rumpun_ilmu.ID_RUMPUN_ILMU output=$rumpun_ilmu.DETAIL selected=$datarumpun.ID_RUMPUN_ILMU}
							{/foreach}
							</select>   
					  </td>
					</tr>
                    <tr>
                      <td>Nama Sub Rumpun Ilmu</td>
                      <td>:</td>
                      <td><input type="text" name="nm_sub_rumpun" id="nm_sub_rumpun" size="40" value="{$datarumpun.NM_SUB_RUMPUN_ILMU}" /></td>
                    </tr>
					<tr>
                      <td>Kode Sub Rumpun Ilmu</td>
                      <td>:</td>
                      <td><input type="text" name="kd_sub_rumpun" id="kd_sub_rumpun" size="40" value="{$datarumpun.KD_SUB_RUMPUN_ILMU}"/></td>
                    </tr>
                    <tr>
                      <td>Keterangan</td>
                      <td>:</td>
                      <td><input type="text" name="keterangan" id="keterangan" size="80" value="{$datarumpun.KETERANGAN}" /></td>
                    </tr>

					
				{/foreach}
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					 <td><input type="submit" name="tambah" value="Update"></td>

					</tr>
				  </table>
	    </form>			
</div>	
<script>$('form').validate();</script>
	  		
   