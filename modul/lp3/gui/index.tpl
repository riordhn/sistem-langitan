<html>
    <head>
        <title>LP3 - {$nama_pt}</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" type="text/css" href="../../css/lp3.css" />
		<link rel="stylesheet" type="text/css" href="includes/jquery-ui.min.css" />
        <link rel="stylesheet" type="text/css" href="../../css/jquery-ui-ppm.custom.css" />
		<link rel="stylesheet" type="text/css" href="../../css/reset.css" />
        <link rel="stylesheet" type="text/css" href="../../css/text.css" />
        <link rel="stylesheet" href="../dosen/js/plugins/chosen/chosen.css">	
		<link rel="stylesheet" type="text/css" href="includes/sortable/themes/green/style.css" />
		
		

       
</head>
    <body>
	  </div>
        <table class="clear-margin-bottom">
            <colgroup>
                <col />
                <col class="main-width"/>
                <col />
            </colgroup>
            <thead>
                <tr>
                    <td class="header-left"></td>
                  <td class="header-center" style="background-image: url('../../img/header/lp3-{$nama_singkat}.png')"> 
			        <div align="right" >
				      <span style="font-size:32px">
					  </span>
					  <br>
				      <span style="font-size:32px">LP3 - {$nama_pt}</span> 
					 </div>
				  </td>
					<td class="header-right"></td>
                </tr>
                <tr>
                    <td class="tab-left"></td>
                    <td class="tab-center">
                        <ul>
                        {foreach $struktur_menu as $m}
                            {if $m.AKSES == 1}
                                <li><a rel="{$m.NM_MODUL}" href="{$m.PAGE}">{$m.TITLE}</a></li>
                                <li class="divider"></li>
                            {/if}
                        {/foreach}
                            <li><a class="disable-ajax" href="../../logout.php">Logout</a></li>
							
                        </ul>
                    </td>
                    <td class="tab-right"></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="body-left">&nbsp;</td>
                    <td class="body-center">
                        <table class="content-table">
                            <colgroup>
                                <col />
                                <col />
                            </colgroup>
                            <tr>
                                <td colspan="2" id="breadcrumbs" class="breadcrumbs">Navigation : <a href="#">Tab</a> / <a href="#">Menu</a></td>
                            </tr>
                            <tr>
                                <td id="menu" class="menu">
                                    
                                </td>
                                <td id="content" class="content">Content</td>
                            </tr>
                        </table>
                    </td>
                    <td class="body-right">&nbsp;</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td class="foot-left">&nbsp;</td>
                    <td class="foot-center">
                        <div class="footer-nav">
                            <a href="#">Home</a> | <a href="#">About</a> | <a href="#">Sitemap</a> | <a href="#">RSS</a> | <a href="#">Contact Us</a>
                        </div>
                        <div class="footer">Copyright &copy; 2015 - {$nama_pt} <br />Sistem Langitan NU <br />oleh <a target="_blank" href="http://unair.ac.id" class="disable-ajax">Universitas Airlangga</a></div>
                    </td>
                    <td class="foot-right">&nbsp;</td>
                </tr>
            </tfoot>
        </table>
        <script type="text/javascript" src="../../js/jquery-1.5.1.js"></script>
            
        <script type="text/javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
        
        <script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="js/lp3.js"></script>

        <script type="text/javascript" src="gui/tab.js"></script>
        <script language="javascript" type="text/javascript" src="gui/datetimepicker.js"></script>
        <script src="../dosen/js/plugins/chosen/chosen.jquery.js" type="text/javascript"></script>
        <script src="../dosen/js/plugins/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
        <script type="text/javascript" src="includes/sortable/jquery.tablesorter.min.js"></script>

    </body>
</html>