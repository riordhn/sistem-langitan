
  
<div class="center_title_bar">Insert Kode Baru [Profesi]</div>
{literal}
  <style>
	input, select{
	height:25px;}
  </style>
  <script>
		$('.chosen-select').chosen();
  </script>
{/literal}

	<div id="tabs">
		<div id="tab1" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
		<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Input</div>
		<div id="tab3" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Update</div>
   	</div>
    <div class="tab_bdr"></div>
	
<div class="panel" id="panel1" style="display: {$disp1}">
{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[4,0],[0,0]],
		headers: {
            7: { sorter: false }
		}
		}
		
	);
}
);
</script>
{/literal}
    
   <table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
   <thead>
	<tr>
		<th>Kode</th>
		<th width="35%">Nama Mata Ajar</th>
		<th>Nama Rumpun</th>
		<th>Nama Sub Rumpun</th>
		<th>Strata</th>
		<th>Nomor</th>
		<th>Aksi</th>
	</tr>
	</thead>
	<tbody>
  {foreach name=test item="list" from=$T_RINCIAN}
  <tr>
	<td >{$list.KD_MATA_KULIAH}</td>
	<td >{$list.NM_MATA_KULIAH}</td>
	<td >{$list.NM_RUMPUN_ILMU}</td>
	<td >{$list.NM_SUB_RUMPUN_ILMU}</td>
	<td >{$list.NM_STRATA_MK}</td>
	<td >{$list.NOMOR}</td>
    <td ><a href="insert_kode_baru_spesialis.php?action=update&id_mata_kuliah={$list.ID_MATA_KULIAH}">Update</a> | 
		 <a href="insert_kode_baru_spesialis.php?action=del&id_mata_kuliah={$list.ID_MATA_KULIAH}">Delete</a></td>
  </tr>
     {foreachelse}
        <tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
	</tbody>
</table>
<br>
</div>
		
    <div class="panel" id="panel2" style="display:{$disp2} ">
	  			 <form action="insert_kode_baru_spesialis.php" method="post" >
				  <input type="hidden" name="action" value="add" >
				  <table class="tb_frame" width="80%" border="0" cellspacing="0" cellpadding="0">
					<tr>
					  <td width="20%">Sub Rumpun Ilmu</td>
					  <td width="2%">:</td>
					  <td width="78%">
					  
							<select class="chosen-select" multiple style="width:98%;" tabindex="4" name="sub_rumpun_ilmu" data-placeholder="Pilih Sub Rumpun Ilmu..." >
								{foreach item="sub_rumpun_ilmu" from=$T_RUMPUN}
								{html_options values=$sub_rumpun_ilmu.ID_SUB_RUMPUN_ILMU output=$sub_rumpun_ilmu.DETAIL}
							{/foreach}
							</select>   
					  </td>
					</tr>
					<tr>
					  <td width="20%">Strata</td>
					  <td width="2%">:</td>
					  <td width="78%">
					  
							<select class="chosen-select" multiple style="width:98%;" tabindex="4" name="strata" data-placeholder="Pilih Strata Mata Ajar..." >
								{foreach item="strata" from=$T_STRATA}
								{html_options values=$strata.KD_STRATA_MK output=$strata.NM_STRATA_MK}
							{/foreach}
							</select>   
					  </td>
					</tr>
                    <tr>
                      <td>Kode Mata Ajar</td>
                      <td>:</td>
                      <td><input type="text" name="kd_mata_kuliah" id="kd_mata_kuliah" size="40" required /></td>
                    </tr>
					<tr>
                      <td>Nama Mata Ajar</td>
                      <td>:</td>
                      <td><input type="text" name="nm_mata_ajar" id="nm_mata_ajar" size="40" required/></td>
                    </tr>
                    <tr>
                      <td>Program Studi</td>
                      <td>:</td>
                      <td>
                      	<select  name="program_studi" >
                      		<option value="">-- Pilih Program Studi --</option>
							{foreach item="prodi" from=$T_DAFTAR_PRODI}
								{html_options values=$prodi.ID_PROGRAM_STUDI output=$prodi.NM_PROGRAM_STUDI}
							{/foreach}
							</select>   
                      </td>
                    </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td><input type="submit" name="ttambah" value="Simpan" id="ttambah"></td>
					</tr>
		  		  </table>
			</form>	
</div>
<div class="panel" id="panel3" style="display: {$disp3} ">
		  <form action="insert_kode_baru_spesialis.php" method="post" >
		  		  <input type="hidden" name="action" value="proses" >
				  <table class="tb_frame" width="80%" border="0" cellspacing="0" cellpadding="0">
				{foreach item="datarumpun" from=$T_MATA_KULIAH}
					<input type="hidden" name="id_mata_kuliah" value="{$datarumpun.ID_MATA_KULIAH}" >
					<tr>
					  <td width="20%">Sub Rumpun Ilmu</td>
					  <td width="2%">:</td>
					  <td width="78%">
					  
							<select class="chosen-select" multiple style="width:98%;" tabindex="4" name="sub_rumpun_ilmu" data-placeholder="Pilih Sub Rumpun Ilmu..." >
								{foreach item="sub_rumpun_ilmu" from=$T_RUMPUN}
								{html_options values=$sub_rumpun_ilmu.ID_SUB_RUMPUN_ILMU output=$sub_rumpun_ilmu.DETAIL selected=$datarumpun.ID_SUB_RUMPUN_ILMU}
							{/foreach}
							</select>   
					  </td>
					</tr>
					<tr>
					  <td width="20%">Strata</td>
					  <td width="2%">:</td>
					  <td width="78%">
					  
							<select class="chosen-select" multiple style="width:98%;" tabindex="4" name="strata" data-placeholder="Pilih Strata Mata Ajar..." >
								{foreach item="strata" from=$T_STRATA}
								{html_options values=$strata.KD_STRATA_MK output=$strata.NM_STRATA_MK selected=$datarumpun.STRATA}
							{/foreach}
							</select>   
					  </td>
					</tr>
                    <tr>
                      <td>Kode Mata Ajar</td>
                      <td>:</td>
                      <td><input type="text" name="kd_mata_kuliah" id="kd_mata_kuliah" value="{$datarumpun.KD_MATA_KULIAH}" size="40" required/></td>
                    </tr>
					<tr>
                      <td>Nama Mata Ajar</td>
                      <td>:</td>
                      <td><input type="text" name="nm_mata_ajar" id="nm_mata_ajar" value="{$datarumpun.NM_MATA_KULIAH}" size="40" required/></td>
                    </tr>
                    <tr>
                      <td>Program Studi</td>
                      <td>:</td>
                      <td>
                      	<select  name="program_studi">
                      		{foreach item="prodi" from=$T_DAFTAR_PRODI}
								{html_options values=$prodi.ID_PROGRAM_STUDI output=$prodi.NM_PROGRAM_STUDI selected=$datarumpun.ID_PROGRAM_STUDI}
							{/foreach}
							</select>   
                      </td>
                    </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					 <td><input type="submit" name="tambah" value="Update"></td>
					</tr>
				{/foreach}
				</table>
	    </form>			
</div>	
<script>$('form').validate();</script>
	  		
   