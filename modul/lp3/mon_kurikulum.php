<?php
require('common.php');

$id_perguruan_tinggi = $user->ID_PERGURUAN_TINGGI;
$fakultas=$db->QueryToArray("select * from fakultas where id_perguruan_tinggi = '{$id_perguruan_tinggi}' order by nm_fakultas");
$smarty->assign('T_FAK', $fakultas);

$kdfak=$_POST['kdfak'];
$smarty->assign('DFAK', $kdfak);

$prodi=$db->QueryToArray("select id_program_studi,nm_jenjang||'-' || nm_program_studi as prodi from program_studi 
			left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_fakultas=$kdfak 
			and status_aktif_prodi = 1 
			order by program_studi.id_jenjang");
$smarty->assign('T_PRODI', $prodi);

$kdprodi=$_POST['kdprodi'];
$smarty->assign('DPRODI', $kdprodi);

if ($_GET['action']=='cek')
{
$smarty->assign('cek', $_GET['action']);
$smarty->assign('prodi', $_GET['prodi']);
$id_kurikulum_prodi=$_GET['id_kurikulum'];
$mk=$db->QueryToArray("select kmk.id_kurikulum_mk,mk1.kd_mata_kuliah,mk1.nm_mata_kuliah,
						kmk.kredit_tatap_muka, kmk.kredit_praktikum,kmk.kredit_semester,kmk1.tingkat_semester,
						nm_status_mk,nm_kelompok_mk,kurikulum_prodi.tahun_kurikulum, kmk.tingkat_semester,prasyarat_mk.id_prasyarat_mk,
						case when trim(mk1.kd_mata_kuliah) in (select trim(kd_mata_kuliah) from mata_kuliah_lp3) then 'Valid' else 'Blm Valid' end as status_lp3,
						wm_concat(mk.kd_mata_kuliah) as mksyarat
						from kurikulum_mk kmk 
						left join mata_kuliah mk1 on kmk.id_mata_kuliah=mk1.id_mata_kuliah 
						left join status_mk on kmk.id_status_mk=status_mk.id_status_mk 
						left join kelompok_mk on kmk.id_kelompok_mk=kelompok_mk.id_kelompok_mk 
						left join kurikulum_prodi on kmk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
						left join prasyarat_mk on kmk.id_kurikulum_mk=prasyarat_mk.id_kurikulum_mk
						left join group_prasyarat_mk on prasyarat_mk.id_prasyarat_mk=group_prasyarat_mk.id_prasyarat_mk
						left join kurikulum_mk kmk1 on group_prasyarat_mk.id_kurikulum_mk=kmk1.id_kurikulum_mk
						left join mata_kuliah mk on kmk1.id_mata_kuliah=mk.id_mata_kuliah
						where kmk.id_kurikulum_prodi=$id_kurikulum_prodi
						group by kmk.id_kurikulum_mk,mk1.kd_mata_kuliah,mk1.nm_mata_kuliah,
						kmk.kredit_tatap_muka, kmk.kredit_praktikum,kmk.kredit_semester,kmk1.tingkat_semester,
						nm_status_mk,nm_kelompok_mk,kurikulum_prodi.tahun_kurikulum, kmk.tingkat_semester,prasyarat_mk.id_prasyarat_mk,
						case when mk1.kd_mata_kuliah in (select kd_mata_kuliah from mata_kuliah_lp3) then 'valid LP3' else '-' end");
$smarty->assign('T_MK', $mk);

}



if ($kdfak =='ALLF' and $kdprodi=='ALLP') {

$kur=$db->QueryToArray("select fakultas.id_fakultas,nm_fakultas,
			a.id_program_studi,nm_jenjang||'-'||nm_program_studi as nm_program_studi, 
			a.id_kurikulum_prodi,nm_kurikulum,
			case when status_aktif=1 then 'Aktif' else 'Tdk Aktif' end as status,
			tahun_kurikulum,nomor_sk_kurikulum,berlaku_mulai||'-'||berlaku_sampai as masa
			from kurikulum_prodi a
			left join kurikulum on a.id_kurikulum=kurikulum.id_kurikulum 
			left join program_studi on a.id_program_studi=program_studi.id_program_studi
			left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
			left join fakultas on program_studi.id_fakultas=fakultas.id_fakultas
			where status_aktif_prodi = 1 and fakultas.id_perguruan_tinggi = '{$id_perguruan_tinggi}'");
			$smarty->assign('T_KUR', $kur);
} 

if ($kdfak !='ALLF' and $kdprodi=='ALLP') {

$kur=$db->QueryToArray("select fakultas.id_fakultas,nm_fakultas,
			a.id_program_studi,nm_jenjang||'-'||nm_program_studi as nm_program_studi, 
			a.id_kurikulum_prodi,nm_kurikulum,
			case when status_aktif=1 then 'Aktif' else 'Tdk Aktif' end as status,
			tahun_kurikulum,nomor_sk_kurikulum,berlaku_mulai||'-'||berlaku_sampai as masa
			from kurikulum_prodi a
			left join kurikulum on a.id_kurikulum=kurikulum.id_kurikulum 
			left join program_studi on a.id_program_studi=program_studi.id_program_studi
			left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
			left join fakultas on program_studi.id_fakultas=fakultas.id_fakultas
			where status_aktif_prodi = 1 and fakultas.id_fakultas=$kdfak");
			$smarty->assign('T_KUR', $kur);
}

if ($kdfak !='ALLF' and $kdprodi!='ALLP') {
$kur=$db->QueryToArray("select fakultas.id_fakultas,nm_fakultas,
			a.id_program_studi,nm_jenjang||'-'||nm_program_studi as nm_program_studi, 
			a.id_kurikulum_prodi,nm_kurikulum,
			case when status_aktif=1 then 'Aktif' else 'Tdk Aktif' end as status,
			tahun_kurikulum,nomor_sk_kurikulum,berlaku_mulai||'-'||berlaku_sampai as masa
			from kurikulum_prodi a
			left join kurikulum on a.id_kurikulum=kurikulum.id_kurikulum 
			left join program_studi on a.id_program_studi=program_studi.id_program_studi
			left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
			left join fakultas on program_studi.id_fakultas=fakultas.id_fakultas
			where status_aktif_prodi = 1 and fakultas.id_fakultas=$kdfak 
			and program_studi.id_program_studi=$kdprodi");
			$smarty->assign('T_KUR', $kur);
}  


$smarty->display('mon_kurikulum.tpl');
?>



