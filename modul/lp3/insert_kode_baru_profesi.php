<?php
/*
YAH 06/06/2011
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');

$id_perguruan_tinggi = $user->ID_PERGURUAN_TINGGI;
$id_pengguna= $user->ID_PENGGUNA;
//print_r($user);


$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');


if ($_GET['action']=='del')
{
	$id_mata_kuliah=$_GET['id_mata_kuliah'];
	$db->Query("delete from mata_kuliah where id_mata_kuliah=$id_mata_kuliah");
}

if ($_GET['action']=='update')
{
	$smarty->assign('disp1','none');
	$smarty->assign('disp2','none');
	$smarty->assign('disp3','block');

	$id_mata_kuliah=$_GET['id_mata_kuliah'];
	$mata_kuliah_update=$db->QueryToArray("select * from mata_kuliah where id_mata_kuliah=$id_mata_kuliah");
	
	
	$smarty->assign('T_MATA_KULIAH', $mata_kuliah_update);  

}

if ($_POST['action']=='proses')
{
	
	$smarty->assign('disp1','block');
	$smarty->assign('disp2','none');
	$smarty->assign('disp3','none');

	$id_sub_rumpun_ilmu=$_POST['sub_rumpun_ilmu'] ? $_POST['sub_rumpun_ilmu']:0 ;
	$kd_mata_kuliah=strtoupper($_POST['kd_mata_kuliah']);
	$nm_mata_kuliah=$_POST['nm_mata_ajar'];
	$strata=$_POST['strata'];
	$id_mata_kuliah=$_POST['id_mata_kuliah'];
	$id_program_studi = $_POST['program_studi'];

	$db->Query("update mata_kuliah set id_sub_rumpun_ilmu=$id_sub_rumpun_ilmu,
				kd_mata_kuliah='$kd_mata_kuliah',nm_mata_kuliah='$nm_mata_kuliah',
				petugas=$id_pengguna,strata=$strata,id_program_studi='$id_program_studi' where id_mata_kuliah=$id_mata_kuliah"); 
	
}

if ($_POST['action']=='add')
{
	$id_sub_rumpun_ilmu=$_POST['sub_rumpun_ilmu'] ? $_POST['sub_rumpun_ilmu']:0 ;
	$kd_mata_kuliah=strtoupper($_POST['kd_mata_kuliah']);
	$nm_mata_kuliah=$_POST['nm_mata_ajar'];
	$strata=$_POST['strata'];
	$id_program_studi = $_POST['program_studi'];

	
	//cek
	$db->Query("select count(*) as cek from mata_kuliah
				where upper(trim(kd_mata_kuliah))='$kd_mata_kuliah'
				and flag=1");
	
	$data=$db->FetchAssoc();

	if ($data['CEK'] > 0)
	{
	echo '<script>alert("Maaf Kode Sudah Ada..")</script>';
	}
	else {
	$db->Query("insert into mata_kuliah (id_sub_rumpun_ilmu,kd_mata_kuliah,
				nm_mata_kuliah,keterangan,flag,petugas,strata,id_program_studi) 
				values ($id_sub_rumpun_ilmu,'$kd_mata_kuliah',
				'$nm_mata_kuliah','INSERT LP3',1,$id_pengguna,$strata,$id_program_studi)");
	} 
	
}

$srt=$db->QueryToArray("select kd_strata_mk,kd_strata_mk||' - '||nm_strata_mk as nm_strata_mk 
						from kd_strata_mk where kd_strata_mk in (5)
						order by kd_strata_mk");
$smarty->assign('T_STRATA', $srt);

$rumpun=$db->QueryToArray("select ID_SUB_RUMPUN_ILMU,kd_rumpun_ilmu||kd_sub_rumpun_ilmu||' - '||
							nm_rumpun_ilmu||' '||nm_sub_rumpun_ilmu as detail 
							from SUB_RUMPUN_ILMU
							join RUMPUN_ILMU on SUB_RUMPUN_ILMU.ID_RUMPUN_ILMU=RUMPUN_ILMU.ID_RUMPUN_ILMU
							where rumpun_ilmu.id_perguruan_tinggi = '{$id_perguruan_tinggi}' order by kd_rumpun_ilmu,kd_sub_rumpun_ilmu");
$smarty->assign('T_RUMPUN', $rumpun);

$rincian=$db->QueryToArray("select id_mata_kuliah,kd_mata_kuliah,nm_mata_kuliah,
							sub_rumpun_ilmu.id_rumpun_ilmu,nm_rumpun_ilmu,
							kd_sub_rumpun_ilmu,nm_sub_rumpun_ilmu,SUB_RUMPUN_ILMU.ID_SUB_RUMPUN_ILMU,nm_strata_mk,nomor
							from mata_kuliah
							left join SUB_RUMPUN_ILMU on MATA_KULIAH.ID_SUB_RUMPUN_ILMU=SUB_RUMPUN_ILMU.ID_SUB_RUMPUN_ILMU
							left join rumpun_ilmu on sub_rumpun_ilmu.id_rumpun_ilmu=rumpun_ilmu.id_rumpun_ilmu
							left join kd_strata_mk on MATA_KULIAH.strata=kd_strata_mk
							where (rumpun_ilmu.id_perguruan_tinggi = '{$id_perguruan_tinggi}' or mata_kuliah.id_program_studi in (select id_program_studi from program_studi where id_jenjang = '9' and id_fakultas in(select id_fakultas from fakultas where id_perguruan_tinggi = '{$id_perguruan_tinggi}'))) and flag=1 and kd_strata_mk in (5)");

$smarty->assign('T_RINCIAN', $rincian);  

$daftar_program_studi=$db->QueryToArray("SELECT * FROM program_studi ps WHERE ps.id_jenjang = '9' AND ps.id_fakultas in (SELECT id_fakultas FROM fakultas WHERE id_perguruan_tinggi = '".$PT->ID_PERGURUAN_TINGGI."')");
$smarty->assign('T_DAFTAR_PRODI', $daftar_program_studi);

$smarty->display('insert_kode_baru_profesi.tpl');
?>



