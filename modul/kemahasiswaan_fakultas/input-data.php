<?php

include 'config.php';
include '../kemahasiswaan/class/beasiswa.class.php';

$b = new beasiswa($db, $user->ID_ROLE);

if (isset($_GET)) {
    if (get('cari') != '') {
        $data_mhs = $b->load_biodata_mhs(get('cari'));
        if ($data_mhs != '') {
            if (isset($_POST)) {
                if (post('mode') == 'tambah') {
                    $b->insert_mahasiswa_beasiswa($data_mhs['ID_MHS'], post('beasiswa'), post('tgl_mulai'), post('tgl_selesai'), post('keterangan'), post('status'));
                } else if (post('mode') == 'edit') {
                    $b->update_sejarah_beasiswa(post('id'), post('beasiswa'), post('tgl_mulai'), post('tgl_selesai'), post('keterangan'), post('status'));
                } else if (post('mode') == 'hapus') {
                    $b->delete_sejarah_beasiswa(post('id'));
                }
            }
            if (get('mode') == 'tambah' || get('mode') == 'edit' || get('mode') == 'hapus') {
                $smarty->assign('history', $b->get_sejarah_beasiswa(get('id')));
                $smarty->assign('data_beasiswa', $b->load_beasiswa_fakultas());
            }
            $smarty->assign('data_mhs', $data_mhs);
            $smarty->assign('data_history', $b->load_sejarah_beasiswa(get('cari')));
        }
    }
}

$smarty->display('beasiswa/input-data.tpl');
?>
