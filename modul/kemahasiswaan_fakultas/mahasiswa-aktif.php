<?php

include 'config.php';
include '../pendidikan/class/report_mahasiswa.class.php';
include '../kemahasiswaan/class/laporan.class.php';
include '../dosen/proses/perwalian.class.php';

$rm = new report_mahasiswa($db);
$lap = new laporan($db);
$perwalian = new perwalian($db, $user->ID_PENGGUNA, $login->id_dosen);
if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        $data_prodi = "
            SELECT * FROM PROGRAM_STUDI PS
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            WHERE PS.ID_FAKULTAS='" . get('fakultas') . "'
            ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI
            ";
        $smarty->assign('data_prodi', $db->QueryToArray($data_prodi));
        $smarty->assign('data_mhs', $lap->get_report_mhs_aktif(get('fakultas'), get('prodi'), get('angkatan')));
    } else if (get('mode') == 'detail'){
        $data_biodata = $perwalian->get_biodata_mahasiswa(get('id_mhs'));
        if (file_exists("../../foto_mhs/{$data_biodata['NIM_MHS']}.JPG"))
            $smarty->assign('foto', "/foto_mhs/{$data_biodata['NIM_MHS']}.JPG");
        else if (file_exists("../../foto_mhs/{$data_biodata['NIM_MHS']}.jpg"))
            $smarty->assign('foto', "/modulx/images/{$data_biodata['NIM_MHS']}.jpg");
        $smarty->assign('data_biodata', $data_biodata);
        $smarty->assign('data_mhs_status', $perwalian->load_mhs_status(get('id_mhs')));
    }
}

$angkatan = $db->QueryToArray("SELECT DISTINCT(THN_ANGKATAN_MHS) ANGKATAN FROM AUCC.MAHASISWA 
        WHERE THN_ANGKATAN_MHS BETWEEN (TO_CHAR(SYSDATE,'YYYY')-10) AND TO_CHAR(SYSDATE,'YYYY')
        ORDER BY ANGKATAN");
$smarty->assign('id_fakultas', $user->ID_FAKULTAS);
$smarty->assign('data_fakultas', $rm->get_fakultas());
$smarty->assign('data_jalur', $rm->get_jalur());
$smarty->assign('data_angkatan', $angkatan);

$smarty->display("laporan/mahasiswa-aktif.tpl");
?>
