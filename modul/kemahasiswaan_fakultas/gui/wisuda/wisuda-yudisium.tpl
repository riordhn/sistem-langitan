<div class="center_title_bar">LAPORAN YUDISIUM</div>
<form action="wisuda-yudisium.php" method="post">
	<table width="700">
    	<tr>
        	<td>Status Bayar</td>
            <td>
            	<select name="status_bayar">
                	<option value="1" {if $status_bayar==1} selected="selected" {/if}>Sudah Bayar</option>
                    <option value="2" {if $status_bayar==2} selected="selected" {/if}>Belum Bayar</option>
					<option value="3" {if $status_bayar==3} selected="selected" {/if}>Yudisium</option>
                    <option value="4" {if $status_bayar==4} selected="selected" {/if}>Batal Yudisium</option>
            	</select>
            </td>
            <td>Periode Yudisium</td>
            <td>
            	<select name="periode">
					<option value="">- PILIH PERIODE -</option>
                	{foreach $periode as $data}
                	<option value="{$data.ID_TARIF_WISUDA}" {if $id_periode==$data.ID_TARIF_WISUDA} selected="selected" {/if}>{$data.NM_TARIF_WISUDA}</option>
					{/foreach}
            	</select>
            </td>
            
            <td>Jenjang</td>
            <td>
            	<select name="jenjang">
					<option value="">Semua Jenjang</option>
                	{foreach $jenjang as $data}
                	<option value="{$data.ID_JENJANG}" {if $smarty.request.jenjang==$data.ID_JENJANG} selected="selected" {/if}>{$data.NM_JENJANG}</option>
					{/foreach}
            	</select>
            </td>
        </tr>
        <tr>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>
{if isset($pengajuan)}
<table style="font-size:12px">
<tr>
	<th>NO</th>
	<th>NIM</th>
    <th>NAMA</th>
    <th>PROGRAM STUDI</th>
	<th>No Ijasah</th>
	<th>Tgl Lulus</th>
	<th>Bayar</th>
	<th>Biodata</th>
	<th>Evaluasi</th>
	<th>Upload File</th>
	<th>Abstrak dan Judul</th>
    <th>Foto</th>
    <th>IPK</th>
    <th>SKS</th>
    <th>SKP</th>
    <th>ELPT</th>
    <th>Lama Studi</th>
    <th>Reguler/ Alih Jenis</th>
</tr>
	{$no = 1}
	{foreach $pengajuan as $data}
    	{if ($no % 2) == 0}
		 	{$warna = "#CCCCCC"}
		{else}
		 	{$warna = ""}
		{/if}
    	<tr bgcolor="{$warna}">
        	<td>{$no++}</td>
            <td>{$data.NIM_MHS}</td>
            <td>{$data.NM_PENGGUNA}</td>
            <td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
			<td>{$data.NO_IJASAH}</td>
			<td>{$data.TGL_LULUS_PENGAJUAN}</td>
			<td>{if $data.TGL_BAYAR <> ''}Sudah{else}Belum{/if}</td>
			<td>{if $data.BIODATA <> 0 or $data.BIODATA <> ''}Sudah{else}Belum{/if}</td>
			<td>{if $data.EVA_HASIL <> 0 or $data.EVA_HASIL <> ''}Sudah{else}Belum{/if}</td>
			<td>{if $data.STAT_UPLOAD <> '' and $data.STAT_UPLOAD <> ''}Sudah{else}Belum{/if}</td>
			<td>{if $data.ABSTRAK_TA_CLOB <> '' and $data.JUDUL_TA <> ''}Sudah{else}Belum{/if}</td>
            <td>
            	{capture assign='foto'}../../foto_wisuda/{$data.NIM_MHS}.jpg{/capture}
                {if $foto|file_exists eq ''} 
  					Belum
                {else}
                	Sudah 
				{/if}
            </td>
            <td>{$data.IPK}</td>
            <td>{$data.SKS}</td>
            <td>{$data.TOTAL_SKP}</td>
            <td>{$data.ELPT}</td>
            <td>{$data.LAMA_STUDI}</td>
             <td>{$data.STATUS}</td>
        </tr>
    {/foreach}
</table>
{elseif isset($pengajuan_bayar)}
<table style="font-size:12px">
<tr>
	<th>NO</th>
	<th>NIM</th>
    <th>NAMA</th>
    <th>TTL</th>
	<th>KELAMIN</th>
    <th>FAKULTAS</th>
    <th>PROGRAM STUDI</th>
	<th>TGL LULUS</th>
	<th>NO IJASAH</th>
	<th>Tanggal Bayar</th>
	<th>Jumlah Bayar</th>
	<th>Jumlah Cicilan</th>	
	<th>Foto</th>
    <th>IPK</th>
    <th>SKS</th>
    <th>SKP</th>
    <th>ELPT</th>
    <th>Lama Studi</th>
     <th>Reguler/ Alih Jenis</th>
</tr>
	{$no = 1}
	{foreach $pengajuan_bayar as $data}
    	{if ($no % 2) == 0}
		 	{$warna = "#CCCCCC"}
		{else}
		 	{$warna = ""}
		{/if}
    	<tr bgcolor="{$warna}">
        	<td>{$no++}</td>
            <td>{$data.NIM_MHS}</td>
            <td>{$data.NM_PENGGUNA}</td>
            <td>{$data.LAHIR_IJAZAH}</td>
			<td>{$data.KELAMIN_PENGGUNA}</td>
            <td>{$data.NM_FAKULTAS}</td>
            <td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
			<td>{$data.TGL_LULUS_PENGAJUAN}</td>
			<td>{$data.NO_IJASAH}</td>
			<td>{$data.TGL_BAYAR}</td>
			<td style="text-align:right">{if $data.TARIF == $data.CICILAN}0{else}{$data.BESAR_BIAYA|number_format:0:"":"."}{/if}</td>
			<td style="text-align:right">{$data.CICILAN|number_format:0:"":"."}</td>
			<td>
            	{capture assign='foto'}../../foto_wisuda/{$data.NIM_MHS}.jpg{/capture}
                {if $foto|file_exists eq ''} 
  					Belum
                {else}
                	Sudah 
				{/if}
            </td>
            <td>{$data.IPK}</td>
            <td>{$data.SKS}</td>
            <td>{$data.TOTAL_SKP}</td>
            <td>{$data.ELPT}</td>
            <td>{$data.LAMA_STUDI}</td>
            <td>{$data.STATUS}</td>
        </tr>
    {/foreach}
</table>
{/if}