<div class="center_title_bar">ABSENSI DAN URUTAN DAFTAR NAMA WISUDAWAN</div>  
<form action="wisuda-absensi.php" method="post" id="f2">
	<table width="700">
    	<tr>
            <td>Periode Wisuda</td>
            <td>
            	<select name="periode">
                	{foreach $periode as $data}
                	<option value="{$data.ID_TARIF_WISUDA}" {if $smarty.post.periode==$data.ID_TARIF_WISUDA} selected="selected" {/if}>{$data.NM_TARIF_WISUDA}
					</option>
					{/foreach}
            	</select>
            </td>
			<td>Tanggal Wisuda</td>
			<td><input type="text" id="tgl_wisuda" value="{$tgl_wisuda}" name="tgl_wisuda" /></td>
		
			<td>Jenjang</td>
			<td>
				<select id="jenjang" name="jenjang" class="required">
                    <option value="">Pilih Jenjang</option> 
                   {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $smarty.post.jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}                    
                </select>
			</td>
		</tr>
        <tr>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>

{if isset($absensi)}
<form action="wisuda-absensi.php" name="f1" method="post">
<table style="font-size:12px">
	<tr>
		<th>NO</th>
		<th>NIM</th>
		<th>NAMA</th>
		<th>FAKULTAS</th>
		<th>PROGRAM STUDI</th>
        <th>TGL WISUDA</th>
		<th><input type="checkbox" id="cek_all" name="cek_all" /></th>
	</tr>
	{$no=1}
	{foreach $absensi as $data}
		{if ($no % 2) == 0}
		 	{$warna = "#CCCCCC"}
		{else}
		 	{$warna = ""}
		{/if}
    	<tr bgcolor="{$warna}">
			<td>{$no++}</td>
			<td>{$data.NIM_MHS}</td>
			<td>{$data.NM_PENGGUNA}</td>
			<td>{$data.NM_FAKULTAS}</td>
			<td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
            <td>{$data.TGL_WISUDA}</td>
			<td style="text-align:center">
				<input type="checkbox" class="cek" name="cek{$no}" {if ($data.TGL_WISUDA == $tgl_wisuda) || $data.TGL_WISUDA != ''}checked="checked"{/if} value="1" />
				<input type="hidden" value="{$data.ID_PEMBAYARAN_WISUDA}" name="id{$no}" />
			</td>
		</tr>
		
	{/foreach}
    <tr>
		<td colspan="7" style="text-align:center">
    		<input type="submit" value="Simpan" />
        </td>
    </tr>
	<tr>
		<td colspan="7" style="text-align:center"> 
			<input type="hidden" value="{$no}" name="no" />
			<input type="hidden" value="{$smarty.post.periode}" name="periode" />
            <input type="hidden" value="{$tgl_wisuda}" name="tgl_wisuda" />
			<input type="hidden" value="{$id_fakultas}" name="fakultas" />
			<input type="hidden" value="{$smarty.post.jenjang}" name="jenjang" />
			<input type="hidden" value="{$smarty.post.program_studi}" name="program_studi" />
			<input type="button" value="Cetak Panggilan MC" onclick="window.open('cetak-wisuda-mc.php?periode={$smarty.post.periode}&fakultas={$id_fakultas}&jenjang={$smarty.post.jenjang}&tgl={$tgl_wisuda}')" />
			<input type="button" value="Cetak Absensi Wisudawan" onclick="window.open('cetak-wisuda-absensi.php?periode={$smarty.post.periode}&fakultas={$id_fakultas}&jenjang={$smarty.post.jenjang}&tgl={$tgl_wisuda}')" />
		</td>
	</tr>
</table>
</form>
{/if}

{literal}
    <script>

		
		
		$("#cek_all").click(function()				
			{
				var checked_status = this.checked;
				$(".cek").each(function()
				{
					this.checked = checked_status;
				});
			});
		
            $("#tgl_wisuda").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
			$('#f2').validate();
    </script>
{/literal}