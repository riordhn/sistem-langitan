<div class="center_title_bar">INFO WISUDAWAN</div>
<form action="wisuda-info.php" method="post">
	<table width="700">
    	<tr>
        	<td>NIM</td>
			<td>:</td>
            <td>
            	<input type="text" name="nim" />
            </td>
        </tr>
        <tr>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>
{if isset($data_mhs)}
<table style="font-size:12px">
<tr>
	<td colspan="3" style="text-align:center"><img src='../../foto_wisuda/{$data_mhs.NIM_MHS}.jpg' width=105 height=150 vspace=1 hspace=2></td>
</tr>
<tr>
	<td>NIM</td>
	<td>:</td>
	<td>{$data_mhs.NIM_MHS}</td>
</tr>
<tr>
	<td>Nama</td>
	<td>:</td>
	<td>{$data_mhs.NM_PENGGUNA}</td>
</tr>
<tr>
	<td>Fakultas</td>
	<td>:</td>
	<td>{$data_mhs.NM_FAKULTAS}</td>
</tr>
<tr>
	<td>Program Studi</td>
	<td>:</td>
	<td>{$data_mhs.NM_JENJANG} - {$data_mhs.NM_PROGRAM_STUDI}</td>
</tr>
<tr>
	<td>Jalur</td>
	<td>:</td>
	<td>{$data_mhs.NM_JALUR}</td>
</tr>
<tr>
	<td>Tempat/ Tanggal Lahir</td>
	<td>:</td>
	<td>{$data_mhs.LAHIR_IJAZAH}</td>
</tr>
<tr>
	<td>Tanggal Lulus</td>
	<td>:</td>
	<td>{$data_mhs.TGL_LULUS_PENGAJUAN}</td>
</tr>
<tr>
	<td>Nomor Ijasah</td>
	<td>:</td>
	<td>{$data_mhs.NO_IJASAH}</td>
</tr>
<tr>
	<td>IPK</td>
	<td>:</td>
	<td>{$data_mhs.IPK}</td>
</tr>
<tr>
	<td>SKS</td>
	<td>:</td>
	<td>{$data_mhs.SKS}</td>
</tr>
<tr>
	<td>SKP</td>
	<td>:</td>
	<td>{$data_mhs.TOTAL_SKP}</td>
</tr>
<tr>
	<td>TOEFL</td>
	<td>:</td>
	<td>{$data_mhs.ELPT}</td>
</tr>
<tr>
	<td>Alamat Terakhir</td>
	<td>:</td>
	<td>{$data_mhs.ALAMAT_ASAL_MHS}</td>
</tr>
<tr>
	<td>Telp/ HP</td>
	<td>:</td>
	<td>{$data_mhs.MOBILE_MHS}</td>
</tr>
<tr>
	<td>Email</td>
	<td>:</td>
	<td>{$data_mhs.EMAIL_PENGGUNA}</td>
</tr>
<tr>
	<td>Periode Wisuda</td>
	<td>:</td>
	<td>{$data_mhs.NM_TARIF_WISUDA}</td>
</tr>
<tr>
	<td>Tanggal Wisuda</td>
	<td>:</td>
	<td>{$data_mhs.TGL_WISUDA}</td>
</tr>
<tr>
	<td colspan="3" style="text-align:center">
		<input type="button" value="Cetak" onclick="window.open('wisuda-info-cetak.php?nim={$smarty.request.nim}');"/>
	</td>
</tr>
</table>
{/if}