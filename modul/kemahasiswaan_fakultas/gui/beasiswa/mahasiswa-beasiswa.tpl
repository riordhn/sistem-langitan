{literal}
    <style>
        .ui-widget-content a{
            text-decoration: none;
            color: brown;
        }
        .ui-widget-content a:hover{
            color: #f09a14;
        }
    </style>
{/literal}
<div class="center_title_bar">Laporan Mahasiswa Beasiswa</div> 
</div>
<form method="get" id="report_form" action="mahasiswa-beasiswa.php">
    <table class="ui-widget-content" style="width:98%">
        <tr>
            <th colspan="4" class="center">Parameter</th>
        </tr>
        <tr>
            <td width="15%">Fakultas</td>
            <td width="25%">
                <select name="fakultas" id="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
            <td width="15%">Program Studi</td>
            <td>
                <select name="prodi" id="prodi">
                    <option value="">Semua</option>
                    {foreach $data_prodi as $data}
                        <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$smarty.get.prodi}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Jenis Beasiswa</td>
            <td>
                <select name="beasiswa">
                    {foreach $data_beasiswa as $data}
                        <option value="{$data['ID_BEASISWA']}" {if $data['ID_BEASISWA']==$smarty.get.beasiswa}selected="true"{/if}>{$data['NM_BEASISWA']} oleh {$data['PENYELENGGARA_BEASISWA']} {$data['NM_GROUP_BEASISWA']}</option>
                    {/foreach}
                </select>
            </td>
            <td colspan="2"></td>
        </tr>
        </tr>
        <tr class="center ui-widget-content">
            <td colspan="4" class="center">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" class="button" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>

{if isset($data_mahasiswa_kerjasama)}
    <table class="ui-widget-content" style="width: 98%">
        <tr>
            <th colspan="8" class="center">LAPORAN KERJASAMA MAHASISWA</th>
        </tr>
        <tr>
            <th>No</th>
            <th>NIM</th>
            <th>Nama</th>
            <th>Prodi</th>
            <th>Jalur</th>
            <th>Beasiswa</th>
            <th>Status Akademik</th>
            <th>Keterangan</th>
        </tr>
        {foreach $data_mahasiswa_kerjasama as $data}
            <tr>
                <td>{$data@index+1}</td>
                <td>{$data.NIM_MHS}</td>
                <td>{$data.NM_PENGGUNA}</td>
                <td>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</td>
                <td class="center">{$data.NM_JALUR}</td>
                <td class="center">{$data.NM_BEASISWA} Dari {$data.PENYELENGGARA_BEASISWA} <br/>{if $data.TGL_MULAI!=''||$data.TGL_SELESAI!=''}({$data.TGL_MULAI}-{$data.TGL_SELESAI}){/if}</td>
                <td>
                    IPS : {$data.IPS}<br/>
                    IPK : {$data.IPK}<br/>
                    TOTAL SKS : {$data.TOTAL_SKS}
                </td>
                <td class="center">{$data.KETERANGAN}</td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="8" class="center"><span style="color: red">Data Kosong</span></td>
            </tr>
        {/foreach}
        <tr>
            <td class="link center" colspan="8">
                <span class="link_button ui-corner-all" onclick="window.location.href='excel-mahasiswa-beasiswa.php?{$smarty.server.QUERY_STRING}'" style="padding:5px;cursor:pointer;">Excel</span>
            </td>	
        </tr>
    </table>
{/if}

{literal}
    <script>
            $('#fakultas').change(function(){
                    $.ajax({
                            type:'post',
                            url:'../keuangan/getProdi.php',
                            data:'id_fakultas='+$(this).val(),
                            success:function(data){
                                    $('#prodi').html(data);
                            }                    
                    })
            });
    </script>
{/literal}