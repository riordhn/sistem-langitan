<div class="center_title_bar">ENTRY URUTAN WISUDAWAN</div>
<form action="wisuda-urutan.php" method="post">
	<table width="700">
    	<tr>
        	<td>NIM</td>
            <td><input type="text" name="nim" /></td>
            <td>Periode Wisuda</td>
            <td>
            	<select name="periode">
                	{foreach $periode as $data}
                	<option value="{$data.ID_TARIF_WISUDA}" {if $smarty.post.periode==$data.ID_TARIF_WISUDA} selected="selected" {/if}>{$data.NM_TARIF_WISUDA}</option>
					{/foreach}
            	</select>
            </td>
            <td><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>


{if isset($wisuda)}
	<form action="wisuda-urutan.php" method="post">

	<table>
    	<tr>
        	<th>NO</th>
            <th>NIM</th>
            <th>NAMA</th>
            <th>FAKULTAS</th>
            <th>PROGRAM STUDI</th>
            <th>URUTAN WISUDAWAN</th>
        </tr>
        {$no = 1}
    	{foreach $wisuda as $data}
        	
            <tr>
            <td>{$no++}</td>
            <td>{$data.NIM_MHS}</td>
            <td>{$data.NM_PENGGUNA}</td>
            <td>{$data.NM_FAKULTAS}</td>
            <td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
            <td style="text-align:center">
            	<select name="urutan_wisudawan">
                		<option value="">Pilih Urutan</option>
                	{foreach $urutan as $data1}
                		<option value="{$data1.NO_URUTAN_WISUDAWAN}" {if $data1.NO_URUTAN_WISUDAWAN == $data.WISUDAWAN_TERBAIK} selected="selected"{/if}>{$data1.NM_URUTAN_WISUDAWAN} ({$data1.NO_URUTAN_WISUDAWAN})</option>
                    {/foreach}
                </select>
            </td>
            </tr>
        	<input type="hidden" name="id" value="{$data.ID_PEMBAYARAN_WISUDA}" />
            <input type="hidden" name="nim" value="{$data.NIM_MHS}" />
        {/foreach}
        	<tr>
            	<td colspan="6" style="text-align:center"><input type="submit" value="Simpan" />
                    <input type="hidden" name="periode" value="{$smarty.post.periode}" />
                </td>
            </tr>
    </table>
	</form>
{/if}