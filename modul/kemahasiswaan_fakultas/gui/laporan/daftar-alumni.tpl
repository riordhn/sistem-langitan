<div class="center_title_bar">Rekapitulasi Kelulusan</div> 
<form method="get" id="report_form" action="daftar-alumni.php">
    <table style="width:98%;">
        <tr>
            <th colspan="4" class="center">Parameter</th>
        </tr>
        <tr>
            <td width="15%">Fakultas</td>
            <td width="25%">
                <select name="fakultas" id="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        {if $data.ID_FAKULTAS==$id_fakultas}
                            <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                        {/if}
                    {/foreach}

                </select>
            </td>
            <td>Tahun Kelulusan</td>
            <td>
                <select name="tahun">
                    <option value="">Pilih Tahun</option>
                    {section name=foo start=$tahun_sekarang-4 loop=$tahun_sekarang+1 step=1}
                        <option value="{$smarty.section.foo.index}" {if $smarty.section.foo.index==$smarty.get.tahun}selected="true"{/if}>{$smarty.section.foo.index}</option>
                    {/section}
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="6" class="center">
                <input type="hidden" name="mode" value="laporan" />
                <input type="submit" name="submit" class="button" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if $smarty.get.mode!=''}
    {if isset($data_rekap)}
        <a class="button disable-ajax" style="background-color: green" href="excel-daftar-alumni.php?{$smarty.server.QUERY_STRING}">Download Excel</a>
        <p></p>
        <table style="width: 98%">
            <tr>
                <th colspan="4" class="center">DATA REKAPITULASI PERTAHUN {$smarty.get.tahun}</th>
            </tr>
            <tr>
                <th>NO</th>
                <th>
                    {if $smarty.get.fakultas!=''}
                        PRODI
                    {else}
                        FAKULTAS
                    {/if}
                </th>
                <th>JUMLAH LULUSAN</th>
                <th>DETAIL</th>
            </tr>
            {foreach $data_rekap as $rek}
                <tr>
                    <td>{$rek@index+1}</td>
                    <td>
                        {if $smarty.get.fakultas!=''}
                            {$rek.NM_JENJANG} {$rek.NM_PROGRAM_STUDI}
                        {else}
                            {$rek.NM_FAKULTAS}
                        {/if}
                    </td>
                    <td>{number_format($rek.JUMLAH_LULUS)}</td>
                    <td>
                        {if $smarty.get.fakultas!=''}
                            <a class="button" href="daftar-alumni.php?mode=detail&fakultas={$smarty.get.fakultas}&prodi={$rek.ID_PROGRAM_STUDI}&tahun={$smarty.get.tahun}">Detail</a>
                        {else}
                            <a class="button" href="daftar-alumni.php?mode=detail&fakultas={$rek.ID_FAKULTAS}&tahun={$smarty.get.tahun}">Detail</a>
                        {/if}
                    </td>
                </tr>
                {$total_lulus=$total_lulus+$rek.JUMLAH_LULUS}
            {/foreach}
            <tr class="total">
                <td colspan="2">TOTAL</td>
                <td>{number_format($total_lulus)}</td>
                <td>
                    {if $smarty.get.fakultas!=''}
                        <a class="button" href="daftar-alumni.php?mode=detail&fakultas={$smarty.get.fakultas}&tahun={$smarty.get.tahun}">Detail</a>
                    {else}
                        <a class="button" href="daftar-alumni.php?mode=detail&tahun={$smarty.get.tahun}">Detail</a>
                    {/if}
                </td>
            </tr>
        </table>
    {else isset($data_detail)}
        <table style="width: 98%;font-size: 0.85em">
            <tr>
                <th>NO</th>
                <th>NIM</th>
                <th>NAMA</th>
                <th>PRODI/FAKULTAS</th>
                <th>ANGKATAN</th>
                <th>TANGGAL LULUS</th>
                <th>PERIODE LULUS</th>
                <th>MOBILE</th>
                <th>EMAIL UNAIR</th>
                <th>EMAIL LAIN</th>
            </tr>
            {foreach $data_detail as $det}
                <tr>
                    <td>{$det@index+1}</td>
                    <td>{$det.NIM_MHS}</td>
                    <td>{$det.NM_PENGGUNA}</td>
                    <td>{$det.NM_JENJANG} {$det.NM_PROGRAM_STUDI}<br/>{$det.NM_FAKULTAS|upper}</td>
                    <td>{$det.THN_ANGKATAN_MHS}</td>
                    <td>{$det.TGL_LULUS}</td>
                    <td>{$det.NM_TARIF_WISUDA}</td>
                    <td>{$det.MOBILE_MHS}</td>
                    <td>{$det.EMAIL_PENGGUNA}</td>
                    <td>{$det.EMAIL_ALTERNATE}</td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="10" class="kosong">Data Kosong</td>
                </tr>
            {/foreach}
            <tr>
                <td colspan="10" class="center">
                    <a class="button disable-ajax" style="background-color: green" href="excel-daftar-alumni.php?{$smarty.server.QUERY_STRING}">Download Excel</a>
                </td>
            </tr>
        </table>
        <span class="button" onclick="history.go(-1)">Kembali</span>
    {/if}
{/if}