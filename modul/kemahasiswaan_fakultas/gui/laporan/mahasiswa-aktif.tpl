<div class="center_title_bar">Mahasiswa Aktif</div>
<form action="mahasiswa-aktif.php" method="get">
    <table style="width: 100%">
        <tr>
            <th colspan="4">PARAMETER MAHASISWA</th>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>
                <select id="fakultas" name="fakultas">
                    <option value="">Pilih</option>
                    {foreach $data_fakultas as $data}
                        {if $data.ID_FAKULTAS==$id_fakultas}
                            <option value="{$data.ID_FAKULTAS}" {if $smarty.get.fakultas==$data.ID_FAKULTAS}selected="true"{/if}>{$data.NM_FAKULTAS|upper}</option>
                        {/if}
                    {/foreach}
                </select>
            </td>
            <td>Angkatan Mahasiswa</td>
            <td>
                <select name="angkatan">
                    <option value="">Semua</option>
                    {foreach $data_angkatan as $data}
                        <option value="{$data.ANGKATAN}" {if $smarty.get.angkatan==$data.ANGKATAN}selected="true"{/if}>{$data.ANGKATAN}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Program Studi</td>
            <td>
                <select id="prodi" name="prodi">
                    <option value="">Semua</option>
                    {foreach $data_prodi as $data}
                        <option value="{$data.ID_PROGRAM_STUDI}" {if $smarty.get.prodi==$data.ID_PROGRAM_STUDI}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI|upper}</option>
                    {/foreach}
                </select>
            </td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="4" class="center">
                <input class="button" type="submit" value="Tampilkan"/>
            </td>
        </tr>
        <input type="hidden" name="mode" value="tampil"/>
    </table>
</form>
{if isset($data_mhs)||isset($data_biodata)}
    {if $smarty.get.mode=='tampil'}        
        <table class="ui-widget-content" style="width: 98%">
            <tr>
                <th>NO</th>
                <th>NIM</th>
                <th>NAMA</th>
                <th>ANGKATAN</th>
                <th>JENJANG</th>
                <th>PROGRAM STUDI</th>
                <th>FAKULTAS</th>
                <th>STATUS</th>
                <th>ELPT SCORE MABA</th>
                <th>ALAMAT</th>
                <th>DETAIL</th>
            </tr>
            {$nomer=1}
            {foreach $data_mhs as $data}
                <tr>
                    <td>{$nomer++}</td>
                    <td>{$data.NIM_MHS}</td>
                    <td>{$data.NM_PENGGUNA}</td>
                    <td>{$data.THN_ANGKATAN_MHS}</td>
                    <td>{$data.NM_JENJANG}</td>
                    <td>{$data.NM_PROGRAM_STUDI}</td>
                    <td>{$data.NM_FAKULTAS}</td>
                    <td>{$data.NM_STATUS_PENGGUNA}</td>
                    <td>{$data.ELPT_SCORE}</td>
                    <td>{$data.ALAMAT_MHS}</td>
                    <td class="center"><a class="button" href="mahasiswa-aktif.php?mode=detail&id_mhs={$data.ID_MHS}">DETAIL</a></td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="10" class="center"><span style="color: red">Data Kosong</span></td>
                </tr>
            {/foreach}
            <tr>
                <td colspan="10" class="center" style="padding: 10px;">
                    <span class="link_button ui-corner-all" onclick="window.location.href='excel-mahasiswa-aktif.php?{$smarty.server.QUERY_STRING}'" style="padding:5px;cursor:pointer;">Excel</span>
                </td>
            </tr>
        </table>
    {else}
        <span class="disable-ajax button" onclick="history.back(-1)">Kembali</span>
        <p></p>
        <table class="ui-widget-content" style="width: 100%">
            <tr>
                <th colspan="2" class="center">Detail Biodata Mahasiswa</th>
            </tr>
            <tr>
                <td colspan="2" class="center">
                    <img src="{$foto}" width="180" height="230"/>
                </td>
            </tr>
            <tr>
                <td>NAMA</td>
                <td>{$data_biodata.NM_PENGGUNA}</td>
            </tr>
            <tr>
                <td>NIM</td>
                <td>{$data_biodata.NIM_MHS}</td>
            </tr>
            <tr>
                <td>ANGKATAN</td>
                <td>{$data_biodata.THN_ANGKATAN_MHS}</td>
            </tr>
            <tr>
                <td>JENJANG</td>
                <td>{$data_biodata.NM_JENJANG}</td>
            </tr>
            <tr>
                <td>FAKULTAS</td>
                <td>{$data_biodata.NM_FAKULTAS|upper}</td>
            </tr>
            <tr>
                <td>PROGRAM STUDI</td>
                <td>{$data_biodata.NM_PROGRAM_STUDI}</td>
            </tr>
            <tr>
                <td>TEMPAT/TANGGAL LAHIR</td>
                <td> {$data_biodata.TEMPAT_LAHIR} / {$data_biodata.TGL_LAHIR_PENGGUNA|date_format:"%d %B %Y"}</td>
            </tr>
            <tr>
                <td>KONTAK</td>
                <td>{$data_biodata.MOBILE_MHS}</td>
            </tr>
            <tr>
                <td>KELAMIN</td>
                <td>
                    {if $data_biodata.KELAMIN_PENGGUNA==1}
                        Laki-laki
                    {else}
                        Perempuan
                    {/if}
                </td>
            </tr>
            <tr>
                <td>EMAIL</td>
                <td>{$data_biodata.EMAIL_PENGGUNA}</td>
            </tr>
            <tr>
                <td>BLOG MAHASISWA</td>
                <td>{$data_biodata.BLOG_PENGGUNA}</td>
            </tr>
            <tr>
                <td>ALAMAT MAHASISWA</td>
                <td>{$data_biodata.ALAMAT_MHS}</td>
            </tr>
            <tr>
                <td>NAMA AYAH</td>
                <td>{$data_biodata.NM_AYAH_MHS}</td>
            </tr>
            <tr>
                <td>ALAMAT AYAH</td>
                <td>{$data_biodata.ALAMAT_AYAH_MHS}</td>
            </tr>
            <tr>
                <td>PENGHASILAN ORTU</td>
                <td>{number_format($data_biodata.PENGHASILAN_ORTU_MHS)}</td>
            </tr>
        </table>
        <br/><hr style="width:100%"/><br/>
        <table class="ui-widget-content" style="width: 100%;">
            <tr>
                <th colspan="6" class="center">Detail Akademik Mahasiswa</th>
            </tr>
            <tr>
                <td>NAMA</td>
                <td style="width: 230px;">: {$data_mhs_status.NM_PENGGUNA}</td>
                <td>IPS</td>
                <td>: {$data_mhs_status.IPS}</td>
                <td>TOTAL SKS</td>
                <td>: {$data_mhs_status.TOTAL_SKS}</td>
            </tr>
            <tr>
                <td>NIM</td>
                <td>: {$data_mhs_status.NIM_MHS}</td>
                <td>IPK</td>
                <td>: {$data_mhs_status.IPK}</td>
                <td>ANGKATAN</td>
                <td>: {$data_mhs_status.THN_ANGKATAN_MHS}</td>           
            </tr>
            <tr>
                <td>PROGRAM STUDI</td>
                <td colspan="5">: ({$data_mhs_status.NM_JENJANG}) {$data_mhs_status.NM_PROGRAM_STUDI}</td>
            </tr>
        </table>
    {/if}
{/if}
{literal}
    <script>
            $('#fakultas').change(function(){
                    $.ajax({
                            type:'post',
                            url:'../keuangan/getProdi.php',
                            data:'id_fakultas='+$(this).val(),
                            success:function(data){
                                    $('#prodi').html(data);
                            }                    
                    })
            });
    </script>
{/literal}