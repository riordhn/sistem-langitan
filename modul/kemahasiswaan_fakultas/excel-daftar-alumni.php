<?php
include 'config.php';
include '../ppkk/class/laporan.class.php';
include '../keuangan/class/list_data.class.php';

$lap = new laporan($db);
$list = new list_data($db);

if (get('mode') == 'laporan') {
    $data_excel = $lap->load_rekap_kelulusan(get('fakultas'), get('tahun'));
} else if (get('mode') == 'detail') {
    $data_excel = $lap->load_detail_kelulusan(get('fakultas'), get('prodi'), get('tahun'));
}
if (get('fakultas') != '' || get('prodi') != '') {
    $fakultas = $list->get_fakultas(get('fakultas'));
    $prodi = $list->get_prodi(get('prodi'));
}
ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #1f4c63;
        text-transform:capitalize;
    }
    td{
        text-align: left;
    }
</style>
<?php
if ($_GET['mode'] == 'laporan') {
    ?>
    <table border='1' align='left' cellpadding='0' cellspacing='0' class='keuangan'>
        <thead>
            <tr>
                <td colspan="3" class="header_text">
                    <?php
                    echo strtoupper('Rekapitulasi Data Kelulusan Mahasiswa<br/>') . $_GET['tahun'];
                    ?>
                </td>
            </tr>
            <tr>
                <td class="header_text">NO</td>
                <td class="header_text">PRODI/FAKULTAS</td>
                <td class="header_text">JUMLAH LULUSAN</td>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            foreach ($data_excel as $data) {
                $fakultas = strtoupper($data['NM_FAKULTAS']);
                echo "<tr>
                        <td>{$no}</td>
                        <td>{$data['NM_JENJANG']} {$data['NM_PROGRAM_STUDI']} {$fakultas}</td>
                        <td>{$data['JUMLAH_LULUS']}</td>
                </tr>";
                $no++;
            }
            ?>
        </tbody>
    </table>

    <?php
} else {
    ?>
    <table border='1' align='left' cellpadding='0' cellspacing='0' class='keuangan'>
        <thead>
            <tr>
                <td colspan="10" class="header_text">
                    <?php
                    echo strtoupper('Rekapitulasi Data Kelulusan Mahasiswa<br/>');
                    if ($fakultas != '') {
                        echo strtoupper('Fakultas ' . $fakultas['NM_FAKULTAS'] . '<br/>');
                    }
                    if ($prodi != '') {
                        echo strtoupper('Program Studi (' . $prodi['NM_JENJANG'] . ') ' . $prodi['NM_PROGRAM_STUDI'] . '<br/>');
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td class="header_text">NO</td>
                <td class="header_text">NIM</td>
                <td class="header_text">NAMA</td>
                <td class="header_text">PRODI/FAKULTAS</td>
                <td class="header_text">ANGKATAN</td>
                <td class="header_text">TANGGAL LULUS</td>
                <td class="header_text">PERIODE LULUS</td>
                <td class="header_text">MOBILE</td>
                <td class="header_text">EMAIL UNAIR</td>
                <td class="header_text">EMAIL LAIN</td>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            foreach ($data_excel as $data) {
                $fakultas = strtoupper($data['NM_FAKULTAS']);
                echo "<tr>
				<td>{$no}</td>
				<td>'{$data['NIM_MHS']}</td>
				<td>{$data['NM_PENGGUNA']}</td>
				<td>{$data['NM_JENJANG']} {$data['NM_PROGRAM_STUDI']} <br/>{$fakultas}</td>
                                <td>{$data['THN_ANGKATAN_MHS']}</td>
				<td>{$data['TGL_LULUS']}</td>
				<td>{$data['NM_TARIF_WISUDA']}</td>
                                <td>{$data['MOBILE_MHS']}</td>
				<td>{$data['EMAIL_PENGGUNA']}</td>
				<td>{$data['EMAIL_ALTERNATE']}</td>
			</tr>";
                $no++;
            }
            ?>
        </tbody>
    </table>

    <?php
}
echo '<b>Data Ini Di Ambil Pada Tanggal ' . date('d F Y  H:i:s') . '</b>';

$nm_file = "Data-Kelulusan(" . date('d-m-Y') . ')';
header("Content-disposition: attachment; filename={$nm_file}.xls");
header("Content-type: application/vnd.ms-excel");
header("Content-Transfer-Encoding: binary");
ob_flush();
?>
