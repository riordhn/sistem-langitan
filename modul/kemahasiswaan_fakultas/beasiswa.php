<?php

include 'config.php';
include '../kemahasiswaan/class/beasiswa.class.php';

$b = new beasiswa($db, $user->ID_ROLE);

if (isset($_GET['mode'])) {
    if (get('mode') == 'add_beasiswa') {
        $smarty->assign('data_jenis', $b->load_jenis_beasiswa());
        $smarty->assign("data_group_beasiswa", $b->load_group_beasiswa());
        $smarty->display("beasiswa/add-beasiswa.tpl");
    } else if (get('mode') == 'edit_beasiswa') {
        $smarty->assign('data_jenis', $b->load_jenis_beasiswa());
        $smarty->assign("data_group_beasiswa", $b->load_group_beasiswa());
        $smarty->assign("beasiswa", $b->get_beasiswa(get('id')));
        $smarty->display("beasiswa/edit-beasiswa.tpl");
    } else if (get('mode') == 'delete_beasiswa') {
        $smarty->assign('data_jenis', $b->load_jenis_beasiswa());
        $smarty->assign("data_group_beasiswa", $b->load_group_beasiswa());
        $smarty->assign("beasiswa", $b->get_beasiswa(get('id')));
        $smarty->display("beasiswa/delete-beasiswa.tpl");
    }
} else {
    if (isset($_POST)) {
        if (post('mode') == 'add_beasiswa') {
            $b->add_beasiswa(strtoupper(post('nama')), strtoupper(post('penyelenggara')), post('jenis'), post('group'), post('keterangan'));
        } else if (post('mode') == 'edit_beasiswa') {
            $b->edit_beasiswa(post('id'), strtoupper(post('nama')), strtoupper(post('penyelenggara')), post('jenis'), post('group'), post('keterangan'));
        } else if (post('mode') == 'delete_beasiswa') {
            $b->delete_beasiswa(post('id'));
        }
    }
    $smarty->assign('data_beasiswa', $b->load_beasiswa());
    $smarty->display("beasiswa/beasiswa.tpl");
}
?>