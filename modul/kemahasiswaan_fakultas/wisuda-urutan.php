<?php
include('config.php');
$kdprodi= $user->ID_PROGRAM_STUDI;
$id_pengguna= $user->ID_PENGGUNA; 
$id_fakultas = $user->ID_FAKULTAS;

$periode = $db->QueryToArray("SELECT * FROM TARIF_WISUDA ORDER BY ID_TARIF_WISUDA DESC");
$smarty->assign('periode', $periode);
$fakultas = $db->QueryToArray("SELECT * FROM FAKULTAS ORDER BY ID_FAKULTAS ASC");
$smarty->assign('fakultas', $fakultas);
$urutan = $db->QueryToArray("SELECT * FROM URUTAN_WISUDAWAN ORDER BY NO_URUTAN_WISUDAWAN ASC");
$smarty->assign('urutan', $urutan);


if(isset($_POST['id']) and $_POST['id'] != ''){

	$db->Query("UPDATE PEMBAYARAN_WISUDA SET WISUDAWAN_TERBAIK = '$_POST[urutan_wisudawan]' WHERE ID_PEMBAYARAN_WISUDA = '$_POST[id]'");
	
}


if(isset($_POST['nim'])){
		
$wisuda = $db->QueryToArray("SELECT NIM_MHS, NM_PENGGUNA, NM_JENJANG, NM_PROGRAM_STUDI,	NM_FAKULTAS, ID_PEMBAYARAN_WISUDA, WISUDAWAN_TERBAIK
										FROM PEMBAYARAN_WISUDA
										JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBAYARAN_WISUDA.ID_MHS
										JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PEMBAYARAN_WISUDA.ID_PERIODE_WISUDA
										JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
								 		JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
								 		JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
								 		JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
										WHERE PERIODE_WISUDA.ID_TARIF_WISUDA = '$_POST[periode]' AND ABSENSI_WISUDA  = 1
										AND NIM_MHS = '$_POST[nim]' AND FAKULTAS.ID_FAKULTAS = '$id_fakultas'
										ORDER BY FAKULTAS.ID_FAKULTAS, NM_JENJANG, NM_PROGRAM_STUDI, MAHASISWA.NIM_MHS");
		$smarty->assign('wisuda', $wisuda);
}
$smarty->display("wisuda/wisuda-urutan.tpl");
?>
