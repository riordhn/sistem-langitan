<?php
include 'config.php';
include '../kemahasiswaan/class/laporan.class.php';

$lap = new laporan($db);

$data_excel = $lap->get_report_mhs_aktif(get('fakultas'), get('prodi'), get('angkatan'));
ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #1f4c63;
        text-transform:capitalize;
    }
    td{
        text-align: left;
    }
</style>
<table border='1' align='left' cellpadding='0' cellspacing='0' class='keuangan'>
    <thead>
        <tr>
            <td colspan="9" class="header_text">
                <?php
                echo 'LAPORAN MAHASISWA AKTIF';
                ?>
            </td>
        </tr>
        <tr>
            <td class="header_text">NO</td>
            <td class="header_text">NIM</td>
            <td class="header_text">NAMA</td>
            <td class="header_text">ANGKATAN</td>
            <td class="header_text">JENJANG</td>
            <td class="header_text">PRODI</td>
            <td class="header_text">FAKULTAS</td>
            <td class="header_text">STATUS AKADEMIK</td>
            <td class="header_text">ALAMAT</td>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach ($data_excel as $data) {
            echo "<tr>
                    <td>{$no}</td>
                    <td>'{$data['NIM_MHS']}</td>
                    <td>{$data['NM_PENGGUNA']}</td>
                    <td>{$data['THN_ANGKATAN_MHS']}</td>
                    <td>{$data['NM_JENJANG']}</td>
                    <td>{$data['NM_PROGRAM_STUDI']}</td>
                    <td>{$data['NM_FAKULTAS']}</td>
                    <td>{$data['NM_STATUS_PENGGUNA']}</td>
                    <td>{$data['ALAMAT_MHS']}</td>
                  </tr>  
                    ";
        }
        ?>
    </tbody>
</table>

<?php
echo '<b>Data Ini Di Ambil Pada Tanggal ' . date('d F Y  H:i:s') . '</b>';

$nm_file = "Mahasiswa-Aktif (" . date('d-m-Y').')';
header("Content-disposition: attachment; filename=$nm_file.xls");
header("Content-type: Application/exe");
header("Content-Transfer-Encoding: binary");
ob_flush();
?>
