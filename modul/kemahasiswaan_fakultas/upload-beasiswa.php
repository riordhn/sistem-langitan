<?php

// Test CVS
include 'config.php';
include '../kemahasiswaan/class/reader.php';
include '../kemahasiswaan/class/beasiswa.class.php';

$b = new beasiswa($db, $user->ID_ROLE);

$excel = new Spreadsheet_Excel_Reader();
$data_biaya = $db->QueryToArray("SELECT * FROM BIAYA ORDER BY NM_BIAYA");
$status_message = '';

if (get('mode') == 'save_excel') {
    foreach ($_SESSION['data_mhs_upload'] as $data) {
        $data_mhs = $b->get_mahasiswa($data['NIM_MHS']);
        if ($data_mhs != '') {
            $b->non_aktif_mahasiswa_beasiswa($data['ID_MHS']);
            $b->insert_mahasiswa_beasiswa($data['ID_MHS'], $_SESSION['beasiswa'], $_SESSION['tgl_mulai'], $_SESSION['tgl_selesai'], $_SESSION['keterangan']);
//            foreach ($_SESSION['data_biaya_input'] as $bi) {
//                $b->insert_detail_biaya_beasiswa($data_mhs['ID_MHS'], $bi['ID_BIAYA'], $bi['BESAR_BIAYA']);
//            }
        }
    }
    $smarty->assign('sukses_upload', alert_success('Data Berhasil Dimasukkan',90));
}



// Set output Encoding.
if (isset($_POST['excel'])) {
    $path_upload = "excel-beasiswa/";
    $sukses_upload = false;
    $filename = $user->ID_PENGGUNA . '_' . date('dmy_H:i:s') . '_' . $_FILES["file"]["name"];
    if ($_FILES["file"]["error"] > 0) {
        $status_message .= "Error : " . $_FILES["file"]["error"] . "<br />";
        $status_upload = alert_error('Upload File Gagal <br/>' . $status_message);
    } else {
        if (file_exists($path_upload . $filename)) {
            $status_message .= 'Keterangan : ' . $filename . " Sudah Ada ";
            $status_upload = alert_error('Upload File Gagal <br/>' . $status_message);
        } else {
            move_uploaded_file($_FILES["file"]["tmp_name"], $path_upload . $filename);
            chmod($path_upload . $filename, 0755);
            $status_upload = alert_success('Berhasil Melakukan Upload File' . $status_message);
            $sukses_upload = true;
        }
    }
    if ($sukses_upload) {
        $excel->setOutputEncoding('CP1251');
        $excel->read("$path_upload$filename");
        $data_mhs_upload = array();
        $data_biaya_input = array();
        for ($j = 1; $j <= count($data_biaya); $j++) {
            if (post('id_biaya' . $j) != '') {
                array_push($data_biaya_input, array(
                    'ID_BIAYA' => post('id_biaya' . $j),
                    'BESAR_BIAYA' => post('besar_biaya' . $j)
                ));
            }
        }
        for ($i = 2; $i <= $excel->sheets[0]['numRows']; $i++) {
            $nim = str_replace("'", "", trim($excel->sheets[0]['cells'][$i][1]));
            $data_mhs = $b->get_mahasiswa($nim);
            $beasiswa_mhs = $b->get_mahasiswa_beasiswa($nim);
            if ($data_mhs != '') {
                if ($beasiswa_mhs != '') {
                    $status_beasiswa = $beasiswa_mhs;
                } else {
                    $status_beasiswa = 'Data Kosong';
                }
                $status_mahasiswa = 'Data Mahasiswa Ada';
            } else {
                $status_mahasiswa = 'Data Mahasiswa Tidak Ada';
                $status_beasiswa = 'Data Kosong';
            }
            array_push($data_mhs_upload, array(
                'ID_MHS' => $data_mhs['ID_MHS'],
                'NIM_MHS' => $nim,
                'NAMA' => $data_mhs['NM_PENGGUNA'],
                'STATUS_MAHASISWA' => $status_mahasiswa,
                'STATUS_BEASISWA' => $status_beasiswa
            ));
        }
        $_SESSION['data_mhs_upload'] = $data_mhs_upload;
        $_SESSION['tgl_mulai'] = post('tgl_mulai');
        $_SESSION['tgl_selesai'] = post('tgl_selesai');
        $_SESSION['beasiswa'] = post('beasiswa');
        $_SESSION['keterangan'] = post('keterangan');
        $_SESSION['data_biaya_input'] = $data_biaya_input;
        $smarty->assign('data_mhs_upload', $_SESSION['data_mhs_upload']);
    }
    $smarty->assign('status_upload', $status_upload);
}

$smarty->assign('data_biaya', $data_biaya);
$smarty->assign('data_beasiswa', $b->load_beasiswa_fakultas());
$smarty->display('beasiswa/upload-beasiswa.tpl');
?>
