<?php

include 'config.php';
include '../keuangan/class/list_data.class.php';
include '../ppkk/class/laporan.class.php';


$list = new list_data($db);
$lap = new laporan($db);

if (isset($_GET)) {
    if (get('mode') == 'laporan') {
        $smarty->assign('data_rekap', $lap->load_rekap_kelulusan(get('fakultas'), get('tahun')));
    } else if (get('mode') == 'detail') {
        $smarty->assign('data_detail', $lap->load_detail_kelulusan(get('fakultas'), get('prodi'), get('tahun')));
    }
}
$smarty->assign('id_fakultas', $user->ID_FAKULTAS);
$smarty->assign('tahun_sekarang', date('Y'));
$smarty->assign('data_prodi', $list->load_list_prodi(get('fakultas')));
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->display('laporan/daftar-alumni.tpl');
?>
