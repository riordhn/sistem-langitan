<?php
include('config.php');

$mode = get('mode', 'view');

$id_pt = $id_pt_user;

if ($request_method == 'POST')
{
	if (post('mode') == 'update')
	{
		$id_c_mhs = post('id_c_mhs');
		$id_voucher = post('id_voucher');
		$id_penerimaan_pindah = post('id_penerimaan_pindah');

		$db->Query("SELECT ID_JALUR FROM PENERIMAAN WHERE ID_PENERIMAAN = '{$id_penerimaan_pindah}'");
		$jalur = $db->FetchAssoc();

		$id_jalur = $jalur['ID_JALUR'];

		$db->Query("update voucher set id_penerimaan = '{$id_penerimaan_pindah}' where id_voucher = '{$id_voucher}'");

		$result = $db->Query("update calon_mahasiswa_baru set id_penerimaan = '{$id_penerimaan_pindah}', id_jalur = '{$id_jalur}' where id_c_mhs = '{$id_c_mhs}'");

		$smarty->assign('edited', $result ? "Data berhasil dirubah" : "Data gagal dirubah");

	}
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    $kode_voucher = get('kode_voucher');
    
    $db->Parse("
		SELECT
			v.id_voucher, v.kode_voucher, v.pin_voucher, bank.nm_bank, nama_bank_via, TGL_AMBIL, tgl_bayar, v.kode_jurusan,
			cmb.id_c_mhs, cmb.id_penerimaan, cmb.nm_c_mhs, cmb.nomor_hp, 
				(select nm_sekolah from sekolah s where s.id_sekolah = cms.id_sekolah_asal) as nm_sekolah_asal
		FROM voucher v
		JOIN penerimaan p on p.id_penerimaan = v.id_penerimaan and p.id_perguruan_tinggi = '{$id_pt}'
		LEFT JOIN bank ON bank.ID_BANK = v.id_bank
		LEFT JOIN BANK_VIA ON BANK_VIA.ID_bank_via = v.id_bank_via
		LEFT JOIN calon_mahasiswa_baru cmb on cmb.kode_voucher = v.kode_voucher
		LEFT JOIN calon_mahasiswa_sekolah cms ON cms.id_c_mhs = cmb.id_c_mhs
		WHERE v.kode_voucher = :kode_voucher");
    $db->BindByName(':kode_voucher', $kode_voucher);
	$db->Execute();
    
    $smarty->assign('voucher', $db->FetchAssoc());

    // memanggil penerimaan khusus tahun ini
    $penerimaan_set = $db->QueryToArray("SELECT ID_PENERIMAAN, GELOMBANG, TAHUN, NM_PENERIMAAN, 
			(SELECT NM_JENJANG FROM JENJANG WHERE JENJANG.ID_JENJANG = PENERIMAAN.ID_JENJANG) AS NM_JENJANG, 
			(SELECT NM_JALUR FROM JALUR WHERE JALUR.ID_JALUR = PENERIMAAN.ID_JALUR) AS NM_JALUR 
			FROM PENERIMAAN 
			WHERE TAHUN = (select extract(year from sysdate) from dual) AND ID_PERGURUAN_TINGGI = '{$id_pt}'
			ORDER BY TAHUN, SEMESTER, GELOMBANG, ID_JENJANG, ID_JALUR, NM_PENERIMAAN");
	$smarty->assign('penerimaan_set', $penerimaan_set);


}

$smarty->display("peserta/pindah/{$mode}.tpl");
?>
