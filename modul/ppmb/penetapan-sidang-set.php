<?php
include('config.php');

$checked = post('checked');
$id_c_mhs = post('id_c_mhs');
$id_program_studi = post('id_program_studi');

if ($checked == 'true')
{
    $db->Parse("
        update calon_mahasiswa_baru set
            tgl_diterima = :tgl_diterima,
            id_verifikator_penetapan = :id_verifikator,
            id_program_studi = :id_program_studi
        where id_c_mhs = {$id_c_mhs}");
    $db->BindByName(':tgl_diterima', date('d-M-Y'));
    $db->BindByName(':id_verifikator', $user->ID_PENGGUNA);
    $db->BindByName(':id_program_studi', $id_program_studi);
    $result = $db->Execute();
    
    if ($result)
        echo "1";
    else
        echo "0";
}
else
{
    $result = $db->Query("
        update calon_mahasiswa_baru set
            tgl_diterima = null,
            id_verifikator_penetapan = null,
            id_program_studi = null
        where id_c_mhs = {$id_c_mhs}");
        
    if ($result)
        echo "1";
    else
        echo "0";
}
?>
