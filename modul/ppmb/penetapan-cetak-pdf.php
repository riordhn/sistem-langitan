<?php
include('../../config.php');
include_once('../../tcpdf/config/lang/ind.php');
include_once('../../tcpdf/tcpdf.php');
include_once('cetak.class.php');

$cetak = new cetak($db);

if (empty($_GET['id_penetapan_penerimaan']) && empty($_GET['id_program_studi']) ) {
    $data = $cetak->get_hasil_penetapan(null,null,$_GET['id_penetapan']);
} 
else {
    $data = $cetak->get_hasil_penetapan($_GET['id_penetapan_penerimaan'],$_GET['id_program_studi'],$_GET['id_penetapan']);
}
$data_penetapan=$cetak->get_penetapan($_GET['id_penetapan']);

$row_per_page = $data_penetapan['ROW_PAGE_1']==""?19:$data_penetapan['ROW_PAGE_1'];
$row_page_n = $data_penetapan['ROW_PAGE_N']==""?25:$data_penetapan['ROW_PAGE_N'];
$width_nama =$data_penetapan['WIDTH_NAMA']==""?230:$data_penetapan['WIDTH_NAMA'];
$width_prodi =$data_penetapan['WIDTH_PRODI']==""?200:$data_penetapan['WIDTH_PRODI'];

class MYPDF extends TCPDF {

	public function Header() {

		$this->Image("../../img/akademik_images/logo-".$GLOBALS[nama_singkat].".gif", 10, 10, 25, 25, "GIF");

		$this->SetXY(40, 10);
		$this->SetFont('times', 'B', 18);
		$this->Cell(0, 0, $GLOBALS[nama_pt_kapital], 0, 1, "C");
		$this->SetFont('times', '', 10);
		$this->Ln(5);
		$this->SetX(40);
		/*$this->Cell(0, 0, "Kampus C Mulyorejo Surabaya 60115 Telp. (031) 5914042, 5914043, 5912546, 5912564 Fax (031) 5981841", 0, 1, "C");*/
        $this->Cell(0, 0, $GLOBALS[alamat_pt]." Telp. ".$GLOBALS[telp_pt]." Fax ".$GLOBALS[fax_pt], 0, 1, "C");
		$this->SetX(40);
		$this->Cell(0, 0, "Website: http://www.".$GLOBALS[nama_singkat].".ac.id, Email: rektor@".$GLOBALS[nama_singkat].".ac.id", 0, 1, "C");

		$this->Line($this->GetX(), $this->GetY() + 5, 200, $this->GetY() + 5);
		$this->Line($this->GetX(), $this->GetY() + 6, 200, $this->GetY() + 6, array('width' => 1));
	}


    // Page footer
    public function Footer() {
        // isi footer
        $footer = <<<EOF
	<style>
	    .header { font-size: 22pt; font-family: times; font-weight: bold; text-align:center;}
	    .title { font-size: 13pt; font-family: serif; margin-top: 0px ;text-align:center; }
	    th {background-color:#000;color:white;}
	    tr.odd {background-color:#dddddd;}
	    td.list {border-bottom:0.5px black solid;}
	    td { font-size: 10pt; }
	</style>
	<table>
        <tr>
            <th colspan="5" align="center">Halaman {hal}</th>
        </tr>
    </table>
EOF;
        $footer = str_replace('{hal}', $this->getAliasNumPage(), $footer);
        // Position at 15 mm from bottom
        $this->SetY(-28);
        // Set font
        $this->SetFont('helvetica', '', 8);
        // Page number
        $this->Cell(0, 10, $this->writeHTML($footer), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator('Sistem Langitan Nahdlatul Ulama');
$pdf->SetAuthor('Sistem Langitan');

// $pdf->setPrintHeader(false);
$pdf->SetTopMargin(50);

//Halaman Pertama
$pdf->AddPage();

$backup_style = <<<EOF

EOF;

$html = <<<EOF
<style>
    .header { font-size: 22pt; font-family: times; font-weight: bold; text-align:center;}
    .title { font-size: 13pt; font-family: serif; margin-top: 0px ;text-align:center; }
    th {background-color:#000;color:white;}
    tr.odd {background-color:#dddddd;}
    td.list {border-bottom: none;}
    td { font-size: 10pt; }
    td.prodi { font-size: 9pt; border-bottom: none;}
</style>
<table width="100%">
    <tr>
        <td width="100%" align="center">
            <span class="title">LAMPIRAN<br/>KEPUTUSAN REKTOR {nama_pt}<br/></span>
            Nomor : {nomor_surat}<br/><br/>
            <span class="title">TENTANG</span>
            <br/><br/>
            <span class="title">{baris1}<br/>
				{baris2}<br/>
				{baris3}<br/>
				{baris4}
			</span>
        </td>
    </tr>
</table>
<br/>
<table width="100%" cellpadding="3">
    <tr>
        <th width="7%" align="center">No</th>
        <th width="13%">No.Ujian</th>
        <th width="{width_nama}px">Nama Peserta</th>
        <th width="{width_prodi}px">Program Studi</th>
    </tr>
    {data_calon_mahasiswa}
    {ttd}
</table>
EOF;
$index_terakhir = 0;
$data_mahasiswa = '';
for ($i = 0; $i < $row_per_page; $i++) {
    if (isset($data[$i])) {
        $gelar = $data[$i]['GELAR'] != '' ? ', ' . $data[$i]['GELAR'] : '';
        $data_mahasiswa .= $i % 2 == 0 ? '<tr class="odd">' : '<tr>';
        $data_mahasiswa .='
            <td class="list" align="center">' . ($i + 1) . '</td>
            <td class="list">' . $data[$i]['NO_UJIAN'] . '</td>
            <td class="list">' . strtoupper($data[$i]['NM_C_MHS']) . $gelar .'</td>
            <td class="list prodi">' .$data[$i]['NM_JENJANG'].'-'. $data[$i]['NM_PROGRAM_STUDI'] . '</td>
        </tr>
        ';
        $index_terakhir = $i;
    }
}

$ttd = '<p></p><p></p><tr><td width="70%" colspan="2"></td><td width="30%" colspan="2" align="center">Surabaya, ' . strftime('%d %B %Y') . '<br/>Rektor<br/><br/><br/><br/>Fasich<br/>NIP.194612311974121001</td></tr>';
$ttd2 = '<p></p><p></p><p></p><p></p><tr><td width="70%" colspan="2"></td><td width="30%" colspan="2" align="center">Surabaya, ' . strftime('%d %B %Y') . '<br/>Rektor<br/><br/><br/><br/>Fasich<br/>NIP.194612311974121001</td></tr>';
if ($index_terakhir < ($row_per_page - 1)) {
    $html = str_replace('{ttd}', $ttd, $html);
}else{
    $html = str_replace('{ttd}', '', $html);
}
$html = str_replace('{nama_pt}', $nama_pt_kapital, $html);
$html = str_replace('{nomor_surat}', $data_penetapan['NO_SURAT'], $html);
$html = str_replace('{data_calon_mahasiswa}', $data_mahasiswa, $html);
$html = str_replace('{baris1}',strtoupper($data_penetapan['BARIS_1']),$html);
$html = str_replace('{baris2}',strtoupper($data_penetapan['BARIS_2']),$html);
$html = str_replace('{baris3}',strtoupper($data_penetapan['BARIS_3']),$html);
$html = str_replace('{baris4}',strtoupper($data_penetapan['BARIS_4']),$html);
$html = str_replace('{width_nama}', $width_nama, $html);
$html = str_replace('{width_prodi}', $width_prodi, $html);

$pdf->writeHTML($html);

$pdf->endPage();

//Halaman Selain Halaman Pertama

$index_sisa = ceil((count($data) - $row_per_page) / $row_page_n);
for ($i = 0; $i < $index_sisa; $i++) {
    $pdf->AddPage();
    $html = <<<EOF
<style>
    .header { font-size: 22pt; font-family: times; font-weight: bold; text-align:center;}
    .title { font-size: 13pt; font-family: serif; margin-top: 0px ;text-align:center; }
    th {background-color:#000;color:white;}
    tr.odd {background-color:#dddddd;}
    td.list {border-bottom:none;}
    td { font-size: 10pt; }
    td.prodi { font-size: 9pt; border-bottom:none; }
</style>
<table width="100%" cellpadding="3">
    <tr>
        <th width="7%" align="center">No</th>
        <th width="13%">No.Ujian</th>
        <th width="230px">Nama Peserta</th>
        <th width="200px">Program Studi</th>
    </tr>
    {data_calon_mahasiswa}
    {ttd}
</table>
EOF;
    $data_mahasiswa = '';
    $hal = $row_per_page;
    for ($j = ($hal + ($row_page_n * $i) ); $j < ($hal + ($row_page_n * $i) + $row_page_n); $j++) {
        if (isset($data[$j])) {
            $gelar = $data[$j]['GELAR'] != '' ? ', ' . $data[$j]['GELAR'] : '';
            $data_mahasiswa .= $j % 2 == 0 ? '<tr class="odd">' : '<tr>';
            $data_mahasiswa .='
            <td class="list" align="center">' . ($j + 1) . '</td>
            <td class="list">' . $data[$j]['NO_UJIAN'] . '</td>
            <td class="list">' . $cetak->compress_nama(strtoupper($data[$j]['NM_C_MHS'])) . $gelar .'</td>
            <td class="list prodi">' .$data[$j]['NM_JENJANG'].'-'. $data[$j]['NM_PROGRAM_STUDI'] . '</td>
        </tr>
        ';
        }
    }
    $html = str_replace('{data_calon_mahasiswa}', $data_mahasiswa, $html);
    $ttd = '<tr><td colspan="4">&nbsp;</td></tr><tr><td width="70%" colspan="2"></td><td width="30%" colspan="2" align="center">Surabaya, ' . strftime('%d %B %Y') . '<br/>Rektor<br/><br/><br/><br/>Fasich<br/>NIP.194612311974121001</td></tr>';
	if ($i == ($index_sisa - 1)) {
        $html = str_replace('{ttd}', $ttd, $html);
    } else {
        $html = str_replace('{ttd}', '', $html);
    }

    $pdf->writeHTML($html);
    $pdf->endPage();
}

$pdf->Output();
?>
