<?php
include('config.php');
include('class/snmptn.class.php');

$mode = get('mode', 'view');
$snmptn = new SnmptnClass($db);

if ($request_method == 'POST')
{
    if (post('mode') == 'edit-pk')
	{
		$id_c_mhs = post('id_c_mhs');
		$is_peserta_khusus = post('is_peserta_khusus');
		
		$result = $db->Query("update calon_mahasiswa_sekolah set is_peserta_khusus = {$is_peserta_khusus} where id_c_mhs = {$id_c_mhs}");
		
		echo $result ? "1" : "0";
		
		exit();
	}
    
    if (post('mode') == 'edit-pkd')
    {
        $id_c_mhs = post('id_c_mhs');
		$is_pkd = post('is_pkd');
		
		$result = $db->Query("update calon_mahasiswa_sekolah set is_pkd = {$is_pkd} where id_c_mhs = {$id_c_mhs}");
		
		echo $result ? "1" : "0";
		
		exit();
    }
    
	if (post('mode') == 'update-pm')
	{
		$id_c_mhs = post('id_c_mhs');
		$id_skor_pm = post('id_skor_pm');
        $checked = post('checked');
        
        if ($checked == 'true')
        {
            $db->BeginTransaction();
            $db->Query("insert into calon_mahasiswa_pm (id_c_mhs, id_skor_pm, id_pengisi_pm) values ({$id_c_mhs}, {$id_skor_pm}, {$user->ID_PENGGUNA})");
            $snmptn->RefreshNilai($id_c_mhs);
            $result = $db->Commit();
        }
        else if ($checked == 'false')
        {
            $db->BeginTransaction();
            $db->Query("delete from calon_mahasiswa_pm where id_c_mhs = {$id_c_mhs} and id_skor_pm = {$id_skor_pm}");
            $snmptn->RefreshNilai($id_c_mhs);
            $result = $db->Commit();
        }
        
        echo $result ? "1" : "0";
		
		exit();
	}
    
    if (post('mode') == 'update-ortu')
    {
        $id_c_mhs = post('id_c_mhs');
        $nm_ortu = post('nm_ortu');
        
        $result = $db->Query("update calon_mahasiswa_pm set nm_ortu = '{$nm_ortu}' where id_c_mhs = {$id_c_mhs}");
        
        echo $result ? "1" : "0"; exit();
    }
    
    if (post('mode') == 'update-sponsor')
    {
        $id_c_mhs = post('id_c_mhs');
        $nm_sponsor = post('nm_sponsor');
        
        $result = $db->Query("update calon_mahasiswa_pm set nm_sponsor = '{$nm_sponsor}' where id_c_mhs = {$id_c_mhs}");
        
        echo $result ? "1" : "0"; exit();
    }
    
    if (post('mode') == 'update-keterangan')
    {
        $id_c_mhs = post('id_c_mhs');
        $keterangan = post('keterangan');
        
        $result = $db->Query("update calon_mahasiswa_pm set keterangan = '{$keterangan}' where id_c_mhs = {$id_c_mhs}");
        
        echo $result ? "1" : "0"; exit();
    }
    
}

if ($request_method == 'GET' or $request_method == 'POST')
{	
    if ($mode == 'view')
    {
        $no_ujian = get('no_ujian', '');
		
		$smarty->assign('skor_pm_set', $db->QueryToArray("select * from skor_pm order by group_pm, nilai desc"));
        
        $smarty->assign('cpm_set', $db->QueryToArray("
            select cpm.* from calon_mahasiswa_pm cpm
            join calon_mahasiswa_baru cmb on cmb.id_c_mhs = cpm.id_c_mhs
            where cmb.no_ujian = '{$no_ujian}'"));
		
		$rows = $db->QueryToArray("select id_c_mhs from calon_mahasiswa_baru where no_ujian = '{$no_ujian}'");
		$cmb = $snmptn->GetDetailPeserta(44, $rows[0]['ID_C_MHS']);
        $smarty->assign('cmb', $cmb);
    }
}

$smarty->display("snmptn/pm2/{$mode}.tpl");
?>