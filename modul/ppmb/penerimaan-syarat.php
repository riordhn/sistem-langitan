<?php
include('config.php');
include('class/Penerimaan.class.php');

if (!in_array($user->ID_JABATAN_PEGAWAI, array(175, 176, 203)))
{
    // 203 = Admin
    echo "Anda tidak punya hak akses halaman ini.";
    exit();
}

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
	if (post('mode') == 'add-syarat-umum')
	{
		$db->Parse("insert into penerimaan_syarat_prodi (id_penerimaan, nm_syarat, keterangan, status_wajib, status_file, urutan) values (:1, :2, :3, :4, :5, :6)");
		$db->BindByName(':1', post('id_penerimaan'));
		$db->BindByName(':2', post('nm_syarat'));
		$db->BindByName(':3', post('keterangan'));
		$db->BindByName(':4', post('status_wajib'));
		$db->BindByName(':5', post('status_file'));
		$db->BindByName(':6', post('urutan'));
		$db->Execute();
	}
	
	if (post('mode') == 'edit-syarat-umum')
	{
		$db->Parse("update penerimaan_syarat_prodi set nm_syarat = :1, keterangan = :2, status_wajib = :3, status_file = :4, urutan = :5 where id_syarat_prodi = :6");
		$db->BindByName(':1', post('nm_syarat'));
		$db->BindByName(':2', post('keterangan'));
		$db->BindByName(':3', post('status_wajib'));
		$db->BindByName(':4', post('status_file'));
		$db->BindByName(':5', post('urutan'));
		$db->BindByName(':6', post('id_syarat_prodi'));
		$db->Execute();
	}
	
	if (post('mode') == 'delete-syarat')
	{
		$id_syarat_prodi = post('id_syarat_prodi');
		$db->Query("delete from penerimaan_syarat_prodi where id_syarat_prodi = {$id_syarat_prodi}");
		exit();
	}
	
	if (post('mode') == 'add-syarat-khusus')
	{
		$db->Parse("insert into penerimaan_syarat_prodi (id_penerimaan, nm_syarat, keterangan, status_wajib, status_file, id_program_studi, urutan) values (:1, :2, :3, :4, :5, :6, :7)");
		$db->BindByName(':1', post('id_penerimaan'));
		$db->BindByName(':2', post('nm_syarat'));
		$db->BindByName(':3', post('keterangan'));
		$db->BindByName(':4', post('status_wajib'));
		$db->BindByName(':5', post('status_file'));
		$db->BindByName(':6', post('id_program_studi'));
		$db->BindByName(':7', post('urutan'));
		$db->Execute();
	}
	
	if (post('mode') == 'edit-syarat-khusus')
	{
		$db->Parse("update penerimaan_syarat_prodi set nm_syarat = :1, keterangan = :2, status_wajib = :3, status_file = :4, id_program_studi = :5, urutan = :6 where id_syarat_prodi = :7");
		$db->BindByName(':1', post('nm_syarat'));
		$db->BindByName(':2', post('keterangan'));
		$db->BindByName(':3', post('status_wajib'));
		$db->BindByName(':4', post('status_file'));
		$db->BindByName(':5', post('id_program_studi'));
		$db->BindByName(':6', post('urutan'));
		$db->BindByName(':7', post('id_syarat_prodi'));
		$db->Execute();
	}
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	$id_penerimaan = get('id_penerimaan','');
	$id_program_studi = get('id_program_studi','');
		
	if ($mode == 'view')
	{
		// Menampilkan list penerimaan
		// class
		$penerimaan = new Penerimaan($db);
		$smarty->assign('penerimaan_set', $penerimaan->GetAllForView());

		if ($id_penerimaan != '')
		{
			$program_studi_set = $db->QueryToArray("
				select program_studi.id_program_studi, nm_jenjang, nm_program_studi, gelar_pendek from penerimaan_prodi 
				join program_studi on penerimaan_prodi.id_program_studi = program_studi.id_program_studi
				join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
				where penerimaan_prodi.id_penerimaan = {$id_penerimaan}
				order by id_fakultas, 3");
			$smarty->assign('program_studi_set', $program_studi_set);
			
			$syarat_umum_set = $db->QueryToArray("
				select * from penerimaan_syarat_prodi
				where id_penerimaan = {$id_penerimaan} and id_program_studi is null
				order by status_file, urutan");
			$smarty->assignByRef('syarat_umum_set', $syarat_umum_set);
			
			$syarat_khusus_set = $db->QueryToArray("
				select nm_jenjang, nm_program_studi, penerimaan_syarat_prodi.*  from penerimaan_syarat_prodi
				join program_studi on program_studi.id_program_studi = penerimaan_syarat_prodi.id_program_studi
				join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
				where id_penerimaan = {$id_penerimaan}
				order by nm_program_studi, urutan");
			$smarty->assignByRef('syarat_khusus_set', $syarat_khusus_set);
		}
	}
	
	if ($mode == 'add-syarat-umum')
	{
		$id_penerimaan = get('id_penerimaan');
		
		$db->Query("select * from penerimaan where id_penerimaan = {$id_penerimaan}");
		$penerimaan = $db->FetchAssoc();
		
		$smarty->assign('data', array(
			'NM_PENERIMAAN'	=> $penerimaan['NM_PENERIMAAN'] . ' Gelombang ' . $penerimaan['GELOMBANG'],
			'NM_SYARAT'		=> '',
			'STATUS_WAJIB'	=> 0
		));
		
		// untuk display
		$mode = 'editor-syarat-umum';
	}
	
	if ($mode == 'edit-syarat-umum')
	{
		$id_syarat_prodi = get('id_syarat_prodi');
		
		$db->Query("
			select id_syarat_prodi, nm_penerimaan, gelombang, nm_syarat, keterangan, status_wajib, status_file, urutan from penerimaan_syarat_prodi
			join penerimaan on penerimaan.id_penerimaan = penerimaan_syarat_prodi.id_penerimaan
			where id_syarat_prodi = {$id_syarat_prodi}");
		$data = $db->FetchAssoc();
		
		$smarty->assign('data', $data);
		
		// untuk display
		$mode = 'editor-syarat-umum';
	}
	
	if ($mode == 'add-syarat-khusus')
	{
		$id_penerimaan = get('id_penerimaan');
		
		$db->Query("select * from penerimaan where id_penerimaan = {$id_penerimaan}");
		$penerimaan = $db->FetchAssoc();
		
		$program_studi_set = $db->QueryToArray("
			select program_studi.id_program_studi, nm_program_studi from penerimaan_prodi
			join program_studi on program_studi.id_program_studi = penerimaan_prodi.id_program_studi
			where id_penerimaan = {$id_penerimaan}
			order by 2");
		$smarty->assignByRef('program_studi_set', $program_studi_set);
		
		$smarty->assign('data', array(
			'NM_PENERIMAAN'	=> $penerimaan['NM_PENERIMAAN'] . ' Gelombang ' . $penerimaan['GELOMBANG'],
			'NM_SYARAT'		=> '',
			'STATUS_WAJIB'	=> 0
		));
		
		// untuk display
		$mode = 'editor-syarat-khusus';
	}
	
	if ($mode == 'edit-syarat-khusus')
	{
		$id_syarat_prodi = get('id_syarat_prodi');
		
		$db->Query("
			select id_syarat_prodi, nm_penerimaan, gelombang, nm_syarat, keterangan, status_wajib, status_file, id_program_studi, urutan from penerimaan_syarat_prodi
			join penerimaan on penerimaan.id_penerimaan = penerimaan_syarat_prodi.id_penerimaan
			where id_syarat_prodi = {$id_syarat_prodi}");
		$data = $db->FetchAssoc();
		
		$smarty->assign('data', $data);
		
		$program_studi_set = $db->QueryToArray("
			select program_studi.id_program_studi, nm_program_studi from penerimaan_prodi
			join program_studi on program_studi.id_program_studi = penerimaan_prodi.id_program_studi
			where id_penerimaan = {$id_penerimaan}
			order by 2");
		$smarty->assignByRef('program_studi_set', $program_studi_set);
		
		// untuk display
		$mode = 'editor-syarat-khusus';
	}
}

$smarty->display("penerimaan/syarat/{$mode}.tpl");
?>