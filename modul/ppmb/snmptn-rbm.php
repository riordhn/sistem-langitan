<?php
include('config.php');
include('class/snmptn.class.php');

$mode = get('mode', 'view');

if ($request_method == 'GET')
{
    $id_penerimaan = 44;
    
    if ($mode == 'view')
    {
        $snmptn = new SnmptnClass($db);
        $smarty->assign('ps_set', $snmptn->GetRekapBidikMisi($id_penerimaan));
    }
}

$smarty->display("snmptn/rbm/{$mode}.tpl");
?>
