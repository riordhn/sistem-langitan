<?php
include('config.php');
include('class/Penerimaan.class.php');

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
	if (post('mode') == 'add-tarif')
	{
		$db->Parse("insert into voucher_tarif (id_penerimaan, kode_jurusan, tarif, deskripsi) values (:id_penerimaan,:kode_jurusan,:tarif,:deskripsi)");
		$db->BindByName(':id_penerimaan', post('id_penerimaan'));
		$db->BindByName(':kode_jurusan', post('kode_jurusan'));
		$db->BindByName(':tarif', post('tarif'));
		$db->BindByName(':deskripsi', post('deskripsi'));
		$db->Execute();
	}
	
	if (post('mode') == 'edit-tarif')
	{
		$db->Parse("update voucher_tarif set tarif = :tarif, kode_jurusan = :kode_jurusan, deskripsi = :deskripsi where id_voucher_tarif = :id_voucher_tarif");
		$db->BindByName(':kode_jurusan', post('kode_jurusan'));
		$db->BindByName(':tarif', post('tarif'));
		$db->BindByName(':deskripsi', post('deskripsi'));
		$db->BindByName(':id_voucher_tarif', post('id_voucher_tarif'));
		$result = $db->Execute();
		
		$smarty->assign('result', $result ? "Tarif berhasil disimpan" : "Tarif gagal disimpan");
	}
	
	if (post('mode') == 'tambah-voucher')
	{
		$no_awal        = post('no_awal');
		$no_akhir       = post('no_akhir');
		$id_penerimaan  = post('id_penerimaan');
		$kode_jurusan   = post('kode_jurusan');

		$status_transaksi = false;

		$db->BeginTransaction();
		
		for ($kode_voucher = $no_awal; $kode_voucher <= $no_akhir; $kode_voucher++)
		{
			// mengganti hasil dari md5 hash
			$pin_voucher = str_replace("a", "1", md5($kode_voucher));
			$pin_voucher = str_replace("b", "2", $pin_voucher);
			$pin_voucher = str_replace("c", "3", $pin_voucher);
			$pin_voucher = str_replace("d", "4", $pin_voucher);
			$pin_voucher = str_replace("e", "5", $pin_voucher);
			$pin_voucher = str_replace("f", "6", $pin_voucher);
			
			// Format PIN 2014 :
			// digit ke 1 : nilai acak antara 1 dan 9
			// digit ke 2-8 : nilai acak yg diambil dari md5(kode_voucher), yg huruf-nya sudah dikonversi
			$pin_voucher = mt_rand(1,9) . substr($pin_voucher, mt_rand(0, 25), 7);
			
			// hasil 
			//echo "{$kode_voucher}; {$pin_voucher};<br/>";
			
			// insert ke database

			$result = $db->Query("insert into voucher (id_penerimaan, kode_voucher, pin_voucher, kode_jurusan, is_tagih, is_aktif) values ({$id_penerimaan},'{$kode_voucher}','{$pin_voucher}', '{$kode_jurusan}', 'Y', 0)");

			if (!$result)
			{
				// Rollback transaksi
				$db->Rollback();

				// Ambil informasi penerimaan
				$db->Query("
					select nm_penerimaan, tahun, gelombang, semester from penerimaan where id_penerimaan in (
						select id_penerimaan from voucher where kode_voucher = '{$kode_voucher}')");
				$row = $db->FetchAssoc();

				$smarty->assign('result', "Gagal menambahkan voucher: Voucher {$kode_voucher} sudah ada di penerimaan {$row['NM_PENERIMAAN']} {$row['TAHUN']} {$row['SEMESTER']} {$row['GELOMBANG']}");
				
				$status_transaksi = false;

				break;
			}
			else
			{
				$status_transaksi = true;
			}
		}

		if ($status_transaksi == true)
		{
			$db->Commit();
			$smarty->assign('result', 'Voucher berhasil ditambahkan');
		}
	}
	
	if (post('mode') == 'aktifkan-voucher')
	{
		$no_awal = post('no_awal');
		$no_akhir = post('no_akhir');
		$id_penerimaan = post('id_penerimaan');
		
		$result = $db->Query("update voucher set is_aktif = 1 where id_penerimaan = {$id_penerimaan} and kode_voucher >= '{$no_awal}' and kode_voucher <= '{$no_akhir}'");
		
		$smarty->assign('result', $result ? "Pengaktifan voucher berhasil" : "Gagal mengaktifkan voucher");
	}
	
	if (post('mode') == 'nonaktif-voucher')
	{
		$no_awal = post('no_awal');
		$no_akhir = post('no_akhir');
		$id_penerimaan = post('id_penerimaan');
		
		$result = $db->Query("update voucher set is_aktif = 0 where id_penerimaan = {$id_penerimaan} and kode_voucher >= '{$no_awal}' and kode_voucher <= '{$no_akhir}'");
		
		$smarty->assign('result', $result ? "Menonaktifkan voucher berhasil" : "Gagal menonaktifkan voucher");
	}
	
	if (post('mode') == 'manual-voucher')
	{
		$no_awal = post('no_awal');
		$no_akhir = post('no_akhir');
		$id_penerimaan = post('id_penerimaan');
		$kode_jurusan = post('kode_jurusan');
		
		$result = $db->Query("update voucher set kode_jurusan = '{$kode_jurusan}', is_aktif = 1, tgl_ambil = current_date, tgl_bayar = current_date, keterangan = '{$user->ID_PENGGUNA}' where id_penerimaan = {$id_penerimaan} and kode_voucher >= '{$no_awal}' and kode_voucher <= '{$no_akhir}' and no_transaksi is null");
		
		$smarty->assign('result', $result ? "Manual bayar voucher berhasil" : "Gagal manual bayar voucher");
	}
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$id_penerimaan = get('id_penerimaan', '');
		
		// master penerimaan
		$penerimaan = new Penerimaan($db);
		$smarty->assign('penerimaan_set', $penerimaan->GetAllForView());
		
		if ($id_penerimaan != '')
		{
			$smarty->assign('voucher_tarif_set', $db->QueryToArray("
				select
					voucher_tarif.*,
					case kode_jurusan when '01' then 'IPA/Reguler' when '02' then 'IPS' when '03' then 'IPC' end as kode_jurusan_parse
				from voucher_tarif
				where id_penerimaan = {$id_penerimaan}
				order by kode_jurusan"));
			
			$rows = $db->QueryToArray("
				select 
					(select count(*) from voucher where id_penerimaan = {$id_penerimaan}) total,
					(select count(*) from voucher where id_penerimaan = {$id_penerimaan} and is_aktif = 0) belum_aktif,
					(select count(*) from voucher where id_penerimaan = {$id_penerimaan} and is_aktif = 1) aktif,
					(select count(*) from voucher where id_penerimaan = {$id_penerimaan} and tgl_ambil is not null) pengambilan,
					(select count(*) from voucher where id_penerimaan = {$id_penerimaan} and tgl_bayar is not null) pembelian,
					(select min(kode_voucher) from voucher where id_penerimaan = {$id_penerimaan}) no_awal,
					(select max(kode_voucher) from voucher where id_penerimaan = {$id_penerimaan}) no_akhir
				from dual");
			$smarty->assign('voucher', $rows[0]);
		}
	}
	
	if ($mode == 'list-voucher')
	{
		$id_penerimaan = get('id_penerimaan', '');
		
		$penerimaan = $db->QueryToArray("select * from penerimaan where id_penerimaan = {$id_penerimaan}");
		$smarty->assign('penerimaan', $penerimaan[0]);
		
		$voucher_set = $db->QueryToArray("
			select kode_voucher, pin_voucher, is_aktif, nm_bank,
				tgl_ambil, tgl_bayar, kode_jurusan
			from voucher v
			left join bank b on b.id_bank = v.id_bank
			where id_penerimaan = {$id_penerimaan} order by kode_voucher");
		$smarty->assign('voucher_set', $voucher_set);
	}
	
	if ($mode == 'add-tarif')
	{
		$id_penerimaan = get('id_penerimaan');
		
		$rows = $db->QueryToArray("select * from penerimaan where id_penerimaan = {$id_penerimaan}");
		$smarty->assign('penerimaan', $rows[0]);
	}
	
	if ($mode == 'edit-tarif')
	{
		$id_voucher_tarif = get('id_voucher_tarif');
		
		$rows = $db->QueryToArray("
			select id_voucher_tarif, vt.id_penerimaan, tarif, deskripsi, kode_jurusan, id_jenjang from voucher_tarif vt
			join penerimaan p on p.id_penerimaan = vt.id_penerimaan
			where vt.id_voucher_tarif = {$id_voucher_tarif}");
		$smarty->assign('vt', $rows[0]);
	}
	
	if ($mode == 'tambah-voucher')
	{
		$id_penerimaan = get('id_penerimaan');
		
		// Select penerimaan
		$rows = $db->QueryToArray("select * from penerimaan where id_penerimaan = {$id_penerimaan}");
		$smarty->assign('penerimaan', $rows[0]);
		
		// Select voucher tarif
		$voucher_tarif_set = $db->QueryToArray("select * from voucher_tarif where id_penerimaan = {$id_penerimaan} order by kode_jurusan");
		$smarty->assign('voucher_tarif_set', $voucher_tarif_set);
		
		// master-master
		$kode_jalur = array(
			'j3'    => '01',  // Mandiri (S1)
			'j6'    => '02',  // Pasca (S2 / S3)
			'j23'   => '04',  // Profesi (Pr)
			'j24'   => '05',  // Spesialis (Sp)
			'j5'    => '06',  // Diploma (D3)
			'j4'    => '07', // Alih Jenis (S1)
			'j20'   => '08', // Mandiri Depag (S1)
			'j27'   => '09', // Mandiri Internasional
			'j34'   => '10', // Kerja sama haluoleo (S2)
			'j32'   => '11', // Kerja sama borneo (S2)
			'j35'   => '12', // Kerja sama jakarta (S2)
		);
		
		// default pengenal H2H
		$kode_voucher = "9";
		
		// tambah kode jalur
		if ($rows[0]['ID_JENJANG'] == 3)
			$kode_voucher .= "02";
		else
			$kode_voucher .= $kode_jalur["j".$rows[0]['ID_JALUR']];
		
		// tambah kode tahun
		$kode_voucher .= substr($rows[0]['TAHUN'], 2, 2);
		
		// kode kelas khusus
		$kode_voucher .= "0";
		
		$smarty->assign('kode_voucher', $kode_voucher);
	}
	
	if ($mode == 'aktifkan-voucher')
	{
		$id_penerimaan = get('id_penerimaan');
		
		$rows = $db->QueryToArray("select * from penerimaan where id_penerimaan = {$id_penerimaan}");
		$smarty->assign('penerimaan', $rows[0]);
	}
	
	if ($mode == 'nonaktif-voucher')
	{
		$id_penerimaan = get('id_penerimaan');
		
		$rows = $db->QueryToArray("select * from penerimaan where id_penerimaan = {$id_penerimaan}");
		$smarty->assign('penerimaan', $rows[0]);
	}
	
	if ($mode == 'manual-voucher')
	{
		$id_penerimaan = get('id_penerimaan');
		
		$rows = $db->QueryToArray("select * from penerimaan where id_penerimaan = {$id_penerimaan}");
		$smarty->assign('penerimaan', $rows[0]);
	}
}


$smarty->display("penerimaan/voucher/{$mode}.tpl");
?>
