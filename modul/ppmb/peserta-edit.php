<?php
include('config.php');

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
	$id_c_mhs = post('id_c_mhs');
	
	$db->Parse("update calon_mahasiswa_baru set gelar = :gelar, nm_c_mhs = :nm_c_mhs where id_c_mhs = :id_c_mhs");
	$db->BindByName(':gelar', post('gelar'));
	$db->BindByName(':nm_c_mhs', post('nm_c_mhs'));
	$db->BindByName(':id_c_mhs', $id_c_mhs);
	$result = $db->Execute();

	$db->Parse("
		update calon_mahasiswa_sekolah set
			jumlah_pelajaran_ijazah = :1,
			nilai_ijazah = :2,
			jumlah_pelajaran_uan = :3,
			nilai_uan = :4,
			id_sekolah_asal = :5
		where id_c_mhs = :id_c_mhs");
	$db->BindByName(':1', post('jumlah_pelajaran_ijazah'));
	$db->BindByName(':2', post('nilai_ijazah'));
	$db->BindByName(':3', post('jumlah_pelajaran_uan'));
	$db->BindByName(':4', post('nilai_uan'));
	$db->BindByName(':5', post('id_sekolah_asal'));
	$db->BindByName(':id_c_mhs', $id_c_mhs);
	$result = $db->Execute();
	
	if ($result)
		$smarty->assign('changed', 'Data berhasil dirubah');
	else
		$smarty->assign('change_failed', 'Data gagal dirubah');
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$kode_voucher = get('kode_voucher', '');

		$db->Parse(
			"SELECT 
				cmb.id_c_mhs, p.id_jenjang, cmb.kode_voucher, nm_c_mhs, gelar, nm_program_studi, telp,
				jumlah_pelajaran_ijazah, nilai_ijazah, jumlah_pelajaran_uan, nilai_uan,
				id_sekolah_asal,
				to_char(v.tgl_bayar, 'YYYY-MM-DD HH24:MI:SS') as tgl_bayar
			from calon_mahasiswa_baru cmb
			join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
			join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
			left join program_studi ps1 on ps1.id_program_studi = cmb.id_pilihan_1
			left join voucher v on v.kode_voucher = cmb.kode_voucher
			where (cmb.kode_voucher = :kode_voucher or no_ujian = :kode_voucher) and rownum <= 1");
		$db->BindByName(':kode_voucher', $kode_voucher);
		$db->Execute();

		$cmb = $db->FetchAssoc();
		$smarty->assignByRef('cmb', $cmb);

		// Mendapatkan data sekolah
		if ($cmb['ID_SEKOLAH_ASAL'] != '')
		{
			// mengambil data id_kota, id_provinsi, id_negara
			$db->Query(
				"SELECT K.ID_KOTA, P.ID_PROVINSI, P.ID_NEGARA
				FROM SEKOLAH S
				JOIN KOTA K ON K.ID_KOTA = S.ID_KOTA
				JOIN PROVINSI P ON P.ID_PROVINSI = K.ID_PROVINSI
				WHERE S.ID_SEKOLAH = {$cmb['ID_SEKOLAH_ASAL']}");
			$sekolah = $db->FetchAssoc();
			$smarty->assignByRef('sekolah_asal', $sekolah);

			$sekolah_set = $db->QueryToArray("SELECT * FROM SEKOLAH WHERE ID_KOTA = {$sekolah['ID_KOTA']} ORDER BY NM_SEKOLAH");
			$smarty->assignByRef('sekolah_set', $sekolah_set);

			$kota_set = $db->QueryToArray("SELECT * FROM KOTA WHERE ID_PROVINSI = {$sekolah['ID_PROVINSI']} ORDER BY TIPE_DATI2, NM_KOTA");
			$smarty->assignByRef('kota_set', $kota_set);

			$provinsi_set = $db->QueryToArray("SELECT * FROM PROVINSI WHERE ID_NEGARA = {$sekolah['ID_NEGARA']} ORDER BY NM_PROVINSI");
			$smarty->assignByRef('provinsi_set', $provinsi_set);
		}
		
		$negara_set = $db->QueryToArray("SELECT * FROM NEGARA ORDER BY NM_NEGARA");
		$smarty->assignByRef('negara_set', $negara_set);
	}
	
	if ($mode == 'get-provinsi')
	{
		$id_negara = get('id_negara');
		$provinsi_set = $db->QueryToArray("SELECT * FROM PROVINSI WHERE ID_NEGARA = {$id_negara} ORDER BY NM_PROVINSI");
		echo '<option value=""></option>';
		foreach ($provinsi_set as $provinsi)
		{
			echo "<option value=\"{$provinsi['ID_PROVINSI']}\">{$provinsi['NM_PROVINSI']}</option>";
		}
		exit();
	}
	
	if ($mode == 'get-kota')
	{
		$id_provinsi = get('id_provinsi');
		$kota_set = $db->QueryToArray("SELECT * FROM KOTA WHERE ID_PROVINSI = {$id_provinsi} ORDER BY TIPE_DATI2, NM_KOTA");
		echo '<option value=""></option>';
		foreach ($kota_set as $kota)
		{
			echo "<option value=\"{$kota['ID_KOTA']}\">{$kota['TIPE_DATI2']} {$kota['NM_KOTA']}</option>";
		}
		exit();
	}
	
	if ($mode == 'get-sekolah')
	{
		$id_kota = get('id_kota');
		$sekolah_set = $db->QueryToArray("SELECT * FROM SEKOLAH WHERE ID_KOTA = {$id_kota} ORDER BY NM_SEKOLAH");
		echo '<option value=""></option>';
		foreach ($sekolah_set as $sekolah)
		{
			echo "<option value=\"{$sekolah['ID_SEKOLAH']}\">{$sekolah['NM_SEKOLAH']}</option>";
		}
		exit();
	}
}

$smarty->display("peserta/edit/view.tpl");