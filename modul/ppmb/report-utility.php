<?php
include('config.php');

$mode = get('mode', 'view');

if ($mode == 'view')
{
	$penerimaan_set = $db->QueryToArray(
		"SELECT 
			p.*, j.nm_jenjang,

			(select count(cms.id_c_mhs) from calon_mahasiswa_baru cmb
			join calon_mahasiswa_syarat cms on cms.id_c_mhs = cmb.id_c_mhs
			where cmb.id_penerimaan = p.id_penerimaan and cmb.no_ujian is not null) as jumlah_file
		FROM penerimaan p
		JOIN jenjang j on j.id_jenjang = p.id_jenjang
		JOIN V_NAMA_PENERIMAAN vnp ON vnp.id_penerimaan = p.id_penerimaan
		where p.is_aktif = 1 and p.id_perguruan_tinggi = '{$id_pt_user}'
		order by p.tahun, p.semester, p.gelombang, p.id_jenjang, p.id_jalur, vnp.nama_penerimaan");
	
	$smarty->assignByRef('penerimaan_set', $penerimaan_set);	
}

if ($mode == 'cek_zip_files')
{
	$id_penerimaan = get('id_penerimaan');
	$response = file_get_contents("http://{$web_pmb}/index.php/api/cek_zip_files/{$id_penerimaan}");
	echo $response;
	exit();
}

if ($mode == 'zip_files')
{
	$id_penerimaan = get('id_penerimaan');
	$response = file_get_contents("http://{$web_pmb}/index.php/api/zip_files/{$id_penerimaan}");
	echo $response;
	exit();
}

if ($mode == 'refresh-pilihan')
{
	$id_penerimaan = get('id_penerimaan');

	$cmb_set = $db->QueryToArray("
		select
			cmb.id_c_mhs, cmb.kode_jurusan,
			cmb.id_pilihan_1, ps1.jurusan_sekolah as jurusan_sekolah_1,
			cmb.id_pilihan_2, ps2.jurusan_sekolah as jurusan_sekolah_2,
			cmb.id_pilihan_3, ps3.jurusan_sekolah as jurusan_sekolah_3,
			cmb.id_pilihan_4, ps4.jurusan_sekolah as jurusan_sekolah_4
		from calon_mahasiswa_baru cmb
		left join program_studi ps1 on ps1.id_program_studi = cmb.id_pilihan_1
		left join program_studi ps2 on ps2.id_program_studi = cmb.id_pilihan_2
		left join program_studi ps3 on ps3.id_program_studi = cmb.id_pilihan_3
		left join program_studi ps4 on ps4.id_program_studi = cmb.id_pilihan_4
		where
			cmb.no_ujian is not null and
			cmb.id_penerimaan = {$id_penerimaan}");

	// Clear record di penerimaan terpilih
	$db->Query(
		"delete from calon_mahasiswa_pilihan where id_c_mhs in (
		select id_c_mhs from calon_mahasiswa_baru where 
		id_penerimaan = {$id_penerimaan})");

	foreach ($cmb_set as $cmb)
	{
		$pilihan_ke = 1;

		// Voucher IPA / IPS
		if ($cmb['KODE_JURUSAN'] == '01' || $cmb['KODE_JURUSAN'] == '02')
		{
			if ( ! empty($cmb['ID_PILIHAN_1']))
			{
				$db->Query("
					insert into calon_mahasiswa_pilihan
					(id_c_mhs, jurusan_sekolah, pilihan_ke, id_pilihan) values
					({$cmb['ID_C_MHS']}, {$cmb['JURUSAN_SEKOLAH_1']}, {$pilihan_ke}, {$cmb['ID_PILIHAN_1']})");
				
				$pilihan_ke++;
			}
			
			if ( ! empty($cmb['ID_PILIHAN_2']))
			{
				$db->Query("
					insert into calon_mahasiswa_pilihan
					(id_c_mhs, jurusan_sekolah, pilihan_ke, id_pilihan) values
					({$cmb['ID_C_MHS']}, {$cmb['JURUSAN_SEKOLAH_2']}, {$pilihan_ke}, {$cmb['ID_PILIHAN_2']})");
			}
		}
		
		$pilihan_ke_ipa = 1;
		$pilihan_ke_ips = 1;
		
		// IPC
		if ($cmb['KODE_JURUSAN'] == '03')
		{
			if ( ! empty($cmb['ID_PILIHAN_1']))
			{
				if ($cmb['JURUSAN_SEKOLAH_1'] == 1)  // prodi ipa
				{
						
					$db->Query("
						insert into calon_mahasiswa_pilihan
						(id_c_mhs, jurusan_sekolah, pilihan_ke, id_pilihan) values
						({$cmb['ID_C_MHS']}, {$cmb['JURUSAN_SEKOLAH_1']}, {$pilihan_ke_ipa}, {$cmb['ID_PILIHAN_1']})");
					
					$pilihan_ke_ipa++;
				}
				
				if ($cmb['JURUSAN_SEKOLAH_1'] == 2)  // prodi ips
				{
					$db->Query("
						insert into calon_mahasiswa_pilihan
						(id_c_mhs, jurusan_sekolah, pilihan_ke, id_pilihan) values
						({$cmb['ID_C_MHS']}, {$cmb['JURUSAN_SEKOLAH_1']}, {$pilihan_ke_ips}, {$cmb['ID_PILIHAN_1']})");
					
					$pilihan_ke_ips++;
				}
			}
			
			if ( ! empty($cmb['ID_PILIHAN_2']))
			{
				if ($cmb['JURUSAN_SEKOLAH_2'] == 1)  // prodi ipa
				{
					$db->Query("
						insert into calon_mahasiswa_pilihan
						(id_c_mhs, jurusan_sekolah, pilihan_ke, id_pilihan) values
						({$cmb['ID_C_MHS']}, {$cmb['JURUSAN_SEKOLAH_2']}, {$pilihan_ke_ipa}, {$cmb['ID_PILIHAN_2']})");
						
					$pilihan_ke_ipa++;
				}
				
				if ($cmb['JURUSAN_SEKOLAH_2'] == 2)  // prodi ips
				{
					$db->Query("
						insert into calon_mahasiswa_pilihan
						(id_c_mhs, jurusan_sekolah, pilihan_ke, id_pilihan) values
						({$cmb['ID_C_MHS']}, {$cmb['JURUSAN_SEKOLAH_2']}, {$pilihan_ke_ips}, {$cmb['ID_PILIHAN_2']})");
					
					$pilihan_ke_ips++;
				}
			}
			
			if ( ! empty($cmb['ID_PILIHAN_3']))
			{
				if ($cmb['JURUSAN_SEKOLAH_3'] == 1)  // prodi ipa
				{
					$db->Query("
						insert into calon_mahasiswa_pilihan
						(id_c_mhs, jurusan_sekolah, pilihan_ke, id_pilihan) values
						({$cmb['ID_C_MHS']}, {$cmb['JURUSAN_SEKOLAH_3']}, {$pilihan_ke_ipa}, {$cmb['ID_PILIHAN_3']})");
						
					$pilihan_ke_ipa++;
				}
				
				if ($cmb['JURUSAN_SEKOLAH_3'] == 2)  // prodi ips
				{
					$db->Query("
						insert into calon_mahasiswa_pilihan
						(id_c_mhs, jurusan_sekolah, pilihan_ke, id_pilihan) values
						({$cmb['ID_C_MHS']}, {$cmb['JURUSAN_SEKOLAH_3']}, {$pilihan_ke_ips}, {$cmb['ID_PILIHAN_3']})");
					
					$pilihan_ke_ips++;
				}
			}
			
			if ( ! empty($cmb['ID_PILIHAN_4']))
			{
				if ($cmb['JURUSAN_SEKOLAH_4'] == 1)  // prodi ipa
				{
					$db->Query("
						insert into calon_mahasiswa_pilihan
						(id_c_mhs, jurusan_sekolah, pilihan_ke, id_pilihan) values
						({$cmb['ID_C_MHS']}, {$cmb['JURUSAN_SEKOLAH_4']}, {$pilihan_ke_ipa}, {$cmb['ID_PILIHAN_4']})");
						
					$pilihan_ke_ipa++;
				}
				
				if ($cmb['JURUSAN_SEKOLAH_4'] == 2)  // prodi ips
				{
					$db->Query("
						insert into calon_mahasiswa_pilihan
						(id_c_mhs, jurusan_sekolah, pilihan_ke, id_pilihan) values
						({$cmb['ID_C_MHS']}, {$cmb['JURUSAN_SEKOLAH_4']}, {$pilihan_ke_ips}, {$cmb['ID_PILIHAN_4']})");
						
					$pilihan_ke_ips++;
				}
			}
		}
	}

	echo "OK";

	exit();
}

if ($mode == 'kapitalis-nama')
{
	$id_penerimaan = get('id_penerimaan');

	$db->Query(
		"update calon_mahasiswa_baru set nm_c_mhs = upper(nm_c_mhs)
		where 
			no_ujian is not null and
			id_penerimaan = {$id_penerimaan}");

	echo "OK";

	exit();
}

// Mengambil jumlah foto yang akan di copy
if ($mode == 'copy-foto')
{
	$id_penerimaan = get('id_penerimaan');

	$db->Query("select count(id_c_mhs) as jumlah from aucc.calon_mahasiswa_baru where id_penerimaan = {$id_penerimaan} and no_ujian is not null");
	$row = $db->FetchAssoc();

	echo $row['JUMLAH'];

	exit();
}

// Melakukan pengambilan foto dari server pendaftaran (10.0.110.55)
if ($mode == 'copy-foto-do')
{
	$id_penerimaan	= get('id_penerimaan');
	$urutan			= get('urutan');

	// Mengambil row CMB
	$db->Query(
		"select t.* from (
			select rank() over (order by no_ujian) urutan, 
				p.tahun, p.id_jenjang, p.id_jalur, p.semester, p.gelombang,
				cmb.id_penerimaan, cmb.id_c_mhs, cmb.no_ujian, cmf.file_foto
			from aucc.calon_mahasiswa_baru cmb
			join aucc.penerimaan p on p.id_penerimaan = cmb.id_penerimaan
			join aucc.calon_mahasiswa_file cmf on cmf.id_c_mhs = cmb.id_c_mhs
			where cmb.id_penerimaan = {$id_penerimaan} and no_ujian is not null
		) t where t.urutan = {$urutan}");
	$cmb = $db->FetchAssoc();

	// Jenjang S1, D4, D5
	if (in_array($cmb['ID_JENJANG'], array(1, 4, 5)))
	{
		$folder_ujian = "ujian-mandiri";
	}
	
	// Jenjang S2, S3, Pr, Sp
	if (in_array($cmb['ID_JENJANG'], array(2, 3, 9, 10)))
	{
		$folder_ujian = "ujian-pasca";
	}

	$base_folder = "/var/www/html/img/foto";

	// create folder tahun jika belum ada
	if ( ! file_exists("{$base_folder}/{$folder_ujian}/{$cmb['TAHUN']}"))
	{
		mkdir("{$base_folder}/{$folder_ujian}/{$cmb['TAHUN']}");
		chmod("{$base_folder}/{$folder_ujian}/{$cmb['TAHUN']}", 0777);
	}

	// create folder semester-gelombang jika belum ada
	if ( ! file_exists("{$base_folder}/{$folder_ujian}/{$cmb['TAHUN']}/{$cmb['SEMESTER']}{$cmb['GELOMBANG']}"))
	{
		mkdir("{$base_folder}/{$folder_ujian}/{$cmb['TAHUN']}/{$cmb['SEMESTER']}{$cmb['GELOMBANG']}/");
		chmod("{$base_folder}/{$folder_ujian}/{$cmb['TAHUN']}/{$cmb['SEMESTER']}{$cmb['GELOMBANG']}/", 0777);
	}

	// change directory ke folder foto peserta
	chdir("/var/www/html/img/foto/{$folder_ujian}/{$cmb['TAHUN']}/{$cmb['SEMESTER']}{$cmb['GELOMBANG']}/");

	// start download file
	exec("wget -q -O {$cmb['NO_UJIAN']}.jpg http://10.0.110.55/files/{$cmb['ID_PENERIMAAN']}/{$cmb['ID_C_MHS']}/{$cmb['FILE_FOTO']} ");
	// echo "wget -q -O {$cmb['NO_UJIAN']}.jpg http://10.0.110.55/files/{$cmb['ID_PENERIMAAN']}/{$cmb['ID_C_MHS']}/{$cmb['FILE_FOTO']} ";

	exit();
}

$smarty->display('report/utility/view.tpl');