<?php
include('config.php');

$data_set = $db->QueryToArray("
    SELECT * FROM (
        SELECT
          ALAMAT, ALAMAT_AYAH, ALAMAT_IBU, ALAMAT_KANTOR, ANGGOTA_KELUARGA,
          BEASISWA, BERKAS_AKTA, BERKAS_IJAZAH, BERKAS_KESANGGUPAN_SP3, BERKAS_KESEHATAN, BERKAS_KP, BERKAS_KSK, BERKAS_LAIN, BERKAS_LAIN_KET, BERKAS_LISTRIK, BERKAS_PEMBAYARAN, BERKAS_PENGHASILAN, BERKAS_SKCK, BERKAS_SKHUN, BERKAS_SPPT, BERKAS_STNK, BERKAS_STTB, BERKAS_TANI, BERKAS_TRANSKRIP,
          ELPT_KEHADIRAN, ELPT_LISTENING, ELPT_READING, ELPT_SCORE, ELPT_STRUCTURE, EMAIL, GELOMBANG_PMDK,
          ID_AGAMA, ID_C_MHS, ID_JADWAL_KESEHATAN, ID_JADWAL_PPMB, ID_JADWAL_TOEFL, ID_JALUR, ID_KOTA_AYAH, ID_KOTA_IBU, ID_KOTA_LAHIR, ID_PENERIMAAN, ID_PILIHAN_1, ID_PILIHAN_2, ID_PILIHAN_3, ID_PILIHAN_4, ID_PROGRAM_STUDI, ID_SEKOLAH_ASAL, ID_VERIFIKATOR, INFO_LAIN,
          JAKET, JENIS_KELAMIN, JUMLAH_ADIK, JUMLAH_KAKAK, JUMLAH_PELAJARAN_IJAZAH, JUMLAH_PELAJARAN_UAN, JURUSAN_SEKOLAH, JURUSAN_SEKOLAH_LAIN,
          KEDIAMAN_ORTU, KEKAYAAN_LAIN, KENDARAAN_R2, KENDARAAN_R4, KESEHATAN_BUTA_PARSIAL, KESEHATAN_BUTA_TOTAL, KESEHATAN_CATATAN, KESEHATAN_KEHADIRAN1, KESEHATAN_KEHADIRAN2, KESEHATAN_KESIMPULAN_AKHIR, KESEHATAN_KESIMPULAN_MATA, KESEHATAN_KESIMPULAN_THT, KESEHATAN_MAKULA, KESEHATAN_PAPIL, KESEHATAN_SUARA_KANAN, KESEHATAN_SUARA_KIRI, KESEHATAN_TIMPANI_KANAN, KESEHATAN_TIMPANI_KIRI, KESEHATAN_VISUS, KEWARGANEGARAAN, KEWARGANEGARAAN_LAIN, KODE_JURUSAN, KODE_VOUCHER,
          LISTRIK, LUAS_BANGUNAN, LUAS_TANAH,
          MUTZ,
          NAMA_AYAH, NAMA_IBU, NILAI_IJAZAH, NILAI_UAN, NIM_LAMA, NJOP, NM_C_MHS, NO_IDENTITAS, NO_IJAZAH, NO_INVOICE, NO_UJIAN, NOMOR_KSK,
          PEKERJAAN_AYAH, PEKERJAAN_AYAH_LAIN, PEKERJAAN_IBU, PEKERJAAN_IBU_LAIN, PENDIDIKAN_AYAH, PENDIDIKAN_IBU, PENGHASILAN_ORTU, PIN_VOUCHER, PT_INSTANSI,
          SKALA_PEKERJAAN_ORTU, SP3, SP3_1, SP3_2, SP3_3, SP3_4, SP3_LAIN, SP3_VERIFIKASI, STATUS, STATUS_ALAMAT_AYAH, STATUS_ALAMAT_IBU, STATUS_BIDIK_MISI, STATUS_BIDIK_MISI_BARU, STATUS_NIM_LAMA, STATUS_PERKAWINAN, STATUS_PRA_UJIAN, STATUS_PT_ASAL, STATUS_TRANSFER, SUMBER_BIAYA, SUMBER_BIAYA_LAIN,
          TAHUN_LULUS, TAHUN_UAN, TELP, TELP_AYAH, TELP_IBU, TEMPAT_TEST, TERIMA_BUKU, TERIMA_JADWAL_ELPT, TERIMA_JADWAL_KESEHATAN,
          TO_CHAR(TGL_AKTIF_PMDK, 'YYYY-MM-DD') AS TGL_AKTIF_PMDK,
          TO_CHAR(TGL_AMBIL_JAKET_MUTZ, 'YYYY-MM-DD') AS TGL_AMBIL_JAKET_MUTZ ,
          TO_CHAR(TGL_IJAZAH, 'YYYY-MM-DD') AS TGL_IJAZAH,
          TO_CHAR(TGL_ISI, 'YYYY-MM-DD') AS TGL_ISI,
          TO_CHAR(TGL_JADWAL_VERIFIKASI, 'YYYY-MM-DD') AS TGL_JADWAL_VERIFIKASI,
          TO_CHAR(TGL_JATUH_TEMPO, 'YYYY-MM-DD') AS TGL_JATUH_TEMPO,
          TO_CHAR(TGL_LAHIR, 'YYYY-MM-DD') AS TGL_LAHIR,
          TO_CHAR(TGL_PSN_JAKET_MUTZ, 'YYYY-MM-DD') AS TGL_PSN_JAKET_MUTZ
        FROM CALON_MAHASISWA CM
    ) WHERE ROWNUM <= 1000");
$smarty->assign('data_set', $data_set);

$smarty->display("transfer-cm.tpl");
?>
