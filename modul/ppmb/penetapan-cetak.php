<?php
include('config.php');

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        $smarty->assign('penetapan_set', $db->QueryToArray("select * from penetapan where id_perguruan_tinggi = {$id_pt_user} order by periode desc"));
    }
    
    if ($mode == 'sidang')
    {
        $id_penetapan = get('id_penetapan');
        
        $db->Query("select * from penetapan where id_penetapan = {$id_penetapan}");
        $row = $db->FetchAssoc();
        $smarty->assign('penetapan', $row);
        
        $penerimaan_set = $db->QueryToArray("
            select pp.id_penetapan_penerimaan, p.nm_penerimaan, p.gelombang, p.semester, p.tahun from penetapan_penerimaan pp
            join penerimaan p on p.id_penerimaan = pp.id_penerimaan
            where pp.id_penetapan = {$id_penetapan}
            order by p.id_jenjang, p.tahun, p.semester, p.gelombang, p.nm_penerimaan");
        
        for ($i = 0; $i < count($penerimaan_set); $i++)
        {
            $penerimaan_set[$i]['program_studi_set'] = $db->QueryToArray("
                select
                    pp.id_penetapan_penerimaan, ps.id_program_studi, f.singkatan_fakultas||' - '||substr(j.nm_jenjang,1,2)||' '||ps.nm_program_studi as nm_program_studi,
                    (select count(*) from calon_mahasiswa_baru cmb where cmb.id_penerimaan = pps.id_penerimaan and cmb.id_pilihan_1 = ps.id_program_studi and no_ujian is not null and tgl_verifikasi_ppmb is not null) as jumlah_peminat,
                    (select count(*) from calon_mahasiswa_baru cmb where cmb.id_penerimaan = pps.id_penerimaan and cmb.id_program_studi = ps.id_program_studi and tgl_diterima is not null) as jumlah_diterima,
                    (select kuota from statistik_cmhs sc where sc.id_penerimaan = pps.id_penerimaan and sc.id_program_studi = ps.id_program_studi) as kuota
                from penetapan_penerimaan pp
                join penerimaan_prodi pps on pps.id_penerimaan = pp.id_penerimaan
                join program_studi ps on ps.id_program_studi = pps.id_program_studi
                join fakultas f on f.id_fakultas = ps.id_fakultas
                join jenjang j on j.id_jenjang = ps.id_jenjang
                where pp.id_penetapan = {$id_penetapan} and pp.id_penetapan_penerimaan = {$penerimaan_set[$i]['ID_PENETAPAN_PENERIMAAN']} and pps.is_aktif = 1
                order by ps.id_fakultas, ps.nm_program_studi");
        }
        
        $smarty->assign('penerimaan_set', $penerimaan_set);
    }
    
    if ($mode == 'detail')
    {
        $id_penetapan_penerimaan = get('id_penetapan_penerimaan');
        $id_program_studi = get('id_program_studi');
        
        // nama penetapan
        $db->Query("
            select p.id_penetapan, p.nm_penetapan, pp.id_penerimaan from penetapan_penerimaan pp
            join penetapan p on p.id_penetapan = pp.id_penetapan
            where id_penetapan_penerimaan = {$id_penetapan_penerimaan}");
        $row = $db->FetchAssoc();
        $id_penerimaan = $row['ID_PENERIMAAN'];
        $smarty->assign('penetapan', $row);
        
        $db->Query("select * from penerimaan where id_penerimaan = {$id_penerimaan}");
        $row = $db->FetchAssoc();
        $smarty->assign('penerimaan', $row);
        
        // nama program studi
        $db->Query("select nm_program_studi from program_studi where id_program_studi = {$id_program_studi}");
        $row = $db->FetchAssoc();
        $smarty->assign('program_studi', $row);
        
        // detail mahasiswa
        $calon_mahasiswa_baru_set =  $db->QueryToArray("
            select no_ujian, nm_c_mhs, n.*
            from calon_mahasiswa_baru cmb
            join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
            left join nilai_cmhs_pasca n on n.id_c_mhs = cmb.id_c_mhs
            where id_pilihan_1 = {$id_program_studi} and p.id_penerimaan = {$id_penerimaan} and cmb.tgl_verifikasi_ppmb is not null
            order by n.total_nilai desc");
            
        for ($i = 0; $i < count($calon_mahasiswa_baru_set); $i++)
        {
            $calon_mahasiswa_baru_set[$i]['NILAI_TPA'] = number_format($calon_mahasiswa_baru_set[$i]['NILAI_TPA'], 2);
            $calon_mahasiswa_baru_set[$i]['NILAI_INGGRIS'] = number_format($calon_mahasiswa_baru_set[$i]['NILAI_INGGRIS'], 2);
            $calon_mahasiswa_baru_set[$i]['NILAI_ILMU'] = number_format($calon_mahasiswa_baru_set[$i]['NILAI_ILMU'], 2);
            $calon_mahasiswa_baru_set[$i]['NILAI_WAWANCARA'] = number_format($calon_mahasiswa_baru_set[$i]['NILAI_WAWANCARA'], 2);
            $calon_mahasiswa_baru_set[$i]['NILAI_IPK'] = number_format($calon_mahasiswa_baru_set[$i]['NILAI_IPK'], 2);
            $calon_mahasiswa_baru_set[$i]['NILAI_KARYA_ILMIAH'] = number_format($calon_mahasiswa_baru_set[$i]['NILAI_KARYA_ILMIAH'], 2);
            $calon_mahasiswa_baru_set[$i]['NILAI_REKOMENDASI'] = number_format($calon_mahasiswa_baru_set[$i]['NILAI_REKOMENDASI'], 2);
            $calon_mahasiswa_baru_set[$i]['NILAI_MATRIKULASI'] = number_format($calon_mahasiswa_baru_set[$i]['NILAI_MATRIKULASI'], 2);
            $calon_mahasiswa_baru_set[$i]['TOTAL_NILAI'] = number_format($calon_mahasiswa_baru_set[$i]['TOTAL_NILAI'], 2);
        }
        
        $smarty->assign('calon_mahasiswa_baru_set', $calon_mahasiswa_baru_set);
        
        // data statistik
        $db->Query("select s.* from statistik_cmhs s where id_program_studi = {$id_program_studi} and id_penerimaan = {$id_penerimaan}");
        $statistik = $db->FetchAssoc();
        
        // menentukan passing grade
        $statistik = array_merge($statistik, array('PEMINAT' => count($calon_mahasiswa_baru_set)));
        if ($statistik['KUOTA'] < $statistik['PEMINAT'])
            $statistik['PASSING_GRADE'] = $statistik['NILAI_RATA'];
        else
            $statistik['PASSING_GRADE'] = $statistik['NILAI_RATA'] - $statistik['STANDAR_DEVIASI'];
        
        $statistik['PASSING_GRADE'] = number_format($statistik['PASSING_GRADE'], 2);
        
        $smarty->assign('statistik', $statistik);
    }
}

$smarty->display("penetapan/cetak/{$mode}.tpl");
?>
