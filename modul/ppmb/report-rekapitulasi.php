<?php
include 'config.php';

if (isset($_POST['refresh-peminat']))
{
    $db->BeginTransaction();
    
    // kosongi tabel
    $db->Query("delete from ppmb_r_peminatan");
    
    // inisialisasi peminatan
    $db->Query("
        insert into ppmb_r_peminatan (id_penerimaan, id_program_studi)
        select p.id_penerimaan, ps.id_program_studi
        from penerimaan p 
        join program_studi ps on ps.id_jenjang = p.id_jenjang
        where p.tahun >= 2011 and ps.status_aktif_prodi = 1");
    
    // update data peminatan
    $db->Query("
        update ppmb_r_peminatan r set
            peminat_1 = (select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb where cmb.no_ujian is not null and cmb.id_penerimaan = r.id_penerimaan and cmb.id_pilihan_1 = r.id_program_studi),
            peminat_2 = (select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb where cmb.no_ujian is not null and cmb.id_penerimaan = r.id_penerimaan and cmb.id_pilihan_2 = r.id_program_studi),
            peminat_3 = (select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb where cmb.no_ujian is not null and cmb.id_penerimaan = r.id_penerimaan and cmb.id_pilihan_3 = r.id_program_studi),
            peminat_4 = (select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb where cmb.no_ujian is not null and cmb.id_penerimaan = r.id_penerimaan and cmb.id_pilihan_4 = r.id_program_studi),
            diterima = (select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb where cmb.no_ujian is not null and cmb.id_penerimaan = r.id_penerimaan and cmb.id_program_studi = r.id_program_studi),
            nilai_rata = nvl((select nilai_rata from statistik_cmhs s where s.id_penerimaan = r.id_penerimaan and s.id_program_studi = r.id_program_studi),0)");
    
    // update rata-rata nilai diterima D3, S1
    $db->Query("
        update ppmb_r_peminatan r set
          nilai_rata_diterima = nvl((
            select round(avg(n.nilai_tpa + n.nilai_prestasi), 2) as nilai_total from calon_mahasiswa_baru cmb
            join program_studi ps on ps.id_program_studi = cmb.id_program_studi
            join nilai_cmhs n on n.id_c_mhs = cmb.id_c_mhs and n.jurusan_sekolah = ps.jurusan_sekolah
            where cmb.id_penerimaan = r.id_penerimaan and cmb.id_program_studi = r.id_program_studi),0)
        where r.id_program_studi in (select id_program_studi from program_studi where id_jenjang in (1, 5))");
    
    // update rata-rata nilai diterima S2, S3, Pr, Sp
    $db->Query("
        update ppmb_r_peminatan r set
          nilai_rata_diterima = nvl((
          select round(avg(n.total_nilai),2) from calon_mahasiswa_baru cmb
          join nilai_cmhs_pasca n on n.id_c_mhs = cmb.id_c_mhs
          where cmb.id_penerimaan = r.id_penerimaan and cmb.id_program_studi = r.id_program_studi),0)
        where r.id_program_studi in (select id_program_studi from program_studi where id_jenjang in (2, 3, 9, 10))");
    
    // update jumlah peminat
    $db->Query("update ppmb_r_peminatan set jumlah = peminat_1 + peminat_2 + peminat_3 + peminat_4");
    
    // delete data tidak relevan
    $db->Query("delete from ppmb_r_peminatan where peminat_1 = 0 and peminat_2 = 0 and peminat_3 = 0 and peminat_4 = 0 and diterima = 0");
    
    $result = $db->Commit();
    
    $smarty->assign('result', $result ? 'Rekapitulasi peminat berhasil direfresh' : 'Rekapitulasi peminat gagal direfresh');
}

if (isset($_POST['refresh-provinsi']))
{
    // Mulai transaksi
    $db->BeginTransaction();
    
    // clear-kan tabel
    $db->Query("delete from ppmb_r_provinsi");
    
    // Tambahkan data ke tabel
    $db->Query("
        insert into ppmb_r_provinsi (id_penerimaan, id_program_studi, id_provinsi)
        select pp.id_penerimaan, id_program_studi, id_provinsi from penerimaan_prodi pp
        cross join provinsi
        where pp.id_penerimaan in (select id_penerimaan from penerimaan where tahun >= 2011) and id_negara = 114
        union
        select pp.id_penerimaan, id_program_studi, 5430 from penerimaan_prodi pp
        where pp.id_penerimaan in (select id_penerimaan from penerimaan where tahun >= 2011)");
    
    // update data provinsi Indonesia (D3, S1)
    $db->Query("
        update ppmb_r_provinsi r set
            peminat = (
                select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb
                join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
                left join sekolah s on s.id_sekolah = cms.id_sekolah_asal
                where cmb.no_ujian is not null and cmb.id_penerimaan = r.id_penerimaan and s.id_provinsi = r.id_provinsi and
                    (cmb.id_pilihan_1 = r.id_program_studi or cmb.id_pilihan_2 = r.id_program_studi or cmb.id_pilihan_3 = r.id_program_studi or cmb.id_pilihan_4 = r.id_program_studi)),
            diterima = (
                select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb
                join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
                left join sekolah s on s.id_sekolah = cms.id_sekolah_asal
                where cmb.no_ujian is not null and cmb.id_penerimaan = r.id_penerimaan and cmb.id_program_studi = r.id_program_studi and s.id_provinsi = r.id_provinsi)
        where id_provinsi <> 5430 and id_program_studi in (select id_program_studi from program_studi where id_jenjang in (1, 5))");
    
    // update data provinsi Luar Negeri (D3, S1)
    $db->Query("
        update ppmb_r_provinsi r set
            peminat = (
                select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb
                join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
                left join sekolah s on s.id_sekolah = cms.id_sekolah_asal
                where cmb.no_ujian is not null and cmb.id_penerimaan = r.id_penerimaan and s.id_negara <> 114 and
                    (cmb.id_pilihan_1 = r.id_program_studi or cmb.id_pilihan_2 = r.id_program_studi or cmb.id_pilihan_3 = r.id_program_studi or cmb.id_pilihan_4 = r.id_program_studi)),
            diterima = (
                select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb
                join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
                left join sekolah s on s.id_sekolah = cms.id_sekolah_asal
                where cmb.no_ujian is not null and cmb.id_penerimaan = r.id_penerimaan and cmb.id_program_studi = r.id_program_studi and s.id_negara <> 114)
        where id_provinsi = 5430 and id_program_studi in (select id_program_studi from program_studi where id_jenjang in (1, 5))");
    
    // update data provinsi Indonesia (S2, S3, Pr, Sp)
    $db->Query("
        update ppmb_r_provinsi r set
            peminat = (
                select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb
                join kota k on k.id_kota = cmb.id_kota
                where cmb.id_penerimaan = r.id_penerimaan and k.id_provinsi = r.id_provinsi and
                (cmb.id_pilihan_1 = r.id_program_studi or cmb.id_pilihan_2 = r.id_program_studi or cmb.id_pilihan_3 = r.id_program_studi or cmb.id_pilihan_4 = r.id_program_studi)),
            diterima = (
                select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb
                join kota k on k.id_kota = cmb.id_kota
                where cmb.id_penerimaan = r.id_penerimaan and cmb.id_program_studi = r.id_program_studi and k.id_provinsi = r.id_provinsi)
        where id_provinsi <> 5430 and id_program_studi in (select id_program_studi from program_studi where id_jenjang in (2, 3, 9, 10))");

    // update data provinsi Luar Negeri (S2, S3, Pr, Sp)
    $db->Query("
        update ppmb_r_provinsi r set
            peminat = (
                select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb
                join kota k on k.id_kota = cmb.id_kota
                join provinsi p on p.id_provinsi = k.id_provinsi
                where cmb.id_penerimaan = r.id_penerimaan and p.id_negara <> 114 and
                (cmb.id_pilihan_1 = r.id_program_studi or cmb.id_pilihan_2 = r.id_program_studi or cmb.id_pilihan_3 = r.id_program_studi or cmb.id_pilihan_4 = r.id_program_studi)),
            diterima = (
                select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb
                join kota k on k.id_kota = cmb.id_kota
                join provinsi p on p.id_provinsi = k.id_provinsi
                where cmb.id_penerimaan = r.id_penerimaan and cmb.id_program_studi = r.id_program_studi and p.id_negara <> 114)
        where id_provinsi = 5430 and id_program_studi in (select id_program_studi from program_studi where id_jenjang in (2, 3, 9, 10))");
    
    // commit transaksi
    $result = $db->Commit();
    
    $smarty->assign('result', $result ? 'Rekapitulasi provinsi berhasil direfresh' : 'Rekapitulasi provinsi gagal direfresh');
}

if (isset($_POST['refresh-gender']))
{
    
    $db->BeginTransaction();
    
    // bersihkan tabel
    $db->Query("delete from ppmb_r_gender");
    
    // inisialisasi data penerimaan dan prodi
    $db->Query("
        insert into ppmb_r_gender (id_penerimaan, id_program_studi)
            select id_penerimaan, id_program_studi from penerimaan_prodi
            where id_penerimaan in (select id_penerimaan from penerimaan where tahun >= 2011)");
    
    // update data
    $db->Query("
        update ppmb_r_gender r set
        peminat_laki = (
            select count(id_c_mhs) from calon_mahasiswa_baru cmb where cmb.id_penerimaan = r.id_penerimaan and cmb.jenis_kelamin = 1 and
            (id_pilihan_1 = r.id_program_studi or id_pilihan_2 = r.id_program_studi or id_pilihan_3 = r.id_program_studi or id_pilihan_4 = r.id_program_studi)),
        peminat_perempuan = (
            select count(id_c_mhs) from calon_mahasiswa_baru cmb where cmb.id_penerimaan = r.id_penerimaan and cmb.jenis_kelamin = 2 and
            (id_pilihan_1 = r.id_program_studi or id_pilihan_2 = r.id_program_studi or id_pilihan_3 = r.id_program_studi or id_pilihan_4 = r.id_program_studi)),
        diterima_laki = (
            select count(id_c_mhs) from calon_mahasiswa_baru cmb where cmb.id_penerimaan = r.id_penerimaan and cmb.jenis_kelamin = 1 and cmb.id_program_studi = r.id_program_studi),
        diterima_perempuan = (
            select count(id_c_mhs) from calon_mahasiswa_baru cmb where cmb.id_penerimaan = r.id_penerimaan and cmb.jenis_kelamin = 2 and cmb.id_program_studi = r.id_program_studi)");
    
    $result = $db->Commit();
    
    $smarty->assign('result', $result ? 'Rekapitulasi jenis kelamin berhasil direfresh' : 'Rekapitulasi jenis kelamin gagal direfresh');
}

if (isset($_POST['refresh-penghasilan']))
{
    $db->BeginTransaction();
    
    $db->Query("delete from ppmb_r_penghasilan_ortu");
    
    $db->Query("
        insert into ppmb_r_penghasilan_ortu (id_penerimaan, id_program_studi, id_penghasilan_ortu)
        select pp.id_penerimaan, id_program_studi, id_penghasilan_ortu from penerimaan_prodi pp
        cross join penghasilan_ortu 
        where pp.id_penerimaan in (select id_penerimaan from penerimaan where tahun >= 2011 and id_jenjang in (1, 5))
        union
        select pp.id_penerimaan, id_program_studi, null from penerimaan_prodi pp
        where pp.id_penerimaan in (select id_penerimaan from penerimaan where tahun >= 2011 and id_jenjang in (1, 5))");
        
    // Update penghasilan not null snmptn
    $db->Query("
        update ppmb_r_penghasilan_ortu r set
            peminat = (
                select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
                join calon_mahasiswa_ortu cmo on cmo.id_c_mhs = cmb.id_c_mhs
                where cmb.id_penerimaan = r.id_penerimaan and cmo.penghasilan_snmptn_ayah = r.id_penghasilan_ortu and
                (cmb.id_pilihan_1 = r.id_program_studi or cmb.id_pilihan_2 = r.id_program_studi or cmb.id_pilihan_3 = r.id_program_studi or cmb.id_pilihan_4 = r.id_program_studi)),
            diterima = (
                select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
                join calon_mahasiswa_ortu cmo on cmo.id_c_mhs = cmb.id_c_mhs
                where cmb.id_penerimaan = r.id_penerimaan and cmb.id_program_studi = r.id_program_studi and cmo.penghasilan_snmptn_ayah = r.id_penghasilan_ortu)
        where id_penghasilan_ortu is not null and r.id_penerimaan in (select id_penerimaan from penerimaan where is_snmptn <> 0 and id_jenjang in (1, 5))");
        
    // Update penghasilan not null mandiri
    $db->Query("
        update ppmb_r_penghasilan_ortu r set
            peminat = (
                select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
                join calon_mahasiswa_ortu cmo on cmo.id_c_mhs = cmb.id_c_mhs
                where cmb.id_penerimaan = r.id_penerimaan and cmo.penghasilan_ortu = r.id_penghasilan_ortu and
                (cmb.id_pilihan_1 = r.id_program_studi or cmb.id_pilihan_2 = r.id_program_studi or cmb.id_pilihan_3 = r.id_program_studi or cmb.id_pilihan_4 = r.id_program_studi)),
            diterima = (
                select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
                join calon_mahasiswa_ortu cmo on cmo.id_c_mhs = cmb.id_c_mhs
                where cmb.id_penerimaan = r.id_penerimaan and cmb.id_program_studi = r.id_program_studi and cmo.penghasilan_ortu = r.id_penghasilan_ortu)
        where id_penghasilan_ortu is not null and r.id_penerimaan in (select id_penerimaan from penerimaan where is_snmptn = 0 and id_jenjang in (1, 5))");
        
    
    
    // Update penghasilan non null
    // $db->Query("
        // update ppmb_r_penghasilan_ortu r set
            // peminat = (
                // select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
                // join calon_mahasiswa_ortu cmo on cmo.id_c_mhs = cmb.id_c_mhs
                // where cmb.id_penerimaan = r.id_penerimaan and cmo.penghasilan_ortu = r.id_penghasilan_ortu and
                // (cmb.id_pilihan_1 = r.id_program_studi or cmb.id_pilihan_2 = r.id_program_studi or cmb.id_pilihan_3 = r.id_program_studi or cmb.id_pilihan_4 = r.id_program_studi)),
            // diterima = (
                // select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
                // join calon_mahasiswa_ortu cmo on cmo.id_c_mhs = cmb.id_c_mhs
                // where cmb.id_penerimaan = r.id_penerimaan and cmb.id_program_studi = r.id_program_studi and cmo.penghasilan_ortu = r.id_penghasilan_ortu)
        // where id_penghasilan_ortu is not null");
    
    // Update penghasilan null snmptn
    $db->Query("
        update ppmb_r_penghasilan_ortu r set
            peminat = (
                select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
                join calon_mahasiswa_ortu cmo on cmo.id_c_mhs = cmb.id_c_mhs
                where cmb.id_penerimaan = r.id_penerimaan and cmo.penghasilan_snmptn_ayah is null and
                (cmb.id_pilihan_1 = r.id_program_studi or cmb.id_pilihan_2 = r.id_program_studi or cmb.id_pilihan_3 = r.id_program_studi or cmb.id_pilihan_4 = r.id_program_studi)),
            diterima = (
                select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
                join calon_mahasiswa_ortu cmo on cmo.id_c_mhs = cmb.id_c_mhs
                where cmb.id_penerimaan = r.id_penerimaan and cmb.id_program_studi = r.id_program_studi and cmo.penghasilan_snmptn_ayah is null)
        where id_penghasilan_ortu is null and r.id_penerimaan in (select id_penerimaan from penerimaan where is_snmptn <> 0 and id_jenjang in (1, 5))");
        
    // Update penghasilan null mandiri
    $db->Query("
        update ppmb_r_penghasilan_ortu r set
            peminat = (
                select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
                join calon_mahasiswa_ortu cmo on cmo.id_c_mhs = cmb.id_c_mhs
                where cmb.id_penerimaan = r.id_penerimaan and cmo.penghasilan_ortu is null and
                (cmb.id_pilihan_1 = r.id_program_studi or cmb.id_pilihan_2 = r.id_program_studi or cmb.id_pilihan_3 = r.id_program_studi or cmb.id_pilihan_4 = r.id_program_studi)),
            diterima = (
                select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
                join calon_mahasiswa_ortu cmo on cmo.id_c_mhs = cmb.id_c_mhs
                where cmb.id_penerimaan = r.id_penerimaan and cmb.id_program_studi = r.id_program_studi and cmo.penghasilan_ortu is null)
        where id_penghasilan_ortu is null and r.id_penerimaan in (select id_penerimaan from penerimaan where is_snmptn = 0 and id_jenjang in (1, 5))");
    
    $result = $db->Commit();
    
    $smarty->assign('result', $result ? 'Rekapitulasi penghasilan ortu berhasil direfresh' : 'Rekapitulasi penghasilan ortu gagal direfresh');
}

if (isset($_POST['refresh-pendidikan']))
{
    $db->BeginTransaction();
    
    $db->Query("delete from ppmb_r_pendidikan_ortu");
    
    $db->Query("
        insert into ppmb_r_pendidikan_ortu (id_penerimaan, id_program_studi, id_pendidikan_ortu)
        select pp.id_penerimaan, id_program_studi, id_pendidikan_ortu from penerimaan_prodi pp
        cross join pendidikan_ortu
        where pp.id_penerimaan in (select id_penerimaan from penerimaan where tahun >= 2011 and id_jenjang in (1, 5))
        union
        select pp.id_penerimaan, id_program_studi, null from penerimaan_prodi pp
        where pp.id_penerimaan in (select id_penerimaan from penerimaan where tahun >= 2011 and id_jenjang in (1, 5))");
    
    $db->Query("
        update ppmb_r_pendidikan_ortu r set
            peminat = (
                select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb
                join calon_mahasiswa_ortu cmo on cmo.id_c_mhs = cmb.id_c_mhs
                where cmb.id_penerimaan = r.id_penerimaan and cmo.pendidikan_ayah = r.id_pendidikan_ortu and
                (cmb.id_pilihan_1 = r.id_program_studi or cmb.id_pilihan_2 = r.id_program_studi or cmb.id_pilihan_3 = r.id_program_studi or cmb.id_pilihan_4 = r.id_program_studi)),
            diterima = (
                select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb
                join calon_mahasiswa_ortu cmo on cmo.id_c_mhs = cmb.id_c_mhs
                where cmb.id_penerimaan = r.id_penerimaan and cmb.id_program_studi = r.id_program_studi and cmo.pendidikan_ayah = r.id_pendidikan_ortu)
        where id_pendidikan_ortu is not null");
    
    $db->Query("
        update ppmb_r_pendidikan_ortu r set
            peminat = (
                select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb
                join calon_mahasiswa_ortu cmo on cmo.id_c_mhs = cmb.id_c_mhs
                where cmb.id_penerimaan = r.id_penerimaan and cmo.pendidikan_ayah is null and
                (cmb.id_pilihan_1 = r.id_program_studi or cmb.id_pilihan_2 = r.id_program_studi or cmb.id_pilihan_3 = r.id_program_studi or cmb.id_pilihan_4 = r.id_program_studi)),
            diterima = (
                select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb
                join calon_mahasiswa_ortu cmo on cmo.id_c_mhs = cmb.id_c_mhs
                where cmb.id_penerimaan = r.id_penerimaan and cmb.id_program_studi = r.id_program_studi and cmo.pendidikan_ayah is null)
        where id_pendidikan_ortu is null");
    
    $result = $db->Commit();
    
    $smarty->assign('result', $result ? 'Rekapitulasi pendidikan ortu berhasil direfresh' : 'Rekapitulasi pendidikan ortu gagal direfresh');
}

if (isset($_POST['refresh-pekerjaan']))
{
    $db->BeginTransaction();
    
    $db->Query("delete from ppmb_r_pekerjaan");
    
    $db->Query("
        insert into ppmb_r_pekerjaan (id_penerimaan, id_program_studi, id_pekerjaan)
        select pp.id_penerimaan, id_program_studi, id_pekerjaan from penerimaan_prodi pp
        cross join pekerjaan
        where pp.id_penerimaan in (select id_penerimaan from penerimaan where tahun >= 2011 and id_jenjang in (1, 5))");
    
    $db->Query("
        update ppmb_r_pekerjaan r set
            peminat = (
              select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb
              join calon_mahasiswa_ortu cmo on cmo.id_c_mhs = cmb.id_c_mhs
              where cmb.id_penerimaan = r.id_penerimaan and pekerjaan_ayah = r.id_pekerjaan and 
              (cmb.id_pilihan_1 = r.id_program_studi or cmb.id_pilihan_2 = r.id_program_studi or cmb.id_pilihan_3 = r.id_program_studi or cmb.id_pilihan_4 = r.id_program_studi)),
            diterima = (
              select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb
              join calon_mahasiswa_ortu cmo on cmo.id_c_mhs = cmb.id_c_mhs
              where cmb.id_penerimaan = r.id_penerimaan and cmb.id_program_studi = r.id_program_studi and pekerjaan_ayah = r.id_pekerjaan)
        where id_pekerjaan <= 10");
    
    $db->Query("
        update ppmb_r_pekerjaan r set
            peminat = (
              select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb
              join calon_mahasiswa_ortu cmo on cmo.id_c_mhs = cmb.id_c_mhs
              where cmb.id_penerimaan = r.id_penerimaan and (pekerjaan_ayah = 11 or pekerjaan_ayah is null) and 
              (cmb.id_pilihan_1 = r.id_program_studi or cmb.id_pilihan_2 = r.id_program_studi or cmb.id_pilihan_3 = r.id_program_studi or cmb.id_pilihan_4 = r.id_program_studi)),
            diterima = (
              select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb
              join calon_mahasiswa_ortu cmo on cmo.id_c_mhs = cmb.id_c_mhs
              where cmb.id_penerimaan = r.id_penerimaan and cmb.id_program_studi = r.id_program_studi and (pekerjaan_ayah = 11 or pekerjaan_ayah is null))
        where id_pekerjaan = 11");
    
    $result = $db->Commit();
    
    $smarty->assign('result', $result ? 'Rekapitulasi pekerjaan ortu berhasil direfresh' : 'Rekapitulasi pekerjaan ortu gagal direfresh');
}

/** FUNGSI INI SUDAH TIDAK DIGUNAKAN sudah digantikan di halaman report utility **/
/**
if (isset($_POST['refresh-pilihan']))
{
	$id_penerimaan = post('id_penerimaan');

	$cmb_set = $db->QueryToArray("
		select
			cmb.id_c_mhs, v.kode_jurusan,
			cmb.id_pilihan_1, ps1.jurusan_sekolah as jurusan_sekolah_1,
			cmb.id_pilihan_2, ps2.jurusan_sekolah as jurusan_sekolah_2,
			cmb.id_pilihan_3, ps3.jurusan_sekolah as jurusan_sekolah_3,
			cmb.id_pilihan_4, ps4.jurusan_sekolah as jurusan_sekolah_4
		from calon_mahasiswa_baru cmb
		JOIN voucher v ON v.kode_voucher = cmb.kode_voucher
		left join program_studi ps1 on ps1.id_program_studi = cmb.id_pilihan_1
		left join program_studi ps2 on ps2.id_program_studi = cmb.id_pilihan_2
		left join program_studi ps3 on ps3.id_program_studi = cmb.id_pilihan_3
		left join program_studi ps4 on ps4.id_program_studi = cmb.id_pilihan_4
		where cmb.id_penerimaan = {$id_penerimaan}");

	// Model pilihan silang untuk IPC
	// Hanya terjadi di D3+AJ Gel 1 2013, MANDIRI 1 2013, D3+AJ Gel 2 2013,
	if (in_array($id_penerimaan, array(113, 112, 111, 131, 130)))
	{
		foreach ($cmb_set as $cmb)
		{
			// delete record first
			$db->Query("delete from calon_mahasiswa_pilihan where id_c_mhs = {$cmb['ID_C_MHS']}");
			
			$pilihan_ke = 1;

			// Voucher IPA / IPS
			if ($cmb['KODE_JURUSAN'] == '01' || $cmb['KODE_JURUSAN'] == '02')
			{
				if ( ! empty($cmb['ID_PILIHAN_1']))
				{
					$db->Query("
						insert into calon_mahasiswa_pilihan
						(id_c_mhs, jurusan_sekolah, pilihan_ke, id_pilihan) values
						({$cmb['ID_C_MHS']}, {$cmb['JURUSAN_SEKOLAH_1']}, {$pilihan_ke}, {$cmb['ID_PILIHAN_1']})");
					
					$pilihan_ke++;
				}
				
				if ( ! empty($cmb['ID_PILIHAN_2']))
				{
					$db->Query("
						insert into calon_mahasiswa_pilihan
						(id_c_mhs, jurusan_sekolah, pilihan_ke, id_pilihan) values
						({$cmb['ID_C_MHS']}, {$cmb['JURUSAN_SEKOLAH_2']}, {$pilihan_ke}, {$cmb['ID_PILIHAN_2']})");
				}
			}
			
			$pilihan_ke_ipa = 1;
			$pilihan_ke_ips = 1;
			
			// IPC
			if ($cmb['KODE_JURUSAN'] == '03')
			{
				if ( ! empty($cmb['ID_PILIHAN_1']))
				{
					if ($cmb['JURUSAN_SEKOLAH_1'] == 1)  // prodi ipa
					{
						$db->Query("
							insert into calon_mahasiswa_pilihan
							(id_c_mhs, jurusan_sekolah, pilihan_ke, id_pilihan) values
							({$cmb['ID_C_MHS']}, {$cmb['JURUSAN_SEKOLAH_1']}, {$pilihan_ke_ipa}, {$cmb['ID_PILIHAN_1']})");
						
						$pilihan_ke_ipa++;
					}
					
					if ($cmb['JURUSAN_SEKOLAH_1'] == 2)  // prodi ips
					{
						$db->Query("
							insert into calon_mahasiswa_pilihan
							(id_c_mhs, jurusan_sekolah, pilihan_ke, id_pilihan) values
							({$cmb['ID_C_MHS']}, {$cmb['JURUSAN_SEKOLAH_1']}, {$pilihan_ke_ips}, {$cmb['ID_PILIHAN_1']})");
						
						$pilihan_ke_ips++;
					}
				}
				
				if ( ! empty($cmb['ID_PILIHAN_2']))
				{
					if ($cmb['JURUSAN_SEKOLAH_2'] == 1)  // prodi ipa
					{
						$db->Query("
							insert into calon_mahasiswa_pilihan
							(id_c_mhs, jurusan_sekolah, pilihan_ke, id_pilihan) values
							({$cmb['ID_C_MHS']}, {$cmb['JURUSAN_SEKOLAH_2']}, {$pilihan_ke_ipa}, {$cmb['ID_PILIHAN_2']})");
							
						$pilihan_ke_ipa++;
					}
					
					if ($cmb['JURUSAN_SEKOLAH_2'] == 2)  // prodi ips
					{
						$db->Query("
							insert into calon_mahasiswa_pilihan
							(id_c_mhs, jurusan_sekolah, pilihan_ke, id_pilihan) values
							({$cmb['ID_C_MHS']}, {$cmb['JURUSAN_SEKOLAH_2']}, {$pilihan_ke_ips}, {$cmb['ID_PILIHAN_2']})");
						
						$pilihan_ke_ips++;
					}
				}
				
				if ( ! empty($cmb['ID_PILIHAN_3']))
				{
					if ($cmb['JURUSAN_SEKOLAH_3'] == 1)  // prodi ipa
					{
						$db->Query("
							insert into calon_mahasiswa_pilihan
							(id_c_mhs, jurusan_sekolah, pilihan_ke, id_pilihan) values
							({$cmb['ID_C_MHS']}, {$cmb['JURUSAN_SEKOLAH_3']}, {$pilihan_ke_ipa}, {$cmb['ID_PILIHAN_3']})");
							
						$pilihan_ke_ipa++;
					}
					
					if ($cmb['JURUSAN_SEKOLAH_3'] == 2)  // prodi ips
					{
						$db->Query("
							insert into calon_mahasiswa_pilihan
							(id_c_mhs, jurusan_sekolah, pilihan_ke, id_pilihan) values
							({$cmb['ID_C_MHS']}, {$cmb['JURUSAN_SEKOLAH_3']}, {$pilihan_ke_ips}, {$cmb['ID_PILIHAN_3']})");
						
						$pilihan_ke_ips++;
					}
				}
				
				if ( ! empty($cmb['ID_PILIHAN_4']))
				{
					if ($cmb['JURUSAN_SEKOLAH_4'] == 1)  // prodi ipa
					{
						$db->Query("
							insert into calon_mahasiswa_pilihan
							(id_c_mhs, jurusan_sekolah, pilihan_ke, id_pilihan) values
							({$cmb['ID_C_MHS']}, {$cmb['JURUSAN_SEKOLAH_4']}, {$pilihan_ke_ipa}, {$cmb['ID_PILIHAN_4']})");
							
						$pilihan_ke_ipa++;
					}
					
					if ($cmb['JURUSAN_SEKOLAH_4'] == 2)  // prodi ips
					{
						$db->Query("
							insert into calon_mahasiswa_pilihan
							(id_c_mhs, jurusan_sekolah, pilihan_ke, id_pilihan) values
							({$cmb['ID_C_MHS']}, {$cmb['JURUSAN_SEKOLAH_4']}, {$pilihan_ke_ips}, {$cmb['ID_PILIHAN_4']})");
							
						$pilihan_ke_ips++;
					}
				}
			}
		}
	}
	else  // Model pilihan normal
	{
		foreach ($cmb_set as $cmb)
		{
			// delete record first
			$db->Query("delete from calon_mahasiswa_pilihan where id_c_mhs = {$cmb['ID_C_MHS']}");
			
			$pilihan_ke = 1;

			if ( ! empty($cmb['ID_PILIHAN_1']))
			{
				$db->Query("
					insert into calon_mahasiswa_pilihan
					(id_c_mhs, jurusan_sekolah, pilihan_ke, id_pilihan) values
					({$cmb['ID_C_MHS']}, {$cmb['JURUSAN_SEKOLAH_1']}, {$pilihan_ke}, {$cmb['ID_PILIHAN_1']})");
				
				$pilihan_ke++;
			}
			
			if ( ! empty($cmb['ID_PILIHAN_2']))
			{
				$db->Query("
					insert into calon_mahasiswa_pilihan
					(id_c_mhs, jurusan_sekolah, pilihan_ke, id_pilihan) values
					({$cmb['ID_C_MHS']}, {$cmb['JURUSAN_SEKOLAH_2']}, {$pilihan_ke}, {$cmb['ID_PILIHAN_2']})");

				$pilihan_ke++;
			}

			if ( ! empty($cmb['ID_PILIHAN_3']))
			{
				$db->Query("
					insert into calon_mahasiswa_pilihan
					(id_c_mhs, jurusan_sekolah, pilihan_ke, id_pilihan) values
					({$cmb['ID_C_MHS']}, {$cmb['JURUSAN_SEKOLAH_3']}, {$pilihan_ke}, {$cmb['ID_PILIHAN_3']})");

				$pilihan_ke++;
			}

			if ( ! empty($cmb['ID_PILIHAN_4']))
			{
				$db->Query("
					insert into calon_mahasiswa_pilihan
					(id_c_mhs, jurusan_sekolah, pilihan_ke, id_pilihan) values
					({$cmb['ID_C_MHS']}, {$cmb['JURUSAN_SEKOLAH_4']}, {$pilihan_ke}, {$cmb['ID_PILIHAN_4']})");
			}
		}
	}
	
	echo "OKe";
}
**/

$smarty->display("report/rekapitulasi/view.tpl");
?>