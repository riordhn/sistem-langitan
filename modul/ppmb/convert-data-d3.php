<?php
include('../../config.php');

// Koneksi ke db mysql
$mysql_server = "210.57.208.19";
$mysql_user = "fauzi";
$mysql_pass = "kungfupanda2";
$mysql_db = "dbpmdk";

$mysql = mysql_connect($mysql_server, $mysql_user, $mysql_pass) or die("Tidak dapat tersambung dengan database.");
mysql_select_db($mysql_db, $mysql);

// Meload data seluruh d3 dan alih jenis yg telah melakukan verifikasi
$cm_set = $db->QueryToArray("
    SELECT
      2011 AS TAHUN, 8 AS GELOMBANG, TGL_AKTIF_PMDK, NO_UJIAN, KODE_VOUCHER, UPPER(NM_C_MHS) AS NM_C_MHS,
      ID_PILIHAN_1, ID_PILIHAN_2, ID_PILIHAN_3, ID_PILIHAN_4, SP3_1, SP3_2, SP3_3, SP3_4,
      SUBSTR(ALAMAT, 1, 50) AS ALAMAT, SUBSTR(TELP, 1, 25) AS TELP, TGL_LAHIR, ID_JALUR AS JALUR, SUBSTR(KODE_JURUSAN, 2, 1) AS KELOMPOK,
      ID_SEKOLAH_ASAL, TAHUN_LULUS,
      SUBSTR(NAMA_AYAH, 1, 30) AS NAMA_ORTU, JUMLAH_ADIK, JUMLAH_KAKAK, PEKERJAAN_AYAH, PEKERJAAN_IBU, SUMBER_BIAYA, PENGHASILAN_ORTU, PENDIDIKAN_AYAH, PENDIDIKAN_IBU,
      KEWARGANEGARAAN, JENIS_KELAMIN, ID_AGAMA, 0 AS ID_STATUS_SLTA, 0 AS ID_JURUSAN_SLTA, 0 AS KODE_PROPINSI, ID_KOTA_AYAH, ALAMAT_AYAH
    FROM CALON_MAHASISWA CM
    WHERE (ID_JALUR = 4 OR ID_JALUR = 5) AND STATUS_PRA_UJIAN = 1");

// Konversi data
for ($i = 0; $i < count($cm_set); $i++)
{
    $cm_set[$i]['TGL_AKTIF_PMDK'] = date('Y-m-d', strtotime($cm_set[$i]['TGL_AKTIF_PMDK']));
    $cm_set[$i]['TGL_LAHIR'] = date('Y-m-d', strtotime($cm_set[$i]['TGL_LAHIR']));
}

// menampilkan
$smarty->assign('cm_set', $cm_set);
$smarty->display('convert-data-d3.tpl');
?>
