<?php

class cetak {

    public $database;

    function __construct($database) {
        $this->database = $database;
    }

    function get_hasil_seleksi($id_fakultas=null, $id_program_studi=null) {
        $data = array();
        if (empty($id_program_studi) && empty($id_fakultas)) {
            $query = "SELECT CM.NO_UJIAN,UPPER(CM.NM_C_MHS) AS NM_C_MHS,UPPER(PR.NM_PROGRAM_STUDI) AS NM_PROGRAM_STUDI FROM CALON_MAHASISWA CM 
            JOIN PROGRAM_STUDI PR ON PR.ID_PROGRAM_STUDI = CM.ID_PROGRAM_STUDI 
            WHERE CM.GELOMBANG_PMDK='3'
            ORDER BY CM.NO_UJIAN,PR.ID_FAKULTAS,PR.ID_PROGRAM_STUDI";
        } else if (isset($id_fakultas) && empty($id_program_studi)) {
            $query = "SELECT CM.NO_UJIAN,UPPER(CM.NM_C_MHS) AS NM_C_MHS,UPPER(PR.NM_PROGRAM_STUDI) AS NM_PROGRAM_STUDI FROM CALON_MAHASISWA CM 
            JOIN PROGRAM_STUDI PR ON PR.ID_PROGRAM_STUDI = CM.ID_PROGRAM_STUDI 
            WHERE CM.GELOMBANG_PMDK='3' AND PR.ID_FAKULTAS ='$id_fakultas'
            ORDER BY CM.NO_UJIAN,PR.ID_PROGRAM_STUDI";
        } else {
            $query = "SELECT CM.NO_UJIAN,UPPER(CM.NM_C_MHS) AS NM_C_MHS,UPPER(PR.NM_PROGRAM_STUDI) AS NM_PROGRAM_STUDI FROM CALON_MAHASISWA CM 
            JOIN PROGRAM_STUDI PR ON PR.ID_PROGRAM_STUDI = CM.ID_PROGRAM_STUDI 
            WHERE CM.GELOMBANG_PMDK='3' AND PR.ID_FAKULTAS ='$id_fakultas' AND PR.ID_PROGRAM_STUDI ='$id_program_studi'
            ORDER BY CM.NO_UJIAN";
        }
        $this->database->Query($query);
        while ($temp = $this->database->FetchArray()) {
            array_push($data, $temp);
        }
        return $data;
    }
	
	function get_penetapan($id_penetapan){
		$this->database->Query("SELECT * FROM PENETAPAN WHERE ID_PENETAPAN='{$id_penetapan}'");
		return $this->database->FetchAssoc();
	}
	
	function get_hasil_penetapan($id_penetapan_penerimaan=null,$id_program_studi=null,$id_penetapan){
		if(empty($id_penetapan_penerimaan)&&empty($id_program_studi)){
			$query="SELECT CMB.NO_UJIAN,CMB.GELAR,UPPER(CMB.NM_C_MHS) AS NM_C_MHS,UPPER(J.NM_JENJANG) AS NM_JENJANG,UPPER(PS.NM_PROGRAM_STUDI) AS NM_PROGRAM_STUDI,UPPER(F.NM_FAKULTAS) AS NM_FAKULTAS FROM PENETAPAN PNT
			  JOIN PENETAPAN_PENERIMAAN PNTP ON PNT.ID_PENETAPAN = PNTP.ID_PENETAPAN
			  JOIN PENERIMAAN PEN ON PEN.ID_PENERIMAAN = PNTP.ID_PENERIMAAN
			  JOIN CALON_MAHASISWA_BARU CMB ON CMB.ID_PENERIMAAN = PEN.ID_PENERIMAAN
			  JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CMB.ID_PROGRAM_STUDI
			  JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
			  JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
			  WHERE CMB.TGL_DITERIMA IS NOT NULL AND PNT.ID_PENETAPAN='{$id_penetapan}'
			  ORDER BY CMB.NO_UJIAN, PS.ID_PROGRAM_STUDI";
		}
		else{
			$query="SELECT CMB.NO_UJIAN,CMB.GELAR,UPPER(CMB.NM_C_MHS) AS NM_C_MHS,UPPER(J.NM_JENJANG) AS NM_JENJANG,UPPER(PS.NM_PROGRAM_STUDI) AS NM_PROGRAM_STUDI,UPPER(F.NM_FAKULTAS) AS NM_FAKULTAS FROM PENETAPAN PNT
				  JOIN PENETAPAN_PENERIMAAN PNTP ON PNT.ID_PENETAPAN = PNTP.ID_PENETAPAN
				  JOIN PENERIMAAN PEN ON PEN.ID_PENERIMAAN = PNTP.ID_PENERIMAAN
				  JOIN CALON_MAHASISWA_BARU CMB ON CMB.ID_PENERIMAAN = PEN.ID_PENERIMAAN
				  JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CMB.ID_PROGRAM_STUDI
				  JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
				  JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
				  WHERE PNTP.ID_PENETAPAN_PENERIMAAN='{$id_penetapan_penerimaan}' AND 
					CMB.TGL_DITERIMA IS NOT NULL AND PNT.ID_PENETAPAN='{$id_penetapan}' AND PS.ID_PROGRAM_STUDI ='{$id_program_studi}'
				  ORDER BY CMB.NO_UJIAN, PS.ID_PROGRAM_STUDI";
		}
		return $this->database->QueryToArray($query);
	}
    
    function get_hasil_seleksi_pasca($id_fakultas=null, $id_program_studi=null) {
        $data = array();
        if (empty($id_program_studi) && empty($id_fakultas)) {
            $query = "SELECT CM.NO_UJIAN,UPPER(CM.NM_C_MHS) AS NM_C_MHS, SUBSTR(NM_JENJANG, 1, 2) || ' ' || UPPER(PR.NM_PROGRAM_STUDI) AS NM_PROGRAM_STUDI FROM CALON_MAHASISWA CM 
            JOIN PROGRAM_STUDI PR ON PR.ID_PROGRAM_STUDI = CM.ID_PROGRAM_STUDI 
            JOIN JENJANG J ON J.ID_JENJANG = PR.ID_JENJANG
            WHERE CM.GELOMBANG_PMDK='3'
            ORDER BY CM.NO_UJIAN,PR.ID_FAKULTAS,PR.ID_PROGRAM_STUDI";
        } else if (isset($id_fakultas) && empty($id_program_studi)) {
            $query = "SELECT CM.NO_UJIAN,UPPER(CM.NM_C_MHS) AS NM_C_MHS, SUBSTR(NM_JENJANG, 1, 2) || ' ' || UPPER(PR.NM_PROGRAM_STUDI) AS NM_PROGRAM_STUDI FROM CALON_MAHASISWA CM 
            JOIN PROGRAM_STUDI PR ON PR.ID_PROGRAM_STUDI = CM.ID_PROGRAM_STUDI 
            JOIN JENJANG J ON J.ID_JENJANG = PR.ID_JENJANG
            WHERE CM.GELOMBANG_PMDK='3' AND PR.ID_FAKULTAS ='$id_fakultas'
            ORDER BY CM.NO_UJIAN,PR.ID_PROGRAM_STUDI";
        } else {
            $query = "SELECT CM.NO_UJIAN,UPPER(CM.NM_C_MHS) AS NM_C_MHS, SUBSTR(NM_JENJANG, 1, 2) || ' ' || UPPER(PR.NM_PROGRAM_STUDI) AS NM_PROGRAM_STUDI FROM CALON_MAHASISWA CM 
            JOIN PROGRAM_STUDI PR ON PR.ID_PROGRAM_STUDI = CM.ID_PROGRAM_STUDI 
            JOIN JENJANG J ON J.ID_JENJANG = PR.ID_JENJANG
            WHERE CM.GELOMBANG_PMDK='3' AND PR.ID_FAKULTAS ='$id_fakultas' AND PR.ID_PROGRAM_STUDI ='$id_program_studi'
            ORDER BY CM.NO_UJIAN";
        }
        $this->database->Query($query);
        while ($temp = $this->database->FetchArray()) {
            array_push($data, $temp);
        }
        return $data;
    }

    function get_hasil_seleksi_d3_alih($id_fakultas=null, $id_program_studi=null)
    {
        $data = array();
        if (empty($id_program_studi) && empty($id_fakultas)) {
            $query = "SELECT CM.NO_UJIAN,UPPER(CM.NM_C_MHS) AS NM_C_MHS,UPPER(PR.NM_PROGRAM_STUDI) || ' (' || NM_JENJANG || ')' AS NM_PROGRAM_STUDI FROM CALON_MAHASISWA CM 
            JOIN PROGRAM_STUDI PR ON PR.ID_PROGRAM_STUDI = CM.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG = PR.ID_JENJANG
            WHERE (CM.ID_JALUR = 4 OR CM.ID_JALUR = 5)
            ORDER BY CM.NO_UJIAN";
        } else if (isset($id_fakultas) && empty($id_program_studi)) {
            
        } else {
            $query = "SELECT CM.NO_UJIAN,UPPER(CM.NM_C_MHS) AS NM_C_MHS, UPPER(PR.NM_PROGRAM_STUDI) || ' (' || NM_JENJANG || ')' AS NM_PROGRAM_STUDI FROM CALON_MAHASISWA CM 
            JOIN PROGRAM_STUDI PR ON PR.ID_PROGRAM_STUDI = CM.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG = PR.ID_JENJANG
            WHERE (CM.ID_JALUR = 4 OR CM.ID_JALUR = 5) AND PR.ID_FAKULTAS ='$id_fakultas' AND PR.ID_PROGRAM_STUDI ='$id_program_studi'
            ORDER BY CM.NO_UJIAN";
        }
        $this->database->Query($query);
        while ($temp = $this->database->FetchArray()) {
            array_push($data, $temp);
        }
        return $data;
    }
    
    function compress_nama($nama)
    {
        return $nama; // by pass kompress
        
        $nama_split = explode(' ', $nama);
        for ($n = 0; $n < count($nama_split); $n++)
            if ($n >= 2)
                $nama_split[$n] = substr($nama_split[$n], 0, 1) . '.';
        return implode(' ', $nama_split);
    }
}

?>
