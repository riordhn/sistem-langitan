<?php
include('config.php');
include('class/Penerimaan.class.php');

if (!in_array($user->ID_JABATAN_PEGAWAI, array(175, 176, 203)))
{
    // 203 = Admin
    echo "Anda tidak punya hak akses halaman ini.";
    exit();
}

$mode = get('mode', 'view');

$id_pt = $id_pt_user;

if ($request_method == 'POST')
{
	if (post('mode') == 'penetapan')
	{
		$id_c_mhs_set	= post('id_c_mhs', array());

		$id_c_mhs		= implode(',', $id_c_mhs_set);

		/*$db->Parse("UPDATE CALON_MAHASISWA_BARU SET NO_UJIAN = KODE_VOUCHER WHERE ID_C_MHS IN (:id_c_mhs)");
		$db->BindByName(':id_c_mhs', $id_c_mhs);
		$result = $db->Execute();*/

		$result = $db->Query("UPDATE CALON_MAHASISWA_BARU SET NO_UJIAN = KODE_VOUCHER WHERE ID_C_MHS IN ({$id_c_mhs})");
		
		if ($result)
			$smarty->assign('added', 'Proses penetapan berhasil');
		else
			$smarty->assign('added', 'Proses penetapan gagal');
	}
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	// Menampilkan list penerimaan
    // class
    $penerimaan = new Penerimaan($db);
    $smarty->assign('penerimaan_set', $penerimaan->GetAllForView());
	
	$id_penerimaan = get('id_penerimaan', '');
	
	if ($mode == 'view' && $id_penerimaan != '')
	{
		$cmb_set = $db->QueryToArray("
			SELECT cmb.ID_C_MHS, KODE_VOUCHER, NM_C_MHS, NOMOR_HP, 
				(select nm_sekolah from sekolah s where s.id_sekolah = cms.id_sekolah_asal) as nm_sekolah_asal,
				(select nm_jenjang || ' - ' || nm_program_studi from program_studi ps join jenjang j on j.id_jenjang = ps.id_jenjang where ps.id_program_studi = cmb.id_pilihan_1) as nm_program_studi
				FROM CALON_MAHASISWA_BARU cmb
				JOIN CALON_MAHASISWA_SEKOLAH cms ON cms.ID_C_MHS = cmb.ID_C_MHS
				WHERE ID_PENERIMAAN = '{$id_penerimaan}' AND TGL_VERIFIKASI_PPMB IS NOT NULL AND NO_UJIAN IS NULL
				ORDER BY KODE_VOUCHER");
		$smarty->assign('cmb_set', $cmb_set);
	}
	
}

$smarty->display("peserta/ppenetapan/{$mode}.tpl");
?>
