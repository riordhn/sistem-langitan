<?php
include 'config.php';
include 'class/Pengawas.class.php';
include 'class/Penetapan.class.php';

$mode = get('mode', 'view');
$Pengawas = new Pengawas($db);
$Penetapan = new Penetapan($db);

if ($request_method == 'GET')
{
    if ($mode == 'view')
    {
        $smarty->assign('penetapan_set', $Penetapan->GetListPenetapan());
    }
    
    if ($mode == 'rekap')
    {
        $smarty->assign('presensi_set', $Penetapan->GetListRekapPresensi(get('id_penetapan')));
    }
    
    if ($mode == 'detail')
    {
        $smarty->assign('cmb_set', $Penetapan->GetListDetailPresensi(get('id_penetapan'), get('id_status_ujian')));
    }
}

$smarty->display("penetapan/presensi/{$mode}.tpl");
?>
