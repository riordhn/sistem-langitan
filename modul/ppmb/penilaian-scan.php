<?php
include('config.php');
include('class/Penerimaan.class.php');

$Penerimaan = new Penerimaan($db);

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    if (post('mode') == 'submit-nilai')
    {
        $keys = array_keys($_POST);
        
        $db->BeginTransaction();
        
        foreach ($keys as $key)
        {
            if (substr($key, 0, 3) == 'tpa')
            {
                $id_c_mhs = str_replace('tpa', '', $key);
                $nilai_tpa = $_POST[$key];
                $nilai_inggris = $_POST['inggris' . $id_c_mhs];
                
                /**
                $db->Query("
                    update nilai_cmhs_pasca set
                        nilai_tpa = {$nilai_tpa},
                        nilai_inggris = {$nilai_inggris},
                        total_nilai = {$nilai_tpa} + {$nilai_inggris} + nilai_ilmu + nilai_wawancara + nilai_ipk + nilai_karya_ilmiah + nilai_rekomendasi + nilai_matrikulasi + nilai_psiko
                    where id_c_mhs = {$id_c_mhs}");
                 * 
                 */
            }
        }
        
        $result = $db->Commit();
        
        $result = true;
        
        $smarty->assign('updated', $result ? "Nilai berhasil diupdate" : "Nilai gagal diupdate");
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        $smarty->assign('penerimaan_set', $Penerimaan->GetAllForView());
    }
    
    if ($mode == 'koreksi')
    {
        $id_penerimaan = post('id_penerimaan');
        
        // Split data stream
        $stream = post('stream');
        $lines = explode("\n",$stream);
        
        // penampung nomer sama
        $no_sama_set   = array();  // ada kesamaan dengan peserta lain
        $no_kosong_set = array();  // tidak ada dalam database
        
        // Pengecekan nomer dobel atau nomer tidak ada di dalam penerimaan
        for ($baris = 0; $baris < count($lines); $baris++)
        {
            // mengambil satu baris
            $line = $lines[$baris];
            
            // split data
            $nilai_line = explode(",", $line);
            
            // no ujian, nama
            $no_ujian = $nilai_line[0];
            $nama = $nilai_line[1];
            
            // mengecek ke database
            $rows = $db->QueryToArray("select no_ujian, nm_c_mhs from calon_mahasiswa_baru where no_ujian = '{$no_ujian}' and id_penerimaan = {$id_penerimaan}");

            // jika tidak ada dalam database
            if (count($rows) == 0)
            {
                array_push($no_kosong_set, array(
                    'BARIS'         => $baris,
                    'NO_UJIAN'      => $no_ujian,
                    'NM_C_MHS'      => $nama,
                ));
            }
            else
            {
                // mengecek nomer-nomer di bawahnya
                for ($i = $baris + 1; $i < count($lines); $i++)
                {
                    // mengambil baris yg akan di cek
                    $line_cek = $lines[$i];

                    // split data yg di cek
                    $nilai_line_cek = explode(",", $line_cek);

                    // mengambil nomer ujian yg di cek
                    $no_ujian_cek = $nilai_line_cek[0];
                    $nama_cek = $nilai_line_cek[1];

                    // mengecek kesamaan
                    if ($no_ujian == $no_ujian_cek)
                    {   
                        // Memasukkan 
                        array_push($no_sama_set, array(
                            'BARIS'         => $baris,
                            'NO_UJIAN'      => $no_ujian,
                            'NM_C_MHS'      => $nama,
                            'BARIS_2'       => $i,
                            'NO_UJIAN_2'    => $no_ujian_cek,
                            'NM_C_MHS_2'    => $nama_cek,
                            'NO_UJIAN_DB'   => $rows['NO_UJIAN'],
                            'NM_C_MHS_DB'   => $nama_db
                        ));
                    }
                }
            }
        }
        
        // memasukkan hasil pengecekan
        $smarty->assign('no_kosong_set', $no_kosong_set);
        $smarty->assign('no_sama_set', $no_sama_set);
        
        
        $penerimaan = $Penerimaan->GetPenerimaan($id_penerimaan);
        
        // Pengecekan jalur penerimaan
        if ($penerimaan['ID_JALUR'] == 3 or $penerimaan['ID_JALUR'] == 4 or $penerimaan['ID_JALUR'] == 5 or $penerimaan['ID_JALUR'] == 20)  // Mandiri, D3, Alih Jenis
        {
            $nilai_set = array();
            
            foreach ($lines as $line)
            {
                // split data
                $nilai_line = explode(",", $line);
                
                // no ujian
                $no_ujian = $nilai_line[0];
                
                // mendapatkan jurusan sekolah untuk menentukan penilaian
                $rows = $db->QueryToArray("
                    select v.kode_jurusan from calon_mahasiswa_baru cmb
                    join voucher v on v.kode_voucher = cmb.kode_voucher
                    where cmb.no_ujian = '{$no_ujian}'");
                $kode_jurusan = $rows[0]['KODE_JURUSAN'];
                
                $kode_jurusan = substr($no_ujian, 4, 2);
                if ($kode_jurusan == '04') { $kode_jurusan = '01'; }
                if ($kode_jurusan == '05') { $kode_jurusan = '02'; }
                
                if (count($nilai_line) > 2)
                {
                    // satuan skor
                    $skor_benar = 4;
                    $skor_salah = 0;
                    $skor_kosong = 0;

                    // inisialisasi skor
                    $jumlah_benar_tpa = 0;
                    $jumlah_salah_tpa = 0;
                    $jumlah_kosong_tpa = 0;

                    // Prestasi IPA
                    $jumlah_benar_prestasi_a = 0;
                    $jumlah_salah_prestasi_a = 0;
                    $jumlah_kosong_prestasi_a = 0;
                    
                    // Prestasi IPS
                    $jumlah_benar_prestasi_s = 0;
                    $jumlah_salah_prestasi_s = 0;
                    $jumlah_kosong_prestasi_s = 0;
                    
                    // Range nomer
                    $no_awal_tpa = 0;
                    $no_akhir_tpa = 49;
                    
                    // Range nomer IPA
                    if ($kode_jurusan == '01')
                    {
                        $no_awal_a = 50;
                        $no_akhir_a = 99;
                    }
                    
                    // Range Nomer IPS
                    if ($kode_jurusan == '02')
                    {
                        $no_awal_s = 50;
                        $no_akhir_s = 99;
                    }
                    
                    // Range Nomer IPC
                    if ($kode_jurusan == '03')
                    {
                        $no_awal_a = 50;
                        $no_akhir_a = 99;
                        
                        $no_awal_s = 100;
                        $no_akhir_s = 139;
                    }
                    
                    // presentase
                    $presentase_tpa = 1;
                    $presentase_prestasi_a = 1;
                    $presentase_prestasi_s = 1;

                    // Mendapatkan kunci jawaban
                    $kunci = $db->QuerySingle("
                        select data_kunci from penerimaan_kunci_soal k
                        where
                            k.kode_soal = '{$nilai_line[3]}' and
                            k.id_penerimaan in (select id_penerimaan from calon_mahasiswa_baru where no_ujian = '{$nilai_line[0]}')");

                    $jawaban = $nilai_line[4];
                    
                    // Iterasi penghitungan soal
                    for ($i = 0; $i < strlen($kunci); $i++)
                    {
                        if (substr($jawaban, $i, 1) == 'X')
                        {
                            if ($no_awal_tpa <= $i && $i <= $no_akhir_tpa) { $jumlah_kosong_tpa += 1; }
                            
                            if ($no_awal_a <= $i && $i <= $no_akhir_a) { $jumlah_kosong_prestasi_a += 1; }
                            if ($no_awal_s <= $i && $i <= $no_akhir_s) { $jumlah_kosong_prestasi_s += 1; }
                            
                        }
                        else if (substr($jawaban, $i, 1) == substr($kunci, $i, 1))
                        {
                            if ($no_awal_tpa <= $i && $i <= $no_akhir_tpa) { $jumlah_benar_tpa += 1; }
                            
                            if ($no_awal_a <= $i && $i <= $no_akhir_a) { $jumlah_benar_prestasi_a += 1; }
                            if ($no_awal_s <= $i && $i <= $no_akhir_s) { $jumlah_benar_prestasi_s += 1; }
                        }
                        else
                        {
                            if ($no_awal_tpa <= $i && $i <= $no_akhir_tpa) { $jumlah_salah_tpa += 1; }
                            
                            if ($no_awal_a <= $i && $i <= $no_akhir_a) { $jumlah_salah_prestasi_a += 1; }
                            if ($no_awal_s <= $i && $i <= $no_akhir_s) { $jumlah_salah_prestasi_s += 1; }
                        }
                    }
                    
                    if ($kode_jurusan == '01')
                    {
                        $jumlah_benar_prestasi_s = 0;
                        $jumlah_salah_prestasi_s = 0;
                        $jumlah_kosong_prestasi_s = 0;
                    }
                    
                    if ($kode_jurusan == '02')
                    {
                        $jumlah_benar_prestasi_a = 0;
                        $jumlah_salah_prestasi_a = 0;
                        $jumlah_kosong_prestasi_a = 0;
                    }
                    
                    array_push($nilai_set, array(
                        'ID_C_MHS'      => $cmb['ID_C_MHS'],
                        'NO_UJIAN'      => $no_ujian,
                        'NM_C_MHS'      => $nilai_line[1],
                        'KODE_SOAL'     => $nilai_line[3],

                        'JUMLAH_BENAR_TPA'     => $jumlah_benar_tpa,
                        'JUMLAH_SALAH_TPA'     => $jumlah_salah_tpa,
                        'JUMLAH_KOSONG_TPA'    => $jumlah_kosong_tpa,
                        'NILAI_TPA'            => ($jumlah_benar_tpa * $skor_benar) - ($jumlah_salah_tpa * $skor_salah),
                        'SKOR_TPA'             => number_format(($jumlah_benar_tpa / ($no_akhir_tpa + 1)) * (100 * $presentase_tpa), 2),

                        'JUMLAH_BENAR_PRESTASI_A'     => $jumlah_benar_prestasi_a,
                        'JUMLAH_SALAH_PRESTASI_A'     => $jumlah_salah_prestasi_a,
                        'JUMLAH_KOSONG_PRESTASI_A'    => $jumlah_kosong_prestasi_a,
                        'NILAI_PRESTASI_A'            => ($jumlah_benar_prestasi_a * $skor_benar) - ($jumlah_salah_prestasi_a * $skor_salah),
                        'SKOR_PRESTASI_A'             => number_format(($jumlah_benar_prestasi_a / ($no_akhir_a + 1 - $no_awal_a)) * (100 * $presentase_prestasi_a), 2),
                        
                        'JUMLAH_BENAR_PRESTASI_S'     => $jumlah_benar_prestasi_s,
                        'JUMLAH_SALAH_PRESTASI_S'     => $jumlah_salah_prestasi_s,
                        'JUMLAH_KOSONG_PRESTASI_S'    => $jumlah_kosong_prestasi_s,
                        'NILAI_PRESTASI_S'            => ($jumlah_benar_prestasi_s * $skor_benar) - ($jumlah_salah_prestasi_s * $skor_salah),
                        'SKOR_PRESTASI_S'             => number_format(($jumlah_benar_prestasi_s / ($no_akhir_s + 1 - $no_awal_s)) * (100 * $presentase_prestasi_s), 2)
                    ));
                }
            }
        }
        else
        {
            // Penghitungan koreksi soal pasca sarjana, profesi, spesialis
            $nilai_set = array();
            foreach ($lines as $line)
            {
                // split data
                $nilai_line = explode(",", $line);

                // no ujian
                $no_ujian = $nilai_line[0];

                // mendapatkan jenis penilaian
                $cmb_set = $db->QueryToArray("
                    select cmb.id_c_mhs, cmb.id_pilihan_1, p.id_jenjang, cmb.id_jalur, ps.id_fakultas from calon_mahasiswa_baru cmb
                    join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
                    left join program_studi ps on ps.id_program_studi = cmb.id_pilihan_1
                    where cmb.no_ujian = '{$no_ujian}'");
                $cmb = $cmb_set[0];

                if (in_array($cmb['ID_JENJANG'], array(2, 3, 9, 10)))  // magister, doktor, profesi, spesialis
                {
                    $presentase_tpa = 1;
                    $presentase_inggris = 1;

                    if (in_array($cmb['ID_JENJANG'], array(2, 3)))  // Magister + Doktor reguler
                    {
                        $presentase_tpa = 0.4; // 40%
                        $presentase_inggris = 0.2; // 20%
                    }

                    if (in_array($cmb['ID_PILIHAN_1'], array(52, 54, 55, 63, 73, 137))) // group hukum, profesi akuntasi, magister mm, magister prof psiko
                    {
                        $presentase_tpa = 0.2;  // 20%
                        $presentase_inggris = 0.1; // 10%
                    }

                    if (in_array($cmb['ID_PILIHAN_1'], array(83)))  // PPDH
                    {
                        $presentase_tpa = 0.4;  // 40%
                        $presentase_inggris = 0.3; // 30%
                    }

                    if (in_array($cmb['ID_PILIHAN_1'], array(74)))  // PPApt
                    {
                        $presentase_tpa = 0.1;  // 10%
                        $presentase_inggris = 0.1; // 10%
                    }

                    if ($cmb['ID_JENJANG'] == 10 and $cmb['ID_FAKULTAS'] == 1)  // PPDS - Dokter Spesialis
                    {
                        $presentase_tpa = 0.1;  // 10%
                        $presentase_inggris = 0.1; // 10%
                    }

                    if ($cmb['ID_JENJANG'] == 10 and $cmb['ID_FAKULTAS'] == 2)  // PPDGS - Dokter Gigi Spesialis
                    {
                        $presentase_tpa = 0.2;  // 20%
                        $presentase_inggris = 0.1; // 10%
                    }

                    $no_awal_tpa = 0;
                    $no_akhir_tpa = 59;

                    $no_awal_inggris = 60;
                    $no_akhir_inggris = 119;
                }

                if (count($nilai_line) > 2)
                {
                    // satuan skor
                    $skor_benar = 4;
                    $skor_salah = 0;
                    $skor_kosong = 0;

                    // inisialisasi skor
                    $jumlah_benar_tpa = 0;
                    $jumlah_salah_tpa = 0;
                    $jumlah_kosong_tpa = 0;

                    $jumlah_benar_inggris = 0;
                    $jumlah_salah_inggris = 0;
                    $jumlah_kosong_inggris = 0;

                    // Mendapatkan kunci jawaban
                    $kunci = $db->QuerySingle("
                        select data_kunci from penerimaan_kunci_soal k
                        where
                            k.kode_soal = '{$nilai_line[3]}' and
                            k.id_penerimaan in (select id_penerimaan from calon_mahasiswa_baru where no_ujian = '{$nilai_line[0]}')");

                    $jawaban = $nilai_line[4];

                    for ($i = 0; $i < strlen($kunci); $i++)
                    {
                        if (substr($jawaban, $i, 1) == 'X')
                        {
                            if ($no_awal_tpa <= $i && $i <= $no_akhir_tpa)
                            {
                                $jumlah_kosong_tpa += 1;
                            }

                            if ($no_awal_inggris <= $i && $i <= $no_akhir_inggris)
                            {
                                $jumlah_kosong_inggris += 1;
                            }

                        }
                        else if (substr($jawaban, $i, 1) == substr($kunci, $i, 1))
                        {
                            if ($no_awal_tpa <= $i && $i <= $no_akhir_tpa)
                            {
                                $jumlah_benar_tpa += 1;
                            }

                            if ($no_awal_inggris <= $i && $i <= $no_akhir_inggris)
                            {
                                $jumlah_benar_inggris += 1;
                            }
                        }
                        else
                        {
                            if ($no_awal_tpa <= $i && $i <= $no_akhir_tpa)
                            {
                                $jumlah_salah_tpa += 1;
                            }

                            if ($no_awal_inggris <= $i && $i <= $no_akhir_inggris)
                            {
                                $jumlah_salah_inggris += 1;
                            }
                        }
                    }

                    array_push($nilai_set, array(
                        'ID_C_MHS'      => $cmb['ID_C_MHS'],
                        'NO_UJIAN'      => $no_ujian,
                        'NM_C_MHS'      => $nilai_line[1],
                        'KODE_SOAL'     => $nilai_line[3],

                        'JUMLAH_BENAR_TPA'     => $jumlah_benar_tpa,
                        'JUMLAH_SALAH_TPA'     => $jumlah_salah_tpa,
                        'JUMLAH_KOSONG_TPA'    => $jumlah_kosong_tpa,
                        'NILAI_TPA'            => ($jumlah_benar_tpa * $skor_benar) - ($jumlah_salah_tpa * $skor_salah),
                        'SKOR_TPA'             => number_format(($jumlah_benar_tpa / ($no_akhir_tpa + 1)) * (100 * $presentase_tpa), 2),

                        'JUMLAH_BENAR_INGGRIS'     => $jumlah_benar_inggris,
                        'JUMLAH_SALAH_INGGRIS'     => $jumlah_salah_inggris,
                        'JUMLAH_KOSONG_INGGRIS'    => $jumlah_kosong_inggris,
                        'NILAI_INGGRIS'            => ($jumlah_benar_inggris * $skor_benar) - ($jumlah_salah_inggris * $skor_salah),
                        'SKOR_INGGRIS'             => number_format(($jumlah_benar_inggris / ($no_akhir_inggris + 1 - $no_awal_inggris)) * (100 * $presentase_inggris), 2)
                    ));
                }
            }
        }
        
        $smarty->assign('nilai_set', $nilai_set);
    }
}

$smarty->display("penilaian/scan/{$mode}.tpl");
?>
