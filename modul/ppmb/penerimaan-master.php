<?php

include('config.php');

$id_pt = $id_pt_user;

if (!in_array($user->ID_JABATAN_PEGAWAI, array(175, 176, 203)))
{
	// 175 : ketua ppmb
	// 176 : skretaris
	// 203 : admin ppmb
	echo "<div class=\"center_title_bar\">Master Penerimaan</div>";
	echo "<h2>Maaf, anda tidak mempunyai hak akses halaman ini</h2>";
	exit();
}

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
	if (post('mode') == 'add')
	{
		// untuk kebutuhan memanggil id_semester
		$tahun_smt = post('tahun');
		$nm_smt = post('semester');

		if($nm_smt == 'Gasal'){
			$nm_smt = 'Ganjil';
		}

		$db->Query("SELECT ID_SEMESTER FROM SEMESTER WHERE THN_AKADEMIK_SEMESTER = '{$tahun_smt}' AND NM_SEMESTER = '{$nm_smt}' AND ID_PERGURUAN_TINGGI = '{$id_pt}'");
		$id_smt = $db->FetchAssoc();

		$id_semester = $id_smt['ID_SEMESTER'];


		// Prepare String : Registrasi / 
		$tgl_awal_registrasi	= post('tgl_awal_reg_Year') . '-' . post('tgl_awal_reg_Month') . '-' . str_pad(post('tgl_awal_reg_Day'), 2, "0", STR_PAD_LEFT) . ' ' . post('jam_awal_registrasi');
		$tgl_akhir_registrasi	= post('tgl_akhir_reg_Year') . '-' . post('tgl_akhir_reg_Month') . '-' . str_pad(post('tgl_akhir_reg_Day'), 2, "0", STR_PAD_LEFT) . ' ' . post('jam_akhir_registrasi');

		// Prepare String : Verifikasi
		$tgl_awal_verifikasi	= post('tgl_awal_ver_Year') . '-' . post('tgl_awal_ver_Month') . '-' . str_pad(post('tgl_awal_ver_Day'), 2, "0", STR_PAD_LEFT) . ' ' . post('jam_awal_verifikasi');
		$tgl_akhir_verifikasi	= post('tgl_akhir_ver_Year') . '-' . post('tgl_akhir_ver_Month') . '-' . str_pad(post('tgl_akhir_ver_Day'), 2, "0", STR_PAD_LEFT) . ' ' . post('jam_akhir_verifikasi');

		// Prepare String : Voucher
		$tgl_awal_voucher		= post('tgl_awal_voucher_Year') . '-' . post('tgl_awal_voucher_Month') . '-' . str_pad(post('tgl_awal_voucher_Day'), 2, "0", STR_PAD_LEFT) . ' ' . post('jam_awal_voucher');
		$tgl_akhir_voucher		= post('tgl_akhir_voucher_Year') . '-' . post('tgl_akhir_voucher_Month') . '-' . str_pad(post('tgl_akhir_voucher_Day'), 2, "0", STR_PAD_LEFT). ' ' . post('jam_akhir_voucher');

		// Prepare String : Pengumuman
		$tgl_pengumuman			= post('tgl_pengumuman_Year') . '-' . post('tgl_pengumuman_Month') . '-' . str_pad(post('tgl_pengumuman_Day'), 2, "0", STR_PAD_LEFT) . ' ' . post('jam_pengumuman');
		
		$db->Parse(
			"INSERT INTO PENERIMAAN
				(
					tahun, 
					id_jalur, 
					id_jenjang, 
					nm_penerimaan, 
					gelombang, 
					semester,
					is_pendaftaran_online,

					tgl_awal_registrasi, 
					tgl_akhir_registrasi, 
					tgl_awal_verifikasi, 
					tgl_akhir_verifikasi, 
					tgl_awal_voucher, 
					tgl_akhir_voucher,
					tgl_pengumuman,

					id_semester,

					is_aktif, 
					no_ujian_awal, 
					no_ujian_akhir, 
					pengambilan_nomor,
					id_form_kuisioner,
					jumlah_pilihan_prodi,
					is_verifikasi,
					is_bayar_voucher,
					nomor_rekening_transfer,
					is_ujian,
					id_perguruan_tinggi
				) 
			VALUES
				(
					:tahun, 
					:id_jalur, 
					:id_jenjang, 
					:nm_penerimaan, 
					:gelombang, 
					:semester,
					:is_pendaftaran_online,

					to_date(:tgl_awal_registrasi, 'YYYY-MM-DD HH24:MI'),
					to_date(:tgl_akhir_registrasi, 'YYYY-MM-DD HH24:MI'),
					to_date(:tgl_awal_verifikasi, 'YYYY-MM-DD HH24:MI'),
					to_date(:tgl_akhir_verifikasi, 'YYYY-MM-DD HH24:MI'),
					to_date(:tgl_awal_voucher, 'YYYY-MM-DD HH24:MI'),
					to_date(:tgl_akhir_voucher, 'YYYY-MM-DD HH24:MI'),
					to_date(:tgl_pengumuman, 'YYYY-MM-DD HH24:MI'),

					:id_semester,

					:is_aktif, 
					:no_ujian_awal, 
					:no_ujian_akhir, 
					:pengambilan_nomor,
					:id_form_kuisioner,
					:jumlah_pilihan_prodi,
					:is_verifikasi,
					:is_bayar_voucher,
					:nomor_rekening_transfer,
					:is_ujian,
					:id_perguruan_tinggi
				)");
		$db->BindByName(':tahun', post('tahun'));
		$db->BindByName(':id_jalur', post('id_jalur'));
		$db->BindByName(':id_jenjang', post('id_jenjang'));
		$db->BindByName(':nm_penerimaan', post('nm_penerimaan'));
		$db->BindByName(':gelombang', post('gelombang'));
		$db->BindByName(':semester', post('semester'));
		$db->BindByName(':is_pendaftaran_online', post('is_pendaftaran_online'));
		$db->BindByName(':tgl_awal_registrasi', $tgl_awal_registrasi);
		$db->BindByName(':tgl_akhir_registrasi', $tgl_akhir_registrasi);
		$db->BindByName(':tgl_awal_verifikasi', $tgl_awal_verifikasi);
		$db->BindByName(':tgl_akhir_verifikasi', $tgl_akhir_verifikasi);
		$db->BindByName(':tgl_awal_voucher', $tgl_awal_voucher);
		$db->BindByName(':tgl_akhir_voucher', $tgl_akhir_voucher);
		$db->BindByName(':tgl_pengumuman', $tgl_pengumuman);
		$db->BindByName(':id_semester', $id_semester);
		$db->BindByName(':is_aktif', post('is_aktif'));
		$db->BindByName(':no_ujian_awal', post('no_ujian_awal'));
		$db->BindByName(':no_ujian_akhir', post('no_ujian_akhir'));
		$db->BindByName(':pengambilan_nomor', post('pengambilan_nomor'));
		$db->BindByName(':id_form_kuisioner', post('id_form_kuisioner'));
		$db->BindByName(':jumlah_pilihan_prodi', post('jumlah_pilihan_prodi'));
		$db->BindByName(':is_verifikasi', post('is_verifikasi'));
		$db->BindByName(':is_bayar_voucher', post('is_bayar_voucher'));
		$db->BindByName(':nomor_rekening_transfer', post('nomor_rekening'));
		$db->BindByName(':is_ujian', post('is_ujian'));
		$db->BindByName(':id_perguruan_tinggi', $id_pt);
		$result = $db->Execute();

		if ($result)
			$smarty->assign('added', 'Data berhasil ditambahkan');
		else
		{
			print_r($db); exit();
			$smarty->assign('added', 'Data gagal ditambahkan');
		}
	}

	if (post('mode') == 'edit')
	{
		// untuk kebutuhan memanggil id_semester
		$tahun_smt = post('tahun');
		$nm_smt = post('semester');

		if($nm_smt == 'Gasal'){
			$nm_smt = 'Ganjil';
		}

		$db->Query("SELECT ID_SEMESTER FROM SEMESTER WHERE THN_AKADEMIK_SEMESTER = '{$tahun_smt}' AND NM_SEMESTER = '{$nm_smt}' AND ID_PERGURUAN_TINGGI = '{$id_pt}'");
		$id_smt = $db->FetchAssoc();

		$id_semester = $id_smt['ID_SEMESTER'];

		// Prepare String : Registrasi / 
		$tgl_awal_registrasi	= post('tgl_awal_reg_Year') . '-' . post('tgl_awal_reg_Month') . '-' . str_pad(post('tgl_awal_reg_Day'), 2, "0", STR_PAD_LEFT) . ' ' . post('jam_awal_registrasi');
		$tgl_akhir_registrasi	= post('tgl_akhir_reg_Year') . '-' . post('tgl_akhir_reg_Month') . '-' . str_pad(post('tgl_akhir_reg_Day'), 2, "0", STR_PAD_LEFT) . ' ' . post('jam_akhir_registrasi');

		// Prepare String : Verifikasi
		$tgl_awal_verifikasi	= post('tgl_awal_ver_Year') . '-' . post('tgl_awal_ver_Month') . '-' . str_pad(post('tgl_awal_ver_Day'), 2, "0", STR_PAD_LEFT) . ' ' . post('jam_awal_verifikasi');
		$tgl_akhir_verifikasi	= post('tgl_akhir_ver_Year') . '-' . post('tgl_akhir_ver_Month') . '-' . str_pad(post('tgl_akhir_ver_Day'), 2, "0", STR_PAD_LEFT) . ' ' . post('jam_akhir_verifikasi');

		// Prepare String : Voucher
		$tgl_awal_voucher		= post('tgl_awal_voucher_Year') . '-' . post('tgl_awal_voucher_Month') . '-' . str_pad(post('tgl_awal_voucher_Day'), 2, "0", STR_PAD_LEFT) . ' ' . post('jam_awal_voucher');
		$tgl_akhir_voucher		= post('tgl_akhir_voucher_Year') . '-' . post('tgl_akhir_voucher_Month') . '-' . str_pad(post('tgl_akhir_voucher_Day'), 2, "0", STR_PAD_LEFT). ' ' . post('jam_akhir_voucher');

		// Prepare String : Pengumuman
		$tgl_pengumuman			= post('tgl_pengumuman_Year') . '-' . post('tgl_pengumuman_Month') . '-' . str_pad(post('tgl_pengumuman_Day'), 2, "0", STR_PAD_LEFT) . ' ' . post('jam_pengumuman');

		$db->Parse(
			"UPDATE PENERIMAAN SET

				tahun = :tahun, 
				id_jalur = :id_jalur, 
				id_jenjang = :id_jenjang, 
				nm_penerimaan = :nm_penerimaan, 
				gelombang = :gelombang, 
				semester = :semester,
				is_pendaftaran_online = :is_pendaftaran_online,

				tgl_awal_registrasi		= to_date(:tgl_awal_registrasi, 'YYYY-MM-DD HH24:MI'),
				tgl_akhir_registrasi	= to_date(:tgl_akhir_registrasi, 'YYYY-MM-DD HH24:MI'),
				tgl_awal_verifikasi		= to_date(:tgl_awal_verifikasi, 'YYYY-MM-DD HH24:MI'),
				tgl_akhir_verifikasi	= to_date(:tgl_akhir_verifikasi, 'YYYY-MM-DD HH24:MI'),
				tgl_awal_voucher		= to_date(:tgl_awal_voucher, 'YYYY-MM-DD HH24:MI'),
				tgl_akhir_voucher		= to_date(:tgl_akhir_voucher, 'YYYY-MM-DD HH24:MI'),
				tgl_pengumuman			= to_date(:tgl_pengumuman, 'YYYY-MM-DD HH24:MI'),

				id_semester 			= :id_semester,

				is_aktif				= :is_aktif,
				no_ujian_awal			= :no_ujian_awal,
				no_ujian_akhir			= :no_ujian_akhir,
				pengambilan_nomor		= :pengambilan_nomor,
				id_form_kuisioner		= :id_form_kuisioner,
				jumlah_pilihan_prodi	= :jumlah_pilihan_prodi,
				is_verifikasi			= :is_verifikasi,
				is_bayar_voucher		= :is_bayar_voucher,
				nomor_rekening_transfer	= :nomor_rekening_transfer,
				is_ujian				= :is_ujian,
				id_perguruan_tinggi 	= :id_perguruan_tinggi

			WHERE ID_PENERIMAAN = :id_penerimaan");
		$db->BindByName(':tahun', post('tahun'));
		$db->BindByName(':id_jalur', post('id_jalur'));
		$db->BindByName(':id_jenjang', post('id_jenjang'));
		$db->BindByName(':nm_penerimaan', post('nm_penerimaan'));
		$db->BindByName(':gelombang', post('gelombang'));
		$db->BindByName(':semester', post('semester'));
		$db->BindByName(':is_pendaftaran_online', post('is_pendaftaran_online'));
		$db->BindByName(':tgl_awal_registrasi', $tgl_awal_registrasi);
		$db->BindByName(':tgl_akhir_registrasi', $tgl_akhir_registrasi);
		$db->BindByName(':tgl_awal_verifikasi', $tgl_awal_verifikasi);
		$db->BindByName(':tgl_akhir_verifikasi', $tgl_akhir_verifikasi);
		$db->BindByName(':tgl_awal_voucher', $tgl_awal_voucher);
		$db->BindByName(':tgl_akhir_voucher', $tgl_akhir_voucher);
		$db->BindByName(':tgl_pengumuman', $tgl_pengumuman);
		$db->BindByName(':id_semester', $id_semester);
		$db->BindByName(':is_aktif', post('is_aktif'));
		$db->BindByName(':no_ujian_awal', post('no_ujian_awal'));
		$db->BindByName(':no_ujian_akhir', post('no_ujian_akhir'));
		$db->BindByName(':pengambilan_nomor', post('pengambilan_nomor'));
		$db->BindByName(':id_form_kuisioner', post('id_form_kuisioner'));
		$db->BindByName(':jumlah_pilihan_prodi', post('jumlah_pilihan_prodi'));
		$db->BindByName(':is_verifikasi', post('is_verifikasi'));
		$db->BindByName(':is_bayar_voucher', post('is_bayar_voucher'));
		$db->BindByName(':nomor_rekening_transfer', post('nomor_rekening'));
		$db->BindByName(':is_ujian', post('is_ujian'));
		$db->BindByName(':id_perguruan_tinggi', $id_pt);
		$db->BindByName(':id_penerimaan', post('id_penerimaan'));
		$result = $db->Execute();

		if ($result)
			$smarty->assign('changed', 'Data berhasil dirubah');
		else
			$smarty->assign('changed', 'Data gagal dirubah');
	}
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$tahun = get('tahun', date('Y'));
		$aktif = get('aktif', 'all');
		
		$smarty->assign('tahun_get', $tahun);
		
		$semester_set = $db->QueryToArray("SELECT count(*) as master_semester FROM semester 
											WHERE tipe_semester = 'REG' AND thn_akademik_semester = {$tahun} AND id_perguruan_tinggi = {$id_pt}");
		$smarty->assign('master_semester', $semester_set[0]['MASTER_SEMESTER']);
		
		if ($aktif == 'all')
			$aktif = "";
		else if ($aktif == 'aktif')
			$aktif = "and P.IS_AKTIF = 1";
		else if ($aktif == 'non-aktif')
			$aktif = "and P.IS_AKTIF = 0";

		$smarty->assign('penerimaan_set', $db->QueryToArray("
			SELECT P.*, nm_jenjang, nm_jalur, to_char(p.tgl_pengumuman, 'YYYY-MM-DD HH24:MI') as pengumuman
			FROM PENERIMAAN P 
			LEFT JOIN JENJANG J ON J.ID_JENJANG = P.ID_JENJANG
			LEFT JOIN JALUR JL ON JL.ID_JALUR = P.ID_JALUR
			WHERE P.TAHUN = {$tahun} {$aktif} AND P.ID_PERGURUAN_TINGGI = {$id_pt}
			ORDER BY p.tahun, p.semester, p.gelombang, p.id_jenjang, p.id_jalur, p.nm_penerimaan"));
			
		$smarty->assign('tahun_set', $db->QueryToArray("select min(tahun)-1 as tahun_awal, max(tahun)+1 as tahun_akhir from penerimaan where id_perguruan_tinggi = {$id_pt}"));
	}

	if ($mode == 'edit')
	{
		$id_penerimaan = get('id_penerimaan');

		$smarty->assign('tahun_set', array(
			array('ID' => 2011, 'VIEW' => 2011),
			array('ID' => 2012, 'VIEW' => 2012)
		));

		$smarty->assign('jenjang_set', array(
			array('ID_JENJANG' => 1, 'NM_JENJANG' => 'S1'),
			array('ID_JENJANG' => 2, 'NM_JENJANG' => 'S2'),
			array('ID_JENJANG' => 3, 'NM_JENJANG' => 'S3'),
			array('ID_JENJANG' => 4, 'NM_JENJANG' => 'D4'),
			array('ID_JENJANG' => 5, 'NM_JENJANG' => 'D3'),
			array('ID_JENJANG' => 9, 'NM_JENJANG' => 'Profesi'),
			array('ID_JENJANG' => 10, 'NM_JENJANG' => 'Spesialis')
		));

		$smarty->assign('gelombang_set', array(
			array('ID' => 1, 'VIEW' => 1),
			array('ID' => 2, 'VIEW' => 2),
			array('ID' => 3, 'VIEW' => 3),
			array('ID' => 4, 'VIEW' => 4),
			array('ID' => 5, 'VIEW' => 5)
		));

		$smarty->assign('semester_set', array(
			array('ID' => '-', 'VIEW' => '-'),
			array('ID' => 'Gasal', 'VIEW' => 'Gasal'),
			array('ID' => 'Genap', 'VIEW' => 'Genap')
		));

		$smarty->assign('status_set', array(
			array('ID' => 0, 'VIEW' => 'Tidak Aktif'),
			array('ID' => 1, 'VIEW' => 'Aktif')
		));

		$smarty->assign('jalur_set', $db->QueryToArray("select * from jalur where id_perguruan_tinggi = {$id_pt} and is_aktif = '1'"));

		// Ambil data form kuisioner
		$kuisioner_set = $db->QueryToArray("SELECT * FROM KUISIONER_FORM WHERE TIPE_FORM = 2 AND ID_PERGURUAN_TINGGI = '{$id_pt}'");
		$smarty->assignByRef('kuisioner_set', $kuisioner_set);

		$rows = $db->QueryToArray("
			select
				id_penerimaan, tahun, id_jalur, id_jenjang, htf.escape_sc(nm_penerimaan) as nm_penerimaan, gelombang, semester, is_aktif, no_ujian_awal, no_ujian_akhir,
				to_char(tgl_awal_registrasi, 'YYYY-MM-DD HH24:MI') tgl_awal_registrasi,
				to_char(tgl_akhir_registrasi, 'YYYY-MM-DD HH24:MI') tgl_akhir_registrasi,
				to_char(tgl_awal_verifikasi, 'YYYY-MM-DD HH24:MI') tgl_awal_verifikasi,
				to_char(tgl_akhir_verifikasi, 'YYYY-MM-DD HH24:MI') tgl_akhir_verifikasi,
				to_char(tgl_pengumuman, 'YYYY-MM-DD HH24:MI') tgl_pengumuman,
				to_char(tgl_awal_voucher, 'YYYY-MM-DD HH24:MI') tgl_awal_voucher,
				to_char(tgl_akhir_voucher, 'YYYY-MM-DD HH24:MI') tgl_akhir_voucher,
				pengambilan_nomor,
				id_form_kuisioner,
				is_pendaftaran_online,
				jumlah_pilihan_prodi,
				is_verifikasi,
				is_bayar_voucher,
				nomor_rekening_transfer,
				is_ujian
			from penerimaan where id_penerimaan = {$id_penerimaan}");

		$smarty->assign('penerimaan', $rows[0]);

		/*$nm_penerimaan = htmlentities($rows[0]['NM_PENERIMAAN']);
		$smarty->assign('nm_penerimaan', $nm_penerimaan);*/
	}

	if ($mode == 'add')
	{
		$smarty->assign('tahun_set', array(
			array('ID' => 2011, 'VIEW' => 2011),
			array('ID' => 2012, 'VIEW' => 2012)
		));

		$smarty->assign('jenjang_set', array(
			array('ID_JENJANG' => 1, 'NM_JENJANG' => 'S1'),
			array('ID_JENJANG' => 2, 'NM_JENJANG' => 'S2'),
			array('ID_JENJANG' => 3, 'NM_JENJANG' => 'S3'),
			array('ID_JENJANG' => 4, 'NM_JENJANG' => 'D4'),
			array('ID_JENJANG' => 5, 'NM_JENJANG' => 'D3'),
			array('ID_JENJANG' => 9, 'NM_JENJANG' => 'Profesi'),
			array('ID_JENJANG' => 10, 'NM_JENJANG' => 'Spesialis')
		));

		$smarty->assign('gelombang_set', array(
			array('ID' => 1, 'VIEW' => 1),
			array('ID' => 2, 'VIEW' => 2),
			array('ID' => 3, 'VIEW' => 3),
			array('ID' => 4, 'VIEW' => 4),
			array('ID' => 5, 'VIEW' => 5)
		));

		$smarty->assign('semester_set', array(
			array('ID' => '-', 'VIEW' => '-'),
			array('ID' => 'Gasal', 'VIEW' => 'Gasal'),
			array('ID' => 'Genap', 'VIEW' => 'Genap')
		));

		$smarty->assign('status_set', array(
			array('ID' => 0, 'VIEW' => 'Tidak Aktif'),
			array('ID' => 1, 'VIEW' => 'Aktif')
		));

		$smarty->assign('jalur_set', $db->QueryToArray("select * from jalur where id_perguruan_tinggi = {$id_pt} and is_aktif = '1'"));

		// Ambil data form kuisioner
		$kuisioner_set = $db->QueryToArray("SELECT * FROM KUISIONER_FORM WHERE TIPE_FORM = 2 AND ID_PERGURUAN_TINGGI = '{$id_pt}'");
		$smarty->assignByRef('kuisioner_set', $kuisioner_set);
	}
}

$smarty->display("penerimaan/master/{$mode}.tpl");
?>
