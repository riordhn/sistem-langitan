<?php
include('config.php');
include('class/snmptn.class.php');

$mode = get('mode', 'view');
$snmptn = new SnmptnClass($db);

if ($request_method == 'GET')
{
    $smarty->assign('cmb_set', $snmptn->GetListPM());
}

$smarty->display("snmptn/rpm/{$mode}.tpl");
?>
