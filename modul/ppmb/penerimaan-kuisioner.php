<?php
include 'config.php';

$mode = get('mode', 'view');

$id_pt = $id_pt_user;

// -----------------------------------------------------------------------
// Master Kuisioner menggunakan tabel kuisioner form dengan tipe_form = 2
// -----------------------------------------------------------------------

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	if (post('mode') == 'add')
	{
		$db->Parse("INSERT INTO KUISIONER_FORM (NM_FORM, TIPE_FORM, ID_PERGURUAN_TINGGI) VALUES (:nm_form, 2, :id_pt)");
		$db->BindByName(':nm_form', post('nm_form'));
		$db->BindByName(':id_pt', $id_pt);
		$result = $db->Execute();

		if ($result)
			$smarty->assign('message', 'Data berhasil disimpan');
		else
			$smarty->assign('message', 'Data gagal disimpan');
	}

	if (post('mode') == 'edit')
	{
		$db->Parse("UPDATE PPMB_KUISIONER SET NM_KUISIONER = :nm_kuisioner WHERE ID_KUISIONER = :id_kuisioner");
		$db->BindByName(':nm_kuisioner', post('nm_kuisioner'));
		$db->BindByName(':id_kuisioner', post('id_kuisioner'));
		$result = $db->Execute();

		if ($result)
			$smarty->assign('message', 'Data berhasil disimpan');
		else
			$smarty->assign('message', 'Data gagal disimpan');
	}

	if (post('mode') == 'add-kategori')
	{
		$db->Parse("INSERT INTO KUISIONER_KATEGORI_SOAL (ID_FORM, KODE_KATEGORI, NM_KATEGORI) VALUES (:id_form, :kode_kategori, :nm_kategori)");
		$db->BindByName(':id_form', post('id_form'));
		$db->BindByName(':kode_kategori', post('kode_kategori'));
		$db->BindByName(':nm_kategori', post('nm_kategori'));
		$result = $db->Execute();

		if ($result)
			$smarty->assign('message', 'Data berhasil disimpan');
		else
			$smarty->assign('message', 'Data gagal disimpan');
	}

	if (post('mode') == 'edit-kategori')
	{
		$db->Parse(
			"UPDATE KUISIONER_KATEGORI_SOAL SET
				KODE_KATEGORI = :kode_kategori, NM_KATEGORI = :nm_kategori
			WHERE ID_KATEGORI_SOAL = :id_kategori_soal");
		$db->BindByName(':kode_kategori', post('kode_kategori'));
		$db->BindByName(':nm_kategori', post('nm_kategori'));
		$db->BindByName(':id_kategori_soal', post('id_kategori_soal'));
		$result = $db->Execute();

		if ($result)
			$smarty->assign('message', 'Data berhasil disimpan');
		else
			$smarty->assign('message', 'Data gagal disimpan');
	}

	if (post('mode') == 'delete-kategori')
	{
		$id_kategori_soal = post('id_kategori_soal');
		$db->Query("DELETE FROM KUISIONER_KATEGORI_SOAL WHERE ID_KATEGORI_SOAL = {$id_kategori_soal}");
		echo "1";
		exit();
	}

	if (post('mode') == 'add-soal')
	{
		$db->Parse(
			"INSERT INTO KUISIONER_SOAL (ID_FORM, NOMER, ID_KATEGORI_SOAL, SOAL, TIPE_SOAL) 
			VALUES (:id_form, :nomer, :id_kategori_soal, :soal, :tipe_soal)");
		$db->BindByName(':id_form', post('id_form'));
		$db->BindByName(':nomer', post('nomer'));
		$db->BindByName(':id_kategori_soal', post('id_kategori_soal'));
		$db->BindByName(':soal', post('soal'));
		$db->BindByName(':tipe_soal', post('tipe_soal'));
		$result = $db->Execute();

		if ($result)
			$smarty->assign('message', 'Data berhasil disimpan');
		else
			$smarty->assign('message', 'Data gagal disimpan');
	}

	if (post('mode') == 'edit-soal')
	{
		$db->Parse(
			"UPDATE KUISIONER_SOAL SET
				NOMER = :nomer,
				ID_KATEGORI_SOAL = :id_kategori_soal,
				SOAL = :soal,
				TIPE_SOAL = :tipe_soal
			WHERE ID_SOAL = :id_soal AND ID_FORM = :id_form");
		$db->BindByName(':nomer', post('nomer'));
		$db->BindByName(':id_kategori_soal', post('id_kategori_soal'));
		$db->BindByName(':soal', post('soal'));
		$db->BindByName(':tipe_soal', post('tipe_soal'));
		$db->BindByName(':id_soal', post('id_soal'));
		$db->BindByName(':id_form', post('id_form'));
		$result = $db->Execute();

		if ($result)
			$smarty->assign('message', 'Data berhasil disimpan');
		else
			$smarty->assign('message', 'Data gagal disimpan');
	}

	if (post('mode') == 'delete-soal')
	{
		$id_soal = post('id_soal');
		$db->Query("DELETE FROM KUISIONER_SOAL WHERE ID_SOAL = {$id_soal}");
		echo "1";
		exit();
	}

	if (post('mode') == 'add-jawaban')
	{
		$db->Parse(
			"INSERT INTO KUISIONER_JAWABAN 
			(ID_SOAL, TIPE_JAWABAN, URUTAN, JAWABAN) 
			VALUES (:id_soal, :tipe_jawaban, :urutan, :jawaban)");
		$db->BindByName(':id_soal', post('id_soal'));
		$db->BindByName(':tipe_jawaban', post('tipe_jawaban'));
		$db->BindByName(':urutan', post('urutan'));
		$db->BindByName(':jawaban', post('jawaban'));
		$result = $db->Execute();

		if ($result)
			$smarty->assign('message', 'Data berhasil disimpan');
		else
			$smarty->assign('message', 'Data gagal disimpan');
	}

	if (post('mode') == 'edit-jawaban')
	{
		$db->Parse(
			"UPDATE KUISIONER_JAWABAN SET
				URUTAN = :urutan,
				JAWABAN = :jawaban
			WHERE ID_JAWABAN = :id_jawaban AND ID_SOAL = :id_soal");
		$db->BindByName(':tipe_jawaban', post('tipe_jawaban'));
		$db->BindByName(':urutan', post('urutan'));
		$db->BindByName(':jawaban', post('jawaban'));
		$db->BindByName(':id_jawaban', post('id_jawaban'));
		$db->BindByName(':id_soal', post('id_soal'));
		$result = $db->Execute();

		if ($result)
			$smarty->assign('message', 'Data berhasil disimpan');
		else
			$smarty->assign('message', 'Data gagal disimpan');
	}

	if (post('mode') == 'delete-jawaban')
	{
		$id_jawaban = post('id_jawaban');
		$db->Query("DELETE FROM KUISIONER_JAWABAN WHERE ID_JAWABAN = {$id_jawaban}");
		echo "1";
		exit();
	}
}

if ($_SERVER['REQUEST_METHOD'] == 'GET' || $_SERVER['REQUEST_METHOD'] == 'POST')
{
	if ($mode == 'view')
	{
		$form_set = $db->QueryToArray("SELECT * FROM KUISIONER_FORM WHERE TIPE_FORM = 2 AND ID_PERGURUAN_TINGGI = '{$id_pt}'");
		$smarty->assignByRef('form_set', $form_set);
	}

	if ($mode == 'edit')
	{
		$id_form = get('id_form');

		$db->Query("SELECT * FROM KUISIONER_FORM WHERE ID_FORM = {$id_form} AND TIPE_FORM = 2");
		$smarty->assignByRef('form', $db->FetchAssoc());
	}

	if ($mode == 'view-soal')
	{
		$id_form = get('id_form');

		$db->Query("SELECT * FROM KUISIONER_FORM WHERE ID_FORM = {$id_form} AND TIPE_FORM = 2");
		$smarty->assignByRef('form', $db->FetchAssoc());

		// data kategori soal
		$kategori_set = $db->QueryToArray("SELECT * FROM KUISIONER_KATEGORI_SOAL WHERE ID_FORM = {$id_form}");
		$smarty->assignByRef('kategori_set', $kategori_set);

		// data soal
		$soal_set = $db->QueryToArray(
			"SELECT 
				S.ID_SOAL, S.NOMER,
				KS.NM_KATEGORI,
				S.SOAL, S.TIPE_SOAL,
				JS.NAMA_JENIS AS JENIS_SOAL
			FROM KUISIONER_SOAL S
			INNER JOIN KUISIONER_JENIS_SOAL JS ON JS.ID_JENIS_SOAL = S.TIPE_SOAL
			LEFT JOIN KUISIONER_KATEGORI_SOAL KS ON KS.ID_KATEGORI_SOAL = S.ID_KATEGORI_SOAL
			WHERE S.ID_FORM = {$id_form}
			ORDER BY TO_NUMBER(S.NOMER)");
		$smarty->assignByRef('soal_set', $soal_set);
	}

	if ($mode == 'add-kategori')
	{
		$id_form = get('id_form');

		$db->Query("SELECT * FROM KUISIONER_FORM WHERE ID_FORM = {$id_form} AND TIPE_FORM = 2");
		$smarty->assignByRef('form', $db->FetchAssoc());
	}

	if ($mode == 'edit-kategori')
	{
		$id_form = get('id_form');
		$id_kategori_soal = get('id_kategori_soal');

		// row FORM
		$db->Query("SELECT * FROM KUISIONER_FORM WHERE ID_FORM = {$id_form} AND TIPE_FORM = 2");
		$smarty->assignByRef('form', $db->FetchAssoc());

		// row KATEGORI_SOAL
		$db->Query("SELECT * FROM KUISIONER_KATEGORI_SOAL WHERE ID_FORM = {$id_form} AND ID_KATEGORI_SOAL = {$id_kategori_soal}");
		$smarty->assignByRef('kategori', $db->FetchAssoc());
	}

	if ($mode == 'add-soal')
	{
		$id_form = get('id_form');

		// row FORM
		$db->Query("SELECT * FROM KUISIONER_FORM WHERE ID_FORM = {$id_form} AND TIPE_FORM = 2");
		$smarty->assignByRef('form', $db->FetchAssoc());

		// data kategori soal
		$kategori_set = $db->QueryToArray("SELECT * FROM KUISIONER_KATEGORI_SOAL WHERE ID_FORM = {$id_form}");
		$smarty->assignByRef('kategori_set', $kategori_set);

		// data jenis soal
		$jenis_soal_set = $db->QueryToArray("SELECT * FROM KUISIONER_JENIS_SOAL");
		$smarty->assignByRef('jenis_soal_set', $jenis_soal_set);
	}

	if ($mode == 'edit-soal')
	{
		$id_form = get('id_form');
		$id_soal = get('id_soal');

		// row FORM
		$db->Query("SELECT * FROM KUISIONER_FORM WHERE ID_FORM = {$id_form} AND TIPE_FORM = 2");
		$smarty->assignByRef('form', $db->FetchAssoc());

		// data kategori soal
		$kategori_set = $db->QueryToArray("SELECT * FROM KUISIONER_KATEGORI_SOAL WHERE ID_FORM = {$id_form}");
		$smarty->assignByRef('kategori_set', $kategori_set);

		// data jenis soal
		$jenis_soal_set = $db->QueryToArray("SELECT * FROM KUISIONER_JENIS_SOAL");
		$smarty->assignByRef('jenis_soal_set', $jenis_soal_set);

		// row SOAL
		$db->Query("SELECT * FROM KUISIONER_SOAL WHERE ID_FORM = {$id_form} AND ID_SOAL = {$id_soal}");
		$smarty->assignByRef('soal', $db->FetchAssoc());
	}

	if ($mode == 'view-jawaban')
	{
		$id_form = get('id_form');
		$id_soal = get('id_soal');

		// row FORM
		$db->Query("SELECT * FROM KUISIONER_FORM WHERE ID_FORM = {$id_form} AND TIPE_FORM = 2");
		$smarty->assignByRef('form', $db->FetchAssoc());

		// row SOAL
		$db->Query("SELECT * FROM KUISIONER_SOAL WHERE ID_FORM = {$id_form} AND ID_SOAL = {$id_soal}");
		$smarty->assignByRef('soal', $db->FetchAssoc());

		// data jawaban
		$jawaban_set = $db->QueryToArray("SELECT * FROM KUISIONER_JAWABAN WHERE ID_SOAL = {$id_soal} ORDER BY TO_NUMBER(URUTAN) ASC");
		$smarty->assignByRef('jawaban_set', $jawaban_set);
	}

	if ($mode == 'add-jawaban')
	{
		$id_form = get('id_form');
		$id_soal = get('id_soal');

		// row FORM
		$db->Query("SELECT * FROM KUISIONER_FORM WHERE ID_FORM = {$id_form} AND TIPE_FORM = 2");
		$smarty->assignByRef('form', $db->FetchAssoc());

		// row SOAL
		$db->Query("SELECT * FROM KUISIONER_SOAL WHERE ID_FORM = {$id_form} AND ID_SOAL = {$id_soal}");
		$smarty->assignByRef('soal', $db->FetchAssoc());
	}

	if ($mode == 'edit-jawaban')
	{
		$id_form = get('id_form');
		$id_soal = get('id_soal');
		$id_jawaban = get('id_jawaban');

		// row FORM
		$db->Query("SELECT * FROM KUISIONER_FORM WHERE ID_FORM = {$id_form} AND TIPE_FORM = 2");
		$smarty->assignByRef('form', $db->FetchAssoc());

		// row SOAL
		$db->Query("SELECT * FROM KUISIONER_SOAL WHERE ID_FORM = {$id_form} AND ID_SOAL = {$id_soal}");
		$smarty->assignByRef('soal', $db->FetchAssoc());

		// row JAWABAN
		$db->Query("SELECT * FROM KUISIONER_JAWABAN WHERE ID_SOAL = {$id_soal} AND ID_JAWABAN = {$id_jawaban}");
		$smarty->assignByRef('jawaban', $db->FetchAssoc());
	}
}

$smarty->display("penerimaan/kuisioner/{$mode}.tpl");