<?php
include 'config.php';
include 'class/Penetapan.class.php';

$mode = get('mode', 'view');
$Penetapan = new Penetapan($db);

if ($request_method == 'GET')
{
    if ($mode == 'view')
    {
        $q = get('q');
		$data_set = $Penetapan->GetListPesertaPascaFindByName($q);
		
		$q = strtoupper($q);
		
		foreach ($data_set as &$data)
		{
			$data['NM_C_MHS'] = str_replace($q, '<span style="background-color: rgba(0,255,0,0.5)">'.$q.'</span>', strtoupper($data['NM_C_MHS']));
		}
		
		$smarty->assign('data_set', $data_set);
    }
}

$smarty->display("penetapan/peserta2/{$mode}.tpl");
?>
