<?php

include('config.php');
include('class/ppmb.class.php');

if (!in_array($user->ID_JABATAN_PEGAWAI, array(175, 176, 203, 222)))
{
	// 203 = Admin
	echo "Anda tidak punya hak akses halaman ini.";
	exit();
}

$jadwal_ppmb = new ppmb($db);
$mode = get('mode', 'list');

if ($request_method == 'POST')
{
	if (post('mode') == 'add')
	{		
		$tgl_test		= date('d-M-Y', mktime(0, 0, 0, post('tgl_test_Month'), post('tgl_test_Day'), post('tgl_test_Year')));
		$tgl_test2 		= date('d-M-Y', mktime(0, 0, 0, post('tgl_test2_Month'), post('tgl_test2_Day'), post('tgl_test2_Year')));
		$tgl_test3		= date('d-M-Y', mktime(0, 0, 0, post('tgl_test3_Month'), post('tgl_test3_Day'), post('tgl_test3_Year')));
		$dua_hari_test	= (post('dua_hari_test') == 'on') ? 1 : 0;
		$tiga_hari_test	= (post('tiga_hari_test') == 'on') ? 1 : 0;
		$jadwal_ppmb->add_jadwal_ppmb(
			post('id_fakultas'),
			post('lokasi'),
			post('alamat'), 
			post('ruang'), 
			$tgl_test, 
			$tgl_test2, 
			$tgl_test3, 
			post('kapasitas'), 
			post('kode_jalur'), 
			post('nomer_awal'), 
			post('id_penerimaan'), 
			post('materi'), 
			$dua_hari_test, 
			$tiga_hari_test,
			post('waktu')
		);
	}
	
	if (post('mode') == 'edit')
	{
		$tgl_test		= date('d-M-Y', mktime(0, 0, 0, post('tgl_test_Month'), post('tgl_test_Day'), post('tgl_test_Year')));
		$tgl_test2 		= date('d-M-Y', mktime(0, 0, 0, post('tgl_test2_Month'), post('tgl_test2_Day'), post('tgl_test2_Year')));
		$tgl_test3		= date('d-M-Y', mktime(0, 0, 0, post('tgl_test3_Month'), post('tgl_test3_Day'), post('tgl_test3_Year')));
		$dua_hari_test	= (post('dua_hari_test') == 'on') ? 1 : 0;
		$tiga_hari_test	= (post('tiga_hari_test') == 'on') ? 1 : 0;
		
		$result = $jadwal_ppmb->update_jadwal_ppmb(
			post('id_jadwal'), 
			post('lokasi'), 
			post('alamat'), 
			post('ruang'), 
			$tgl_test, 
			$tgl_test2, 
			$tgl_test3, 
			post('kapasitas'), 
			post('kode_jalur'), 
			post('nomer_awal'), 
			post('materi'), 
			$dua_hari_test, 
			$tiga_hari_test,
			post('waktu')
		);
		$smarty->assign('result', $result ? "Data berhasil di simpan" : "Data gagal disimpan");
	}

	if (post('mode') == 'edit-aktif')
	{
		$tgl_test		= date('d-M-Y', mktime(0, 0, 0, post('tgl_test_Month'), post('tgl_test_Day'), post('tgl_test_Year')));
		$tgl_test2 		= date('d-M-Y', mktime(0, 0, 0, post('tgl_test2_Month'), post('tgl_test2_Day'), post('tgl_test2_Year')));
		$tgl_test3		= date('d-M-Y', mktime(0, 0, 0, post('tgl_test3_Month'), post('tgl_test3_Day'), post('tgl_test3_Year')));
		$dua_hari_test	= (post('dua_hari_test') == 'on') ? 1 : 0;
		$tiga_hari_test	= (post('tiga_hari_test') == 'on') ? 1 : 0;
		
		$result = $jadwal_ppmb->update_aktif_jadwal_ppmb(
			post('id_fakultas'), 
			post('id_jadwal'), 
			post('lokasi'), 
			post('alamat'), 
			post('ruang'), 
			$tgl_test, 
			$tgl_test2, 
			$tgl_test3, 
			post('materi'), 
			$dua_hari_test, 
			$tiga_hari_test,
			post('waktu')
		);
		$smarty->assign('result', $result ? "Data berhasil di simpan" : "Data gagal disimpan");
	}

	if (post('mode') == 'delete')
	{
		$jadwal_ppmb->delete_jadwal_ppmb(post('id_jadwal'));
	}

	if (post('mode') == 'aktifkan')
	{
		$jadwal_ppmb->aktifkan(post('id_jadwal'));
	}

	if (post('mode') == 'non-aktifkan')
	{
		$jadwal_ppmb->nonaktifkan(post('id_jadwal_ppmb'));
	}
}

if ($mode == 'add')
{
	$smarty->assign('fakultas_set', $jadwal_ppmb->GetListFakultas());
	$smarty->assign('no_ujian_akhir', $jadwal_ppmb->GetNoTerakhir(get('id_penerimaan')));
}

if ($mode == 'view')
{
	$id_penerimaan = get('id_penerimaan');

	// Informasi pembelian voucher
	$db->Query("
		select
			(select count(*) from voucher where id_penerimaan = {$id_penerimaan} and tgl_bayar is not null and kode_jurusan = '01') as IPA,
			(select count(*) from voucher where id_penerimaan = {$id_penerimaan} and tgl_bayar is not null and kode_jurusan = '02') as IPS,
			(select count(*) from voucher where id_penerimaan = {$id_penerimaan} and tgl_bayar is not null and kode_jurusan = '03') as IPC
		from dual");
	$voucher = $db->FetchAssoc();

	$smarty->assign('voucher', $voucher);
	$smarty->assign('data_penerimaan_by_id', $jadwal_ppmb->load_penerimaan_by_id(get('id_penerimaan')));
	$smarty->assign('data_jadwal_ppmb', $jadwal_ppmb->load_jadwal_ppmb(get('id_penerimaan')));
}

if ($mode == 'edit')
{
	$smarty->assign('fakultas_set', $jadwal_ppmb->GetListFakultas());
	$smarty->assign('data_jadwal_ppmb_by_id', $jadwal_ppmb->load_jadwal_ppmb_by_id(get('id_jadwal')));
}

if ($mode == 'edit-aktif')
{
	$smarty->assign('data_jadwal_ppmb_by_id', $jadwal_ppmb->load_jadwal_ppmb_by_id(get('id_jadwal')));
	$smarty->assign('fakultas_set', $jadwal_ppmb->GetListFakultas());
}

if ($mode == 'delete')
{
	$smarty->assign('data_jadwal_ppmb_by_id', $jadwal_ppmb->load_jadwal_ppmb_by_id(get('id_jadwal')));
}

if ($mode == 'aktifkan')
{
	$jadwal = $jadwal_ppmb->load_jadwal_ppmb_by_id(get('id_jadwal'));
	$smarty->assign('no_awal', $jadwal['NOMER_AWAL']);
	$smarty->assign('data_jadwal_ppmb_by_id', $jadwal);
}

if ($mode == 'non-aktifkan')
{
	$smarty->assign('jp', $jadwal_ppmb->load_jadwal_ppmb_by_id(get('id_jadwal')));
}

if ($mode == 'pengawas')
{
	if (post('mode') == 'add-pengawas')
	{
		$db->Parse("
			insert into pengawas_ujian_ppmb
			( id_jadwal_ppmb, pengawas1, status1, pengawas2, status2, pengawas3, status3, pengawas4, status4, nomer_awal, nomer_akhir, amplop1, amplop2, distributor, koordinator) values
			(:id_jadwal_ppmb,:pengawas1,:status1,:pengawas2,:status2,:pengawas3,:status3,:pengawas4,:status4,:nomer_awal,:nomer_akhir,:amplop1,:amplop2,:distributor,:koordinator)");
		$db->BindByName(':id_jadwal_ppmb', post('id_jadwal_ppmb'));
		$db->BindByName(':pengawas1', post('pengawas1'));
		$db->BindByName(':status1', post('status1'));
		$db->BindByName(':pengawas2', post('pengawas2'));
		$db->BindByName(':status2', post('status2'));
		$db->BindByName(':pengawas3', post('pengawas3'));
		$db->BindByName(':status3', post('status3'));
		$db->BindByName(':pengawas4', post('pengawas4'));
		$db->BindByName(':status4', post('status4'));
		$db->BindByName(':nomer_awal', post('nomer_awal'));
		$db->BindByName(':nomer_akhir', post('nomer_akhir'));
		$db->BindByName(':amplop1', post('amplop1'));
		$db->BindByName(':amplop2', post('amplop2'));
		$db->BindByName(':distributor', post('distributor'));
		$db->BindByName(':koordinator', post('koordinator'));
		$result = $db->Execute();

		if (!$result)
		{
			print_r(error_get_last());
			exit();
		}
	}

	$id_jadwal_ppmb = get('id_jadwal');

	$db->Query("select * from jadwal_ppmb where id_jadwal_ppmb = {$id_jadwal_ppmb}");
	$row = $db->FetchAssoc();
	$smarty->assign('jadwal_ppmb', $row);

	$smarty->assign('pengawas_ujian_set', $db->QueryToArray("select * from pengawas_ujian_ppmb where id_jadwal_ppmb = {$id_jadwal_ppmb} order by nomer_awal"));
}
else if ($mode == 'add-pengawas')
{
	$id_jadwal_ppmb = get('id_jadwal');

	$db->Query("select * from jadwal_ppmb where id_jadwal_ppmb = {$id_jadwal_ppmb}");
	$row = $db->FetchAssoc();
	$smarty->assign('jadwal_ppmb', $row);
}
else if ($mode == 'edit-pengawas')
{
	if (post('mode') == 'edit-pengawas')
	{
		$id_pengawas = post('id_pengawas');

		$result = $db->Query("
			update pengawas_ujian_ppmb set 
				distributor = '" . post('distributor') . "', koordinator = '" . post('koordinator') . "',
				pengawas1 = '" . post('pengawas1') . "', status1 = '" . post('status1') . "',
				pengawas2 = '" . post('pengawas2') . "', status2 = '" . post('status2') . "',
				pengawas3 = '" . post('pengawas3') . "', status3 = '" . post('status3') . "',
				pengawas4 = '" . post('pengawas4') . "', status4 = '" . post('status4') . "',
				nomer_awal = '" . post('nomer_awal') . "', nomer_akhir = '" . post('nomer_akhir') . "',
				amplop1 = '" . post('amplop1') . "', amplop2 = '" . post('amplop2') . "'
			where id_pengawas_ujian_ppmb = {$id_pengawas}");

		if ($result)
			$smarty->assign('edited', 'Berhasil di simpan');
		else
			$smarty->assign('edited', 'gagal di simpan');
	}

	$id_pengawas = get('id_pengawas');

	$db->Query("select * from pengawas_ujian_ppmb where id_pengawas_ujian_ppmb = {$id_pengawas}");
	$row = $db->FetchAssoc();
	$smarty->assign('pu', $row);

	$db->Query("select * from jadwal_ppmb where id_jadwal_ppmb = {$row['ID_JADWAL_PPMB']}");
	$row = $db->FetchAssoc();
	$smarty->assign('jadwal_ppmb', $row);
}
else if ($mode == 'del-pengawas')
{
	$id_pengawas = get('id_pengawas');
	$db->Query("
		select id_penerimaan, id_jadwal_ppmb from jadwal_ppmb where id_jadwal_ppmb in (
		select id_jadwal_ppmb from pengawas_ujian_ppmb where id_pengawas_ujian_ppmb = {$id_pengawas})");
	$row = $db->FetchAssoc();
	$smarty->assign('pu', $row);

	$db->Query("delete from pengawas_ujian_ppmb where id_pengawas_ujian_ppmb = {$id_pengawas}");
}

$smarty->assign('data_penerimaan', $jadwal_ppmb->load_penerimaan());
$smarty->display("verifikasi/ruang/{$mode}.tpl");
?>
