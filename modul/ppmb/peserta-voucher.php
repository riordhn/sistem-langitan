<?php
include('config.php');
include 'includes/function.php';

require_once 'class/Peserta.class.php';

$mode = get('mode', 'view');

$id_pt = $id_pt_user;

$peserta = new Peserta($db);

$depan = time();
$belakang = strrev(time());

if ($request_method == 'POST')
{
	if (post('mode') == 'bayar')
	{
		$kode_voucher = post('kode_voucher');
		$id_voucher = post('id_voucher');

		/*$db->Query("update voucher set tgl_bayar = SYSDATE 
						where kode_voucher = '{$kode_voucher}' 
						and exists (select 1 from penerimaan p 
													where voucher.id_penerimaan = p.id_penerimaan and p.id_perguruan_tinggi = '{$id_pt_user}')");*/

		$db->Query("update voucher set tgl_bayar = SYSDATE where id_voucher = '{$id_voucher}'");

	}

	if (post('mode') == 'ganti')
	{
		$kode_voucher = post('kode_voucher');
		$kode_jurusan = post('kode_jurusan');

		$db->Query("update voucher set kode_jurusan = '{$kode_jurusan}' where kode_voucher = '{$kode_voucher}'");
	}
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    $kode_voucher = get('kode_voucher');
    
    $db->Parse("
		SELECT
			v.id_voucher, v.kode_voucher, v.pin_voucher, bank.nm_bank, nama_bank_via, TGL_AMBIL, tgl_bayar, v.kode_jurusan,
			cmb.nm_c_mhs, p.tahun
		FROM voucher v
		JOIN penerimaan p on p.id_penerimaan = v.id_penerimaan and p.id_perguruan_tinggi = '{$id_pt}'
		LEFT JOIN bank ON bank.ID_BANK = v.id_bank
		LEFT JOIN BANK_VIA ON BANK_VIA.ID_bank_via = v.id_bank_via
		LEFT JOIN calon_mahasiswa_baru cmb on cmb.kode_voucher = v.kode_voucher
		WHERE v.kode_voucher = :kode_voucher");
    $db->BindByName(':kode_voucher', $kode_voucher);
	$db->Execute();
    
    $smarty->assign('voucher', $db->FetchAssoc());

	    $bayar1 = (int) get('bayar1');
	    $bayar2 = (int) get('bayar2');
	    $bayar3 = (int) get('bayar3');
	    $bayar4 = (int) get('bayar4');
	    $bayar5 = (int) get('bayar5');
	    $bayar6 = (int) get('bayar6');
	    $bayar7 = (int) get('bayar7');
	    $bayar8 = (int) get('bayar8');
	    $total_bayar = (int) $bayar1+$bayar2+$bayar3+$bayar4+$bayar5+$bayar6+$bayar7+$bayar8;
	    //$terbilang = get('terbilang');
	    $terbilang = $peserta->getTerbilang($total_bayar)." rupiah";
	    $no_kwitansi = get('no_kwitansi');
	    $is_tunai = get('is_tunai');
	    $tgl_transfer = get('tgl_transfer');
	    $ukuran_jas = get('ukuran_jas');
	    $tgl_tes_tulis = get('tgl_tes_tulis');

	    $smarty->assign('bayar1', number_format($bayar1));
	    $smarty->assign('bayar2', number_format($bayar2));
	    $smarty->assign('bayar3', number_format($bayar3));
	    $smarty->assign('bayar4', number_format($bayar4));
	    $smarty->assign('bayar5', number_format($bayar5));
	    $smarty->assign('bayar6', number_format($bayar6));
	    $smarty->assign('bayar7', number_format($bayar7));
	    $smarty->assign('bayar8', number_format($bayar8));
	    $smarty->assign('total_bayar', number_format($total_bayar));
	    $smarty->assign('terbilang', $terbilang);
	    $smarty->assign('no_kwitansi', $no_kwitansi);
	    $smarty->assign('is_tunai', $is_tunai);
	    $smarty->assign('tgl_transfer', $tgl_transfer);
	    $smarty->assign('ukuran_jas', $ukuran_jas);
	    $smarty->assign('tgl_tes_tulis', $tgl_tes_tulis);

    $link = 'pdf/kwitansi-pdf.php?' . paramEncrypt($depan . '=' . $belakang . $depan . '&kode_voucher=' . $kode_voucher . '&' . $belakang . '=' . $depan . $belakang . '&bayar1=' . $bayar1 . '&bayar2=' . $bayar2 . '&bayar3=' . $bayar3 . '&bayar4=' . $bayar4 . '&bayar5=' . $bayar5 . '&bayar6=' . $bayar6 . '&bayar7=' . $bayar7 . '&bayar8=' . $bayar8 . '&total_bayar=' . $total_bayar . '&terbilang=' . $terbilang . '&no_kwitansi=' . $no_kwitansi . '&is_tunai=' . $is_tunai . '&tgl_transfer=' . $tgl_transfer . '&ukuran_jas=' . $ukuran_jas . '&tgl_tes_tulis=' . $tgl_tes_tulis ) . '';
	$smarty->assign('LINK', $link);
}

$smarty->display("peserta/voucher/{$mode}.tpl");
?>
