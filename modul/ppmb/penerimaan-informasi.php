<?php
include 'config.php';

$mode = get('mode', 'view');

$id_pt = $id_pt_user;

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	if (post('mode') == 'add')
	{
		$db->Query("SELECT ID_INFORMASI_PMB FROM INFORMASI_PMB WHERE ID_PERGURUAN_TINGGI = '{$id_pt}' AND IS_AKTIF = '1'");
		$maxID = $db->FetchAssoc();

		if(!empty($maxID['ID_INFORMASI_PMB'])){
			$db->Query("UPDATE INFORMASI_PMB SET IS_AKTIF = '0' WHERE ID_INFORMASI_PMB = '{$maxID['ID_INFORMASI_PMB']}'");
		}

		$db->Parse("INSERT INTO INFORMASI_PMB (ISI_INFORMASI, ID_PERGURUAN_TINGGI, ID_PENGGUNA, IS_AKTIF) VALUES (:1, :2, :3, :4)");
		$db->BindByName(':1', post('editor1'));
		$db->BindByName(':2', $id_pt);
		$db->BindByName(':3', $user->ID_PENGGUNA);
		$db->BindByName(':4', $x="1");
		
		$result = $db->Execute();
		
		if ($result)
			$smarty->assign('message', 'Data berhasil ditambahkan');
		else
			$smarty->assign('message', 'Data gagal ditambahkan');
	}
	 
}

if ($_SERVER['REQUEST_METHOD'] == 'GET' || $_SERVER['REQUEST_METHOD'] == 'POST')
{
	if ($mode == 'view')
	{
		$db->Query("SELECT ISI_INFORMASI FROM INFORMASI_PMB WHERE ID_PERGURUAN_TINGGI = '{$id_pt}' AND IS_AKTIF = '1'");
		$isi_informasi = $db->FetchAssoc();

		$smarty->assign('isi_informasi', $isi_informasi['ISI_INFORMASI']);
	}
	
}

$smarty->display("penerimaan/informasi/{$mode}.tpl");