<?php
include('config.php');

if (!in_array($user->ID_JABATAN_PEGAWAI, array(175, 176, 203, 678)))
{
    // 203 = Admin
    echo "Anda tidak punya hak akses halaman ini.";
    exit();
}

if ($request_method == 'POST')
{
    $id_c_mhs = post('id_c_mhs');
    
    $db->BeginTransaction();
    
    $result = $db->Query("
        update calon_mahasiswa_baru set
            id_pilihan_1 = null, id_pilihan_2 = null, id_pilihan_3 = null, id_pilihan_4 = null, id_kelas_pilihan = null
        where id_c_mhs = {$id_c_mhs}");
    if (!$result)
        echo "GAGAL 1";
        
    $result = $db->Query("
        update calon_mahasiswa_pasca set
            kelas_pilihan = null, id_prodi_minat = null
        where id_c_mhs = {$id_c_mhs}");
    if (!$result)
        echo "GAGAL 2";
    
    $result = $db->Commit();
    if (!$result)
        echo "GAGAL 3";
}

if ($request_method == 'GET' or $request_method == 'POST')
{   
    $db->Parse("
        select
            id_c_mhs, nm_c_mhs, kode_voucher,
            ps1.nm_program_studi as nm_program_studi_1,
            ps2.nm_program_studi as nm_program_studi_2,
            ps3.nm_program_studi as nm_program_studi_3,
            ps4.nm_program_studi as nm_program_studi_4,
            tgl_verifikasi_ppmb, p.id_jenjang
        from calon_mahasiswa_baru cmb
        left join program_studi ps1 on ps1.id_program_studi = cmb.id_pilihan_1
        left join program_studi ps2 on ps2.id_program_studi = cmb.id_pilihan_2
        left join program_studi ps3 on ps3.id_program_studi = cmb.id_pilihan_3
        left join program_studi ps4 on ps4.id_program_studi = cmb.id_pilihan_4
        join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
        where kode_voucher = :kode_voucher");
    $db->BindByName(':kode_voucher', get('kode_voucher', ''));
    $db->Execute();
    
    $smarty->assign('cmb', $db->FetchAssoc());
}

$smarty->display("peserta/rpilihan/view.tpl");
?>