<?php
include 'config.php';
include 'class/Penetapan.class.php';

$mode = get('mode', 'view');
$Penetapan = new Penetapan($db);

if ($request_method == 'GET')
{
    if ($mode == 'view')
    {	
		$id_penetapan = get('id_penetapan', '');
		$term = strtoupper(get('q', ''));
		
        $smarty->assign('penetapan_set', $Penetapan->GetListPenetapanByTahun());
		$skor_tulis = "0.8";
		$skor_bl = "0.2";
		
		if ($id_penetapan != '' && $term != '' && strlen($term) >= 3)
		{			
			$data_set = $db->QueryToArray("
				select * from (
					select 
						posisi.*, row_number() over (partition by id_pilihan order by total_nilai desc, nilai_tpa desc, nilai_prestasi desc) as urutan
					from (
						select
							cmb.id_c_mhs, cmb.no_ujian, nm_c_mhs, cmp.id_pilihan, (((n.nilai_tpa + n.nilai_prestasi)*{$skor_tulis}) + (n.nilai_bl*{$skor_bl})) as total_nilai,
							ps1.nm_program_studi as nm_pilihan, n.nilai_tpa, n.nilai_prestasi
						from calon_mahasiswa_baru cmb
						join nilai_cmhs n on n.id_c_mhs = cmb.id_c_mhs
						join calon_mahasiswa_pilihan cmp on cmp.id_c_mhs = cmb.id_c_mhs and cmp.jurusan_sekolah = n.jurusan_sekolah
						left join program_studi ps on ps.id_program_studi = cmb.id_program_studi
						left join program_studi ps1 on ps1.id_program_studi = cmp.id_pilihan
						where
							cmb.tgl_verifikasi_ppmb is not null and 
							cmb.id_penerimaan in (select id_penerimaan from penetapan_penerimaan where id_penetapan = {$id_penetapan}) and
							cmp.pilihan_ke = 1
					) posisi
				) searching 
				where nm_c_mhs like '%{$term}%' or no_ujian like '{$term}'");
				
			$smarty->assign('data_set', $data_set);
		}
    }
}

$smarty->display("penetapan/peserta/{$mode}.tpl");
?>
