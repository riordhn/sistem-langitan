<?php
include('../../../config.php');
$smarty->template_dir = '../gui';
$smarty->compile_dir = '../gui_c';

$mode		= get('mode', 'view');
$password	= 'pusatpenerimaan';

if ($request_method == 'POST')
{
	if ($mode == 'login')
	{
		if (md5(post('password')) == md5($password))
		{
			$_SESSION['ppmb_rekap'] = $password;
			header("location: index.php");
			exit();
		}
	}
	
	if ($mode == 'logout')
	{
		unset($_SESSION['ppmb_rekap']);
		header("location: index.php");
		exit();
	}
}

if (empty($_SESSION['ppmb_rekap']))
{
	$smarty->display('rekap/login.tpl');
	exit();
}
else
{
	include '../class/Report.class.php';
	$report = new Report($db);
	
	$id_penerimaan		= get('id_penerimaan', '');
	$id_program_studi	= get('id_program_studi', '');
		
	if ($_SESSION['ppmb_rekap'] != $password)
	{
		unset($_SESSION['ppmb_rekap']);
		header("location: index.php");
		exit();
	}
	
	if ($mode == 'view')
	{
		$penerimaan_set = $report->GetReportPenerimaanAktifList();
		$smarty->assign('penerimaan_set', $penerimaan_set);

		$voucher_set = $report->GetVoucherAktifList();
		$smarty->assign('voucher_set', $voucher_set);
		$smarty->assign('show_voucher', $true = FALSE);
	}

	if ($mode == 'prodi' && is_numeric($id_penerimaan))
	{
		$db->Query("select * from penerimaan where id_penerimaan = {$id_penerimaan}");
		$penerimaan = $db->FetchAssoc();
		$smarty->assignByRef('penerimaan', $penerimaan);

		$program_studi_set = $report->GetPerProdiList($id_penerimaan);
		$smarty->assignByRef('program_studi_set', $program_studi_set);
	}
	
	if ($mode == 'submit_form' && is_numeric($id_penerimaan) && is_numeric($id_program_studi))
	{
		$db->Query("select * from penerimaan where id_penerimaan = {$id_penerimaan}");
		$smarty->assignByRef('penerimaan', $db->FetchAssoc());
		
		$smarty->assignByRef('ps', $report->GetInfoProgramStudi($id_program_studi));
		
		$cmb_set = $report->GetCMBSubmitFormList($id_penerimaan, $id_program_studi);
		$smarty->assignByRef('cmb_set', $cmb_set);
	}
	
	if ($mode == 'antri_verifikasi' && is_numeric($id_penerimaan) && is_numeric($id_program_studi))
	{
		$db->Query("select * from penerimaan where id_penerimaan = {$id_penerimaan}");
		$smarty->assignByRef('penerimaan', $db->FetchAssoc());
		
		$smarty->assignByRef('ps', $report->GetInfoProgramStudi($id_program_studi));
		
		$cmb_set = $report->GetCMBAntriVerifikasiList($id_penerimaan, $id_program_studi);
		$smarty->assignByRef('cmb_set', $cmb_set);
		
		$data_set = $report->GetDataAntriVerifikasiList($id_penerimaan, $id_program_studi);
		$smarty->assignByRef('data_set', $data_set);
		
		$syarat_umum_set = $report->GetSyaratUmumPenerimaan($id_penerimaan);
		$smarty->assignByRef('syarat_umum_set', $syarat_umum_set);
		
		$syarat_prodi_set = $report->GetSyaratProdiPenerimaan($id_penerimaan, $id_program_studi);
		$smarty->assignByRef('syarat_prodi_set', $syarat_prodi_set);
	}
	
	if ($mode == 'verifikasi_kembali' && is_numeric($id_penerimaan) && is_numeric($id_program_studi))
	{
		$db->Query("select * from penerimaan where id_penerimaan = {$id_penerimaan}");
		$smarty->assignByRef('penerimaan', $db->FetchAssoc());
		
		$smarty->assignByRef('ps', $report->GetInfoProgramStudi($id_program_studi));
		
		$cmb_set = $report->GetCMBVerifikasiKembaliList($id_penerimaan, $id_program_studi);
		$smarty->assignByRef('cmb_set', $cmb_set);
		
		$data_set = $report->GetDataVerifikasiKembaliList($id_penerimaan, $id_program_studi);
		$smarty->assignByRef('data_set', $data_set);
		
		$syarat_umum_set = $report->GetSyaratUmumPenerimaan($id_penerimaan);
		$smarty->assignByRef('syarat_umum_set', $syarat_umum_set);
		
		$syarat_prodi_set = $report->GetSyaratProdiPenerimaan($id_penerimaan, $id_program_studi);
		$smarty->assignByRef('syarat_prodi_set', $syarat_prodi_set);
	}
	
	if ($mode == 'verifikasi' && is_numeric($id_penerimaan) && is_numeric($id_program_studi))
	{
		$db->Query("select * from penerimaan where id_penerimaan = {$id_penerimaan}");
		$smarty->assignByRef('penerimaan', $db->FetchAssoc());
		
		$smarty->assignByRef('ps', $report->GetInfoProgramStudi($id_program_studi));
		
		$cmb_set = $report->GetCMBVerifikasiList($id_penerimaan, $id_program_studi);
		$smarty->assignByRef('cmb_set', $cmb_set);
		
		$data_set = $report->GetDataVerifikasiList($id_penerimaan, $id_program_studi);
		$smarty->assignByRef('data_set', $data_set);
		
		$syarat_umum_set = $report->GetSyaratUmumPenerimaan($id_penerimaan);
		$smarty->assignByRef('syarat_umum_set', $syarat_umum_set);
		
		$syarat_prodi_set = $report->GetSyaratProdiPenerimaan($id_penerimaan, $id_program_studi);
		$smarty->assignByRef('syarat_prodi_set', $syarat_prodi_set);
	}
	
	if ($mode == 'bayar' && is_numeric($id_penerimaan) && is_numeric($id_program_studi))
	{
		$db->Query("select * from penerimaan where id_penerimaan = {$id_penerimaan}");
		$smarty->assignByRef('penerimaan', $db->FetchAssoc());
		
		$smarty->assignByRef('ps', $report->GetInfoProgramStudi($id_program_studi));
		
		$smarty->assignByRef('cmb_set', $report->GetCMBBayarList($id_penerimaan, $id_program_studi));
	}

	if ($mode == 'detail' && is_numeric($id_penerimaan))
	{
		$db->Query("select * from penerimaan where id_penerimaan = {$id_penerimaan}");
		$row = $db->FetchAssoc();
		
		$smarty->assign('penerimaan', $row);

		if ($row['SEMESTER'] != '-')
			$smarty->assign('nm_penerimaan', "{$row['NM_PENERIMAAN']} Gel. {$row['GELOMBANG']} Semester {$row['SEMESTER']} Tahun {$row['TAHUN']}");
		else
			$smarty->assign('nm_penerimaan', "{$row['NM_PENERIMAAN']} Gel. {$row['GELOMBANG']} Tahun {$row['TAHUN']}");

		$penerimaan_prodi_set = $db->QueryToArray("
			select pp.id_penerimaan, pp.id_program_studi, nm_program_studi from penerimaan_prodi pp
			join program_studi ps on ps.id_program_studi = pp.id_program_studi
			where pp.id_penerimaan = {$id_penerimaan} and pp.is_aktif = 1
			order by ps.id_fakultas");

		foreach ($penerimaan_prodi_set as &$pp)
		{
			$pp['cmb_set'] = $db->QueryToArray("
				select
					nm_c_mhs, gelar, telp, email, ptn_s1, prodi_s1, alamat,
					(select tipe_dati2||' '||nm_kota as nm_kota from kota k where k.id_kota = cmb.id_kota) as nm_kota,
					(select s.nm_sekolah from sekolah s where s.id_sekolah = cms.id_sekolah_asal) as nm_sekolah
				from calon_mahasiswa_baru cmb
				join calon_mahasiswa_pasca cmp on cmp.id_c_mhs = cmb.id_c_mhs
				join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
				where 
					cmb.id_penerimaan = {$pp['ID_PENERIMAAN']} and 
					cmb.id_pilihan_1 = {$pp['ID_PROGRAM_STUDI']}");
		}

		$smarty->assign('penerimaan_prodi_set', $penerimaan_prodi_set);
	}

	$smarty->display("rekap/{$mode}.tpl");
}
?>