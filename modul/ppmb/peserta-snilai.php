<?php
include 'config.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$id_c_mhs_1 = $_POST['id_c_mhs_1'];
	$id_c_mhs_2 = $_POST['id_c_mhs_2'];

	$db->BeginTransaction();

	$nilai1_set = $db->QueryToArray("select * from nilai_cmhs where id_c_mhs = {$id_c_mhs_1}");
	$nilai2_set = $db->QueryToArray("select * from nilai_cmhs where id_c_mhs = {$id_c_mhs_2}");

	// Hapus dulu data sebelumnya
	$db->Query("delete from nilai_cmhs where id_c_mhs = {$id_c_mhs_1}");
	$db->Query("delete from nilai_cmhs where id_c_mhs = {$id_c_mhs_2}");

	// re-insert data dengan membaliknya
	foreach ($nilai1_set as $nilai1)
	{
		$db->Query("
			insert into nilai_cmhs
			(id_c_mhs, nilai_tpa, nilai_prestasi, jurusan_sekolah, nilai_bl) values 
			({$id_c_mhs_2}, '{$nilai1['NILAI_TPA']}', '{$nilai1['NILAI_PRESTASI']}', '{$nilai1['JURUSAN_SEKOLAH']}', '{$nilai1['NILAI_BL']}')");
	}

	// re-insert data dengan membaliknya
	foreach ($nilai2_set as $nilai2)
	{
		$db->Query("
			insert into nilai_cmhs
			(id_c_mhs, nilai_tpa, nilai_prestasi, jurusan_sekolah, nilai_bl) values 
			({$id_c_mhs_1}, '{$nilai2['NILAI_TPA']}', '{$nilai2['NILAI_PRESTASI']}', '{$nilai2['JURUSAN_SEKOLAH']}', '{$nilai2['NILAI_BL']}')");
	}

	$db->Commit();
}

if ($_SERVER['REQUEST_METHOD'] == 'POST' or $_SERVER['REQUEST_METHOD'] == 'GET')
{
	$no_ujian_1 = trim(get('no_ujian_1', ''));
	$no_ujian_2 = trim(get('no_ujian_2', ''));

	if ($no_ujian_1 != '' && $no_ujian_2 != '')
	{
		$db->Query("select id_c_mhs, nm_c_mhs from calon_mahasiswa_baru where no_ujian = '{$no_ujian_1}'");
		$cmb1 = $db->FetchAssoc();
		$smarty->assignByRef('cmb1', $cmb1);

		$db->Query("select id_c_mhs, nm_c_mhs from calon_mahasiswa_baru where no_ujian = '{$no_ujian_2}'");
		$cmb2 = $db->FetchAssoc();
		$smarty->assignByRef('cmb2', $cmb2);

		$nilai1 = $db->QueryToArray("select * from nilai_cmhs where id_c_mhs = {$cmb1['ID_C_MHS']}");
		$smarty->assignByRef('nilai1', $nilai1);

		$nilai2 = $db->QueryToArray("select * from nilai_cmhs where id_c_mhs = {$cmb2['ID_C_MHS']}");
		$smarty->assignByRef('nilai2', $nilai2);
	}
}

$smarty->display('peserta/snilai/view.tpl');
?>