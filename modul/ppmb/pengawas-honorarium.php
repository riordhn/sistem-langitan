<?php
include 'config.php';
include 'class/Penerimaan.class.php';
include 'class/Pengawas.class.php';

$Penerimaan = new Penerimaan($db);
$Pengawas   = new Pengawas($db);

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    if (post('mode') == 'add')
    {
        $result = $Pengawas->AddHonorPengawas(post('id_ujian'), $_POST);
        
        if (!$result) { print_r(error_get_last()); exit(); }
        
        $smarty->assign('result', $result ? "Jenis honor berhasil ditambahkan" : "Jenis horor gagal ditambahkan");
    }
    
    if (post('mode') == 'delete')
    {
        $result = $Pengawas->DeleteHonorPengawas(post('id_honor'));
        echo $result ? "1" : "0";
        exit();
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        $smarty->assign('ujian_set', $Pengawas->GetListUjian());
        $smarty->assign('honor_set', $Pengawas->GetListHonorarium(get('id_ujian')));
    }
    
    if ($mode == 'add')
    {
        $smarty->assign('jabatan_set', $Pengawas->GetListJabatan());
    }
}

$smarty->display("pengawas/honorarium/{$mode}.tpl");
?>
