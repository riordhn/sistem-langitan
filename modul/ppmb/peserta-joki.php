<?php
include 'config.php';
include 'class/Penerimaan.class.php';

$mode = get('mode', 'view');
$Penerimaan = new Penerimaan($db);

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
}

if ($_SERVER['REQUEST_METHOD'] == 'GET' || $_SERVER['REQUEST_METHOD'] == 'POST')
{
	if ($mode == 'view')
	{

		$id_penerimaan = get('id_penerimaan', '');
		$smarty->assign('penerimaan_set', $Penerimaan->GetAllForView());

		if ($id_penerimaan != '')
		{
			$cmb_set = $db->QueryToArray(
				"SELECT
					cmb.id_c_mhs, cmb.no_ujian, nm_c_mhs, tgl_lahir, cmb.kode_jurusan,
					jp.lokasi, jp.nm_ruang,
					ps1.nm_program_studi as nm_program_studi_1,
					ps2.nm_program_studi as nm_program_studi_2,
					ps3.nm_program_studi as nm_program_studi_3,
					ps4.nm_program_studi as nm_program_studi_4,
					pp.nm_pengawas
				from calon_mahasiswa_baru cmb
				inner join plot_jadwal_ppmb pjp on pjp.id_c_mhs = cmb.id_c_mhs
				inner join jadwal_ppmb jp on jp.id_jadwal_ppmb = pjp.id_jadwal_ppmb
				left join program_studi ps1 on ps1.id_program_studi = cmb.id_pilihan_1
				left join program_studi ps2 on ps2.id_program_studi = cmb.id_pilihan_2
				left join program_studi ps3 on ps3.id_program_studi = cmb.id_pilihan_3
				left join program_studi ps4 on ps4.id_program_studi = cmb.id_pilihan_4
				inner join ppmb_koordinator_ruang pkr ON pkr.id_koordinator_ruang = jp.id_koordinator_ruang
				inner join ppmb_pengawas pp on pp.id_pengawas = pkr.id_pengawas
				where 
				  cmb.id_penerimaan = {$id_penerimaan} and 
				  lower(nm_c_mhs) in (
				    select lower(nm_c_mhs) from calon_mahasiswa_baru
				    where id_penerimaan = {$id_penerimaan} and no_ujian is not null
				    group by lower(nm_c_mhs) having count(id_c_mhs) > 1)
				order by lower(nm_c_mhs)");

			$smarty->assignByRef('cmb_set', $cmb_set);
		}
	}
}

$smarty->display("peserta/joki/{$mode}.tpl");