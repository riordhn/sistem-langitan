<?php

include 'config.php';
include 'class/Verifikasi.class.php';

$Verifikasi = new Verifikasi($db);
$mode = get('mode', 'view');

if ($request_method == 'POST')
{
	if (post('mode') == 'save_verifikasi')
	{
		$id_c_mhs = post('id_c_mhs');

		$db->Parse("
            select 
                cmb.id_c_mhs, cmb.no_ujian, p.id_jalur, id_pilihan_1, cmb.id_penerimaan,
                p.id_jenjang, p.no_ujian_awal, p.no_ujian_akhir, p.pengambilan_nomor
            from calon_mahasiswa_baru cmb
            join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
            where id_c_mhs = :id_c_mhs");
		$db->BindByName(':id_c_mhs', $id_c_mhs);
		$db->Execute();

		$cmb = $db->FetchAssoc();

		if (in_array($cmb['ID_JALUR'], array(6, 27, 23, 24, 32, 34)))  // pasca, profesi, spesialis
		{
			$where_kelas = '';

			// Pengaturan Kelas Khusus Magister
			if ($cmb['ID_PILIHAN_1'] == 55)
			{
				$where_kelas = "and substr(pjp.no_ujian,5,2) = '02'";
			}  // MIH - Ilmu Hukum
			if ($cmb['ID_PILIHAN_1'] == 52)
			{
				$where_kelas = "and substr(pjp.no_ujian,5,2) = '03'";
			}  // MHP - Sains Hukum Pembangunan
			if ($cmb['ID_PILIHAN_1'] == 54)
			{
				$where_kelas = "and substr(pjp.no_ujian,5,2) = '04'";
			}  // MKN - Magister Kenotariatan
			if ($cmb['ID_PILIHAN_1'] == 73)
			{
				$where_kelas = "and substr(pjp.no_ujian,5,2) = '05'";
			}  // MM - Magister Manajemen
			// Pengaturan Kelas Khusus profesi
			if ($cmb['ID_PILIHAN_1'] == 63)
			{
				$where_kelas = "and substr(pjp.no_ujian,5,2) = '01'";
			}  // Profesi Akuntansi
			if ($cmb['ID_PILIHAN_1'] == 83)
			{
				$where_kelas = "and substr(pjp.no_ujian,5,2) = '02'";
			}  // Profesi Dokter Hewan
			// Kelas Jakarta
			// if ($cmb['ID_JALUR'] == 35)
			// {
			// $where_kelas = "and substr(pjp.no_ujian,5,2) = '08'";  // All Prodi, sementara masih Ilmu hukum saja.
			// }
		}

		if ($cmb['PENGAMBILAN_NOMOR'] == 1)  // acak
			$order_by = "order by dbms_random.value";
		else if ($cmb['PENGAMBILAN_NOMOR'] == 2) // urut
			$order_by = "order by no_ujian";

		// mendapatkan jadwal dan nomer ujian
		$db->Query("
            select a.* from (
                select cmb.id_c_mhs, pjp.id_jadwal_ppmb, pjp.id_plot_jadwal_ppmb, pjp.no_ujian
                from plot_jadwal_ppmb pjp
                join jadwal_ppmb jp on jp.id_jadwal_ppmb = pjp.id_jadwal_ppmb
                join penerimaan p on p.id_penerimaan = jp.id_penerimaan
                join calon_mahasiswa_baru cmb on cmb.id_penerimaan = p.id_penerimaan
                join voucher v on (v.kode_voucher = cmb.kode_voucher and to_number(v.kode_jurusan) = jp.kode_jalur)
                where pjp.id_c_mhs is null and cmb.no_ujian is null and p.is_aktif = 1 and cmb.id_c_mhs = {$id_c_mhs} 
                {$where_kelas}
                {$order_by}
            ) a where rownum <= 1");
		$jadwal = $db->FetchAssoc();

		// untuk yg belum punya nomer ujian
		if ($cmb['NO_UJIAN'] == '' && !empty($jadwal))
		{
			$db->BeginTransaction();

			$db->Query("update calon_mahasiswa_baru set no_ujian = {$jadwal['NO_UJIAN']} where id_c_mhs = {$id_c_mhs}");
			$db->Query("update calon_mahasiswa_data set id_jadwal_ppmb = {$jadwal['ID_JADWAL_PPMB']} where id_c_mhs = {$id_c_mhs}");
			$db->Query("update plot_jadwal_ppmb set id_c_mhs = {$id_c_mhs} where id_plot_jadwal_ppmb = {$jadwal['ID_PLOT_JADWAL_PPMB']}");
			$db->Query("update jadwal_ppmb set terisi = terisi + 1 where id_jadwal_ppmb = {$jadwal['ID_JADWAL_PPMB']}");

			$db->Commit();
		}

		$db->BeginTransaction();

		// update data cmb
		$db->Parse("
            update calon_mahasiswa_baru set
                sp3_1 = :sp3_1,
                sp3_2 = :sp3_2,
                sp3_3 = :sp3_3,
                sp3_4 = :sp3_4,
                
                id_kelompok_biaya = :id_kelompok_biaya,
                is_matrikulasi = :is_matrikulasi,

                tgl_verifikasi_ppmb = :tgl_verifikasi_ppmb,
                id_verifikator_ppmb = :id_verifikator_ppmb
            where id_c_mhs = :id_c_mhs");
		$db->BindByName(':sp3_1', post('sp3_1'));
		$db->BindByName(':sp3_2', post('sp3_2'));
		$db->BindByName(':sp3_3', post('sp3_3'));
		$db->BindByName(':sp3_4', post('sp3_4'));
		$db->BindByName(':id_kelompok_biaya', post('id_kelompok_biaya'));
		$db->BindByName(':is_matrikulasi', post('is_matrikulasi'));
		$db->BindByName(':tgl_verifikasi_ppmb', date('d-M-Y'));
		$db->BindByName(':id_verifikator_ppmb', $user->ID_PENGGUNA);
		$db->BindByName(':id_c_mhs', $id_c_mhs);
		$db->Execute();

		// update data cmd
		$db->Parse("
            update calon_mahasiswa_data set
                berkas_ijazah = :berkas_ijazah,
                berkas_skhun = :berkas_skhun,
                berkas_kesanggupan_sp3 = :berkas_kesanggupan_sp3,
                berkas_lain = :berkas_lain,
                berkas_lain_ket = :berkas_lain_ket
            where id_c_mhs = :id_c_mhs");
		$db->BindByName(':berkas_ijazah', post('berkas_ijazah'));
		$db->BindByName(':berkas_skhun', $berkas_skhun = post('berkas_skhun') ? 1 : 0);
		$db->BindByName(':berkas_kesanggupan_sp3', $berkas_kesanggupan_sp3 = post('berkas_kesanggupan_sp3') ? 1 : 0);
		$db->BindByName(':berkas_lain', $berkas_lain = post('berkas_lain') ? 1 : 0);
		$db->BindByName(':berkas_lain_ket', post('berkas_lain_ket', ''));
		$db->BindByName(':id_c_mhs', $id_c_mhs);
		$db->Execute();


		// Mengupdate data-data syarat
		$db->Query("delete from calon_mahasiswa_syarat where id_c_mhs = {$id_c_mhs}");   // diclear-kan dulu
		foreach ($_POST as $key => $value)
		{
			if (strpos($key, 'syarat_prodi_') !== false)
			{
				$id_syarat_prodi = str_replace('syarat_prodi_', '', $key);
				$db->Query("insert into calon_mahasiswa_syarat (id_c_mhs, id_syarat_prodi) values ({$cmb['ID_C_MHS']}, {$id_syarat_prodi})");
			}
		}

		$db->Commit();
	}
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$kode_voucher = get('kode_voucher', '');

		$cmb = $Verifikasi->GetData($kode_voucher);

		$smarty->assign('cmb', $cmb);  // data calon_mahasiswa_baru
		// Pengecekan prodi minat saat form registrasi gagal simpan prodi minat
		$smarty->assign('jumlah_prodi_minat', $Verifikasi->GetJumlahProdiMinat($cmb['ID_PILIHAN_1'], $cmb['ID_C_MHS']));

		if (in_array($cmb['ID_JALUR'], array(41, 4, 5)))
		{
			if ($cmb['ID_JALUR'] == 41) // Kelompok Internasional Khusus
			{
				$id_kelompok_biaya = 239;  
			}
			else if ($cmb['ID_JALUR'] == 5) // D3
			{
				$id_kelompok_biaya = 1;
			}
			else if ($cmb['ID_JALUR'] == 4) // Alih Jenis
			{
				$id_kelompok_biaya = 1;
			}
			
			// Versi lama menggunakan SP3
			$smarty->assign('sp3_1_min', $Verifikasi->GetSp3Min($cmb['ID_SEMESTER'], $cmb['ID_JALUR'], $cmb['ID_PILIHAN_1'], $id_kelompok_biaya));
			$smarty->assign('sp3_2_min', $Verifikasi->GetSp3Min($cmb['ID_SEMESTER'], $cmb['ID_JALUR'], $cmb['ID_PILIHAN_2'], $id_kelompok_biaya));
			$smarty->assign('sp3_3_min', $Verifikasi->GetSp3Min($cmb['ID_SEMESTER'], $cmb['ID_JALUR'], $cmb['ID_PILIHAN_3'], $id_kelompok_biaya));
			$smarty->assign('sp3_4_min', $Verifikasi->GetSp3Min($cmb['ID_SEMESTER'], $cmb['ID_JALUR'], $cmb['ID_PILIHAN_4'], $id_kelompok_biaya));
		}
		else
		{
			// Versi baru menggunakan UKA
			$smarty->assign('sp3_1_min', $Verifikasi->GetUKAMin($cmb['ID_SEMESTER'], $cmb['ID_JALUR'], $cmb['ID_PILIHAN_1'], 1));
			$smarty->assign('sp3_2_min', $Verifikasi->GetUKAMin($cmb['ID_SEMESTER'], $cmb['ID_JALUR'], $cmb['ID_PILIHAN_2'], 1));
			$smarty->assign('sp3_3_min', $Verifikasi->GetUKAMin($cmb['ID_SEMESTER'], $cmb['ID_JALUR'], $cmb['ID_PILIHAN_3'], 1));
			$smarty->assign('sp3_4_min', $Verifikasi->GetUKAMin($cmb['ID_SEMESTER'], $cmb['ID_JALUR'], $cmb['ID_PILIHAN_4'], 1));
		}

		// mengumpulkan prodi pilihan
		$prodi_pilihan = array($cmb['ID_PILIHAN_1']);
		if ( ! empty($cmb['ID_PILIHAN_2'])) array_push($prodi_pilihan, $cmb['ID_PILIHAN_2']);
		if ( ! empty($cmb['ID_PILIHAN_3'])) array_push($prodi_pilihan, $cmb['ID_PILIHAN_3']);
		if ( ! empty($cmb['ID_PILIHAN_4'])) array_push($prodi_pilihan, $cmb['ID_PILIHAN_4']);
		$prodi_pilihan = implode(',', $prodi_pilihan);
		
		// Mendapatkan prasyarat-prasyarat
		$syarat_set = $db->QueryToArray("
			select psp.id_syarat_prodi, nm_syarat, status_wajib, psp.id_program_studi, ps.nm_program_studi, nvl2(cms.id_syarat_prodi, 1, 0) checked
			from penerimaan_syarat_prodi psp
			left join program_studi ps on ps.id_program_studi = psp.id_program_studi
			left join calon_mahasiswa_syarat cms on cms.id_c_mhs = {$cmb['ID_C_MHS']} and cms.id_syarat_prodi = psp.id_syarat_prodi
			where id_penerimaan = {$cmb['ID_PENERIMAAN']} and (psp.id_program_studi is null or psp.id_program_studi in ({$prodi_pilihan}))
			order by psp.id_program_studi desc, psp.urutan, psp.nm_syarat");
		$smarty->assignByRef('syarat_set', $syarat_set);

		// Jika jalur seperti yg ada di array
		if (in_array($cmb['ID_JALUR'], array(6, 23, 27, 24, 32, 34, 35, 41)))
		{
			$except_non_mandiri = "AND BK.ID_KELOMPOK_BIAYA NOT IN (99, 129)";
			// 47 = Kelas Jakarta
			// 99 = Reguler (Untuk non 2011)
			// 129 = Kelas Magister Ilmu Hukum - Hukum Bisnis (non 2011)

			if ($cmb['ID_PILIHAN_1'] == 85) // Vaksinologi dan Imunoterapetika
			{
				$except_non_mandiri = "AND BK.ID_KELOMPOK_BIAYA NOT IN (45, 99, 129)";
				// 45 == Alumni
			}

			if ($cmb['ID_PILIHAN_1'] == 73) // Magister Manajemen
			{
				$except_non_mandiri = "AND BK.ID_KELOMPOK_BIAYA NOT IN (1)";
				// 45 == Alumni
			}

			$kelompok_biaya_set = $db->QueryToArray("
				select bk.id_kelompok_biaya, kb.nm_kelompok_biaya from calon_mahasiswa_baru cmb
				join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
				join biaya_kuliah bk on bk.id_program_studi = cmb.id_pilihan_1 and bk.id_jalur = p.id_jalur
				join kelompok_biaya kb on kb.id_kelompok_biaya = bk.id_kelompok_biaya
				join semester s on
				  s.thn_akademik_semester = p.tahun and
				  substr(s.nm_semester,1,2) = substr(p.semester,1,2) and
				  s.tipe_semester = 'REG' and
				  s.id_semester = bk.id_semester
				where cmb.kode_voucher = '{$kode_voucher}'
				{$except_non_mandiri}
				group by bk.id_kelompok_biaya, kb.nm_kelompok_biaya
				order by kb.nm_kelompok_biaya");
			$smarty->assign('kelompok_biaya_set', $kelompok_biaya_set);

			$smarty->assign('matrikulasi_set', array(
				array('VALUE' => '1', 'VIEW' => 'Ya'),
				array('VALUE' => '0', 'VIEW' => 'Tidak')
			));
		}
	}
	
	if ($mode == 'validasi-sp3')
	{
		return 'false';
	}
	
	if ($mode == 'get-sp3')
	{
		$id_program_studi	= get('id_program_studi');
		$id_jalur		= get('id_jalur');
		$id_semester		= get('id_semester');
		$id_kelompok_biaya	= get('id_kelompok_biaya');
		
		if (in_array($id_jalur, array(41, 4, 5)))
		{
			$nama_biaya = 'SP3';
			$id_kelompok_biaya = '239';  // di ganti ke kelompok khusus

			if ($id_jalur == 4 || $id_jalur == 5) // D3 dan Alih Jenis
				$id_kelompok_biaya = 1;
		}
		else
		{
			$nama_biaya = 'UKA';
		}

		$db->Query("
			SELECT DB.BESAR_BIAYA FROM DETAIL_BIAYA DB
			LEFT JOIN BIAYA_KULIAH BK ON BK.ID_BIAYA_KULIAH = DB.ID_BIAYA_KULIAH
			LEFT JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
			WHERE B.NM_BIAYA = '{$nama_biaya}' AND BK.ID_SEMESTER = {$id_semester} AND BK.ID_JALUR = {$id_jalur}
			AND BK.ID_KELOMPOK_BIAYA = {$id_kelompok_biaya} AND BK.ID_PROGRAM_STUDI = {$id_program_studi}");
		$row = $db->FetchAssoc();

		if ($row)
		{
			echo $row['BESAR_BIAYA'];
		}
		else
		{
			echo "0";
		}
	}
}

$smarty->display("verifikasi/berkas/{$mode}.tpl");
?>
