<?php
include('config.php');

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    $id_c_mhs = post('id_c_mhs');
    
    $db->Parse("
        update nilai_cmhs_pasca set
            nilai_tpa = :nilai_tpa,
            nilai_inggris = :nilai_inggris,
            nilai_ilmu = :nilai_ilmu,
            nilai_wawancara = :nilai_wawancara,
            nilai_ipk = :nilai_ipk,
            nilai_karya_ilmiah = :nilai_karya_ilmiah,
            nilai_rekomendasi = :nilai_rekomendasi,
            nilai_matrikulasi = :nilai_matrikulasi,
            nilai_psiko = :nilai_psiko,
            nilai_tulis = :nilai_tulis,
            nilai_lain = :nilai_lain,
			nilai_gab_ppds = :nilai_gab_ppds,
			total_nilai = :nilai_tpa + :nilai_inggris + :nilai_ilmu + :nilai_wawancara + :nilai_ipk +
				:nilai_karya_ilmiah + :nilai_rekomendasi + :nilai_matrikulasi + :nilai_psiko + :nilai_tulis + :nilai_lain + :nilai_gab_ppds
        where id_c_mhs = {$id_c_mhs}");
    // total_nilai = :nilai_tpa + :nilai_inggris + :nilai_ilmu + :nilai_wawancara + :nilai_ipk + :nilai_karya_ilmiah + :nilai_karya_ilmiah + :nilai_matrikulasi
    $db->BindByName(':nilai_tpa', post('nilai_tpa'));
    $db->BindByName(':nilai_inggris', post('nilai_inggris'));
    $db->BindByName(':nilai_ilmu', post('nilai_ilmu'));
    $db->BindByName(':nilai_wawancara', post('nilai_wawancara'));
    $db->BindByName(':nilai_ipk', post('nilai_ipk'));
    $db->BindByName(':nilai_karya_ilmiah', post('nilai_karya_ilmiah'));
    $db->BindByName(':nilai_rekomendasi', post('nilai_rekomendasi'));
    $db->BindByName(':nilai_matrikulasi', post('nilai_matrikulasi'));
    $db->BindByName(':nilai_psiko', post('nilai_psiko'));
    $db->BindByName(':nilai_tulis', post('nilai_tulis'));
    $db->BindByName(':nilai_lain', post('nilai_lain'));
	$db->BindByName(':nilai_gab_ppds', post('nilai_gab_ppds'));
    //$db->BindByName(':total_nilai', post('nilai_tpa', 0) + post('nilai_inggris', 0) + post('nilai_ilmu', 0) + post('nilai_wawancara', 0) + post('nilai_ipk', 0) + post('nilai_karya_ilmiah', 0) + post('nilai_rekomendasi', 0) + post('nilai_matrikulasi', 0));
    $result = $db->Execute();
    
    if ($result)
        $smarty->assign('edited', 'Data berhasil disimpan');
    else
        $smarty->assign('edited', 'Data gagal disimpan');
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    $no_ujian = get('no_ujian');
    
    $db->Query("
        select cmb.nm_c_mhs, n.* from calon_mahasiswa_baru cmb
        join nilai_cmhs_pasca n on n.id_c_mhs = cmb.id_c_mhs
        where cmb.no_ujian = '{$no_ujian}'");
    $row = $db->FetchAssoc();
    $smarty->assign('cmb', $row);
}

$smarty->display("penilaian/edit/{$mode}.tpl");
?>
