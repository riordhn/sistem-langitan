<?php
include('../../config.php');
include_once('../../tcpdf/config/lang/ind.php');
include_once('../../tcpdf/tcpdf.php');
include_once('cetak.class.php');

$cetak = new cetak($db);

$row_per_page = 20;

if (empty($_GET['id_penetapan_penerimaan']) ) {
    $data = $cetak->get_hasil_penetapan();
} 
else {
    $data = $cetak->get_hasil_penetapan($_GET['id_penetapan_penerimaan']);
}

class MYPDF extends TCPDF {

    // Page footer
    public function Footer() {
        // isi footer
        $footer = <<<EOF
<style>
    .header { font-size: 22pt; font-family: times; font-weight: bold; text-align:center;}
    .title { font-size: 13pt; font-family: serif; margin-top: 0px ;text-align:center; }
    th {background-color:#000;color:white;}
    tr.odd {background-color:#dddddd;}
    td.list {border-bottom:0.5px black solid;}
    td { font-size: 10pt; }
</style>
<table>
        <tr>
            <th colspan="5" align="center">Halaman {hal}</th>
        </tr>
        <tr>
            <td colspan="5" align="center">
                <strong>Kampus C Mulyorejo Surabaya Telp. (031) 5914042, 5912546, 5912564 Fax. (031) 5981941<br/>Website : http://www.unair.ac.d ; e-mail : sekretaris_ua@unair.ac.id<br/></strong>
                <span>&copy; - 2011 PPMB Universitas Airlangga.</span>
            </td>
        </tr>
    </table>
EOF;
        $footer = str_replace('{hal}', $this->getAliasNumPage(), $footer);
        // Position at 15 mm from bottom
        $this->SetY(-28);
        // Set font
        $this->SetFont('helvetica', '', 8);
        // Page number
        $this->Cell(0, 10, $this->writeHTML($footer), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }

}

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');

$pdf->setPrintHeader(false);

//Halaman Pertama
$pdf->AddPage();

$html = <<<EOF
<style>
    .header { font-size: 22pt; font-family: times; font-weight: bold; text-align:center;}
    .title { font-size: 13pt; font-family: serif; margin-top: 0px ;text-align:center; }
    th {background-color:#000;color:white;}
    tr.odd {background-color:#dddddd;}
    td.list {border-bottom: none;}
    td { font-size: 10pt; }
    td.prodi { font-size: 9pt; border-bottom: none;}
</style>
<table width="100%">
    <tr>
        <td width="100%" align="center">
            <img src="../../img/maba/logo_unair.gif" width="60px" height="60px"/><br/>
            <span class="header">UNIVERSITAS AIRLANGGA</span>
        </td>
    </tr>
    <tr>
        <td width="100%" align="center">
            <span class="title"><br/><br/>LAMPIRAN<br/>KEPUTUSAN REKTOR UNIVERSITAS AIRLANGGA<br/></span>
            Nomor : {nomor_surat}<br/><br/>
            <span class="title">TENTANG</span>
            <br/><br/>
            <span class="title">PENERIMAAN CALON MAHASISWA BARU<br/>PROGRAM MAGISTER, PROGAM DOKTOR, PROGRAM PROFESI GELOMBANG III DAN PROGRAM PENDIDIKAN DOKTER GIGI SPESIALIS GELOMBANG II<br/>SEMESTER GASAL TAHUN AKADEMIK 2011/2012<br/>UNIVERSITAS AIRLANGGA</span>
        </td>
    </tr>
</table>
<br/>
<table cellpadding="3">
    <tr>
        <th width="7%" align="center">No</th>
        <th width="13%">No.Ujian</th>
        <th width="32%">Nama Peserta</th>
        <th width="48%">Program Studi</th>
    </tr>
    {data_calon_mahasiswa}
    {ttd}
</table>
EOF;
$index_terakhir = 0;
$data_mahasiswa = '';
for ($i = 0; $i < $row_per_page; $i++) {
    if (isset($data[$i])) {
        $data_mahasiswa .= $i % 2 == 0 ? '<tr class="odd">' : '<tr>';
        $data_mahasiswa .='
            <td class="list" align="center">' . ($i + 1) . '</td>
            <td class="list">' . $data[$i]['NO_UJIAN'] . '</td>
            <td class="list">' . $cetak->compress_nama($data[$i]['NM_C_MHS']) . '</td>
            <td class="list prodi">' . $data[$i]['NM_PROGRAM_STUDI'] . '</td>
        </tr>
        ';
        $index_terakhir = $i;
    }
}
if ($_GET['nomor_surat'] != '') {
    $html = str_replace('{nomor_surat}', $_GET['nomor_surat'], $html);
} else {
    $html = str_replace('{nomor_surat}', '894/H3/KR/2011', $html);
}

$ttd = '<p></p><p></p><tr><td width="70%" colspan="2"></td><td width="30%" colspan="2" align="center">Surabaya, ' . strftime('%d %B %Y') . '<br/>Rektor<br/><br/><br/><br/>Fasich<br/>NIP.194612311974121001</td></tr>';
if ($index_terakhir < ($row_per_page - 1)) {
    $html = str_replace('{ttd}', $ttd, $html);
}else{
    $html = str_replace('{ttd}', '', $html);
}
$html = str_replace('{data_calon_mahasiswa}', $data_mahasiswa, $html);

$pdf->writeHTML($html);

$pdf->endPage();

//Halaman Selain Halaman Pertama

$index_sisa = ceil((count($data) - $row_per_page) / 30);
for ($i = 0; $i < $index_sisa; $i++) {
    $pdf->AddPage();
    $html = <<<EOF
<style>
    .header { font-size: 22pt; font-family: times; font-weight: bold; text-align:center;}
    .title { font-size: 13pt; font-family: serif; margin-top: 0px ;text-align:center; }
    th {background-color:#000;color:white;}
    tr.odd {background-color:#dddddd;}
    td.list {border-bottom:none;}
    td { font-size: 10pt; }
    td.prodi { font-size: 9pt; border-bottom:none; }
</style>
<table width="100%">
    <tr>
        <td width="100%" align="center">
            <img src="../../img/maba/logo_unair.gif" width="60px" height="60px"/><br/>
            <span class="header">UNIVERSITAS AIRLANGGA</span>
        </td>
    </tr>
</table>
<br/>
<br/>
<table width="100%" cellpadding="3">
    <tr>
        <th width="7%" align="center">No</th>
        <th width="13%">No.Ujian</th>
        <th width="32%">Nama Peserta</th>
        <th width="48%">Program Studi</th>
    </tr>
    {data_calon_mahasiswa}
    {ttd}
</table>
EOF;
    $data_mahasiswa = '';
    $hal = $row_per_page;
    for ($j = ($hal + (30 * $i) ); $j < ($hal + (30 * $i) + 30); $j++) {
        if (isset($data[$j])) {
            $data_mahasiswa .= $j % 2 == 0 ? '<tr class="odd">' : '<tr>';
            $data_mahasiswa .='
            <td class="list" align="center">' . ($j + 1) . '</td>
            <td class="list">' . $data[$j]['NO_UJIAN'] . '</td>
            <td class="list">' . $cetak->compress_nama($data[$j]['NM_C_MHS']) . '</td>
            <td class="list prodi">' . $data[$j]['NM_PROGRAM_STUDI'] . '</td>
        </tr>
        ';
        }
    }
    $html = str_replace('{data_calon_mahasiswa}', $data_mahasiswa, $html);
    if ($i == ($index_sisa - 1)) {
        $ttd = '<tr><td colspan="4">&nbsp;</td></tr><tr><td width="70%" colspan="2"></td><td width="30%" colspan="2" align="center">Surabaya, ' . strftime('%d %B %Y') . '<br/>Rektor<br/><br/><br/><br/>Fasich<br/>NIP.194612311974121001</td></tr>';
        $html = str_replace('{ttd}', $ttd, $html);
    } else {
        $html = str_replace('{ttd}', '', $html);
    }

    $pdf->writeHTML($html);
    $pdf->endPage();
}

$pdf->Output();
?>
