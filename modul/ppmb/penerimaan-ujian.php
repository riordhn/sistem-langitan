<?php
include 'config.php';
include 'class/Pengawas.class.php';

$Pengawas = new Pengawas($db);

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    if (post('mode') == 'add')
    {
        $result = $Pengawas->AddUjian($_POST);
        $smarty->assign('result', $result ? "Master ujian berhasil di tambahkan" : "Master ujian gagal ditambahkan");
    }
    
    if (post('mode') == 'edit')
    {
        $result = $Pengawas->UpdateUjian(post('id_ujian'), $_POST);
        $smarty->assign('result', $result ? "Master ujian berhasil di edit" : "Master ujian gagal di edit");
    }
    
    if (post('mode') == 'del-ujian')
    {
        $result = $Pengawas->DeleteUjian(post('id_ujian'));
        
        echo $result ? "1" : "0";
        exit();
    }
    
    if (post('mode') == 'add-penerimaan')
    {
        $result = $Pengawas->AddPenerimaanToUjian(post('id_penerimaan'), post('id_ujian'));
        $smarty->assign('result', $result ? "Penerimaan berhasil ditambahkan" : "Penerimaan gagal ditambahkan");
    }
    
    if (post('mode') == 'del-penerimaan')
    {
        $result = $Pengawas->DeletePenerimaanFromUjian(post('id_penerimaan'));
        
        echo $result ? "1" : "0";
        exit();
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        $smarty->assign('ujian_set', $Pengawas->GetListUjian());
    }
    
    if ($mode == 'edit')
    {
        $smarty->assign('ujian', $Pengawas->GetUjian(get('id_ujian')));
    }
    
    if ($mode == 'list-penerimaan')
    {
        $smarty->assign('ujian', $Pengawas->GetUjian(get('id_ujian')));
        $smarty->assign('penerimaan_set' ,$Pengawas->GetListPenerimaanByUjian(get('id_ujian')));
    }
    
    if ($mode == 'add-penerimaan')
    {
        $smarty->assign('penerimaan_set', $Pengawas->GetListPenerimaanForUjian());
    }
}

$smarty->display("penerimaan/ujian/{$mode}.tpl");
?>
