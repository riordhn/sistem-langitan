<?php
include('config.php');

if (!in_array($user->ID_JABATAN_PEGAWAI, array(175, 176, 203)))
{
	// 203	= Admin PPMB
	// 202	= Verifikator PPMB
	// 		= Ketua PPMB
	
	echo "<div class=\"center_title_bar\">--</div>";
	echo "<h2>Mohon maaf, anda tidak mempunyai akses kesini</h2>";
	exit();
}

$mode = get('mode', 'view');

$smarty->display("peserta/foto/{$mode}.tpl");
?>
