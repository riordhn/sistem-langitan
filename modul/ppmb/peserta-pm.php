<?php
include 'config.php';
include 'class/Penerimaan.class.php';
include 'class/BL.class.php';

$mode = get('mode', 'view');
$Penerimaan = new Penerimaan($db);
$BL = new BL($db);

$id_penerimaan = get('id_penerimaan', '');

if ($request_method == 'GET')
{
    if ($mode == 'view')
    {
        $smarty->assign('penerimaan_set', $Penerimaan->GetAllNonPasca());
		
		if ($id_penerimaan != '')
		{
			$smarty->assign('data_set', $BL->GetListBL($id_penerimaan));
		}
    }
}

$smarty->display("peserta/pm/{$mode}.tpl");
?>
