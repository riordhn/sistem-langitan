<?php
include('config.php');
include('class/Penerimaan.class.php');

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    if (post('mode') == 'add')
    {
        $db->Parse("insert into penerimaan_kunci_soal (id_penerimaan, kode_soal, data_kunci) values (:id_penerimaan, :kode_soal, :data_kunci)");
        $db->BindByName(':id_penerimaan', post('id_penerimaan'));
        $db->BindByName(':kode_soal', post('kode_soal'));
        $db->BindByName(':data_kunci', post('data_kunci'));
        $result = $db->Execute();
        
        $smarty->assign('result', $result ? "Data berhasil ditambahkan" : "Data gagal ditambahkan");
    }
    
    if (post('mode') == 'delete')
    {
        $id_penerimaan_kunci_soal = post('id_penerimaan_kunci_soal','');
        $result = $db->Query("delete from penerimaan_kunci_soal where id_penerimaan_kunci_soal = {$id_penerimaan_kunci_soal}");
        
        echo $result ? "1" : "0";
        
        exit();
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        // class
        $penerimaan = new Penerimaan($db);
        $smarty->assign('penerimaan_set', $penerimaan->GetAllForView());
        
        $id_penerimaan = get('id_penerimaan', '');
        
        if ($id_penerimaan != '')
        {
            $smarty->assign('kunci_set', $db->QueryToArray("select * from penerimaan_kunci_soal where id_penerimaan = {$id_penerimaan} order by kode_soal"));
        }
    }
}

$smarty->display("penilaian/kunci/{$mode}.tpl");
?>
