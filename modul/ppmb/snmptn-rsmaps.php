<?php
include('config.php');
include('class/snmptn.class.php');

$mode = get('mode', 'view');
$snmptn = new SnmptnClass($db);

if ($request_method == 'GET')
{
    $id_penerimaan = 44;
    
    if ($mode == 'view')
    {
        $smarty->assign('prodi_set', $snmptn->GetRekapSekolahPerProdi($id_penerimaan));
    }
    
    if ($mode == 'detail-prodi')
    {
        $id_program_studi = get('id_program_studi');
        $smarty->assign('sekolah_set', $snmptn->GetRekapPesertaPerSekolahByProdi($id_penerimaan, $id_program_studi));
        
        $smarty->assign('ps', $snmptn->GetProgramStudi($id_program_studi));
    }
    
    if ($mode == 'detail-peserta')
    {
        $id_program_studi = get('id_program_studi');
        $id_sekolah = get('id_sekolah');
        
        $smarty->assign('cmb_set', $snmptn->GetListPesertaByProdiAndSekolah($id_penerimaan, $id_program_studi, $id_sekolah));
        
        $smarty->assign('ps', $snmptn->GetProgramStudi($id_program_studi));
        $smarty->assign('s', $snmptn->GetSekolah($id_sekolah_asal));
    }
}

$smarty->display("snmptn/rsmaps/{$mode}.tpl");
?>
