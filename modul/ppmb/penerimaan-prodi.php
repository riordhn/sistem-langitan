<?php
include('config.php');
include('class/Penerimaan.class.php');

if (!in_array($user->ID_JABATAN_PEGAWAI, array(175, 176, 203)))
{
    // 203 = Admin
    echo "Anda tidak punya hak akses halaman ini.";
    exit();
}

$mode = get('mode', 'view');

$id_pt = $id_pt_user;

if ($request_method == 'POST')
{
	if (post('mode') == 'addprodi')
	{
		$db->Parse("insert into penerimaan_prodi (id_penerimaan, id_program_studi) values (:id_penerimaan, :id_program_studi)");
		$db->BindByName(':id_penerimaan', post('id_penerimaan'));
		$db->BindByName(':id_program_studi', post('id_program_studi'));
		$result = $db->Execute();
        
        $db->Parse("insert into statistik_cmhs (id_penerimaan, id_program_studi) values (:id_penerimaan, :id_program_studi)");
		$db->BindByName(':id_penerimaan', post('id_penerimaan'));
		$db->BindByName(':id_program_studi', post('id_program_studi'));
		$result = $db->Execute();
		
		if ($result)
			$smarty->assign('added', 'Data berhasil ditambah');
		else
			$smarty->assign('added', 'Data gagal ditambahkan');
	}
	
	if (post('mode') == 'delprodi')
	{
		$id_penerimaan_prodi = post('id_penerimaan_prodi');
        
        $rows = $db->QueryToArray("select id_penerimaan, id_program_studi from penerimaan_prodi where id_penerimaan_prodi = {$id_penerimaan_prodi}");
        
        $result = $db->Query("delete from statistik_cmhs where id_penerimaan = {$rows[0]['ID_PENERIMAAN']} and id_program_studi = {$rows[0]['ID_PROGRAM_STUDI']}");
		$result = $db->Query("delete from penerimaan_prodi where id_penerimaan_prodi = {$id_penerimaan_prodi}");
		
		if ($result)
			$smarty->assign('deleted', 'Data berhasil dihapus');
		else
			$smarty->assign('deleted', 'Data gagal dihapus');
	}
	
	if (post('mode') == 'addminat')
	{
		$db->Parse("insert into penerimaan_prodi_minat (id_penerimaan, id_prodi_minat) values (:id_penerimaan, :id_prodi_minat)");
		$db->BindByName(':id_penerimaan', post('id_penerimaan'));
		$db->BindByName(':id_prodi_minat', post('id_prodi_minat'));
		$result = $db->Execute();
		
		if ($result)
			$smarty->assign('added', 'Data berhasil ditambah');
		else
			$smarty->assign('added', 'Data gagal ditambahkan');
	}
	
	if (post('mode') == 'delminat')
	{
		$id_penerimaan_pm = post('id_penerimaan_pm');
		$result = $db->Query("delete from penerimaan_prodi_minat where id_penerimaan_pm = {$id_penerimaan_pm}");
		
		if ($result)
			$smarty->assign('deleted', 'Data berhasil dihapus');
		else
			$smarty->assign('deleted', 'Data gagal dihapus');
	}
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	// Menampilkan list penerimaan
    // class
    $penerimaan = new Penerimaan($db);
    $smarty->assign('penerimaan_set', $penerimaan->GetAllForView());
	
	$id_penerimaan = get('id_penerimaan', '');
	$id_penerimaan_prodi = get('id_penerimaan_prodi', '');
	$id_penerimaan_pm = get('id_penerimaan_pm', '');
	
	if ($mode == 'view' && $id_penerimaan != '')
	{
		$penerimaan_prodi_set = $db->QueryToArray("
			select pp.id_penerimaan_prodi, nm_fakultas, nm_jenjang, nm_program_studi, gelar_pendek, pp.is_aktif
			from penerimaan_prodi pp
			join penerimaan p on (p.id_penerimaan = pp.id_penerimaan and p.id_penerimaan = {$id_penerimaan})
			join program_studi ps on ps.id_program_studi = pp.id_program_studi
			join fakultas f on f.id_fakultas = ps.id_fakultas
			join jenjang j on j.id_jenjang = ps.id_jenjang");
		$smarty->assign('penerimaan_prodi_set', $penerimaan_prodi_set);
		
		$penerimaan_prodi_minat_set = $db->QueryToArray("
			select ppm.id_penerimaan_pm, nm_jenjang, nm_program_studi, nm_prodi_minat, ppm.is_aktif
			from penerimaan_prodi_minat ppm
			join prodi_minat pm on pm.id_prodi_minat = ppm.id_prodi_minat
			join program_studi ps on ps.id_program_studi = pm.id_program_studi
			join jenjang j on j.id_jenjang = ps.id_jenjang
			where ppm.id_penerimaan = {$id_penerimaan}
			order by ps.id_program_studi");
		$smarty->assign('penerimaan_prodi_minat_set', $penerimaan_prodi_minat_set);
	}
	
	if ($mode == 'addprodi' && $id_penerimaan != '')
	{
		$db->Query("
			select id_penerimaan, nm_penerimaan, p.id_jenjang, j.nm_jenjang from penerimaan p
			join jenjang j on j.id_jenjang = p.id_jenjang
			where id_penerimaan = {$id_penerimaan}");
		$penerimaan = $db->FetchAssoc();
		$smarty->assign('penerimaan', $penerimaan);
		
		$smarty->assign('program_studi_set', $db->QueryToArray("
			select id_program_studi, nm_jenjang||' '||nm_program_studi||' ('||gelar_pendek||')' as nm_program_studi from program_studi ps
			join fakultas f on f.id_fakultas = ps.id_fakultas and f.id_perguruan_tinggi = '{$id_pt}'
            join jenjang j on j.id_jenjang = ps.id_jenjang
			where ps.id_jenjang = {$penerimaan['ID_JENJANG']} and
                id_program_studi not in (select id_program_studi from penerimaan_prodi where id_penerimaan = {$id_penerimaan}) and
                status_aktif_prodi = 1
			order by nm_program_studi"));
	}
	
	if ($mode == 'addminat' && $id_penerimaan != '')
	{
		$db->Query("
			select id_penerimaan, nm_penerimaan, p.id_jenjang, j.nm_jenjang from penerimaan p
			join jenjang j on j.id_jenjang = p.id_jenjang
			where id_penerimaan = {$id_penerimaan}");
		$penerimaan = $db->FetchAssoc();
		$smarty->assign('penerimaan', $penerimaan);
		
		$smarty->assign('prodi_minat_set', $db->QueryToArray("
			select id_prodi_minat, nm_prodi_minat, nm_program_studi from prodi_minat pm
			join program_studi ps on ps.id_program_studi = pm.id_program_studi
			where
                        ps.id_program_studi in (select id_program_studi from penerimaan_prodi where id_penerimaan = {$id_penerimaan}) and
                        pm.id_prodi_minat not in (select id_prodi_minat from penerimaan_prodi_minat where id_penerimaan = {$id_penerimaan})
                        order by ps.id_program_studi"));
	}
	
	if ($mode == 'delprodi' && $id_penerimaan_prodi != '')
	{
		$db->Query("
			select pp.id_penerimaan_prodi, pp.id_penerimaan, ps.nm_program_studi, p.nm_penerimaan from penerimaan_prodi pp
			left join program_studi ps on ps.id_program_studi = pp.id_program_studi
			left join penerimaan p on p.id_penerimaan = pp.id_penerimaan
			where id_penerimaan_prodi = {$id_penerimaan_prodi}");
		$smarty->assign('pp', $db->FetchAssoc());
	}
	
	if ($mode == 'delminat' && $id_penerimaan_pm != '')
	{
		$db->Query("
			select ppm.id_penerimaan_pm, ppm.id_penerimaan, p.nm_penerimaan, pm.nm_prodi_minat, ps.nm_program_studi from penerimaan_prodi_minat ppm
			left join penerimaan p on p.id_penerimaan = ppm.id_penerimaan
			left join prodi_minat pm on pm.id_prodi_minat = ppm.id_prodi_minat
			left join program_studi ps on ps.id_program_studi = pm.id_program_studi
			where id_penerimaan_pm = {$id_penerimaan_pm}");
		$smarty->assign('ppm', $db->FetchAssoc());
	}
}

$smarty->display("penerimaan/prodi/{$mode}.tpl");
?>
