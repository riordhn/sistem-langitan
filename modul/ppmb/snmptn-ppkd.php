<?php
include("config.php");

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        // program studi pilihan
        $id_program_studi = get('id_program_studi', '');
        $pilihan_ke = get('pilihan_ke', '1');
        $id_provinsi = get('id_provinsi', '');
        
        $smarty->assign('program_studi_set', $db->QueryToArray("
            select id_program_studi, nm_program_studi, nm_jenjang from program_studi ps
            join jenjang j on j.id_jenjang = ps.id_jenjang
            where kode_snmptn is not null order by nm_program_studi"));
        
        $smarty->assign('pilihan_ke', array(
            array('ID' => 1),
            array('ID' => 2)
        ));
        
        $smarty->assign('provinsi_set', $db->QueryToArray("select * from provinsi where id_negara = 1 order by nm_provinsi"));
        
        if ($id_program_studi != '' && $pilihan_ke != '' && $id_provinsi != '')
        {
            // statistik
            $statistik_set = $db->QueryToArray("select * from statistik_cmhs where id_penerimaan = 44 and id_program_studi = {$id_program_studi}");
            $smarty->assign('statistik', $statistik_set[0]);
            
            if ($id_provinsi == 'all')
                $where_provinsi = "";
            else
                $where_provinsi = "and k.id_provinsi = {$id_provinsi}";
            
            // calon mahasiswa baru
            $cmb_set = $db->QueryToArray("
                select * from (
                    select a.*,
                        (((a.nilai_rapor * 0.35) + (a.nilai_akd * 0.25) + (a.nilai_nakd * 0.15) + (a.nilai_pm * 0.2) + (a.nilai_kelas * 0.05)) * 0.6) +
                        (((a.nilai_akreditasi * 0.3) + (a.nilai_snmptn * 0.3) + (a.nilai_mandiri * 0.1) + (a.nilai_ipk * 0.3)) * 0.4) as nilai_total
                    from (
                        select cmb.id_c_mhs, no_ujian, nm_c_mhs, cms.nilai_pm, p.nm_provinsi,

                            nvl((select nilai from skor_rapor skr where skr.batas_bawah <= cmns.nilai_rata and cmns.nilai_rata <= skr.batas_atas),0) as nilai_rapor,

                            nvl((select max(sp.nilai) from calon_mahasiswa_prestasi cmp
                            join skor_prestasi sp on sp.id_skor_prestasi = cmp.id_skor_prestasi
                            where sp.is_akademik = 1 and cmp.id_c_mhs = cmb.id_c_mhs),0) as nilai_akd,

                            nvl((select max(sp.nilai) from calon_mahasiswa_prestasi cmp
                            join skor_prestasi sp on sp.id_skor_prestasi = cmp.id_skor_prestasi
                            where sp.is_akademik = 0 and cmp.id_c_mhs = cmb.id_c_mhs),0) as nilai_nakd,

                            nvl((select nilai from skor_kelas sk where sk.jenis_kelas = cms.jenis_kelas),0) as nilai_kelas,

                            nvl((select nilai from skor_akreditasi sa where sa.akreditasi_sekolah = cms.akreditasi_sekolah),0) as nilai_akreditasi,

                            nvl(round(((sr.rasio_mandiri / (select max(rasio_mandiri) from sekolah_rasio)) * 100),2),0) as nilai_mandiri,
                            nvl(round(((sr.rasio_snmptn / (select max(rasio_snmptn) from sekolah_rasio)) * 100),2),0) as nilai_snmptn,

                            nvl((select nilai from skor_ipk si where si.batas_bawah <= sr.rata_ipk and sr.rata_ipk <= si.batas_atas),20) as nilai_ipk

                        from calon_mahasiswa_baru cmb
                        left join program_studi ps1 on ps1.id_program_studi = id_pilihan_1
                        left join program_studi ps2 on ps2.id_program_studi = id_pilihan_2
                        left join calon_mahasiswa_nilai_sekolah cmns on cmns.id_c_mhs = cmb.id_c_mhs
                        left join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
                        left join sekolah_rasio sr on sr.id_sekolah = cms.id_sekolah_asal
                        left join sekolah s on s.id_sekolah = cms.id_sekolah_asal
                        left join kota k on k.id_kota = s.id_kota
                        left join provinsi p on p.id_provinsi = k.id_provinsi
                        where id_penerimaan = 44 and id_pilihan_{$pilihan_ke} = {$id_program_studi} and cmb.tgl_diterima is null {$where_provinsi}
                    ) a
                ) a order by nilai_total desc");
            $smarty->assign('cmb_set', $cmb_set);
        }
    }
}

$smarty->display("snmptn/ppkd/{$mode}.tpl");
?>
