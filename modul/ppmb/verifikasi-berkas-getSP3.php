<?php
include('config.php');

$id_program_studi = get('id_program_studi');
$id_jalur = get('id_jalur');
$id_semester = get('id_semester');
$id_kelompok_biaya = get('id_kelompok_biaya');

$db->Query("
    SELECT DB.BESAR_BIAYA FROM DETAIL_BIAYA DB
    LEFT JOIN BIAYA_KULIAH BK ON BK.ID_BIAYA_KULIAH = DB.ID_BIAYA_KULIAH
    LEFT JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
    WHERE B.NM_BIAYA = 'SP3' AND BK.ID_SEMESTER = {$id_semester} AND BK.ID_JALUR = {$id_jalur}
    AND BK.ID_KELOMPOK_BIAYA = {$id_kelompok_biaya} AND BK.ID_PROGRAM_STUDI = {$id_program_studi}");
$row = $db->FetchAssoc();

if ($row)
{
    echo $row['BESAR_BIAYA'];
}
else
{
    echo "0";
}
?>