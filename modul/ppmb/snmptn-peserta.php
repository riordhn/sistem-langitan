<?php
include('config.php');
include('class/snmptn.class.php');

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        // program studi pilihan
        $id_program_studi = get('id_program_studi', '');
        
        $smarty->assign('program_studi_set', $db->QueryToArray("
            select id_program_studi, nm_program_studi, nm_jenjang from program_studi ps
            join jenjang j on j.id_jenjang = ps.id_jenjang
            where kode_snmptn is not null order by nm_program_studi"));
        
        if ($id_program_studi != '')
        {
            // calon mahasiswa baru
            $cmb_set = $db->QueryToArray("
                select cmb.id_c_mhs, no_ujian, nm_c_mhs,
                    cms.nilai_pm, cms.jenis_kelas, s.akreditasi as akreditasi_sekolah, cms.nilai_rapor,
                    (select count(*) from calon_mahasiswa_prestasi cmp
                     join skor_prestasi sp on sp.id_skor_prestasi = cmp.id_skor_prestasi and sp.is_akademik = 1
                     where cmp.id_c_mhs = cmb.id_c_mhs) prestasi_akd,
                    (select count(*) from calon_mahasiswa_prestasi cmp
                     join skor_prestasi sp on sp.id_skor_prestasi = cmp.id_skor_prestasi and sp.is_akademik = 0
                     where cmp.id_c_mhs = cmb.id_c_mhs) prestasi_non_akd,
                    sr.rasio_mandiri, sr.rasio_snmptn, sr.rata_ipk, cms.id_sekolah_asal,
                    cmd.status_bidik_misi
                from calon_mahasiswa_baru cmb
                join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
				join calon_mahasiswa_data cmd on cmd.id_c_mhs = cmb.id_c_mhs
                left join sekolah_rasio sr on sr.id_sekolah = cms.id_sekolah_asal
				left join sekolah s on s.id_sekolah = cms.id_sekolah_asal
                where id_penerimaan = 44 and id_pilihan_1 = {$id_program_studi}
                order by no_ujian");
            $smarty->assign('cmb_set', $cmb_set);
        }
    }
    
    if ($mode == 'detail')
    {
		$snmptn = new SnmptnClass($db);
		
        $id_c_mhs = get('id_c_mhs');
		
		$smarty->assign('cmb', $snmptn->GetDetailPeserta($id_c_mhs));
    }
}

$smarty->display("snmptn/peserta/{$mode}.tpl");
?>
