<?php
include 'config.php';
include 'class/Pengawas.class.php';

$mode = get('mode', 'view');
$Pengawas = new Pengawas($db);

if ($request_method == 'POST')
{
    if (post('mode') == 'upload-file')
    {
        $result = $Pengawas->UploadFileAbsensi(post('token'), $_FILES['file']);
        $smarty->assign('result', $result ? $result : "Gagal upload file, silahkan load browser");
    }
    
    if (post('mode') == 'proses-file')
    {
        $result_set = $Pengawas->ProsesFileAbsensi(post('token'), new PHPExcel_Reader_Excel5());
        $smarty->assign('result_set', $result_set);
    }
}

if ($request_method == 'GET')
{
    if ($mode == 'view')
    {
        
    }
}

$smarty->display("pengawas/absen/{$mode}.tpl");
?>
