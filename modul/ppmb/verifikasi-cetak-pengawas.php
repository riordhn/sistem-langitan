<?php
include('config.php');

$id_penerimaan = get('id_penerimaan', '');

$db->Query("select p.*, j.nm_jenjang from penerimaan p join jenjang j on j.id_jenjang = p.id_jenjang where id_penerimaan = {$id_penerimaan}");
$row = $db->FetchAssoc();

$filename = "Daftar Pengawas Penerimaan {$row['NM_PENERIMAAN']} Gelombang {$row['GELOMBANG']} Semester {$row['SEMESTER']} Tahun {$row['TAHUN']}";

if ($id_penerimaan == '')
    $where = "jp.id_penerimaan in (select id_penerimaan from penerimaan where is_aktif = 1)";
else
    $where = "jp.id_penerimaan = {$id_penerimaan}";

$jadwal_ujian_set = $db->QueryToArray("
    select jp.lokasi, jp.nm_ruang, jp.kapasitas, jp.terisi, pu.* from jadwal_ppmb jp
    join pengawas_ujian_ppmb pu on pu.id_jadwal_ppmb = jp.id_jadwal_ppmb
    where {$where}
    order by pu.nomer_awal");
$smarty->assign('jp_set', $jadwal_ujian_set);

// create excel object
$excel = new PHPExcel();

// get active sheet
$sheet = $excel->getActiveSheet();

// set column header
$sheet->setCellValue('A1', "Lokasi");
$sheet->setCellValue('B1', "Ruang");
$sheet->setCellValue('C1', "Kapasitas");
$sheet->setCellValue('D1', "Distributor Naskah");
$sheet->setCellValue('E1', "Koordinator");
$sheet->setCellValue('F1', "Pengawas");
$sheet->setCellValue('G1', "Status");
$sheet->setCellValue('H1', "No Peserta");
$sheet->setCellValue('I1', "Amplop Soal");

for ($i = 0; $i < count($jadwal_ujian_set); $i++)
{
    $index = $i + 2;
    $ju = &$jadwal_ujian_set[$i];
    
    // wordwarp pengawas
    if ($ju['PENGAWAS2'] != '') $ju['PENGAWAS2'] = "\n".$ju['PENGAWAS2'];
    if ($ju['PENGAWAS3'] != '') $ju['PENGAWAS3'] = "\n".$ju['PENGAWAS3'];
    if ($ju['PENGAWAS4'] != '') $ju['PENGAWAS3'] = "\n".$ju['PENGAWAS4'];
    
    // wordwarp status
    if ($ju['STATUS2'] != '') $ju['STATUS2'] = "\n".$ju['STATUS2'];
    if ($ju['STATUS3'] != '') $ju['STATUS3'] = "\n".$ju['STATUS3'];
    if ($ju['STATUS4'] != '') $ju['STATUS4'] = "\n".$ju['STATUS4'];
    
    // wordwrap amplop
    if ($ju['AMPLOP2'] != '') $ju['AMPLOP2'] = "\n".$ju['AMPLOP2'];
    
    $sheet->setCellValue("A{$index}", "{$ju['LOKASI']}");
    $sheet->setCellValue("B{$index}", "{$ju['NM_RUANG']}");
    $sheet->setCellValue("C{$index}", "{$ju['KAPASITAS']}");
    $sheet->setCellValue("D{$index}", "{$ju['DISTRIBUTOR']}");
    $sheet->setCellValue("E{$index}", "{$ju['KOORDINATOR']}");
    $sheet->setCellValue("F{$index}", "{$ju['PENGAWAS1']}{$ju['PENGAWAS2']}{$ju['PENGAWAS3']}{$ju['PENGAWAS4']}");
    $sheet->setCellValue("G{$index}", "{$ju['STATUS1']}{$ju['STATUS2']}{$ju['STATUS3']}{$ju['STATUS4']}");
    $sheet->setCellValue("H{$index}", "{$ju['NOMER_AWAL']} - {$ju['NOMER_AKHIR']}");
    $sheet->setCellValue("I{$index}", "{$ju['AMPLOP1']}{$ju['AMPLOP2']}");
    
    // set wordwarp
    $sheet->getStyle("F{$index}")->getAlignment()->setWrapText(true);
    $sheet->getStyle("G{$index}")->getAlignment()->setWrapText(true);
    $sheet->getStyle("I{$index}")->getAlignment()->setWrapText(true);
}

// set autosize column
$sheet->getColumnDimension('A')->setAutoSize(true);
$sheet->getColumnDimension('B')->setAutoSize(true);
$sheet->getColumnDimension('C')->setAutoSize(true);
$sheet->getColumnDimension('D')->setAutoSize(true);
$sheet->getColumnDimension('E')->setAutoSize(true);
$sheet->getColumnDimension('F')->setAutoSize(true);
$sheet->getColumnDimension('G')->setAutoSize(true);
$sheet->getColumnDimension('H')->setAutoSize(true);
$sheet->getColumnDimension('I')->setAutoSize(true);

// header output
header('Content-Type: application/vnd.ms-excel');
header("Content-Disposition: attachment;filename=\"Daftar Pengawas.xls\"");
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
$objWriter->save('php://output');
?>
