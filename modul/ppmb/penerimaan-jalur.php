<?php
include('config.php');

$mode = get('mode', 'view');

$id_pt = $id_pt_user;

if ($request_method == 'POST')
{
    if (post('mode') == 'add')
    {
		$result = $db->Query("insert into jalur (nm_jalur, id_perguruan_tinggi) values ('{$_POST['nm_jalur']}','{$id_pt}')");
		$smarty->assign('result', $result ? 'add' : 'add-failed');
	}
	
	if (post('mode') == 'edit')
	{
		$result = $db->Query("update jalur set nm_jalur = '".post('nm_jalur')."' where id_jalur = ".post('id'));
		$smarty->assign('result', $result ? 'edit' : 'edit-failed');
	}
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$smarty->assign('jalur_set', $db->QueryToArray("
			select jalur.*, 
			(select count(*) from penerimaan where penerimaan.id_jalur = jalur.id_jalur and penerimaan.id_perguruan_tinggi = '{$id_pt}') jumlah_penerimaan
			from jalur where id_perguruan_tinggi = '{$id_pt}' and is_deleted = '0' order by nm_jalur"));
	}
	
	if ($mode == 'edit')
	{
		$db->Query("select * from jalur where id_jalur = {$_GET['id']}");
		$smarty->assign('jalur', $db->FetchAssoc());
	}
}

$smarty->display("penerimaan/jalur/{$mode}.tpl");
?>
