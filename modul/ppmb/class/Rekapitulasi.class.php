<?php

/***
 * Deskripsi :
 * Class Reporting dan rekapitulasi agar standar dan bisa digunakan
 * oleh modul lain.
 * 
 * Publikasi :
 * 1. Di dalam modul PPMB
 * 2. Di Rekapitulasi PPMB Internal -- /rekapitulasi/ppmb-internal/
 * 3. Di rekapitulasi PPMB Publik -- /rekapitulasi/ppmb/
 * 4. Di dalam modul Pendidikan
 */

class RekapitulasiPPMB 
{
	private $db;
	
	public function __construct($db)
	{
		$this->db = $db;
	}
	
	/**
	 * Mendapatkan data penerimaan yang telah selesai
	 */
	public function get_penerimaan_selesai_list()
	{
		$result = array();
		
		// group tahun, semester
		$result = $this->db->QueryToArray("SELECT DISTINCT tahun, semester FROM penerimaan WHERE tgl_pengumuman < SYSDATE ORDER BY tahun DESC, semester DESC");
		
		// group pasca-sarjana / non-pasca
		foreach ($result as &$row)
		{
			// list data penerimaan non-pasca
			$row['non_pasca_set'] = $this->db->QueryToArray(
				"SELECT id_penerimaan, gelombang, nm_penerimaan, nm_jalur, is_pendaftaran_online FROM penerimaan
				JOIN jalur ON jalur.id_jalur = penerimaan.id_jalur
				WHERE 
					tgl_pengumuman < SYSDATE AND id_jenjang in (1,4,5) and
					tahun = '{$row['TAHUN']}' AND semester = '{$row['SEMESTER']}'
				ORDER BY gelombang desc, nm_penerimaan");
			
			// list data penerimaan pasca
			$row['pasca_set'] = $this->db->QueryToArray(
				"SELECT id_penerimaan, gelombang, nm_penerimaan, nm_jalur, is_pendaftaran_online FROM penerimaan
				JOIN jalur ON jalur.id_jalur = penerimaan.id_jalur
				WHERE 
					tgl_pengumuman < SYSDATE AND id_jenjang not in (1,4,5) and
					tahun = '{$row['TAHUN']}' AND semester = '{$row['SEMESTER']}'
				ORDER BY gelombang desc, nm_penerimaan");
		}

		// return
		return $result;
	}
	
	public function get_data_rekap_list($id_penerimaan)
	{
		$result = $this->db->QueryToArray(
			"SELECT 
				ps.id_program_studi, nm_jenjang, nm_program_studi, nm_fakultas, singkatan_fakultas,
				antr.jumlah AS antri, verk.jumlah AS verifikasi_kembali, ver.jumlah AS verifikasi, 
				(nvl(antr.jumlah,0) + nvl(verk.jumlah,0) + nvl(ver.jumlah,0)) AS peminat,
				P1.JUMLAH AS P1, P2.JUMLAH AS P2, P3.JUMLAH AS P3, P4.JUMLAH AS P4,
				nvl(PD.JUMLAH, 0) AS diterima,
				SC.KUOTA, SC.NILAI_MIN, SC.NILAI_MAX, SC.NILAI_RATA, 
				SC2.NILAI_MIN_2, SC2.NILAI_MAX_2, SC2.NILAI_RATA_2, 
				NILAI.NILAI_RATA_DITERIMA, NILAI_PASCA.NILAI_RATA_DITERIMA_PASCA, NILAI_PASCA2.NILAI_RATA_TDK_DITERIMA
			FROM penerimaan_prodi pp
			JOIN program_studi ps ON ps.id_program_studi = pp.id_program_studi
			JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
			JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
			LEFT JOIN (
				SELECT id_pilihan_1 AS id_pilihan, COUNT(id_c_mhs) AS jumlah
				FROM v_status_calon_mahasiswa
				WHERE id_penerimaan = '{$id_penerimaan}' AND is_antri_verifikasi = 1
				GROUP BY id_pilihan_1) antr ON antr.id_pilihan = ps.id_program_studi
			LEFT JOIN (
				SELECT id_pilihan_1 AS id_pilihan, COUNT(id_c_mhs) AS jumlah
				FROM v_status_calon_mahasiswa
				WHERE id_penerimaan = '{$id_penerimaan}' AND is_verifikasi_kembali = 1
				GROUP BY id_pilihan_1) verk ON verk.id_pilihan = ps.id_program_studi
			LEFT JOIN (
				SELECT id_pilihan_1 AS id_pilihan, COUNT(id_c_mhs) AS jumlah
				FROM v_status_calon_mahasiswa
				WHERE id_penerimaan = '{$id_penerimaan}' AND is_verifikasi = 1
				GROUP BY id_pilihan_1) ver ON ver.id_pilihan = ps.id_program_studi
			LEFT JOIN (
				SELECT ID_PILIHAN_1, COUNT(ID_C_MHS) JUMLAH FROM CALON_MAHASISWA_BARU CMB
				WHERE CMB.ID_PENERIMAAN = '{$id_penerimaan}' AND CMB.NO_UJIAN IS NOT NULL
				GROUP BY ID_PILIHAN_1) P1 ON P1.ID_PILIHAN_1 = PS.ID_PROGRAM_STUDI
			LEFT JOIN (
				SELECT ID_PILIHAN_2, COUNT(ID_C_MHS) JUMLAH FROM CALON_MAHASISWA_BARU CMB
				WHERE CMB.ID_PENERIMAAN = '{$id_penerimaan}' AND CMB.NO_UJIAN IS NOT NULL
				GROUP BY ID_PILIHAN_2) P2 ON P2.ID_PILIHAN_2 = PS.ID_PROGRAM_STUDI
			LEFT JOIN (
				SELECT ID_PILIHAN_3, COUNT(ID_C_MHS) JUMLAH FROM CALON_MAHASISWA_BARU CMB
				WHERE CMB.ID_PENERIMAAN = '{$id_penerimaan}' AND CMB.NO_UJIAN IS NOT NULL
				GROUP BY ID_PILIHAN_3) P3 ON P3.ID_PILIHAN_3 = PS.ID_PROGRAM_STUDI
			LEFT JOIN (
				SELECT ID_PILIHAN_4, COUNT(ID_C_MHS) JUMLAH FROM CALON_MAHASISWA_BARU CMB
				WHERE CMB.ID_PENERIMAAN = '{$id_penerimaan}' AND CMB.NO_UJIAN IS NOT NULL
				GROUP BY ID_PILIHAN_4) P4 ON P4.ID_PILIHAN_4 = PS.ID_PROGRAM_STUDI
			LEFT JOIN (
				SELECT ID_PROGRAM_STUDI, COUNT(ID_C_MHS) JUMLAH FROM CALON_MAHASISWA_BARU CMB
				WHERE CMB.ID_PENERIMAAN = '{$id_penerimaan}' AND CMB.ID_PROGRAM_STUDI IS NOT NULL
				GROUP BY ID_PROGRAM_STUDI) PD ON PD.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
			LEFT JOIN STATISTIK_CMHS SC ON SC.ID_PENERIMAAN = '{$id_penerimaan}' AND SC.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
			LEFT JOIN (
				SELECT id_pilihan, MIN(nilai) AS nilai_min_2, MAX(nilai) AS nilai_max_2, round(avg(nilai),2) AS nilai_rata_2 
				FROM (
					SELECT cmb.id_penerimaan, ps.jurusan_sekolah, 1 AS pilihan_ke, id_pilihan_1 AS id_pilihan, nilai_tpa + nilai_prestasi as nilai
					FROM calon_mahasiswa_baru cmb
					JOIN program_studi ps ON ps.id_program_studi = cmb.id_pilihan_1
					JOIN nilai_cmhs n ON n.id_c_mhs = cmb.id_c_mhs AND n.jurusan_sekolah = ps.jurusan_sekolah
					WHERE cmb.id_penerimaan = '{$id_penerimaan}' AND cmb.no_ujian IS NOT NULL AND nilai_tpa + nilai_prestasi > 0
					UNION ALL
					SELECT cmb.id_penerimaan, ps.jurusan_sekolah, 2 AS pilihan_ke, id_pilihan_2 AS id_pilihan, nilai_tpa + nilai_prestasi as nilai
					FROM calon_mahasiswa_baru cmb
					JOIN program_studi ps ON ps.id_program_studi = cmb.id_pilihan_2
					JOIN nilai_cmhs n ON n.id_c_mhs = cmb.id_c_mhs AND n.jurusan_sekolah = ps.jurusan_sekolah
					WHERE cmb.id_penerimaan = '{$id_penerimaan}' AND cmb.no_ujian IS NOT NULL AND nilai_tpa + nilai_prestasi > 0
					UNION ALL
					SELECT cmb.id_penerimaan, ps.jurusan_sekolah, 3 AS pilihan_ke, id_pilihan_3 AS id_pilihan, nilai_tpa + nilai_prestasi as nilai
					FROM calon_mahasiswa_baru cmb
					JOIN program_studi ps ON ps.id_program_studi = cmb.id_pilihan_3
					JOIN nilai_cmhs n ON n.id_c_mhs = cmb.id_c_mhs AND n.jurusan_sekolah = ps.jurusan_sekolah
					WHERE cmb.id_penerimaan = '{$id_penerimaan}' AND cmb.no_ujian IS NOT NULL AND nilai_tpa + nilai_prestasi > 0
					UNION ALL
					SELECT cmb.id_penerimaan, ps.jurusan_sekolah, 4 AS pilihan_ke, id_pilihan_4 AS id_pilihan, nilai_tpa + nilai_prestasi as nilai
					FROM calon_mahasiswa_baru cmb
					JOIN program_studi ps ON ps.id_program_studi = cmb.id_pilihan_4
					JOIN nilai_cmhs n ON n.id_c_mhs = cmb.id_c_mhs AND n.jurusan_sekolah = ps.jurusan_sekolah
					WHERE cmb.id_penerimaan = '{$id_penerimaan}' AND cmb.no_ujian IS NOT NULL AND nilai_tpa + nilai_prestasi > 0
				) T
				group by id_pilihan
			) SC2 ON SC2.ID_PILIHAN = PS.ID_PROGRAM_STUDI
			LEFT JOIN (
				SELECT CMB.ID_PROGRAM_STUDI, ROUND(AVG(NILAI_TPA + NILAI_PRESTASI), 2) AS NILAI_RATA_DITERIMA FROM CALON_MAHASISWA_BARU CMB
				INNER JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CMB.ID_PROGRAM_STUDI
				INNER JOIN NILAI_CMHS N ON N.ID_C_MHS = CMB.ID_C_MHS AND N.JURUSAN_SEKOLAH = PS.JURUSAN_SEKOLAH
				WHERE CMB.ID_PENERIMAAN IN '{$id_penerimaan}'
				GROUP BY CMB.ID_PROGRAM_STUDI
			) NILAI ON NILAI.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
			LEFT JOIN (
				SELECT CMB.ID_PROGRAM_STUDI, ROUND(AVG(NCP.TOTAL_NILAI),2) AS NILAI_RATA_DITERIMA_PASCA FROM CALON_MAHASISWA_BARU CMB
				INNER JOIN NILAI_CMHS_PASCA NCP ON NCP.ID_C_MHS = CMB.ID_C_MHS
				WHERE CMB.ID_PENERIMAAN IN '{$id_penerimaan}' AND CMB.TGL_DITERIMA IS NOT NULL
				GROUP BY CMB.ID_PROGRAM_STUDI
			) NILAI_PASCA ON NILAI_PASCA.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
			LEFT JOIN (
				SELECT CMB.ID_PILIHAN_1, ROUND(AVG(NCP.TOTAL_NILAI),2) AS NILAI_RATA_TDK_DITERIMA FROM CALON_MAHASISWA_BARU CMB
				INNER JOIN NILAI_CMHS_PASCA NCP ON NCP.ID_C_MHS = CMB.ID_C_MHS
				WHERE CMB.ID_PENERIMAAN IN '{$id_penerimaan}' AND CMB.NO_UJIAN IS NOT NULL AND CMB.TGL_DITERIMA IS NULL
				GROUP BY CMB.ID_PILIHAN_1
			) NILAI_PASCA2 ON NILAI_PASCA2.ID_PILIHAN_1 = PS.ID_PROGRAM_STUDI
			WHERE pp.id_penerimaan = '{$id_penerimaan}' AND pp.is_aktif = 1
			ORDER BY ps.id_fakultas, nm_program_studi");
			
		// Proses keketatan	
		foreach ($result as &$row)
		{
			$total_pemilih = $row['P1'] + $row['P2'] + $row['P3'] + $row['P4'];
			if ($total_pemilih > 0)
				$row['KEKETATAN'] = $row['DITERIMA'] / $total_pemilih;
			else
				$row['KEKETATAN'] = 0;
		}

		return $result;
	}
}