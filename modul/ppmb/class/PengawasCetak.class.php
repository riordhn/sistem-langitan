<?php

/**
 * Class Cetak Pengawas
 */
class PengawasCetak
{ 
    private $db;
    
    function __construct(MyOracle $db)
    {   
        $this->db = $db;
    }
    
    function MasterPlot($id_ujian)
    {
        $jadwal_ppmb_set = $this->db->QueryToArray("
            select id_jadwal_ppmb, nm_jurusan, nm_ruang, kapasitas, nomer_awal, (nomer_awal+kapasitas-1) as nomer_akhir, lokasi
            from jadwal_ppmb jp
            join penerimaan p on p.id_penerimaan = jp.id_penerimaan
            join jurusan_sekolah js on js.id_jurusan = jp.kode_jalur
            where p.id_ujian = {$id_ujian}
            order by nomer_awal");
        
        foreach ($jadwal_ppmb_set as &$jp)
        {
            $jp['pengawas_set'] = $this->db->QueryToArray("
                select jp.singkatan_jabatan, p.nm_pengawas, a.nm_asal from ppmb_pengawas_ruang pr
                join ppmb_pengawas p on p.id_pengawas = pr.id_pengawas
                join ppmb_jabatan_pengawas jp on jp.id_jabatan = pr.id_jabatan
                join ppmb_asal_pengawas a on a.id_asal = p.id_asal
                where pr.id_jadwal_ppmb = {$jp['ID_JADWAL_PPMB']}
                order by pr.id_jabatan desc");
        }
        
        $objPHPExcel = new PHPExcel();
        
        $sheet = &$objPHPExcel->setActiveSheetIndex(0);
        $sheet->setCellValue('A1', 'Kelompok');
        $sheet->setCellValue('B1', 'Ruang');
        $sheet->setCellValue('C1', 'Kapasitas');
        $sheet->setCellValue('D1', 'Nomer Awal');
        $sheet->setCellValue('E1', 'Nomer Akhir');
        $sheet->setCellValue('F1', 'Jabatan');
        $sheet->setCellValue('G1', 'Pengawas');
        $sheet->setCellValue('H1', 'Asal');
        $sheet->setCellValue('I1', 'Lokasi');
        
        $row = 2;
        foreach ($jadwal_ppmb_set as &$jp)
        {
            $sheet->setCellValue('A'.$row, $jp['NM_JURUSAN']);
            $sheet->setCellvalue('B'.$row, $jp['NM_RUANG']);
            $sheet->setCellvalue('C'.$row, $jp['KAPASITAS']);
            $sheet->setCellvalue('D'.$row, $jp['NOMER_AWAL']);
            $sheet->setCellvalue('E'.$row, $jp['NOMER_AKHIR']);
            
            if (!empty($jp['pengawas_set']))
            {
                $p = $jp['pengawas_set'][0];
                $sheet->setCellValue('F'.$row, $p['SINGKATAN_JABATAN']);
                $sheet->setCellValue('G'.$row, $p['NM_PENGAWAS']);
                $sheet->setCellValue('H'.$row, $p['NM_ASAL']);
                $sheet->setCellValue('I'.$row, $jp['LOKASI']);
                
                if (count($jp['pengawas_set']) > 1)
                {
                    for($i = 1; $i < count($jp['pengawas_set']); $i++)
                    {
                        $row++;
                        
                        $p = $jp['pengawas_set'][$i];
                        $sheet->setCellValue('F'.$row, $p['SINGKATAN_JABATAN']);
                        $sheet->setCellValue('G'.$row, $p['NM_PENGAWAS']);
                        $sheet->setCellValue('H'.$row, $p['NM_ASAL']);
                    }
                }
            }
            
            $row++;
        }
        
        // Autosize all columns
        for ($chr = ord('A'); $chr <= ord('I'); $chr++)
        {
            $sheet->getColumnDimension(chr($chr))->setAutoSize(true);
        }
        
        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="master-plot.xlsx"');
        header('Cache-Control: max-age=0');

        // 
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit();
    }
    
    function DaftarHonorarium($id_ujian)
    {
        $koordinator_set = $this->db->QueryToArray("
            select 
                ko.id_koordinator_ruang, p.nm_pengawas as nm_koordinator,
                (select max(lokasi) from jadwal_ppmb where id_koordinator_ruang = ko.id_koordinator_ruang) as lokasi,
                (select min(nomer_awal) from jadwal_ppmb where id_koordinator_ruang = ko.id_koordinator_ruang) as nomer_awal,
                (select to_char(min(tgl_test),'YYYY-MM-DD') from jadwal_ppmb where id_koordinator_ruang = ko.id_koordinator_ruang) as tgl_test
            from ppmb_koordinator_ruang ko
            join ppmb_pengawas p on p.id_pengawas = ko.id_pengawas
            where ko.id_ujian = {$id_ujian}
            order by 4");
        
        foreach ($koordinator_set as &$k)
        {
            $this->db->Query("
                select jp.id_koordinator_ruang, j.nm_jalur, p.gelombang, p.tahun, u.no_sk
                from penerimaan p
                join jalur j on j.id_jalur = p.id_jalur
                join jadwal_ppmb jp on jp.id_penerimaan = p.id_penerimaan
                join ppmb_ujian u on u.id_ujian = p.id_ujian
                where jp.id_koordinator_ruang = {$k['ID_KOORDINATOR_RUANG']} and rownum < 2");
            $k['penerimaan'] = $this->db->FetchAssoc();
            
            $k['kepala_ruang_set'] = $this->db->QueryToArray("
                select
                    p.nm_pengawas, j.nm_jabatan, js.nm_jurusan, jp.nm_ruang, p.golongan_pengawas,
                    h.besar_honor, h.pajak
                from ppmb_pengawas_ruang pr
                join ppmb_pengawas p on p.id_pengawas = pr.id_pengawas
                join ppmb_jabatan_pengawas j on j.id_jabatan = pr.id_jabatan
                join jadwal_ppmb jp on jp.id_jadwal_ppmb = pr.id_jadwal_ppmb
                join jurusan_sekolah js on js.id_jurusan = jp.kode_jalur
                join penerimaan pen on pen.id_penerimaan = jp.id_penerimaan
                left join ppmb_honor_pengawas h on h.id_ujian = pen.id_ujian and h.id_jabatan = pr.id_jabatan and h.golongan = p.golongan_pengawas and h.kode_jurusan = jp.kode_jalur
                where pr.id_jabatan = 2 and jp.id_koordinator_ruang = {$k['ID_KOORDINATOR_RUANG']}
                order by jp.nomer_awal");
            
            $k['pengawas_set'] = $this->db->QueryToArray("
                select
                    p.nm_pengawas, j.nm_jabatan, js.nm_jurusan, jp.nm_ruang, p.golongan_pengawas,
                    h.besar_honor, h.pajak
                from ppmb_pengawas_ruang pr
                join ppmb_pengawas p on p.id_pengawas = pr.id_pengawas
                join ppmb_jabatan_pengawas j on j.id_jabatan = pr.id_jabatan
                join jadwal_ppmb jp on jp.id_jadwal_ppmb = pr.id_jadwal_ppmb
                join jurusan_sekolah js on js.id_jurusan = jp.kode_jalur
                join penerimaan pen on pen.id_penerimaan = jp.id_penerimaan
                left join ppmb_honor_pengawas h on h.id_ujian = pen.id_ujian and h.id_jabatan = pr.id_jabatan and h.golongan = p.golongan_pengawas and h.kode_jurusan = jp.kode_jalur
                where pr.id_jabatan = 1 and jp.id_koordinator_ruang = {$k['ID_KOORDINATOR_RUANG']}
                order by jp.nomer_awal");
        }
            
        $objPHPExcel = new PHPExcel();
        
        $iSheet = 1;
        for ($iKoordinator = 0; $iKoordinator < count($koordinator_set); $iKoordinator++)
        {  
            $k = &$koordinator_set[$iKoordinator];
            
            // Untuk KR
            $objPHPExcel->createSheet();
            $sheet = &$objPHPExcel->getSheet($iSheet);
            $sheet->setTitle("KR_" . (($iSheet + 1) / 2));
            
            // merging cell
            $sheet->mergeCells('A1:K1');
            $sheet->mergeCells('A2:K2');
            $sheet->mergeCells('A3:K3');
            $sheet->mergeCells('A4:K4');
            $sheet->mergeCells('A5:K5');
            
            // Header
            $sheet->setCellValue('A1', 'Daftar Pembayaran Honorarium');
            $sheet->setCellValue('A2', 'Pelaksanaan Ujian Penerimaan Mahasiswa Baru Universitas Airlangga Jalur ' . $k['penerimaan']['NM_JALUR'] . ' Gel '. $k['penerimaan']['GELOMBANG'] . ' Tahun '. $k['penerimaan']['TAHUN']);
            $sheet->setCellValue('A3', 'Berdasarkan SP ' . $k['penerimaan']['NO_SK']);
            $sheet->setCellValue('A4', 'Koordinator : ' . $k['NM_KOORDINATOR']);
            $sheet->setCellValue('A5', 'Lokasi : ' . $k['LOKASI']);
            
            // Header Style
            $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');
            $sheet->getStyle('A2')->getAlignment()->setHorizontal('center');
            $sheet->getStyle('A3')->getAlignment()->setHorizontal('center');
            $sheet->getStyle('A4')->getAlignment()->setHorizontal('center');
            $sheet->getStyle('A5')->getAlignment()->setHorizontal('center');
            
            // Column Header
            $sheet->setCellValue('A6', 'NO');
            $sheet->setCellValue('B6', 'NAMA');
            $sheet->setCellValue('C6', 'TUGAS');
            $sheet->setCellValue('D6', "KELOMPOK\nUJIAN");
            $sheet->setCellValue('E6', 'RUANG');
            $sheet->setCellValue('F6', 'GOL');
            $sheet->setCellValue('G6', 'BRUTO');
            $sheet->setCellValue('H6', 'PPh 5%');
            $sheet->setCellValue('I6', 'PPh 15%');
            $sheet->setCellValue('J6', 'NETTO');
            $sheet->setCellValue('K6', 'TANDA TANGAN');
            
            $total_bruto = 0; $total_pph_5 = 0; $total_pph_15 = 0; $total_netto = 0;
            
            $iRow = 7;
            foreach ($k['kepala_ruang_set'] as $kr)
            {
                $sheet->setCellValue('A'.$iRow, $iRow - 6);
                $sheet->setCellValue('B'.$iRow, $kr['NM_PENGAWAS']);
                $sheet->setCellValue('C'.$iRow, $kr['NM_JABATAN']);
                $sheet->setCellValue('D'.$iRow, $kr['NM_JURUSAN']);
                $sheet->setCellValue('E'.$iRow, $kr['NM_RUANG']);
                $sheet->setCellValue('F'.$iRow, $kr['GOLONGAN_PENGAWAS']);
                $sheet->setCellValue('G'.$iRow, $kr['BESAR_HONOR']);
                
                $total_bruto += $kr['BESAR_HONOR'];
                
                if ($kr['PAJAK'] == 5)
                {
                    $pph_5 = $kr['BESAR_HONOR'] * $kr['PAJAK'] / 100;
                    $netto = $kr['BESAR_HONOR'] - $pph_5;
                    $total_pph_5 += $pph_5;
                    $total_netto += $netto;
                    
                    $sheet->setCellValue('H'.$iRow, $pph_5);
                }
                else if ($kr['PAJAK'] == 15)
                {
                    $pph_15 = $kr['BESAR_HONOR'] * $kr['PAJAK'] / 100;
                    $netto = $kr['BESAR_HONOR'] - $pph_15;
                    $total_pph_15 += $pph_15;
                    $total_netto += $netto;
                    
                    $sheet->setCellValue('I'.$iRow, $pph_15);
                }
                else
                {
                    $netto = $kr['BESAR_HONOR'];
                    $total_netto += $netto;
                }
                
                $sheet->setCellValue('J'.$iRow, $netto);
                $sheet->setCellValue('K'.$iRow, ($iRow - 6) . " ..........");
                
                $iRow++;
            }
            
            // Total total
            $sheet->mergeCells("A{$iRow}:F{$iRow}");
            $sheet->setCellValue('A'.$iRow, 'JUMLAH');
            $sheet->setCellValue('G'.$iRow, $total_bruto);
            $sheet->setCellValue('H'.$iRow, $total_pph_5);
            $sheet->setCellValue('I'.$iRow, $total_pph_15);
            $sheet->setCellValue('J'.$iRow, $total_netto);
            
            // Tanda tangan Ketua PPMB + Bendahara
            $sheet->setCellValue('B'.($iRow + 2),'Mengetahui,');
            $sheet->setCellValue('B'.($iRow + 3),'Ketua PPMB');
            $sheet->setCellValue('B'.($iRow + 7),'Prof. Dr. Bambang Sektiari L., drh., DEA');
            $sheet->setCellValue('B'.($iRow + 8),'NIP : 196208111989031009');
            $sheet->setCellValue('I'.($iRow + 2),'Surabaya, ' . strftime('%d %B %Y', strtotime($k['TGL_TEST'])));
            $sheet->setCellValue('I'.($iRow + 3),'Bendahara PPMB');
            $sheet->setCellValue('I'.($iRow + 7),'Izana Aprillia Revienda P, A.Md.');
            $sheet->setCellValue('I'.($iRow + 8),'NIK. 139101223');
            
            // set font style ketua + bendahara
            $sheet->getStyle('B'.($iRow + 7))->getFont()->setBold(true);
            $sheet->getStyle('B'.($iRow + 7))->getFont()->setUnderline(true);
            $sheet->getStyle('I'.($iRow + 7))->getFont()->setBold(true);
            $sheet->getStyle('I'.($iRow + 7))->getFont()->setUnderline(true);
            
            // set border
            $borderStyle = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            );
            $sheet->getStyle('A6:K'.$iRow)->applyFromArray($borderStyle);
            
            // set alignment style
            $sheet->getStyle('A6:K6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('D6')->getAlignment()->setWrapText(true);
            $sheet->getStyle('A6:K'.$iRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->getStyle('A7:A'.$iRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('C7:C'.$iRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('D7:D'.$iRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('E7:E'.$iRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('F7:F'.$iRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('K7:K'.$iRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('A'.$iRow)->getAlignment()->setHorizontal('center');
            
            // number format
            $sheet->getStyle('G7:J'.$iRow)->getNumberFormat()->setFormatCode('#,##0');
            
            // set height
            for ($iRD = 7; $iRD < $iRow; $iRD++)
            {
                $sheet->getRowDimension($iRD)->setRowHeight(30);
            }
            
            // auto column
            for ($chr = ord('A'); $chr <= ord('K'); $chr++)
            {
                if (chr($chr) != 'I')
                    $sheet->getColumnDimension(chr($chr))->setAutoSize(true);
            }
            
            $iSheet++;
            
            // Untuk Pengawas
            $objPHPExcel->createSheet();
            $sheet = &$objPHPExcel->getSheet($iSheet);
            $sheet->setTitle("P_" . ($iSheet / 2));
            
            // merging cell
            $sheet->mergeCells('A1:K1');
            $sheet->mergeCells('A2:K2');
            $sheet->mergeCells('A3:K3');
            $sheet->mergeCells('A4:K4');
            $sheet->mergeCells('A5:K5');
            
            $sheet->setCellValue('A1', 'Daftar Pembayaran Honorarium');
            $sheet->setCellValue('A2', 'Pelaksanaan Ujian Penerimaan Mahasiswa Baru Universitas Airlangga Jalur ' . $k['penerimaan']['NM_JALUR'] . ' Gel '. $k['penerimaan']['GELOMBANG'] . ' Tahun '. $k['penerimaan']['TAHUN']);
            $sheet->setCellValue('A3', 'Berdasarkan SP ' . $k['penerimaan']['NO_SK']);
            $sheet->setCellValue('A4', 'Koordinator : ' . $k['NM_KOORDINATOR']);
            $sheet->setCellValue('A5', 'Lokasi : ' . $k['LOKASI']);
            
            $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');
            $sheet->getStyle('A2')->getAlignment()->setHorizontal('center');
            $sheet->getStyle('A3')->getAlignment()->setHorizontal('center');
            $sheet->getStyle('A4')->getAlignment()->setHorizontal('center');
            $sheet->getStyle('A5')->getAlignment()->setHorizontal('center');
                      
            $sheet->setCellValue('A6', 'NO');
            $sheet->setCellValue('B6', 'NAMA');
            $sheet->setCellValue('C6', 'TUGAS');
            $sheet->setCellValue('D6', "KELOMPOK\nUJIAN");
            $sheet->setCellValue('E6', 'RUANG');
            $sheet->setCellValue('F6', 'GOL');
            $sheet->setCellValue('G6', 'BRUTO');
            $sheet->setCellValue('H6', 'PPh 5%');
            $sheet->setCellValue('I6', 'PPh 15%');
            $sheet->setCellValue('J6', 'NETTO');
            $sheet->setCellValue('K6', 'TANDA TANGAN');
            
            $total_bruto = 0; $total_pph_5 = 0; $total_pph_15 = 0; $total_netto = 0;
            
            $iRow = 7;
            foreach ($k['pengawas_set'] as $kr)
            {
                $sheet->setCellValue('A'.$iRow, $iRow - 6);
                $sheet->setCellValue('B'.$iRow, $kr['NM_PENGAWAS']);
                $sheet->setCellValue('C'.$iRow, $kr['NM_JABATAN']);
                $sheet->setCellValue('D'.$iRow, $kr['NM_JURUSAN']);
                $sheet->setCellValue('E'.$iRow, $kr['NM_RUANG']);
                $sheet->setCellValue('F'.$iRow, $kr['GOLONGAN_PENGAWAS']);
                $sheet->setCellValue('G'.$iRow, $kr['BESAR_HONOR']);
                
                $total_bruto += $kr['BESAR_HONOR'];
                
                if ($kr['PAJAK'] == 5)
                {
                    $pph_5 = $kr['BESAR_HONOR'] * $kr['PAJAK'] / 100;
                    $netto = $kr['BESAR_HONOR'] - $pph_5;
                    $total_pph_5 += $pph_5;
                    $total_netto += $netto;
                    
                    $sheet->setCellValue('H'.$iRow, $pph_5);
                }
                else if ($kr['PAJAK'] == 15)
                {
                    $pph_15 = $kr['BESAR_HONOR'] * $kr['PAJAK'] / 100;
                    $netto = $kr['BESAR_HONOR'] - $pph_15;
                    $total_pph_15 += $pph_15;
                    $total_netto += $netto;
                    
                    $sheet->setCellValue('I'.$iRow, $pph_15);
                }
                else
                {
                    $netto = $kr['BESAR_HONOR'];
                    $total_netto += $netto;
                }
                
                $sheet->setCellValue('J'.$iRow, $netto);
                $sheet->setCellValue('K'.$iRow, ($iRow - 6) . " ..........");
                
                $iRow++;
            }
            
            // Total total
            $sheet->mergeCells("A{$iRow}:F{$iRow}");
            $sheet->setCellValue('A'.$iRow, 'JUMLAH');
            $sheet->setCellValue('G'.$iRow, $total_bruto);
            $sheet->setCellValue('H'.$iRow, $total_pph_5);
            $sheet->setCellValue('I'.$iRow, $total_pph_15);
            $sheet->setCellValue('J'.$iRow, $total_netto);
            
            // Tanda tangan Ketua PPMB + Bendahara
            $sheet->setCellValue('B'.($iRow + 2),'Mengetahui,');
            $sheet->setCellValue('B'.($iRow + 3),'Ketua PPMB');
            $sheet->setCellValue('B'.($iRow + 7),'Prof. Dr. Bambang Sektiari L., drh., DEA');
            $sheet->setCellValue('B'.($iRow + 8),'NIP : 196208111989031009');
            $sheet->setCellValue('I'.($iRow + 2),'Surabaya, ' . strftime('%d %B %Y', strtotime($k['TGL_TEST'])));
            $sheet->setCellValue('I'.($iRow + 3),'Bendahara PPMB');
            $sheet->setCellValue('I'.($iRow + 7),'Sunarman, SH');
            $sheet->setCellValue('I'.($iRow + 8),'NIP : 195805141980031001');
            
            // set font style ketua + bendahara
            $sheet->getStyle('B'.($iRow + 7))->getFont()->setBold(true);
            $sheet->getStyle('B'.($iRow + 7))->getFont()->setUnderline(true);
            $sheet->getStyle('I'.($iRow + 7))->getFont()->setBold(true);
            $sheet->getStyle('I'.($iRow + 7))->getFont()->setUnderline(true);
            
            // set border
            $borderStyle = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            );
            $sheet->getStyle('A6:K'.$iRow)->applyFromArray($borderStyle);
            
            // set alignment style
            $sheet->getStyle('A6:K6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('D6')->getAlignment()->setWrapText(true);
            $sheet->getStyle('A6:K'.$iRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->getStyle('A7:A'.$iRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('C7:C'.$iRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('D7:D'.$iRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('E7:E'.$iRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('F7:F'.$iRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('K7:K'.$iRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('A'.$iRow)->getAlignment()->setHorizontal('center');
            
            // number format
            $sheet->getStyle('G7:J'.$iRow)->getNumberFormat()->setFormatCode('#,##0');
            
            // set height
            for ($iRD = 7; $iRD < $iRow; $iRD++)
            {
                $sheet->getRowDimension($iRD)->setRowHeight(30);
            }
            
            // auto column
            for ($chr = ord('A'); $chr <= ord('K'); $chr++)
            {
                if (chr($chr) != 'I')
                    $sheet->getColumnDimension(chr($chr))->setAutoSize(true);
            }
            
            $iSheet++;
            
        }
        
        // remove first
        $objPHPExcel->removeSheetByIndex(0);
        
        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="daftar-honorarium.xlsx"');
        header('Cache-Control: max-age=0');

        // 
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit();
    }
    
    function DaftarPengawasPerAsal($id_ujian)
    {
        $asal_set = $this->db->QueryToArray("
            select u.tgl_ujian, p.tahun, j.nm_jalur, p.gelombang, a.id_asal, a.nm_asal from ppmb_ujian u
            join penerimaan p on p.id_ujian = u.id_ujian
            join jalur j on j.id_jalur = p.id_jalur
            join jadwal_ppmb jp on jp.id_penerimaan = p.id_penerimaan
            join ppmb_pengawas_ruang pr on pr.id_jadwal_ppmb = jp.id_jadwal_ppmb
            join ppmb_pengawas peng on peng.id_pengawas = pr.id_pengawas
            join ppmb_asal_pengawas a on a.id_asal = peng.id_asal
            where u.id_ujian = {$id_ujian}
            group by u.tgl_ujian, p.tahun, j.nm_jalur, p.gelombang, a.id_asal, nm_asal
            order by nm_asal"); 
        
        foreach ($asal_set as &$a)
        {
            $a['pengawas_set'] = $this->db->QueryToArray("
                select peng.nm_pengawas, nm_asal from ppmb_ujian u
                join penerimaan p on p.id_ujian = u.id_ujian
                join jadwal_ppmb jp on jp.id_penerimaan = p.id_penerimaan
                join ppmb_pengawas_ruang pr on pr.id_jadwal_ppmb = jp.id_jadwal_ppmb
                join ppmb_pengawas peng on peng.id_pengawas = pr.id_pengawas
                join ppmb_asal_pengawas a on a.id_asal = peng.id_asal
                where u.id_ujian = {$id_ujian} and a.id_asal = {$a['ID_ASAL']}");
        }
        
        // create excel object
        $objPHPExcel = new PHPExcel();
        
        $iSheet = 0;
        foreach ($asal_set as &$a)
        {
            $sheet = &$objPHPExcel->getSheet($iSheet);
            $sheet->setTitle(str_replace(" ","",$a['NM_ASAL']));
            
            // Header kolom
            $sheet->setCellValue('A1', 'NO');
            $sheet->setCellValue('B1', 'NAMA');
            $sheet->setCellValue('C1', 'FAKULTAS / UNIT');
            $sheet->setCellValue('D1', 'NO TELP');
            $sheet->setCellValue('E1', 'TANDA TANGAN');
            
            // daftar pengawas
            $iRow = 0;
            foreach ($a['pengawas_set'] as $pengawas)
            {
                // Mengisi value
                $sheet->setCellValue('A'.($iRow+2), ($iRow + 1));
                $sheet->setCellValue('B'.($iRow+2), $pengawas['NM_PENGAWAS']);
                $sheet->setCellValue('C'.($iRow+2), $pengawas['NM_ASAL']);
                $sheet->setCellValue('E'.($iRow+2), ($iRow + 1) . " ...............");
                
                // Mengatur Style
                $sheet->getStyle('A'.($iRow+2))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $sheet->getStyle('A'.($iRow+2))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('B'.($iRow+2))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $sheet->getStyle('C'.($iRow+2))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $sheet->getStyle('C'.($iRow+2))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('D'.($iRow+2))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $sheet->getStyle('E'.($iRow+2))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_BOTTOM);
                $sheet->getStyle('E'.($iRow+2))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                
                // Row height
                $sheet->getRowDimension($iRow+2)->setRowHeight(25);
                
                $iRow++;
            }
            
            // Column width
            $sheet->getColumnDimension('A')->setAutoSize(true);
            $sheet->getColumnDimension('B')->setAutoSize(true);
            $sheet->getColumnDimension('C')->setAutoSize(true);
            $sheet->getColumnDimension('D')->setWidth(22);
            $sheet->getColumnDimension('E')->setWidth(20);
            
            // Border
            $borderStyle = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            );
            $sheet->getStyle('A1:E'.($iRow+1))->applyFromArray($borderStyle);
            
            $iSheet++;
            if ($iSheet <= count($asal_set) - 1) { $objPHPExcel->createSheet(); }
        }
        
        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="daftar-pengawas-per-asal.xlsx"');
        header('Cache-Control: max-age=0');

        // excel file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit();
    }
    
    function DaftarPengawasMailMerge($id_ujian)
    {
        $data_set = $this->db->QueryToArray("
            select
                j.nm_jalur, p.gelombang, p.tahun, (p.tahun+1) as tahun2, to_char(u.tgl_ujian,'YYYY-MM-DD') as tgl_ujian,
                peng.nm_pengawas, jab.nm_jabatan, js.nm_jurusan,
                jp.nm_ruang, jp.lokasi
            from ppmb_ujian u
            join penerimaan p on p.id_ujian = u.id_ujian
            join jalur j on j.id_jalur = p.id_jalur
            left join jadwal_ppmb jp on jp.id_penerimaan = p.id_penerimaan
            left join ppmb_pengawas_ruang pr on pr.id_jadwal_ppmb = jp.id_jadwal_ppmb
            left join ppmb_pengawas peng on peng.id_pengawas = pr.id_pengawas
            left join ppmb_jabatan_pengawas jab on jab.id_jabatan = pr.id_jabatan
            left join jurusan_sekolah js on js.id_jurusan = jp.kode_jalur
            where u.id_ujian = {$id_ujian}
            order by jp.nomer_awal, pr.id_jabatan desc");
            
        $objPHPExcel = new PHPExcel();
        $sheet = &$objPHPExcel->getActiveSheet();
        
        // header
        $sheet->setCellValue('A1', 'Penerimaan');
        $sheet->setCellValue('B1', 'Pengawas');
        $sheet->setCellValue('C1', 'Hari, Tanggal');
        $sheet->setCellValue('D1', 'Tugas');
        $sheet->setCellValue('E1', 'Kelompok');
        $sheet->setCellValue('F1', 'Ruang');
        $sheet->setCellValue('G1', 'Lokasi');
        
        // looping data
        $iRow = 2;
        foreach ($data_set as $d)
        {
            $sheet->setCellValue('A'.$iRow, "JALUR {$d['NM_JALUR']} GELOMBANG {$d['GELOMBANG']} TAHUN AKADEMIK {$d['TAHUN']}/{$d['TAHUN2']}");
            $sheet->setCellValue('B'.$iRow, $d['NM_PENGAWAS']);
            $sheet->setCellValue('C'.$iRow, strftime('%d %B %Y', strtotime($d['TGL_UJIAN'])));
            $sheet->setCellValue('D'.$iRow, $d['NM_JABATAN']);
            $sheet->setCellValue('E'.$iRow, $d['NM_JURUSAN']);
            $sheet->setCellValue('F'.$iRow, $d['NM_RUANG']);
            $sheet->setCellValue('G'.$iRow, $d['LOKASI']);
            
            $iRow++;
        }
        
        // auto-width
        for ($chr = ord('A'); $chr <= ord('G'); $chr++)
            $sheet->getColumnDimension(chr($chr))->setAutoSize(true);
        
        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="daftar-pengawas-mail-merge.xlsx"');
        header('Cache-Control: max-age=0');

        // excel file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit();
    }
    
    function RayonisasiPengawas($id_ujian)
    {
        $koordinator_set = $this->db->QueryToArray("
            select jp.*, pp.nm_pengawas as nm_koordinator
            from ppmb_koordinator_ruang pkr
            join ppmb_pengawas pp on pp.id_pengawas = pkr.id_pengawas
            join (
                select jp.id_koordinator_ruang, j.nm_jalur, p.gelombang, js.nm_jurusan, f.nm_fakultas, jp.alamat, min(jp.nomer_awal) as nomer_awal
                from jadwal_ppmb jp
                join fakultas f on f.id_fakultas = jp.id_fakultas
                join penerimaan p on p.id_penerimaan =jp.id_penerimaan
                join jalur j on j.id_jalur = p.id_jalur
                join jurusan_sekolah js on js.id_jurusan = jp.kode_jalur
                where jp.id_koordinator_ruang is not null
                group by jp.id_koordinator_ruang, j.nm_jalur, p.gelombang, js.nm_jurusan, f.nm_fakultas, jp.alamat) jp on jp.id_koordinator_ruang = pkr.id_koordinator_ruang
            where pkr.id_ujian = {$id_ujian}
            order by jp.nomer_awal");
            
        // Ruangan
        foreach ($koordinator_set as &$k)
        {
            $k['ruang_set'] = $this->db->QueryToArray("
                select jp.id_jadwal_ppmb, jp.nm_ruang, jp.kapasitas, jp.nomer_awal, (jp.nomer_awal+jp.kapasitas - 1) as nomer_akhir
                from jadwal_ppmb jp
                where jp.id_koordinator_ruang = {$k['ID_KOORDINATOR_RUANG']}
                order by jp.nomer_awal");
                
            // Pengawasnya
            foreach ($k['ruang_set'] as &$r)
            {
                $r['pengawas_set'] = $this->db->QueryToArray("
                    select p.nm_pengawas, jab.singkatan_jabatan, a.nm_asal
                    from ppmb_pengawas_ruang pr
                    join ppmb_pengawas p on p.id_pengawas = pr.id_pengawas
                    join ppmb_jabatan_pengawas jab on jab.id_jabatan = pr.id_jabatan
                    join ppmb_asal_pengawas a on a.id_asal = p.id_asal
                    where pr.id_jadwal_ppmb = {$r['ID_JADWAL_PPMB']}
                    order by pr.id_jabatan desc");
            }
        }
        
        $objPHPExcel = new PHPExcel();
        
        $iSheet = 1;
        for ($iKoordinator = 0; $iKoordinator < count($koordinator_set); $iKoordinator++)
        {           
            // melakukan pengaturan sheet
            if ($iKoordinator > 0) { $objPHPExcel->createSheet(); }
            $sheet = &$objPHPExcel->getSheet($iKoordinator);
            $sheet->setTitle("K{$iSheet}");
            
            // get row
            $k = &$koordinator_set[$iKoordinator];
            
            // main header
            $sheet->setCellValue('A1', "LOKASI UJIAN {$k['NM_JALUR']}-{$k['GELOMBANG']} {$k['NM_JURUSAN']}");
            $sheet->mergeCells('A1:F1');
            $sheet->getStyle('A1')->getFont()->setSize(16)->setBold(true);
            $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');
            
            // Informasi koordinator kiri
            $sheet->setCellValue('A2', 'Nomor Lokasi');
            $sheet->setCellValue('A3', 'Nama Lokasi');
            $sheet->setCellValue('A4', 'Alamat');
            $sheet->setCellValue('A5', 'Jumlah Koordinator');
            $sheet->setCellValue('A6', 'Jumlah Peserta');
            $sheet->setCellValue('A8', 'Nomor Koordinator');
            $sheet->setCellValue('A9', 'Ruang Koordinator');
            $sheet->setCellValue('A10', 'Nama Koordinator');
            
            // Informasi koordinator kanan
            $sheet->setCellValue('B2', ": ");
            $sheet->setCellValue('B3', ": {$k['NM_FAKULTAS']}");
            $sheet->setCellValue('B4', ": {$k['ALAMAT']}");
            $sheet->setCellValue('B5', ": ");
            $sheet->setCellValue('B6', ": ");
            $sheet->setCellValue('B8', ": ");
            $sheet->setCellValue('B9', ": ");
            $sheet->setCellValue('B10', ": {$k['NM_KOORDINATOR']}");
            
            // Header
            $sheet->setCellValue('A12', 'RUANG');
            $sheet->setCellValue('B12', 'PESERTA');
            $sheet->setCellValue('D12', 'PENGAWAS');
            $sheet->setCellValue('F12', 'ASAL');
            $sheet->setCellValue('B13', 'JUMLAH');
            $sheet->setCellValue('C13', 'NOMOR');
            
            // merging header
            $sheet->mergeCells('A12:A13');
            $sheet->mergeCells('B12:C12');
            $sheet->mergeCells('D12:E13');
            $sheet->mergeCells('F12:F13');
            
            // aligment header
            $sheet->getStyle('A12')->getAlignment()->setHorizontal('center')->setVertical('center');
            $sheet->getStyle('B12')->getAlignment()->setHorizontal('center')->setVertical('center');
            $sheet->getStyle('D12')->getAlignment()->setHorizontal('center')->setVertical('center');
            $sheet->getStyle('F12')->getAlignment()->setHorizontal('center')->setVertical('center');
            
            // penghitungan2
            $jumlah_peserta = 0;
            $nomer_awal = "";
            $nomer_akhir = "";
            $total_pengawas = 0;
            $total_kr = 0;
            $total_p = 0;
            
            $iRow = 14;
            foreach ($k['ruang_set'] as $r)
            {
                // mengambil nomer awal
                if ($iRow == 14) { $nomer_awal = $r['NOMER_AWAL']; }
                $jumlah_peserta += $r['KAPASITAS'];
                $total_pengawas += count($r['pengawas_set']);
                
                // data ruang
                $sheet->setCellValue('A'.$iRow, $r['NM_RUANG']);
                $sheet->setCellValue('B'.$iRow, $r['KAPASITAS']);
                $sheet->setCellValue('C'.$iRow, "{$r['NOMER_AWAL']} s/d {$r['NOMER_AKHIR']}");
                
                // merging
                $next_merge = $iRow + count($r['pengawas_set']) - 1;
                $sheet->mergeCells('A'.$iRow.':A'.$next_merge);
                $sheet->mergeCells('B'.$iRow.':B'.$next_merge);
                $sheet->mergeCells('C'.$iRow.':C'.$next_merge);
                
                // aligment
                $sheet->getStyle('A'.$iRow)->getAlignment()->setHorizontal('center')->setVertical('center');
                $sheet->getStyle('B'.$iRow)->getAlignment()->setHorizontal('center')->setVertical('center');
                $sheet->getStyle('C'.$iRow)->getAlignment()->setHorizontal('center')->setVertical('center');
                
                // pengawas
                for ($iPengawas = 0; $iPengawas < count($r['pengawas_set']); $iPengawas++)
                {
                    $p = $r['pengawas_set'][$iPengawas];
                    
                    if ($p['SINGKATAN_JABATAN'] == 'KR') { $total_kr += 1; }
                    if ($p['SINGKATAN_JABATAN'] == 'P') { $total_p += 1; }
                    
                    $sheet->setCellValue('D'.$iRow, $p['NM_PENGAWAS']);
                    $sheet->setCellValue('E'.$iRow, $p['SINGKATAN_JABATAN']);
                    $sheet->setCellValue('F'.$iRow, $p['NM_ASAL']);
                    
                    $iRow++;
                }
                
                $nomer_akhir = $r['NOMER_AKHIR'];
                
                if (count($r['pengawas_set']) <= 1) { $iRow++; }
            }
            
            // isi
            $sheet->setCellValue('A'.$iRow, "Jumlah Peserta : {$jumlah_peserta} ({$nomer_awal} s/d {$nomer_akhir})       Jumlah Pengawas : {$total_pengawas} ( {$total_kr} KR + {$total_p} P )");
            $sheet->setCellValue('A'.($iRow+1), "KR : Kepala Ruang                P : Pengawas");
            
            // Border
            $borderStyle = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            );
            $sheet->getStyle('A12:F'.$iRow)->applyFromArray($borderStyle);
            
            // Width
            $sheet->getColumnDimension('A')->setWidth(18);
            $sheet->getColumnDimension('C')->setWidth(27);
            $sheet->getColumnDimension('D')->setAutoSize(true);
            $sheet->getColumnDimension('E')->setAutoSize(true);
            $sheet->getColumnDimension('F')->setAutoSize(true);
            
            $iSheet++;
        }
        
        // set active sheet
        $objPHPExcel->setActiveSheetIndex(0);
        
        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="rayonisasi-pengawas.xlsx"');
        header('Cache-Control: max-age=0');

        // excel file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit();
    }
}
?>