<?php
/**
 * Description of Penetapan
 *
 * @author Fathoni
 */
class Penetapan
{
	private $db;
	
	function __construct(MyOracle $myoracle)
	{
		$this->db = $myoracle;
	}
	
	function GetProgramStudi($id_program_studi)
	{
		$this->db->Query("
			select id_program_studi, nm_program_studi, nm_jenjang, ps.id_jenjang
			from program_studi ps join jenjang j on j.id_jenjang = ps.id_jenjang
			where id_program_studi = {$id_program_studi}");
		return $this->db->FetchAssoc();
	}
	
	function GetPenerimaan($id_penerimaan)
	{
		$this->db->Query("select * from penerimaan where id_penerimaan = {$id_penerimaan}");
		return $this->db->FetchAssoc();
	}
	
	function GetListPenetapan()
	{
		return $this->db->QueryToArray("select * from penetapan order by periode desc");
	}
	
	function GetPenetapan($id_penetapan)
	{
		$this->db->Query("select * from penetapan where id_penetapan = {$id_penetapan}");
		return $this->db->FetchAssoc();
	}
	
	function GetListPenerimaanForPenetapan($id_penetapan)
	{
		$penerimaan_set = $this->db->QueryToArray("
			select pp.id_penetapan_penerimaan, p.nm_penerimaan, p.gelombang, p.semester, p.tahun from penetapan_penerimaan pp
			join penerimaan p on p.id_penerimaan = pp.id_penerimaan
			where pp.id_penetapan = {$id_penetapan}
			order by p.id_jenjang, p.tahun, p.semester, p.gelombang, p.nm_penerimaan");
		
		for ($i = 0; $i < count($penerimaan_set); $i++)
		{
			$penerimaan_set[$i]['program_studi_set'] = $this->db->QueryToArray("
				select
					pp.id_penetapan_penerimaan, ps.id_program_studi, f.singkatan_fakultas||' - '||substr(j.nm_jenjang,1,2)||' '||ps.nm_program_studi as nm_program_studi,
					(select count(*) from calon_mahasiswa_baru cmb where cmb.id_penerimaan = pps.id_penerimaan and cmb.id_pilihan_1 = ps.id_program_studi and no_ujian is not null and tgl_verifikasi_ppmb is not null) as jumlah_peminat,
					(select count(*) from calon_mahasiswa_baru cmb where cmb.id_penerimaan = pps.id_penerimaan and cmb.id_program_studi = ps.id_program_studi and tgl_diterima is not null) as jumlah_diterima,
					(select kuota from statistik_cmhs sc where sc.id_penerimaan = pps.id_penerimaan and sc.id_program_studi = pps.id_program_studi) as jumlah_kuota
				from penetapan_penerimaan pp
				join penerimaan_prodi pps on pps.id_penerimaan = pp.id_penerimaan
				join program_studi ps on ps.id_program_studi = pps.id_program_studi
				join jenjang j on j.id_jenjang = ps.id_jenjang
				join fakultas f on f.id_fakultas = ps.id_fakultas
				where pp.id_penetapan = {$id_penetapan} and pp.id_penetapan_penerimaan = {$penerimaan_set[$i]['ID_PENETAPAN_PENERIMAAN']} and pps.is_aktif = 1
				order by ps.id_fakultas, ps.nm_program_studi");
		}
		
		return $penerimaan_set;
	}
	
	function GetPenetapanPenerimaan($id_penetapan_penerimaan)
	{
		$this->db->Query("
			select p.id_penetapan, p.nm_penetapan, pp.id_penerimaan, p.tgl_penetapan from penetapan_penerimaan pp
			join penetapan p on p.id_penetapan = pp.id_penetapan
			where id_penetapan_penerimaan = {$id_penetapan_penerimaan}");
		return $this->db->FetchAssoc();
	}
	
	function GetListCalonMahasiswaPasca($id_program_studi, $id_penerimaan)
	{
		$cmb_set =  $this->db->QueryToArray("
			select cmb.id_c_mhs, no_ujian, nm_c_mhs, gelar, tgl_diterima, n.*
			from calon_mahasiswa_baru cmb
			join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
			left join nilai_cmhs_pasca n on n.id_c_mhs = cmb.id_c_mhs
			where 
				id_pilihan_1 = {$id_program_studi} and 
				p.id_penerimaan = {$id_penerimaan} and 
				cmb.tgl_verifikasi_ppmb is not null and 
				cmb.no_ujian not in ('51160507')
			order by n.total_nilai desc, n.nilai_tpa desc");

		for ($i = 0; $i < count($cmb_set); $i++)
		{
			$cmb_set[$i]['NILAI_TPA']			= number_format($cmb_set[$i]['NILAI_TPA'], 2);
			$cmb_set[$i]['NILAI_INGGRIS']		= number_format($cmb_set[$i]['NILAI_INGGRIS'], 2);
			$cmb_set[$i]['NILAI_ILMU']			= number_format($cmb_set[$i]['NILAI_ILMU'], 2);
			$cmb_set[$i]['NILAI_WAWANCARA']		= number_format($cmb_set[$i]['NILAI_WAWANCARA'], 2);
			$cmb_set[$i]['NILAI_IPK']			= number_format($cmb_set[$i]['NILAI_IPK'], 2);
			$cmb_set[$i]['NILAI_KARYA_ILMIAH']	= number_format($cmb_set[$i]['NILAI_KARYA_ILMIAH'], 2);
			$cmb_set[$i]['NILAI_REKOMENDASI']	= number_format($cmb_set[$i]['NILAI_REKOMENDASI'], 2);
			$cmb_set[$i]['NILAI_MATRIKULASI']	= number_format($cmb_set[$i]['NILAI_MATRIKULASI'], 2);
			$cmb_set[$i]['NILAI_PSIKO']			= number_format($cmb_set[$i]['NILAI_PSIKO'], 2);
			$cmb_set[$i]['NILAI_TULIS']			= number_format($cmb_set[$i]['NILAI_TULIS'], 2);
			$cmb_set[$i]['NILAI_LAIN']			= number_format($cmb_set[$i]['NILAI_LAIN'], 2);
			$cmb_set[$i]['NILAI_GAB_PPDS']		= number_format($cmb_set[$i]['NILAI_GAB_PPDS'], 2);
			$cmb_set[$i]['TOTAL_NILAI']			= number_format($cmb_set[$i]['TOTAL_NILAI'], 2);
			
			// Tambahan baru untuk PPDS 2014
			$cmb_set[$i]['NILAI_TPA_MENTAH']		= number_format($cmb_set[$i]['NILAI_TPA_MENTAH'], 2);
			$cmb_set[$i]['NILAI_INGGRIS_MENTAH']	= number_format($cmb_set[$i]['NILAI_INGGRIS_MENTAH'], 2);
			$cmb_set[$i]['NILAI_BAKORDIK_MENTAH']	= number_format($cmb_set[$i]['NILAI_BAKORDIK_MENTAH'], 2);
			$cmb_set[$i]['NILAI_BAKORDIK_TERBOBOT']	= number_format($cmb_set[$i]['NILAI_BAKORDIK_TERBOBOT'], 2);
			$cmb_set[$i]['TOTAL_NILAI_PPDS']		= number_format($cmb_set[$i]['TOTAL_NILAI_PPDS'], 2);
			
			// S1-Alih Jenis
			// $cmb_set[$i]['NILAI_PRESTASI']			= number_format($cmb_set[$i]['NILAI_PRESTASI'], 2);
			$cmb_set[$i]['NILAI_AKREDITASI']		= number_format($cmb_set[$i]['NILAI_AKREDITASI'], 2);

			// S1-PBSB
			// $cmb_set[$i]['NILAI_KESANTRIAN']		= number_format($cmb_set[$i]['NILAI_KESANTRIAN'], 2);
		}

		return $cmb_set;
	}
	
	/**
	 * Versi lama
	 * @param int $id_program_studi
	 * @param int $id_penerimaan
	 * @param int $pilihan
	 * @return array
	 */
	function GetListCalonMahasiswaMandiriOld($id_program_studi, $id_penerimaan, $pilihan = 1)
	{
		$cmb_set = $this->db->QueryToArray("
			select
				cmb.id_c_mhs, cmb.no_ujian, upper(nm_c_mhs) as nm_c_mhs, n.nilai_tpa, n.nilai_prestasi, (n.nilai_tpa + n.nilai_prestasi) as total_nilai,
				cmb.id_program_studi, id_pilihan_1, id_pilihan_2, id_pilihan_3, id_pilihan_4, tgl_diterima
			from calon_mahasiswa_baru cmb
			join nilai_cmhs n on n.id_c_mhs = cmb.id_c_mhs
			join program_studi ps on ps.id_program_studi = cmb.id_pilihan_{$pilihan} and ps.jurusan_sekolah = n.jurusan_sekolah
			where tgl_verifikasi_ppmb is not null and id_penerimaan = {$id_penerimaan} and id_pilihan_{$pilihan} = {$id_program_studi}
			order by (nilai_tpa + nilai_prestasi) desc, nilai_tpa desc, nilai_prestasi desc");
			
		foreach ($cmb_set as &$cmb)
		{
			// Jika diterima
			if ($cmb['TGL_DITERIMA'] != '')
			{
				$cmb['POSISI_DITERIMA'] = '';
				
				if ($cmb['ID_PROGRAM_STUDI'] == $cmb['ID_PILIHAN_1'] && $pilihan != 1)
				{
					$cmb['POSISI_DITERIMA'] = 'P1';
				}
				else if ($cmb['ID_PROGRAM_STUDI'] == $cmb['ID_PILIHAN_2'] && $pilihan != 2)
				{
					$cmb['POSISI_DITERIMA'] = 'P2';
				}
				else if ($cmb['ID_PROGRAM_STUDI'] == $cmb['ID_PILIHAN_3'] && $pilihan != 3)
				{
					$cmb['POSISI_DITERIMA'] = 'P3';
				}
				else if ($cmb['ID_PROGRAM_STUDI'] == $cmb['ID_PILIHAN_4'] && $pilihan != 4)
				{
					$cmb['POSISI_DITERIMA'] = 'P4';
				}
			}
		}
		
		return $cmb_set;
	}
	
	/**
	 * Versi terbaru list calon mahasiswa, model IPC di pecah
	 * Modified : 11 Juli 2013
	 *  - Perhitungan Nilai BL masuk
	 * @since 2013
	 * @param int $id_program_studi
	 * @param int $id_penerimaan
	 * @param int $pilihan
	 */
	function GetListCalonMahasiswaMandiri($id_program_studi, $id_penerimaan, $pilihan = 1)
	{
		$bobot_skor_tulis	= "1.0";  //dibuat string biar titik
		$bobot_skor_bl		= "0.0";
		
		$cmb_set = $this->db->QueryToArray("
			select
				cmb.id_c_mhs, v.kode_jurusan, cmb.no_ujian, nm_c_mhs,
				n.nilai_tpa, n.nilai_prestasi, (((n.nilai_tpa + n.nilai_prestasi)*{$bobot_skor_tulis}) + (n.nilai_bl*{$bobot_skor_bl})) as total_nilai,
				cmb.id_program_studi, id_pilihan_1, id_pilihan_2, id_pilihan_3, id_pilihan_4, 
				tgl_diterima,
				ps.nm_program_studi as nm_pilihan_diterima,
				ps1.nm_program_studi as nm_pilihan_1,
				ps2.nm_program_studi as nm_pilihan_2,
				ps3.nm_program_studi as nm_pilihan_3,
				ps4.nm_program_studi as nm_pilihan_4,
				pd.nm_penerimaan || ' ' || pd.tahun as nm_penerimaan_lain
			from calon_mahasiswa_baru cmb
			left join nilai_cmhs n on n.id_c_mhs = cmb.id_c_mhs
			join calon_mahasiswa_pilihan cmp on cmp.id_c_mhs = cmb.id_c_mhs and cmp.jurusan_sekolah = n.jurusan_sekolah
			join voucher v on v.kode_voucher = cmb.kode_voucher
			left join program_studi ps on ps.id_program_studi = cmb.id_program_studi
			left join program_studi ps1 on ps1.id_program_studi = cmb.id_pilihan_1
			left join program_studi ps2 on ps2.id_program_studi = cmb.id_pilihan_2
			left join program_studi ps3 on ps3.id_program_studi = cmb.id_pilihan_3
			left join program_studi ps4 on ps4.id_program_studi = cmb.id_pilihan_4
			left join penerimaan pd on pd.id_penerimaan = cmb.id_penerimaan_diterima_lain
			where
				cmb.no_ujian is not null and
				cmb.tgl_verifikasi_ppmb is not null and 
				cmb.id_penerimaan = {$id_penerimaan} and
				cmp.pilihan_ke = {$pilihan} and
				cmp.id_pilihan = {$id_program_studi}
			order by 7 desc, nilai_tpa desc, nilai_prestasi desc");
		
		// Proses informasi diterima
		foreach ($cmb_set as &$cmb)
		{	
			// Title case
			$cmb['NM_PILIHAN_1'] = ucwords(strtolower($cmb['NM_PILIHAN_1']));
			$cmb['NM_PILIHAN_2'] = ucwords(strtolower($cmb['NM_PILIHAN_2']));
			$cmb['NM_PILIHAN_3'] = ucwords(strtolower($cmb['NM_PILIHAN_3']));
			$cmb['NM_PILIHAN_4'] = ucwords(strtolower($cmb['NM_PILIHAN_4']));
			
			// Jika diterima
			if ($cmb['TGL_DITERIMA'] != '')
			{
				$cmb['POSISI_DITERIMA'] = '';
				
				if ($cmb['ID_PROGRAM_STUDI'] == $cmb['ID_PILIHAN_1'] && $pilihan != 1)
				{
					$cmb['POSISI_DITERIMA'] = 'P1';
				}
				else if ($cmb['ID_PROGRAM_STUDI'] == $cmb['ID_PILIHAN_2'] && $pilihan != 2)
				{
					$cmb['POSISI_DITERIMA'] = 'P2';
				}
				else if ($cmb['ID_PROGRAM_STUDI'] == $cmb['ID_PILIHAN_3'] && $pilihan != 3)
				{
					$cmb['POSISI_DITERIMA'] = 'P3';
				}
				else if ($cmb['ID_PROGRAM_STUDI'] == $cmb['ID_PILIHAN_4'] && $pilihan != 4)
				{
					$cmb['POSISI_DITERIMA'] = 'P4';
				}
			}
		}
		
		return $cmb_set;
	}

	function GetListCalonMahasiswaPBSB($id_program_studi, $id_penerimaan, $pilihan = 1)
	{
		$cmb_set = $this->db->QueryToArray(
			"select
				cmb.id_c_mhs, cmb.kode_jurusan, cmb.no_ujian, nm_c_mhs,
				cmb.id_program_studi, id_pilihan_1, id_pilihan_2, id_pilihan_3, id_pilihan_4, tgl_diterima,
				ps.nm_program_studi as nm_pilihan_diterima,
				ps1.nm_program_studi as nm_pilihan_1, 
				ps2.nm_program_studi as nm_pilihan_2, 
				ps3.nm_program_studi as nm_pilihan_3, 
				ps4.nm_program_studi as nm_pilihan_4,
				n.nilai_tpa, n.nilai_prestasi, n.nilai_kesantrian, n.total_nilai
			from calon_mahasiswa_baru cmb
			left join nilai_cmhs_pasca n on n.id_c_mhs = cmb.id_c_mhs
			left join program_studi ps on ps.id_program_studi = cmb.id_program_studi
			left join program_studi ps1 on ps1.id_program_studi = cmb.id_pilihan_1
			left join program_studi ps2 on ps2.id_program_studi = cmb.id_pilihan_2
			left join program_studi ps3 on ps3.id_program_studi = cmb.id_pilihan_3
			left join program_studi ps4 on ps4.id_program_studi = cmb.id_pilihan_4
			where 
				cmb.no_ujian is not null and
				cmb.tgl_verifikasi_ppmb is not null and 
				cmb.id_penerimaan = {$id_penerimaan} and
				cmb.id_pilihan_{$pilihan} = {$id_program_studi}
			order by n.total_nilai desc");
		
		// Proses informasi diterima
		foreach ($cmb_set as &$cmb)
		{	
			// Title case
			$cmb['NM_PILIHAN_1'] = ucwords(strtolower($cmb['NM_PILIHAN_1']));
			$cmb['NM_PILIHAN_2'] = ucwords(strtolower($cmb['NM_PILIHAN_2']));
			$cmb['NM_PILIHAN_3'] = ucwords(strtolower($cmb['NM_PILIHAN_3']));
			$cmb['NM_PILIHAN_4'] = ucwords(strtolower($cmb['NM_PILIHAN_4']));
			
			// Jika diterima
			if ($cmb['TGL_DITERIMA'] != '')
			{
				$cmb['POSISI_DITERIMA'] = '';
				
				if ($cmb['ID_PROGRAM_STUDI'] == $cmb['ID_PILIHAN_1'] && $pilihan != 1)
				{
					$cmb['POSISI_DITERIMA'] = 'P1';
				}
				else if ($cmb['ID_PROGRAM_STUDI'] == $cmb['ID_PILIHAN_2'] && $pilihan != 2)
				{
					$cmb['POSISI_DITERIMA'] = 'P2';
				}
				else if ($cmb['ID_PROGRAM_STUDI'] == $cmb['ID_PILIHAN_3'] && $pilihan != 3)
				{
					$cmb['POSISI_DITERIMA'] = 'P3';
				}
				else if ($cmb['ID_PROGRAM_STUDI'] == $cmb['ID_PILIHAN_4'] && $pilihan != 4)
				{
					$cmb['POSISI_DITERIMA'] = 'P4';
				}
			}
		}

		return $cmb_set;
	}
	
	function GetStatistik($id_penerimaan, $id_program_studi)
	{
		$this->db->Query("
			select s1.id_program_studi, s1.nilai_min, s1.nilai_max, s1.nilai_rata, s1.standar_deviasi, (s1.nilai_rata - s1.standar_deviasi) passing_grade, s1.kuota, s1.kuota_total,
			(select count(id_c_mhs) from calon_mahasiswa_baru cmb where cmb.id_penerimaan = s1.id_penerimaan and cmb.id_pilihan_1 = s1.id_program_studi) peminat,
			s2.nilai_min nilai_min2, s2.nilai_max nilai_max2, s2.nilai_rata nilai_rata2, s2.standar_deviasi standar_deviasi2, (s2.nilai_rata - s2.standar_deviasi) passing_grade2, s2.kuota kuota2,
			(select count(id_c_mhs) from calon_mahasiswa_baru cmb where cmb.id_penerimaan = s2.id_penerimaan and cmb.id_pilihan_1 = s2.id_program_studi) peminat2
			from statistik_cmhs s1
			left join statistik_cmhs s2 on s2.id_statistik_cmhs = s1.id_statistik_sebelumnya
			where s1.id_penerimaan = {$id_penerimaan} and s1.id_program_studi = {$id_program_studi}");
		$statistik = $this->db->FetchAssoc();

		// formating statistik
		$statistik['NILAI_MIN'] = number_format($statistik['NILAI_MIN'], 2);
		$statistik['NILAI_RATA'] = number_format($statistik['NILAI_RATA'], 2);
		$statistik['NILAI_MAX'] = number_format($statistik['NILAI_MAX'], 2);
		$statistik['STANDAR_DEVIASI'] = number_format($statistik['STANDAR_DEVIASI'], 2);
		$statistik['PASSING_GRADE'] = number_format($statistik['PASSING_GRADE'], 2);

		if ($id_program_studi == 137)
		{
			$statistik['PASSING_GRADE'] = number_format($statistik['NILAI_RATA'], 2);
		}
		
		return $statistik;
	}
	
	function GetCalonMahasiswa($id_c_mhs)
	{
		$this->db->Query("
			SELECT
				CMB.ID_C_MHS, CMB.NO_UJIAN, UPPER(CMB.NM_C_MHS) AS NM_C_MHS, CMB.GELAR, CMB.ALAMAT, P.ID_JENJANG, P.TAHUN, P.GELOMBANG, P.SEMESTER,
				K.NM_KOTA AS NM_KOTA_LAHIR, CMB.TGL_LAHIR,
				'' AS FILE_FOTO,
				CMB.ID_PILIHAN_1, CMB.ID_PILIHAN_2, CMB.ID_PILIHAN_3, CMB.ID_PILIHAN_4, CMB.ID_PROGRAM_STUDI, PS1.ID_JENJANG,
				PS1.NM_PROGRAM_STUDI AS PS1, PS2.NM_PROGRAM_STUDI AS PS2, PS3.NM_PROGRAM_STUDI AS PS3, PS4.NM_PROGRAM_STUDI AS PS4, PS1.ID_JENJANG,
				CMP.PTN_S1, CMP.IP_S1, CMP.PRODI_S1, CMP.PTN_S2, CMP.IP_S2, CMP.PRODI_S2,
				N.NILAI_TPA, N.NILAI_INGGRIS, N.NILAI_WAWANCARA, N.NILAI_IPK, N.NILAI_KARYA_ILMIAH, N.NILAI_REKOMENDASI, N.NILAI_ILMU, N.NILAI_PSIKO, N.TOTAL_NILAI,
				CMD.BERKAS_IJAZAH
			FROM CALON_MAHASISWA_BARU CMB
			LEFT JOIN AGAMA A ON A.ID_AGAMA = CMB.ID_AGAMA
			LEFT JOIN KOTA K ON K.ID_KOTA = CMB.ID_KOTA_LAHIR
			LEFT JOIN PROGRAM_STUDI PS1 ON PS1.ID_PROGRAM_STUDI = CMB.ID_PILIHAN_1
			LEFT JOIN PROGRAM_STUDI PS2 ON PS2.ID_PROGRAM_STUDI = CMB.ID_PILIHAN_2
			LEFT JOIN PROGRAM_STUDI PS3 ON PS3.ID_PROGRAM_STUDI = CMB.ID_PILIHAN_3
			LEFT JOIN PROGRAM_STUDI PS4 ON PS4.ID_PROGRAM_STUDI = CMB.ID_PILIHAN_4
			LEFT JOIN CALON_MAHASISWA_PASCA CMP ON CMP.ID_C_MHS = CMB.ID_C_MHS
			LEFT JOIN NILAI_CMHS_PASCA N ON N.ID_C_MHS = CMB.ID_C_MHS
			LEFT JOIN PENERIMAAN P ON P.ID_PENERIMAAN = CMB.ID_PENERIMAAN
			LEFT JOIN CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS = CMB.ID_C_MHS
			WHERE CMB.ID_C_MHS = {$id_c_mhs}");
		return $this->db->FetchAssoc();
	}
	
	function GetListMandiriFindByName($nm_c_mhs, $id_penetapan)
	{
		$nm_c_mhs = strtoupper($nm_c_mhs);
		
		if ($nm_c_mhs != '')
		{
			return $this->db->QueryToArray("
				select * from (
					select cmb.id_c_mhs, cmb.no_ujian, nm_c_mhs, ps.nm_program_studi,
						rank() over (partition by id_pilihan_1 order by (nilai_tpa + nilai_prestasi) desc, nilai_tpa desc, nilai_prestasi desc, cmb.no_ujian) urutan,
						pp.id_penetapan_penerimaan, id_pilihan_1, nilai_tpa, nilai_prestasi, sc.kuota, sc.nilai_rata, sc.standar_deviasi
					from calon_mahasiswa_baru cmb
					join nilai_cmhs n on n.id_c_mhs = cmb.id_c_mhs
					join program_studi ps on ps.id_program_studi = cmb.id_pilihan_1 and ps.jurusan_sekolah = n.jurusan_sekolah
					join penetapan_penerimaan pp on pp.id_penerimaan = cmb.id_penerimaan
					join statistik_cmhs sc on sc.id_penerimaan = cmb.id_penerimaan and sc.id_program_studi = cmb.id_pilihan_1
					where tgl_verifikasi_ppmb is not null and cmb.id_penerimaan in (select id_penerimaan from penetapan_penerimaan where id_penetapan = {$id_penetapan})) t
				where upper(t.nm_c_mhs) like '%{$nm_c_mhs}%' or t.no_ujian like '%{$nm_c_mhs}%'
				order by 3");
		}
		else
			return null;
	}
	
	function GetListRekapPresensi($id_penetapan)
	{
		return $this->db->QueryToArray("
			select su.id_status_ujian, nm_status_ujian, count(id_c_mhs) as jumlah from calon_mahasiswa_baru cmb
			join status_ujian su on su.id_status_ujian = cmb.status_ujian
			where cmb.id_penerimaan in (select id_penerimaan from penetapan_penerimaan where id_penetapan = {$id_penetapan}) and
			  cmb.tgl_verifikasi_ppmb is not null
			group by id_status_ujian, nm_status_ujian
			order by id_status_ujian");
	}
	
	function GetListDetailPresensi($id_penetapan, $status_ujian)
	{
		return $this->db->QueryToArray("
			select cmb.no_ujian, cmb.nm_c_mhs,
			  (select nm_program_studi from program_studi where id_program_studi = id_pilihan_1) as nm_pilihan_1,
			  (select nm_program_studi from program_studi where id_program_studi = id_pilihan_2) as nm_pilihan_2,
			  (select nm_program_studi from program_studi where id_program_studi = id_pilihan_3) as nm_pilihan_3,
			  (select nm_program_studi from program_studi where id_program_studi = id_pilihan_4) as nm_pilihan_4,
			  nm_sekolah
			from calon_mahasiswa_baru cmb
			join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
			join sekolah s on s.id_sekolah = cms.id_sekolah_asal
			where cmb.id_penerimaan in (select id_penerimaan from penetapan_penerimaan where id_penetapan = {$id_penetapan}) and
			  cmb.tgl_verifikasi_ppmb is not null and status_ujian = {$status_ujian}
			order by no_ujian");
	}
	
	function GetListPesertaPascaFindByName($nm_c_mhs)
	{
		if ($nm_c_mhs != '')
		{
			$nm_c_mhs = strtoupper($nm_c_mhs);
			
			return $this->db->QueryToArray("
				select cmb.id_c_mhs, cmb.no_ujian, cmb.nm_c_mhs, j.nm_jenjang, ps.nm_program_studi, p.nm_penerimaan, p.gelombang, p.tahun, p.semester
				from calon_mahasiswa_baru cmb
				join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
				join program_studi ps on ps.id_program_studi = cmb.id_pilihan_1 
				join jenjang j on j.id_jenjang = ps.id_jenjang
				where cmb.no_ujian is not null and j.id_jenjang in (2,3,9,10) and p.id_penerimaan in (select id_penerimaan from penetapan_penerimaan) and
				(upper(cmb.nm_c_mhs) like '{$nm_c_mhs}%' or upper(cmb.nm_c_mhs) like '% {$nm_c_mhs}%')
				order by 3, 2");
		}
		else
		{
			return null;
		}
	}
	
	function GetListPenetapanByTahun($tahun = null)
	{
		if ($tahun == NULL)
		{
			$tahun = date('Y');
		}
		
		return $this->db->QueryToArray("
			select id_penetapan, nm_penetapan
			from penetapan where to_char(tgl_penetapan, 'YYYY') = '{$tahun}'
			order by tgl_penetapan desc");
	}
}

?>
