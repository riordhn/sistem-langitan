<?php

class Report
{
	private $db;
	
	public function __construct(MyOracle $db)
	{
		$this->db = $db;
	}
	
	public function GetInfoProgramStudi($id_program_studi)
	{
		$this->db->Query(
			"SELECT nm_program_studi, nm_jenjang FROM program_studi 
			JOIN jenjang ON jenjang.id_jenjang = program_studi.id_jenjang 
			WHERE id_program_studi = {$id_program_studi}");
		return $this->db->FetchAssoc();
	}
	
	public function GetReportPenerimaanAktifList()
	{
		return $this->db->QueryToArray(
			"SELECT 
				-- informasi penerimaan
				p.id_penerimaan, tahun, j.nm_jalur, jg.NM_JENJANG_SNGKAT, nm_penerimaan, gelombang, semester,
				p.id_jenjang, p.id_jalur,

				-- jumlah submit form
				(SELECT count(cmb.id_c_mhs) FROM calon_mahasiswa_baru cmb 
				WHERE cmb.id_penerimaan = p.id_penerimaan AND cmb.tgl_registrasi IS NOT NULL) AS jumlah_submit_form,

				-- jumlah antri verifikasi
				(SELECT count(cmb.id_c_mhs) FROM calon_mahasiswa_baru cmb 
				WHERE cmb.id_penerimaan = p.id_penerimaan AND 
					cmb.tgl_registrasi IS NOT NULL AND 
					cmb.tgl_submit_verifikasi IS NOT NULL AND 
					cmb.tgl_verifikasi_ppmb IS NULL
				) AS jumlah_antri_verifikasi,

				-- jumlah verifikasi dikembalikan
				(SELECT COUNT(DISTINCT cmb.id_c_mhs) FROM calon_mahasiswa_baru cmb
				JOIN calon_mahasiswa_syarat cms ON cms.id_c_mhs = cmb.id_c_mhs
				WHERE cmb.id_penerimaan = p.id_penerimaan and
					cmb.tgl_registrasi IS NOT NULL AND 
					cmb.tgl_submit_verifikasi IS NULL AND
					cms.pesan_verifikator IS NOT NULL
				) as jumlah_verifikasi_kembali,

				-- jumlah terverifikasi
				(SELECT count(cmb.id_c_mhs) FROM calon_mahasiswa_baru cmb 
				WHERE cmb.id_penerimaan = p.id_penerimaan AND 
					cmb.tgl_registrasi IS NOT NULL AND
					cmb.tgl_verifikasi_ppmb IS NOT NULL
				) AS jumlah_verifikasi,

				-- jumlah bayar voucher
				(SELECT count(v.id_voucher) FROM voucher v
				WHERE v.id_penerimaan = p.id_penerimaan AND v.tgl_bayar IS NOT NULL) AS jumlah_bayar,

				-- jumlah pengambilan kartu
				(SELECT count(cmb.id_c_mhs) FROM calon_mahasiswa_baru cmb
				WHERE cmb.id_penerimaan = p.id_penerimaan AND 
				cmb.tgl_ambil_no_ujian IS NOT NULL) AS jumlah_kartu
				
			FROM penerimaan p
			JOIN jalur j ON j.id_jalur = p.id_jalur
			JOIN jenjang jg ON jg.id_jenjang = p.id_jenjang
			WHERE p.is_aktif = 1
			ORDER BY p.tahun, p.semester, p.gelombang, p.id_jalur, p.id_jenjang");
	}
	
	public function GetVoucherAktifList()
	{
		return $this->db->QueryToArray(
			"select
				p.id_penerimaan, p.nm_penerimaan, vt.deskripsi,

				(select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
				where cmb.id_penerimaan = p.id_penerimaan and cmb.kode_jurusan = vt.kode_jurusan and
				cmb.tgl_registrasi is not null) as jumlah_submit_form,

				(select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
				where cmb.id_penerimaan = p.id_penerimaan and cmb.kode_jurusan = vt.kode_jurusan and
				cmb.tgl_registrasi is not null and cmb.tgl_submit_verifikasi is not null and
				cmb.tgl_verifikasi_ppmb is null) as jumlah_antri_verifikasi,

				(select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
				where cmb.id_penerimaan = p.id_penerimaan and cmb.kode_jurusan = vt.kode_jurusan and
				cmb.tgl_registrasi is not null and cmb.tgl_verifikasi_ppmb is not null) as jumlah_verifikasi,

				(select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
				join voucher v on v.kode_voucher = cmb.kode_voucher
				where cmb.id_penerimaan = p.id_penerimaan and cmb.kode_jurusan = vt.kode_jurusan
				and v.tgl_bayar is not null) as jumlah_bayar,

				(select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
				where cmb.id_penerimaan = p.id_penerimaan and cmb.kode_jurusan = vt.kode_jurusan and
				cmb.tgl_registrasi is not null and cmb.tgl_ambil_no_ujian is not null) as jumlah_kartu
			  
			from voucher_tarif vt
			join penerimaan p on p.id_penerimaan = vt.id_penerimaan
			where p.is_aktif = 1 and p.id_jenjang in (1,5)
			order by p.nm_penerimaan");
	}
	
	public function GetPerProdiList($id_penerimaan)
	{
		return $this->db->QueryToArray(
			"SELECT 
				/* Fakultas dan Prodi */
				f.nm_fakultas, j.nm_jenjang, ps.nm_program_studi,

				/* SUBMIT FORM */
				(SELECT count(cmb.id_c_mhs) FROM calon_mahasiswa_baru cmb
				WHERE cmb.id_penerimaan = p.id_penerimaan AND 
				cmb.id_pilihan_1 = ps.id_program_studi AND
				cmb.tgl_registrasi IS NOT NULL) AS jumlah_submit_form,

				/* ANTRI VERIFIKASI */
				(SELECT count(cmb.id_c_mhs) FROM calon_mahasiswa_baru cmb 
				WHERE cmb.id_penerimaan = p.id_penerimaan AND 
				cmb.id_pilihan_1 = ps.id_program_studi AND
				cmb.tgl_registrasi IS NOT NULL AND 
				cmb.tgl_submit_verifikasi IS NOT NULL AND 
				cmb.tgl_verifikasi_ppmb IS NULL
				) AS jumlah_antri_verifikasi,

				/* Verifikasi dikembalikan */
				(SELECT COUNT(DISTINCT cmb.id_c_mhs) FROM calon_mahasiswa_baru cmb
				JOIN calon_mahasiswa_syarat cms ON cms.id_c_mhs = cmb.id_c_mhs
				WHERE cmb.id_penerimaan = p.id_penerimaan AND cmb.id_pilihan_1 = ps.id_program_studi AND
				  cmb.tgl_registrasi IS NOT NULL AND cmb.tgl_submit_verifikasi IS NULL AND 
				  cms.pesan_verifikator IS NOT NULL
				) AS jumlah_verifikasi_kembali,

				/* Terverifikasi */
				(SELECT count(cmb.id_c_mhs) FROM calon_mahasiswa_baru cmb 
				WHERE cmb.id_penerimaan = p.id_penerimaan AND cmb.id_pilihan_1 = ps.id_program_studi AND
				  cmb.tgl_registrasi IS NOT NULL AND
				  cmb.tgl_verifikasi_ppmb IS NOT NULL
				) AS jumlah_verifikasi,

				/* Bayar voucher */
				(SELECT count(cmb.id_c_mhs) FROM calon_mahasiswa_baru cmb
				JOIN voucher v ON v.kode_voucher = cmb.kode_voucher
				WHERE cmb.id_penerimaan = p.id_penerimaan AND cmb.id_pilihan_1 = ps.id_program_studi AND
				v.tgl_bayar IS NOT NULL) AS jumlah_bayar,

				/* ID-ID */
				p.id_penerimaan, f.id_fakultas, ps.id_program_studi

			FROM penerimaan p
			JOIN penerimaan_prodi pp ON pp.id_penerimaan = p.id_penerimaan
			JOIN program_studi ps ON ps.id_program_studi = pp.id_program_studi
			JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
			JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
			WHERE p.id_penerimaan = {$id_penerimaan}
			ORDER BY f.id_fakultas, ps.nm_program_studi");
	}
	
	public function GetCMBSubmitFormList($id_penerimaan, $id_program_studi)
	{
		return $this->db->QueryToArray(
			"SELECT nm_c_mhs, gelar, email, telp, pm.nm_prodi_minat,
				cmp.ptn_s1, cmp.prodi_s1, cmp.ip_s1,
				cmp.ptn_s2, cmp.prodi_s2, cmp.ip_s2
			FROM calon_mahasiswa_baru cmb
			JOIN calon_mahasiswa_pasca cmp ON cmp.id_c_mhs = cmb.id_c_mhs
			LEFT JOIN prodi_minat pm ON pm.id_prodi_minat = cmp.id_prodi_minat
			WHERE id_penerimaan = {$id_penerimaan} AND id_pilihan_1 = {$id_program_studi} AND
				tgl_registrasi IS NOT NULL
			ORDER BY nm_c_mhs");
	}
	
	public function GetCMBAntriVerifikasiList($id_penerimaan, $id_program_studi)
	{
		return $this->db->QueryToArray(
			"SELECT cmb.id_c_mhs, nm_c_mhs, email, cmf.file_foto FROM calon_mahasiswa_baru cmb
			JOIN calon_mahasiswa_file cmf ON cmf.id_c_mhs = cmb.id_c_mhs
			WHERE id_penerimaan = {$id_penerimaan} AND id_pilihan_1 = {$id_program_studi} AND
				tgl_registrasi IS NOT NULL AND 
				cmb.tgl_submit_verifikasi IS NOT NULL AND 
				cmb.tgl_verifikasi_ppmb IS NULL
			ORDER BY nm_c_mhs");
	}
	
	public function GetDataAntriVerifikasiList($id_penerimaan, $id_program_studi)
	{
		return $this->db->QueryToArray(
			"SELECT cms.* FROM calon_mahasiswa_baru cmb
			JOIN calon_mahasiswa_syarat cms ON cms.id_c_mhs = cmb.id_c_mhs
			WHERE cmb.id_penerimaan = {$id_penerimaan} AND cmb.id_pilihan_1 = {$id_program_studi} AND
				tgl_registrasi IS NOT NULL AND tgl_submit_verifikasi IS NOT NULL
				AND tgl_verifikasi_ppmb IS NULL");
	}
	
	public function GetCMBVerifikasiKembaliList($id_penerimaan, $id_program_studi)
	{
		return $this->db->QueryToArray(
			"SELECT DISTINCT cmb.id_c_mhs, nm_c_mhs, email, file_foto FROM calon_mahasiswa_baru cmb
			JOIN calon_mahasiswa_syarat cms ON cms.id_c_mhs = cmb.id_c_mhs
			JOIN calon_mahasiswa_file cmf ON cmf.id_c_mhs = cmb.id_c_mhs
			WHERE cmb.id_penerimaan = {$id_penerimaan} AND
				cmb.id_pilihan_1 = {$id_program_studi} and
				cmb.tgl_registrasi IS NOT NULL AND 
				cmb.tgl_submit_verifikasi IS NULL AND
				cms.pesan_verifikator IS NOT NULL");
	}
	
	public function GetDataVerifikasiKembaliList($id_penerimaan, $id_program_studi)
	{
		return $this->db->QueryToArray(
			"SELECT * FROM calon_mahasiswa_syarat WHERE id_c_mhs IN (
				SELECT cmb.id_c_mhs FROM calon_mahasiswa_baru cmb
				JOIN calon_mahasiswa_syarat cms ON cms.id_c_mhs = cmb.id_c_mhs
				JOIN calon_mahasiswa_file cmf ON cmf.id_c_mhs = cmb.id_c_mhs
				WHERE cmb.id_penerimaan = {$id_penerimaan} AND
					cmb.id_pilihan_1 = {$id_program_studi} and
					cmb.tgl_registrasi IS NOT NULL AND 
					cmb.tgl_submit_verifikasi IS NULL AND
					cms.pesan_verifikator IS NOT NULL)");
	}
	
	public function GetCMBVerifikasiList($id_penerimaan, $id_program_studi)
	{
		return $this->db->QueryToArray(
			"SELECT cmb.id_c_mhs, nm_c_mhs, email, cmf.file_foto FROM calon_mahasiswa_baru cmb
			JOIN calon_mahasiswa_file cmf ON cmf.id_c_mhs = cmb.id_c_mhs
			WHERE id_penerimaan = {$id_penerimaan} AND id_pilihan_1 = {$id_program_studi} AND
				tgl_registrasi IS NOT NULL AND 
				cmb.tgl_verifikasi_ppmb IS NOT NULL
			ORDER BY nm_c_mhs");
	}
	
	public function GetCMBBayarList($id_penerimaan, $id_program_studi)
	{
		return $this->db->QueryToArray(
			"SELECT cmb.kode_voucher, no_ujian, nm_c_mhs,
				to_char(tgl_bayar, 'YYYY-MM-DD HH24:MI:SS') as tgl_bayar
			FROM calon_mahasiswa_baru cmb
			JOIN voucher v ON v.kode_voucher = cmb.kode_voucher AND v.tgl_bayar IS NOT NULL
			WHERE cmb.id_penerimaan = {$id_penerimaan} AND cmb.id_pilihan_1 = {$id_program_studi}
			ORDER BY v.tgl_bayar");
	}
	
	public function GetDataVerifikasiList($id_penerimaan, $id_program_studi)
	{
		return $this->db->QueryToArray(
			"SELECT cms.* FROM calon_mahasiswa_baru cmb
			JOIN calon_mahasiswa_syarat cms ON cms.id_c_mhs = cmb.id_c_mhs
			WHERE cmb.id_penerimaan = {$id_penerimaan} AND cmb.id_pilihan_1 = {$id_program_studi} AND
				tgl_registrasi IS NOT NULL AND
				tgl_verifikasi_ppmb IS NOT NULL");
	}
	
	public function GetSyaratUmumPenerimaan($id_penerimaan)
	{
		return $this->db->QueryToArray(
			"SELECT sp.* FROM penerimaan p
			JOIN penerimaan_syarat_prodi sp ON sp.id_penerimaan = p.id_penerimaan AND sp.status_file = 1
			WHERE p.id_penerimaan = {$id_penerimaan} AND sp.id_program_studi IS NULL
			ORDER BY sp.urutan");
	}
	
	public function GetSyaratProdiPenerimaan($id_penerimaan, $id_program_studi)
	{
		return $this->db->QueryToArray(
			"SELECT sp.* FROM penerimaan p
			JOIN penerimaan_syarat_prodi sp ON sp.id_penerimaan = p.id_penerimaan AND sp.status_file = 1
			WHERE p.id_penerimaan = {$id_penerimaan} AND sp.id_program_studi = {$id_program_studi}
			ORDER BY sp.urutan");
	}
}