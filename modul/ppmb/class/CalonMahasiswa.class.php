<?php
class CalonMahasiswa
{
    private $db;
    
    function __construct($oracle_db)
    {
        $this->db = $oracle_db;
    }
    
    function GetListAgama()
    {
        return array_merge(
            array(array('ID_AGAMA' => '', 'NM_AGAMA' => '')),
            $this->db->QueryToArray("select * from agama order by id_agama")
        );
    }
    
    function GetListJenisKelamin()
    {
        return array(
            array('JENIS_KELAMIN' => '', 'NM_JENIS_KELAMIN' => ''),
            array('JENIS_KELAMIN' => 1, 'NM_JENIS_KELAMIN' => 'Laki-Laki'),
            array('JENIS_KELAMIN' => 2, 'NM_JENIS_KELAMIN' => 'Perempuan'),
        );
    }
    
    function GetListKewarganegaraan()
    {
        return array_merge(
            array(array('ID_KEWARGANEGARAAN'=>'','NM_KEWARGANEGARAAN'=>'')),
            $this->db->QueryToArray("select * from kewarganegaraan order by id_kewarganegaraan")
        );
    }
    
    function GetListSumberBiaya()
    {
        return array_merge(
            array(array('ID_SUMBER_BIAYA'=>'','NM_SUMBER_BIAYA'=>'')),
            $this->db->QueryToArray("select * from sumber_biaya order by id_sumber_biaya")
        );
    }
    
    function GetListJurusanSekolah()
    {
        return array_merge(
            array(array('ID_JURUSAN_SEKOLAH'=>'','NM_JURUSAN_SEKOLAH'=>'')),
            $this->db->QueryToArray("select * from jurusan_sekolah_cmhs order by id_jurusan_sekolah")
        );
    }
    
    function GetListPendidikanOrtu()
    {
        return array_merge(
            array(array('ID_PENDIDIKAN_ORTU'=>'','NM_PENDIDIKAN_ORTU'=>'')),
            $this->db->QueryToArray("select * from pendidikan_ortu order by id_pendidikan_ortu")
        );
    }
    
    function GetListPekerjaan()
    {
        return array_merge(
            array(array('ID_PEKERJAAN'=>'','NM_PEKERJAAN'=>'')),
            $this->db->QueryToArray("select * from pekerjaan order by id_pekerjaan")
        );
    }
    
    function GetListPenghasilanOrtu()
    {
        return array(
            array('PENGHASILAN_ORTU'=>'','NM_PENGHASILAN_ORTU'=>''),
            array('PENGHASILAN_ORTU'=>'1','NM_PENGHASILAN_ORTU'=>'Lebih besar dari 7.500.000'),
            array('PENGHASILAN_ORTU'=>'2','NM_PENGHASILAN_ORTU'=>'2.500.000 sampai 7.500.000'),
            array('PENGHASILAN_ORTU'=>'3','NM_PENGHASILAN_ORTU'=>'1.350.000 sampai 2.500.000'),
            array('PENGHASILAN_ORTU'=>'4','NM_PENGHASILAN_ORTU'=>'Kurang dari 1.350.000'),
        );
    }
    
    function GetListSkalaPekerjaan()
    {
        return array(
            array('SKALA_PEKERJAAN'=>'','NM_SKALA_PEKERJAAN'=>''),
            array('SKALA_PEKERJAAN'=>'1','NM_SKALA_PEKERJAAN'=>'Besar'),
            array('SKALA_PEKERJAAN'=>'2','NM_SKALA_PEKERJAAN'=>'Menengah'),
            array('SKALA_PEKERJAAN'=>'3','NM_SKALA_PEKERJAAN'=>'Kecil'),
            array('SKALA_PEKERJAAN'=>'4','NM_SKALA_PEKERJAAN'=>'Mikro'),
        );
    }
    
    function GetListKediamanOrtu()
    {
        return array(
            array('KEDIAMAN_ORTU'=>'','NM_KEDIAMAN_ORTU'=>''),
            array('KEDIAMAN_ORTU'=>'1','NM_KEDIAMAN_ORTU'=>'Mewah / Besar'),
            array('KEDIAMAN_ORTU'=>'2','NM_KEDIAMAN_ORTU'=>'Sedang'),
            array('KEDIAMAN_ORTU'=>'3','NM_KEDIAMAN_ORTU'=>'Rumah Sederhana'),
            array('KEDIAMAN_ORTU'=>'4','NM_KEDIAMAN_ORTU'=>'Rumah Sangat Sederhana'),
        );
    }
    
    function GetListLuasTanah()
    {
        return array(
            array('LUAS_TANAH'=>'','NM_LUAS_TANAH'=>''),
            array('LUAS_TANAH'=>'1','NM_LUAS_TANAH'=>'Lebih luas dari 200m²'),
            array('LUAS_TANAH'=>'2','NM_LUAS_TANAH'=>'100m² sampai 200m²'),
            array('LUAS_TANAH'=>'3','NM_LUAS_TANAH'=>'45m² sampai 100m²'),
            array('LUAS_TANAH'=>'4','NM_LUAS_TANAH'=>'Kurang dari 45m²'),
        );
    }
    
    function GetListLuasBangunan()
    {
        return array(
            array('LUAS_BANGUNAN'=>'','NM_LUAS_BANGUNAN'=>''),
            array('LUAS_BANGUNAN'=>'1','NM_LUAS_BANGUNAN'=>'Lebih luas dari 100m²'),
            array('LUAS_BANGUNAN'=>'2','NM_LUAS_BANGUNAN'=>'56m² sampai 100m²'),
            array('LUAS_BANGUNAN'=>'3','NM_LUAS_BANGUNAN'=>'27m² sampai 56m²'),
            array('LUAS_BANGUNAN'=>'4','NM_LUAS_BANGUNAN'=>'Kurang dari 27m²'),
        );
    }
    
    function GetListNJOP()
    {
        return array(
            array('NJOP'=>'','NM_NJOP'=>''),
            array('NJOP'=>'1','NM_NJOP'=>'Lebih besar dari 300jt'),
            array('NJOP'=>'2','NM_NJOP'=>'100jt sampai 300jt'),
            array('NJOP'=>'3','NM_NJOP'=>'50jt sampai 100jt'),
            array('NJOP'=>'4','NM_NJOP'=>'Kurang dari 50jt'),
        );
    }
    
    function GetListListrik()
    {
        return array(
            array('LISTRIK'=>'','NM_LISTRIK'=>''),
            array('LISTRIK'=>'1','NM_LISTRIK'=>'Lebih dari 2200'),
            array('LISTRIK'=>'2','NM_LISTRIK'=>''),
            array('LISTRIK'=>'3','NM_LISTRIK'=>''),
            array('LISTRIK'=>'4','NM_LISTRIK'=>''),
        );
    }
    
    function GetListKendaraanR4()
    {
        return array(
            array('KENDARAAN'=>'','NM_KENDARAAN'=>''),
            array('KENDARAAN'=>'1','NM_KENDARAAN'=>'Lebih dari 1'),
            array('KENDARAAN'=>'2','NM_KENDARAAN'=>'1'),
            array('KENDARAAN'=>'3','NM_KENDARAAN'=>'Tidak Punya'),
        );
    }
    
    function GetListKendaraanR2()
    {
        return array(
            array('KENDARAAN'=>'','NM_KENDARAAN'=>''),
            array('KENDARAAN'=>'1','NM_KENDARAAN'=>'Lebih dari 2'),
            array('KENDARAAN'=>'2','NM_KENDARAAN'=>'2'),
            array('KENDARAAN'=>'3','NM_KENDARAAN'=>'1'),
            array('KENDARAAN'=>'4','NM_KENDARAAN'=>'Tidak Punya'),
        );
    }
    
    function GetListKota($criteria)
    {
        return $this->db->QueryToArray("");
    }
}
?>