<?php

class SnmptnClass
{
	private $db;
	
	function __construct(&$database)
	{
		$this->db = $database;
	}
    
    function QueryDiterima($id_penerimaan)
    {
        // Kolom : ID_C_MHS, ID_PILIHAN_PINDAH, URUTAN, IS_DITERIMA
        return "
            select
                cmb.id_c_mhs, cms.id_sekolah_asal, s.id_kota, k.id_provinsi, cmb.id_pilihan_1, cmb.id_pilihan_2, n.id_pilihan_pindah, cmd.status_bidik_misi, n.nilai_total, cms.is_pkd,
                rank() over (partition by n.id_pilihan_pindah order by cms.is_pkd desc, n.nilai_total desc) as urutan,
                case when (rank() over (partition by n.id_pilihan_pindah order by cms.is_pkd desc, n.nilai_total desc)) <= kuota then 1 else 0 end as is_diterima
            from calon_mahasiswa_baru cmb
            join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
            join calon_mahasiswa_data cmd on cmd.id_c_mhs = cmb.id_c_mhs
            join nilai_cmhs_snmptn n on (n.id_c_mhs = cmb.id_c_mhs and n.putaran = 2)
            join statistik_cmhs sc on (sc.id_program_studi = n.id_pilihan_pindah and sc.id_penerimaan = cmb.id_penerimaan)
            join sekolah s on s.id_sekolah = cms.id_sekolah_asal
            join kota k on k.id_kota = s.id_kota
            where cms.is_peserta_khusus = 0 and cmb.id_penerimaan = {$id_penerimaan}";
    }
	
	function GetDetailPeserta($id_penerimaan, $id_c_mhs)
	{
		$rows = $this->db->QueryToArray("
			select cmb.id_c_mhs, no_ujian, nm_c_mhs, ps1.nm_program_studi nm_program_studi1, ps2.nm_program_studi nm_program_studi2, qd.urutan,
                s.nm_sekolah||', '||k.nm_kota||', '||p.nm_provinsi as nm_sekolah,
                s.akreditasi as akreditasi_sekolah, cms.jenis_kelas, cms.rata_semester_3, cms.rata_semester_4, cms.rata_semester_5,
                cms.nilai_rapor, cms.id_skor_pm, cms.is_peserta_khusus, cms.is_pkd, n.nilai_total, qd.is_diterima
            from calon_mahasiswa_baru cmb
            left join program_studi ps1 on ps1.id_program_studi = id_pilihan_1
            left join program_studi ps2 on ps2.id_program_studi = id_pilihan_2
            left join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
            left join sekolah s on s.id_sekolah = cms.id_sekolah_asal
            left join kota k on k.id_kota = s.id_kota
            left join provinsi p on p.id_provinsi = k.id_provinsi
            left join nilai_cmhs_snmptn n on n.id_c_mhs = cmb.id_c_mhs and n.putaran = 2
            left join (".$this->QueryDiterima($id_penerimaan).") qd on qd.id_c_mhs = cmb.id_c_mhs
            where cmb.id_c_mhs = {$id_c_mhs}");
        
        $rows[0]['cpm_set'] = $this->db->QueryToArray("
            select nm_skor_pm, nilai from calon_mahasiswa_pm cpm
            join skor_pm spm on spm.id_skor_pm = cpm.id_skor_pm
            where id_c_mhs = {$id_c_mhs}
            order by group_pm");
        
		return $rows[0];
	}
	
	function GetListPeserta($id_program_studi)
	{
		$rows = $this->db->QueryToArray("
			select a.* from (
				select a.*,
					(((a.nilai_rapor * 0.35) + (a.nilai_pa * 0.25) + (a.nilai_pna * 0.15) + (a.nilai_pm * 0.2) + (a.nilai_jk * 0.05)) * 0.6) +
					(((a.nilai_as * 0.3) + (a.nilai_rs * 0.3) + (a.nilai_rm * 0.1) + (a.nilai_ipk * 0.3)) * 0.4) as nilai_total,
					null as posisi_pilihan, null as urutan_1, null as urutan_2
				from (
					select
						cmb.id_c_mhs, no_ujian, nm_c_mhs, cmb.tgl_diterima, cmd.status_bidik_misi, cms.jurusan_sekolah, s.nm_sekolah,
            
						cms.nilai_rapor,

						nvl((select max(sp.nilai) from calon_mahasiswa_prestasi cmp
						join skor_prestasi sp on sp.id_skor_prestasi = cmp.id_skor_prestasi
						where sp.is_akademik = 1 and cmp.id_c_mhs = cmb.id_c_mhs),0) as nilai_pa,

						nvl((select max(sp.nilai) from calon_mahasiswa_prestasi cmp
						join skor_prestasi sp on sp.id_skor_prestasi = cmp.id_skor_prestasi
						where sp.is_akademik = 0 and cmp.id_c_mhs = cmb.id_c_mhs),0) as nilai_pna,
            
						nvl((select sum(spm.nilai) from calon_mahasiswa_pm cpm
                        join skor_pm spm on spm.id_skor_pm = cpm.id_skor_pm
                        where cpm.id_c_mhs = cmb.id_c_mhs and (cpm.status_verifikasi = 1)),0) as nilai_pm,

						nvl((select nilai from skor_kelas sk where sk.jenis_kelas = cms.jenis_kelas),0) as nilai_jk,

						nvl((select nilai from skor_akreditasi sa where sa.akreditasi_sekolah = cms.akreditasi_sekolah),0) as nilai_as,

						nvl(round(((sr.rasio_snmptn / (select max(rasio_snmptn) from sekolah_rasio)) * 100),2),0) as nilai_rs,
						nvl(round(((sr.rasio_mandiri / (select max(rasio_mandiri) from sekolah_rasio)) * 100),2),0) as nilai_rm,

						nvl((select nilai from skor_ipk si where si.batas_bawah <= sr.rata_ipk and sr.rata_ipk <= si.batas_atas),20) as nilai_ipk

					from calon_mahasiswa_baru cmb
					join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
					join calon_mahasiswa_data cmd on cmd.id_c_mhs = cmb.id_c_mhs
					join statistik_cmhs sc on sc.id_penerimaan = cmb.id_penerimaan and sc.id_program_studi = cmb.id_pilihan_1
					left join sekolah_rasio sr on sr.id_sekolah = cms.id_sekolah_asal
					left join sekolah s on s.id_sekolah = cms.id_sekolah_asal
					left join kota k on k.id_kota = s.id_kota
					left join provinsi p on p.id_provinsi = k.id_provinsi
					where cmb.id_penerimaan = 44 and cmb.id_pilihan_1 = {$id_program_studi}
				) a
			) a order by nilai_total desc");
		
		return $rows;
	}
	
	function GetStatistik($id_program_studi, $id_penerimaan)
	{
		$rows = $this->db->QueryToArray("select * from statistik_cmhs where id_program_studi = {$id_program_studi} and id_penerimaan = {$id_penerimaan}");
		return $rows[0];
	}
	
	function GetListPesertaPutaran1($id_program_studi)
	{
		$rows = $this->db->QueryToArray("
			select cmb.* from (
				select cmb.nm_c_mhs, cmb.no_ujian, n.nilai_total, n.id_pilihan_1, n.id_pilihan_pindah, sc.kuota, s.nm_sekolah, ps2.nm_program_studi as ps2
				from calon_mahasiswa_baru cmb
				join nilai_cmhs_snmptn n on n.id_c_mhs = cmb.id_c_mhs
				join statistik_cmhs sc on sc.id_penerimaan = cmb.id_penerimaan and sc.id_program_studi = cmb.id_pilihan_1
				join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
				left join sekolah s on s.id_sekolah = cms.id_sekolah_asal
				left join program_studi ps2 on ps2.id_program_studi = cmb.id_pilihan_2
				where n.putaran = 2 and n.id_pilihan_1 = {$id_program_studi}
				order by n.nilai_total desc
			) cmb where rownum <= kuota");
		return $rows;
	}
	
	function GetListPesertaPutaran2($id_program_studi)
	{
		$rows = $this->db->QueryToArray("
            select cmb.* from (
                select cmb.id_c_mhs, cmb.nm_c_mhs, cmb.no_ujian, n.nilai_total, n.id_pilihan_1, n.id_pilihan_pindah,
                    sc.kuota, s.nm_sekolah, ps2.nm_program_studi as ps2, cms.is_pkd,
                    rank() over (partition by n.id_pilihan_pindah order by cms.is_pkd desc, n.nilai_total desc) urutan
                from calon_mahasiswa_baru cmb
                join nilai_cmhs_snmptn n on n.id_c_mhs = cmb.id_c_mhs
                join statistik_cmhs sc on sc.id_penerimaan = cmb.id_penerimaan and sc.id_program_studi = n.id_pilihan_pindah
                join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
                left join sekolah s on s.id_sekolah = cms.id_sekolah_asal
                left join program_studi ps2 on ps2.id_program_studi = cmb.id_pilihan_2
                where cms.is_peserta_khusus = 0 and n.putaran = 2 and n.id_pilihan_pindah = {$id_program_studi}
                order by cms.is_pkd desc, n.nilai_total desc
            ) cmb where urutan <= kuota order by id_pilihan_pindah, urutan");
                
        // Update Nilai PKD
        foreach ($rows as &$row)
        {
            if ($row['IS_PKD'] == 1) { $row['NILAI_TOTAL'] = $row['NILAI_TOTAL'] + 40; }
        }
		
		return $rows;
	}
    
    function GetListPesertaPutaran2All($id_penerimaan)
    {
        $rows = $this->db->QueryToArray("
            select 
                cmb.no_ujian, cmb.nm_c_mhs,
                case is_pkd when 1 then nilai_total + 40 else nilai_total end nilai_total,
                nm_sekolah, nm_program_studi as ps1, ps.kode_snmptn
            from (".$this->QueryDiterima($id_penerimaan).") dq
            join calon_mahasiswa_baru cmb on cmb.id_c_mhs = dq.id_c_mhs
            left join sekolah s on s.id_sekolah = dq.id_sekolah_asal
            join program_studi ps on ps.id_program_studi = dq.id_pilihan_pindah
            where is_diterima = 1
            order by id_pilihan_pindah, urutan");
        
        // Update Nilai PKD
        foreach ($rows as &$row)
        {
            if ($row['IS_PKD'] == 1) { $row['NILAI_TOTAL'] = 70; }
        }
        
        return $rows;
    }
	
	function GetListKuotaOnly()
	{
		$rows = $this->db->QueryToArray("
			select * from (
				select n.id_c_mhs, n.nilai_total, cmb.id_pilihan_1, cmb.id_pilihan_2, cms.id_sekolah_asal,
					rank() over (partition by n.id_pilihan_pindah order by n.nilai_total desc) as urutan, sc.kuota
				from nilai_cmhs_snmptn n
				join calon_mahasiswa_baru cmb on cmb.id_c_mhs = n.id_c_mhs
				join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
				join statistik_cmhs sc on sc.id_penerimaan = cmb.id_penerimaan and sc.id_program_studi = n.id_pilihan_pindah
				where cmb.id_penerimaan = 44 and n.putaran = 2
			) a where urutan <= kuota");
		return $rows;
	}
	
	/**
	 * This is method GetKeketatanProdi
	 *
	 * @param int $id_penerimaan This is a description
	 * @return array This is the return value description
	 *
	 */	
	function GetRekapProdi($id_penerimaan)
	{
		$rows = $this->db->QueryToArray("
			select
                ps.id_program_studi, ps.nm_program_studi,
                p1.peminat1, p2.peminat2, sc.kuota, dt.diterima
            from program_studi ps
            join statistik_cmhs sc on sc.id_program_studi = ps.id_program_studi and sc.id_penerimaan = {$id_penerimaan}
            join (
                select cmb.id_pilihan_1, count(cmb.id_c_mhs) as peminat1
                from calon_mahasiswa_baru cmb
                join nilai_cmhs_snmptn n on n.id_c_mhs = cmb.id_c_mhs and n.putaran = 2
                group by cmb.id_pilihan_1) p1 on p1.id_pilihan_1 = ps.id_program_studi
            join (
                select cmb.id_pilihan_2, count(cmb.id_c_mhs) as peminat2
                from calon_mahasiswa_baru cmb
                join nilai_cmhs_snmptn n on n.id_c_mhs = cmb.id_c_mhs and n.putaran = 2
                group by cmb.id_pilihan_2) p2 on p2.id_pilihan_2 = ps.id_program_studi
            join (
                select id_pilihan_pindah, count(id_c_mhs) diterima from (".$this->QueryDiterima($id_penerimaan).") t
                where is_diterima = 1
                group by id_pilihan_pindah) dt on dt.id_pilihan_pindah = ps.id_program_studi 
            where ps.id_jenjang = 1
            order by (kuota / peminat1)");
		return $rows;
	}
	
	function GetKeketatanSekolah($id_penerimaan)
	{
		$rows = $this->db->QueryToArray("
			select
				s.id_sekolah,
				count(cmb.id_c_mhs) peserta,
				(select sum(kuota) from statistik_cmhs where id_penerimaan = {$id_penerimaan}) kuota
			from sekolah s
			join (
				select * from (
					select n.id_c_mhs, cms.id_sekolah_asal,
						rank() over (partition by n.id_pilihan_pindah order by n.nilai_total desc) as urutan, sc.kuota
					from nilai_cmhs_snmptn n
					join calon_mahasiswa_baru cmb on cmb.id_c_mhs = n.id_c_mhs
					join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
					join statistik_cmhs sc on sc.id_penerimaan = cmb.id_penerimaan and sc.id_program_studi = n.id_pilihan_pindah
					where n.putaran = 2 and cmb.id_penerimaan = {$id_penerimaan}
				) a where a.urutan <= a.kuota
			) cmb on cmb.id_sekolah_asal = s.id_sekolah
			where s.kode_snmptn is not null
			group by s.id_sekolah");
		
		foreach ($rows as &$row)
			$row['RASIO'] = str_replace(",",".", ($row['PESERTA'] / $row['KUOTA']) * 100);
		
		return $rows;
	}
	
	function RefreshNilai($id_c_mhs)
	{
		$rows = $this->db->QueryToArray("
			select a.* from (
			  select a.*,
				(((a.nilai_rapor * 0.35) + (a.nilai_pa * 0.25) + (a.nilai_pna * 0.15) + (a.nilai_pm * 0.2) + (a.nilai_jk * 0.05)) * 0.6) +
				(((a.nilai_as * 0.3) + (a.nilai_rs * 0.3) + (a.nilai_rm * 0.1) + (a.nilai_ipk * 0.3)) * 0.4) as nilai_total
			  from (
				select
				  cmb.id_c_mhs,
      
				  cms.nilai_rapor,

				  nvl((select max(sp.nilai) from calon_mahasiswa_prestasi cmp
				  join skor_prestasi sp on sp.id_skor_prestasi = cmp.id_skor_prestasi
				  where sp.is_akademik = 1 and cmp.id_c_mhs = cmb.id_c_mhs),0) as nilai_pa,

				  nvl((select max(sp.nilai) from calon_mahasiswa_prestasi cmp
				  join skor_prestasi sp on sp.id_skor_prestasi = cmp.id_skor_prestasi
				  where sp.is_akademik = 0 and cmp.id_c_mhs = cmb.id_c_mhs),0) as nilai_pna,
      
				  nvl((select sum(spm.nilai) from calon_mahasiswa_pm cpm
                  join skor_pm spm on spm.id_skor_pm = cpm.id_skor_pm
                  where cpm.id_c_mhs = cmb.id_c_mhs and (cpm.status_verifikasi = 1)),0) as nilai_pm,

				  nvl((select nilai from skor_kelas sk where sk.jenis_kelas = cms.jenis_kelas),0) as nilai_jk,

				  nvl((select nilai from skor_akreditasi sa where sa.akreditasi_sekolah = cms.akreditasi_sekolah),0) as nilai_as,

				  nvl(round(((sr.rasio_snmptn / (select max(rasio_snmptn) from sekolah_rasio)) * 100),2),0) as nilai_rs,
				  nvl(round(((sr.rasio_mandiri / (select max(rasio_mandiri) from sekolah_rasio)) * 100),2),0) as nilai_rm,

				  nvl((select nilai from skor_ipk si where si.batas_bawah <= sr.rata_ipk and sr.rata_ipk <= si.batas_atas),20) as nilai_ipk

				from calon_mahasiswa_baru cmb
				join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
				join calon_mahasiswa_data cmd on cmd.id_c_mhs = cmb.id_c_mhs
				join statistik_cmhs sc on sc.id_penerimaan = cmb.id_penerimaan and sc.id_program_studi = cmb.id_pilihan_1
				left join sekolah_rasio sr on sr.id_sekolah = cms.id_sekolah_asal
				left join sekolah s on s.id_sekolah = cms.id_sekolah_asal
				left join kota k on k.id_kota = s.id_kota
				left join provinsi p on p.id_provinsi = k.id_provinsi
				where cmb.id_penerimaan = 44
			  ) a
			) a where id_c_mhs = {$id_c_mhs}");
		
		$this->db->Parse("
			update nilai_cmhs_snmptn set
				nilai_rapor = :nilai_rapor,
				nilai_pa = :nilai_pa,
				nilai_pna = :nilai_pna,
				nilai_pm = :nilai_pm,
				nilai_jk = :nilai_jk,
				nilai_as = :nilai_as,
				nilai_rs = :nilai_rs,
				nilai_rm = :nilai_rm,
				nilai_ipk = :nilai_ipk,
				nilai_total = :nilai_total
			where id_c_mhs = :id_c_mhs");
		$this->db->BindByName(':nilai_rapor', $rows[0]['NILAI_RAPOR']);
		$this->db->BindByName(':nilai_pa', $rows[0]['NILAI_PA']);
		$this->db->BindByName(':nilai_pna', $rows[0]['NILAI_PNA']);
		$this->db->BindByName(':nilai_pm', $rows[0]['NILAI_PM']);
		$this->db->BindByName(':nilai_jk', $rows[0]['NILAI_JK']);
		$this->db->BindByName(':nilai_as', $rows[0]['NILAI_AS']);
		$this->db->BindByName(':nilai_rs', $rows[0]['NILAI_RS']);
		$this->db->BindByName(':nilai_rm', $rows[0]['NILAI_RM']);
		$this->db->BindByName(':nilai_ipk', $rows[0]['NILAI_IPK']);
		$this->db->BindByName(':nilai_total', $rows[0]['NILAI_TOTAL']);
		$this->db->BindByName(':id_c_mhs', $rows[0]['ID_C_MHS']);
		
		return $this->db->Execute();
	}
	
	function GetListPesertaKhusus()
	{
		$rows = $this->db->QueryToArray("
			select cmb.id_c_mhs, cmb.no_ujian, cmb.nm_c_mhs, s.nm_sekolah, ps1.nm_program_studi ps1, ps2.nm_program_studi ps2 from calon_mahasiswa_baru cmb
			join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
			left join sekolah s on s.id_sekolah = cms.id_sekolah_asal
			left join program_studi ps1 on ps1.id_program_studi = cmb.id_pilihan_1
			left join program_studi ps2 on ps2.id_program_studi = cmb.id_pilihan_2
			where cms.is_peserta_khusus = 1");
		return $rows;
	}
    
    function GetRekapBidikMisi($id_penerimaan)
    {
        return $this->db->QueryToArray("
            select
                ps.id_program_studi, ps.nm_program_studi, qd.bm, kuota
            from program_studi ps
            join (
                select id_pilihan_pindah, count(id_c_mhs) bm from (".$this->QueryDiterima($id_penerimaan).") t
                where is_diterima = 1 and status_bidik_misi = 1
                group by id_pilihan_pindah) qd on qd.id_pilihan_pindah = ps.id_program_studi
            join statistik_cmhs sc on sc.id_program_studi = ps.id_program_studi and sc.id_penerimaan = {$id_penerimaan}");
    }
    
    function GetListPM()
    {
        $rows = $this->db->QueryToArray("
            select
                cmb.id_c_mhs, cmb.no_ujian, cmb.nm_c_mhs, cpm.nm_ortu, cpm.nm_sponsor, cpm.keterangan, ps.nm_program_studi, cpm.status_verifikasi,
                cpm2.nm_skor_pm as status_anak, sc.kuota, n.urutan, n2.urutan2, f.nm_fakultas, s.nm_sekolah, sum(spm.nilai) as nilai_pm
            from calon_mahasiswa_pm cpm
            join calon_mahasiswa_baru cmb on cmb.id_c_mhs = cpm.id_c_mhs
            join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
            join sekolah s on s.id_sekolah = cms.id_sekolah_asal
            join program_studi ps on ps.id_program_studi = cmb.id_pilihan_1
            join fakultas f on f.id_fakultas = ps.id_fakultas
            join skor_pm spm on spm.id_skor_pm = cpm.id_skor_pm
            join statistik_cmhs sc on sc.id_program_studi = cmb.id_pilihan_1 and sc.id_penerimaan = cmb.id_penerimaan
            left join (
                select cpm.id_c_mhs, nm_skor_pm from calon_mahasiswa_pm cpm
                join skor_pm spm on spm.id_skor_pm = cpm.id_skor_pm
                where group_pm = 2) cpm2 on cpm2.id_c_mhs = cpm.id_c_mhs
            left join (
                select id_c_mhs, rank() over (partition by id_pilihan_1 order by nilai_total desc) as urutan
                from nilai_cmhs_snmptn n
                where putaran = 1) n on n.id_c_mhs = cmb.id_c_mhs
            left join (
                select id_c_mhs, rank() over (partition by id_pilihan_1 order by nilai_total desc) as urutan2
                from nilai_cmhs_snmptn n
                where putaran = 2) n2 on n2.id_c_mhs = cmb.id_c_mhs
            where cmb.id_penerimaan = 44
            group by
                cmb.id_c_mhs, cmb.no_ujian, cmb.nm_c_mhs, cpm.nm_ortu, cpm.nm_sponsor, cpm.keterangan, ps.nm_program_studi, cpm.status_verifikasi,
                cpm2.nm_skor_pm, sc.kuota, n.urutan, n2.urutan2, f.nm_fakultas, s.nm_sekolah
            order by cmb.nm_c_mhs");
        return $rows;
    }
    
    function GetRekapSekolahPerProdi($id_penerimaan)
    {
        return $this->db->QueryToArray("
            select
                ps.id_program_studi, ps.nm_program_studi, jumlah_sma, kuota
            from program_studi ps
            join (
                select id_pilihan_pindah, count(distinct(id_sekolah_asal)) jumlah_sma
                from (".$this->QueryDiterima($id_penerimaan).") t
                where is_diterima = 1 group by id_pilihan_pindah) qd on qd.id_pilihan_pindah = ps.id_program_studi
            join statistik_cmhs sc on sc.id_program_studi = ps.id_program_studi and sc.id_penerimaan = {$id_penerimaan}
            where ps.id_jenjang = 1");
    }
    
    function GetRekapPesertaPerSekolahByProdi($id_penerimaan, $id_program_studi)
    {
        return $this->db->QueryToArray("
            select 
                s.id_sekolah, s.nm_sekolah, jumlah
            from sekolah s
            join (
                select id_sekolah_asal, count(id_c_mhs) jumlah from (".$this->QueryDiterima($id_penerimaan).") t
                where is_diterima = 1 and id_pilihan_pindah = {$id_program_studi}
                group by id_sekolah_asal) qd on qd.id_sekolah_asal = s.id_sekolah
            order by jumlah desc");
    }
    
    function GetProgramStudi($id_program_studi)
    {
        $rows = $this->db->QueryToArray("select id_program_studi, nm_program_studi from program_studi where id_program_studi = {$id_program_studi}");
        return $rows[0];
    }
    
    function GetListPesertaByProdiAndSekolah($id_penerimaan, $id_program_studi, $id_sekolah)
    {
        return $this->db->QueryToArray("
            select
                cmb.no_ujian, cmb.nm_c_mhs
            from calon_mahasiswa_baru cmb
            join (".$this->QueryDiterima($id_penerimaan).") qd on
                qd.id_c_mhs = cmb.id_c_mhs and
                qd.is_diterima = 1 and
                qd.id_pilihan_pindah = {$id_program_studi} and
                qd.id_sekolah_asal = {$id_sekolah}");
    }
    
    function GetRekapPesertaPerProvinsi($id_penerimaan)
    {
        return $this->db->QueryToArray("
            select 
                p.id_provinsi, p.nm_provinsi, peserta, nvl(kuota,0) kuota
            from (
                select k.id_provinsi, count(cmb.id_c_mhs) peserta
                from calon_mahasiswa_baru cmb
                join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
                left join sekolah s on s.id_sekolah = cms.id_sekolah_asal
                left join kota k on k.id_kota = s.id_kota
                where cmb.id_penerimaan = {$id_penerimaan}
                group by k.id_provinsi) pst
            left join provinsi p on p.id_provinsi = pst.id_provinsi
            left join (
                select id_provinsi, count(id_c_mhs) kuota from (".$this->QueryDiterima($id_penerimaan).") t
                where is_diterima = 1
                group by id_provinsi) qd on qd.id_provinsi = pst.id_provinsi
            order by kuota desc, peserta desc");
    }
    
    function GetRekapPesertaPerKota($id_penerimaan, $id_provinsi)
    {
        return $this->db->QueryToArray("
            select
                k.id_kota, k.nm_kota, pst.peserta, nvl(qd.kuota,0) kuota
            from (
                select s.id_kota, count(cmb.id_c_mhs) peserta
                from calon_mahasiswa_baru cmb
                join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
                join sekolah s on s.id_sekolah = cms.id_sekolah_asal
                join kota k on k.id_kota = s.id_kota
                where cmb.id_penerimaan = {$id_penerimaan} and k.id_provinsi = {$id_provinsi}
                group by s.id_kota) pst
            left join kota k on k.id_kota = pst.id_kota
            left join (
                select id_kota, count(id_c_mhs) kuota from (".$this->QueryDiterima($id_penerimaan).") t
                where is_diterima = 1
                group by id_kota) qd on qd.id_kota = pst.id_kota
            order by kuota desc, peserta desc");
    }
    
    function GetRekapPesertaPerSekolah($id_penerimaan, $id_kota)
    {
        return $this->db->QueryToArray("
            select
                s.id_sekolah, s.nm_sekolah, pst.peserta, nvl(qd.kuota,0) kuota
            from (
                select cms.id_sekolah_asal, count(cmb.id_c_mhs) peserta
                from calon_mahasiswa_baru cmb
                join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
                join sekolah s on s.id_sekolah = cms.id_sekolah_asal
                where cmb.id_penerimaan = {$id_penerimaan} and s.id_kota = {$id_kota}
                group by cms.id_sekolah_asal) pst
            join sekolah s on s.id_sekolah = pst.id_sekolah_asal
            left join (
                select id_sekolah_asal, count(id_c_mhs) kuota from (".$this->QueryDiterima($id_penerimaan).") t
                where is_diterima = 1
                group by id_sekolah_asal) qd on qd.id_sekolah_asal = pst.id_sekolah_asal
            order by kuota desc, peserta desc");
    }
    
    function GetRekapProvinsiPerProdi($id_penerimaan)
    {
        return $this->db->QueryToArray("
            select 
                ps.id_program_studi, ps.nm_program_studi, provinsi, sc.kuota
            from program_studi ps
            join (
                select id_pilihan_pindah, count(distinct(id_provinsi)) provinsi from (".$this->QueryDiterima($id_penerimaan).") t
                where is_diterima = 1 group by id_pilihan_pindah) qd on qd.id_pilihan_pindah = ps.id_program_studi
            join statistik_cmhs sc on sc.id_program_studi = ps.id_program_studi and sc.id_penerimaan = {$id_penerimaan}
            where ps.id_jenjang = 1
            order by provinsi desc");
    }
    
    function GetRekapPesertaPerProvinsiByProdi($id_penerimaan, $id_program_studi)
    {
        return $this->db->QueryToArray("
            select 
                p.id_provinsi, p.nm_provinsi, jumlah
            from provinsi p
            join (
                select id_provinsi, count(id_c_mhs) jumlah from (".$this->QueryDiterima($id_penerimaan).") t
                where is_diterima = 1 and id_pilihan_pindah = {$id_program_studi}
                group by id_provinsi) qd on qd.id_provinsi = p.id_provinsi
            order by jumlah desc");
    }
    
    function GetListPesertaBySekolah($id_penerimaan, $id_sekolah)
    {   
        return $this->db->QueryToArray("
            select cmb.id_c_mhs, cmb.no_ujian, cmb.nm_c_mhs, ps1.nm_program_studi prodi1, ps2.nm_program_studi prodi2, qd.is_diterima
            from calon_mahasiswa_baru cmb
            join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
            join program_studi ps1 on ps1.id_program_studi = cmb.id_pilihan_1
            left join program_studi ps2 on ps2.id_program_studi = cmb.id_pilihan_2
            join (".$this->QueryDiterima($id_penerimaan).") qd on qd.id_c_mhs = cmb.id_c_mhs
            where cmb.id_penerimaan = 44 and cms.id_sekolah_asal = {$id_sekolah}
            order by cmb.no_ujian");
    }
    
    function GetProvinsi($id_provinsi)
    {
        $rows = $this->db->QueryToArray("select id_provinsi, nm_provinsi from provinsi where id_provinsi = {$id_provinsi}");
        return $rows[0];
    }
    
    function GetSekolah($id_sekolah)
    {
        $rows = $this->db->QueryToArray("
            select s.id_sekolah, s.nm_sekolah, k.id_kota, k.nm_kota, p.id_provinsi, p.nm_provinsi from sekolah s
            join kota k on k.id_kota = s.id_kota
            join provinsi p on p.id_provinsi = k.id_provinsi
            where s.id_sekolah = {$id_sekolah}");
        return $rows[0];
    }
    
    
    
    function GetListPKD()
    {
        return $this->db->QueryToArray("
            select
                cmb.id_c_mhs, cmb.no_ujian, cmb.nm_c_mhs,
                ps1.nm_program_studi ps1, ps2.nm_program_studi ps2,
                s.nm_sekolah
            from calon_mahasiswa_baru cmb
            join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
            join program_studi ps1 on ps1.id_program_studi = cmb.id_pilihan_1
            left join program_studi ps2 on ps2.id_program_studi = cmb.id_pilihan_2
            join sekolah s on s.id_sekolah = cms.id_sekolah_asal
            where cms.is_pkd = 1");
    }
}
?>