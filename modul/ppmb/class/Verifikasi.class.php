<?php
/**
 * Description of Verifikasi
 *
 * @author Fathoni
 */
class Verifikasi
{
    private $db;
    
    function __construct(MyOracle $oracle)
    {
        $this->db = $oracle;
    }
    
    function GetData($kode_voucher)
    {
        $rows = $this->db->QueryToArray("
            select
                -- data peserta
                cmb.id_c_mhs, cmb.kode_voucher, v.kode_jurusan, no_ujian, nm_c_mhs, tgl_registrasi, p.id_jalur, j.nm_jalur, nm_penerimaan, cmb.id_kelompok_biaya, p.id_penerimaan,

                -- data kelas
                cmp.kelas_pilihan, pk.nm_prodi_kelas, is_matrikulasi,
            
                cmb.id_pilihan_1, cmb.id_pilihan_2, cmb.id_pilihan_3, cmb.id_pilihan_4, cmp.id_prodi_minat,
            
                -- lookup prodi
                (select nm_jenjang||' '||nm_program_studi from program_studi ps join jenjang j on j.id_jenjang = ps.id_jenjang where ps.id_program_studi = cmb.id_pilihan_1) as prodi1,
                (select nm_jenjang||' '||nm_program_studi from program_studi ps join jenjang j on j.id_jenjang = ps.id_jenjang where ps.id_program_studi = cmb.id_pilihan_2) as prodi2,
                (select nm_jenjang||' '||nm_program_studi from program_studi ps join jenjang j on j.id_jenjang = ps.id_jenjang where ps.id_program_studi = cmb.id_pilihan_3) as prodi3,
                (select nm_jenjang||' '||nm_program_studi from program_studi ps join jenjang j on j.id_jenjang = ps.id_jenjang where ps.id_program_studi = cmb.id_pilihan_4) as prodi4,

                -- data Sp3 prodi
                cmb.sp3_1, cmb.sp3_2, cmb.sp3_3, cmb.sp3_4,

                -- data berkas
                cmd.berkas_ijazah, cmd.berkas_skhun, cmd.berkas_kesanggupan_sp3, cmd.berkas_lain, cmd.berkas_lain_ket,

                -- Mendapatkan pt_s1
                cmp.ptn_s1,
            
                -- Mendapatkan asal sekolah
                cms.id_sekolah_asal,
            
                -- Mendapatkan semester penerimaan
                (select id_semester from semester s where s.thn_akademik_semester = p.tahun and s.nm_semester = (case p.semester when 'Gasal' then 'Ganjil' when 'Genap' then 'Genap' end)) as id_semester
            
            from calon_mahasiswa_baru cmb
            join calon_mahasiswa_data cmd on cmd.id_c_mhs = cmb.id_c_mhs
            join calon_mahasiswa_pasca cmp on cmp.id_c_mhs = cmb.id_c_mhs
            join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
            join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
            join jalur j on j.id_jalur = p.id_jalur
            join voucher v on v.kode_voucher = cmb.kode_voucher
            left join prodi_kelas pk on (pk.id_program_studi = cmb.id_pilihan_1 and pk.singkatan = cmp.kelas_pilihan)
            where cmb.kode_voucher = '{$kode_voucher}' and tgl_awal_verifikasi <= current_date and current_date <= tgl_akhir_verifikasi and p.is_aktif = 1");
        
        return $rows[0];
    }
    
    function GetSp3Min($id_semester, $id_jalur, $id_program_studi, $id_kelompok_biaya)
    {
        return $this->db->QuerySingle("
            select db.besar_biaya from detail_biaya db
            left join biaya_kuliah bk on bk.id_biaya_kuliah = db.id_biaya_kuliah
            left join biaya b on b.id_biaya = db.id_biaya
            where b.nm_biaya = 'SP3' and bk.id_semester = {$id_semester} and bk.id_jalur = {$id_jalur}
            and bk.id_kelompok_biaya = {$id_kelompok_biaya} and bk.id_program_studi = {$id_program_studi}");
    }
	
	function GetUKAMin($id_semester, $id_jalur, $id_program_studi, $id_kelompok_biaya)
    {	
        return $this->db->QuerySingle("
            select db.besar_biaya from detail_biaya db
            left join biaya_kuliah bk on bk.id_biaya_kuliah = db.id_biaya_kuliah
            left join biaya b on b.id_biaya = db.id_biaya
            where b.nm_biaya = 'UKA' and bk.id_semester = {$id_semester} and bk.id_jalur = {$id_jalur}
            and bk.id_kelompok_biaya = {$id_kelompok_biaya} and bk.id_program_studi = {$id_program_studi}");
    }
    
    function GetJumlahProdiMinat($id_program_studi, $id_c_mhs)
    {
        $this->db->Query("
            select count(*) as jumlah from calon_mahasiswa_baru cmb
            join penerimaan_prodi pp on pp.id_penerimaan = cmb.id_penerimaan
            join penerimaan_prodi_minat ppm on ppm.id_penerimaan = cmb.id_penerimaan
            join prodi_minat pm on (pm.id_prodi_minat = ppm.id_prodi_minat and pm.id_program_studi = pp.id_program_studi)
            where pp.is_aktif = 1 and ppm.is_aktif = 1 and pp.id_program_studi = {$id_program_studi} and cmb.id_c_mhs = {$id_c_mhs}
            order by nm_prodi_minat");
        $row = $this->db->FetchAssoc();
        
        return $row['JUMLAH'];
    }
}

?>
