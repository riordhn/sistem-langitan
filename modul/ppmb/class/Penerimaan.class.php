<?php

$id_pt = $id_pt_user;

class Penerimaan
{
    private $db;
    
    function __construct(MyOracle $db)
    {
        $this->db = $db;
    }
    
    function GetAllForView()
    {
        $penerimaan_set = $this->db->QueryToArray("select distinct tahun, semester from penerimaan where id_perguruan_tinggi = '{$GLOBALS[id_pt]}' order by tahun desc, semester desc");
        
        foreach ($penerimaan_set as &$p)
            $p['p_set'] = $this->db->QueryToArray("
                select p.id_penerimaan, j.nm_jenjang, p.nm_penerimaan,jl.nm_jalur, p.gelombang, j.id_jenjang from penerimaan p
                join jenjang j on j.id_jenjang = p.id_jenjang
                join jalur jl on jl.id_jalur = p.id_jalur
                where p.tahun = {$p['TAHUN']} and p.semester = '{$p['SEMESTER']}' and p.id_perguruan_tinggi = '{$GLOBALS[id_pt]}'
                order by p.gelombang asc, p.id_jalur, p.id_jenjang");

        return $penerimaan_set;
    }
	
	function GetAllNonPasca()
	{
		$penerimaan_set = $this->db->QueryToArray("select distinct tahun, semester from penerimaan where semester = 'Gasal' order by tahun desc, semester desc");
		
		foreach ($penerimaan_set as &$p)
            $p['p_set'] = $this->db->QueryToArray("
                select p.id_penerimaan, j.nm_jenjang, p.nm_penerimaan,jl.nm_jalur, p.gelombang, j.id_jenjang from penerimaan p
                join jenjang j on j.id_jenjang = p.id_jenjang
                join jalur jl on jl.id_jalur = p.id_jalur
                where p.tahun = {$p['TAHUN']} and p.semester = '{$p['SEMESTER']}' and j.id_jenjang in (1, 5)
                order by p.gelombang asc, p.id_jalur, p.id_jenjang");

        return $penerimaan_set;
	}
    
    function GetListProdiPenerimaan($id_penerimaan, $is_aktif = '')
    {
        $is_aktif = ($is_aktif != '') ? "and pp.is_aktif = {$is_aktif}" : "";
        
        return $this->db->QueryToArray("
            select pp.id_penerimaan_prodi, ps.id_program_studi, j.id_jenjang, j.nm_jenjang, ps.nm_program_studi
            from penerimaan_prodi pp
            join program_studi ps on ps.id_program_studi = pp.id_program_studi
            join jenjang j on j.id_jenjang = ps.id_jenjang
            where pp.id_penerimaan = {$id_penerimaan} {$is_aktif}
            order by ps.nm_program_studi");
    }
    
    function GetProdiPenerimaan($id_penerimaan_prodi)
    {
        $rows = $this->db->QueryToArray("
            select
                pp.id_penerimaan_prodi, ps.id_program_studi, j.id_jenjang, j.nm_jenjang, ps.nm_program_studi,
                p.id_penerimaan, p.nm_penerimaan, p.semester, p.tahun
            from penerimaan_prodi pp
            join program_studi ps on ps.id_program_studi = pp.id_program_studi
            join jenjang j on j.id_jenjang = ps.id_jenjang
            join penerimaan p on p.id_penerimaan = pp.id_penerimaan
            where pp.id_penerimaan_prodi = {$id_penerimaan_prodi}");
            
        return $rows[0];
    }
    
    function GetPenerimaan($id_penerimaan)
    {
        $rows = $this->db->QueryToArray("select * from penerimaan where id_penerimaan = {$id_penerimaan}");
        return $rows[0];
    }
}
?>
