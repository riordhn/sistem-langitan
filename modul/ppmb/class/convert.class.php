<?php

class convert_data {

    public $db_oracle;

    function __construct($db_oracle) {
        $this->db_oracle = $db_oracle;
    }

    function get_tahun() {
        return date('Y');
    }

    function get_tanggal() {
        return date('Y-m-d');
    }

    function load_calon_mahasiswa_pasca() {
        $data = array();
        $this->db_oracle->Query("SELECT CMB.*,CMP.*,P.GELOMBANG,SUBSTR(J.NM_JENJANG, 1,2) NM_JENJANG FROM CALON_MAHASISWA_BARU CMB
		LEFT JOIN CALON_MAHASISWA_PASCA CMP ON CMP.ID_C_MHS = CMB.ID_C_MHS
		JOIN PENERIMAAN P ON P.ID_PENERIMAAN = CMB.ID_PENERIMAAN 
		JOIN JENJANG J ON P.ID_JENJANG = J.ID_JENJANG
		WHERE P.IS_AKTIF=1 AND CMB.TGL_VERIFIKASI_PPMB IS NOT NULL ORDER BY CMB.ID_PILIHAN_1");
        while ($temp = $this->db_oracle->FetchArray()) {
            array_push($data, $temp);
        }
        return $data;
    }

    function load_calon_mahasiswa_pmdk_2() {
        $data = array();
        $this->db_oracle->Query("SELECT * FROM CALON_MAHASISWA WHERE KODE_VOUCHER IS NOT NULL AND STATUS_PRA_UJIAN ='1' AND STATUS_TRANSFER ='0'");
        while ($temp = $this->db_oracle->FetchArray()) {
            array_push($data, $temp);
        }
        return $data;
    }

    function load_calon_mahasiswa_profesi() {
        $data = array();
        $this->db_oracle->Query("SELECT CM.* FROM CALON_MAHASISWA CM
        JOIN PROGRAM_STUDI PR ON PR.ID_PROGRAM_STUDI = CM.ID_PILIHAN_1
        JOIN JENJANG J ON J.ID_JENJANG = PR.ID_JENJANG
        WHERE PR.ID_JENJANG != '1' AND STATUS_TRANSFER ='0'");
        while ($temp = $this->db_oracle->FetchArray()) {
            array_push($data, $temp);
        }
        return $data;
    }

    function load_calon_mahasiswa_d3() {
        $data = array();
        $this->db_oracle->Query("SELECT CM.* FROM CALON_MAHASISWA CM
        WHERE CM.ID_JALUR = '5' AND STATUS_TRANSFER ='0' AND STATUS_PRA_UJIAN =1");
        while ($temp = $this->db_oracle->FetchArray()) {
            array_push($data, $temp);
        }
        return $data;
    }

    function load_calon_mahasiswa_alih() {
        $data = array();
        $this->db_oracle->Query("SELECT CM.* FROM CALON_MAHASISWA CM
        WHERE CM.ID_JALUR = '4' AND CM.STATUS_TRANSFER ='0' AND STATUS_PRA_UJIAN =1");
        while ($temp = $this->db_oracle->FetchArray()) {
            array_push($data, $temp);
        }
        return $data;
    }

    function get_jalur($jalur, $gelombang=null) {
        switch ($jalur) {
            case '1':return '3';
                break;
            case '2':return '1';
                break;
            case '3':return ($gelombang == '1') ? '2' : '4';
                break;
            case '4':return ($gelombang == '1') ? '25' : '27';
                break;
            case '5':return '5';
                break;
            case '6':
                if ($gelombang == '1') {
                    return '7';
                } else if ($gelombang == '2') {
                    return '8';
                }else
                    return '9';
                break;
            case '20':return'6';
                break;
        }
    }

    function get_propinsi($id_provinsi) {
        $this->db_oracle->Query("SELECT NM_PROVINSI FROM PROVINSI WHERE ID_PROVINSI ='$id_provinsi'");
        $data_oracle = $this->db_oracle->FetchArray();
        $q = mysql_query("select kode from tbpropinsi where propinsi like '%{$data_oracle['NM_PROVINSI']}%'");
        $data_mysql = mysql_fetch_array($q);
        return $data_mysql['kode'];
    }

    function get_kota($id_kota) {
        $this->db_oracle->Query("SELECT NM_KOTA FROM KOTA WHERE ID_KOTA ='$id_kota'");
        $data_oracle = $this->db_oracle->FetchArray();
        $q = mysql_query("select kodekota from tbpropkotakab where kota like '{$data_oracle['NM_KOTA']}'");
        $data_mysql = mysql_fetch_array($q);
        return $data_mysql['kodekota'];
    }

    function get_prodi($id_prodi, $jenjang) {
        if ($id_prodi != '') {
            $this->db_oracle->Query("SELECT NM_PROGRAM_STUDI FROM PROGRAM_STUDI WHERE ID_PROGRAM_STUDI ='$id_prodi'");
            $data_oracle = $this->db_oracle->FetchArray();
            $q = mysql_query("select kode from tbprodi where prodi like '{$data_oracle['NM_PROGRAM_STUDI']}' or prodi like 'Magister {$data_oracle['NM_PROGRAM_STUDI']}' or prodi like 'Ilmu {$data_oracle['NM_PROGRAM_STUDI']}'");
            //$data_mysql = mysql_fetch_array($q);
            if ($data_mysql == null) {
                switch ($id_prodi) {
					case '74': return '0554'; break;
					case '63': return '0453'; break;
					case '83': return '0651'; break;
					case '130': return '1051'; break;
					case '88': return '0652'; break;
					case '114': return '0853'; break;
					case '115': return '0852'; break;
					case '148': return '1450'; break;
					case '172': return '0552'; break;
					case '11': return '0156'; break;
					case '101': return '0753'; break;
					case '87': return '0650'; break;
					case '68': return '0454'; break;
					case '77': return '0550'; break;
					case '55': return '0350'; break;
					case '14': return '0150'; break;
					case '13': return '0154'; break;
					case '42': return '0250'; break;
					case '133': return '1053'; break;
					case '131': return '1050'; break;
					case '10': return '0151'; break;
					case '9': return '0152'; break;
					case '72': return '0450'; break;
					case '103': return '0751'; break;
					case '86': return '0653'; break;
					case '102': return '0754'; break;
					case '104': return '0750'; break;
					case '12': return '0153'; break;
					case '98': return '0752'; break;
					case '132': return '1052'; break;
					case '116': return '0851'; break;
					case '70': return '0455'; break;
					case '71': return '0452'; break;
					case '53': return '0352'; break;
					case '143': return '1250'; break;
					case '54': return '0351'; break;
					case '146': return '0155'; break;
					case '73': return '0456'; break;
					case '73': return '0451'; break;
					case '135': return '1152'; break;
					case '137': return '1150'; break;
					case '69': return '0458'; break;
					case '52': return '0353'; break;
					case '136': return '1151'; break;
					case '150': return '0755'; break;
					case '76': return '0551'; break;
					case '99': return '0756'; break;
					case '85': return '0654'; break;
					case '122': return '0980'; break;
					case '152': return '0981'; break;
					case '125': return '0976'; break;
					case '118': return '0983'; break;
					case '56': return '0975'; break;
					case '126': return '0973'; break;
					case '121': return '0978'; break;
					case '124': return '0977'; break;
					case '117': return '0974'; break;
					case '123': return '0979'; break;
					case '120': return '0984'; break;
					case '127': return '0982'; break;
					case '22': return '0178'; break;
					case '35': return '0169'; break;
					case '32': return '0177'; break;
					case '24': return '0180'; break;
					case '44': return '0252'; break;
					case '25': return '0173'; break;
					case '21': return '0175'; break;
					case '36': return '0159'; break;
					case '37': return '0160'; break;
					case '18': return '0176'; break;
					case '27': return '0170'; break;
					case '46': return '0254'; break;
					case '26': return '0158'; break;
					case '33': return '0163'; break;
					case '29': return '0162'; break;
					case '34': return '0161'; break;
					case '43': return '0255'; break;
					case '30': return '0157'; break;
					case '15': return '0166'; break;
					case '45': return '0257'; break;
					case '38': return '0167'; break;
					case '31': return '0164'; break;
					case '23': return '0179'; break;
					case '49': return '0251'; break;
					case '16': return '0171'; break;
					case '17': return '0172'; break;
					case '50': return '0253'; break;
					case '48': return '0256'; break;
					case '19': return '0168'; break;
					case '78': return '0553'; break;
					case '28': return '0174'; break;
                }
            } else {
                return $data_mysql['kode'];
            };
        }
    }

    function get_sekolah($id_sekolah) {
        $this->db_oracle->Query("SELECT NM_SEKOLAH FROM SEKOLAH WHERE ID_SEKOLAH ='$id_sekolah'");
        $data_oracle = $this->db_oracle->FetchArray();
        $q = mysql_query("select kodesekolah from tbkodesekolah where namasekolah like '{$data_oracle['NM_SEKOLAH']}'");
        $data_mysql = mysql_fetch_array($q);
        return $data_mysql['kodesekolah'];
    }

    function insert_data($no_peserta, $kode_jurusan, $tgl_daftar, $gelombang, $nama,$gelar, $alamat, $telp, $tgl_lahir, $kelamin, $warga, $agama, $kota, $sekolah, $nama_ortu, $alamat_ortu, $pendidikan_ayah, $kerja_ayah, $pendidikan_ibu, $kerja_ibu, $sumber_biaya, $adik, $kakak, $jalur, $pilihan_1, $pilihan_2, $pilihan_3, $pilihan_4,$jenjang,$pt_s1,$prodi_s1,$ip_s1,$pt_s2,$prodi_s2,$ip_s2) {
        $var_tahun = $this->get_tahun();
        $tgl_daftar = date('Y-m-d', strtotime($tgl_daftar));
        $tgl_lahir = date('Y-m-d', strtotime($tgl_lahir));
        $var_kota = $this->get_kota($kota);
        $var_sekolah = $this->get_sekolah($sekolah);
//        $var_prodi_1 = $jalur == '3' ? $this->get_prodi($pilihan_1, 'S1') : $this->get_prodi($pilihan_1, 'Sp');
//        $var_prodi_2 = $jalur == '3' ? $this->get_prodi($pilihan_2, 'S1') : $this->get_prodi($pilihan_2, 'Sp');
//        $var_prodi_3 = $jalur == '3' ? $this->get_prodi($pilihan_3, 'S1') : $this->get_prodi($pilihan_3, 'Sp');
//        $var_prodi_4 = $jalur == '3' ? $this->get_prodi($pilihan_4, 'S1') : $this->get_prodi($pilihan_4, 'Sp');
//        $var_prodi_1 = $jalur == '5' ? $this->get_prodi($pilihan_1, 'D3') : $this->get_prodi($pilihan_1, 'S1');
//        $var_prodi_2 = $jalur == '5' ? $this->get_prodi($pilihan_2, 'D3') : $this->get_prodi($pilihan_2, 'S1');
//        $var_prodi_3 = $jalur == '5' ? $this->get_prodi($pilihan_3, 'D3') : $this->get_prodi($pilihan_3, 'S1');
//        $var_prodi_4 = $jalur == '5' ? $this->get_prodi($pilihan_4, 'D3') : $this->get_prodi($pilihan_4, 'S1');
        $var_prodi_1 = $jalur == '6' ? $this->get_prodi($pilihan_1, 'S2') : $this->get_prodi($pilihan_1, 'S1');
        $var_prodi_2 = $jalur == '6' ? $this->get_prodi($pilihan_2, 'S2') : $this->get_prodi($pilihan_2, 'S1');
        $var_prodi_3 = $jalur == '6' ? $this->get_prodi($pilihan_3, 'S2') : $this->get_prodi($pilihan_3, 'S1');
        $var_prodi_4 = $jalur == '6' ? $this->get_prodi($pilihan_4, 'S2') : $this->get_prodi($pilihan_4, 'S1');
        $query = "
                insert into tbpasca_peserta (tahun,gelombang,saat_daftar,nomor_peserta,nama,gelar,alamat,telepon,tanggal_lahir,kelamin,agama,kode_kabupaten,jalur,pilihan_1,pilihan_2,strata,pt_s1,prodi_s1,ipk_s1,pt_s2,prodi_s2,ipk_s2)
        values 
                ('$var_tahun','$gelombang','$tgl_daftar','$no_peserta','$nama','$gelar','$alamat','$telp','$tgl_lahir','$kelamin','$agama','$var_kota','$jalur','$var_prodi_1','$var_prodi_2','$jenjang','$pt_s1','$prodi_s1','$ip_s1','$pt_s2','$prodi_s2','$ip_s2');</br>";
        return $query;
        //return mysql_query($query);
    }

    function update_status_transfer_no($no_ujian) {
        $this->db_oracle->Query("UPDATE CALON_MAHASISWA_BARU SET STATUS_TRANSFER ='1' WHERE NO_UJIAN ='$no_ujian'");
    }

    function update_status_transfer_id($id_c_mhs) {
        $this->db_oracle->Query("UPDATE CALON_MAHASISWA_BARU SET STATUS_TRANSFER ='1' WHERE ID_C_MHS ='$id_c_mhs'");
    }

}

?>
