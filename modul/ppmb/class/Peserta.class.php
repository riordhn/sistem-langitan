<?php
/**
 * Description of Peserta
 *
 * @author Fathoni
 */
class Peserta
{
    public $db;
    
    function __construct($db)
    {
        $this->db = $db;
    }

    function getTerbilang($x)
	{
	    $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
	    if ($x < 12) {
	        return " " . $abil[$x];
	    } elseif ($x < 20) {
	        return $this->getTerbilang($x - 10) . "belas";
	    } elseif ($x < 100) {
	        return $this->getTerbilang($x / 10) . " puluh" . $this->getTerbilang($x % 10);
	    } elseif ($x < 200) {
	        return " seratus" . $this->getTerbilang($x - 100);
	    } elseif ($x < 1000) {
	        return $this->getTerbilang($x / 100) . " ratus" . $this->getTerbilang($x % 100);
	    } elseif ($x < 2000) {
	        return " seribu" . $this->getTerbilang($x - 1000);
	    } elseif ($x < 1000000) {
	        return $this->getTerbilang($x / 1000) . " ribu" . $this->getTerbilang($x % 1000);
	    } elseif ($x < 1000000000) {
	        return $this->getTerbilang($x / 1000000) . " juta" . $this->getTerbilang($x % 1000000);
	    }
	}
}
?>
