<?php

class Penilaian
{
    private $db;
    
    function __construct(MyOracle $db)
    {
        $this->db = $db;
    }
    
    function AddKomponenNilai($id_penerimaan_prodi, $nm_komponen, $singkatan, $bobot, $is_tes_tulis, $keterangan)
    {
        // Mendapatkan urutan terbesar
        $urutan = $this->db->QuerySingle("select nvl(max(urutan),0) from penerimaan_komponen_nilai where id_penerimaan_prodi = {$id_penerimaan_prodi}") + 1;
        
        // Menginsert data
        $this->db->Parse("
            insert into penerimaan_komponen_nilai
                ( id_penerimaan_prodi, nm_komponen, singkatan, bobot, is_tes_tulis, urutan, keterangan)
            values
                (:id_penerimaan_prodi, :nm_komponen,:singkatan,:bobot,:is_tes_tulis,:urutan,:keterangan)");
        $this->db->BindByName(':id_penerimaan_prodi', $id_penerimaan_prodi);
        $this->db->BindByName(':nm_komponen', $nm_komponen);
        $this->db->BindByName(':singkatan', $singkatan);
        $this->db->BindByName(':bobot', $bobot);
        $this->db->BindByName(':is_tes_tulis', $is_tes_tulis);
        $this->db->BindByName(':urutan', $urutan);
        $this->db->BindByName(':keterangan', $keterangan);
        return $this->db->Execute();
    }
    
    function UpdateKomponenNilai($id_komponen_nilai, $nm_komponen, $singkatan, $bobot, $is_tes_tulis, $keterangan)
    {
        // update data
        $this->db->Parse("
            update penerimaan_komponen_nilai set
                nm_komponen = :nm_komponen,
                singkatan = :singkatan,
                bobot = :bobot,
                is_tes_tulis = :is_tes_tulis,
                keterangan = :keterangan
            where id_penerimaan_komponen_nilai = {$id_komponen_nilai}");
        $this->db->BindByName(':nm_komponen', $nm_komponen);
        $this->db->BindByName(':singkatan', $singkatan);
        $this->db->BindByName(':bobot', $bobot);
        $this->db->BindByName(':is_tes_tulis', $is_tes_tulis);
        $this->db->BindByName(':keterangan', $keterangan);
        return $this->db->Execute();
    }
    
    function DeleteKomponenNilai($id_komponen_nilai)
    {
        // reorder urutan
        $this->db->Query("
            update penerimaan_komponen_nilai set urutan = urutan - 1
            where 
                id_penerimaan_prodi = (select id_penerimaan_prodi from penerimaan_komponen_nilai where id_penerimaan_komponen_nilai = {$id_komponen_nilai}) and
                urutan > (select urutan from penerimaan_komponen_nilai where id_penerimaan_komponen_nilai = {$id_komponen_nilai})");
        
        // delete record
        return $this->db->Query("delete from penerimaan_komponen_nilai where id_penerimaan_komponen_nilai = {$id_komponen_nilai}");
    }
    
    function CopyKomponenNilai($id_penerimaan_prodi, $id_penerimaan_prodi_set)
    {
        
    }
    
    function GetKomponenNilai($id_komponen_nilai)
    {
        $rows = $this->db->QueryToArray("select * from penerimaan_komponen_nilai where id_penerimaan_komponen_nilai = {$id_komponen_nilai}");
        return $rows[0];
    }
    
    function GetListKomponenNilai($id_penerimaan_prodi)
    {
        return $this->db->QueryToArray("
            select * from penerimaan_komponen_nilai kn
            where id_penerimaan_prodi = {$id_penerimaan_prodi}
            order by urutan");
    }
}

?>
