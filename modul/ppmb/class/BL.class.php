<?php
/**
 * @property MyOracle $db
 */
class BL
{
	private $db;
	
	function __construct(&$db)
	{
		$this->db = $db;
	}
	
	function GetListBL($id_penerimaan)
	{
		return $this->db->QueryToArray("
			SELECT CMB.ID_C_MHS, CMB.NO_UJIAN, P.TAHUN, CMB.NM_C_MHS, CMD.PM_ORTU, CMD.PM_SPONSOR, CMD.PM_KETERANGAN, CMD.PM_STATUS,
				SPM.NM_SKOR_PM AS STATUS_ANAK, PS.NM_PROGRAM_STUDI
			FROM
			AUCC.CALON_MAHASISWA_BARU CMB
			INNER JOIN AUCC.CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS = CMB.ID_C_MHS
			LEFT JOIN AUCC.SKOR_PM SPM ON SPM.ID_SKOR_PM = CMD.ID_STATUS_ANAK_PM
			INNER JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CMB.ID_PILIHAN_1
			INNER JOIN AUCC.PENERIMAAN P ON P.ID_PENERIMAAN = CMB.ID_PENERIMAAN
			WHERE
			CMB.ID_PENERIMAAN = {$id_penerimaan} AND
			CMD.ID_PENGISI_PM IS NOT NULL");
	}
	
	function GetListInputBL($id_penerimaan, $id_pilihan_1 = '')
	{
		$id_pilihan_1 = ($id_pilihan_1 != '') ? "CMB.ID_PILIHAN_1 = {$id_pilihan_1} AND" : "";
		
		$skor_tulis = "0.8";
		$skor_bl = "0.2";
		
		$query_posisi = "
			select id_c_mhs, id_pilihan, total_nilai, row_number() over (partition by id_pilihan order by total_nilai desc) urutan
			from (
				select
					cmb.id_c_mhs, cmp.id_pilihan, (((n.nilai_tpa + n.nilai_prestasi)*{$skor_tulis}) + (n.nilai_bl*{$skor_bl})) as total_nilai
				from calon_mahasiswa_baru cmb
				join nilai_cmhs n on n.id_c_mhs = cmb.id_c_mhs
				join calon_mahasiswa_pilihan cmp on cmp.id_c_mhs = cmb.id_c_mhs and cmp.jurusan_sekolah = n.jurusan_sekolah
				where
					cmb.tgl_verifikasi_ppmb is not null and 
					cmb.id_penerimaan = {$id_penerimaan} and
					cmp.pilihan_ke = 1
				order by 3 desc
				) posisi";

		// Query Posisi di disable karena melakukan perhitungan skor tulis dan skor bl, hal ini hanya terjadi di ujian mandiri,
		// untuk peserta SNMPTN tidak ada skor tulis dan bl
		
		$cmb_set = $this->db->QueryToArray("
			SELECT 
				PMI.ID_PENERIMAAN, PMI.NO_UJIAN, PMI.ORTU, PMI.SPONSOR, PMI.KETERANGAN, PMI.ID_STATUS_ANAK, 
				CMB.NM_C_MHS, 
				SPM.NM_SKOR_PM AS STATUS_ANAK,
				PENERIMAAN.TAHUN, 
				PS1.NM_PROGRAM_STUDI AS NM_PILIHAN_1, 
				PS2.NM_PROGRAM_STUDI AS NM_PILIHAN_2,
				PS3.NM_PROGRAM_STUDI AS NM_PILIHAN_3,
				PS4.NM_PROGRAM_STUDI AS NM_PILIHAN_4
				/* posisi.urutan */
			FROM AUCC.CALON_MAHASISWA_PM_INPUT PMI
			LEFT JOIN AUCC.CALON_MAHASISWA_BARU CMB ON CMB.NO_UJIAN = PMI.NO_UJIAN
			LEFT JOIN PENERIMAAN ON PENERIMAAN.ID_PENERIMAAN = CMB.ID_PENERIMAAN
			LEFT JOIN PROGRAM_STUDI PS1 ON PS1.ID_PROGRAM_STUDI = CMB.ID_PILIHAN_1
			LEFT JOIN PROGRAM_STUDI PS2 ON PS2.ID_PROGRAM_STUDI = CMB.ID_PILIHAN_2
			LEFT JOIN PROGRAM_STUDI PS3 ON PS3.ID_PROGRAM_STUDI = CMB.ID_PILIHAN_3
			LEFT JOIN PROGRAM_STUDI PS4 ON PS4.ID_PROGRAM_STUDI = CMB.ID_PILIHAN_4
			LEFT JOIN AUCC.SKOR_PM SPM ON SPM.ID_SKOR_PM = PMI.ID_STATUS_ANAK
			/* LEFT JOIN ({$query_posisi}) posisi on posisi.id_c_mhs = cmb.id_c_mhs and posisi.id_pilihan = cmb.id_pilihan_1 */
			WHERE
				{$id_pilihan_1}
				PMI.ID_PENERIMAAN = {$id_penerimaan}
			ORDER BY PS1.NM_PROGRAM_STUDI, CMB.NM_C_MHS ASC, PMI.NO_UJIAN ASC");

		// Proses ini hanya untuk SNMPTN 2015 sementara
		if ($id_penerimaan == 273)
		{
			foreach ($cmb_set as &$cmb)
			{
				if (strlen($cmb['NO_UJIAN']) == 10)
				{
					$urutan = file_get_contents('http://10.0.110.55/snmptn2015/index.php/api/urutan_siswa_2/' . $cmb['NO_UJIAN']);
					$cmb['URUTAN'] = $urutan;
				}
				else
				{
					$cmb['URUTAN'] = $urutan;
				}
			}
		}

		return $cmb_set;
	}
	
	function GetPenerimaan($id_penerimaan)
	{
		$this->db->Query("select * from penerimaan where id_penerimaan = {$id_penerimaan}");
		return $this->db->FetchAssoc();
	}
	
	function GetListStatusAnak()
	{
		return $this->db->QueryToArray("select * from skor_pm where group_pm = 2");
	}
	
	function GetListJabatan()
	{
		return $this->db->QueryToArray("select * from skor_pm where group_pm = 1");
	}
	
	function GetListJabatanByNoUjian($no_ujian)
	{
		return $this->db->QueryToArray("
			select id_skor_pm, nm_skor_pm, nilai, nvl2(CMPM.no_ujian, 1, 0) checked from skor_pm spm
			left join CALON_MAHASISWA_PM_INPUT2 cmpm on CMPM.ID_JABATAN = SPM.ID_SKOR_PM and no_ujian = '{$no_ujian}'
			where group_pm = 1 
			order by id_skor_pm");
	}
	
	function AddBL(&$post)
	{
		$post['no_ujian'] = trim($post['no_ujian']);
		
		// Proses pengecekan
		$this->db->Query("select no_ujian from calon_mahasiswa_pm_input where no_ujian = '{$post['no_ujian']}' and id_penerimaan = {$post['id_penerimaan']}");
		$row = $this->db->FetchAssoc();
		
		if ( ! $row)
		{
			$this->db->BeginTransaction();

			$this->db->Parse("insert into calon_mahasiswa_pm_input (id_penerimaan, no_ujian, id_status_anak, ortu, sponsor, keterangan) values (:id_penerimaan,:no_ujian,:id_status_anak,:ortu,:sponsor,:keterangan)");
			$this->db->BindByName(':id_penerimaan', $post['id_penerimaan']);
			$this->db->BindByName(':no_ujian', $post['no_ujian']);
			$this->db->BindByName(':id_status_anak', $post['id_status_anak']);
			$this->db->BindByName(':ortu', $post['ortu']);
			$this->db->BindByName(':sponsor', $post['sponsor']);
			$this->db->BindByName(':keterangan', $post['keterangan']);
			$this->db->Execute();

			$this->db->Query("delete from calon_mahasiswa_pm_input2 where no_ujian = '{$post['no_ujian']}'");

			foreach ($post['id_jabatans'] as $id_jabatan)
				$this->db->Query("insert into calon_mahasiswa_pm_input2 (no_ujian, id_jabatan) values ('{$post['no_ujian']}', {$id_jabatan})");

			return $this->db->Commit();
		}
		else
		{
			return false;
		}
	}
	
	function UpdateBL(&$post)
	{
		
		$this->db->BeginTransaction();
		
		$this->db->Query("
			update calon_mahasiswa_pm_input set
				no_ujian = '{$post['no_ujian']}',
				id_status_anak = ".(($post['id_status_anak']=='')?'NULL':$post['id_status_anak']).",
				ortu = '{$post['ortu']}',
				sponsor = '{$post['sponsor']}',
				keterangan = '{$post['keterangan']}'
			where no_ujian = '{$post['no_ujian']}' and id_penerimaan = {$post['id_penerimaan']}");
			
		$this->db->Query("delete from calon_mahasiswa_pm_input2 where no_ujian = '{$post['no_ujian']}'");
		
		foreach ($post['id_jabatans'] as $id_jabatan)
			$this->db->Query("insert into calon_mahasiswa_pm_input2 (no_ujian, id_jabatan) values ('{$post['no_ujian']}', {$id_jabatan})");
			
		$this->db->Commit();
		
	}
	
	function GetBL($no_ujian)
	{
		$this->db->Query("select * from calon_mahasiswa_pm_input where no_ujian = '{$no_ujian}'");
		$row = $this->db->FetchAssoc();
		$row['jabatan_set'] = $this->GetListJabatanByNoUjian($row['NO_UJIAN']);
		return $row;
	}
	
	function DeleteBL($no_ujian, $id_penerimaan)
	{
		return $this->db->Query("delete from calon_mahasiswa_pm_input where no_ujian = '{$no_ujian}' and id_penerimaan = {$id_penerimaan}");
	}
}

?>
