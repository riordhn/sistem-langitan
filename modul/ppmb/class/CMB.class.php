<?php

class CMB
{
	private $db;
	
	public function __construct($oracle_db)
	{
		$this->db = $oracle_db;
	}
	
	/**
	 * Mendapatkan data SYARAT FILE calon_mahasiswa 
	 * @param int $id_c_mhs
	 */
	public function GetListSyaratProdi($id_c_mhs)
	{
		return $this->db->QueryToArray(
			"select psp.id_syarat_prodi, psp.nm_syarat, cmsy.file_syarat
			from calon_mahasiswa_syarat cmsy
			join calon_mahasiswa_baru cmb on cmb.id_c_mhs = cmsy.id_c_mhs
			join penerimaan_syarat_prodi psp on psp.id_syarat_prodi = cmsy.id_syarat_prodi
			where cmb.id_c_mhs = {$id_c_mhs}");
	}
}
