<?php

$id_pt = $id_pt_user;

/**
 * Description of Pengawas
 *
 * @author User
 */
class Pengawas
{
	public $Print;
		
	private $db;
	
	function __construct(MyOracle $myoracle)
	{
		$this->db = $myoracle;
		
		
	}
	
	function GetListGolongan()
	{
		return $this->db->QueryToArray("select * from golongan order by nm_golongan");
	}
	
	function GetListJabatan()
	{
		return $this->db->QueryToArray("select * from ppmb_jabatan_pengawas");
	}
	
	function GetListAsalPengawas()
	{
		return $this->db->QueryToArray("select * from ppmb_asal_pengawas order by id_asal");
	}
	
	function GetListUjian()
	{
		return $this->db->QueryToArray("
			select id_ujian, nm_ujian, no_sk, to_char(tgl_ujian, 'YYYY-MM-DD') as tgl_ujian, gelombang, semester, tahun
			from ppmb_ujian 
			where id_perguruan_tinggi = '{$GLOBALS[id_pt]}'
			order by tgl_ujian desc");
	}
	
	function GetUjian($id_ujian)
	{
		$this->db->Query("
			select id_ujian, nm_ujian, no_sk, to_char(tgl_ujian, 'YYYY-MM-DD') as tgl_ujian, gelombang, semester, tahun
			from ppmb_ujian where id_ujian = {$id_ujian}");
		return $this->db->FetchAssoc();
	}
	
	function UpdateUjian($id_ujian, &$post)
	{
		$tgl_ujian = $post['tgl_ujian_Year'] . $post['tgl_ujian_Month'] . str_pad($post['tgl_ujian_Day'], 2, '0', STR_PAD_LEFT);
		return $this->db->Query("
			update ppmb_ujian set
				nm_ujian    = '{$post['nm_ujian']}',
				no_sk       = '{$post['no_sk']}',
				gelombang   = '{$post['gelombang']}',
				tgl_ujian   = to_date('{$tgl_ujian}','YYYYMMDD'),
				semester    = '{$post['semester']}',
				tahun       = '{$post['tahun']}'
			where id_ujian = {$id_ujian}");
	}
	
	function AddUjian(&$post)
	{
		$tgl_ujian = $post['tgl_ujian_Year'] . $post['tgl_ujian_Month'] . str_pad($post['tgl_ujian_Day'], 2, '0', STR_PAD_LEFT);
		
		$this->db->Parse("insert into ppmb_ujian ( nm_ujian, no_sk, tgl_ujian, gelombang, semester, tahun, id_perguruan_tinggi) values (:nm_ujian,:no_sk,to_date(:tgl_ujian,'YYYYMMDD'),:gelombang,:semester,:tahun,:id_perguruan_tinggi)");
		$this->db->BindByName(':nm_ujian', $post['nm_ujian']);
		$this->db->BindByName(':no_sk', $post['no_sk']);
		$this->db->BindByName(':tgl_ujian', $tgl_ujian);
		$this->db->BindByName(':gelombang', $post['gelombang']);
		$this->db->BindByName(':semester', $post['semester']);
		$this->db->BindByName(':tahun', $post['tahun']);
		$this->db->BindByName(':id_perguruan_tinggi', $GLOBALS[id_pt]);
		return $this->db->Execute();
	}
	
	function DeleteUjian($id_ujian)
	{
		$this->db->BeginTransaction();
		
		$this->db->Query("update penerimaan set id_ujian = null where id_ujian = {$id_ujian}");
		$this->db->Query("delete from ppmb_honor_pengawas       where id_ujian = {$id_ujian}");
		$this->db->Query("delete from ppmb_koordinator          where id_ujian = {$id_ujian}");
		$this->db->Query("delete from ppmb_distributor          where id_ujian = {$id_ujian}");
		$this->db->Query("delete from ppmb_ujian                where id_ujian = {$id_ujian}");
		
		return $this->db->Commit();
	}
	
	function GetListPenerimaanByUjian($id_ujian)
	{
		return $this->db->QueryToArray("
			select p.id_penerimaan, nm_jalur, nm_jenjang, nm_penerimaan, gelombang, semester, tahun
			from penerimaan p 
			join jalur jl on jl.id_jalur = p.id_jalur
			join jenjang jj on jj.id_jenjang = p.id_jenjang
			where id_ujian = {$id_ujian}");
	}
	
	function DeletePenerimaanFromUjian($id_penerimaan)
	{
		return $this->db->Query("update penerimaan set id_ujian = null where id_penerimaan = {$id_penerimaan}");
	}
	
	function AddPenerimaanToUjian($id_penerimaan, $id_ujian)
	{
		return $this->db->Query("update penerimaan set id_ujian = {$id_ujian} where id_penerimaan = {$id_penerimaan}");
	}
	
	function GetListPenerimaanForUjian()
	{
		$penerimaan_set = $this->db->QueryToArray("select distinct tahun, semester from penerimaan where id_ujian is null and id_perguruan_tinggi = '{$GLOBALS[id_pt]}' order by tahun desc, semester desc");
		
		foreach ($penerimaan_set as &$p)
			$p['p_set'] = $this->db->QueryToArray("
				select p.id_penerimaan, j.nm_jenjang, p.nm_penerimaan,jl.nm_jalur, p.gelombang, j.id_jenjang from penerimaan p
				join jenjang j on j.id_jenjang = p.id_jenjang
				join jalur jl on jl.id_jalur = p.id_jalur
				where id_ujian is null and p.tahun = {$p['TAHUN']} and p.semester = '{$p['SEMESTER']}'
				order by p.gelombang asc, p.id_jalur, p.id_jenjang");

		return $penerimaan_set;
	}
	
	function SetKoordinatorRuang(&$post)
	{    
		// inputan : id_jadwal_ppmb, id_pengawas, id_ujian
		
		$this->db->BeginTransaction();
		
		// ambil pengawas
		$this->db->Query("select count(*) from ppmb_koordinator_ruang where id_ujian = {$post['id_ujian']} and id_pengawas = {$post['id_pengawas']}");
		$row = $this->db->FetchRow();
		$koordinator_ruang_exist = ($row[0] > 0) ? true : false;
			
		if (!$koordinator_ruang_exist)
		{
			$this->db->Query("insert into ppmb_koordinator_ruang (id_ujian, id_pengawas) values ({$post['id_ujian']}, {$post['id_pengawas']})");
		}
		
		// ambil data id_koordinator ruang
		$this->db->Query("select id_koordinator_ruang from ppmb_koordinator_ruang where id_ujian = {$post['id_ujian']} and id_pengawas = {$post['id_pengawas']}");
		$row = $this->db->FetchRow();
		$id_koordinator_ruang = $row[0];
		
		// set jadwal
		$this->db->Query("update jadwal_ppmb set id_koordinator_ruang = {$id_koordinator_ruang} where id_jadwal_ppmb = {$post['id_jadwal_ppmb']}");
		
		$this->db->Commit();
		
		// Mengambil data nama pengawas
		$this->db->Query("select nm_pengawas from ppmb_pengawas where id_pengawas = {$post['id_pengawas']}");
		$row = $this->db->FetchRow();
		
		return $row[0];
	}
	
	function UnsetKoordinatorRuang(&$post)
	{
		$this->db->BeginTransaction();
		
		$this->db->Query("select id_koordinator_ruang from jadwal_ppmb where id_jadwal_ppmb = {$post['id_jadwal_ppmb']}");
		$row = $this->db->FetchRow();
		$id_koordinator_ruang = $row[0];
		
		$return = $this->db->Query("update jadwal_ppmb set id_koordinator_ruang = null where id_jadwal_ppmb = {$post['id_jadwal_ppmb']}");
		
		// cek jika tidak ada jadwal lagi terhadap koordinator, maka koordinator perlu di hapus
		$this->db->Query("select count(*) from jadwal_ppmb where id_koordinator_ruang = {$id_koordinator_ruang}");
		$row = $this->db->FetchRow();
		
		if ($row[0] == 0) 
			$this->db->Query("delete from ppmb_koordinator_ruang where id_koordinator_ruang = {$id_koordinator_ruang}");
			
		return $this->db->Commit();
	}
	
	function SetPengawas(&$post)
	{
		$this->db->Query("
			insert into ppmb_pengawas_ruang (id_jadwal_ppmb, id_pengawas, id_jabatan) values
			({$post['id_jadwal_ppmb']}, {$post['id_pengawas']}, {$post['id_jabatan']})");
		
		$this->db->Query("
			select pr.id_jadwal_ppmb, p.id_pengawas, jp.singkatan_jabatan, p.nm_pengawas, p.jabatan, a.nm_asal from ppmb_pengawas_ruang pr
			join ppmb_pengawas p on p.id_pengawas = pr.id_pengawas
			join ppmb_jabatan_pengawas jp on jp.id_jabatan = pr.id_jabatan
			join ppmb_asal_pengawas a on a.id_asal = p.id_asal
			where pr.id_jadwal_ppmb = {$post['id_jadwal_ppmb']} and pr.id_pengawas = {$post['id_pengawas']}");
			
		return $this->db->FetchAssoc();
	}
	
	function UnsetPengawas(&$post)
	{
		return $this->db->Query("delete from ppmb_pengawas_ruang where id_jadwal_ppmb = {$post['id_jadwal_ppmb']} and id_pengawas = {$post['id_pengawas']}");
	}
	
	function GetListJadwalAktif($id_penerimaan)
	{
		return $this->db->QueryToArray("
			select id_jadwal_ppmb, nomer_awal, nomer_akhir, lokasi, nm_ruang, kode_jalur, kapasitas from jadwal_ppmb
			where id_penerimaan = {$id_penerimaan} and status_aktif = 1
			order by kode_jalur, nomer_awal");
	}
	
	function GetListPlotPengawas($id_ujian)
	{
		$jadwal_set = $this->db->QueryToArray("
			select 
				-- jadwal ppmb
				jp.id_jadwal_ppmb, jp.nomer_awal, jp.nomer_akhir, j.nm_jalur, js.nm_jurusan, jp.lokasi, jp.nm_ruang, jp.kapasitas,
				-- koordinator
				(select p.nm_pengawas from ppmb_koordinator_ruang ko
				join ppmb_pengawas p on p.id_pengawas = ko.id_pengawas 
				where ko.id_koordinator_ruang = jp.id_koordinator_ruang) as nm_koordinator
			from jadwal_ppmb jp
			join penerimaan p on p.id_penerimaan = jp.id_penerimaan
			join jalur j on j.id_jalur = p.id_jalur
			join jurusan_sekolah js on js.id_jurusan = jp.kode_jalur
			where p.id_ujian = {$id_ujian}
			order by nomer_awal");
			
		foreach ($jadwal_set as &$j)
		{
			$j['pengawas_ruang_set'] = $this->db->QueryToArray("
				select pr.id_pengawas, p.nm_pengawas, jp.singkatan_jabatan, a.nm_asal, p.jabatan
				from ppmb_pengawas_ruang pr
				join ppmb_pengawas p on p.id_pengawas = pr.id_pengawas
				join ppmb_jabatan_pengawas jp on jp.id_jabatan = pr.id_jabatan
				join ppmb_asal_pengawas a on a.id_asal = p.id_asal
				where pr.id_jabatan in (1, 2) and pr.id_jadwal_ppmb = {$j['ID_JADWAL_PPMB']}
				order by pr.id_jabatan desc");
		}
			
		return $jadwal_set;
	}
	
	function GetJadwalPPMB($id_jadwal_ppmb)
	{
		$rows = $this->db->QueryToArray("
			select nomer_awal, nomer_akhir, lokasi, nm_ruang, nm_jurusan, j.nm_jalur
			from jadwal_ppmb jp
			join jurusan_sekolah js on js.id_jurusan = kode_jalur
			join penerimaan p on p.id_penerimaan = jp.id_penerimaan
			join jalur j on j.id_jalur = p.id_jalur
			where jp.id_jadwal_ppmb = {$id_jadwal_ppmb}");
		return $rows[0];
	}
	
	function GetListHonorarium($id_ujian)
	{
		if ($id_ujian == '') { return 0; }
		
		return $this->db->QueryToArray("
			select h.id_honor, j.nm_jabatan, golongan, besar_honor, pajak,
				case kode_jurusan when 1 then 'IPA' when 2 then 'IPS' when 3 then 'IPC' end as kode_jurusan
			from ppmb_honor_pengawas h
			join ppmb_jabatan_pengawas j on j.id_jabatan = h.id_jabatan
			where h.id_ujian = {$id_ujian}
			order by h.kode_jurusan asc, h.id_jabatan desc, golongan desc");
	}
	
	function AddHonorPengawas($id_ujian, &$post)
	{
		$this->db->BeginTransaction();
		
		// memastikan ada centangan
		if (!empty($post['golongan']) && !empty($post['kode_jurusan']))
		{
			// looping golongan
			foreach ($post['golongan'] as $golongan)
			{
				// looping jurusan
				foreach ($post['kode_jurusan'] as $kode_jurusan)
				{
					$this->db->Parse("insert into ppmb_honor_pengawas (id_ujian, id_jabatan, golongan, kode_jurusan, besar_honor, pajak) values (:id_ujian,:id_jabatan,:golongan,:kode_jurusan,:besar_honor,:pajak)");
					$this->db->BindByName(':id_ujian', $id_ujian);
					$this->db->BindByName(':id_jabatan', $post['id_jabatan']);
					$this->db->BindByName(':golongan', $golongan);
					$this->db->BindByName(':kode_jurusan', $kode_jurusan);
					$this->db->BindByName(':besar_honor', $post['besar_honor']);
					$this->db->BindByName(':pajak', $post['pajak']);
					$this->db->Execute();
				}
			}
		}
		
		return $this->db->Commit();
	}
	
	function DeleteHonorPengawas($id_honor)
	{
		return $this->db->Query("delete from ppmb_honor_pengawas where id_honor = {$id_honor}");
	}
	
	function GetListPengawasReady($id_jadwal_ppmb, $id_asal)
	{
		$query = "
			SELECT pp.*, pap.nm_asal
			FROM ppmb_pengawas pp
			join ppmb_asal_pengawas pap on pap.id_asal = pp.id_asal
			WHERE
				pp.id_asal = {$id_asal} and pp.id_pengawas NOT IN (
					SELECT id_pengawas
					FROM ppmb_pengawas_ruang
					WHERE id_jadwal_ppmb IN (
							SELECT id_jadwal_ppmb
							FROM jadwal_ppmb
							WHERE id_penerimaan IN (
								select id_penerimaan from penerimaan where id_ujian in (
									select id_ujian from penerimaan where id_penerimaan in (
										SELECT id_penerimaan FROM jadwal_ppmb WHERE ID_JADWAL_PPMB = {$id_jadwal_ppmb}
									)
								)
							)
					)
			)
			order by nm_pengawas";
		return $this->db->QueryToArray($query);

		// Query Lama
		/**
		return $this->db->QueryToArray("
			select p.*, a.nm_asal from ppmb_pengawas p
			join ppmb_asal_pengawas a on a.id_asal = p.id_asal
			where p.id_asal = {$id_asal} and id_pengawas not in (
				select id_pengawas from ppmb_pengawas_ruang
				where id_jadwal_ppmb in (
					select jp.id_jadwal_ppmb from penerimaan p
					join jadwal_ppmb jp on jp.id_penerimaan = p.id_penerimaan
					where jp.status_aktif = 1 and id_ujian in (
						select p.id_ujian from jadwal_ppmb jp
						join penerimaan p on p.id_penerimaan = jp.id_penerimaan
						where jp.id_jadwal_ppmb = {$id_jawdal_ppmb})))
			order by nm_asal, nm_pengawas");
		**/
	}
	
	function GetListPengawas()
	{
		return $this->db->QueryToArray("
			select id_pengawas, nip_pengawas, nm_pengawas, golongan_pengawas, nm_asal, jabatan
			from ppmb_pengawas p
			join ppmb_asal_pengawas a on a.id_asal = p.id_asal
			order by nm_asal, nm_pengawas");
	}
	
	function GetListPengawasByAsal($id_asal)
	{
		return $this->db->QueryToArray("
			select id_pengawas, nip_pengawas, nm_pengawas, golongan_pengawas, nm_asal, jabatan
			from ppmb_pengawas p
			join ppmb_asal_pengawas a on a.id_asal = p.id_asal
			where p.id_asal = {$id_asal}
			order by nm_asal, nm_pengawas");
	}
	
	function AddPengawas(&$post)
	{
		$this->db->Parse("
			insert into ppmb_pengawas
			( nip_pengawas, nm_pengawas, golongan_pengawas, id_asal, jabatan) values
			(:nip_pengawas,:nm_pengawas,:golongan_pengawas,:id_asal,:jabatan)");
		$this->db->BindByName(':nip_pengawas', $post['nip_pengawas']);
		$this->db->BindByName(':nm_pengawas', $post['nm_pengawas']);
		$this->db->BindByName(':golongan_pengawas', $post['golongan_pengawas']);
		$this->db->BindByName(':id_asal', $post['id_asal']);
		$this->db->BindByName(':jabatan', $post['jabatan']);
		return $this->db->Execute();
	}
	
	function GetPengawas($id_pengawas) 
	{
		$this->db->Query("select * from ppmb_pengawas where id_pengawas = {$id_pengawas}");
		return $this->db->FetchAssoc();
	}
	
	function UpdatePengawas(&$post)
	{
		return $this->db->Query("
			update ppmb_pengawas set 
				nip_pengawas        = '{$post['nip_pengawas']}',
				nm_pengawas         = '{$post['nm_pengawas']}',
				golongan_pengawas   = '{$post['golongan_pengawas']}',
				id_asal             = '{$post['id_asal']}',
				jabatan             = '{$post['jabatan']}'
			where id_pengawas = {$post['id_pengawas']}");
	}
	
	function DeletePengawas($id_pengawas)
	{
		return $this->db->Query("delete from ppmb_pengawas where id_pengawas = {$id_pengawas}");
	}
	
	function UploadFilePengawas($token, &$file)
	{
		if ($file['error'] == 0)
		{
			// cek file harus excel
			if ($file['type'] == 'application/vnd.ms-excel')
			{
				if (move_uploaded_file($file['tmp_name'], "/var/www/html/files/upload/ppmb/pengawas/{$token}"))
				{
					return $file['name'];
				}
			}
		}
		
		return false;
	}
	
	function ProsesFilePengawas($token, PHPExcel_Reader_Excel5 &$excel_reader)
	{
		$excel_reader->setReadDataOnly(true);
		$excel = $excel_reader->load("/var/www/html/files/upload/ppmb/pengawas/{$token}");
		$sheet = &$excel->getSheet(0); // sheet pertama
		
		$result = array("add" => 0, "update" => 0, "error" => 0, "error_set" => array());
		
		for ($i = 2; $i <= $sheet->getHighestRow(); $i++)
		{
			// mendapatkan nilainya
			$asal       = $sheet->getCell("A".$i)->getValue();
			$nip        = $sheet->getCell("B".$i)->getValue();
			$nama       = $sheet->getCell("C".$i)->getValue();
			$golongan   = $sheet->getCell("D".$i)->getValue();
			$jabatan    = $sheet->getCell("E".$i)->getValue();
			
			// Cek golongan
			if (!in_array($golongan, array('I', 'II', 'III', 'IV', 'H')))
			{
				$result['error'] = $result['error'] + 1;
				array_push($result['error_set'], array($i, $nip, $nama, $golongan, $asal, $jabatan, 'Golongan tidak sesuai'));
				continue;
			}
			
			// cek jabatan
			if (!in_array($jabatan, array('D', 'TK')))
			{
				$result['error'] = $result['error'] + 1;
				array_push($result['error_set'], array($i, $nip, $nama, $golongan, $asal, $jabatan, 'Jabatan tidak sesuai'));
				continue;
			}
			
			// cek id_asal
			$this->db->Query("select id_asal from ppmb_asal_pengawas where nm_asal = '{$asal}'");
			$row_asal = $this->db->FetchRow();
			
			if (!$row_asal)
			{
				$result['error'] = $result['error'] + 1;
				array_push($result['error_set'], array($i, $nip, $nama, $golongan, $asal, $jabatan, 'Asal pengawas tidak sesuai'));
				continue;
			}
			
			// cek nip_pengawas
			if (!is_numeric($nip))
			{
				$result['error'] = $result['error'] + 1;
				array_push($result['error_set'], array($i, $nip, $nama, $golongan, $asal, $jabatan, 'NIP bukan angka'));
				continue;
			}

			// cek di database
			$this->db->Query("select count(*) from ppmb_pengawas where nip_pengawas = '{$nip}'");
			$jumlah = $this->db->FetchRow();
			
			// trim nama
			$nama = trim($nama);

			if ($jumlah[0] == 0) // belum ada, maka di tambahkan
			{
				$r = $this->db->Query("
					insert into ppmb_pengawas (nip_pengawas, nm_pengawas, golongan_pengawas, id_asal, jabatan) values
					('{$nip}','{$nama}','{$golongan}',{$row_asal[0]},'{$jabatan}')");
				if ($r)
					$result['add'] = $result['add'] + 1;
				else
				{
					$result['error'] = $result['error'] + 1;
					array_push($result['error_set'], array($i, $nip, $nama, $golongan, $asal, $jabatan, 'Gagal insert'));
				}
			}
			else // sudah ada, maka diupdate
			{
				$r = $this->db->Query("
					update ppmb_pengawas set
						nm_pengawas         = '{$nama}',
						golongan_pengawas   = '{$golongan}',
						id_asal             = '{$row_asal[0]}',
						jabatan             = '{$jabatan}'
					where nip_pengawas = '{$nip}'");
				if ($r)
					$result['update'] = $result['update'] + 1;
				else
				{
					$result['error'] = $result['error'] + 1;
					array_push($result['error_set'], array($i, $nip, $nama, $golongan, $asal, $jabatan, 'Gagal update'));
				}
			}
			
		}
		
		return $result;
	}
	
	function UploadFileAbsensi($token, &$file)
	{
		if ($file['error'] == 0)
		{
			// cek file harus excel
			if ($file['type'] == 'application/vnd.ms-excel')
			{
				if (move_uploaded_file($file['tmp_name'], "/data/files/upload/ppmb/absensi/{$token}"))
				{
					return $file['name'];
				}
				else
				{
					return false;
				}
			}
		}

		return false;
	}
	
	function ProsesFileAbsensi($token, PHPExcel_Reader_Excel5 &$excel_reader)
	{
		$excel_reader->setReadDataOnly(true);
		$excel = $excel_reader->load("/data/files/upload/ppmb/absensi/{$token}");
		$sheet = &$excel->getSheet(0); // sheet pertama
		
		// konstanta2
		$status_master = array('Tidak Ujian', 'Datang Ujian', 'Salah Identitas', 'Melakukan Kecurangan', 'Salah Kode Soal');
		$result_set = array();
		
		for ($i = 2; $i <= $sheet->getHighestRow(); $i++)
		{
			// mengambil variabel dulu
			$no_ujian = $sheet->getCell('A'.$i)->getValue();
			$status_ujian = $sheet->getCell('B'.$i)->getValue();
			
			// mengecek eksistensi no ujian
			$this->db->Query("
				select count(*) from calon_mahasiswa_baru cmb
				where id_penerimaan in (select id_penerimaan from penerimaan where is_aktif = 1) and
				no_ujian = '{$no_ujian}'");
			$row = $this->db->FetchRow();
			
			if ($row[0] > 0)
			{
				$result = $this->db->Query("update calon_mahasiswa_baru set status_ujian = {$status_ujian} where no_ujian = '{$no_ujian}'");
				
				// Jika ada
				if ($result)
					array_push($result_set, array($no_ujian, $status_master[$status_ujian], $status_ujian));
				else
					array_push($result_set, array($no_ujian, "Gagal update"));
			}
			else
			{
				// Jika tidak ada
				array_push($result_set, array($no_ujian, 'Nomer ujian tidak ditemukan'));
			}
			
			
		}
		
		//print_r($result_set); exit();
		
		return $result_set;
	}
	
	function GetTotalPengawas($id_ujian)
	{
		$this->db->Query("
			select (
				select count(id_pengawas) from ppmb_ujian u
				  join penerimaan p on p.id_ujian = u.id_ujian
				  join jadwal_ppmb jp on jp.id_penerimaan = p.id_penerimaan
				  join ppmb_pengawas_ruang pr on pr.id_jadwal_ppmb = jp.id_jadwal_ppmb
				  where u.id_ujian = {$id_ujian} and pr.id_jabatan = 1
				  group by u.id_ujian
				) as jumlah_pengawas,
				(
				  select count(id_pengawas) from ppmb_ujian u
				  join penerimaan p on p.id_ujian = u.id_ujian
				  join jadwal_ppmb jp on jp.id_penerimaan = p.id_penerimaan
				  join ppmb_pengawas_ruang pr on pr.id_jadwal_ppmb = jp.id_jadwal_ppmb
				  where u.id_ujian = {$id_ujian} and pr.id_jabatan = 2
				  group by u.id_ujian
				) as jumlah_kr
			  from dual");
		return $this->db->FetchAssoc();
	}
}

?>
