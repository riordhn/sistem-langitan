<?php

class ppmb
{

    public $database;

    function __construct(MyOracle $database)
    {
        $this->database = $database;
    }

    function load_penerimaan()
    {
        return $this->database->QueryToArray("
			SELECT JAL.NM_JALUR,J.NM_JENJANG,P.* FROM PENERIMAAN P
			JOIN JENJANG J ON P.ID_JENJANG = J.ID_JENJANG
			JOIN JALUR JAL ON JAL.ID_JALUR=P.ID_JALUR
			WHERE IS_AKTIF = 1
			ORDER BY P.IS_AKTIF DESC,P.TAHUN,P.TAHUN, P.SEMESTER, P.GELOMBANG, P.ID_JALUR, P.ID_JENJANG
		");
    }

    function load_penerimaan_by_id($id_penerimaan)
    {
        $this->database->Query("SELECT * FROM PENERIMAAN WHERE ID_PENERIMAAN='$id_penerimaan'");
        return $this->database->FetchAssoc();
    }

    function load_jadwal_ppmb($id_penerimaan)
    {
        return $this->database->QueryToArray("
			SELECT JP.*,P.IS_AKTIF,P.NM_PENERIMAAN,
                        (select count(*) from plot_jadwal_ppmb pjp where pjp.id_jadwal_ppmb = jp.id_jadwal_ppmb and pjp.id_c_mhs is not null) diisi
			FROM JADWAL_PPMB JP 
			LEFT JOIN PENERIMAAN P ON P.ID_PENERIMAAN = JP.ID_PENERIMAAN
			WHERE JP.ID_PENERIMAAN = '{$id_penerimaan}' 
			ORDER BY JP.GELOMBANG,JP.KODE_JALUR,JP.NOMER_AWAL,JP.ID_JADWAL_PPMB");
    }

    function load_jadwal_ppmb_by_id($id_jadwal)
    {
        $this->database->Query("SELECT * FROM JADWAL_PPMB WHERE ID_JADWAL_PPMB ='$id_jadwal'");
        return $this->database->FetchAssoc();
    }

    function load_ruang_ppmb()
    {
        $data = array();
        $this->database->Query("SELECT * FROM RUANG_PPMB ORDER BY ID_RUANG_PPMB");
        while ($temp = $this->database->FetchArray())
        {
            array_push($data, $temp);
        }
        return $data;
    }

    function load_ruang_ppmb_by_id($id_ruang)
    {
        $this->database->Query("SELECT * FROM RUANG_PPMB WHERE ID_RUANG_PPMB ='$id_ruang'");
        $temp = $this->database->FetchArray();
        return $temp;
    }
    
    function GetNoTerakhir($id_penerimaan)
    {
        $this->database->Query("select max(nomer_awal + kapasitas - 1) as no_akhir from jadwal_ppmb where id_penerimaan = {$id_penerimaan}");
        $row = $this->database->FetchRow();
        return $row[0];
    }

    function add_jadwal_ppmb($id_fakultas, $lokasi, $alamat, $ruang, $tgl, $tgl2, $tgl3, $kapasitas, $jalur, $nomer_awal, $id_penerimaan, $materi, $dua_hari_test, $tiga_hari_test, $waktu)
    {
        $this->database->Query("
            insert into jadwal_ppmb 
                (id_fakultas, lokasi, alamat, nm_ruang, tgl_test, tgl_test2, tgl_test3, kapasitas, kode_jalur, nomer_awal, id_penerimaan, terisi, materi, dua_hari_test, tiga_hari_test, waktu) 
            values 
                ({$id_fakultas}, '{$lokasi}','{$alamat}','{$ruang}','{$tgl}','{$tgl2}','{$tgl3}','{$kapasitas}','{$jalur}','{$nomer_awal}','{$id_penerimaan}',0,'{$materi}', {$dua_hari_test}, {$tiga_hari_test},'{$waktu}')");
    }

    function add_ruang_ppmb($nm_ruang, $alamat, $kapasitas)
    {
        $this->database->Query("INSERT INTO RUANG_PPMB (NM_RUANG_PPMB,ALAMAT,KAPASITAS) VALUES ('$nm_ruang','$alamat','$kapasitas')");
    }

    function update_jadwal_ppmb($id_jadwal, $lokasi, $alamat, $ruang, $tgl, $tgl2, $tgl3, $kapasitas, $jalur, $nomer_awal, $materi, $dua_hari_test, $tiga_hari_test, $waktu)
    {
        return $this->database->Query("
            UPDATE JADWAL_PPMB SET 
                LOKASI='$lokasi',
                ALAMAT='$alamat',
                NM_RUANG='$ruang',
                TGL_TEST='$tgl',
                TGL_TEST2='$tgl2',
				TGL_TEST3='$tgl3',
                DUA_HARI_TEST='$dua_hari_test',
				TIGA_HARI_TEST='$tiga_hari_test',
                KAPASITAS='$kapasitas',
                NOMER_AWAL='$nomer_awal',
                KODE_JALUR='$jalur',
                MATERI='$materi',
                WAKTU='{$waktu}'
            WHERE ID_JADWAL_PPMB='$id_jadwal'");
    }

    function update_aktif_jadwal_ppmb($id_fakultas, $id_jadwal, $lokasi, $alamat, $ruang, $tgl, $tgl2, $tgl3, $materi, $dua_hari_test, $tiga_hari_test, $waktu)
    {
        return $this->database->Query("
            UPDATE JADWAL_PPMB SET 
                ID_FAKULTAS = '{$id_fakultas}',
                LOKASI='$lokasi',
                ALAMAT='$alamat',
                NM_RUANG='$ruang',
                TGL_TEST='$tgl',
                TGL_TEST2='$tgl2',
				TGL_TEST3='$tgl3',
                DUA_HARI_TEST='$dua_hari_test',
				TIGA_HARI_TEST='$tiga_hari_test',
                MATERI='$materi',
                WAKTU='{$waktu}'
            WHERE ID_JADWAL_PPMB='$id_jadwal'");
    }

    function update_jadwal_ppmb_nomer($id_jadwal, $no_awal, $no_akhir)
    {
        $this->database->Query("UPDATE JADWAL_PPMB SET NOMER_AWAL ='$no_awal',NOMER_AKHIR ='$no_akhir' WHERE ID_JADWAL_PPMB ='$id_jadwal'");
    }

    function delete_jadwal_ppmb($id_jadwal)
    {
        $this->delete_plot_ppmb($id_jadwal);
        $this->database->Query("DELETE JADWAL_PPMB WHERE ID_JADWAL_PPMB ='$id_jadwal'");
    }

    function cek_no_peserta($kode_jalur, $gelombang)
    {
        $this->database->Query("SELECT * FROM (SELECT ID_JADWAL_PPMB,NOMER_AKHIR FROM JADWAL_PPMB WHERE KODE_JALUR ='$kode_jalur' AND GELOMBANG='$gelombang' AND NOMER_AKHIR IS NOT NULL ORDER BY ID_JADWAL_PPMB DESC) T WHERE ROWNUM <= 1");
        $temp = $this->database->FetchArray();
        return $temp;
    }

    function cek_no_peserta_pasca($gelombang)
    {
        $this->database->Query("SELECT * FROM (SELECT ID_JADWAL_PPMB,NOMER_AKHIR,NOMER_AWAL FROM JADWAL_PPMB WHERE GELOMBANG='$gelombang' ORDER BY ID_JADWAL_PPMB DESC) T WHERE ROWNUM <= 1");
        $temp = $this->database->FetchArray();
        return $temp;
    }

    function aktifkan($id_jadwal)
    {
        $data = $this->load_jadwal_ppmb_by_id($id_jadwal);
        $this->update_jadwal_ppmb_nomer($id_jadwal, $data['NOMER_AWAL'], $data['NOMER_AWAL'] + ($data['KAPASITAS'] - 1));
        $this->plot_jadwal_ppmb($id_jadwal);
        $this->database->Query("UPDATE JADWAL_PPMB SET STATUS_AKTIF ='1' WHERE ID_JADWAL_PPMB='$id_jadwal'");
    }

    function nonaktifkan($id_jadwal_ppmb)
    {
        $this->database->BeginTransaction();
        $this->database->Query("delete from plot_jadwal_ppmb where id_jadwal_ppmb = {$id_jadwal_ppmb}");
        $this->database->Query("update jadwal_ppmb set nomer_akhir = null, status_aktif = 0 where id_jadwal_ppmb = {$id_jadwal_ppmb}");
        $this->database->Commit();
    }

    function delete_plot_ppmb($id_jadwal)
    {
        $this->database->Query("DELETE FROM PLOT_JADWAL_PPMB WHERE ID_JADWAL_PPMB ='$id_jadwal'");
    }

    function plot_jadwal_ppmb($id_jadwal)
    {
        $temp = $this->load_jadwal_ppmb_by_id($id_jadwal);

        $format_kiri = substr($temp['NOMER_AWAL'], 0, 6);
        $no_awal = intval(substr($temp['NOMER_AWAL'], 6, 5));
        $no_akhir = intval(substr($temp['NOMER_AKHIR'], 6, 5));

        $this->database->BeginTransaction();

        for ($i = $no_awal; $i <= $no_akhir; $i++)
        {
            $no_ujian = $format_kiri . str_pad($i, 5, '0', STR_PAD_LEFT);
            $this->database->Query("
				INSERT INTO PLOT_JADWAL_PPMB (NO_UJIAN,KODE_JALUR,ID_JADWAL_PPMB,GELOMBANG)
				VALUES ('{$no_ujian}','{$temp['KODE_JALUR']}','{$temp['ID_JADWAL_PPMB']}','{$temp['GELOMBANG']}')");
        }

        $this->database->Commit();
    }

    function GetListFakultas()
    {
        return $this->database->QueryToArray("select * from fakultas order by id_fakultas");
    }
}

?>
