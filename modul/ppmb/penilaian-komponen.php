<?php
include('config.php');
include('class/Penerimaan.class.php');
include('class/Penilaian.class.php');

$penerimaan = new Penerimaan($db);
$penilaian  = new Penilaian($db);

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    if (post('mode') == 'add')
    {
        $is_tes_tulis = post('is_tes_tulis') == 'on' ? 1 : 0;
        
        $result = $penilaian->AddKomponenNilai(post('id_penerimaan_prodi'), post('nm_komponen'), post('singkatan'), post('bobot'), $is_tes_tulis, post('keterangan'));
        
        $smarty->assign('result', $result ? "Komponen berhasil ditambah" : "Komponen gagal ditambahkan");
    }
    
    if (post('mode') == 'edit')
    {
        $is_tes_tulis = post('is_tes_tulis') == 'on' ? 1 : 0;
        
        $result = $penilaian->UpdateKomponenNilai(post('id_komponen_nilai'), post('nm_komponen'), post('singkatan'), post('bobot'), $is_tes_tulis, post('keterangan'));
        
        $smarty->assign('result', $result ? "Komponen berhasil disimpan" : "Komponen gagal disimpan");
    }
    
    if (post('mode') == 'copy-to')
    {
        
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        $id_penerimaan = get('id_penerimaan', '');
        $id_penerimaan_prodi = get('id_penerimaan_prodi', '');
        
        $smarty->assign('penerimaan_set', $penerimaan->GetAllForView());
        
        if ($id_penerimaan != '')
        {
            $smarty->assign('program_studi_set', $penerimaan->GetListProdiPenerimaan($id_penerimaan));
        }
        
        if ($id_penerimaan_prodi != '')
        {
            $smarty->assign('komponen_nilai_set', $penilaian->GetListKomponenNilai($id_penerimaan_prodi));
        }
    }
    
    if ($mode == 'add')
    {
        $id_penerimaan_prodi = get('id_penerimaan_prodi');
        
        // Program Studi
        $smarty->assign('ps', $penerimaan->GetProdiPenerimaan($id_penerimaan_prodi));
    }
    
    if ($mode == 'edit')
    {
        $id_komponen_nilai = get('id_komponen_nilai');
        
        $kn = $penilaian->GetKomponenNilai($id_komponen_nilai);
        $pp = $penerimaan->GetProdiPenerimaan($kn['ID_PENERIMAAN_PRODI']);
        
        $smarty->assign('kn', $kn);
        $smarty->assign('ps', $pp);
    }
    
    if ($mode == 'copy-to')
    {
        $id_penerimaan_prodi = get('id_penerimaan_prodi');
        $ps = $penerimaan->GetProdiPenerimaan($id_penerimaan_prodi);
        
        $pp_set = $penerimaan->GetListProdiPenerimaan($ps['ID_PENERIMAAN']);
        
        $smarty->assign('ps', $ps);
        $smarty->assign('pp_set', $pp_set);
    }
    
    if ($mode == 'get-prodi')
    {
        $id_penerimaan = get('id_penerimaan');
        
        $penerimaan_prodi_set = $penerimaan->GetListProdiPenerimaan($id_penerimaan);
        
        echo "<option value=\"\">--</option>";
        foreach ($penerimaan_prodi_set as $pp)
            echo "<option value=\"{$pp['ID_PENERIMAAN_PRODI']}\">{$pp['NM_JENJANG']} {$pp['NM_PROGRAM_STUDI']}</option>";
        exit();
    }
    
    if ($mode == 'delete')
    {
        $id_komponen_nilai = get('id_komponen_nilai');
        
        $kn = $penilaian->GetKomponenNilai($id_komponen_nilai);
        $pp = $penerimaan->GetProdiPenerimaan($kn['ID_PENERIMAAN_PRODI']);
        
        $result = $penilaian->DeleteKomponenNilai($id_komponen_nilai);
        
        echo "<script>alert('".($result ? "Komponen berhasi dihapus" : "Komponen gagal dihapus" )."');window.location = '#penilaian-komp!penilaian-komponen.php?id_penerimaan={$pp['ID_PENERIMAAN']}&id_penerimaan_prodi={$pp['ID_PENERIMAAN_PRODI']}';</script>";
        
        exit();
    }
}

$smarty->display("penilaian/komponen/{$mode}.tpl");
?>
