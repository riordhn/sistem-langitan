<?php
include 'config.php';
include 'class/Penerimaan.class.php';
include 'class/Pengawas.class.php';

$Penerimaan = new Penerimaan($db);
$Pengawas   = new Pengawas($db);

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    if (post('mode') == 'set-pengawas')
    {
        $row = $Pengawas->SetPengawas($_POST);
        echo json_encode($row);
        exit();
    }
    
    if (post('mode') == 'unset-pengawas')
    {
        $result = $Pengawas->UnsetPengawas($_POST);
        echo $result ? "1" : "0";
        exit();
    }
    
    if (post('mode') == 'set-koordinator')
    {
        $result = $Pengawas->SetKoordinatorRuang($_POST);
        echo $result;
        exit();
    }
    
    if (post('mode') == 'unset-koordinator')
    {
        $result = $Pengawas->UnsetKoordinatorRuang($_POST);
        echo $result ? "1" : "0";
        exit();
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        $smarty->assign('ujian_set', $Pengawas->GetListUjian());
        $smarty->assign('jadwal_set', $Pengawas->GetListPlotPengawas(get('id_ujian')));
        $smarty->assign('pengawas', $Pengawas->GetTotalPengawas(get('id_ujian')));
    }
    
    if ($mode == 'set-koordinator')
    {
        $smarty->assign('asal_pengawas_set', $Pengawas->GetListAsalPengawas());
    }
    
    if ($mode == 'set-pengawas')
    {
        $smarty->assign('asal_pengawas_set', $Pengawas->GetListAsalPengawas());
    }
    
    if ($mode == 'get-pengawas-ready')
    {
        echo json_encode($Pengawas->GetListPengawasReady(get('id_jadwal_ppmb'), get('id_asal')));
        exit();
    }
    
    if ($mode == 'get-pengawas')
    {
        echo json_encode($Pengawas->GetListPengawasByAsal(get('id_asal')));
        exit();
    }
}

$smarty->display("pengawas/plot/{$mode}.tpl");
?>
