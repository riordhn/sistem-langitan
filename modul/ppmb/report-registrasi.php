<?php
require 'config.php';

// Maksimum eksekusi memory dinaikan
ini_set("memory_limit", "-1");

$mode = get('mode', 'view');
$id_penerimaan = get('id_penerimaan', '');

$id_pt = $id_pt_user;

if ($mode == 'view') {
    $penerimaan_set = $db->QueryToArray(
        "SELECT 
			p.id_penerimaan, p.tahun, p.id_jenjang, p.id_jalur, vnp.nama_penerimaan, p.gelombang,
			SUM(vcmb.is_submit_form) AS jumlah_submit_form,
			sum(vcmb.is_antri_verifikasi) AS jumlah_antri_verifikasi,
			sum(vcmb.IS_VERIFIKASI_KEMBALI) AS jumlah_verifikasi_kembali,
			sum(vcmb.is_verifikasi) AS jumlah_verifikasi,
			sum(vcmb.IS_BAYAR) AS jumlah_bayar,
			sum(vcmb.IS_AMBIL_KARTU) as jumlah_kartu
		FROM penerimaan p
		JOIN V_STATUS_CALON_MAHASISWA vcmb ON vcmb.ID_PENERIMAAN = p.ID_PENERIMAAN
		JOIN V_NAMA_PENERIMAAN vnp ON vnp.id_penerimaan = p.id_penerimaan
		WHERE p.is_aktif = 1 and p.id_perguruan_tinggi = '{$id_pt}'
		GROUP BY p.id_penerimaan, p.tahun, p.semester, p.gelombang, p.id_jenjang, p.id_jalur, vnp.nama_penerimaan
		ORDER BY p.tahun, p.semester, p.gelombang, p.id_jenjang, p.id_jalur, vnp.nama_penerimaan");

    $smarty->assign('penerimaan_set', $penerimaan_set);

    $penerimaan2_set = $db->QueryToArray(
        "SELECT 
			p.*,
			(SELECT count(v.kode_voucher) FROM voucher v 
			WHERE v.id_penerimaan = p.id_penerimaan AND v.tgl_bayar IS NOT NULL) AS jumlah_bayar
		FROM (
			SELECT
				p.id_penerimaan, p.tahun,
				vnp.nama_penerimaan,
				sum(is_submit_form) AS jumlah_submit_form,
				sum(is_verifikasi) as jumlah_verifikasi
			FROM penerimaan p
			JOIN V_STATUS_CALON_MAHASISWA_2 vcmb ON vcmb.ID_PENERIMAAN = p.ID_PENERIMAAN
			JOIN V_NAMA_PENERIMAAN vnp ON vnp.id_penerimaan = p.id_penerimaan
			WHERE p.is_aktif = 1 and p.id_perguruan_tinggi = '{$id_pt}'
			GROUP BY p.id_penerimaan, p.tahun, vnp.nama_penerimaan) p");
    $smarty->assign('penerimaan2_set', $penerimaan2_set);
}

if ($mode == 'prodi-isiform' && is_numeric($id_penerimaan)) {
    $db->Query(
        "SELECT p.id_jenjang, vnp.nama_penerimaan FROM v_nama_penerimaan vnp 
		JOIN penerimaan p ON p.id_penerimaan = vnp.id_penerimaan 
		WHERE vnp.id_penerimaan = {$id_penerimaan}");
    $row = $db->FetchAssoc();
    $smarty->assignByRef('penerimaan', $row);

    // Program Studi Set
    $penerimaan_set = $db->QueryToArray(
        "SELECT pp.id_program_studi, ps.id_fakultas, f.nm_fakultas, j.nm_jenjang||' '||ps.nm_program_studi AS nm_program_studi,
			nvl(sum(vcmb.is_submit_form),0) AS jumlah_submit_form,
			nvl(sum(vcmb.is_antri_verifikasi),0) AS jumlah_antri_verifikasi,
			nvl(sum(vcmb.IS_VERIFIKASI_KEMBALI),0) AS jumlah_verifikasi_kembali,
			nvl(sum(vcmb.is_verifikasi),0) AS jumlah_verifikasi,
			nvl(sum(vcmb.IS_BAYAR),0) AS jumlah_bayar
		FROM penerimaan_prodi pp
		JOIN program_studi ps ON ps.id_program_studi = pp.id_program_studi
		JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
		JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
		LEFT JOIN V_STATUS_CALON_MAHASISWA vcmb ON vcmb.id_penerimaan = pp.id_penerimaan and vcmb.id_pilihan_1 = pp.id_program_studi
		WHERE pp.id_penerimaan = '{$id_penerimaan}' AND pp.is_aktif = 1
		GROUP BY pp.id_program_studi, ps.id_fakultas, f.nm_fakultas, j.nm_jenjang||' '||ps.nm_program_studi
		order by 2, 4");

    // Prodi minat
    foreach ($penerimaan_set as &$pp) {
        $pp['prodi_minat_set'] = $db->QueryToArray(
            "SELECT pm.nm_prodi_minat,
				sum(vcmb.is_submit_form) AS jumlah_submit_form,
				sum(vcmb.is_antri_verifikasi) AS jumlah_antri_verifikasi,
				sum(vcmb.IS_VERIFIKASI_KEMBALI) AS jumlah_verifikasi_kembali,
				sum(vcmb.is_verifikasi) AS jumlah_verifikasi,
				sum(vcmb.IS_BAYAR) AS jumlah_bayar
			  FROM prodi_minat pm 
			  JOIN calon_mahasiswa_pasca cmp ON cmp.id_prodi_minat = pm.id_prodi_minat
			  JOIN calon_mahasiswa_baru cmb ON cmb.id_c_mhs = cmp.id_c_mhs AND 
				cmb.id_penerimaan = '{$id_penerimaan}' AND cmb.id_pilihan_1 = '{$pp['ID_PROGRAM_STUDI']}'
			  join V_STATUS_CALON_MAHASISWA vcmb on vcmb.id_c_mhs = cmb.id_c_mhs
			  GROUP BY pm.nm_prodi_minat");
    }

    $smarty->assign('penerimaan_set', $penerimaan_set);

    // Informasi pembelian voucher, isi form, verifikasi
    $voucher_set = $db->QueryToArray("
		select
			(select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
			where cmb.id_penerimaan = {$id_penerimaan} and cmb.tgl_registrasi is not null and 
			cmb.tgl_submit_verifikasi is not null and cmb.tgl_verifikasi_ppmb is null and
			cmb.kode_jurusan = '01') as IPA,
			(select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
			where cmb.id_penerimaan = {$id_penerimaan} and cmb.tgl_registrasi is not null and 
			cmb.tgl_submit_verifikasi is not null and cmb.tgl_verifikasi_ppmb is null and
			cmb.kode_jurusan = '02') as IPS,
			(select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
			where cmb.id_penerimaan = {$id_penerimaan} and cmb.tgl_registrasi is not null and 
			cmb.tgl_submit_verifikasi is not null and cmb.tgl_verifikasi_ppmb is null and
			cmb.kode_jurusan = '03') as IPC
		from dual
		union all
		select 
			(select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
			where cmb.id_penerimaan = {$id_penerimaan} and cmb.tgl_registrasi is not null and 
			cmb.tgl_verifikasi_ppmb is not null and cmb.kode_jurusan = '01') as IPA,
			(select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
			where cmb.id_penerimaan = {$id_penerimaan} and cmb.tgl_registrasi is not null and 
			cmb.tgl_verifikasi_ppmb is not null and cmb.kode_jurusan = '02') as IPS,
			(select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
			where cmb.id_penerimaan = {$id_penerimaan} and cmb.tgl_registrasi is not null and 
			cmb.tgl_verifikasi_ppmb is not null and cmb.kode_jurusan = '03') as IPC
		from dual
		union all
		select
			(select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
			join voucher v on v.kode_voucher = cmb.kode_voucher
			where cmb.id_penerimaan = {$id_penerimaan} and v.tgl_bayar is not null and
			cmb.kode_jurusan = '01') IPA,
			(select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
			join voucher v on v.kode_voucher = cmb.kode_voucher
			where cmb.id_penerimaan = {$id_penerimaan} and v.tgl_bayar is not null and
			cmb.kode_jurusan = '02') IPS,
			(select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
			join voucher v on v.kode_voucher = cmb.kode_voucher
			where cmb.id_penerimaan = {$id_penerimaan} and v.tgl_bayar is not null and
			cmb.kode_jurusan = '03') IPC
		from dual
		union all
		select 
			(select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
			where cmb.id_penerimaan = {$id_penerimaan} and cmb.tgl_ambil_no_ujian is not null and
			cmb.kode_jurusan = '01') as IPA,
			(select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
			where cmb.id_penerimaan = {$id_penerimaan} and cmb.tgl_ambil_no_ujian is not null and
			cmb.kode_jurusan = '02') as IPS,
			(select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb 
			where cmb.id_penerimaan = {$id_penerimaan} and cmb.tgl_ambil_no_ujian is not null and
			cmb.kode_jurusan = '03') as IPC
		from dual");
    $smarty->assign('voucher_set', $voucher_set);
}

if ($mode == 'detail-isiform' && is_numeric($id_penerimaan)) {
    $db->Query("select * from penerimaan where id_penerimaan = {$id_penerimaan}");
    $row = $db->FetchAssoc();
    $smarty->assign('penerimaan', $row);

    if ($row['SEMESTER'] != '-')
        $smarty->assign('nm_penerimaan', "{$row['NM_PENERIMAAN']} Gel. {$row['GELOMBANG']} Semester {$row['SEMESTER']} Tahun {$row['TAHUN']}");
    else
        $smarty->assign('nm_penerimaan', "{$row['NM_PENERIMAAN']} Gel. {$row['GELOMBANG']} Tahun {$row['TAHUN']}");

    $penerimaan_prodi_set = $db->QueryToArray("
		select pp.id_penerimaan, pp.id_program_studi, nm_program_studi from penerimaan_prodi pp
		join program_studi ps on ps.id_program_studi = pp.id_program_studi
		where pp.id_penerimaan = {$id_penerimaan} and pp.is_aktif = 1
		order by ps.id_fakultas");

    for ($i = 0; $i < count($penerimaan_prodi_set); $i++) {
        $penerimaan_prodi_set[$i]['cmb_set'] = $db->QueryToArray("
			select
				v.kode_voucher, cmb.kode_jurusan, no_ujian, nm_c_mhs, gelar, telp, nomor_hp, email, ptn_s1, ptn_s2, prodi_s1, prodi_s2, pm.nm_prodi_minat, p.id_jenjang,
				(select nm_sekolah from sekolah s where s.id_sekolah = cms.id_sekolah_asal) as nm_sekolah_asal
			from calon_mahasiswa_baru cmb
			join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
			join voucher v on v.kode_voucher = cmb.kode_voucher
			left join calon_mahasiswa_pasca cmp on cmp.id_c_mhs = cmb.id_c_mhs
			left join prodi_minat pm on pm.id_prodi_minat = cmp.id_prodi_minat
			left join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
			where cmb.id_penerimaan = {$penerimaan_prodi_set[$i]['ID_PENERIMAAN']} and cmb.id_pilihan_1 = {$penerimaan_prodi_set[$i]['ID_PROGRAM_STUDI']}
			order by no_ujian asc, kode_voucher asc");
    }

    $smarty->assign('penerimaan_prodi_set', $penerimaan_prodi_set);
}
if ($mode == 'detail-rekap' && is_numeric($id_penerimaan)) {
    $data_rekap_plot = array();
    $jadwal_ppmb = $db->QueryToArray("SELECT * FROM JADWAL_PPMB WHERE ID_PENERIMAAN ='{$id_penerimaan}' order by nomer_awal");
    foreach ($jadwal_ppmb as $data) {
        array_push($data_rekap_plot, array(
            'id_jadwal_ppmb' => $data['ID_JADWAL_PPMB'],
            'tgl_test' => $data['TGL_TEST'],
            'tgl_test2' => $data['TGL_TEST2'],
            'kapasitas' => $data['KAPASITAS'],
            'terisi' => $data['TERISI'],
            'ruang' => $data['NM_RUANG'],
            'alamat' => $data['ALAMAT'],
            'lokasi' => $data['LOKASI'],
            'data_plot' => $db->QueryToArray("
							SELECT PJP.NO_UJIAN,UPPER(NVL(CMB.NM_C_MHS,'')) AS NAMA,PS.NM_PROGRAM_STUDI,P.NM_PENGGUNA,CMB.SP3_1 FROM AUCC.PLOT_JADWAL_PPMB PJP
							LEFT JOIN AUCC.CALON_MAHASISWA_BARU CMB ON CMB.NO_UJIAN = PJP.NO_UJIAN
							LEFT JOIN PROGRAM_STUDI PS ON CMB.ID_PILIHAN_1 = PS.ID_PROGRAM_STUDI
							LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA = CMB.ID_VERIFIKATOR_PPMB
							WHERE PJP.ID_JADWAL_PPMB='{$data['ID_JADWAL_PPMB']}'
							ORDER BY PJP.NO_UJIAN")
        ));
    }
    $smarty->assign('data_rekap_plot', $data_rekap_plot);
}

if ($mode == 'download-peserta' && is_numeric($id_penerimaan)) {
    $rows = $db->QueryToArray("select * from penerimaan where id_penerimaan = {$id_penerimaan}");
    $penerimaan = $rows[0];

    $title = "Penerimaan {$penerimaan['NM_PENERIMAAN']} Gelombang {$penerimaan['GELOMBANG']} " .
        "Semester {$penerimaan['SEMESTER']} Tahun {$penerimaan['TAHUN']}";

    $rows = $db->QueryToArray(
        "select 
            cmb.kode_voucher, cmb.no_ujian, cmb.nm_c_mhs, cmb.gelar, cmb.tgl_lahir, kl.nm_kota as kota_lahir,
            case cmb.jenis_kelamin when 1 then 'L' when 2 then 'P' end as jenis_kelamin,
            a.nm_agama as agama, 
            cmb.alamat_jalan||' '||cmb.alamat_dusun ||' '||cmb.alamat_kelurahan||' '||cmb.alamat_kecamatan AS alamat, 
            k.nm_kota as kota_alamat, 
            cmb.telp, cmb.nomor_hp, cmb.email,
            kwn.nm_kewarganegaraan as kewarganegaraan,
            case cmb.status_perkawinan when 1 then 'Menikah' when 2 then 'Belum Menikah' end as status_perkawinan,
            sb.nm_sumber_biaya as sumber_biaya, 
            dsb.nm_disabilitas, cmb.nik_c_mhs, cmb.alamat_kodepos, cmb.nm_jenis_tinggal, cmb.nomor_kps,
            
            -- Pilihan Program Studi
            ps1.nm_program_studi as prodi1, sp3_1,
            ps2.nm_program_studi as prodi2, sp3_2,
            ps3.nm_program_studi as prodi3, sp3_3,
            ps4.nm_program_studi as prodi4, sp3_4,
            
            -- Kelompok Biaya
            kb.nm_kelompok_biaya,

            -- Sekolah
            sekolah.nm_sekolah, ksekolah.nm_kota as nm_kota_sekolah, psekolah.nm_provinsi as nm_provinsi_sekolah,
            jsc.nm_jurusan_sekolah, cms.tahun_lulus, cms.no_ijazah, cms.tgl_ijazah, cms.jumlah_pelajaran_ijazah,
            cms.nilai_ijazah, cms.jumlah_pelajaran_uan, cms.nilai_uan,	

            -- Ayah
            cmo.nama_ayah, cmo.tgl_lahir_ayah, cmo.alamat_ayah, kayah.nm_kota as kota_ayah, cmo.telp_ayah,
            cmo.pendidikan_ayah, kerjaayah.nm_pekerjaan as pekerjaan_ayah, cmo.penghasilan_ayah, 
            dsbayah.nm_disabilitas as disabilitas_ayah,

            -- Ibu
            cmo.nama_ibu, cmo.tgl_lahir_ibu, cmo.alamat_ibu, kibu.nm_kota as kota_ibu, cmo.telp_ibu,
            cmo.pendidikan_ibu, kerjaibu.nm_pekerjaan as pekerjaan_ibu, cmo.penghasilan_ibu, 
            dsbibu.nm_disabilitas as disabilitas_ibu,

            -- Wali
            cmo.nama_wali, cmo.tgl_lahir_wali, cmo.alamat_wali, kwali.nm_kota as kota_wali, cmo.telp_wali,
            cmo.pendidikan_wali, kerjawali.nm_pekerjaan as pekerjaan_wali, cmo.penghasilan_wali, 
            dsbwali.nm_disabilitas as disabilitas_wali,


            pekerjaan, asal_instansi, alamat_instansi, telp_instansi, nrp, karpeg, pangkat,
            ptn_s1, status_ptn_s1, prodi_s1, tgl_masuk_s1, tgL_lulus_s1, lama_studi_s1, ip_s1,
            ptn_s2, status_ptn_s2, prodi_s2, tgL_masuk_s2, tgl_lulus_s2, lama_studi_s2, ip_s2,
            jumlah_karya_ilmiah, tgl_diterima
        from calon_mahasiswa_baru cmb
        left join disabilitas dsb on dsb.id_disabilitas = cmb.id_disabilitas
        left join kota kl on kl.id_kota = cmb.id_kota_lahir
        left join agama a on a.id_agama = cmb.id_agama
        left join kota k on k.id_kota = cmb.id_kota
        left join kewarganegaraan kwn on kwn.id_kewarganegaraan = cmb.kewarganegaraan
        left join sumber_biaya sb on sb.id_sumber_biaya = cmb.sumber_biaya
        join program_studi ps1 on ps1.id_program_studi = cmb.id_pilihan_1
        left join program_studi ps2 on ps2.id_program_studi = cmb.id_pilihan_2
        left join program_studi ps3 on ps3.id_program_studi = cmb.id_pilihan_3
        left join program_studi ps4 on ps4.id_program_studi = cmb.id_pilihan_4
        join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
        left join sekolah on sekolah.id_sekolah = cms.id_sekolah_asal
        left join kota ksekolah on ksekolah.id_kota = sekolah.id_kota
        left join provinsi psekolah on psekolah.id_provinsi = ksekolah.id_provinsi
        left join jurusan_sekolah_cmhs jsc on jsc.id_jurusan_sekolah = cms.jurusan_sekolah
        join calon_mahasiswa_pasca cmp on cmp.id_c_mhs = cmb.id_c_mhs
        left join prodi_minat pm on pm.id_prodi_minat = cmp.id_prodi_minat
        left join kelompok_biaya kb on kb.id_kelompok_biaya = cmb.id_kelompok_biaya
        join calon_mahasiswa_ortu cmo on cmo.id_c_mhs = cmb.id_c_mhs
        left join kota kayah on kayah.id_kota = cmo.id_kota_ayah
        left join pekerjaan kerjaayah on kerjaayah.id_pekerjaan = cmo.pekerjaan_ayah
        left join disabilitas dsbayah on dsbayah.id_disabilitas = cmo.id_disabilitas_ayah
        left join kota kibu on kibu.id_kota = cmo.id_kota_ibu
        left join pekerjaan kerjaibu on kerjaibu.id_pekerjaan = cmo.pekerjaan_ibu
        left join disabilitas dsbibu on dsbibu.id_disabilitas = cmo.id_disabilitas_ibu
        left join kota kwali on kwali.id_kota = cmo.id_kota_wali
        left join pekerjaan kerjawali on kerjawali.id_pekerjaan = cmo.pekerjaan_wali
        left join disabilitas dsbwali on dsbwali.id_disabilitas = cmo.id_disabilitas_wali
        where id_penerimaan = {$id_penerimaan}
        order by ps1.nm_program_studi, cmb.no_ujian");

    // Create new PHPSpreadsheet
    $excel = new PhpOffice\PhpSpreadsheet\Spreadsheet();

    // Set properties
    $excel->getProperties()->setCreator("Langitan {$nama_pt}");
    $excel->getProperties()->setLastModifiedBy("Langitan {$nama_pt}");
    $excel->getProperties()->setTitle("Daftar Peserta");

    // create sheet
    $sheet = $excel->getActiveSheet();
    $sheet->setTitle('PESERTA');

    // insert header
    $sheet->setCellValue('A1', 'NO UJIAN');
    $sheet->setCellValue('B1', 'NOMOR PENDAFTARAN');
    $sheet->setCellValue('C1', 'NAMA');
    $sheet->setCellValue('D1', 'GELAR');
    $sheet->setCellValue('E1', 'JENIS KELAMIN');
    $sheet->setCellValue('F1', 'TEMPAT LAHIR');
    $sheet->setCellValue('G1', 'TGL LAHIR');
    $sheet->setCellValue('H1', 'AGAMA');
    $sheet->setCellValue('I1', 'SUMBER BIAYA');
    $sheet->setCellValue('J1', 'DISABILITAS');

    $sheet->setCellValue('K1', 'NIK');
    $sheet->setCellValue('L1', 'KEWARGANEGARAAN');
    $sheet->setCellValue('M1', 'ALAMAT');
    $sheet->setCellValue('N1', 'KODE POS');
    $sheet->setCellValue('O1', 'KOTA ALAMAT');
    $sheet->setCellValue('P1', 'JENIS TINGGAL');
    $sheet->setCellValue('Q1', 'TELP');
    $sheet->setCellValue('R1', 'NO HP');
    $sheet->setCellValue('S1', 'EMAIL');
    $sheet->setCellValue('T1', 'NOMOR KPS');

    $sheet->setCellValue('U1', 'NISN');
    $sheet->setCellValue('V1', 'KOTA SEKOLAH');
    $sheet->setCellValue('W1', 'ASAL SEKOLAH');
    $sheet->setCellValue('X1', 'JURUSAN SEKOLAH');
    $sheet->setCellValue('Y1', 'TAHUN LULUS');
    $sheet->setCellValue('Z1', 'NOMOR IJAZAH');
    $sheet->setCellValue('AA1', 'TGL IJAZAH');
    $sheet->setCellValue('AB1', 'JML PELAJARAN IJAZAH');
    $sheet->setCellValue('AC1', 'NILAI RATA-RATA IJAZAH');
    $sheet->setCellValue('AD1', 'JML PELAJARAN UAN');
    $sheet->setCellValue('AE1', 'NILAI RATA-RATA UAN');

    $sheet->setCellValue('AF1', 'NAMA AYAH');
    $sheet->setCellValue('AG1', 'TGL LAHIR AYAH');
    $sheet->setCellValue('AH1', 'ALAMAT AYAH');
    $sheet->setCellValue('AI1', 'ALAMAT KOTA AYAH');
    $sheet->setCellValue('AJ1', 'TELP AYAH');
    $sheet->setCellValue('AK1', 'PENDIDIKAN TERAKHIR AYAH');
    $sheet->setCellValue('AL1', 'PEKERJAAN AYAH');
    $sheet->setCellValue('AM1', 'PENGHASILAN AYAH');
    $sheet->setCellValue('AN1', 'DISABILITAS AYAH');

    $sheet->setCellValue('AO1', 'NAMA IBU');
    $sheet->setCellValue('AP1', 'TGL LAHIR IBU');
    $sheet->setCellValue('AQ1', 'ALAMAT IBU');
    $sheet->setCellValue('AR1', 'ALAMAT KOTA IBU');
    $sheet->setCellValue('AS1', 'TELP IBU');
    $sheet->setCellValue('AT1', 'PENDIDIKAN TERAKHIR IBU');
    $sheet->setCellValue('AU1', 'PEKERJAAN IBU');
    $sheet->setCellValue('AV1', 'PENGHASILAN IBU');
    $sheet->setCellValue('AW1', 'DISABILITAS IBU');

    $sheet->setCellValue('AX1', 'NAMA WALI');
    $sheet->setCellValue('AY1', 'TGL LAHIR WALI');
    $sheet->setCellValue('AZ1', 'ALAMAT WALI');
    $sheet->setCellValue('BA1', 'ALAMAT KOTA WALI');
    $sheet->setCellValue('BB1', 'TELP WALI');
    $sheet->setCellValue('BC1', 'PENDIDIKAN TERAKHIR WALI');
    $sheet->setCellValue('BD1', 'PEKERJAAN WALI');
    $sheet->setCellValue('BE1', 'PENGHASILAN WALI');
    $sheet->setCellValue('BF1', 'DISABILITAS WALI');

    $sheet->setCellValue('BG1', 'STATUS PERKAWINAN');
    $sheet->setCellValue('BH1', 'PRODI PILIHAN');

    if (in_array($penerimaan['ID_JENJANG'], array(1, 5, 4))) {
        $sheet->setCellValue('BI1', 'PRODI PILIHAN 2');
        $sheet->setCellValue('BJ1', 'PRODI PILIHAN 3');
        $sheet->setCellValue('BK1', 'PRODI PILIHAN 4');
        /*$sheet->setCellValue('U1', 'ASAL SEKOLAH');
        $sheet->setCellValue('V1', 'KOTA SEKOLAH');
        $sheet->setCellValue('W1', 'PROPINSI SEKOLAH');
        $sheet->setCellValue('X1', 'STATUS DITERIMA');*/
    } else if (in_array($penerimaan['ID_JENJANG'], array(2, 3, 9, 10))) {
        $sheet->setCellValue('BI1', 'PEKERJAAN');
        $sheet->setCellValue('BJ1', 'ASAL INSTANSI');
        $sheet->setCellValue('BK1', 'ALAMAT INSTANSI');
        $sheet->setCellValue('BL1', 'TELP INSTANSI');
        $sheet->setCellValue('BM1', 'NRP');
        $sheet->setCellValue('BN1', 'KARPEG');
        $sheet->setCellValue('BO1', 'PANGKAT');
        $sheet->setCellValue('BP1', 'UNIVERSITAS S1');
        $sheet->setCellValue('BQ1', 'PRODI S1');
        $sheet->setCellValue('BR1', 'TGL MASUK S1');
        $sheet->setCellValue('BS1', 'TGL LULUS S1');
        $sheet->setCellValue('BT1', 'LAMA STUDI S1');
        $sheet->setCellValue('BU1', 'IPK S1');
        $sheet->setCellValue('BV1', 'UNIVERSITAS S2/PR');
        $sheet->setCellValue('BW1', 'PRODI S2/PR');
        $sheet->setCellValue('BX1', 'TGL MASUK S2/PR');
        $sheet->setCellValue('BY1', 'TGL LULUS S2/PR');
        $sheet->setCellValue('BZ1', 'LAMA STUDI S2/PR');
        $sheet->setCellValue('CA1', 'IPK S2/PR');
        $sheet->setCellValue('CB1', 'KARYA ILMIAH');
        $sheet->setCellValue('CC1', 'STATUS DITERIMA');
        $sheet->setCellValue('CD1', 'STATUS PT S1');
        $sheet->setCellValue('CE1', 'STATUS PT S2/PR');
        $sheet->setCellValue('CF1', 'KELOMPOK BIAYA');
    }

    for ($i = 0; $i < count($rows); $i++) {
        $row = $rows[$i];

        $sheet->setCellValue('A' . ($i + 2), $row['NO_UJIAN']);
        $sheet->setCellValue('B' . ($i + 2), $row['KODE_VOUCHER']);
        $sheet->setCellValue('C' . ($i + 2), strtoupper($row['NM_C_MHS']));
        $sheet->setCellValue('D' . ($i + 2), $row['GELAR']);
        $sheet->setCellValue('E' . ($i + 2), $row['JENIS_KELAMIN']);
        $sheet->setCellValue('F' . ($i + 2), $row['KOTA_LAHIR']);
        $sheet->setCellValue('G' . ($i + 2), $row['TGL_LAHIR']);
        $sheet->setCellValue('H' . ($i + 2), $row['AGAMA']);
        $sheet->setCellValue('I' . ($i + 2), $row['SUMBER_BIAYA']);
        $sheet->setCellValue('J' . ($i + 2), $row['NM_DISABILITAS']);

        $sheet->setCellValue('K' . ($i + 2), $row['NIK_C_MHS']);
        $sheet->setCellValue('L' . ($i + 2), $row['KEWARGANEGARAAN']);
        $sheet->setCellValue('M' . ($i + 2), $row['ALAMAT']);
        $sheet->setCellValue('N' . ($i + 2), $row['ALAMAT_KODEPOS']);
        $sheet->setCellValue('O' . ($i + 2), $row['KOTA_ALAMAT']);
        $sheet->setCellValue('P' . ($i + 2), $row['NM_JENIS_TINGGAL']);
        $sheet->setCellValue('Q' . ($i + 2), $row['TELP']);
        $sheet->setCellValue('R' . ($i + 2), $row['NOMOR_HP']);
        $sheet->setCellValue('S' . ($i + 2), $row['EMAIL']);
        $sheet->setCellValue('T' . ($i + 2), $row['NOMOR_KPS']);

        $sheet->setCellValue('U' . ($i + 2), $row['NISN']);
        $sheet->setCellValue('V' . ($i + 2), $row['NM_KOTA_SEKOLAH']);
        $sheet->setCellValue('W' . ($i + 2), $row['NM_SEKOLAH']);
        $sheet->setCellValue('X' . ($i + 2), $row['NM_JURUSAN_SEKOLAH']);
        $sheet->setCellValue('Y' . ($i + 2), $row['TAHUN_LULUS']);
        $sheet->setCellValue('Z' . ($i + 2), $row['NO_IJAZAH']);
        $sheet->setCellValue('AA' . ($i + 2), $row['TGL_IJAZAH']);
        $sheet->setCellValue('AB' . ($i + 2), $row['JUMLAH_PELAJARAN_IJAZAH']);
        $sheet->setCellValue('AC' . ($i + 2), $row['NILAI_IJAZAH']);
        $sheet->setCellValue('AD' . ($i + 2), $row['JUMLAH_PELAJARAN_UAN']);
        $sheet->setCellValue('AE' . ($i + 2), $row['NILAI_UAN']);

        $sheet->setCellValue('AF' . ($i + 2), $row['NAMA_AYAH']);
        $sheet->setCellValue('AG' . ($i + 2), $row['TGL_LAHIR_AYAH']);
        $sheet->setCellValue('AH' . ($i + 2), $row['ALAMAT_AYAH']);
        $sheet->setCellValue('AI' . ($i + 2), $row['KOTA_AYAH']);
        $sheet->setCellValue('AJ' . ($i + 2), $row['TELP_AYAH']);
        $sheet->setCellValue('AK' . ($i + 2), $row['PENDIDIKAN_AYAH']);
        $sheet->setCellValue('AL' . ($i + 2), $row['PEKERJAAN_AYAH']);
        $sheet->setCellValue('AM' . ($i + 2), $row['PENGHASILAN_AYAH']);
        $sheet->setCellValue('AN' . ($i + 2), $row['DISABILITAS_AYAH']);

        $sheet->setCellValue('AO' . ($i + 2), $row['NAMA_IBU']);
        $sheet->setCellValue('AP' . ($i + 2), $row['TGL_LAHIR_IBU']);
        $sheet->setCellValue('AQ' . ($i + 2), $row['ALAMAT_IBU']);
        $sheet->setCellValue('AR' . ($i + 2), $row['KOTA_IBU']);
        $sheet->setCellValue('AS' . ($i + 2), $row['TELP_IBU']);
        $sheet->setCellValue('AT' . ($i + 2), $row['PENDIDIKAN_IBU']);
        $sheet->setCellValue('AU' . ($i + 2), $row['PEKERJAAN_IBU']);
        $sheet->setCellValue('AV' . ($i + 2), $row['PENGHASILAN_IBU']);
        $sheet->setCellValue('AW' . ($i + 2), $row['DISABILITAS_IBU']);

        $sheet->setCellValue('AX' . ($i + 2), $row['NAMA_WALI']);
        $sheet->setCellValue('AY' . ($i + 2), $row['TGL_LAHIR_WALI']);
        $sheet->setCellValue('AZ' . ($i + 2), $row['ALAMAT_WALI']);
        $sheet->setCellValue('BA' . ($i + 2), $row['KOTA_WALI']);
        $sheet->setCellValue('BB' . ($i + 2), $row['TELP_WALI']);
        $sheet->setCellValue('BC' . ($i + 2), $row['PENDIDIKAN_WALI']);
        $sheet->setCellValue('BD' . ($i + 2), $row['PEKERJAAN_WALI']);
        $sheet->setCellValue('BE' . ($i + 2), $row['PENGHASILAN_WALI']);
        $sheet->setCellValue('BF' . ($i + 2), $row['DISABILITAS_WALI']);


        $sheet->setCellValue('BG' . ($i + 2), $row['STATUS_PERKAWINAN']);
        $sheet->setCellValue('BH' . ($i + 2), $row['PRODI1']);

        if (in_array($penerimaan['ID_JENJANG'], array(1, 5, 4))) {
            $sheet->setCellValue('BI' . ($i + 2), $row['PRODI2']);
            $sheet->setCellValue('BJ' . ($i + 2), $row['PRODI3']);
            $sheet->setCellValue('BK' . ($i + 2), $row['PRODI4']);
            /*$sheet->setCellValue('U'.($i+2), $row['NM_SEKOLAH']);
            $sheet->setCellValue('V'.($i+2), $row['NM_KOTA_SEKOLAH']);
            $sheet->setCellValue('W'.($i+2), $row['NM_PROVINSI_SEKOLAH']);
            $sheet->setCellValue('X'.($i+2), $row['TGL_DITERIMA'] != '' ? "Diterima" : "-");*/
        } else if (in_array($penerimaan['ID_JENJANG'], array(2, 3, 9, 10))) {
            $sheet->setCellValue('BI' . ($i + 2), $row['PEKERJAAN']);
            $sheet->setCellValue('BJ' . ($i + 2), $row['ASAL_INSTANSI']);
            $sheet->setCellValue('BK' . ($i + 2), $row['ALAMAT_INSTANSI']);
            $sheet->setCellValue('BL' . ($i + 2), $row['TELP_INSTANSI']);
            $sheet->setCellValue('BM' . ($i + 2), $row['NRP']);
            $sheet->setCellValue('BN' . ($i + 2), $row['KARPEG']);
            $sheet->setCellValue('BO' . ($i + 2), $row['PANGKAT']);
            $sheet->setCellValue('BP' . ($i + 2), $row['PTN_S1']);
            $sheet->setCellValue('BQ' . ($i + 2), $row['PRODI_S1']);
            $sheet->setCellValue('BR' . ($i + 2), $row['TGL_MASUK_S1']);
            $sheet->setCellValue('BS' . ($i + 2), $row['TGL_LULUS_S1']);
            $sheet->setCellValue('BT' . ($i + 2), $row['LAMA_STUDI_S1']);
            $sheet->setCellValue('BU' . ($i + 2), $row['IP_S1']);
            $sheet->setCellValue('BV' . ($i + 2), $row['PTN_S2']);
            $sheet->setCellValue('BW' . ($i + 2), $row['PRODI_S2']);
            $sheet->setCellValue('BX' . ($i + 2), $row['TGL_MASUK_S2']);
            $sheet->setCellValue('BY' . ($i + 2), $row['TGL_LULUS_S2']);
            $sheet->setCellValue('BZ' . ($i + 2), $row['LAMA_STUDI_S2']);
            $sheet->setCellValue('CA' . ($i + 2), $row['IP_S2']);
            $sheet->setCellValue('CB' . ($i + 2), $row['JUMLAH_KARYA_ILMIAH']);
            $sheet->setCellValue('CC' . ($i + 2), $row['TGL_DITERIMA'] != '' ? "Diterima" : "-");
            $sheet->setCellValue('CD' . ($i + 2), $row['STATUS_PTN_S1'] == '1' ? 'Negeri' : 'Swasta');
            $sheet->setCellValue('CE' . ($i + 2), $row['STATUS_PTN_S2'] == '1' ? 'Negeri' : 'Swasta');
            $sheet->setCellValue('CF' . ($i + 2), $row['NM_KELOMPOK_BIAYA']);
        }

    }

    $sheet->getColumnDimension('A')->setAutoSize(true);
    $sheet->getColumnDimension('B')->setAutoSize(true);
    $sheet->getColumnDimension('C')->setAutoSize(true);
    $sheet->getColumnDimension('D')->setAutoSize(true);
    $sheet->getColumnDimension('E')->setAutoSize(true);
    $sheet->getColumnDimension('F')->setAutoSize(true);
    $sheet->getColumnDimension('G')->setAutoSize(true);
    $sheet->getColumnDimension('H')->setAutoSize(true);
    $sheet->getColumnDimension('I')->setAutoSize(true);
    $sheet->getColumnDimension('J')->setAutoSize(true);
    $sheet->getColumnDimension('K')->setAutoSize(true);
    $sheet->getColumnDimension('L')->setAutoSize(true);
    $sheet->getColumnDimension('M')->setAutoSize(true);
    $sheet->getColumnDimension('N')->setAutoSize(true);
    $sheet->getColumnDimension('O')->setAutoSize(true);
    $sheet->getColumnDimension('P')->setAutoSize(true);
    $sheet->getColumnDimension('Q')->setAutoSize(true);
    $sheet->getColumnDimension('R')->setAutoSize(true);
    $sheet->getColumnDimension('S')->setAutoSize(true);
    $sheet->getColumnDimension('T')->setAutoSize(true);
    $sheet->getColumnDimension('U')->setAutoSize(true);
    $sheet->getColumnDimension('V')->setAutoSize(true);
    $sheet->getColumnDimension('W')->setAutoSize(true);
    $sheet->getColumnDimension('X')->setAutoSize(true);
    $sheet->getColumnDimension('Y')->setAutoSize(true);
    $sheet->getColumnDimension('Z')->setAutoSize(true);
    $sheet->getColumnDimension('AA')->setAutoSize(true);
    $sheet->getColumnDimension('AB')->setAutoSize(true);
    $sheet->getColumnDimension('AC')->setAutoSize(true);
    $sheet->getColumnDimension('AD')->setAutoSize(true);
    $sheet->getColumnDimension('AE')->setAutoSize(true);
    $sheet->getColumnDimension('AF')->setAutoSize(true);
    $sheet->getColumnDimension('AG')->setAutoSize(true);
    $sheet->getColumnDimension('AH')->setAutoSize(true);
    $sheet->getColumnDimension('AI')->setAutoSize(true);
    $sheet->getColumnDimension('AJ')->setAutoSize(true);
    $sheet->getColumnDimension('AK')->setAutoSize(true);
    $sheet->getColumnDimension('AL')->setAutoSize(true);
    $sheet->getColumnDimension('AM')->setAutoSize(true);
    $sheet->getColumnDimension('AN')->setAutoSize(true);
    $sheet->getColumnDimension('AO')->setAutoSize(true);
    $sheet->getColumnDimension('AP')->setAutoSize(true);
    $sheet->getColumnDimension('AQ')->setAutoSize(true);
    $sheet->getColumnDimension('AR')->setAutoSize(true);
    $sheet->getColumnDimension('AS')->setAutoSize(true);
    $sheet->getColumnDimension('AT')->setAutoSize(true);
    $sheet->getColumnDimension('AU')->setAutoSize(true);
    $sheet->getColumnDimension('AV')->setAutoSize(true);
    $sheet->getColumnDimension('AW')->setAutoSize(true);
    $sheet->getColumnDimension('AX')->setAutoSize(true);
    $sheet->getColumnDimension('AY')->setAutoSize(true);
    $sheet->getColumnDimension('AZ')->setAutoSize(true);
    $sheet->getColumnDimension('BA')->setAutoSize(true);
    $sheet->getColumnDimension('BB')->setAutoSize(true);
    $sheet->getColumnDimension('BC')->setAutoSize(true);
    $sheet->getColumnDimension('BD')->setAutoSize(true);
    $sheet->getColumnDimension('BE')->setAutoSize(true);
    $sheet->getColumnDimension('BF')->setAutoSize(true);
    $sheet->getColumnDimension('BG')->setAutoSize(true);
    $sheet->getColumnDimension('BH')->setAutoSize(true);


    if (in_array($penerimaan['ID_JENJANG'], array(1, 5, 4))) {
        $sheet->getColumnDimension('BI')->setAutoSize(true);
        $sheet->getColumnDimension('BJ')->setAutoSize(true);
        $sheet->getColumnDimension('BK')->setAutoSize(true);
    } else if (in_array($penerimaan['ID_JENJANG'], array(2, 3, 9, 10))) {
        $sheet->getColumnDimension('BI')->setAutoSize(true);
        $sheet->getColumnDimension('BJ')->setAutoSize(true);
        $sheet->getColumnDimension('BK')->setAutoSize(true);
        $sheet->getColumnDimension('BL')->setAutoSize(true);
        $sheet->getColumnDimension('BM')->setAutoSize(true);
        $sheet->getColumnDimension('BN')->setAutoSize(true);
        $sheet->getColumnDimension('BO')->setAutoSize(true);
        $sheet->getColumnDimension('BP')->setAutoSize(true);
        $sheet->getColumnDimension('BQ')->setAutoSize(true);
        $sheet->getColumnDimension('BR')->setAutoSize(true);
        $sheet->getColumnDimension('BS')->setAutoSize(true);
        $sheet->getColumnDimension('BT')->setAutoSize(true);
        $sheet->getColumnDimension('BU')->setAutoSize(true);
        $sheet->getColumnDimension('BV')->setAutoSize(true);
        $sheet->getColumnDimension('BW')->setAutoSize(true);
        $sheet->getColumnDimension('BX')->setAutoSize(true);
        $sheet->getColumnDimension('BY')->setAutoSize(true);
        $sheet->getColumnDimension('BZ')->setAutoSize(true);
        $sheet->getColumnDimension('CA')->setAutoSize(true);
        $sheet->getColumnDimension('CB')->setAutoSize(true);
        $sheet->getColumnDimension('CC')->setAutoSize(true);
    }

    // header output
    header('Content-Type: application/vnd.ms-excel');
    header("Content-Disposition: attachment;filename=\"{$title}.xlsx\"");
    header('Cache-Control: max-age=0');

    $excelWriter = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($excel);
    $excelWriter->save('php://output');
    exit();
}

$smarty->display("report/registrasi/{$mode}.tpl");
