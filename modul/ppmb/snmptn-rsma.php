<?php
include("config.php");
include('class/snmptn.class.php');

$mode = get('mode', 'view');
$snmptn = new SnmptnClass($db);

if ($request_method == 'GET')
{
    $id_penerimaan = 44;
    
    if ($mode == 'view')
    {
        $smarty->assign('provinsi_set', $snmptn->GetRekapPesertaPerProvinsi($id_penerimaan));
    }
    
    if ($mode == 'detail-provinsi')
    {
        $id_provinsi = get('id_provinsi', '');
        $smarty->assign('provinsi', $snmptn->GetProvinsi($id_provinsi));
        
        $smarty->assign('kota_set', $snmptn->GetRekapPesertaPerKota($id_penerimaan, $id_provinsi));
    }
    
    if ($mode == 'detail-kota')
    {
        $id_kota = get('id_kota', '');
        
        $kota = $db->QueryToArray("
            select k.nm_kota, p.nm_provinsi, k.id_provinsi from kota k
            join provinsi p on p.id_provinsi = k.id_provinsi
            where k.id_kota = {$id_kota}");
        $smarty->assign('kota', $kota[0]);
        
        $smarty->assign('sekolah_set', $snmptn->GetRekapPesertaPerSekolah($id_penerimaan, $id_kota));
    }
    
    if ($mode == 'detail-peserta')
    {
        $id_sekolah = get('id_sekolah', '');
        
        $smarty->assign('sekolah', $snmptn->GetSekolah($id_sekolah));
        $smarty->assign('cmb_set', $snmptn->GetListPesertaBySekolah($id_penerimaan, $id_sekolah));
    }
    
    if ($mode == 'download-excel')
    {
        
        
        exit();
    }
}

$smarty->display("snmptn/rsma/{$mode}.tpl");
?>
