<?php
include('config.php');
include('class/snmptn.class.php');

$mode = get('mode', 'view');
$snmptn = new SnmptnClass($db);

if ($request_method == 'POST')
{
    if (post('mode') == 'upload1')
    {      
        $lines = file($_FILES['file_hasil']['tmp_name']);
        
        $db->BeginTransaction();
        
        foreach ($lines as $line)
        {
            $split = explode(",", $line);
            $id_c_mhs = $split[2];
            
            $db->Query("
                update nilai_cmhs_snmptn n set
                    n.id_pilihan_pindah = (select cmb.id_pilihan_1 from calon_mahasiswa_baru cmb where cmb.id_c_mhs = n.id_c_mhs)
                where n.putaran = 2 and n.id_c_mhs = {$id_c_mhs}");
        }
        
        $result = $db->Commit();
        
        echo $result ? 'Iterasi berhasil diupdate. ' : 'Iterasi gagal di update. ';
        echo '<a href="snmptn-iterasi.php?mode=upload">Kembali</a>';
        
        exit();
    }
    
    if (post('mode') == 'upload2')
    {      
        $lines = file($_FILES['file_hasil']['tmp_name']);
        
        $db->BeginTransaction();
        
        foreach ($lines as $line)
        {
            $split = explode(",", $line);
            $id_c_mhs = $split[0];
            
            $db->Query("
                update nilai_cmhs_snmptn n set
                    n.id_pilihan_pindah = (select cmb.id_pilihan_2 from calon_mahasiswa_baru cmb where cmb.id_c_mhs = n.id_c_mhs)
                where n.putaran = 2 and n.id_c_mhs = {$id_c_mhs}");
        }
        
        $result = $db->Commit();
        
        echo $result ? 'Iterasi berhasil diupdate. ' : 'Iterasi gagal di update. ';
        echo '<a href="snmptn-iterasi.php?mode=upload">Kembali</a>';
        
        exit();
    }
}

if ($request_method == 'GET')
{
    if ($mode == 'view')
    {
    }
    
    if ($mode == 'upload')
    {
    }
    
    if ($mode == 'download-all')
    {
        // Force download file
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment; filename=pesertaHitam.txt;");
		
		foreach ($snmptn->GetListKuotaOnly() as $row)
		{
			echo "{$row['ID_C_MHS']},{$row['NILAI_TOTAL']},{$row['ID_PILIHAN_1']},{$row['ID_PILIHAN_2']},{$row['ID_SEKOLAH_ASAL']}\r\n";
		}
		
		exit();
    }
    
    if ($mode == 'download-prodi')
	{
		// Force download file
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment; filename=KeketatanProdi.txt;");
		
		foreach ($snmptn->GetKeketatanProdi(44) as $row)
		{
			$peluang = number_format($row['PELUANG'],2);
			echo ",{$row['ID_PROGRAM_STUDI']},{$row['NM_PROGRAM_STUDI']},{$row['KUOTA']},{$row['PESERTA']},{$peluang}\r\n";
		}
		
		exit();
	}
	
	if ($mode == 'download-sma')
	{
		// Force download file
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment; filename=sekolahProsentase.txt;");
		
		foreach ($snmptn->GetKeketatanSekolah(44) as $row)
			echo "{$row['ID_SEKOLAH']},0,0,0,0,{$row['RASIO']}\r\n";
		
		exit();
	}
}

$smarty->display("snmptn/iterasi/{$mode}.tpl");
?>
