<?php
include("config.php");
include("class/snmptn.class.php");

$mode = get('mode', 'view');

if ($request_method == 'GET')
{
	$snmptn = new SnmptnClass($db);
	
	if ($mode == 'view')
	{
		// program studi pilihan
		$id_program_studi = get('id_program_studi', '');
		
		$smarty->assign('program_studi_set', $db->QueryToArray("
            select id_program_studi, nm_program_studi, nm_jenjang from program_studi ps
            join jenjang j on j.id_jenjang = ps.id_jenjang
            where kode_snmptn is not null order by nm_program_studi"));
		
		if ($id_program_studi != '')
		{
			$smarty->assign('cmb_set', $snmptn->GetListPesertaPutaran2($id_program_studi));
		}
	}
}

$smarty->display("snmptn/penetapan3/{$mode}.tpl");
?>
