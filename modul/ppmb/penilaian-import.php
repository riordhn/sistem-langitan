<?php
include('config.php');

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    if (post('mode') == 'import')
    {
        $stream_nilai = post('stream_nilai');
        
        $smarty->assign('stream_nilai', post('stream_nilai'));
        
        $nilai_set = explode("\n", $stream_nilai);
        for ($i = 0; $i < count($nilai_set); $i++)
        {
            $nilai_set[$i] = explode(";", str_replace(",", ".", $nilai_set[$i]));
            $db->Query("select id_c_mhs, nm_c_mhs from calon_mahasiswa_baru where no_ujian = '{$nilai_set[$i][0]}'");
            $row = $db->FetchAssoc();
            $nilai_set[$i]['NM_C_MHS'] = $row['NM_C_MHS'];
            $nilai_set[$i]['ID_C_MHS'] = $row['ID_C_MHS'];
            
            // Hitung nilai total
            $nilai_set[$i]['TOTAL_NILAI'] =
                str_replace(',', '.',
                $nilai_set[$i][1] + $nilai_set[$i][2] + $nilai_set[$i][3] + $nilai_set[$i][4] +
                $nilai_set[$i][5] + $nilai_set[$i][6] + $nilai_set[$i][7] + $nilai_set[$i][8]);
            
        }
        
        $smarty->assign('nilai_set', $nilai_set);
        
    }
    
    if (post('mode') == 'stream')
    {
        $db->BeginTransaction();
        
        for ($i = 0; $i < count($_POST['id_c_mhs']); $i++)
        {
            $id_c_mhs = $_POST['id_c_mhs'][$i];
            $nilai_tpa = $_POST['tpa' . $id_c_mhs];
            $nilai_inggris = $_POST['bing' . $id_c_mhs];
            $nilai_ilmu = $_POST['bil' . $id_c_mhs];
            $nilai_wawancara = $_POST['wcr' . $id_c_mhs];
            $nilai_ipk = $_POST['ipk' . $id_c_mhs];
            $nilai_karya_ilmiah = $_POST['ki' . $id_c_mhs];
            $nilai_rekomendasi = $_POST['rkm' . $id_c_mhs];
            $nilai_matrikulasi = $_POST['mtk'. $id_c_mhs];
            $total_nilai = $_POST['total' . $id_c_mhs];
            
            /**
             * NILAI_TPA
                NILAI_INGGRIS
                NILAI_ILMU
                NILAI_WAWANCARA
                NILAI_IPK
                NILAI_KARYA_ILMIAH
                NILAI_REKOMENDASI
                NILAI_MATRIKULASI
                TOTAL_NILAI
             */
           /* 
            echo "
                update nilai_cmhs_pasca set
                    nilai_tpa = {$nilai_tpa}, nilai_inggris = {$nilai_inggris}, nilai_ilmu = {$nilai_ilmu}, nilai_wawancara = {$nilai_wawancara}, nilai_ipk = {$nilai_ipk},
                    nilai_karya_ilmiah = {$nilai_karya_ilmiah}, nilai_rekomendasi = {$nilai_rekomendasi}, nilai_matrikulasi = {$nilai_matrikulasi}, total_nilai = {$total_nilai}
                where id_c_mhs = {$id_c_mhs}<br/>"; */
            
            // Cek Nilai_Cmhs_Pasca if not exist
            $db->Query("select id_c_mhs from nilai_cmhs_pasca where id_c_mhs = {$id_c_mhs}");
            $row = $db->FetchAssoc();
            
            if (!$row)
            {
                $db->Query("insert into nilai_cmhs_pasca (id_c_mhs) values ({$id_c_mhs})");
            }
                
            $result = $db->Query("
                update nilai_cmhs_pasca set
                    nilai_tpa = {$nilai_tpa}, nilai_inggris = {$nilai_inggris}, nilai_ilmu = {$nilai_ilmu}, nilai_wawancara = {$nilai_wawancara}, nilai_ipk = {$nilai_ipk},
                    nilai_karya_ilmiah = {$nilai_karya_ilmiah}, nilai_rekomendasi = {$nilai_rekomendasi}, nilai_matrikulasi = {$nilai_matrikulasi}, total_nilai = {$total_nilai}
                where id_c_mhs = {$id_c_mhs}");
            if (!$result)
            {
                echo "GAGAL";
                break;
            }
        }
        
        $db->Commit();
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
}

$smarty->display("penilaian/import/{$mode}.tpl");
?>
