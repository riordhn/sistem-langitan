<?php
include('config.php');

if (!in_array($user->ID_JABATAN_PEGAWAI, array(175, 176, 203)))
{
    // 203 = Admin
    echo "Anda tidak punya hak akses halaman ini.";
    exit();
}



if ($request_method == 'POST')
{
    // disable sejak 2014 D3 + Aj Gelombang 1
    exit();

    $id_c_mhs = post('id_c_mhs');
    
    $db->BeginTransaction();
    
    $db->Query("select id_jadwal_ppmb from calon_mahasiswa_data where id_c_mhs = {$id_c_mhs}");
    $row = $db->FetchAssoc();
    $id_jadwal_ppmb = $row['ID_JADWAL_PPMB'];
    
    // cmb
    $db->Query("
        update calon_mahasiswa_baru set
            no_ujian = null,
        
            SP3_1 = NULL, SP3_2 = NULL, SP3_3 = NULL, SP3_4 = NULL,
        
            id_kelompok_biaya = null,
            is_matrikulasi = null,
        
            tgl_verifikasi_ppmb = null,
            id_verifikator_ppmb = null
        
        where id_c_mhs = {$id_c_mhs}");
       
    // cmd
    $db->Query("
        update calon_mahasiswa_data set
            id_jadwal_ppmb = null,
            BERKAS_IJAZAH = 0,
            BERKAS_SKHUN = 0,
            BERKAS_KESANGGUPAN_SP3 = 0, 
            BERKAS_LAIN = 0,
            BERKAS_LAIN_KET = NULL
        where id_c_mhs = {$id_c_mhs}");
		
	// syarat prodi
	$db->Query("delete from calon_mahasiswa_syarat where id_c_mhs = {$id_c_mhs}");
        
    // plot_jadwal_ppmb
    $db->Query("UPDATE PLOT_JADWAL_PPMB SET ID_C_MHS = NULL WHERE ID_C_MHS = {$id_c_mhs}");
    
    // jadwal_ppmb
    $db->Query("UPDATE JADWAL_PPMB SET TERISI = TERISI - 1 WHERE ID_JADWAL_PPMB = {$id_jadwal_ppmb}");
    
    $db->Commit();
}

if ($request_method == 'GET' or $request_method == 'POST')
{   
    $db->Parse("
        select id_c_mhs, nm_c_mhs, no_ujian from calon_mahasiswa_baru
        where kode_voucher = :kode_voucher");
    $db->BindByName(':kode_voucher', get('kode_voucher', ''));
    $db->Execute();
    
    $smarty->assign('cmb', $db->FetchAssoc());
}

$smarty->display("verifikasi/rpeserta/view.tpl");
?>
