<?php
include 'config.php';
include 'class/Pengawas.class.php';

$mode = get('mode', 'view');

$Pengawas = new Pengawas($db);

if ($request_method == 'POST')
{
    if (post('mode') == 'add')
    {
        $result = $Pengawas->AddPengawas($_POST);
        $smarty->assign('result', $result ? "Pengawas berhasil ditambahkan" : "Pengawas gagal ditambahkan");
    }
    
    if (post('mode') == 'edit')
    {
        $result = $Pengawas->UpdatePengawas($_POST);
        $smarty->assign('result', $result ? "Pengawas berhasil di edit" : "Pengawas gagal di edit");
    }
    
    if (post('mode') == 'delete')
    {
        $result = $Pengawas->DeletePengawas(post('id_pengawas'));
        echo $result ? "1" : "0";
        exit();
    }
    
    if (post('mode') == 'upload-file')
    {
        $result = $Pengawas->UploadFilePengawas(post('token'), $_FILES['pengawas']);
        $smarty->assign('result', $result == false ? "Gagal upload file, silahkan refresh browser" : $result);
    }
    
    if (post('mode') == 'proses-file')
    {
        $result = $Pengawas->ProsesFilePengawas(post('token'), new PHPExcel_Reader_Excel5());
        $smarty->assign('result', $result);
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        $smarty->assign('pengawas_set', $Pengawas->GetListPengawas());
    }
    
    if ($mode == 'add')
    {
        $smarty->assign('asal_pengawas_set', $Pengawas->GetListAsalPengawas());
    }
    
    if ($mode == 'edit')
    {
        $smarty->assign('pengawas', $Pengawas->GetPengawas(get('id_pengawas')));
        $smarty->assign('asal_pengawas_set', $Pengawas->GetListAsalPengawas());
    }
    
    if ($mode == 'upload')
    {
        // create token
        $smarty->assign('token', time());
    }
}

$smarty->display("pengawas/master/{$mode}.tpl");
?>
