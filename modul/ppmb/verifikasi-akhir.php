<?php
include 'config.php';

if ($request_method == 'GET')
{
    $no_ujian = get('no_ujian');
    
    if ($no_ujian != '')
    {
        $rows = $db->QueryToArray("
            select
                cmb.no_ujian, nm_c_mhs, jp.lokasi, jp.materi, to_char(jp.tgl_test,'yyyy-mm-dd') as tgl_test,
                (select j.nm_jenjang||' '||ps.nm_program_studi from program_studi ps join jenjang j on j.id_jenjang = ps.id_jenjang where ps.id_program_studi = cmb.id_pilihan_1) as nm_pilihan_1,
                (select j.nm_jenjang||' '||ps.nm_program_studi from program_studi ps join jenjang j on j.id_jenjang = ps.id_jenjang where ps.id_program_studi = cmb.id_pilihan_2) as nm_pilihan_2,
                (select j.nm_jenjang||' '||ps.nm_program_studi from program_studi ps join jenjang j on j.id_jenjang = ps.id_jenjang where ps.id_program_studi = cmb.id_pilihan_3) as nm_pilihan_3,
                (select j.nm_jenjang||' '||ps.nm_program_studi from program_studi ps join jenjang j on j.id_jenjang = ps.id_jenjang where ps.id_program_studi = cmb.id_pilihan_4) as nm_pilihan_4,
                cmb.sp3_1, cmb.sp3_2, cmb.sp3_3, cmb.sp3_4
            from calon_mahasiswa_baru cmb
            join plot_jadwal_ppmb pjp on pjp.id_c_mhs = cmb.id_c_mhs
            join jadwal_ppmb jp on jp.id_jadwal_ppmb = pjp.id_jadwal_ppmb
            where cmb.no_ujian = '{$no_ujian}'");
        $smarty->assign('cmb', $rows[0]);
    }
}

$smarty->display("verifikasi/akhir/view.tpl");
?>
