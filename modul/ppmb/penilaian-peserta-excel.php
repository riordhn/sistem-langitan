<?php
include('config.php');
include('class/Penerimaan.class.php');

$Penerimaan = new Penerimaan($db);

$mode = get('mode');

if ($request_method == 'GET')
{
    if ($mode == 'template')
    {
        $id_penerimaan = get('id_penerimaan');
        
        // Mendapatkan penerimaan
        $penerimaan = $Penerimaan->GetPenerimaan($id_penerimaan);
        
        $title = strtoupper("{$penerimaan['NM_PENERIMAAN']} Gelombang {$penerimaan['GELOMBANG']} Semester {$penerimaan['SEMESTER']} Tahun {$penerimaan['TAHUN']}");
        
        // Mendapatkan prodi penerimaan
        $penerimaan_prodi_set = $db->QueryToArray("
            select rownum, substr(j.nm_jenjang,1,2)||' '||ps.nm_program_studi nm_program_studi, pp.id_program_studi from penerimaan_prodi pp
            join program_studi ps on ps.id_program_studi = pp.id_program_studi
            join jenjang j on j.id_jenjang = ps.id_jenjang
            where pp.id_penerimaan = {$id_penerimaan} and pp.is_aktif = 1");
            
        // Create new PHPExcel object
        $excel = new PHPExcel();

        // Set properties
        $excel->getProperties()->setCreator("Universitas Airlangga Cyber Campus");
        $excel->getProperties()->setLastModifiedBy("Universitas Airlangga Cyber Campus");
        $excel->getProperties()->setTitle("Template Penilaian");
        $excel->getProperties()->setSubject("{$title}");
        $excel->getProperties()->setDescription("Template penilaian untuk penerimaan {$title}.");
        $excel->getProperties()->setKeywords("penilaian ppmb cybercampus universitas airlangga");
        
        // create sheet
        $sheet = $excel->getActiveSheet();
        $sheet->setTitle('PENILAIAN');

        // insert header
        $sheet->setCellValue('A1', 'No Ujian');
        $sheet->setCellValue('B1', 'Nama Peserta');
        $sheet->setCellValue('C1', 'Ilmu');
        $sheet->setCellValue('D1', 'WWC');
        $sheet->setCellValue('E1', 'IPK');
        $sheet->setCellValue('F1', 'KI');
        $sheet->setCellValue('G1', 'RKM');
        $sheet->setCellValue('H1', 'MTRK');
        $sheet->setCellValue('I1', 'PSI');
        $sheet->setCellValue('J1', 'TULIS');
        $sheet->setCellValue('K1', 'LAIN');

        // set list no ujian
        $cmb_set = $db->QueryToArray("
            select no_ujian, nm_c_mhs, n.* from calon_mahasiswa_baru cmb
            left join nilai_cmhs_pasca n on n.id_c_mhs = cmb.id_c_mhs
            where id_penerimaan = {$id_penerimaan} and tgl_verifikasi_ppmb is not null order by no_ujian");

        // set list no ujian + nama
        for ($i = 0; $i < count($cmb_set); $i++)
        {
            $cmb = &$cmb_set[$i];
            $sheet->setCellValue('A'.($i+2), $cmb['NO_UJIAN']);
            $sheet->setCellValue('B'.($i+2), $cmb['NM_C_MHS']);
            $sheet->setCellValue('C'.($i+2), ($_GET['nilai'] != 'true') ? 0 : $cmb['NILAI_ILMU']);
            $sheet->setCellValue('D'.($i+2), ($_GET['nilai'] != 'true') ? 0 : $cmb['NILAI_WAWANCARA']);
            $sheet->setCellValue('E'.($i+2), ($_GET['nilai'] != 'true') ? 0 : $cmb['NILAI_IPK']);
            $sheet->setCellValue('F'.($i+2), ($_GET['nilai'] != 'true') ? 0 : $cmb['NILAI_KARYA_ILMIAH']);
            $sheet->setCellValue('G'.($i+2), ($_GET['nilai'] != 'true') ? 0 : $cmb['NILAI_REKOMENDASI']);
            $sheet->setCellValue('H'.($i+2), ($_GET['nilai'] != 'true') ? 0 : $cmb['NILAI_MATRIKULASI']);
            $sheet->setCellValue('I'.($i+2), ($_GET['nilai'] != 'true') ? 0 : $cmb['NILAI_PSIKO']);
            $sheet->setCellValue('J'.($i+2), ($_GET['nilai'] != 'true') ? 0 : $cmb['NILAI_TULIS']);
            $sheet->setCellValue('K'.($i+2), ($_GET['nilai'] != 'true') ? 0 : $cmb['NILAI_LAIN']);
        }

        // AUTO WIDTH
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        //$sheet->getColumnDimension('D')->setAutoSize(true);
        //$sheet->getColumnDimension('E')->setAutoSize(true);
        //$sheet->getColumnDimension('F')->setAutoSize(true);
        //$sheet->getColumnDimension('G')->setAutoSize(true);
        //$sheet->getColumnDimension('H')->setAutoSize(true);
        
        // delete first sheet
        //$excel->removeSheetByIndex(0);
        
        // Rename sheet
        //$excel->getActiveSheet()->setTitle('Simple');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        //$excel->setActiveSheetIndex(0);

        // header output
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=\"{$title}.xls\"");
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
        $objWriter->save('php://output');
        
        echo "selesai"; exit();
    }
}
?>
