<?php
include 'config.php';

$mode = get('mode', 'view');

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	if (post('mode') == 'add')
	{
		$db->Parse("INSERT INTO KOTA (ID_PROVINSI, KODE_SNMPTN, TIPE_DATI2, NM_KOTA) VALUES (:1, :2, :3, :4)");
		$db->BindByName(':1', post('id_provinsi'));
		$db->BindByName(':2', post('kode_snmptn'));
		$db->BindByName(':3', post('tipe_dati2'));
		$db->BindByName(':4', post('nm_kota'));
		
		$result = $db->Execute();
		
		if ($result)
			$smarty->assign('message', 'Data berhasil ditambahkan');
		else
			$smarty->assign('message', 'Data gagal ditambahkan');
	}
	
	if (post('mode') == 'edit')
	{
		$db->Parse("UPDATE KOTA SET KODE_SNMPTN = :1, TIPE_DATI2 = :2, NM_KOTA = :3 WHERE ID_KOTA = :id_kota");
		$db->BindByName(':1', post('kode_snmptn'));
		$db->BindByName(':2', post('tipe_dati2'));
		$db->BindByName(':3', post('nm_kota'));
		$db->BindByName(':id_kota', post('id_kota'));
		
		$result = $db->Execute();
		
		if ($result)
			$smarty->assign('message', 'Data berhasil disimpan');
		else
			$smarty->assign('message', 'Data gagal disimpan');
	}
}

if ($_SERVER['REQUEST_METHOD'] == 'GET' || $_SERVER['REQUEST_METHOD'] == 'POST')
{
	if ($mode == 'view')
	{
		$id_negara		= get('id_negara', '');
		$id_provinsi	= get('id_provinsi', '');

		$negara_set = $db->QueryToArray("SELECT * FROM NEGARA ORDER BY NM_NEGARA");
		$smarty->assignByRef('negara_set', $negara_set);

		if ($id_negara != '')
		{
			$provinsi_set = $db->QueryToArray("SELECT * FROM PROVINSI WHERE ID_NEGARA = {$id_negara} ORDER BY NM_PROVINSI");
			$smarty->assignByRef('provinsi_set', $provinsi_set);
		} 

		if ($id_negara != '' && $id_provinsi != '')
		{
			$kota_set = $db->QueryToArray("SELECT * FROM KOTA WHERE ID_PROVINSI = {$id_provinsi} ORDER BY TIPE_DATI2, NM_KOTA");
			$smarty->assignByRef('kota_set', $kota_set);
		}
	}
	
	if ($mode == 'add')
	{
		$id_negara = get('id_negara');
		$id_provinsi = get('id_provinsi');
		
		$db->Query("SELECT * FROM NEGARA WHERE ID_NEGARA = {$id_negara}");
		$smarty->assignByRef('negara', $db->FetchAssoc());
		
		$db->Query("SELECT * FROM PROVINSI WHERE ID_PROVINSI = {$id_provinsi}");
		$smarty->assignByRef('provinsi', $db->FetchAssoc());
	}
	
	if ($mode == 'edit')
	{
		$id_negara = get('id_negara');
		$id_provinsi = get('id_provinsi');
		$id_kota = get('id_kota');
		
		$db->Query("SELECT * FROM NEGARA WHERE ID_NEGARA = {$id_negara}");
		$smarty->assignByRef('negara', $db->FetchAssoc());
		
		$db->Query("SELECT * FROM PROVINSI WHERE ID_PROVINSI = {$id_provinsi}");
		$smarty->assignByRef('provinsi', $db->FetchAssoc());
		
		$db->Query("SELECT * FROM KOTA WHERE ID_KOTA = {$id_kota}");
		$smarty->assignByRef('kota', $db->FetchAssoc());
	}

	if ($mode == 'aktifkan')
	{
		$id_kota = get('id_kota', '');
		$db->Query("update kota set is_aktif = 1 where id_kota = {$id_kota}");
		echo "1";
		exit();
	}

	if ($mode == 'non-aktifkan')
	{
		$id_kota = get('id_kota', '');
		$db->Query("update kota set is_aktif = 0 where id_kota = {$id_kota}");
		echo "1";
		exit();
	}
}

// sementara khusus umaha, master kota tidak boleh di-edit
if($id_pt_user == 1){
	$smarty->display("penerimaan/kota/{$mode}.tpl");
}
else{
	exit();
}