<?php
include('config.php');

$kode_voucher = get('kode_voucher');

if ($request_method == 'POST')
{
    $sp3_1 = post('sp3_1');
    $id_c_mhs = post('id_c_mhs');
    $result = $db->Query("update calon_mahasiswa_baru set sp3_1 = '{$sp3_1}' where id_c_mhs = {$id_c_mhs}");
    
    $smarty->assign('result', $result ? "Update sp3 berhasil" : "Gagal update sp3");
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    $cmb_set = $db->QueryToArray("select id_c_mhs, nm_c_mhs, sp3_1 from calon_mahasiswa_baru where kode_voucher = '{$kode_voucher}'");
    $smarty->assign('cmb', $cmb_set[0]);
}

$smarty->display('verifikasi/sp3/view.tpl');
?>
