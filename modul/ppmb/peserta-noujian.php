<?php
include('config.php');

if (!in_array($user->ID_JABATAN_PEGAWAI, array(175, 176, 203)))
{
	// 203	= Admin PPMB
	// 202	= Verifikator PPMB
	// 		= Ketua PPMB
	
	echo "<div class=\"center_title_bar\">Ambil No Ujian Peserta</div>";
	echo "<h2>Mohon maaf, anda tidak mempunyai akses kesini</h2>";
	exit();
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	$kode_voucher = get('kode_voucher', '');
	
	$db->Parse("
		select cmb.id_c_mhs, kode_voucher, nm_c_mhs, no_ujian, id_calon_pendaftar
		from calon_mahasiswa_baru cmb
		where kode_voucher = :kode_voucher");
	$db->BindByName(':kode_voucher', $kode_voucher);
	$db->Execute();
	
	$cmb = $db->FetchAssoc();
	$smarty->assign('cmb', $cmb);
}

$smarty->display("peserta/noujian/view.tpl");
?>