<?php
include('config.php');
include('class/snmptn.class.php');

$mode = get('mode', 'view');

if ($request_method == 'GET')
{
	$snmptn = new SnmptnClass($db);
	
    if ($mode == 'view')
    {
		$smarty->assign('program_studi_set', $snmptn->GetRekapProdi(44));
    }
}

$smarty->display("snmptn/rps/{$mode}.tpl");
?>
