<div class="center_title_bar">Impor Nilai Ujian</div>
{literal}<style>
.content form table.stream tr td { padding: 1px; font-size: small; }
</style>{/literal}
<form action="penilaian-import.php" method="post">
    {if $stream_nilai}
        <a href="penilaian-import.php?mode=view">Batal</a>
        <input type="submit" value="Simpan" />
        <input type="hidden" name="mode" value="stream" /><br/>
        <table class="stream">
            <tr>
                <th>No Ujian</th>
                <th>Nama</th>
                <th>TPA</th>
                <th>B.Ing</th>
                <th>B.Ilmu</th>
                <th>Wcr</th>
                <th>IPK</th>
                <th>KI</th>
                <th>RKM</th>
                <th>MTK</th>
                <th>Total</th>
            </tr>
            {foreach $nilai_set as $n}
            <tr>
                <td>{$n[0]}</td>
                <td>{$n.NM_C_MHS}<input type="hidden" name="id_c_mhs[]" value="{$n.ID_C_MHS}" /></td>
                <td>
                    <input type="text" size="3" maxlength="5" name="tpa{$n.ID_C_MHS}" value="{$n[1]}" readonly="readonly"/>
                </td>
                <td>
                    <input type="text" size="3" maxlength="5" name="bing{$n.ID_C_MHS}" value="{$n[2]}" readonly="readonly"/>
                </td>
                <td>
                    <input type="text" size="3" maxlength="5" name="bil{$n.ID_C_MHS}" value="{$n[3]}" readonly="readonly"/>
                </td>
                <td>
                    <input type="text" size="3" maxlength="5" name="wcr{$n.ID_C_MHS}" value="{$n[4]}" readonly="readonly"/>
                </td>
                <td>
                    <input type="text" size="3" maxlength="5" name="ipk{$n.ID_C_MHS}" value="{$n[5]}" readonly="readonly"/>
                </td>
                <td>
                    <input type="text" size="3" maxlength="5" name="ki{$n.ID_C_MHS}" value="{$n[6]}" readonly="readonly"/>
                </td>
                <td>
                    <input type="text" size="3" maxlength="5" name="rkm{$n.ID_C_MHS}" value="{$n[7]}" readonly="readonly"/>
                </td>
                <td>
                    <input type="text" size="3" maxlength="5" name="mtk{$n.ID_C_MHS}" value="{$n[8]}" readonly="readonly"/>
                </td>
                <td>
                    <input type="text" size="3" maxlength="5" name="total{$n.ID_C_MHS}" value="{$n.TOTAL_NILAI}" readonly="readonly" />
                </td>
            </tr>
            {/foreach}
        </table>
    {else}
        <input type="submit" value="Upload" />
        <label>Input Data :</label><br/>
        <input type="hidden" name="mode" value="import" /><br/>
        <textarea name="stream_nilai" cols="100" rows="30">{$stream_nilai}</textarea>
    {/if}
</form>
<br/> <br/>
<label>Format data impor adalah sebagai berikut :</label><br/>
<textarea cols="100" rows="3" readonly="readonly">[no_ujian];[tpa];[inggris];[ilmu];[wawancara];[ipk];[ki];[rekomendai];[matrikulasi]
[no_ujian2];[tpa];[inggris];[ilmu];[wawancara];[ipk];[ki];[rekomendai];[matrikulasi]
</textarea>
<p>Tanda koma angka [,] di ganti dengan titik [.]</p>