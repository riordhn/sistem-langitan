<div class="center_title_bar">Master Kunci Soal</div>

<form action="penilaian-kunci.php" method="get" id="filter">
<table>
    <tr>
        <td>Penerimaan</td>
        <td>
            <select name="id_penerimaan" onchange="$('#filter').submit(); return false;">
                <option value="">-- Pilih Penerimaan --</option>
            {foreach $penerimaan_set as $p}
                <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                {foreach $p.p_set as $p2}
                <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN}</option>
                {/foreach}
                </optgroup>
            {/foreach}
            </select>
        </td>
    </tr>
</table>
</form>

<script>
    function DeleteKunci(id_kunci)
    {
        if (confirm('Apakah kunci ini yakin akan dihapus ?') == true)
        {
            $.ajax({
                url: 'penilaian-kunci.php',
                type: 'POST',
                data: 'mode=delete&id_penerimaan_kunci_soal='+id_kunci,
                success: function(data) {
                    if (data == 1)
                        $('#r'+id_kunci).remove();
                    else
                        alert('Kunci gagal dihapus');
                }
            });
        }
    }
</script>

{if $result}<h2>{$result}</h2>{/if}
        
{if $smarty.get.id_penerimaan != ''}
<table>
    <tr>
        <th>No</th>
        <th>Kode</th>
        <th>Kunci</th>
        <th>Aksi</th>
    </tr>
    {foreach $kunci_set as $k}
    <tr id="r{$k.ID_PENERIMAAN_KUNCI_SOAL}">
        <td>{$k@index + 1}</td>
        <td>{$k.KODE_SOAL}</td>
        <td><textarea readonly="readonly" cols="50" rows="3">{$k.DATA_KUNCI}</textarea></td>
        <td>
            <span class="anchor-span" onclick="DeleteKunci({$k.ID_PENERIMAAN_KUNCI_SOAL}); return false;">Hapus</span>
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="4">
            <a href="penilaian-kunci.php?mode=add&id_penerimaan={$smarty.get.id_penerimaan}">Tambah Kunci</a>
        </td>
    </tr>
</table>
{/if}