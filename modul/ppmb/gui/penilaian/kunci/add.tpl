<div class="center_title_bar">Master Kunci Soal - Tambah</div>

<form action="penilaian-kunci.php?id_penerimaan={$smarty.get.id_penerimaan}" method="post">
<input type="hidden" name="mode" value="add" />
<input type="hidden" name="id_penerimaan" value="{$smarty.get.id_penerimaan}" />
<table>
    <tr>
        <th colspan="2">Master Kunci</th>
    </tr>
    <tr>
        <td>Kode Soal</td>
        <td><input type="text" name="kode_soal" /></td>
    </tr
    <tr>
        <td>Data Kunci</td>
        <td>
            <textarea name="data_kunci" cols="50" rows="5"></textarea>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <a href="penilaian-kunci.php?id_penerimaan={$smarty.get.id_penerimaan}">Kembali</a>
            <input type="submit" value="Simpan"/>
        </td>
    </tr>
</table>
</form>

<script type="text/javascript">
    $('form').validate({
        rules: {
            kode_kunci: 'required',
            data_kunci: 'required'
        }
    });
</script>