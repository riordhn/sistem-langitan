<div class="center_title_bar">Komponen Penilaian - Copy To</div>

<h2>{$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI}</h2>

<a href="penilaian-komponen.php?id_penerimaan={$ps.ID_PENERIMAAN}&id_penerimaan_prodi={$ps.ID_PENERIMAAN_PRODI}">Kembali</a>

<form action="penilaian-komponen.php?id_penerimaan={$ps.ID_PENERIMAAN}&id_penerimaan_prodi={$ps.ID_PENERIMAAN_PRODI}" method="post">
    <input type="hidden" name="mode" value="copy-to" />
    <input type="hidden" name="id_penerimaan_prodi" value="{$ps.ID_PENERIMAAN_PRODI}" />
    <table>
        <tr>
            <th>No</th>
            <th>Nama Program Studi</th>
            <th>Aksi</th>
        </tr>
        {$no = 1}
        {foreach $pp_set as $pp}
            {if $pp.ID_PENERIMAAN_PRODI != $ps.ID_PENERIMAAN_PRODI}
            <tr>
                <td class="center">{$no}</td>
                <td>{$pp.NM_JENJANG} {$pp.NM_PROGRAM_STUDI}</td>
                <td class="center"><input type="checkbox" name="id_penerimaan_prodi_set[]" value="{$pp.ID_PENERIMAAN_PRODI}" /></td>
            </tr>
            {$no = $no+1}
            {/if}
        {/foreach}
        <tr>
            <td colspan="3">
                <a href="penilaian-komponen.php?id_penerimaan={$ps.ID_PENERIMAAN}&id_penerimaan_prodi={$ps.ID_PENERIMAAN_PRODI}">Kembali</a>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>