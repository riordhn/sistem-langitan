<div class="center_title_bar">Komponen Penilaian</div>

<script type="text/javascript">
    $('select[name="id_penerimaan"]').change(function() {
        $.ajax({
            url: 'penilaian-komponen.php',
            type: 'GET',
            data: 'mode=get-prodi&id_penerimaan='+this.value,
            success: function(data) {
                $('select[name="id_penerimaan_prodi"]').html(data);
            }
        });
    });
</script>

{if $result}<script>alert('{$result}');</script>{/if}

<form action="penilaian-komponen.php" method="get" id="filter">
<table>
    <tr>
        <td>Penerimaan</td>
        <td>
            <select name="id_penerimaan">
                <option value="">-- Pilih Penerimaan --</option>
            {foreach $penerimaan_set as $p}
                <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                {foreach $p.p_set as $p2}
                <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN}</option>
                {/foreach}
                </optgroup>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Program Studi</td>
        <td>
            <select name="id_penerimaan_prodi" onchange="$('#filter').submit()">
            {if !empty($program_studi_set)}
            {foreach $program_studi_set as $ps}
                <option value="{$ps.ID_PENERIMAAN_PRODI}" {if $ps.ID_PENERIMAAN_PRODI == $smarty.get.id_penerimaan_prodi}selected="selected"{/if}>{$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI}</option>
            {/foreach}
            {/if}
            </select>
        </td>
    </tr>
</table>
</form>

{if $smarty.get.id_penerimaan_prodi != ''}
<table>
    <tr>
        <th>No</th>
        <th>Komponen</th>
        <th>Singkatan</th>
        <th>Bobot</th>
        <th>Ujian Tulis</th>
        <th>Keterangan</th>
        <th>Aksi</th>
    </tr>
    {$bobot = 0}
    {foreach $komponen_nilai_set as $kn}
    <tr>
        <td class="center">{$kn@index + 1}</td>
        <td>{$kn.NM_KOMPONEN}</td>
        <td class="center">{$kn.SINGKATAN}</td>
        <td class="center">{$kn.BOBOT}%</td>
        <td class="center">{if $kn.IS_TES_TULIS}Ya{else}-{/if}</td>
        <td>{$kn.KETERANGAN}</td>
        <td class="center">
            <a href="penilaian-komponen.php?mode=edit&id_komponen_nilai={$kn.ID_PENERIMAAN_KOMPONEN_NILAI}">Edit</a> |
            <a href="penilaian-komponen.php?mode=delete&id_komponen_nilai={$kn.ID_PENERIMAAN_KOMPONEN_NILAI}">Hapus</a>
        </td>
    </tr>
    {$bobot = $bobot + $kn.BOBOT}
    {/foreach}
    <tr>
        <td></td>
        <td colspan="2">Total</td>
        <td class="center">{$bobot}%</td>
        <td colspan="3"></td>
    </tr>
    <tr>
        <td colspan="8" class="center">
            {if $bobot < 100}
            <a href="penilaian-komponen.php?mode=add&id_penerimaan_prodi={$smarty.get.id_penerimaan_prodi}">Tambah</a> |
            {/if}
            <a href="penilaian-komponen.php?mode=copy-to&id_penerimaan_prodi={$smarty.get.id_penerimaan_prodi}">Copy To</a>
        </td>
    </tr>
</table>
{/if}