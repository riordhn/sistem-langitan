<div class="center_title_bar">Komponen Penilaian - Edit</div>

<h2>{$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI}</h2>

<form action="penilaian-komponen.php?id_penerimaan={$ps.ID_PENERIMAAN}&id_penerimaan_prodi={$ps.ID_PENERIMAAN_PRODI}" method="post">
<input type="hidden" name="mode" value="edit"/>
<input type="hidden" name="id_penerimaan_prodi" value="{$ps.ID_PENERIMAAN_PRODI}" />
<input type="hidden" name="id_komponen_nilai" value="{$kn.ID_PENERIMAAN_KOMPONEN_NILAI}" />
<table>
    <tr>
        <th colspan="2">Detail</th>
    </tr>
    <tr>
        <td>Komponen</td>
        <td><input type="text" name="nm_komponen" maxlength="30" size="20" value="{$kn.NM_KOMPONEN}" /></td>
    </tr>
    <tr>
        <td>Singkatan</td>
        <td><input type="text" name="singkatan" maxlength="5" size="5" value="{$kn.SINGKATAN}"/></td>
    </tr>
    <tr>
        <td>Bobot</td>
        <td><input type="text" name="bobot" maxlength="4" size="4" value="{$kn.BOBOT}"/>%</td>
    </tr>
    <tr>
        <td>Ujian Tulis</td>
        <td><input type="checkbox" name="is_tes_tulis" {if $kn.IS_TES_TULIS}checked="checked"{/if} /></td>
    </tr>
    <tr>
        <td>Keterangan</td>
        <td><input type="text" name="keterangan" maxlength="50" size="50" {$kn.KETERANGAN}/></td>
    </tr>
    <tr>
        <td colspan="2">
            <a href="penilaian-komponen.php?id_penerimaan={$ps.ID_PENERIMAAN}&id_penerimaan_prodi={$ps.ID_PENERIMAAN_PRODI}">Kembali</a>
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>