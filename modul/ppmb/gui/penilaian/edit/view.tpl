<div class="center_title_bar">Edit Nilai Peserta</div>

<table>
    <tr>
        <td>
            <label>No Ujian :</label>
        </td>
        <td>
            <form action="penilaian-edit.php" method="get">
            <input type="search" name="no_ujian" {if isset($smarty.get.no_ujian)}value="{$smarty.get.no_ujian}"{/if} />
            <input type="submit" value="Cari..." />
            </form>
        </td>
    </tr>
</table>
            
{if $edited}<h2>{$edited}</h2>{/if}
            
{if !empty($cmb)}

<form action="penilaian-edit.php?{$smarty.server.QUERY_STRING}" method="post">    
<input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
<table>
    <tr>
        <th colspan="2">Detail Nilai Peserta</th>
    </tr>
    <tr>
        <td>Nama</td>
        <td>{$cmb.NM_C_MHS}</td>
    </tr>
    <tr>
        <td>Nilai TPA</td>
        <td><input type="text" name="nilai_tpa" size="5" maxlength="5" value="{$cmb.NILAI_TPA}" /></td>
    </tr>
    <tr>
        <td>Nilai B Inggris</td>
        <td><input type="text" name="nilai_inggris" size="5" maxlength="5" value="{$cmb.NILAI_INGGRIS}" /></td>
    </tr>
    <tr>
        <td>Nilai Bidang Ilmu</td>
        <td><input type="text" name="nilai_ilmu" size="5" maxlength="5" value="{$cmb.NILAI_ILMU}" /></td>
    </tr>
    <tr>
        <td>Nilai Wawancara</td>
        <td><input type="text" name="nilai_wawancara" size="5" maxlength="5" value="{$cmb.NILAI_WAWANCARA}" /></td>
    </tr>
    <tr>
        <td>Nilai IPK</td>
        <td><input type="text" name="nilai_ipk" size="5" maxlength="5" value="{$cmb.NILAI_IPK}" /></td>
    </tr>
    <tr>
        <td>Nilai Karya Ilmiah</td>
        <td><input type="text" name="nilai_karya_ilmiah" size="5" maxlength="5" value="{$cmb.NILAI_KARYA_ILMIAH}" /></td>
    </tr>
    <tr>
        <td>Nilai Rekomendasi</td>
        <td><input type="text" name="nilai_rekomendasi" size="5" maxlength="5" value="{$cmb.NILAI_REKOMENDASI}" /></td>
    </tr>
    <tr>
        <td>Nilai Matrikulasi</td>
        <td><input type="text" name="nilai_matrikulasi" size="5" maxlength="5" value="{$cmb.NILAI_MATRIKULASI}" /></td>
    </tr>
    <tr>
        <td>Nilai Psiko</td>
        <td><input type="text" name="nilai_psiko" size="5" maxlength="5" value="{$cmb.NILAI_PSIKO}" /></td>
    </tr>
    <tr>
        <td>Nilai Tulis</td>
        <td><input type="text" name="nilai_tulis" size="5" maxlength="5" value="{$cmb.NILAI_TULIS}" /></td>
    </tr>
    <tr>
        <td>Nilai Lain</td>
        <td><input type="text" name="nilai_lain" size="5" maxlength="5" value="{$cmb.NILAI_LAIN}" /></td>
    </tr>
	<tr>
        <td>Nilai Gabungan PPDS</td>
        <td><input type="text" name="nilai_gab_ppds" size="5" maxlength="5" value="{$cmb.NILAI_GAB_PPDS}" /></td>
    </tr>
    <tr>
        <td>Total Nilai</td>
        <td><input type="text" name="total_nilai" size="5" maxlength="5" value="{$cmb.TOTAL_NILAI}" readonly="readonly" /></td>
    </tr>
    <tr>
        <td class="center" colspan="2">
            <a href="penilaian-edit.php">Batal</a>
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
    
</form>
    
{/if}