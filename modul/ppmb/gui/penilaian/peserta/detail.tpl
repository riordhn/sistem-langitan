<div class="center_title_bar">Penilaian Peserta - {$nm_program_studi}</div>

{literal}<style>
    .content table.statistik tr td { padding: 2px; font-size: small; }
    .kondisi-1 td { color: yellow; }
    .kondisi-2 td { color: #000; }
    .kondisi-3 td { color: #00f; }
    .kondisi-4 td { color: #f00; }
</style>{/literal}

<table style="width: 40%; margin: 0px auto;" class="statistik">
    <tr>
        <th colspan="2">Statistik</th>
    </tr>
    <tr>
        <td>Nilai Min</td>
        <td class="center">{$statistik.NILAI_MIN}</td>
    </tr>
    <tr>
        <td>Nilai Rata</td>
        <td class="center">{$statistik.NILAI_RATA}</td>
    </tr>
    <tr>
        <td>Nilai Max</td>
        <td class="center">{$statistik.NILAI_MAX}</td>
    </tr>
    <tr>
        <td>Standar Deviasi</td>
        <td class="center">{$statistik.STANDAR_DEVIASI}</td>
    </tr>
    <tr>
        <td>Passing Grade</td>
        <td class="center">{$statistik.PASSING_GRADE}</td>
    </tr>
    <tr>
        <td>Kuota</td>
        <td class="center">{$statistik.KUOTA}</td>
    </tr>
    <tr>
        <td>Peminat</td>
        <td class="center">{$statistik.PEMINAT}</td>
    </tr>
</table>
<br/>
<table style="width: 100%">
    <tr>
        <th rowspan="2" style="width: 25px; vertical-align: middle;">No</th>
        <th rowspan="2" style="width: 65px; vertical-align: middle;">No Ujian</th>
        <th rowspan="2" style="vertical-align: middle;">Nama Peserta</th>
        <th colspan="7">Nilai terbobot</th>
        <th rowspan="2" style="width:40px; vertical-align: middle;">Total</th>
    </tr>
    <tr>
        <th style="width:40px">TPA</th>
        <th style="width:40px">ING</th>
        <th style="width:40px">WWC</th>
        <th style="width:40px">IPK</th>
        <th style="width:40px">Ilmu</th>
        <th style="width:40px">KYI</th>
        <th style="width:40px">RKM</th>
    </tr>
    {foreach $calon_mahasiswa_baru_set as $cmb}
    <tr {if $cmb.TOTAL_NILAI < ($statistik.NILAI_RATA - ($statistik.STANDAR_DEVIASI*2))}
            class="kondisi-1"
        {else}
            {if $cmb.TOTAL_NILAI < $statistik.PASSING_GRADE}
                class="kondisi-4"
            {else}
                {if ($cmb@index + 1) <= $statistik.KUOTA}
                    class="kondisi-2"
                {else}
                    class="kondisi-3"
                {/if}
            {/if}
        {/if}>
        <td class="center">{$cmb@index + 1}</td>
        <td>{$cmb.NO_UJIAN}</td>
        <td><span>{$cmb.NM_C_MHS}, {$cmb.GELAR}</span></td>
        <td class="center">{$cmb.NILAI_TPA}</td>
        <td class="center">{$cmb.NILAI_INGGRIS}</td>
        <td class="center">{$cmb.NILAI_WAWANCARA}</td>
        <td class="center">{$cmb.NILAI_IPK}</td>
        <td class="center">{$cmb.NILAI_ILMU}</td>
        <td class="center">{$cmb.NILAI_KARYA_ILMIAH}</td>
        <td class="center">{$cmb.NILAI_REKOMENDASI}</td>       
        <td class="center">{$cmb.TOTAL_NILAI}</td>
    </tr>
    {/foreach}
</table>
    
<a href="penilaian-peserta.php?mode=prodi&id_penerimaan={$penerimaan.ID_PENERIMAAN}">Kembali</a>