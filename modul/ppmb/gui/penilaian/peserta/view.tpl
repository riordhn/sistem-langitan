<div class="center_title_bar">Penilaian Peserta</div>

<script type="text/javascript">
    function getTemplate(id_penerimaan)
    {
        var is_include_nilai = false;
        if (confirm("Apakah nilai yang sudah masuk disertakan ?\n[OK] untuk Ya, [Cancel] untuk Tidak"))
            is_include_nilai = true;
        
        window.location = 'penilaian-peserta-excel.php?mode=template&id_penerimaan=' + id_penerimaan + '&nilai=' + is_include_nilai;
    }
</script>

<table class="filter">
    <tr>
        <th>Tahun</th>
        <th>Jalur</th>
        <th>Jenjang</th>
        <th>Penerimaan</th>
        <th>Gel</th>
        <th>Semester</th>
        <th>Aksi</th>
    </tr>
    {foreach $penerimaan_set as $p}
    <tr {if $p@index is div by 2}class="row1"{/if}>
        <td class="center">{$p.TAHUN}</td>
        <td>{$p.NM_JALUR}</td>
        <td class="center">{$p.NM_JENJANG}</td>
        <td>{$p.NM_PENERIMAAN}</td>
        <td class="center">{$p.GELOMBANG}</td>
        <td class="center">{$p.SEMESTER}</td>
        <td>
            <a href="penilaian-peserta.php?mode=prodi&id_penerimaan={$p.ID_PENERIMAAN}">Lihat</a> |
            <span class="anchor-span" onclick="getTemplate({$p.ID_PENERIMAAN}); return false;">Template</span> |
            <a href="penilaian-peserta.php?mode=xml&id_penerimaan={$p.ID_PENERIMAAN}" class="disable-ajax" target="_blank">Get XML</a>
        </td>
    </tr>
    {/foreach}
</table>
{*
<h2>Upload Nilai</h2>
<iframe src="penilaian-peserta-upload.php?id_penerimaan={$smarty.get.id_penerimaan}" style="width: 600px; height: 60px"></iframe>

<p>Perhatian : Setiap excel yang diupload akan langsung mereplace nilai yang sudah ada.</p>
*}