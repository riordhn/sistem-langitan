<div class="center_title_bar">Statistik Nilai Peserta - Generate</div>

<h2>{$penerimaan.NM_PENERIMAAN} Gelombang {$penerimaan.GELOMBANG} Semester {$penerimaan.SEMESTER} Tahun {$penerimaan.TAHUN}</h2>
<p><a href="penilaian-statistik.php?id_penerimaan={$penerimaan.ID_PENERIMAAN}">Kembali</a></p>
<form action="penilaian-statistik.php?id_penerimaan={$penerimaan.ID_PENERIMAAN}" method="post">
<input type="hidden" name="mode" value="generate" />
<input type="hidden" name="id_penerimaan" value="{$penerimaan.ID_PENERIMAAN}" />

<table>
    <tr>
        <td>

        <table>
            <tr>
                <th>Program studi yang diikutkan perhitungan statistik</th>
            </tr>
            {foreach $program_studi_set as $ps}
            <tr {if $ps@index is div by 2}class="row1"{/if}>
                <td>
                    <label><input type="checkbox" name="pp_asal[]" value="{$ps.ID_PENERIMAAN_PRODI}" />
                    {$ps.NM_PENERIMAAN} - {$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI}
                    </label>
                </td>
            </tr>
            {/foreach}
        </table>
        
        </td>
        <td>
        
            <table>
                <tr>
                    <th>Program studi tujuan</th>
                </tr>
                {foreach $program_studi_set as $ps}
                <tr {if $ps@index is div by 2}class="row1"{/if}>
                    <td>
                        <label><input type="checkbox" name="pp_tujuan[]" value="{$ps.ID_PENERIMAAN_PRODI}" />
                        {$ps.NM_PENERIMAAN} - {$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI}
                        </label>
                    </td>
                </tr>
                {/foreach}
            </table>
        
        </td>
    </tr>
    <tr>
        <td colspan="2"><input type="submit" value="Generate" /></td>
    </tr>
</table>
</form>