<div class="center_title_bar">Statistik Nilai Peserta</div>

<form action="penilaian-statistik.php" method="get" id="filter">
<table>
    <tr>
        <td>Penerimaan</td>
        <td>
            <select name="id_penerimaan" onchange="$('#filter').submit()">
                <option value="">-- Pilih Penerimaan --</option>
            {foreach $penerimaan_set as $p}
                <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                {foreach $p.p_set as $p2}
                <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN}</option>
                {/foreach}
                </optgroup>
            {/foreach}
            </select>
            <!--<input type="submit" value="Lihat" />-->
            {if $smarty.get.id_penerimaan != ''}
            <input type="button" value="Generate" onclick="window.location = '#penilaian-gstat!penilaian-statistik.php?mode=generate&id_penerimaan={$smarty.get.id_penerimaan}';" />
            {/if}
        </td>
    </tr>
</table>
</form>
            
{if !empty($statistik_set)}
    
    {if $updated}<h2>{$updated}</h2>{/if}
    {if $changed}<h2>{$changed}</h2>{/if}
    
    <form action="penilaian-statistik.php?id_penerimaan={$smarty.get.id_penerimaan}" method="post">
        <input type="hidden" name="mode" value="edit-kuota" />
        <table>
            <tr>
                <th>No</th>
                <th>Prodi</th>
                <th>Min</th>
                <th>Rata</th>
                <th>Max</th>
                <th title="Standar Deviasi">SD</th>
                <th title="Passing Grade">PG</th>
                <th>Diterima</th>
                <th>Kuota</th>
				<th>Kuota Total</th>
            </tr>
            {foreach $statistik_set as $s}
            <tr {if $s@index is not div by 2}class="row1"{/if}>
                <td class="center">{$s@index + 1}</td>
                <td>{$s.NM_JENJANG} {$s.NM_PROGRAM_STUDI}</td>    
                <td class="center">{number_format($s.NILAI_MIN, 2)}</td>
                <td class="center">{number_format($s.NILAI_RATA, 2)}</td>
                <td class="center">{number_format($s.NILAI_MAX, 2)}</td>
                <td class="center">{number_format($s.STANDAR_DEVIASI, 2)}</td>
                <td class="center">{number_format($s.PASSING_GRADE, 2)}</td>
                <td class="center">{$s.DITERIMA}</td>
                <td class="center">
                    <input name="k_{$s.ID_STATISTIK_CMHS}" type="text" value="{$s.KUOTA}" style="text-align: center" maxlength="4" size="2" />
                </td>
				<td class="center">
                    <input name="kt_{$s.ID_STATISTIK_CMHS}" type="text" value="{$s.KUOTA_TOTAL}" style="text-align: center" maxlength="4" size="2" />
                </td>
            </tr>
            {/foreach}
            <tr>
                <td class="center" colspan="10"><input type="submit" value="Simpan" /></td>
            </tr>
        </table>
    </form>
{/if}

<a href="/modul/rekapitulasi/ppmb-statistik/" class="disable-ajax" target="_blank">Lihat Rekapitulasi Statistik</a>