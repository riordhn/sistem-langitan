<div class="center_title_bar">Scan LJK</div>


{if !empty($no_kosong_set)}
<b>Nomer tidak ada dalam database</b>
<table>
    <tr>
        <th>No</th>
        <th>No Ujian</th>
        <th>Nama</th>
        <th>Baris</th>
    </tr>
    {foreach $no_kosong_set as $nk}
    <tr>
        <td>{$nk@index + 1}</td>
        <td>{$nk.NO_UJIAN}</td>
        <td>{$nk.NM_C_MHS}</td>
        <td>{$nk.BARIS}</td>
    </tr>
    {/foreach}
</table>
{/if}

{if !empty($no_sama_set)}
<table>
    <tr>
        <th>No</th>
        <th>Scanning</th>
        <th>Database</th>
    </tr>
    {foreach $no_sama_set as $ns}
    <tr>
        <td>{$ns@index + 1}</td>
        <td>[{$ns.BARIS}] {$ns.NO_UJIAN} - {$ns.NM_C_MHS}<br/>[{$ns.BARIS_2}] {$ns.NO_UJIAN_2} - {$ns.NM_C_MHS_2}</td>
        <td>{$ns.NO_UJIAN_DB} - {$ns.NM_C_MHS_DB}</td>
    </tr>
    {/foreach}
</table>
{else}
<form action="penilaian-scan.php" method="post" id="nilai">
    <input type="hidden" name="mode" value="submit-nilai" />
    <table>
        <tr>
            <th>No</th>
            <th>No Ujian</th>
            <th>Nama</th>
            <th>Kode</th>
            <th>Bidang TPA</th>
            <th>Skor TPA</th>
            <th>Prestasi IPA</th>
            <th>Skor IPA</th>
            <th>Prestasi IPS</th>
            <th>Skor IPS</th>
        </tr>
        {foreach $nilai_set as $n}
        <tr {if $n@index is not div by 2}class="row1"{/if}>
            <td class="center">{$n@index + 1}</td>
            <td>{$n.NO_UJIAN}</td>
            <td>{$n.NM_C_MHS}</td>
            <td>{$n.KODE_SOAL}</td>
            <td class="center">{$n.JUMLAH_BENAR_TPA} | {$n.JUMLAH_SALAH_TPA} | {$n.JUMLAH_KOSONG_TPA}</td>
            <td class="center">{$n.SKOR_TPA}</td>
            <td class="center">{$n.JUMLAH_BENAR_PRESTASI_A} | {$n.JUMLAH_SALAH_PRESTASI_A} | {$n.JUMLAH_KOSONG_PRESTASI_A}</td>
            <td class="center">{$n.SKOR_PRESTASI_A}
            </td>
            <td class="center">{$n.JUMLAH_BENAR_PRESTASI_S} | {$n.JUMLAH_SALAH_PRESTASI_S} | {$n.JUMLAH_KOSONG_PRESTASI_S}</td>
            <td class="center">{$n.SKOR_PRESTASI_S}
            </td>
        </tr>
        {/foreach}
        <tr>
            <td colspan="10">
                <a href="penilaian-scan.php">Kembali</a>
                <button onclick="$('#nilai').submit(); return false;">Simpan</button>
            </td>
        </tr>
    </table>
</form>
{*
<form action="penilaian-scan.php" method="post" id="nilai">
<input type="hidden" name="mode" value="submit-nilai" />
<table>
    <tr>
        <th>No</th>
        <th>No Ujian</th>
        <th>Nama</th>
        <th>Kode</th>
        <th>Bidang TPA</th>
        <th>Skor TPA</th>
        <th>Inggris</th>
        <th>Skor Inggris</th>
    </tr>
    {foreach $nilai_set as $n}
    <tr {if $n@index is not div by 2}class="row1"{/if}>
        <td class="center">{$n@index + 1}</td>
        <td>{$n.NO_UJIAN}</td>
        <td>{$n.NM_C_MHS}</td>
        <td>{$n.KODE_SOAL}</td>
        <td class="center">{$n.JUMLAH_BENAR_TPA} / {$n.JUMLAH_SALAH_TPA} / {$n.JUMLAH_KOSONG_TPA}</td>
        <td class="center">{$n.SKOR_TPA}</td>
        <td class="center">{$n.JUMLAH_BENAR_INGGRIS} / {$n.JUMLAH_SALAH_INGGRIS} / {$n.JUMLAH_KOSONG_INGGRIS}</td>
        <td class="center">{$n.SKOR_INGGRIS}<input type="hidden" name="tpa{$n.ID_C_MHS}" value="{$n.SKOR_TPA}" /><input type="hidden" name="inggris{$n.ID_C_MHS}" value="{$n.SKOR_INGGRIS}" />
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="8">
            <a href="penilaian-scan.php">Kembali</a>
            <button onclick="$('#nilai').submit(); return false;">Simpan</button>
        </td>
    </tr>
</table>
</form>
*}
{/if}

<p>Keterangan Nilai :  <br/>- BENAR / SALAH / KOSONG <br/>
- Ketika sudah disubmit, nilai di database akan di replace</p>