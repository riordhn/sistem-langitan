<div class="center_title_bar">Scan LJK</div>

{if $updated}<script type="text/javascript">alert('{$updated}');</script>{/if}

<form action="penilaian-scan.php?mode=koreksi" method="post">
<table>
    <tr>
        <td>Penerimaan</td>
        <td>
            <select name="id_penerimaan">
                <option value="">-- Pilih Penerimaan --</option>
            {foreach $penerimaan_set as $p}
                <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                {foreach $p.p_set as $p2}
                <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN}</option>
                {/foreach}
                </optgroup>
            {/foreach}
            </select>
        </td>
    </tr>
</table>
<p>Input Data Hasil Scan</p>
<textarea name="stream" cols="75" rows="5"></textarea><br/>
<input type="submit" value="Proses" />
</form>