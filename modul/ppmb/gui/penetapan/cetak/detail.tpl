<div class="center_title_bar">{$penetapan.NM_PENETAPAN} - {$program_studi.NM_PROGRAM_STUDI}</div>

{literal}<style>
    .content table.statistik tr td { padding: 2px; font-size: small; }
    .kondisi-1 td { color: yellow; }
    .kondisi-2 td { color: #000; }
    .kondisi-3 td { color: #00f; }
    .kondisi-4 td { color: #f00; }
</style>{/literal}

<table style="width: 40%; margin: 0px auto;" class="statistik">
    <tr>
        <th colspan="2">Statistik</th>
    </tr>
    <tr>
        <td>Nilai Min</td>
        <td class="center">{$statistik.NILAI_MIN}</td>
    </tr>
    <tr>
        <td>Nilai Max</td>
        <td class="center">{$statistik.NILAI_MAX}</td>
    </tr>
    <tr>
        <td>Nilai Rata</td>
        <td class="center">{$statistik.NILAI_RATA}</td>
    </tr>
    <tr>
        <td>Standar Deviasi</td>
        <td class="center">{$statistik.STANDAR_DEVIASI}</td>
    </tr>
    <tr>
        <td>Passing Grade</td>
        <td class="center">{$statistik.PASSING_GRADE}</td>
    </tr>
    <tr>
        <td>Kuota</td>
        <td class="center">{$statistik.KUOTA}</td>
    </tr>
    <tr>
        <td>Peminat</td>
        <td class="center">{$statistik.PEMINAT}</td>
    </tr>
</table>
<br/>
<table style="width: 100%">
    <tr>
        <th rowspan="2" style="width: 25px; vertical-align: middle;">No</th>
        <th rowspan="2" style="width: 35px; vertical-align: middle;">Lulus</th>
        <th rowspan="2" style="width: 65px; vertical-align: middle;">No Ujian</th>
        <th rowspan="2" style="vertical-align: middle;">Nama Peserta</th>
        <th {if $penerimaan.ID_JENJANG == 2 or $penerimaan.ID_JENJANG == 3}
                {if $smarty.get.id_program_studi == 52 or $smarty.get.id_program_studi == 53 or $smarty.get.id_program_studi == 54 or $smarty.get.id_program_studi == 55}
                    colspan="4"
                {else}
                    colspan="6"
                {/if}
            {elseif $penerimaan.ID_JENJANG == 10}
                colspan="5"
            {elseif $penerimaan.ID_JENJANG == 9}
                colspan="4"
            {/if}>Nilai terbobot</th>
        <th rowspan="2" style="width:40px; vertical-align: middle;">Total</th>
    </tr>
    <tr>
        <th style="width:40px">TPA</th>
        <th style="width:40px">ING</th>
        <th style="width:40px">WWC</th>
        {if $penerimaan.ID_JENJANG == 2 or $penerimaan.ID_JENJANG == 3}
            {if $smarty.get.id_program_studi == 52 or $smarty.get.id_program_studi == 53 or $smarty.get.id_program_studi == 54 or $smarty.get.id_program_studi == 55}
                <th style="width:40px">Ilmu</th>
            {else}
                <th style="width:40px">IPK</th>
                <th style="width:40px">KYI</th>
                <th style="width:40px">RKM</th>        
            {/if}
        {elseif $penerimaan.ID_JENJANG == 10}
        <th style="width:40px">IPK</th>
        <th style="width:40px">Ilmu</th>
        {elseif $penerimaan.ID_JENJANG == 9}
        <th style="width:40px">Ilmu</th>    
        {/if}
    </tr>
    {foreach $calon_mahasiswa_baru_set as $cmb}
    <tr {if $cmb.TOTAL_NILAI < ($statistik.NILAI_RATA - ($statistik.STANDAR_DEVIASI*2))}
            class="kondisi-1"
        {else}
            {if $cmb.TOTAL_NILAI < $statistik.PASSING_GRADE}
                class="kondisi-4"
            {else}
                {if ($cmb@index + 1) <= $statistik.KUOTA}
                    class="kondisi-2"
                {else}
                    class="kondisi-3"
                {/if}
            {/if}
        {/if}>
        <td class="center">{$cmb@index + 1}</td>
        <td>
            <input type="checkbox" name="cmb{$cmb.ID_C_MHS}" value="{$cmb.IS_LULUS}" />
        </td>
        <td>{$cmb.NO_UJIAN}</td>
        <td><span>{$cmb.NM_C_MHS}</span></td>
        <td class="center">{$cmb.NILAI_TPA}</td>
        <td class="center">{$cmb.NILAI_INGGRIS}</td>
        <td class="center">{$cmb.NILAI_WAWANCARA}</td>
        {if $penerimaan.ID_JENJANG == 2 or $penerimaan.ID_JENJANG == 3}
            {if $smarty.get.id_program_studi == 52 or $smarty.get.id_program_studi == 53 or $smarty.get.id_program_studi == 54 or $smarty.get.id_program_studi == 55}
                <td class="center">{$cmb.NILAI_ILMU}</td>
            {else}
                <td class="center">{$cmb.NILAI_IPK}</td>
                <td class="center">{$cmb.NILAI_KARYA_ILMIAH}</td>
                <td class="center">{$cmb.NILAI_REKOMENDASI}</td>       
            {/if}
        {elseif $penerimaan.ID_JENJANG == 10}
        <td class="center">{$cmb.NILAI_IPK}</td>
        <td class="center">{$cmb.NILAI_ILMU}</td>
        {elseif $penerimaan.ID_JENJANG == 9}
        <td class="center">{$cmb.NILAI_ILMU}</td>    
        {/if}
        <td class="center">{$cmb.TOTAL_NILAI}</td>
    </tr>
    {/foreach}
</table>
    
<a href="penetapan-cetak.php?mode=sidang&id_penetapan={$penetapan.ID_PENETAPAN}">Kembali</a>