{literal}
	<style>
	.button-print{
		-webkit-border-radius: 10px;
		-moz-border-radius: 10px;
		border-radius: 10px;
		color:white;
		cursor:pointer;
		padding:5px;
		background-color:#233D0E;
	}
	</style>
{/literal}
<div class="center_title_bar">{$penetapan.NM_PENETAPAN}</div>

{foreach $penerimaan_set as $p}
<table style="width: 90%">
    <caption>{$p.NM_PENERIMAAN} Gelombang {$p.GELOMBANG} Semester {$p.SEMESTER} Tahun {$p.TAHUN}</caption>
    <tr>
        <th style="width: 30px">No</th>
        <th>Program Studi</th>
        <th style="width: 40px">Peminat</th>
        <th style="width: 40px">Kuota</th>
		<th style="width: 40px">Diterima</th>
        <th style="width: 40px"></th>
    </tr>
	{$jumlah_peminat = 0}
	{$jumlah_diterima = 0}
	{$jumlah_kuota = 0}
    {foreach $p.program_studi_set as $ps}
    <tr>
        <td class="center">{$ps@index + 1}</td>
        <td>{$ps.NM_PROGRAM_STUDI}</td>
        <td class="center">{$ps.JUMLAH_PEMINAT}</td>
        <td class="center">{$ps.KUOTA}</td>
		<td class="center">{$ps.JUMLAH_DITERIMA}</td>
        <td>
            <span style="color:green;cursor:pointer" onclick="window.open('/modul/ppmb/penetapan-cetak-pdf.php?id_penetapan_penerimaan={$p.ID_PENETAPAN_PENERIMAAN}&id_program_studi={$ps.ID_PROGRAM_STUDI}&id_penetapan={$smarty.get.id_penetapan}', '_blank');">Cetak</span>
        </td>
    </tr>
	{$jumlah_peminat = $jumlah_peminat + $ps.JUMLAH_PEMINAT}
	{$jumlah_kuota = $jumlah_kuota + $ps.KUOTA}
	{$jumlah_diterima = $jumlah_diterima + $ps.JUMLAH_DITERIMA}
    {/foreach}
	<tr>
		<td colspan="2"></td>
		<td class="center">{$jumlah_peminat}</td>
		<td class="center">{$jumlah_kuota}</td>
		<td class="center">{$jumlah_diterima}</td>
		<td></td>
	</tr>
</table>
{/foreach}
<div style="width:100%;text-align:center">
	<span class="button-print" onclick="window.open('/modul/ppmb/penetapan-cetak-pdf.php?id_penetapan={$smarty.get.id_penetapan}', '_blank');">Cetak All</span>
</div>
<a href="penetapan-cetak.php">Kembali</a>