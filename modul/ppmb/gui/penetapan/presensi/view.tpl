<div class="center_title_bar">Rekap Presensi &gt; Pilih Jadwal Persidangan</div>

<table style="width: 100%">
    <tr>
        <th style="width: 30px">No</th>
        <th>Persidangan Penetapan</th>
        <th style="width: 60px">Sidang Ke</th>
        <th style="width: 40px"></th>
    </tr>
    {foreach $penetapan_set as $p}
    <tr>
        <td class="center">{$p@index+1}</td>
        <td>{$p.NM_PENETAPAN}</td>
        <td class="center">{$p.PERIODE}</td>
        <td>
            <a href="penetapan-presensi.php?mode=rekap&id_penetapan={$p.ID_PENETAPAN}">Lihat</a>
        </td>
    </tr>
    {/foreach}
</table>