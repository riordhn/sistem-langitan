<div class="center_title_bar">Rekap Presensi &gt; Rekap</div>

<table>
    <tr>
        <th>No</th>
        <th>Status Presensi</th>
        <th>Jumlah</th>
        <th>Aksi</th>
    </tr>
    {$total = 0}
    {foreach $presensi_set as $p}
    <tr>
        <td class="center">{$p@index + 1}</td>
        <td>{$p.NM_STATUS_UJIAN}</td>
        <td class="center">{$p.JUMLAH}</td>
        <td><a href="penetapan-presensi.php?mode=detail&id_penetapan={$smarty.get.id_penetapan}&id_status_ujian={$p.ID_STATUS_UJIAN}">Lihat</a></td>
    </tr>
    {$total = $total + $p.JUMLAH}
    {/foreach}
    <tr>
        <td colspan="2">Jumlah</td>
        <td class="center">{$total}</td>
        <td></td>
    </tr>
</table>