<div class="center_title_bar">Rekap Presensi &gt; Detail</div>

<a href="penetapan-presensi.php?mode=rekap&id_penetapan={$smarty.get.id_penetapan}">Kembali</a>
<table>
    <tr>
        <th>No</th>
        <th>No Ujian</th>
        <th>Nama</th>
        <th>Prodi Pilihan 1</th>
        <th>Prodi Pilihan 2</th>
        <th>Prodi Pilihan 3</th>
        <th>Prodi Pilihan 4</th>
        <th>Asal SMA</th>
    </tr>
    {foreach $cmb_set as $cmb}
    <tr>
        <td>{$cmb@index + 1}</td>
        <td>{$cmb.NO_UJIAN}</td>
        <td>{$cmb.NM_C_MHS}</td>
        <td>{$cmb.NM_PILIHAN_1}</td>
        <td>{$cmb.NM_PILIHAN_2}</td>
        <td>{$cmb.NM_PILIHAN_3}</td>
        <td>{$cmb.NM_PILIHAN_4}</td>
        <td>{$cmb.NM_SEKOLAH}</td>
    </tr>
    {/foreach}
</table>