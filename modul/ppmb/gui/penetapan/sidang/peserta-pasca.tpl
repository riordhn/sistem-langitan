<style type="text/css">
	ol {
		margin-left: 1em;
	}
</style>

<div class="center_title_bar">{$cmb.NM_C_MHS}</div>

<div id="auto-heighter" >
<table style="width: 100%;" id="info">
	<tbody>
		<tr>
			<td rowspan="17" style="width: 192px">
				<img src="{$cmb.FILE_FOTO}" style="width: 225px" />
			</td>
			<td style="width: 164px">NAMA</td>
			<td>{$cmb.NM_C_MHS}, {$cmb.GELAR}</td>
		</tr>
		<tr>
			<td>TEMPAT TANGGAL LAHIR</td>
			<td>{$cmb.NM_KOTA_LAHIR}, {strftime('%d %B %Y', strtotime($cmb.TGL_LAHIR))}</td>
		</tr>
		<tr>
			<td>ALAMAT</td>
			<td>{$cmb.ALAMAT}</td>
		</tr>
		<tr>
			<td>PENDIDIKAN S1</td>
			<td>{$cmb.PRODI_S1} - {$cmb.PTN_S1} ({if $cmb.STATUS_PTN_S1 == 1}Negeri{else}Swasta{/if})</td>
		</tr>
		<tr>
			<td>IPK S1</td>
			<td>{$cmb.IP_S1}</td>
		</tr>
		{if $cmb.ID_JENJANG == 3}
		<tr>
			<td>PENDIDIKAN S2</td>
			<td>{$cmb.PRODI_S2} - {$cmb.PTN_S2} ({if $cmb.STATUS_PTN_S2 == 1}Negeri{else}Swasta{/if})</td>
		</tr>
		<tr>
			<td>IPK S2</td>
			<td>{$cmb.IP_S2}</td>
		</tr>
		{else if $cmb.ID_JENJANG == 10}
			<tr>
				<td>PENDIDIKAN Profesi</td>
				<td>{$cmb.PRODI_S2} - {$cmb.PTN_S2} ({if $cmb.STATUS_PTN_S2 == 1}Negeri{else}Swasta{/if})</td>
			</tr>
			<tr>
				<td>IPK Profesi</td>
				<td>{if $cmb.STATUS_PTN_S2 == 1}{$cmb.IP_S2}{else}{$cmb.IP_S2}{/if}</td>
			</tr>
			<tr>
				<td>STATUS PTT</td>
				<td>{$cmb.STATUS_PTT}</td>
			</tr>
			<tr>
				<td>SKOR TPA</td>
				<td>{$cmb.NILAI_TPA_PASCA}</td>
			</tr>
			<tr>
				<td>SKOR B. INGGRIS</td>
				<td>{$cmb.NILAI_INGGRIS}</td>
			</tr>
		{/if}
		
		{if in_array($cmb.ID_JENJANG, array(2, 3))}{* S2 / S3 *}
			
			<tr>
				<td>SKOR TPA</td>
				<td>{$cmb.NILAI_TPA_PASCA}</td>
			</tr>
			<tr>
				<td>SKOR B. INGGRIS</td>
				<td>{$cmb.NILAI_INGGRIS}</td>
			</tr>
			
			{if in_array($cmb.ID_PILIHAN_1, array(52, 53, 54, 55))}{* MM, MIH, MKN, Sain Hukum, MHP *}
				<tr>
					<td>SKOR WAWANCARA</td>
					<td>{$cmb.NILAI_WAWANCARA}</td>
				</tr>
				<tr>
					<td>SKOR ILMU</td>
					<td>{$cmb.NILAI_ILMU}</td>
				</tr>
			{elseif $cmb.ID_PILIHAN_1 == 137}
				<tr>
					<td>SKOR WAWANCARA</td>
					<td>{$cmb.NILAI_WAWANCARA}</td>
				</tr>
				<tr>
					<td>SKOR ILMU</td>
					<td>{$cmb.NILAI_ILMU}</td>
				</tr>
				<tr>
					<td>SKOR PSIKOTES</td>
					<td>{$cmb.NILAI_PSIKO}</td>
				</tr>
			{elseif $cmb.ID_PILIHAN_1 == 73}
				<tr>
					<td>SKOR ILMU</td>
					<td>{$cmb.NILAI_ILMU}</td>
				</tr>
			{else}
				<tr>
					<td>SKOR WAWANCARA</td>
					<td>{$cmb.NILAI_WAWANCARA}</td>
				</tr>
				<tr>
					<td>SKOR IPK</td>
					<td>{$cmb.NILAI_IPK}</td>
				</tr>
				<tr>
					<td>SKOR KARYA ILMIAH</td>
					<td>{$cmb.NILAI_KARYA_ILMIAH}</td>
				</tr>
				<tr>
					<td>SKOR REKOMENDASI</td>
					<td>{$cmb.NILAI_REKOMENDASI}</td>
				</tr>
			{/if}
		{elseif $cmb.ID_JENJANG == 10}
			{if $cmb.NM_PENERIMAAN == 'PPDS'}
				<tr>
					<td>NILAI PRODI (100%)</td>
					<td>{$cmb.NILAI_GAB_PPDS}</td>
				</tr>
			{else if $cmb.NM_PENERIMAAN == 'PPDGS'}
				<tr>
					<td>SKOR WAWANCARA</td>
					<td>{$cmb.NILAI_WAWANCARA}</td>
				</tr>
				<tr>
					<td>SKOR IPK</td>
					<td>{$cmb.NILAI_IPK}</td>
				</tr>
				<tr>
					<td>SKOR ILMU</td>
					<td>{$cmb.NILAI_ILMU}</td>
				</tr>
			{/if}
		{elseif $cmb.ID_JENJANG == 9}
		<tr>
			<td>SKOR WAWANCARA</td>
			<td>{$cmb.NILAI_WAWANCARA}</td>
		</tr>
		<tr>
			<td>SKOR ILMU</td>
			<td>{$cmb.NILAI_ILMU}</td>
		</tr>    
		{/if}
		<tr>
			<td>NILAI AKHIR</td>
			<td><strong>{$cmb.TOTAL_NILAI}</strong></td>
		</tr>
		<tr>
			<td>PILIHAN 1</td>
			<td>{$cmb.NM_PILIHAN_1}</td>
		</tr>
		{if $cm.ID_PILIHAN_2 != ''}
		<tr>
			<td>PILIHAN 2</td>
			<td>{$cmb.NM_PILIHAN_2}</td>
		</tr>
		{/if}
		<tr>
			<td>Keterangan Ujian</td>
			<td>
				{if $cmb.STATUS_UJIAN == 0}Tidak Hadir{/if}
				{if $cmb.STATUS_UJIAN == 1}Hadir{/if}
				{if $cmb.STATUS_UJIAN == 2}Salah Identitas{/if}
				{if $cmb.STATUS_UJIAN == 3}Melakukan Kecurangan{/if}
				{if $cmb.STATUS_UJIAN == 4}Salah Kode Soal{/if}
			</td>
		</tr>
		<tr>
			<td>IJAZAH</td>
			<td>
				Ijazah
				{*{if $cmb.BERKAS_IJAZAH == 1}Ijazah{else}SKL{/if}*}
			</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="3">
				Berkas Peserta
				<ol>
				{foreach $syarat_prodi_set as $sp}
					<li><a href="http://pendaftaran.unair.ac.id/files/{$cmb.ID_PENERIMAAN}/{$cmb.ID_C_MHS}/{$sp.FILE_SYARAT}" class="disable-ajax" target="_blank">{$sp.NM_SYARAT}</a></li>
				{/foreach}
				</ol>
				</td>
			</tr>
	</tfoot>
</table>
</div>

<a href="penetapan-sidang.php?mode=detail&id_penetapan_penerimaan={$smarty.get.id_penetapan_penerimaan}&id_program_studi={$smarty.get.id_program_studi}">Kembali</a>