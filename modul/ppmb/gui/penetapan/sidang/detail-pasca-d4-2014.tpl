<!--
	File template ini hanya digunakan untuk D4 tahun 2014,
	yang bersifat khusus.
-->

<div class="center_title_bar">{$penetapan.NM_PENETAPAN}</div>

<style>
	.content table.statistik tr td { padding: 2px; font-size: small; }
	.kondisi-1 td, .kondisi-1 td a { color: yellow; }
	.kondisi-2 td, .kondisi-2 td a { color: #000; } /* Black */
	.kondisi-3 td, .kondisi-3 td a { color: #00f; } /* Blue */
	.kondisi-4 td, .kondisi-4 td a { color: #f00; } /* Red */
	.statistik-off { display:none; }
</style>

<table style="width: 50%; margin: 0px auto;">
	<tr>
		<td colspan="2" class="center">
		<strong>{$penerimaan.NM_PENERIMAAN} {*Gelombang {$penerimaan.GELOMBANG}*} Semester {$penerimaan.SEMESTER} TA {$penerimaan.TAHUN}/{$penerimaan.TAHUN+1}</strong>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="center">
			<strong>{$program_studi.NM_JENJANG} - {$program_studi.NM_PROGRAM_STUDI}</strong>
		</td>
	</tr>
	<tr>
		<td {if $statistik.NILAI_RATA2 == ''}class="statistik-off"{/if} style="vertical-align: top">
			<table style="width: 100%; margin: 0px auto;" class="statistik">
				<tr>
					<th colspan="2" style="background-color: yellow; color: #000;">Statistik Sebelumnya</th>
				</tr>
				<tr>
					<td>Nilai Min</td>
					<td class="center">{$statistik.NILAI_MIN2}</td>
				</tr>
				<tr>
					<td>Nilai Rata</td>
					<td class="center">{$statistik.NILAI_RATA2}</td>
				</tr>
				<tr>
					<td>Nilai Max</td>
					<td class="center">{$statistik.NILAI_MAX2}</td>
				</tr>
				<tr>
					<td>Standar Deviasi</td>
					<td class="center">{$statistik.STANDAR_DEVIASI2}</td>
				</tr>
				<tr>
					<td>Passing Grade</td>
					<td class="center">{$statistik.PASSING_GRADE2}</td>
				</tr>
				
				<tr style="">
					<td>Kuota</td>
					<td class="center">{$statistik.KUOTA2}</td>
				</tr>
				<tr style="display: none">
					<td>Verivikasi</td>
					<td class="center">{$statistik.PEMINAT2}</td>
				</tr>
			</table>
		</td>
		<td>
			<table style="width: 100%; margin: 0px auto;" class="statistik">
				<tr>
					<th colspan="2">Statistik</th>
				</tr>
				<tr>
					<td>Nilai Min</td>
					<td class="center">{$statistik.NILAI_MIN}</td>
				</tr>
				<tr>
					<td>Nilai Rata</td>
					<td class="center">{$statistik.NILAI_RATA}</td>
				</tr>
				<tr>
					<td>Nilai Max</td>
					<td class="center">{$statistik.NILAI_MAX}</td>
				</tr>
				<tr>
					<td>Standar Deviasi</td>
					<td class="center">{$statistik.STANDAR_DEVIASI}</td>
				</tr>
				<tr>
					<td>Passing Grade</td>
					<td class="center">{$statistik.PASSING_GRADE}</td>
				</tr>
				
				<tr>
					<td>Kuota</td>
					<td class="center">{$statistik.KUOTA}</td>
				</tr>
				<tr style="">
					<td>Jumlah Peserta</td>
					<td class="center">{count($calon_mahasiswa_baru_set)}</td>
				</tr>
				<!--
				<tr>
					<td>Verifikasi</td>
					<td class="center">{$statistik.PEMINAT}</td>
				</tr>
				-->
			</table>
		</td>
	</tr>
</table>

<br/>
<a href="penetapan-sidang.php?mode=sidang&id_penetapan={$penetapan.ID_PENETAPAN}">Kembali</a>
<table style="width: 100%">
	<thead>
		<tr>
			<th rowspan="2" style="width: 25px; vertical-align: middle;">No</th>
			<th rowspan="2" style="width: 35px; vertical-align: middle;">Lulus</th>
			<th rowspan="2" style="width: 65px; vertical-align: middle;">No Ujian</th>
			<th rowspan="2" style="vertical-align: middle;">Nama Peserta</th>
			{if $penerimaan.ID_JENJANG == 2 or $penerimaan.ID_JENJANG == 3} {* S2 / S3 *}
				{if in_array($smarty.get.id_program_studi, array(52, 53, 54, 55))}
					<th colspan="4" class="center">Nilai</th>
				{elseif $smarty.get.id_program_studi == 137}
					<th colspan="5" class="center">Nilai</th>
				{elseif $smarty.get.id_program_studi == 73}
					<th colspan="3" class="center">Nilai</th>
				{else}
					<th colspan="6" class="center">Nilai</th>
				{/if}
			{elseif $penerimaan.ID_JENJANG == 10}{* SPESIALIS *}
				{if $penerimaan.NM_PENERIMAAN == 'PPDS'}
					<th colspan="3" class="center">Nilai</th>
				{else if $penerimaan.NM_PENERIMAAN == 'PPDGS'}
					<th colspan="5" class="center">Nilai</th>
				{/if}
			{elseif $penerimaan.ID_JENJANG == 9}{* PROFESI *}
				<th colspan="4" class="center">Nilai</th>
			{elseif $penerimaan.ID_JENJANG == 1 and $penerimaan.ID_JALUR == 4}{* S1-Alih Jenis *}
				<th colspan="3" class="center">Nilai</th>
			{elseif $penerimaan.ID_JENJANG == 1 and $penerimaan.ID_JALUR == 20}{* S1-PBSB *}
				<th colspan="3" class="center">Nilai</th>
			{/if}
			<th rowspan="2" style="width:40px; vertical-align: middle;">Nilai</th>
		</tr>
		<tr>
			{if $penerimaan.ID_JENJANG == 2 or $penerimaan.ID_JENJANG == 3}
				{if in_array($smarty.get.id_program_studi, array(52, 53, 54, 55))}
					<th style="width:40px">TPA</th>
					<th style="width:40px">ING</th>
					<th style="width:40px">WWC</th>
					<th style="width:40px">Ilmu</th>
				{elseif $smarty.get.id_program_studi == 137}
					<th style="width:40px">TPA</th>
					<th style="width:40px">ING</th>
					<th style="width:40px">WWC</th>
					<th style="width:40px">Ilmu</th>
					<th style="width:40px">Psi</th>
				{elseif $smarty.get.id_program_studi == 73}
					<th style="width:40px">TPA</th>
					<th style="width:40px">ING</th>
					<th style="width:40px">Ilmu</th>
				{else}
					<th style="width:40px">TPA</th>
					<th style="width:40px">ING</th>
					<th style="width:40px">WWC</th>
					<th style="width:40px">IPK</th>
					<th style="width:40px">KYI</th>
					<th style="width:40px">RKM</th>        
				{/if}
			{elseif $penerimaan.ID_JENJANG == 10}
				{if $penerimaan.NM_PENERIMAAN == 'PPDS'}
					<th style="width:40px">TPA</th>
					<th style="width:40px">ING</th>
					<th style="width:40px">PRODI</th>
				{else if $penerimaan.NM_PENERIMAAN == 'PPDGS'}
					<th style="width:40px">TPA</th>
					<th style="width:40px">ING</th>
					<th style="width:40px">WWC</th>
					<th style="width:40px">IPK</th>
					<th style="width:40px">Ilmu</th>
				{/if}
			{elseif $penerimaan.ID_JENJANG == 9}
				{if $smarty.get.id_program_studi == 83} {* PPDH *}
					<th style="width:40px">TPA</th>
					<th style="width:40px">ING</th>
					<th style="width:40px">WWC</th>
					<th style="width:40px">IPK</th>
				{else}
					<th style="width:40px">TPA</th>
					<th style="width:40px">ING</th>
					<th style="width:40px">WWC</th>
					<th style="width:40px">Ilmu</th>    
				{/if}
			{elseif $penerimaan.ID_JENJANG == 1 and $penerimaan.ID_JALUR == 4} {* S1-Alih Jenis *}
				<th style="width:40px">TPA</th>
				<th style="width:40px">Prestasi</th>
				<th style="width:40px">Akreditasi</th>
			{elseif $penerimaan.ID_JENJANG == 1 and $penerimaan.ID_JALUR == 20} {* S1-PBSB *}
				<th style="width:40px">TPA</th>
				<th style="width:40px">Prestasi</th>
				<th style="width:40px">Kesantrian</th>
			{/if}
		</tr>
	</thead>
	<tbody>
		{$nomer = 0}
		{foreach $calon_mahasiswa_baru_set as $cmb}
			{$nomer = $nomer + 1}
			<tr {if $cmb.TOTAL_NILAI < 60}
					class="kondisi-4"
				{else}
					class="kondisi-2"
				{/if}>
				<td class="center">{$nomer}</td>
				<td class="center">
					{if $penetapan.TGL_PENETAPAN == $tgl_sekarang}
					<input type="checkbox" name="cmb{$cmb.ID_C_MHS}" {if $cmb.TGL_DITERIMA != ''}checked="checked"{/if} />
					{else}
						{if $cmb.TGL_DITERIMA != ''}
							<input type="checkbox" checked="checked" disabled="disabled"/>
						{else}
							<input type="checkbox" disabled="disabled"/>
						{/if}
					{/if}
				</td>
				<td>{$cmb.NO_UJIAN}</td>
				<td>
					<a href="penetapan-sidang.php?mode=peserta&id_c_mhs={$cmb.ID_C_MHS}&id_penetapan_penerimaan={$smarty.get.id_penetapan_penerimaan}&id_program_studi={$smarty.get.id_program_studi}">{$cmb.NM_C_MHS}{if $cmb.GELAR != ''}, {$cmb.GELAR}{/if}</a>
				</td>
				{if $penerimaan.ID_JENJANG == 2 or $penerimaan.ID_JENJANG == 3}
					{if in_array($smarty.get.id_program_studi, array(52, 53, 54,55))}
						<td class="center">{$cmb.NILAI_TPA}</td>
						<td class="center">{$cmb.NILAI_INGGRIS}</td>
						<td class="center">{$cmb.NILAI_WAWANCARA}</td>
						<td class="center">{$cmb.NILAI_ILMU}</td>
						<td class="center">{$cmb.TOTAL_NILAI}</td>
					{elseif $smarty.get.id_program_studi == 137}
						<td class="center">{$cmb.NILAI_TPA}</td>
						<td class="center">{$cmb.NILAI_INGGRIS}</td>
						<td class="center">{$cmb.NILAI_WAWANCARA}</td>
						<td class="center">{$cmb.NILAI_ILMU}</td>
						<td class="center">{$cmb.NILAI_PSIKO}</td>
						<td class="center">{$cmb.TOTAL_NILAI}</td>
					{elseif $smarty.get.id_program_studi == 73}
						<td class="center">{$cmb.NILAI_TPA}</td>
						<td class="center">{$cmb.NILAI_INGGRIS}</td>
						<td class="center">{$cmb.NILAI_ILMU}</td>
						<td class="center">{$cmb.TOTAL_NILAI}</td>
					{else}
						<td class="center">{$cmb.NILAI_TPA}</td>
						<td class="center">{$cmb.NILAI_INGGRIS}</td>
						<td class="center">{$cmb.NILAI_WAWANCARA}</td>
						<td class="center">{$cmb.NILAI_IPK}</td>
						<td class="center">{$cmb.NILAI_KARYA_ILMIAH}</td>
						<td class="center">{$cmb.NILAI_REKOMENDASI}</td>
						<td class="center">{$cmb.TOTAL_NILAI}</td>
					{/if}
				{elseif $penerimaan.ID_JENJANG == 4} {* Jenjang Diploma 4 Alih Jenis *}
					<td class="center">{$cmb.TOTAL_NILAI}</td>
				{elseif $penerimaan.ID_JENJANG == 10}
					{if $penerimaan.NM_PENERIMAAN == 'PPDS'}
						<td class="center">{$cmb.NILAI_TPA}</td>
						<td class="center">{$cmb.NILAI_INGGRIS}</td>
						<td class="center">{$cmb.NILAI_GAB_PPDS}</td>
						<td class="center">{$cmb.TOTAL_NILAI}</td>
					{else if $penerimaan.NM_PENERIMAAN == 'PPDGS'}
						<td class="center">{$cmb.NILAI_TPA}</td>
						<td class="center">{$cmb.NILAI_INGGRIS}</td>
						<td class="center">{$cmb.NILAI_WAWANCARA}</td>
						<td class="center">{$cmb.NILAI_IPK}</td>
						<td class="center">{$cmb.NILAI_ILMU}</td>
						<td class="center">{$cmb.TOTAL_NILAI}</td>
					{/if}
				{elseif $penerimaan.ID_JENJANG == 9}
					{if $smarty.get.id_program_studi == 83}
						<td class="center">{$cmb.NILAI_TPA}</td>
						<td class="center">{$cmb.NILAI_INGGRIS}</td>
						<td class="center">{$cmb.NILAI_WAWANCARA}</td>
						<td class="center">{$cmb.NILAI_IPK}</td>
						<td class="center">{$cmb.TOTAL_NILAI}</td>
					{else}
						<td class="center">{$cmb.NILAI_TPA}</td>
						<td class="center">{$cmb.NILAI_INGGRIS}</td>
						<td class="center">{$cmb.NILAI_WAWANCARA}</td>
						<td class="center">{$cmb.NILAI_ILMU}</td>
						<td class="center">{$cmb.TOTAL_NILAI}</td>
					{/if}
				{elseif $penerimaan.ID_JENJANG == 1 and $penerimaan.ID_JALUR == 4} {* S1-Alih Jenis *}
					<td class="center">{$cmb.NILAI_TPA}</td>
					<td class="center">{$cmb.NILAI_PRESTASI}</td>
					<td class="center">{$cmb.NILAI_AKREDITASI}</td>
					<td class="center">{$cmb.TOTAL_NILAI}</td>
				{elseif $penerimaan.ID_JENJANG == 1 and $penerimaan.ID_JALUR == 20} {* S1-PBSB *}
					<td class="center">{number_format($cmb.NILAI_TPA,2)}</td>
					<td class="center">{number_format($cmb.NILAI_PRESTASI,2)}</td>
					<td class="center">{number_format($cmb.NILAI_KESANTRIAN,2)}</td>
					<td class="center">{number_format($cmb.TOTAL_NILAI,2)}</td>
				{/if}
			</tr>
	</tbody>
	{/foreach}
</table>

{if $penerimaan.ID_JENJANG == 2 or $penerimaan.ID_JENJANG == 3}
	{if in_array($smarty.get.id_program_studi, array(52, 53, 54, 55))}
		<p><!-- MIH MKN MHP -->
		Keterangan :<br/>
		TPA : Tes Potensi Akademik (nilai maksimal = 20)<br/>
		ING : Bahasa Inggris (nilai maksimal = 10)<br/>
		WWC : Wawancara (nilai maksimal = 20)<br/>
		Ilmu : Bidang Keilmuan (nilai maksimal = 50)
		</p>
	{elseif $smarty.get.id_program_studi == 137}
		<p><!-- MPro -->
		Keterangan :<br/>
		TPA : Tes Potensi Akademik (nilai maksimal = 20)<br/>
		ING : Bahasa Inggris (nilai maksimal = 10)<br/>
		WWC : Wawancara (nilai maksimal = 40)<br/>
		Ilmu : Bidang Keilmuan (nilai maksimal = 15)<br/>
		Psi : Psikotes (nilai maksimal = 15)
		</p>
	{elseif $smarty.get.id_program_studi == 73}
		<p><!-- Magister Manajemen -->
		Keterangan :<br/>
		TPA : Tes Potensi Akademik (nilai maksimal = 20)<br/>
		ING : Bahasa Inggris (nilai maksimal = 10)<br/>
		Ilmu : Bidang Keilmuan (nilai maksimal = 70)
		</p>
	{else}
		<p><!-- S2 dan S3 Umum -->
		Keterangan :<br/>
		TPA : Tes Potensi Akademik (nilai maksimal = 40)<br/>
		ING : Bahasa Inggris (nilai maksimal = 20)<br/>
		WWC : Wawancara (nilai maksimal = 20)<br/>
		IPK : IPK (nilai maksimal = 5)<br/>
		KYI : Karya Ilmiah (nilai maksimal = 10) <br/>
		RKM : Rekomendasi (nilai maksimal = 5)
		</p>
	{/if}
{elseif $penerimaan.ID_JENJANG == 10}
	{if $penerimaan.NM_PENERIMAAN == 'PPDS'}
		{* PPDS LAMA
		<p><!-- Spesialis Umum - PPDS -->
		Keterangan :<br/>
		TPA : Tes Potensi Akademik (nilai maksimal = 10)<br/>
		ING : Bahasa Inggris (nilai maksimal = 10)<br/>
		WWC : Wawancara (nilai maksimal = 40)<br/>
		Tulis : Ujian Tulis (nilai maksimal = 24)<br/>
		Psi : Psikotes (nilai maksimal = 8)<br/>
		Lain : Lain-Lain (nilai maksimal = 8)
		</p>*}
		<p><!-- Spesialis Umum - PPDS -->
		Keterangan :<br/>
		TPA : Tes Potensi Akademik (10%)<br/>
		ING : Bahasa Inggris (10%)<br/>
		PRODI: Jumlah nilai prodi (80%) = Wawancara + Ujian Tulis + Psikotes + Lain-Lain
		</p>
	{else if $penerimaan.NM_PENERIMAAN == 'PPDGS'}
		<p><!-- Spesialis Umum - PPDGS -->
		Keterangan :<br/>
		TPA : Tes Potensi Akademik (nilai maksimal = 20)<br/>
		ING : Bahasa Inggris (nilai maksimal = 10)<br/>
		WWC : Wawancara (nilai maksimal = 40)<br/>
		IPK : IPK (nilai maksimal = 10)<br/>
		Ilmu : Bidang Keilmuan (nilai maksimal = 20)
		</p>
	{/if}
{elseif $penerimaan.ID_JENJANG == 9}
	{if $smarty.get.id_program_studi == 83}
		<p><!-- Profesi Dokter Hewan -->
		Keterangan :<br/>
		TPA : Tes Potensi Akademik (nilai maksimal = 40)<br/>
		ING : Bahasa Inggris (nilai maksimal = 30)<br/>
		WWC : Wawancara (nilai maksimal = 25)<br/>
		IPK : IPK (nilai maksimal = 5)
		</p>
	{elseif $smarty.get.id_program_studi == 63}
		<p><!-- Profesi Akuntansi -->
		Keterangan :<br/>
		TPA : Tes Potensi Akademik (nilai maksimal = 20)<br/>
		ING : Bahasa Inggris (nilai maksimal = 10)<br/>
		WWC : Wawancara (nilai maksimal = 20)<br/>
		Ilmu : Bidang Keilmuan (nilai maksimal = 50)
		</p>
	{elseif $smarty.get.id_program_studi == 74}
		<p><!-- Profesi Apoteker -->
		Keterangan :<br/>
		TPA : Tes Potensi Akademik (nilai maksimal = 10)<br/>
		ING : Bahasa Inggris (nilai maksimal = 10)<br/>
		WWC : Wawancara (nilai maksimal = 30)<br/>
		Ilmu : Bidang Keilmuan (nilai maksimal = 50)
		</p>
	{/if}
{else if $penerimaan.ID_JENJANG == 1 and $penerimaan.ID_JALUR == 4}
	<p>
		Keterangan :<br/>
		TPA : Tes Potensi Akademik (nilai maksimal = 90)<br/>
		Prestasi : Tes Prestasi Akademik IPA/IPS (nilai maksimal = 90)<br/>
		Akreditasi : Nilai Akreditasi Prodi (nilai maksimal = 20)
	</p>
{else if $penerimaan.ID_JENJANG == 1 and $penerimaan.ID_JALUR == 20}
	<p>
		Keterangan :<br/>
		TPA : Tes Potensi Akademik (nilai maksimal = 45)<br/>
		Prestasi : Tes Prestasi Akademik IPA/IPS (nilai maksimal = 45)<br/>
		Kesantrian : Nilai Kesantrian dari Kemenag (nilai maksimal = 10)
	</p>
{/if}



<input type="hidden" name="id_program_studi" value="{$program_studi.ID_PROGRAM_STUDI}" />
<script type="text/javascript">
	$('input[name^="cmb"]').change(function() {
		var checked = this.checked;
		var id_c_mhs = this.name.replace('cmb', '');
		var id_program_studi = $('input[name="id_program_studi"]').val();
		$.ajax({
			url: 'penetapan-sidang-set.php',
			type: 'POST',
			data: 'checked='+checked+'&id_c_mhs='+id_c_mhs+'&id_program_studi='+id_program_studi,
			success: function(data) { 
				if (data === '0')
				{
					alert('Silahkan ulangi aksi, peserta gagal diupdate');
				}
			}
		});
	});
</script>
	
<a href="penetapan-sidang.php?mode=sidang&id_penetapan={$penetapan.ID_PENETAPAN}">Kembali</a>