<style type="text/css">
	ol {
		margin-left: 1em;
	}
</style>

<div class="center_title_bar">Detail Peserta</div>

{if $result}<script type="text/javascript">alert('{$result}');</script>{/if}
			
{if $cmb.ID_C_MHS != ''}
	<input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
	<table>
		<thead>
			<tr>
				<th colspan="3">Detail Peserta</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td rowspan="12" style="vertical-align: top">
					<img src="{$cmb.FILE_FOTO}" style="width: 200px" />
				</td>
				<td class="tebal">No Ujian</td>
				<td>{$cmb.NO_UJIAN}</td>
			</tr>
			<tr>
				<td class="tebal">Nama</td>
				<td>{$cmb.NM_C_MHS}</td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td>{$cmb.ALAMAT}</td>
			</tr>
			<tr>
				<td>Tanggal Lahir</td>
				<td>{$cmb.NM_KOTA_LAHIR}, {strftime('%d %B %Y', strtotime($cmb.TGL_LAHIR))}</td>
			</tr>

			{if $cmb.ID_PILIHAN_1}
				<tr>
					<td class="tebal">Pilihan 1</td>
					<td>{$cmb.NM_PILIHAN_1}</td>
				</tr>
			{/if}

			{if $cmb.ID_PILIHAN_2}
				<tr>
					<td class="tebal">Pilihan 2</td>
					<td>{$cmb.NM_PILIHAN_2}</td>
				</tr>
			{/if}

			{if $cmb.ID_PILIHAN_3}
				<tr>
					<td class="tebal">Pilihan 3</td>
					<td>{$cmb.NM_PILIHAN_3}</td>
				</tr>
			{/if}

			{if $cmb.ID_PILIHAN_4}
				<tr>
					<td class="tebal">Pilihan 4</td>
					<td>{$cmb.NM_PILIHAN_4}</td>
				</tr>
			{/if}

			{if $cmb.ID_JALUR == 4}
				<tr>
					<td>Perguruan Tinggi Asal</td>
					<td>{$cmb.PTN_S1}</td>
				</tr>
				<tr>
					<td>Prodi Asal</td>
					<td>{$cmb.PRODI_S1}</td>
				</tr>
				<tr>
					<td>Akreditasi Prodi</td>
					<td>{$cmb.JENIS_AKREDITASI_S1} - 
						{$cmb.PERINGKAT_AKREDITASI_S1}
					</td>
				</tr>
			{/if}

			{if $cmb.ID_JALUR == 3 or $cmb.ID_JALUR == 5 or $cmb.ID_JALUR == 27}
				<tr>
					<td>Sekolah Asal</td>
					<td>{$cmb.NM_SEKOLAH_ASAL}</td>
				</tr>
			{/if}
			
			<!--
			{if $cmb.KODE_JURUSAN == '01' or $cmb.KODE_JURUSAN == '03'}
			<tr>
				<td>Total Nilai {if $cmb.KODE_JURUSAN == '03'}IPA{/if}</td>
				<td>{$cmb.NILAI_TOTAL_IPA}</td>
			</tr>
			{/if}
			{if $cmb.KODE_JURUSAN == '02' or $cmb.KODE_JURUSAN == '03'}
			<tr>
				<td>Total Nilai {if $cmb.KODE_JURUSAN == '03'}IPS{/if}</td>
				<td>{$cmb.NILAI_TOTAL_IPS}</td>
			</tr>
			{/if}
			-->
			<tr>
				<td>Keterangan Ujian</td>
				<td>
					{if $cmb.STATUS_UJIAN == 0}Tidak Hadir{/if}
					{if $cmb.STATUS_UJIAN == 1}Hadir{/if}
					{if $cmb.STATUS_UJIAN == 2}Salah Identitas{/if}
					{if $cmb.STATUS_UJIAN == 3}Melakukan Kecurangan{/if}
					{if $cmb.STATUS_UJIAN == 4}Salah Kode Soal{/if}
				</td>
			</tr>
			<tr>
				<td>Berkas Upload</td>
				<td>
					
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3">
					Berkas Peserta
					<ol>
					{foreach $syarat_prodi_set as $sp}
						<li><a href="http://pendaftaran.unair.ac.id/files/{$cmb.ID_PENERIMAAN}/{$cmb.ID_C_MHS}/{$sp.FILE_SYARAT}" class="disable-ajax" target="_blank">{$sp.NM_SYARAT}</a></li>
					{/foreach}
					</ol>
				</td>
			</tr>
		</tfoot>
	</table>
{/if}

<!--<a href="penetapan-sidang.php?mode=detail&id_penetapan_penerimaan={$smarty.get.id_penetapan_penerimaan}&id_program_studi={$smarty.get.id_program_studi}">Kembali</a>-->
<span class="anchor-span" onclick="window.history.back(); return false;">Kembali</span>