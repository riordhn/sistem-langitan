<div class="center_title_bar">Jadwal Persidangan</div>

<form action="penetapan-sidang.php" method="get">
	<table>
		<tr>
			<td>Tahun</td>
			<td>
				<select name="tahun" onchange="$('form').submit();">
				<option value=""></option>
				{for $tahun=$tahun_set[0]['TAHUN_AWAL'] to $tahun_set[0]['TAHUN_AKHIR']}
					<option value="{$tahun}" 
						{if !empty($smarty.get.tahun)}
							{if $smarty.get.tahun == $tahun}selected{/if}
						{else}
							{if $tahun == date('Y')}selected{/if}
						{/if}
						>{$tahun}</option>
				{/for}
				</select>
			</td>
		</tr>
	</table>
</form>

<table style="width: 100%">
    <tr>
        <th style="width: 30px">No</th>
        <th>Persidangan Penetapan</th>
        <th style="width: 60px">Sidang Ke</th>
        <th style="width: 40px"></th>
    </tr>
    {foreach $penetapan_set as $p}
    <tr>
        <td class="center">{$p@index+1}</td>
        <td>{$p.NM_PENETAPAN}</td>
        <td class="center">{$p.PERIODE}</td>
        <td>
            <a href="penetapan-sidang.php?mode=sidang&id_penetapan={$p.ID_PENETAPAN}">Lihat</a>
        </td>
    </tr>
	{foreachelse}
	<tr>
		<td colspan="4">Belum ada data</td>
	</tr>
    {/foreach}
</table>