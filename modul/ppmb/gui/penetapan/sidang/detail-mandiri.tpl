<!--
	Modifikasi : 11 Juli 2013
	Keterangan : 
		# Menghilangkan tampilan Prestasi dan TPA, dengan menggabungkan nilai BL secara langsung
-->

<style>
	
	table > thead > tr > th { text-align: center; vertical-align: middle; }

	.content table.statistik tr td { padding: 2px; font-size: small; }
	.kondisi-1 td, .kondisi-1 td a { color: yellow; }
	.kondisi-2 td, .kondisi-2 td a { color: #000; }
	.kondisi-3 td, .kondisi-3 td a { color: #00f; }
	.kondisi-4 td, .kondisi-4 td a { color: #f00; }
	
	.tabel-peserta tr:hover td { background-color: #eaeaea; }
</style>

<div class="center_title_bar">{$penetapan.NM_PENETAPAN}</div>
			  
<table style="width: 50%; margin: 0px auto;">
	<tr>
		<td colspan="2" class="center">
		{if $penerimaan.ID_JENJANG == 1 or $penerimaan.ID_JENJANG == 5}
		<strong>{$penerimaan.NM_PENERIMAAN} TA {$penerimaan.TAHUN}/{$penerimaan.TAHUN+1}</strong>
		{else}
		<strong>{$penerimaan.NM_PENERIMAAN} {*Gelombang {$penerimaan.GELOMBANG}*} Semester {$penerimaan.SEMESTER} TA {$penerimaan.TAHUN}/{$penerimaan.TAHUN+1}</strong>
		{/if}
		</td>
	</tr>
	<tr>
		<td colspan="2" class="center">
			<strong>{$program_studi.NM_JENJANG} - {$program_studi.NM_PROGRAM_STUDI}</strong>
		</td>
	</tr>
	<tr>
		<td {if $statistik.NILAI_RATA2 == ''}style="display:none"{/if} style="vertical-align: top">
			<table style="width: 100%; margin: 0px auto;" class="statistik">
				<tr>
					<th colspan="2" style="background-color: yellow; color: #000;">Statistik Sebelumnya</th>
				</tr>
				<tr>
					<td>Nilai Min</td>
					<td class="center">{$statistik.NILAI_MIN2}</td>
				</tr>
				<tr>
					<td>Nilai Rata</td>
					<td class="center">{$statistik.NILAI_RATA2}</td>
				</tr>
				<tr>
					<td>Nilai Max</td>
					<td class="center">{$statistik.NILAI_MAX2}</td>
				</tr>
				<tr>
					<td>Standar Deviasi</td>
					<td class="center">{$statistik.STANDAR_DEVIASI2}</td>
				</tr>
				<tr>
					<td>Passing Grade</td>
					<td class="center">{$statistik.PASSING_GRADE2}</td>
				</tr>
				
				<tr style="display: none;">
					<td>Kuota</td>
					<td class="center">{$statistik.KUOTA2}</td>
				</tr>
				<tr style="display: none">
					<td>Verivikasi</td>
					<td class="center">{$statistik.PEMINAT2}</td>
				</tr>
			</table>
		</td>
		<td>
			<table style="width: 100%; margin: 0px auto;" class="statistik">
				<tr>
					<th colspan="2">Statistik</th>
				</tr>
				<tr>
					<td>Nilai Min</td>
					<td class="center">{$statistik.NILAI_MIN}</td>
				</tr>
				<tr>
					<td>Nilai Rata</td>
					<td class="center">{$statistik.NILAI_RATA}</td>
				</tr>
				<tr>
					<td>Nilai Max</td>
					<td class="center">{$statistik.NILAI_MAX}</td>
				</tr>
				<tr>
					<td>Standar Deviasi</td>
					<td class="center">{$statistik.STANDAR_DEVIASI}</td>
				</tr>
				<tr>
					<td>Passing Grade</td>
					<td class="center">{$statistik.PASSING_GRADE}</td>
				</tr>
				
				<tr>
					<td>Kuota</td>
					<td class="center">{$statistik.KUOTA}</td>
				</tr>
				<tr style="">
					<td>Jumlah Peserta</td>
					<td class="center">{count($cmb_set)}</td>
				</tr>
				<!--<tr>
					<td>Kuota Total</td>
					<td class="center">{$statistik.KUOTA_TOTAL}</td>
				</tr>-->
				<!--
				<tr>
					<td>Verifikasi</td>
					<td class="center">{$statistik.PEMINAT}</td>
				</tr>
				-->
				<tr>
					<td>Pilihan</td>
					<td class="center">
						<form action="penetapan-sidang.php" method="get" id="filter">
						<input type="hidden" name="mode" value="detail" />
						<input type="hidden" name="id_program_studi" value="{$smarty.get.id_program_studi}" /> 
						<input type="hidden" name="id_penetapan_penerimaan" value="{$smarty.get.id_penetapan_penerimaan}" />
						<select name="pilihan" onchange="$('#filter').submit(); return false;">
							<option value="1" {if $smarty.get.pilihan == 1}selected="selected"{/if}>Pilihan 1</option>
							<option value="2" {if $smarty.get.pilihan == 2}selected="selected"{/if}>Pilihan 2</option>
							<option value="3" {if $smarty.get.pilihan == 3}selected="selected"{/if}>Pilihan 3</option>
							<option value="4" {if $smarty.get.pilihan == 4}selected="selected"{/if}>Pilihan 4</option>
						</select>
						</form>
					</td>
				</tr>   
			</table>
		</td>
	</tr>
</table>
				
<a href="penetapan-sidang.php?mode=sidang&id_penetapan={$penetapan.ID_PENETAPAN}">Kembali</a>

<table style="width: 100%" class="tabel-peserta">
	<thead>
		<tr>
			<th rowspan="2">No</th>
			<th rowspan="2">Lulus</th>
			<th rowspan="2">No Ujian</th>
			<th rowspan="2" style="width: auto">Nama Peserta</th>
			{if $penerimaan.ID_JALUR == 3 or $penerimaan.ID_JALUR == 5}{* Mandiri S1/D3 *}
				<th colspan="2">Nilai</th>
			{else if $penerimaan.ID_JALUR == 27}{* FK-Int *}
				<th colspan="2">Nilai</th>
			{else if $penerimaan.ID_JALUR == 20}{* PBSB *}
				<th colspan="3">Nilai</th>
			{/if}
			<th rowspan="2">Nilai<br/>Akhir</th>
		</tr>
		<tr>
			{if $penerimaan.ID_JALUR == 3 or $penerimaan.ID_JALUR == 5}{* Mandiri S1/D3 *}
				<th>TPA</th>
				<th>Prestasi</th>
			{else if $penerimaan.ID_JALUR == 27}{* FK-Int *}
				<th>TPA</th>
				<th>Prestasi</th>
			{else if $penerimaan.ID_JALUR == 20}{* PBSB *}
				<th>TPA</th>
				<th>Prestasi</th>
				<th>Kesantrian</th>
			{/if}
		</tr>
	</thead>
	<tbody>
		{foreach $cmb_set as $cmb}
			<tr {if $cmb.TOTAL_NILAI < ($statistik.NILAI_RATA - ($statistik.STANDAR_DEVIASI*2))}
					class="kondisi-1"
				{else}
					{if $cmb.TOTAL_NILAI < $statistik.PASSING_GRADE}
						class="kondisi-4"
					{else}
						{if ($cmb@index + 1) <= $statistik.KUOTA}
							class="kondisi-2"
						{else}
							class="kondisi-3"
						{/if}
					{/if}
				{/if}>
				<td class="center" style="color: black">{$cmb@index + 1}</td>
				<td class="center" style="font-weight: bold">
					<!--
							{if $cmb.POSISI_DITERIMA == ''}
								<input type="checkbox" checked="checked" disabled="disabled"/>
							{else}
								{$cmb.POSISI_DITERIMA}
							{/if}
					-->
					{if $penetapan.TGL_PENETAPAN == $tgl_sekarang}
						{if $cmb.TGL_DITERIMA != ''}
							{if $cmb.ID_PROGRAM_STUDI == $smarty.get.id_program_studi}
								<input type="checkbox" name="cmb{$cmb.ID_C_MHS}" checked="checked" />
							{else}
								<span class="info-diterima" title="{$cmb.NM_PILIHAN_DITERIMA}">Sudah Diterima di Prodi Lain</span>
							{/if}
						{else}
							<input type="checkbox" name="cmb{$cmb.ID_C_MHS}" />
						{/if}
						
					{else}
						{if $cmb.TGL_DITERIMA != ''}
							{if $cmb.ID_PROGRAM_STUDI == $smarty.get.id_program_studi}
								<input type="checkbox" name="cmb{$cmb.ID_C_MHS}" checked disabled/>
							{else}
								<span class="info-diterima" title="{$cmb.NM_PILIHAN_DITERIMA}">Sudah Diterima di Prodi Lain</span>
							{/if}
						{else}
							<input type="checkbox" disabled="disabled"/>
						{/if}
					{/if}
				</td>
				<td {if $cmb.KODE_JURUSAN == '03'}data-jurusan="ipc" title="{$cmb.NM_PILIHAN_1}<br/>{$cmb.NM_PILIHAN_2}<br/>{$cmb.NM_PILIHAN_3}<br/>{$cmb.NM_PILIHAN_4}"{/if}>
					{$cmb.NO_UJIAN}
					{if $cmb.KODE_JURUSAN == '03'}(IPC){/if}
				</td>
				<td><a href="penetapan-sidang.php?mode=peserta&id_c_mhs={$cmb.ID_C_MHS}&id_penetapan_penerimaan={$smarty.get.id_penetapan_penerimaan}&id_program_studi={$smarty.get.id_program_studi}">{$cmb.NM_C_MHS}</a></td>
				{if $penerimaan.ID_JALUR == 3 or $penerimaan.ID_JALUR == 5}
					<td class="center">{number_format($cmb.NILAI_TPA,2)}</td>
					<td class="center">{number_format($cmb.NILAI_PRESTASI,2)}</td>
					<td class="center">{number_format($cmb.TOTAL_NILAI,2)}</td>
				{else if $penerimaan.ID_JALUR == 27}
					<td class="center">{number_format($cmb.NILAI_TPA,2)}</td>
					<td class="center">{number_format($cmb.NILAI_PRESTASI,2)}</td>
					<td class="center">{number_format($cmb.TOTAL_NILAI,2)}</td>
				{else if $penerimaan.ID_JALUR == 20}
					<td class="center">{number_format($cmb.NILAI_TPA,2)}</td>
					<td class="center">{number_format($cmb.NILAI_PRESTASI,2)}</td>
					<td class="center">{number_format($cmb.NILAI_KESANTRIAN,2)}</td>
					<td class="center">{number_format($cmb.TOTAL_NILAI,2)}</td>
				{/if}
			</tr>
		{/foreach}
	</tbody>
</table>

{if $penerimaan.ID_JALUR == 20}
	<p>
		Keterangan :<br/>
		TPA : Tes Potensi Akademik (nilai maksimal = 90)<br/>
		Prestasi : Tes Prestasi Akademik IPA/IPS (nilai maksimal = 90)<br/>
		Kesantrian : Nilai Kesantrian dari Kemenag (nilai maksimal = 20)
	</p>
{/if}

<input type="hidden" name="id_program_studi" value="{$program_studi.ID_PROGRAM_STUDI}" />

<script type="text/javascript">
	jQuery(document).ready(function() {
		
		$('input[name^="cmb"]').change(function() {
			var checked = this.checked;
			var id_c_mhs = this.name.replace('cmb', '');
			var id_program_studi = $('input[name="id_program_studi"]').val();
			
			$.ajax({
				url: 'penetapan-sidang-set.php',
				type: 'POST',
				data: 'checked='+checked+'&id_c_mhs='+id_c_mhs+'&id_program_studi='+id_program_studi,
				success: function(data) { 
					if (data === '0')
					{
						alert('Silahkan ulangi aksi, peserta gagal diupdate');
					}
				}
			});
		});
		
		$('td[data-jurusan="ipc"]').tooltip({ 
			track: false,
			show: false,
			hide: false
		});
		
		$('.info-diterima').tooltip({
			show: false,
			hide: false
		});
		
	});
</script>

<a href="penetapan-sidang.php?mode=sidang&id_penetapan={$penetapan.ID_PENETAPAN}">Kembali</a>