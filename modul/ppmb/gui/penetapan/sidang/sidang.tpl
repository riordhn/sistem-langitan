<div class="center_title_bar">{$penetapan.NM_PENETAPAN}</div>

{foreach $penerimaan_set as $p}
<table style="width: 90%">
    <caption>{$p.NM_PENERIMAAN} Gelombang {$p.GELOMBANG} Semester {$p.SEMESTER} Tahun {$p.TAHUN}/{$p.TAHUN+1}</caption>
    <tr>
        <th style="width: 30px">No</th>
        <th>Program Studi</th>
        <th style="width: 40px">Peserta</th>
        <th style="width: 40px">Kuota</th>
        <th style="width: 40px">Diterima</th>
        <th style="width: 40px"></th>
    </tr>
    {$total_peminat = 0}
    {$total_kuota = 0}
    {$total_diterima = 0}
    {foreach $p.program_studi_set as $ps}
    <tr>
        <td class="center">{$ps@index + 1}</td>
        <td>{$ps.NM_PROGRAM_STUDI}</td>
        <td class="center">{$ps.JUMLAH_PEMINAT}</td>
        <td class="center">{$ps.JUMLAH_KUOTA}</td>
        <td class="center">{$ps.JUMLAH_DITERIMA}</td>
        <td>
            <a href="penetapan-sidang.php?mode=detail&id_penetapan_penerimaan={$p.ID_PENETAPAN_PENERIMAAN}&id_program_studi={$ps.ID_PROGRAM_STUDI}">Lihat</a>
        </td>
    </tr>
    {$total_peminat = $total_peminat + $ps.JUMLAH_PEMINAT}
    {$total_kuota = $total_kuota + $ps.JUMLAH_KUOTA}
    {$total_diterima = $total_diterima + $ps.JUMLAH_DITERIMA}
    {/foreach}
    <tr>
        <td></td>
        <td><strong>TOTAL</strong></td>
        <td class="center"><strong>{$total_peminat}</strong></td>
        <td class="center"><strong>{$total_kuota}</strong></td>
        <td class="center"><strong>{$total_diterima}</strong></td>
        <td></td>
    </tr>
</table>
{/foreach}

<a href="penetapan-sidang.php">Kembali</a>