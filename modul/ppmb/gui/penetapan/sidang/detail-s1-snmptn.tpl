<div class="center_title_bar">{$penetapan.NM_PENETAPAN}</div>

<style>.content table tr td a { color: #000; text-decoration: none; }</style>

<h3>{$program_studi.NM_JENJANG} - {$program_studi.NM_PROGRAM_STUDI}</h3>

<table>
    <tr>
        <th>No</th>
        <th>No Ujian</th>
        <th>Nama</th>
        <th>Skor</th>
        <th>Asal Sekolah</th>
    </tr>
    {foreach $cmb_set as $cmb}
    <tr>
        <td class="center">{$cmb@index + 1}</td>
        <td><a href="penetapan-sidang.php?mode=peserta&id_c_mhs={$cmb.ID_C_MHS}&id_penetapan_penerimaan={$smarty.get.id_penetapan_penerimaan}&id_program_studi={$smarty.get.id_program_studi}">{$cmb.NO_UJIAN}</a></td>
        <td>{$cmb.NM_C_MHS}</td>
        <td class="center">{number_format($cmb.NILAI_TOTAL,2)}</td>
        <td>{$cmb.NM_SEKOLAH}</td>
    </tr>
    {/foreach}
</table>