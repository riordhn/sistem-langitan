<div class="center_title_bar">Pencarian Peserta</div>

<form action="penetapan-peserta2.php" method="get">
    <table>
        <tr>
            <td>Nama</td>
            <td>
                <input type="search" name="q" value="{$smarty.get.q}"/>
                <input type="submit" value="Cari" />
            </td>
        </tr>
    </table>
</form>
				
{if $data_set}
	<table>
		<tr>
			<th>No</th>
			<th>No Ujian</th>
			<th>Nama</th>
			<th>Pilihan</th>
			<th>Penerimaan</th>
		</tr>
		{foreach $data_set as $data}
		<tr {if $data@index is not div by 2}class="row1"{/if}>
			<td>{$data@index + 1}</td>
			<td>{$data.NO_UJIAN}</td>
			<td>{$data.NM_C_MHS}</td>
			<td>{$data.NM_JENJANG} {$data.NM_PROGRAM_STUDI}</td>
			<td>{$data.NM_PENERIMAAN} {$data.SEMESTER} {$data.GELOMBANG} Tahun {$data.TAHUN}</td>
		</tr>
		{/foreach}
	</table>
{/if}