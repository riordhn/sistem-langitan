<div class="center_title_bar">Master Penetapan - Edit</div>

{if $edited}<h2>{$edited}</h2>{/if}

<form action="penetapan-master.php?{$smarty.server.QUERY_STRING}" method="post">
    <input type="hidden" name="id_penetapan" value="{$smarty.get.id_penetapan}" />
    <input type="hidden" name="mode" value="edit" />
    <table>
        <tr>
            <th colspan="2">Detail Info Penetapan</th>
        </tr>
        <tr>
            <td>Nama Penetapan</td>
            <td><input type="text" name="nm_penetapan" size="60" maxlength="100" value="{$penetapan.NM_PENETAPAN}" /></td>
        </tr>
        <tr>
            <td>No Surat</td>
            <td><input type="text" name="no_surat" size="20" maxlength="20" value="{$penetapan.NO_SURAT}" /></td>
        </tr>
        <tr>
            <td>Baris 1</td>
            <td><input type="text" name="baris_1" size="80" maxlength="100" value="{$penetapan.BARIS_1}" /></td>
        </tr>
        <tr>
            <td>Baris 2</td>
            <td><input type="text" name="baris_2" size="80" maxlength="100" value="{$penetapan.BARIS_2}" /></td>
        </tr>
        <tr>
            <td>Baris 3</td>
            <td><input type="text" name="baris_3" size="80" maxlength="100" value="{$penetapan.BARIS_3}" /></td>
        </tr>
        <tr>
            <td>Baris 4</td>
            <td><input type="text" name="baris_4" size="80" maxlength="100" value="{$penetapan.BARIS_4}" /></td>
        </tr>
        <tr>
            <td>Tanggal Penetapan</td>
            <td>
                {html_select_date prefix="tgl_penetapan_" field_order="DMY" time=$penetapan.TGL_PENETAPAN}
            </td>
        </tr>
        <tr>
            <td>Sidang Ke</td>
            <td><input type="text" name="periode" size="3" maxlength="2" value="{$penetapan.PERIODE}" /></td>
        </tr>
        <tr>
            <td>Baris Halaman 1</td>
            <td><input type="text" name="row_page_1" size="2" maxlength="2" value="{$penetapan.ROW_PAGE_1}" /> (default: 19)</td>
        </tr>
        <tr>
            <td>Baris Halaman ke-N</td>
            <td><input type="text" name="row_page_n" size="2" maxlength="2" value="{$penetapan.ROW_PAGE_N}" /> (default: 25)</td>
        </tr>
        <tr>
            <td>Lebar Kolom Nama</td>
            <td><input type="text" name="width_nama" size="4" maxlength="4" value="{$penetapan.WIDTH_NAMA}" /> (default: 230)</td>
        </tr>
        <tr>
            <td>Lebar Kolom Prodi</td>
            <td><input type="text" name="width_prodi" size="4" maxlength="4" value="{$penetapan.WIDTH_PRODI}" /> (default: 200)</td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <a href="penetapan-master.php">Kembali</a>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>