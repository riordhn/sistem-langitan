<div class="center_title_bar">Master Penetapan - Hapus</div>

<h2>{$penetapan.NM_PENETAPAN}</h2>

{if !empty($penetapan_penerimaan_set)}
<table>
    <tr>
        <th>Tahun</th>
        <th>Jalur</th>
        <th>Jenjang</th>
        <th>Penerimaan</th>
        <th>Gel</th>
        <th>Semester</th>
        <th>Status</th>
    </tr>
    {foreach $penetapan_penerimaan_set as $pp}
    <tr>
        <td class="center">{$pp.TAHUN}</td>
        <td>{$pp.NM_JALUR}</td>
        <td class="center">{$pp.NM_JENJANG}</td>
        <td>{$pp.NM_PENERIMAAN}</td>
        <td class="center">{$pp.GELOMBANG}</td>
        <td class="center">{$pp.SEMESTER}</td>
        <td class="center">{if $pp.IS_AKTIF}Aktif{else}Tidak Aktif{/if}</td>
    </tr>
    {/foreach}
</table>
    <h3>Sidang penetapan tidak bisa dihapus sebelum penerimaan dihapus.</h3>
    <a href="penetapan-master.php">Batal</a>
{else}
    <h3>Apakah sidang penetapan ini akan dihapus ?</h3>
    <form action="penetapan-master.php" method="post">
        <input type="hidden" name="mode" value="delete" />
        <input type="hidden" name="id_penetapan" value="{$penetapan.ID_PENETAPAN}" />
        <a href="penetapan-master.php">Batal</a>
        <input type="submit" value="Hapus" />
    </form>
{/if}