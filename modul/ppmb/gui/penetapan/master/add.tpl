<div class="center_title_bar">Master Penetapan - Tambah</div>

<form action="penetapan-master.php" method="post">
    <input type="hidden" name="mode" value="add" />
    <table>
        <tr>
            <th colspan="2">Detail Info Penetapan</th>
        </tr>
        <tr>
            <td>Nama Penetapan</td>
            <td><input type="text" name="nm_penetapan" size="60" maxlength="100" /></td>
        </tr>
        <tr>
            <td>No Surat</td>
            <td><input type="text" name="no_surat" size="20" maxlength="20" /></td>
        </tr>
        <tr>
            <td>Baris 1</td>
            <td><input type="text" name="baris_1" size="80" maxlength="100" /></td>
        </tr>
        <tr>
            <td>Baris 2</td>
            <td><input type="text" name="baris_2" size="80" maxlength="100" /></td>
        </tr>
        <tr>
            <td>Baris 3</td>
            <td><input type="text" name="baris_3" size="80" maxlength="100" /></td>
        </tr>
        <tr>
            <td>Baris 4</td>
            <td><input type="text" name="baris_4" size="80" maxlength="100" /></td>
        </tr>
        <tr>
            <td>Tanggal Penetapan</td>
            <td>
                {html_select_date prefix='tgl_penetapan_' field_order='DMY'}
            </td>
        </tr>
        <tr>
            <td>Sidang Ke</td>
            <td><input type="text" name="periode" size="3" maxlength="2" /></td>
        </tr>
        <tr>
            <td>Baris Halaman 1</td>
            <td><input type="text" name="row_page_1" size="2" maxlength="2" /> (default: 19)</td>
        </tr>
        <tr>
            <td>Baris Halaman ke-N</td>
            <td><input type="text" name="row_page_n" size="2" maxlength="2" /> (default: 25)</td>
        </tr>
        <tr>
            <td>Lebar Kolom Nama</td>
            <td><input type="text" name="width_nama" size="4" maxlength="4" /> (default: 230)</td>
        </tr>
        <tr>
            <td>Lebar Kolom Prodi</td>
            <td><input type="text" name="width_prodi" size="4" maxlength="4" /> (default: 200)</td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <a class="button" href="penetapan-master.php">Kembali</a>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>