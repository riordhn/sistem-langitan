<div class="center_title_bar">{$penetapan.NM_PENETAPAN}</div>

<h2>Penambahan Penerimaan pada sidang penetapan</h2>

<form action="penetapan-master.php?mode=view-penerimaan&id_penetapan={$smarty.get.id_penetapan}" method="post">
    <input type="hidden" name="mode" value="add-penerimaan" />
    <input type="hidden" name="id_penetapan" value="{$penetapan.ID_PENETAPAN}" />
    <table>
        <tr>
            <td>Penerimaan</td>
            <td><select name="id_penerimaan">
                {foreach $penerimaan_set as $p}
                    <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                    {foreach $p.p_set as $p2}
                    <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN}</option>
                    {/foreach}
                    </optgroup>
                {/foreach}
                </select>
            </td>
            <td>
                <input type="submit" value="Tambah" />
            </td>
        </tr>
    </table>
</form>
                
<a href="penetapan-master.php?mode=view-penerimaan&id_penetapan={$penetapan.ID_PENETAPAN}">Kembali</a>