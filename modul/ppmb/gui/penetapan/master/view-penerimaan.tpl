<div class="center_title_bar">{$penetapan.NM_PENETAPAN}</div>

{if $deleted}<h2>{$deleted}</h2>{/if}
{if $added}<h2>{$added}</h2>{/if}

<table>
    <tr>
        <th>Tahun</th>
        <th>Jalur</th>
        <th>Jenjang</th>
        <th>Penerimaan</th>
        <th>Gel</th>
        <th>Semester</th>
        <th>Status</th>
        <th></th>
    </tr>
    {foreach $penetapan_penerimaan_set as $pp}
    <tr>
        <td class="center">{$pp.TAHUN}</td>
        <td>{$pp.NM_JALUR}</td>
        <td class="center">{$pp.NM_JENJANG}</td>
        <td>{$pp.NM_PENERIMAAN}</td>
        <td class="center">{$pp.GELOMBANG}</td>
        <td class="center">{$pp.SEMESTER}</td>
        <td class="center">{if $pp.IS_AKTIF}Aktif{else}Tidak Aktif{/if}</td>
        <td>
            <form action="penetapan-master.php?{$smarty.server.QUERY_STRING}" method="post">
                <input type="hidden" name="id_penetapan_penerimaan" value="{$pp.ID_PENETAPAN_PENERIMAAN}" />
                <input type="hidden" name="mode" value="del-penerimaan" />
                <input type="submit" value="Hapus" />
            </form>
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="8" class="center">
         <a href="penetapan-master.php?mode=add-penerimaan&id_penetapan={$penetapan.ID_PENETAPAN}">Tambah Penerimaan</a>
        </td>
    </tr>
</table>

<a href="penetapan-master.php">Kembali</a>