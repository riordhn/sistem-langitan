<div class="center_title_bar">Master Penetapan</div>

{if $deleted}<h2>{$deleted}</h2>{/if}

<table style="width: 100%">
    <tr>
        <th style="width: 30px">No</th>
        <th>Persidangan Penetapan</th>
        <th style="width: 60px">Sidang Ke</th>
        <th style="width: 160px"></th>
    </tr>
    {foreach $penetapan_set as $p}
    <tr>
        <td class="center">{$p@index+1}</td>
        <td>{$p.NM_PENETAPAN}</td>
        <td class="center">{$p.PERIODE}</td>
        <td>
            <a href="penetapan-master.php?mode=edit&id_penetapan={$p.ID_PENETAPAN}">Edit</a> |
            <a href="penetapan-master.php?mode=delete&id_penetapan={$p.ID_PENETAPAN}">Hapus</a> |
            <a href="penetapan-master.php?mode=view-penerimaan&id_penetapan={$p.ID_PENETAPAN}">Penerimaan</a>
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="4">
            <a href="penetapan-master.php?mode=add">Tambah</a>
        </td>
    </tr>
</table>