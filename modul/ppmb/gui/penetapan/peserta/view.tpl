<div class="center_title_bar">Pencarian Peserta</div>

<form action="penetapan-peserta.php" method="get">
    <table>
		<tbody>
			<tr>
				<td>Persidangan</td>
				<td>
					<select name="id_penetapan">
						<option value="">--</option>
						{foreach $penetapan_set as $p}
							<option value="{$p.ID_PENETAPAN}" {if !empty($smarty.get.id_penetapan)}{if $p.ID_PENETAPAN == $smarty.get.id_penetapan}selected{/if}{/if}>{$p.NM_PENETAPAN}</option>
						{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td>Nama / Nomer</td>
				<td>
					<input type="search" name="q" value="{$smarty.get.q}"/>
					<input type="submit" value="Cari" />
				</td>
			</tr>
		</tbody>
        
    </table>
</form>
                
{if $data_set}
    <table>
        <tr>
            <th>No</th>
            <th>No Ujian</th>
            <th>Nama</th>
            <th>Program Studi Pilihan</th>
			<th>Nilai</th>
			<th>Urutan</th>
        </tr>
        {foreach $data_set as $d}
        <tr {if $d@index is not div by 2}class="row1"{/if}>
            <td class="center">{$d@index + 1}</td>
            <td>{$d.NO_UJIAN}</td>
            <td>{$d.NM_C_MHS}</td>
            <td>{$d.NM_PILIHAN}</td>
			<td class="center">{$d.TOTAL_NILAI}</td>
			<td class="center">{$d.URUTAN}</td>
        </tr>
        {/foreach}
    </table>
{/if}