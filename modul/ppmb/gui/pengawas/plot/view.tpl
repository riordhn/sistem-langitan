<div class="center_title_bar">Plot Pengawas</div>

<form action="pengawas-plot.php" method="get" id="filter">
    <table class="filter" border="1">
        <tr>
            <td>Ujian</td>
            <td>
                <select name="id_ujian" onchange="$('#filter').submit(); return false;">
                    <option value="">-- Pilih Aktifitas Ujian --</option>
                {foreach $ujian_set as $u}
                    <option value="{$u.ID_UJIAN}" {if $u.ID_UJIAN == $smarty.get.id_ujian}selected="selected"{/if}>
                        {$u.NM_UJIAN} Gelombang {$u.GELOMBANG} Semester {$u.SEMESTER} Tahun {$u.TAHUN}
                    </option>
                {/foreach}
                </select>
            </td>
        </tr>
    </table>
</form>
                
<table>
    <tr>
        <th>Jumlah Pengawas</th>
        <th>Jumlah Kepala Ruang</th>
    </tr>
    <tr>
        <td class="center">{$pengawas.JUMLAH_PENGAWAS}</td>
        <td class="center">{$pengawas.JUMLAH_KR}</td>
    </tr>
</table>
                              
<table>
    <tr>
        <th>No</th>
        <th>Nomer Ujian</th>
        <th>Kelompok</th>
        <th>Lokasi</th>
        <th style="width: 300px">Pengawas</th>
        <th style="width: 150px">Koordinator</th>
    </tr>
    {foreach $jadwal_set as $j}
        <tr {if $j@index is not div by 2}class="row1"{/if}>
            <td class="center">{$j@index + 1}</td>
            <td class="center">{$j.NOMER_AWAL}<br/>s.d.<br/>{$j.NOMER_AKHIR}</td>
            <td class="center">{$j.NM_JALUR}<br/>{$j.NM_JURUSAN}</td>
            <td>{$j.LOKASI}<br/>Ruang: {$j.NM_RUANG}<br/>Kapasitas: {$j.KAPASITAS}</td>
            <td>
                {foreach $j.pengawas_ruang_set as $p}
                    <div>[{$p.SINGKATAN_JABATAN}] {$p.NM_PENGAWAS} ({$p.NM_ASAL}/{$p.JABATAN}) <span class="anchor-span" action="unset-pengawas" id-jadwal-ppmb="{$j.ID_JADWAL_PPMB}" id-pengawas="{$p.ID_PENGAWAS}">x</span></div>
                {/foreach}
                <span class="anchor-span" action="set-pengawas" id-jadwal-ppmb={$j.ID_JADWAL_PPMB}>+ Pengawas</span>
            </td>
            <td>
                {if $j.NM_KOORDINATOR != ''}
                    <div>{$j.NM_KOORDINATOR} <span class="anchor-span" action="unset-koordinator" id-jadwal-ppmb="{$j.ID_JADWAL_PPMB}">x</span></div>
                {else}
                    <div><span class="anchor-span" action="set-koordinator" id-jadwal-ppmb="{$j.ID_JADWAL_PPMB}">Set Koordinator</span></div>
                {/if}
            </td>
        </tr>
    {/foreach}
</table>

<script>
    $('span[action="set-koordinator"]').live('click', function() {

        var span = this;
        var id_jadwal_ppmb = $(this).attr('id-jadwal-ppmb');
        var id_ujian = $('select[name="id_ujian"]').val();
        
        console.log(id_ujian);
        
        if ($('#d').length == 0) { $('#content').append('<div id="d"></div>'); }

        $('#d').html('<div style="text-align: center"><img src="/img/ppmb/ajax-loading.gif"/></div>');
        $('#d').load('pengawas-plot.php?mode=set-koordinator&id_jadwal_ppmb='+id_jadwal_ppmb+'&id_ujian='+id_ujian);

        $('#d').dialog({
            modal: true,
            title: 'Set koordinator',
            width: 400,
            buttons: {
                1: { text: 'Set', click: function() {
                    $.post('pengawas-plot.php', $('#koordinator-form').serialize(), function(r) {
                        $(span).parent().html(r + ' <span class="anchor-span" action="unset-koordinator" id-jadwal-ppmb="'+id_jadwal_ppmb+'">x</span>');
                    });
                    $('#d').dialog('destroy');
                }},
                2: { text: 'Batal', click: function() {
                    $('#d').dialog('destroy');
                }}
            }
        });

    });

    $('span[action="unset-koordinator"]').live('click', function() {

        var span = this;
        var id_jadwal_ppmb = $(this).attr('id-jadwal-ppmb');
       
        if ($('#d').length == 0) { $('#content').append('<div id="d"></div>'); }

        $('#d').html('<p>Apakah koordinator ini akan di batalkan plot ?</p>');

        $('#d').dialog({
            modal: true,
            title: 'Konfirmasi',
            buttons: {
                1: { text: 'Unplot', click: function() {
                    $.post('pengawas-plot.php', 'mode=unset-koordinator&id_jadwal_ppmb=' + id_jadwal_ppmb, function(r) {
                        if (r == 1)
                            $(span).parent().html('<span class="anchor-span" action="set-koordinator" id-jadwal-ppmb="'+id_jadwal_ppmb+'">Set Koordinator</span>');
                    });
                    $('#d').dialog('destroy');
                    $('#d').remove();
                }},
                2: { text: 'Batal', click: function() {
                    $('#d').dialog('destroy');
                    $('#d').remove();
                }}
            }
        });

    });

    $('span[action="unset-pengawas"]').live('click', function() {

        var span = this;
        var id_pengawas = $(this).attr('id-pengawas');
        var id_jadwal_ppmb = $(this).attr('id-jadwal-ppmb');
        
        if ($('#d').length == 0) { $('#content').append('<div id="d"></div>'); }

        $('#d').html('<p>Apakah pengawas ini akan dihapus ?</p>');

        $('#d').dialog({
            modal: true,
            title: 'Konfirmasi',
            buttons: {
                1: { text: 'Hapus', click: function() {
                    $.post('pengawas-plot.php', 'mode=unset-pengawas&id_jadwal_ppmb='+id_jadwal_ppmb+'&id_pengawas='+id_pengawas,
                        function(r) { 
                            if (r == 1) { $(span).parent().remove(); }
                        });
                    $('#d').dialog('destroy');
                    $('#d').remove();
                }},
                2: { text: 'Batal', click: function() {
                    $('#d').dialog('destroy');
                    $('#d').remove();
                }}
            }
        });
    });

    $('span[action="set-pengawas"]').live('click', function() { 

        var span = this;
        var id_jadwal_ppmb = $(this).attr('id-jadwal-ppmb');
        
        if ($('#d').length == 0) { $('#content').append('<div id="d"></div>'); }

        $('#d').html('<div style="text-align: center"><img src="/img/ppmb/ajax-loading.gif"/></div>');
        $('#d').load('pengawas-plot.php?mode=set-pengawas&id_jadwal_ppmb='+id_jadwal_ppmb);

        $('#d').dialog({
            modal: true,
            title: 'Tambah Pengawas',
            width: 450,
            buttons: {
                1: { text: 'Tambah', click: function() {
                    $.post('pengawas-plot.php', $('#pengawas-form').serialize(), function(data) {
                        $(span).before('<div>['+data.SINGKATAN_JABATAN+'] '+data.NM_PENGAWAS+' ('+data.NM_ASAL+'/'+data.JABATAN+') <span class="anchor-span" action="unset-pengawas" id-jadwal-ppmb="'+data.ID_JADWAL_PPMB+'" id-pengawas="'+data.ID_PENGAWAS+'">x</span></div>');
                    }, 'json');
                    $('#d').dialog('destroy');
                    $('#d').remove();
                }},
                2: { text: 'Batal', click: function() {
                    $('#d').dialog('destroy');
                    $('#d').remove();
                }}
            }
        });
    });
</script>