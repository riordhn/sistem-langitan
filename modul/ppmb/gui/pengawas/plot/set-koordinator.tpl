<form action="pengawas-plot.php" method="post" id="koordinator-form">
    <input type="hidden" name="id_jadwal_ppmb" value="{$smarty.get.id_jadwal_ppmb}" />
    <input type="hidden" name="id_ujian" value="{$smarty.get.id_ujian}" />
    <input type="hidden" name="mode" value="set-koordinator" />
    <table>
        <tr>
            <td>Asal Pengawas</td>
            <td>
                <select name="id_asal">
                {foreach $asal_pengawas_set as $a}
                    <option value="{$a.ID_ASAL}">{$a.NM_ASAL}</option>
                {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Nama Koordinator</td>
            <td>
                <select name="id_pengawas">
                {foreach $pengawas_set as $p}
                    <option value="{$p.ID_PENGAWAS}">{$p.NM_ASAL} - {$p.NM_PENGAWAS} ({$p.GOLONGAN_PENGAWAS})</option>
                {/foreach}
                </select>
            </td>
        </tr>
    </table>
</form>
                
<script>
    $('select[name="id_asal"]').change(function() { 
        
        var id_jadwal_ppmb = $('input[name="id_jadwal_ppmb"]').val();
        var id_asal = $(this).val();
        
        $.ajax({
            type: 'GET',
            url : 'pengawas-plot.php',
            data: 'mode=get-pengawas&id_asal='+id_asal,
            dataType: 'json',
            beforeSend: function() {
                $('select[name="id_pengawas"]').html('');
            },
            success: function(data) {
                $.each(data, function(i,item) {
                    $('select[name="id_pengawas"]').append('<option value="'+item.ID_PENGAWAS+'">'+item.NM_PENGAWAS+'</option>');
                });
            }
        });
    });
</script>