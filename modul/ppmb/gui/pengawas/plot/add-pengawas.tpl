<div class="center_title_bar">Plot Pengawas - Tambah Pengawas</div>

<p>Kelompok : {$jadwal.NM_JALUR} / {$jadwal.NM_JURUSAN}<br/>
Nomer Ujian : {$jadwal.NOMER_AWAL} - {$jadwal.NOMER_AKHIR}<br/>
Ruang : {$jadwal.NM_RUANG}<br/>
Lokasi : {$jadwal.LOKASI}</p>

<form action="pengawas-plot.php?id_ujian={$smarty.get.id_ujian}" method="post" autocomplete="off">
    <input type="hidden" name="mode" value="add-pengawas" />
    <input type="hidden" name="id_jadwal_ppmb" value="{$smarty.get.id_jadwal_ppmb}" />
    <table>
        <tr>
            <th colspan="2">Keterangan Pengawas</th>
        </tr>
        <tr>
            <td>Nama Pengawas</td>
            <td>
                <input type="text" name="nm_pengawas" size=30 maxlength=50 />
            </td>
        </tr>
        <tr>
            <td>Golongan</td>
            <td>
                <select name="golongan">
                    <option value="IV">IV</option>
                    <option value="III">III</option>
                    <option value="II">II</option>
                    <option value="I">I</option>
                    <option value="H">Honorer</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Jabatan</td>
            <td>
                <select name="id_jabatan">
                    <option value="1">Pengawas</option>
                    <option value="2">Kepala Ruang</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Asal</td>
            <td>
                <select name="id_asal">
                {foreach $asal_pengawas_set as $a}
                    <option value="{$a.ID_ASAL}">{$a.NM_ASAL}</option>
                {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <a href="pengawas-plot.php?id_ujian={$smarty.get.id_ujian}">Kembali</a>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
    
    
    