<form action="pengawas-plot.php" method="post" id="pengawas-form">
    <input type="hidden" name="id_jadwal_ppmb" value="{$smarty.get.id_jadwal_ppmb}" />
    <input type="hidden" name="mode" value="set-pengawas" />
    <table>
        <tr>
            <td>Asal Pengawas</td>
            <td>
                <select name="id_asal">
                {foreach $asal_pengawas_set as $a}
                    <option value="{$a.ID_ASAL}">{$a.NM_ASAL}</option>
                {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Nama Pengawas</td>
            <td>
                <select name="id_pengawas"></select>
            </td>
        </tr>
        <tr>
            <td>Jabatan</td>
            <td>
                <select name="id_jabatan">
                    <option value="">-- Pilih Jabatan --</option>
                    <option value="1">Pengawas</option>
                    <option value="2">Kepala Ruang</option>
                </select>
            </td>
        </tr>
    </table>
</form>

<script>
    $('select[name="id_asal"]').change(function() { 
        
        var id_jadwal_ppmb = $('input[name="id_jadwal_ppmb"]').val();
        var id_asal = $(this).val();
        
        $.ajax({
            type: 'GET',
            url : 'pengawas-plot.php',
            data: 'mode=get-pengawas-ready&id_jadwal_ppmb='+id_jadwal_ppmb+'&id_asal='+id_asal,
            dataType: 'json',
            beforeSend: function() {
                $('select[name="id_pengawas"]').html('');
            },
            success: function(data) {
            	console.log(data);
                $.each(data, function(i,item) {
                    $('select[name="id_pengawas"]').append('<option value="'+item.ID_PENGAWAS+'">'+item.NM_PENGAWAS+'</option>');
                });
            }
        });
    });
</script>