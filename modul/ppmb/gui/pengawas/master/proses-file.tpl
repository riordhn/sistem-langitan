<div class="center_title_bar">Master Pengawas &gt; Upload File</div>

<table>
    <tr>
        <th colspan="2">Hasil</th>
    </tr>
    <tr>
        <td>Jumlah Ditambah</td>
        <td>{$result.add}</td>
    </tr>
    <tr>
        <td>Jumlah Diupdate</td>
        <td>{$result.update}</td>
    </tr>
    <tr>
        <td>Gagal</td>
        <td>{$result.error}</td>
    </tr>
    {if $result.error > 0}
    <tr>
        <th colspan="2">Daftar Gagal</th>
    </tr>
    <tr>
        <td colspan="2">
            <table>
                {foreach $result.error_set as $e}
                <tr>
                    <td>{$e[0]}</td>
                    <td>{$e[1]}</td>
                    <td>{$e[2]}</td>
                    <td>{$e[3]}</td>
                    <td>{$e[4]}</td>
                    <td>{$e[5]}</td>
                    <td>{$e[6]}</td>
                </tr>
                {/foreach}
            </table>
        </td>
    </tr>
    {/if}
    <tr>
        <td colspan="2"><a href="pengawas-master.php">Kembali</a></td>
    </tr>
</table>