<div class="center_title_bar">Master Pengawas &gt; Edit</div>

<form action="pengawas-master.php" method="post" autocomplete="off">
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="id_pengawas" value="{$smarty.get.id_pengawas}" />
    <table>
        <tr>
            <th colspan="2">Detail Pengawas</th>
        </tr>
        <tr>
            <td>NIP / NIK</td>
            <td>
                <input type="text" name="nip_pengawas" size="20" maxlength="20" value="{$pengawas.NIP_PENGAWAS}" />
            </td>
        </tr>
        <tr>
            <td>Nama Pengawas</td>
            <td><input type="text" name="nm_pengawas" size="30" maxlength="50" value="{$pengawas.NM_PENGAWAS}"/></td>
        </tr>
        <tr>
            <td>Golongan</td>
            <td>
                <select name="golongan_pengawas">
                    <option value="" > -- </option>
                    <option value="IV" {if $pengawas.GOLONGAN_PENGAWAS == 'IV'}selected="selected"{/if}>IV</option>
                    <option value="III" {if $pengawas.GOLONGAN_PENGAWAS == 'III'}selected="selected"{/if}>III</option>
                    <option value="IIIa" {if $pengawas.GOLONGAN_PENGAWAS == 'IIIa'}selected="selected"{/if}>IIIa</option>
                    <option value="II" {if $pengawas.GOLONGAN_PENGAWAS == 'II'}selected="selected"{/if}>II</option>
                    <option value="I" {if $pengawas.GOLONGAN_PENGAWAS == 'I'}selected="selected"{/if}>I</option>
                    <option value="H" {if $pengawas.GOLONGAN_PENGAWAS == 'H'}selected="selected"{/if}>Honorer</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Asal</td>
            <td>
                <select name="id_asal">
                    <option value=""> -- Pilih Kantor Asal -- </option>
                {foreach $asal_pengawas_set as $a}
                    <option value="{$a.ID_ASAL}" {if $pengawas.ID_ASAL == $a.ID_ASAL}selected="selected"{/if}>{$a.NM_ASAL}</option>
                {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Jabatan</td>
            <td>
                <select name="jabatan">
                    <option value=""> -- Pilih Jabatan -- </option>
                    <option value="D" {if $pengawas.JABATAN == 'D'}selected="selected"{/if}>Dosen</option>
                    <option value="TK" {if $pengawas.JABATAN == 'TK'}selected="selected"{/if}>Tenaga Kepegawaian</option>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <a href="pengawas-master.php">Kembali</a>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
