<div class="center_title_bar">Master Pengawas &gt; Upload</div>

<form action="pengawas-master.php?mode=proses-file" method="post">
    <input type="hidden" name="mode" value="proses-file" />
    <input type="hidden" name="token" value="{$token}" />
    <table>
        <tr>
            <th colspan="2">Upload File Excel</th>
        </tr>
        <tr>
            <td>File</td>
            <td>
                <iframe src="pengawas-master.php?mode=upload-file&token={$token}" style="height: 26px; width: 400px; overflow: hidden" scrolling="no"></iframe>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <a href="pengawas-master.php">Kembali</a>
                <input type="submit" value="Proses" />
            </td>
        </tr>
    </table>
</form>