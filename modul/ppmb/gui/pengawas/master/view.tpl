<div class="center_title_bar">Master Pengawas</div>

{if $result}
<div class="ui-widget" id="information">
    <div style="margin: 20px 0px; padding: 3px;" class="ui-state-highlight ui-corner-all"> 
        <span style="float: left; margin-right: .3em;" class="ui-icon ui-icon-info"></span>
        <strong>{$result}</strong>
        <span style="float: right; cursor: pointer" class="ui-icon ui-icon-close" onclick="$('#information').remove();">abc</span>
    </div>
</div>
{/if}

<table>
    <tr>
        <th>No</th>
        <th>NIP/NIK</th>
        <th>Nama</th>
        <th>Golongan</th>
        <th>Asal</th>
        <th>Jabatan</th>
        <th>Aksi</th>
    </tr>
    {foreach $pengawas_set as $p}
    <tr>
        <td class="center">{$p@index + 1}</td>
        <td>{$p.NIP_PENGAWAS}</td>
        <td>{$p.NM_PENGAWAS}</td>
        <td class="center">{$p.GOLONGAN_PENGAWAS}</td>
        <td>{$p.NM_ASAL}</td>
        <td class="center">{$p.JABATAN}</td>
        <td>
            <a href="pengawas-master.php?mode=edit&id_pengawas={$p.ID_PENGAWAS}">Edit</a>
            <span class="anchor-span" action="delete" id-pengawas="{$p.ID_PENGAWAS}">Hapus</span><br/>
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="7" class="center">
            <a href="pengawas-master.php?mode=add">Tambah Pengawas</a> |
            <a href="pengawas-master.php?mode=upload">Upload</a>
        </td>
    </tr>
</table>
    
<script type="text/javascript">
    $('span[action="delete"]').each(function(i, e) {
        $(this).click(function() {
            
            var span = this;
            var id_pengawas = $(this).attr('id-pengawas');
            
            var x = document.createElement('div');
            
            $(x).html('<p>Apakah pengawas ini akan di hapus ?</p>');
            $(x).dialog({
                title: 'Konfirmasi penghapusan pengawas',
                modal: true,
                buttons: {
                    1: {
                        text: 'Batal',
                        click: function() { $(x).dialog('destroy'); }
                    },
                    2: {
                        text: 'Ya',
                        click: function() {
                            $.ajax({
                                type: 'POST',
                                url: 'pengawas-master.php',
                                data: 'mode=delete&id_pengawas='+id_pengawas,
                                success: function(r) {
                                    if (r == 1)
                                        $(span).parent().parent().remove();
                                    else
                                        alert('Pengawas gagal dihapus');
                                    $(x).dialog('destroy');
                                }
                            });
                        }
                    }
                }
            });
            
        });
    });
</script>