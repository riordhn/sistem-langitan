<div class="center_title_bar">Master Pengawas &gt; Tambah</div>

<form action="pengawas-master.php" method="post" autocomplete="off">
    <input type="hidden" name="mode" value="add" />
    <table>
        <tr>
            <th colspan="2">Detail Pengawas</th>
        </tr>
        <tr>
            <td>NIP / NIK</td>
            <td>
                <input type="text" name="nip_pengawas" size="20" maxlength="20" />
            </td>
        </tr>
        <tr>
            <td>Nama Pengawas</td>
            <td><input type="text" name="nm_pengawas" size="30" maxlength="50" /></td>
        </tr>
        <tr>
            <td>Golongan</td>
            <td>
                <select name="golongan_pengawas">
                    <option value=""> -- </option>
                    <option value="IV">IV</option>
                    <option value="III">III</option>
                    <option value="IIIa">IIIa</option>
                    <option value="II">II</option>
                    <option value="I">I</option>
                    <option value="H">Honorer</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Asal</td>
            <td>
                <select name="id_asal">
                    <option value=""> -- Pilih Kantor Asal -- </option>
                {foreach $asal_pengawas_set as $a}
                    <option value="{$a.ID_ASAL}">{$a.NM_ASAL}</option>
                {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Jabatan</td>
            <td>
                <select name="jabatan">
                    <option value=""> -- Pilih Jabatan -- </option>
                    <option value="D">Dosen</option>
                    <option value="TK">Tenaga Kepegawaian</option>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <a href="pengawas-master.php">Kembali</a>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
