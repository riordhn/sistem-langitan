<div class="center_title_bar">Hasil Upload Absensi</div>
<table>
    <tr>
        <th>No</th>
        <th>No Ujian</th>
        <th>Status</th>
    </tr>
    {foreach $result_set as $r}
    <tr {if $r@index is not div by 2}class="row1"{/if}>
        <td class="center">{$r@index + 1}</td>
        <td>{$r[0]}</td>
        <td {if $r[2] == 0}style="background-color: #808080"{/if}
            {if $r[2] == 2}style="background-color: #ffff00"{/if}
            {if $r[2] == 3}style="background-color: #ff8080"{/if}
            {if $r[2] == 4}style="background-color: #ffff80"{/if}>{$r[1]}</td>
    </tr>
    {/foreach}
</table>