<div class="center_title_bar">Master Honorarium</div>

{if $result}<h2>{$result}</h2>{/if}

<form action="pengawas-honorarium.php" method="get" id="filter">
    <table class="filter" border="1">
        <tr>
            <td>Ujian</td>
            <td>
                <select name="id_ujian" onchange="$('#filter').submit(); return false;">
                    <option value="">-- Pilih Aktifitas Ujian --</option>
                {foreach $ujian_set as $u}
                    <option value="{$u.ID_UJIAN}" {if $u.ID_UJIAN == $smarty.get.id_ujian}selected="selected"{/if}>
                        {$u.NM_UJIAN} Gelombang {$u.GELOMBANG} Semester {$u.SEMESTER} Tahun {$u.TAHUN}
                    </option>
                {/foreach}
                </select>
            </td>
        </tr>
    </table>
</form>

{if $smarty.get.id_ujian}
<table>
    <tr>
        <th>No</th>
        <th>Jabatan</th>
        <th>Golongan</th>
        <th>Kelompok</th>
        <th>Besar Honor</th>
        <th>Pajak</th>
        <th>Aksi</th>
    </tr>
    {foreach $honor_set as $h}
    <tr {if $h@index is not div by 2}class="row1"{/if}>
        <td class="center">{$h@index + 1}</td>
        <td>{$h.NM_JABATAN}</td>
        <td class="center">{$h.GOLONGAN}</td>
        <td>{$h.KODE_JURUSAN}</td>
        <td style="text-align: right">{number_format($h.BESAR_HONOR)}</td>
        <td class="center">{$h.PAJAK}%</td>
        <td><span class="anchor-span" action="delete" id-honor={$h.ID_HONOR}>Hapus</span></td>
    </tr>
    {/foreach}
    <tr>
        <td class="center" colspan="7">
            <a href="pengawas-honorarium.php?mode=add&id_ujian={$smarty.get.id_ujian}">Tambah Jenis Honorarium</a>
        </td>
    </tr>
</table>
{/if}

<script>
    $('span[action="delete"]').each(function(i, e) {
        $(this).click(function() {
            var button = this;
            var id_honor = $(this).attr('id-honor');
            
            var x = document.createElement('div');
            
            $(x).html('<p>Apakah jenis honor ini akan di hapus ?</p>');
            $(x).dialog({
                title: 'Konfirmasi penghapusan jenis honor',
                buttons: {
                    1: {
                        text: 'Batal',
                        click: function() { $(x).dialog('destroy'); }
                    },
                    2: {
                        text: 'Ya',
                        click: function() {
                            $.ajax({
                                type: 'POST',
                                url: 'pengawas-honorarium.php',
                                data: 'mode=delete&id_honor='+id_honor,
                                success: function(r) {
                                    if (r == 1)
                                        $(button).parent().parent().remove();
                                    else
                                        alert('Jenis honor gagal dihapus');
                                    $(x).dialog('destroy');
                                }
                            });
                        }
                    }
                }
            });
            
        });
    });
</script>