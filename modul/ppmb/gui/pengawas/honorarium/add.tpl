<div class="center_title_bar">Master Honorarium - Tambah</div>

<form action="pengawas-honorarium.php?id_ujian={$smarty.get.id_ujian}" method="post" autocomplete="off">
    <input type="hidden" name="mode" value="add" />
    <input type="hidden" name="id_ujian" value="{$smarty.get.id_ujian}" />
    <table>
        <tr>
            <th colspan="2">Tambah Jenis Honor</th>
        </tr>
        <tr>
            <td>Jabatan</td>
            <td>
                <select name="id_jabatan">
                    <option value=""> -- </option>
                {foreach $jabatan_set as $j}
                    <option value="{$j.ID_JABATAN}">{$j.NM_JABATAN}</option>
                {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Golongan</td>
            <td>
                <label><input type="checkbox" name="golongan[]" value="IV" />IV</label><br/>
                <label><input type="checkbox" name="golongan[]" value="III" />III</label><br/>
                <label><input type="checkbox" name="golongan[]" value="IIIa" />IIIa</label><br/>
                <label><input type="checkbox" name="golongan[]" value="II" />II</label><br/>
                <label><input type="checkbox" name="golongan[]" value="I" />I</label><br/>
                <label><input type="checkbox" name="golongan[]" value="H" />Honorer</label>
                <!--
                <select name="golongan">
                    <option value=""> -- </option>
                    <option value="IV">IV</option>
                    <option value="III">III</option>
                    <option value="II">II</option>
                    <option value="I">I</option>
                    <option value="H">Honorer</option>
                </select>-->
            </td>
        </tr>
        <tr>
            <td>Kelompok</td>
            <td>
                <label><input type="checkbox" name="kode_jurusan[]" value="1" />IPA/Reguler</label><br/>
                <label><input type="checkbox" name="kode_jurusan[]" value="2" />IPS</label><br/>
                <label><input type="checkbox" name="kode_jurusan[]" value="3" />IPC</label>
                <!--
                <select name="kode_jurusan">
                    <option value="">--</option>
                    <option value="1">IPA/Reguler</option>
                    <option value="2">IPS</option>
                    <option value="3">IPC</option>
                </select>
                -->
            </td>
        </tr>
        <tr>
            <td>Besar Honor</td>
            <td><input type="text" name="besar_honor" size="12" maxlength="12" /></td>
        </tr>
        <tr>
            <td>Pajak</td>
            <td>
                <select name="pajak">
                    <option value=""> -- </option>
                    <option value="0">0 %</option>
                    <option value="5">5 %</option>
                    <option value="15">15 %</option>
                </select>
            </td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <a href="pengawas-honorarium.php?id_ujian={$smarty.get.id_ujian}">Kembali</a> 
                <input type="submit" value="OK"/>
            </td>
        </tr>
    </table>
</form>
                
<script>
    $('form').validate({
        rules: {
            id_jabatan: { required: true },
            golongan:   { required: true },
            kode_jurusan:   { required: true },
            besar_honor:{ required: true, number: true },
            pajak:      { required: true}
        },
        messages: {
            id_jabatan: { required: 'Pilih salah satu' },
            golongan:   { required: 'Pilih salah satu' },
            kode_jurusan:   { required: 'Pilih salah satu' },
            besar_honor:{ required: 'Tidak boleh kosong', number: 'Isi dengan angka' },
            pajak:      { required: 'Pilih salah satu' }
        }
    });
</script>