<div class="center_title_bar">Cetak Plot Pengawas</div>

<form action="pengawas-cetak.php" method="get" id="filter">
<table>
    <tr>
        <th colspan="2">Pengaturan</th>
    </tr>
    <tr>
        <td>Ujian</td>
        <td>
            <select name="id_ujian" onchange="$('#filter').submit(); return false;">
                <option value="">-- Pilih Aktifitas Ujian --</option>
            {foreach $ujian_set as $u}
                <option value="{$u.ID_UJIAN}" {if $u.ID_UJIAN == $smarty.get.id_ujian}selected="selected"{/if}>
                    {$u.NM_UJIAN} Gelombang {$u.GELOMBANG} Semester {$u.SEMESTER} Tahun {$u.TAHUN}
                </option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Jenis Cetak</td>
        <td>
            <select name="cetak" onchange="$('#filter').submit(); return false;">
                <option value="">-- Pilih Jenis Cetak --</option>
                <option value="1" {if $smarty.get.cetak == '1'}selected="selected"{/if}>Master Plot</option>
                <option value="2" {if $smarty.get.cetak == '2'}selected="selected"{/if}>Daftar Pembayaran Honorarium</option>
                <option value="3" {if $smarty.get.cetak == '3'}selected="selected"{/if}>Daftar Hadir per Asal Pengawas</option>
                <option value="4" {if $smarty.get.cetak == '4'}selected="selected"{/if}>Daftar Pengawas (untuk Mail Merge)</option>
                <option value="5" {if $smarty.get.cetak == '5'}selected="selected"{/if}>Rayonisasi Pengawas</option>
            </select>
        </td>
    </tr>
    {if !empty($smarty.get.id_ujian) && !empty($smarty.get.cetak)}
    <tr>
        <td colspan="2" class="center">
            <button onclick="window.open('pengawas-cetak.php?mode=excel&{$smarty.server.QUERY_STRING}'); return false;">Cetak</button>
        </td>
    </tr>
    {/if}
</table>
</form>