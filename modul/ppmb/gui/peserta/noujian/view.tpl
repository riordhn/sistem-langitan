<div class="center_title_bar">Ambil No Ujian</div>

<table>
	<tr>
		<td>
			<label>Kode Voucher :</label>
		</td>
		<td>
			<form action="peserta-noujian.php" method="get">
			<input type="search" name="kode_voucher" {if !empty($smarty.get.kode_voucher)}value="{$smarty.get.kode_voucher}"{/if} />
			<input type="submit" value="Cari..." />
			</form>
		</td>
	</tr>
</table>

{if empty($cmb) == false}

<table style="width: auto">
	<tr>
		<th colspan="2">Info Peserta</th>
	</tr>
	<tr>
		<td>Kode Voucher</td>
		<td>{$cmb.KODE_VOUCHER}</td>
	</tr>
	<tr>
		<td>Nama</td>
		<td>{$cmb.NM_C_MHS}</td>
	</tr>
	<tr>
		<td>No Ujian</td>
		<td>
			{* memastikan login manual *}
			{if $cmb.ID_CALON_PENDAFTAR == ''}
				{if $cmb.NO_UJIAN == ''}
					<button id="btn_ambil" data-kode-voucher="{$cmb.KODE_VOUCHER}">Ambil No Ujian</button>
				{else}
					{$cmb.NO_UJIAN}<br/>
					<button id="btn_batal" data-kode-voucher="{$cmb.KODE_VOUCHER}">Batalkan No Ujian</button>
				{/if}
			{else}
				{$cmb.NO_UJIAN}
			{/if}
		</td>
	</tr>
</table>

<script type="text/javascript">
	$(document).ready(function() {
		$('#btn_ambil').click(function(e) {
			e.preventDefault();
			var kode_voucher = $(this).data('kode-voucher');
			$.ajax({
				async: false,
				crossDomain: true,
				url: 'http://pendaftaran.unair.ac.id/index.php/api/set_no_ujian/' + kode_voucher,
				success: function(r) {
					if (r === 'COMPLETE' || r === 'SUDAH_AMBIL')
					{
						location.reload();
					}
				}
			});
		});

		$('#btn_batal').click(function(e) {
			e.preventDefault();
			var kode_voucher = $(this).data('kode-voucher');
			$.ajax({
				async: false,
				crossDomain: true,
				url: 'http://pendaftaran.unair.ac.id/index.php/api/unset_no_ujian/' + kode_voucher,
				success: function(r) {
					if (r === 'COMPLETE' || r === 'SUDAH_AMBIL')
					{
						location.reload();
					}
				}
			});
		});
	});
</script>
{/if}