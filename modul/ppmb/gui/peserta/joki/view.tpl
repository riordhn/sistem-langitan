<div class="center_title_bar">Pengecekan Joki</div>

<form action="peserta-joki.php" method="get" id="filter">
<table>
	<tr>
		<td>Penerimaan</td>
		<td>
			<select name="id_penerimaan" onchange="$('#filter').submit()">
				<option value="">-- Pilih Penerimaan --</option>
			{foreach $penerimaan_set as $p}
				<optgroup label="{$p.TAHUN} {$p.SEMESTER}">
				{foreach $p.p_set as $p2}
				<option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN}</option>
				{/foreach}
				</optgroup>
			{/foreach}
			</select>
		</td>
	</tr>
</table>
</form>

{if isset($cmb_set)}
	<table>
		<thead>
			<tr>
				<th>#</th>
				<th>No Ujian</th>
				<th>Nama</th>
				<th>Tgl Lahir</th>
				<th>Jurusan</th>
				<th>Lokasi Ujian / Koordinator</th>
				<th>Pilihan Prodi</th>
			</tr>
		</thead>
		<tbody>
			{foreach $cmb_set as $cmb}
				<tr>
					<td>{$cmb@index + 1}</td>
					<td>{$cmb.NO_UJIAN}</td>
					<td>{$cmb.NM_C_MHS}</td>
					<td>{$cmb.TGL_LAHIR}</td>
					<td>
						{if $cmb.KODE_JURUSAN == '01'} IPA
						{else if $cmb.KODE_JURUSAN == '02'} IPS
						{else if $cmb.KODE_JURUSAN == '03'} IPC
						{/if}
					</td>
					<td>
						{$cmb.NM_RUANG}<br/>
						{$cmb.LOKASI}<br/>
						{$cmb.NM_PENGAWAS}
					</td>
					<td>
						<ul>
							<li>{$cmb.NM_PROGRAM_STUDI_1}</li>
							{if $cmb.NM_PROGRAM_STUDI_2 != ''}<li>{$cmb.NM_PROGRAM_STUDI_2}</li>{/if}
							{if $cmb.NM_PROGRAM_STUDI_3 != ''}<li>{$cmb.NM_PROGRAM_STUDI_3}</li>{/if}
							{if $cmb.NM_PROGRAM_STUDI_4 != ''}<li>{$cmb.NM_PROGRAM_STUDI_4}</li>{/if}
						</ul>
					</td>
				</tr>
			{foreachelse}
				<tr>
					<td colspan="7"></td>
				</tr>
			{/foreach}
		</tbody>
	</table>
{/if}