<div class="center_title_bar">Cek Nomor Formulir</div>

<table>
	<tr>
		<td>
			<label>Nomor Formulir :</label>
		</td>
		<td>
			<form action="peserta-voucher.php" method="get">
			<input type="search" name="kode_voucher" {if isset($smarty.get.kode_voucher)}value="{$smarty.get.kode_voucher}"{/if} />
			<input type="submit" value="Cari..." name="cari" />
			</form>
		</td>
	</tr>
</table>
			
{if !empty($voucher)}
	{if $voucher.TGL_BAYAR != ''}
	<form action="peserta-voucher.php" method="get">
	<table>

		<tr>
			<th colspan="2">Detail Voucher</th>
		</tr>
		<tr>
			<td>Kode Voucher</td>
			<td>{$voucher.KODE_VOUCHER}</td>
		</tr>
		<tr>
			<td>Pin Voucher</td>
			<td>{if $voucher.TGL_BAYAR != ''}{$voucher.PIN_VOUCHER}{else}Voucher belum dibayar{/if}</td>
		</tr>
		 <tr>
			<td>Tanggal Pengambilan</td>
			<td>{$voucher.TGL_AMBIL}</td>
		</tr>
		<tr>
			<td>Jenis Voucher</td>
			<td>
				{if $voucher.KODE_JURUSAN == '01'}IPA
				{else if $voucher.KODE_JURUSAN == '02'}IPS
				{else if $voucher.KODE_JURUSAN == '03'}IPC{/if}
			</td>
		</tr>
		<tr>
			<td>Tanggal Pembelian</td>
			<td>{$voucher.TGL_BAYAR}</td>
		</tr>
		<tr>
			<td>Bank</td>
			<td>{$voucher.NM_BANK}</td>
		</tr>
		<tr>
			<td>Via</td>
			<td>{$voucher.NAMA_BANK_VIA}</td>
		</tr>
		<tr>
			<td>Nama Calon Mahasiswa</td>
			<td>{$voucher.NM_C_MHS|capitalize}</td>
		</tr>

		{if empty($is_tunai)}
			<tr>
				<td>DPP Semester 1</td>
				<td>
					<input type="hidden" name="kode_voucher" value="{$voucher.KODE_VOUCHER}" />
					Rp <input type="text" id="bayar1" name="bayar1" value="0" style="text-align:right;" />
				</td>
			</tr>
			<tr>
				<td>DPP Semester 2</td>
				<td>
					Rp <input type="text" id="bayar2" name="bayar2" value="0" style="text-align:right;" />
				</td>
			</tr>
			<tr>
				<td>Pasak, Atribut, Buku Pedoman</td>
				<td>
					Rp <input type="text" id="bayar3" name="bayar3" value="0" style="text-align:right;" />
				</td>
			</tr>
			<tr>
				<td>Her Registrasi Semester 1</td>
				<td>
					Rp <input type="text" id="bayar4" name="bayar4" value="0" style="text-align:right;" />
				</td>
			</tr>
			<tr>
				<td>IKM Semester 1</td>
				<td>
					Rp <input type="text" id="bayar5" name="bayar5" value="0" style="text-align:right;" />
				</td>
			</tr>
			<tr>
				<td>SPP Bulan September {$voucher.TAHUN}</td>
				<td>
					Rp <input type="text" id="bayar6" name="bayar6" value="0" style="text-align:right;" />
				</td>
			</tr>
			<tr>
				<td>UTS Semester 1</td>
				<td>
					Rp <input type="text" id="bayar7" name="bayar7" value="0" style="text-align:right;" />
				</td>
			</tr>
			<tr>
				<td>UAS Semester 1</td>
				<td>
					Rp <input type="text" id="bayar8" name="bayar8" value="0" style="text-align:right;" />
				</td>
			</tr>
			<tr>
				<td>Total Pembayaran</td>
				<td>
					Rp <input type="text" id="total_bayar" name="total_bayar" value="0" style="text-align:right;" readonly />
				</td>
			</tr>
			<tr>
				<td>No. Kwitansi</td>
				<td>
					<input type="text" name="no_kwitansi" />
				</td>
			</tr>
			<tr>
				<td>Tunai/Transfer</td>
				<td>
					<select name="is_tunai" required>
					  <option value="1">Tunai</option>
					  <option value="2">Transfer</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Tgl Transfer (Diisi Apabila Transfer)</td>
				<td>
					<input type="text" name="tgl_transfer" />
				</td>
			</tr>
			<tr>
				<td>Ukuran Jas</td>
				<td>
					<input type="text" name="ukuran_jas" />
				</td>
			</tr>
			<tr>
				<td>Tgl Tes Tulis & Psikologi</td>
				<td>
					<input type="text" name="tgl_tes_tulis" />
				</td>
			</tr>
			<tr>
				<td class="center" colspan="2">
					<input type="submit" value="Input Value Kwitansi" name="input" />
				</td>
			</tr>
		{else}
			<tr>
				<td>DPP Semester 1</td>
				<td>
					Rp {$bayar1}
				</td>
			</tr>
			<tr>
				<td>DPP Semester 2</td>
				<td>
					Rp {$bayar2}
				</td>
			</tr>
			<tr>
				<td>Pasak, Atribut, Buku Pedoman</td>
				<td>
					Rp {$bayar3}
				</td>
			</tr>
			<tr>
				<td>Her Registrasi Semester 1</td>
				<td>
					Rp {$bayar4}
				</td>
			</tr>
			<tr>
				<td>IKM Semester 1</td>
				<td>
					Rp {$bayar5}
				</td>
			</tr>
			<tr>
				<td>SPP Bulan September {$voucher.TAHUN}</td>
				<td>
					Rp {$bayar6}
				</td>
			</tr>
			<tr>
				<td>UTS Semester 1</td>
				<td>
					Rp {$bayar7}
				</td>
			</tr>
			<tr>
				<td>UAS Semester 1</td>
				<td>
					Rp {$bayar8}
				</td>
			</tr>
			<tr>
				<td>Total Pembayaran</td>
				<td>
					Rp {$total_bayar}
				</td>
			</tr>
			<tr>
				<td>Terbilang</td>
				<td>
					{$terbilang}
				</td>
			</tr>
			<tr>
				<td>No. Kwitansi</td>
				<td>
					{$no_kwitansi}
				</td>
			</tr>
			<tr>
				<td>Tunai/Transfer</td>
				<td>
					{if $is_tunai == 1}
						Tunai
					{else}
						Transfer
					{/if}
				</td>
			</tr>
			<tr>
				<td>Tgl Transfer (Diisi Apabila Transfer)</td>
				<td>
					{$tgl_transfer}
				</td>
			</tr>
			<tr>
				<td>Ukuran Jas</td>
				<td>
					{$ukuran_jas}
				</td>
			</tr>
			<tr>
				<td>Tgl Tes Tulis & Psikologi</td>
				<td>
					{$tgl_tes_tulis}
				</td>
			</tr>
			<tr>
				<td class="center" colspan="2">
					<input type="button" name="cetak" value="Cetak Kwitansi" onclick="window.open('{$LINK}', 'baru');" />
				</td>
			</tr>
		{/if}
	</table>
	</form>
	{/if}
	
	{* {if $voucher.KODE_JURUSAN == '01' or $voucher.KODE_JURUSAN == '02'} *}
		<!-- <tr>
			<td>Ganti Jenis Voucher</td>
			<td>
				<form action="peserta-voucher.php?kode_voucher={$voucher.KODE_VOUCHER}" method="post">
					<input type="hidden" name="mode" value="ganti" />
					<input type="hidden" name="kode_voucher" value="{$voucher.KODE_VOUCHER}" />
					<select name="kode_jurusan" onchange="javascript: $('form').submit();">
						<option value="01" {if $voucher.KODE_JURUSAN == '01'}selected{/if}>IPA</option>
						<option value="02" {if $voucher.KODE_JURUSAN == '02'}selected{/if}>IPS</option>
					</select>
				</form>
			</td>
		</tr> -->
	{* {/if} *}
	{if $voucher.TGL_BAYAR == ''}
	<table>
		<tr>
			<th colspan="2">Detail Voucher</th>
		</tr>
		<tr>
			<td>Kode Voucher</td>
			<td>{$voucher.KODE_VOUCHER}</td>
		</tr>
		<tr>
			<td>Pin Voucher</td>
			<td>{if $voucher.TGL_BAYAR != ''}{$voucher.PIN_VOUCHER}{else}Voucher belum dibayar{/if}</td>
		</tr>
		 <tr>
			<td>Tanggal Pengambilan</td>
			<td>{$voucher.TGL_AMBIL}</td>
		</tr>
		<tr>
			<td class="center" colspan="2">
				<form action="peserta-voucher.php?kode_voucher={$voucher.KODE_VOUCHER}" method="post">
					<input type="hidden" name="mode" value="bayar" />
					<input type="hidden" name="kode_voucher" value="{$voucher.KODE_VOUCHER}" />
					<input type="hidden" name="id_voucher" value="{$voucher.ID_VOUCHER}" />
					<input type="submit" value="Bayar" />
				</form>
			</td>
		</tr>
	</table>
	{/if}
{/if}

<script>
    $('#bayar1').keyup(function(){
        var bayar1 = parseInt($('#bayar1').val());
        var bayar2 = parseInt($('#bayar2').val());
        var bayar3 = parseInt($('#bayar3').val());
        var bayar4 = parseInt($('#bayar4').val());
        var bayar5 = parseInt($('#bayar5').val());
        var bayar6 = parseInt($('#bayar6').val());
        var bayar7 = parseInt($('#bayar7').val());
        var bayar8 = parseInt($('#bayar8').val());
        var total_bayar = bayar1 + bayar2 + bayar3 + bayar4 + bayar5 + bayar6 + bayar7 + bayar8;
        var total_bayar_int = parseInt(total_bayar);
        $("#total_bayar").val(total_bayar_int);
    });
    $('#bayar2').keyup(function(){
        var bayar1 = parseInt($('#bayar1').val());
        var bayar2 = parseInt($('#bayar2').val());
        var bayar3 = parseInt($('#bayar3').val());
        var bayar4 = parseInt($('#bayar4').val());
        var bayar5 = parseInt($('#bayar5').val());
        var bayar6 = parseInt($('#bayar6').val());
        var bayar7 = parseInt($('#bayar7').val());
        var bayar8 = parseInt($('#bayar8').val());
        var total_bayar = bayar1 + bayar2 + bayar3 + bayar4 + bayar5 + bayar6 + bayar7 + bayar8;
        var total_bayar_int = parseInt(total_bayar);
        $("#total_bayar").val(total_bayar_int);
    });
    $('#bayar3').keyup(function(){
        var bayar1 = parseInt($('#bayar1').val());
        var bayar2 = parseInt($('#bayar2').val());
        var bayar3 = parseInt($('#bayar3').val());
        var bayar4 = parseInt($('#bayar4').val());
        var bayar5 = parseInt($('#bayar5').val());
        var bayar6 = parseInt($('#bayar6').val());
        var bayar7 = parseInt($('#bayar7').val());
        var bayar7 = parseInt($('#bayar7').val());
        var bayar8 = parseInt($('#bayar8').val());
        var total_bayar = bayar1 + bayar2 + bayar3 + bayar4 + bayar5 + bayar6 + bayar7 + bayar8;
        var total_bayar_int = parseInt(total_bayar);
        $("#total_bayar").val(total_bayar_int);
    });
    $('#bayar4').keyup(function(){
        var bayar1 = parseInt($('#bayar1').val());
        var bayar2 = parseInt($('#bayar2').val());
        var bayar3 = parseInt($('#bayar3').val());
        var bayar4 = parseInt($('#bayar4').val());
        var bayar5 = parseInt($('#bayar5').val());
        var bayar6 = parseInt($('#bayar6').val());
        var bayar7 = parseInt($('#bayar7').val());
        var bayar8 = parseInt($('#bayar8').val());
        var total_bayar = bayar1 + bayar2 + bayar3 + bayar4 + bayar5 + bayar6 + bayar7 + bayar8;
        var total_bayar_int = parseInt(total_bayar);
        $("#total_bayar").val(total_bayar_int);
    });
    $('#bayar5').keyup(function(){
        var bayar1 = parseInt($('#bayar1').val());
        var bayar2 = parseInt($('#bayar2').val());
        var bayar3 = parseInt($('#bayar3').val());
        var bayar4 = parseInt($('#bayar4').val());
        var bayar5 = parseInt($('#bayar5').val());
        var bayar6 = parseInt($('#bayar6').val());
        var bayar7 = parseInt($('#bayar7').val());
        var bayar8 = parseInt($('#bayar8').val());
        var total_bayar = bayar1 + bayar2 + bayar3 + bayar4 + bayar5 + bayar6 + bayar7 + bayar8;
        var total_bayar_int = parseInt(total_bayar);
        $("#total_bayar").val(total_bayar_int);
    });
    $('#bayar6').keyup(function(){
        var bayar1 = parseInt($('#bayar1').val());
        var bayar2 = parseInt($('#bayar2').val());
        var bayar3 = parseInt($('#bayar3').val());
        var bayar4 = parseInt($('#bayar4').val());
        var bayar5 = parseInt($('#bayar5').val());
        var bayar6 = parseInt($('#bayar6').val());
        var bayar7 = parseInt($('#bayar7').val());
        var bayar8 = parseInt($('#bayar8').val());
        var total_bayar = bayar1 + bayar2 + bayar3 + bayar4 + bayar5 + bayar6 + bayar7 + bayar8;
        var total_bayar_int = parseInt(total_bayar);
        $("#total_bayar").val(total_bayar_int);
    });
    $('#bayar7').keyup(function(){
        var bayar1 = parseInt($('#bayar1').val());
        var bayar2 = parseInt($('#bayar2').val());
        var bayar3 = parseInt($('#bayar3').val());
        var bayar4 = parseInt($('#bayar4').val());
        var bayar5 = parseInt($('#bayar5').val());
        var bayar6 = parseInt($('#bayar6').val());
        var bayar7 = parseInt($('#bayar7').val());
        var bayar8 = parseInt($('#bayar8').val());
        var total_bayar = bayar1 + bayar2 + bayar3 + bayar4 + bayar5 + bayar6 + bayar7 + bayar8;
        var total_bayar_int = parseInt(total_bayar);
        $("#total_bayar").val(total_bayar_int);
    });
    $('#bayar8').keyup(function(){
        var bayar1 = parseInt($('#bayar1').val());
        var bayar2 = parseInt($('#bayar2').val());
        var bayar3 = parseInt($('#bayar3').val());
        var bayar4 = parseInt($('#bayar4').val());
        var bayar5 = parseInt($('#bayar5').val());
        var bayar6 = parseInt($('#bayar6').val());
        var bayar7 = parseInt($('#bayar7').val());
        var bayar8 = parseInt($('#bayar8').val());
        var total_bayar = bayar1 + bayar2 + bayar3 + bayar4 + bayar5 + bayar6 + bayar7 + bayar8;
        var total_bayar_int = parseInt(total_bayar);
        $("#total_bayar").val(total_bayar_int);
    });
</script>
