<div class="center_title_bar">Bina Lingkungan</div>

<form action="peserta-pm.php" method="get" id="filter">
    <table>
        <tr>
            <td>Penerimaan</td>
            <td>
                <select name="id_penerimaan" onchange="$('#filter').submit()">
                    <option value="">-- Pilih Penerimaan --</option>
                {foreach $penerimaan_set as $p}
                    <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                    {foreach $p.p_set as $p2}
                    <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN}</option>
                    {/foreach}
                    </optgroup>
                {/foreach}
                </select>
            </td>
        </tr> 
    </table>
</form>
<style>#content table tr td { vertical-align: top; }</style>
<table>
    <tr>
        <th>No</th>
        <th>No Ujian</th>
        <th>Peserta</th>
        <th>Keterangan BL</th>
        <th>Prodi Pilihan</th>
		<th></th>
    </tr>
	{foreach $data_set as $data}
	<tr {if $data@index is not div by 2}class="row1"{/if}>
		<td class="center">{$data@index + 1}</td>
		<td><img src="../../img/foto/ujian-snmptn/{$data.TAHUN}/{$data.NO_UJIAN}.jpg" width="60px"/><br/>{$data.NO_UJIAN}</td>
		<td>{$data.NM_C_MHS}</td>
		<td>Status Anak : {$data.STATUS_ANAK}<br/>Orang Tua : <strong>{$data.PM_ORTU}</strong><br/>Sponsor : <strong>{$data.PM_SPONSOR}</strong><br/>Keterangan: {$data.PM_KETERANGAN}</td>
		<td>{$data.NM_PROGRAM_STUDI}</td>
		<td></td>
	</tr>
	{/foreach}
	{if !empty($smarty.get.id_penerimaan)}
	<tr>
		<td colspan="9" class="center"><a href="peserta-pm.php?mode=add&id_penerimaan={$smarty.get.id_penerimaan}">Tambah Data</a></td>
	</tr>
	{/if}
</table>