<div class="center_title_bar">Peserta Pindah Penerimaan</div>

{if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

<table>
	<tr>
		<td>
			<label>Nomor Pendaftaran :</label>
		</td>
		<td>
			<form action="peserta-pindah.php" method="get">
			<input type="search" name="kode_voucher" {if isset($smarty.get.kode_voucher)}value="{$smarty.get.kode_voucher}"{/if} />
			<input type="submit" value="Cari..." />
			</form>
		</td>
	</tr>
</table>
			
{if !empty($voucher)}
<table>
	<tr>
		<th colspan="2">Detail Calon Mahasiswa</th>
	</tr>
	<tr>
		<td>Nomor Pendaftaran</td>
		<td>{$voucher.KODE_VOUCHER}</td>
	</tr>
	<tr>
		<td>Nama</td>
		<td>{$voucher.NM_C_MHS}</td>
	</tr>
	 <tr>
		<td>No HP</td>
		<td>{$voucher.NOMOR_HP}</td>
	</tr>
	<tr>
		<td>Asal Sekolah</td>
		<td>{$voucher.NM_SEKOLAH_ASAL}</td>
	</tr>
</table>

<form action="peserta-pindah.php?kode_voucher={$smarty.get.kode_voucher}" method="post">
	<b>Format : Nama Penerimaan - Jenjang - Jalur</b>
	<table>
	        <!-- <tr>
	            <th colspan="2">Format Perguruan Tinggi</th>
	        </tr> -->
	        <tr>
	            <td>Penerimaan Sekarang</td>
	            <td>
		            <select name="id_penerimaan_awal" disabled >
			            {foreach $penerimaan_set as $penerimaan}
			                <option value="{$penerimaan.ID_PENERIMAAN}" {if $penerimaan.ID_PENERIMAAN == $voucher.ID_PENERIMAAN}selected="selected"{/if}>{$penerimaan.NM_PENERIMAAN} Gelombang {$penerimaan.GELOMBANG} - {$penerimaan.NM_JENJANG} - {$penerimaan.NM_JALUR}</option>
			            {/foreach}
		            </select>
	            </td>
	        </tr>
	        <tr>
	            <td>Pindah Ke</td>
	            <td>
		            <select name="id_penerimaan_pindah">
			            {foreach $penerimaan_set as $penerimaan}
			            	{if $penerimaan.ID_PENERIMAAN != $voucher.ID_PENERIMAAN}
			                	<option value="{$penerimaan.ID_PENERIMAAN}">{$penerimaan.NM_PENERIMAAN} Gelombang {$penerimaan.GELOMBANG} - {$penerimaan.NM_JENJANG} - {$penerimaan.NM_JALUR}</option>
			                {/if}
			            {/foreach}
		            </select>
	            </td>
	        </tr>
	        <tr>
	            <td colspan="2" class="center">
		            <input type="submit" value="Simpan" />
	                <input type="hidden" name="mode" value="update" />
	                <input type="hidden" name="id_c_mhs" value="{$voucher.ID_C_MHS}" />
	                <input type="hidden" name="id_voucher" value="{$voucher.ID_VOUCHER}" />
                </td>
	        </tr>
	</table>
</form>
{/if}