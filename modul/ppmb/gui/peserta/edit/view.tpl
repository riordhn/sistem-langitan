<div class="center_title_bar">Edit Data Peserta</div>

<table>
    <tr>
        <td>
            <label>Kode Voucher / No Ujian :</label>
        </td>
        <td>
            <form action="peserta-edit.php" method="get">
				<input type="search" name="kode_voucher" {if isset($smarty.get.kode_voucher)}value="{$smarty.get.kode_voucher}"{/if} />
				<input type="submit" value="Cari..." />
            </form>
        </td>
    </tr>
</table>

{if $changed}
	<h2>{$changed}</h2>
{/if}

{if $change_failed}
	<h2>{$change_failed}</h2>
{/if}

{if empty($cmb) == false}

	<form action="peserta-edit.php?kode_voucher={$cmb.KODE_VOUCHER}" method="post">
		<input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
		<table>
			<tbody>
				<tr>
					<th colspan="2">Info Peserta</th>
				</tr>
				<tr>
					<td>Nama</td>
					<td><input name="nm_c_mhs" type="text" value="{$cmb.NM_C_MHS}" size="50" maxlength="50" /></td>
				</tr>
				<tr>
					<td>Pilihan</td>
					<td>{$cmb.NM_PROGRAM_STUDI}</td>
				</tr>
				<tr>
					<td>Telp</td>
					<td>{$cmb.TELP}</td>
				</tr>
				<tr>
					<td>Gelar</td>
					<td><input name="gelar" type="text" value="{$cmb.GELAR}" /></td>
				</tr>
				<tr>
					<td>Tanggal Bayar Voucher</td>
					<td>{$cmb.TGL_BAYAR}</td>
				</tr>
				{if $cmb.ID_JENJANG == 1 or $cmb.ID_JENJANG == 5}
					<tr>
						<td>Sekolah</td>
						<td>
							<label>Negara</label>
							<select id="negara">
								<option value=""></option>
								{foreach $negara_set as $negara}
									<option value="{$negara.ID_NEGARA}" {if $sekolah_asal.ID_NEGARA == $negara.ID_NEGARA}selected{/if}>{$negara.NM_NEGARA}</option>
								{/foreach}
							</select>
							<label>Provinsi</label>
							<select id="provinsi">
								<option value=""></option>
								{if !empty($provinsi_set)}
									{foreach $provinsi_set as $provinsi}
										<option value="{$provinsi.ID_PROVINSI}" {if $sekolah_asal.ID_PROVINSI == $provinsi.ID_PROVINSI}selected{/if}>{$provinsi.NM_PROVINSI}</option>
									{/foreach}
								{/if}
							</select>
							<br/>
							<label>Kota</label>
							<select id="kota">
								<option value=""></option>
								{if !empty($kota_set)}
									{foreach $kota_set as $kota}
										<option value="{$kota.ID_KOTA}" {if $sekolah_asal.ID_KOTA == $kota.ID_KOTA}selected{/if}>{$kota.TIPE_DATI2} {$kota.NM_KOTA}</option>
									{/foreach}
								{/if}
							</select>
							<label>Sekolah</label>
							<select id="sekolah" name="id_sekolah_asal">
								<option value=""></option>
								{if !empty($sekolah_set)}
									{foreach $sekolah_set as $sekolah}
										<option value="{$sekolah.ID_SEKOLAH}" {if $cmb.ID_SEKOLAH_ASAL == $sekolah.ID_SEKOLAH}selected{/if}>{$sekolah.NM_SEKOLAH}</option>
									{/foreach}
								{/if}
							</select>
						</td>
					</tr>
				{/if}
				<tr>
					<td>Ijazah : Jumlah Pelajaran</td>
					<td><input type="text" name="jumlah_pelajaran_ijazah" size="2" maxlength="2" value="{$cmb.JUMLAH_PELAJARAN_IJAZAH}"/></td>
				</tr>
				<tr>
					<td>Ijazah : Jumlah Nilai</td>
					<td><input type="text" name="nilai_ijazah" size="5" maxlength="5" value="{$cmb.NILAI_IJAZAH}" /></td>
				</tr>
				<tr>
					<td>UAN : Jumlah Pelajaran</td>
					<td><input type="text" name="jumlah_pelajaran_uan" size="2" maxlength="2" value="{$cmb.JUMLAH_PELAJARAN_UAN}"/></td>
				</tr>
				<tr>
					<td>UAN : Jumlah Nilai</td>
					<td><input type="text" name="nilai_uan" size="5" maxlength="5" value="{$cmb.NILAI_UAN}"/></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" value="Simpan" /></td>
				</tr>
			</tbody>
		</table>
	</form>

	<script type="text/javascript">
		$(document).ready(function() {
			
			// DropDownList Negara
			$('#negara').on('change', function() {
				var id_negara = $(this).val();
				// Clear Provinsi, Kota, Sekolah
				$('#provinsi').empty();
				$('#kota').empty();
				$('#sekolah').empty();
				// Ajax Provinsi
				$.ajax({
					async: false,
					url: 'peserta-edit.php?mode=get-provinsi&id_negara=' + id_negara
				}).done(function(r){
					$('#provinsi').append(r);
				});
			});
			
			// DropDownList Provinsi
			$('#provinsi').on('change', function(){
				var id_provinsi = $(this).val();
				// clear kota, sekolah
				$('#kota').empty();
				$('#sekolah').empty();
				// ajax kota
				$.ajax({
					async: false,
					url: 'peserta-edit.php?mode=get-kota&id_provinsi=' + id_provinsi
				}).done(function(r){
					$('#kota').append(r);
				});
			});
			
			// DropDownList Kota
			$('#kota').on('change', function(){
				var id_kota = $(this).val();
				// clear sekolah
				$('#sekolah').empty();
				// ajax sekolah
				$.ajax({
					async: false,
					url: 'peserta-edit.php?mode=get-sekolah&id_kota=' + id_kota
				}).done(function(r){
					$('#sekolah').append(r);
				});
			});
		});
	</script>
{/if}