<div class="center_title_bar">Edit Nama Peserta</div>

<form action="peserta-nama.php" method="get" id="filter">
<table>
    <tr>
        <td>Penerimaan</td>
        <td>
            <select name="id_penerimaan" onchange="$('#filter').submit()">
                <option value="">-- Pilih Penerimaan --</option>
            {foreach $penerimaan_set as $p}
                <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                {foreach $p.p_set as $p2}
                <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN}</option>
                {/foreach}
                </optgroup>
            {/foreach}
            </select>
        </td>
    </tr>
</table>
</form>

{if !empty($cmb_set)}
<table>
    <tr>
        <th>No</th>
        <th>No Ujian</th>
        <th>Nama</th>
        <th>Gelar</th>
        <th>Edit Nama</th>
        <th>Edit Gelar</th>
        <th></th>
		<th>Tgl Voucher</th>
    </tr>
    {foreach $cmb_set as $cmb}
    <tr {if $cmb@index is not div by 2}class="row1"{/if}>
        <td class="center">{$cmb@index + 1}</td>
        <td>{$cmb.NO_UJIAN}</td>
        <td>{$cmb.NM_C_MHS}</td>
        <td>{$cmb.GELAR}</td>
        <td class="center"><input type="text" id="nm_c_mhs_{$cmb.ID_C_MHS}" value="{$cmb.NM_C_MHS}" maxlength="64" /></td>
        <td class="center"><input type="text" id="gelar_{$cmb.ID_C_MHS}" value="{$cmb.GELAR}" size="6" maxlength="10" /></td>
        <td><button id-cmhs={$cmb.ID_C_MHS}>Simpan</button></td>
		<td>{$cmb.TGL_BAYAR}</td>
    </tr>
    {/foreach}
</table>
    
<script>
$(document).ready(function() {
    $('button').click(function() {
        var idCMhs = $(this).attr('id-cmhs');
        var cellNama = $(this).parent().prev().prev();
        var cellGelar = $(this).parent().prev();
        
        var nama = $('#nm_c_mhs_'+idCMhs).val();
        var gelar = $('#gelar_'+idCMhs).val();
        
        $.ajax({
            type: 'POST',
            url: 'peserta-nama.php',
            data: 'mode=edit&id_c_mhs='+idCMhs+'&nm_c_mhs='+nama+'&gelar='+gelar,
            success: function(r) {
                console.log(r);
            }
        });
        
        $(cellNama).html('OK');
        $(cellGelar).html('OK');
    });
});
</script>
{/if}