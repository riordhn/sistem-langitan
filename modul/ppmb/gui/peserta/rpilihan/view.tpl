<div class="center_title_bar">Reset Pilihan Prodi Peserta</div>

<table>
    <tr>
        <td>
            <label>Kode Voucher :</label>
        </td>
        <td>
            <form action="peserta-rpilihan.php" method="get">
            <input type="search" name="kode_voucher" {if isset($smarty.get.kode_voucher)}value="{$smarty.get.kode_voucher}"{/if} />
            <input type="submit" value="Cari..." />
            </form>
        </td>
    </tr>
</table>
            
{if !empty($cmb)}

<form action="peserta-rpilihan.php{if isset($smarty.get.kode_voucher)}?kode_voucher={$smarty.get.kode_voucher}{/if}" method="post">
<input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
<table>
    <tr>
        <th colspan="2">Pilihan Peserta</th>
    </tr>
    <tr>
        <td>Nama</td>
        <td>{$cmb.NM_C_MHS}</td>
    </tr>
    <tr>
        <td>Pilihan 1</td>
        <td>{$cmb.NM_PROGRAM_STUDI_1}</td>
    </tr>
    {if $cmb.ID_JENJANG == 1 or $cmb.ID_JENJANG == 5}
    <tr>
        <td>Pilihan 2</td>
        <td>{$cmb.NM_PROGRAM_STUDI_2}</td>
    </tr>
    <tr>
        <td>Pilihan 3</td>
        <td>{$cmb.NM_PROGRAM_STUDI_3}</td>
    </tr>
    <tr>
        <td>Pilihan 4</td>
        <td>{$cmb.NM_PROGRAM_STUDI_4}</td>
    </tr>
    {/if}
    <tr>
        <td class="center" colspan="2">
            {if $cmb.TGL_VERIFIKASI_PPMB == ''}
            <a href="peserta-rpilihan.php">Batal</a>
            <input type="submit" value="Reset" />
            {else}
            Peserta sudah diverifikasi, data pilihan tidak bisa direset.
            {/if}
        </td>
    </tr>
</table>
</form>
{/if}