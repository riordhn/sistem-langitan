<div class="center_title_bar">Input Bina Lingkungan</div>

<form action="peserta-pmi.php?id_penerimaan={$smarty.get.id_penerimaan}" method="post">
	<input type="hidden" name="id_penerimaan" value="{$smarty.get.id_penerimaan}" />
	<input type="hidden" name="mode" value="add" />
	<table>
		<tr>
			<th colspan="2">Detail Bina Lingkungan Peserta</th>
		</tr>
		<tr>
			<td>Penerimaan</td>
			<td>{$penerimaan.NM_PENERIMAAN} {$penerimaan.TAHUN}</td>
		</tr>
		<tr>
			<td>No Ujian</td>
			<td><input type="text" name="no_ujian" value="" /></td>
		</tr>
		<tr>
			<td>Status Anak</td>
			<td>
				<select name="id_status_anak">
					<option value="">--</option>
					{foreach $status_anak_set as $sa}
						<option value="{$sa.ID_SKOR_PM}">{$sa.NM_SKOR_PM}</option>
					{/foreach}
				</select>
			</td>
		</tr>
		<tr>
			<td>Jabatan Sponsor</td>
			<td>
				{foreach $jabatan_set as $j}
					<label><input type="checkbox" name="id_jabatans[]" value="{$j.ID_SKOR_PM}" />{$j.NM_SKOR_PM}</label><br/>
				{/foreach}
			</td>
		</tr>
		<tr>
			<td>Nama Orang Tua</td>
			<td><input type="text" name="ortu" size="50"/></td>
		</tr>
		<tr>
			<td>Nama Sponsor</td>
			<td><input type="text" name="sponsor" size="50"/></td>
		</tr>
		<tr>
			<td>Keterangan</td>
			<td><textarea name="keterangan" cols="50" style="font-family: Arial; font-size: inherit"></textarea></td>
		</tr>
		<tr>
			<td colspan="2">
				<a href="peserta-pmi.php?id_penerimaan={$smarty.get.id_penerimaan}">Kembali</a>
				<input type="submit" value="Simpan" />
			</td>
		</tr>
	</table>
</form>