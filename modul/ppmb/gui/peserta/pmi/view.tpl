<div class="center_title_bar">Input Bina Lingkungan</div>

<form action="peserta-pmi.php" method="get" id="filter">
	<table>
		<tr>
			<td>Penerimaan</td>
			<td>
				<select name="id_penerimaan" onchange="$('#filter').submit()">
					<option value="">-- Pilih Penerimaan --</option>
				{foreach $penerimaan_set as $p}
					<optgroup label="{$p.TAHUN} {$p.SEMESTER}">
					{foreach $p.p_set as $p2}
					<option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN}</option>
					{/foreach}
					</optgroup>
				{/foreach}
				</select>
			</td>
		</tr>
		<tr>
			<td>Program Studi 1</td>
			<td>
				<select name="id_program_studi" onchange="$('#filter').submit()">
					<option value="">Semua Program Studi</option>
					{if !empty($program_studi_set)}
						{foreach $program_studi_set as $ps}
							<option value="{$ps.ID_PROGRAM_STUDI}"
									{if !empty($smarty.get.id_program_studi)}{if $ps.ID_PROGRAM_STUDI == $smarty.get.id_program_studi}selected{/if}{/if}
									>{$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI}</option>
						{/foreach}
					{/if}
				</select>
			</td>
		</tr> 
	</table>
</form>
<style>#content table tr td { vertical-align: top; }</style>
<table>
	<tr>
		<th>No</th>
		<th>No Ujian</th>
		<th>Peserta</th>
		<th>Keterangan BL</th>
		<th>Prodi Pilihan</th>
		<th>Urutan</th>
		<th style="width: 60px"></th>
	</tr>
	{if !empty($smarty.get.id_penerimaan)}
	<tr>
		<td colspan="7" class="center"><a href="peserta-pmi.php?mode=add&id_penerimaan={$smarty.get.id_penerimaan}">Tambah Data</a></td>
	</tr>
	{/if}
	{$pilihan_1 = ''}
	{foreach $data_set as $data}
		{if $pilihan_1 != $data.NM_PILIHAN_1}
			<tr>
				<td colspan="7" class="center" style="font-size: 1.3em">{$data.NM_PILIHAN_1}</td>
			</tr>
			{$pilihan_1 = $data.NM_PILIHAN_1}
		{/if}
		<tr {if $data@index is not div by 2}class="row1"{/if}>
			<td class="center">{$data@index + 1}</td>
			<td>
				<!-- YANG ASLI -->
				 
				<a href="{$foto_path}/{$data.NO_UJIAN}.jpg" target="_blank" class="disable-ajax"><img src="{$foto_path}/{$data.NO_UJIAN}.jpg" width="60px" /></a><br/>

				{$data.NO_UJIAN}
			</td>
			<td>{$data.NM_C_MHS}</td>
			<td>Status Anak : {$data.STATUS_ANAK}<br/>Orang Tua : <strong>{$data.ORTU}</strong><br/>Sponsor : <strong>{$data.SPONSOR}</strong><br/>Keterangan: {$data.KETERANGAN}</td>
			<td>P1: {$data.NM_PILIHAN_1}<br/>P2: {$data.NM_PILIHAN_2}</td>
			<td class="center">
				{if strlen($data.URUTAN) < 10}{$data.URUTAN}{/if}
			</td>
			<td>
				<a href="peserta-pmi.php?mode=edit&no_ujian={$data.NO_UJIAN}&id_penerimaan={$smarty.get.id_penerimaan}">Edit</a> |
				<a href="peserta-pmi.php?mode=delete&no_ujian={$data.NO_UJIAN}&id_penerimaan={$smarty.get.id_penerimaan}">Hapus</a>
			</td>
		</tr>
	{/foreach}
	{if !empty($smarty.get.id_penerimaan)}
	<tr>
		<td colspan="7" class="center"><a href="peserta-pmi.php?mode=add&id_penerimaan={$smarty.get.id_penerimaan}">Tambah Data</a></td>
	</tr>
	{/if}
</table>