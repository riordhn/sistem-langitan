<div class="center_title_bar">Proses Penetapan</div>

<form action="peserta-ppenetapan.php" method="get" id="filter">
    <table class="filter" border="1">
        <tr>
            <td>Penerimaan</td>
            <td>
                <select name="id_penerimaan" onchange="$('#filter').submit(); return false;">
                    <option value="">-- Pilih Penerimaan --</option>
                {foreach $penerimaan_set as $p}
                    <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                    {foreach $p.p_set as $p2}
                    <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} </option>
                    {/foreach}
                    </optgroup>
                {/foreach}
                </select>
            </td>
        </tr>
    </table>
</form>

{if $deleted}<h2>{$deleted}</h2>{/if}
{if $added}<h2>{$added}</h2>{/if}

{if $smarty.get.id_penerimaan != ''}

<form action="peserta-ppenetapan.php?id_penerimaan={$smarty.get.id_penerimaan}" method="post">
	<input type="hidden" name="id_penerimaan" value="{$smarty.get.id_penerimaan}" />
	<input type="hidden" name="mode" value="penetapan" />
	
	<table border="1px" class="view" width="100%">
		<tr>
	        <th>No</th>
	        <th>
	        	<input type="checkbox" name="select_all" />
	        </th>
			<th>Nomor Pendaftaran</th>
			<th>Nama</th>
			<th>No HP</th>
			<th>Asal Sekolah</th>
			<th>Pilihan 1</th>
		</tr>
		{foreach $cmb_set as $cmb}
		<tr>
	        <td class="center">{$cmb@index + 1}</td>
	        <td>
				<input type="checkbox" name="id_c_mhs[]" value="{$cmb.ID_C_MHS}" />
	        </td>
			<td>{$cmb.KODE_VOUCHER}</td>
			<td>{ucwords(strtolower($cmb.NM_C_MHS))}</td>
			<td>{$cmb.NOMOR_HP}</td>
			<td>{$cmb.NM_SEKOLAH_ASAL}</td>
			<td>{$cmb.NM_PROGRAM_STUDI}</td>
		</tr>
		{/foreach}
		<tr>
			<td colspan="7" class="center"><input type="submit" value="Penetapan" /></td>
		</tr>
	</table>
</form>
        
{/if}

<script type="text/javascript">
    $(document).ready(function() {
        
        /* Select All Checkbox */
        $('input[name="select_all"]').change(function() {
            var select_all_checked = this.checked;
            $('input[name="id_c_mhs[]"]').each(function() {
                this.checked = select_all_checked;
            });
        });
    });
</script>