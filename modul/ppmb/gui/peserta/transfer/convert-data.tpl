<div class="center_title_bar">Transfer Pasca Data ke SQL</div>

<table class="filter">
    <tr>
        <th>Tahun</th>
		<th>Jalur</th>
        <th>Jenjang</th>
        <th>Penerimaan</th>
        <th>Gel</th>
        <th>Semester</th>
        <th>Verifikasi</th>
       
    </tr>
    {foreach $penerimaan_set as $p}
    <tr {if $p@index is div by 2}class="row1"{/if}>
        <td class="center">{$p.TAHUN}</td>
        <td>{$p.NM_JALUR}</td>
        <td class="center">{$p.NM_JENJANG}</td>
        <td>{$p.NM_PENERIMAAN}</td>
        <td class="center">{$p.GELOMBANG}</td>
        <td class="center">{$p.SEMESTER}</td>
        <td class="center">{$p.JUMLAH_VERIFIKASI}</td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="7" class="center">
            <input type="button" value="Generate All" onclick="javascript: window.open('peserta-transfer.php?mode=generate-pasca');"/>
        </td>
    </tr>
</table>


<!--
<table style="text-align: center;border-collapse: collapse;">
    <input type="hidden" name="transfer"/>
    <tr>
        <th>Jenjang</th>
        <th>Jumlah Calon Mahasiswa Belum Ter Transfer</th>
        <th>Proses</th>
    </tr>
    <tr>
        <td>Pasca Non Unair
        <td>{*$jumlah_pasca_tidak_tertransfer*}</td>
        <td>
            <input type="button" value="Transfer" onclick="window.location.href=('convert-data.php?jenjang=pasca')" />
        </td>
    </tr>
    <tr>
        <td>Alih</td>
        <td>{*$jumlah_alih_tidak_tertransfer*}</td>
        <td>
            <input type="button" value="Transfer" onclick="window.location.href=('convert-data.php?jenjang=alih')" />
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <input type="button" value="Reload" onclick="window.location.href=('convert-data.php')" />
        </td>
    </tr>
</table>
-->