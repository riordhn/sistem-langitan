<div class="center_title_bar">Switch Nilai Peserta</div>

<form action="peserta-snilai.php" method="get">
	<table>
		<tbody>
			<tr>
				<th colspan="2">Peserta Yang akan di switch</th>
			</tr>
			<tr>
				<td>No Peserta 1</td>
				<td><input type="text" name="no_ujian_1" value="{if !empty($smarty.get.no_ujian_1)}{$smarty.get.no_ujian_1}{/if}"></td>
			</tr>
			<tr>
				<td>No Peserta 2</td>
				<td><input type="text" name="no_ujian_2" value="{if !empty($smarty.get.no_ujian_2)}{$smarty.get.no_ujian_2}{/if}"></td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" name="submit" value="Lihat" />
				</td>
			</tr>
		</tbody>
	</table>
</form>

{if !empty($cmb1) and !empty($cmb2)}
	<form action="peserta-snilai.php?{$smarty.server.QUERY_STRING}" method="post">
		<table>
			<tbody>
				<tr>
					<th>Nilai Peserta : {$cmb1.NM_C_MHS}</th>
					<th>Nilai Peserta : {$cmb2.NM_C_MHS}</th>
				</tr>
				{for $row = 0 to 1}
				<tr>
					<td>
						ID_NILAI_CMHS : {$nilai1[$row].ID_NILAI_CMHS}
					</td>
					<td>
						ID_NILAI_CMHS : {$nilai2[$row].ID_NILAI_CMHS}
					</td>
				</tr>
				<tr>
					<td>
						NILAI TPA : {$nilai1[$row].NILAI_TPA}
					</td>
					<td>
						NILAI TPA : {$nilai2[$row].NILAI_TPA}
					</td>
				</tr>
				<tr>
					<td>
						NILAI PRESTASI : {$nilai1[$row].NILAI_PRESTASI}
					</td>
					<td>
						NILAI PRESTASI : {$nilai2[$row].NILAI_PRESTASI}
					</td>
				</tr>
				<tr>
					<td>
						NILAI BL : {$nilai1[$row].NILAI_BL}
					</td>
					<td>
						NILAI BL : {$nilai2[$row].NILAI_BL}
					</td>
				</tr>
				{/for}
				<tr>
					<td colspan="2">
						<input type="hidden" name="id_c_mhs_1" value="{$cmb1.ID_C_MHS}" />
						<input type="hidden" name="id_c_mhs_2" value="{$cmb2.ID_C_MHS}" />
						<input type="submit" value="Switch" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
{/if}