<div class="center_title_bar">Rekap Bidik Misi</div>

<table>
    <tr>
        <th>No</th>
        <th>Program Studi</th>
        <th>Bidik Misi Diterima</th>
        <th>Kuota Undangan</th>
        <th>Persentase</th>
        <th>Kuota Total</th>
    </tr>
    {$bm = 0}
    {$kuota = 0}
    {$total_kuota = 0}
    {foreach $ps_set as $ps}
    <tr>
        <td class="center">{$ps@index + 1}</td>
        <td>{$ps.NM_PROGRAM_STUDI}</td>
        <td class="center">{$ps.BM}</td>
        <td class="center">{$ps.KUOTA}</td>
        <td class="center">{number_format($ps.BM / $ps.KUOTA * 100, 0)}%</td>
        <td class="center">{(($ps.KUOTA / 4) * 10)}</td>
    </tr>
    {$bm = $bm + $ps.BM}
    {$kuota = $kuota + $ps.KUOTA}
    {$total_kuota = $total_kuota + (($ps.KUOTA / 4) * 10)}
    {/foreach}
    <tr>
        <td colspan="2">Total</td>
        <td class="center">{$bm}</td>
        <td class="center">{$kuota}</td>
        <td></td>
        <td class="center">{$total_kuota}</td>
    </tr>
</table>