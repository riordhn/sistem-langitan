<div class="center_title_bar">Rekap SMA per Program Studi - Detail Prodi</div>

<h1>S1 {$ps.NM_PROGRAM_STUDI}</h1>
<a href="snmptn-rsmaps.php">Kembali</a>
<table>
    <tr>
        <th>No</th>
        <th>Nama Sekolah</th>
        <th>Jumlah</th>
    </tr>
    {foreach $sekolah_set as $s}
    <tr>
        <td class="center">{$s@index + 1}</td>
        <td><a href="snmptn-rsmaps.php?mode=detail-peserta&id_program_studi={$ps.ID_PROGRAM_STUDI}&id_sekolah={$s.ID_SEKOLAH}">{$s.NM_SEKOLAH}</a></td>
        <td class="center">{$s.JUMLAH}</td>
    </tr>
    {/foreach}
</table>