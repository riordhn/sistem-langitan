<div class="center_title_bar">Rekap SMA per Program Studi</div>

<table>
    <tr>
        <th>No</th>
        <th>Program Studi</th>
        <th>Jumlah SMA</th>
        <th>Kuota</th>
        <th>% Variasi</th>
    </tr>
    {foreach $prodi_set as $ps}
    <tr>
        <td class="center">{$ps@index + 1}</td>
        <td><a href="snmptn-rsmaps.php?mode=detail-prodi&id_program_studi={$ps.ID_PROGRAM_STUDI}">{$ps.NM_PROGRAM_STUDI}</a></td>
        <td class="center">{$ps.JUMLAH_SMA}</td>
        <td class="center">{$ps.KUOTA}</td>
        <td class="center">{number_format(($ps.JUMLAH_SMA / $ps.KUOTA) * 100, 0)}%</td>
    </tr>
    {/foreach}
</table>