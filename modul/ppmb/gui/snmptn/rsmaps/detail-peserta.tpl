<div class="center_title_bar">Rekap SMA per Program Studi - Detail Mahasiswa</div>

<h1>S1 {$ps.NM_PROGRAM_STUDI}</h1>
<h2>{$s.NM_SEKOLAH}</h2>
<a href="snmptn-rsmaps.php?mode=detail-prodi&id_program_studi={$ps.ID_PROGRAM_STUDI}">Kembali</a>

<table>
    <tr>
        <th>No</th>
        <th>No Ujian</th>
        <th>Nama Peserta</th>
    </tr>
    {foreach $cmb_set as $cmb}
    <tr>
        <td class="center">{$cmb@index + 1}</td>
        <td>{$cmb.NO_UJIAN}</td>
        <td>{$cmb.NM_C_MHS}</td>
    </tr>
    {/foreach}
</table>