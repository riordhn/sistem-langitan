<div class="center_title_bar">Peserta SNMPTN</div>

<form action="snmptn-peserta.php" method="get" id="filter">
<table>
    <tr>
        <td>Program Studi</td>
        <td>
            <select name="id_program_studi" onchange="$('#filter').submit();">
                <option value="">--</option>
            {foreach $program_studi_set as $ps}
                <option value="{$ps.ID_PROGRAM_STUDI}" {if $smarty.get.id_program_studi == $ps.ID_PROGRAM_STUDI}selected="selected"{/if}>{$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI}</option>
            {/foreach}
            </select>
        </td>
    </tr>
</table>
</form>

<table>
    <tr>
        <th rowspan="2">No</th>
        <th rowspan="2">No Ujian</th>
        <th rowspan="2">Nama</th>
        <th rowspan="2">BM</th>
        <th colspan="5">Index Siswa</th>
        <th colspan="4">Index Sekolah</th>
    </tr>
    <tr>
        <th>Rapor</th>
        <th title="Prestasi Akademik">Pr.Akd</th>
        <th title="Prestasi Non-Akademik">Pr.NAkd</th>
        <th title="Pemberdayaan Masyarakat">PM</th>
        <th title="Jenis Kelas">JK</th>
        <th title="Akreditasi Sekolah">Ak. S</th>
        <th title="Rasio SNMPTN">R. SNMPTN</th>
        <th title="Rasio Mandiri">R. MDR</th>
        <th title="Rata IPK">IPK</th>
    </tr>
    {foreach $cmb_set as $cmb}
    <tr>
        <td class="center">{$cmb@index+1}</td>
        <td><a href="snmptn-peserta.php?mode=detail&id_c_mhs={$cmb.ID_C_MHS}&{$smarty.server.QUERY_STRING}">{$cmb.NO_UJIAN}</a></td>
        <td>{$cmb.NM_C_MHS}</td>
        <td class="center">{if $cmb.STATUS_BIDIK_MISI}Y{else}-{/if}</td>
        <td class="center">{$cmb.NILAI_RAPOR}</td>
        <td class="center">{$cmb.PRESTASI_AKD}</td>
        <td class="center">{$cmb.PRESTASI_NON_AKD}</td>
        <td class="center">{$cmb.NILAI_PM}</td>
        <td class="center">{$cmb.JENIS_KELAS}</td>
        <td class="center">{$cmb.AKREDITASI_SEKOLAH}</td>
        <td class="center">{number_format($cmb.RASIO_MANDIRI,4)}</td>
        <td class="center">{number_format($cmb.RASIO_SNMPTN,4)}</td>
        <td class="center">{number_format($cmb.RATA_IPK,2)}</td>
    </tr>
    {/foreach}
</table>