<div class="center_title_bar">Peserta SNMPTN - Detail Peserta</div>
<style>.tebal { font-weight: bold }</style>
<a href="snmptn-peserta.php?id_program_studi={$smarty.get.id_program_studi}&pilihan_ke={$smarty.get.pilihan_ke}">Kembali</a>

<table>
    <tr>
        <th colspan="3">Detail Peserta</th>
    </tr>
    <tr>
        <td rowspan="12">
            <img src="/img/foto/ujian-snmptn/2012/{$cmb.NO_UJIAN}.jpg" width="200px" />
        </td>
        <td class="tebal">No Ujian</td>
        <td>{$cmb.NO_UJIAN}</td>
    </tr>
    <tr>
        <td class="tebal">Nama</td>
        <td>{$cmb.NM_C_MHS}</td>
    </tr>
    <tr>
        <td class="tebal">Pilihan 1</td>
        <td>{$cmb.NM_PROGRAM_STUDI1}</td>
    </tr>
    <tr>
        <td class="tebal">Pilihan 2</td>
        <td>{$cmb.NM_PROGRAM_STUDI2}</td>
    </tr>
    <tr>
        <td class="tebal">Sekolah Asal</td>
        <td>{$cmb.NM_SEKOLAH}</td>
    </tr>
    <tr>
        <td class="tebal">Akreditasi Sekolah</td>
        <td>{$cmb.AKREDITASI_SEKOLAH}</td>
    </tr>
    <tr>
        <td class="tebal">Jenis Kelas</td>
        <td>{$cmb.JENIS_KELAS}</td>
    </tr>
    <tr>
        <td class="tebal">Rata Nilai</td>
        <td>{$cmb.NILAI_RAPOR}</td>
    </tr>
    <tr>
        <td>Semester 3</td>
        <td>{$cmb.RATA_SEMESTER_3}</td>
    </tr>
    <tr>
        <td>Semester 4</td>
        <td>{$cmb.RATA_SEMESTER_4}</td>
    </tr>
    <tr>
        <td>Semester 5</td>
        <td>{$cmb.RATA_SEMESTER_5}</td>
    </tr>
</table>