<div class="center_title_bar">Rekap Program Studi</div>

<table>
    <tr>
        <th>No</th>
        <th>Program Studi</th>
        <th>Pilihan 1</th>
        <th>Pilihan 2</th>
        <th>Kuota</th>
        <th>Diterima</th>
        <th>Keketatan</th>
    </tr>
    {$peminat1 = 0}{$peminat2 = 0}{$kuota = 0}{$diterima = 0}
    {foreach $program_studi_set as $ps}
    <tr>
        <td class="center">{$ps@index + 1}</td>
        <td>{$ps.NM_PROGRAM_STUDI}</td>
        <td class="center">{$ps.PEMINAT1}</td>
        <td class="center">{$ps.PEMINAT2}</td>
        <td class="center">{$ps.KUOTA}</td>
        <td class="center">{$ps.DITERIMA}</td>
        <td class="center">{number_format($ps.KUOTA/$ps.PEMINAT1,2)}</td>
    </tr>
    {$peminat1 = $peminat1 + $ps.PEMINAT1}
    {$peminat2 = $peminat2 + $ps.PEMINAT2}
    {$kuota = $kuota + $ps.KUOTA}
    {$diterima = $diterima + $ps.DITERIMA}
    {/foreach}
    <tr>
        <td colspan="2">Jumlah</td>
        <td class="center">{$peminat1}</td>
        <td class="center">{$peminat2}</td>
        <td class="center">{$kuota}</td>
        <td class="center">{$diterima}</td>
        <td></td>
    </tr>
</table>