<div class="center_title_bar">Detail Peserta</div>

{if $result}<script type="text/javascript">alert('{$result}');</script>{/if}

<form action="snmptn-pm2.php" method="get">
<table>
    <tr>
        <td>No Ujian</td>
        <td><input type="search" name="no_ujian" {if $smarty.get.no_ujian}value="{$smarty.get.no_ujian}"{/if}/>
            <input type="submit" value="Cari" />
        </td>
    </tr>
</table>
</form>

{if $cmb.ID_C_MHS != ''}
<input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
<table>
    <tr>
        <th colspan="3">Detail Peserta</th>
    </tr>
    <tr>
        <td rowspan="18" style="vertical-align: top">
            <img src="/img/foto/ujian-snmptn/2012/{$cmb.NO_UJIAN}.jpg" width="200px" />
        </td>
        <td class="tebal">Status</td>
        <td><strong style="font-size: 16px">{if $cmb.IS_DITERIMA}DITERIMA{else}TIDAK DITERIMA{/if} - {$cmb.URUTAN}</strong></td>
    </tr>
    <tr>
        <td class="tebal">No Ujian</td>
        <td>{$cmb.NO_UJIAN}</td>
    </tr>
    <tr>
        <td class="tebal">Nama</td>
        <td>{$cmb.NM_C_MHS}</td>
    </tr>
    <tr>
        <td class="tebal">Pilihan 1</td>
        <td>{$cmb.NM_PROGRAM_STUDI1}</td>
    </tr>
    <tr>
        <td class="tebal">Pilihan 2</td>
        <td>{$cmb.NM_PROGRAM_STUDI2}</td>
    </tr>
    <tr>
        <td class="tebal">Sekolah Asal</td>
        <td>{$cmb.NM_SEKOLAH}</td>
    </tr>
    <tr>
        <td class="tebal">Akreditasi Sekolah</td>
        <td>{$cmb.AKREDITASI_SEKOLAH}</td>
    </tr>
    <tr>
        <td class="tebal">Jenis Kelas</td>
        <td>{$cmb.JENIS_KELAS}</td>
    </tr>
    <tr>
        <td class="tebal">Rata Nilai</td>
        <td>{$cmb.NILAI_RAPOR}</td>
    </tr>
    <tr>
        <td>Semester 3</td>
        <td>{$cmb.RATA_SEMESTER_3}</td>
    </tr>
    <tr>
        <td>Semester 4</td>
        <td>{$cmb.RATA_SEMESTER_4}</td>
    </tr>
    <tr>
        <td>Semester 5</td>
        <td>{$cmb.RATA_SEMESTER_5}</td>
    </tr>
    <!--
    <tr>
		<td>Peserta Khusus (SM)</td>
		<td>
			<input type="checkbox" name="pk" {if $cmb.IS_PESERTA_KHUSUS}checked="checked"{/if} onclick="updatePK();" />
		</td>
	</tr>
    <tr>
        <td>Peserta PKD</td>
        <td>
            <input type="checkbox" name="pkd" {if $cmb.IS_PKD}checked="checked"{/if} onclick="updatePKD();" />
        </td>
    </tr>
    -->
	<tr>
		<td>PM</td>
		<td>
            {foreach $skor_pm_set as $spm}
                <label style="cursor: pointer">
                    <input type="checkbox" name="spm{$spm.ID_SKOR_PM}" value="{$spm.ID_SKOR_PM}"
                    {foreach $cpm_set as $cpm}{if $cpm.ID_SKOR_PM == $spm.ID_SKOR_PM}checked="checked"{break}{/if}{/foreach}
                    onclick="changePM({$spm.ID_SKOR_PM});"/>({$spm.NILAI}) - {$spm.NM_SKOR_PM} 
                </label>
                <br/>
            {/foreach}
		</td>
	</tr>
    <tr>
        <td>Nama Ortu PM: </td>
        <td>
            <input type="text" name="nm_ortu" value="{$cpm_set[0]['NM_ORTU']}" required="required" size="50" onkeydown="$('#ortuButton').removeAttr('disabled')"/>
            <button disabled="disabled" id="ortuButton" onclick="updateNamaOrtu()">Simpan</button>
        </td>
    </tr>
    <tr>
        <td>Sponsor PM : </td>
        <td>
            <input type="text" name="nm_sponsor" value="{$cpm_set[0]['NM_SPONSOR']}" size="50" onkeydown="$('#sponsorButton').removeAttr('disabled')"/>
            <button disabled="disabled" id="sponsorButton" onclick="updateSponsor()">Simpan</button>
        </td>
    </tr>
    <tr>
        <td>Keterangan PM :</td>
        <td>
            <textarea name="keterangan" cols="50" style="font-family: Arial; font-size: 13px" onkeydown="$('#keteranganButton').removeAttr('disabled')">{$cpm_set[0]['KETERANGAN']}</textarea>
            <button disabled="disabled" id="keteranganButton" onclick="updateKeterangan()">Simpan</button>
        </td>
    </tr>
</table>

<script type="text/javascript">
    function updatePK()
    {
        var id_c_mhs = $('input[name="id_c_mhs"]').val();
        var is_peserta_khusus = $('input[name="pk"]').attr('checked') ? 1 : 0;

        $.ajax({
            url: 'snmptn-pm.php',
            type: 'POST',
            data: 'mode=edit-pk&id_c_mhs='+id_c_mhs+'&is_peserta_khusus='+is_peserta_khusus,
            success: function(r) {
                if (r != 1) alert('Gagal update PM');
            }
        });
    }
    
    function updatePKD()
    {
        var id_c_mhs = $('input[name="id_c_mhs"]').val();
        var is_pkd = $('input[name="pkd"]').attr('checked') ? 1 : 0;
        
        $.ajax({
            url: 'snmptn-pm.php',
            type: 'POST',
            data: 'mode=edit-pkd&id_c_mhs='+id_c_mhs+'&is_pkd='+is_pkd,
            success: function(r) {
                if (r != 1) alert('Gagal update PKD');
            }
        });
    }
    
	function changePM(id_skor_pm)
	{
		var id_c_mhs = $('input[name="id_c_mhs"]').val();
        var checked = $('input[name="spm'+id_skor_pm+'"]').attr('checked');
        
		$.ajax({
			url: 'snmptn-pm.php',
			type: 'POST',
			data: 'mode=update-pm&id_c_mhs='+id_c_mhs+'&id_skor_pm='+id_skor_pm+'&checked='+checked,
			success: function(data) {
                if (data != 1) alert('Gagal update nilai');
			}
		});
	}
    
    function updateInfo()
    {
        var nm_ortu = $('input[name="nm_ortu"]').val();
        var nm_sponsor = $('input[name="nm_sponsor"]').val();
        var keterangan = $('textarea[name="keterangan"]').val();
        var id_c_mhs = $('input[name="id_c_mhs"]').val();
        
        //alert(nm_ortu + ' ' + nm_sponsor + ' ' + keterangan);
    }
    
    function updateNamaOrtu()
    {
        var id_c_mhs = $('input[name="id_c_mhs"]').val();
        var nm_ortu = $('input[name="nm_ortu"]').val();
        
        $('#ortuButton').attr('disabled', 'disabled');
        
        $.ajax({
            url: 'snmptn-pm.php',
            type: 'POST',
            data: 'mode=update-ortu&id_c_mhs=' + id_c_mhs + '&nm_ortu=' + nm_ortu,
            success: function(r) {
                if (r != 1) alert('Data gagal di update');
            }
        });
    }
    
    function updateSponsor()
    {
        var id_c_mhs = $('input[name="id_c_mhs"]').val();
        var nm_sponsor = $('input[name="nm_sponsor"]').val();
        
        $('#sponsorButton').attr('disabled', 'disabled');
        
        $.ajax({
            url: 'snmptn-pm.php',
            type: 'POST',
            data: 'mode=update-sponsor&id_c_mhs=' + id_c_mhs + '&nm_sponsor=' + nm_sponsor,
            success: function(r) {
                if (r != 1) alert('Data gagal di update');
            }
        });
    }
    
    function updateKeterangan()
    {
        var id_c_mhs = $('input[name="id_c_mhs"]').val();
        var keterangan = $('textarea[name="keterangan"]').val();
        
        $('#keteranganButton').attr('disabled', 'disabled');
        
        $.ajax({
            url: 'snmptn-pm.php',
            type: 'POST',
            data: 'mode=update-keterangan&id_c_mhs=' + id_c_mhs + '&keterangan=' + keterangan,
            success: function(r) {
                if (r != 1) alert('Data gagal di update');
            }
        });
    }
</script>
{/if}