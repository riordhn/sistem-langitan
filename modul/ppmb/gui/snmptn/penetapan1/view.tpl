<div class="center_title_bar">Penetapan SNMPTN</div>

<style>
    .content table.statistik tr td { padding: 2px; font-size: small; }
    .kondisi-1 td, .kondisi-1 td a { color: yellow; }
    .kondisi-2 td, .kondisi-2 td a { color: #000; }
    .kondisi-3 td, .kondisi-3 td a { color: #00f; }
    .kondisi-4 td, .kondisi-4 td a { color: #f00; }
</style>

<form action="snmptn-penetapan.php" method="get" id="filter">
<table>
    <tr>
        <td>Program Studi</td>
        <td>
            <select name="id_program_studi" onchange="$('#filter').submit();">
                <option value="">--</option>
            {foreach $program_studi_set as $ps}
                <option value="{$ps.ID_PROGRAM_STUDI}" {if $smarty.get.id_program_studi == $ps.ID_PROGRAM_STUDI}selected="selected"{/if}>{$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI}</option>
            {/foreach}
            </select>
        </td>
    </tr>
</table>
</form>

<table>
    <tr>
        <th rowspan="2">No</th>
        <th rowspan="2">No Ujian</th>
        <th rowspan="2">Nama</th>
        <th rowspan="2">BM</th>
        <th rowspan="2">Jur</th>
        <th colspan="5">Index Siswa</th>
        <th colspan="4">Index Sekolah</th>
        <th rowspan="2">Skor</th>
        <th rowspan="2">SMA</th>
    </tr>
    <tr>
        <th>Rapor</th>
        <th title="Prestasi Akademik">PA</th>
        <th title="Prestasi Non-Akademik">PNA</th>
        <th title="Pemberdayaan Masyarakat">PM</th>
        <th title="Jenis Kelas">JK</th>
        <th title="Akreditasi Sekolah">AS</th>
        <th title="Rasio SNMPTN">RS</th>
        <th title="Rasio Mandiri">RM</th>
        <th title="Rata IPK">IPK</th>
    </tr>
    {foreach $cmb_set as $cmb}
    <tr 
        {if ($cmb@index + 1) <= $statistik.KUOTA}
            class="kondisi-2"
        {else}
            class="kondisi-3"
        {/if}>
        <td class="center">{$cmb@index+1}</td>
        <!--<td><a href="snmptn-penetapan.php?mode=detail&id_c_mhs={$cmb.ID_C_MHS}&{$smarty.server.QUERY_STRING}">{$cmb.NO_UJIAN}</a></td>-->
		<td><a href="snmptn-pm.php?no_ujian={$cmb.NO_UJIAN}">{$cmb.NO_UJIAN}</a></td>
        <td>{$cmb.NM_C_MHS}</td>
        <td class="center">{if $cmb.STATUS_BIDIK_MISI}Y{else}-{/if}</td>
        <td>{$cmb.JURUSAN_SEKOLAH}</td>
        <td class="center">{$cmb.NILAI_RAPOR}</td>
        <td class="center">{$cmb.NILAI_PA}</td>
        <td class="center">{$cmb.NILAI_PNA}</td>
        <td class="center">{$cmb.NILAI_PM}</td>
        <td class="center">{$cmb.NILAI_JK}</td>
        <td class="center">{$cmb.NILAI_AS}</td>
        <td class="center">{number_format($cmb.NILAI_RS,2)}</td>
        <td class="center">{number_format($cmb.NILAI_RM,2)}</td>
        <td class="center">{$cmb.NILAI_IPK}</td>
        <td class="center">{number_format($cmb.NILAI_TOTAL,3)}</td>
        <td>{$cmb.NM_SEKOLAH}</td>
    </tr>
    {/foreach}
</table>