<div class="center_title_bar">Peserta SNMPTN - Detail Peserta</div>
<style>.tebal { font-weight: bold }</style>
<button onclick="window.history.back();">Kembali</button>

<table>
    <tr>
        <th colspan="3">Detail Peserta</th>
    </tr>
    <tr>
        <td rowspan="12">
            <img src="/img/foto/ujian-snmptn/2012/{$cmb.NO_UJIAN}.jpg" width="200px" />
        </td>
        <td class="tebal">No Ujian</td>
        <td>{$cmb.NO_UJIAN}</td>
    </tr>
    <tr>
        <td class="tebal">Nama</td>
        <td>{$cmb.NM_C_MHS}</td>
    </tr>
    <tr>
        <td class="tebal">Pilihan 1</td>
        <td>{$cmb.NM_PROGRAM_STUDI1}</td>
    </tr>
    <tr>
        <td class="tebal">Pilihan 2</td>
        <td>{$cmb.NM_PROGRAM_STUDI2}</td>
    </tr>
    <tr>
        <td class="tebal">Sekolah Asal</td>
        <td>{$cmb.NM_SEKOLAH}</td>
    </tr>
    <tr>
        <td class="tebal">Akreditasi Sekolah</td>
        <td>{$cmb.AKREDITASI_SEKOLAH}</td>
    </tr>
    <tr>
        <td class="tebal">Jenis Kelas</td>
        <td>{$cmb.JENIS_KELAS}</td>
    </tr>
    <tr>
        <td class="tebal">Rata Nilai</td>
        <td>{$cmb.NILAI_RAPOR}</td>
    </tr>
    <tr>
        <td>Semester 3</td>
        <td>{$cmb.RATA_SEMESTER_3}</td>
    </tr>
    <tr>
        <td>Semester 4</td>
        <td>{$cmb.RATA_SEMESTER_4}</td>
    </tr>
    <tr>
        <td>Semester 5</td>
        <td>{$cmb.RATA_SEMESTER_5}</td>
    </tr>
	<tr>
		<td>Peserta Khusus</td>
		<td>
			<input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
			<input type="checkbox" name="pk" {if $cmb.IS_PESERTA_KHUSUS}checked="checked"{/if} onclick="updatePK();" />
			<script type="text/javascript">
				function updatePK() {
					var id_c_mhs = $('input[name="id_c_mhs"]').val();
					var is_peserta_khusus = $('input[name="pk"]').attr('checked') ? 1 : 0;
					
					$.ajax({
						url: 'snmptn-penetapan.php',
						type: 'POST',
						data: 'mode=edit-pk&id_c_mhs='+id_c_mhs+'&is_peserta_khusus='+is_peserta_khusus,
						success: function(data) {
							if (data != 1)
								alert('Gagal update PM');
						}
					});
				}
			</script>
		</td>
	</tr>
</table>