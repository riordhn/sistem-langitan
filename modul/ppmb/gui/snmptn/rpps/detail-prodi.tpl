<div class="center_title_bar">Rekap Provinsi per Prodi - Detail Prodi</div>

<h2>S1 {$program_studi.NM_PROGRAM_STUDI}</h2>

<a href="snmptn-rpps.php">Kembali</a>
<table>
    <tr>
        <th>No</th>
        <th>Provinsi</th>
        <th>Jumlah</th>
    </tr>
    {foreach $provinsi_set as $p}
    <tr>
        <td class="center">{$p@index + 1}</td>
        <td>{$p.NM_PROVINSI}</td>
        <td class="center">{$p.JUMLAH}</td>
    </tr>
    {/foreach}
</table>