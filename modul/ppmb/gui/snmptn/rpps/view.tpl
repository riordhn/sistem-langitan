<div class="center_title_bar">Rekap Provinsi per Program Studi</div>

<table>
    <tr>
        <th>No</th>
        <th>Program Studi</th>
        <th>Provinsi</th>
        <th>Kuota</th>
        <th>Variasi</th>
    </tr>
    {foreach $program_studi_set as $ps}
    <tr>
        <td class="center">{$ps@index + 1}</td>
        <td><a href="snmptn-rpps.php?mode=detail-prodi&id_program_studi={$ps.ID_PROGRAM_STUDI}">{$ps.NM_PROGRAM_STUDI}</a></td>
        <td class="center">{$ps.PROVINSI}</td>
        <td class="center">{$ps.KUOTA}</td>
        <td class="center">{number_format($ps.PROVINSI / $ps.KUOTA * 100, 0)}%</td>
    </tr>
    {/foreach}
</table>