<div class="center_title_bar">Penetapan PKD SNMPTN</div>

<style>
    .content table.statistik tr td { padding: 2px; font-size: small; }
    .kondisi-1 td, .kondisi-1 td a { color: yellow; }
    .kondisi-2 td, .kondisi-2 td a { color: #000; }
    .kondisi-3 td, .kondisi-3 td a { color: #00f; }
    .kondisi-4 td, .kondisi-4 td a { color: #f00; }
</style>

<form action="snmptn-ppkd.php" method="get" id="filter">
<table>
    <tr>
        <td>Program Studi</td>
        <td>
            <select name="id_program_studi" onchange="$('#filter').submit();">
                <option value="">--</option>
            {foreach $program_studi_set as $ps}
                <option value="{$ps.ID_PROGRAM_STUDI}" {if $smarty.get.id_program_studi == $ps.ID_PROGRAM_STUDI}selected="selected"{/if}>{$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI}</option>
            {/foreach}
            </select>
            Pilihan Ke
            <select name="pilihan_ke" onchange="$('#filter').submit();">
            {foreach $pilihan_ke as $pk}
                <option value="{$pk.ID}" {if $smarty.get.pilihan_ke == $pk.ID}selected="selected"{/if}>{$pk.ID}</option>
            {/foreach}
            </select>
    </tr>
    <tr>
        <td>
            Provinsi
        </td>
        <td>
            <select name="id_provinsi" onchange="$('#filter').submit();">
                <option value="all">SEMUA</option>
            {foreach $provinsi_set as $p}
                <option value="{$p.ID_PROVINSI}" {if $p.ID_PROVINSI == $smarty.get.id_provinsi}selected="selected"{/if}>{$p.NM_PROVINSI}</option>
            {/foreach}
            </select>
        </td>
    </tr>
</table>
</form>

<table>
    <tr>
        <th rowspan="2">No</th>
        <th rowspan="2">No Ujian</th>
        <th rowspan="2">Nama</th>
        <th colspan="5">Index Siswa</th>
        <th colspan="4">Index Sekolah</th>
        <th rowspan="2">Skor</th>
        <th rowspan="2">Provinsi</th>
    </tr>
    <tr>
        <th>Rapor</th>
        <th title="Prestasi Akademik">Pr.Akd</th>
        <th title="Prestasi Non-Akademik">Pr.NAkd</th>
        <th title="Pemberdayaan Masyarakat">PM</th>
        <th title="Jenis Kelas">JK</th>
        <th title="Akreditasi Sekolah">Ak. S</th>
        <th title="Rasio SNMPTN">R. SNMPTN</th>
        <th title="Rasio Mandiri">R. MDR</th>
        <th title="Rata IPK">IPK</th>
    </tr>
    {foreach $cmb_set as $cmb}
    <tr>
        <td class="center">{$cmb@index+1}</td>
        <td><a href="snmptn-ppkd.php?mode=detail&id_c_mhs={$cmb.ID_C_MHS}&{$smarty.server.QUERY_STRING}">{$cmb.NO_UJIAN}</a></td>
        <td>{$cmb.NM_C_MHS}</td>
        <td class="center">{$cmb.NILAI_RAPOR}</td>
        <td class="center">{$cmb.NILAI_AKD}</td>
        <td class="center">{$cmb.NILAI_NAKD}</td>
        <td class="center">{$cmb.NILAI_PM}</td>
        <td class="center">{$cmb.NILAI_KELAS}</td>
        <td class="center">{$cmb.NILAI_AKREDITASI}</td>
        <td class="center">{number_format($cmb.NILAI_SNMPTN,2)}</td>
        <td class="center">{number_format($cmb.NILAI_MANDIRI,2)}</td>
        <td class="center">{$cmb.NILAI_IPK}</td>
        <td class="center">{number_format($cmb.NILAI_TOTAL,2)}</td>
        <td>{$cmb.NM_PROVINSI}</Td>
    </tr>
    {/foreach}
</table>