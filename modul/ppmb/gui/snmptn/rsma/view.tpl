<div class="center_title_bar">Rekap Peserta Per Provinsi</div>

<style>.content table tr td a { color: #000; text-decoration: none; }</style>

<table>
    <tr>
        <th>No</th>
        <th>Provinsi</th>
        <th>Peserta</th>
        <th>Masuk Kuota</th>
        <th>% Masuk Kuota</th>
    </tr>
    {$peserta = 0}{$kuota = 0}
    {foreach $provinsi_set as $p}
    <tr {if $p@index is not div by 2}class="row1"{/if}>
        <td class="center">{$p@index + 1}</td>
        <td><a href="snmptn-rsma.php?mode=detail-provinsi&id_provinsi={$p.ID_PROVINSI}">{$p.NM_PROVINSI}</a></td>
        <td class="center">{$p.PESERTA}</td>
        <td class="center">{$p.KUOTA}</td>
        <td class="center">{number_format(($p.KUOTA / 1884)*100,2)}%</td>
    </tr>
    {$peserta = $peserta + $p.PESERTA}
    {$kuota = $kuota + $p.KUOTA}
    {/foreach}
    <tr>
        <td colspan="2">Total</td>
        <td class="center">{$peserta}</td>
        <td class="center">{$kuota}</td>
        <td></td>
    </tr>
</table>