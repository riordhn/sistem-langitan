<div class="center_title_bar">Rekap SMA - Detail Kota</div>

<style>
.content table tr td a { color: #000; text-decoration: none; }
</style>

<h2>{$kota.NM_KOTA}, {$kota.NM_PROVINSI}</h2>

<a href="snmptn-rsma.php?mode=detail-provinsi&id_provinsi={$kota.ID_PROVINSI}">Kembali</a>

<table>
    <tr>
        <th>No</th>
        <th>Sekolah</th>
        <th>Peserta</th>
        <th>Masuk Kuota</th>
        <th>% Masuk Kuota</th>
    </tr>
    {$peserta = 0}{$kuota = 0}
    {foreach $sekolah_set as $s}
    <tr {if $s@index is not div by 2}class="row1"{/if}>
        <td class="center">{$s@index + 1}</td>
        <td><a href="snmptn-rsma.php?mode=detail-peserta&id_sekolah={$s.ID_SEKOLAH}">{$s.NM_SEKOLAH}</a></td>
        <td class="center">{$s.PESERTA}</td>
        <td class="center">{$s.KUOTA}</td>
        <td class="center">{number_format(($s.KUOTA / 1884)*100,2)}%</td>
    </tr>
    {$peserta = $peserta + $s.PESERTA}
    {$kuota = $kuota + $s.KUOTA}
    {/foreach}
    <tr>
        <td colspan="2">Total</td>
        <td class="center">{$peserta}</td>
        <td class="center">{$kuota}</td>
        <td></td>
    </tr>
</table>