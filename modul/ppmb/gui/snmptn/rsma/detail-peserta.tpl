<div class="center_title_bar">Rekap SMA - Detail Peserta</div>

<h2>{$sekolah.NM_SEKOLAH}, {$sekolah.NM_KOTA}, {$sekolah.NM_PROVINSI}</h2>

<style>
    .diterima td { background-color: #8f8; }
</style>

<a href="snmptn-rsma.php?mode=detail-kota&id_kota={$sekolah.ID_KOTA}">Kembali</a>
<table id="peserta">
    <tr>
        <th>No</th>
        <th>No Ujian</th>
        <th>Nama Peserta</th>
        <th>Pilihan 1</th>
        <th>Pilihan 2</th>
    </tr>
    {foreach $cmb_set as $cmb}
    <tr {if $cmb.IS_DITERIMA}class="diterima"{/if}>
        <td class="center">{$cmb@index + 1}</td>
        <td>{$cmb.NO_UJIAN}</td>
        <td>{$cmb.NM_C_MHS}</td>
        <td>{$cmb.PRODI1}</td>
        <td>{$cmb.PRODI2}</td>
    </tr>
    {/foreach}
</table>