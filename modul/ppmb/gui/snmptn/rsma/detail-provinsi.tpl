<div class="center_title_bar">Rekap SMA - Detail Provinsi</div>

<style>
.content table tr td a { color: #000; text-decoration: none; }
</style>

<h2>Provinsi {$provinsi.NM_PROVINSI}</h2>

<a href="snmptn-rsma.php">Kembali</a>

<table>
    <tr>
        <th>No</th>
        <th>Kota / Kabupaten</th>
        <th>Peserta</th>
        <th>Masuk Kuota</th>
        <th>% Masuk Kuota</th>
    </tr>
    {$peserta = 0}{$kuota = 0}
    {foreach $kota_set as $k}
    <tr {if $k@index is not div by 2}class="row1"{/if}>
        <td class="center">{$k@index + 1}</td>
        <td><a href="snmptn-rsma.php?mode=detail-kota&id_kota={$k.ID_KOTA}">{$k.NM_KOTA}</a></td>
        <td class="center">{$k.PESERTA}</td>
        <td class="center">{$k.KUOTA}</td>
        <td class="center">{number_format(($k.KUOTA / 1884)*100,2)}%</td>
    </tr>
    {$peserta = $peserta + $k.PESERTA}
    {$kuota = $kuota + $k.KUOTA}
    {/foreach}
    <tr>
        <td colspan="2">Total</td>
        <td class="center">{$peserta}</td>
        <td class="center">{$kuota}</td>
        <td></td>
    </tr>
</table>