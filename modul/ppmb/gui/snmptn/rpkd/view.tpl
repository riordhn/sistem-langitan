<div class="center_title_bar">Rekap PKD</div>

<table>
    <tr>
        <th>No</th>
		<th>No Ujian</th>
        <th>Nama</th>
        <th>Asal Sekolah</th>
        <th>Pilihan 1</th>
        <th>Pilihan 2</th>
    </tr>
    {foreach $cmb_set as $cmb}
	<tr>
		<td>{$cmb@index + 1}</td>
		<td><a href="snmptn-pm.php?no_ujian={$cmb.NO_UJIAN}">{$cmb.NO_UJIAN}</a></td>
		<td>{$cmb.NM_C_MHS}</td>
		<td>{$cmb.NM_SEKOLAH}</td>
		<td>{$cmb.PS1}</td>
		<td>{$cmb.PS2}</td>
	</tr>
    {/foreach}
</table>