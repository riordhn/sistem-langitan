<div class="center_title_bar">Penetapan SNMPTN Proses Ke 2</div>

<style>
	.dipindah td { background-color: #666; }
</style>

<form action="snmptn-penetapan2.php" method="get" id="filter">
<table>
    <tr>
        <td>Program Studi</td>
        <td>
            <select name="id_program_studi" onchange="$('#filter').submit();">
                <option value="">--</option>
            {foreach $program_studi_set as $ps}
                <option value="{$ps.ID_PROGRAM_STUDI}" {if $smarty.get.id_program_studi == $ps.ID_PROGRAM_STUDI}selected="selected"{/if}>{$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI}</option>
            {/foreach}
            </select>
        </td>
    </tr>
</table>
</form>

<table>
    <tr>
        <th>No</th>
        <th>No Ujian</th>
        <th>Nama</th>
        <th>Skor</th>
        <th>Sekolah</th>
		<th>Pilihan 2</th>
    </tr>
    {foreach $cmb_set as $cmb}
    <tr {if $cmb.ID_PILIHAN_1 != $cmb.ID_PILIHAN_PINDAH}class="dipindah"{/if}>
        <td class="center">{$cmb@index + 1}</td>
        <td>{$cmb.NO_UJIAN}</td>
        <td>{$cmb.NM_C_MHS}</td>
        <td class="center">{number_format($cmb.NILAI_TOTAL,2)}</td>
        <td>{$cmb.NM_SEKOLAH}</td>
		<td>{$cmb.PS2}</td>
    </tr>
    {/foreach}
</table>

