{extends file="rekap/index.tpl"}

{block name="title"}{$ps.NM_PROGRAM_STUDI} - {$penerimaan.NM_PENERIMAAN} {$penerimaan.SEMESTER} Gel. {$penerimaan.GELOMBANG} Tahun {$penerimaan.TAHUN}{/block}

{block name="content"}
	
	<div class="center_title_bar">{$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI} - {$penerimaan.NM_PENERIMAAN} {$penerimaan.SEMESTER} Gel. {$penerimaan.GELOMBANG} Tahun {$penerimaan.TAHUN}</div>
	
	<h1 style="margin-bottom: 0">SUBMIT FORM - {$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI}</h1>
	
	<p style="margin-top: 0"><a href="?mode=prodi&id_penerimaan={$penerimaan.ID_PENERIMAAN}">Ke Rekap Prodi</a> / <a href="index.php">Ke Report Registrasi</a></p>
	
	<table class="rekap">
		<thead>
			<tr>
				<th>#</th>
				<th>Nama</th>
				<th>Gelar</th>
				<th>Telp / Email</th>
				<th>Asal S1</th>
				<th>IPK S1</th>
				<th>Asal S2/Pr</th>
				<th>IPK S2/Pr</th>
				<th>Prodi Minat</th>
			</tr>
		</thead>
		<tbody>
			{foreach $cmb_set as $cmb}
				<tr>
					<td>{$cmb@index + 1}</td>
					<td>{$cmb.NM_C_MHS}</td>
					<td>{$cmb.GELAR}</td>
					<td>{$cmb.TELP}<br/><span style="font-size: .9em">{$cmb.EMAIL}</span></td>
					<td>{$cmb.PTN_S1}<br/><span style="font-size: .9em">{$cmb.PRODI_S1}</span></td>
					<td class="center">{$cmb.IP_S1}</td>
					<td>{$cmb.PTN_S2}<br/><span style="font-size: .9em">{$cmb.PRODI_S2}</span></td>
					<td class="center">{$cmb.IP_S2}</td>
					<td>{$cmb.NM_PRODI_MINAT}</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	
{/block}