{extends file="rekap/index.tpl"}

{block name="title"}{$ps.NM_PROGRAM_STUDI} - {$penerimaan.NM_PENERIMAAN} {$penerimaan.SEMESTER} Gel. {$penerimaan.GELOMBANG} Tahun {$penerimaan.TAHUN}{/block}

{block name="content"}
	
	<div class="center_title_bar">{$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI} - {$penerimaan.NM_PENERIMAAN} {$penerimaan.SEMESTER} Gel. {$penerimaan.GELOMBANG} Tahun {$penerimaan.TAHUN}</div>
	
	<h1 style="margin-bottom: 0">ANTRI VERIFIKASI - {$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI}</h1>
	
	<p style="margin-top: 0"><a href="?mode=prodi&id_penerimaan={$penerimaan.ID_PENERIMAAN}">Ke Rekap Prodi</a> / <a href="index.php">Ke Report Registrasi</a></p>
	
	<table class="rekap">
		<thead>
			<tr>
				<th rowspan="2">#</th>
				<th rowspan="2">Nama</th>
				<th rowspan="2">Foto</th>
				<th colspan="{count($syarat_umum_set)}">Syarat Umum</th>
				<th colspan="{count($syarat_prodi_set)}">Syarat Prodi</th>
			</tr>
			<tr>
				{foreach $syarat_umum_set as $su}
				<th title="{$su.NM_SYARAT}">SU{$su@index+1}{if $su.STATUS_WAJIB}*{/if}</th>
				{/foreach}
				{foreach $syarat_prodi_set as $sp}
				<th title="{$sp.NM_SYARAT}">SP{$sp@index+1}{if $sp.STATUS_WAJIB}*{/if}</th>
				{/foreach}
			</tr>
		</thead>
		<tbody>
			{foreach $cmb_set as $cmb}
				<tr>
					<td>{$cmb@index + 1}</td>
					<td>{$cmb.NM_C_MHS}<br/><span style="font-size: .9em">{$cmb.EMAIL}</span></td>
					<td><img style="width: 100px" src="http://pendaftaran.unair.ac.id/files/{$penerimaan.ID_PENERIMAAN}/{$cmb.ID_C_MHS}/{$cmb.FILE_FOTO}" /></td>
					{foreach $syarat_umum_set as $su}
						<td class="center">
						{foreach $data_set as $data}
							{if $data.ID_C_MHS == $cmb.ID_C_MHS and $data.ID_SYARAT_PRODI == $su.ID_SYARAT_PRODI}
								ADA{if $data.IS_VERIFIED},<br/>VRF{/if}
							{/if}
						{/foreach}
						</td>
					{/foreach}
					{foreach $syarat_prodi_set as $sp}
						<td class="center">
						{foreach $data_set as $data}
							{if $data.ID_C_MHS == $cmb.ID_C_MHS and $data.ID_SYARAT_PRODI == $sp.ID_SYARAT_PRODI}
								ADA{if $data.IS_VERIFIED},<br/>VRF{/if}
							{/if}
						{/foreach}
						</td>
					{/foreach}
				</tr>
			{/foreach}
		</tbody>
	</table>
		
		<p style="margin-bottom: 0">Keterangan :</p>
		<ul>
			<li>* : Syarat Wajib</li>
			<li>ADA : Terdapat file yg diupload</li>
			<li>VRF : File sudah terverifikasi</li>
		</ul>
	
{/block}