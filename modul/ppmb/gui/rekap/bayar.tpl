{extends file="rekap/index.tpl"}

{block name="title"}{$ps.NM_PROGRAM_STUDI} - {$penerimaan.NM_PENERIMAAN} {$penerimaan.SEMESTER} Gel. {$penerimaan.GELOMBANG} Tahun {$penerimaan.TAHUN}{/block}

{block name="content"}
	
	<div class="center_title_bar">{$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI} - {$penerimaan.NM_PENERIMAAN} {$penerimaan.SEMESTER} Gel. {$penerimaan.GELOMBANG} Tahun {$penerimaan.TAHUN}</div>
	
	<h1 style="margin-bottom: 0">PEMBAYARAN VOUCHER - {$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI}</h1>
	
	<p style="margin-top: 0"><a href="?mode=prodi&id_penerimaan={$penerimaan.ID_PENERIMAAN}">Ke Rekap Prodi</a> / <a href="index.php">Ke Report Registrasi</a></p>
	
	<table class="rekap">
		<thead>
			<tr>
				<th>#</th>
				<th>Nama</th>
				<th>No Ujian</th>
				<th>Kode Voucher</th>
				<th>Tgl Bayar Voucher</th>
			</tr>
		</thead>
		<tbody>
			{foreach $cmb_set as $cmb}
				<tr>
					<td>{$cmb@index + 1}</td>
					<td>{$cmb.NM_C_MHS}</td>
					<td class="center">{$cmb.NO_UJIAN}</td>
					<td class="center">{$cmb.KODE_VOUCHER}</td>
					<td class="center">{$cmb.TGL_BAYAR|date_format:'%d/%m/%Y %H:%M:%S'}</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	
{/block}