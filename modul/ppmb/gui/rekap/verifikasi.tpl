{extends file="rekap/index.tpl"}

{block name="title"}{$ps.NM_PROGRAM_STUDI} - {$penerimaan.NM_PENERIMAAN} {$penerimaan.SEMESTER} Gel. {$penerimaan.GELOMBANG} Tahun {$penerimaan.TAHUN}{/block}

{block name="content"}
	
	<div class="center_title_bar">{$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI} - {$penerimaan.NM_PENERIMAAN} {$penerimaan.SEMESTER} Gel. {$penerimaan.GELOMBANG} Tahun {$penerimaan.TAHUN}</div>
	
	<h1 style="margin-bottom: 0">VERIFIKASI - {$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI}</h1>
	
	<p style="margin-top: 0"><a href="?mode=prodi&id_penerimaan={$penerimaan.ID_PENERIMAAN}">Ke Rekap Prodi</a> / <a href="index.php">Ke Report Registrasi</a></p>
	
	<table class="rekap">
		<thead>
			<tr>
				<th>#</th>
				<th>Nama</th>
				<th>Syarat Umum</th>
				<th>Syarat Prodi</th>
			</tr>
		</thead>
		<tbody>
			{foreach $cmb_set as $cmb}
				<tr {if $cmb@index is not div by 2}class="row1"{/if}>
					<td>{$cmb@index + 1}</td>
					<td><img style="width: 100px" src="http://pendaftaran.unair.ac.id/files/{$penerimaan.ID_PENERIMAAN}/{$cmb.ID_C_MHS}/{$cmb.FILE_FOTO}" /><br/>
						{$cmb.NM_C_MHS}<br/><span style="font-size: .9em">{$cmb.EMAIL}</span></td>
					<td style="padding: 0">
						<table style="margin-bottom: 0">
							<tbody>
								{foreach $syarat_umum_set as $su}
									{$is_verified = '-'}{$pesan_verifikator = ""}
									<tr>
										<td>{if $su.STATUS_WAJIB == 1}* {/if}{$su.NM_SYARAT}</td>
										<td>
											{foreach $data_set as $data}
												{if $data.ID_C_MHS == $cmb.ID_C_MHS and $data.ID_SYARAT_PRODI == $su.ID_SYARAT_PRODI}
													<a href="http://pendaftaran.unair.ac.id/files/{$penerimaan.ID_PENERIMAAN}/{$cmb.ID_C_MHS}/{$data.FILE_SYARAT}" target="_blank">ADA</a>
													{if $data.IS_VERIFIED}{$is_verified = 'Terverifikasi'}{/if}
													{$pesan_verifikator = $data.PESAN_VERIFIKATOR}
												{/if}
											{/foreach}
										</td>
										<td>
											{$is_verified}
										</td>
										<td>
											<em>{$pesan_verifikator}</em>
										</td>
									</tr>
								{/foreach}
							</tbody>
						</table>
					</td>
					<td style="padding: 0;">
						<table style="margin-bottom: 0">
							<tbody>
								{foreach $syarat_prodi_set as $sp}
									{$is_verified = '-'}{$pesan_verifikator = ""}
									<tr>
										<td>{$sp.NM_SYARAT}</td>
										<td>
											{foreach $data_set as $data}
												{if $data.ID_C_MHS == $cmb.ID_C_MHS and $data.ID_SYARAT_PRODI == $sp.ID_SYARAT_PRODI}
													<a href="http://pendaftaran.unair.ac.id/files/{$penerimaan.ID_PENERIMAAN}/{$cmb.ID_C_MHS}/{$data.FILE_SYARAT}" target="_blank">ADA</a>
													{if $data.IS_VERIFIED}{$is_verified = 'Terverifikasi'}{/if}
													{$pesan_verifikator = $data.PESAN_VERIFIKATOR}
												{/if}
											{/foreach}
										</td>
										<td>
											{$is_verified}
										</td>
										<td>
											<em>{$pesan_verifikator}</em>
										</td>
									</tr>
								{/foreach}
							</tbody>
						</table>
					</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
		
	<p style="margin-bottom: 0">Keterangan :</p>
	<ul>
		<li>* : Syarat Wajib</li>
		<li>ADA : Terdapat file yg diupload</li>
		<li>VRF : File sudah terverifikasi</li>
		<li>CAT : Terdapat catatan dari verifikator</li>
	</ul>
	
{/block}