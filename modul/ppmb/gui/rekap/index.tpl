<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="/css/text.css" />
		<link rel="stylesheet" type="text/css" href="/css/ppmb.css" />
		<style>
			body {
				background-color: #eee;
			}
			table.rekap {
				width: 100%;
			}
			table.rekap > tbody > tr > td > a {
				color: black;
				text-decoration: none;
			}
			table.rekap > tbody > tr > td > a:hover {
				color: black;
				text-decoration: underline;
			}
		</style>
		<title>{block name="title"}{/block}</title>
	</head>
	<body>
		<div class="content">
			{block name="content"}{/block}
		</div>
	</body>
</html>