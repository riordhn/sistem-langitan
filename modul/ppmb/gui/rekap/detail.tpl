<html>
<head>
	<title>Rekap Penerimaan</title>
	<link rel="stylesheet" type="text/css" href="/css/text.css" />
	<link rel="stylesheet" type="text/css" href="/css/ppmb.css" />
</head>
<body>
<div class="content">

<div class="center_title_bar">Detail Peserta {$nm_penerimaan}</div>

{foreach $penerimaan_prodi_set as $pp}
    {if count($pp.cmb_set) > 0}
    <table style="width: 100%">
        <caption>{$pp.NM_PROGRAM_STUDI}</caption>
        
        {* PASCA / SPESIALIS *}
        {if in_array($penerimaan.ID_JALUR, array(6, 23, 24))} 
            <tr>
                <th width="5%">No</th>
                <th width="40%">Nama</th>
                <th width="10%">Gelar</th>
                <th>No Telp</th>
                <th>PT S1</th>
            </tr>
            {foreach $pp.cmb_set as $cmb}
            <tr>
                <td class="center">{$cmb@index+1}</td>
                <td>{$cmb.NM_C_MHS}</td>
                <td>{$cmb.GELAR}</td>
                <td>{$cmb.TELP}</td>
                <td>{$cmb.PTN_S1}</td>
            </tr>
            {foreachelse}
            <tr>
                <td colspan="5" class="center">Tidak ada data</td>
            </tr>
            {/foreach}
        {/if}
        
        {* MANDIRI / DEPAG / INTERNASIONAL *}
        {if $penerimaan.ID_JALUR == 3 or $penerimaan.ID_JALUR == 5 or $penerimaan.ID_JALUR == 20 or $penerimaan.ID_JALUR == 27}
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>No Telp</th>
                <th>Alamat</th>
                <th>Asal Sekolah</th>
            </tr>
            {foreach $pp.cmb_set as $cmb}
            <tr {if $cmb@index is not div by 2}class="row1"{/if}>
                <td class="center">{$cmb@index+1}</td>
                <td>{$cmb.NM_C_MHS}</td>
                <td>{$cmb.TELP}</td>
                <td>{$cmb.ALAMAT} - {$cmb.NM_KOTA}</td>
                <td>{$cmb.NM_SEKOLAH}</td>
            </tr>
            {foreachelse}
            <tr>
                <td colspan="5" class="center">Tidak ada data</td>
            </tr>
            {/foreach}
        {/if}
        
        {* ALIH JENIS *}
        {if $penerimaan.ID_JALUR == 4} 
            <tr>
                <th width="5%">No</th>
                <th width="40%">Nama</th>
                <th>No Telp</th>
                <th title="Perguruan Tinggi D3 Asal">PT D3</th>
                <th>Prodi</th>
            </tr>
            {foreach $pp.cmb_set as $cmb}
            <tr>
                <td class="center">{$cmb@index+1}</td>
                <td>{$cmb.NM_C_MHS}</td>
                <td>{$cmb.TELP}</td>
                <td>{$cmb.PTN_S1}</td>
                <td>{$cmb.PRODI_S1}</td>
            </tr>
            {foreachelse}
            <tr>
                <td colspan="5" class="center">Tidak ada data</td>
            </tr>
            {/foreach}
        {/if}
        
    </table>
    <a href="index.php">Kembali</a><br/><br/>
    {/if}
{/foreach}

</div>
</body>
</html>