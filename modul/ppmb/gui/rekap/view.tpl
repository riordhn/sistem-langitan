{extends file="rekap/index.tpl"}

{block name="title"}Report Registrasi{/block}

{block name="content"}

	<div class="center_title_bar">Report Registrasi</div>

	<table class="rekap">
		<thead>
			<tr>
				<th>Tahun</th>
				<th>Jenjang</th>
				<th>Jalur</th>
				<th>Penerimaan</th>
				<th>Submit Form</th>
				<th title="Antri verifikasi">Antri Verifikasi</th>
				<th>Verifikasi Kembali</th>
				<th title="Verifikasi">Verifikasi</th>
				<th title="Sudah bayar">Bayar</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			{foreach $penerimaan_set as $p}
				<tr {if $p@index is not div by 2}class="row1"{/if}>
					<td class="center">{$p.TAHUN}</td>
					<td class="center">{$p.NM_JENJANG_SNGKAT}</td>
					<td>{$p.NM_JALUR}</td>
					<td>{$p.NM_PENERIMAAN} {$p.SEMESTER} Gel. {$p.GELOMBANG}</td>
					<td class="center">{$p.JUMLAH_SUBMIT_FORM}</td>
					<td class="center">{$p.JUMLAH_ANTRI_VERIFIKASI}</td>
					<td class="center">{$p.JUMLAH_VERIFIKASI_KEMBALI}</td>
					<td class="center">{$p.JUMLAH_VERIFIKASI}</td>
					<td class="center">{$p.JUMLAH_BAYAR}</td>
					<td>
						<a href="index.php?mode=prodi&id_penerimaan={$p.ID_PENERIMAAN}" title="Klik untuk rekap per prodi">Rekap per Prodi</a>
					</td>
				</tr>
			{/foreach}
		</tbody>
	</table>

	{if $show_voucher}
		<table>
			<tr>
				<th>No</th>
				<th>Nama Penerimaan</th>
				<th>Jurusan</th>
				<th>Submit Form</th>
				<th title="Antri verifikasi">A. Verifikasi</th>
				<th title="Verifikasi">Verifikasi</th>
				<th title="Sudah bayar">Bayar</th>
				<th title="Sudah plot ruang">Plot</th>
			</tr>
			{foreach $voucher_set as $v}
				<tr>
					<td>{$v@index + 1}</td>
					<td>{$v.NM_PENERIMAAN}</td>
					<td>{$v.DESKRIPSI}</td>
					<td class="center">{$v.JUMLAH_SUBMIT_FORM}</td>
					<td class="center">{$v.JUMLAH_ANTRI_VERIFIKASI}</td>
					<td class="center">{$v.JUMLAH_VERIFIKASI}</td>
					<td class="center">{$v.JUMLAH_BAYAR}</td>
					<td class="center">{$v.JUMLAH_KARTU}</td>
				</tr>
			{/foreach}
		</table>
	{/if}

	<form action="index.php?mode=logout" method="post"><input type="submit" value="Logout" /></form>
	
{/block}