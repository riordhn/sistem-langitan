{extends file="rekap/index.tpl"}

{block name="title"}{$penerimaan.NM_PENERIMAAN} {$penerimaan.SEMESTER} Gel. {$penerimaan.GELOMBANG} Tahun {$penerimaan.TAHUN}{/block}

{block name="content"}
	
	<div class="center_title_bar">{$penerimaan.NM_PENERIMAAN} {$penerimaan.SEMESTER} Gel. {$penerimaan.GELOMBANG} Tahun {$penerimaan.TAHUN}</div>

	<table class="rekap">
		<thead>
			<tr>
				<th>No</th>
				<th>Fakultas</th>
				<th>Program Studi</th>
				<th>Submit Form</th>
				<th title="Antri verifikasi">Antri Verifikasi</th>
				<th>Verifikasi Kembali</th>
				<th title="Verifikasi">Verifikasi</th>
				<th title="Sudah bayar">Bayar</th>
			</tr>
		</thead>
		<tbody>
			{$submit = 0}{$antri = 0}{$kembali = 0}{$verifikasi = 0}{$bayar = 0}
			{foreach $program_studi_set as $ps}
				<tr {if $ps@index is not div by 2}class="row1"{/if}>
					<td class="center">{$ps@index+1}</td>
					<td>{$ps.NM_FAKULTAS}</td>
					<td>{$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI}</td>
					<td class="center">
						<a href="?mode=submit_form&id_penerimaan={$ps.ID_PENERIMAAN}&id_program_studi={$ps.ID_PROGRAM_STUDI}">{$ps.JUMLAH_SUBMIT_FORM}</a>
					</td>
					<td class="center">
						<a href="?mode=antri_verifikasi&id_penerimaan={$ps.ID_PENERIMAAN}&id_program_studi={$ps.ID_PROGRAM_STUDI}">{$ps.JUMLAH_ANTRI_VERIFIKASI}</a>
					</td>
					<td class="center">
						<a href="?mode=verifikasi_kembali&id_penerimaan={$ps.ID_PENERIMAAN}&id_program_studi={$ps.ID_PROGRAM_STUDI}">{$ps.JUMLAH_VERIFIKASI_KEMBALI}</a>
					</td>
					<td class="center">
						<a href="?mode=verifikasi&id_penerimaan={$ps.ID_PENERIMAAN}&id_program_studi={$ps.ID_PROGRAM_STUDI}">{$ps.JUMLAH_VERIFIKASI}</a>
					</td>
					<td class="center">
						<a href="?mode=bayar&id_penerimaan={$ps.ID_PENERIMAAN}&id_program_studi={$ps.ID_PROGRAM_STUDI}">{$ps.JUMLAH_BAYAR}</a>
					</td>
				</tr>
				{$submit = $submit + $ps.JUMLAH_SUBMIT_FORM}
				{$antri = $antri + $ps.JUMLAH_ANTRI_VERIFIKASI}
				{$kembali = $kembali + $ps.JUMLAH_VERIFIKASI_KEMBALI}
				{$verifikasi = $verifikasi + $ps.JUMLAH_VERIFIKASI}
				{$bayar = $bayar + $ps.JUMLAH_BAYAR}
			{/foreach}
			<tr>
				<td colspan="3">Jumlah</td>
				<td class="center">{$submit}</td>
				<td class="center">{$antri}</td>
				<td class="center">{$kembali}</td>
				<td class="center">{$verifikasi}</td>
				<td class="center">{$bayar}</td>
			</tr>
		</tbody>
	</table>

	<a href="index.php">Kembali</a>

{/block}