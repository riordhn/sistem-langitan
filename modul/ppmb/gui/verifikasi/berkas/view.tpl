<div class="center_title_bar">Verifikasi Berkas Peserta</div>

{literal}
	<style>
		#formulir table tr td { font-size: 13px; }
		#formulir table { width: 95% }
		label.error { color: #f00; font-size: 12px; }
	</style>
{/literal}

<table>
	<tr>
		<td>
			<label>Kode Voucher :</label>
		</td>
		<td>
			<form action="verifikasi-berkas.php" method="get">
				<input type="text" name="kode_voucher" {if isset($smarty.get.kode_voucher)}value="{$smarty.get.kode_voucher}"{/if} />
				<input type="submit" value="Cari..." />
			</form>
		</td>
	</tr>
</table>

{if !empty($cmb)}

	{$pengecekan = false}

	{* Mengecek pengisian formulir *}
	{if $cmb.NM_C_MHS == ''}
		<h2>Peserta belum melakukan registrasi online.</h2>
		{$pengecekan = false}
	{else}
		{$pengecekan = true}
	{/if}

	{* Mengecek isian sekolah D3 / S1 Mandiri *}
	{if $cmb.ID_JALUR == 3 or $cmb.ID_JALUR == 5}
		{if $cmb.ID_SEKOLAH_ASAL == ''}
			<h2>Peserta belum mengisi asal sekolah pada registrasi online.</h2>
			<h2>Harap menghubungi panitia pendaftaran.</h2>
			{$pengecekan = false}
		{else}
			{$pengecekan = true}
		{/if}
	{/if}

	{* Mengecek prodi minat yang kosong *}
	{if $cmb.ID_JALUR == 6 or $cmb.ID_JALUR == 23}
		{if $jumlah_prodi_minat > 0 and $cmb.ID_PRODI_MINAT == ''}
			<h2>Peserta belum melakukan pemilihan prodi minat.</h2>
			<h2>Harap menghubungi panitia pendaftaran.</h2>
			{$pengecekan = false}
		{/if}
	{/if}

	{* Pengecekan master biaya yang kosong *}
	{if $cmb.ID_JALUR == 6 or $cmb.ID_JALUR == 23 or $cmb.ID_JALUR == 24}
		{if count($kelompok_biaya_set) == 0}
			<h2>Verifikasi peserta untuk saat ini tidak bisa dilakukan karena master biaya belum di set oleh bagian Keuangan</h2>
			{$pengecekan = false}
		{/if}
	{/if}

	{if $pengecekan == true}
		{if $cmb.ID_JALUR == 3 or $cmb.ID_JALUR == 5}
			{if $cmb.KODE_JURUSAN == 1}
				{$nama_jurusan = 'IPA'}
			{/if}
			{if $cmb.KODE_JURUSAN == 2}
				{$nama_jurusan = 'IPS'}
			{/if}
			{if $cmb.KODE_JURUSAN == 3}
				{$nama_jurusan = 'IPC'}
			{/if}
		{/if}
		<h2>Jalur Penerimaan : {$cmb.NM_JALUR} - {$cmb.NM_PENERIMAAN} {$nama_jurusan}</h2>
		<h2>Nomer Ujian : {$cmb.NO_UJIAN}</h2>
		<form action="verifikasi-berkas.php?kode_voucher={$cmb.KODE_VOUCHER}" method="post" id="formulir" autocomplete="off">
			<input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
			<input type="hidden" name="kode_jalur" value="{$cmb.KODE_JURUSAN}" />
			<input type="hidden" name="mode" value="save_verifikasi" />
			<table>
				<tr>
					<td colspan="2" align="center" bgcolor="#233D0E"><font color="#FFFFFF"><b>I. Data Diri</b></font></td>
				</tr>
				<tr>
					<td>Nama</td>
					<td>{$cmb.NM_C_MHS}</td>
				</tr>
				<tr>
					<td colspan="2" align="center" bgcolor="#233D0E"><font color="#FFFFFF"><b>II. Program Studi Pilihan</b></font></td>
				</tr>
				<tr>
					<td>Prodi Pilihan 1</td>
					<td>{$cmb.PRODI1}</td>
				</tr>
				{if $cmb.KELAS_PILIHAN != ''}
					<tr>
						<td>Kelas Pilihan</td>
						<td>{$cmb.NM_PRODI_KELAS}</td>
					</tr>
				{/if}
				{if $cmb.ID_JALUR == 3 or $cmb.ID_JALUR == 4 or $cmb.ID_JALUR == 5} {* MANDIRI / ALIH-JENIS / D3 *}
						{if $cmb.ID_PILIHAN_2}
							<tr>
								<td>Prodi Pilihan 2</td>
								<td>{$cmb.PRODI2}</td>
							</tr>
						{/if}
						{if $cmb.ID_PILIHAN_3}
							<tr>
								<td>Prodi Pilihan 3</td>
								<td>{$cmb.PRODI3}</td>
							</tr>
						{/if}
						{if $cmb.ID_PILIHAN_4}
							<tr>
								<td>Prodi Pilihan 4</td>
								<td>{$cmb.PRODI4}</td>
							</tr>
						{/if}
					{/if}
					<tr>
						<td colspan="2" align="center" bgcolor="#233D0E"><font color="#FFFFFF"><b>III. Kesanggupan SP3</b></font></td>
					</tr>
					<tr>
						<td>SP3 untuk Prodi Pilihan 1</td>
						<td>
							<div>
								<input type="text" id="sp3_1" name="sp3_1" value="{$cmb.SP3_1}" style="font-size: 150%"/>
							</div>
							<label class="info_sp3">Minimal: <span id="sp3_1_min_view">{number_format($sp3_1_min, 0, ",", ".")}</span>, <span id="status_sp3_1"></span></label>
							<input type="hidden" id="sp3_1_min" value="{$sp3_1_min}" />
						</td>
					</tr>
					{if $cmb.ID_JALUR == 3 or $cmb.ID_JALUR == 4 or $cmb.ID_JALUR == 5}
						{if $cmb.ID_PILIHAN_2}
							<tr>
								<td>UKA untuk Prodi Pilihan 2</td>
								<td>
									<div>
										<input type="text" id="sp3_2" name="sp3_2" value="{$cmb.SP3_2}" style="font-size: 150%"/>
									</div>
									<label class="info_sp3" name="sp3_2_min_view">Minimal: {number_format($sp3_2_min, 0, ",", ".")}, <span id="status_sp3_2"></span></label>
									<input type="hidden" id="sp3_2_min" value="{$sp3_2_min}" />
								</td>
							</tr>
						{/if}
						{if $cmb.ID_PILIHAN_3}
							<tr>
								<td>UKA untuk Prodi Pilihan 3</td>
								<td>
									<div>
										<input type="text" id="sp3_3" name="sp3_3" value="{$cmb.SP3_3}" style="font-size: 150%"/>
									</div>
									<label class="info_sp3" name="sp3_3_min_view">Minimal: {number_format($sp3_3_min, 0, ",", ".")}, <span id="status_sp3_3"></span></label>
									<input type="hidden" id="sp3_3_min" value="{$sp3_3_min}" />
								</td>
							</tr>
						{/if}
						{if $cmb.ID_PILIHAN_4}
							<tr>
								<td>UKA untuk Prodi Pilihan 4</td>
								<td>
									<div>
										<input type="text" id="sp3_4" name="sp3_4" value="{$cmb.SP3_4}" style="font-size: 150%"/>
									</div>
									<label class="info_sp3" name="sp3_4_min_view">Minimal: {number_format($sp3_4_min, 0, ",", ".")}, <span id="status_sp3_4"></span></label>
									<input type="hidden" id="sp3_4_min" value="{$sp3_4_min}" />
								</td>
							</tr>
						{/if}
					{else}
						<tr>
							<td>Kelompok Biaya</td>
							<td>
								<select name="id_kelompok_biaya">
									<option value=""></option>
									{foreach $kelompok_biaya_set as $kb}
										<option value="{$kb.ID_KELOMPOK_BIAYA}" {if $cmb['ID_KELOMPOK_BIAYA']==$kb.ID_KELOMPOK_BIAYA}selected="selected"{/if}>{$kb.NM_KELOMPOK_BIAYA}</option>
									{/foreach}
								</select>
								<input type="hidden" name="cmb_id_program_studi" value="{$cmb.ID_PILIHAN_1}" />
								<input type="hidden" name="cmb_id_jalur" value="{$cmb.ID_JALUR}" />
								<input type="hidden" name="cmb_id_semester" value="{$cmb.ID_SEMESTER}" />
								<input type="hidden" name="is_matrikulasi" value="0" />
							</td>
						</tr>
						<!--
						<tr>
							<td>Matrikulasi</td>
							<td><select name="is_matrikulasi" class="required">
									<option value=""></option>
						{foreach $matrikulasi_set as $m}
							<option value="{$m.VALUE}" {if $m.VALUE == $cmb.IS_MATRIKULASI}selected="selected"{/if}>{$m.VIEW}</option>
						{/foreach}
						</select>
					</td>
				</tr>
						-->
					{/if}
					<tr>
						<td colspan="2" align="center" bgcolor="#233D0E"><font color="#FFFFFF"><b>IV. Berkas Verifikasi &amp; Syarat</b></font></td>
					</tr>
					<!--
					<tr>
						<td colspan="2">
							<label><input type="radio" name="berkas_ijazah" value="2" {if $cmb['BERKAS_IJAZAH']==2}checked="checked"{/if}/>Surat Keterangan</label>
							<label><input type="radio" name="berkas_ijazah" value="1" {if $cmb['BERKAS_IJAZAH']==1}checked="checked"{/if}/>Ijazah</label>
							<label><input type="radio" name="berkas_ijazah" value="0" {if $cmb['BERKAS_IJAZAH']==0}checked="checked"{/if}/>SKL (Surat Keterangan Lulus)</label>
							<label for="berkas_ijazah" class="error" style="display: none">Harus ada</label>
						</td>
					</tr>
					-->
					<tr style="display: none">
						<td colspan="2">
							<label><input type="checkbox" name="berkas_skhun" {if $cmb['BERKAS_SKHUN']==1}checked="checked"{/if}/>SKHUN (Surat Keterangan Hasil Ujian Nasional)</label>
							<label for="berkas_skhun" class="error" style="display: none">Harus ada</label>
						</td>
					</tr>
					<tr style="display: none">
						<td colspan="2">
							<label><input type="checkbox" name="berkas_kesanggupan_sp3" {if $cmb['BERKAS_KESANGGUPAN_SP3']==1}checked="checked"{/if}/>Berkas Kesanggupan SP3</label>
							<label for="berkas_kesanggupan_sp3" class="error" style="display: none">Harus ada</label>
						</td>
					</tr>
					<tr style="display: none">
						<td colspan="2" >
							<label style="vertical-align: top"><input type="checkbox" name="berkas_lain" {if $cmb['BERKAS_LAIN']==1}checked="checked"{/if} style="vertical-align: top"/>Lain-lain</label>
							<!--<input type="text" name="berkas_lain_ket" maxlength="30" value="{$cmb['BERKAS_LAIN_KET']}" />-->
							<textarea name="berkas_lain_ket" cols="75" rows="2" style="font-family: 'Trebuchet MS'">{$cmb['BERKAS_LAIN_KET']}</textarea>
						</td>
					</tr>
					{foreach $syarat_set as $syarat}
						<tr>
							<td colspan="2">
								<label><input type="checkbox" name="syarat_prodi_{$syarat.ID_SYARAT_PRODI}" {if $syarat.STATUS_WAJIB == 1}class="required"{/if} {if $syarat.CHECKED}checked{/if}/> {if !empty($syarat.ID_PROGRAM_STUDI)}<strong>{$syarat.NM_PROGRAM_STUDI}</strong> - {/if}{$syarat.NM_SYARAT} </label>
							</td>
						</tr>
					{/foreach}
					<tr>
						<td colspan="2" style="text-align: center">
							<input type="submit" value="Simpan" />
						</td>
					</tr>
				</table>
			</form>

			<script type="text/javascript">

				jQuery(document).ready(function() {

					function formatCurrency(num) {

						if (num == null) {
							return '0';
						}

						num = num.toString().replace(/\\$|\\,/g, '');
						if (isNaN(num))
							num = '0';
						sign = (num == (num = Math.abs(num)));
						num = Math.floor(num * 100 + 0.50000000001);
						cents = num % 100;
						num = Math.floor(num / 100).toString();
						if (cents < 10)
							cents = '0' + cents;

						for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
							num = num.substring(0, num.length - (4 * i + 3)) + '.' + num.substring(num.length - (4 * i + 3));

						return (((sign) ? '' : '-') + '' + num);
					}

					function sp3_validate(source, min, display) {
						var sp3 = $(source).val();
						var min = $(min).val();

						var jumlah = sp3 - min;

						$(source).rules('remove');

						$(source).rules('add', {
							min: min,
							max: min,
							required: true,
							digits: true,
							messages: {
								required: 'Nilai UKA harus di isi',
								digits: 'Masukkan format angka saja',
								min: 'Isian harus = ' + formatCurrency(min),
								max: 'Isian harus = ' + formatCurrency(min)
							}
						});

						if (jumlah == 0)
							$(display).html('<font style="color: #080;">Isian sama</font>');
						else if (jumlah > 0)
							$(display).html('<font style="color: #00f;">Isian lebih : ' + formatCurrency(jumlah) + '</font>');
						else if (jumlah < 0)
							$(display).html('<font style="color: #f00;">Isian kurang : ' + formatCurrency(Math.abs(jumlah)) + '</font>');
					}

					$('#sp3_1').bind('keyup', function() {
						sp3_validate('#sp3_1', '#sp3_1_min', '#status_sp3_1');
					});

					$('#sp3_2').bind('keyup', function() {
						sp3_validate('#sp3_2', '#sp3_2_min', '#status_sp3_2');
					});

					$('#sp3_3').bind('keyup', function() {
						sp3_validate('#sp3_3', '#sp3_3_min', '#status_sp3_3');
					});

					$('#sp3_4').bind('keyup', function() {
						sp3_validate('#sp3_4', '#sp3_4_min', '#status_sp3_4');
					});
					
					// kelompok biaya di rubah
					$('select[name="id_kelompok_biaya"]').change(function() {
						var id_program_studi = $('input[name="cmb_id_program_studi"]').val();
						var id_jalur = $('input[name="cmb_id_jalur"]').val();
						var id_semester = $('input[name="cmb_id_semester"]').val();
						var id_kelompok_biaya = this.value;

						// alert('mode=get-sp3&id_program_studi='+id_program_studi+'&id_jalur='+id_jalur+'&id_semester=5&id_kelompok_biaya='+id_kelompok_biaya);
						
						$.ajax({
							url: 'verifikasi-berkas-getSP3.php',
							data: 'id_program_studi='+id_program_studi+'&id_jalur='+id_jalur+'&id_kelompok_biaya='+id_kelompok_biaya+'&id_semester='+id_semester,
							dataType: 'json',
							success: function(r) {
								// Nilai r
								$('#sp3_1_min').val(r);
								$('#sp3_1_min_view').html(formatCurrency(r));
								$('#sp3_1').trigger('keyup');
							}
						});
					});

					$('#formulir').validate({
						rules: {
							id_kelompok_biaya: {
								required: true
							}
						},
						messages: {
							id_kelompok_biaya: {
								required: 'Tentukan kelompok biaya yang sesuai'
							}
						},
						errorPlacement: function(error, element) {
							error.appendTo(element.parent().last());
						}
					});
				});

			</script>

			<!--
			{*
			<script type="text/javascript">
		
			function formatCurrency(num) {
				
			if (num == null) { return '0'; }
			
			num = num.toString().replace(/\\$|\\,/g,'');
			if (isNaN(num)) num = '0';
			sign = (num == (num = Math.abs(num)));
			num = Math.floor(num*100+0.50000000001);
			cents = num%100;
			num = Math.floor(num/100).toString();
			if (cents < 10) cents = '0' + cents;
	
			for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
			num = num.substring(0,num.length-(4*i+3))+'.'+ num.substring(num.length-(4*i+3));
	
			return (((sign)?'':'-') + '' + num);
			}
			
			function refreshSP3()
			{
			$('#formulir').validate({
			rules: {
			id_kelompok_biaya : { required: true },
			sp3_1: { required: true, number: true, min: $('#sp3_1_min').val() },
			sp3_2: { required: true, number: true, min: $('#sp3_2_min').val() },
			sp3_3: { required: true, number: true, min: $('#sp3_3_min').val() },
			sp3_4: { required: true, number: true, min: $('#sp3_4_min').val() }
			},
			messages: {
			id_kelompk_biaya : { required: 'Tentukan kelompok biaya yang sesuai' },
			sp3_1: { required: 'Masih kosong', number: 'Masukan hanya angka', min: 'Minimal ' + formatCurrency($('#sp3_1_min').val()) },
			sp3_2: { required: 'Masih kosong', number: 'Masukan hanya angka', min: 'Minimal ' + formatCurrency($('#sp3_2_min').val()) },
			sp3_3: { required: 'Masih kosong', number: 'Masukan hanya angka', min: 'Minimal ' + formatCurrency($('#sp3_3_min').val()) },
			sp3_4: { required: 'Masih kosong', number: 'Masukan hanya angka', min: 'Minimal ' + formatCurrency($('#sp3_4_min').val()) }
			}
			});
			}
		
			$(document).ready(function() {
			
			// kelompok biaya di rubah
			$('select[name="id_kelompok_biaya"]').change(function() {
			var id_program_studi = $('input[name="cmb_id_program_studi"]').val();
			var id_jalur = $('input[name="cmb_id_jalur"]').val();
			var id_semester = $('input[name="cmb_id_semester"]').val();
			var id_kelompok_biaya = this.value;
	
			//alert('id_program_studi='+id_program_studi+'&id_jalur='+id_jalur+'&id_semester=5&id_kelompok_biaya='+id_kelompok_biaya);
			$.ajax({
			url: 'verifikasi-berkas-getSP3.php',
			data: 'id_program_studi='+id_program_studi+'&id_jalur='+id_jalur+'&id_kelompok_biaya='+id_kelompok_biaya+'&id_semester='+id_semester,
			dataType: 'json',
			success: function(r) {
			$('#sp3_1_min').val(r);
			$('label[name="sp3_1_min_view"]').html('Minimal: ' + formatCurrency(r) + ', <span id="status_sp3_1"></span>');
			refreshSP3();
			}
			});
			});
	
			$('#sp3_1').live('keyup', function() {
				
			var sp3 = $('#sp3_1').val();
			var min = $('#sp3_1_min').val();
			
			var jumlah = sp3 - min;
	
			if (jumlah == 0)
			$('#status_sp3_1').html('<font style="color: #080;">Isian sama</font>');
			else if (jumlah > 0)
			$('#status_sp3_1').html('<font style="color: #00f;">Isian lebih : ' + formatCurrency(jumlah) + '</font>');
			else if (jumlah < 0)
			$('#status_sp3_1').html('<font style="color: #f00;">Isian kurang : ' + formatCurrency(Math.abs(jumlah)) + '</font>');
			});
	
			$('#sp3_2').live('keyup', function() {
			var sp3 = $('#sp3_2').val();
			var min = $('#sp3_2_min').val();
	
			var jumlah = sp3 - min;
	
			if (jumlah == 0)
			$('#status_sp3_2').html('<font style="color: #080;">Isian sama</font>');
			else if (jumlah > 0)
			$('#status_sp3_2').html('<font style="color: #00f;">Isian lebih : ' + formatCurrency(jumlah) + '</font>');
			else if (jumlah < 0)
			$('#status_sp3_2').html('<font style="color: #f00;">Isian kurang : ' + formatCurrency(Math.abs(jumlah)) + '</font>');
			});
	
			$('#sp3_3').live('keyup', function() {
			var sp3 = $('#sp3_3').val();
			var min = $('#sp3_3_min').val();
	
			var jumlah = sp3 - min;
	
			if (jumlah == 0)
			$('#status_sp3_3').html('<font style="color: #080;">Isian sama</font>');
			else if (jumlah > 0)
			$('#status_sp3_3').html('<font style="color: #00f;">Isian lebih : ' + formatCurrency(jumlah) + '</font>');
			else if (jumlah < 0)
			$('#status_sp3_3').html('<font style="color: #f00;">Isian kurang : ' + formatCurrency(Math.abs(jumlah)) + '</font>');
			});
	
			$('#sp3_4').live('keyup', function() {
			var sp3 = $('#sp3_4').val();
			var min = $('#sp3_4_min').val();
	
			var jumlah = sp3 - min;
	
			if (jumlah == 0)
			$('#status_sp3_4').html('<font style="color: #080;">Isian sama</font>');
			else if (jumlah > 0)
			$('#status_sp3_4').html('<font style="color: #00f;">Isian lebih : ' + formatCurrency(jumlah) + '</font>');
			else if (jumlah < 0)
			$('#status_sp3_4').html('<font style="color: #f00;">Isian kurang : ' + formatCurrency(Math.abs(jumlah)) + '</font>');
			});
	
			
			});
			</script>
			*}
			-->
			<p>- Nomer Ujian akan muncul setelah tombol simpan di klik.</p>
		{/if}
	{/if}