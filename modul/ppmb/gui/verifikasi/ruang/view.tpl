<div class="center_title_bar">Jadwal Tes {$data_penerimaan_by_id['NM_PENERIMAAN']} Gelombang {$data_penerimaan_by_id['GELOMBANG']} Tahun {$data_penerimaan_by_id['TAHUN']}</div>

<table>
	<thead>
		<tr>
			<th>IPA</th>
			<th>IPS</th>
			<th>IPC</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>{$voucher.IPA}</td>
			<td>{$voucher.IPS}</td>
			<td>{$voucher.IPC}</td>
		</tr>
	</tbody>
</table>
<table style="width: 100%">
    <tr>
        <th class="center" style="width: 5%">No</th>
        <th>Ruang</th>
        <!--<th>No.Peserta</th>-->
        <!--<th width="100px">Lokasi</th>-->
        <!--<th width="50px">Ruang</th>-->
        <!--<th width="50px">Jadwal</th>-->
        <th title="Terisi / Kapasitas">Trs / Kap.</th>
        <th>Jalur</th>
        <th>Status</th>
        <th>Operasi</th>
    </tr>
    {$index=1}
    {foreach $data_jadwal_ppmb as $jadwal_ppmb}
		<tr {if $jadwal_ppmb['STATUS_AKTIF'] == 1}{if $jadwal_ppmb['DIISI']<$jadwal_ppmb['KAPASITAS'] && $jadwal_ppmb['NOMER_AWAL'] != ''}style="background-color: #dfd;"{elseif $jadwal_ppmb['DIISI']==$jadwal_ppmb['KAPASITAS']}style="background-color: #fdd;"{/if}{/if}>
			<td class="center">{$index++}</td>
			<td>
				Nomer : <span>{$jadwal_ppmb['NOMER_AWAL']}-{$jadwal_ppmb['NOMER_AKHIR']}</span><br/>
				Ruang : <span><strong>{$jadwal_ppmb['NM_RUANG']}</strong></span><br/>
				Lokasi : <span> / {$jadwal_ppmb['LOKASI']}</span><br/>
				Tanggal : <span>{$jadwal_ppmb['TGL_TEST']|date_format:"%d-%m-%y"}</span>
			</td>
			<!--<td>{$jadwal_ppmb['NOMER_AWAL']}-{$jadwal_ppmb['NOMER_AKHIR']}</td>-->
			<!--<td style="font-size: 11px">{$jadwal_ppmb['LOKASI']}<br/>{$jadwal_ppmb['ALAMAT']}</td>-->
			<!--<td class="center">{$jadwal_ppmb['NM_RUANG']}</td>-->
			<!--<td>{$jadwal_ppmb['TGL_TEST']|date_format:"%d-%m-%y"}</td>-->
			<td class="center">{$jadwal_ppmb['DIISI']} / {$jadwal_ppmb['KAPASITAS']}</td>
			<td class="center">
				{if $jadwal_ppmb['KODE_JALUR']=='1'}
					IPA 
				{else if $jadwal_ppmb['KODE_JALUR']=='2'} 
					IPS 
				{else if $jadwal_ppmb['KODE_JALUR']=='3'} 
					IPC 
				{else if $jadwal_ppmb['KODE_JALUR']=='7'} 
					DEPAG 
				{/if}
			</td>
			<td class="center">
				{if $jadwal_ppmb['STATUS_AKTIF']=='0'} 
					Tidak Aktif 
				{else if $jadwal_ppmb['STATUS_AKTIF']=='1'} 
					Aktif
				{/if}
			</td>
			<td>
				{if $jadwal_ppmb['STATUS_AKTIF']=='0'}
					<a href="verifikasi-ruang.php?mode=edit&id_jadwal={$jadwal_ppmb['ID_JADWAL_PPMB']}&id_penerimaan={$smarty.get.id_penerimaan}">Edit</a> |
					<a href="verifikasi-ruang.php?mode=delete&id_jadwal={$jadwal_ppmb['ID_JADWAL_PPMB']}&id_penerimaan={$smarty.get.id_penerimaan}">Delete</a> |
					<a href="verifikasi-ruang.php?mode=aktifkan&id_jadwal={$jadwal_ppmb['ID_JADWAL_PPMB']}&id_penerimaan={$smarty.get.id_penerimaan}">Aktifkan</a> |
				{else}
                    {if $jadwal_ppmb['TERISI'] == 0}
                    <a href="verifikasi-ruang.php?mode=non-aktifkan&id_jadwal={$jadwal_ppmb['ID_JADWAL_PPMB']}&id_penerimaan={$smarty.get.id_penerimaan}">Non-Aktifkan</a> |
                    {/if}
					<a href="verifikasi-ruang.php?mode=edit-aktif&id_jadwal={$jadwal_ppmb['ID_JADWAL_PPMB']}&id_penerimaan={$smarty.get.id_penerimaan}">Edit Aktif</a> |
				{/if}<br/>
                <a href="verifikasi-ruang.php?mode=pengawas&id_jadwal={$jadwal_ppmb['ID_JADWAL_PPMB']}&id_penerimaan={$smarty.get.id_penerimaan}">Pengawas</a>
			</td>
		</tr>     
    {/foreach}
    <tr>
        <td colspan="11" class="center">
            <a href="verifikasi-ruang.php">Kembali</a>
            <a href="verifikasi-ruang.php?mode=add&id_penerimaan={$smarty.get.id_penerimaan}">Tambah</a>
                <!--<input type="button" value="Cetak Daftar Pengawas"
                       onclick="javascript:window.open('verifikasi-cetak-pengawas.php?id_penerimaan={$smarty.get.id_penerimaan}');">-->
        </td>
    </tr>
</table>