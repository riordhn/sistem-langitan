<div class="center_title_bar">Jadwal Tes PPMB - Tambah</div>

<form id="form_add_jadwal_ppmb" action="verifikasi-ruang.php?mode=view&id_penerimaan={$smarty.get.id_penerimaan}" method="post">
    <input type="hidden" name="id_penerimaan" value="{$smarty.get.id_penerimaan}"/>
    <input type="hidden" name="mode" value="add" />
    <table>
        <tr>
            <th colspan="2">Detail Pembukaan Ruangan</th>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>
                <select name="id_fakultas">
                    <option value="NULL">--</option>
                {foreach $fakultas_set as $f}
                    <option value="{$f.ID_FAKULTAS}" {if $f.ID_FAKULTAS == $data_jadwal_ppmb_by_id.ID_FAKULTAS}selected="selected"{/if}>{$f.NM_FAKULTAS}</option>
                {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Lokasi</td>
            <td>
                <input type="text" name="lokasi" class="required" size="50" maxlength="250"/>
            </td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>
                <input type="text" name="alamat" class="required" size="75" maxlength="250"/>
            </td>
        </tr>
        <tr>
            <td>Ruang</td>
            <td>
                <input type="text" name="ruang" class="required" size="25" maxlength="100"/>
            </td>
        </tr>
        <tr>
            <td>Tanggal Ujian</td>
            <td>
                {html_select_date prefix="tgl_test_" field_order="DMY" end_year="+10"}
            </td>
        </tr>
        <tr>
            <td>Tanggal Ujian 2</td>
            <td>
                {html_select_date prefix="tgl_test2_" field_order="DMY" end_year="+10"}
                <label title="Centang ini jika test berlangsung dua hari">
                    <input type="checkbox" name="dua_hari_test"/> Dua Hari
                </label>
            </td>
        </tr>
		<tr>
            <td>Tanggal Ujian 3</td>
            <td>
                {html_select_date prefix="tgl_test3_" field_order="DMY" end_year="+10"}
                <label title="Centang ini jika test berlangsung tiga hari">
                    <input type="checkbox" name="tiga_hari_test"/> Tiga Hari
                </label>
            </td>
        </tr>
        <tr>
            <td>Waktu</td>
            <td><input type="text" name="waktu" size="20" maxlength="50" class="required" /></td>
        </tr>
        <tr>
            <td>Kapasitas</td>
            <td><input type="text" name="kapasitas" size="4" maxlength="4" class="required number"/></td>
        </tr>
        <tr>
            <td>Nomer Awal</td>
            <td>
                <input type="text" name="nomer_awal" remote="cek-nomer-awal.php" class="required"/>
                <br/>
                <label style="font-size: smaller">Nomer sebelumnya : {$no_ujian_akhir}</label>
            </td>
        </tr>
        <tr>
            <td>Jalur</td>
            <td>
                <select name="kode_jalur">
                    <option value="0">Kosongi</option>
                    <option value="1">IPA</option>
                    <option value="2">IPS</option>
                    <option value="3">IPC</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Materi Ujian</td>
            <td>
                <input type="text" name="materi" class="required" size="50" maxlength="50" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <a class="button" href="verifikasi-ruang.php?mode=view&id_penerimaan={$smarty.get.id_penerimaan}">Batal</a>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>

{literal}
<script type="text/javascript">
    $('#form_add_jadwal_ppmb').validate();
</script>
{/literal}