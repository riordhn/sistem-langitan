<div class="center_title_bar">Pengawas Ruang {$jadwal_ppmb.NM_RUANG} - Edit</div>

{if $edited}<h3>{$edited}</h3>{/if}

<form action="verifikasi-ruang.php?mode=edit-pengawas&id_pengawas={$smarty.get.id_pengawas}" method="post">
    <input type="hidden" name="mode" value="edit-pengawas" />
    <input type="hidden" name="id_pengawas" value="{$pu.ID_PENGAWAS_UJIAN_PPMB}" />
    <table>
        <tr>
            <th colspan="2">Detail Pengawas</th>
        </tr>
        <tr>
            <td>Nama Pengawas 1</td>
            <td><input type="text" name="pengawas1" size="30" maxlength="30" value="{$pu.PENGAWAS1}" /></td>
        </tr>
        <tr>
            <td>Status Pengawas 1</td>
            <td><select name="status1">
                    <option value="">-</option>
                    <option value="KR" {if $pu.STATUS1 == 'KR'}selected="selected"{/if}>KR - Kepala Ruangan</option>
                    <option value="P" {if $pu.STATUS1 == 'P'}selected="selected"{/if}>P - Petugas Ruangan</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Nama Pengawas 2</td>
            <td><input type="text" name="pengawas2" size="30" maxlength="30" value="{$pu.PENGAWAS2}" /></td>
        </tr>
        <tr>
            <td>Status Pengawas 2</td>
            <td><select name="status2">
                    <option value="">-</option>
                    <option value="KR" {if $pu.STATUS2 == 'KR'}selected="selected"{/if}>KR - Kepala Ruangan</option>
                    <option value="P" {if $pu.STATUS2 == 'P'}selected="selected"{/if}>P - Petugas Ruangan</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Nama Pengawas 3</td>
            <td><input type="text" name="pengawas3" size="30" maxlength="30" value="{$pu.PENGAWAS3}" /></td>
        </tr>
        <tr>
            <td>Status Pengawas 3</td>
            <td><select name="status3">
                    <option value="">-</option>
                    <option value="KR" {if $pu.STATUS3 == 'KR'}selected="selected"{/if}>KR - Kepala Ruangan</option>
                    <option value="P" {if $pu.STATUS3 == 'P'}selected="selected"{/if}>P - Petugas Ruangan</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Nama Pengawas 4</td>
            <td><input type="text" name="pengawas4" size="30" maxlength="30" value="{$pu.PENGAWAS4}" /></td>
        </tr>
        <tr>
            <td>Status Pengawas 4</td>
            <td><select name="status4">
                    <option value="">-</option>
                    <option value="KR" {if $pu.STATUS4 == 'KR'}selected="selected"{/if}>KR - Kepala Ruangan</option>
                    <option value="P" {if $pu.STATUS4 == 'P'}selected="selected"{/if}>P - Petugas Ruangan</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Nomer Awal </td>
            <td><select name="nomer_awal">
                    <option value=""></option>
                {for $i=$jadwal_ppmb.NOMER_AWAL to $jadwal_ppmb.NOMER_AKHIR}
                    <option value="{$i}" {if $i == $pu.NOMER_AWAL}selected="selected"{/if}>{$i}</option>
                {/for}
                </select>
            </td>
        </tr>
        <tr>
            <td>Nomer Akhir</td>
            <td><select name="nomer_akhir">
                    <option value=""></option>
                {for $i=$jadwal_ppmb.NOMER_AWAL to $jadwal_ppmb.NOMER_AKHIR}
                    <option value="{$i}" {if $i == $pu.NOMER_AKHIR}selected="selected"{/if}>{$i}</option>
                {/for}
                </select>
            </td>
        </tr>
        <tr>
            <td>Amplop 1</td>
            <td><input type="text" name="amplop1" size="30" maxlength="30" value="{$pu.AMPLOP1}" /></td>
        </tr>
        <tr>
            <td>Amplop 2</td>
            <td><input type="text" name="amplop2" size="30" maxlength="30" value="{$pu.AMPLOP2}"/></td>
        </tr>
        <tr>
            <td>Distributor Naskah</td>
            <td><input type="text" name="distributor" size="30" maxlength="30" value="{$pu.DISTRIBUTOR}"/></td>
        </tr>
        <tr>
            <td>Koordinator</td>
            <td><input type="text" name="koordinator" size="30" maxlength="30" value="{$pu.KOORDINATOR}"/></td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <a href="verifikasi-ruang.php?mode=pengawas&id_jadwal={$jadwal_ppmb.ID_JADWAL_PPMB}&id_penerimaan={$jadwal_ppmb.ID_PENERIMAAN}">Kembali</a>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>