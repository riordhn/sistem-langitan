<div class="center_title_bar">Pembukaan Ruang</div>

<table class="filter">
    <tr>
        <th>Tahun</th>
        <th>Jenjang</th>
		<th>Penerimaan</th>
        <th>Gel</th>
        <th>Semester</th>
        <th>Status Aktif</th>
        <th>Detail</th>
    </tr>
    {foreach $data_penerimaan as $data}
    <tr {if $data@index is div by 2}class="row1"{/if}>
        <td class="center">{$data.TAHUN}</td>
        <td class="center">{$data.NM_JENJANG}</td>
        <td>{$data.NM_PENERIMAAN}</td>
        <td class="center">{$data.GELOMBANG}</td>
        <td class="center">{$data.SEMESTER}</td>
        <td class="center">{if $data.IS_AKTIF==1}Aktif{else}Tidak Aktif{/if}</td>
        <td>
            <a href="verifikasi-ruang.php?mode=view&id_penerimaan={$data.ID_PENERIMAAN}">Detail</a>
        </td>
    </tr>
    {/foreach}
    <tr>
        <td class="center" colspan="7"><a href="verifikasi-cetak-pengawas.php" class="disable-ajax">Cetak Semua Daftar Pengawas</a></td>
    </tr>
</table>