<div class="center_title_bar">Jadwal Tes PPMB - Edit</div>

{if $result}
<div class="ui-widget" style="margin-bottom: 20px">
    <div class="ui-state-highlight ui-corner-all"> 
        <div style="margin: 10px"><span class="ui-icon ui-icon-info" style="float: left; margin-right: 5px"></span>{$result}</div>
    </div>
</div>
{/if}

<form id="form_edit_jawdal_ppmb" action="verifikasi-ruang.php?{$smarty.server.QUERY_STRING}" method="post">
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="id_jadwal" value="{$data_jadwal_ppmb_by_id['ID_JADWAL_PPMB']}" />
    <table>
        <tr>
            <th colspan="2">Detail Pembukaan Ruangan</th>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>
                <select name="id_fakultas">
                    <option value="">--</option>
                {foreach $fakultas_set as $f}
                    <option value="{$f.ID_FAKULTAS}" {if $f.ID_FAKULTAS == $data_jadwal_ppmb_by_id.ID_FAKULTAS}selected="selected"{/if}>{$f.NM_FAKULTAS}</option>
                {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Lokasi</td>
            <td>
                <input type="text" name="lokasi" class="required" value="{$data_jadwal_ppmb_by_id['LOKASI']}" size="50" maxlength="250"/>
            </td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>
                <input type="text" name="alamat" class="required" value="{$data_jadwal_ppmb_by_id['ALAMAT']}" size="75" maxlength="250"/>
            </td>
        </tr>
        <tr>
            <td>Ruang</td>
            <td>
                <input type="text" name="ruang" class="required" value="{$data_jadwal_ppmb_by_id['NM_RUANG']}" size="25" maxlength="100"/>
            </td>
        </tr>
        <tr>
            <td>Tanggal Ujian</td>
            <td>
                {html_select_date prefix="tgl_test_" field_order="DMY" time=$data_jadwal_ppmb_by_id['TGL_TEST']}
            </td>
        </tr>
        <tr>
            <td>Tanggal Ujian 2</td>
            <td>
                {html_select_date prefix="tgl_test2_" field_order="DMY" time=$data_jadwal_ppmb_by_id['TGL_TEST2']}
                <label title="Centang ini jika test berlangsung dua hari">
                    <input type="checkbox" name="dua_hari_test" {if $data_jadwal_ppmb_by_id['DUA_HARI_TEST'] == 1}checked="checked"{/if} />Dua Hari
                </label>
            </td>
        </tr>
		<tr>
            <td>Tanggal Ujian 3</td>
            <td>
                {html_select_date prefix="tgl_test3_" field_order="DMY" end_year="+10" time=$data_jadwal_ppmb_by_id['TGL_TEST3']}
                <label title="Centang ini jika test berlangsung tiga hari">
                    <input type="checkbox" name="tiga_hari_test" {if $data_jadwal_ppmb_by_id['TIGA_HARI_TEST'] == 1}checked="checked"{/if}/> Tiga Hari
                </label>
            </td>
        </tr>
        <tr>
            <td>Waktu</td>
            <td><input type="text" name="waktu" value="{$data_jadwal_ppmb_by_id['WAKTU']}" size="20" maxlength="50" class="required" /></td>
        </tr>
        <tr>
            <td>Kapasitas</td>
            <td><input name="kapasitas" type="text" class="required number" value="{$data_jadwal_ppmb_by_id['KAPASITAS']}" size="4" maxlength="4"/></td>
        </tr>
        <tr>
            <td>Nomer Awal</td>
            <td>
                <input name="nomer_awal" type="text" class="required number" value="{$data_jadwal_ppmb_by_id['NOMER_AWAL']}" size="11" maxlength="11"/>
            </td>
        </tr>
        <tr>
            <td>Jalur</td>
            <td>
                <select name="kode_jalur">
					<option value="0" {if $data_jadwal_ppmb_by_id['KODE_JALUR']=='0'} selected="true"{/if}>Kosongi</option>
                    <option value="1" {if $data_jadwal_ppmb_by_id['KODE_JALUR']=='1'} selected="true"{/if}>IPA</option>
                    <option value="2" {if $data_jadwal_ppmb_by_id['KODE_JALUR']=='2'} selected="true"{/if}>IPS</option>
                    <option value="3" {if $data_jadwal_ppmb_by_id['KODE_JALUR']=='3'} selected="true"{/if}>IPC</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Materi Ujian</td>
            <td>
                <input type="text" name="materi" class="required" value="{$data_jadwal_ppmb_by_id['MATERI']}" size="50" maxlength="50" />
            </td>
        </tr>
        <!--<tr>
            <td>Gelombang</td>
            <td>
                <select name="gelombang">
                    <!--<option value="2" {if $data_jadwal_ppmb_by_id['GELOMBANG']=='2'} selected="true"{/if}>Mandiri 2</option>
                    <option value="9" {if $data_jadwal_ppmb_by_id['GELOMBANG']=='9'} selected="true"{/if}>DEPAG</option> 
                    <option value="4" {if $data_jadwal_ppmb_by_id['GELOMBANG']=='4'} selected="true"{/if}>DIPLOMA</option>
                    <option value="3" {if $data_jadwal_ppmb_by_id['GELOMBANG']=='3'} selected="true"{/if}>ALIH JENIS</option>
                    <option value="3" {if $data_jadwal_ppmb_by_id['GELOMBANG']=='5'} selected="true"{/if}>Pascasrjana</option>
                </select>
            </td>
        </tr>-->
        <tr>
            <td colspan="2" style="text-align: center">
                <a href="verifikasi-ruang.php?mode=view&id_penerimaan={$smarty.get.id_penerimaan}">Batal</a>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
{literal}
	<script type="text/javascript">
		$('#form_edit_jawdal_ppmb').validate();
	</script>
{/literal}