<html><head><title>Pengawas</title></head>
<body>
    <table border="1px">
        <tr>
            <th>Lokasi</th>
            <th>Ruang</th>
            <th>Kapasitas</th>
            <th>Distributor Naskah</th>
            <th>Koordinator</th>
            <th>Pengawas</th>
            <th>Status</th>
            <th>No Peserta</th>
            <th>Amplop Soal</th>
        </tr>
        {foreach $jp_set as $jp}
        <tr>
            <td style="vertical-align: middle">{$jp.LOKASI}</td>
            <td style="vertical-align: middle">{$jp.NM_RUANG}</td>
            <td style="vertical-align: middle; text-align: center">{$jp.KAPASITAS}</td>
            <td style="vertical-align: middle">{$jp.DISTRIBUTOR}</td>
            <td style="vertical-align: middle">{$jp.KOORDINATOR}</td>
            <td>
                {if $jp.PENGAWAS1 != ''}{$jp.PENGAWAS1}{/if}
                {if $jp.PENGAWAS2 != ''}<br/>{$jp.PENGAWAS2}{/if}
                {if $jp.PENGAWAS3 != ''}<br/>{$jp.PENGAWAS3}{/if}
                {if $jp.PENGAWAS4 != ''}<br/>{$jp.PENGAWAS4}{/if}
            </td>
            <td>
                {if $jp.STATUS1 != ''}{$jp.STATUS1}{/if}
                {if $jp.STATUS2 != ''}<br/>{$jp.STATUS2}{/if}
                {if $jp.STATUS3 != ''}<br/>{$jp.STATUS3}{/if}
                {if $jp.STATUS4 != ''}<br/>{$jp.STATUS4}{/if}
            </td>
            <td style="vertical-align: middle">{$jp.NOMER_AWAL} - {$jp.NOMER_AKHIR}</td>
            <td style="vertical-align: middle">
                {if $jp.AMPLOP1 != ''}{$jp.AMPLOP1}{/if}
                {if $jp.AMPLOP2 != ''}<br/>{$jp.AMPLOP2}{/if}
            </td>
        </tr>
        {/foreach}
    </table>
</body>
</html>