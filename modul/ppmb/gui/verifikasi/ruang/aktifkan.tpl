<div class="center_title_bar">Jadwal Tes PPMB - Aktivasi Jadwal</div>
<form action="verifikasi-ruang.php?mode=view&id_penerimaan={$smarty.get.id_penerimaan}" method="post">
    <table>
        <input type="hidden" name="mode" value="aktifkan" />
        <input type="hidden" name="gelombang" value="{$data_jadwal_ppmb_by_id['GELOMBANG']}" />
        <input type="hidden" name="jalur" value="{$data_jadwal_ppmb_by_id['KODE_JALUR']}" />
        <input type="hidden" name="kapasitas" value="{$data_jadwal_ppmb_by_id['KAPASITAS']}" />
        <input type="hidden" name="id_jadwal" value="{$data_jadwal_ppmb_by_id['ID_JADWAL_PPMB']}"/>
        <tr>
            <td>Lokasi</td>
            <td>{$data_jadwal_ppmb_by_id['LOKASI']} {$data_jadwal_ppmb_by_id['ALAMAT']}</td>
        </tr>
        <tr>
            <td>Ruang</td>
            <td>{$data_jadwal_ppmb_by_id['NM_RUANG']}</td>
        </tr>
        <tr>
            <td>Kapasitas</td>
            <td>{$data_jadwal_ppmb_by_id['KAPASITAS']}</td>
        </tr>
        <tr>
            <td>Range Nomer</td>
            <td>{$no_awal} - {$no_awal+$data_jadwal_ppmb_by_id['KAPASITAS']-1}</td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value="Aktifkan"/>
                <a href="verifikasi-ruang.php?mode=view&id_penerimaan={$smarty.get.id_penerimaan}">Batal</a>
            </td>
        </tr>
    </table>
</form>