<div class="center_title_bar">Pengawas Ruang {$jadwal_ppmb.NM_RUANG}</div>

<table>
    <tr>
        <th>No</th>
        <th>Distributor Naskah</th>
        <th>Koordinator</th>
        <th>Pengawas</th>
        <th>Status</th>
        <th>No Peserta</th>
        <th>Amplop Soal</th>
        <th>Aksi</th>
    </tr>
    {foreach $pengawas_ujian_set as $pu}
    <tr>
        <td class="center">{$pu@index + 1}</td>
        <td>{$pu.DISTRIBUTOR}</td>
        <td>{$pu.KOORDINATOR}</td>
        <td>{if $pu.PENGAWAS1 != ''}{$pu.PENGAWAS1}{/if}
            {if $pu.PENGAWAS2 != ''}<br/>{$pu.PENGAWAS2}{/if}
            {if $pu.PENGAWAS3 != ''}<br/>{$pu.PENGAWAS3}{/if}
            {if $pu.PENGAWAS4 != ''}<br/>{$pu.PENGAWAS4}{/if}
        </td>
        <td class="center">
            {if $pu.STATUS1 != ''}{$pu.STATUS1}{/if}
            {if $pu.STATUS2 != ''}<br/>{$pu.STATUS2}{/if}
            {if $pu.STATUS3 != ''}<br/>{$pu.STATUS3}{/if}
            {if $pu.STATUS4 != ''}<br/>{$pu.STATUS4}{/if}
        </td>
        <td>{$pu.NOMER_AWAL} - {$pu.NOMER_AKHIR}</td>
        <td>
                {if $pu.AMPLOP1 != ''}{$pu.AMPLOP1}{/if}
                {if $pu.AMPLOP2 != ''}<br/>{$pu.AMPLOP2}{/if}
        </td>
        <td>
            <a href="verifikasi-ruang.php?mode=edit-pengawas&id_pengawas={$pu.ID_PENGAWAS_UJIAN_PPMB}">Edit</a> |
            <a href="verifikasi-ruang.php?mode=del-pengawas&id_pengawas={$pu.ID_PENGAWAS_UJIAN_PPMB}">Hapus</a>
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="8" class="center">
            <a href="verifikasi-ruang.php?mode=view&id_penerimaan={$smarty.get.id_penerimaan}">Kembali</a>
            <a href="verifikasi-ruang.php?mode=add-pengawas&id_jadwal={$jadwal_ppmb.ID_JADWAL_PPMB}">Tambah</a>
        </td>
    </tr>
</table>

