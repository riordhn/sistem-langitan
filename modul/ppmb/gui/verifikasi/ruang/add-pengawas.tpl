<div class="center_title_bar">Pengawas Ruang {$jadwal_ppmb.NM_RUANG} - Tambah</div>

<form action="verifikasi-ruang.php?mode=pengawas&id_jadwal={$jadwal_ppmb.ID_JADWAL_PPMB}&id_penerimaan={$jadwal_ppmb.ID_PENERIMAAN}" method="post">
    <input type="hidden" name="mode" value="add-pengawas" />
    <input type="hidden" name="id_jadwal_ppmb" value="{$jadwal_ppmb.ID_JADWAL_PPMB}" />
    <table>
        <tr>
            <th colspan="2">Detail Pengawas</th>
        </tr>
        <tr>
            <td>Nama Pengawas 1</td>
            <td><input type="text" name="pengawas1" size="30" maxlength="30" /></td>
        </tr>
        <tr>
            <td>Status Pengawas 1</td>
            <td><select name="status1">
                    <option value="">-</option>
                    <option value="KR">KR - Kepala Ruang</option>
                    <option value="P">P - Pengawas Ruang</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Nama Pengawas 2</td>
            <td><input type="text" name="pengawas2" size="30" maxlength="30" /></td>
        </tr>
        <tr>
            <td>Status Pengawas 2</td>
            <td><select name="status2">
                    <option value="">-</option>
                    <option value="KR">KR - Kepala Ruang</option>
                    <option value="P">P - Pengawas Ruang</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Nama Pengawas 3</td>
            <td><input type="text" name="pengawas3" size="30" maxlength="30" /></td>
        </tr>
        <tr>
            <td>Status Pengawas 3</td>
            <td><select name="status3">
                    <option value="">-</option>
                    <option value="KR">KR - Kepala Ruang</option>
                    <option value="P">P - Pengawas Ruang</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Nama Pengawas 4</td>
            <td><input type="text" name="pengawas4" size="30" maxlength="30" /></td>
        </tr>
        <tr>
            <td>Status Pengawas 4</td>
            <td><select name="status4">
                    <option value="">-</option>
                    <option value="KR">KR - Kepala Ruang</option>
                    <option value="P">P - Pengawas Ruang</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Nomer Awal </td>
            <td><select name="nomer_awal">
                    <option value=""></option>
                {for $i=$jadwal_ppmb.NOMER_AWAL to $jadwal_ppmb.NOMER_AKHIR}
                    <option value="{$i}">{$i}</option>
                {/for}
                </select>
            </td>
        </tr>
        <tr>
            <td>Nomer Akhir</td>
            <td><select name="nomer_akhir">
                    <option value=""></option>
                {for $i=$jadwal_ppmb.NOMER_AWAL to $jadwal_ppmb.NOMER_AKHIR}
                    <option value="{$i}">{$i}</option>
                {/for}
                </select>
            </td>
        </tr>
        <tr>
            <td>Amplop 1</td>
            <td><input type="text" name="amplop1" size="30" maxlength="30" /></td>
        </tr>
        <tr>
            <td>Amplop 2</td>
            <td><input type="text" name="amplop2" size="30" maxlength="30" /></td>
        </tr>
        <tr>
            <td>Distributor Naskah</td>
            <td><input type="text" name="distributor" size="30" maxlength="30" value="{$pu.DISTRIBUTOR}"/></td>
        </tr>
        <tr>
            <td>Koordinator</td>
            <td><input type="text" name="koordinator" size="30" maxlength="30" value="{$pu.KOORDINATOR}"/></td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <a class="button" href="verifikasi-ruang.php?mode=pengawas&id_jadwal={$jadwal_ppmb.ID_JADWAL_PPMB}&id_penerimaan={$jadwal_ppmb.ID_PENERIMAAN}">Kembali</a>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>