<div class="center_title_bar">Pembukaan Voucher - Edit Tarif</div>

{if $result}<script type="text/javascript">alert('{$result}');</script>{/if}

<form action="penerimaan-voucher.php?mode=edit-tarif&id_voucher_tarif={$smarty.get.id_voucher_tarif}" method="post">
<input type="hidden" name="mode" value="edit-tarif" />
<input type="hidden" name="id_voucher_tarif" value="{$smarty.get.id_voucher_tarif}" />
<table>
    <tr>
        <th colspan="2">Detail Tarif</th>
    </tr>
    <tr>
        <td>Kode Jurusan</td>
        <td>
            <select name="kode_jurusan">
                <option value="01" {if $vt.KODE_JURUSAN == '01'}selected="selected"{/if}>IPA/Reguler</option>
                <option value="02" {if $vt.KODE_JURUSAN == '02'}selected="selected"{/if}>IPS</option>
                <option value="03" {if $vt.KODE_JURUSAN == '03'}selected="selected"{/if}>IPC</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>Besar Tarif</td>
        <td><input type="text" name="tarif" size="11" value="{$vt.TARIF}" /></td>
    </tr>
    <tr>
        <td>Keterangan</td>
        <td><input type="text" name="deskripsi" size="50" value="{$vt.DESKRIPSI}"/></td>
    </tr>
    <tr>
        <td colspan="2">
            <a href="penerimaan-voucher.php?id_penerimaan={$vt.ID_PENERIMAAN}">Kembali</a>
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>