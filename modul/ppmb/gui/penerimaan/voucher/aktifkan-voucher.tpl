<div class="center_title_bar">Pembukaan Voucher - Mengaktifkan Voucher</div>

<h2>Penerimaan {$penerimaan.NM_PENERIMAAN} Gelombang {$penerimaan.GELOMBANG} Semester {$penerimaan.SEMESTER}</h2>

<form action="penerimaan-voucher.php?id_penerimaan={$smarty.get.id_penerimaan}" method="post">
<input type="hidden" name="mode" value="aktifkan-voucher" />
<input type="hidden" name="id_penerimaan" value="{$smarty.get.id_penerimaan}" />
<table>
    <tr>
        <th colspan="2">Aktifkan Kode Voucher</th>
    </tr>
    <tr>
        <td>Nomer Awal</td>
        <td><input type="text" name="no_awal" size="15" maxlength="11" value="{$kode_voucher}" /></td>
    </tr>
    <tr>
        <td>Nomer Akhir</td>
        <td><input type="text" name="no_akhir" size="15" maxlength="11" value="{$kode_voucher}"/></td>
    </tr>
    <tr>
        <td colspan="2">
            <a href="penerimaan-voucher.php?id_penerimaan={$smarty.get.id_penerimaan}">Kembali</a>
            <input type="submit" value="Aktifkan" />
        </td>
    </tr>
</table>
</form>