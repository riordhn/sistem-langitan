<div class="center_title_bar">Pembukaan Voucher - Tambah Tarif</div>

<form action="penerimaan-voucher.php?id_penerimaan={$smarty.get.id_penerimaan}" method="post">
<input type="hidden" name="mode" value="add-tarif" />
<input type="hidden" name="id_penerimaan" value="{$smarty.get.id_penerimaan}" />
<table>
    <tr>
        <th colspan="2">Detail Tarif</th>
    </tr>
    <tr>
        <td>Kode Jurusan</td>
        <td>
            <select name="kode_jurusan">
                <option value="01">IPA/Reguler</option>
                <option value="02">IPS</option>
                <option value="03">IPC</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>Besar Tarif</td>
        <td><input type="text" name="tarif" size="11" /></td>
    </tr>
    <tr>
        <td>Keterangan</td>
        <td><input type="text" name="deskripsi" size="50" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <a href="penerimaan-voucher.php?id_penerimaan={$smarty.get.id_penerimaan}">Kembali</a>
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>