<div class="center_title_bar">Pembukaan Voucher - List Voucher</div>

<h2>Voucher {$penerimaan.NM_PENERIMAAN} Gelombang {$penerimaan.GELOMBANG} Semester {$penerimaan.SEMESTER} Tahun {$penerimaan.TAHUN}</h2>

<a href="penerimaan-voucher.php?id_penerimaan={$penerimaan.ID_PENERIMAAN}">Kembali</a>

<table>
	<thead>
		<tr>
			<th>No</th>
			<th>Kode Voucher</th>
			<th>Status Aktif</th>
			<th>Pengambilan</th>
			<th>Pembayaran</th>
			<th>Jurusan</th>
			<th>Bank</th>
			<th>Keterangan</th>
			<th>PIN Voucher</th>
		</tr>
	</thead>
	<tbody>
		{foreach $voucher_set as $v}
			<tr>
				<td class="center">{$v@index + 1}</td>
				<td>{$v.KODE_VOUCHER}</td>
				<td class="center">{if $v.IS_AKTIF}Aktif{else}Tidak Aktif{/if}</td>
				<td class="center">{$v.TGL_AMBIL}</td>
				<td class="center">{$v.TGL_BAYAR}</td>
				<td class="center">{if $v.KODE_JURUSAN == '01'}IPA/Reg{else if $v.KODE_JURUSAN == '02'}IPS{else if $v.KODE_JURUSAN == '03'}IPC{/if}</td>
				<td>{$v.NM_BANK}</td>
				<td>{$v.KETERANGAN}</td>
				<td>
					{if $v.TGL_BAYAR != ''}{$v.PIN_VOUCHER}{/if}
				</td>
			</tr>
		{/foreach}
	</tbody>
</table>