<div class="center_title_bar">Pembukaan Voucher</div>

<form action="penerimaan-voucher.php" method="get" id="filter">
    <table class="filter" border="1">
        <tr>
            <td>Penerimaan</td>
            <td>
                <select name="id_penerimaan" onchange="$('#filter').submit(); return false;">
                    <option value="">-- Pilih Penerimaan --</option>
                {foreach $penerimaan_set as $p}
                    <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                    {foreach $p.p_set as $p2}
                    <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} </option>
                    {/foreach}
                    </optgroup>
                {/foreach}
                </select>
            </td>
        </tr>
    </table>
</form>
                
{if $result}<script type="text/javascript">alert('{$result}');</script>{/if}
                
{if $smarty.get.id_penerimaan != ''}
<h2 style="margin-bottom: 10px">Tarif</h2>
<table>
    <tr>
        <th>Kode Jurusan</th>
        <th>Tarif</th>
        <th>Deskripsi</th>
        <th>Aksi</th>
    </tr>
    {foreach $voucher_tarif_set as $vt}
    <tr>
        <td>{$vt.KODE_JURUSAN_PARSE}</td>
        <td>{$vt.TARIF}</td>        
        <td>{$vt.DESKRIPSI}</td>
        <td><a href="penerimaan-voucher.php?mode=edit-tarif&id_voucher_tarif={$vt.ID_VOUCHER_TARIF}">Edit</a></td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="4">
            <a href="penerimaan-voucher.php?mode=add-tarif&id_penerimaan={$smarty.get.id_penerimaan}">Tambah Tarif</a>
        </td>
    </tr>
</table>

<h2>Voucher</h2>
<table>
    <tr>
        <th>Jumlah</th>
        <th>Belum Aktif</th>
        <th>Aktif</th>
        <th>Pengambilan</th>
        <th>Pembelian</th>
        <th>Voucher</th>
    </tr>
    <tr>
        <td class="center"><a class="anchor-black" href="penerimaan-voucher.php?id_penerimaan={$smarty.get.id_penerimaan}&mode=list-voucher">{$voucher.TOTAL}</a></td>
        <td class="center"><a class="anchor-black" href="penerimaan-voucher.php?id_penerimaan={$smarty.get.id_penerimaan}&mode=list-voucher">{$voucher.BELUM_AKTIF}</a></td>
        <td class="center"><a class="anchor-black" href="penerimaan-voucher.php?id_penerimaan={$smarty.get.id_penerimaan}&mode=list-voucher">{$voucher.AKTIF}</a></td>
        <td class="center"><a class="anchor-black" href="penerimaan-voucher.php?id_penerimaan={$smarty.get.id_penerimaan}&mode=list-voucher">{$voucher.PENGAMBILAN}</a></td>
        <td class="center"><a class="anchor-black" href="penerimaan-voucher.php?id_penerimaan={$smarty.get.id_penerimaan}&mode=list-voucher">{$voucher.PEMBELIAN}</a></td>
        <td class="center"><span>{$voucher.NO_AWAL}</span> - <span>{$voucher.NO_AKHIR}</span></td>
    </tr>
    <tr>    
        <td colspan="6">
            {if count($voucher_tarif_set) > 0}
            <a href="penerimaan-voucher.php?id_penerimaan={$smarty.get.id_penerimaan}&mode=tambah-voucher">Tambah</a> |
            <a href="penerimaan-voucher.php?id_penerimaan={$smarty.get.id_penerimaan}&mode=aktifkan-voucher">Aktifkan</a> |
            <a href="penerimaan-voucher.php?id_penerimaan={$smarty.get.id_penerimaan}&mode=nonaktif-voucher">Nonaktifkan</a> |
            <a href="penerimaan-voucher.php?id_penerimaan={$smarty.get.id_penerimaan}&mode=manual-voucher">Manual</a>
            {else}
            Tentukan tarif terlebih dahulu
            {/if}
        </td>
    </tr>
</table>
{/if}