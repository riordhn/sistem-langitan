<div class="center_title_bar">Petugas Penerimaan - Tambah Petugas</div>

<form action="penerimaan-petugas.php?id_penerimaan={$smarty.get.id_penerimaan}" method="post" id="form">
	<input type="hidden" name="mode" value="add" />
	<input type="hidden" name="id_penerimaan" value="{$smarty.get.id_penerimaan}" />
	<input type="hidden" name="id_pengguna" value="" />
	<table>
		<thead>
			<tr>
				<th colspan="2">Petugas</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Penerimaan</td>
				<td>{$penerimaan.NM_PENERIMAAN} Gelombang {$penerimaan.GELOMBANG} Tahun {$penerimaan.TAHUN}</td>
			</tr>
			<tr>
				<td>NIP Petugas</td>
				<td>
					<input type="text" name="nip" placeholder="NIK/NIP Petugas" id="nip" />
					<button id="cari_petugas">Cari</button>
				</td>
			</tr>
			<tr>
				<td>Nama</td>
				<td>
					<label id="nama_petugas"></label>
				</td>
			</tr>
			<tr>
				<td>Tugas</td>
				<td>
					<select name="id_jabatan_panitia">
						{foreach $jabatan_panitia_set as $jp}
							<option value="{$jp.ID_JABATAN_PANITIA}">{$jp.NM_JABATAN_PANITIA}</option>
						{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="center">
					<a href="penerimaan-petugas.php?id_penerimaan={$smarty.get.id_penerimaan}">Kembali</a>
					<input type="submit" value="Simpan" id="submit_button" />
				</td>
			</tr>
			
		</tbody>
	</table>
</form>

<p>Setelah memasukkan NIP, klik Cari sampai nama Petugas muncul.</p>

<script type="text/javascript">
	$(document).ready(function() {
		$('#cari_petugas').on('click',function(e) {
			e.preventDefault();

			var nip = $('#nip').val();

			$.ajax({
				url: 'penerimaan-petugas.php?mode=cari-petugas&nip=' + nip,
				dataType: 'json',
				beforeSend: function() {
					$('#nama_petugas').html('');
					$('input[name="id_pengguna"]').val('');
				}
			}).done(function(r) {
				$('#nama_petugas').html(r.NM_PENGGUNA);
				$('input[name="id_pengguna"]').val(r.ID_PENGGUNA);
			});
		});

		$('#submit_button').on('click', function(e) {
			e.preventDefault();

			if ($('input[name="id_pengguna"]').val() === '')
			{
				alert('Harap memilih petugas terlebih dahulu');
				$('#nip').focus();
			}
			else
			{
				$('#form').submit();
			}

		});
	});
</script>