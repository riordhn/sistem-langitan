<div class="center_title_bar">Petugas Penerimaan</div>

<form action="penerimaan-petugas.php" method="get">
	<table class="filter" border="1">
		<tr>
			<td>Penerimaan</td>
			<td>
				<select name="id_penerimaan" onchange="javascript: $(this).submit(); ">
					<option value="">-- Pilih Penerimaan --</option>
					{foreach $penerimaan_set as $p}
						<optgroup label="{$p.TAHUN} {$p.SEMESTER}">
						{foreach $p.p_set as $p2}
							<option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} </option>
						{/foreach}
						</optgroup>
					{/foreach}
				</select>
			</td>
		</tr>
	</table>
</form>

{if !empty($smarty.get.id_penerimaan)}
	<table>
		<thead>
			<tr>
				<th>#</th>
				<th>NIK/NIP</th>
				<th>Nama Petugas</th>
				<th>Jabatan</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			{foreach $panitia_set as $panitia}
				<tr>
					<td>{$panitia@index + 1}</td>
					<td>{$panitia.USERNAME}</td>
					<td>{$panitia.NM_PENGGUNA}</td>
					<td>{$panitia.NM_JABATAN_PANITIA}</td>
					<td>
						<a href="penerimaan-petugas.php?mode=delete&id_penerimaan={$smarty.get.id_penerimaan}&id_penerimaan_panitia={$panitia.ID_PENERIMAAN_PANITIA}">Hapus</a>
					</td>
				</tr>
			{/foreach}
			<tr>
				<td colspan="5">
					<a href="penerimaan-petugas.php?mode=add&id_penerimaan={$smarty.get.id_penerimaan}">Tambah Petugas</a>
				</td>
			</tr>
		</tbody>
	</table>
{/if}