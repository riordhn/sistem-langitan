<div class="center_title_bar">Penawaran Prodi Penerimaan - Hapus Prodi Minat</div>

<h2>Apakah prodi minat ini akan dihapus dari penerimaan ?</h2>

<form action="penerimaan-prodi.php?id_penerimaan={$ppm.ID_PENERIMAAN}" method="post">
<input type="hidden" name="mode" value="delminat" />
<input type="hidden" name="id_penerimaan_pm" value="{$ppm.ID_PENERIMAAN_PM}" />
<table>
	<tr>
		<th colspan="2">Info Program Studi</th>
	</tr>
	<tr>
		<td>Penerimaan</td>
		<td>{$ppm.NM_PENERIMAAN}</td>
	</tr>
	<tr>
		<td>Nama Program Studi</td>
		<td>{$ppm.NM_PROGRAM_STUDI}</td>
	</tr>
	<tr>
		<td>Minat Studi</td>
		<td>{$ppm.NM_PRODI_MINAT}</td>
	</tr>
	<tr>
		<td colspan="2" class="center">
			<a href="penerimaan-prodi.php?id_penerimaan={$ppm.ID_PENERIMAAN}">Batal</a>
			<input type="submit" value="Hapus" />
		</td>
	</tr>
</table>
</form>