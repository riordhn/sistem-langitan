<div class="center_title_bar">Penawaran Prodi Penerimaan - Hapus Prodi</div>

<h2>Apakah prodi ini akan dihapus dari penerimaan ?</h2>

<form action="penerimaan-prodi.php?id_penerimaan={$pp.ID_PENERIMAAN}" method="post">
<input type="hidden" name="mode" value="delprodi" />
<input type="hidden" name="id_penerimaan_prodi" value="{$pp.ID_PENERIMAAN_PRODI}" />
<table>
	<tr>
		<th colspan="2">Info Program Studi</th>
	</tr>
	<tr>
		<td>Penerimaan</td>
		<td>{$pp.NM_PENERIMAAN}</td>
	</tr>
	<tr>
		<td>Nama Program Studi</td>
		<td>{$pp.NM_PROGRAM_STUDI}</td>
	</tr>
	<tr>
		<td colspan="2" class="center">
			<a href="penerimaan-prodi.php?id_penerimaan={$pp.ID_PENERIMAAN}">Batal</a>
			<input type="submit" value="Hapus" />
		</td>
	</tr>
</table>
</form>