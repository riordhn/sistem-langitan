<div class="center_title_bar">Penawaran Prodi Penerimaan - Tambah Prodi</div>

<form action="penerimaan-prodi.php?id_penerimaan={$penerimaan.ID_PENERIMAAN}" method="post">
<input type="hidden" name="mode" value="addprodi" />
<input type="hidden" name="id_penerimaan" value="{$penerimaan.ID_PENERIMAAN}" />
<table>
	<tr>
		<th colspan="2">Info Program Studi</th>
	</tr>
	<tr>
		<td>Penerimaan</td>
		<td>{$penerimaan.NM_PENERIMAAN}</td>
	</tr>
	<tr>
		<td>Jenjang</td>
		<td>{$penerimaan.NM_JENJANG}</td>
	</tr>
	<tr>
		<td>Nama Program Studi</td>
		<td><select name="id_program_studi">
			<option value="">-- Pilih Program Studi --</option>
			{foreach $program_studi_set as $ps}
				<option value="{$ps.ID_PROGRAM_STUDI}">{$ps.NM_PROGRAM_STUDI}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="center">
			<a href="penerimaan-prodi.php?id_penerimaan={$penerimaan.ID_PENERIMAAN}">Batal</a>
			<input type="submit" value="Tambah" />
		</td>
	</tr>
</table>
</form>