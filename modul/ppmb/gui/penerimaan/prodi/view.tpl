<div class="center_title_bar">Penawaran Prodi Penerimaan</div>

<form action="penerimaan-prodi.php" method="get" id="filter">
    <table class="filter" border="1">
        <tr>
            <td>Penerimaan</td>
            <td>
                <select name="id_penerimaan" onchange="$('#filter').submit(); return false;">
                    <option value="">-- Pilih Penerimaan --</option>
                {foreach $penerimaan_set as $p}
                    <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                    {foreach $p.p_set as $p2}
                    <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} </option>
                    {/foreach}
                    </optgroup>
                {/foreach}
                </select>
            </td>
        </tr>
    </table>
</form>

{if $deleted}<h2>{$deleted}</h2>{/if}
{if $added}<h2>{$added}</h2>{/if}

{if $smarty.get.id_penerimaan != ''}

<table border="1px" class="view" width="100%">
	<caption>Penawaran Program Studi</caption>
	<tr>
        <th>No</th>
		<th>Fakultas</th>
		<th>Program Studi</th>
		<th style="width: 100px">Status Aktif</th>
		<th>Aksi</th>
	</tr>
	{foreach $penerimaan_prodi_set as $pp}
	<tr>
        <td class="center">{$pp@index + 1}</td>
		<td>{$pp.NM_FAKULTAS}</td>
		<td>{$pp.NM_JENJANG} {$pp.NM_PROGRAM_STUDI} ({$pp.GELAR_PENDEK})</td>
		<td class="center">
			<label>
				<input type="checkbox" value="pp{$pp.ID_PENERIMAAN_PRODI}" {if $pp.IS_AKTIF == 1}checked="checked"{/if} />
				{if $pp.IS_AKTIF == 1}
				<span>Aktif</span>
				{else}
				<span>Tidak Aktif</span>
				{/if}
			</label>
			
			<script type="text/javascript">
				$('input[value="pp{$pp.ID_PENERIMAAN_PRODI}"]').click(function() {literal}{
					var is_aktif = 0;
					var id_pp = this.value.replace('pp', '');
					var objThis = this;
						
					if (this.checked == true)
						is_aktif = 1;
					else
						is_aktif = 0;
					
					$.ajax({
						url: 'ubahProdiAktif.php',
						data: 'id_penerimaan_prodi=' + id_pp + '&is_aktif=' + is_aktif,
						success: function(data) {
							if (data == 1)
								if (is_aktif == 1) 
									$(objThis).next().text('Aktif');
								else
									$(objThis).next().text('Tidak Aktif');
						}
					});
				}); {/literal}
			</script>
			
		</td>
		<td>
			<a href="penerimaan-prodi.php?mode=delprodi&id_penerimaan_prodi={$pp.ID_PENERIMAAN_PRODI}">Hapus</a>
		</td>
	</tr>
	{/foreach}
	<tr>
		<td colspan="5"><a href="penerimaan-prodi.php?mode=addprodi&id_penerimaan={$smarty.get.id_penerimaan}">Tambah Prodi</a></td>
	</tr>
</table>

<table border="1px" class="view">
	<caption>Penawaran Minat Program Studi</caption>
	<tr>
        <th>No</th>
		<th>Program Studi</th>
		<th>Prodi Minat</th>
		<th style="width: 100px">Status</th>
		<th>Aksi</th>
	</tr>
	{foreach $penerimaan_prodi_minat_set as $ppm}
	<tr>
        <td class="center">{$ppm@index + 1}</td>
		<td>{$ppm.NM_JENJANG} {$ppm.NM_PROGRAM_STUDI}</td>
		<td>{$ppm.NM_PRODI_MINAT}</td>
		<td class="center">
			<label>
				<input type="checkbox" value="ppm{$ppm.ID_PENERIMAAN_PM}" {if $ppm.IS_AKTIF == 1}checked="checked"{/if} />
				{if $ppm.IS_AKTIF == 1}
				<span>Aktif</span>
				{else}
				<span>Tidak Aktif</span>
				{/if}
			</label>
			<script type="text/javascript">
				$('input[value="ppm{$ppm.ID_PENERIMAAN_PM}"]').click(function() {literal}{
					var is_aktif = 0;
					var id_ppm = this.value.replace('ppm', '');
					var objThis = this;
						
					if (this.checked == true)
						is_aktif = 1;
					else
						is_aktif = 0;
					
					$.ajax({
						url: 'ubahProdiMinatAktif.php',
						data: 'id_penerimaan_pm=' + id_ppm + '&is_aktif=' + is_aktif,
						success: function(data) {
							if (data == 1)
								if (is_aktif == 1) 
									$(objThis).next().text('Aktif');
								else
									$(objThis).next().text('Tidak Aktif');
						}
					});
				}); {/literal}
			</script>
		</td>
		<td>
			<a href="penerimaan-prodi.php?mode=delminat&id_penerimaan_pm={$ppm.ID_PENERIMAAN_PM}">Hapus</a>
		</td>
	</tr>
	{/foreach}
	<tr>
		<td colspan="5">
			<a href="penerimaan-prodi.php?mode=addminat&id_penerimaan={$smarty.get.id_penerimaan}">Tambah</a>
		</td>
	</tr>
</table>
        
{/if}