<div class="center_title_bar">Penawaran Prodi Penerimaan - Tambah Prodi Minat</div>

<form action="penerimaan-prodi.php?id_penerimaan={$penerimaan.ID_PENERIMAAN}" method="post">
<input type="hidden" name="mode" value="addminat" />
<input type="hidden" name="id_penerimaan" value="{$penerimaan.ID_PENERIMAAN}" />
<table>
	<tr>
		<th colspan="2">Info Program Studi</th>
	</tr>
	<tr>
		<td>Penerimaan</td>
		<td>{$penerimaan.NM_PENERIMAAN}</td>
	</tr>
	<tr>
		<td>Jenjang</td>
		<td>{$penerimaan.NM_JENJANG}</td>
	</tr>
	<tr>
		<td>Nama Prodi Minat</td>
		<td><select name="id_prodi_minat">
			<option value="">-- Pilih Prodi Minat --</option>
			{foreach $prodi_minat_set as $pm}
				<option value="{$pm.ID_PRODI_MINAT}">{$pm.NM_PROGRAM_STUDI} - {$pm.NM_PRODI_MINAT}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="center">
			<a class="button" href="penerimaan-prodi.php?id_penerimaan={$penerimaan.ID_PENERIMAAN}">Batal</a>
			<input type="submit" value="Tambah" />
		</td>
	</tr>
</table>
</form>