<div class="center_title_bar">Master Kota - Tambah Kota</div>

<form action="penerimaan-kota.php?id_negara={$smarty.get.id_negara}&id_provinsi={$smarty.get.id_provinsi}" method="post">
	<input type="hidden" name="mode" value="add" />
	<input type="hidden" name="id_provinsi" value="{$smarty.get.id_provinsi}" />
	<table>
		<thead>
			<tr>
				<th colspan="2">Detail Kota</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Negara</td>
				<td>{$negara.NM_NEGARA}</td>
			</tr>
			<tr>
				<td>Provinsi</td>
				<td>{$provinsi.NM_PROVINSI}</td>
			</tr>
			<tr>
				<td>Kode SNMPTN</td>
				<td>
					<input type="text" name="kode_snmptn" />
				</td>
			</tr>
			<tr>
				<td>Tipe Dati 2</td>
				<td>
					<select name="tipe_dati2">
						<option value="">--</option>
						<option value="KOTA">KOTA</option>
						<option value="KABUPATEN">KABUPATEN</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Nama Kota/Kabupaten</td>
				<td>
					<input type="text" name="nm_kota" />
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<a href="penerimaan-kota.php?id_negara={$smarty.get.id_negara}&id_provinsi={$smarty.get.id_provinsi}">Kembali</a>
					<input type="submit" value="Tambahkan" />
				</td>
			</tr>
		</tbody>
	</table>
</form>

<p>Pastikan melakukan pengecekan sebelum menambahkan kota baru, terutama untuk Kota Indonesia,<br/>karena kota-kota di Indonesia semua sudah mempunyai kode SNMPTN</p>