<div class="center_title_bar">Master Kota</div>

<form action="penerimaan-kota.php" method="get">
	<table>
		<tbody>
			<tr>
				<td>
					<label>Negara :</label>
					<select name="id_negara" onchange="javascript: $('#provinsi option[value=\'\']').attr('selected', true);
							$(this).submit();">
						<option>-- PILIH NEGARA --</option>
						{foreach $negara_set as $negara}
							<option value="{$negara.ID_NEGARA}" {if !empty($smarty.get.id_negara)}{if $negara.ID_NEGARA == $smarty.get.id_negara}selected{/if}{/if}>{$negara.NM_NEGARA}</option>
						{/foreach}
					</select>
				</td>
			</tr>
			{if !empty($smarty.get.id_negara)}
				<tr>
					<td>
						<label>Provinsi :</label>
						<select id="provinsi" name="id_provinsi" onchange="javascript: $(this).submit();">
							<option value="">-- PILIH PROVINSI --</option>
							{foreach $provinsi_set as $provinsi}
								<option value="{$provinsi.ID_PROVINSI}" {if !empty($smarty.get.id_provinsi)}{if $provinsi.ID_PROVINSI == $smarty.get.id_provinsi}selected{/if}{/if}>{$provinsi.NM_PROVINSI}</option>
							{/foreach}
						</select>
					</td>
				</tr>
			{/if}
		</tbody>
	</table>
</form>

{if !empty($message)}
	<h3>{$message}</h3>
{/if}

{if !empty($smarty.get.id_provinsi)}
	<table>
		<thead>
			<tr>
				<th>#</th>
				<th>Kode<br/>SNMPTN</th>
				<th>Kota / Kabupaten</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			{foreach $kota_set as $kota}
				<tr>
					<td>{$kota@index + 1}</td>
					<td>{$kota.KODE_SNMPTN}</td>
					<td>{$kota.TIPE_DATI2} {$kota.NM_KOTA}</td>
					<td class="center">
						{if $kota.IS_AKTIF == 1}
							<button data-action="non-aktifkan" data-id-kota="{$kota.ID_KOTA}">Non-Aktifkan</button>
						{else}
							<button data-action="aktifkan" data-id-kota="{$kota.ID_KOTA}">Aktifkan</button>
						{/if}
						<a href="penerimaan-kota.php?mode=edit&id_negara={$smarty.get.id_negara}&id_provinsi={$smarty.get.id_provinsi}&id_kota={$kota.ID_KOTA}">Edit</a>
					</td>
				</tr>
			{/foreach}
		</tbody>
		<tfoot>
			<tr>
				<td colspan="4" class="center">
					<a href="penerimaan-kota.php?mode=add&id_negara={$smarty.get.id_negara}&id_provinsi={$smarty.get.id_provinsi}">Tambah Kota</a>
				</td>
			</tr>
		</tfoot>
	</table>
{/if}

<script type="text/javascript">
	$(document).ready(function() {
		$('button').on('click', function() {
			var action = $(this).data('action');
			var id_kota = $(this).data('id-kota');
			var btn = $(this);

			$.ajax({
				url: 'penerimaan-kota.php?mode=' + action + '&id_kota=' + id_kota
			}).done(function(r) {
				if (action === 'non-aktifkan')
				{
					$(btn).data('action', 'aktifkan');
					$(btn).html('Aktifkan');
				}
				else if (action === 'aktifkan')
				{
					$(btn).data('action', 'non-aktifkan');
					$(btn).html('Non-Aktifkan');
				}
			});
		});
	});
</script>