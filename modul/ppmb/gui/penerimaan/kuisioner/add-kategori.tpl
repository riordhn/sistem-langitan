<div class="center_title_bar">{$form.NM_FORM} : Tambah Kategori Soal</div>

<form action="penerimaan-kuisioner.php?mode=view-soal&id_form={$smarty.get.id_form}" method="post">
	<input type="hidden" name="mode" value="add-kategori" />
	<input type="hidden" name="id_form" value="{$form.ID_FORM}" />
	<table>
		<thead>
			<tr>
				<th colspan="2">Kategori Soal</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Kode Kategori</td>
				<td><input type="text" name="kode_kategori" value="" style="width: 100px" /></td>
			</tr>
			<tr>
				<td>Nama Kategori</td>
				<td><input type="text" name="nm_kategori" value="" style="width: 300px"/></td>
			</tr>
			<tr>
				<td colspan="2" class="center">
					<a href="penerimaan-kuisioner.php?mode=view-soal&id_form={$smarty.get.id_form}">Kembali</a>
					<input type="submit" value="Simpan" />
				</td>
			</tr>
		</tbody>
	</table>
</form>