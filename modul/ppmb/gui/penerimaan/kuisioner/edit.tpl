<div class="center_title_bar">Master Kuisioner : Edit</div>

{if !empty($message)}
	<h3>{$message}</h3>
{/if}

<form action="penerimaan-kuisioner.php?mode=edit&id_form={$smarty.get.id_form}" method="post">
	<input type="hidden" name="mode" value="edit" />
	<input type="hidden" name="id_form" value="{$form.ID_FORM}" />
	<table>
		<thead>
			<tr>
				<th colspan="2">Form Kuisioner</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Nama Form Kuisioner</td>
				<td><input type="text" name="nm_form" value="{$form.NM_FORM}" style="width: 200px" /></td>
			</tr>
			<tr>
				<td colspan="2" class="center">
					<a href="penerimaan-kuisioner.php">Kembali</a>
					<input type="submit" value="Simpan" />
				</td>
			</tr>
		</tbody>
	</table>
</form>