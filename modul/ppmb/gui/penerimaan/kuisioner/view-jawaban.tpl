<div class="center_title_bar">{$form.NM_FORM} - Pilihan Jawaban</div>

<h4>Soal : {$soal.NOMER}. {$soal.SOAL}</h4>

<p><a href="penerimaan-kuisioner.php?mode=view-soal&id_form={$smarty.get.id_form}">Kembali ke Master Soal</a></p>

{if !empty($message)}
	<h3>{$message}</h3>
{/if}

<table>
	<thead>
		<tr>
			<th>Urutan</th>
			<th>Pilihan Jawaban</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		{foreach $jawaban_set as $jawaban}
			<tr>
				<td class="center">{$jawaban.URUTAN}</td>
				<td>{$jawaban.JAWABAN}</td>
				<td>
					<a href="penerimaan-kuisioner.php?mode=edit-jawaban&id_form={$smarty.get.id_form}&id_soal={$smarty.get.id_soal}&id_jawaban={$jawaban.ID_JAWABAN}">Edit</a>
					<span action="delete-jawaban" data-id-jawaban="{$jawaban.ID_JAWABAN}" class="anchor-span">Hapus</span>
				</td>
			</tr>
		{/foreach}
		<tr>
			<td colspan="3" class="center">
				<a href="penerimaan-kuisioner.php?mode=add-jawaban&id_form={$smarty.get.id_form}&id_soal={$smarty.get.id_soal}">Tambah Pilihan Jawaban</a>
			</td>
		</tr>
	</tbody>
</table>

<script type="text/javascript">
	$(document).ready(function() {
		$('span[action="delete-jawaban"]').on('click', function() {
			if (confirm('Apakah data ini akan dihapus ?'))
			{
				var id_jawaban = $(this).data('id-jawaban');
				var span = this;

				$.ajax({
					type: 'POST',
					url: 'penerimaan-kuisioner.php',
					data: 'mode=delete-jawaban&id_jawaban=' + id_jawaban
				}).done(function() {
					$(span).parentsUntil('tbody').remove();
				});
			}
		});
	});
</script>