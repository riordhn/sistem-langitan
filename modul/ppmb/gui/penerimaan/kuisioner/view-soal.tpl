<div class="center_title_bar">{$form.NM_FORM} - Master Soal</div>

<p><a href="penerimaan-kuisioner.php">Kembali ke Master Kuisioner</a></p>

{if !empty($message)}
	<h3>{$message}</h3>
{/if}

<table>
	<thead>
		<tr>
			<th>No</th>
			<th>Kode Kategori</th>
			<th>Deskripsi Kategori</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		{foreach $kategori_set as $kategori}
			<tr>
				<td>{$kategori@index + 1}</td>
				<td>{$kategori.KODE_KATEGORI}</td>
				<td>{$kategori.NM_KATEGORI}</td>
				<td>
					<a href="penerimaan-kuisioner.php?mode=edit-kategori&id_form={$smarty.get.id_form}&id_kategori_soal={$kategori.ID_KATEGORI_SOAL}">Edit</a>
					<span action="delete-kategori" data-id-kategori-soal="{$kategori.ID_KATEGORI_SOAL}" class="anchor-span">Hapus</span>
				</td>
			</tr>
		{/foreach}
		<tr>
			<td colspan="4" class="center">
				<a href="penerimaan-kuisioner.php?mode=add-kategori&id_form={$smarty.get.id_form}">Tambah Kategori</a>
			</td>
		</tr>
	</tbody>
</table>

<table>
	<thead>
		<tr>
			<th>Nomer</th>
			<th>Kategori</th>
			<th>Soal</th>
			<th>Jenis Soal</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		{foreach $soal_set as $soal}
			<tr>
				<td class="center">{$soal.NOMER}</td>
				<td>{$soal.NM_KATEGORI}</td>
				<td>{$soal.SOAL}</td>
				<td>
					{$soal.JENIS_SOAL}
					{if $soal.TIPE_SOAL == 1}
						<br/>
						<a href="penerimaan-kuisioner.php?mode=view-jawaban&id_form={$smarty.get.id_form}&id_soal={$soal.ID_SOAL}">Jawaban</a>
					{/if}
				</td>
				<td>
					<a href="penerimaan-kuisioner.php?mode=edit-soal&id_form={$smarty.get.id_form}&id_soal={$soal.ID_SOAL}">Edit</a>
					<span action="delete-soal" data-id-soal="{$soal.ID_SOAL}" class="anchor-span">Hapus</span>
				</td>
			</tr>
		{/foreach}
		<tr>
			<td colspan="6" class="center">
				<a href="penerimaan-kuisioner.php?mode=add-soal&id_form={$smarty.get.id_form}">Tambah Soal</a>
			</td>
		</tr>
	</tbody>
</table>

<script type="text/javascript">
	$(document).ready(function() {
		$('span[action="delete-kategori"]').on('click', function() {
			if (confirm('Apakah data ini akan dihapus ?'))
			{
				var id_kategori_soal = $(this).data('id-kategori-soal');
				var span = this;

				$.ajax({
					type: 'POST',
					url: 'penerimaan-kuisioner.php',
					data: 'mode=delete-kategori&id_kategori_soal=' + id_kategori_soal
				}).done(function() {
					$(span).parentsUntil('tbody').remove();
				});
			}
		});

		$('span[action="delete-soal"]').on('click', function() {
			if (confirm('Apakah data ini akan dihapus ?'))
			{
				var id_soal = $(this).data('id-soal');
				var span = this;

				$.ajax({
					type: 'POST',
					url: 'penerimaan-kuisioner.php',
					data: 'mode=delete-soal&id_soal=' + id_soal
				}).done(function() {
					$(span).parentsUntil('tbody').remove();
				});
			}
		});
	});
</script>