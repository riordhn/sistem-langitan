<div class="center_title_bar">Master Form Kuisioner</div>

{if !empty($message)}
	<h3>{$message}</h3>
{/if}

<table>
	<thead>
		<tr>
			<th>No</th>
			<th>Nama Form Kuisioner</th>
			<th colspan="2">Aksi</th>
		</tr>
	</thead>
	<tbody>
		{foreach $form_set as $form}
			<tr>
				<td>{$form@index + 1}</td>
				<td>{$form.NM_FORM}</td>
				<td>
					<a href="penerimaan-kuisioner.php?mode=view-soal&id_form={$form.ID_FORM}">Soal</a>
				</td>
				<td>
					<a href="penerimaan-kuisioner.php?mode=edit&id_form={$form.ID_FORM}">Edit</a>
				</td>
			</tr>
		{/foreach}
		<tr>
			<td colspan="4" class="center">
				<a href="penerimaan-kuisioner.php?mode=add">Tambah Form Kuisioner</a>
			</td>
		</tr>
	</tbody>
</table>