<div class="center_title_bar">{$form.NM_FORM} - Edit Pilihan Jawaban</div>

<h4>Soal : {$soal.NOMER}. {$soal.SOAL}</h4>

{if !empty($message)}
	<h3>{$message}</h3>
{/if}

<form action="penerimaan-kuisioner.php?mode=edit-jawaban&id_form={$smarty.get.id_form}&id_soal={$smarty.get.id_soal}&id_jawaban={$smarty.get.id_jawaban}" method="post">
	<input type="hidden" name="mode" value="edit-jawaban" />
	<input type="hidden" name="id_jawaban" value="{$jawaban.ID_JAWABAN}" />
	<input type="hidden" name="id_soal" value="{$soal.ID_SOAL}" />
	<input type="hidden" name="tipe_jawaban" value="1" />
	<table>
		<thead>
			<tr>
				<th colspan="2">Pilihan Jawaban</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Urutan</td>
				<td>
					<input type="text" name="urutan" style="width: 50px" value="{$jawaban.URUTAN}"/>
				</td>
			</tr>
			<tr>
				<td>Jawaban</td>
				<td>
					<input type="text" name="jawaban" style="width: 200px" value="{$jawaban.JAWABAN}"/>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="center">
					<a href="penerimaan-kuisioner.php?mode=view-jawaban&id_form={$smarty.get.id_form}&id_soal={$soal.ID_SOAL}&id_jawaban={$smarty.get.id_jawaban}">Kembali</a>
					<input type="submit" value="Simpan" />
				</td>
			</tr>
		</tbody>
	</table>
</form>