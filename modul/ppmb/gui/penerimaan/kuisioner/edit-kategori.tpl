<div class="center_title_bar">{$form.NM_FORM} : Edit Kategori Soal</div>

{if !empty($message)}
	<h3>{$message}</h3>
{/if}

<form action="penerimaan-kuisioner.php?mode=edit-kategori&id_form={$smarty.get.id_form}&id_kategori_soal={$smarty.get.id_kategori_soal}" method="post">
	<input type="hidden" name="mode" value="edit-kategori" />
	<input type="hidden" name="id_form" value="{$form.ID_FORM}" />
	<input type="hidden" name="id_kategori_soal" value="{$kategori.ID_KATEGORI_SOAL}" />

	<table>
		<thead>
			<tr>
				<th colspan="2">Kategori Soal</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Kode Kategori</td>
				<td><input type="text" name="kode_kategori" value="{$kategori.KODE_KATEGORI}" style="width: 100px" /></td>
			</tr>
			<tr>
				<td>Nama Kategori</td>
				<td><input type="text" name="nm_kategori" value="{$kategori.NM_KATEGORI}" style="width: 300px"/></td>
			</tr>
			<tr>
				<td colspan="2" class="center">
					<a href="penerimaan-kuisioner.php?mode=view-soal&id_form={$smarty.get.id_form}">Kembali</a>
					<input type="submit" value="Simpan" />
				</td>
			</tr>
		</tbody>
	</table>
</form>