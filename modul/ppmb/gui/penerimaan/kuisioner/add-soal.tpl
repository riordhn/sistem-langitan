<div class="center_title_bar">{$form.NM_FORM} : Tambah Soal</div>

<form action="penerimaan-kuisioner.php?mode=view-soal&id_form={$smarty.get.id_form}" method="post">
	<input type="hidden" name="mode" value="add-soal" />
	<input type="hidden" name="id_form" value="{$form.ID_FORM}" />

	<table>
		<thead>
			<tr>
				<th colspan="2">Soal</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Nomer</td>
				<td>
					<input type="text" name="nomer" style="width: 50px"/>
				</td>
			</tr>
			<tr>
				<td>Kategori Soal</td>
				<td>
					<select name="id_kategori_soal">
						<option value="">--</option>
						{foreach $kategori_set as $kategori}
							<option value="{$kategori.ID_KATEGORI_SOAL}">[{$kategori.KODE_KATEGORI}] {$kategori.NM_KATEGORI}</option>
						{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td>Soal</td>
				<td>
					<textarea name="soal" placeholder="Tulis soal disini" cols="80" rows="2" style="font-family: Trebuchet MS"></textarea>
				</td>
			</tr>
			<tr>
				<td>Jenis Soal</td>
				<td>
					<select name="tipe_soal">
						{foreach $jenis_soal_set as $jenis_soal}
							<option value="{$jenis_soal.ID_JENIS_SOAL}">{$jenis_soal.NAMA_JENIS}</option>
						{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="center">
					<a href="penerimaan-kuisioner.php?mode=view-soal&id_form={$smarty.get.id_form}">Kembali</a>
					<input type="submit" value="Simpan" />
				</td>
			</tr>
		</tbody>
	</table>

</form>

<p>Keterangan : </p>
<ul>
	{foreach $jenis_soal_set as $jenis_soal}
		<li><strong>{$jenis_soal.NAMA_JENIS}</strong>, {$jenis_soal.KETERANGAN}</li>
	{/foreach}	
</ul>