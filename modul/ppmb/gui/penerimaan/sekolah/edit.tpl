<div class="center_title_bar">Master Kota - Edit Kota</div>

{if !empty($message)}
	<h3>{$message}</h3>
{/if}

<form action="penerimaan-kota.php?mode=edit&id_negara={$smarty.get.id_negara}&id_provinsi={$smarty.get.id_provinsi}&id_kota={$smarty.get.id_kota}" method="post">
	<input type="hidden" name="mode" value="edit" />
	<input type="hidden" name="id_kota" value="{$smarty.get.id_kota}" />
	<table>
		<thead>
			<tr>
				<th colspan="2">Detail Kota</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Negara</td>
				<td>{$negara.NM_NEGARA}</td>
			</tr>
			<tr>
				<td>Provinsi</td>
				<td>{$provinsi.NM_PROVINSI}</td>
			</tr>
			<tr>
				<td>Kode SNMPTN</td>
				<td>
					<input type="text" name="kode_snmptn" value="{$kota.KODE_SNMPTN}"/>
				</td>
			</tr>
			<tr>
				<td>Tipe Dati 2</td>
				<td>
					<select name="tipe_dati2">
						<option value="">--</option>
						<option value="KOTA" {if $kota.TIPE_DATI2 == 'KOTA'}selected{/if}>KOTA</option>
						<option value="KABUPATEN" {if $kota.TIPE_DATI2 == 'KABUPATEN'}selected{/if}>KABUPATEN</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Nama Kota/Kabupaten</td>
				<td>
					<input type="text" name="nm_kota" value="{$kota.NM_KOTA}"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<a href="penerimaan-kota.php?id_negara={$smarty.get.id_negara}&id_provinsi={$smarty.get.id_provinsi}">Kembali</a>
					<input type="submit" value="Simpan" />
				</td>
			</tr>
		</tbody>
	</table>
</form>