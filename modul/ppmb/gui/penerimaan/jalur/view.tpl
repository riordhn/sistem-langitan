<div class="center_title_bar">Master Jalur</div>

{if $result == 'add'}
	<h2>Jalur berhasil ditambahkan</h2>
{else if $result == 'add-failed'}
	<h2>Jalur gagal ditambahkan</h2>
{/if}

{if $result == 'edit'}
	<h2>Jalur berhasil diedit</h2>
{else if $result == 'edit-failed'}
	<h2>Jalur gagal diedit</h2>
{/if}

<table>
	<tr>
		<th>No</th>
		<th>Nama Jalur</th>
		<th>Jumlah Penerimaan</th>
		<th>Aksi</th>
	</tr>
	{foreach $jalur_set as $jalur}
	<tr {if $jalur@index is not div by 2}class="row1"{/if}>
		<td class="center">{$jalur@index + 1}</td>
		<td>{$jalur.NM_JALUR}</td>
		<td class="center">{$jalur.JUMLAH_PENERIMAAN}</td>
		<td>
			<a href="penerimaan-jalur.php?mode=edit&id={$jalur.ID_JALUR}">Edit</a>
		</td>
	</tr>
	{/foreach}
	<tr>
		<td class="center" colspan="4">
			<a href="penerimaan-jalur.php?mode=add">Tambah</a>
		</td>
	</tr>
</table>
	
<p>NB : Setiap penambahan jalur baru, mohon untuk di koordinasikan dengan Dit.Keuangan,
	karena akan berpengaruh pada proses pengaturan master biaya untuk proses verifikasi.</p>