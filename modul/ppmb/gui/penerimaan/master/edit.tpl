<div class="center_title_bar">Master Penerimaan - Edit</div>

{if $changed}
	<h2>{$changed}</h2>
{/if}

<form action="penerimaan-master.php?mode=edit&id_penerimaan={$smarty.get.id_penerimaan}" method="post">
	<input type="hidden" name="mode" value="edit" />
	<input type="hidden" name="id_penerimaan" value="{$penerimaan.ID_PENERIMAAN}" />
	<table>
		<tr>
			<th colspan="2">Info Penerimaan</th>
		</tr>
		<tr>
			<td>Tahun</td>
			<td>
				<input name="tahun" type="text" maxlength="4" size="5" value="{$penerimaan.TAHUN}" />
			</td>
		</tr>
		<tr>
			<td>Jalur</td>
			<td><select name="id_jalur">
				{foreach $jalur_set as $j}
					<option value="{$j.ID_JALUR}" {if $penerimaan.ID_JALUR == $j.ID_JALUR}selected="selected"{/if}>{$j.NM_JALUR}</option>
				{/foreach}
				</select>
			</td>
		</tr>
		<tr>
			<td>Jenjang</td>
			<td><select name="id_jenjang">
				{foreach $jenjang_set as $j}
					<option value="{$j.ID_JENJANG}" {if $penerimaan.ID_JENJANG == $j.ID_JENJANG}selected="selected"{/if}>{$j.NM_JENJANG}</option>
				{/foreach}
				</select>
			</td>
		</tr>
		<tr>
			<td>Nama Penerimaan</td>
			<td>
				<input type="text" name="nm_penerimaan" maxlength="50" size="50" value="{$penerimaan.NM_PENERIMAAN}" />
			</td>
		</tr>
		<tr>
			<td>Gelombang</td>
			<td><select name="gelombang">
				{foreach $gelombang_set as $g}
					<option value="{$g.ID}" {if $penerimaan.GELOMBANG == $g.ID}selected="selected"{/if}>{$g.VIEW}</option>
				{/foreach}
				</select>
			</td>
		</tr>
		<tr>
			<td>Semester</td>
			<td><select name="semester">
				{foreach $semester_set as $s}
					<option value="{$s.ID}" {if $penerimaan.SEMESTER == $s.ID}selected{/if}>{$s.VIEW}</option>
				{/foreach}
				</select>
			</td>
		</tr>
		<tr>
			<td>Jenis Online</td>
			<td>
				<select name="is_pendaftaran_online">
					<option value="1" {if $penerimaan.IS_PENDAFTARAN_ONLINE == 1}selected="selected"{/if}>Pendaftaran Online</option>
					<option value="0" {if $penerimaan.IS_PENDAFTARAN_ONLINE == 0}selected="selected"{/if}>Manual Voucher</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Pembukaan Registrasi</td>
			<td>
				{html_select_date prefix="tgl_awal_reg_" field_order="DMY" year_as_text=true all_empty="" time=$penerimaan.TGL_AWAL_REGISTRASI}
				Jam <input type="text" name="jam_awal_registrasi" value="{date('H:i', strtotime($penerimaan.TGL_AWAL_REGISTRASI))}" size="5"/>
			</td>
		</tr>
		<tr>
			<td>Penutupan Registrasi</td>
			<td>
				{html_select_date prefix="tgl_akhir_reg_" field_order="DMY" year_as_text=true all_empty="" time=$penerimaan.TGL_AKHIR_REGISTRASI}
				Jam <input type="text" name="jam_akhir_registrasi" value="{date('H:i', strtotime($penerimaan.TGL_AKHIR_REGISTRASI))}" size="5"/>
			</td>
		</tr>
		<tr>
			<td>Pembukaan Verifikasi</td>
			<td>
				{html_select_date prefix="tgl_awal_ver_" field_order="DMY" year_as_text=true all_empty="" time=$penerimaan.TGL_AWAL_VERIFIKASI}
				Jam <input type="text" name="jam_awal_verifikasi" value="{date('H:i', strtotime($penerimaan.TGL_AWAL_VERIFIKASI))}" size="5"/>
			</td>
		</tr>
		<tr>
			<td>Penutupan Verifikasi</td>
			<td>
				{html_select_date prefix="tgl_akhir_ver_" field_order="DMY" year_as_text=true all_empty="" time=$penerimaan.TGL_AKHIR_VERIFIKASI}
				Jam <input type="text" name="jam_akhir_verifikasi" value="{date('H:i', strtotime($penerimaan.TGL_AKHIR_VERIFIKASI))}" size="5"/>
			</td>
		</tr>
		<tr>
			<td>Pembukaan Voucher (Pembayaran)</td>
			<td>
				{html_select_date prefix="tgl_awal_voucher_" field_order="DMY" year_as_text=true all_empty="" time=$penerimaan.TGL_AWAL_VOUCHER}
				Jam <input type="text" name="jam_awal_voucher" value="{date('H:i', strtotime($penerimaan.TGL_AWAL_VOUCHER))}" size="5" />
			</td>
		</tr>
		<tr>
			<td>Penutupan Voucher (Pembayaran)</td>
			<td>
				{html_select_date prefix="tgl_akhir_voucher_" field_order="DMY" year_as_text=true all_empty="" time=$penerimaan.TGL_AKHIR_VOUCHER}
				Jam <input type="text" name="jam_akhir_voucher" value="{date('H:i', strtotime($penerimaan.TGL_AKHIR_VOUCHER))}" size="5" />
			</td>
		</tr>
		<tr>
			<td>Pengumuman</td>
			<td>
				{html_select_date prefix="tgl_pengumuman_" field_order="DMY" year_as_text=true all_empty="" time=$penerimaan.TGL_PENGUMUMAN}
				Jam <input type="text" name="jam_pengumuman" value="{date('H:i', strtotime($penerimaan.TGL_PENGUMUMAN))}" size="5"/>
			</td>
		</tr>
		<tr>
			<td>Status Aktif</td>
			<td><select name="is_aktif">
				{foreach $status_set as $s}
					<option value="{$s.ID}" {if $s.ID == $penerimaan.IS_AKTIF}selected="selected"{/if}>{$s.VIEW}</option>
				{/foreach}
				</select>
			</td>
		</tr>
		<tr>
			<td>Nomer Ujian Awal</td>
			<td><input type="text" name="no_ujian_awal" maxlength="11" size="12" value="{$penerimaan.NO_UJIAN_AWAL}" /></td>
		</tr>
		<tr>
			<td>Nomer Ujian Akhir</td>
			<td><input type="text" name="no_ujian_akhir" maxlength="11" size="12" value="{$penerimaan.NO_UJIAN_AKHIR}" /></td>
		</tr>
		<tr>
			<td>Memakai Ujian</td>
			<td>
				<select name="is_ujian">
					<option></option>
					<option value="0" {if $penerimaan.IS_UJIAN == '0'}selected="selected"{/if}>Tidak</option>
					<option value="1" {if $penerimaan.IS_UJIAN == '1'}selected="selected"{/if}>Ya</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Pengambilan Nomor</td>
			<td>
				<select name="pengambilan_nomor">
					<option value="1" {if $penerimaan.PENGAMBILAN_NOMOR == 1}selected="selected"{/if}>Acak</option>
					<option value="2" {if $penerimaan.PENGAMBILAN_NOMOR == 2}selected="selected"{/if}>Urut</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Kuisioner</td>
			<td>
				<select name="id_form_kuisioner">
					<option value=""></option>
					{foreach $kuisioner_set as $kuisioner}
						<option value="{$kuisioner.ID_FORM}" {if $kuisioner.ID_FORM == $penerimaan.ID_FORM_KUISIONER}selected{/if}>{$kuisioner.NM_FORM}</option>
					{/foreach}
				</select>
			</td>
		</tr>
		<tr>
			<td>Jumlah Pilihan Prodi</td>
			<td>
				<select name="jumlah_pilihan_prodi">
					<option></option>
					<option value="1" {if $penerimaan.JUMLAH_PILIHAN_PRODI == 1}selected="selected"{/if}>1</option>
					<option value="2" {if $penerimaan.JUMLAH_PILIHAN_PRODI == 2}selected="selected"{/if}>2</option>
					<option value="3" {if $penerimaan.JUMLAH_PILIHAN_PRODI == 3}selected="selected"{/if}>3</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Verifikasi Berkas</td>
			<td>
				<select name="is_verifikasi">
					<option></option>
					<option value="0" {if $penerimaan.IS_VERIFIKASI == 0}selected="selected"{/if}>Tidak</option>
					<option value="1" {if $penerimaan.IS_VERIFIKASI == 1}selected="selected"{/if}>Ya</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Pembayaran Sebelum Isi Form</td>
			<td>
				<select name="is_bayar_voucher">
					<option></option>
					<option value="0" {if $penerimaan.IS_BAYAR_VOUCHER == 0}selected="selected"{/if}>Tidak</option>
					<option value="1" {if $penerimaan.IS_BAYAR_VOUCHER == 1}selected="selected"{/if}>Ya</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Nomor Rekening Pembayaran</td>
			<td>
				<input type="text" name="nomor_rekening" maxlength="60" size="60" value="{$penerimaan.NOMOR_REKENING_TRANSFER}" />
			</td>
		</tr>
		<tr>
			<td colspan="2" class="center">
				<a class="button" href="penerimaan-master.php">Kembali</a>
				<input type="submit" value="Simpan" />
			</td>
		</tr>
	</table>
</form>
	
<p>Keterangan :<br/>Untuk penerimaan Spesialis, Profesi, Kelas Kerjasama mohon jalur di set yang sesuai.</p>