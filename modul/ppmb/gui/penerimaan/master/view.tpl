<div class="center_title_bar">Master Penerimaan</div>

<form action="penerimaan-master.php" method="get">
<table>
	<tr>
		<td>Tahun :
			<select name="tahun">
				<option value=""></option>
			{$tahun_awal = $tahun_set[0]['TAHUN_AWAL']}
			{$tahun_akhir = $tahun_set[0]['TAHUN_AKHIR']}
			{for $tahun = $tahun_awal to $tahun_akhir}
				<option value="{$tahun}" 
				{if $smarty.get.tahun}
					{if $smarty.get.tahun == $tahun}selected="selected"{/if}
				{else}
					{if $tahun == date('Y')}selected="selected"{/if}
				{/if}
				>{$tahun}</option>
			{/for}
			</select>
		</td>
		<td>Aktif :
			<select name="aktif">
				<option value="all" {if $smarty.get.aktif}{if $smarty.get.aktif == 'all'}selected="selected"{/if}{/if}>Semua</option>
				<option value="aktif" {if $smarty.get.aktif}{if $smarty.get.aktif == 'aktif'}selected="selected"{/if}{/if}>Aktif</option>
				<option value="non-aktif" {if $smarty.get.aktif}{if $smarty.get.aktif == 'non-aktif'}selected="selected"{/if}{/if}>Tidak Aktif</option>
			</select>
		</td>
		<td><input type="submit" value="Lihat" /></td>
	</tr>
</table>
</form>

{if $added}
<h2>{$added}</h2>
{/if}

<table>
	<tr>
		<th>ID</th>
		<th>Tahun</th>
		<th>Jenjang</th>
		<th>Jalur</th>
		<th>Penerimaan</th>
		<th>Gel</th>
		<th>Semester</th>
		<th>Status</th>
		<th>Pengumuman</th>
		<!-- <th>Jumlah Pilihan Prodi</th>
		<th>Verifikasi Berkas</th> -->
		<th>Aksi</th>
	</tr>
	{foreach $penerimaan_set as $p}
	<tr {if $p@index is not div by 2}class="row1"{/if}>
		<td class="center">{$p.ID_PENERIMAAN}</td>
		<td class="center">{$p.TAHUN}</td>
		<td class="center">{substr($p.NM_JENJANG,0,2)}</td>
		<td>{$p.NM_JALUR}</td>
		<td>{$p.NM_PENERIMAAN}</td>
		<td class="center">{$p.GELOMBANG}</td>
		<td class="center">{$p.SEMESTER}</td>
		<td class="center">{if $p.IS_AKTIF}Aktif{else}Tidak Aktif{/if}</td>
		<td class="center">{if $p.PENGUMUMAN <= date('Y-m-d')}Publish{else}-{/if}</td>
		<!-- <td class="center">{$p.JUMLAH_PILIHAN_PRODI}</td>
		<td class="center">{if $p.IS_VERIFIKASI == '1'}Ya{else}Tidak{/if}</td> -->
		<td>
			<a href="penerimaan-master.php?mode=edit&id_penerimaan={$p.ID_PENERIMAAN}">Edit</a>
		</td>
	</tr>
	{/foreach}
	<tr>
		<td colspan="11" class="center">
			{if $master_semester == 0}
				<!-- <p>Master Semester Akademik untuk Tahun {$tahun_get} belum diset oleh Dit. Pendidikan.<br/>Silahkan hubungi Dit. Pendidikan terlebih dahulu.</p> -->
				<p>Master Semester Akademik untuk Tahun {$tahun_get} belum diset melalui modul Pendidikan</p>
			{else}
			<a class="button" href="penerimaan-master.php?mode=add">Tambah Penerimaan</a>
			{/if}
		</td>
	</tr>
</table>

<!-- <span>*NB : <br/>PMDK = Mandiri</span> -->