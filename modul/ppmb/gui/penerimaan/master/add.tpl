<div class="center_title_bar">Master Penerimaan - Tambah</div>

<form action="penerimaan-master.php" method="post">
<input type="hidden" name="mode" value="add" />
<table>
	<tr>
		<th colspan="2">Info Penerimaan</th>
	</tr>
	<tr>
		<td>Tahun</td>
		<td>
			<input name="tahun" type="text" maxlength="4" size="5" />
		</td>
	</tr>
	<tr>
		<td>Jalur</td>
		<td><select name="id_jalur">
			{foreach $jalur_set as $j}
				<option value="{$j.ID_JALUR}">{$j.NM_JALUR}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Jenjang</td>
		<td><select name="id_jenjang">
			{foreach $jenjang_set as $j}
				<option value="{$j.ID_JENJANG}">{$j.NM_JENJANG}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Nama Penerimaan</td>
		<td>
			<input type="text" name="nm_penerimaan" maxlength="50" size="50"/>
		</td>
	</tr>
	<tr>
		<td>Gelombang</td>
		<td><select name="gelombang">
			{foreach $gelombang_set as $g}
				<option value="{$g.ID}">{$g.VIEW}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Semester</td>
		<td><select name="semester">
			{foreach $semester_set as $s}
				<option value="{$s.ID}">{$s.VIEW}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
			<td>Jenis Online</td>
			<td>
				<select name="is_pendaftaran_online">
					<option value="1">Pendaftaran Online</option>
					<option value="0">Manual Voucher</option>
				</select>
			</td>
	</tr>
	<tr>
		<td>Pembukaan Registrasi</td>
		<td>
			{html_select_date prefix="tgl_awal_reg_" field_order="DMY" year_as_text=true all_empty=""}
			Jam <input type="text" name="jam_awal_registrasi" value="00:00" size="5"/>
		</td>
	</tr>
	<tr>
		<td>Penutupan Registrasi</td>
		<td>
			{html_select_date prefix="tgl_akhir_reg_" field_order="DMY" year_as_text=true all_empty=""}
			Jam <input type="text" name="jam_akhir_registrasi" value="23:59" size="5"/>
		</td>
	</tr>
	<tr>
		<td>Pembukaan Verifikasi</td>
		<td>
			{html_select_date prefix="tgl_awal_ver_" field_order="DMY" year_as_text=true all_empty=""}
			Jam <input type="text" name="jam_awal_verifikasi" value="08:00" size="5"/>
		</td>
	</tr>
	<tr>
		<td>Penutupan Verifikasi</td>
		<td>
			{html_select_date prefix="tgl_akhir_ver_" field_order="DMY" year_as_text=true all_empty=""}
			Jam <input type="text" name="jam_akhir_verifikasi" value="16:00" size="5"/>
		</td>
	</tr>
	<tr>
	   <td>Pembukaan Voucher</td>
	   <td>
		   {html_select_date prefix="tgl_awal_voucher_" field_order="DMY" year_as_text=true all_empty=""}
		   Jam <input type="text" name="jam_awal_voucher" value="08:00" size="5" />
	   </td>
   </tr>
   <tr>
	   <td>Penutupan Voucher</td>
	   <td>
		   {html_select_date prefix="tgl_akhir_voucher_" field_order="DMY" year_as_text=true all_empty=""}
		   Jam <input type="text" name="jam_akhir_voucher" value="23:59" size="5" />
	   </td>
   </tr>
   <tr>
		<td>Pengumuman</td>
		<td>
			{html_select_date prefix="tgl_pengumuman_" field_order="DMY" year_as_text=true all_empty=""}
			Jam <input type="text" name="jam_pengumuman" value="19:00" size="5" />
		</td>
	</tr>
	<tr>
		<td>Status Aktif</td>
		<td><select name="is_aktif">
			{foreach $status_set as $s}
				<option value="{$s.ID}">{$s.VIEW}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Nomer Ujian Awal</td>
		<td><input type="text" name="no_ujian_awal" maxlength="11" /></td>
	</tr>
	<tr>
		<td>Nomer Ujian Akhir</td>
		<td><input type="text" name="no_ujian_akhir" maxlength="11" /></td>
	</tr>
	<tr>
		<td>Memakai Ujian</td>
		<td>
			<select name="is_ujian">
				<option></option>
				<option value="0">Tidak</option>
				<option value="1">Ya</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>Pengambilan Nomor</td>
		<td>
			<select name="pengambilan_nomor">
				<option value="">--</option>
				<option value="1">Acak</option>
				<option value="2">Urut</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>Kuisioner</td>
		<td>
			<select name="id_form_kuisioner">
				<option value=""></option>
				{foreach $kuisioner_set as $kuisioner}
					<option value="{$kuisioner.ID_FORM}">{$kuisioner.NM_FORM}</option>
				{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Jumlah Pilihan Prodi</td>
		<td>
			<select name="jumlah_pilihan_prodi">
				<option></option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>Verifikasi Berkas</td>
		<td>
			<select name="is_verifikasi">
				<option></option>
				<option value="0">Tidak</option>
				<option value="1">Ya</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>Pembayaran Sebelum Isi Form</td>
		<td>
			<select name="is_bayar_voucher">
				<option></option>
				<option value="0">Tidak</option>
				<option value="1">Ya</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>Nomor Rekening Pembayaran</td>
		<td>
			<input type="text" name="nomor_rekening" maxlength="60" size="60"/>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="center">
			<a class="button" href="penerimaan-master.php">Kembali</a>
			<input type="submit" value="Simpan" />
		</td>
	</tr>
</table>
</form>
			
<p>Keterangan :<br/>Untuk penerimaan Spesialis, Profesi, Kelas Kerjasama mohon jalur di set yang sesuai.</p>