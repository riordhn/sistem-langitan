<div class="center_title_bar">Master Ujian - Tambah</div>

<form action="penerimaan-ujian.php" method="post">
    <input type="hidden" name="mode" value="add" />
    <table>
        <tr>
            <th colspan="2">Detail Master Ujian</th>
        </tr>
        <tr>
            <td>Nama Ujian</td>
            <td>
                <input type="text" name="nm_ujian" size="50" maxlength="100" />
            </td>
        </tr>
        <tr>
            <td>No SK</td>
            <td>
                <input type="text" name="no_sk" size="30" maxlength="50" />
            </td>
        </tr>
        <tr>
            <td>Tanggal Ujian</td>
            <td>
                {html_select_date field_order='DMY' prefix="tgl_ujian_" start_year='-10'}
            </td>
        </tr>
        <tr>
            <td>Gelombang</td>
            <td>
                <select name="gelombang">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Semester</td>
            <td>
                <select name="semester">
                    <option value="Gasal">Gasal</option>
                    <option value="Genap">Genap</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Tahun</td>
            <td>
                <input type="text" name="tahun" size="4" maxlength="4"/>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <a href="penerimaan-ujian.php">Kembali</a>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
