<div class="center_title_bar">Master Ujian - List Penerimaan</div>


    
<table>
    <caption>{$ujian.NM_UJIAN} Gelombang {$ujian.GELOMBANG} Semester {$ujian.SEMESTER} Tahun {$ujian.TAHUN}</caption>
    <tr>
        <th>No</th>
        <th>Jalur</th>
        <th>Jenjang</th>
        <th>Penerimaan</th>
        <th>Gelombang</th>
        <th>Semester</th>
        <th>Tahun</th>
        <th>Aksi</th>
    </tr>
    {foreach $penerimaan_set as $p}
    <tr>
        <td class="center">{$p@index + 1}</td>
        <td>{$p.NM_JALUR}</td>
        <td>{$p.NM_JENJANG}</td>
        <td>{$p.NM_PENERIMAAN}</td>
        <td class="center">{$p.GELOMBANG}</td>
        <td class="center">{$p.SEMESTER}</td>
        <td class="center">{$p.TAHUN}</td>
        <td>
            <span class="anchor-span" action="del-penerimaan" id-penerimaan="{$p.ID_PENERIMAAN}">Hapus</span>
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="8">
            <a href="penerimaan-ujian.php?mode=add-penerimaan&id_ujian={$ujian.ID_UJIAN}">Tambah Penerimaan</a>
        </td>
    </tr>
</table>
        
<a href="penerimaan-ujian.php">Kembali</a>
        
<script type="text/javascript">
    $('span[action="del-penerimaan"]').each(function(i, e) {
        $(this).click(function() {
            
            var span = this;
            var id_penerimaan = $(this).attr('id-penerimaan');
            
            var x = document.createElement('div');
            
            $(x).html('<p>Apakah penerimaan ini akan di hapus ?</p>');
            $(x).dialog({
                title: 'Konfirmasi penghapusan penerimaan',
                modal: true,
                buttons: {
                    1: {
                        text: 'Batal',
                        click: function() { $(x).dialog('destroy'); }
                    },
                    2: {
                        text: 'Ya',
                        click: function() {
                            $.ajax({
                                type: 'POST',
                                url: 'penerimaan-ujian.php',
                                data: 'mode=del-penerimaan&id_penerimaan='+id_penerimaan,
                                success: function(r) {
                                    if (r == 1)
                                        $(span).parent().parent().remove();
                                    else
                                        alert('Penerimaan gagal dihapus');
                                    $(x).dialog('destroy');
                                }
                            });
                        }
                    }
                }
            });
            
        });
    });
</script>