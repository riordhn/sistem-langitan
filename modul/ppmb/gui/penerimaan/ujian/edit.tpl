<div class="center_title_bar">Master Ujian - Edit</div>

<form action="penerimaan-ujian.php" method="post">
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="id_ujian" value="{$smarty.get.id_ujian}" />
    <table>
        <tr>
            <th colspan="2">Detail Master Ujian</th>
        </tr>
        <tr>
            <td>Nama Ujian</td>
            <td>
                <input type="text" name="nm_ujian" size="50" maxlength="100" value="{$ujian.NM_UJIAN}" />
            </td>
        </tr>
        <tr>
            <td>No SK</td>
            <td>
                <input type="text" name="no_sk" size="30" maxlength="50" value="{$ujian.NO_SK}" />
            </td>
        </tr>
        <tr>
            <td>Tanggal Ujian</td>
            <td>
                {html_select_date field_order='DMY' prefix="tgl_ujian_" start_year='-10' time=$ujian.TGL_UJIAN}
            </td>
        </tr>
        <tr>
            <td>Gelombang</td>
            <td>
                <select name="gelombang">
                    <option value="1" {if $ujian.GELOMBANG == 1}selected="selected"{/if}>1</option>
                    <option value="2" {if $ujian.GELOMBANG == 2}selected="selected"{/if}>2</option>
                    <option value="3" {if $ujian.GELOMBANG == 3}selected="selected"{/if}>3</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Semester</td>
            <td>
                <select name="semester">
                    <option value="Gasal" {if $ujian.SEMESTER == 'Gasal'}selected="selected"{/if}>Gasal</option>
                    <option value="Genap" {if $ujian.SEMESTER == 'Genap'}selected="selected"{/if}>Genap</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Tahun</td>
            <td>
                <input type="text" name="tahun" size="4" maxlength="4" value="{$ujian.TAHUN}"/>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <a href="penerimaan-ujian.php">Kembali</a>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
