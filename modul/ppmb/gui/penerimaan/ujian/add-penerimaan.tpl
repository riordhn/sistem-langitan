<div class="center_title_bar">Master Ujian - Tambah Penerimaan</div>

<form action="penerimaan-ujian.php?mode=list-penerimaan&id_ujian={$smarty.get.id_ujian}" method="post">
    <input type="hidden" name="id_ujian" value="{$smarty.get.id_ujian}" />
    <input type="hidden" name="mode" value="add-penerimaan" />
    <table>
        <tr>
            <th colspan="2">Tambah Penerimaan</th>
        </tr>
        <tr>
            <td>Penerimaan</td>
            <td>
                <select name="id_penerimaan">
                    <option value="">-- Pilih Penerimaan --</option>
                {foreach $penerimaan_set as $p}
                    <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                    {foreach $p.p_set as $p2}
                    <option value="{$p2.ID_PENERIMAAN}">Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN}</option>
                    {/foreach}
                    </optgroup>
                {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <a href="penerimaan-ujian.php?mode=list-penerimaan&id_ujian={$smarty.get.id_ujian}">Kembali</a>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>