<div class="center_title_bar">Master Ujian</div>

{if $result}<h2>{$result}</h2>{/if}

<table>
    <tr>
        <th>No</th>
        <th>Nama Ujian</th>
        <th>No SK</th>
        <th>Tgl Ujian</th>
        <th title="Gelombang">Gel.</th>
        <th title="Semester">Sem.</th>
        <th>Tahun</th>
        <th>Aksi</th>
    </tr>
    {foreach $ujian_set as $u}
    <tr>
        <td class="center">{$u@index + 1}</td>
        <td>{$u.NM_UJIAN}</td>
        <td>{$u.NO_SK}</td>
        <td class="center">{$u.TGL_UJIAN|date_format:"%d %B %Y"}</td>
        <td class="center">{$u.GELOMBANG}</td>
        <td class="center">{$u.SEMESTER}</td>
        <td class="center">{$u.TAHUN}</td>
        <td>
            <a href="penerimaan-ujian.php?mode=edit&id_ujian={$u.ID_UJIAN}">Edit</a>
            <a href="penerimaan-ujian.php?mode=list-penerimaan&id_ujian={$u.ID_UJIAN}">Penerimaan</a>
            <span class="anchor-span" action="del-ujian" id-ujian="{$u.ID_UJIAN}">Hapus</span>
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="8" class="center">
            <a href="penerimaan-ujian.php?mode=add">Tambah Ujian</a>
        </td>
    </tr>
</table>
    
<script type="text/javascript">
    $('span[action="del-ujian"]').each(function(i, e) {
        $(this).click(function() {
            
            var span = this;
            var id_ujian = $(this).attr('id-ujian');
            
            var x = document.createElement('div');
            
            $(x).html('<p>Apakah master ujian ini akan di hapus ?</p>');
            $(x).dialog({
                title: 'Konfirmasi penghapusan master ujian',
                modal: true,
                buttons: {
                    1: {
                        text: 'Batal',
                        click: function() { $(x).dialog('destroy'); }
                    },
                    2: {
                        text: 'Ya',
                        click: function() {
                            $.ajax({
                                type: 'POST',
                                url: 'penerimaan-ujian.php',
                                data: 'mode=del-ujian&id_ujian='+id_ujian,
                                success: function(r) {
                                    if (r == 1)
                                        $(span).parent().parent().remove();
                                    else
                                        alert('Master ujian gagal dihapus');
                                    $(x).dialog('destroy');
                                }
                            });
                        }
                    }
                }
            });
            
        });
    });
</script>