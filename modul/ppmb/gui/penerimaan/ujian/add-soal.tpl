<div class="center_title_bar">{$form.NM_FORM} : Tambah Soal</div>

<form action="penerimaan-kuisioner.php?mode=view-soal&id_form={$smarty.get.id_form}" method="post">
	<input type="hidden" name="mode" value="add-soal" />
	<input type="hidden" name="id_form" value="{$form.ID_FORM}" />

	<table>
		<thead>
			<tr>
				<th>Soal</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Nomer</td>
				<td>
					<input type="text" name="nomer" />
				</td>
			</tr>
			<tr>
				<td>Kategori Soal</td>
				<td>
					<select name="id_kategori_soal">
						<option value="">--</option>
						{foreach $kategori_set as $kategori}
							<option value="">[{$kategori.KODE_KATEGORI}] {$kategori.NM_KATEGORI}</option>
						{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td>Soal</td>
				<td>
					<input type="text" name="soal" style="width: 100%" />
				</td>
			</tr>
		</tbody>
	</table>

</form>