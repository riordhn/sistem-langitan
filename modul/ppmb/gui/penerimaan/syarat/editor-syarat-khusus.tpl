<div class="center_title_bar">Tambah Syarat Khusus Program Studi</div>

<form action="penerimaan-syarat.php?id_penerimaan={$smarty.get.id_penerimaan}" method="post">
	<table>
		<thead>
			<tr>
				<th colspan="2">Detail Syarat Khusus</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Penerimaan</td>
				<td>{$data.NM_PENERIMAAN}</td>
			</tr>
			<tr>
				<td>Program Studi</td>
				<td>
					<select name="id_program_studi">
						{foreach $program_studi_set as $ps}
							<option value="{$ps.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI == $ps.ID_PROGRAM_STUDI}selected{/if}>{$ps.NM_PROGRAM_STUDI}</option>
						{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td>Syarat</td>
				<td>
					<input type="text" name="nm_syarat" value="{$data.NM_SYARAT}" size="75" autocomplete="off" />
				</td>
			</tr>
			<tr>
				<td>Keterangan</td>
				<td>
					<textarea name="keterangan" rows="5" cols="100">{$data.KETERANGAN}</textarea>
				</td>
			</tr>
			<tr>
				<td>Status Wajib</td>
				<td>
					<select name="status_wajib">
						<option value="0" {if $data.STATUS_WAJIB == 0}selected{/if}>Tidak Wajib</option>
						<option value="1" {if $data.STATUS_WAJIB == 1}selected{/if}>Wajib</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Upload File</td>
				<td>
					<select name="status_file">
						<option value="0" {if $data.STATUS_FILE == 0}selected{/if}>Tidak</option>
						<option value="1" {if $data.STATUS_FILE == 1}selected{/if}>Ya</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Urutan</td>
				<td>
					<input type="text" name="urutan" value="{$data.URUTAN}" size="3" autocomplete="off" />
				</td>
			</tr>
			<tr>
				<td colspan="2" class="center">
					<input type="hidden" name="id_penerimaan" value="{$smarty.get.id_penerimaan}" />
					<input type="hidden" name="mode" value="{$smarty.get.mode}" />
					{if !empty($smarty.get.id_syarat_prodi)}
						<input type="hidden" name="id_syarat_prodi" value="{$smarty.get.id_syarat_prodi}" />
					{/if}
					<input type="submit" value="Submit" />
					<a href="penerimaan-syarat.php?id_penerimaan={$smarty.get.id_penerimaan}">Kembali</a>
				</td>
			</tr>
		</tbody>
	</table>
</form>