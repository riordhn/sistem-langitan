<div class="center_title_bar">Setting Syarat Program Studi</div>

<form action="penerimaan-syarat.php" method="get" id="filter">
    <table class="filter" border="1">
        <tr>
            <td>Penerimaan</td>
            <td>
                <select name="id_penerimaan" onchange="$('#filter').submit();
						return false;">
                    <option value="">-- Pilih Penerimaan --</option>
					{foreach $penerimaan_set as $p}
						<optgroup label="{$p.TAHUN} {$p.SEMESTER}">
							{foreach $p.p_set as $p2}
								<option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} </option>
							{/foreach}
						</optgroup>
					{/foreach}
                </select>
            </td>
		</tr>
    </table>
</form>

{if !empty($smarty.get.id_penerimaan)}
	<h2>Syarat Umum</h2>
	<table>
		<thead>
			<tr>
				<th>No</th>
				<th>Syarat</th>
				<th>Keterangan</th>
				<th>Status</th>
				<th>Upload</th>
				<th>Urutan</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			{foreach $syarat_umum_set as $su}
				<tr>
					<td>{$su@index + 1}</td>
					<td>{$su.NM_SYARAT}</td>
					<td>{$su.KETERANGAN}</td>
					<td>{if $su.STATUS_WAJIB == 1}Wajib{else}Tidak Wajib{/if}</td>
					<td>{if $su.STATUS_FILE == 1}Ya{else}Tidak{/if}</td>
					<td class="center">{$su.URUTAN}</td>
					<td>
						<a href="penerimaan-syarat.php?mode=edit-syarat-umum&id_penerimaan={$smarty.get.id_penerimaan}&id_syarat_prodi={$su.ID_SYARAT_PRODI}">Edit</a>
						<span class="anchor-span" action="delete-syarat" data-id-syarat-prodi="{$su.ID_SYARAT_PRODI}">Delete</span>
					</td>
				</tr>
			{/foreach}
			<tr>
				<td colspan="7">
					<a href="penerimaan-syarat.php?mode=add-syarat-umum&id_penerimaan={$smarty.get.id_penerimaan}">Tambah syarat umum</a>
				</td>
			</tr>
		</tbody>
	</table>

	<h2>Syarat Khusus Prodi</h2>
	<table>
		<thead>
			<tr>
				<th>No</th>
				<th>Program Studi</th>
				<th>Syarat</th>
				<th>Keterangan</th>
				<th>Status</th>
				<th>Upload</th>
				<th>Urutan</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			{foreach $syarat_khusus_set as $sk}
				<tr>
					<td>{$sk@index + 1}</td>
					<td>{$sk.NM_JENJANG} {$sk.NM_PROGRAM_STUDI}</td>
					<td>{$sk.NM_SYARAT}</td>
					<td>{$sk.KETERANGAN}</td>
					<td>{if $sk.STATUS_WAJIB == 1}Wajib{else}Tidak Wajib{/if}</td>
					<td>{if $sk.STATUS_FILE == 1}Ya{else}Tidak{/if}</td>
					<td class="center">{$sk.URUTAN}</td>
					<td>
						<a href="penerimaan-syarat.php?mode=edit-syarat-khusus&id_penerimaan={$smarty.get.id_penerimaan}&id_syarat_prodi={$sk.ID_SYARAT_PRODI}">Edit</a>
						<span class="anchor-span" action="delete-syarat" data-id-syarat-prodi="{$sk.ID_SYARAT_PRODI}">Delete</span>
					</td>
				</tr>
			{/foreach}
			<tr>
				<td colspan="8">
					<a href="penerimaan-syarat.php?mode=add-syarat-khusus&id_penerimaan={$smarty.get.id_penerimaan}">Tambah syarat khusus prodi</a>
				</td>
			</tr>
		</tbody>
	</table>

	<script type="text/javascript">
	jQuery(document).ready(function() {
		$('span[action="delete-syarat"]').click(function() { 
			var result = confirm('Apakah data ini akan dihapus ?');
			if (result) {
				var id_syarat_prodi = $(this).attr('data-id-syarat-prodi');
				$.ajax({
					url: 'penerimaan-syarat.php',
					type: 'POST',
					data: 'mode=delete-syarat&id_syarat_prodi=' + id_syarat_prodi,
					success: function() {
						window.location.reload();
					}
				});
			}
		});
	});
	</script>
{/if}