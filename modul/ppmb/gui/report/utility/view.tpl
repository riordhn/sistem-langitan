<div class="center_title_bar">Utility Report Penerimaan</div>

<table>
	<thead>
		<tr>
			<th>Thn</th>
			<th>Penerimaan</th>
			<th>Gelombang</th>
			<th>File<br/>Upload</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		{foreach $penerimaan_set as $p}
		<tr {if $p@index is div by 2}class="row1"{/if}>
			<td>{$p.TAHUN}</td>
			<td>
				{$p.NM_PENERIMAAN} {if in_array($p.ID_JENJANG, array(2,3,9,10))}Semester {$p.SEMESTER}{/if}
			</td>
			<td class="center">
				{$p.GELOMBANG}
			</td>
			<td>
				<button data-action="fu_zip" data-id="{$p.ID_PENERIMAAN}">Zip ({$p.JUMLAH_FILE})</button>
				<br/>
				<button data-action="fu_dl" data-id="{$p.ID_PENERIMAAN}">Download</button>
			</td>
			<td>
				{* {if in_array($p.ID_JENJANG, array(2, 3, 9, 10))} *}
					<button data-action="dl_pdf" data-id="{$p.ID_PENERIMAAN}">Download Biodata PDF</button>
				{* {/if} *}
				{if in_array($p.ID_JENJANG, array(1, 4, 5))}
					{* {if in_array($p.ID_JALUR, array(3, 5, 20, 27))} *}
					<button data-action="refresh-pilihan" data-id="{$p.ID_PENERIMAAN}">Refresh Pilihan</button>
					{* {/if} *}
				{/if}
				<button data-action="kapitalis-nama" data-id="{$p.ID_PENERIMAAN}">Kapitalisasi Nama</button>
				{* <button data-action="copy-foto" data-id="{$p.ID_PENERIMAAN}">Copy Foto</button> *}
			</td>
		</tr>
		{/foreach}
	</tbody>
</table>

<p style="color: red">
	MOHON DIPERHATIKAN : Mohon mengkonsultasikan ke developer sebelum mengklik tombol zip, karena file bisa sangat besar.
</p>

<script type="text/javascript">
	
	function do_copy_foto(element, id_penerimaan, jumlah_file, urutan) {
		if (urutan <= jumlah_file)
		{
			/* Mulai melakukan copy foto */
			$.ajax({
				url: 'report-utility.php?mode=copy-foto-do&id_penerimaan=' + id_penerimaan + '&urutan=' + urutan,
				beforeSend: function() {
					$(element).html('proses copy... ('+(jumlah_file - urutan)+')').prop('disabled', true);
				},
				success: function() {
					urutan++;
					// recursive call
					do_copy_foto(element, id_penerimaan, jumlah_file, urutan);
				}
			});	
		}
		else
		{
			// re-enabled Button
			$(element).html('Copy Foto').prop('disabled', false);
		}
	}

	$(document).ready(function() {
		
		// Process per button item
		$('button').each(function(index, element) {

			var action = $(element).data('action');
			var id_penerimaan = $(element).data('id');

			// Saat tombol Zip File Upload
			if (action === 'fu_dl') {

				// Cek file 
				$.ajax({
					url: 'report-utility.php?mode=cek_zip_files&id_penerimaan=' + id_penerimaan,
					beforeSend: function() {
						// hidden dulu
						$(element).hide();
					},
					success: function(r) {
						// Jika sudah ada langsung ditampilkan
						if (r === '1') {
							$(element).show();

							// Binding click
							$(element).click(function(e) {
								e.preventDefault();
								// Link Download
								window.location = 'http://pendaftaran.unair.ac.id/files/download/' + id_penerimaan + '.tar';
							});
						}
					}
				});
			}

			if (action === 'fu_zip')
			{
				$(element).click(function(e) {
					e.preventDefault();
					
					$.ajax({
						url: 'report-utility.php?mode=zip_files&id_penerimaan=' + id_penerimaan,
						beforeSend: function() {
							// hidden dulu
							$(element).hide();
							// tampilkan loading images
							$(element).after('<img src="../../img/ppmb/small-ajax-loading.gif" />');
						},
						success: function(r) {
							if (r === 'OK') {
								// reshow button
								$(element).show();
								// refresh page
								window.location.reload();
							}
						}
					});
				});
				
			}

			if (action === 'dl_pdf')
			{
				$(element).click(function(e){
					e.preventDefault();
					window.open('http://pendaftaran.unair.ac.id/index.php/api/download_pdf/' + id_penerimaan);
				});
			}

			if (action === 'refresh-pilihan')
			{
				$(element).click(function(e){
					e.preventDefault();

					if (confirm('Proses generate pilihan bisa lama tergantung jumlah peserta. JANGAN melakukan refresh halaman jika belum selesai.\n\rApakah ingin melanjutkan ?'))
					{
						$.ajax({
							url: 'report-utility.php?mode=' + action + '&id_penerimaan=' + id_penerimaan,
							beforeSend: function() {
								$(element).html('loading...').prop('disabled', true);
							},
							success: function(r) {
								alert('Pesan dari server : ' + r);
								$(element).html('Refresh Pilihan').prop('disabled', false);
							}
						});
					}
				});
			}

			if (action === 'kapitalis-nama')
			{
				$(element).click(function(e){
					e.preventDefault();

					if (confirm('Mohon tidak melakukan refresh sebelum selesai.\n\rApakah ingin melanjutkan ?'))
					{
						$.ajax({
							url: 'report-utility.php?mode=' + action + '&id_penerimaan=' + id_penerimaan,
							beforeSend: function() {
								$(element).html('loading...').prop('disabled', true);
							},
							success: function(r) {
								alert('Pesan dari server : ' + r);
								$(element).html('Kapitalisasi Nama').prop('disabled', false);
							}
						});
					}
				});
			}

			if (action === 'copy-foto')
			{
				$(element).click(function(e){
					e.preventDefault();

					if (confirm('Anda akan melakukan eksekusi pemindahan foto dari server pendaftaran ke server cybercampus. Hendaknya melakukan aksi ini setelah verifikasi selesai. Mohon tidak melakukan refresh sebelum selesai.\n\rApakah ingin melanjutkan ?'))
					{
						/* Mengambil jumlah file */
						$.ajax({
							url: 'report-utility.php?mode=' + action + '&id_penerimaan=' + id_penerimaan,
							beforeSend: function() {
								$(element).html('loading...').prop('disabled', true);
							},
							success: function(r) {
								var jumlah_file = r;
								var urutan = 1;

								// exekusi copy foto (ajax recursive)
								do_copy_foto(element, id_penerimaan, jumlah_file, urutan);
							}
						});

							
					}
				});
			}

		});

	});
</script>