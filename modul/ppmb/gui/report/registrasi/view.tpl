<div class="center_title_bar">Report Registrasi</div>

{if count($penerimaan_set) > 0}
<table class="filter">
	<thead>
		<tr>
			<th>Thn</th>
			<th>Penerimaan</th>
			<th>Gel.</th>
			<th title="Submit form">Submit<br/>Form</th>
			<th title="Antri verifikasi">Antri<br/>Verifikasi</th>
			<th>Verifikasi<br/>Kembali</th>
			<th title="Verifikasi">Verifikasi</th>
			<th title="Sudah bayar">Bayar</th>
			<th title="Sudah plot ruang">Plot</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		{foreach $penerimaan_set as $p}
			<tr {if $p@index is div by 2}class="row1"{/if}>
				<td>{$p.TAHUN}</td>
				<td>
					{*{$p.NM_PENERIMAAN} Gelombang {$p.GELOMBANG} {if in_array($p.ID_JENJANG, array(2,3,9,10))}Semester {$p.SEMESTER}{/if}*}
					{$p.NAMA_PENERIMAAN}
				</td>
				<td class="center">{$p.GELOMBANG}</td>
				<td class="center">{$p.JUMLAH_SUBMIT_FORM}</td>
				<td class="center">{$p.JUMLAH_ANTRI_VERIFIKASI}</td>
				<td class="center">{$p.JUMLAH_VERIFIKASI_KEMBALI}</td>
				<td class="center">{$p.JUMLAH_VERIFIKASI}</td>
				<td class="center">{$p.JUMLAH_BAYAR}</td>
				<td class="center">{$p.JUMLAH_KARTU}</td>
				<td>
					<a href="report-registrasi.php?mode=prodi-isiform&id_penerimaan={$p.ID_PENERIMAAN}" title="Klik untuk rekap per prodi">Rekap</a>
					<a href="report-registrasi.php?mode=detail-isiform&id_penerimaan={$p.ID_PENERIMAAN}">Detail</a>
					<a href="report-registrasi.php?mode=detail-rekap&id_penerimaan={$p.ID_PENERIMAAN}">Plot</a>
					<a href="report-registrasi.php?mode=download-peserta&id_penerimaan={$p.ID_PENERIMAAN}" class="disable-ajax" title="Download Excel">
						<img src="/img/ppmb/icon-excel.png" />
					</a>
				</td>
			</tr>
		{/foreach}
	</tbody>
</table>
{/if}

{if count($penerimaan2_set) > 0}
<table class="filter">
	<caption>Penerimaan Model Lama</caption>
	<thead>
		<tr>
			<th>Thn</th>
			<th>Penerimaan</th>
			<th title="Bayar voucher">Bayar<br/>Voucher</th>
			<th title="Submit form">Submit<br/>Form</th>
			<th title="Verifikasi">Verifikasi</th>		
			<th></th>
		</tr>
	</thead>
	<tbody>
		{foreach $penerimaan2_set as $p2}
			<tr {if $p2@index is div by 2}class="row1"{/if}>
				<td>{$p2.TAHUN}</td>
				<td>
					{$p2.NAMA_PENERIMAAN}
				</td>
				<td class="center">{$p2.JUMLAH_BAYAR}</td>
				<td class="center">{$p2.JUMLAH_SUBMIT_FORM}</td>
				<td class="center">{$p2.JUMLAH_VERIFIKASI}</td>				
				<td>
					<a href="report-registrasi.php?mode=prodi-isiform&id_penerimaan={$p2.ID_PENERIMAAN}" title="Klik untuk rekap per prodi">Rekap</a>
					<a href="report-registrasi.php?mode=detail-isiform&id_penerimaan={$p2.ID_PENERIMAAN}">Detail</a>
					<a href="report-registrasi.php?mode=detail-rekap&id_penerimaan={$p2.ID_PENERIMAAN}">Plot</a>
					<a href="report-registrasi.php?mode=download-peserta&id_penerimaan={$p2.ID_PENERIMAAN}" class="disable-ajax" title="Download Excel">
						<img src="/img/ppmb/icon-excel.png" />
					</a>
				</td>
			</tr>
		{/foreach}
	</tbody>
</table>
{/if}
