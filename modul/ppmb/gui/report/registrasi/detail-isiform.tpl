<div class="center_title_bar">Detail Peserta {$nm_penerimaan}</div>

{foreach $penerimaan_prodi_set as $pp}

{*
{if in_array($penerimaan.ID_JALUR, array(4,6,23,24,35))}
    {$is_jalur_pasca = true}
{else}
    {$is_jalur_pasca = false}
{/if}
*}

<table style="width: 100%">
    <caption>{$pp.NM_PROGRAM_STUDI}</caption>
    <tr>
        <th>No</th>
        <th>No Ujian</th>
        <th>Nomor Pendaftaran</th>
		<!-- <th title="Jenis Voucher">J</th> -->
        <th>Nama</th>
        {* {if $is_jalur_pasca} *}{* ALIH-JENIS / PASCA / PROFESI / SPESIALIS *}
            <!-- <th>Gelar</th> -->
        {* {/if} *}
        <th>No HP</th>
        {* {if $is_jalur_pasca} *}{* ALIH-JENIS / PASCA / PROFESI / SPESIALIS *}
            <!-- <th>PT Asal</th> -->
        {* {else} *}
            <th>Asal Sekolah</th>
        {* {/if} *}
    </tr>
    {foreach $pp.cmb_set as $cmb}
    <tr {if $cmb@index is not div by 2}class="row1"{/if}>
        <td class="center">{$cmb@index+1}</td>
        <td>{$cmb.NO_UJIAN}</td>
        <td>{$cmb.KODE_VOUCHER}</td>
		<!-- <td>
			{if $cmb.KODE_JURUSAN == '01'}
				IPA
			{else if $cmb.KODE_JURUSAN == '02'}
				IPS
			{else if $cmb.KODE_JURUSAN == '03'}
				IPC
			{/if}
		</td> -->
        <td>{ucwords(strtolower($cmb.NM_C_MHS))}
            {if $cmb.NM_PRODI_MINAT != ''}
                <br/><span style="font-size: 10px">Prodi Minat: {$cmb.NM_PRODI_MINAT}</span>
            {/if}
        </td>
        {* {if $is_jalur_pasca} *}{* ALIH-JENIS / PASCA / PROFESI / SPESIALIS *}
        <!-- <td>{$cmb.GELAR}</td> -->
        {* {/if} *}
        <td>{$cmb.NOMOR_HP}</td>
        {* {if $is_jalur_pasca} *}
            <!-- <td style="font-size: 12px">{if $penerimaan.ID_JALUR == 6}S1{else if $penerimaan.ID_JALUR == 4}D3{else}S1{/if} : {$cmb.PTN_S1}
                <br/><span style="font-size: 10px">Prodi: {$cmb.PRODI_S1}</span>
                {if $cmb.ID_JENJANG == 3 or $cmb.ID_JENJANG == 10}
                <br/>S2 : {$cmb.PTN_S2}
                <br/><span style="font-size: 10px">Prodi: {$cmb.PRODI_S2}</span>    
                {/if}
            </td> -->
        {* {else} *}
            <td>{$cmb.NM_SEKOLAH_ASAL}</td>
        {* {/if} *}
    </tr>
    {foreachelse}
    {* {if $is_jalur_pasca} *}{* ALIH-JENIS / PASCA / PROFESI / SPESIALIS *}
    <!-- <tr>
        <td colspan="8" class="center">Tidak ada data</td>
    </tr> -->
    {* {else} *}
    <tr>
        <td colspan="7" class="center">Tidak ada data</td>
    </tr>    
    {* {/if} *}
    {/foreach}
</table>
<a href="report-registrasi.php">Kembali</a><br/><br/>
{/foreach}