<div class="center_title_bar">{$penerimaan.NAMA_PENERIMAAN}</div>

{* REGULER *}
{if in_array($penerimaan.ID_JENJANG, array(1, 5))}
<table>
	<thead>
		<tr>
			<th>No</th>
			<th>Fakultas</th>
			<th>Program Studi</th>
			<th>P 1</th>
			<th>P 2</th>
			<th>P 3</th>
			<th>P 4</th>
			<th>Ver</th>
			<th>Byr</th>
		</tr>
	</thead>
	<tbody>
		{$prodi_ilmu_hukum_exist = false}
		{$total_pilihan_1 = 0}
		{$total_pilihan_2 = 0}
		{$total_pilihan_3 = 0}
		{$total_pilihan_4 = 0}
		{$total_verifikasi = 0}
		{$total_bayar = 0}
		{foreach $penerimaan_set as $p}
			<tr {if $p@index is div by 2}class="row1"{/if}>
				<td class="center">{$p@index+1}</td>
				<td>{$p.NM_FAKULTAS}</td>
				<td>{$p.NM_PROGRAM_STUDI}</td>
				<td class="center">{$p.JUMLAH_PILIHAN_1}</td>
				<td class="center">{$p.JUMLAH_PILIHAN_2}</td>
				<td class="center">{$p.JUMLAH_PILIHAN_3}</td>
				<td class="center">{$p.JUMLAH_PILIHAN_4}</td>
				<td class="center">{$p.JUMLAH_VERIFIKASI}</td>
				<td class="center">{$p.JUMLAH_BAYAR}</td>
			</tr>
			{$total_registrasi = $total_registrasi + $p.JUMLAH_REGISTRASI}
			{$total_pilihan_1 = $total_pilihan_1 + $p.JUMLAH_PILIHAN_1}
			{$total_pilihan_2 = $total_pilihan_2 + $p.JUMLAH_PILIHAN_2}
			{$total_pilihan_3 = $total_pilihan_3 + $p.JUMLAH_PILIHAN_3}
			{$total_pilihan_4 = $total_pilihan_4 + $p.JUMLAH_PILIHAN_4}
			{$total_verifikasi = $total_verifikasi + $p.JUMLAH_VERIFIKASI}
			{$total_bayar = $total_bayar + $p.JUMLAH_BAYAR}
			{if $p.ID_PROGRAM_STUDI == 55}{$prodi_ilmu_hukum_exist = true}{/if}
		{/foreach}
		<tr>
			<td colspan="3">Jumlah</td>
			<td class="center">{$total_pilihan_1}</td>
			<td class="center">{$total_pilihan_2}</td>
			<td class="center">{$total_pilihan_3}</td>
			<td class="center">{$total_pilihan_4}</td>
			<td class="center">{$total_verifikasi}</td>
			<td class="center">{$total_bayar}</td>
		</tr>
	</tbody>
</table>	
<table>
	<thead>
		<tr>
			<th>Keterangan</th>
			<th>IPA</th>
			<th>IPS</th>
			<th>IPC</th>
			<th>Total</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Antrian Verifikasi</td>
			<td class="center">{$voucher_set[0]['IPA']}</td>
			<td class="center">{$voucher_set[0]['IPS']}</td>
			<td class="center">{$voucher_set[0]['IPC']}</td>
			<td class="center"><strong>{$voucher_set[0]['IPA']+$voucher_set[0]['IPS']+$voucher_set[0]['IPC']}</strong></td>
		</tr>
		<tr>
			<td>Verifikasi</td>
			<td class="center">{$voucher_set[1]['IPA']}</td>
			<td class="center">{$voucher_set[1]['IPS']}</td>
			<td class="center">{$voucher_set[1]['IPC']}</td>
			<td class="center"><strong>{$voucher_set[1]['IPA']+$voucher_set[1]['IPS']+$voucher_set[1]['IPC']}</strong></td>
		</tr>
		<tr>
			<td>Bayar</td>
			<td class="center">{$voucher_set[2]['IPA']}</td>
			<td class="center">{$voucher_set[2]['IPS']}</td>
			<td class="center">{$voucher_set[2]['IPC']}</td>
			<td class="center"><strong>{$voucher_set[2]['IPA']+$voucher_set[2]['IPS']+$voucher_set[2]['IPC']}</strong></td>
		</tr>
		<tr>
			<td>Plot</td>
			<td class="center">{$voucher_set[3]['IPA']}</td>
			<td class="center">{$voucher_set[3]['IPS']}</td>
			<td class="center">{$voucher_set[3]['IPC']}</td>
			<td class="center"><strong>{$voucher_set[3]['IPA']+$voucher_set[3]['IPS']+$voucher_set[3]['IPC']}</strong></td>
		</tr>
	</tbody>
</table>
{/if}

{* PASCA SARJANA *}
{if in_array($penerimaan.ID_JENJANG, array(2,3,9,10))}
	
	<table>
		<thead>
			<tr>
				<th>No</th>
				<th>Fakultas</th>
				<th>Program Studi</th>
				<th title="Submit form">Submit<br/>Form</th>
				<th title="Antri verifikasi">Antri<br/>Verifikasi</th>
				<th>Verifikasi<br/>Kembali</th>
				<th title="Verifikasi">Verifikasi</th>
				<th title="Sudah bayar">Bayar</th>
			</tr>
		</thead>
		<tbody>
			{$total_submit_form = 0}
			{$total_v_antri = 0}
			{$total_v_kembali = 0}
			{$total_verifikasi = 0}
			{$total_bayar = 0}
			{foreach $penerimaan_set as $p}
				<tr {if $p@index is div by 2}class="row1"{/if}>
					<td class="center">{$p@index+1}</td>
					<td>{$p.NM_FAKULTAS}</td>
					<td>{$p.NM_PROGRAM_STUDI}</td>
					<td class="center">{$p.JUMLAH_SUBMIT_FORM}</td>
					<td class="center">{$p.JUMLAH_ANTRI_VERIFIKASI}</td>
					<td class="center">{$p.JUMLAH_VERIFIKASI_KEMBALI}</td>
					<td class="center">{$p.JUMLAH_VERIFIKASI}</td>
					<td class="center">{$p.JUMLAH_BAYAR}</td>
				</tr>
				{$total_submit_form = $total_submit_form + $p.JUMLAH_SUBMIT_FORM}
				{$total_v_antri = $total_v_antri + $p.JUMLAH_ANTRI_VERIFIKASI}
				{$total_v_kembali = $total_v_kembali + $p.JUMLAH_VERIFIKASI_KEMBALI}
				{$total_verifikasi = $total_verifikasi + $p.JUMLAH_VERIFIKASI}
				{$total_bayar = $total_bayar + $p.JUMLAH_BAYAR}
				{if $p.ID_PROGRAM_STUDI == 55}{$prodi_ilmu_hukum_exist = true}{/if}
			{/foreach}
			<tr>
				<td colspan="3">Jumlah</td>
				<td class="center">{$total_submit_form}</td>
				<td class="center">{$total_v_antri}</td>
				<td class="center">{$total_v_kembali}</td>
				<td class="center">{$total_verifikasi}</td>
				<td class="center">{$total_bayar}</td>
			</tr>
		</tbody>
	</table>
	
	{foreach $penerimaan_set as $p}
		{if $p.prodi_minat_set}
		<table>
			<tr>
				<th>Minat {$p.NM_PROGRAM_STUDI}</th>
				<th title="Submit form">Submit<br/>Form</th>
				<th title="Antri verifikasi">Antri<br/>Verifikasi</th>
				<th>Verifikasi<br/>Kembali</th>
				<th title="Verifikasi">Verifikasi</th>
				<th title="Sudah bayar">Bayar</th>
			</tr>
			{foreach $p.prodi_minat_set as $pm}
			<tr>
				<td>{$pm.NM_PRODI_MINAT}</td>
				<td class="center">{$pm.JUMLAH_SUBMIT_FORM}</td>
				<td class="center">{$pm.JUMLAH_ANTRI_VERIFIKASI}</td>
				<td class="center">{$pm.JUMLAH_VERIFIKASI_KEMBALI}</td>
				<td class="center">{$pm.JUMLAH_VERIFIKASI}</td>
				<td class="center">{$pm.JUMLAH_BAYAR}</td>
			</tr>
			{/foreach}
		</table>
		{/if}
	{/foreach}
{/if}

<a href="report-registrasi.php">Kembali</a>