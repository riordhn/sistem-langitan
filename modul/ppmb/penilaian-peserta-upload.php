<?php
include('config.php');

if ($request_method == 'POST')
{
    if ($_FILES["excel"]["error"] > 0)
    {
        $result = "Error: " . $_FILES["excel"]["error"] . "<br />";
    }
    else
    {
        // lokasi file upload
        $upload_dir = "/var/www/html/files/upload/ppmb/penilaian/";
        $destination = $upload_dir . str_replace(".xls", "_" . date('YmdHis') . ".xls", strtolower(str_replace(" ","_", $_FILES["excel"]["name"])));
        
        // pindah lokasi file
        move_uploaded_file($_FILES["excel"]["tmp_name"], $destination);

        // info file
        $result = "Upload: " . $_FILES["excel"]["name"] . "<br />";
        $result .= "Type: " . $_FILES["excel"]["type"] . "<br />";
        $result .= "Size: " . ($_FILES["excel"]["size"] / 1024) . " Kb<br />";
        $result .= "Stored in: " . $_FILES["excel"]["tmp_name"];
        
        // excel reader
        $objReader = new PHPExcel_Reader_Excel5();
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($destination);

        // sheet reader
        $sheet = &$objPHPExcel->getActiveSheet();
        
        // mengambil row aktif
        for ($i = 2; $i <= $sheet->getHighestRow(); $i++)
        {
            // mengambil data
            $no_ujian = $sheet->getCell("A{$i}")->getValue();
            $nilai_ilmu = str_replace(",",".",$sheet->getCell("C{$i}")->getValue());
            $nilai_wawancara = str_replace(",",".",$sheet->getCell("D{$i}")->getValue());
            $nilai_ipk = str_replace(",",".",$sheet->getCell("E{$i}")->getValue());
            $nilai_karya_ilmiah = str_replace(",",".",$sheet->getCell("F{$i}")->getValue());
            $nilai_rekomendasi = str_replace(",",".",$sheet->getCell("G{$i}")->getValue());
            $nilai_matrikulasi = str_replace(",",".",$sheet->getCell("H{$i}")->getValue());
            $nilai_psiko = str_replace(",",".",$sheet->getCell("I{$i}")->getValue());
            $nilai_tulis = str_replace(",",".",$sheet->getCell("J{$i}")->getValue());
            $nilai_lain = str_replace(",",".",$sheet->getCell("K{$i}")->getValue());
            
            // total
            $nilai_total = str_replace(",",".",$nilai_ilmu + $nilai_wawancara + $nilai_ipk + $nilai_karya_ilmiah + $nilai_rekomendasi + $nilai_matrikulasi + $nilai_psiko + $nilai_tulis + $nilai_lain);
            
            //echo "{$nilai_tulis};{$nilai_lain};<br/>";
            /**
            echo "update nilai_cmhs_pasca set
                    nilai_ilmu = {$nilai_ilmu},
                    nilai_wawancara = {$nilai_wawancara},
                    nilai_ipk = {$nilai_ipk},
                    nilai_karya_ilmiah = {$nilai_karya_ilmiah},
                    nilai_rekomendasi = {$nilai_rekomendasi},
                    nilai_matrikulasi = {$nilai_matrikulasi},
                    total_nilai = (nilai_tpa + nilai_inggris + {$nilai_total})
                where id_c_mhs = (select id_c_mhs from calon_mahasiswa_baru where no_ujian = '{$no_ujian}')<br/>";
            **/
            if (1 == 1)  // set to 1 == 2 to debug
            {
                $result = $db->Query("
                    update nilai_cmhs_pasca set
                        nilai_ilmu = {$nilai_ilmu},
                        nilai_wawancara = {$nilai_wawancara},
                        nilai_ipk = {$nilai_ipk},
                        nilai_karya_ilmiah = {$nilai_karya_ilmiah},
                        nilai_rekomendasi = {$nilai_rekomendasi},
                        nilai_matrikulasi = {$nilai_matrikulasi},
                        nilai_psiko = {$nilai_psiko},
                        nilai_tulis = {$nilai_tulis},
                        nilai_lain = {$nilai_lain},
                        total_nilai = (nilai_tpa + nilai_inggris + {$nilai_total})
                    where id_c_mhs = (select id_c_mhs from calon_mahasiswa_baru where no_ujian = '{$no_ujian}')");
            }
            //echo $db->affected_rows . "<br/>";
        }
    }
}

?>
<html>
    <body style="padding: 0px; margin: 0px;">
        <?php if ($request_method == 'GET') { ?>
        <form action="penilaian-peserta-upload.php" method="post" enctype="multipart/form-data">
            <table border="1px">
                <tr>
                    <td>
                    <label style="font-family: 'Trebuchet MS'; font-size: 13px;">Input File Excel</label> :
                    <input type="file" name="excel" />
                    </td>
                    <td><input type="submit" value="Upload" /></td>
                </tr>
            </table>
        </form>
        <?php } else if ($request_method == 'POST') { ?>
        <p style="font-family: 'Trebuchet MS'; font-size: 13px;">Nilai berhasil di upload</p>
        <a href="penilaian-peserta-upload.php" style="font-family: 'Trebuchet MS'; font-size: 13px;">Kembali</a>
        <?php } ?>
    </body>
</html>