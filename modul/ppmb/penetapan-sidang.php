<?php
include 'config.php';
include 'class/snmptn.class.php';
include 'class/Penetapan.class.php';
include 'class/CMB.class.php';
include '../registrasi/class/CalonMahasiswa.class.php';

// Cek ID pak bambang ketua PPMB saat ini
/*if (!in_array($user->ID_PENGGUNA, array(24350, 2))) {
	echo "Halaman di tutup untuk sementara. ({$user->ID_PENGGUNA})"; exit();
}*/

$Snmptn         = new SnmptnClass($db);
$Penetapan      = new Penetapan($db);
$CalonMahasiswa = new CalonMahasiswa($db);
$CMB			= new CMB($db);

$mode = get('mode', 'view');


if ($request_method == 'POST')
{
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$tahun = empty($_GET['tahun']) ? date('Y') : $_GET['tahun'];

		$smarty->assign('penetapan_set', $db->QueryToArray("select * from penetapan where to_char(tgl_penetapan, 'YYYY') = '{$tahun}' and id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI} order by periode desc"));
		$smarty->assign('tahun_set', $db->QueryToArray("select min(tahun)-1 as tahun_awal, max(tahun)+1 as tahun_akhir from penerimaan"));
	}
	
	if ($mode == 'sidang')
	{
		$id_penetapan = get('id_penetapan');
		
		$smarty->assign('penetapan', $Penetapan->GetPenetapan($id_penetapan));
		$smarty->assign('penerimaan_set', $Penetapan->GetListPenerimaanForPenetapan($id_penetapan));
	}
	
	if ($mode == 'detail')
	{
		$id_penetapan_penerimaan = get('id_penetapan_penerimaan');
		$id_program_studi = get('id_program_studi');
		$pilihan = get('pilihan', 1);
		
		// tgl sekarang
		$smarty->assign('tgl_sekarang', strtoupper(date('d-M-y')));
		
		// nama penetapan
		$penetapan_penerimaan = $Penetapan->GetPenetapanPenerimaan($id_penetapan_penerimaan);
		$id_penerimaan = $penetapan_penerimaan['ID_PENERIMAAN'];
		$smarty->assign('penetapan', $penetapan_penerimaan);
		
		$penerimaan = $Penetapan->GetPenerimaan($id_penerimaan);
		$smarty->assign('penerimaan', $penerimaan);
		
		// nama program studi
		$program_studi = $Penetapan->GetProgramStudi($id_program_studi);
		$smarty->assign('program_studi', $program_studi);
		$smarty->assign('pilihan', $pilihan);

		// Untuk Jenjang S1 / D3
		if (in_array($penerimaan['ID_JENJANG'], array(1, 4, 5)))
		{

			$mode = 'detail-mandiri';
				
			$cmb_set = $Penetapan->GetListCalonMahasiswaMandiri($id_program_studi, $id_penerimaan, $pilihan);
			
			$smarty->assignByRef('cmb_set', $cmb_set);
			
			// data statistik
			$statistik = $Penetapan->GetStatistik($id_penerimaan, $id_program_studi);
			$smarty->assign('statistik', $statistik);

			/**
			// S1-SNMPTN
			if ($penerimaan['ID_JALUR'] == 1) 
			{
				$mode = 'detail-s1-snmptn';
				
				$smarty->assign('cmb_set', $Snmptn->GetListPesertaPutaran2($program_studi['ID_PROGRAM_STUDI']));
			}
			
			// S1-Mandiri, D3, internasional
			if (in_array($penerimaan['ID_JALUR'], array(3, 5, 27))) 
			{
				$mode = 'detail-mandiri';
				
				$cmb_set = $Penetapan->GetListCalonMahasiswaMandiri($id_program_studi, $id_penerimaan, $pilihan);
				
				$smarty->assignByRef('cmb_set', $cmb_set);
				
				// data statistik
				$statistik = $Penetapan->GetStatistik($id_penerimaan, $id_program_studi);
				$smarty->assign('statistik', $statistik);
			}

			// S1-Alih Jenis
			if ($penerimaan['ID_JALUR'] == 4)
			{
				$mode = 'detail-pasca';

				// detail mahasiswa
				$calon_mahasiswa_baru_set = $Penetapan->GetListCalonMahasiswaPasca($id_program_studi, $id_penerimaan);
				$smarty->assign('calon_mahasiswa_baru_set', $calon_mahasiswa_baru_set);

				// data statistik
				$statistik = $Penetapan->GetStatistik($id_penerimaan, $id_program_studi);
				$smarty->assign('statistik', $statistik);
			}

			// S1-PBSB
			if ($penerimaan['ID_JALUR'] == 20)
			{
				$mode = 'detail-mandiri';

				// detail mahasiswa
				$cmb_set = $Penetapan->GetListCalonMahasiswaPBSB($id_program_studi, $id_penerimaan, $pilihan);
				$smarty->assign('cmb_set', $cmb_set);

				// data statistik
				$statistik = $Penetapan->GetStatistik($id_penerimaan, $id_program_studi);
				$smarty->assign('statistik', $statistik);
			}
			*/
		}
		
		/**
		// D4 Alih Jenis
		if ($program_studi['ID_JENJANG'] == 4)
		{
			// Khusus untuk D4 tahun 2014 saja
			$mode = 'detail-pasca-d4-2014';

			// detail mahasiswa
			$calon_mahasiswa_baru_set = $Penetapan->GetListCalonMahasiswaPasca($id_program_studi, $id_penerimaan);
			$smarty->assign('calon_mahasiswa_baru_set', $calon_mahasiswa_baru_set);

			// data statistik
			$statistik = $Penetapan->GetStatistik($id_penerimaan, $id_program_studi);
			$smarty->assign('statistik', $statistik);
		}

		// S2 / S3 / Pr / Sp
		if (in_array($program_studi['ID_JENJANG'], array(2, 3, 9, 10)))  
		{
			$mode = 'detail-pasca';
			
			// detail mahasiswa
			$calon_mahasiswa_baru_set = $Penetapan->GetListCalonMahasiswaPasca($id_program_studi, $id_penerimaan);
			$smarty->assign('calon_mahasiswa_baru_set', $calon_mahasiswa_baru_set);

			// data statistik
			$statistik = $Penetapan->GetStatistik($id_penerimaan, $id_program_studi);
			$smarty->assign('statistik', $statistik);
		}
		*/
	}
	
	if ($mode == 'peserta')
	{
		$id_c_mhs = get('id_c_mhs');
		
		$cmb				= $CalonMahasiswa->GetData($id_c_mhs);
		$syarat_prodi_set	= $CMB->GetListSyaratProdi($id_c_mhs);
				
		$id_jenjang = $cmb['ID_JENJANG'];
		$tahun = $cmb['TAHUN_PENERIMAAN'];
		$gelombang = $cmb['GELOMBANG_PENERIMAAN'];
		$semester = $cmb['SEMESTER_PENERIMAAN'];
		
		// Jenjang S1, D4, D3
		if (in_array($cmb['ID_JENJANG'], array(1, 4, 5)))
		{
			$folder = "ujian-mandiri";
			$mode = 'peserta-mandiri';
		}
		
		// Jenjang s2, s3, profesi, spesialis
		if (in_array($cmb['ID_JENJANG'], array(2, 3, 9, 10)))
		{
			$folder = "ujian-pasca";
			$mode = 'peserta-pasca';
		}
		
		if (file_exists("../../img/foto/{$folder}/{$cmb['TAHUN_PENERIMAAN']}/{$cmb['SEMESTER_PENERIMAAN']}{$cmb['GELOMBANG_PENERIMAAN']}/{$cmb['NO_UJIAN']}.jpg"))
		{
			$cmb['FILE_FOTO'] = "/img/foto/{$folder}/{$cmb['TAHUN_PENERIMAAN']}/{$cmb['SEMESTER_PENERIMAAN']}{$cmb['GELOMBANG_PENERIMAAN']}/{$cmb['NO_UJIAN']}.jpg";
		}
		else if (file_exists("../../img/foto/{$folder}/{$cmb['TAHUN_PENERIMAAN']}/{$cmb['SEMESTER_PENERIMAAN']}{$cmb['GELOMBANG_PENERIMAAN']}/{$cmb['NO_UJIAN']}.JPG"))
		{
			$cmb['FILE_FOTO'] = "/img/foto/{$folder}/{$cmb['TAHUN_PENERIMAAN']}/{$cmb['SEMESTER_PENERIMAAN']}{$cmb['GELOMBANG_PENERIMAAN']}/{$cmb['NO_UJIAN']}.JPG";
		}
		else
		{
			if ($cmb['JENIS_KELAMIN'] == 1)
				$cmb['FILE_FOTO'] = "/img/foto/no-image.png";
			else
				$cmb['FILE_FOTO'] = "/img/foto/no-image-female.jpg";
		}
		
		//$mode = 'peserta-s1';
		//$smarty->assign('cmb', $Snmptn->GetDetailPeserta($id_c_mhs));
		
		$smarty->assignByRef('syarat_prodi_set', $syarat_prodi_set);
		$smarty->assignByRef('cmb', $cmb);
	}
}

$smarty->assign('ID_PENGGUNA', $user->ID_PENGGUNA);
$smarty->display("penetapan/sidang/{$mode}.tpl");

?>
