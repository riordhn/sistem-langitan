<?php
include 'config.php';
include 'class/Penerimaan.class.php';

$mode = get('mode', 'view');
$Penerimaan = new Penerimaan($db);

if ($request_method == 'POST')
{
    if (post('mode') == 'edit')
    {   
        $db->BeginTransaction();
        
        $db->Query("
            insert into log_cmb_nama (id_c_mhs, nm_c_mhs, gelar, id_editor)
            values ({$_POST['id_c_mhs']}, '{$_POST['nm_c_mhs']}', '{$_POST['gelar']}', {$user->ID_PENGGUNA})");
            
        $db->Query("
            update calon_mahasiswa_baru set nm_c_mhs = upper('{$_POST['nm_c_mhs']}'), gelar = '{$_POST['gelar']}' 
            where id_c_mhs = {$_POST['id_c_mhs']}");
        
        $result = $db->Commit();
        
        echo $result;
        
        exit();
    }
}

if ($request_method == 'GET')
{
    if ($mode == 'view')
    {
        $id_penerimaan = get('id_penerimaan', '');
        $smarty->assign('penerimaan_set', $Penerimaan->GetAllForView());
        
        if ($id_penerimaan != '')
        {
            $smarty->assign('cmb_set', $db->QueryToArray("
                select id_c_mhs, no_ujian, nm_c_mhs, gelar,
					to_char(tgl_bayar, 'DD/MM/YYYY') as tgl_bayar
				from calon_mahasiswa_baru cmb
				join voucher v on v.kode_voucher = cmb.kode_voucher
                where
					cmb.id_penerimaan = {$id_penerimaan} and 
					v.tgl_bayar is not null
                order by no_ujian"));
        }
    }
}

$smarty->display("peserta/nama/{$mode}.tpl");
?>