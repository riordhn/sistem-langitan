<?php
include('config.php');
include('class/Penerimaan.class.php');

$mode = get('mode', 'view');

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	if (post('mode') == 'add')
	{
		$id_penerimaan		= post('id_penerimaan');
		$id_pengguna		= post('id_pengguna');
		$id_jabatan_panitia	= post('id_jabatan_panitia');

		$db->Query("INSERT INTO PENERIMAAN_PANITIA (ID_PENERIMAAN, ID_PENGGUNA, ID_JABATAN_PANITIA) VALUES ({$id_penerimaan}, {$id_pengguna}, {$id_jabatan_panitia})");
	}
}

if ($_SERVER['REQUEST_METHOD'] == 'GET' || $_SERVER['REQUEST_METHOD'] == 'POST')
{
	$penerimaan = new Penerimaan($db);
	$smarty->assign('penerimaan_set', $penerimaan->GetAllForView());

	if ($mode == 'view')
	{
		$id_penerimaan = get('id_penerimaan', '');

		if ($id_penerimaan != '')
		{
			$panitia_set = $db->QueryToArray(
				"SELECT PP.ID_PENERIMAAN_PANITIA, P.USERNAME, P.NM_PENGGUNA, JP.NM_JABATAN_PANITIA
				FROM PENERIMAAN_PANITIA PP
				INNER JOIN PENGGUNA P ON P.ID_PENGGUNA = PP.ID_PENGGUNA
				INNER JOIN PPMB_JABATAN_PANITIA JP ON JP.ID_JABATAN_PANITIA = PP.ID_JABATAN_PANITIA
				WHERE PP.ID_PENERIMAAN = '{$id_penerimaan}'
				ORDER BY JP.ID_JABATAN_PANITIA, P.NM_PENGGUNA");
			$smarty->assignByRef('panitia_set', $panitia_set);
		}
	}

	if ($mode == 'add')
	{
		$id_penerimaan = get('id_penerimaan', '');

		$db->Query("SELECT * FROM PENERIMAAN WHERE ID_PENERIMAAN = {$id_penerimaan}");
		$smarty->assignByRef('penerimaan', $db->FetchAssoc());

		$jabatan_panitia_set = $db->QueryToArray("SELECT * FROM PPMB_JABATAN_PANITIA ORDER BY 1");
		$smarty->assignByRef('jabatan_panitia_set', $jabatan_panitia_set);
	}

	if ($mode == 'cari-petugas')
	{
		$nip = get('nip');

		$db->Query("SELECT ID_PENGGUNA, NM_PENGGUNA FROM PENGGUNA WHERE USERNAME LIKE '{$nip}%' AND ID_PERGURUAN_TINGGI = '{$id_pt_user}' AND ROWNUM <= 1");
		echo json_encode($db->FetchAssoc());
		exit();
	}

	if ($mode == 'delete')
	{
		$id_penerimaan 			= get('id_penerimaan');
		$id_penerimaan_panitia	= get('id_penerimaan_panitia');

		$db->Query("DELETE FROM PENERIMAAN_PANITIA WHERE ID_PENERIMAAN_PANITIA = {$id_penerimaan_panitia}");

		// redirect script
		echo '<script>window.location = \'#penerimaan-petugas!penerimaan-petugas.php?id_penerimaan='.$id_penerimaan.'\';</script>';
		exit();
	}
}

$smarty->display("penerimaan/petugas/{$mode}.tpl");