<?php
include 'config.php';
include 'class/Pengawas.class.php';
include 'class/PengawasCetak.class.php';

$Pengawas = new Pengawas($db);
$PengawasCetak = new PengawasCetak($db);

$mode = get('mode', 'view');

function IsChecked($chkname,$value)
{
    if(!empty($_GET[$chkname]))
    {
        foreach($_GET[$chkname] as $chkval)
        {
            if($chkval == $value) { return true; }
        }
    }
    
    return false;
}

if (IsChecked('bulan', 'April')) { echo "april dicentang"; }

if ($request_method == 'GET')
{
    if ($mode == 'view')
    {
        $smarty->assign('ujian_set', $Pengawas->GetListUjian());
    }
    
    if ($mode == 'excel')
    {
        switch (get('cetak'))
        {             
            case '1': $PengawasCetak->MasterPlot(get('id_ujian')); break;
            case '2': $PengawasCetak->DaftarHonorarium(get('id_ujian')); break;
            case '3': $PengawasCetak->DaftarPengawasPerAsal(get('id_ujian')); break;
            case '4': $PengawasCetak->DaftarPengawasMailMerge(get('id_ujian')); break;
            case '5': $PengawasCetak->RayonisasiPengawas(get('id_ujian')); break;
        }
    }
}

$smarty->display("pengawas/cetak/{$mode}.tpl");
?>
