<?php

include 'config.php';
include 'class/convert.class.php';

if (!in_array($user->ID_JABATAN_PEGAWAI, array(175, 176, 203)))
{
	// 203	= Admin PPMB
	// 202	= Verifikator PPMB
	// 		= Ketua PPMB
	
	echo "<div class=\"center_title_bar\">--</div>";
	echo "<h2>Mohon maaf, anda tidak mempunyai akses kesini</h2>";
	exit();
}

if ($request_method == 'GET')
{
    if (get('mode') == 'generate-pasca')
    {
        $convert = new convert_data($db);
        $result = "";
        foreach ($convert->load_calon_mahasiswa_pasca() as $data)
        {
            //echo $data['ID_PILIHAN_1'].'-'.$convert->get_prodi($data['ID_PILIHAN_1'],'').'<br/>';
            $result .= $convert->insert_data(
                    $data['NO_UJIAN'],
                    $data['KODE_JURUSAN'],
                    $data['TGL_REGISTRASI'],
                    $data['GELOMBANG'],
                    substr(str_replace("'", "''", strtoupper($data['NM_C_MHS'])), 0, 50),
                    $data['GELAR'],
                    substr(str_replace("'", "''", strtoupper($data['ALAMAT'])), 0, 100),
                    $data['TELP'],
                    $data['TGL_LAHIR'],
                    $data['JENIS_KELAMIN'],
                    $data['KEWARGANEGARAAN'],
                    $data['ID_AGAMA'],
                    $data['ID_KOTA_LAHIR'], '', '', '', '', '', '', '', '', '', '',
                    $data['ID_JALUR'], $data['ID_PILIHAN_1'], $data['ID_PILIHAN_2'], $data['ID_PILIHAN_3'], $data['ID_PILIHAN_4'],
                    $data['NM_JENJANG'], $data['PTN_S1'], $data['PRODI_S1'], $data['IP_S1'], $data['PTN_S2'], $data['PRODI_S2'], $data['IP_S2']);
        }
        
        $smarty->assign('result_query', $result);
        $smarty->display('peserta/transfer/sql-pasca.tpl');
        exit();
    }
}

/*
if (isset($_GET['jenjang']))
{
    $index = 0;
    if ($_GET['jenjang'] == 'pasca')
    {
        foreach ($convert->load_calon_mahasiswa_pasca() as $data)
        {
            //echo $data['ID_PILIHAN_1'].'-'.$convert->get_prodi($data['ID_PILIHAN_1'],'').'<br/>';
            $convert->insert_data($data['NO_UJIAN'], $data['KODE_JURUSAN'], $data['TGL_REGISTRASI'], $data['GELOMBANG'], str_replace("'", "''", strtoupper($data['NM_C_MHS'])), $data['GELAR'], str_replace("'", "''", strtoupper($data['ALAMAT'])), $data['TELP'], $data['TGL_LAHIR'], $data['JENIS_KELAMIN'], $data['KEWARGANEGARAAN'], $data['ID_AGAMA'], $data['ID_KOTA_LAHIR'], '', '', '', '', '', '', '', '', '', '', $data['ID_JALUR'], $data['ID_PILIHAN_1'], $data['ID_PILIHAN_2'], $data['ID_PILIHAN_3'], $data['ID_PILIHAN_4'], $data['NM_JENJANG'], $data['PTN_S1'], $data['PRODI_S1'], $data['IP_S1'], $data['PTN_S2'], $data['PRODI_S2'], $data['IP_S2']);
        }
    }
    else
    {
        foreach ($convert->load_calon_mahasiswa_alih() as $data)
        {
            $temp = $convert->insert_data($data['NO_UJIAN'], $data['KODE_JURUSAN'], $data['TGL_AKTIF_PMDK'], $data['GELOMBANG_PMDK'], str_replace("'", "''", strtoupper($data['NM_C_MHS'])), str_replace("'", "''", strtoupper($data['ALAMAT'])), $data['TELP'], $data['TGL_LAHIR'], $data['JENIS_KELAMIN'], $data['KEWARGANEGARAAN'], $data['ID_AGAMA'], $data['ID_KOTA_LAHIR'], $data['ID_SEKOLAH_ASAL'], str_replace("'", "''", strtoupper($data['NAMA_AYAH'])), str_replace("'", "''", strtoupper($data['ALAMAT_AYAH'])), $data['PENDIDIKAN_AYAH'], $data['PEKERJAAN_AYAH'], $data['PENDIDIKAN_IBU'], $data['PEKERJAAN_IBU'], $data['SUMBER_BIAYA'], $data['JUMLAH_ADIK'], $data['JUMLAH_KAKAK'], $data['ID_JALUR'], $data['ID_PILIHAN_1'], $data['ID_PILIHAN_2'], $data['ID_PILIHAN_3'], $data['ID_PILIHAN_4']);
            if ($temp)
            {
                $temp;
                $convert->update_status_transfer_id($data['ID_C_MHS']);
                $index++;
            }
        }
        echo '<script> alert("' . $index . ' Data Telah Di Transfer")</script>';
    }
}
**/

$penerimaan_set = $db->QueryToArray("
    select p.tahun, p.nm_penerimaan, p.gelombang, p.semester, j.nm_jenjang, j2.nm_jalur,
        (select count(*) from calon_mahasiswa_baru cmb where cmb.id_penerimaan = p.id_penerimaan and tgl_verifikasi_ppmb is not null) as jumlah_verifikasi
    from penerimaan p
    join jenjang j on j.id_jenjang = p.id_jenjang
    join jalur j2 on j2.id_jalur = p.id_jalur
    where p.is_aktif = 1 order by p.tahun, p.semester, p.gelombang, p.id_jalur, p.id_jenjang");
$smarty->assign('penerimaan_set', $penerimaan_set);


//$smarty->assign('jumlah_pasca_tidak_tertransfer', count($convert->load_calon_mahasiswa_pasca()));
//$smarty->assign('jumlah_alih_tidak_tertransfer', count($convert->load_calon_mahasiswa_alih()));
$smarty->display('peserta/transfer/convert-data.tpl');
?>
