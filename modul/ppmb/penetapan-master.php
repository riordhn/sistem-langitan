<?php
include('config.php');
include('class/Penerimaan.class.php');

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    if (post('mode') == 'add')
    {
        $db->Parse("
            insert into penetapan
                (nm_penetapan, no_surat, baris_1, baris_2, baris_3, baris_4, tgl_penetapan, periode, id_perguruan_tinggi) values
                (:nm_penetapan, :no_surat, :baris_1, :baris_2, :baris_3, :baris_4, :tgl_penetapan, :periode, :id_perguruan_tinggi)");
        $db->BindByName(':nm_penetapan', post('nm_penetapan'));
        $db->BindByName(':no_surat', post('no_surat'));
        $db->BindByName(':baris_1', post('baris_1'));
        $db->BindByName(':baris_2', post('baris_2'));
        $db->BindByName(':baris_3', post('baris_3'));
        $db->BindByName(':baris_4', post('baris_4'));
        $db->BindByName(':tgl_penetapan', date('d-M-Y', mktime(0, 0, 0, post('tgl_penetapan_Month'), post('tgl_penetapan_Day'), post('tgl_penetapan_Year'))));
        $db->BindByName(':periode', post('periode'));
        $db->BindByName(':id_perguruan_tinggi', $user->ID_PERGURUAN_TINGGI);
        $result = $db->Execute();
        
        if ($result)
            $smarty->assign('added', 'Data berhasil ditambahkan');
        else
            $smarty->assign('added', 'Data gagal ditambahkan');
    }
    
    if (post('mode') == 'edit')
    {
        $id_penetapan = post('id_penetapan');
        
        $db->Parse("
            update penetapan set
                nm_penetapan = :nm_penetapan,
                no_surat = :no_surat,
                baris_1 = :baris_1,
                baris_2 = :baris_2,
                baris_3 = :baris_3,
                baris_4 = :baris_4,
                tgl_penetapan = :tgl_penetapan,
                periode = :periode,
                row_page_1 = :row_page_1,
                row_page_n = :row_page_n,
                width_nama = :width_nama,
                width_prodi = :width_prodi
            where id_penetapan = {$id_penetapan}");
        $db->BindByName(':nm_penetapan', post('nm_penetapan'));
        $db->BindByName(':no_surat', post('no_surat'));
        $db->BindByName(':baris_1', post('baris_1'));
        $db->BindByName(':baris_2', post('baris_2'));
        $db->BindByName(':baris_3', post('baris_3'));
        $db->BindByName(':baris_4', post('baris_4'));
        $db->BindByName(':tgl_penetapan', date('d-M-Y', mktime(0, 0, 0, post('tgl_penetapan_Month'), post('tgl_penetapan_Day'), post('tgl_penetapan_Year'))));
        $db->BindByName(':periode', post('periode'));
        $db->BindByName(':row_page_1', post('row_page_1'));
        $db->BindByName(':row_page_n', post('row_page_n'));
        $db->BindByName(':width_nama', post('width_nama'));
        $db->BindByName(':width_prodi', post('width_prodi'));
        $result = $db->Execute();
        
        if ($result)
            $smarty->assign('edited', 'Data berhasil disimpan');
        else
            $smarty->assign('edited', 'Data gagal disimpan');
    }
    
    if (post('mode') == 'delete')
    {
        $id_penetapan = post('id_penetapan');
        
        $result = $db->Query("delete from penetapan where id_penetapan = {$id_penetapan}");
        
        if ($result)
            $smarty->assign('deleted', 'Data berhasil dihapus');
        else
            $smarty->assign('deleted', 'Data gagal dihapus');
    }
    
    if (post('mode') == 'add-penerimaan')
    {
        $id_penetapan = post('id_penetapan');
        $id_penerimaan = post('id_penerimaan');
        
        $result = $db->Query("insert into penetapan_penerimaan (id_penetapan, id_penerimaan) values ({$id_penetapan}, {$id_penerimaan})");
        
        if ($result)
            $smarty->assign('added', 'Data berhasil ditambahkan');
        else
            $smarty->assign('added', 'Data gagal ditambahkan');
    }
    
    if (post('mode') == 'del-penerimaan')
    {
        $id_penetapan_penerimaan = post('id_penetapan_penerimaan');
        $result = $db->Query("delete from penetapan_penerimaan where id_penetapan_penerimaan = {$id_penetapan_penerimaan}");
        
        if ($result)
            $smarty->assign('deleted', 'Data berhasil dihapus');
        else
            $smarty->assign('deleted', 'Data gagal dihapus');
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    
    if ($mode == 'view')
    {
        $smarty->assign('penetapan_set', $db->QueryToArray("select * from penetapan where id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI} order by periode desc"));
    }
    
    if ($mode == 'edit')
    {
        $id_penetapan = get('id_penetapan');
        
        $db->Query("select * from penetapan where id_penetapan = {$id_penetapan}");
        $row = $db->FetchAssoc();
        $smarty->assign('penetapan', $row);
    }
    
    if ($mode == 'delete')
    {
        $id_penetapan = get('id_penetapan');
        
        $db->Query("select id_penetapan, nm_penetapan from penetapan where id_penetapan = {$id_penetapan}");
        $row = $db->FetchAssoc();
        $smarty->assign('penetapan', $row);
        
        $smarty->assign('penetapan_penerimaan_set', $db->QueryToArray("
            select pp.id_penetapan_penerimaan, p.tahun, nm_jenjang, nm_jalur, nm_penerimaan, gelombang, is_aktif, semester from penetapan_penerimaan pp
            join penerimaan p on p.id_penerimaan = pp.id_penerimaan
            join jalur jl on jl.id_jalur = p.id_jalur
            join jenjang j on j.id_jenjang = p.id_jenjang
            where pp.id_penetapan = {$id_penetapan}
            order by p.tahun, p.semester, p.gelombang, p.id_jalur, p.id_jenjang"));
    }
    
    if ($mode == 'view-penerimaan')
    {
        $id_penetapan = get('id_penetapan');
        
        $db->Query("select id_penetapan, nm_penetapan from penetapan where id_penetapan = {$id_penetapan}");
        $row = $db->FetchAssoc();
        $smarty->assign('penetapan', $row);
        
        $smarty->assign('penetapan_penerimaan_set', $db->QueryToArray("
            select pp.id_penetapan_penerimaan, p.tahun, nm_jenjang, nm_jalur, nm_penerimaan, gelombang, p.is_aktif, semester from penetapan_penerimaan pp
            join penerimaan p on p.id_penerimaan = pp.id_penerimaan
            join jalur jl on jl.id_jalur = p.id_jalur
            join jenjang j on j.id_jenjang = p.id_jenjang
            where pp.id_penetapan = {$id_penetapan}
            order by p.tahun, p.semester, p.gelombang, p.id_jalur, p.id_jenjang"));
    }
    
    if ($mode == 'add-penerimaan')
    {
        $id_penetapan = get('id_penetapan');
        
        $db->Query("select id_penetapan, nm_penetapan from penetapan where id_penetapan = {$id_penetapan}");
        $row = $db->FetchAssoc();
        $smarty->assign('penetapan', $row);
            
        $penerimaan = new Penerimaan($db);
        $smarty->assign('penerimaan_set', $penerimaan->GetAllForView());
    }
}

$smarty->display("penetapan/master/{$mode}.tpl");
?>
