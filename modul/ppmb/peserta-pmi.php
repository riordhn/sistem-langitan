<?php
include 'config.php';
include 'class/Penerimaan.class.php';
include 'class/BL.class.php';

$mode = get('mode', 'view');
$Penerimaan = new Penerimaan($db);
$BL = new BL($db);

$id_penerimaan		= get('id_penerimaan', '');
$id_program_studi	= get('id_program_studi', '');

if ($request_method == 'POST')
{
	if (post('mode') == 'add')
	{
		$result = $BL->AddBL($_POST);
		
		$time = time();
		if ( ! $result)
		{
			echo "Data gagal ditambahkan (atau sudah ada). <a href=\"peserta-pmi.php?id_penerimaan={$_POST['id_penerimaan']}&_r={$time}\">Klik untuk kembali</a>";
			exit();
		}
	}
	
	if (post('mode') == 'edit')
	{
		$BL->UpdateBL($_POST);
	}
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        $smarty->assign('penerimaan_set', $Penerimaan->GetAllForView());
		
		if ($id_penerimaan != '')
		{
			$smarty->assign('data_set', $BL->GetListInputBL($id_penerimaan, $id_program_studi));
			$smarty->assign('program_studi_set', $Penerimaan->GetListProdiPenerimaan($id_penerimaan, 1));

			// Ambil data penerimaan
			$penerimaans = $db->QueryToArray("select id_jalur, tahun, semester, gelombang from penerimaan where id_penerimaan = {$id_penerimaan}");

			
			if (in_array($penerimaans[0]['ID_JALUR'], array(3, 4, 5, 20, 27)))   // Mandiri ALL
			{
				$smarty->assign('foto_path', $var = "/img/foto/ujian-mandiri/{$penerimaans[0]['TAHUN']}/{$penerimaans[0]['SEMESTER']}{$penerimaans[0]['GELOMBANG']}");
			}
			else if (in_array($penerimaans[0]['ID_JALUR'], array(1))) // Untuk SNMPTN
			{
				$smarty->assign('foto_path', $var = "/img/foto/ujian-snmptn/{$penerimaans[0]['TAHUN']}");
			}
		}
    }
	
	if ($mode == 'add')
	{
		$smarty->assign('penerimaan', $BL->GetPenerimaan($id_penerimaan));
		$smarty->assign('status_anak_set', $BL->GetListStatusAnak());
		$smarty->assign('jabatan_set', $BL->GetListJabatan());
	}
	
	if ($mode == 'edit')
	{
		$smarty->assign('data', $BL->GetBL(get('no_ujian')));
		$smarty->assign('penerimaan', $BL->GetPenerimaan($id_penerimaan));
		$smarty->assign('status_anak_set', $BL->GetListStatusAnak());
	}
	
	if ($mode == 'delete')
	{
		$BL->DeleteBL(get('no_ujian'), get('id_penerimaan'));
		echo "<script>window.location = '#peserta-pmi!peserta-pmi.php?id_penerimaan={$id_penerimaan}';</script>";
		exit();
	}
}

$smarty->display("peserta/pmi/{$mode}.tpl");
?>
