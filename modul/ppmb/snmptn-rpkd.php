<?php
include('config.php');
include('class/snmptn.class.php');

$mode = get('mode', 'view');
$snmptn = new SnmptnClass($db);

if ($request_method == 'GET')
{
    if ($mode == 'view')
    {
        $smarty->assign('cmb_set', $snmptn->GetListPKD());
    }
}

$smarty->display("snmptn/rpkd/{$mode}.tpl");
?>
