<?php
include("config.php");
include("class/snmptn.class.php");

$mode = get('mode', 'view');

if ($request_method == 'GET')
{
	$snmptn = new SnmptnClass($db);
	
    if ($mode == 'view')
    {
        // program studi pilihan
        $id_program_studi = get('id_program_studi', '');
        $pilihan_ke = get('pilihan_ke', '1');
        
        $smarty->assign('program_studi_set', $db->QueryToArray("
            select id_program_studi, nm_program_studi, nm_jenjang from program_studi ps
            join jenjang j on j.id_jenjang = ps.id_jenjang
            where kode_snmptn is not null order by nm_program_studi"));
        
        if ($id_program_studi != '')
        {
			$smarty->assign('cmb_set', $snmptn->GetListPesertaPutaran1($id_program_studi));
        }
    }
	
	if ($mode == 'download-all')
	{
		// Force download file
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment; filename=pesertaHitam.txt;");
		
		foreach ($snmptn->GetListKuotaOnly() as $row)
		{
			echo "{$row['ID_C_MHS']},{$row['NILAI_TOTAL']},{$row['ID_PILIHAN_1']},{$row['ID_PILIHAN_2']},{$row['ID_SEKOLAH_ASAL']}\r\n";
		}
		
		exit();
	}
}

$smarty->display("snmptn/penetapan2/{$mode}.tpl");
?>
