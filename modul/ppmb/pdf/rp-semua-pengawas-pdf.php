<?php
include '../config.php';
include '../../../tcpdf/config/lang/ind.php';
include '../../../tcpdf/tcpdf.php';

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->SetMargins(15, 2, 15);

$pdf->AddPage();

$html = <<<EOF
<style>
    td {font-family: times;}
    .title { font-size:40px;font-weight:bold; }
    .header { font-size:47px;font-weight:bold; }
    .address { font-size:30px; }
</style>
<table width="100%" border="0">
    <tr>
        <td width="100%" align="center">
            <span class="title">DEPARTEMEN PENDIDIKAN NASIONAL</span><br/>
            <span class="header">UNIVERSITAS AIRLANGGA</span><br/>
            <span class="title">PUSAT PENERIMAAN MAHASISWA BARU (PPMB) 2012</span><br/>
            <span class="address">Sekretariat : Jl. Dharmawangsa 29 Surabaya, Telp. / Fax (031) 5048654</span>
        </td>
    </tr>
</table>
<hr style="height:2px;" />
<table width="100%" border="0">
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td align="left"><span style="font-size:40px;font-weight:bold;">Daftar Ploting Pengawas</span></td>
        <td align="right"></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
</table>
<table width="100%" cellpadding="2" align="center">
    <tr>
        <td style="border-top:1px solid black;border-bottom:1px solid black;" width="5%">No.</td>
        <td style="border-top:1px solid black;border-bottom:1px solid black;" width="15%">Asal</td>
        <td style="border-top:1px solid black;border-bottom:1px solid black;" width="20%">Nama</td>
        <td style="border-top:1px solid black;border-bottom:1px solid black;" width="18%">Lokasi</td>
        <td style="border-top:1px solid black;border-bottom:1px solid black;" width="10%">Ruang</td>
        <td style="border-top:1px solid black;border-bottom:1px solid black;" width="10%">Subruang</td>
        <td style="border-top:1px solid black;border-bottom:1px solid black;" width="10%">Jabatan</td>
        <td style="border-top:1px solid black;border-bottom:1px solid black;" width="12%">Kelompok</td>
    </tr>
    <tr>
        <td>1</td>
        <td align="left">DIR. KEU</td>
        <td align="left">Ichwan Efendi,</td>
        <td align="left">1</td>
        <td>R-301</td>
        <td>1</td>
        <td>1</td>
        <td>C</td>
    </tr>
</table>


EOF;

$pdf->writeHTML($html);

$pdf->Output();
?>
