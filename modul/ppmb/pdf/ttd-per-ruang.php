<?php
include '../../../includes/fpdf/fpdf.php';

// Data dummi
$ujian = array(
    'NM_UJIAN'  => 'Ujian Penerimaan Mahasiswa Baru Universitas Airlangga Jalur Pascasarjana',
    'GELOMBANG' => '2',
    'SEMESTER'  => 'Gasal',
    'TAHUN'     => '2012',
    'NO_SURAT'  => 'SP NO.570/PPMB-Unair/SP/V/2012'
);

$ruang = array(
    'LOKASI'            => 'Fakultas Ekonomi dan Bisnis',
    'NM_RUANG'          => '201',
    'NM_KOORDINATOR'    => 'Nama Koordinator'
);


$pdf = new FPDF();

// Add font verdana
$pdf->AddFont('Verdana', '', 'verdana.php');
$pdf->AddFont('Verdana', 'B', 'verdanab.php');

$pdf->AddPage('L');

$border = 0;

$pdf->SetFont('Verdana', 'B', 9);
$pdf->Cell(null, 4, "Daftar Pembayaran Honorarium", $border, true, 'C');
$pdf->Cell(null, 4, "Pelaksanaan {$ujian['NM_UJIAN']} Gel. {$ujian['GELOMBANG']} Sem. {$ujian['SEMESTER']} Tahun {$ujian['TAHUN']}", $border, true, 'C');
$pdf->Cell(null, 4, "Berdasarkan {$ujian['NO_SURAT']}", $border, true, 'C');
$pdf->Cell(null, 4, "Koordinator : {$ruang['NM_KOORDINATOR']}", $border, true, 'C');
$pdf->Cell(null, 4, "Lokasi : {$ruang['NM_RUANG']} - {$ruang['LOKASI']}", $border, true, 'C');

$pdf->Ln(2);

// Header table
$tempY = $pdf->GetY();
$pdf->SetFont('Verdana', 'B', 8);
$pdf->Cell(10, 8, 'NO', 1, false, 'C');
$pdf->Cell(70, 8, 'NAMA', 1, false, 'C');
$pdf->Cell(25, 8, 'TUGAS', 1, false, 'C');
$pdf->MultiCell(20, 4, "Kelompok Ujian", 1, 'C');
$pdf->SetXY(135, $tempY);
$pdf->Cell(20, 8, "RUANG", 1, false, 'C');
$pdf->Cell(20, 8, "GOL", 1, false, 'C');
$pdf->Cell(20, 8, "BRUTO", 1, false, 'C');
$pdf->Cell(20, 8, "PPh 0%", 1, false, 'C');
$pdf->Cell(20, 8, "PPh 5%", 1, false, 'C');
$pdf->Cell(20, 8, "PPh 15%", 1, false, 'C');


$pdf->Output();
?>