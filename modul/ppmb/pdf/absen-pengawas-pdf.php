<?php
include '../config.php';
include '../../../tcpdf/config/lang/ind.php';
include '../../../tcpdf/tcpdf.php';

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->SetMargins(15, 2, 15);

$pdf->AddPage();

$html = <<<EOF
<style>
    td {font-family: times;}
    .title { font-size:40px;font-weight:bold; }
    .header { font-size:47px;font-weight:bold; }
    .address { font-size:30px; }
</style>
<table width="100%" border="0">
    <tr>
        <td width="100%" align="center">
            <span class="title">DEPARTEMEN PENDIDIKAN NASIONAL</span><br/>
            <span class="header">UNIVERSITAS AIRLANGGA</span><br/>
            <span class="title">PUSAT PENERIMAAN MAHASISWA BARU (PPMB) 2012</span><br/>
            <span class="address">Sekretariat : Jl. Dharmawangsa 29 Surabaya, Telp. / Fax (031) 5048654</span>
        </td>
    </tr>
</table>
<hr style="height:2px;" />
<table width="100%" border="0">
    <tr>
        <td colspan="3"></td>
    </tr>
    <tr>
        <td width="100%" colspan="2" align="center">
            <span style="font-size:36px;font-weight:bold;">DAFTAR HADIR : PENGAWAS MANDIRI - 1 IPA</span>
        </td>
    </tr>
    <tr>
        <td align="left"><span style="font-size:36px;font-weight:bold;">Koordinator No. 1</span></td>
        <td align="right"><span style="font-size:36px;font-weight:bold;">Nama : Aris Armuninggar</span></td>
    </tr>
    <tr>
        <td colspan="3"></td>
    </tr>
</table>
<table width="100%" border="1" cellpadding="4" align="center">
    <tr>
        <td width="7%" rowspan="2">NO.</td>
        <td width="35%" rowspan="2">NAMA</td>
        <td width="20%" rowspan="2">RUANG</td>
        <td width="38%">TANDA TANGAN</td>
    </tr>
    <tr>
        <td>03 Juli 2011</td>
    </tr>
    <tr>
        <td>1</td>
        <td align="left">Zuhri</td>
        <td>A-301</td>
        <td align="right">1...................</td>
    </tr>
    <tr>
        <td>2</td>
        <td align="Left">Eddy Supriyono</td>
        <td>A-302</td>
        <td align="Left">2...................</td>
    </tr>
</table>


EOF;

$pdf->writeHTML($html);

$pdf->Output();
?>
