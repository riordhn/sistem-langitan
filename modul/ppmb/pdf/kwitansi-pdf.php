<?php
require '../../../config.php';

$db = new MyOracle();

require_once '../../../tcpdf/config/lang/eng.php';
require_once '../../../tcpdf/tcpdf.php';

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sistem Langitan');
$pdf->SetTitle('Kwitansi PMB');
$pdf->SetSubject('Kwitansi PMB');

include '../includes/function.php';
$var = decode($_SERVER['REQUEST_URI']);
$kode_voucher = $var['kode_voucher'];
$bayar1 = $var['bayar1'];
$bayar2 = $var['bayar2'];
$bayar3 = $var['bayar3'];
$bayar4 = $var['bayar4'];
$bayar5 = $var['bayar5'];
$bayar6 = $var['bayar6'];
$bayar7 = $var['bayar7'];
$bayar8 = $var['bayar8'];
$total_bayar = $var['total_bayar'];
$terbilang = $var['terbilang'];
$no_kwitansi = $var['no_kwitansi'];
$is_tunai = $var['is_tunai'];
$tgl_transfer = $var['tgl_transfer'];
$ukuran_jas = $var['ukuran_jas'];
$tgl_tes_tulis = $var['tgl_tes_tulis'];

if($is_tunai == 1) {
	$is_tunai = "Tunai";
}
else {
	$is_tunai = "Transfer Tgl. ".$tgl_transfer;
}

$id_pt = $id_pt_user;

$nm_pengguna = $user->NM_PENGGUNA;



/*$biomhs="select mhs.nim_mhs, case when j.id_jenjang=2 then p.nm_pengguna||', '||p.gelar_belakang else p.nm_pengguna end , j.nm_jenjang, ps.nm_program_studi, mhs.status_akademik_mhs, case when f.id_fakultas=9 then 'PROGRAM '||f.nm_fakultas else 'FAKULTAS '||f.nm_fakultas end,
			f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas, f.id_fakultas,
			kt.nm_kota, TO_CHAR(p.tgl_lahir_pengguna, 'DD-MM-YYYY'), TO_CHAR(mhs.tgl_lulus_mhs, 'DD-MM-YYYY'), pw.no_ijasah, TO_CHAR(mhs.tgl_terdaftar_mhs, 'DD-MM-YYYY'), mhs.id_mhs, pw.judul_ta, pw.lahir_ijazah,
			j.id_jenjang,mhs.id_program_studi,pm.nm_prodi_minat,mhs.id_mhs, thn_angkatan_mhs
			from mahasiswa mhs
			left join kota kt on kt.id_kota=mhs.lahir_kota_mhs
			left join pengajuan_wisuda pw on pw.id_mhs=mhs.id_mhs
			left join pengguna p on mhs.id_pengguna=p.id_pengguna
			left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
			left join fakultas f on ps.id_fakultas=f.id_fakultas
			left join jenjang j on ps.id_jenjang=j.id_jenjang
			left join prodi_minat pm on mhs.id_prodi_minat=pm.id_prodi_minat
			where mhs.nim_mhs='".$nim."'";
$result1 = $db->Query($biomhs)or die("salah kueri biodata ");
$r1 = $db->FetchRow();


	$agk = $r1[25];

	if($agk < '2014'){

$biomhs="select mhs.nim_mhs, case when j.id_jenjang=2 then p.nm_pengguna||', '||p.gelar_belakang else p.nm_pengguna end , j.nm_jenjang, ps.nm_program_studi, mhs.status_akademik_mhs, case when f.id_fakultas=9 then 'PROGRAM '||f.nm_fakultas else 'FAKULTAS '||f.nm_fakultas end,
			f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas, f.id_fakultas,
			kt.nm_kota, TO_CHAR(p.tgl_lahir_pengguna, 'DD-MM-YYYY'), TO_CHAR(mhs.tgl_lulus_mhs, 'DD-MM-YYYY'), pw.no_ijasah, TO_CHAR(mhs.tgl_terdaftar_mhs, 'DD-MM-YYYY'), mhs.id_mhs, pw.judul_ta, pw.lahir_ijazah,
			j.id_jenjang,mhs.id_program_studi,pm.nm_prodi_minat,mhs.id_mhs, thn_angkatan_mhs
			from mahasiswa mhs
			left join kota kt on kt.id_kota=mhs.lahir_kota_mhs
			left join pengajuan_wisuda pw on pw.id_mhs=mhs.id_mhs
			left join pengguna p on mhs.id_pengguna=p.id_pengguna
			left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
			left join fakultas f on ps.id_fakultas=f.id_fakultas
			left join jenjang j on ps.id_jenjang=j.id_jenjang
			left join prodi_minat pm on mhs.id_prodi_minat=pm.id_prodi_minat
			where mhs.nim_mhs='".$nim."'";
$result1 = $db->Query($biomhs)or die("salah kueri biodata ");

$r1 = $db->FetchRow();

	}
	
	$nim_mhs = $r1[0];
	$nm_mhs = $r1[1];
	$jenjang = $r1[2];
	$prodi = $r1[3];
	$status = $r1[4];
	$fak = $r1[5];
	$alm_fak = $r1[6];
	$pos_fak = $r1[7];
	$tel_fak = $r1[8];
	$fax_fak = $r1[9];
	$web_fak = $r1[10];
	$eml_fak = $r1[11];
	$kd_fak = $r1[12];
	$tpt_lhr = $r1[13];
	$tgl_lhr = $r1[14];
	$tgl_lls = $r1[15];
	$no_ijazah = $r1[16];
	$tgl_dtr = $r1[17];
	$id_mhs = $r1[18];
	$jdl_ta = $r1[19];
	$ttl = $r1[20];
	$jjg1 = $r1[21];
	$idprodi = $r1[22];
	$minatmhs = $r1[23];
	$id_mhs = $r1[24];



$list2="SELECT SYSDATE FROM DUAL";
$result22 = $db->Query($list2)or die("salah kueri list2 ");
while($r22 = $db->FetchRow()) {
	$tglcetak=$r22[0];
}


$dekan="select d.nip_dosen ,case when p.gelar_belakang is not null then 
trim(p.gelar_depan||' '||upper(p.nm_pengguna)||', '||p.gelar_belakang) else 
trim(p.gelar_depan||' '||upper(p.nm_pengguna)) end as nm_dosen, 
case when f.id_fakultas=9 then 'Direktur' else 'Dekan' end as dekan,
case when f.id_fakultas=9 then 'Wakil Direktur 1' else 'Wakil Dekan 1' end as wadek
from fakultas f, pengguna p, dosen d
where p.id_pengguna=f.id_wadek1 and f.id_fakultas=".$kd_fak." and p.id_pengguna=d.id_pengguna";
$resultdekan = $db->Query($dekan)or die("salah kueri dekan haha ");
while($dkn = $db->FetchRow()) {
	$nm_dkn = $dkn[1];
	$nip_dkn = $dkn[0];
	$ttd_dekan=$dkn[2];
	$ttd_wadek=$dkn[3];
}*/


$biocmhs="select cmhs.id_c_mhs, p.id_penerimaan, cmhs.nm_c_mhs, s.nm_sekolah, cms.tahun_lulus, 
				p.tahun, j.nm_jalur, p.gelombang,
				j.nm_jenjang || ' - ' || ps.nm_program_studi, 
				j1.nm_jenjang || ' - ' || ps1.nm_program_studi,
				j2.nm_jenjang || ' - ' || ps2.nm_program_studi
			from calon_mahasiswa_baru cmhs
			join penerimaan p on p.id_penerimaan = cmhs.id_penerimaan
			join jalur j on j.id_jalur = p.id_jalur
			join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmhs.id_c_mhs
			left join sekolah s on s.id_sekolah = cms.id_sekolah_asal
			left join program_studi ps on ps.id_program_studi = cmhs.id_program_studi
			left join jenjang j on j.id_jenjang = ps.id_jenjang
			left join program_studi ps1 on ps1.id_program_studi = cmhs.id_pilihan_1
			left join jenjang j1 on j1.id_jenjang = ps1.id_jenjang
			left join program_studi ps2 on ps2.id_program_studi = cmhs.id_pilihan_2
			left join jenjang j2 on j2.id_jenjang = ps2.id_jenjang
			where cmhs.kode_voucher = '".$kode_voucher."' and p.id_perguruan_tinggi='".$id_pt."'";
$result1 = $db->Query($biocmhs)or die("salah kueri biodata ");
$cmhs = $db->FetchRow();

$nm_cmhs = $cmhs[2];
$nm_sekolah = $cmhs[3];
$tahun_lulus = $cmhs[4];
$tahun_penerimaan = $cmhs[5];
$nm_jalur = $cmhs[6];
$gelombang = $cmhs[7];

if ($cmhs[8] != '') {
	$nm_prodi_diterima = $cmhs[8];
}
else {
	$nm_prodi_diterima = "-";
}

if ($cmhs[9] != '') {
	$nm_prodi_1 = $cmhs[9];
}
else {
	$nm_prodi_1 = "-";
}

if ($cmhs[10] != '') {
	$nm_prodi_2 = $cmhs[10];
}
else {
	$nm_prodi_2 = "-";
}

$list2="SELECT SYSDATE FROM DUAL";
$result22 = $db->Query($list2)or die("salah kueri list2 ");
while($r22 = $db->FetchRow()) {
	$tglcetak=$r22[0];
}

// potong nama calon mhs maksimal 29 karakter
if (strlen($nm_cmhs) > 30)
{
    $nm_cmhs = substr($nm_cmhs, 0, 30)."...";
}
else
{
    $nm_cmhs = $nm_cmhs;
}

// potong nama sekolah maksimal 29 karakter
if (strlen($nm_sekolah) > 30)
{
    $nm_sekolah = substr($nm_sekolah, 0, 30)."...";
}
else
{
    $nm_sekolah = $nm_sekolah;
}


$bulan['01'] = "Januari"; $bulan['02'] = "Pebruari"; $bulan['03'] = "Maret"; $bulan['04'] = "April"; $bulan['05'] = "Mei"; $bulan['06'] = "Juni"; $bulan['07'] = "Juli"; $bulan['08'] = "Agustus"; $bulan['09'] = "September"; $bulan['10'] = "Oktober";  $bulan['11'] = "Nopember";  $bulan['12'] = "Desember";


// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 8, 10);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 10);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 8);

// add a page
$pdf->AddPage('P', 'A4');
/*$pdf->AddPage('L', 'F4');*/

// draw jpeg image
//$pdf->Image('../includes/logo_unair.png', 75, 25, 150, '', '', '', '', false, 72);
//$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
// set the starting point for the page content
$pdf->setPageMark();

if ($id_pt == 1) {
	$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td width="8%" align="left" valign="middle"><img src="../../../img/akademik_images/logo-'.$nama_singkat.'.gif" width="45" border="0"></td>
    <td width="92%" align="left" valign="middle"><font size="16"><b>'.strtoupper($nama_pt).'</b></font><br>'.strtoupper($alamat).' Telp. '.$telp_pt.'<br>No. HP PMB: 0857-0666-6661</td>
  </tr>
</table>';
}
else {
	$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td width="8%" align="left" valign="middle"><img src="../../../img/akademik_images/logo-'.$nama_singkat.'.gif" width="45" border="0"></td>
    <td width="92%" align="left" valign="middle"><font size="14"><b>'.strtoupper($nama_pt).'</b></font><br>'.strtoupper($alamat).' Telp. '.$telp_pt.'</td>
  </tr>
</table>';
}


$html .= '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2">
		<hr>
	</td>
  </tr>
  <tr>
    <td colspan="2" align="center"><font size="14"><b>TANDA BUKTI PEMBAYARAN</b></font><br><font size="10"><b>NOMOR PENDAFTARAN : '.$kode_voucher.'</b></font></td>
  </tr>
  <tr>
    <td colspan="2">
		<hr>
	</td>
  </tr>
  <tr>
    <td width="15%"><font size="10">Nama</font></td>
	<td width="40%"><font size="10"> : '.strtoupper($nm_cmhs).'</font></td>
	<td width="15%"><font size="10">Gelombang</font></td>
	<td width="30%"><font size="10"> : '.$gelombang.'</font></td>
  </tr>
  <tr>
    <td><font size="10">Sekolah</font></td>
	<td><font size="10"> : '.strtoupper($nm_sekolah).'</font></td>
	<td width="15%"><font size="10">Prodi Diterima</font></td>
	<td width="30%"><font size="10"> : '.ucwords(strtolower($nm_prodi_diterima)).'</font></td>
  </tr>
  <tr>
    <td width="15%"><font size="10">Tahun Lulus</font></td>
	<td width="40%"><font size="10"> : '.$tahun_lulus.'</font></td>
	<td width="15%"><font size="10">Program Studi 1</font></td>
	<td width="30%"><font size="10"> : '.ucwords(strtolower($nm_prodi_1)).'</font></td>
  </tr>
  <tr>
    <td><font size="10">Kelas/Jalur</font></td>
	<td><font size="10"> : '.$nm_jalur.'</font></td>
	<td width="15%"><font size="10">Program Studi 2</font></td>
	<td width="30%"><font size="10"> : '.ucwords(strtolower($nm_prodi_2)).'</font></td>
  </tr>
  <tr>
  	<td colspan="2"><br>&nbsp;</td>
  </tr>
</table>

<table width="100%" border="1" cellpadding="4">

	<tr>
		 <th align="center" width="5%"><font size="10">No</font></th>
		 <th align="center" width="50%"><font size="10">Jenis Bayar</font></th>
		 <th align="center" width="25%"><font size="10">Jumlah</font></th>
		 <th align="center" width="20%"><font size="10">Keterangan</font></th>			 			 
    </tr>

    <tr>
		 <th align="center" width="5%"><font size="10">1</font></th>
		 <th align="left" width="50%"><font size="10">DPP Semester 1</font></th>
		 <th align="left" width="4%"><font size="10">Rp</font></th>
		 <th align="right" width="21%"><font size="10">'.number_format($bayar1).'</font></th>
		 <th align="center" valign="middle" width="20%" rowspan="2"><font size="12"><b>No. Kwitansi</b></font></th>			 			 
    </tr>

    <tr>
		 <th align="center" width="5%"><font size="10">2</font></th>
		 <th align="left" width="50%"><font size="10">DPP Semester 2</font></th>
		 <th align="left" width="4%"><font size="10">Rp</font></th>
		 <th align="right" width="21%"><font size="10">'.number_format($bayar2).'</font></th>			 			 
    </tr>

    <tr>
		 <th align="center" width="5%"><font size="10">3</font></th>
		 <th align="left" width="50%"><font size="10">Pasak, Atribut, Buku Pedoman</font></th>
		 <th align="left" width="4%"><font size="10">Rp</font></th>
		 <th align="right" width="21%"><font size="10">'.number_format($bayar3).'</font></th>
		 <th align="center" valign="middle" width="20%" rowspan="20"><font size="16"><b>'.$no_kwitansi.'</b></font></th>	 
    </tr>

    <tr>
		 <th align="center" width="5%"><font size="10">4</font></th>
		 <th align="left" width="50%"><font size="10">Her Registrasi Semester 1</font></th>
		 <th align="left" width="4%"><font size="10">Rp</font></th>
		 <th align="right" width="21%"><font size="10">'.number_format($bayar4).'</font></th>	 
    </tr>

    <tr>
		 <th align="center" width="5%"><font size="10">5</font></th>
		 <th align="left" width="50%"><font size="10">IKM Semester 1</font></th>
		 <th align="left" width="4%"><font size="10">Rp</font></th>
		 <th align="right" width="21%"><font size="10">'.number_format($bayar5).'</font></th>		 			 
    </tr>

    <tr>
		 <th align="center" width="5%"><font size="10">6</font></th>
		 <th align="left" width="50%"><font size="10">SPP Bulan September '.$tahun_penerimaan.'</font></th>
		 <th align="left" width="4%"><font size="10">Rp</font></th>
		 <th align="right" width="21%"><font size="10">'.number_format($bayar6).'</font></th>				 
    </tr>

    <tr>
		 <th align="center" width="5%"><font size="10">7</font></th>
		 <th align="left" width="50%"><font size="10">UTS Semester 1</font></th>
		 <th align="left" width="4%"><font size="10">Rp</font></th>
		 <th align="right" width="21%"><font size="10">'.number_format($bayar7).'</font></th>	 			 
    </tr>

    <tr>
		 <th align="center" width="5%"><font size="10">8</font></th>
		 <th align="left" width="50%"><font size="10">UAS Semester 1</font></th>
		 <th align="left" width="4%"><font size="10">Rp</font></th>
		 <th align="right" width="21%"><font size="10">'.number_format($bayar8).'</font></th>	 			 
    </tr>
    ';

if ($id_pt == 1) {
	$html .= '
    <tr>
		 <th align="left" colspan="2"><font size="10"><b>Total Yang Harus Dibayar</b></font></th>
		 <th align="left" width="4%"><font size="10">Rp</font></th>
		 <th align="right" width="21%"><font size="10"><b>'.number_format($total_bayar).'</b></font></th>	 			 
    </tr>

    <tr>
		 <th align="left" colspan="4"><font size="10"><b><i>Terbilang : &nbsp; &nbsp; '.$terbilang.'</i></b></font></th>	 			 
    </tr>

</table>
';
}
else {
	$html .= '
   	<tr>
		 <th align="center" width="5%"><font size="10">9</font></th>
		 <th align="left" width="50%"></th>
		 <th align="left" width="25%"><font size="10">Rp </font></th>	 			 
    </tr>

    <tr>
		 <th align="center" width="5%"><font size="10">10</font></th>
		 <th align="left" width="50%"></th>
		 <th align="left" width="25%"><font size="10">Rp </font></th>	 			 
    </tr>

    <tr>
		 <th align="center" width="5%"><font size="10">11</font></th>
		 <th align="left" width="50%"></th>
		 <th align="left" width="25%"><font size="10">Rp </font></th>	 			 
    </tr>

    <tr>
		 <th align="left" colspan="2"><font size="10"><b>Total Yang Harus Dibayar</b</font>></th>
		 <th align="left" width="25%"><font size="10"><b>Rp </b></font></th>	 			 
    </tr>

    <tr>
		 <th align="left" colspan="3"><font size="10"><b><i>Terbilang : </i></b></font></th>	 			 
    </tr>

</table>
';
}

$html .= '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2"><br>&nbsp;</td>
  </tr>
  <tr>
    <td width="10%"><font size="10">Lembar 1</font></td>
	<td width="25%"><font size="10"> : Untuk Mahasiswa</font></td>
	<td width="35%" align="center"><font size="10"><b>Jenis Pembayaran</b></font></td>
    <td width="30%" align="center"><font size="10">'.ucwords(strtolower($kota_pt)).',&nbsp;'.date("d", strtotime($tglcetak)).' '.$bulan[date("m", strtotime($tglcetak))].' '.date("Y", strtotime($tglcetak)).'</font></td>
  </tr>
  <tr>
    <td width="10%"><font size="10">Lembar 2</font></td>
	<td width="25%"><font size="10"> : Bagian Keuangan</font></td>
	<td width="35%" align="center"><font size="10"><b>'. $is_tunai .'</b></font></td>
    <td width="30%" align="center"><font size="10">Petugas</font></td>
  </tr>
  <tr>
    <td width="10%"><font size="10">Lembar 3</font></td>
	<td width="25%"><font size="10"> : PR UMAHA</font></td>
	<td width="35%" align="left"></td>
    <td width="30%"></td>
  </tr>
  <tr>
    <td width="10%"></td>
	<td width="25%"></td>
	<td width="35%" align="center"></td>
	<td width="30%"></td>
  </tr>
  <tr>
  	<td colspan="2"><br>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4"><font size="10">*) Periksa Kembali Kwitansi yang sudah diterima</font></td>
  </tr>
  <tr>
    <td colspan="3"><font size="10">*) Uang yang sudah dibayarkan tidak bisa dikembalikan</font></td>
	<td width="30%" align="center">' . $nm_pengguna . '</td>
  </tr>
  <tr>
  	<td colspan="2"><br>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4"><font size="10"><b>Tes Tulis & Psikologi Dilaksanakan Pada Tanggal : '. $tgl_tes_tulis .'</b></font></td>
  </tr>
  <tr>
  	<td colspan="2"><br>&nbsp;</td>
  </tr>
  <tr>
  	<td colspan="2"><br>&nbsp;</td>
  </tr>
  <tr>
  	<td colspan="2"><br>&nbsp;</td>
  </tr>
  <tr>
  	<td colspan="2"><br>&nbsp;</td>
  </tr>
</table>

<table width="100%" border="1" cellpadding="4">

	<tr>
		 <th align="left" width="50%" height="70"><font size="10">UKURAN JAS</font></th>
		 <th align="center" width="50%" height="70"><font size="10">' . $ukuran_jas . '</font></th>		 			 
    </tr>

	<tr>
		 <th align="left" width="50%" height="70"><font size="10">PENGAMBILAN JAS</font></th>
		 <th align="center" width="50%" height="70"></th>		 			 
    </tr>

    <tr>
		 <th align="left" width="50%" height="70"><font size="10">PENGAMBILAN KTM</font></th>
		 <th align="center" width="50%" height="70"></th>		 			 
    </tr>

    <tr>
		 <th align="left" width="50%" height="70"><font size="10">PENGAMBILAN BUKU PEDOMAN</font></th>
		 <th align="center" width="50%" height="70"></th>		 			 
    </tr>    

</table>


<br><br>
<b><i>Waktu cetak : ' . date('d F Y  H:i:s') . '</i><b>
';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('Kwitansi PMB '.strtoupper($kode_voucher).'.pdf', 'I');