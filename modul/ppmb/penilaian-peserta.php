<?php
include('config.php');

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        $smarty->assign('penerimaan_set', $db->QueryToArray("
            select p.id_penerimaan, j.nm_jenjang, p.nm_penerimaan, p.tahun, p.gelombang, p.semester, jl.nm_jalur
            from penerimaan p
            join jenjang j on j.id_jenjang = p.id_jenjang
            join jalur jl on jl.id_jalur = p.id_jalur
            where p.is_aktif = 1
            order by p.is_aktif desc, p.tahun, p.semester, p.gelombang, p.id_jalur, p.id_jenjang"));
    }
    
    if ($mode == 'prodi')
    {
        $id_penerimaan = get('id_penerimaan');
        
        $db->Query("select * from penerimaan where id_penerimaan = {$id_penerimaan}");
        $row = $db->FetchAssoc();
        $smarty->assign('nm_penerimaan', "{$row['NM_PENERIMAAN']} Gel. {$row['GELOMBANG']} SEMESTER {$row['SEMESTER']} TAHUN {$row['TAHUN']}");
        
        $smarty->assign('penerimaan_prodi_set', $db->QueryToArray("
            select pp.id_penerimaan, pp.id_program_studi, ps.nm_program_studi, f.nm_fakultas from penerimaan_prodi pp
            join penerimaan p on p.id_penerimaan = pp.id_penerimaan
            join program_studi ps on ps.id_program_studi = pp.id_program_studi
            join fakultas f on f.id_fakultas = ps.id_fakultas
            where p.is_aktif = 1 and pp.is_aktif = 1 and p.id_penerimaan = {$id_penerimaan}
            order by f.id_fakultas, ps.id_program_studi"));
    }
    
    if ($mode == 'detail')
    {
        $id_penerimaan = get('id_penerimaan');
        $id_program_studi = get('id_program_studi');
        
        // penerimaan
        $db->Query("select * from penerimaan where id_penerimaan = {$id_penerimaan}");
        $row = $db->FetchAssoc();
        $smarty->assign('penerimaan', $row);
        
        // prodi
        $db->Query("select nm_program_studi from program_studi where id_program_studi = {$id_program_studi}");
        $row = $db->FetchAssoc();
        $smarty->assign('nm_program_studi', $row['NM_PROGRAM_STUDI']);
        
        // detail mahasiswa
        $calon_mahasiswa_baru_set =  $db->QueryToArray("
            select no_ujian, nm_c_mhs, gelar, n.*
            from calon_mahasiswa_baru cmb
            join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
            left join nilai_cmhs_pasca n on n.id_c_mhs = cmb.id_c_mhs
            where id_pilihan_1 = {$id_program_studi} and p.id_penerimaan = {$id_penerimaan} and cmb.tgl_verifikasi_ppmb is not null
            order by n.total_nilai desc");
            
        for ($i = 0; $i < count($calon_mahasiswa_baru_set); $i++)
        {
            $calon_mahasiswa_baru_set[$i]['NILAI_TPA'] = number_format($calon_mahasiswa_baru_set[$i]['NILAI_TPA'], 2);
            $calon_mahasiswa_baru_set[$i]['NILAI_INGGRIS'] = number_format($calon_mahasiswa_baru_set[$i]['NILAI_INGGRIS'], 2);
            $calon_mahasiswa_baru_set[$i]['NILAI_ILMU'] = number_format($calon_mahasiswa_baru_set[$i]['NILAI_ILMU'], 2);
            $calon_mahasiswa_baru_set[$i]['NILAI_WAWANCARA'] = number_format($calon_mahasiswa_baru_set[$i]['NILAI_WAWANCARA'], 2);
            $calon_mahasiswa_baru_set[$i]['NILAI_IPK'] = number_format($calon_mahasiswa_baru_set[$i]['NILAI_IPK'], 2);
            $calon_mahasiswa_baru_set[$i]['NILAI_KARYA_ILMIAH'] = number_format($calon_mahasiswa_baru_set[$i]['NILAI_KARYA_ILMIAH'], 2);
            $calon_mahasiswa_baru_set[$i]['NILAI_REKOMENDASI'] = number_format($calon_mahasiswa_baru_set[$i]['NILAI_REKOMENDASI'], 2);
            $calon_mahasiswa_baru_set[$i]['NILAI_MATRIKULASI'] = number_format($calon_mahasiswa_baru_set[$i]['NILAI_MATRIKULASI'], 2);
            $calon_mahasiswa_baru_set[$i]['TOTAL_NILAI'] = number_format($calon_mahasiswa_baru_set[$i]['TOTAL_NILAI'], 2);
        }
        
        $smarty->assign('calon_mahasiswa_baru_set', $calon_mahasiswa_baru_set);
        
        // data statistik
        $db->Query("select s.* from statistik_cmhs s where id_program_studi = {$id_program_studi} and id_penerimaan = {$id_penerimaan}");
        $statistik = $db->FetchAssoc();
        
        // menentukan passing grade
        $statistik = array_merge($statistik, array('PEMINAT' => count($calon_mahasiswa_baru_set)));
        if ($statistik['KUOTA'] < $statistik['PEMINAT'])
            $statistik['PASSING_GRADE'] = $statistik['NILAI_RATA'] - $statistik['STANDAR_DEVIASI']; /* bypass passing grade untuk gel I genap 2011 */
        else
            $statistik['PASSING_GRADE'] = $statistik['NILAI_RATA'] - $statistik['STANDAR_DEVIASI'];
        
        $statistik['PASSING_GRADE'] = number_format($statistik['PASSING_GRADE'], 2);
        
        $smarty->assign('statistik', $statistik);
    }
    
    if ($mode == 'xml')
    {
        header("Content-type: text/xml");
        
        $id_penerimaan = get('id_penerimaan');
        
        echo $db->QueryToXML("
            select
                no_ujian, nm_c_mhs as nama_peserta,
                to_char(cmb.tgl_lahir,'YYYY-MM-DD') as tgl_lahir,
                (select kode_ppmb from program_studi where id_program_studi = id_pilihan_1) as pilihan_1,
                (select kode_ppmb from program_studi where id_program_studi = id_pilihan_2) as pilihan_2,
                (select kode_ppmb from program_studi where id_program_studi = id_pilihan_3) as pilihan_3,
                (select kode_ppmb from program_studi where id_program_studi = id_pilihan_4) as pilihan_4
            from calon_mahasiswa_baru cmb
            where tgl_verifikasi_ppmb is not null and id_penerimaan = {$id_penerimaan}
            order by no_ujian");
            
        exit();
    }
}

$smarty->display("penilaian/peserta/{$mode}.tpl");
?>
