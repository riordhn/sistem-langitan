<?php

include('config.php');
include('class/Penerimaan.class.php');

$Penerimaan = new Penerimaan($db);

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    if (post('mode') == 'edit-kuota')
    {
        $db->BeginTransaction();
        
        foreach (array_keys($_POST) as $key)
        {
			
            if (substr($key, 0, 2) == 'k_')
            {
                $id_statistik_cmhs = str_replace("k_", "", $key);
                $db->Query("update statistik_cmhs set kuota = {$_POST[$key]} where id_statistik_cmhs = {$id_statistik_cmhs}");
				//echo "update statistik_cmhs set kuota = {$_POST[$key]} where id_statistik_cmhs = {$id_statistik_cmhs};<br/>";
				//continue;
            }
			
			if (substr($key, 0, 3) == 'kt_')
            {
                $id_statistik_cmhs = str_replace("kt_", "", $key);
                $db->Query("update statistik_cmhs set kuota_total = {$_POST[$key]} where id_statistik_cmhs = {$id_statistik_cmhs}");
				//echo "update statistik_cmhs set kuota_total = {$_POST[$key]} where id_statistik_cmhs = {$id_statistik_cmhs};<br/>";
				//continue;
            }
        }
        
        $result = $db->Commit();
        
        if ($result)
        {
            $smarty->assign('changed', 'Kuota berhasil dirubah.');
        }
    }

    if (post('mode') == 'generate')
    {
        $id_penerimaan = post('id_penerimaan');
        $prodi_asal_set = $_POST['prodi_asal'];
        $prodi_tujuan_set = $_POST['prodi_tujuan'];

        $pp_asal_set = post('pp_asal');
        $pp_tujuan_set = post('pp_tujuan');

        $pp_asal = implode(',', $pp_asal_set);
        $pp_tujuan = implode(',', $pp_tujuan_set);

        $prodi_asal = implode(',', $prodi_asal_set);
        $prodi_tujuan = implode(',', $prodi_tujuan_set);

        $penerimaan = $Penerimaan->GetPenerimaan($id_penerimaan);
        
		// mandiri d3 depag asing
        if (in_array($penerimaan['ID_JALUR'], array(3, 5, 27))) 
        {
			/**
            $statistik_set = $db->QueryToArray("
                select 
                    (select round(avg(nilai_prestasi+nilai_tpa),3) from nilai_cmhs n
                    join calon_mahasiswa_baru cmb on cmb.id_c_mhs = n.id_c_mhs
                    join program_studi ps on (ps.id_program_studi = cmb.id_pilihan_1 and ps.jurusan_sekolah = n.jurusan_sekolah)
                    join penerimaan_prodi pp on (pp.id_penerimaan = cmb.id_penerimaan and pp.id_program_studi = cmb.id_pilihan_1)
                    where pp.id_penerimaan_prodi in ({$pp_asal}) and (nilai_prestasi+nilai_tpa > 0)) as nilai_rata,

                    (select min(nilai_prestasi+nilai_tpa) from nilai_cmhs n
                    join calon_mahasiswa_baru cmb on cmb.id_c_mhs = n.id_c_mhs
                    join program_studi ps on (ps.id_program_studi = cmb.id_pilihan_1 and ps.jurusan_sekolah = n.jurusan_sekolah)
                    join penerimaan_prodi pp on (pp.id_penerimaan = cmb.id_penerimaan and pp.id_program_studi = cmb.id_pilihan_1)
                    where pp.id_penerimaan_prodi in ({$pp_asal}) and (nilai_prestasi+nilai_tpa > 0)) as nilai_min,

                    (select max(nilai_prestasi+nilai_tpa) from nilai_cmhs n
                    join calon_mahasiswa_baru cmb on cmb.id_c_mhs = n.id_c_mhs
                    join program_studi ps on (ps.id_program_studi = cmb.id_pilihan_1 and ps.jurusan_sekolah = n.jurusan_sekolah)
                    join penerimaan_prodi pp on (pp.id_penerimaan = cmb.id_penerimaan and pp.id_program_studi = cmb.id_pilihan_1)
                    where pp.id_penerimaan_prodi in ({$pp_asal}) and (nilai_prestasi+nilai_tpa > 0)) as nilai_max,

                    (select round(stddev(nilai_prestasi+nilai_tpa),3) from nilai_cmhs n
                    join calon_mahasiswa_baru cmb on cmb.id_c_mhs = n.id_c_mhs
                    join program_studi ps on (ps.id_program_studi = cmb.id_pilihan_1 and ps.jurusan_sekolah = n.jurusan_sekolah)
                    join penerimaan_prodi pp on (pp.id_penerimaan = cmb.id_penerimaan and pp.id_program_studi = cmb.id_pilihan_1)
                    where pp.id_penerimaan_prodi in ({$pp_asal}) and (nilai_prestasi+nilai_tpa > 0)) as standar_deviasi
                from dual");
			 * 
			 */
			
			// Modifikasi : 11 Juli 2013
			// Keterangan : penambahan perhitungan untuk nilai BL
			$bobot_skor_tulis	= "1";   //0.8  // dibuat string biar titiknya tetap
			$bobot_skor_bl		= "0.0"; //0.2  // nilai 0.2 dan 0.8 untuk penerimaan lama
			$skor_total			= "((nilai_prestasi+nilai_tpa)*{$bobot_skor_tulis})+(nilai_bl*{$bobot_skor_bl})";
			
			// Statistik model IPC campur
			$statistik_set = $db->QueryToArray("
				select 
					(select round(avg( {$skor_total} ),3) from nilai_cmhs n
					join calon_mahasiswa_baru cmb on cmb.id_c_mhs = n.id_c_mhs
					join calon_mahasiswa_pilihan cmp on cmp.id_c_mhs = cmb.id_c_mhs and cmp.jurusan_sekolah = n.jurusan_sekolah
					join program_studi ps on (ps.id_program_studi = cmp.id_pilihan and ps.jurusan_sekolah = n.jurusan_sekolah)
					join penerimaan_prodi pp on (pp.id_penerimaan = cmb.id_penerimaan and pp.id_program_studi = cmp.id_pilihan)
					where pp.id_penerimaan_prodi in ({$pp_asal}) and cmp.pilihan_ke = (1) and (nilai_prestasi+nilai_tpa > 0)) as nilai_rata,
					
					(select min( {$skor_total} ) from nilai_cmhs n
					join calon_mahasiswa_baru cmb on cmb.id_c_mhs = n.id_c_mhs
					join calon_mahasiswa_pilihan cmp on cmp.id_c_mhs = cmb.id_c_mhs and cmp.jurusan_sekolah = n.jurusan_sekolah
					join program_studi ps on (ps.id_program_studi = cmp.id_pilihan and ps.jurusan_sekolah = n.jurusan_sekolah)
					join penerimaan_prodi pp on (pp.id_penerimaan = cmb.id_penerimaan and pp.id_program_studi = cmp.id_pilihan)
					where pp.id_penerimaan_prodi in ({$pp_asal}) and cmp.pilihan_ke = (1) and (nilai_prestasi+nilai_tpa > 0)) as nilai_min,
					
					(select max( {$skor_total} ) from nilai_cmhs n
					join calon_mahasiswa_baru cmb on cmb.id_c_mhs = n.id_c_mhs
					join calon_mahasiswa_pilihan cmp on cmp.id_c_mhs = cmb.id_c_mhs and cmp.jurusan_sekolah = n.jurusan_sekolah
					join program_studi ps on (ps.id_program_studi = cmp.id_pilihan and ps.jurusan_sekolah = n.jurusan_sekolah)
					join penerimaan_prodi pp on (pp.id_penerimaan = cmb.id_penerimaan and pp.id_program_studi = cmp.id_pilihan)
					where pp.id_penerimaan_prodi in ({$pp_asal}) and cmp.pilihan_ke = (1) and (nilai_prestasi+nilai_tpa > 0)) as nilai_max,
					
					(select round(stddev( {$skor_total} ),3) from nilai_cmhs n
					join calon_mahasiswa_baru cmb on cmb.id_c_mhs = n.id_c_mhs
					join calon_mahasiswa_pilihan cmp on cmp.id_c_mhs = cmb.id_c_mhs and cmp.jurusan_sekolah = n.jurusan_sekolah
					join program_studi ps on (ps.id_program_studi = cmp.id_pilihan and ps.jurusan_sekolah = n.jurusan_sekolah)
					join penerimaan_prodi pp on (pp.id_penerimaan = cmb.id_penerimaan and pp.id_program_studi = cmp.id_pilihan)
					where pp.id_penerimaan_prodi in ({$pp_asal}) and cmp.pilihan_ke = (1) and (nilai_prestasi+nilai_tpa > 0)) as standar_deviasi
				from dual");
        }

		// pasca profesi, spesialis, kerjasama, s1-alih jenis, s1-pbsb
        if (in_array($penerimaan['ID_JALUR'], array(4, 6, 20, 23, 24, 34, 35, 41)))  
        {
            $statistik_set = $db->QueryToArray("
                select 
                    (select min(n.total_nilai) from calon_mahasiswa_baru cmb
                    join nilai_cmhs_pasca n on n.id_c_mhs = cmb.id_c_mhs
                    join penerimaan_prodi pp on pp.id_program_studi = cmb.id_pilihan_1 and pp.id_penerimaan = cmb.id_penerimaan
                    where pp.id_penerimaan_prodi in ({$pp_asal}) and n.total_nilai > 0) nilai_min,
                    
                    (select round(avg(n.total_nilai),3) from calon_mahasiswa_baru cmb
                    join nilai_cmhs_pasca n on n.id_c_mhs = cmb.id_c_mhs
                    join penerimaan_prodi pp on pp.id_program_studi = cmb.id_pilihan_1 and pp.id_penerimaan = cmb.id_penerimaan
                    where pp.id_penerimaan_prodi in ({$pp_asal}) and n.total_nilai > 0) nilai_rata,

                    (select max(n.total_nilai) from calon_mahasiswa_baru cmb
                    join nilai_cmhs_pasca n on n.id_c_mhs = cmb.id_c_mhs
                    join penerimaan_prodi pp on pp.id_program_studi = cmb.id_pilihan_1 and pp.id_penerimaan = cmb.id_penerimaan
                    where pp.id_penerimaan_prodi in ({$pp_asal}) and n.total_nilai > 0) nilai_max,

                    (select round(stddev(n.total_nilai),3) from calon_mahasiswa_baru cmb
                    join nilai_cmhs_pasca n on n.id_c_mhs = cmb.id_c_mhs
                    join penerimaan_prodi pp on pp.id_program_studi = cmb.id_pilihan_1 and pp.id_penerimaan = cmb.id_penerimaan
                    where pp.id_penerimaan_prodi in ({$pp_asal}) and n.total_nilai > 0) standar_deviasi
                from dual");
        }

        $result = $db->Query("
            update statistik_cmhs set
                nilai_min = {$statistik_set[0]['NILAI_MIN']}, 
                nilai_rata = {$statistik_set[0]['NILAI_RATA']},
                nilai_max = {$statistik_set[0]['NILAI_MAX']},
                standar_deviasi = {$statistik_set[0]['STANDAR_DEVIASI']}
            where id_statistik_cmhs in (
                select s.id_statistik_cmhs from statistik_cmhs s
                join penerimaan_prodi pp on pp.id_program_studi = s.id_program_studi and pp.id_penerimaan = s.id_penerimaan
                where pp.id_penerimaan_prodi in ({$pp_tujuan}))");

        if ($result)
            $smarty->assign('updated', 'Statistik berhasil generate');
        else
            $smarty->assign('updated', 'Statistik gagal generate');
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        // class

        $smarty->assign('penerimaan_set', $Penerimaan->GetAllForView());

        // Mengambil data statistik per penerimaan
        $id_penerimaan = get('id_penerimaan', '');
        if ($id_penerimaan != '')
        {
            $statistik_set = $db->QueryToArray("
                select substr(j.nm_jenjang, 1, 2) as nm_jenjang, ps.nm_program_studi, id_statistik_cmhs, kuota, kuota_total, s.nilai_min, nilai_rata, nilai_max, standar_deviasi, (nilai_rata - standar_deviasi) as passing_grade,
                    (select count(id_c_mhs) from calon_mahasiswa_baru cmb where cmb.id_penerimaan = s.id_penerimaan and cmb.id_program_studi = s.id_program_studi) diterima
                from statistik_cmhs s
                join program_studi ps on ps.id_program_studi = s.id_program_studi
                join jenjang j on j.id_jenjang = ps.id_jenjang
                join fakultas f on f.id_fakultas = ps.id_fakultas
                where s.id_penerimaan = {$id_penerimaan}
                order by ps.id_fakultas");

            $smarty->assign('statistik_set', $statistik_set);
        }
    }

    if ($mode == 'generate')
    {
        $id_penerimaan = get('id_penerimaan', '');

        $penerimaan_set = $db->QueryToArray("select id_penerimaan, nm_penerimaan, tahun, gelombang, semester from penerimaan where id_penerimaan = {$id_penerimaan}");
        $smarty->assign('penerimaan', $penerimaan_set[0]);

        $program_studi_set = $db->QueryToArray("
            select pp.id_penerimaan_prodi, pp.id_penerimaan, ps.nm_program_studi, ps.id_program_studi, upper(j.nm_jenjang) nm_jenjang, p.nm_penerimaan
			from penerimaan_prodi pp
            join program_studi ps on ps.id_program_studi = pp.id_program_studi
            join jenjang j on j.id_jenjang = ps.id_jenjang
            join penerimaan p on p.id_penerimaan = pp.id_penerimaan
            where
				pp.is_aktif = 1 and
                pp.id_penerimaan in ({$id_penerimaan})
            order by p.nm_penerimaan, ps.nm_program_studi");
        $smarty->assign('program_studi_set', $program_studi_set);
    }
}

$smarty->display("penilaian/statistik/{$mode}.tpl");
?>
