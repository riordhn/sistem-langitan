<?php
include('config.php');
include('class/snmptn.class.php');

$mode = get('mode', 'view');

if ($request_method == 'GET')
{
	$snmptn = new SnmptnClass($db);
    $id_penerimaan = 44;
	
    if ($mode == 'view')
    {
		$smarty->assign('program_studi_set', $snmptn->GetRekapProvinsiPerProdi($id_penerimaan));
    }
    
    if ($mode == 'detail-prodi')
    {
        $id_program_studi = get('id_program_studi');
        
        $smarty->assign('program_studi', $snmptn->GetProgramStudi($id_program_studi));
        $smarty->assign('provinsi_set', $snmptn->GetRekapPesertaPerProvinsiByProdi($id_penerimaan, $id_program_studi));
    }
}

$smarty->display("snmptn/rpps/{$mode}.tpl");
?>
