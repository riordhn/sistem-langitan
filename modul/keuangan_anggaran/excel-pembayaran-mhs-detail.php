<?php
include 'config.php';
include 'class/laporan.class.php';
include 'class/list_data.class.php';

$list = new list_data($db);
$laporan = new laporan($db);

$data_excel = $laporan->load_report_detail_mhs(get('tgl_awal'), get('tgl_akhir'), get('fakultas'), get('prodi'), get('bank'));
if (get('fakultas') != '' || get('prodi') != '') {
    $fakultas = $list->get_fakultas(get('fakultas'));
    $prodi = $list->get_prodi(get('prodi'));
}
ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #2b1212;
    }
    td{
        text-align: left;
    }
</style>
<table width='950' border='1' align='left' cellpadding='0' cellspacing='0' class='keuangan'>
    <thead>
        <tr>
            <td colspan="9" class="header_text">
                <?php
                echo 'Rekapitulasi Pembayaran Mahasiswa<br/>';
                if ($fakultas != '') {
                    echo 'Fakultas ' . $fakultas['NM_FAKULTAS'] . '<br/>';
                }
                if ($prodi != '') {
                    echo 'Program Studi (' . $prodi['NM_JENJANG'] . ') ' . $prodi['NM_PROGRAM_STUDI'] . '<br/>';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td class="header_text">NO</td>
            <td class="header_text">NIM</td>
            <td class="header_text">NAMA</td>
            <td class="header_text">PRODI</td>
            <td class="header_text">JENJANG</td>
            <td class="header_text">JALUR</td>
            <td class="header_text">TGL BAYAR</td>
            <td class="header_text">BESAR BIAYA</td>
            <td class="header_text">DENDA BIAYA</td>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach ($data_excel as $data) {
            echo "<tr>
				<td>{$no}</td>
				<td>'{$data['NIM_MHS']}</td>
				<td>{$data['NM_PENGGUNA']}</td>
				<td>{$data['NM_PROGRAM_STUDI']}</td>
				<td>{$data['NM_JENJANG']}</td>
				<td>{$data['NM_JALUR']}</td>
				<td>{$data['TGL_BAYAR']}</td>
				<td align='right'>{$data['BIAYA']}</td>
				<td align='right'>{$data['DENDA']}</td>
			</tr>";
            $no++;
            $total_pembayaran = $total_pembayaran + $data['BIAYA'];
            $total_denda = $total_denda + $data['DENDA'];
        }
        ?>
        <tr>
            <td bgcolor='#CCFF99'></td>
            <td bgcolor='#CCFF99'></td>
            <td bgcolor='#CCFF99'></td>
            <td bgcolor='#CCFF99'></td>
            <td bgcolor='#CCFF99'></td>
            <td bgcolor='#CCFF99' colspan='2'><b>JUMLAH</b></td>
            <td bgcolor='#CCFF99' align='right'><?php echo $total_pembayaran; ?></td>
            <td bgcolor='#CCFF99' align='right'><?php echo $total_denda; ?></td>
        </tr>
    </tbody>
</table>

<?php
echo '<b>Data Ini Di Ambil Pada Tanggal ' . date('d F Y  H:i:s') . '</b>';

$nm_file = "detail-pembayaran-mhs-detail__" . date('d-m-Y');
header("Content-disposition: attachment; filename=$nm_file.xls");
header("Content-type: Application/exe");
header("Content-Transfer-Encoding: binary");
ob_flush();
?>
