<?php
include 'config.php';
include 'class/laporan.class.php';
include 'class/list_data.class.php';

$list = new list_data($db);
$laporan = new laporan($db);

$data_laporan = $laporan->load_report_fakultas_cmhs(get('tgl_awal'), get('tgl_akhir'), get('fakultas'), get('prodi'), get('bank'),get('jalur'));
$data_biaya = $list->load_biaya();
if (get('fakultas') != '' || get('prodi') != '') {
    $fakultas = $list->get_fakultas(get('fakultas'));
    $prodi = $list->get_prodi(get('prodi'));
    $jalur = $list->get_jalur(get('jalur'));
}
ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #2b1212;
    }
    td{
        text-align: left;
    }
</style>
<table width='950' border='1' align='left' cellpadding='0' cellspacing='0' class='keuangan'>
    <thead>
        <tr>
            <td colspan="<?php
if (get('fakultas') != ''){
    $colspan = count($data_biaya) + 5;}
else{
    $colspan=count($data_biaya) + 4;}
echo $colspan;
?>" class="header_text">
                <?php
                echo 'Rekapitulasi Pembayaran Mahasiswa<br/>';
                if ($fakultas != '') {
                    echo 'Fakultas ' . $fakultas['NM_FAKULTAS'] . '<br/>';
                }
                if ($prodi != '') {
                    echo 'Program Studi (' . $prodi['NM_JENJANG'] . ') ' . $prodi['NM_PROGRAM_STUDI'] . '<br/>';
                    echo 'Jalur ' . $jalur['NM_JALUR'] . '<br/>';
                }
                ?>
            </td>
        </tr>
        <tr>
            <th>No</th>
            <th>
                <?php
                if (get('prodi') != '') {
                    echo 'Nama';
                } else if (get('fakultas') != '') {
                    echo 'Program Studi ';
                } else {
                    echo 'Fakultas';
                }
                ?>
            </th>
            <?php if (get('prodi')) {
                ?>
                <th>No Ujian</th>
                <?php
            }
            if (get('prodi')!=''){
                echo '<th>Program Studi</th>';
            }
            else if (get('fakultas') != '') {
                echo '<th>Jalur</th>';
            }
            if (get('prodi') == '') {
                echo '<th>Jumlah Mhs</th>';
            }?>
            <?php
            foreach ($data_biaya as $data) {
                echo "<th>{$data['NM_BIAYA']}</th>";
            }
            ?>
            <th>Total Pembayaran</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        $total_pembayaran_bottom = array();
        foreach ($data_laporan as $data) {
            echo "<tr>
                        <td>{$no}</td>";
            if (get('prodi') != '') {
                echo "<td> {$data['NM_C_MHS']}</td>";
            } else if (get('fakultas') != '') {
                echo "<td>({$data['NM_JENJANG']}) {$data['NM_PROGRAM_STUDI']}</td>";
            } else {
                echo "<td>{$data['NM_FAKULTAS']}</td>";
            }
            if (get('prodi') != '') {
                echo "<td> '{$data['NO_UJIAN']}</td><td>{$data['NM_PROGRAM_STUDI']}</td>";
            } else if (get('fakultas') != '')
                echo "<td>{$data['NM_JALUR']}</td><td>{$data['JUMLAH_MHS']}</td>";
            else
                echo "<td>{$data['JUMLAH_MHS']}</td>";
            $total_mhs +=$data['JUMLAH_MHS'];
            $total_pembayaran_right = 0;
            $no_pembayaran = 0;
            foreach ($data['TOTAL_PEMBAYARAN'] as $data_pembayaran) {
                echo "<td>{$data_pembayaran['BESAR_BIAYA']}</td>";
                $total_pembayaran_right +=$data_pembayaran['BESAR_BIAYA'];
                $total_pembayaran_bottom[$no_pembayaran]+=$data_pembayaran['BESAR_BIAYA'];
                $no_pembayaran++;
            }
            $no++;
            echo "<td>{$total_pembayaran_right}</td>
                    </tr>";
        }
        ?>
        <tr>
            <td bgcolor='#CCFF99' colspan="<?php
        if (get('prodi') != '')
            $colspan = 4;
        else if (get('fakultas') != '')
            $colspan = 3;
        else
            $colspan=2;
        echo $colspan
        ?>">Jumlah</td>
            <?php if(get('prodi')=='') echo "<td bgcolor='#CCFF99'>".$total_mhs."</td>"; ?>
            <?php
            $no_total = 0;
            foreach ($data_biaya as $data) {
                echo "<td bgcolor='#CCFF99'>{$total_pembayaran_bottom[$no_total]}</td>";
                $no_total++;
            }
            ?>
            <td bgcolor='#CCFF99' align='right'><?php echo array_sum($total_pembayaran_bottom); ?></td>
        </tr>
    </tbody>
</table>

<?php
echo '<b>Data Ini Di Ambil Pada Tanggal ' . date('d F Y  H:i:s') . '</b>';

$nm_file = "detail-pembayaran-cmhs-fakultas__" . date('d-m-Y');
header("Content-disposition: attachment; filename=$nm_file.xls");
header("Content-type: Application/exe");
header("Content-Transfer-Encoding: binary");
ob_flush();
?>
