<?php

include 'config.php';
include '../keuangan/class/laporan.class.php';
include '../keuangan/class/list_data.class.php';
include '../keuangan/class/paging.class.php';

$laporan = new laporan($db);
$list = new list_data($db);

if (isset($_GET)) {
    if (get('mode') == 'laporan') {
        $semester_aktif = $laporan->get_semester_aktif();
        $semester_sebelum_aktif = $laporan->get_semester_sebelum_aktif();

        $semester_sebelum = "IN (" . $laporan->get_semester_sebelumnya() . ")";
        $semester_sekarang = "=" . $semester_aktif['ID_SEMESTER'];
        $semester_sebelum_sekarang = "=" . $semester_sebelum_aktif['ID_SEMESTER'];
        $semester_setelah = "IN (" . $laporan->get_semester_setelahnya() . ")";

        $semester = array($semester_sebelum, $semester_sebelum_sekarang, $semester_sekarang, $semester_setelah);
        $smarty->assign('semester_aktif', $semester_aktif);
        $smarty->assign('semester_sebelum_aktif', $semester_sebelum_aktif);
        $smarty->assign('semester', $semester);
        if (get('tampil') == 0) {
             $smarty->assign('data_laporan', $laporan->load_laporan_tahunan_perfakultas(get('tgl_awal'), get('tgl_akhir'), $semester));
        } elseif (get('tampil') == 1) {
            $smarty->assign('data_laporan', $laporan->load_laporan_tahunan_perprodi(get('fakultas'), get('tgl_awal'), get('tgl_akhir'), $semester));
        } else if (get('tampil') == 2) {
            $smarty->assign('data_laporan', $laporan->load_laporan_tahunan_perprodi_perjalur(get('fakultas'), get('tgl_awal'), get('tgl_akhir'), $semester));
        }
    } else if (get('mode') == 'detail') {
        
    }
}

$smarty->assign('tgl_awal', get('tgl_awal'));
$smarty->assign('tgl_akhir', get('tgl_akhir'));
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->display('laporan/laporan-perprodi.tpl');
?>
