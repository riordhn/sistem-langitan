<?php

class history {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    function get_data_mhs($nim) {
        if (is_numeric($nim) && strlen($nim) < 9) {
            $query = "M.ID_MHS='{$nim}'";
        } else {
            $query = "M.NIM_MHS='{$nim}'";
        }
        $this->db->Query("
            SELECT M.ID_MHS,M.NIM_MHS,M.NIM_LAMA,M.NIM_BANK,M.THN_ANGKATAN_MHS,M.STATUS_CEKAL,P.NM_PENGGUNA,PR.NM_PROGRAM_STUDI
                ,J.NM_JENJANG,JAL.NM_JALUR,KB.NM_KELOMPOK_BIAYA,F.NM_FAKULTAS,SP.NM_STATUS_PENGGUNA,CMB.NO_UJIAN
            FROM MAHASISWA M
            JOIN PENGGUNA P ON M.ID_PENGGUNA = P.ID_PENGGUNA
            JOIN PROGRAM_STUDI PR ON M.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG = PR.ID_JENJANG
            LEFT JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
            LEFT JOIN JALUR_MAHASISWA JM ON M.ID_MHS = JM.ID_MHS
            LEFT JOIN JALUR JAL ON JAL.ID_JALUR = JM.ID_JALUR
            LEFT JOIN SEMESTER S ON S.ID_SEMESTER = JM.ID_SEMESTER
            LEFT JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = M.ID_KELOMPOK_BIAYA
            LEFT JOIN CALON_MAHASISWA_BARU CMB ON CMB.NIM_MHS = M.NIM_MHS
            JOIN FAKULTAS F ON F.ID_FAKULTAS = PR.ID_FAKULTAS
            WHERE {$query}
            ORDER BY S.TAHUN_AJARAN DESC,S.NM_SEMESTER DESC");
        return $this->db->FetchAssoc();
    }

    function get_data_cmhs($no_ujian) {
        if (is_numeric($no_ujian)) {
            $query = "CMB.NO_UJIAN ='{$no_ujian}' OR CMB.ID_C_MHS='{$no_ujian}'";
        } else {
            $query = "CMB.NO_UJIAN ='{$no_ujian}'";
        }
        $this->db->Query("
            SELECT CMB.NM_C_MHS,CMB.NO_UJIAN,PS.NM_PROGRAM_STUDI,F.NM_FAKULTAS,J.NM_JENJANG,JAL.NM_JALUR,KB.NM_KELOMPOK_BIAYA 
            FROM CALON_MAHASISWA_BARU CMB
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CMB.ID_PROGRAM_STUDI
            JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
            JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            JOIN JALUR JAL ON JAL.ID_JALUR = CMB.ID_JALUR
            LEFT JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = CMB.ID_KELOMPOK_BIAYA
            WHERE {$query}
            ");
        return $this->db->FetchAssoc();
    }

    function get_history_bayar_mhs($id_mhs, $semester, $no_transaksi, $tgl_bayar, $keterangan) {
        $q_no_transaksi = $no_transaksi != '' ? "AND PEM.NO_TRANSAKSI='{$no_transaksi}'" : "AND PEM.NO_TRANSAKSI IS NULL";
        $q_tgl_bayar = $tgl_bayar != '' ? "AND TO_DATE(PEM.TGL_BAYAR)='{$tgl_bayar}'" : "AND PEM.TGL_BAYAR IS NULL";
        $q_keterangan = $keterangan != '' ? "AND PEM.KETERANGAN ='{$keterangan}'" : "AND PEM.KETERANGAN IS NULL";
        return $this->db->QueryToArray("
        SELECT PEM.ID_PEMBAYARAN,B.NM_BIAYA,PEM.BESAR_BIAYA,PEM.DENDA_BIAYA,PEM.TGL_BAYAR,BNK.NM_BANK,BV.NAMA_BANK_VIA,PEM.NO_TRANSAKSI,PEM.KETERANGAN,PEM.IS_TAGIH
        FROM PEMBAYARAN PEM
        JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
        JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
        LEFT JOIN BANK BNK ON BNK.ID_BANK=PEM.ID_BANK
        LEFT JOIN BANK_VIA BV ON BV.ID_BANK_VIA = PEM.ID_BANK_VIA
        WHERE PEM.ID_MHS='{$id_mhs}' AND PEM.ID_SEMESTER='{$semester}' {$q_tgl_bayar} {$q_no_transaksi} {$q_keterangan}
        ORDER BY PEM.ID_PEMBAYARAN");
    }

    function get_history_bayar_cmhs($id_cmhs, $semester, $no_transaksi, $tgl_bayar) {
        if ($no_transaksi != '' && $tgl_bayar != '') {
            $query = "AND (PEM.NO_TRANSAKSI='{$no_transaksi}' OR PEM.TGL_BAYAR ='{$tgl_bayar}')";
        } else {
            if ($no_transaksi != '') {
                $query = "AND PEM.NO_TRANSAKSI='{$no_transaksi}' AND PEM.TGL_BAYAR IS NULL";
            } else if ($tgl_bayar != '') {
                $query = "AND PEM.TGL_BAYAR ='{$tgl_bayar}' AND PEM.NO_TRANSAKSI IS NULL";
            } else {
                $query = "AND PEM.NO_TRANSAKSI IS NULL AND PEM.TGL_BAYAR IS NULL";
            }
        }
        return $this->db->QueryToArray("
        SELECT B.NM_BIAYA,PEM.BESAR_BIAYA,PEM.DENDA_BIAYA,PEM.TGL_BAYAR,BNK.NM_BANK,BV.NAMA_BANK_VIA,PEM.NO_TRANSAKSI,PEM.KETERANGAN 
        FROM PEMBAYARAN_CMHS PEM
        JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
        JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
        LEFT JOIN BANK BNK ON BNK.ID_BANK=PEM.ID_BANK
        LEFT JOIN BANK_VIA BV ON BV.ID_BANK_VIA = PEM.ID_BANK_VIA
        WHERE PEM.ID_C_MHS='{$id_cmhs}' AND PEM.ID_SEMESTER='{$semester}' {$query}
        ORDER BY PEM.ID_PEMBAYARAN_CMHS");
    }

    function load_history_bayar_cmhs($no_ujian) {
        $data_history_bayar = array();
        if (is_numeric($no_ujian)) {
            $query = "CMB.NO_UJIAN ='{$no_ujian}' OR CMB.ID_C_MHS='{$no_ujian}'";
        } else {
            $query = "CMB.NO_UJIAN ='{$no_ujian}'";
        }
        foreach ($this->db->QueryToArray("
            SELECT SUM(PEM.BESAR_BIAYA) JUMLAH_PEMBAYARAN,PEM.ID_C_MHS,S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN,S.THN_AKADEMIK_SEMESTER,B.NM_BANK,BV.NAMA_BANK_VIA,PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.KETERANGAN
            FROM PEMBAYARAN_CMHS PEM
            JOIN CALON_MAHASISWA_BARU CMB ON PEM.ID_C_MHS = CMB.ID_C_MHS
            JOIN SEMESTER S ON S.ID_SEMESTER = PEM.ID_SEMESTER
            LEFT JOIN BANK B ON B.ID_BANK=PEM.ID_BANK
            LEFT JOIN BANK_VIA BV ON BV.ID_BANK_VIA=PEM.ID_BANK_VIA
            WHERE {$query}
            GROUP BY PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,B.NM_BANK,BV.NAMA_BANK_VIA,PEM.KETERANGAN,S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN,S.THN_AKADEMIK_SEMESTER,PEM.ID_C_MHS
            ORDER BY S.TAHUN_AJARAN DESC,S.NM_SEMESTER DESC") as $data) {
            array_push($data_history_bayar, array(
                'ID_C_MHS' => $data['ID_C_MHS'],
                'ID_SEMESTER' => $data['ID_SEMESTER'],
                'NM_SEMESTER' => $data['NM_SEMESTER'],
                'TAHUN_AJARAN' => $data['TAHUN_AJARAN'],
                'JUMLAH_PEMBAYARAN' => $data['JUMLAH_PEMBAYARAN'],
                'NO_TRANSAKSI' => $data['NO_TRANSAKSI'],
                'TGL_BAYAR' => $data['TGL_BAYAR'],
                'NM_BANK' => $data['NM_BANK'],
                'NAMA_BANK_VIA' => $data['NAMA_BANK_VIA'],
                'KETERANGAN' => $data['KETERANGAN'],
                'DATA_PEMBAYARAN' => $this->get_history_bayar_cmhs($data['ID_C_MHS'], $data['ID_SEMESTER'], $data['NO_TRANSAKSI'], $data['TGL_BAYAR'])
            ));
        }
        return $data_history_bayar;
    }

    function load_history_bayar_mhs($nim) {
        $data_history_bayar = array();
        if (is_numeric($nim) && strlen($nim) < 9) {
            $query = "M.ID_MHS='{$nim}'";
        } else {
            $query = "M.NIM_MHS='{$nim}'";
        }
        foreach ($this->db->QueryToArray("
           SELECT SUM(PEM.BESAR_BIAYA+PEM.DENDA_BIAYA) JUMLAH_PEMBAYARAN,PEM.ID_MHS,S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN
                ,S.THN_AKADEMIK_SEMESTER,B.ID_BANK,B.NM_BANK,BV.ID_BANK_VIA,BV.NAMA_BANK_VIA,PEM.NO_TRANSAKSI,TO_CHAR(PEM.TGL_BAYAR,'DD-MON-YY') TGL_BAYAR,PEM.KETERANGAN,PEM.IS_TAGIH,SP.NAMA_STATUS,SP.ID_STATUS_PEMBAYARAN
           FROM PEMBAYARAN PEM 
           JOIN MAHASISWA M ON M.ID_MHS = PEM.ID_MHS
           JOIN SEMESTER S ON S.ID_SEMESTER = PEM.ID_SEMESTER
           LEFT JOIN BANK B ON B.ID_BANK=PEM.ID_BANK
           LEFT JOIN BANK_VIA BV ON BV.ID_BANK_VIA=PEM.ID_BANK_VIA
           LEFT JOIN STATUS_PEMBAYARAN SP ON SP.ID_STATUS_PEMBAYARAN = PEM.ID_STATUS_PEMBAYARAN 
           WHERE {$query}
           GROUP BY PEM.NO_TRANSAKSI,B.ID_BANK,B.NM_BANK,BV.ID_BANK_VIA,BV.NAMA_BANK_VIA,PEM.TGL_BAYAR,PEM.KETERANGAN,S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN,S.THN_AKADEMIK_SEMESTER,SP.ID_STATUS_PEMBAYARAN
           ,PEM.ID_MHS,PEM.IS_TAGIH,SP.NAMA_STATUS
           ORDER BY S.TAHUN_AJARAN DESC,S.NM_SEMESTER DESC") as $data) {
            array_push($data_history_bayar, array(
                'ID_MHS' => $data['ID_MHS'],
                'ID_SEMESTER' => $data['ID_SEMESTER'],
                'NM_SEMESTER' => $data['NM_SEMESTER'],
                'TAHUN_AJARAN' => $data['TAHUN_AJARAN'],
                'JUMLAH_PEMBAYARAN' => $data['JUMLAH_PEMBAYARAN'],
                'NO_TRANSAKSI' => $data['NO_TRANSAKSI'],
                'TGL_BAYAR' => $data['TGL_BAYAR'],
                'NM_BANK' => $data['NM_BANK'],
                'NAMA_BANK_VIA' => $data['NAMA_BANK_VIA'],
                'KETERANGAN' => $data['KETERANGAN'],
                'ID_BANK' => $data['ID_BANK'],
                'ID_BANK_VIA' => $data['ID_BANK_VIA'],
                'IS_TAGIH' => $data['IS_TAGIH'],
                'NAMA_STATUS'=>$data['NAMA_STATUS'],
                'ID_STATUS_PEMBAYARAN'=>$data['ID_STATUS_PEMBAYARAN'],
                'DATA_PEMBAYARAN' => $this->get_history_bayar_mhs($data['ID_MHS'], $data['ID_SEMESTER'], $data['NO_TRANSAKSI'], $data['TGL_BAYAR'], $data['KETERANGAN'])
            ));
        }
        return $data_history_bayar;
    }

    function load_detail_biaya($nim) {
        return $this->db->QueryToArray("
            SELECT DB.ID_DETAIL_BIAYA,B.NM_BIAYA FROM BIAYA_KULIAH_MHS BKM
            JOIN BIAYA_KULIAH BK ON BK.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH
            JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BK.ID_BIAYA_KULIAH
            JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
            JOIN MAHASISWA M ON M.ID_MHS = BKM.ID_MHS
            WHERE M.NIM_MHS='{$nim}'");
    }

    function insert_detail_biaya($cari, $id_detail_biaya, $id_semester) {
        $this->db->Query("SELECT ID_MHS FROM MAHASISWA WHERE NIM_MHS='{$cari}'");
        $mhs = $this->db->FetchAssoc();

        $this->db->Query("INSERT INTO PEMBAYARAN (ID_MHS,ID_SEMESTER,ID_DETAIL_BIAYA,BESAR_BIAYA,IS_TAGIH) VALUES ('{$mhs['ID_MHS']}','{$id_semester}','$id_detail_biaya','0','Y')");
    }

    function update_pembayaran_mhs($cari, $semester, $semester_asli, $tgl_bayar, $tgl_bayar_asli, $bank, $bank_asli, $bank_via, $bank_via_asli, $no_transaksi, $no_transaksi_asli, $keterangan, $keterangan_asli, $is_tagih, $is_tagih_asli,$status) {
        if (is_numeric($nim) && strlen($nim) < 9) {
            $query = "AND ID_MHS='{$cari}'";
        } else {
            $this->db->Query("SELECT ID_MHS FROM MAHASISWA WHERE NIM_MHS='{$cari}'");
            $mhs = $this->db->FetchAssoc();
            $query = "AND ID_MHS='{$mhs['ID_MHS']}'";
        }
        $q_tgl_bayar = $tgl_bayar_asli == '' ? 'TGL_BAYAR IS NULL' : "TO_DATE(TGL_BAYAR)='{$tgl_bayar_asli}' ";
        $q_bank = $bank_asli == '' ? "ID_BANK IS NULL" : "ID_BANK='{$bank_asli}'";
        $q_bank_via = $bank_via_asli == '' ? "ID_BANK_VIA IS NULL" : "ID_BANK_VIA='{$bank_via_asli}'";
        $q_no_transaksi = $no_transaksi_asli == '' ? "NO_TRANSAKSI IS NULL" : "NO_TRANSAKSI ='{$no_transaksi_asli}'";
        $q_keterangan = $keterangan_asli == '' ? "KETERANGAN IS NULL" : "KETERANGAN='{$keterangan_asli}'";
        $this->db->Query("
          UPDATE PEMBAYARAN SET 
                ID_SEMESTER='{$semester}',
                IS_TAGIH='{$is_tagih}',
                TGL_BAYAR='{$tgl_bayar}',
                ID_BANK='{$bank}',
                ID_BANK_VIA='{$bank_via}',
                NO_TRANSAKSI='{$no_transaksi}',
                KETERANGAN='{$keterangan}',
                ID_STATUS_PEMBAYARAN='{$status}'
            WHERE ID_SEMESTER='{$semester_asli}' 
                AND {$q_tgl_bayar}
                AND {$q_bank} 
                AND {$q_bank_via}
                AND {$q_no_transaksi}
                AND {$q_keterangan}
                AND IS_TAGIH='{$is_tagih_asli}'
                {$query}");
    }

    function hapus_pembayaran_mhs($cari, $id_semester, $id_bank, $id_bank_via, $tgl_bayar, $no_transaksi, $keterangan, $is_tagih, $id_pengguna) {
        $this->backup_pembayaran_dihapus($cari, $id_semester, $id_bank, $id_bank_via, $tgl_bayar, $no_transaksi, $keterangan, $is_tagih, $id_pengguna);
        if (is_numeric($nim) && strlen($nim) < 9) {
            $query = "AND ID_MHS='{$cari}'";
        } else {
            $this->db->Query("SELECT ID_MHS FROM MAHASISWA WHERE NIM_MHS='{$cari}'");
            $mhs = $this->db->FetchAssoc();
            $query = "AND ID_MHS='{$mhs['ID_MHS']}'";
        }
        $q_tgl_bayar = $tgl_bayar == '' ? 'TGL_BAYAR IS NULL' : "TO_DATE(TGL_BAYAR)='{$tgl_bayar}' ";
        $q_bank = $id_bank == '' ? "ID_BANK IS NULL" : "ID_BANK='{$id_bank}'";
        $q_bank_via = $id_bank_via == '' ? "ID_BANK_VIA IS NULL" : "ID_BANK_VIA='{$id_bank_via}'";
        $q_no_transaksi = $no_transaksi == '' ? "NO_TRANSAKSI IS NULL" : "NO_TRANSAKSI ='{$no_transaksi}'";
        $q_keterangan = $keterangan == '' ? "KETERANGAN IS NULL" : "KETERANGAN='{$keterangan}'";

        $this->db->Query("
          DELETE FROM PEMBAYARAN
            WHERE ID_SEMESTER='{$id_semester}' 
                AND {$q_tgl_bayar}
                AND {$q_bank} 
                AND {$q_bank_via}
                AND {$q_no_transaksi}
                AND {$q_keterangan}
                AND IS_TAGIH='{$is_tagih}'
                {$query}");
    }

    function backup_pembayaran_dihapus($cari, $id_semester, $id_bank, $id_bank_via, $tgl_bayar, $no_transaksi, $keterangan, $is_tagih, $id_pengguna) {
        if (is_numeric($nim) && strlen($nim) < 9) {
            $query = "AND ID_MHS='{$cari}'";
        } else {
            $this->db->Query("SELECT ID_MHS FROM MAHASISWA WHERE NIM_MHS='{$cari}'");
            $mhs = $this->db->FetchAssoc();
            $query = "AND ID_MHS='{$mhs['ID_MHS']}'";
        }
        $q_tgl_bayar = $tgl_bayar == '' ? 'TGL_BAYAR IS NULL' : "TO_DATE(TGL_BAYAR)='{$tgl_bayar}' ";
        $q_bank = $id_bank == '' ? "ID_BANK IS NULL" : "ID_BANK='{$id_bank}'";
        $q_bank_via = $id_bank_via == '' ? "ID_BANK_VIA IS NULL" : "ID_BANK_VIA='{$id_bank_via}'";
        $q_no_transaksi = $no_transaksi == '' ? "NO_TRANSAKSI IS NULL" : "NO_TRANSAKSI ='{$no_transaksi}'";
        $q_keterangan = $keterangan == '' ? "KETERANGAN IS NULL" : "KETERANGAN='{$keterangan}'";
        $this->db->Query("
          INSERT INTO PEMBAYARAN_DIHAPUS (ID_MHS,ID_SEMESTER,ID_BANK,ID_BANK_VIA,TGL_BAYAR,NO_TRANSAKSI,KETERANGAN,BESAR_BIAYA,ID_PENGHAPUS)
            (SELECT ID_MHS,ID_SEMESTER,ID_BANK,ID_BANK_VIA,TGL_BAYAR,NO_TRANSAKSI,KETERANGAN,BESAR_BIAYA, '{$id_pengguna}' AS ID_PENGHAPUS FROM PEMBAYARAN
            WHERE ID_SEMESTER='{$id_semester}' 
                AND {$q_tgl_bayar}
                AND {$q_bank} 
                AND {$q_bank_via}
                AND {$q_no_transaksi}
                AND {$q_keterangan}
                AND IS_TAGIH='{$is_tagih}'
                {$query})");
    }

    function update_detail_pembayaran($id_pembayaran, $besar_biaya, $denda_biaya) {
        $this->db->Query("UPDATE PEMBAYARAN SET BESAR_BIAYA='{$besar_biaya}',DENDA_BIAYA='{$denda_biaya}' WHERE ID_PEMBAYARAN='{$id_pembayaran}'");
    }

}

?>
