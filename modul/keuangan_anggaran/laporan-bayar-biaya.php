<?php

include 'config.php';
include '../keuangan/class/laporan.class.php';
include '../keuangan/class/list_data.class.php';
include '../keuangan/class/paging.class.php';

$laporan = new laporan($db);
$list = new list_data($db);

if (isset($_GET)) {
    if (get('mode') == 'bank') {
        $smarty->assign('data_pew_bank', $laporan->load_laporan_bayar_perbiaya_bank(get('tgl_awal'), get('tgl_akhir'), get('biaya')));
    } else if (get('mode') == 'detail') {
        $smarty->assign('data_bank_one', $list->get_bank(get('bank')));
        $smarty->assign('data_fakultas_one', $list->get_fakultas(get('fakultas')));
        $smarty->assign('data_pew_detail', $laporan->load_laporan_bayar_perbiaya_detail(get('tgl_awal'), get('tgl_akhir'), get('biaya'), get('bank'), get('fakultas')));
    } else if (get('mode') == 'fakultas') {
        $smarty->assign('data_bank_one', $list->get_bank(get('bank')));
        $smarty->assign('data_pew_fakultas', $laporan->load_laporan_bayar_perbiaya_fakultas(get('tgl_awal'), get('tgl_akhir'), get('biaya'), get('bank')));
    }
}

$smarty->assign('tgl_awal', get('tgl_awal'));
$smarty->assign('tgl_akhir', get('tgl_akhir'));
$smarty->assign('data_biaya',$list->load_biaya());
$smarty->assign('bank', get('bank'));
$smarty->assign('data_periode_wisuda', $laporan->load_periode_wisuda());
$smarty->display('laporan/laporan-bayar-biaya.tpl');
?>
