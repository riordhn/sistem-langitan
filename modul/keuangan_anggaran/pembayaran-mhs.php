<?php

include 'config.php';
include '../keuangan/class/laporan.class.php';
include 'class/list_data.class.php';
include 'class/paging.class.php';

$laporan = new laporan($db);
$list = new list_data($db);
$paging = new paging('pembayaran-mhs.php');

//Update Is Tagih Pembayaran Manual
$db->Query("UPDATE AUCC.PEMBAYARAN SET IS_TAGIH='T' WHERE IS_TAGIH='Y' AND ID_BANK IS NOT NULL AND TGL_BAYAR IS NOT NULL AND ID_STATUS_PEMBAYARAN=1");

if (isset($_GET['mode'])) {
    if (get('mode') == 'bank') {
        if (get('fakultas') != '' || get('prodi') != '') {
            $smarty->assign('data_prodi', $list->load_list_prodi(get('fakultas')));
            $smarty->assign('data_fakultas_one', $list->get_fakultas(get('fakultas')));
            $smarty->assign('data_prodi_one', $list->get_prodi(get('prodi')));
        }
        $smarty->assign('data_report_bank', $laporan->load_report_bank_mhs(get('tgl_awal'), get('tgl_akhir'), get('fakultas'), get('prodi')));
    } else if (get('mode') == 'detail') {
        // pendefinisan untuk paging
        $batas = 100;
        $posisi = $paging->cari_posisi($batas);
        $jml_data = count($laporan->load_report_detail_mhs(get('tgl_awal'), get('tgl_akhir'), get('fakultas'), get('prodi'), get('bank'),get('jalur')));
        $jml_halaman = $paging->jumlah_halaman($jml_data, $batas);
        $link_halaman = $paging->nav_halaman(get('page'), $jml_halaman);
        if (get('fakultas') != '' || get('prodi') != '') {
            $smarty->assign('data_prodi', $list->load_list_prodi(get('fakultas')));
            $smarty->assign('data_fakultas_one', $list->get_fakultas(get('fakultas')));
            $smarty->assign('data_prodi_one', $list->get_prodi(get('prodi')));
        }
        $smarty->assign('data_report_detail_mhs', $laporan->load_report_detail_mhs_page(get('tgl_awal'), get('tgl_akhir'), get('fakultas'), get('prodi'), get('bank'),get('jalur'), $batas, $posisi));
        $smarty->assign('link_halaman', $link_halaman);
    } else if (get('mode') == 'fakultas') {
        if (get('fakultas') != '' || get('prodi') != '') {
            $smarty->assign('data_jalur_one',$list->get_jalur(get('jalur')));
            $smarty->assign('data_prodi', $list->load_list_prodi(get('fakultas')));
            $smarty->assign('data_fakultas_one', $list->get_fakultas(get('fakultas')));
            $smarty->assign('data_prodi_one', $list->get_prodi(get('prodi')));
        }
        $smarty->assign('count_data_biaya', count($list->load_biaya()));
        $smarty->assign('data_biaya', $list->load_biaya());
        $smarty->assign('data_report_fakultas', $laporan->load_report_fakultas_mhs(get('tgl_awal'), get('tgl_akhir'), get('fakultas'), get('prodi'),get('bank'),get('jalur'),get('angkatan')));
    }
}

$smarty->assign('tgl_awal', get('tgl_awal'));
$smarty->assign('tgl_akhir', get('tgl_akhir'));
$smarty->assign('fakultas', get('fakultas'));
$smarty->assign('prodi', get('prodi'));
$smarty->assign('bank', get('bank'));
$smarty->assign('jalur', get('jalur'));
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->assign('data_jalur', $list->load_list_jalur());
$smarty->display('laporan/pembayaran-mhs.tpl');
?>
