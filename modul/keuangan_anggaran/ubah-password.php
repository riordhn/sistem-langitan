<?php
include('config.php');

if ($request_method == 'POST')
{
    if (post('mode') == 'change-password')
    {
        
        if (post('password_baru') == post('password_baru2'))
        {
            $hasil = $user->ChangePassword(post("password_lama"), post("password_baru"));
            if($hasil == true)
            {
                $smarty->assign('result','Password berhasil dirubah');
            }
            else
            {
                $smarty->assign('result','Password gagal dirubah');
            }
        }
        else
        {
            $smarty->assign('result','Password baru yang diulang tidak sama');
        }
     
    }
    
    if (post('mode') == 'change-role')
    {
        $id_pengguna = post('id_pengguna');
        $id_role = post('id_role');
        
        $id_template_role = $db->QuerySingle("select id_template_role from role_pengguna where id_pengguna = {$id_pengguna} and id_role = {$id_role}");
        
        $db->Parse("update pengguna set id_role = :id_role, id_template_role = :id_template_role where id_pengguna = :id_pengguna");
        $db->BindByName(':id_role', $id_role);
        $db->BindByName(':id_template_role', $id_template_role);
        $db->BindByName(':id_pengguna', $id_pengguna);
        $result = $db->Execute();
        
        echo ($result) ? "1" : "0";
        
        exit();
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    $smarty->assign('id_pengguna', $user->ID_PENGGUNA);
    $smarty->assign('id_role', $user->ID_ROLE);
    $smarty->assign('role_set', $db->QueryToArray("select * from role where id_role in (select id_role from role_pengguna where id_pengguna = {$user->ID_PENGGUNA})"));
}

$smarty->display('biodata/ubah_password/view.tpl');
?>