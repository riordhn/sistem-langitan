<div class="center_title_bar">Laporan Anggaran Data Mahasiswa Aktif</div> 
<form method="get" id="report_form" action="data-mahasiswa.php">
    <table class="ui-widget" style="width:90%;">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Fakultas</td>
            <td>
                <select name="fakultas" id="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
            <td>Jenis Laporan</td>
            <td>
                <select name="jenis">
                    <option value="1" {if $smarty.get.jenis==1}selected="true"{/if}>Berdasarkan Tarif</option>
                    <option value="2" {if $smarty.get.jenis==2}selected="true"{/if}>Berdasarkan Mahasiswa Aktif</option>
                </select>
            </td>
        </tr>
        <tr class="center ui-widget-content">
            <td colspan="4">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" name="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>
    </table>
</form>
{if isset($data_mahasiswa)}
    {if $smarty.get.mode=='detail'}
        <table class="ui-widget-content" style="width: 90%">
            <tr class="ui-widget-header">
                <th colspan="13" class="header-coloumn">
                    Daftar Mahasiswa<br/>
                    {if isset($data_fakultas_one)}
                        FAKULTAS {$data_fakultas_one.NM_FAKULTAS|upper} <br/>
                    {/if}
                    {if isset($data_prodi_one)}
                        PROGRAM STUDI {$data_prodi_one.NM_JENJANG} {$data_prodi_one.NM_PROGRAM_STUDI|upper} <br/>
                    {/if}
                    Jumlah Mahasiswa : {count($data_mahasiswa)}<br/>
                </th>
            </tr>
            <tr class="ui-widget-header">
                <th>No</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Jenjang</th>
                <th>Program Studi</th>
                <th>Fakultas</th>
                <th>Jalur</th>
                <th>Status</th>
                <th>SOP</th>
                <th>SP3</th>
                <th>Matrik</th>
                <th>Praktikum</th>
                <th>Detail</th>
            </tr>
            {$total_sop=0}
            {$total_sp3=0}
            {$total_matrik=0}
            {$total_praktikum=0}
            {foreach $data_mahasiswa as $data}
                {$total_sop=$total_sop+$data.SOP}
                {$total_sp3=$total_sp3+$data.SP3}
                {$total_matrik=$total_matrik+$data.MATRIK}
                {$total_praktikum=$total_praktikum+$data.PRATIKUM}
                <tr>
                    <td>{$data@index+1}</td>
                    <td><a href="history-bayar.php?cari={$data.NIM_MHS}">{$data.NIM_MHS}</a></td>
                    <td>{$data.NM_PENGGUNA}</td>
                    <td>{$data.NM_JENJANG}</td>
                    <td>{$data.NM_PROGRAM_STUDI|upper}</td>
                    <td>{$data.NM_FAKULTAS|upper}</td>
                    <td>{$data.NM_JALUR}</td>
                    <td>{$data.NM_STATUS_PENGGUNA}</td>
                    <td>{number_format($data.SOP)}</td>
                    <td>{number_format($data.SP3)}</td>
                    <td>{number_format($data.MATRIK)}</td>
                    <td>{number_format($data.PRAKTIKUM)}</td>
                    <td class="center">
                        <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="history-bayar.php?cari={$data.NIM_MHS}">Detail</a>
                    </td>
                </tr>
            {foreachelse}
                <tr class="ui-widget-content">
                    <td colspan="13" class="center"><span style="color: red">Data Kosong</span></td>
                </tr>                
            {/foreach}
            <tr class="total">
                <td colspan="13" class="center">
                    TOTAL SOP : {number_format($total_sop)}<br/>
                    TOTAL SP3 : {number_format($total_sp3)}<br/>
                    TOTAL MATRIK : {number_format($total_matrik)}<br/>
                    TOTAL PRAKTIKUM : {number_format($total_praktikum)}<br/>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td class="link center" colspan="13">
                    <span class="link_button ui-corner-all" onclick="window.location.href='excel-data-mahasiswa.php?{$smarty.server.QUERY_STRING}'" style="padding:5px;cursor:pointer;">Excel</span>
                </td>	
            </tr>
        </table>
    {else}
        <table class="ui-widget-content" style="width: 90%">
            <tr class="ui-widget-header">
                <th colspan="13" class="header-coloumn">
                    Daftar Mahasiswa<br/>
                    {if isset($data_fakultas_one)}
                        FAKULTAS {$data_fakultas_one.NM_FAKULTAS|upper} <br/>
                    {/if}
                    {if isset($data_prodi_one)}
                        PROGRAM STUDI {$data_prodi_one.NM_JENJANG} {$data_prodi_one.NM_PROGRAM_STUDI|upper} <br/>
                    {/if}
                </th>
            </tr>
            <tr class="ui-widget-header">
                <th>No</th>
                <th>Fakultas</th>
                <th>Jenjang</th>
                <th>Program Studi</th>
                <th>Angkatan</th>
                <th>Jalur</th>
                <th>Kelompok Biaya</th>
                <th>Jumlah Mahasiswa</th>
                <th>SOP</th>
                <th>SP3</th>
                <th>MATRIK</th>
                <th>PRAKTIKUM</th>
                <th>Detail</th>
            </tr>
            {$total_sop=0}
            {$total_sp3=0}
            {$total_matrik=0}
            {$total_praktikum=0}
            {$total_mhs=0}
            {$total_kelompok_kosong=0}
            {foreach $data_mahasiswa as $data}
                {$total_sop=$total_sop+$data.SOP}
                {$total_sp3=$total_sp3+$data.SP3}
                {$total_matrik=$total_matrik+$data.MATRIK}
                {$total_praktikum=$total_praktikum+$data.PRATIKUM}
                {$total_mhs=$total_mhs+$data.JUMLAH_MHS}
                <tr {if $smarty.get.jenis==2 and $data.NM_KELOMPOK_BIAYA==''}{$total_kelompok_kosong=$total_kelompok_kosong+$data.JUMLAH_MHS}style="background-color: #eab8b8"{/if}>
                    <td>{$data@index+1}</td>
                    <td>{$data.NM_FAKULTAS|upper}</td>
                    <td>{$data.NM_JENJANG}</td>
                    <td>{$data.NM_PROGRAM_STUDI}</td>
                    <td>
                        {if $smarty.get.jenis=='1'}
                            {$data.THN_AKADEMIK_SEMESTER}
                        {else}
                            {$data.THN_ANGKATAN_MHS}
                        {/if}
                    </td>
                    <td>{$data.NM_JALUR}</td>
                    <td>
                        {if $data.NM_KELOMPOK_BIAYA!=''}
                            {$data.NM_KELOMPOK_BIAYA}
                        {else}
                            <span class="data-kosong">Tidak Ada</span>
                        {/if}
                    </td>
                    <td>{$data.JUMLAH_MHS}</td>
                    <td>{number_format($data.SOP)}</td>
                    <td>{number_format($data.SP3)}</td>
                    <td>{number_format($data.MATRIK)}</td>
                    <td>{number_format($data.PRAKTIKUM)}</td>
                    <td class="center">
                        {if $smarty.get.jenis==1}
                            <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="data-mahasiswa.php?fakultas={$smarty.get.fakultas}&biaya_kuliah={$data.ID_BIAYA_KULIAH}&mode=detail">Detail</a>
                        {else}
                            <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="data-mahasiswa.php?fakultas={$smarty.get.fakultas}&kelompok_biaya={$data.NM_KELOMPOK_BIAYA}&prodi={$data.ID_PROGRAM_STUDI}&jalur={$data.ID_JALUR}&angkatan={$data.THN_ANGKATAN_MHS}&jenis={$smarty.get.jenis}&mode=detail">Detail</a>
                        {/if}
                    </td>
                </tr>
            {foreachelse}
                <tr class="ui-widget-content">
                    <td colspan="13" class="center"><span style="color: red">Data Kosong</span></td>
                </tr>                
            {/foreach}
            <tr>
                <td colspan="13" class="total center">    
                    <span class="data-kosong">TOTAL MHS YANG TIDAK PUNYA KELOMPOK BIAYA :{$total_kelompok_kosong}</span><br/>
                    TOTAL MHS : {number_format($total_mhs)}<br/>
                    TOTAL SOP : {number_format($total_sop)}<br/>
                    TOTAL SP3 : {number_format($total_sp3)}<br/>
                    TOTAL MATRIK : {number_format($total_matrik)}<br/>
                    TOTAL PRAKTIKUM : {number_format($total_praktikum)}<br/>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td class="link center" colspan="13">
                    <span class="link_button ui-corner-all" onclick="window.location.href='excel-data-mahasiswa.php?{$smarty.server.QUERY_STRING}'" style="padding:5px;cursor:pointer;">Excel</span>
                </td>	
            </tr>
        </table>
    {/if}
{/if}
{literal}
    <script>
            
            $('#fakultas').change(function(){
                    $.ajax({
                            type:'post',
                            url:'getProdi.php',
                            data:'id_fakultas='+$(this).val(),
                            success:function(data){
                                    $('#prodi').html(data);
                            }                    
                    })
            });
    </script>
{/literal}