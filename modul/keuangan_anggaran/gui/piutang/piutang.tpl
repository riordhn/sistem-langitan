{if isset($smarty.get.mode)}
    <link rel="stylesheet" type="text/css" href="../../css/jquery-ui-custom-keu.css" />
    <link rel="stylesheet" type="text/css" href="../../css/keuangan_tab.css" />
    <script language="javascript" src="../../js/jquery-1.5.1.min.js"></script>
    <script language="javascript" src="../../js/jquery.validate.js"></script>
    <script language="javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
    <style>
        .center{
            text-align: center;
        }
    </style>
    <form method="get" id="report_form" action="piutang.php">
        <table class="ui-widget" style="width:90%;">
            <tr class="ui-widget-header">
                <th colspan="6" class="header-coloumn">Parameter</th>
            </tr>
            <tr class="ui-widget-content">
                <td width="15%">Fakultas</td>
                <td width="25%">
                    <select name="fakultas" id="fakultas">
                        {foreach $data_fakultas as $data}
                            {if $data.ID_FAKULTAS==$id_fakultas}
                                <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                            {/if}
                        {/foreach}
                    </select>
                </td>
                <td  width="15%">Program Studi</td>
                <td width="45%">
                    <select name="prodi" id="prodi">
                        <option value="">Semua</option>
                        {foreach $data_prodi as $data}
                            <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$prodi}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
                        {/foreach}
                    </select>
                </td>
                <td  width="15%">Status Mahasiswa</td>
                <td width="45%">
                    <select name="status">
                        <option value="">Semua</option>
                        {foreach $data_status as $data}
                            <option value="{$data.ID_STATUS_PENGGUNA}" {if $data.ID_STATUS_PENGGUNA==$status}selected="true"{/if}>{$data.NM_STATUS_PENGGUNA}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr class="center ui-widget-content">
                <td colspan="6">
                    <input type="hidden" name="mode" value="new_page" />
                    <input type="submit" name="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
                </td>
            </tr>

        </table>
    </form>
{else}
    <div class="center_title_bar">Laporan Piutang Mahasiswa</div>
    <span class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="window.open('piutang.php?mode=new_page','_blank')">Buka Halaman</span>
{/if}
{if isset($data_piutang)}
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="{$data_semester|@count+7}" class="header-coloumn">Laporan Piutang Mahasiswa</th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>NIM</th>
            <th>NAMA</th>
            <th>PROGRAM STUDI</th>
            <th>STATUS</th>
            {foreach $data_semester as $data}
                <th>{$data['NM_SEMESTER']} <br/>({$data['TAHUN_AJARAN']})</th>
                {/foreach}
            <th>Σx Semester Piutang</th>
            <th>Piutang Mahasiswa</th>
        </tr>
        {foreach $data_piutang as $data}
            <tr class="ui-widget-content"{if $data.ID_STATUS==14||$data.ID_STATUS==15||$data.ID_STATUS==10} style="background-color: lightpink"{/if}>
                <td class="center">{$data@index+1}</td>
                <td class="center">{$data.NIM_MHS}</td>
                <td class="center">{$data.NM_PENGGUNA}</td>
                <td class="center">{$data.PRODI}</td>
                <td class="center">{$data.STATUS}</td>
                {$piutang=0}
                {$piutang_semester=0}
                {foreach $data.DATA_PIUTANG as $piutang_mhs}
                    <td class="center" {if $piutang_mhs.TOTAL_BIAYA==''}style="background-color: white"{/if}>
                        {if $piutang_mhs.TOTAL_BIAYA!=''}
                            {if $piutang_mhs.TOTAL_BIAYA<0}
                                <font style="color: red">{number_format($piutang_mhs.TOTAL_BIAYA*-1)}</font>
                                {$piutang=$piutang+($piutang_mhs.TOTAL_BIAYA*-1)}
                                {$piutang_semester=$piutang_semester+1}
                            {else if $piutang_mhs.TOTAL_BIAYA==''}
                                -
                            {else}
                                {number_format($piutang_mhs.TOTAL_BIAYA)}
                            {/if} 
                        {/if}
                    </td>
                {/foreach}
                <td class="center total">{$piutang_semester}</td>
                <td class="center total">{number_format($piutang)}</td>
                {$total_piutang = $total_piutang+$piutang}
            </tr>
        {/foreach}
        <tr class="total center ui-widget-content">
            <td colspan="{$data_semester|@count+6}">TOTAL PIUTANG</td>
            <td>{number_format($total_piutang)}</td>
        </tr>
        <tr class="center ui-widget-content">
            <td class="link center" colspan="{$data_semester|@count+7}">
                <a class="link_button ui-corner-all" style="margin-top:10px;padding:5px;cursor:pointer;color: white;text-decoration: none" href="excel-piutang.php?fakultas={$fakultas}&prodi={$prodi}&status={$status}">Excel</a>
            </td>	
        </tr>

    </table>
{/if}
{literal}
    <script>
            $('#fakultas').change(function(){
                    $.ajax({
                            type:'post',
                            url:'getProdi.php',
                            data:'id_fakultas='+$(this).val(),
                            success:function(data){
                                    $('#prodi').html(data);
                            }                    
                    })
            });
    </script>
{/literal}