<div class="center_title_bar">History Pembayaran Mahasiswa</div>
<form method="get" id="form" action="history-bayar.php">
    <div id="alert" class="ui-widget" style="display:none;margin-left:15px;">
        <div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;"> 
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                Data Harus Diisi.</p>
        </div>
    </div>
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Input Form</th>
        </tr>
        <tr class="ui-widget-content">
            <td>NIM/NO Ujian Mahasiswa</td>
            <td><input id="cari" type="text" name="cari" value="{if isset($smarty.get.cari)}{$smarty.get.cari}{/if}"/></td>
            <td><input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" value="Tamplikan"/></td>
        </tr>
    </table>
</form> 
{if isset($data_detail_mahasiswa)||isset($data_detail_calon_mahasiswa)}
    {if $data_detail_mahasiswa!=''||$data_detail_calon_mahasiswa!=''}
        <table style="width: 400px;" class="ui-widget">
            <tr class="ui-widget-header">
                <th colspan="2" class="header-coloumn" style="text-align: center">Detail Biodata {if $jenis=='cmhs'} Calon {/if}Mahasiswa</th>
            </tr>
            {if $jenis=='mhs'}
                <tr class="ui-widget-content">
                    <td>NIM</td>
                    <td width="200px">{$data_detail_mahasiswa.NIM_MHS}</td>
                </tr>
                <tr class="ui-widget-content">
                    <td>NIM BANK</td>
                    <td>{$data_detail_mahasiswa.NIM_BANK}</td>
                </tr>
                <tr class="ui-widget-content">
                    <td>NIM LAMA</td>
                    <td>{$data_detail_mahasiswa.NIM_LAMA}</td>
                </tr>
                <tr class="ui-widget-content">
                    <td>No Ujian</td>
                    <td>{$data_detail_mahasiswa.NO_UJIAN}</td>
                </tr>
                <tr class="ui-widget-content">
                    <td>Nama</td>
                    <td>{$data_detail_mahasiswa.NM_PENGGUNA}</td>
                </tr>
                <tr class="ui-widget-content">
                    <td>Fakultas</td>
                    <td>{$data_detail_mahasiswa.NM_FAKULTAS}</td>
                </tr>
                <tr class="ui-widget-content">
                    <td>Program Studi</td>
                    <td>{$data_detail_mahasiswa.NM_PROGRAM_STUDI}</td>
                </tr>
                <tr class="ui-widget-content">
                    <td>Jenjang</td>
                    <td>{$data_detail_mahasiswa.NM_JENJANG}</td>
                </tr>
                <tr class="ui-widget-content">
                    <td>Jalur</td>
                    <td>{$data_detail_mahasiswa.NM_JALUR}</td>
                </tr>
                <tr class="ui-widget-content">
                    <td>Angkatan Akademik Mahasiswa</td>
                    <td>{$data_detail_mahasiswa.THN_ANGKATAN_MHS}</td>
                </tr>
                <tr class="ui-widget-content">
                    <td>Kelompok Biaya</td>
                    <td>{$data_detail_mahasiswa.NM_KELOMPOK_BIAYA}</td>
                </tr>
                <tr class="ui-widget-content">
                    <td>Status Cekal</td>
                    <td>{$data_detail_mahasiswa.STATUS_CEKAL}</td>
                </tr>
                <tr class="ui-widget-content">
                    <td>Status Akademik</td>
                    <td>{$data_detail_mahasiswa.NM_STATUS_PENGGUNA}</td>
                </tr>
            {else}
                <tr class="ui-widget-content">
                    <td>No Ujian</td>
                    <td width="200px">{$data_detail_calon_mahasiswa.NO_UJIAN}</td>
                </tr>
                <tr class="ui-widget-content">
                    <td>Nama</td>
                    <td>{$data_detail_calon_mahasiswa.NM_C_MHS}</td>
                </tr>
                <tr class="ui-widget-content">
                    <td>Fakultas</td>
                    <td>{$data_detail_calon_mahasiswa.NM_FAKULTAS}</td>
                </tr>
                <tr class="ui-widget-content">
                    <td>Program Studi</td>
                    <td>{$data_detail_calon_mahasiswa.NM_PROGRAM_STUDI}</td>
                </tr>
                <tr class="ui-widget-content">
                    <td>Jenjang</td>
                    <td>{$data_detail_calon_mahasiswa.NM_JENJANG}</td>
                </tr>
                <tr class="ui-widget-content">
                    <td>Jalur</td>
                    <td>{$data_detail_calon_mahasiswa.NM_JALUR}</td>
                </tr>
                <tr class="ui-widget-content">
                    <td>Kelompok Biaya</td>
                    <td>{$data_detail_calon_mahasiswa.NM_KELOMPOK_BIAYA}</td>
                </tr>
            {/if}
        </table>
        <p></p>
        <form action="history-bayar.php?cari={$smarty.get.cari}" method="post">
            <table class="ui-widget" width="90%">
                <tr class="ui-widget-header">
                    <th colspan="11">DETAIL PEMBAYARAN {if $jenis=='cmhs'} CALON {/if} MAHASISWA</th>
                </tr>
                <tr class="ui-widget-header">
                    <th>No</th>
                    <th>Semester</th>
                    <th>Tagihkan</th>
                    <th>Total Biaya</th>
                    <th>Nama Bank</th>
                    <th>Via Bank</th>
                    <th>Tanggal Bayar</th>
                    <th>No Transaksi</th>
                    <th>Keterangan</th>
                    <th>Status Bayar</th>
                    <th>Operasi</th>
                </tr>
                {foreach $data_pembayaran as $data}
                    <tr class="total ui-widget-content">
                        {if $smarty.get.id_semester==$data.ID_SEMESTER&&$smarty.get.tgl_bayar==$data.TGL_BAYAR&&$smarty.get.id_bank==$data.ID_BANK&&$smarty.get.id_bank_via==$data.ID_BANK_VIA&&$smarty.get.no_transaksi==$data.NO_TRANSAKSI&&$smarty.get.keterangan==$data.KETERANGAN&&$smarty.get.is_tagih==$data.IS_TAGIH}
                            <td>{$data@index+1}</td>
                            <td>    
                                <select name="semester">
                                    {foreach $data_semester as $semester}
                                        <option value="{$semester['ID_SEMESTER']}" {if $data['ID_SEMESTER']==$semester['ID_SEMESTER']}selected="true"{/if}>{$semester['NM_SEMESTER']}({$semester['TAHUN_AJARAN']})</option>
                                    {/foreach}
                                </select>
                                <input type="hidden" name="semester_asli" value="{$data.ID_SEMESTER}"/>
                            </td>
                            <td>
                                <select name="is_tagih">
                                    <option {if $data.IS_TAGIH=='Y'}selected="true" {/if} value="Y">Ya</option>
                                    <option {if $data.IS_TAGIH=='T'}selected="true" {/if} value="T">Tidak</option>
                                </select>
                                <input type="hidden" name="is_tagih_asli" value="{$data.IS_TAGIH}"/>
                            </td>
                            <td class="center">{number_format($data.JUMLAH_PEMBAYARAN)}</td>
                            <td>
                                <select name="bank">
                                    <option value=""></option>
                                    {foreach $data_bank as $data_b}
                                        <option value="{$data_b['ID_BANK']}" {if $data_b['ID_BANK']==$data['ID_BANK']}selected="true"{/if}>{$data_b['NM_BANK']}</option>
                                    {/foreach}
                                </select>
                                <input type="hidden" name="bank_asli" value="{$data.ID_BANK}"/>
                            </td>
                            <td>
                                <select name="bank_via">
                                    <option value=""></option>
                                    {foreach $data_bank_via as $data_bv}
                                        <option value="{$data_bv['ID_BANK_VIA']}" {if $data_bv['ID_BANK_VIA']==$data['ID_BANK_VIA']}selected="true"{/if}>{$data_bv['NAMA_BANK_VIA']}</option>
                                    {/foreach}
                                </select>
                                <input type="hidden" name="bank_via_asli" value="{$data.ID_BANK_VIA}"/>
                            </td>
                            <td>
                                <input type="text" style="text-transform: uppercase" class="date" name="tgl_bayar" size="7" value="{$data['TGL_BAYAR']}" />
                                <input type="hidden" name="tgl_bayar_asli" value="{$data.TGL_BAYAR}"/>
                            </td>
                            <td>
                                <input type="text" name="no_transaksi" size="10" value="{$data['NO_TRANSAKSI']}" />
                                <input type="hidden" name="no_transaksi_asli" value="{$data.NO_TRANSAKSI}"/>
                            </td>
                            <td>
                                <textarea name="keterangan" cols="15">{$data['KETERANGAN']}</textarea>
                                <input type="hidden" name="keterangan_asli" value="{$data.KETERANGAN}"/>
                            </td>
                            <td>
                                <select name="status">
                                    <option value=""></option>
                                    {foreach $data_status as $data_s}
                                        <option value="{$data_s.ID_STATUS_PEMBAYARAN}" {if $data_s.ID_STATUS_PEMBAYARAN==$data.ID_STATUS_PEMBAYARAN}selected="true" {/if}>{$data_s.NAMA_STATUS}</option>
                                    {/foreach}
                                </select>
                            </td>
                            <td>
                                <span onclick="show_detail('{$data@index+1}')" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;">Detail</span><br/>
                            </td>
                        {else}
                            <td>{$data@index+1}</td>
                            <td>{$data.NM_SEMESTER}<br/> ({$data.TAHUN_AJARAN})</td>
                            <td>
                                {if $data.IS_TAGIH=='Y'}
                                    Ya
                                {else}
                                    Tidak
                                {/if}
                            </td>
                            <td class="center">{number_format($data.JUMLAH_PEMBAYARAN)}</td>
                            <td>{$data.NM_BANK}</td>
                            <td>{$data.NAMA_BANK_VIA}</td>
                            <td>{$data.TGL_BAYAR}</td>
                            <td>{$data.NO_TRANSAKSI}</td>
                            <td>{$data.KETERANGAN}</td>
                            <td>{$data.NAMA_STATUS}</td>
                            <td>
                                <span onclick="show_detail('{$data@index+1}')" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;">Detail</span><br/>
                            </td>
                        {/if}
                    </tr>
                    <tr id="detail{$data@index+1}" style="display: none" class="ui-widget-content">
                        <td colspan="11">
                            <table class="ui-widget" width="95%">
                                <tr class="ui-widget-header">
                                    <th colspan="{if $jenis=='cmhs'}8{else}9{/if}" class="header-coloumn" style="text-align: center;">Detail Pembayaran {if $jenis=='cmhs'} Calon {/if} Mahasiswa </th>
                                </tr>
                                <tr class="ui-widget-header">
                                    <th>Nama Biaya</th>
                                    <th>Besar Biaya</th>
                                    {if $jenis=='mhs'}
                                        <th>Denda Biaya</th>
                                    {/if}
                                    <th>Tagihkan</th>
                                    <th>Nama Bank</th>
                                    <th>Via Bank</th>
                                    <th>Tanggal Bayar</th>
                                    <th>No Transaksi</th>
                                    <th>Keterangan</th>
                                </tr>
                                {foreach $data.DATA_PEMBAYARAN as $data_pembayaran}
                                    {if $jenis=='cmhs'}
                                        {if $data_pembayaran.TGL_BAYAR==NULL}
                                            {$total_tagihan=$total_tagihan+$data_pembayaran.BESAR_BIAYA}
                                        {else}
                                            {$total_pembayaran=$total_pembayaran+$data_pembayaran.BESAR_BIAYA}
                                        {/if}
                                    {else}
                                        {if $data_pembayaran.TGL_BAYAR==NULL}
                                            {$total_tagihan=$total_tagihan+$data_pembayaran.BESAR_BIAYA+$data_pembayaran.DENDA_BIAYA}
                                        {else}
                                            {$total_pembayaran=$total_pembayaran+$data_pembayaran.BESAR_BIAYA+$data_pembayaran.DENDA_BIAYA}
                                        {/if}
                                    {/if}
                                    <tr class="ui-widget-content">
                                        <td>
                                            {$data_pembayaran['NM_BIAYA']}
                                        </td>
                                        <td>
                                            {if $smarty.get.id_semester==$data.ID_SEMESTER&&$smarty.get.tgl_bayar==$data.TGL_BAYAR&&$smarty.get.id_bank==$data.ID_BANK&&$smarty.get.id_bank_via==$data.ID_BANK_VIA&&$smarty.get.no_transaksi==$data.NO_TRANSAKSI&&$smarty.get.keterangan==$data.KETERANGAN}
                                                <input type="text" name="besar_biaya{$data_pembayaran@index+1}" value="{number_format($data_pembayaran['BESAR_BIAYA'])}"/>
                                                {$total_detail=$data_pembayaran@index+1}
                                            {else}
                                                {number_format($data_pembayaran['BESAR_BIAYA'])}
                                            {/if}                                            
                                        </td>
                                        <td>
                                            {if $jenis=='mhs'}
                                                {if $smarty.get.id_semester==$data.ID_SEMESTER&&$smarty.get.tgl_bayar==$data.TGL_BAYAR&&$smarty.get.id_bank==$data.ID_BANK&&$smarty.get.id_bank_via==$data.ID_BANK_VIA&&$smarty.get.no_transaksi==$data.NO_TRANSAKSI&&$smarty.get.keterangan==$data.KETERANGAN}
                                                    <input type="text" name="denda_biaya{$data_pembayaran@index+1}" value="{number_format($data_pembayaran['DENDA_BIAYA'])}"/>
                                                    <input type="hidden" name="id_pembayaran{$data_pembayaran@index+1}" value="{$data_pembayaran.ID_PEMBAYARAN}"/>
                                                    {$total_detail=$data_pembayaran@index+1}
                                                {else}
                                                    {number_format($data_pembayaran['DENDA_BIAYA'])}
                                                {/if}
                                            {/if}
                                        </td>
                                        <td>
                                            {if $data_pembayaran['IS_TAGIH']=='Y'}
                                                Y
                                            {else}
                                                Tidak
                                            {/if}
                                        </td>
                                        <td>
                                            {$data_pembayaran['NM_BANK']}
                                        </td>
                                        <td>
                                            {$data_pembayaran['NAMA_BANK_VIA']}
                                        </td>
                                        <td>
                                            {$data_pembayaran['TGL_BAYAR']}
                                        </td>
                                        <td>
                                            {$data_pembayaran['NO_TRANSAKSI']}
                                        </td>
                                        <td>
                                            {$data_pembayaran['KETERANGAN']}
                                        </td>
                                    </tr>
                                {/foreach}

                                <tr class="ui-widget-content">
                                    <td class="center" colspan="{if $jenis=='cmhs'}8{else}9{/if}">
                                        <span onclick="hide_detail('{$data@index+1}')" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;">Tutup</span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                {foreachelse}
                    <tr class="total ui-widget-content">
                        <td class="center" colspan="11">
                            <span style="color:red">
                                Data Pembayaran Kosong
                            </span>
                        </td>
                    </tr>
                {/foreach}
            </table>
            <input type="hidden" name="total_detail" value="{$total_detail}"/>
        </form>
        <p></p>
        {if $jenis=='mhs'}
            <table class="ui-widget" style="width: 90%">
                <tr class="ui-widget-header">
                    <th class="header-coloumn" colspan="18">RIWAYAT PEMBAYARAN LAMA</th>
                </tr>
                <tr class="ui-widget-header">
                    <td>
                        <P><STRONG>Sem</STRONG></FONT></P></td>
                    <td>
                        <P><STRONG>Lunas</STRONG></FONT></P></td>
                    <td>
                        <P><STRONG>Bank</STRONG></FONT></P></td>
                    <td style="width: 100px">
                        <P><STRONG>Tgl bayar</STRONG></FONT></P></td>
                    <td>
                        <P><STRONG>SPP</STRONG></FONT></P></td>
                    <td>
                        <P><STRONG>Prakt.</STRONG></FONT></P></td>
                    <td>
                        <P><STRONG>Asuransi</STRONG></FONT></P></td>
                    <td>
                        <P><STRONG>SP3</STRONG></FONT></P></td>
                    <td>
                        <P><STRONG>PKL</STRONG></FONT></P></td>
                    <td>
                        <P><STRONG>Registr.</STRONG></FONT></P></td>
                    <td>
                        <P><STRONG>Denda</STRONG></FONT></P></td>
                    <td>
                        <P><STRONG>Piutang</STRONG></FONT></P></td>
                    <td>
                        <P><STRONG>Matrikulasi</STRONG></FONT></P></td>
                    <td>
                        <P><STRONG>KKN</STRONG></FONT></P></td>
                    <td>
                        <P><STRONG>Wisuda</STRONG></FONT></P></td>
                    <td>
                        <P><STRONG>Pembinaan</STRONG></FONT></P></td>
                    <td>
                        <P><STRONG>Ikoma</STRONG></FONT></P></td>
                </tr>
                {if $tampil_pembayaran!=''}
                    {$tampil_pembayaran}
                {else}
                    <tr class="ui-widget-content">
                        <td colspan="18" class="center"><span style="color: red">Data Pembayaran Kosong</span></td>
                    </tr>
                {/if}
            </table>
        {/if}
    {else}
        <div id="alert" class="ui-widget" style="margin-left:15px;">
            <div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;"> 
                <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                    Data Tidak Ditemukan.</p>
            </div>
            <span class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;margin-top:20px;" onclick="history.back(-1)">Kembali</span>    
        </div>
    {/if}   
{/if}
{literal}
    <script>
        function show_detail(index){
            $('#detail'+index).fadeToggle();
        }
        function hide_detail(index){
            $('#detail'+index).fadeOut();
        }
        function confirm_delete(nim,semester,tgl_bayar,bank,bank_via,no_transaksi,keterangan,is_tagih){
            var status = confirm('Apakah anda yakin menghapus ini?');
            if(status==true){
                $.ajax({
                    url:'history-bayar.php?cari='+nim+'',
                    type:'post',
                    data:'hapus=hapus&cari='+nim+'&id_semester='+semester+
                    '&tgl_bayar='+tgl_bayar+'&id_bank='+bank+'&id_bank_via='+bank_via+
                    '&no_transaksi='+no_transaksi+'&keterangan='+keterangan+'&is_tagih='+is_tagih,
                    beforeSend : function(){
                        $('#center_content').html('<div style="width: 100%;" align="center"><img src="/js/loading.gif" /></div>');
                    },
                    success :function(data){
                        $('#center_content').html(data);
                    }
                });
            }
        }
        $('#form').submit(function(){
            if($('#cari').val()==''){
                $('#alert').fadeIn();
                return false;
            }    
        });
        $( ".date" ).datepicker({dateFormat:'dd-M-yy',changeMonth: true,
                        changeYear: true});
    </script>
{/literal}