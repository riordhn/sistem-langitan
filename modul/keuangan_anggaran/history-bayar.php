<?php

include 'config.php';
include 'config-mysql.php';
include 'class/history.class.php';
include 'class/utility.class.php';
include 'class/master.class.php';

$history = new history($db);
$list = new utility($db);
$master = new master($db);

if (isset($_GET['cari'])) {
    if (strlen(trim(get('cari'))) == 9 || strlen(trim(get('cari'))) == 10 || get('jenis') == 'mhs') {
        if (isset($_POST['save'])) {
            $history->update_pembayaran_mhs(get('cari'), post('semester'), post('semester_asli'), post('tgl_bayar'), post('tgl_bayar_asli'), post('bank'), post('bank_asli'), post('bank_via'), post('bank_via_asli'), post('no_transaksi'), post('no_transaksi_asli'), post('keterangan'), post('keterangan_asli'), post('is_tagih'), post('is_tagih_asli'),post('status'));
            for ($i = 1; $i <= post('total_detail'); $i++) {
                $history->update_detail_pembayaran(post('id_pembayaran' . $i), str_replace(',', '', post('besar_biaya' . $i)), str_replace(',', '', post('denda_biaya' . $i)));
            }
        } else if (isset($_POST['tambah'])) {
            if (isset($_POST['pendaftaran'])) {
                $q_pendaftaran = "";
            } else {
                $q_pendaftaran = "AND DB.PENDAFTARAN_BIAYA='0'";
            }
            $db->Query("
              INSERT INTO PEMBAYARAN (ID_MHS, ID_SEMESTER, ID_DETAIL_BIAYA, BESAR_BIAYA, IS_TAGIH,ID_STATUS_PEMBAYARAN)
              SELECT BKM.ID_MHS, {$_POST['semester']} AS ID_SEMESTER, DB.ID_DETAIL_BIAYA, DB.BESAR_BIAYA, 'Y' AS IS_TAGIH,'2' AS ID_STATUS_PEMBAYARAN
              FROM BIAYA_KULIAH_MHS BKM
              JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH
              LEFT JOIN MAHASISWA M ON M.ID_MHS = BKM.ID_MHS
              WHERE
              M.ID_MHS IN (SELECT ID_MHS FROM BIAYA_KULIAH_MHS) {$q_pendaftaran} AND
              M.NIM_MHS='{$_POST['cari']}'");
        } else if (isset($_POST['tambah_detail'])) {
            $history->insert_detail_biaya(get('cari'), post('detail_biaya'), post('semester'));
        } else if (isset($_POST['hapus'])) {
            $history->hapus_pembayaran_mhs(post('cari'), post('id_semester'), post('id_bank'), post('id_bank_via'), post('tgl_bayar'), post('no_transaksi'), post('keterangan'), post('is_tagih'), $user->ID_PENGGUNA);
        }
        $nim = $_GET['cari'] != '' ? $_GET['cari'] : $_POST['cari'];
        //Riwayat Pembayaran Lama
        $biomhs01 = "SELECT Mhs_ua.nama, Mhs_ua.nim, Mhs_ua.id_mhs,Mhs_ua.id_prod FROM kd_fakul Kd_fakul, kd_prodi Kd_prodi, mhs_ua Mhs_ua 
			WHERE Kd_fakul.kd_fakulta = Kd_prodi.kd_fakulta   AND Kd_prodi.id_prod = Mhs_ua.id_prod AND Mhs_ua.nim = '$nim'";
        $biomhs1 = mysql_fetch_assoc(mysql_query($biomhs01));
        if ($biomhs1 != '') {
            $idmhs = $biomhs1[id_mhs];

            $datatrans01 = "SELECT * FROM transspp where id_mhs='$idmhs' order by (9999-id_thn)";
            $datatrans1 = mysql_query($datatrans01);

            while ($datatrans = mysql_fetch_array($datatrans1)) {
                $amb_sp3 = number_format($datatrans[sp3]);
                $amb_pkl = number_format($datatrans[pkl]);
                $amb_asu = number_format($datatrans[asuransi]);
                $amb_reg = number_format($datatrans[regis]);
                $amb_spp = number_format($datatrans[spp]);
                $amb_pra = number_format($datatrans[praktikum]);
                $amb_iko = number_format($datatrans[denda]);
                $amb_piu = number_format($datatrans[piutang]);
                $amb_matrikulasi = number_format($datatrans[matrikulasi]);
                $amb_kkn = number_format($datatrans[kkn]);
                $amb_wisuda = number_format($datatrans[wisuda]);
                $amb_pembinaan = number_format($datatrans[pembinaan]);
                $amb_ikoma = number_format($datatrans[ikoma]);

                $amb_tgl = $datatrans[tgl_bayar];
                $amb_ban = $datatrans[id_bank];
                $amb_sem = $datatrans[id_thn];
                $amb_sta = $datatrans[stat_spp];
                if ($datatrans["dibebaskan"] == "Y") {
                    $amb_sudahbayar = "Y";
                } else if ($datatrans["dibebaskan"] == "T") {
                    if ($datatrans["tgl_bayar"] == '0000-00-00') {
                        $amb_sudahbayar = "T";
                    } else {
                        $amb_sudahbayar = "Y";
                    }
                }


                $biothnsem01 = "SELECT * FROM thn_sem WHERE id_thn = '$amb_sem'";
                $biothnsem1 = mysql_query($biothnsem01);
                $biothnsem = mysql_fetch_array($biothnsem1);
                $amb_semes = $biothnsem[nama_semes];
                $thn_ring = $biothnsem[thn_akad];
                $sem_ring = $biothnsem[sem_akad];

                $databaru01 = "SELECT * FROM tbl_bank where id_bank='$amb_ban'";
                $databaru1 = mysql_query($databaru01);
                $databank = mysql_fetch_array($databaru1);
                $amb_bank = $databank[nm_bank];

                list($thn9, $bln9, $tgl9) = split("-", $amb_tgl);

                $tampil_pembayaran .=
                        "<tr class='ui-widget-content'>
			<td>
				<P><FONT face=Arial color=#000099 size=1><STRONG>$amb_semes</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_sudahbayar</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_bank</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$tgl9-$bln9-$thn9</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_spp</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_pra</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_asu</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_sp3</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_pkl</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_reg</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_iko</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_piu</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_matrikulasi</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_kkn</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_wisuda</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_pembinaan</STRONG></FONT></P></td>
			<td>
				<P><STRONG>$amb_ikoma</STRONG></FONT></P></td>
		</tr>";
            }
        }
        $smarty->assign('tampil_pembayaran', $tampil_pembayaran);
        $smarty->assign('jenis', 'mhs');
        $smarty->assign('data_detail_biaya', $history->load_detail_biaya(get('cari')));
        $smarty->assign('data_detail_mahasiswa', $history->get_data_mhs(trim(get('cari'))));
        $smarty->assign('data_pembayaran', $history->load_history_bayar_mhs(trim(get('cari'))));
    } else {
        $smarty->assign('jenis', 'cmhs');
        $smarty->assign('data_detail_calon_mahasiswa', $history->get_data_cmhs(trim(get('cari'))));
        $smarty->assign('data_pembayaran', $history->load_history_bayar_cmhs(trim(get('cari'))));
    }
}
$smarty->assign('data_status', $master->load_status_pembayaran());
$smarty->assign('data_semester', $list->get_semester_all());
$smarty->assign('data_bank', $list->get_bank());
$smarty->assign('data_bank_via', $list->get_via_bank());
$smarty->display('history/history-bayar.tpl');
?>
