<?php

include 'config.php';
include '../keuangan/class/list_data.class.php';
include '../keuangan/class/detail.class.php';

$list = new list_data($db);
$detail = new detail($db);

if (isset($_GET)) {
    if (get('mode') == 'tampil' || get('mode') == 'detail') {
        $smarty->assign('data_mahasiswa', $detail->load_data_mahasiswa_unair(get('fakultas'), get('biaya_kuliah'), get('kelompok_biaya'), get('prodi'), get('jalur'), get('angkatan'), get('mode'), get('jenis')));
    }
}

$smarty->assign('data_prodi', $list->load_list_prodi(get('fakultas')));
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->assign('data_angkatan', $list->load_angkatan_mhs());
$smarty->assign('data_fakultas_one', $list->get_fakultas(get('fakultas')));
$smarty->assign('data_prodi_one', $list->get_prodi(get('prodi')));
$smarty->display('detail/data-mahasiswa.tpl');
?>
