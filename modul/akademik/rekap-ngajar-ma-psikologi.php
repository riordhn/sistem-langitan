<?php
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$id_pengguna= $user->ID_PENGGUNA; 
$kdfak=$user->ID_FAKULTAS;

//Fakultas Lain
if($kdfak!=11){
	echo 'Anda tidak memiliki hak akses';
} else {

$smtall=getData("select id_semester, tahun_ajaran||' - '||nm_semester as smt from semester where thn_akademik_semester in (
select * from (select distinct thn_akademik_semester
from semester where thn_akademik_semester<=EXTRACT(YEAR FROM sysdate) order by thn_akademik_semester desc)
where rownum<=3) and nm_semester in ('Ganjil', 'Genap')
order by thn_akademik_semester desc, nm_semester desc");
$smarty->assign('T_ST', $smtall);

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True'");
$smt1 = isSet($_REQUEST['smt']) ? $_REQUEST['smt'] : $smtaktif['ID_SEMESTER'];
$smarty->assign('SMT',$smt1);

$dataprodi=getData("select id_program_studi,nm_jenjang||'-'||nm_program_studi as nm_prodi from program_studi
					left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
					where id_fakultas=$kdfak");
$smarty->assign('T_PRODI',$dataprodi);

if ($_POST['action']=='rekap') 
{
	$awal=$_POST['calawal'];
	$akhir=$_POST['calakhir'];
	$prodi=$_POST['prodi'];
	
	if ($prodi<>'ALL') {
	$rekap=getData("select gelar_depan||' '||nm_pengguna||', '||gelar_belakang as dosen,kd_mata_kuliah,nm_mata_kuliah,id_kelas_mk,
					kurikulum_mk.kredit_semester,nama_kelas,sum(case when presensi_mkdos.id_dosen is null then 0 else 1 end) as masuk,
					count(presensi_kelas.id_kelas_mk) as total
					from kelas_mk
					left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
					left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
					left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
					left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
					left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
					left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
					left join presensi_kelas on kelas_mk.id_kelas_mk=presensi_kelas.id_kelas_mk
					left join presensi_mkdos on presensi_kelas.id_presensi_kelas=presensi_mkdos.id_presensi_kelas and dosen.id_dosen=presensi_mkdos.id_dosen
					where kelas_mk.id_program_studi=$prodi and kelas_mk.id_semester=$smt1 and
					tgl_presensi_kelas between to_date ('$awal', 'dd-mm-yy') AND to_date ('$akhir', 'dd-mm-yy')
					group by gelar_depan,nm_pengguna,gelar_belakang,kd_mata_kuliah,nm_mata_kuliah,id_kelas_mk,
					kurikulum_mk.kredit_semester,nama_kelas
					order by nm_pengguna,kd_mata_kuliah");
	$span=getData("select kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,id_kelas_mk,
					count(distinct id_pengampu_mk) as span
					from kelas_mk
					left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
					left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
					left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
					left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
					left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
					left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
					left join presensi_kelas on kelas_mk.id_kelas_mk=presensi_kelas.id_kelas_mk
					left join presensi_mkdos on presensi_kelas.id_presensi_kelas=presensi_mkdos.id_presensi_kelas and dosen.id_dosen=presensi_mkdos.id_dosen
					where kelas_mk.id_program_studi=$prodi and kelas_mk.id_semester=$smt1 and
					tgl_presensi_kelas between to_date ('$awal', 'dd-mm-yy') AND to_date ('$akhir', 'dd-mm-yy')
					group by kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,id_kelas_mk
					order by kd_mata_kuliah");
	}
	else
	{
	$rekap=getData("select gelar_depan||' '||nm_pengguna||', '||gelar_belakang as dosen,kd_mata_kuliah,nm_mata_kuliah,id_kelas_mk,
					kurikulum_mk.kredit_semester,nama_kelas,sum(case when presensi_mkdos.id_dosen is null then 0 else 1 end) as masuk,
					count(presensi_kelas.id_kelas_mk) as total
					from kelas_mk
					left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
					left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
					left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
					left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
					left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
					left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
					left join presensi_kelas on kelas_mk.id_kelas_mk=presensi_kelas.id_kelas_mk
					left join presensi_mkdos on presensi_kelas.id_presensi_kelas=presensi_mkdos.id_presensi_kelas and dosen.id_dosen=presensi_mkdos.id_dosen
					where kelas_mk.id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak) and kelas_mk.id_semester=$smt1 and
					tgl_presensi_kelas between to_date ('$awal', 'dd-mm-yy') AND to_date ('$akhir', 'dd-mm-yy')
					group by gelar_depan,nm_pengguna,gelar_belakang,kd_mata_kuliah,nm_mata_kuliah,id_kelas_mk,
					kurikulum_mk.kredit_semester,nama_kelas
					order by nm_pengguna,kd_mata_kuliah");
	$span=getData("select kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,id_kelas_mk,
					count(distinct id_pengampu_mk) as span
					from kelas_mk
					left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
					left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
					left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
					left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
					left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
					left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
					left join presensi_kelas on kelas_mk.id_kelas_mk=presensi_kelas.id_kelas_mk
					left join presensi_mkdos on presensi_kelas.id_presensi_kelas=presensi_mkdos.id_presensi_kelas and dosen.id_dosen=presensi_mkdos.id_dosen
					where kelas_mk.id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak) and kelas_mk.id_semester=$smt1 and
					tgl_presensi_kelas between to_date ('$awal', 'dd-mm-yy') AND to_date ('$akhir', 'dd-mm-yy')
					group by kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,id_kelas_mk
					order by kd_mata_kuliah");
	}
	$smarty->assign('T_REKAP',$rekap);
	$smarty->assign('T_SPAN',$span);
	
	
}

$smarty->display('rekap-ngajar-ma-psikologi.tpl');
      
}
?>
