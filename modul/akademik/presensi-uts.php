<?php
// FITUR DIPINDAH KE PRODI
// FIKRIE
// 14-12-2016
echo "Fitur ini Pindah ke Prodi"; exit();
/*
Yudi Sulistya 14/10/2012
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$kdprodi=$user->ID_PROGRAM_STUDI;

$id_pt = $id_pt_user;

$id_pengguna=$user->ID_PENGGUNA;
$kdfak=$user->ID_FAKULTAS;
$smarty->assign('FAK', $kdfak);

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True' and id_perguruan_tinggi = '{$id_pt}'");
$smt=$smtaktif['ID_SEMESTER'];

if($kdfak == 0){
$dataprodi=getData("select id_program_studi||'-'||id_fakultas as id_program_studi,nm_jenjang||'-'||nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_fakultas=$kdfak");
$smarty->assign('T_PRODI', $dataprodi);	
}else{
$dataprodi=getData("select id_program_studi||'-'||id_fakultas as id_program_studi,nm_jenjang||'-'||nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_fakultas=$kdfak and status_aktif_prodi = 1");
$smarty->assign('T_PRODI', $dataprodi);
}

if (isset($_POST['action'])=='view') {
$var = explode("-", $_POST['kdprodi']);

if ($var[0]=='all') {
$dataprodi1=getData("select (': FAKULTAS '||nm_fakultas) as prodi from fakultas where id_fakultas=".$var[1]."");
$smarty->assign('NM_PRODI', $dataprodi1);

$jaf=getData("
select a.*, b.peserta from
(
(
select umk.id_ujian_mk,umk.id_kelas_mk,umk.nm_ujian_mk,umk.tgl_ujian,umk.jam_mulai||' - '||umk.jam_selesai as jam,kur.kredit_semester,
smt.nm_semester,smt.tahun_ajaran,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kmk.id_program_studi,nm_jenjang||'-'||nm_singkat_prodi as prodi,rg.nm_ruangan
from ujian_mk umk
left join jadwal_ujian_mk jad on umk.id_ujian_mk=jad.id_ujian_mk
left join ruangan rg on jad.id_ruangan=rg.id_ruangan
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join semester smt on kmk.id_semester=smt.id_semester
left join nama_kelas nmk on nmk.id_nama_kelas=kmk.no_kelas_mk
left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
where umk.id_kegiatan=(select id_kegiatan from kegiatan where id_perguruan_tinggi = '{$id_pt}' and kode_kegiatan = 'UTS') and ps.id_fakultas=".$var[1]." and umk.id_semester=$smt) a
left join
(select id_ujian_mk, count(*) as peserta from ujian_mk_peserta
where id_ujian_mk in (select id_ujian_mk from ujian_mk where id_semester=$smt and id_kegiatan=(select id_kegiatan from kegiatan where id_perguruan_tinggi = '{$id_pt}' and kode_kegiatan = 'UTS'))
group by id_ujian_mk) b
on a.id_ujian_mk=b.id_ujian_mk
)
");
$smarty->assign('T_MK', $jaf);

} else {
$dataprodi1=getData("select (': PRODI '||nm_jenjang||'-'||nm_program_studi) as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_program_studi=".$var[0]."");
$smarty->assign('NM_PRODI', $dataprodi1);

$jaf=getData("
select a.*, b.peserta from
(
(
select umk.id_ujian_mk,umk.id_kelas_mk,umk.nm_ujian_mk,umk.tgl_ujian,umk.jam_mulai||' - '||umk.jam_selesai as jam,kur.kredit_semester,
smt.nm_semester,smt.tahun_ajaran,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kmk.id_program_studi,nm_jenjang||'-'||nm_singkat_prodi as prodi,rg.nm_ruangan
from ujian_mk umk
left join jadwal_ujian_mk jad on umk.id_ujian_mk=jad.id_ujian_mk
left join ruangan rg on jad.id_ruangan=rg.id_ruangan
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join semester smt on kmk.id_semester=smt.id_semester
left join nama_kelas nmk on nmk.id_nama_kelas=kmk.no_kelas_mk
left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
where umk.id_kegiatan=(select id_kegiatan from kegiatan where id_perguruan_tinggi = '{$id_pt}' and kode_kegiatan = 'UTS') and ps.id_program_studi=".$var[0]." and umk.id_semester=$smt) a
left join
(select id_ujian_mk, count(*) as peserta from ujian_mk_peserta
where id_ujian_mk in (select id_ujian_mk from ujian_mk where id_semester=$smt and id_kegiatan=(select id_kegiatan from kegiatan where id_perguruan_tinggi = '{$id_pt}' and kode_kegiatan = 'UTS'))
group by id_ujian_mk) b
on a.id_ujian_mk=b.id_ujian_mk
)
");
$smarty->assign('T_MK', $jaf);
}
}

//Khusus Fakultas Psikologi
if($kdfak==11){
$smarty->display('presensi-uts-psikologi.tpl');

//Fakultas Lain
} else {
$smarty->display('presensi-uts.tpl');
}
?>