<?php
/*
Yudi Sulistya 10/01/2012
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$kdprodi=$user->ID_PROGRAM_STUDI;

$id_pengguna= $user->ID_PENGGUNA; 
$fak=getvar("select fakultas.id_fakultas from pegawai left join unit_kerja on pegawai.id_unit_kerja=unit_kerja.id_unit_kerja 
left join program_studi on unit_kerja.id_program_studi=program_studi.id_program_studi 
left join fakultas on program_studi.id_fakultas=fakultas.id_fakultas 
where pegawai.id_pengguna=$id_pengguna");
$kdfak=$fak['ID_FAKULTAS'];
$smarty->assign('FAK', $kdfak);

$smt=getvar("select id_semester, tahun_ajaran, nm_semester from semester where status_aktif_semester='True'");
$smtaktif=$smt['ID_SEMESTER'];
$thn_ajar=$smt['TAHUN_AJARAN'];
$nm_smt=$smt['NM_SEMESTER'];

$smarty->assign('SMT', $smtaktif);
$smarty->assign('THN', $thn_ajar);
$smarty->assign('NM_SMT', $nm_smt);

$jaf=getData("select mhs.nim_mhs, upper(pg.nm_pengguna) as nm_pengguna, mk.kd_mata_kuliah, upper(mk.nm_mata_kuliah) as nm_mata_kuliah, pma.persen_presensi from 
mahasiswa mhs 
left join pengguna pg on mhs.id_pengguna=pg.id_pengguna
left join pengambilan_mk pma on mhs.id_mhs=pma.id_mhs 
left join kelas_mk kma on pma.id_kelas_mk=kma.id_kelas_mk 
left join kurikulum_mk kmk on kma.id_kurikulum_mk=kmk.id_kurikulum_mk 
left join mata_kuliah mk on kmk.id_mata_kuliah=mk.id_mata_kuliah 
left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
where pma.id_semester='".$smtaktif."' and ps.id_fakultas='".$kdfak."' and pma.status_cekal=0 
order by mhs.nim_mhs, mk.nm_mata_kuliah");
$smarty->assign('T_MK', $jaf);

$smarty->display('report-presensi-cekal.tpl');  
?>