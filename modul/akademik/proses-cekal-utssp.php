<?php
require('common.php');
require_once ('ociFunction.php');
//require('../../../config.php');
$db = new MyOracle();

error_reporting (E_ALL & ~E_NOTICE);
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$id_pengguna= $user->ID_PENGGUNA;

$kdfak=$user->ID_FAKULTAS;

//Khusus Fakultas Psikologi
if($kdfak !=4){

	echo '<script>alert("Fitur ini Khusus FEB")</script>';

} else {
$smtaktif=getvar("select id_semester from semester where status_aktif_sp='True'");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);

$smt1 = isSet($_POST['smt']) ? $_POST['smt'] : $smtaktif['ID_SEMESTER'];

$kuliah=getvar("SELECT TGL_MULAI_JSF,TGL_SELESAI_JSF FROM JADWAL_SEMESTER_FAKULTAS WHERE ID_SEMESTER=94 and ID_FAKULTAS=$kdfak and ID_KEGIATAN=86");
$uts=getvar("SELECT TGL_MULAI_JSF,TGL_SELESAI_JSF FROM JADWAL_SEMESTER_FAKULTAS WHERE ID_SEMESTER=94 and ID_FAKULTAS=$kdfak and ID_KEGIATAN=83");
$uas=getvar("SELECT TGL_MULAI_JSF,TGL_SELESAI_JSF FROM JADWAL_SEMESTER_FAKULTAS WHERE ID_SEMESTER=94 and ID_FAKULTAS=$kdfak and ID_KEGIATAN=84");

$smt=getData("select id_semester, tahun_ajaran||' - '||nm_semester||'/'||group_semester as smt from semester order by thn_akademik_semester desc,nm_semester desc");
$smarty->assign('T_ST', $smt);

//$action = isSet($_GET['action']) ? $_GET['action'] : 'tampil';

if ($_GET['action']=='proses')
{
$id_prodi=$_GET['idprodi'];
if ($smt1!='') {
		$smarty->assign('SMT',$smt1);
if 	($kuliah[TGL_MULAI_JSF]==null ||$kuliah[TGL_SELESAI_JSF]==null|| $uts[TGL_SELESAI_JSF]==null ||$kuliah[TGL_MULAI_JSF]=='' || $uts[TGL_SELESAI_JSF]==''||$kuliah[TGL_SELESAI_JSF]=='')
{
	echo '<script>alert("JADWAL AKADEMIK PERKULIAHAN DAN UTS SEMESTER PENDEK BELUM DIISI!!!")</script>';
} else {
	//echo "test";
	$hitungcekal_utssp = "select kelas, mhs,pertemuan, masuk,round(((masuk/pertemuan)*100),2) as persen,
						case when (round(((masuk/pertemuan)*100),2)>=66 and status='0') or 
						(round(((masuk/pertemuan)*100),2)>=0 and status in ('1','2','3','4')) or
						(round(((masuk/pertemuan)*100),2)=100 and status='5')
						then 1 else 3 end as cekal_uas,id_pengambilan_mk
						from
						(select presensi_kelas.id_kelas_mk, presensi_kelas.id_kelas_mk as kelas, count(presensi_kelas.id_kelas_mk) as pertemuan,
						kurikulum_mk.status_mkta as status
						from presensi_kelas
						inner join kelas_mk on presensi_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						inner join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
						where kelas_mk.id_program_studi=$id_prodi and kelas_mk.id_semester=$smt1
						and tgl_presensi_kelas between to_date ('$kuliah[TGL_MULAI_JSF]', 'dd-mm-yy') AND to_date ('$uts[TGL_MULAI_JSF]', 'dd-mm-yy')
						group by presensi_kelas.id_kelas_mk, presensi_kelas.id_kelas_mk, kurikulum_mk.status_mkta) k
						left join
						(select pengambilan_mk.id_kelas_mk, status_pengambilan_mk,id_pengambilan_mk,pengambilan_mk.id_mhs as mhs,
						sum(case when presensi_mkmhs.kehadiran >0 then 1 else 0 end) as masuk
						from pengambilan_mk
						inner join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk and kelas_mk.id_semester=$smt1
						left join presensi_kelas on kelas_mk.id_kelas_mk=presensi_kelas.id_kelas_mk
						left join presensi_mkmhs on presensi_kelas.id_presensi_kelas=presensi_mkmhs.id_presensi_kelas and pengambilan_mk.id_mhs=presensi_mkmhs.id_mhs
						where kelas_mk.id_program_studi=$id_prodi and pengambilan_mk.id_semester=$smt1
						and tgl_presensi_kelas between to_date ('$kuliah[TGL_MULAI_JSF]', 'dd-mm-yy') AND to_date ('$uts[TGL_MULAI_JSF]', 'dd-mm-yy')
						group by pengambilan_mk.id_kelas_mk, status_pengambilan_mk, id_pengambilan_mk, pengambilan_mk.id_mhs)m
						on k.id_kelas_mk=m.id_kelas_mk";	
						
	

	$result = $db->Query($hitungcekal_utssp)or die("salah kueri hitungcekal_utssp");
	while($r = $db->FetchRow()) {
		gantidata("update pengambilan_mk set status_cekal_uts='".$r[5]."' where id_pengambilan_mk='".$r[6]."' and id_semester=$smt1
			   and id_pengambilan_mk not in (select id_pengambilan_mk from buka_cekal where jenis_cekal='BUKA CEKAL UTS-SP')
			   and status_cekal_uts !='4'");
		//echo "update pengambilan_mk set status_cekal_uts='".$r[5]."' where id_pengambilan_mk='".$r[6]."' and id_semester=$smt1
		//	   and id_pengambilan_mk not in (select id_pengambilan_mk from buka_cekal where jenis_cekal='BUKA CEKAL UTS-SP')
		//	   and status_cekal_uts !='4'";
	}
	}
		gantidata("update pengambilan_mk set status_cekal='1' where id_semester=$smt1 and id_kurikulum_mk
					in (select id_kurikulum_mk from kurikulum_mk where status_mkta in ('1','2','3','4') and id_program_studi=$id_prodi)");


echo '<script>alert("Proses CEKAL UTS-SP.. Sudah SELESAI...")</script>';
}

}


if ($_GET['action']=='proses_per_mk')
{
//$smarty->assign('disp1','none');
//$smarty->assign('disp2','block');

$id_kelas_mk=$_GET['idkelasmk'];
$id_prodi=$_GET['idprodi'];
$st=$_GET['st'];

if 	($kuliah[TGL_MULAI_JSF]==null ||$kuliah[TGL_SELESAI_JSF]==null|| $uts[TGL_SELESAI_JSF]==null ||$kuliah[TGL_MULAI_JSF]=='' || $uts[TGL_SELESAI_JSF]==''||$kuliah[TGL_SELESAI_JSF]=='')
{
	echo '<script>alert("JADWAL AKADEMIK PERKULIAHAN DAN UTS SEMESTER PENDEK BELUM DIISI!!!")</script>';
} else {
	$cek= getvar ("select status_mkta from kelas_mk
				   left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
				   where kelas_mk.id_kelas_mk=$id_kelas_mk and kelas_mk.id_semester=$smt1");
	$status=$cek['STATUS_MKTA'];
	
	if ($status =='1' or $status =='2' or $status =='3' or $status =='4')
	{
		gantidata("update pengambilan_mk set status_cekal='1' where id_semester=$smt1 and id_kelas_mk=$id_kelas_mk");
		
	} else
	{
	
	$hitungcekal_utssp_permk = "select kelas, mhs,pertemuan, masuk,round(((masuk/pertemuan)*100),2) as persen,
						case when (round(((masuk/pertemuan)*100),2)>=66 and status='0') or 
						(status in ('1','2','3','4')) or
						(round(((masuk/pertemuan)*100),2)=100 and status='5')
						then 1 else 3 end as cekal_uas,id_pengambilan_mk
						from
						(select presensi_kelas.id_kelas_mk, presensi_kelas.id_kelas_mk as kelas, count(presensi_kelas.id_kelas_mk) as pertemuan,
						kurikulum_mk.status_mkta as status
						from presensi_kelas
						inner join kelas_mk on presensi_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						inner join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
						where presensi_kelas.id_kelas_mk=$id_kelas_mk
						and tgl_presensi_kelas between to_date ('$kuliah[TGL_MULAI_JSF]', 'dd-mm-yy') AND to_date ('$uts[TGL_MULAI_JSF]', 'dd-mm-yy')
						group by presensi_kelas.id_kelas_mk, presensi_kelas.id_kelas_mk, kurikulum_mk.status_mkta) k
						left join
						(select pengambilan_mk.id_kelas_mk, status_pengambilan_mk,id_pengambilan_mk,pengambilan_mk.id_mhs as mhs,
						sum(case when presensi_mkmhs.kehadiran >0 then 1 else 0 end) as masuk
						from pengambilan_mk
						inner join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk and kelas_mk.id_semester=$smt1
						left join presensi_kelas on kelas_mk.id_kelas_mk=presensi_kelas.id_kelas_mk
						left join presensi_mkmhs on presensi_kelas.id_presensi_kelas=presensi_mkmhs.id_presensi_kelas and pengambilan_mk.id_mhs=presensi_mkmhs.id_mhs
						where kelas_mk.id_kelas_mk=$id_kelas_mk and pengambilan_mk.id_semester=$smt1
						and tgl_presensi_kelas between to_date ('$kuliah[TGL_MULAI_JSF]', 'dd-mm-yy') AND to_date ('$uts[TGL_MULAI_JSF]', 'dd-mm-yy')
						group by pengambilan_mk.id_kelas_mk, status_pengambilan_mk, id_pengambilan_mk, pengambilan_mk.id_mhs)m
						on k.id_kelas_mk=m.id_kelas_mk";	
	
	
	 	$result = $db->Query($hitungcekal_uas_per_mk)or die("salah kueri hitungcekal_uas_per_mk");
	while($r = $db->FetchRow()) {
		gantidata("update pengambilan_mk set status_cekal_uts='".$r[5]."' where id_pengambilan_mk='".$r[6]."' and id_semester=$smt1
			and id_pengambilan_mk not in (select id_pengambilan_mk from buka_cekal where jenis_cekal='BUKA CEKAL UTS-SP')
			and status_cekal_uts !='4'");
		//echo "update pengambilan_mk set status_cekal='".$r[5]."' where id_pengambilan_mk='".$r[6]."' and id_semester=$smt1
		//      and id_pengambilan_mk not in (select id_pengambilan_mk from buka_cekal where jenis_cekal='BUKA CEKAL UAS')";
	}
	}
		//gantidata("update pengambilan_mk set status_cekal='1' where id_semester=$smt1 and id_kurikulum_mk
		//			in (select id_kurikulum_mk from kurikulum_mk where status_mkta in ('1','2','3','4') and id_program_studi=$id_prodi) and status_cekal !='1'");
		//echo "update pengambilan_mk set status_cekal='1' where id_semester=$smt1 and id_kurikulum_mk
			//		in (select id_kurikulum_mk from kurikulum_mk where status_mkta in ('1','2') and id_program_studi=$id_prodi) and status_cekal !='1'";

$smarty->assign('disp1','none');
$smarty->assign('disp2','block');

echo '<script>location.href="javascript:history.go(-1)";</script>';
echo '<script>alert("Proses CEKAL UTS-SP PER MK.. Sudah SELESAI...")</script>';
}

//echo '<script> $("#presensi-pcuas").load("proses-cekal-utssp.php?action=detail&idprodi=$id_prodi&st=$st"); </scr;
//echo '<script>location.href="javascript:history.go(-1)";</script;

}



if ($_GET['action']=='detail')
{
$smarty->assign('disp1','none');
$smarty->assign('disp2','block');

$id_prodi=$_GET['idprodi'];
$st=$_GET['st'];

if ($st==2) {

$detail_ma=getData("select s1.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,tm,tmhs,s1.id_program_studi from
(select kelas_mk.id_kelas_mk,kelas_mk.id_program_studi,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,count(presensi_kelas.id_presensi_kelas) as tm from kelas_mk
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join presensi_kelas on kelas_mk.id_kelas_mk=presensi_kelas.id_kelas_mk
where id_semester=$smt1 and kelas_mk.id_program_studi=$id_prodi  
and kelas_mk.id_kelas_mk in
(select distinct id_kelas_mk from pengambilan_mk where status_cekal_uts='2' and id_semester=$smt1) 
group by kelas_mk.id_kelas_mk, kelas_mk.id_program_studi,kd_mata_kuliah, nm_mata_kuliah, nama_kelas) s1
left join
(select kelas_mk.id_kelas_mk,count(distinct presensi_mkmhs.id_presensi_kelas) as tmhs
from kelas_mk
left join presensi_kelas on kelas_mk.id_kelas_mk=presensi_kelas.id_kelas_mk
left join presensi_mkmhs on presensi_kelas.id_presensi_kelas=presensi_mkmhs.id_presensi_kelas
where id_semester=$smt1 and kelas_mk.id_program_studi=$id_prodi  
and tgl_presensi_kelas between to_date ('$kuliah[TGL_MULAI_JSF]', 'dd-mm-yy') AND to_date ('$uts[TGL_MULAI_JSF]', 'dd-mm-yy')
and kelas_mk.id_kelas_mk in
(select distinct id_kelas_mk from pengambilan_mk where status_cekal_uts='2' and id_semester=$smt1) 
group by kelas_mk.id_kelas_mk)s2 on s1.id_kelas_mk=s2.id_kelas_mk
order by kd_mata_kuliah,nama_kelas");

}
if ($st==1) {

$detail_ma=getData("select s1.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,tm,tmhs,s1.id_program_studi from
(select kelas_mk.id_kelas_mk,kelas_mk.id_program_studi,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,count(presensi_kelas.id_presensi_kelas) as tm from kelas_mk
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join presensi_kelas on kelas_mk.id_kelas_mk=presensi_kelas.id_kelas_mk
where id_semester=$smt1 and kelas_mk.id_program_studi=$id_prodi  
and kelas_mk.id_kelas_mk in
(select distinct id_kelas_mk from pengambilan_mk where status_cekal_uts !='2' and id_semester=$smt1) 
group by kelas_mk.id_kelas_mk, kelas_mk.id_program_studi,kd_mata_kuliah, nm_mata_kuliah, nama_kelas) s1
left join
(select kelas_mk.id_kelas_mk,count(distinct presensi_mkmhs.id_presensi_kelas) as tmhs
from kelas_mk
left join presensi_kelas on kelas_mk.id_kelas_mk=presensi_kelas.id_kelas_mk
left join presensi_mkmhs on presensi_kelas.id_presensi_kelas=presensi_mkmhs.id_presensi_kelas
where id_semester=$smt1 and kelas_mk.id_program_studi=$id_prodi  
and tgl_presensi_kelas between to_date ('$kuliah[TGL_MULAI_JSF]', 'dd-mm-yy') AND to_date ('$uts[TGL_MULAI_JSF]', 'dd-mm-yy')
and kelas_mk.id_kelas_mk in
(select distinct id_kelas_mk from pengambilan_mk where status_cekal_uts !='2' and id_semester=$smt1) 
group by kelas_mk.id_kelas_mk)s2 on s1.id_kelas_mk=s2.id_kelas_mk
order by kd_mata_kuliah,nama_kelas");

}
$smarty->assign('status', $st);
$smarty->assign('DT_MA', $detail_ma);


}

$prodi1=getData("select id_program_studi,prodi,sum(case when status='SUDAH' then 1 else 0 end) as ttl_sdh,
sum(case when status='BELUM' then 1 else 0 end) as ttl_blm from
(select program_studi.id_program_studi,nm_jenjang||'-'||nm_program_studi as prodi,id_kelas_mk,count(id_kelas_mk) from kelas_mk
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where id_fakultas=$kdfak
group by nm_jenjang||'-'||nm_program_studi, id_kelas_mk,program_studi.id_program_studi)s1
inner join (
select id_kelas_mk,case when (sum(case when status_cekal_uts='2' then 1 else 0 end)>0)then 'BELUM' else 'SUDAH' end as status from pengambilan_mk
where status_apv_pengambilan_mk=1 and id_kelas_mk in (select id_kelas_mk from kelas_mk where id_program_studi in
(select id_program_studi from program_studi where id_fakultas=$kdfak) and pengambilan_mk.id_semester='$smtaktif[ID_SEMESTER]')
group by id_kelas_mk)s2
on s1.id_kelas_mk=s2.id_kelas_mk
group by prodi,id_program_studi
order by id_program_studi");

$smarty->assign('T_PRODI1', $prodi1);

$smarty->display('proses-cekal-utssp.tpl');
}
?>
