<?php
//error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$db = new MyOracle();
$db1 = new MyOracle();
$db2 = new MyOracle();
$db3 = new MyOracle();

//CEK


$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');

$id_pengguna= $user->ID_PENGGUNA; 
$kdfak=$user->ID_FAKULTAS;
//$kdfak=4; 
$smarty->assign('FAK',$kdfak);
//smt aktif
$smtaktif=getvar("select id_semester,thn_akademik_semester,nm_semester from semester where status_aktif_sp='True'");	
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);
$id_semester=$smtaktif['ID_SEMESTER'];

//smt bayar
$smtbayar=getvar("select id_semester from semester where status_aktif_semester='True'");	
$id_semester_bayar=$smtbayar['ID_SEMESTER'];

//last smt
if($smtaktif['NM_SEMESTER']=="Ganjil") {
		$last_smt=getvar("select id_semester from semester where thn_akademik_semester=$smtaktif[THN_AKADEMIK_SEMESTER]-1 and nm_semester='Genap'");	
		
	}else if($smtaktif['NM_SEMESTER']=="Genap") {
		$last_smt=getvar("select id_semester from semester where thn_akademik_semester=$smtaktif[THN_AKADEMIK_SEMESTER] and nm_semester='Ganjil'");	
		
	}


$nim_mhs= $_REQUEST['nim'];
$smarty->assign('nim',$nim_mhs);

if ($nim_mhs !='') {
$detailmhs= getvar("select MAHASISWA.id_mhs,MAHASISWA.id_program_studi,nm_pengguna,nm_jenjang||'-'||nm_program_studi as nm_program_studi,nim_mhs,jenjang.id_jenjang from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where nim_mhs like '$nim_mhs' and program_studi.id_fakultas=$kdfak");

//cek
$cek= getvar("select status_krs from program_studi where id_program_studi='$detailmhs[ID_PROGRAM_STUDI]'");
if (($cek['STATUS_KRS']== 5) || ($cek['STATUS_KRS']== 4) || ($cek['STATUS_KRS']== 6)){
//if (($cek['STATUS_KRS']<> '11') && ($cek['STATUS_KRS']<>'33')){

$smarty->assign('nim', $detailmhs['NIM_MHS']);
$smarty->assign('nama', $detailmhs['NM_PENGGUNA']);
$smarty->assign('prodi', $detailmhs['NM_PROGRAM_STUDI']);

		
//get IPS
$datamhs= getvar("select id_mhs, sum(kredit_semester) as sks_total, sum((bobot*kredit_semester)) as bobot_total, 
case when sum(bobot*kredit_semester)=0 then 0 else 
round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips
from 
(select m.id_mhs, 
case when (a.nilai_angka <= 0 or a.nilai_huruf = 'E' or a.nilai_angka is null or a.nilai_huruf is null) and d.status_mkta in (1,2) then 0
else d.kredit_semester end as kredit_semester,
case when a.nilai_huruf is null then 'E' else a.nilai_huruf end as nilai_huruf,
case when e.nilai_standar_nilai is null then 0 else e.nilai_standar_nilai end as bobot
from pengambilan_mk a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
left join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where a.id_semester=$last_smt[ID_SEMESTER] and a.status_apv_pengambilan_mk='1' and m.nim_mhs='$nim_mhs'
)
group by id_mhs");

$smarty->assign('ips',$datamhs['IPS']);

//get IPK
$dataipk= getvar("select id_mhs, sum(sks) as ttl_sks, round((sum((sks * bobot)) / sum(sks)), 2) as ipk
from
(select id_mhs, kd_mata_kuliah as kode, nm_mata_kuliah as nama, kredit_semester as sks, min(nilai_huruf) as nilai, max(nilai_standar_nilai) as bobot
from 
(select m.id_mhs, c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai
from pengambilan_mk a
join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
join mahasiswa m on a.id_mhs=m.id_mhs
where a.status_apv_pengambilan_mk = 1 and a.status_hapus=0 and a.nilai_huruf<>'E' and a.nilai_huruf is not null and a.id_semester<>$id_semester and m.nim_mhs='$nim_mhs')
group by id_mhs, kd_mata_kuliah, nm_mata_kuliah, kredit_semester)
group by id_mhs");

$smarty->assign('ttl_sks',$dataipk['TTL_SKS']);
$smarty->assign('ipk',$dataipk['IPK']);


//paket sp kedokteran
$paket=getvar("select status_paket, group_semester
from pengambilan_mk
left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join semester on semester.id_semester = pengambilan_mk.id_semester
where pengambilan_mk.id_mhs='$detailmhs[ID_MHS]' and pengambilan_mk.id_semester=$id_semester and status_apv_pengambilan_mk=1
group by status_paket, group_semester");

if ($kdfak==4 or $kdfak==10 or $kdfak==13) {
$jatahsks=10; } else
{$jatahsks=8;}

if($kdfak==1 and $paket['STATUS_PAKET'] == 'sp1'){
	$jatahsks=10;
}elseif($kdfak==1 and $paket['STATUS_PAKET'] == 'sp3'){
	$jatahsks=10;
}elseif($kdfak==1 and $paket['STATUS_PAKET'] == 'sp2' and $paket['GROUP_SEMESTER'] == 'Genap'){
	$jatahsks=10;
}elseif($kdfak==1 and $paket['STATUS_PAKET'] == 'sp2' and $paket['GROUP_SEMESTER'] == 'Ganjil'){
	$jatahsks=16;
}

$smarty->assign('sks_maks',$jatahsks);


//sks max
$sksambil=getvar("select sum(kurikulum_mk.kredit_semester) as ttlsks from pengambilan_mk
left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
where pengambilan_mk.id_mhs='$detailmhs[ID_MHS]' and pengambilan_mk.id_semester=$id_semester and status_apv_pengambilan_mk=1");
$smarty->assign('sksdisetujui', $sksambil['TTLSKS']);


$status = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'krs';
//echo $status;

switch($status) {

case 'add':
		 // pilih
		$smarty->assign('disp1','block');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		
		$id_kelas_mk= $_GET['id_kelas_mk'];
		$sks= $_GET['sks'];
		$kaps= $_GET['kaps'];
		
		//get jumlah pst
		$cekpst= getvar("select count ( distinct id_mhs) as pst from pengambilan_mk where id_kelas_mk=$id_kelas_mk and status_pengambilan_mk=1 and id_semester=$id_semester");
		
//tambahan dayat
//cek pembayaran
$cek_pembayaran = getvar("select count(*) as jumlah_bayar from pembayaran where id_mhs='$detailmhs[ID_MHS]' 
							and (tgl_bayar is not null or id_status_pembayaran <> 2) and id_semester=$id_semester_bayar");
							
$cek_pembayaran_cmhs = getvar("select count(*) as jumlah_bayar 
								from pembayaran_cmhs where id_c_mhs in (select id_c_mhs from mahasiswa where id_mhs='$detailmhs[ID_MHS]')
								and tgl_bayar is not null and id_semester=$id_semester_bayar");

if ($cek_pembayaran['JUMLAH_BAYAR'] > 0 or $cek_pembayaran_cmhs['JUMLAH_BAYAR'] > 0){								


//cek sidik jari
$cek_sidik = getvar("SELECT FINGERPRINT_MAHASISWA.FINGER_DATA AS A, FINGERPRINT_CMHS.FINGER_DATA AS B
					FROM FINGERPRINT_MAHASISWA
					LEFT JOIN MAHASISWA ON MAHASISWA.NIM_MHS = FINGERPRINT_MAHASISWA.NIM_MHS
					LEFT JOIN CALON_MAHASISWA_BARU ON CALON_MAHASISWA_BARU.NIM_MHS = MAHASISWA.NIM_MHS
					LEFT JOIN FINGERPRINT_CMHS ON FINGERPRINT_CMHS.ID_C_MHS = MAHASISWA.ID_C_MHS
					WHERE MAHASISWA.NIM_MHS = '".$nim_mhs."'");

if ($cek_sidik['A'] <> "" or $cek_sidik['B'] <> "") {

	//cek cekal akademik
$cekal = getvar("SELECT STATUS_CEKAL 
				FROM MAHASISWA
				LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
				WHERE STATUS_PENGGUNA.STATUS_AKTIF = 1 AND NIM_MHS = '".$nim_mhs."'");		

if($cekal['STATUS_CEKAL'] <> '' or $cekal['STATUS_CEKAL'] <> '0'){
		
		if($kdfak == 4 and $detailmhs['ID_JENJANG'] == 5){
			$bonus_sks = 0;
		}elseif($kdfak == 4 and $detailmhs['ID_JENJANG'] == 1){
			$bonus_sks = 0;
		}else{
			$bonus_sks = 0;
		}
		
		

		if (($cekpst['PST']+1) <= $kaps) {
		if (($sksambil['TTLSKS']+$sks) <= $jatahsks+$bonus_sks) {
			// mahasiswa profesi -> dayat
			//ambil id_kur
					$id_kur=getvar("select id_kurikulum_mk,id_mata_kuliah from kurikulum_mk kmk1
									where kmk1.id_program_studi='$detailmhs[ID_PROGRAM_STUDI]' and kmk1.id_mata_kuliah in
									(select id_mata_kuliah from mata_kuliah where kd_mata_kuliah in
									(select c.kd_mata_kuliah from kelas_mk a, kurikulum_mk b, mata_kuliah c
									where a.id_kurikulum_mk=b.id_kurikulum_mk and a.id_kelas_mk='".$id_kelas_mk."' 
									and c.id_mata_kuliah=b.id_mata_kuliah))");
			//cek prasyarat
			
			$lolos_prasyarat=true; $kode_mk_prasyarat="";
			$kueri = "select distinct id_prasyarat_mk from prasyarat_mk where id_kurikulum_mk='".$id_kur['ID_KURIKULUM_MK']."' ";
			//echo $kueri;
			$result = $db->Query($kueri)or die("salah kueri cek prayarat");
			
			while($r = $db->FetchRow()) {
				$lolos_prasyarat=true;
				$id_prasyarat = $r[0];

				$kueri2 = "select id_kurikulum_mk, case when (min_nilai_huruf='D') then 1 else
		           0 end as min_nilai_huruf
		           from group_prasyarat_mk where id_prasyarat_mk='".$id_prasyarat."' "; 
				//echo $kueri2;
				$result2 = $db1->Query($kueri2)or die("salah kueri 169");
				while($r2 = $db1->FetchRow()) {
					$ambil_kuri = $r2[0];
					$ambil_nilai = $r2[1];
					$kueri3 = "select count(*) from 
      					(select case when (b.nilai_huruf='A') then 4 else
      					case when (b.nilai_huruf='AB') then 3.5 else
      					case when (b.nilai_huruf='B') then 3 else
      					case when (b.nilai_huruf='BC') then 2.5 else
      					case when (b.nilai_huruf='C') then 2 else
      					case when (b.nilai_huruf='CD') then 1.5 else
      					case when (b.nilai_huruf='D') then 1 else 
					case when (b.nilai_huruf is null) then 0 else 0
      					end end end end end end end end as nilai_huruf
					from kurikulum_mk a, pengambilan_mk b
					where a.id_kurikulum_mk=b.id_kurikulum_mk and a.id_kurikulum_mk='".$ambil_kuri."' and b.id_mhs='".$detailmhs['ID_MHS']."' and b.id_semester!='".$id_semester."' ) 
					where nilai_huruf >= '".$ambil_nilai."'";

				$result3 = $db2->Query($kueri3)or die("salah kueri 315");
				$r3 = $db2->FetchRow();
				//echo $r3[0];
				if($r3[0] > 0) {
					// ada
				}else if($r3[0] == 0) {
					// ambil kode mtk
				$kueri4 = "select b.kd_mata_kuliah
				from kurikulum_mk a, mata_kuliah b
				where a.id_mata_kuliah=b.id_mata_kuliah and a.id_kurikulum_mk='".$ambil_kuri."'";
				$result4 = $db3->Query($kueri4)or die("salah kueri 325");
				while($r4 = $db3->FetchRow()) {
					$kode_mk_prasyarat = $r4[0];
				}
				$lolos_prasyarat = false;
				//break;
				}
				}
					if($lolos_prasyarat==true) {
					break;
				}
				}
				if($lolos_prasyarat==false) {
				//$lanjut = false
				echo '<script>alert("Tidak lolos pada prasyarat ".$kode_mk_prasyarat."\n")</script>';
				}
				else {
									
					InsertData("insert into pengambilan_mk (id_mhs,id_semester,status_pengambilan_mk,status_apv_pengambilan_mk,id_kelas_mk,id_kurikulum_mk)
					values ('$detailmhs[ID_MHS]','$id_semester','9','1','$id_kelas_mk','".$id_kur['ID_KURIKULUM_MK']."')");
					
					//echo "insert into pengambilan_mk (id_mhs,id_semester,status_pengambilan_mk,status_apv_pengambilan_mk,id_kelas_mk,id_kurikulum_mk)
					//values ('$detailmhs[ID_MHS]','$id_semester','9','1','$id_kelas_mk','".$id_kur['ID_KURIKULUM_MK']."')";
				}
				
		} else {
			 echo '<script>alert("SKS Anda Melebihi Batas Maksimum yang Ditentukan")</script>';
		}
		} else
		{
			 echo '<script>alert("Kapasitas Kelas Tidak Cukup")</script>';
		}
    
	 }else{
	 	echo '<script>alert("Cekal Akademik")</script>';
	 } 
	}
	else{
		 echo '<script>alert("Belum melakukan sidik jari")</script>';
	}
}else{
	 echo '<script>alert("Data pembayaran semester ini masih kosong")</script>';
}

	break;
	
case 'krs':
		 // pilih
		
        break; 
   
case 'del':
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block');
		

	   $id_pengambilan_mk= $_GET['idpmk'];
	
       deletedata("delete from nilai_mk where id_pengambilan_mk=$id_pengambilan_mk");
	   deletedata("delete from pengambilan_mk where id_pengambilan_mk=$id_pengambilan_mk");

        break;   
}

	
//dayat =  and status_apv_pengambilan_mk=1 -> saya hapus
$mhs_status1=getData("select krs_prodi.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,nm_jadwal_hari,nm_jadwal_jam, 
sum(case when status_pengambilan_mk>=1 then 1 else 0 end) as pst, kapasitas_kelas_mk from krs_prodi
left join pengambilan_mk on krs_prodi.id_kelas_mk=pengambilan_mk.id_kelas_mk and krs_prodi.id_semester=pengambilan_mk.id_semester 
left join kelas_mk on krs_prodi.id_kelas_mk=kelas_mk.id_kelas_mk and krs_prodi.id_semester=kelas_mk.id_semester
left join nama_kelas on kelas_mk.no_kelas_mk=id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
where krs_prodi.id_program_studi='$detailmhs[ID_PROGRAM_STUDI]' and krs_prodi.id_semester=$id_semester  and 
krs_prodi.id_kelas_mk not in (select id_kelas_mk from pengambilan_mk where id_mhs in (
select id_mhs from mahasiswa where nim_mhs='$nim_mhs') and id_semester=$id_semester)
GROUP BY krs_prodi.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,nm_jadwal_hari,nm_jadwal_jam,kapasitas_kelas_mk
order by kd_mata_kuliah,nama_kelas");


$smarty->assign('T_KRS', $mhs_status1);

$data_mhs_krs=getData("select id_pengambilan_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,nm_jadwal_hari,nm_jadwal_jam, 
status_pengambilan_mk,case when (status_apv_pengambilan_mk=1) then 'DiSetujui' else 'Blm Disetujui' end as status, (jam_mulai||':'||menit_mulai||' - '||jam_selesai||':'||menit_selesai) as jam
 from pengambilan_mk
left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk
left join nama_kelas on kelas_mk.no_kelas_mk=id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
where pengambilan_mk.id_mhs in (select id_mhs from mahasiswa where nim_mhs='$nim_mhs') and pengambilan_mk.id_semester=$id_semester order by kd_mata_kuliah,nama_kelas");


$smarty->assign('T_AMBIL', $data_mhs_krs);

$sksambil=getvar("select sum(kurikulum_mk.kredit_semester) as ttlsks from pengambilan_mk
left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
where pengambilan_mk.id_mhs='$datamhs[ID_MHS]' and pengambilan_mk.id_semester=$id_semester and status_apv_pengambilan_mk=1");
$smarty->assign('sksdisetujui', $sksambil['TTLSKS']);



	
} else
{
	 echo '<script>alert("Sudah bukan waktunya KRS-SP")</script>';
}
}
$smarty->display('akademik-krs-sp.tpl');
?>
