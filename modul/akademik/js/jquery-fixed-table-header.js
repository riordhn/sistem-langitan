$.fn.fixedTableHeader = function () {
    var t = $(this);

    var tableOffset = t.offset().top;
    var fixed_table = $('<table></table>').css({ 'display':'none', 'position':'fixed', 'top':'0' });
    t.parent().append(fixed_table.append(t.find("thead").clone()));

    fixed_table.find("th").each(function (i) {
        $(this).width(t.find("th").eq(i).width() + 1);
    });

    $(window).bind("scroll", function () {
        var offset = $(this).scrollTop();
        if (offset >= tableOffset && fixed_table.is(":hidden")) {
            fixed_table.show();
        }
        else if (offset < tableOffset) {
            fixed_table.hide();
        }
    });
    return t;
}

$(function(){
	positionFooter(); 
	function positionFooter(){
		$("#pageFooterOuter").css({position: "absolute",top:($(window).scrollTop()+$(window).height()-$("#pageFooterOuter").height())+"px"})	
	}
 
	$(window)
		.scroll(positionFooter)
		.resize(positionFooter)
});