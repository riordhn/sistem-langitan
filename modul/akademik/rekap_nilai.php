<?php
require 'common.php';
require 'includes/function.php';

$akademik = new Akademik($db);  // lokasi /includes/Akademik.class.php

$kdprodi = $user->ID_PROGRAM_STUDI;
$id_pengguna = $user->ID_PENGGUNA;

$db->Query(
	"SELECT fakultas.id_fakultas, nm_fakultas from pegawai 
	left join unit_kerja on pegawai.id_unit_kerja=unit_kerja.id_unit_kerja 
	left join fakultas on unit_kerja.id_fakultas=fakultas.id_fakultas 
	where pegawai.id_pengguna = {$id_pengguna}");
$fak = $db->FetchAssoc();

$kdfak = $fak['ID_FAKULTAS'];
$nmfak = $fak['NM_FAKULTAS'];
$smarty->assign('FAK', $kdfak);
$smarty->assign('NM_FAK', $nmfak);

$depan = time();
$belakang = strrev(time());

if (isset($_POST['action']) == 'view')
{
	$nim = $_POST['nim'];

	if (isset($_POST['id_pengambilan_mk']))
	{
		// $db->Query("update pengambilan_mk set status_hapus=mod(1-status_hapus,2) where id_pengambilan_mk in {$_POST['id_pengambilan_mk']}");
	}
	
	$smarty->assign('NIMX', $nim);

	$db->Query("SELECT ': '||nim_mhs||' - '||nm_pengguna as nm_mhs from mahasiswa left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna where nim_mhs='" . $nim . "' and id_program_studi in (select id_program_studi from program_studi where id_fakultas='" . $kdfak . "')");
	$datamhs = $db->FetchAssoc();
	$smarty->assign('MHS', $datamhs);
	
	$db->Query(
		"SELECT mhs.nim_mhs,mhs.id_mhs, p.nm_pengguna, j.nm_jenjang, ps.nm_program_studi, mhs.status_akademik_mhs, sp.status_aktif
		from mahasiswa mhs 
		left join pengguna p on mhs.id_pengguna=p.id_pengguna 
		left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
		left join jenjang j on ps.id_jenjang=j.id_jenjang
		left join status_pengguna sp on sp.id_status_pengguna = mhs.status_akademik_mhs
		where mhs.nim_mhs = '{$nim}' and mhs.id_program_studi in (select id_program_studi from program_studi where id_fakultas='{$kdfak}')"
	);
	$biomhs = $db->FetchAssoc();
	$smarty->assign('T_MHS', $biomhs);

	$pengambilan_mk_set = $akademik->list_pengambilan_mk_mhs($biomhs['ID_MHS']);
	$smarty->assign('T_MK', $pengambilan_mk_set);
	
	$ipk = $akademik->get_ipk_mhs($biomhs['ID_MHS']);
	$smarty->assign('T_IPK', $ipk);

	$link = 'proses/daftarnilai-cetak.php?' . paramEncrypt($depan . '=' . $belakang . $depan . '&cetak=' . $biomhs['ID_MHS'] . '&' . $belakang . '=' . $depan . $belakang) . '';
	$smarty->assign('LINK', $link);
	$link1 = 'proses/daftarnilai-cetak-eng.php?' . paramEncrypt($depan . '=' . $belakang . $depan . '&cetak=' . $nim . '&' . $belakang . '=' . $depan . $belakang) . '';
	$smarty->assign('LINK1', $link1);
	$link2 = 'proses/daftarnilai-cetak-eng-dean.php?' . paramEncrypt($depan . '=' . $belakang . $depan . '&cetak=' . $nim . '&' . $belakang . '=' . $depan . $belakang) . '';
	$smarty->assign('LINK2', $link2);
	$link3 = 'proses/daftarnilai-cetak-view.php?' . paramEncrypt($depan . '=' . $belakang . $depan . '&cetak=' . $nim . '&' . $belakang . '=' . $depan . $belakang) . '';
	$smarty->assign('LINK3', $link3);
}
else
{
	
}

$smarty->display('rekap_nilai.tpl');