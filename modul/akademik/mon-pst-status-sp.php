<?php
require('common.php');
require_once ('ociFunction.php');
//require('../../../config.php');
$db = new MyOracle();

error_reporting (E_ALL & ~E_NOTICE);
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$id_pengguna= $user->ID_PENGGUNA;

$kdfak=$user->ID_FAKULTAS;
$smarty->assign('kdfakul',$kdfak);

if($kdfak==4)
{
$smtaktif=getvar("select id_semester from semester where status_aktif_sp='True'");
$smtaktifcek=getvar("select id_semester from semester where status_aktif_semester='True'");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);

$smt1 = isSet($_POST['smt']) ? $_POST['smt'] : $smtaktif['ID_SEMESTER'];


if ($_GET['action']=='detail')
{
	$id_prodi=$_GET['idprodi'];
	$st=$_GET['st'];
	//echo $id_prodi;
	//echo $st;
	
	if ($st==1)
	{
	 $detailprodi=getData("select status_sp.id_mhs,mahasiswa.nim_mhs,nm_pengguna,
						nm_jenjang||'-'||nm_program_studi as prodi,mahasiswa.id_program_studi,mahasiswa.status
						from status_sp
						left join mahasiswa on status_sp.id_mhs=mahasiswa.id_mhs
						left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
						left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
						left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
						where status_sp.id_semester=$smt1 and id_fakultas=$kdfak and mahasiswa.status='R'
						and mahasiswa.id_program_studi=$id_prodi");
	}
	if ($st==2)
	{
	 $detailprodi=getData("select status_sp.id_mhs,mahasiswa.nim_mhs,nm_pengguna,
						nm_jenjang||'-'||nm_program_studi as prodi,mahasiswa.id_program_studi,mahasiswa.status
						from status_sp
						left join mahasiswa on status_sp.id_mhs=mahasiswa.id_mhs
						left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
						left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
						left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
						where status_sp.id_semester=$smt1 and id_fakultas=$kdfak and mahasiswa.status='AJ'
						and mahasiswa.id_program_studi=$id_prodi");
	}
	
	
	
	$smarty->assign('T_DETPRODI', $detailprodi);
	
	$smarty->assign('disp1','none');
	$smarty->assign('disp2','block');
} 

$cari1=$_POST['status'];
$nim=$_POST['nim'];

if ($nim!="" or $nim != null )
{
	
	//cek bayar genap
	$cek_pembayaran = getvar("select count(*) as jumlah_bayar from pembayaran where id_mhs
					  in (select id_mhs from mahasiswa where nim_mhs='$nim')
					  and id_status_pembayaran in (1, 3) and id_semester='$smtaktifcek[ID_SEMESTER]'");
	
	if ($cek_pembayaran['JUMLAH_BAYAR'] > 0){
	//cek cekal
	$cekal = getvar("SELECT STATUS_CEKAL 
				FROM MAHASISWA
				LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
				WHERE STATUS_PENGGUNA.STATUS_AKTIF = 1 AND NIM_MHS = '".$nim_mhs."'");		

	if($cekal['STATUS_CEKAL'] <> '' or $cekal['STATUS_CEKAL'] <> '0'){
	//proses daftar
		$mhs=getvar("select id_mhs from mahasiswa where nim_mhs='$nim'");
		InsertData("insert into status_sp (id_mhs,id_semester) values ('$mhs[ID_MHS]',$smt1)");
		echo '<script>alert("Data Telah Masuk")</script>';
	} else {
		echo '<script>alert("Cekal Akademik")</script>';
	}
	} else {
		echo '<script>alert("Belum Regristrasi")</script>';
	}
	
}

$prodi1=getData("select prodi,s1.id_program_studi,coalesce(r1,0) as r1,
coalesce(aj1,0) as aj1,coalesce(r2,0) as r2,
coalesce(aj2,0) as aj2,coalesce(r1-r2,0) as belumr,
coalesce(aj1-aj2,0) as belumaj
from (
select nm_jenjang||'-'||nm_program_studi as prodi, program_studi.id_program_studi
from program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where id_fakultas=$kdfak 
order by nm_jenjang,nm_program_studi) s1
left join (
select id_program_studi,
sum(case when mahasiswa.status='R' then 1 else 0 end) as r1,
sum(case when mahasiswa.status='AJ' then 1 else 0 end) as aj1
from status_sp
join mahasiswa on status_sp.id_mhs=mahasiswa.id_mhs
where status_sp.id_semester=$smt1
group by id_program_studi)s2 on s1.id_program_studi=s2.id_program_studi
left join (
select program_studi.id_program_studi,
sum(case when mahasiswa.status='R' then 1 else 0 end) as r2,
sum(case when mahasiswa.status='AJ' then 1 else 0 end) as aj2
from pengambilan_mk
join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
where pengambilan_mk.id_semester=$smt1 and id_fakultas=$kdfak
group by program_studi.id_program_studi)s3 on s1.id_program_studi=s3.id_program_studi
order by prodi");

$smarty->assign('R_PRODI1', $prodi1);


$smarty->display('mon-pst-status-sp.tpl');
} else
{
	echo '<script>alert("Maaf Fitur ini Khusus FEB")</script>';
}
?>
