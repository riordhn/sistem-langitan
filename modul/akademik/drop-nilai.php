<?php
//error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

//CEK
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$id_pengguna= $user->ID_PENGGUNA; 
$kdfak=$user->ID_FAKULTAS; 
$smarty->assign('FAK',$kdfak);

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True' and id_perguruan_tinggi = '{$id_pt_user}'");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);

$id_semester=$smtaktif['ID_SEMESTER'];


$nim_mhs= $_REQUEST['nim'];
$smarty->assign('nim',$nim_mhs);

if ($_POST['action']=='drop') {
			$id_pengambilan_mk=$_POST['id_peng_mk'];
			UpdateData("update pengambilan_mk set flagnilai='0', status_hapus=1, keterangan='Drop Nilai Yudisium' where id_pengambilan_mk=$id_pengambilan_mk");
}

if ($_POST['action']=='batal') {
			$id_pengambilan_mk=$_POST['id_peng_mk'];
			UpdateData("update pengambilan_mk set flagnilai='1', status_hapus=0, keterangan='Batal Drop Nilai' where id_pengambilan_mk=$id_pengambilan_mk");
			$smarty->assign('disp1','none');
			$smarty->assign('disp2','block');
}

if ($_POST['action']=='drop-krs') {
			$id_pengambilan_mk=$_POST['id_peng_mk'];
			$id_kelas_mk=$_POST['id_kelas_mk'];
			$id_mhs=$_POST['id_mhs'];
			$id_semester=$_POST['id_semester'];
			$tgl_ubah=$_POST['tgl_ubah'];
			$id_kurikulum_mk=$_POST['id_kurikulum_mk'];

			InsertData("INSERT INTO LOG_DEL_PMK (ID_PENGAMBILAN_MK, ID_KELAS_MK, ID_MHS, ID_SEMESTER, TGL_UBAH, ID_PENGGUNA, ID_KURIKULUM_MK)
								VALUES ('{$id_pengambilan_mk}', '{$id_kelas_mk}', '{$id_mhs}', '{$id_semester}', to_date('$tgl_ubah', 'YYYY-MM-DD'), '{$id_pengguna}', '{$id_kurikulum_mk}')") or die ('error insert log');

			deleteData("delete from pengambilan_mk where id_pengambilan_mk={$id_pengambilan_mk}");

			$smarty->assign('disp1','none');
			$smarty->assign('disp2','block');
}

/*
$datamhs= getData("select a.id_mhs,b.nim_mhs,h.nm_pengguna,g.nm_jenjang||'-'||f.nm_program_studi as prodi,sum(a.kredit_semester) as skstotal,
		round(sum(a.kredit_semester*(case a.nilai_huruf 
		when 'A' then 4 
		when 'AB' then 3.5 
		when 'B' then 3
		when 'BC' then 2.5
		when 'C' then 2
		when 'D' then 1
		end))/sum(a.kredit_semester),2) as IPK
		from
		(
		select a.id_mhs,e.nm_mata_kuliah,nvl(e.kredit_semester,d.kredit_semester) kredit_semester,a.nilai_huruf from (
		select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
		from pengambilan_mk a 
		left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
		left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
		where a.nilai_huruf !='E' and a.nilai_huruf is not null 
		and a.id_semester is not null and a.status_hapus=0 and a.status_apv_pengambilan_mk=1
		) a
		left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
		left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
		where rangking=1 and id_mhs in (select id_mhs from mahasiswa where nim_mhs='$nim_mhs')
		) a
		left join mahasiswa b on a.id_mhs=b.id_mhs
    left join pengguna h on b.id_pengguna=h.id_pengguna
		left join program_studi f on b.id_program_studi=f.id_program_studi
    left join jenjang g on f.id_jenjang=g.id_jenjang
		where a.id_mhs  in (select id_mhs from mahasiswa where nim_mhs='$nim_mhs') and f.id_fakultas=$kdfak
   group by a.id_mhs, b.nim_mhs, h.nm_pengguna, g.nm_jenjang||'-'||f.nm_program_studi");
*/
$datamhs= getData("select id_mhs,nim_mhs,nm_pengguna,prodi,
sum(sks) as skstotal, round((sum((bobot*sks))/sum(sks)),2) as ipk,sum(bobot*sks) as bobot 
from
(
select a.id_mhs,m.nim_mhs,pg.nm_pengguna,j.nm_jenjang||'-'||ps.nm_program_studi as prodi,
e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,d.kredit_semester sks,a.nilai_huruf nilai,
f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah,d.kredit_semester order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join pengguna pg on m.id_pengguna=pg.id_pengguna
left join program_studi ps on m.id_program_studi=ps.id_program_studi
left join jenjang j on ps.id_jenjang=j.id_jenjang
where rangking=1 and m.nim_mhs='$nim_mhs' and ps.id_fakultas=$kdfak
)
group by id_mhs, nim_mhs,nm_pengguna,prodi");

   
$smarty->assign('T_DATAMHS', $datamhs);

$detailnilai= getData("select id_pengambilan_mk,thn_akademik_semester||'-'||nm_semester as smt,kd_mata_kuliah,nm_mata_kuliah,
kurikulum_mk.kredit_semester,nilai_huruf from pengambilan_mk
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join semester on pengambilan_mk.id_semester=semester.id_semester
where id_mhs in (select id_mhs from mahasiswa join pengguna on pengguna.id_pengguna = mahasiswa.id_pengguna where mahasiswa.nim_mhs='$nim_mhs' and pengguna.id_perguruan_tinggi = '{$id_pt_user}') and pengambilan_mk.status_hapus=0
order by thn_akademik_semester,nm_semester,kd_mata_kuliah");

$smarty->assign('T_NILAI', $detailnilai);

$nilaibatal= getData("select id_pengambilan_mk,thn_akademik_semester||'-'||nm_semester as smt,kd_mata_kuliah,nm_mata_kuliah,
kurikulum_mk.kredit_semester,nilai_huruf,pengambilan_mk.keterangan, 
pengambilan_mk.id_kelas_mk, pengambilan_mk.id_mhs, pengambilan_mk.id_semester, to_char(pengambilan_mk.tgl_ubah, 'YYYY-MM-DD') as tgl_ubah, pengambilan_mk.id_kurikulum_mk
from pengambilan_mk
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join semester on pengambilan_mk.id_semester=semester.id_semester
where id_mhs in (select id_mhs from mahasiswa join pengguna on pengguna.id_pengguna = mahasiswa.id_pengguna where mahasiswa.nim_mhs='$nim_mhs' and pengguna.id_perguruan_tinggi = '{$id_pt_user}') and pengambilan_mk.status_hapus=1
order by thn_akademik_semester,nm_semester,kd_mata_kuliah");

$smarty->assign('T_DROP', $nilaibatal);

$smarty->display('drop-nilai.tpl');

?>
