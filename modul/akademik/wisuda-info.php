<?php
require 'config.php';

$kdprodi= $user->ID_PROGRAM_STUDI;
$id_pengguna= $user->ID_PENGGUNA; 
$id_fakultas = $user->ID_FAKULTAS;

if(isset($_POST['nim'])){		
	
		$ipk = "LEFT JOIN (select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
				from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
				c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
				row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
				count(*) over(partition by c.nm_mata_kuliah) terulang
					from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
					where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
					and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs 
					and h.id_program_studi = g.id_program_studi and h.id_fakultas = '$id_fakultas'
				) x
				where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
				group by id_mhs) C ON C.ID_MHS = MAHASISWA.ID_MHS ";
	
		$db->Query("SELECT MAHASISWA.NIM_MHS, NM_PENGGUNA, NM_JENJANG, NM_PROGRAM_STUDI, NM_FAKULTAS, 
								NO_IJASAH, TGL_BAYAR, PEMBAYARAN_WISUDA.BESAR_BIAYA, (SELECT SUM(PEMBAYARAN.BESAR_BIAYA) AS JML FROM PEMBAYARAN
									LEFT JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBAYARAN.ID_MHS
									LEFT JOIN DETAIL_BIAYA ON DETAIL_BIAYA.ID_DETAIL_BIAYA = PEMBAYARAN.ID_DETAIL_BIAYA
									WHERE (TGL_BAYAR IS NOT NULL OR PEMBAYARAN.ID_STATUS_PEMBAYARAN = 1) 
									AND ID_BIAYA = 115 AND PEMBAYARAN.ID_MHS = PEMBAYARAN_WISUDA.ID_MHS) AS CICILAN,
								LAHIR_IJAZAH, TO_CHAR(TGL_LULUS_PENGAJUAN, 'dd-mm-YYYY') as TGL_LULUS_PENGAJUAN, PERIODE_WISUDA.BESAR_BIAYA AS TARIF,
								(CASE WHEN KELAMIN_PENGGUNA = 1 THEN 'L' WHEN KELAMIN_PENGGUNA = 2 THEN 'P' ELSE '' END) AS KELAMIN_PENGGUNA,
								(CASE WHEN C.IPK IS NULL THEN PENGAJUAN_WISUDA.IPK ELSE C.IPK END) AS IPK, 
								(CASE WHEN C.SKS IS NULL THEN PENGAJUAN_WISUDA.SKS_TOTAL ELSE C.SKS END) AS SKS,TOTAL_SKP, PENGAJUAN_WISUDA.ELPT, MAHASISWA.STATUS, NM_JALUR,
								ALAMAT_ASAL_MHS, MOBILE_MHS, EMAIL_PENGGUNA, NM_TARIF_WISUDA, COALESCE(TO_CHAR(TGL_WISUDA, 'dd-mm-YYYY'), 'Belum Wisuda') as TGL_WISUDA
								 FROM MAHASISWA
								 LEFT JOIN PEMBAYARAN_WISUDA ON MAHASISWA.ID_MHS = PEMBAYARAN_WISUDA.ID_MHS
								 JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
								 JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
								 JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
								 JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG 
								 LEFT JOIN JALUR_MAHASISWA ON JALUR_MAHASISWA.ID_MHS = MAHASISWA.ID_MHS AND ID_JALUR_AKTIF = 1
								 LEFT JOIN JALUR ON JALUR.ID_JALUR = JALUR_MAHASISWA.ID_JALUR
								 LEFT JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PEMBAYARAN_WISUDA.ID_PERIODE_WISUDA
								 LEFT JOIN TARIF_WISUDA ON TARIF_WISUDA.ID_TARIF_WISUDA = PERIODE_WISUDA.ID_TARIF_WISUDA
								 LEFT JOIN PENGAJUAN_WISUDA ON PENGAJUAN_WISUDA.ID_MHS = PEMBAYARAN_WISUDA.ID_MHS 
								 LEFT JOIN (SELECT SUM(SKOR_KRP_KHP) AS TOTAL_SKP, ID_MHS FROM KRP_KHP GROUP BY ID_MHS)  KRP_KHP ON KRP_KHP.ID_MHS = MAHASISWA.ID_MHS 
								 $ipk
								WHERE MAHASISWA.NIM_MHS = '$_POST[nim]' AND FAKULTAS.ID_FAKULTAS = '$id_fakultas'
								ORDER BY FAKULTAS.ID_FAKULTAS, NM_JENJANG, NM_PROGRAM_STUDI, MAHASISWA.NIM_MHS, PEMBAYARAN_WISUDA.TGL_BAYAR");

		$data_mhs = $db->FetchAssoc();
		$smarty->assign('data_mhs', $data_mhs);
	
}

$smarty->display("wisuda-info.tpl");
?>
