<?php
/*
Yudi Sulistya 10/05/2012
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$kdprodi=$user->ID_PROGRAM_STUDI;

$id_pengguna= $user->ID_PENGGUNA;
$kdfak=$user->ID_FAKULTAS;
$smarty->assign('FAK', $kdfak);

$thnsmt=getData("select id_semester, tahun_ajaran||' - '||nm_semester as thn_smt from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester in (
select * from (select distinct thn_akademik_semester
from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester<=EXTRACT(YEAR FROM sysdate) order by thn_akademik_semester desc)
where rownum<=10) and nm_semester in ('Ganjil', 'Genap')
order by thn_akademik_semester desc, nm_semester desc");
$smarty->assign('T_THNSMT', $thnsmt);

$dataprodi=getData("select id_program_studi,nm_jenjang||'-' || nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_fakultas=$kdfak and status_aktif_prodi = 1");
$smarty->assign('T_PRODI', $dataprodi);

if (isset($_REQUEST['action'])=='view') {
$kdprodi = $_POST['kdprodi'];
$smarty->assign('PRODI', $kdprodi);

$smt = $_POST['kdthnsmt'];
$smarty->assign('SMT_AKAD', $smt);
$ipk_smt=getvar("select tahun_ajaran||' - '||nm_semester as thn_smt,thn_akademik_semester from semester where id_semester=$smt");
$smarty->assign('THN_AKAD', $ipk_smt['THN_SMT']);
$smarty->assign('TAHUN_SMT', $ipk_smt['THN_AKADEMIK_SEMESTER']);
//echo $smt;

$dataprodi1=getvar("select nm_jenjang||'-'||nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_program_studi=$kdprodi and status_aktif_prodi = 1");
$smarty->assign('NM_PRODI', $dataprodi1['PRODI']);

$smt_temp=getvar("select case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,thn_akademik_semester from semester where id_semester=$smt");
$smarty->assign('THN_HITUNG', $smt_temp['TAHUN']);

$jml_mhs=getvar("
select count(distinct(nim_mhs)) as jml_mhs from 
( 
select m.nim_mhs, p.nm_pengguna, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join pengguna p on p.id_pengguna=m.id_pengguna
where rangking=1 and m.id_program_studi=$kdprodi and a.id_semester in 
(select id_semester from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND nm_semester in ('Ganjil', 'Genap')
and thn_akademik_semester||(case when nm_semester='Ganjil' then 1 else 2 end)<=$smt)
and m.id_mhs in (select id_mhs from pengambilan_mk where id_semester=(select id_semester from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND nm_semester in ('Ganjil', 'Genap')
and thn_akademik_semester||(case when nm_semester='Ganjil' then 1 else 2 end)=$smt))
)
");
$smarty->assign('JML_MHS', $jml_mhs['JML_MHS']);


$mk=getData("select s1.id_mhs,s1.nim_mhs,upper(s1.nm_pengguna) as NM_PENGGUNA,
coalesce(sks_sem,0) as sks_sem,round(coalesce(ips,0),2)as ips,
coalesce(SKS_TOTAL_MHS,0) as SKS_TOTAL_MHS,round(coalesce(IPK_MHS,0),2)as IPK_MHS,
dosen,nip_dosen,nidn_dosen
from
(select id_mhs,nim_mhs,nm_pengguna,case when sum(bobot*kredit_semester)=0 then 0 else 
round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips,
sum(kredit_semester) as sks_sem
--case when id_fakultas=7 then sum(sksreal) else sum(kredit_semester) end as sks_sem 
from 
(select id_mhs,nim_mhs,nm_pengguna,id_kurikulum_mk,id_fakultas,kredit_semester, sksreal,min(nilai_huruf) as nilai, max(bobot) as bobot from
(select a.id_mhs,m.nim_mhs,pg.nm_pengguna,ps.id_fakultas,a.id_kurikulum_mk, 
case when (a.nilai_huruf = 'E' or a.nilai_huruf is null) 
and d.status_mkta in (1,2) then 0
else d.kredit_semester end as kredit_semester,d.kredit_semester as sksreal,
case when a.nilai_huruf is null then 'E' else a.nilai_huruf end as nilai_huruf,
case when e.nilai_standar_nilai is null then 0 else e.nilai_standar_nilai end as bobot
from pengambilan_mk a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join pengguna pg on m.id_pengguna=pg.id_pengguna 
left join program_studi ps on m.id_program_studi=ps.id_program_studi
left join semester s on a.id_semester=s.id_semester
where group_semester||thn_akademik_semester in
(select group_semester||thn_akademik_semester from semester where id_Semester=$smt)
and tipe_semester in ('UP','REG','RD') 
and a.status_apv_pengambilan_mk='1' and m.id_program_studi=$kdprodi and a.status_hapus=0 
and a.status_pengambilan_mk !=0)
group by id_mhs, nim_mhs,nm_pengguna,id_kurikulum_mk, id_fakultas, kredit_semester, sksreal
)
group by id_mhs,nim_mhs,nm_pengguna,id_fakultas)s1
left join
(select id_mhs, sum(kredit_semester) as SKS_TOTAL_MHS, 
round((sum((kredit_semester * nilai_standar_nilai)) / sum(kredit_semester)), 2) as IPK_MHS
from 
(select a.id_mhs, c.kd_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai,
case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,
row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a
join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
join semester smt on a.id_semester=smt.id_semester
join mahasiswa m on a.id_mhs=m.id_mhs
where a.status_apv_pengambilan_mk = 1 and (a.nilai_huruf<>'E' or a.nilai_huruf<>null) 
and m.id_program_studi=$kdprodi and a.status_hapus=0 
and a.status_pengambilan_mk !=0)
where tahun<='$smt_temp[TAHUN]' and rangking=1
group by id_mhs) s2 on s1.id_mhs=s2.id_mhs
left join 
(select distinct id_mhs,gelar_depan||' '||nm_pengguna||', '||gelar_belakang as dosen,nip_dosen,nidn_dosen from dosen_wali
join dosen on DOSEN_WALI.id_dosen=DOSEN.ID_DOSEN
join pengguna on DOSEN.id_pengguna=PENGGUNA.id_pengguna
where status_dosen_wali=1
and id_mhs in (select id_mhs from mahasiswa where id_program_studi=$kdprodi))s3 on s1.id_mhs=s3.id_mhs
");

$smarty->assign('T_MK', $mk);

}

$smarty->display('rekap-ipk.tpl');