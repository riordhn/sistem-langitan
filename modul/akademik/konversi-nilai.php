<?php

require('common.php');
require_once ('ociFunction.php');
$kd_fak = $user->ID_FAKULTAS;
$id_pengguna = $user->ID_PENGGUNA;

$smarty->assign('disp1', 'block');
$smarty->assign('disp2', 'none');


$smtaktif = getvar("select id_semester from semester where status_aktif_semester='True'");

if (($_GET['action']) == 'viewproses')
{
	$smarty->assign('disp1', 'none');
	$smarty->assign('disp2', 'block');

	$id_pengambilan_mk = $_GET['id_pmk'];
	$id_prodi = $_GET['id_prodi'];

	$datamk = getvar("select kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nilai_huruf,id_mhs,
					pengambilan_mk.id_semester,id_pengambilan_mk,pengambilan_mk.id_kurikulum_mk,
					thn_akademik_semester||'/'||group_semester||'/'||nm_semester as smt
					from pengambilan_mk
					left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
					left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
					left join semester on pengambilan_mk.id_semester=semester.id_semester
					where id_pengambilan_mk=$id_pengambilan_mk");
	$smarty->assign('datamk', $datamk);

	$kd_konversi = getData("select kd_mata_kuliah, nm_mata_kuliah, id_kurikulum_mk,kurikulum_mk.kredit_semester,tahun_kurikulum as tahun
						 from kurikulum_mk
						join kurikulum_prodi on kurikulum_mk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi 
						join mata_kuliah on mata_kuliah.id_mata_kuliah = kurikulum_mk.id_mata_kuliah
						where kurikulum_mk.id_program_studi=$id_prodi and (mata_kuliah.kd_mata_kuliah not in ('$datamk[KD_MATA_KULIAH]') 
						or upper(mata_kuliah.nm_mata_kuliah) not in upper('$datamk[NM_MATA_KULIAH]'))
						order by tahun_kurikulum,kd_mata_kuliah asc");
	$smarty->assign('kd_konversi', $kd_konversi);
}


if (($_POST['action']) == 'proses')
{
	if (strtoupper($_POST['nilai']) != 'A' and strtoupper($_POST['nilai']) != 'AB' and strtoupper($_POST['nilai']) != 'B' and strtoupper($_POST['nilai']) != 'BC' and strtoupper($_POST['nilai']) != 'C' and strtoupper($_POST['nilai']) != 'D' and strtoupper($_POST['nilai']) != 'E')
	{
		echo '<script>alert("Masukkan Nilai Huruf [A/AB/B/BC/C/D/E]")</script>';
	}
	else
	{
		$ip = $_SERVER['REMOTE_ADDR'];
		$id_pengambilan_mk = $_POST['id_pengambilan_mk'];

		InsertData("insert into log_konversi_nilai (id_pengguna, id_mhs, id_semester, id_pengambilan_mk, id_kurikulum_mk_lama, id_kurikulum_mk_baru, ip_address, tanggal, nilai_huruf_lama, nilai_huruf_baru)
		select '$id_pengguna', id_mhs, '$smtaktif[ID_SEMESTER]', id_pengambilan_mk, id_kurikulum_mk, '$_POST[kd_konversi]', '$ip', sysdate, nilai_huruf, upper('$_POST[nilai]')
		from pengambilan_mk where id_pengambilan_mk=$id_pengambilan_mk") or die('gagal 1');

		UpdateData("update pengambilan_mk set id_kurikulum_mk = '$_POST[kd_konversi]', nilai_huruf = upper('$_POST[nilai]'), keterangan = 'Konversi nilai'
				where id_pengambilan_mk=$id_pengambilan_mk") or die('gagal 2');
	}
}

$nim = $_REQUEST['nim'];
$smarty->assign('nim', $nim);

$nilai = getData("select kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nilai_huruf,
					id_pengambilan_mk,mahasiswa.id_program_studi,pengambilan_mk.id_kurikulum_mk,thn_akademik_semester||'/'||group_semester||'/'||nm_semester as smt
					from pengambilan_mk
					left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
					left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
					left join semester on pengambilan_mk.id_semester=semester.id_semester
					join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
					join pengguna on pengguna.id_pengguna = mahasiswa.id_pengguna and pengguna.id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI}
					where nim_mhs='$nim' and id_program_studi in(select id_program_studi from program_studi where id_fakultas=$kd_fak)
				    order by thn_akademik_semester,group_semester,nm_semester,kd_mata_kuliah");

$smarty->assign('T_NILAI', $nilai);


$smarty->display('konversi-nilai.tpl');