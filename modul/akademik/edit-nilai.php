<?php
require('common.php');
require_once ('ociFunction.php');
$kd_fak= $user->ID_FAKULTAS;
$id_pengguna= $user->ID_PENGGUNA; 

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');



$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True' and id_perguruan_tinggi = {$id_pt_user}");

if (($_GET['action'])=='viewproses') {
	$smarty->assign('disp1','none');
	$smarty->assign('disp2','block');

	$id_pengambilan_mk =$_GET['id_pmk'];
	$id_log =$_GET['id_log'];
	$smarty->assign('log',$id_log);
	
	$datamk=getvar("select kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nilai_huruf,id_mhs,
					pengambilan_mk.id_semester,id_pengambilan_mk,pengambilan_mk.id_kurikulum_mk,
					thn_akademik_semester||'/'||nm_semester as smt, (select count(*) from kelas_mk kmk where kmk.id_kelas_mk = pengambilan_mk.id_kelas_mk) ada_kelas, 
					pengguna.gelar_depan || ' ' || pengguna.nm_pengguna || ', '|| pengguna.gelar_belakang as nm_pengguna, pengguna.username
					from pengambilan_mk
					left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
					left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
					left join semester on pengambilan_mk.id_semester=semester.id_semester 
					left join kelas_mk on kelas_mk.id_kelas_mk = pengambilan_mk.id_kelas_mk
					left join pengampu_mk on pengampu_mk.id_kelas_mk = kelas_mk.id_kelas_mk
					left join dosen on dosen.id_dosen = pengampu_mk.id_dosen
					left join pengguna on pengguna.id_pengguna = dosen.id_pengguna 
					where id_pengambilan_mk=$id_pengambilan_mk");
	$smarty->assign('datamk', $datamk);
}


if(($_POST['action'])=='proses'){
	
	if(strtoupper($_POST['nilai']) != 'A' and strtoupper($_POST['nilai']) != 'AB' and strtoupper($_POST['nilai']) != 'B' and strtoupper($_POST['nilai']) != 'BC' and strtoupper($_POST['nilai']) != 'C' and strtoupper($_POST['nilai']) != 'D' and strtoupper($_POST['nilai']) != 'E'){
	echo '<script>alert("Masukkan Nilai Huruf [A/AB/B/BC/C/D/E]")</script>';
	}else{
	$ip = $_SERVER['REMOTE_ADDR'];
	$id_pengambilan_mk =$_POST['id_pengambilan_mk'];
	$id_log =$_POST['log'];
	$referensi =$_POST['catatan'];

	if($_POST['catatan_pilih'] == '0'){
		$referensi =$_POST['catatan'];
	}
	else{
		$referensi =$_POST['catatan_pilih'];
	}
	
	//log perubahan nilai
	/*InsertData("insert into log_update_nilai (id_pengguna,referensi,id_pengambilan_mk,ip_address,tgl_update,nilai_huruf_lama,nilai_huruf_baru,id_log_granted_nilai)
		select '$id_pengguna','$referensi',id_pengambilan_mk,'$ip', sysdate, nilai_huruf, upper('$_POST[nilai]'),$id_log
		from pengambilan_mk where id_pengambilan_mk=$id_pengambilan_mk") or die('gagal 1');*/
	InsertData("insert into log_update_nilai (id_pengguna,referensi,id_pengambilan_mk,ip_address,tgl_update,nilai_huruf_lama,nilai_huruf_baru)
		select '$id_pengguna','$referensi',id_pengambilan_mk,'$ip', sysdate, nilai_huruf, upper('$_POST[nilai]')
		from pengambilan_mk where id_pengambilan_mk=$id_pengambilan_mk") or die('gagal 1');
	
	//cek proses
	/*$datacek=getvar("select count(*) as ada from log_update_nilai
					where id_pengguna=$id_pengguna and id_pengambilan_mk=$id_pengambilan_mk
					and id_log_granted_nilai=$id_log and nilai_huruf_baru=upper('$_POST[nilai]')");*/
	$datacek=getvar("select count(*) as ada from log_update_nilai
					where id_pengguna=$id_pengguna and id_pengambilan_mk=$id_pengambilan_mk
					and nilai_huruf_baru=upper('$_POST[nilai]')");

	if ($datacek[ADA]>0)
	{
	//nilai di ubah
		if($_POST['status_kelas'] < 1){
			UpdateData("update pengambilan_mk set nilai_huruf=upper('$_POST[nilai]') where id_pengambilan_mk=$id_pengambilan_mk");
		}
		else{
			UpdateData("update pengambilan_mk set nilai_huruf=upper('$_POST[nilai]'), nilai_angka='$_POST[nilai_angka]' where id_pengambilan_mk=$id_pengambilan_mk");
		}
	//closed log_granted_nilai
	/*UpdateData("update log_granted_nilai set status=0 where id_log_granted_nilai=$id_log")
	or die ('<script>alert("Proses Gagal.. Mhn diCek..")</script>') ;*/
	echo '<script>alert("Proses Edit Nilai Telah Berhasil")</script>';
	} else
	{
	echo '<script>alert("Proses Edit Nilai GAGAL")</script>';
	}
	}
}

$nim =$_REQUEST['nim'];
$smarty->assign('nim', $nim);

$nilai=getData("select kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nilai_huruf,
					pengambilan_mk.id_pengambilan_mk,mahasiswa.id_program_studi,pengambilan_mk.id_kurikulum_mk,
					thn_akademik_semester||'/'||nm_semester as smt,log_granted_nilai.status,
					id_log_granted_nilai
					from pengambilan_mk
					left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
					left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
					left join semester on pengambilan_mk.id_semester=semester.id_semester
					left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
					left join log_granted_nilai on pengambilan_mk.id_pengambilan_mk=log_granted_nilai.id_pengambilan_mk
					and pengambilan_mk.id_mhs=log_granted_nilai.id_mhs and pengambilan_mk.id_semester=log_granted_nilai.id_semester
					and log_granted_nilai.status=1
					where nim_mhs='$nim' 
					and id_program_studi in(select id_program_studi from program_studi where id_fakultas=$kd_fak)
					order by thn_akademik_semester,group_semester,nm_semester,kd_mata_kuliah");
	
$smarty->assign('T_NILAI', $nilai);


$smarty->display('edit-nilai.tpl');

?>
