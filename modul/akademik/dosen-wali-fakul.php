<?php
/*
update by Yudi Sulistya on 29/12/2011
*/

error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

//$smarty->clearAllAssign();

//if (!isFindData("jadwal_kelas","id_kelas_mk=162")) {echo "a";}


$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$kdprodi=$user->ID_PROGRAM_STUDI;
$kdfak=$user->ID_FAKULTAS;
$smarty->assign('kdfak',$kdfak);
//$kdfak=getvar("select id_fakultas from program_studi where id_program_studi=$kdprodi");
$kddep=getvar("select id_departemen from program_studi where id_program_studi=$kdprodi");


$default=getvar("select angkatan from
(select distinct thn_angkatan_mhs as angkatan from mahasiswa
where id_program_studi=$kdprodi and status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
order by thn_angkatan_mhs)
where rownum=1");

//Untuk Fakultas Psikologi
if($kdfak==11){
$smt=getData("select id_semester, tahun_ajaran||' - '||nm_semester as smt from semester where id_perguruan_tinggi = {$id_pt_user} and thn_akademik_semester in (
select * from (select distinct thn_akademik_semester
from semester where id_perguruan_tinggi = {$id_pt_user} and thn_akademik_semester<=EXTRACT(YEAR FROM sysdate) order by thn_akademik_semester desc)
where rownum<=10) and nm_semester in ('Ganjil', 'Genap')
order by thn_akademik_semester desc, nm_semester desc");
$smarty->assign('T_ST', $smt);
} else {
$smt=getData("select id_semester, tahun_ajaran||' - '||nm_semester as smt from semester where id_perguruan_tinggi = {$id_pt_user} and nm_semester in ('Ganjil', 'Genap') order by thn_akademik_semester desc,nm_semester desc");
$smarty->assign('T_ST', $smt);
}


$smtaktif=getvar("select id_semester,nm_semester,thn_akademik_semester,case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun from semester where id_perguruan_tinggi = {$id_pt_user} and status_aktif_semester='True'");

$smt = isset($_POST['smt'])? $_POST['smt'] :$smtaktif['ID_SEMESTER'];
$smarty->assign('smtpil',$smt);
$smtpil=getvar("select nm_semester,thn_akademik_semester,case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun from semester where id_semester=$smt");
$smarty->assign('smtpiltahun',$smtpil['TAHUN']);
//get last smt
if($smtpil['NM_SEMESTER']=="Ganjil") {
		$last_smt=getvar("select id_semester from semester where thn_akademik_semester=$smtpil[THN_AKADEMIK_SEMESTER]-1 and nm_semester='Genap'");	
		
	}else if($smtpil['NM_SEMESTER']=="Genap") {
		$last_smt=getvar("select id_semester from semester where thn_akademik_semester=$smtpil[THN_AKADEMIK_SEMESTER] and nm_semester='Ganjil'");	
		
	}

$smarty->assign('last_smt',$last_smt['ID_SEMESTER']);



$jaf2=getData("select dosen.id_dosen,nip_dosen, trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nm_pengguna,
sum(case when ps.id_jenjang=1 then 1 else 0 end) as S1,sum(case when ps.id_jenjang=5 then 1 else 0 end) as D3,
sum(case when ps.id_jenjang=2 then 1 else 0 end) as S2,sum(case when ps.id_jenjang=3 then 1 else 0 end) as S3,
sum(case when ps.id_jenjang not in (1,2,3,5) then 1 else 0 end) as lain,
count(distinct dosen_wali.id_mhs) as ttl from dosen
left join program_studi on dosen.id_program_studi=program_studi.id_program_studi
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
left join dosen_wali on dosen.id_dosen=dosen_wali.id_dosen
left join mahasiswa on dosen_wali.id_mhs=mahasiswa.id_mhs
left join program_studi ps on mahasiswa.id_program_studi=ps.id_program_studi
where program_studi.id_fakultas=$kdfak
and dosen_wali.id_mhs in (select id_mhs from mahasiswa where status_akademik_mhs in 
(select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1))
and dosen_wali.id_mhs in (select id_mhs from pengambilan_mk where id_semester=$smt)
and status_dosen_wali=1
group by dosen.id_dosen, nip_dosen, trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang)
order by nm_pengguna
");
$smarty->assign('SEBDOLI',$jaf2);

$status = isset($_REQUEST['action'])? $_REQUEST['action'] : 'tampil';
//echo $status;

switch($status) {

case 'detailwali':

		$smarty->assign('disp2','block');
		$smarty->assign('disp1','none');

		$id_dosen=$_GET['dosen'];

		$dat=getvar("select nip_dosen, trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as dosen
					from dosen left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
					where dosen.id_dosen=$id_dosen");
		$smarty->assign('NIPDSN', $dat['NIP_DOSEN']);
		$smarty->assign('NMDSN', $dat['DOSEN']);

		$det=getData("select s1.nim_mhs,mhs,prodi,coalesce(s4.sks_skrg,0)as sks_skrg,coalesce(SKS_TOTAL_MHS,0) as skstotal,
					coalesce(IPK_MHS,0)as ipk,coalesce(sks_lalu,0) as sks_lalu,coalesce(ips,0) as ips
					from
					(select mahasiswa.id_mhs,mahasiswa.nim_mhs,upper(pengguna.nm_pengguna) as mhs,
					upper(nm_jenjang)||' - '||upper(nm_singkat_prodi) as prodi
					from mahasiswa
					left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
					left join dosen_wali on dosen_wali.id_mhs=mahasiswa.id_mhs
					left join program_studi on program_studi.id_program_studi=mahasiswa.id_program_studi
					left join jenjang on jenjang.id_jenjang=program_studi.id_jenjang
					where dosen_wali.id_dosen=$id_dosen and mahasiswa.status_akademik_mhs in 
					(select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
					and dosen_wali.id_mhs in (select id_mhs from pengambilan_mk where id_semester=$smt)
					and dosen_wali.status_dosen_wali='1')s1
					left join (
					select id_mhs, sum(kredit_semester) as SKS_TOTAL_MHS, 
					round((sum((kredit_semester * nilai_standar_nilai)) / sum(kredit_semester)), 2) as IPK_MHS
					from 
					(select a.id_mhs, c.kd_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai,
					case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,
					row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by nilai_huruf) rangking
					from pengambilan_mk a
					join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
					join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
					join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
					join semester smt on a.id_semester=smt.id_semester
					join mahasiswa m on a.id_mhs=m.id_mhs
					join program_studi ps on m.id_program_studi=ps.id_program_studi
					where a.status_apv_pengambilan_mk = 1 and (a.nilai_huruf<>'E' or a.nilai_huruf<>null) 
					and ps.id_fakultas=$kdfak and a.status_hapus=0 
					and a.status_pengambilan_mk !=0)
					where tahun<='$smtpil[TAHUN]' and rangking=1
					group by id_mhs)s2 on s1.id_mhs=s2.id_mhs
					left join 
					(select id_mhs,case when sum(bobot*kredit_semester)=0 then 0 else 
					round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips,
					case when id_fakultas=$kdfak then sum(sksreal) else sum(kredit_semester) end as sks_lalu 
					from 
					(select id_mhs,nim_mhs,id_kurikulum_mk,id_fakultas,kredit_semester, sksreal,min(nilai_huruf) as nilai, max(bobot) as bobot from
					(select a.id_mhs,m.nim_mhs,ps.id_fakultas,a.id_kurikulum_mk, 
					case when (a.nilai_huruf = 'E' or a.nilai_huruf is null) 
					and d.status_mkta in (1,2) then 0
					else d.kredit_semester end as kredit_semester,d.kredit_semester as sksreal,
					case when a.nilai_huruf is null then 'E' else a.nilai_huruf end as nilai_huruf,
					case when e.nilai_standar_nilai is null then 0 else e.nilai_standar_nilai end as bobot
					from pengambilan_mk a
					left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
					left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
					left join mahasiswa m on a.id_mhs=m.id_mhs
					left join program_studi ps on m.id_program_studi=ps.id_program_studi
					left join semester s on a.id_semester=s.id_semester
					where group_semester||thn_akademik_semester in
					(select group_semester||thn_akademik_semester from semester where id_Semester='$last_smt[ID_SEMESTER]')
					and tipe_semester in ('UP','REG','RD') 
					and a.status_apv_pengambilan_mk='1' and ps.id_fakultas=$kdfak and a.status_hapus=0 
					and a.status_pengambilan_mk !=0)
					group by id_mhs, nim_mhs, id_kurikulum_mk, id_fakultas, kredit_semester, sksreal
					)
					group by id_mhs,id_fakultas)s3 on s1.id_mhs=s3.id_mhs
					left join 
					(select a.id_mhs,sum(d.kredit_semester) as sks_skrg
					from pengambilan_mk a
					left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
					left join mahasiswa m on a.id_mhs=m.id_mhs
					left join program_studi ps on m.id_program_studi=ps.id_program_studi
					where id_semester=$smt and id_fakultas=$kdfak and status_apv_pengambilan_mk=1
					group by a.id_mhs)s4 on s1.id_mhs=s4.id_mhs
					order by prodi,nim_mhs");
	
	
		$smarty->assign('DET', $det);

		break;
}



$smarty->display('dosen-wali-fakul.tpl');
?>
