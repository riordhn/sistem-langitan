<?php
require('common.php');
require_once ('ociFunction.php');

error_reporting (E_ALL & ~E_NOTICE);

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');
$smarty->assign('disp5','none');

$kdprodi= $user->ID_PROGRAM_STUDI; 

$kdfak=$user->ID_FAKULTAS; 

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True'");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);

$smtaktifup=getvar("select id_semester from semester where status_aktif_sp='True'");
$smarty->assign('smtaktif_up',$smtaktifup['ID_SEMESTER']);

$id_prodi=getvar("select id_program_studi from program_studi where id_fakultas=$kdfak and rownum<2 and id_jenjang=1 order by id_jenjang");
$smarty->assign('pilprodi',$id_prodi['ID_PROGRAM_STUDI']);


$idprodi=getData("select id_program_studi,nm_jenjang||'-'||nm_program_studi as prodi from program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where id_fakultas=$kdfak order by program_studi.id_jenjang");

$smarty->assign('T_PRODI', $idprodi);

$smt=getData("select id_semester, tahun_ajaran||' - '||nm_semester||'/'||group_semester as smt from semester where status_aktif_sp='True'");
$smarty->assign('T_ST', $smt);

$kelas=getData("select id_nama_kelas,'KELAS-'||nama_kelas as nama_kelas from nama_kelas");
$smarty->assign('NM_KELAS', $kelas);

$status = isset($_REQUEST['action'])? $_REQUEST['action'] : 'tampil';

switch($status) {

case 'add':
		 // pilih
		 
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
		$smarty->assign('disp5','none');
		
		$id_mk=$_GET['id_mk'];
		$idprodi=$_GET['id_prodi'];

		//tambahdata("kelas_mk","id_kurikulum_mk,id_semester,id_program_studi","'$id_mk','".$smtaktifup['ID_SEMESTER']."','$id_prodi'");
		//echo "insert into kelas_mk (id_kurikulum_mk,id_semester,id_program_studi,status) values ('$id_mk','".$smtaktifup['ID_SEMESTER']."','$id_prodi','SP')";
		
		InsertData("insert into kelas_mk (id_kurikulum_mk,id_semester,id_program_studi,status) values ('$id_mk','".$smtaktifup['ID_SEMESTER']."','$idprodi','SP')");
		$id_kelas=getvar("select id_kelas_mk from kelas_mk where id_kurikulum_mk=$id_mk and id_semester='".$smtaktifup['ID_SEMESTER']."' and id_program_studi=$idprodi");
		$id_kelas_mk=$id_kelas['ID_KELAS_MK'];
		InsertData("insert into jadwal_kelas (id_kelas_mk) values ($id_kelas_mk)");

		$tawar=getData("select id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_tatap_muka,kredit_praktikum,
kmk.kredit_semester, nm_status_mk,nm_kelompok_mk, tahun_kurikulum,kmk.id_program_studi,
tingkat_semester from kurikulum_mk kmk 
left join mata_kuliah on kmk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join status_mk on kmk.id_status_mk=status_mk.id_status_mk
left join kelompok_mk on kmk.id_kelompok_mk=kelompok_mk.id_kelompok_mk
left join kurikulum_prodi on kmk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
where kmk.id_program_studi=$idprodi and kurikulum_prodi.status_aktif=1 and id_kurikulum_mk not in 
(select id_kurikulum_mk from kelas_mk where id_program_studi=$idprodi and id_semester='".$smtaktifup['ID_SEMESTER']."')
" );
		$smarty->assign('TAWAR', $tawar);
	
        break;
case 'del':
		 // pilih
	
		$id_klsmk= $_GET['id_klsmk'];
		
		deleteData("delete from jadwal_kelas where id_kelas_mk=$id_klsmk");
		deleteData("delete from pengampu_mk where id_kelas_mk=$id_klsmk");		
		deleteData("delete from kelas_mk where id_kelas_mk=$id_klsmk");
		
		$smarty->assign('disp1','block');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
		$smarty->assign('disp5','none');

		$jaf=getData("select kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester, 
jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam, 
ruangan.id_ruangan,nm_ruangan,case when pengampu_mk.id_dosen is not null then 'sdh terisi' else 'blm terisi' end as status_pjmk,
nm_jenjang||'-'||nm_singkat_prodi as prodi_asal,
no_kelas_mk,nama_kelas.nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk from kelas_mk 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join semester on kelas_mk.id_semester=semester.id_semester 
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk 
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester='".$smtaktifup['ID_SEMESTER']."'
");
		$smarty->assign('T_MK', $jaf);

		
        break; 

case 'updateview':
		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','block');
		$smarty->assign('disp4','none');
		$smarty->assign('disp5','none');
	
		$id_klsmk= $_GET['id_klsmk'];
		
		
		$ruang=getData("select id_ruangan,nm_ruangan||' ('||kapasitas_ruangan||')' as ruang from ruangan
left join gedung on gedung.id_gedung=ruangan.id_gedung
left join gedung_kuliah on gedung.id_gedung=gedung_kuliah.id_gedung
where gedung.id_fakultas=$kdfak");
		$smarty->assign('T_RUANG', $ruang);
		
		$hari=getData("select id_jadwal_hari,nm_jadwal_hari from jadwal_hari");
		$smarty->assign('T_HARI', $hari);

		$jam=getData("select id_jadwal_jam,nm_jadwal_jam from jadwal_jam where id_fakultas=$kdfak");
		$smarty->assign('T_JAM', $jam);
		
		$jaf=getData("select distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
		kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt,
jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,kelas_mk.id_semester,
ruangan.id_ruangan,nm_ruangan,no_kelas_mk,nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.id_program_studi
from kelas_mk
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas  
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join semester on kelas_mk.id_semester=semester.id_semester 
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
where kelas_mk.id_kelas_mk=$id_klsmk and rownum=1");
		$smarty->assign('TJAF1', $jaf);
	
        break; 

case 'tambahkelas':
		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','block');
		$smarty->assign('disp5','none');
	
		$kap_kelas = $_POST['kap_kelas'];
		$kelas_mk = $_POST['kelas_mk'];
		$ren_kul = $_POST['ren_kul'];
		$id_kur_mk= $_POST['id_kur_mk'];
		$id_hari = $_POST['hari'];
		$id_jam= $_POST['jam'];
		$id_prodi= $_POST['prodi'];
		$status= $_POST['status'];
		//$smt1= $_POST['smtb'];
		$smt1=$smtaktifup['ID_SEMESTER'];
		$idruang= $_POST['ruangan'];

		tambahdata("kelas_mk","id_kurikulum_mk,id_semester,id_program_studi,no_kelas_mk,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,status","'$id_kur_mk','$smt1','$id_prodi','$kelas_mk','$kap_kelas','$ren_kul','$status'");
		$id_kelas=getvar("select id_kelas_mk from kelas_mk where id_kurikulum_mk=$id_kur_mk and id_semester=$smt1 and id_program_studi=$id_prodi and no_kelas_mk=$kelas_mk");
		$id_kelas_mk=$id_kelas['ID_KELAS_MK'];
		tambahdata("jadwal_kelas","id_kelas_mk,id_jadwal_hari,id_jadwal_jam,id_ruangan","'$id_kelas_mk','$id_hari','$id_jam','$idruang'");	

		$jaf=getData("select kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester, 
jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam, 
ruangan.id_ruangan,nm_ruangan,case when pengampu_mk.id_dosen is not null then 'sdh terisi' else 'blm terisi' end as status_pjmk,
nm_jenjang||'-'||nm_singkat_prodi as prodi_asal,
no_kelas_mk,nama_kelas.nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk from kelas_mk 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join semester on kelas_mk.id_semester=semester.id_semester 
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk 
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester='".$smtaktifup['ID_SEMESTER']."'
");
		$smarty->assign('T_MK', $jaf);
	
        break;      

case 'tambahhari':
		 // pilih
		$smarty->assign('disp1','block');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
		$smarty->assign('disp5','none');
	
		$kap_kelas = $_POST['kap_kelas'];
		$id_kelas_mk = $_POST['id_kelas_mk'];
		$ren_kul = $_POST['ren_kul'];
		$id_kur_mk= $_POST['id_kur_mk'];
		$id_hari = $_POST['hari'];
		$id_jam= $_POST['jam'];
		//$smt1= $_POST['smtb'];
		$smt1= $smtaktifup['ID_SEMESTER'];
		$idruang= $_POST['ruangan'];

		tambahdata("jadwal_kelas","id_kelas_mk,id_jadwal_hari,id_jadwal_jam,id_ruangan","$id_kelas_mk,$id_hari,$id_jam,$idruang");	

		$jaf=getData("select kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester, 
jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam, 
ruangan.id_ruangan,nm_ruangan,case when pengampu_mk.id_dosen is not null then 'sdh terisi' else 'blm terisi' end as status_pjmk,
nm_jenjang||'-'||nm_singkat_prodi as prodi_asal,
no_kelas_mk,nama_kelas.nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk from kelas_mk 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join semester on kelas_mk.id_semester=semester.id_semester 
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk 
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester='".$smtaktifup['ID_SEMESTER']."'
");
		$smarty->assign('T_MK', $jaf);
	
        break;      
		
case 'adkelview':
		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','block');
		$smarty->assign('disp5','none');
	
		$id_klsmk= $_GET['id_klsmk'];
		
		$ruang=getData("select id_ruangan,nm_ruangan||' ('||kapasitas_ruangan||')' as ruang from ruangan
left join gedung on gedung.id_gedung=ruangan.id_gedung
left join gedung_kuliah on gedung.id_gedung=gedung_kuliah.id_gedung
where gedung.id_fakultas=$kdfak");
		$smarty->assign('T_RUANG', $ruang);
		
		$hari=getData("select id_jadwal_hari,nm_jadwal_hari from jadwal_hari");
		$smarty->assign('T_HARI', $hari);

		$jam=getData("select id_jadwal_jam,nm_jadwal_jam from jadwal_jam where id_fakultas=$kdfak");
		$smarty->assign('T_JAM', $jam);
		
		$jaf=getData("select distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kelas_mk.id_kurikulum_mk,
		kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt,kelas_mk.status,
jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,kelas_mk.id_semester,kelas_mk.id_program_studi,
ruangan.id_ruangan,nm_ruangan,no_kelas_mk,nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk from kelas_mk
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas  
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join semester on kelas_mk.id_semester=semester.id_semester 
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
where kelas_mk.id_kelas_mk=$id_klsmk and rownum=1");
	
		$smarty->assign('TJAF1', $jaf);

        break; 

case 'adhariview':
		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
		$smarty->assign('disp5','block');
	
		$id_klsmk= $_GET['id_klsmk'];
		
		$ruang=getData("select id_ruangan,nm_ruangan||' ('||kapasitas_ruangan||')' as ruang from ruangan
left join gedung on gedung.id_gedung=ruangan.id_gedung
left join gedung_kuliah on gedung.id_gedung=gedung_kuliah.id_gedung
where gedung.id_fakultas=$kdfak");
		$smarty->assign('T_RUANG', $ruang);
		
		$hari=getData("select id_jadwal_hari,nm_jadwal_hari from jadwal_hari");
		$smarty->assign('T_HARI', $hari);

		$jam=getData("select id_jadwal_jam,nm_jadwal_jam from jadwal_jam where id_fakultas=$kdfak");
		$smarty->assign('T_JAM', $jam);
		
		$jaf=getData("select distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kelas_mk.id_kurikulum_mk,
		kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt,
jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,kelas_mk.id_semester,
ruangan.id_ruangan,nm_ruangan,no_kelas_mk,nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk from kelas_mk
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas  
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join semester on kelas_mk.id_semester=semester.id_semester 
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
where kelas_mk.id_kelas_mk=$id_klsmk ");
		$smarty->assign('TJAF2', $jaf);
	
        break; 

case 'penawaran':
		$idprodi= $_POST['idprodi'];
		$id_kur= $_POST['thkur'];
		
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
		
		$smarty->assign('id_prodi',$idprodi);

$tawar=getData("select id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_tatap_muka,kredit_praktikum,
kmk.kredit_semester, nm_status_mk,nm_kelompok_mk, tahun_kurikulum,kmk.id_program_studi,
tingkat_semester from kurikulum_mk kmk 
left join mata_kuliah on kmk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join status_mk on kmk.id_status_mk=status_mk.id_status_mk
left join kelompok_mk on kmk.id_kelompok_mk=kelompok_mk.id_kelompok_mk
left join kurikulum_prodi on kmk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
where kmk.id_program_studi='$idprodi' and kurikulum_prodi.status_aktif=1 and id_kurikulum_mk not in 
(select id_kurikulum_mk from kelas_mk where id_program_studi='$idprodi' and id_semester='".$smtaktifup['ID_SEMESTER']."')
order by kd_mata_kuliah");

		$smarty->assign('TAWAR', $tawar);
		break;
	
case 'update1':
	
		$kap_kelas = $_POST['kap_kelas'];
		$kelas_mk = $_POST['kelas_mk'];
		$ren_kul = $_POST['ren_kul'];
		$id_kelas_mk= $_POST['id_kelas_mk'];
		$id_hari = $_POST['hari'];
		$id_jam= $_POST['jam'];
		$smt1= $_POST['smtb'];
		$idruang= $_POST['ruangan'];
		$id_prodi= $_POST['id_prodi'];
		
		//if (!isFindData("jadwal_kelas","id_kelas_mk=$id_kelas_mk")) {tambahdata("jadwal_kelas","id_kelas_mk","$id_kelas_mk");}
		//echo "update kelas_mk set kapasitas_kelas_mk=$kap_kelas, jumlah_pertemuan_kelas_mk=$ren_kul,no_kelas_mk='$kelas_mk' where id_kelas_mk=$id_kelas_mk";
		//echo "update jadwal_kelas set id_jadwal_jam=$id_jam, id_jadwal_hari=$id_hari,id_ruangan='$idruang' where id_kelas_mk=$id_kelas_mk";
		UpdateData("update kelas_mk set kapasitas_kelas_mk=$kap_kelas, jumlah_pertemuan_kelas_mk=$ren_kul,no_kelas_mk='$kelas_mk' where id_kelas_mk=$id_kelas_mk");
		
		UpdateData("update jadwal_kelas set id_jadwal_jam=$id_jam, id_jadwal_hari=$id_hari,id_ruangan='$idruang' where id_kelas_mk=$id_kelas_mk");
	
		echo '<script> $("#usulan-mk-up").load("usulan-mk-up.php"); </script>';

		break;

case 'tampil':

		$jaf=getData("select kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester, 
jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam, 
ruangan.id_ruangan,nm_ruangan,case when pengampu_mk.id_dosen is not null then 'sdh terisi' else 'blm terisi' end as status_pjmk,
nm_jenjang||'-'||nm_singkat_prodi as prodi_asal,
no_kelas_mk,nama_kelas.nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk from kelas_mk 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join semester on kelas_mk.id_semester=semester.id_semester 
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk 
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester='".$smtaktifup['ID_SEMESTER']."'
");
		$smarty->assign('T_MK', $jaf);
	
        break; 
			 
}
$smarty->display('usulan-mk-sp.tpl');

?>
