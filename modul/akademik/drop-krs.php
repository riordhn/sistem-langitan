<?php
//error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

//CEK
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$id_pengguna= $user->ID_PENGGUNA; 
$kdfak=$user->ID_FAKULTAS; 
$smarty->assign('FAK',$kdfak);

//Khusus Fakultas Psikologi
if($kdfak==11){

	echo '
		<script>
		location.href="#utility-drop_krs!drop-krs-psikologi.php";
		</script>
	';

//Fakultas Lain
} else {

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True' and id_perguruan_tinggi = '{$id_pt_user}'");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);

$id_semester=$smtaktif['ID_SEMESTER'];

$nim_mhs= $_REQUEST['nim'];
$smarty->assign('nim',$nim_mhs);

$ket_drop=$_REQUEST['ket_drop'];
$smarty->assign('ket_drop',$ket_drop);

//echo $ket_drop;

if ($ket_drop=='Pilih') {
	echo '<script>alert("Keterangan Wajib di Pilih alias di Isi!!!!")</script>';
}
else
{

if ($_GET['action']=='drop') {
			$id_pengambilan_mk=$_GET['id_peng_mk'];
			$ket_drop=$_GET['ket_drop'];
			UpdateData("update pengambilan_mk set status_pengambilan_mk='0', status_apv_pengambilan_mk='0' , keterangan='$ket_drop' where id_pengambilan_mk=$id_pengambilan_mk");
}

if ($_GET['action']=='batal') {
			$id_pengambilan_mk=$_GET['id_peng_mk'];
			$ket_drop=$_GET['ket_drop'];
			UpdateData("update pengambilan_mk set status_pengambilan_mk='1', status_apv_pengambilan_mk='1' , keterangan='Batal Drop' where id_pengambilan_mk=$id_pengambilan_mk");

			$smarty->assign('disp1','none');
			$smarty->assign('disp2','block');
			
}
$datamhs= getData("select a.id_mhs,b.nim_mhs,h.nm_pengguna,g.nm_jenjang||'-'||f.nm_program_studi as prodi,sum(a.kredit_semester) as skstotal,
		round(sum(a.kredit_semester*(case a.nilai_huruf 
		when 'A' then 4 
		when 'AB' then 3.5 
		when 'B' then 3
		when 'BC' then 2.5
		when 'C' then 2
		when 'D' then 1
		end))/sum(a.kredit_semester),2) as IPK
		from
		(
		select a.id_mhs,e.nm_mata_kuliah,nvl(e.kredit_semester,d.kredit_semester) kredit_semester,a.nilai_huruf from (
		select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
		from pengambilan_mk a 
		left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
		left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
		where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.id_semester is not null and a.status_hapus=0
		) a
		left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
		left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
		where rangking=1 and id_mhs in (select id_mhs from mahasiswa join pengguna on pengguna.id_pengguna = mahasiswa.id_pengguna where mahasiswa.nim_mhs='$nim_mhs' and pengguna.id_perguruan_tinggi = '{$id_pt_user}')
		) a
		left join mahasiswa b on a.id_mhs=b.id_mhs
    left join pengguna h on b.id_pengguna=h.id_pengguna
		left join program_studi f on b.id_program_studi=f.id_program_studi
    left join jenjang g on f.id_jenjang=g.id_jenjang
		where a.id_mhs  in (select id_mhs from mahasiswa join pengguna on pengguna.id_pengguna = mahasiswa.id_pengguna where mahasiswa.nim_mhs='$nim_mhs' and pengguna.id_perguruan_tinggi = '{$id_pt_user}') and f.id_fakultas=$kdfak
   group by a.id_mhs, b.nim_mhs, h.nm_pengguna, g.nm_jenjang||'-'||f.nm_program_studi");
  

   
$smarty->assign('T_DATAMHS', $datamhs);

$krsmhs= getData("select id_pengambilan_mk,thn_akademik_semester||'-'||nm_semester as smt,kd_mata_kuliah,nm_mata_kuliah,
kurikulum_mk.kredit_semester,nilai_huruf from pengambilan_mk
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join semester on pengambilan_mk.id_semester=semester.id_semester
where id_mhs in (select id_mhs from mahasiswa join pengguna on pengguna.id_pengguna = mahasiswa.id_pengguna where mahasiswa.nim_mhs='$nim_mhs' and pengguna.id_perguruan_tinggi = '{$id_pt_user}') and pengambilan_mk.id_semester=$id_semester
and status_pengambilan_mk='1' and status_apv_pengambilan_mk='1'
order by thn_akademik_semester,nm_semester,kd_mata_kuliah");
$smarty->assign('T_KRSMHS', $krsmhs);

$krsbatal= getData("select id_pengambilan_mk,thn_akademik_semester||'-'||nm_semester as smt,kd_mata_kuliah,nm_mata_kuliah,
kurikulum_mk.kredit_semester,nilai_huruf,pengambilan_mk.keterangan from pengambilan_mk
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join semester on pengambilan_mk.id_semester=semester.id_semester
where id_mhs in (select id_mhs from mahasiswa join pengguna on pengguna.id_pengguna = mahasiswa.id_pengguna where mahasiswa.nim_mhs='$nim_mhs' and pengguna.id_perguruan_tinggi = '{$id_pt_user}') and pengambilan_mk.id_semester=$id_semester
and status_pengambilan_mk='0' and status_apv_pengambilan_mk='0' and pengambilan_mk.keterangan in ('Pengajuan MHS','Sanksi UTS','Sanksi UAS','Sanksi Akdemik')
order by thn_akademik_semester,nm_semester,kd_mata_kuliah");

$smarty->assign('T_DROP', $krsbatal);

}
$smarty->display('drop-krs.tpl');
}
?>
