<div class="center_title_bar"> Fitur Menu Aktifitas Semester </div>
<table width="660" border="0" cellspacing="1" cellpadding="1">
  <tr>
    <td width="38" bgcolor="#FFCC33"><div align="center">No</div></td>
    <td width="178" bgcolor="#FFCC33"><div align="center">Modul</div></td>
    <td width="434" bgcolor="#FFCC33"><div align="center">Uraian</div></td>
  </tr>
  <tr>
    <td><center>1</center></td>
    <td>Daftar Nama Semester</td>
    <td>Difungsikan untuk melihat tahun ajaran yang sedang berjalan dan sudah terisi ke master semester </td>
  </tr>
  <tr>
    <td><center>2</center></td>
    <td>Nama Kegiatan</td>
    <td>Difungsikan untuk merencanakan kegiatan akademik dalam kurun satu semester </td>
  </tr>
  <tr>
    <td><center>3</center></td>
    <td>Jadwal Akademik</td>
    <td>Difungsikan untuk merencanakan jadwal kalendar kegiatan akademik </td>
  </tr>
  <tr>
    <td><center>4</center></td>
    <td>Usulan Mata Ajar Prodi</td>
    <td>Difungsikan untuk melihat usulan mata ajar yang akan ditawarkan oleh prodi dalam kurun satu semester </td>
  </tr>
  <tr>
    <td><center>5</center></td>
    <td>Ploting Jadwal Kuliah</td>
    <td>Difungsikan untuk memonitoring dan mengontrol pengolahan jadwal dan kelas dalam satu fakultas </td>
  </tr>
  <tr>
    <td><center>6</center></td>
    <td>Jadwal Ruang</td>
    <td>Difungsikan untuk meonitoring penggunaan Ruangan untuk kegiatan PBM</td>
  </tr>
  <tr>
    <td><center>7</center></td>
    <td>Konversi Nilai</td>
    <td>Difungsikan untuk melakukan konversi nilai bagi Mahasiswa tertentu yang akan pindah jenjang atau jalur</td>
  </tr>
</table>