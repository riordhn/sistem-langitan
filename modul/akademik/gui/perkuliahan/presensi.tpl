<div class="center_title_bar"> Fitur Menu Presensi </div>
<table width="660" border="0" cellspacing="1" cellpadding="1">
  <tr>
    <td width="38" bgcolor="#FFCC33"><div align="center">No</div></td>
    <td width="178" bgcolor="#FFCC33"><div align="center">Modul</div></td>
    <td width="434" bgcolor="#FFCC33"><div align="center">Uraian</div></td>
  </tr>
  <tr>
    <td><center>1</center></td>
    <td>Entry Presensi</td>
    <td>Difungsikan untuk kegiatan presensi Dosen dan Mahasiswa beserta materi dan kegiatan perkuliahan</td>
  </tr>
  <tr>
    <td><center>2</center></td>
    <td>Proses Cekal</td>
    <td>Difungsikan untuk melakukan kalkulasi prosentase kehadiran Mahasiswa dalam kegiatan perkuliahan </td>
  </tr>
  <tr>
    <td><center>3</center></td>
    <td>Susulan</td>
    <td>Difungsikan untuk aktifitas kegiatan ujian Susulan </td>
  </tr>
  <tr>
    <td><center>4</center></td>
    <td>Daftar Cekal</td>
    <td>Difungsikan untuk mencetak daftar mahasiswa yang terkena cekal Ujian Akhir Semester pada semester aktif</td>
  </tr>
  <tr>
    <td><center>5</center></td>
    <td>Cetak Presensi Kuliah</td>
    <td>Difungsikan untuk mencetak daftar presensi kuliah per mata ajar</td>
  </tr>
  <tr>
    <td><center>6</center></td>
    <td>Cetak Presensi UTS</td>
    <td>Difungsikan untuk mencetak daftar peserta kegiatan UTS</td>
  </tr>
  <tr>
    <td><center>7</center></td>
    <td>Cetak Presensi UAS</td>
    <td>Difungsikan untuk mencetak daftar peserta kegiatan UAS</td>
  </tr>
  <tr>
    <td><center>8</center></td>
    <td>Cetak Presensi KPRS</td>
    <td>Difungsikan untuk mencetak daftar peserta kuliah per mata ajar yang berubah karena KPRS</td>
  </tr>
</table>