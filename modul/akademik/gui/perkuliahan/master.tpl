<div class="center_title_bar"> Fitur Menu Master Perkuliahan </div>
<table width="100%" border="0" cellspacing="1" cellpadding="1">
  <tr>
    <td bgcolor="#FFCC33"><div align="center">No</div></td>
    <td bgcolor="#FFCC33"><div align="center">Modul</div></td>
    <td bgcolor="#FFCC33"><div align="center">Uraian</div></td>
  </tr>
  <tr>
    <td><center>1</center></td>
    <td>Kurikulum</td>
    <td>Difungsikan untuk melihat kurikulum yang berlaku di setiap prodi dalam satu Fakultas </td>
  </tr>
  <tr>
    <td><center>2</center></td>
    <td>Daftar Mata Ajar</td>
    <td>Difungsikan untuk melihat daftar mata ajar pada setiap kurikulum di setiap prodi dalam satu fakultas </td>
  </tr>
  <tr>
    <td><center>3</center></td>
    <td>Rentang Nilai Mutu</td>
    <td>Difungsikan untuk melihat rentang nilai A,AB,dst </td>
  </tr>
  <tr>
    <td><center>4</center></td>
    <td>Beban SKS</td>
    <td>Difungsikan untuk melihat jatah pengambilan SKS sesuai dengan IPS dalam satu semester </td>
  </tr>
  <tr>
    <td><center>5</center></td>
    <td>Jam</td>
    <td>Difungsikan untuk mengisi dan mengupdate master Jam yang dipergunakan oleh Fakultas untuk kegiatan PBM, </td>
  </tr>
  <tr>
    <td><center>6</center></td>
    <td>Fakultas </td>
    <td>Difungsikan untuk mengisi dan mengupdate informasi Fakultas (digunakan untuk header KHS, Transkrip, dll)</td>
  </tr>
  <tr>
    <td><center>7</center></td>
    <td>Gedung </td>
    <td>Difungsikan untuk mengisi dan mengupdate fasilitas gedung yang dimiliki oleh Fakultas</td>
  </tr>
  <tr>
    <td><center>8</center></td>
    <td>Fasilitas</td>
    <td>Difungsikan untuk mengisi dan mengupdate fasilitas dan inventaris barang yang dimiliki oleh Fakultas</td>
  </tr>
   <tr>
    <td><center>9</center></td>
    <td>Ruang</td>
    <td>Difungsikan untuk mengisi dan mengupdate fasilitas ruangan yang dimiliki oleh Fakultas</td>
  </tr>
   <tr>
    <td><center>10</center></td>
    <td>Konversi Kurikulum</td>
    <td>Difungsikan untuk melihat dan memonitoring konversi antar kurikulum yang masih berlaku di Prodi</td>
  </tr>
  <tr>
    <td><center>11</center></td>
    <td>Jenjang</td>
    <td>Difungsikan untuk melihat daftar Jenjang Pendidikan yang ada di Universitas</td>
  </tr>
  <tr>
    <td><center>12</center></td>
    <td>Predikat Kelulusan</td>
    <td>Difungsikan untuk melihat daftar Predikat kelulusan</td>
  </tr>
</table>