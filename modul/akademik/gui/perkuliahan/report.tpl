<div class="center_title_bar"> Fitur Menu Report </div>
<table width="660" border="0" cellspacing="1" cellpadding="1">
  <tr>
    <td width="38" bgcolor="#FFCC33"><div align="center">No</div></td>
    <td width="178" bgcolor="#FFCC33"><div align="center">Modul</div></td>
    <td width="434" bgcolor="#FFCC33"><div align="center">Uraian</div></td>
  </tr>
  <tr>
    <td><center>1</center></td>
    <td>Rekap Presensi</td>
    <td>Difungsikan untuk rekapitulasi presensi perkuliahan Dosen dan Mahasiswa per semester</td>
  </tr>
  <tr>
    <td><center>2</center></td>
    <td>Daftar Nilai Mata Ajar</td>
    <td>Difungsikan untuk melihat daftar nilai mata ajar yang diperoleh per Mahasiswa pada semester berjalan</td>
  </tr>
  <tr>
    <td><center>3</center></td>
    <td>Distribusi Nilai Total</td>
    <td>Difungsikan untuk rekapitulasi distribusi prosentase nilai seluruh mata ajar fakultas atau prodi per semester</td>
  </tr>
  <tr>
    <td><center>4</center></td>
    <td>Distribusi Nilai Mata Ajar</td>
    <td>Difungsikan untuk rekapitulasi distribusi prosentase nilai setiap mata ajar fakultas atau prodi per semester dan per angkatan</td>
   <tr>
    <td><center>5</center></td>
    <td>Distribusi IPK Mhs</td>
    <td>Difungsikan untuk rekapitulasi distribusi prosentase IPK Mahasiswa Aktif fakultas atau prodi per semester dan per angkatan</td>
  </tr>
   <tr>
    <td><center>6</center></td>
    <td>Distribusi IPK Lulusan</td>
    <td>Difungsikan untuk rekapitulasi distribusi prosentase IPK Mahasiswa Lulus fakultas atau prodi per semester dan per angkatan</td>
  </tr>
  </tr>
    <tr>
    <td><center>7</center></td>
    <td>Histori Dosen</td>
    <td>Difungsikan untuk rekapitulasi histori ajar Dosen</td>
  </tr>
  <tr>
    <td><center>8</center></td>
    <td>Histori Mahasiswa</td>
    <td>Difungsikan untuk rekapitulasi histori perkuliahan, nilai dan Indeks Prestasi Mahasiswa</td>
  </tr>
</table>