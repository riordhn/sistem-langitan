<div class="center_title_bar">SANKSI UJIAN UTS/UAS </div>

<form action="sanksi-ujian.php" method="post" name="mhs_krs" id="mhs_krs">
<input type="hidden" name="action" value="krs" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr>
					  <td>Nim Mahasiswa </td>
					  <td>:</td>
					  <td><input type="text" name="nim"></td>
			</tr>

			<tr>
					 <td>Semester </td>
					  <td>:</td>
					  <td><select name="smt">
    		   			<option value='Pilih'>-- Pilih --</option>
				   		{foreach item="list_smt" from=$T_THNSMT}
							{html_options values=$list_smt.ID_SEMESTER output=$list_smt.THN_SMT selected= $smt1}
						{/foreach}
			   		 </select></td>
					  <td><input type="submit" name="cari1" value="Proses"></td>
		   </tr>
</table>
</form>

<table width="80%" border="0" cellspacing="0" cellpadding="0">
{foreach name=test item="list3" from=$T_DATAMHS}
					<tr>
					  <td width="19%">NIM</td>
					  <td width="1%">:</td>
					  <td width="80%">{$list3.NIM_MHS}</td>
					</tr>
					<tr>
					  <td>NAMA </td>
					  <td>:</td>
					  <td>{$list3.NM_PENGGUNA}</td>
				    </tr>
					<tr>
					  <td>PRODI </td>
					  <td>:</td>
					  <td>{$list3.PRODI}</td>
				    </tr>

							
{/foreach}
</table>

<div id="tabs">
	<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">SANKSI</div>
</div>

<div class="panel" id="panel1" style="display: {$disp1} ">
<p> </p>

<form action="sanksi-ujian.php" method="post" name="mhs_krs1" id="mhs_krs1">
<input type="hidden" name="action" value="drop" />
<input type="hidden" name="smt" value="{$smt1}" />
<input type="hidden" name="nim" value="{$nim}" />


<table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr class="left_menu" align="center">
         <td width="2%" bgcolor="#333333"><font color="#FFFFFF">No</font></td>
		 <td width="8%" bgcolor="#333333"><font color="#FFFFFF">Semester</font></td>
         <td width="5%" bgcolor="#333333"><font color="#FFFFFF">Kode</font></td>
         <td width="33%" bgcolor="#333333"><font color="#FFFFFF">Nama  Mata Kuliah</font> </td>
         <td width="5%" bgcolor="#333333"><font color="#FFFFFF">SKS</font></td>
	     <td width="5%" bgcolor="#333333"><font color="#FFFFFF">Nilai</font></td>
		 <td width="10%" bgcolor="#333333"><font color="#FFFFFF">Keterangan</font></td>
	     <td width="15%" bgcolor="#333333"><font color="#FFFFFF">Aksi</font></td>
        </tr>
          {foreach name=test item="list" from=$T_KRSMHS}
		
        <tr>
          <td >{$smarty.foreach.test.iteration}</td>
          <td >{$list.SMT}</td>
		  <td >{$list.KD_MATA_KULIAH}</td>
          <td >{$list.NM_MATA_KULIAH}</td>
          <td >{$list.KREDIT_SEMESTER}</td>
		  <td >{$list.NILAI_HURUF}</td>
		  <td >{$list.KET}</td>
		  <td><select name="ket_drop{$smarty.foreach.test.iteration}">
    		   			<option value='Pilih'>-- Pilih --</option>
				   		<option value='Sanksi UTS-KERJASAMA'>Sanksi UTS-KERJASAMA</option>
						<option value='Sanksi UTS-NGERPEK'>Sanksi UTS-NGERPEK</option>
						<option value='Sanksi UAS-KERJASAMA'>Sanksi UAS-KERJASAMA</option>
						<option value='Sanksi UAS-NGERPEK'>Sanksi UAS-NGERPEK</option>
						<option value='Sanksi UTS-Administrasi'>Sanksi UTS-Administrasi</option>
						<option value='Sanksi UAS-Administrasi'>Sanksi UAS-Administrasi</option>
			   </select>
		  </td>
		  <input type="hidden" name="id_peng_mk{$smarty.foreach.test.iteration}" value="{$list.ID_PENGAMBILAN_MK}" />
        </tr>
         {foreachelse}
        <tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		<input type="hidden" name="count" value="{$smarty.foreach.test.iteration}" />
</table>
		<tr><input type="submit" name="proses" value="Proses"></tr>
</form>
</div>

<div class="panel" id="panel2" style="display: {$disp2} ">
<p> </p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr class="left_menu" align="center">
         <td width="4%" bgcolor="#333333"><font color="#FFFFFF">No</font></td>
		 <td width="10%" bgcolor="#333333"><font color="#FFFFFF">Semester</font></td>
         <td width="5%" bgcolor="#333333"><font color="#FFFFFF">Kode</font></td>
         <td width="33%" bgcolor="#333333"><font color="#FFFFFF">Nama  Mata Kuliah</font> </td>
         <td width="5%" bgcolor="#333333"><font color="#FFFFFF">SKS</font></td>
	     <td width="5%" bgcolor="#333333"><font color="#FFFFFF">KETERANGAN</font></td>
	     <td width="20%" bgcolor="#333333"><font color="#FFFFFF">Aksi</font></td>
        </tr>
          {foreach name=test item="list" from=$T_DROP}
		
        <tr>
          <td >{$smarty.foreach.test.iteration}</td>
          <td >{$list.SMT}</td>
		  <td >{$list.KD_MATA_KULIAH}</td>
          <td >{$list.NM_MATA_KULIAH}</td>
          <td >{$list.KREDIT_SEMESTER}</td>
		  <td >{$list.KETERANGAN}</td>
		  {if {$list.KETERANGAN} != ""}
		  <td height="0" align="center"><input name="Button" type="button" class="button" onClick="location.href='#utility-KRS!sanksi-ujian.php?action=batal&id_peng_mk={$list.ID_PENGAMBILAN_MK}&nim={$nim}&ket_drop={$list.KETERANGAN}&smt={$smt1}';disableButtons()" onMouseOver="window.status='Click untuk Batal Drop';return true" onMouseOut="window.status='BATAL'" value="BATAL" /> </td>
		  {else}
			<td ></td>
		  {/if}
		</tr>
         {foreachelse}
        <tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
</table>
</div>

<script>$('form').validate();</script>
