{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]]
		}
	); 
} 
);
</script>
{/literal}
<div class="center_title_bar">Data Mahasiswa {foreach item="list1" from=$NM_PRODI}{$list1.PRODI} {/foreach} </div>  
<form action="data-mhs-baru.php" method="post" name="data_mhs_baru" id="data_mhs_baru">
<input type="hidden" name="action" value="view" >
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td>
			 	Prodi : 
                <select name="kdprodi">
    		   <option value=''>-- PRODI --</option>
	 		   {foreach item="list" from=$T_PRODI}
    		   {html_options  values=$list.ID_PROGRAM_STUDI output=$list.PRODI}
	 		   {/foreach}
			   </select>
			 </td>
			 <td>
			   Angkatan : 
                <select name="th_agk">
    		   <option value=''>-- Angkatan --</option>
	 		   {foreach item="list" from=$T_AGK}
    		   {html_options  values=$list.THN_ANGKATAN_MHS output=$list.THN_ANGKATAN_MHS}
	 		   {/foreach}
			   </select>
			   </td>
			   <td>
			   <input type="submit" name="View" value="View">
		     </td> 
           </tr>		   
</table>
</form>	

{* khusus fakultas psikologi *}
{if $FAK == '11'}
{if {$list1.PRODI} == ''}
{else}
<p>
<input type=button name="cetak" value="Ekspor Data ke MS. Excel" onclick="window.open('proses/data-mhs-xls.php?prodi={$PRODI}&nm_prodi={$list1.NAMA_PRODI}&angk={$ANGK}','baru2');">&nbsp;
<input type=button name="cetak" value="Ekspor Data ke MS. Word" onclick="window.open('proses/data-mhs-doc.php?prodi={$PRODI}&nm_prodi={$list1.NAMA_PRODI}&angk={$ANGK}','baru2');">
</p>
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0" width="600">
		<thead>
           <tr>
             <th>No</th>
             <th>NIM</th>
             <th>Nama Mahasiswa</th>
             <th>Alamat</th>
			 <th>Sklh Asal</th>
			 <th>Telepon</th>
             <th>NEM</th>
             <th>Agama</th>
			 <th>L/P</th>
			 <th>Tpt Lahir</th>
             <th>Tgl Lahir</th>
             <th>Jalur</th>
           </tr>
		</thead>
		<tbody>
		   {foreach name=test item="list" from=$T_MK}
           <tr>
             <td><center>{$smarty.foreach.test.iteration}</center></td>
             <td>{$list.NIM_MHS}</td>
             <td>{$list.NM_PENGGUNA}</td>
             <td>{$list.ALAMAT_ASAL_MHS}</td>
             <td>{$list.SEKOLAH_ASAL_MHS}</td>
             <td>{$list.MOBILE_MHS}</td>
             <td>{$list.NEM_PELAJARAN_MHS}</td>
             <td>{$list.NM_AGAMA}</td>
			 <td><center>{$list.KELAMIN}</center></td>
             <td>{$list.NM_KOTA}</td>
             <td>{$list.TGL_LAHIR_PENGGUNA}</td>
             <td>{$list.NM_JALUR}</td>
           </tr>
            {foreachelse}
        <tr><td colspan="12"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
   	</table>
{/if}

{* fakultas selain psikologi *}
{else}
<p>
<input type=button name="cetak" value="Ekspor ke Excel" onclick="window.open('proses/data-mhs-xls.php?prodi={$PRODI}&nm_prodi={$list1.NAMA_PRODI}&angk={$ANGK}','baru2');">&nbsp;
</p>
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
             <th width="3%">No</th>
             <th width="8%">NIM</th>
             <th width="17%">Nama Mahasiswa</th>
             <th width="20%">Alamat</th>
			 <th width="8%">Sklh Asal</th>
			 <th width="8%">Telepon</th>
             <th width="3%">NEM</th>
             <th width="6%">Agama</th>
			 <th width="3%">L/P</th>
			 <th width="8%">Tpt Lahir</th>
             <th width="6%">Tgl Lahir</th>
             <th width="8%">Jalur</th>
           </tr>
		</thead>
		<tbody>
		   {foreach name=test item="list" from=$T_MK}
           <tr>
             <td><center>{$smarty.foreach.test.iteration}</center></td>
             <td>{$list.NIM_MHS}</td>
             <td>{$list.NM_PENGGUNA}</td>
             <td>{$list.ALAMAT_ASAL_MHS}</td>
             <td>{$list.SEKOLAH_ASAL_MHS}</td>
             <td>{$list.MOBILE_MHS}</td>
             <td>{$list.NEM_PELAJARAN_MHS}</td>
             <td>{$list.NM_AGAMA}</td>
			 <td><center>{$list.KELAMIN}</center></td>
             <td>{$list.NM_KOTA}</td>
             <td>{$list.TGL_LAHIR_PENGGUNA}</td>
             <td>{$list.NM_JALUR}</td>
           </tr>
            {foreachelse}
        <tr><td colspan="12"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
   	</table>
{/if}
		
   