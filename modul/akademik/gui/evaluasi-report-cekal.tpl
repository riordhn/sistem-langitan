<div class="center_title_bar">REPORT CEKAL PEMBAYARAN MAHASISWA</div>

{if isset($cekal_prodi)}
<table>
	<tr>
		<th colspan="5" style="text-align:center">{if $smarty.get.cekal == 1}CEKAL{else}TIDAK CEKAL{/if}</th>
	</tr>
	<tr>
		<th>No</th>
		<th>NIM</th>
		<th>Nama</th>
		<th>Program Studi</th>
		<th>Status Akademik</th>
	</tr>
	{$no = 1}
	{foreach $cekal_prodi as $data}
	<tr>
		<td>{$no++}</td>
		<td><a href="evaluasi-report-cekal.php?id_mhs={$data.ID_MHS}">{$data.NIM_MHS}</a></td>
		<td>{$data.NM_PENGGUNA}</td>
		<td>{$data.PRODI}</td>
		<td>{$data.NM_STATUS_PENGGUNA}</td>
	</tr>
	{/foreach}
</table>	
{elseif isset($cekal_mhs)}
<form action="evaluasi-report-cekal.php" method="post" id="f1">
<table>
	<tr>
			<th colspan="3">Setting Cekal Pembayaran</th>
		</tr>
		<tr>
			<td>NIM</td>
			<td>:</td>
			<td>{$cekal_mhs[0]['NIM_MHS']}</td>
		</tr>
		<tr>
			<td>Nama</td>
			<td>:</td>
			<td>{$cekal_mhs[0]['NM_PENGGUNA']}</td>
		</tr>
		<tr>
			<td>Program Studi</td>
			<td>:</td>
			<td>{$cekal_mhs[0]['NM_PROGRAM_STUDI']}</td>
		</tr>
		<tr>
			<td>Status Akademik</td>
			<td>:</td>
			<td>{$cekal_mhs[0]['NM_STATUS_PENGGUNA']}</td>
		</tr>
		<tr>
			<td>Status Cekal Pembayaran</td>
			<td>:</td>
			<td>{if $cekal_mhs[0]['STATUS_CEKAL'] == 0}Boleh Bayar{else}<font color="#FF0000"><b>Tidak Boleh Bayar</b></font>{/if}</td>
		</tr>
		<tr>
			<td>History</td>
			<td>:</td>
			<td>
				{if $cekal_mhs[0]['ID_MHS'] == ''}
					Belum pernah tercekal pembayaran
				{else}
				<table>
					<tr>
						<th>Status Cekal</th>
						<th>Semester</th>
						<th>Keterangan</th>
					</tr>
					{foreach $cekal_mhs as $data}
					<tr>
						<td>{if $data.CEKAL_UNIVERSITAS == 0}Boleh Bayar{else}Tidak Boleh Bayar{/if}</td>
						<td>{$data.TAHUN_AJARAN} ({$data.NM_SEMESTER})</td>
						<td>{$data.KETERANGAN}</td>
					</tr>
					{/foreach}
				</table>
				{/if}
			</td>
		</tr>
	<tr>
			<td>
				Keterangan
			</td>
			<td>:</td>
			<td><textarea cols="60" rows="5" name="keterangan" id="keterangan" class="required"></textarea></td>
		</tr>
		<tr>
			<td style="text-align:center" colspan="3">
				<input type="submit" value="Proses Cekal" />
				<input type="hidden" name="id_mhs" value="{$smarty.get.id_mhs}" />
				<input type="hidden" name="mode" value="proses_cekal" />
			</td>
		</tr>
</table>
</form>
<script>$('#f1').validate();</script>
{else}
<table>
	<tr>
		<th>Program Studi</th>
		<th>Cekal</th>
		<th>Tidak Cekal</th>
	</tr>
	{foreach $cekal as $data}
	<tr>
		<td>{$data.PRODI}</td>
		<td style="text-align:center"><a href="evaluasi-report-cekal.php?id={$data.ID_PROGRAM_STUDI}&cekal=1">{$data.CEKAL}</a></td>
		<td style="text-align:center"><a href="evaluasi-report-cekal.php?id={$data.ID_PROGRAM_STUDI}&cekal=0">{$data.TIDAK_CEKAL}</a></td>
	</tr>
	{$total_cekal = $total_cekal + $data.CEKAL}
	{$total_tcekal = $total_tcekal + $data.TIDAK_CEKAL}
	{/foreach}
	<tr>
		<td>Total</td>
		<td style="text-align:center"><a href="evaluasi-report-cekal.php?cekal=1">{$total_cekal}</a></td>
		<td style="text-align:center"><a href="evaluasi-report-cekal.php?cekal=0">{$total_tcekal}</a></td>
	</tr>
</table>
{/if}