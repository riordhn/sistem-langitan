<div class="center_title_bar">Set Lulus Mahasiswa</div>  

<h2><font color="#FF0000">{$pesan}</font></h2>
{if $mode == 'view'}
<table>
  <tr>
      <th colspan="6" style="text-align:center">Data Mahasiswa Wisuda</th>
    </tr>
    <tr>
      <th>No.</th>
        <th>NIM</th>
        <th>Nama</th>
        <th>Program Studi</th>
        <th>Waktu Wisuda</th>
        <th>Detail</th>
    </tr>
    {foreach $pengajuan_wisuda as $data}
    
    <tr>
        <td class="center">{$data@index + 1}</td>
        <td>{$data.NIM_MHS}</td>
        <td>{$data.NM_PENGGUNA}</td>
        <td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
        <td>{$data.NM_TARIF_WISUDA}</td>
        <td><a href="wisuda-kelulusan.php?mode=detail&id_mhs={$data.ID_MHS}"><b style="color: red">Edit</b></a></td>
    </tr>
    {foreachelse}
    <tr>
        <td class="center" colspan="6">Maaf, Belum Ada Mahasiswa Sudah Yudisium Dengan Data Lengkap..</td>
    </tr>
    {/foreach}
</table>
{elseif $mode == 'detail'}
<form method="post" name="lengkap" id="lengkap" action="wisuda-kelulusan.php">
<h2><font color="#FF0000">Mohon Periksa Kembali Data Mahasiswa Calon Lulus Sebelum Melakukan Set Lulus Mahasiswa!</font></h2>
  <table width="500">
  <tr>
    <th colspan="2" style="text-align:center">Biodata</th>
  </tr>
  <tr>
    <td>NIM</td>
    <td>{$pengajuan_wisuda[0]['NIM_MHS']}</td>
  </tr>
  <tr>
    <td>Nama</td>
    <td>{$pengajuan_wisuda[0]['NM_PENGGUNA']}</td>
  </tr>
  <tr>
    <td>Program Studi</td>
    <td>{$pengajuan_wisuda[0]['NM_JENJANG']} - {$pengajuan_wisuda[0]['NM_PROGRAM_STUDI']}</td>
  </tr>
  <tr>
    <td>Periode</td>
    <td>{if $pengajuan_wisuda[0]['NM_TARIF_WISUDA'] == ' - '}{$periode['NM_TARIF_WISUDA']}{else}{$pengajuan_wisuda[0]['NM_TARIF_WISUDA']}{/if}</td>
  </tr>
    <tr>
      <td>Tanggal Keluar</td>
      <td>
        {$tgl_keluar}       
      </td>
    </tr>
    <tr>
      <td>Nomor SK Yudisium</td>
      <td>{$pengajuan_wisuda[0]['SK_YUDISIUM']}</td>
    </tr>
    <tr>
      <td>Tanggal SK Yudisium</td>
      <td>
          {$tgl_sk_yudisium}       
      </td>
    </tr>
    <tr>
      <td>IPK</td>
      <td>{$pengajuan_wisuda[0]['IPK']}</td>
    </tr>
    <tr>
      <td>No Seri Ijazah</td>
      <td>{$pengajuan_wisuda[0]['NO_IJASAH']}</td>
    </tr>
    <tr>
      <td>Judul Skripsi</td>
      <td>{$pengajuan_wisuda[0]['JUDUL_TA']}</td>
    </tr>
    <tr>
      <td>Judul Skripsi (English)</td>
      <td>{$pengajuan_wisuda[0]['JUDUL_TA_EN']}</td>
    </tr>
    <tr>
      <td>Bulan Awal Bimbingan</td>
      <td>
          {$bulan_awal_bimbingan}        
      </td>
    </tr>
    <tr>
      <td>Bulan Akhir Bimbingan</td>
      <td>
          {$bulan_akhir_bimbingan}        
      </td>
    </tr>
    <tr>
      <td>Keterangan <br/>*Boleh Kosong</td>
      <td>
          <textarea cols="50" rows="3" name="keterangan"></textarea>        
      </td>
    </tr>
    <tr>
      <td colspan="2" style="text-align:center">
            <input type="hidden" value="{$pengajuan_wisuda[0]['NIM_MHS']}" name="nim" />
            <input type="hidden" value="{$pengajuan_wisuda[0]['ID_MHS']}" name="id_mhs" />
            <input type="hidden" value="{$pengajuan_wisuda[0]['ID_PENGAJUAN_WISUDA']}" name="id_pengajuan_wisuda" />
            <input type="hidden" value="lulus" name="mode" />
            {if $pengajuan_wisuda[0]['YUDISIUM'] == '2'}
                <input type="submit" value="Set Lulus" />
            {else}
               Silakan Cek Status Yudisium Mahasiswa
            {/if}
      </td>
    </tr>
    </table>
</form>
{/if}
