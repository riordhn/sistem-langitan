<div class="center_title_bar">Master Fakultas </div>
<div id="tabs">
	<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Update</div>
</div>
<div class="panel" id="panel1" style="display: {$disp1}">
<p> </p>
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
		{foreach item="datafak" from=$T_FAK}
		<tr>
          <td>Nama Fakultas</td>
		  <td><center>:</center></td>
		  <td>{$datafak.NM_FAKULTAS}</td>
        </tr>
		<tr>
          <td>Nama Fakultas (English)</td>
		  <td><center>:</center></td>
		  <td>{$datafak.NM_FAKULTAS_ENG}</td>
        </tr>
        <tr>
          <td>Alamat Fakultas</td>
		  <td><center>:</center></td>
		  <td>{$datafak.ALAMAT_FAKULTAS}</td>
        </tr>
		<tr>
          <td>Alamat Fakultas (English)</td>
		  <td><center>:</center></td>
		  <td>{$datafak.ALAMAT_FAKULTAS_ENG}</td>
        </tr>
        <tr>
          <td>Kode Pos</td>
		  <td><center>:</center></td>
		  <td>{$datafak.KODEPOS_FAKULTAS}</td>
        </tr>
        <tr>
          <td>No. Telepon Fakultas</td>
		  <td><center>:</center></td>
		  <td>{$datafak.TELPON_FAKULTAS}</td>
        </tr>
        <tr>
          <td>No. Faksimili Fakultas</td>
		  <td><center>:</center></td>
		  <td>{$datafak.FAKSIMILI_FAKULTAS}</td>
        </tr>
        <tr>
          <td>Alamat Website Fakultas</td>
		  <td><center>:</center></td>
		  <td>{$datafak.WEBSITE_FAKULTAS}</td>
        </tr>
        <tr>
          <td>Alamat Email Fakultas</td>
		  <td><center>:</center></td>
		  <td>{$datafak.EMAIL_FAKULTAS}</td>
        </tr>
        <tr>
          <td>Kode Ijazah</td>
		  <td><center>:</center></td>
		  <td>{$datafak.KODE_IJAZAH_FAKULTAS}</td>
        </tr>
         {foreachelse}
        <tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
      </table>
</div>
<div class="panel" id="panel2" style="display: {$disp2}">
<p> </p>
	<form action="fakultas.php" method="post" >
	 <input type="hidden" name="action" value="update" />
	 <input type="hidden" name="kdfak" value="{$FAK}" />
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
		{foreach item="updfak" from=$T_FAK}
		<tr>
          <td>Nama Fakultas</td>
		  <td><center>:</center></td>
		  <td><input type="text" name="nmfak" value="{$updfak.NM_FAKULTAS}" style="width:500px;" /></td>
        </tr>
		<tr>
          <td>Nama Fakultas (English)</td>
		  <td><center>:</center></td>
		  <td><input type="text" name="nmfakeng" value="{$updfak.NM_FAKULTAS_ENG}" style="width:500px;" /></td>
        </tr>
        <tr>
          <td>Alamat Fakultas</td>
		  <td><center>:</center></td>
		  <td><input type="text" name="alamat" value="{$updfak.ALAMAT_FAKULTAS}" style="width:500px;" /></td>
        </tr>
		<tr>
          <td>Alamat Fakultas (English)</td>
		  <td><center>:</center></td>
		  <td><input type="text" name="alamateng" value="{$updfak.ALAMAT_FAKULTAS_ENG}" style="width:500px;" /></td>
        </tr>
        <tr>
          <td>Kode Pos</td>
		  <td><center>:</center></td>
		  <td><input type="text" name="kdpos" value="{$updfak.KODEPOS_FAKULTAS}" style="width:150px;" /></td>
        </tr>
        <tr>
          <td>No. Telepon Fakultas</td>
		  <td><center>:</center></td>
		  <td><input type="text" name="telp" value="{$updfak.TELPON_FAKULTAS}" style="width:300px;" /></td>
        </tr>
        <tr>
          <td>No. Faksimili Fakultas</td>
		  <td><center>:</center></td>
		  <td><input type="text" name="fax" value="{$updfak.FAKSIMILI_FAKULTAS}" style="width:300px;" /></td>
        </tr>
        <tr>
          <td>Alamat Website Fakultas</td>
		  <td><center>:</center></td>
		  <td><input type="text" name="web" value="{$updfak.WEBSITE_FAKULTAS}" style="width:300px;" /></td>
        </tr>
        <tr>
          <td>Alamat Email Fakultas</td>
		  <td><center>:</center></td>
		  <td><input type="text" name="email" value="{$updfak.EMAIL_FAKULTAS}" style="width:300px;" /></td>
        </tr>
         <tr>
          <td>Kode Ijazah Fakultas</td>
		  <td><center>:</center></td>
		  <td><input type="text" name="kode_ijazah" value="{$updfak.KODE_IJAZAH_FAKULTAS}" style="width:300px;" /></td>
        </tr>
        {/foreach}
	  </table>
	  	<p><input type="submit" name="tambah" value="Update"></p>
	  </form>
</div>
