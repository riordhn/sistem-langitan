<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
             <th>ID</th>
             <th>Nim</th>
             <th>Nama Mahasiswa</th>
			 <th>Tgl Lahir</th>
			 <th>Kelamin</th>
			 <th>THN MSK</th>
			 <th>NUN</th>
             <th>SKS SMT</th>
			 <th>IPS</th>
			 <th>TTL SKS</th>
			 <th>IPK</th>
			 <th>SMT</th>
           </tr>
		</thead>
		<tbody>
		   {foreach name=test item="list" from=$T_MK}
           <tr>
             <td>{$list.ID_MHS}</td>
             <td>{$list.NIM_MHS}</td>
             <td>{$list.NM_PENGGUNA}</td>
			 <td><center>{$list.TGL_LAHIR}</center></td>
			 <td><center>{$list.KELAMIN}</center></td>
			 <td><center>{$list.THN_ANGKATAN_MHS}</center></td>
			 <td><center>{$list.NEM_PELAJARAN}</center></td>
             <td><center>{$list.SKS_SEM}</center></td>
			 <td><center>{$list.IPS}</center></td>
			 <td><center>{$list.SKS_TOTAL}</center></td>
			 <td><center>{$list.IPK}</center></td>
			 <td><center>{$list.SMT}</center></td>
           </tr>
        {/foreach}
		</tbody>
</table>
