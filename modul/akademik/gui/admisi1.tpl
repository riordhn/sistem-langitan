<div class="center_title_bar">ADMISI MAHASISWA </div>
		
		
		  
        <div id="tabs">
        <div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
        <div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Input</div>
		<div id="tab3" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Update</div>
    	</div>
        <div class="tab_bdr"></div>
    <div class="panel" id="panel1" style="display: {$disp1}">
	
	<br />
	  <table width="90%" border="0" cellspacing="0" cellpadding="0">
        <tr class="left_menu">
		  <td width="5%" bgcolor="#333333"><font color="#FFFFFF">No</font></td>
          <td width="10%" bgcolor="#333333"><font color="#FFFFFF">Nim Mhs</font></td>
          <td width="20%" bgcolor="#333333"><font color="#FFFFFF">Nama Mhs</font></td>
		  <td width="20%" bgcolor="#333333"><font color="#FFFFFF">Prodi</font></td>
          <td width="5%" bgcolor="#333333"><font color="#FFFFFF">Status AKD</font> </td>
          <td width="5%" bgcolor="#333333"><font color="#FFFFFF">Status APV</font></td>
		  <td width="10%" bgcolor="#333333"><font color="#FFFFFF">SK</font></td>
		  <td width="5%" bgcolor="#333333"><font color="#FFFFFF">TGL Usulan</font></td>
		  <td width="5%" bgcolor="#333333"><font color="#FFFFFF">TGL Apv</font></td>
		  <td width="5%" bgcolor="#333333"><font color="#FFFFFF">TGL SK</font></td>
		  <td width="10%" bgcolor="#333333"><font color="#FFFFFF">NO IJS</font></td>
		  <td width="10%" bgcolor="#333333"><font color="#FFFFFF">TGL LLS</font></td>
		  <td width="10%" bgcolor="#333333"><font color="#FFFFFF">TOEFL</font></td>
          <td width="10%" bgcolor="#333333"><font color="#FFFFFF">Aksi</font></td>
        </tr>
		
		{foreach name=test item="datamhs" from=$T_ADMISI}
          <td>{$smarty.foreach.test.iteration}</td
          ><td >{$datamhs.NIM_MHS}</td>
          <td >{$datamhs.NM_PENGGUNA}</td>
		  <td >{$datamhs.NM_PROGRAM_STUDI}</td>
          <td >{$datamhs.STATUS_AKD_MHS}</td>
          <td >{$datamhs.STATUS_APV}</td>
		  <td >{$datamhs.NO_SK}</td>
          <td >{$datamhs.TGL_USULAN}</td>
		  <td >{$datamhs.TGL_APV}</td>
		  <td >{$datamhs.TGL_SK}</td>
		  <td >{$datamhs.NO_IJASAH}</td>
		  <td >{$datamhs.TGL_LULUS}</td>
		  <td >{$datamhs.TOEFL}</td>
          <td ><a href="admisi.php?action=tampil&idmhs={$datamhs.ID_MHS}">Update</a> | <a href="admisi.php?action=tampil&idmhs={$datamhs.ID_MHS}">DEL</a>
        </tr>
         {foreachelse}
        <tr><td colspan="13"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
      </table>
	  <p>&nbsp;</p>
</div>


<div class="panel" id="panel2" style="display: {$disp2}"><br>
	<form action="admisi.php" method="post" >
	 <input type="hidden" name="action" value="add" />
				  <table class="tb_frame" width="70%" border="0" cellspacing="0" cellpadding="0">
					<tr>
					  <td width="28%">NIM</td>
					  <td width="1%">:</td>
					  <td><input type="text" name="nim"></td>
					</tr>
					<tr>
					  <td>Status Akademik </td>
					  <td>:</td>
					  <td><input type="text" name="status_akd_mhs"></td>
				    </tr>
					<tr>
					  <td>Semester </td>
					  <td>:</td>
					  <td><input type="text" name="id_semester"></td>
				    </tr>
					<tr>
					  <td>SK </td>
					  <td>:</td>
					  <td><input type="text" name="no_sk"></td>
				    </tr>
					<tr>
					  <td>No Ijasah </td>
					  <td>:</td>
					  <td><input type="text" name="no_ijasah"></td>
				    </tr>
					<tr>
					  <td>Tgl Lulus</td>
					  <td>:</td>
					  <td></td>
					</tr>
					<tr>
					  <td>TOEFL</td>
					  <td>:</td>
					  <td><input type="text" name="toefl"></td>
					</tr>
				  </table>
				  <td><input type="submit" name="Submit" value="Tambah"></td>
	  </form>
</div>

<div class="panel" id="panel3" style="display: {$disp3}"><br>
	<form action="admisi.php" method="post" >
	 <input type="hidden" name="action" value="update" />
				  <table class="tb_frame" width="70%" border="0" cellspacing="0" cellpadding="0">
					{foreach item="data" from=$T_ADMISI1}
					<input type="hidden" name="id_mhs" value="{$data.ID_MHS}" />
					<tr>
					  <td width="28%">NIM</td>
					  <td width="1%">:</td>
					  <td>{$data.NIM_MHS}</td>
					</tr>
					<tr>
					  <td>Nama Mahasiswa </td>
					  <td>:</td>
					  <td>{$data.NM_PENGGUNA}</td>
				    </tr>
					<tr>
					  <td>Status Akademik </td>
					  <td>:</td>
					  <td><input type="text" name="ips" class="required" value="{$data.STATUS_AKD_MHS}" ></td>
				    </tr>
					<tr>
					  <td>Semester </td>
					  <td>:</td>
					  <td><input type="text" name="ips" class="required" value="{$data.STATUS_AKD_MHS}" ></td>
				    </tr>
					<tr>
					 <tr>
					  <td>NO Ijasah</td>
					  <td>:</td>
					  <td><input type="text" name="sksttl" class="required" value="{$data.NO_IJASAH}"></td>
				    </tr>
					<tr>
					  <td>Tgl Lulus</td>
					  <td>:</td>
					   <td></td>
				    </tr>

					{/foreach}
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td><input type="submit" name="tambah" value="Update"></td>
					</tr>
				  </table>
	  </form>
</div>
<script>$('form').validate();</script>
