
<div class="center_title_bar">EDIT NILAI (SEKALI EKSEKUSI) : {$nim}</div> 
<form action="edit-nilai.php" method="post">
<input type="hidden" name="action" value="viewdetail" >
     <table width="100%" border="1">
        <tr>
               <td>NIM</td>
               <td>:</td>
               <td><input name="nim" type="text" value="{if !empty($smarty.post.nim)}{$smarty.post.nim}{/if}"></td>
			   <td><input type="submit" name="Submit" value="Tampil"></td>
        </tr>
	</table>
</form>	

<div id="tabs">
	<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">EDIT NILAI</div>
</div>

<div class="panel" id="panel1" style="display: {$disp1}">
<p> </p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <!-- <tr>
			 <td colspan="7"><center>*Jika TOMBOL EDIT tidak MUNCUL, Hubungi Wadek I Untuk melakukan Granted PERUBAHAN NILAI</center></td>
		  </tr>	 -->
          <tr>
			 <td width="5%" bgcolor="#FFCC33"><center>No</center></td>
             <td width="5%" bgcolor="#FFCC33"><center>Kode MA</center></td>
			 <td width="20%" bgcolor="#FFCC33"><center>Nama MA</center></td>
             <td width="5%" bgcolor="#FFCC33"><center>SKS</center></td>
             <td width="10%" bgcolor="#FFCC33"><center>Smt</center></td>
			 <td width="5%" bgcolor="#FFCC33"><center>Nilai</center></td>
			 <td width="10%" bgcolor="#FFCC33"><center>Aksi</center></td>
		  </tr>
		  {foreach $T_NILAI as $list}
			<tr>
			 <td>{$list@index+1}</td>
             <td>{$list.KD_MATA_KULIAH}</td>
			 <td>{$list.NM_MATA_KULIAH}</td>
             <td><center>{$list.KREDIT_SEMESTER}</center></td>
             <td>{$list.SMT}</td>
			 <td><center>{$list.NILAI_HURUF}</center></td>
             <!-- sementara -->
             {$list.STATUS = 1}
			 {if $list.STATUS==1 }
			 <td height="0" align="center"><center><input name="Button" type="button" class="button" onClick="location.href='#aktivitas-updnilai!edit-nilai.php?action=viewproses&id_pmk={$list.ID_PENGAMBILAN_MK}&id_log={$list.ID_LOG_GRANTED_NILAI}&nim={$nim}';disableButtons()" onMouseOver="window.status='Click untuk Insert';return true" onMouseOut="window.status='Edit Nilai'" value="EDIT NILAI" /></center> </td> 
			 {else}
			 <td></td>
			 {/if}
			</tr>
		  {/foreach}
</table>
</div>


<div>

<div class="panel" id="panel2" style="display: {$disp2} ">
<p> </p>
<form action="edit-nilai.php" method="post" name="id_edit_nilai" id="id_edit_nilai">
<input type="hidden" name="action" value="proses" >
<input type="hidden" name="id_pengambilan_mk" value="{$datamk.ID_PENGAMBILAN_MK}" >

	<table>
        <tr>
        	<td>Kode MA</td>
            <td>{$datamk.KD_MATA_KULIAH}</td>
        </tr>
		<tr>
        	<td>Nama MA</td>
            <td>{$datamk.NM_MATA_KULIAH}</td>
        </tr>
		<tr>
        	<td>SKS</td>
            <td>{$datamk.KREDIT_SEMESTER}</td>
        </tr>
		<tr>
        	<td>Nilai Lama</td>
            <td>{$datamk.NILAI_HURUF}</td>
        </tr>
		<tr>
        	<td>Semester</td>
            <td>{$datamk.SMT}</td>
        </tr>
        {if ($datamk.ADA_KELAS > 0)}
        <tr>
            <td>Nilai Angka Baru</td>
            <td>
                <input type="text" name="nilai_angka" maxlength="5" size="5" />
            </td>
        </tr>
        {/if}
        <tr>
        	<td>Nilai Huruf Baru</td>
            <td>
            	<input type="text" name="nilai" maxlength="2" size="2" class="required"/>
            </td>
        </tr>
        <tr>
            <td>Status Kelas</td>
            <td>
                {if ($datamk.ADA_KELAS < 1)} Tidak Ada {else} Ada - {$datamk.NM_PENGGUNA} ({$datamk.USERNAME}) {/if}
            </td>
        </tr>
		<tr>
        	<td>Alasan</td>
            <td>
                <select name="catatan_pilih">
                    <option value='0'>Lain-Lain</option>
                    <option value='Penyesuaian Transkrip'>Penyesuaian Transkrip</option>
                    <option value='Ujian Perbaikan'>Ujian Perbaikan</option>
                </select>
                <br/>
            	<textarea name="catatan" cols="60"></textarea><br/><strong>[WAJIB DI-ISI APABILA ALASAN TIDAK TERDAPAT PADA DAFTAR]</strong>
            </td>
        </tr>
        <tr>
        	<td colspan="2" style="text-align:center">
            	<input type="hidden" value="{$nim}" name="nim" />
				<input type="hidden" value="{$log}" name="log" />
                <input type="hidden" value="{$datamk.ADA_KELAS}" name="status_kelas" />
            	<input type="submit" name="Submit" value="Proses">
            </td>
        </tr>
    </table>
</form>	
</div>


	
{literal}
    <script>
			$("#id_edit_nilai").validate();
    </script>
{/literal}
    