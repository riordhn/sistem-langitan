<div class="center_title_bar">Presensi Susulan</div>
<div id="tabs">
<div id="tab1" {if $smarty.get.action=='entry'}class="tab_sel"{else}class="tab"{/if} align="center" onclick="javascript: displayPanel('1');">Rincian</div>
<div id="tab2" {if $smarty.get.action=='presensi'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Entry Susulan</div>
</div>
<div class="panel" id="panel1" style="display: {$disp1} ">
<p> </p>
	<table class="tb_frame">
	{foreach item="dtkelas" from=$DT_KELAS}
	  <tr>
		<td>Kode Mata Ajar</td>
		<td>:</td>
		<td>{$dtkelas.KD_MATA_KULIAH}</td>
	  </tr>
	  <tr>
		<td>Nama Mata Ajar</td>
		<td>:</td>
		<td>{$dtkelas.NM_MATA_KULIAH}</td>
	  </tr>
	  <tr>
		<td>SKS</td>
		<td>:</td>
		<td>{$dtkelas.KREDIT_SEMESTER}</td>
	  </tr>
	  <tr>
		<td>Kelas</td>
		<td>:</td>
		<td>{$dtkelas.NAMA_KELAS}</td>
	  </tr>
	  <tr>
		<td>Ruang</td>
		<td>:</td>
		<td>{$dtkelas.NM_RUANGAN}</td>
	  </tr>
	 {/foreach}
	</table>
	<table>
	<thead>
	<tr>
	 <th><center>Kuliah ke</center></th>
	 <th><center>Tgl</center></th>
	 <th><center>Jam</center></th>
	 <th><center>Materi</center></th>
	 <th><center>Dosen</center></th>
	 <th><center>MHS Msk</center></th>
	 <th><center>Absen</center></th>
	 <th><center>Aksi</center></th>
	</tr>
	</thead>
	<tbody>
		{foreach name=test item="presensi" from=$DATA_PRESENSI}
	<tr>
	 <td><center>{$smarty.foreach.test.iteration}</center></td>
	 <td><center>{$presensi.TGL_PRESENSI_KELAS}</center></td>
	 <td><center>{$presensi.JAM}</center></td>
	 <td>{$presensi.ISI_MATERI_MK}</td>
	 <td><ol>{$presensi.NMDOS}</ol></td>
	 <td><center>{$presensi.HADIR}</center></td>
	 <td><center>{$presensi.ABSEN}</center></td>
	 <td><center><a href="entry-presensi-susulan-psikologi.php?action=presensi&id_presensi={$presensi.IDPRESENSI}&id_kelas={$smarty.get.id_kelas_mk}" onclick="displayPanel('2')">Entry Susulan</a></center></td>
	</tr>
		{foreachelse}
		<tr><td colspan="9"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
	</tbody>
	</table>
</div>
<div class="panel" id="panel2" style="display: {$disp2}">
{if $smarty.get.id_presensi == ''}
<p> </p>
{else}
<p> </p>
<table class="tb_frame">
	 {foreach item="list" from=$DT_SUSULAN}
	  <tr>
		<td width="30%">Tgl Kuliah</td>
		<td width="1%">:</td>
		<td width="69%">{$list.TGL_PRESENSI_KELAS}</td>
	  </tr>
	  <tr>
		<td>Jadwal Kuliah</td>
		<td>:</td>
		<td>{$list.WAKTU_MULAI} - {$list.WAKTU_SELESAI}</td>
	  </tr>
	  <tr>
		<td>Materi</td>
		<td>:</td>
		<td>{$list.ISI_MATERI_MK}</td>
	  </tr>
	  {/foreach}
</table>
<form action="entry-presensi-susulan.php" method="post">
<input type="hidden" name="action" value="susulan">
<input type="hidden" name="id_kelas" value="{$smarty.get.id_kelas}">
<input type="hidden" name="id_presensi" value="{$smarty.get.id_presensi}">
NIM	: <input type="text" name="nim">
&nbsp;
Presensi : 
<select name="ket">
    <option value='0'>Tidak hadir</option>
    <option value='1'>Hadir</option>
    <option value='2'>Ijin</option>
    <option value='3'>Sakit</option>
</select>
&nbsp;
<input type="submit" name="simpan" value="Simpan">
&nbsp;
<input type="button" name="batal" value="Batal" onclick="javascript:history.go(-1);" />
</form>
<p> </p>
{/if}
</div>