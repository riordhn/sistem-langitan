{literal}
<link href="js/jquery.dataTables.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('table.display').dataTable({
		"sDom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>',
		"sPaginationType": "full_numbers",
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Semua"]]
	});
});
</script>
{/literal}

<div class="center_title_bar">Usulan / Penawaran Mata Ajar</div>
<form action="usulan-mk-psikologi.php" method="post" >
<input type="hidden" name="action" value="view" >
<table>
    <tr>
		<td>
		Prodi : 
		<select name="kdprodi">
	 	   {foreach item="list" from=$T_PRODI}
    	   {html_options  values=$list.ID_PROGRAM_STUDI output=$list.PRODI selected=$PRODI}
	 	   {/foreach}
		</select>
		Tahun ajaran : 
		<select name="thakd">
	 	   {foreach item="list" from=$T_SMT}
    	   {html_options  values=$list.ID_SEMESTER output=$list.SMT selected=$SMT_AKTIF}
	 	   {/foreach}
		</select>
		<input type="submit" name="Submit" value="Preview">
        </td>
    </tr>
</table>
</form>

<table id="" class="display" cellspacing="0" width="100%">
	<thead>
	<tr>
		<th>Kode</th>
		<th>Nama Mata Ajar</th>
		<th>SKS</th>
		<th>Kelas</th>
		<th>Tim Pengajar</th>
		<th>Ruang</th>
		<th>Hari</th>
		<th>Jam</th>
	</tr>
	</thead>
	<tbody>
	{foreach name=test item="list" from=$T_USULMK}
	<tr>
		<td>{$list.KD_MATA_KULIAH}</td>
		<td>{$list.NM_MATA_KULIAH}</td>
		<td><center>{$list.KREDIT_SEMESTER}</center></td>
		<td><center>{$list.NAMA_KELAS}</center></td>
		<td style="padding-left:10px;"><br><ol>{$list.PENGAJAR}</li></ol></td>
		<td><center>{$list.NM_RUANGAN}</center></td>
		<td><center>{$list.NM_JADWAL_HARI}</center></td>
		<td><center>{$list.NM_JADWAL_JAM}</center></td>
	</tr>
	{foreachelse}
	<tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
	<tfoot>
	<tr>
		<th>Kode</th>
		<th>Nama Mata Ajar</th>
		<th>SKS</th>
		<th>Kelas</th>
		<th>Tim Pengajar</th>
		<th>Ruang</th>
		<th>Hari</th>
		<th>Jam</th>
	</tr>
	</tfoot>
</table>
</p><br>&nbsp;</p>