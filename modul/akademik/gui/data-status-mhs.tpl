<div class="center_title_bar">Data Status Mahasiswa</div>
{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]]
		}
	); 
} 
);
</script>
{/literal}
<table>
    <tr>
        <td>NIM/ Nama</td>
        <td><form action="data-status-mhs.php" method="post" id="fcari">
            	<input name="nim" type="text" value="{if $smarty.get.nim != ''}{$smarty.get.nim}{else}{$smarty.post.nim}{/if}" size="30" class="required" />
          		<input type="submit" value="Cari" />
            </form>
        </td>
    </tr>
</table>


{if isset($data_mahasiswa)}
    <table>
        <tr>
            <th>NAMA</th>
            <th>NIM</th>
            <th>FAKULTAS</th>
            <th>PROGRAM STUDI</th>
			<th>STATUS AKADEMIK</th>
            <th></th>
        </tr>
        {if $data_mahasiswa==null}
            <tr>
                <td colspan="8" class="center">Hasil Pencarian Tidak Ditemukan</td>
            </tr>
        {else}
            {foreach $data_mahasiswa as $data}
                <tr>
                    <td>{$data.NM_PENGGUNA}</td>
                    <td>{$data.NIM_MHS}</td>
                    <td>{$data.NM_FAKULTAS}</td>
                    <td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
					<td>{$data.NM_STATUS_PENGGUNA}</td>
                    <td><a href="data-status-mhs.php?nim={$data.NIM_MHS}">Detail</a></td>
                </tr>
            {/foreach}
        {/if}
    </table>
	<a href="data-status-mhs.php" class="button">Kembali</a>
{/if}

{if isset($mhs)}
<table>
	<tr>
    	<td style="text-align:center" colspan="2">
        	<img src="{$base_url}foto_mhs/{$nama_singkat}/{$mhs[0]['NIM_MHS']}.jpg"  width="150" height="180" />
        </td>
    </tr>
    <tr>
    	<th colspan="2">BIODATA MAHASISWA</th>
    </tr>
    <tr>
    	<td>NIM</td>
        <td>{$mhs[0]['NIM_MHS']}</td>
    </tr>
    <tr>
    	<td>Nama</td>
        <td>{$mhs[0]['NM_PENGGUNA']}</td>
    </tr>
    <tr>
    	<td>Fakultas</td>
        <td>{$mhs[0]['NM_FAKULTAS']}</td>
    </tr>
    <tr>
    	<td>Prodi</td>
        <td>{$mhs[0]['NM_PROGRAM_STUDI']} ({$mhs[0]['NM_JENJANG']})</td>
    </tr>
    <tr>
    	<td>Status Akademik</td>
        <td>{$mhs[0]['NM_STATUS_PENGGUNA']}</td>
    </tr>
    <tr>
    	<td>Status Pembayaran</td>
        <td>{if $mhs[0]['STATUS_CEKAL'] == 1}Cekal{else}Tidak Cekal{/if}</td>
    </tr>
	<tr>
    	<td>Dosen Wali</td>
        <td>{$mhs[0]['DOSEN_WALI']}</td>
    </tr>
</table>

	<table cellpadding=3 cellspacing=0 border=1 align=center>
	<tr><th colspan="9" style="text-align:center">Riwayat Status Mahasiswa</th></tr>
    <tr>
		<th align=center><B>No</B></th>
		<th align=center><B>Semester</B></th>
		<th align=center><B>Status</B></th>
		<th align=center><B>No. SK</B></th>
		<th align=center><B>Tgl. SK</B></th>
		<th align=center><B>Keterangan</B></th>
		<th align=center><B>Waktu</B></th>
		<th align=center><B>Tgl Lulus</B></th>
		<th align=center><B>No Ijasah</B></th>
	</tr>
	{$no=1}
	{foreach $admisi as $data}
		<tr>
			<td>{$no++}</td>
			<td>{$data.THN_AKADEMIK_SEMESTER}/{$data.NM_SEMESTER}</td>
			<td>{$data.NM_STATUS_PENGGUNA}</td>
			<td>{$data.NO_SK}</td>
			<td>{$data.TGL_SK}</td>
			<td>{$data.ALASAN}</td>
			<td>{if $data.KURUN_WAKTU <> ''}{$data.KURUN_WAKTU} Semester{/if}</td>
			<td>{$data.TGL_LULUS}</td>
			<td>{$data.NO_IJASAH}</td>
		</tr>
	{/foreach}
	</table>
	
	{if isset($pembayaran_mhs)}
		<table>
        	<tr><th colspan="9" style="text-align:center">Riwayat Pembayaran Mahasiswa</th></tr>
			<tr>
				<th>Tahun Ajaran</th>
				<th>Tgl Bayar</th>
				<th>Bank</th>
				<th>Via</th>
				<th>Status Tagihan</th>
				<th>Status Pembayaran</th>
				<th>Keterangan</th>
				<th>Besar Biaya</th>
				<th>Denda</th>
			</tr>
			{foreach $pembayaran_mhs as $data}
			
			<tr {if $data.ID_STATUS_PEMBAYARAN == 2} bgcolor="#CCFF66" {/if}>
				<td>{$data.TAHUN_AJARAN} ({$data.NM_SEMESTER})</td>
				<td>{$data.TGL_BAYAR}</td>
				<td>{$data.NM_BANK}</td>
				<td>{$data.NAMA_BANK_VIA}</td>
				<td style="text-align:center">{$data.IS_TAGIH}</td>
				<td>{$data.NAMA_STATUS}</td>
				<td>{$data.KETERANGAN}</td>
				<td style="text-align:right">{$data.BESAR_BIAYA|number_format:0:",":"."}</td>
				<td style="text-align:right">{$data.DENDA_BIAYA|number_format:0:",":"."}</td>
			</tr>
			{/foreach}
		</table>
	{/if}
    
    <table>
        <tr>
            <th colspan="6" style="text-align:center">HISTORY AKADEMIK MAHASISWA</th>
        </tr>
        <tr>
            <th style="text-align:center">No</th>
            <th style="text-align:center">Tahun Ajaran</th>
            <th style="text-align:center">IPS</th>
            <th style="text-align:center">SKS SEMESTER</th>
            <th style="text-align:center">IPK SEMESTER</th>
            <th style="text-align:center">Total SKS</th>
        </tr>
        {$ips}
    </table>
    
    {$isitranskrip}

    
{/if}