<div class="center_title_bar">Rekapitulasi Nilai {if isset($T_MHS)}{$T_MHS.NM_MHS} {/if} </div>
<form action="rekap_nilai.php" method="post" name="id_transkrip" id="id_transkrip">
	<input type="hidden" name="action" value="view" >
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td>NIM : <input id="nim_mhs" type="text" name="nim" value="{if !empty($smarty.post.nim)}{$smarty.post.nim}{/if}"></td>
			<td><input type="submit" name="View" value="View"></td>
		</tr>
	</table>
</form>

{if isset($T_MHS)}
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>NIM</td>
			<td><center>:</center></td>
			<td>{$T_MHS.NIM_MHS}<input type="hidden" id="nimx" name="nimx" value="{$T_MHS.NIM_MHS}" />
			</td>
		</tr>
		<tr>			 
			<td>Nama Mahasiswa</td>
			<td><center>:</center></td>
			<td>{$T_MHS.NM_PENGGUNA}</td>
		</tr>
		<tr>
			<td>Program Studi</td>
			<td><center>:</center></td>
			<td>{$T_MHS.NM_JENJANG} {$T_MHS.NM_PROGRAM_STUDI}</td>
		</tr>
		<tr>
			<td colspan='3'>
				<input type="button" name="cetak" value="Cetak" onclick="window.open('{$LINK}', 'baru');" />
				<input type="button" name="cetak" value="Cetak View" onclick="window.open('{$LINK3}', 'baru');" />
				<!-- FITUR INI BELUM AKTIF -->
				<!-- &nbsp;&nbsp;&nbsp;<input type="button" name="cetak" value="English" onclick="window.open('{$LINK1}','baru');">
				&nbsp;&nbsp;&nbsp;<input type="button" name="cetak" value="English-Dean" onclick="window.open('{$LINK2}','baru');"> -->
			</td>
		</tr>
	</table>
{/if}

{if isset($T_MK)}
	<table width="100%" class="tablesorter" border="0" cellspacing="0" cellpadding="0" id="tabel-mk">
		<thead>
			<tr>
				<th><center>No</center></th>
				<th><center>Semester</center></th>
				<th><center>Kode MK</center></th>
				<th><center>Nama MK</center></th>
				<th><center>SKS</center></th>
				<th><center>Nilai</center></th>
				<th><center>Bobot</center></th>
				<th><center>Transfer</center></th>
				<th><center>Transkrip</center></th>
			</tr>
		</thead>
		<tbody>
			{foreach $T_MK as $list}

				<tr>
					<td class="center">{$list@iteration}</td>
					<td class="center">{$list.NM_SEMESTER} {$list.TAHUN_AJARAN}</td>
					<td>{$list.KD_MATA_KULIAH}</td>
					<td>{$list.NM_MATA_KULIAH}</td>
					<td class="center">{$list.KREDIT_SEMESTER}</td>

					<td class="center">{$list.NILAI_HURUF}</td>
					<td class="center">{$list.BOBOT}</td>
					
					<td class="center">
						{if $list.STATUS_TRANSFER == 1}YA{/if}
					</td>

					<td class="center">
						{if $list.STATUS_HAPUS == 1}Hapus{/if}
					</td>
				</tr>
				
			{/foreach}
		</tbody>
		<tfoot>
			{$footer = $T_IPK}
			<tr>
				<td colspan="4"><b>Total SKS dan Bobot</b></td>
				<td class="center"><b>{$footer.TOTAL_SKS}</b></td>
				<td></td>
				<td class="center"><b>{$footer.TOTAL_BOBOT}</b></td>
			</tr>
			<tr>
				<td colspan="4"><b>Indeks Prestasi Kumulatif</b></td>
				<td colspan="3"><b>{$footer.IPK}</b></td>
			</tr>
		</tfoot>
	</table>
{/if}