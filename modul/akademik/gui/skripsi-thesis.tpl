{literal}
<script language="javascript" type="text/javascript" src="js/datetimepicker.js"></script>
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[0,0]],
		headers: {
            9: { sorter: false }
		}
		}
	);
}
);

function hapus(hapus_jadwal){
	$.ajax({
	type: "POST",
	url: "skripsi-thesis.php",
	data: "action=del&hapus="+hapus_jadwal,
	cache: false,
	success: function(data){
		if (data == ' ')
		{
			window.location.reload(true);
		}
		else
		{
			alert('gagal hapus');
		}
	}
	});
}

</script>
{/literal}

<div class="center_title_bar">JADWAL TA/SKRIPSI/THESIS/DESERTASI</div>
<div id="tabs">
	<div id="tab1" {if $smarty.get.action == 'penjadwalan' || $smarty.get.action == 'updateview' || $smarty.get.action == 'penguji' || $smarty.get.action == 'cari_penguji' || $smarty.get.action == 'ruang' || $smarty.get.action == 'tim'}class="tab"{else}class="tab_sel"{/if} align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" {if $smarty.get.action == 'penjadwalan'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Penjadwalan</div>
	<div id="tab3" {if $smarty.get.action == 'updateview'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Update</div>
	<div id="tab4" {if $smarty.get.action == 'penguji' || $smarty.get.action == 'cari_penguji'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('4');">Penguji</div>
</div>
<div class="panel" id="panel1" style="display: {$disp1} ">
<p><br/><input type="button" name="cetak_all" value="Cetak All Jadwal TA/SKRIPSI/THESIS/DESERTASI" onClick="window.open('proses/jadwal-skripsi-thesis-cetak.php?cetak=71&smt={$SMT}','baru');"></p>
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
             <th>NIM</th>
             <th>Nama Mahasiswa</td>
             <th>Prodi</th>
             <th>Jenis</th>
             <th>Tanggal</th>
             <th>Jam</th>
			 <th>Ruang</th>
			 <th>Judul</th>
			 <th>Tim Penguji</th>
             <th class="noheader">Aksi</th>
           </tr>
		</thead>
		<tbody>
           {foreach item="list" from=$T_MK}
		   {if $list.TGL_UJIAN == '' || $list.NM_RUANGAN == '' || $list.JAM|strlen < 13}
		   <tr bgcolor="#ff828e">
		   {elseif $list.TIM == '<li><font color="black"> , </font>'}
		   <tr bgcolor="#f2f3a2">
		   {else}
		   <tr>
		   {/if}
             <td>{$list.NIM_MHS}</td>
             <td>{$list.NM_PENGGUNA}</td>
             <td>{$list.PRODI}</td>
			 <td>{$list.NM_UJIAN_MK}</td>
			 <td><center>{$list.TGL_UJIAN}</center></td>
			 <td><center>{$list.JAM}</center></td>
			 <td><center>{$list.NM_RUANGAN}</center></td>
			 <td>{$list.JUDUL}</td>
			 <td>{$list.TIM}</td>
			 <td>
				<center>
				<a href="skripsi-thesis.php?action=updateview&ujian={$list.ID_UJIAN_MK}&jad={$list.ID_JADWAL_UJIAN_MK}" alt="Update jadwal" title="Update jadwal">Update</a><br/>
				<a href="skripsi-thesis.php" name="hapus_jadwal" id="hapus_jadwal{$list.ID_UJIAN_MK}" alt="Hapus jadwal" title="Hapus jadwal" onclick="javascript:if(confirm('Anda yakin untuk menghapus jadwal?'))hapus({$list.ID_UJIAN_MK});">Hapus</a><br/>
				<a href="skripsi-thesis.php?action=cari_penguji&ujian={$list.ID_UJIAN_MK}&jad={$list.ID_JADWAL_UJIAN_MK}" alt="Ploting penguji" title="Ploting penguji">Penguji</a><br/>
				</center>
			</td>
           </tr>
		   {foreachelse}
        <tr><td colspan="10"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
   	</table>
</div>

{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable1").tablesorter(
		{
		sortList: [[0,0]],
		headers: {
            5: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
<div class="panel" id="panel2" style="display: {$disp2} ">
<p> </p>
	<table id="myTable1" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
             <th>NIM</th>
             <th>Nama Mahasiswa</td>
             <th>Prodi</th>
			 <th>Judul</th>
			 <th>Progres</th>
             <th class="noheader">Aksi</th>
           </tr>
		</thead>
		<tbody>
           {foreach item="list" from=$TAWAR}
		   <tr>
             <td>{$list.NIM_MHS}</td>
             <td>{$list.NM_PENGGUNA}</td>
             <td>{$list.PRODI}</td>
             <td>{$list.JUDUL}</td>
			 <td style="text-align:right;">{$list.PROGRESS}&nbsp;&nbsp;&nbsp;</td>
			 {if $list.PEMBIMBING == null}
			 <td><center><i>Pembimbing belum diset</i></center></td>
			 {else}
			 <td><center><a href="skripsi-thesis.php?action=add&kls={$list.ID_KELAS_MK}&mhs={$list.ID_MHS}&nm={$list.NM_UJIAN}&jad={$list.ID_JADWAL_KELAS}">Jadwalkan</a></center></td>
			 {/if}
           </tr>
		   {foreachelse}
        <tr><td colspan="5"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
   	</table>
</div>

<div class="panel" id="panel3" style="display: {$disp3}">
{if $smarty.get.action == '' || $smarty.get.jad == ''}
<p> </p>
{else}
<p> </p>
<form action="skripsi-thesis.php" method="post">
<input type="hidden" name="action" value="update">
<table width="100%" class="tablesorter" cellspacing="0" cellpadding="0">
  <tr><th colspan="3"><strong> Update Jadwal TA/SKRIPSI/THESIS/DESERTASI </strong></th></tr>
	{foreach item="ubah" from=$UMK}
	<input type="hidden" name="ujian" value="{$ubah.ID_UJIAN_MK}" />
	<input type="hidden" name="jad" value="{$ubah.ID_JADWAL_UJIAN_MK}" />
	<tr>
	  <td>NIM</td>
	  <td>:</td>
	  <td>{$ubah.NIM_MHS}</td>
	</tr>
	<tr>
	  <td>Nama Mahasiswa</td>
	  <td>:</td>
	  <td>{$ubah.NM_PENGGUNA}</td>
	</tr>
	<tr>
	  <td>Prodi</td>
	  <td>:</td>
	  <td>{$ubah.PRODI}</td>
	</tr>
	<tr>
	  <td>Jenis</td>
	  <td>:</td>
	  <td>{$ubah.NM_UJIAN_MK}</td>
	</tr>
	<tr>
	  <td>Judul</td>
	  <td>:</td>
	  <td>{$ubah.JUDUL}</td>
	</tr>
	<tr>
	  <td>Tanggal Ujian</td>
	  <td>:</td>
	  <td><input type="text" name="tgl_ujian" id="tgl_ujian" maxlength="25" value="{$ubah.TGL_UJIAN}" onClick="NewCssCal('tgl_ujian','ddmmmyyyy');" /></td>
	</tr>
	<tr>
	  <td>Jam Mulai</td>
	  <td>:</td>
	  <td><input type="text" name="jam_mulai" value="{$ubah.JAM_MULAI}" maxlength="5" />&nbsp;<font color="grey"><em>Format jam: 08:00</em></font></td>
	</tr>
	<tr>
	  <td>Jam Selesai</td>
	  <td>:</td>
	  <td><input type="text" name="jam_selesai" value="{$ubah.JAM_SELESAI}" maxlength="5" />&nbsp;<font color="grey"><em>Format jam: 14:00</em></font></td>
	</tr>
	<tr>
	  <td>Ruangan</td>
	  <td>:</td>
	  <td>
		<select name="ruangan" id="ruangan">
			{foreach item="ruangan" from=$T_RUANG}
			{html_options values=$ruangan.ID_RUANGAN output=$ruangan.RUANG selected=$ubah.ID_RUANGAN}
			{/foreach}
		  </select>
	  </td>
	</tr>
	<tr>
		<td colspan="2"></td>
		<td><input type="button" name="kembali" value="Batal" onClick="window.location.href='#ujian-thesis!skripsi-thesis.php'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="Simpan1" value="Simpan"></td>
	</tr>
	{/foreach}
</table>
</form>
<table width="100%" class="tablesorter" cellspacing="0" cellpadding="0">
	<tr><th colspan="4"><strong> Tim Penguji Ujian </strong></th></tr>
	{foreach item="dsn" from=$DOSEN}
	<tr>
	  <td>{$dsn.PENGUJI}</td>
	</tr>
	{foreachelse}
    <tr>
	<td><em>Data tidak ditemukan</em></td></tr>
    {/foreach}
	<tr>
	  <td>&nbsp;</td>
	  <td><center><input type="button" name="add" value="Tambah" onClick="window.location.href='#ujian-thesis!skripsi-thesis.php?action=cari_penguji&ujian={$ubah.ID_UJIAN_MK}&jad={$ubah.ID_JADWAL_UJIAN_MK}'"></center></td>
	</tr>
</table>
{/if}
</div>

<div class="panel" id="panel4" style="display: {$disp4}">
{if $smarty.get.action == '' || $smarty.get.jad == ''}
<p> </p>
{else}
<p> </p>
<table width="100%" class="tablesorter" cellspacing="0" cellpadding="0">
  <tr><th colspan="3"><strong> Penguji TA/SKRIPSI/THESIS/DESERTASI </strong></th></tr>
	{foreach item="ubah" from=$UMK}
	<tr>
	  <td>NIM / Nama</td>
	  <td>:</td>
	  <td>{$ubah.NIM_MHS} / {$ubah.NM_PENGGUNA}</td>
	</tr>
	<tr>
	  <td>Prodi</td>
	  <td>:</td>
	  <td>{$ubah.PRODI}</td>
	</tr>
	<tr>
	  <td>Jenis</td>
	  <td>:</td>
	  <td>{$ubah.NM_UJIAN_MK}</td>
	</tr>
	<tr>
	  <td>Judul</td>
	  <td>:</td>
	  <td>{$ubah.JUDUL}</td>
	</tr>
	<tr>
	  <td>Tempat & Waktu Ujian</td>
	  <td>:</td>
	  <td>{$ruangan.RUANG}; tanggal: {$ubah.TGL_UJIAN}, pukul: {$ubah.JAM_MULAI} - {$ubah.JAM_SELESAI}</td>
	</tr>
	{/foreach}
</table>
<table width="100%" class="tablesorter" cellspacing="0" cellpadding="0">
	<tr><th colspan="4"><strong> Tim Penguji Ujian </strong></th></tr>
	{foreach item="dsn" from=$DOSEN}
	<tr>
	  <td>{$dsn.PENGUJI}</td>
	  <td><center><input type="button" name="del" value="Hapus" onClick="window.location.href='#ujian-thesis!skripsi-thesis.php?action=del_tim&tim={$dsn.ID_TIM_PENGAWAS_UJIAN}&ujian={$smarty.get.ujian}&jad={$smarty.get.jad}'"></center></td>
	</tr>
	{foreachelse}
    <tr>
	<td colspan="2"><em>Data tidak ditemukan</em></td></tr>
    {/foreach}
</table>
<form action="skripsi-thesis.php?action=cari_penguji&ujian={$smarty.get.ujian}&jad={$smarty.get.jad}" method="post" id="cari1">
<table width="100%" class="tablesorter" cellspacing="0" cellpadding="0">
<tr><th colspan="4"><strong> Tambah Tim Penguji Ujian </strong></th></tr>
	<tr>
		<td>Nama Penguji: <input name="namadosen" type="text" style="width:300px;" /> <input name="getdosen" type="hidden" value="1" /> <input type="submit" name="cari" value="Cari"></td>
	</tr>
</table>
</form>
<table width="100%" class="tablesorter" cellspacing="0" cellpadding="0">
	<tr>
	 <th>No</th>
	 <th>Photo</th>
	 <th>NIP/NIK</th>
	 <th>Nama</th>
	 <th>Prodi</th>
	 <th>Aksi</th>
	</tr>
	<tr>
	{foreach name=test item="list" from=$CARI_DOSEN}
	 <td>{$smarty.foreach.test.iteration}</td>
	 <td><img src="{$list.FOTO_PENGGUNA}/{$list.NIP_DOSEN}.JPG" width="160px"/></td>
	 <td>{$list.NIP_DOSEN}</td>
	 <td>{$list.NM_PENGGUNA}</td>
	 <td>{$list.NM_PROGRAM_STUDI}</td>
	 <td><a href="skripsi-thesis.php?action=add_tim&pgg={$list.ID_PENGGUNA}&jad={$smarty.get.jad}">Pilih</a></td>
	</tr>
	{/foreach}
</table>
{/if}
</div>