{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[3,0],[0,0],[2,1]],
		}
	);
}
);
</script>
{/literal}

<div class="center_title_bar">Master Kurikulum </div>
<form action="kurikulum.php?kirim=view" method="post" name="id_prodi" id="id_prodi">
<table class="tb_frame" width="80%" border="0" cellpadding="0" cellspacing="0">
   <tr>
	<td>Prodi :
	   <select name="kdprodi">
	   <option value=''>-- PRODI --</option>
	   {foreach item="list" from=$T_PRODI}
	   {html_options  values=$list.ID_PROGRAM_STUDI output=$list.PRODI selected=$smarty.request.kdprodi}
	   {/foreach}
	   </select>
	   <input type="submit" name="View" value="View"></td>
   </tr>
</table>
</form>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
		<th>Prodi</th>
		<th>Nama kurikulum</th>
		<th>Tahun</th>
		<th>Status</th>
		<th>NO SK</th>
		<th>Berlaku</th>
	</tr>
	</thead>
	<tbody>
  {foreach item="list" from=$T_KUR}
	<tr>
    <td>{$list.NM_PROGRAM_STUDI}</td>
    <td>{$list.NM_KURIKULUM}</td>
    <td>{$list.TAHUN_KURIKULUM}</td>
    <td>{$list.STATUS}</td>
    <td>{$list.NOMOR_SK_KURIKULUM}</td>
    <td>{$list.MASA}</td>
	</tr>
  {foreachelse}
	<tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
  {/foreach}
  </tbody>
</table>