{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
            8: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}

<div class="center_title_bar">Cetak Presensi UAS {foreach item="list1" from=$NM_PRODI}{$list1.PRODI} {/foreach}</div>
<form action="presensi-uas.php" method="post" name="id_daftarmk" id="id_daftarmk">
<input type="hidden" name="action" value="view">
<table class="tb_frame" width="80%" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td>
			 	Prodi :
                <select name="kdprodi">
    		   <option value=''>------</option>
			   <option value='all-{$FAK}' style="font-weight: bold; color: #fff; background-color: #233d0e;">LIHAT SEMUA</option>
	 		   {foreach item="list" from=$T_PRODI}
    		   {html_options values=$list.ID_PROGRAM_STUDI output=$list.PRODI}
	 		   {/foreach}
			   </select>
			   <input type="submit" name="View" value="View">
			   {if $FAK==11}
			   <br/>
			   <font color="#233d0e">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>*<a href="proses-cekal-psikologi.php">Pastikan Proses Cekal sudah dijalankan</a>.</b></font>
			   {else}
   			   <font color="#233d0e">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>*<a href="proses-cekal-uas.php">Pastikan Proses Cekal sudah dijalankan</a>.</b></font>
			   {/if}
		     </td>
           </tr>
</table>
</form>
{if {$list1.PRODI} == ''}
{else}
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
             <th width="5%">Kode</th>
             <th width="42%">Nama Mata Ajar</th>
             <th width="5%">SKS</th>
			 <th width="5%">KLS</th>
             <th width="5%">Hari</th>
			 <th width="12%">Jam</th>
			 <th width="12%">Ruangan</th>
			 <th width="5%">Peserta</th>
             <th class="noheader" width="8%">Aksi</th>
           </tr>
		</thead>
		<tbody>
           {foreach name=test item="list" from=$T_MK}
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td><center>{$list.KREDIT_SEMESTER}</center></td>
			 <td><center>{$list.NAMA_KELAS}</center></td>
             <td><center>{$list.NM_JADWAL_HARI}</center></td>
			 <td><center>{$list.JAM}</center></td>
			 <td><center>{$list.NM_RUANGAN}</center></td>
			 <td><center>{$list.PESERTA}</center></td>
			{if $list.PESERTA == 0}
			<td>&nbsp;</td>
			{else}
             <td>
			 <center>
			<input type="button" name="cetak5" value="Cetak" onclick="window.open('proses/presensi-uas-cetak.php?cetak={$list.ID_KELAS_MK}&prodi={$list.ID_PROGRAM_STUDI}&smt={$list.NM_SEMESTER}&thn={$list.TAHUN_AJARAN}','baru2');">
             </center>
			 </td>
			{/if}
           </tr>
		   {foreachelse}
        <tr><td colspan="9"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
   	 </table>
{/if}