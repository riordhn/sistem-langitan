<div class="center_title_bar">INPUT NO IJASAH</div>
<form id="form1" name="form1" method="post" action="wisuda-ijasah.php">
<table>
<tr>
	<td>NIM</td>
	<td> : </td>
	<td><input type="text" name="nim" /></td>
	<td><input type="submit" value="Tampil" /></td>
</tr>
</table>
</form>

<h2><font color="#FF0000">{$pesan}</font></h2>

{if isset($ijasah)}
<form action="wisuda-ijasah.php" method="post" id="f2">
<table width="600">
  <tr>
    <th colspan="2" style="text-align:center">Biodata</th>
  </tr>
  <tr>
    <td>NIM</td>
    <td>{$ijasah[0]['NIM_MHS']}</td>
  </tr>
  <tr>
    <td>Nama</td>
    <td>{$ijasah[0]['NM_PENGGUNA']}</td>
  </tr>
  <tr>
    <td>Program Studi</td>
    <td>{$ijasah[0]['NM_JENJANG']} - {$ijasah[0]['NM_PROGRAM_STUDI']}</td>
  </tr>
  <tr>
    <td>No Ijasah</td>
    <td>{$ijasah[0]['NO_IJASAH']}</td>
  </tr>
  <tr>
    <td>SK. Yudisium</td>
    <td>{$ijasah[0]['SK_YUDISIUM']}</td>
  </tr>
</table>

<table>
	<tr>
    <th colspan="2" style="text-align:center">No Ijasah</th>
  </tr>
	<tr>
    	<td>No. Urut Ijasah</td>
    	<td><input name="no_ijasah" type="text" id="no_ijasah"  class="required number" size="5" value="{$kode_ijasah}" /> 
    	*hanya no urut </td>
    </tr>
  <tr>
    	<td>Tgl Lulus</td>
    	<td><input type="text" name="tgl_lulus" id="tgl_lulus" class="datepicker" value="{$ijasah[0]['TGL_LULUS']}" /></td>
    </tr>
    <tr>
    	<td>SK Yudisium</td>
    	<td><input type="text" name="sk_yudisium" size="30" value="{$ijasah[0]['SK_YUDISIUM']}"/></td>
    </tr>
    <tr>
    	<td>Tgl SK Yudisium</td>
    	<td><input type="text" name="tgl_sk_yudisium" id="tgl_sk_yudisium" class="datepicker" value="{$ijasah[0]['TGL_SK_YUDISIUM']}" /></td>
    </tr>
    <tr>
      <td>IPK</td>
      <td><input type="text" name="ipk" size="4" value="{$ijasah[0]['IPK']}"/></td>
    </tr>
    <tr>
      <td>SKS Total</td>
      <td><input type="text" name="sks_total" size="4" value="{$ijasah[0]['SKS_TOTAL']}"/></td>
    </tr>
    <tr>
      <td>ELPT</td>
      <td><input type="text" name="elpt" size="4" value="{$ijasah[0]['ELPT']}"/></td>
    </tr>
    <tr>
    	<td colspan="2" style="text-align:center">
        	<input type="submit" value="Simpan" />
        	<input type="hidden" value="{$ijasah[0]['NIM_MHS']}" name="nim" />
            <input type="hidden" value="{$ijasah[0]['ID_PROGRAM_STUDI']}" name="id_prodi" />
            <input type="hidden" value="{$ijasah[0]['ID_JENJANG']}" name="id_jenjang" />
            <input type="hidden" value="{$ijasah[0]['ID_MHS']}" name="id_mhs" />
            <input type="hidden" value="{$ijasah[0]['NO_IJASAH']}" name="ijasah_admisi" />
        </td>
    </tr>
</table>
</form>
Contoh No Ijasah : 
<font color="#FF0000">16</font>/<font color="#0066FF">0113</font>/<font color="#00FF00">01</font>.<font color="#FF00FF">8</font>/<font color="#993300">Sp</font>/<font color="#330033">2012</font>
<br />
<font color="#FF0000">Merah </font> : No. urut Ijasah
<br />
<font color="#0066FF">Biru </font> : Kode Universitas
<br />
<font color="#00FF00">Hijau </font> : Kode Fakultas
<br />
<font color="#FF00FF">Pink </font> : Kode Program Studi Ijasah
<br />
<font color="#993300">Coklat </font> : Kode Jenjang Ijasah
<br />
<font color="#330033">Ungu </font> : Tahun Wisuda
{/if}

{literal}
<script language="javascript">
/* $('#f2').submit(function() {
                    if($('#tgl_lulus').val()=='' || $('#tgl_sk_yudisium').val()==''){
                            $('#alert').fadeIn();
                    return false;
                    }  
            });*/
            $('#no_ijasah').validate();
            $( ".datepicker" ).datepicker({dateFormat:'dd-mm-yy',changeMonth: true,
			changeYear: true});
</script>
{/literal}