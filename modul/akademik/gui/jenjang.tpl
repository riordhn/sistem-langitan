{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[0,0]]
		}
	); 
} 
);
</script>
{/literal}

<div class="center_title_bar">Master Jenjang</div>  
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
	  <th>Kode Jenjang</th>
	  <th>Nama Jenjang</th>
	  <th>Keterangan</th>
	</tr>
	</thead>
	<tbody>
	{foreach name=list item="list" from=$T_JENJANG}
	{if $smarty.foreach.list.iteration % 2 == 0}
	<tr bgcolor="lightgray">
	{else}
	<tr>
	{/if}
	  <td><center>{$list.ID_JENJANG}</center></td>
	  <td>{$list.NM_JENJANG}</td>
	  <td>{$list.NM_JENJANG_IJASAH}</td>
	</tr>
	{foreachelse}
	<tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
