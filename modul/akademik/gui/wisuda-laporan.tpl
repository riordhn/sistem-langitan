<div class="center_title_bar">LAPORAN MAHASISWA YANG AKAN IKUT WISUDA</div>
<form action="wisuda-laporan.php" method="post">
	<table width="700">
    	<tr>
            <td>Periode Wisuda</td>
            <td>
            	<select name="periode">
					<option value="">- PILIH PERIODE -</option>
                	{foreach $periode as $data}
                	<option value="{$data.ID_TARIF_WISUDA}" {if $smarty.post.periode==$data.ID_TARIF_WISUDA} selected="selected" {/if}>{$data.NM_TARIF_WISUDA}</option>
					{/foreach}
            	</select>
            </td>
        </tr>
        <tr>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>

{if isset($pengajuan_bayar)}
<table style="font-size:12px">
<tr>
	<th>NO</th>
	<th>NIM</th>
    <th>NAMA</th>
    <th>TTL</th>
	<th>KELAMIN</th>
    <th>FAKULTAS</th>
    <th>PROGRAM STUDI</th>
	<th>TGL LULUS</th>
	<th>NO IJASAH</th>
	<th>Tanggal Bayar</th>
	<th>Jumlah Bayar</th>
	<th>Jumlah Cicilan</th>
</tr>
	{$no = 1}
	{foreach $pengajuan_bayar as $data}
    	{if ($no % 2) == 0}
		 	{$warna = "#CCCCCC"}
		{else}
		 	{$warna = ""}
		{/if}
    	<tr bgcolor="{$warna}">
        	<td>{$no++}</td>
            <td>{$data.NIM_MHS}</td>
            <td>{$data.NM_PENGGUNA}</td>
            <td>{$data.LAHIR_IJAZAH}</td>
			<td>{$data.KELAMIN_PENGGUNA}</td>
            <td>{$data.NM_FAKULTAS}</td>
            <td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
			<td>{$data.TGL_LULUS_PENGAJUAN}</td>
			<td>{$data.NO_IJASAH}</td>
			<td>{$data.TGL_BAYAR}</td>
			<td style="text-align:right">{$data.BESAR_BIAYA|number_format:0:"":"."}</td>
			<td style="text-align:right">{$data.CICILAN|number_format:0:"":"."}</td>
        </tr>
    {/foreach}
</table>
{/if}