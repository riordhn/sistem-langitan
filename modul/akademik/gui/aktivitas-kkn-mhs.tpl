<div class="center_title_bar">Kuliah Kerja Nyata Mahasiswa</div>

{if isset($mhs)}
<input type="button" value="Cetak" onclick="window.open('proses/cetak-kkn-mhs.php?fak={$id_fakultas}');"/>
<table>
	<tr>
		<th>NO</th>
		<th>NIM</th>
		<th>NAMA</th>
		<th>PRODI</th>
        <th>IPK</th>
        <th>SKS</th>
        <th>SEMESTER</th>
	</tr>
	{$no = 1}
	{foreach $mhs as $data}
		<tr>
			<td>{$no++}</td>
			<td>{$data.NIM_MHS}</td>
			<td>{$data.NM_PENGGUNA}</td>
			<td>{$data.NM_PROGRAM_STUDI}</td>
            <td>{$data.IPK}</td>
            <td>{$data.SKS}</td>
            <td>{$data.TAHUN_AJARAN} {$data.NM_SEMESTER}</td>
		</tr>
	{/foreach}
</table>
{/if}