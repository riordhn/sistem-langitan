<div class="center_title_bar">Monitoring KRS</div> 
<p></p>

{if isset($smarty.request.nim)}
<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
			 <td width="5%" bgcolor="#FFCC33"><center>No</center></td>
             <td width="5%" bgcolor="#FFCC33"><center>NIM</center></td>
			 <td width="20%" bgcolor="#FFCC33"><center>Nama Mhs</center></td>
             <td width="10%" bgcolor="#FFCC33"><center>Prodi</center></td>
			 <td width="5%" bgcolor="#FFCC33"><center>STATUS</center></td>
			 <td width="5%" bgcolor="#FFCC33"><center>Kode MA</center></td>
             <td width="5%" bgcolor="#FFCC33"><center>Nama MA</center></td>
             <td width="5%" bgcolor="#FFCC33"><center>SKS</center></td>
			 <td width="5%" bgcolor="#FFCC33"><center>Kelas</center></td>
		  </tr>
          	{$total=0}
		  {foreach name=test item="list" from=$detail}

			<tr>
			 <td>{$smarty.foreach.test.iteration}</td>
             <td>{$list.NIM_MHS}</td>
			 <td>{$list.NM_PENGGUNA}</td>
             <td>{$list.PRODI}</td>
			 <td>{$list.STATUS}</td>
			 <td>{$list.KD_MATA_KULIAH}</td>
			 <td>{$list.NM_MATA_KULIAH}</td>
             <td>{$list.KREDIT_SEMESTER}</td>
			 <td>{$list.NAMA_KELAS}</td>		
			</tr>
				{$total = $list.KREDIT_SEMESTER + $total}
		  {/foreach}
          <tr>
			 <td colspan="7">Total</td>
             <td colspan="2">{$total}</td>		
			</tr>
</table>
{else if isset($smarty.request.prodi)}
<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
			 <td width="5%" bgcolor="#FFCC33"><center>No</center></td>
             <td width="5%" bgcolor="#FFCC33"><center>NIM</center></td>
			 <td width="20%" bgcolor="#FFCC33"><center>Nama Mhs</center></td>
             <td width="10%" bgcolor="#FFCC33"><center>Prodi</center></td>
			 <td width="5%" bgcolor="#FFCC33"><center>STATUS</center></td>
			 <td width="5%" bgcolor="#FFCC33"><center>SKS DITEMPUH</center></td>
             <td width="5%" bgcolor="#FFCC33"><center>SKS MAKSIMAL</center></td>
			 <td width="5%" bgcolor="#FFCC33"><center>SKS DIAMBIL</center></td>
		  </tr>
          	
		  {foreach name=test item="list" from=$report}

			<tr>
			 <td>{$smarty.foreach.test.iteration}</td>
             <td>{$list.NIM_MHS}</td>
			 <td>{$list.NM_PENGGUNA}</td>
             <td>{$list.PRODI}</td>
			 <td>{$list.STATUS}</td>
			 <td>{$list.SKS_DIPEROLEH}</td>
			 <td>{$list.SKS_MAKS}</td>
			 <td><a href="report-mon-krs.php?nim={$list.NIM_MHS}">{$list.SKS_SEM}</a></td>		
			</tr>
			
		  {/foreach}
</table>
{else}
<form action="report-mon-krs.php" method="post">
<select name="prodi" required="true">
		<option></option>
	{foreach $prodi as $data}
    	<option value="{$data.ID_PROGRAM_STUDI}">{$data.PRODI}</option>
    {/foreach}
</select>
<input type="submit" value="Tampil" />
</form>
{/if}

    