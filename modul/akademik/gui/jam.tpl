{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[1,0],[2,0]],
		headers: {
            3: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}

<div class="center_title_bar">Master Jam Kuliah (Fakultas)</div>
<div id="tabs">
	<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Input</div>
	{*<div id="tab3" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Update</div>*}
</div>

<div class="panel" id="panel1" style="display:{$disp1} ">
<p> </p>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
		<th>Nama Jam </th>
		<th>Jam Mulai </th>
		<th>Jam Selesai </th>
		<th class="noheader">Aksi</th>
	</tr>
	</thead>
	<tbody>
	{foreach name=list item="datajam" from=$T_JAM}
	{if $smarty.foreach.list.iteration % 2 == 0}
	<tr bgcolor="lightgray">
	{else}
	<tr>
	{/if}
		<td>{$datajam.NM_JADWAL_JAM}</td>
		<td><center>{$datajam.JAM_MULAI}</center></td>
		<td><center>{$datajam.JAM_SELESAI}</center></td>
		<td>
			<center>
			<a href="jam.php?action=tampil&id_jam={$datajam.ID_JADWAL_JAM}">Update</a> 
			{*| <a href="jam.php?action=del&id_jam={$datajam.ID_JADWAL_JAM}">Delete</a>*}
			</center>
		</td>
	</tr>
	{foreachelse}
	<tr><td colspan="5"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>

<div class="panel" id="panel2" style="display:{$disp2} ">
<p><br><b>Input master jam kuliah</b></p>
<form action="jam.php" method="post" >
<input type="hidden" name="action" value="add" >
<table class="tb_frame" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>Nama Jam</td>
		<td><center>:</center></td>
		<td><input type="text" name="nama_jam" class="required"  /></td>
	</tr>
	<tr>
		<td>Jam Mulai</td>
		<td><center>:</center></td>
		<td><input type="text" name="jam_mulai" id="jam_mulai" class="required"  /> Contoh: 07:00</td>
	</tr>
	<tr>
		<td>Jam Selesai</td>
		<td><center>:</center></td>
		<td><input type="text" name="jam_selesai" id="jam_selesai" class="required"  /> Contoh: 10:00</td>
	</tr>
</table>
<p><input type="button" name="kembali" value="Kembali" onClick="javascript: displayPanel('1');">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="tambah" value="Tambah"></p>
</form>
</div>

<div class="panel" id="panel3" style="display: {$disp3} ">
<p><br><b>Update master jam kuliah</b></p>
<form action="jam.php" method="post" >
<input type="hidden" name="action" value="update" >
<table class="tb_frame" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>Fakultas</td>
		<td><center>:</center></td>
		<td>{$datajam.NM_FAKULTAS}</td>
	</tr>
	{foreach item="datajam" from=$T_JAM}
	{if $datajam.ID_JADWAL_JAM==$idjam}
	<input type="hidden" name="id_jam" value="{$datajam.ID_JADWAL_JAM}" >
	<tr>
		<td>ID Jam</td>
		<td><center>:</center></td>
		<td>{$datajam.ID_JADWAL_JAM}</td>
	</tr>
	<tr>
		<td>Nama Jam</td>
		<td><center>:</center></td>
		<td><input type="text" name="nama_jam" class="required" value="{$datajam.NM_JADWAL_JAM}"></td>
	</tr>
	<tr>
		<td>Jam Mulai</td>
		<td><center>:</center></td>
		<td><input type="text" name="jam_mulai" class="required" id="jam_mulai1" value="{$datajam.JAM_MULAI}"></td>
	</tr>
	<tr>
		<td>Jam Selesai</td>
		<td><center>:</center></td>
		<td><input type="text" name="jam_selesai" class="required" id="jam_selesai1" value="{$datajam.JAM_SELESAI}"></td>
	</tr>
	{/if}
	{/foreach}
</table>
<p><input type="button" name="kembali" value="Kembali" onClick="window.location.href='#perkuliahan-jam!jam.php'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="tambah" value="Update"></p>
</form>
</div>

{literal}
    <script>
	    $("#jam_mulai").timepicker({timeFormat: 'hh:mm'});
	    $("#jam_selesai").timepicker({timeFormat: 'hh:mm'});
            $('form').validate();
		$("#jam_mulai1").timepicker({timeFormat: 'hh:mm'});
	    $("#jam_selesai1").timepicker({timeFormat: 'hh:mm'});
    </script>
{/literal}