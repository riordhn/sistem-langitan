<div class="center_title_bar">MOnitoring Sebaran Dosen Wali </div>
<form action="dosen-wali-fakul.php" method="post">
<input type="hidden" name="action" value="tampil" >
<table class="tb_frame" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
		Tahun Ajaran : 
			<select name="smt" id="smt">
				<option value="">-- Tahun Ajaran --</option>
				{foreach item="smt" from=$T_ST}
					{html_options  values=$smt.ID_SEMESTER output=$smt.SMT selected=$smtpil}
				{/foreach}
			</select>
			<input type="submit" name="View" value="View">
		</td>
	</tr>
</table>
</form>

<div id="tabs">
	<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Dist. Perwalian</div>
	<div id="tab2" class="tab" align="center" onclick="javascript: displayPanel('2');">Detil Perwalian</div>
</div>


{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable2").tablesorter(
		{
		sortList: [[2,1]],
		}
	);
}
);
</script>
{/literal}
<div class="panel" id="panel1" style="display: {$disp1}">
<p> </p>
<table id="myTable2" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>NIP/NIK</th>
			<th>Nama Dosen</th>
			<th>D3</th>
			<th>S1</th>
			<th>S2</th>
			<th>S3</th>
			<th>LAIN</th>
			<th>Total Mahasiswa</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
	{foreach name=test item="seb" from=$SEBDOLI}
		<tr>
			<td>{$seb.NIP_DOSEN}</td>
			<td>{$seb.NM_PENGGUNA}</td>
			<td>{$seb.D3}</td>
			<td>{$seb.S1}</td>
			<td>{$seb.S2}</td>
			<td>{$seb.S3}</td>
			<td>{$seb.LAIN}</td>
			<td><center><a href="dosen-wali-fakul.php?action=detailwali&dosen={$seb.ID_DOSEN}&smt={$smtpil}">{$seb.TTL}</a></center></td>
			{if {$kdfak == 7} }
				<td><center><input type="button" name="cetak" value="Cetak" onclick="window.open('proses/cetak-doli-fisip.php?cetak={$seb.ID_DOSEN}&smt={$smtpil}&fak={$kdfak}&last_smt={$last_smt}&smtpiltahun={$smtpiltahun}','baru');"></center></td>
			{else}
				<td><center><input type="button" name="cetak" value="Cetak" onclick="window.open('proses/cetak-doli.php?cetak={$seb.ID_DOSEN}&smt={$smtpil}&fak={$kdfak}','baru');"></center></td>
			{/if}
		</tr>
	{foreachelse}
		<tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>

<div class="panel" id="panel2" style="display: {$disp2}">
<p> </p>
<p><strong>Daftar Anak Wali Dosen {$NMDSN} ({$NIPDSN})</strong></p>
<table class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>NO</th>
			<th>NIM</th>
			<th>Nama Mahasiswa</th>
			<th>Program Studi</th>
			<th>SKS Ambil</th>
			<th>IPS</th>
			<th>SKS TTL</th>
			<th>IPK</th>
		</tr>
	</thead>
	<tbody>

	{foreach name=test item="det" from=$DET}
		<tr>
			<td>{$smarty.foreach.test.iteration}</td>
			<td>{$det.NIM_MHS}</td>
			<td>{$det.MHS}</td>
			<td>{$det.PRODI}</td>
			<td>{$det.SKS_SKRG}</td>
			<td>{$det.IPS}</td>
			<td>{$det.SKSTOTAL}</td>
			<td>{$det.IPK}</td>
		</tr>
	{foreachelse}
		<tr><td colspan="2"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>

{literal}
 <script>$('form').validate();</script>
{/literal}

