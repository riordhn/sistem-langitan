{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
	$("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
			10: { sorter: false }
		}
		}
	); 
} 
);
</script>
{/literal}
<div class="center_title_bar">Data KRS Mahasiswa {foreach item="list1" from=$NM_PRODI}{$list1.PRODI}{/foreach}, Tahun Akademik {foreach item="list2" from=$THN_AKAD}{$list2.THN_SMT}{/foreach}</div>  
<form action="rekap-krs.php" method="post" name="id_daftarmk" id="id_daftarmk">
	<input type="hidden" name="action" value="view" >
	<p>
		Prodi : 
		<select name="kdprodi" required="true">
			<option value=''>-- PRODI --</option>
			{foreach item="list" from=$T_PRODI}
				{html_options  values=$list.ID_PROGRAM_STUDI output=$list.PRODI selected=$smarty.post.kdprodi}
			{/foreach}
		</select>
		Tahun/Semester : 
		<select name="kdthnsmt" required="true">
			<option value=''>------</option>
			{foreach item="list_smt" from=$T_THNSMT}
				{html_options values=$list_smt.ID_SEMESTER output=$list_smt.THN_SMT selected=$smarty.post.kdthnsmt}
			{/foreach}
		</select>
		<input type="submit" name="View" value="View">
	</p>
</form>	

{* khusus fakultas psikologi *}
{if $FAK == '11'}
{if {$list1.PRODI} == ''}
{else}
<p>
<input type=button name="cetak" value="Ekspor Data ke MS. Excel" onclick="window.open('proses/rekap-krs-xls.php?prodi={$PRODI}&smt={$SMT_AKAD}&nm_prodi={$list1.PRODI}','baru2');">&nbsp;
<input type=button name="cetak" value="Ekspor Data ke MS. Word" onclick="window.open('proses/rekap-krs-doc.php?prodi={$PRODI}&smt={$SMT_AKAD}&nm_prodi={$list1.PRODI}','baru2');">&nbsp;
<input type=button name="cetak" value="Ekspor Semua ke MS. Excel" onclick="window.open('proses/rekap-krs-psikologi.php?prodi={$PRODI}&smt={$SMT_AKAD}&nm_prodi={$list1.PRODI}','baru2');">&nbsp;
Jumlah Mahasiswa yang melakukan KRS: {$JML_MHS} mahasiswa
</p>
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
		   <tr>
			 <th>No</th>
			 <th>Nim</th>
			 <th>Nama Mahasiswa</th>
			 <th>Kode MA</td>
			 <th>Nama MA</td>
			 <th>SKS</th>
			 <th>Kls</th>
			 <th>Status</th>  
			 <th>Keterangan</th> 
		   </tr>
		</thead>
		<tbody>
		   {foreach name=test item="list" from=$T_MK}
		   <tr>
			 <td><center>{$smarty.foreach.test.iteration}</center></td>
			 <td>{$list.NIM_MHS}</td>
			 <td>{$list.NM_PENGGUNA}</td>
			 <td>{$list.KD_MATA_KULIAH}</td>
			 <td>{$list.NM_MATA_KULIAH}</td>
			 <td><center>{$list.KREDIT_SEMESTER}</center></td>
			 <td><center>{$list.NAMA_KELAS}</center></td> 
			 <td>{$list.STATUS_KRS}</td>
			 <td>{$list.KETERANGAN}</td> 
		   </tr>
			{foreachelse}
		<tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
		</tbody>
	</table>
{/if}

{* fakultas selain psikologi *}
{else}
<p>
<input type=button name="cetak" value="Ekspor ke Excel" onclick="window.open('proses/rekap-krs-xls.php?prodi={$PRODI}&smt={$SMT_AKAD}&nm_prodi={$list1.PRODI}','baru2');">&nbsp;
</p>

	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0" width="100%">
		<thead>
		   <tr>
			 <th width="3%">No</th>
			 <th width="12%">Nim</th>
			 <th width="25%">Nama Mhs </th>
			 <th width="8%">Kode MA</font> </td>
		<th width="32%">Nama MA</font> </td>
			 <th width="4%">SKS</th>
			 <th width="4%">Kls </th>
		<th width="12%">Smt</th>
		<th>Status</th>
		   </tr>
		</thead>
		<tbody>
		   {foreach name=test item="list" from=$T_MK}
		   <tr>
			 <td><center>{$smarty.foreach.test.iteration}</center></td>
			 <td>{$list.NIM_MHS}</td>
			 <td>{$list.NM_PENGGUNA}</td>
			 <td>{$list.KD_MATA_KULIAH}</td>
			 <td>{$list.NM_MATA_KULIAH}</td>
			 <td><center>{$list.KREDIT_SEMESTER}</center></td>
			 <td><center>{$list.NAMA_KELAS}</center></td>
			 <td>{$list.THN_AKADEMIK_SEMESTER} - {$list.NM_SEMESTER}</td>
			 <td>{$list.STATUS_KRS}</td>
		   </tr>
			{foreachelse}
		<tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
		</tbody>
	</table>
{/if}
