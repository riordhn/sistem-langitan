{literal}
<script type="text/javascript">
function validate(){
if (document.forms.khs.smt.selectedIndex == "" || document.forms.khs.smt.selectedIndex == null){
	alert ("Pilih Tahun Semester.");
	return false;
}
return true;
}
</script>
{/literal}
<div class="center_title_bar">KARTU HASIL STUDI {foreach item="list1" from=$MHS}{$list1.NM_MHS} {/foreach} </div>  

<form action="proses/cetakkhs.php" method="post" name="ctkhs" id="khs" target="_new">
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		  <td colspan="9" class="center"><strong>Cetak Kolektif</strong></td>
		</tr> 
		<tr>
		  <td>Program Studi </td>
		  <td>: </td>
		  <td colspan="2">
		  	<select name="prodi">
			 {foreach item='lprodi' from=$prodinya}
			 {html_options values=$lprodi.ID_PROGRAM_STUDI output=$lprodi.NM_PROGRAM_STUDI selected=$smarty.post.prodi}
			 {/foreach}
			</select>
		   </td>
		</tr> 
		<tr>
		  <td>Tahun/Semester </td>
		  <td>: </td>
		  <td>
		  	<select name="id_smt">
			<option value=''>------</option>
			{foreach item="list_smt" from=$T_THNSMT}
			{html_options values=$list_smt.ID_SEMESTER output=$list_smt.SEMESTER selected=$smarty.post.id_smt}
			{/foreach}
			</select>
		  </td>
		  <td><input type="submit" value="CETAK KHS"></td>
		</tr>
	</table>
</form>
			   
<hr />

<form action="khs.php" method="post" name="khs" id="khs" onSubmit="return validate();">
	<input type="hidden" name="action" value="view" >
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		  <td colspan="9" class="center"><strong>Cetak Per Mahasiswa</strong></td>
		</tr> 
		<tr>
			<td>NIM </td>
			<td>: </td>
			<td><input type="text" name="nim" value="{if !empty($smarty.post.nim)}{$smarty.post.nim}{/if}"></td>
			<td>Tahun Ajaran / Semester </td>
			<td>: </td>
			<td>
				<select name="smt">
				   <option value=''>------</option>
				   {foreach item="list_smt" from=$T_THNSMT}
					   <option value="{$list_smt.ID_SEMESTER}" {if !empty($smarty.post.smt)}{if $smarty.post.smt == $list_smt.ID_SEMESTER}selected{/if}{/if}>{$list_smt.TAHUN_AJARAN} {$list_smt.NM_SEMESTER}</option>
				   {/foreach}
			   </select>
			</td>
		</tr>
		<tr>
			<td>Tanggal Cetak KRS </td>
			<td>: </td>
			<td><input type="text" name="tgl_cetak_krs" value="{if !empty($smarty.post.tgl_cetak_krs)}{$smarty.post.tgl_cetak_krs}{/if}"></td>
			<td>Tanggal Cetak KHS </td>
			<td>: </td>
			<td><input type="text" name="tgl_cetak_khs" value="{if !empty($smarty.post.tgl_cetak_khs)}{$smarty.post.tgl_cetak_khs}{/if}"></td>
		</tr>
		<tr>
			<td colspan="9" class="center"><input type="submit" name="View" value="View"></td>
		</tr>
	</table>
</form>	

	{foreach item="cek" from=$T_MHS}
		{if {$cek.STATUS_AKADEMIK_MHS} > 4 && {$cek.STATUS_AKADEMIK_MHS} < 16}
			<p><b>Maaf, {$cek.NM_PENGGUNA} dengan NIM:{$cek.NIM_MHS} sedang dalam status tidak aktif.</b></p>
		{else}
			
		{foreach item="list0" from=$T_MHS}
			<div class="panel" id="panel1" style="display: block">
			<p></p>

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<th colspan="3">{$NM_SMT}</th>
						</tr>
					   <tr>
						 <td>NIM</td>
						 <td><center>:</center></td>
						 <td>{$list0.NIM_MHS}&nbsp;&nbsp;&nbsp;<input type=button name="cetak" value="Cetak KHS" onclick="window.open('{$LINK}','baru2');">&nbsp;<input type=button name="cetak" value="Cetak KRS" onclick="window.open('{$LINK_KRS}','baru');"></td>
					   <tr>			 
						 <td>Nama Mahasiswa</td>
						 <td><center>:</center></td>
						 <td>{$list0.NM_PENGGUNA}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					   </tr>
					   <tr>
						 <td>Program Studi</td>
						 <td><center>:</center></td>
						 <td>{$list0.NM_JENJANG} {$list0.NM_PROGRAM_STUDI}</td>
					   </tr>
			</table>
		{/foreach}
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr class="left_menu">
			  <td width="5%" bgcolor="#333333"><font color="#FFFFFF">No</font></td>
			  <td bgcolor="#333333"><font color="#FFFFFF">Kode MA</font></td>
			  <td bgcolor="#333333"><font color="#FFFFFF">Nama Mata Ajar</font></td>
			  <td width="5%" bgcolor="#333333"><font color="#FFFFFF">SKS</font></td>
			  <td width="5%" bgcolor="#333333"><font color="#FFFFFF">UTS</font></td>
			  <td width="5%" bgcolor="#333333"><font color="#FFFFFF">UAS</font></td>
			  <td width="5%" bgcolor="#333333"><font color="#FFFFFF">Akhir</font></td>
			  <td width="5%" bgcolor="#333333"><font color="#FFFFFF">Nilai</font></td>
			  <td width="5%" bgcolor="#333333"><font color="#FFFFFF">Bobot</font></td>
			  <td></td>
			  <td bgcolor="#333333"><font color="#FFFFFF">Kurikulum</font></td>
			</tr>
			{foreach name=test item="list" from=$T_MK}
			<tr>
			  <td ><center>{$smarty.foreach.test.iteration}</center></td>
			  <td >{$list.KODE}</td>
			  <td >{$list.NAMA}</td>
			  <td ><center>{$list.SKS}</center></td>
			  <td ><center><!-- {$list.UTS} --></center></td>
			  <td ><center><!-- {$list.UAS} --></center></td>
			  <td ><center>{$list.AKHIR}</center></td>
			  <td ><center>{$list.NILAI}</center></td>
			  <td ><center>{$list.BOBOT_TOTAL}</center></td>
				<td></td>
			  <td><!-- {$list.KURIKULUM} --></td>
			</tr>
			 {foreachelse}
			<tr><td colspan="11"><em>Data tidak ditemukan</em></td></tr>
			{/foreach}
				{foreach item="footer" from=$T_IPS}
				   <tr>
					 <td colspan="2"></td>
					 <td><b>Total SKS dan Bobot</b></td>
					 <td><center><b>{$footer.SKS_TOTAL}</b></center></td>
					 <td></td>
					 <td></td>
					 <td></td>
					 <td></td>
					 <td><center><b>{$footer.BOBOT_TOTAL}</b></center></td>
				   </tr>
				   <tr>
					 <td colspan="2"></td>
					 <td><b>Indeks Prestasi Semester</b></td>
					 <td colspan="6"><b><center>{$footer.IPS}</b></center></td>
				   </tr>
				{/foreach}	
				{foreach item="footer1" from=$T_IPK}
				   <tr>
					 <td colspan="2"></td>
					 <td><b>Total SKS Sampai Semester Terpilih</b></td>
					 <td colspan="6"><b><center>{$footer1.TOTAL_SKS}</b></center></td>
				   </tr>
				   <tr>
					 <td colspan="2"></td>
					 <td><b>Indeks Prestasi Kumulatif</b></td>
					 <td colspan="6"><b><center>{$footer1.IPK}</b></center></td>
				   </tr>
				{/foreach}   
		</table>
		</div>
		{/if}
	{/foreach}
