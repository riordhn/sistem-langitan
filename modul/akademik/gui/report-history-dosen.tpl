<div class="center_title_bar">Historis Dosen {foreach item="list1" from=$DSN}{$list1.NM_DOSEN} {/foreach} </div>  
<form action="report-history-dosen.php" method="post" name="id_historis-mhs" id="id_historis-mhs">
<input type="hidden" name="action" value="view" >
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td>
			 	Prodi : 
               <select name="kdprodi">
    		   <option value=''>------</option>
			   <option value='all-{$FAK}' style="font-weight: bold; color: #fff; background-color: #233d0e;">LIHAT SEMUA</option>
	 		   {foreach item="list" from=$T_PRODI}
    		   {html_options values=$list.ID_PROGRAM_STUDI output=$list.PRODI selected=$smarty.post.kdprodi}
	 		   {/foreach}
			   </select>
		     </td>
			 <td>
			 	Tahun/Semester : 
               <select name="kdthnsmt">
    		   <option value=''>------</option>
	 		   {foreach item="list_smt" from=$T_THNSMT}
    		   {html_options values=$list_smt.ID_SEMESTER output=$list_smt.THN_SMT selected=$smarty.post.kdthnsmt}
	 		   {/foreach}
			   </select>
		     </td>
           </tr>
           <tr>		   
			 <td>Dosen : 
			   <select name="id_dosen">
    		   <option value=''>------</option>
	 		   {foreach item="list_dsn" from=$T_DSN}
    		   {html_options values=$list_dsn.ID_DOSEN output=$list_dsn.NM_PENGGUNA selected=$smarty.post.id_dosen}
	 		   {/foreach}
			   </select>
			 </td>
			 <td><input type="submit" name="View" value="View"></td>
           </tr>
</table>
</form>	
{if {$list1.NM_DOSEN} == ''}
{else}
<div class="panel" id="panel1" style="display: block">
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr class="left_menu">
             <td width="5%" bgcolor="#333333"><font color="#FFFFFF">No</font></td>
             <td width="10%" bgcolor="#333333"><font color="#FFFFFF">Kode MA</font> </td>
			 <td width="30%" bgcolor="#333333"><font color="#FFFFFF">Nama MA</font> </td>
             <td width="5%" bgcolor="#333333"><font color="#FFFFFF">SKS</font></td>
             <td width="30%" bgcolor="#333333"><font color="#FFFFFF">Program Studi</font></td>
			 <td width="10%" bgcolor="#333333"><font color="#FFFFFF">Tahun</font></td>
			 <td width="10%" bgcolor="#333333"><font color="#FFFFFF">Semester</font></td>
           </tr>
		   {foreach name=test item="list" from=$T_MK}
           <tr>
             <td>{$smarty.foreach.test.iteration}</td>
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
			 <td>{$list.KREDIT_SEMESTER}</td>
             <td>{$list.NM_JENJANG} {$list.NM_PROGRAM_STUDI}</td>
             <td>{$list.TAHUN_AJARAN}</td>
			 <td>{$list.NM_SEMESTER}</td>
           </tr>
            {foreachelse}
        <tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
</table>
</div>
{/if}
   