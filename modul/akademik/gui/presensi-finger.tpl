{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
            8: { sorter: false }
		}
		}
	); 
} 
);

			$("#cek_all").click(function()				
			{
				var checked_status = this.checked;
				$(".cek").each(function()
				{
					this.checked = checked_status;
				});
			});
</script>
{/literal}

<div class="center_title_bar">Presensi Fingerprint</div>

<div id="tabs">
        <div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Setting Presensi</div>
</div>


<div class="panel" id="panel1" style="display: {$disp1} ">
	<form method="post" action="presensi-finger.php">
    <table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
			<tr>
             <th>Kode</th>
             <th>Nama Mata Ajar</th>
			 <th>KLS</th>
             <th>SMT</th>
             <th>Hari</th>
			 <th>Jam</th>
			 <th>Peserta</th>
			 <th>Prodi MA</th>
             <th><input type="checkbox" id="cek_all" name="cek_all"/></th>
           </tr>
		</thead>
		<tbody>
        	{$no=1}
           {foreach name=test item="list" from=$T_MK}
		   <tr>
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
			 <td><center>{$list.NAMA_KELAS}</center></td>
             <td><center>{$list.TINGKAT_SEMESTER}</center></td>
             <td><center>{$list.NM_JADWAL_HARI}</center></td>
			 <td><center>{$list.JAM}</center></td>
			 <td><center>{$list.KLS_TERISI}</center></td>
			 <td>{$list.PRODIASAL}</td>
             <td style="text-align:center">
				<input type="checkbox" class="cek" name="cek[{$no++}]" value="{$list.ID_KELAS_MK}" />
			</td>
           </tr>
		   {foreachelse}
			<tr><td colspan="10"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}        
		</tbody>
        <tr>
            <td colspan="16" style="text-align:center">
                <input type="hidden" value="{$no}" name="no" />
                <input type="submit" value="Simpan" />
                <input type="submit" value="Masukkan Presensi" />
            </td>
        </tr>
   	 </table>
     </form>
</div>
