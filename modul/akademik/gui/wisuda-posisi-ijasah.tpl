<div class="center_title_bar">POSISI IJASAH</div>  
<form action="wisuda-posisi-ijasah.php" method="post">
	<table width="700">
    	<tr>
            <td>Periode Wisuda</td>
            <td>
            	<select name="periode">
                	{foreach $periode as $data}
                	<option value="{$data.ID_TARIF_WISUDA}" {if $id_periode==$data.ID_TARIF_WISUDA} selected="selected" {/if}>{$data.NM_TARIF_WISUDA}</option>
					{/foreach}
            	</select>
            </td>
        </tr>
        <tr>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>
{if isset($pengajuan)}
<form name="f3" action="wisuda-posisi-ijasah.php" method="post">
<table style="font-size:12px; width:1100px">
<tr>
	<th rowspan="2" style="text-align:center">NO</th>
	<th rowspan="2" style="text-align:center">NIM</th>
    <th rowspan="2" style="text-align:center">NAMA</th>
    <th rowspan="2" style="text-align:center">JENJANG</th>
    <th rowspan="2" style="text-align:center">PROGRAM STUDI</th>
    <th rowspan="2" style="text-align:center">TEMPAT DAN TANGGAL LAHIR</th>
	<th rowspan="2" style="text-align:center">TGL LULUS</th>
    <th rowspan="2" style="text-align:center">NO IJASAH</th>
    <th rowspan="2" style="text-align:center">NO SERI</th>
    <th colspan="7" style="text-align:center">POSISI IJASAH</th>
	<th colspan="7" style="text-align:center">TANGGAL POSISI IJASAH</th>
</tr>
	{$no = 1}
        <tr>
		  <th>Dirpen Cetak</th>
    	  <th>Dirpen ke Fakultas (Ttd Dekan)</th>
    	  <th>Fakultas ke Dirpen (Ttd Rektor) <input type="checkbox" id="cek_all3" name="cek_all3" /></th>
		  <th>Dirpen ke Fakultas</th>
    	  <th>Fakultas ke Mhs <input type="checkbox" id="cek_all5" name="cek_all5" /></th>
          <th>Fakultas ke Dirpen (> 3 Bulan) <input type="checkbox" id="cek_all6" name="cek_all6" /></th>
		  <th>Dirpen ke Mhs</th>
		  
		  <th>Dirpen Cetak</th>
    	  <th>Dirpen ke Fakultas (Ttd Dekan)</th>
    	  <th>Fakultas ke Dirpen (Ttd Rektor)</th>
		  <th>Dirpen ke Fakultas</th>
    	  <th>Fakultas ke Mhs</th>
          <th>Fakultas ke Dirpen (> 3 Bulan)</th>
		  <th>Dirpen ke Mhs</th>
      	</tr>
	{foreach $pengajuan as $data}
		{if ($no % 2) == 0}
		 	{$warna = "#CCCCCC"}
		{else}
		 	{$warna = ""}
		{/if}
    	<tr bgcolor="{$warna}">
        	<td>{$no++}</td>
            <td>{$data.NIM_MHS}</td>
            <td>{$data.NM_PENGGUNA}</td>
            <td>{$data.NM_JENJANG}</td>
            <td>{$data.NM_PROGRAM_STUDI}</td>
            <td>{$data.LAHIR_IJAZAH}</td>
            <td>{$data.TGL_LULUS_PENGAJUAN}</td>
            <td>{$data.NO_IJASAH}</td>
            <td>{$data.NO_SERI_KERTAS}</td>
			<td style="text-align:center">{if $data.POSISI_IJASAH == 1}Ya{else}Tidak{/if}</td>
            <td style="text-align:center">
					{if $data.POSISI_IJASAH == 2}Ya{else}Tidak{/if}
			</td>
			<td style="text-align:center">
				<input type="radio" class="cek3" value="3" name="cek{$no}" {if $data.POSISI_IJASAH == 3}checked="checked"{/if}/>
			</td>
			<td style="text-align:center">{if $data.POSISI_IJASAH == 4}Ya{else}Tidak{/if}</td>
            <td style="text-align:center">
					<input type="radio" class="cek5" value="5" name="cek{$no}" {if $data.POSISI_IJASAH == 5}checked="checked"{/if}/>
			</td>
			<td style="text-align:center">
					<input type="radio" class="cek6" value="6" name="cek{$no}" {if $data.POSISI_IJASAH == 6}checked="checked"{/if}/>
			</td>
			<td style="text-align:center">{if $data.POSISI_IJASAH == 7}Ya{else}Tidak{/if}</td>
			
			<td style="text-align:center">{$data.TGL_POSISI_IJASAH}</td>
			<td style="text-align:center">{$data.TGL_POSISI_IJASAH2}</td>
			<td style="text-align:center">{$data.TGL_POSISI_IJASAH3}</td>
			<td style="text-align:center">{$data.TGL_POSISI_IJASAH4}</td>
            <td style="text-align:center">{$data.TGL_POSISI_IJASAH5}</td>
			<td style="text-align:center">{$data.TGL_POSISI_IJASAH6}</td>
			<td style="text-align:center">{$data.TGL_POSISI_IJASAH7}</td>
        </tr>
        <input type="hidden" name="id_mhs{$no}" value="{$data.ID_MHS}" />
    {/foreach}
    <tr>
    	<td colspan="20" style="text-align:center">
			<input type="submit" value="Simpan" name="simpan" />
			<input type="button" value="Cetak" />
        	<input type="hidden" name="no" value="{$no}" />
            <input type="hidden" name="periode" value="{$id_periode}" />
            <input type="hidden" name="fakultas" value="{$id_fakultas}" />
        </td>
    </tr>
</table>
</form>
{/if}

{literal}
    <script type="text/javascript">
			
		$("#cek_all1").click(function(){
				var checked_status = this.checked;
				$(".cek1").each(function()
				{
					this.checked = checked_status;
				});
		});
		
		$("#cek_all2").click(function(){
				var checked_status = this.checked;
				$(".cek2").each(function()
				{
					this.checked = checked_status;
				});
		});
		
		$("#cek_all3").click(function(){
				var checked_status = this.checked;
				$(".cek3").each(function()
				{
					this.checked = checked_status;
				});
		});
		
		$("#cek_all4").click(function(){
				var checked_status = this.checked;
				$(".cek4").each(function()
				{
					this.checked = checked_status;
				});
		});
		
		$("#cek_all5").click(function(){
				var checked_status = this.checked;
				$(".cek5").each(function()
				{
					this.checked = checked_status;
				});
		});
		
		$("#cek_all6").click(function(){
				var checked_status = this.checked;
				$(".cek6").each(function()
				{
					this.checked = checked_status;
				});
		});
		
    </script>
{/literal}