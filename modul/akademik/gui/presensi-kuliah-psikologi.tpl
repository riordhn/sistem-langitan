{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
            8: { sorter: false }
		}
		}
	); 
} 
);
</script>
{/literal}

<div class="center_title_bar">Cetak Presensi Perkuliahan {foreach item="list1" from=$NM_PRODI}{$list1.PRODI} {/foreach}</div>
<form action="presensi-kuliah.php" method="post" name="id_daftarmk" id="id_daftarmk">
<input type="hidden" name="action" value="view">
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td>
			 	Prodi : 
                <select name="kdprodi">
    		   <option value=''>------</option>
			   <option value='all-{$FAK}' style="font-weight: bold; color: #fff; background-color: #233d0e;">LIHAT SEMUA</option>
	 		   {foreach item="list" from=$T_PRODI}
    		   {html_options values=$list.ID_PROGRAM_STUDI output=$list.PRODI}
	 		   {/foreach}
			   </select>
			   <input type="submit" name="View" value="View">
		     </td> 
           </tr>
</table>
</form>
{if {$list1.PRODI} == ''}
{else}
		<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
             <th width="5%">Kode</th>
             <th width="42%">Nama Mata Ajar</th>
             <th width="5%">SKS</th>
			 <th width="5%">KLS</th>
             <th width="5%">Hari</th>
			 <th width="12%">Jam</th>
			 <th width="12%">Ruangan</th>
			 <th width="5%">Peserta</th>
             <th class="noheader" width="8%">Aksi</th>
           </tr>
		</thead>
		<tbody>
           {foreach name=test item="list" from=$T_MK}
		   <tr>
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td><center>{$list.KREDIT_SEMESTER}</center></td>
			 <td><center>{$list.NAMA_KELAS}</center></td>
             <td><center>{$list.NM_JADWAL_HARI}</center></td>
			 <td><center>{$list.JAM}</center></td>
			 <td><center>{$list.NM_RUANGAN}</center></td>
			 <td><center>{$list.PESERTA}</center></td>
		{if $list.ID_JENJANG == 1}
			{if $list.PESERTA == 0}
			<td>&nbsp;</td>
			{else}
            <td>
			<input type=button name="cetak1" value="MHS" onclick="window.open('proses/presensi-kuliah-cetak.php?cetak={$list.ID_KELAS_MK}&hari={$list.ID_JADWAL_HARI}&prodi={$list.ID_PROGRAM_STUDI}&smt={$list.NM_SEMESTER}&thn={$list.TAHUN_AJARAN}','baru2');"><br>
			<input type=button name="cetak2" value="DSN" onclick="window.open('proses/presensi-kuliah-dosen-s1-psikologi-cetak.php?cetak={$list.ID_KELAS_MK}&hari={$list.ID_JADWAL_HARI}&prodi={$list.ID_PROGRAM_STUDI}&smt={$list.NM_SEMESTER}&thn={$list.TAHUN_AJARAN}','baru2');"><br>
			<input type=button name="cetak3" value="Excel" onclick="window.open('proses/presensi-kuliah-xls.php?xls={$list.ID_KELAS_MK}&hari={$list.ID_JADWAL_HARI}&prodi={$list.ID_PROGRAM_STUDI}&smt={$list.NM_SEMESTER}&thn={$list.TAHUN_AJARAN}','baru2');">
            </td>
			{/if}
		{else}
			{if $list.PESERTA == 0}
			<td>&nbsp;</td>
			{else}
            <td>
			<input type=button name="cetak1" value="MHS" onclick="window.open('proses/presensi-kuliah-cetak.php?cetak={$list.ID_KELAS_MK}&hari={$list.ID_JADWAL_HARI}&prodi={$list.ID_PROGRAM_STUDI}&smt={$list.NM_SEMESTER}&thn={$list.TAHUN_AJARAN}','baru2');"><br>
			<input type=button name="cetak2" value="DSN" onclick="window.open('proses/presensi-kuliah-dosen-s2-s3-psikologi-cetak.php?cetak={$list.ID_KELAS_MK}&hari={$list.ID_JADWAL_HARI}&prodi={$list.ID_PROGRAM_STUDI}&smt={$list.NM_SEMESTER}&thn={$list.TAHUN_AJARAN}','baru2');"><br>
			<input type=button name="cetak3" value="Excel" onclick="window.open('proses/presensi-kuliah-xls.php?xls={$list.ID_KELAS_MK}&hari={$list.ID_JADWAL_HARI}&prodi={$list.ID_PROGRAM_STUDI}&smt={$list.NM_SEMESTER}&thn={$list.TAHUN_AJARAN}','baru2');">
            </td>
			{/if}
		{/if}
           </tr>
		   {foreachelse}
        <tr><td colspan="9"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
   	 </table>
{/if}