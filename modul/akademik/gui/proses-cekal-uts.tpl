<div class="center_title_bar">PROSES ALL CEKAL UTS</div> 

<div class="panel" id="panel1" style="display: {$disp1} ">

<!-- set id_pt pada config.php global -->
{if $id_pt==1 }
<p><a href="jadwal-akademik.php">* Pastikan jadwal kegiatan akademik sudah terisi (Perkuliahan dan Ujian Akhir Semester)</a><br /></p> 
{else}
<p>* Proses cekal mengikuti kebijakan Perguruan Tinggi masing-masing<br /></p>
{/if}
<form action="proses-cekal-uts.php" method="post">
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			     <td width="20%" bgcolor="#FFCC33"><center>Prodi</center></td>
				 <td width="10%" bgcolor="#FFCC33"><center>MA Sdh Diproses</center></td>
				 <td width="10%" bgcolor="#FFCC33"><center>MA Blm Diproses</center></td>
				 <td width="10%" bgcolor="#FFCC33"><center>Aksi</center></td>
		</tr>
		
		<tr>
			     {foreach name=test item="prodi" from=$T_PRODI1}
				<tr>
				 <td>{$prodi.PRODI}</td>
				 <td><a href="proses-cekal-uts.php?action=detail&idprodi={$prodi.ID_PROGRAM_STUDI}&st=1">{$prodi.TTL_SDH}</a></td>
				 <td><a href="proses-cekal-uts.php?action=detail&idprodi={$prodi.ID_PROGRAM_STUDI}&st=2">{$prodi.TTL_BLM}</a></td>
				 <td><input name="Button" type="button" class="button" onClick="location.href='#presensi-pcuas!proses-cekal-uts.php?action=proses&idprodi={$prodi.ID_PROGRAM_STUDI}';disableButtons()" onMouseOver="window.status='Click untuk Proses';return true" onMouseOut="window.status='PROSES CEKAL UTS'" value="PROSES CEKAL UTS ALL/PRODI" /> </td>
			    </tr>
				{foreachelse}
        		<tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
        		{/foreach}
		</tr>   		
</table>
</form>	 
</div>
<div class="panel" id="panel2" style="display: {$disp2} ">
<p> </p> 
<p><div align="right"><input type="button" value="Kembali" onClick="location.href='#presensi-pcuas!proses-cekal-uts.php'"></p>

<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			     <td width="4%"  bgcolor="#FFCC33"><center>No</center></td>
				 <td width="10%" bgcolor="#FFCC33"><center>Kode Mata Ajar</center></td>
				 <td width="20%" bgcolor="#FFCC33"><center>Nama Mata Ajar</center></td>
				 <td width="10%" bgcolor="#FFCC33"><center>Kelas</center></td>
				 <td width="10%" bgcolor="#FFCC33"><center>TM</center></td>
				 <td width="10%" bgcolor="#FFCC33"><center>Di-entry</center></td>
				 <td width="10%" bgcolor="#FFCC33"><center>Aksi</center></td>
		</tr>
		
		{if $status==2 }
		<tr>
			     {foreach name=test item="ma" from=$DT_MA}
				<tr>
				 <td >{$smarty.foreach.test.iteration}</td>	
				 <td><a href="entry-presensi.php?action=entry&id_kelas_mk={$ma.ID_KELAS_MK}">{$ma.KD_MATA_KULIAH}</a></td>
				 <td>{$ma.NM_MATA_KULIAH}</td>
				 <td>{$ma.NAMA_KELAS}</td>
				 <td>{$ma.TM}</td>
				 <td>{$ma.TMHS}</td>
				 <td><input name="Button" type="button" class="button" onClick="location.href='#presensi-pcuas!proses-cekal-uts.php?action=proses_per_mk&idkelasmk={$ma.ID_KELAS_MK}&idprodi={$ma.ID_PROGRAM_STUDI}&st=2';disableButtons()" onMouseOver="window.status='Click untuk Proses Per MK';return true" onMouseOut="window.status='PROSES CEKAL UTS'" value="PROSES CEKAL UTS per MK" /> </td>
			    </tr>
				{foreachelse}
        		<tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
        		{/foreach}
		</tr> 
		{/if}
		{if $status==1 }
		<tr>
			     {foreach name=test item="ma" from=$DT_MA}
				<tr>
				 <td >{$smarty.foreach.test.iteration}</td>	
				 <td><a href="entry-presensi.php?action=entry&id_kelas_mk={$ma.ID_KELAS_MK}">{$ma.KD_MATA_KULIAH}</a></td>
				 <td>{$ma.NM_MATA_KULIAH}</td>
				 <td>{$ma.NAMA_KELAS}</td>
				 <td>{$ma.TM}</td>
				 <td>{$ma.TMHS}</td>
				 <td><input name="Button" type="button" class="button" onClick="location.href='#presensi-pcuas!proses-cekal-uts.php?action=proses_per_mk&idkelasmk={$ma.ID_KELAS_MK}&idprodi={$ma.ID_PROGRAM_STUDI}&st=1';disableButtons()" onMouseOver="window.status='Click untuk Proses Ulang';return true" onMouseOut="window.status='PROSES ULANG CEKAL UTS'" value="PROSES ULANG CEKAL UTS per MK" /> </td>
			    </tr>
				{foreachelse}
        		<tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
        		{/foreach}
		</tr> 
		{/if}
		
</table>
</div>
    


