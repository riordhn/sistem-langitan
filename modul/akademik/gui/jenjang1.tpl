{* smarty file for jenjang.php*}
{* Editor	: Yudi Sulistya *}
{* Updated	: 04/05/2011 *}

   		<div class="center_title_bar">Master Jenjang</div>  
<br><br>

        <div id="tabs">
        <div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
        <div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Input</div>
    	</div>
        <div class="tab_bdr"></div>
    <div class="panel" id="panel1" style="display: block">    
	  <table width="70%" border="0" cellspacing="0" cellpadding="0">
        <tr class="left_menu">
          <td width="20%" bgcolor="#333333" align="center"><font color="#FFFFFF">Kode Jenjang</font></td>
          <td width="40%" bgcolor="#333333" align="center"><font color="#FFFFFF">Nama Jenjang</font> </td>
          <td width="40%" bgcolor="#333333" align="center"><font color="#FFFFFF">Tindakan</font></td>
        </tr>
		{foreach item="list" from=$T_JENJANG}
        <tr>
		  <td align="center"><input name="edit_kode" type="text" style="width:60px;" value="{$list.ID_JENJANG}"></td>
		  <td align="center"><input name="edit_jenjang" type="text" value="{$list.NM_JENJANG}"></td>
		  <td align="center"><input type="button" name="ubah" value="Ubah">&nbsp;&nbsp;&nbsp;<input type="button" name="hapus" value="Hapus"></td>
        </tr>
        {foreachelse}
        <tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
      </table>
</div>
		
<div class="panel" id="panel2" style="display: none">
<br>
		<b>INPUT MASTER JENJANG</b>
<div id="loading" style="display:none;"><img src="../../../../img/akademik_images/fbloading.gif" alt="loading..." /></div>
<div id="result" style="display:none;"></div>
				<form id="addJenjang" action="{$smarty.server.REQUEST_URI}" method="POST">
				  <table class="tb_frame" width="70%" border="0" cellspacing="0" cellpadding="0">
					<tr>
					  <td width="20%">Kode Jenjang</td>
					  <td width="5%">:</td>
					  <td width="75%"><input type="text" name="add_kode"></td>
					</tr>
					<tr>
					  <td>Nama Jenjang</td>
					  <td>:</td>
					  <td><input type="text" name="add_jenjang"></td>
				    </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td><input type="submit" name="tambah" value="Tambah"></td>
					</tr>
				  </table>
				</form>
</div>
