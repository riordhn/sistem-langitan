   <div class="center_title_bar">Detail Biodata Mahasiswa</div>
<table style="width: 80%">
  <tr>
    <td colspan="8" style="border: none;padding: 5px 0px 15px 0px;">
    <input type="button" onclick="history.back()" value="Kembali" /></td>
  </tr>
  <tr>
    <th colspan="2" class="center">BIODATA MAHASISWA</th>
  </tr>
  <tr>
    <td colspan="2" class="center"><img src="../../foto_mhs/{$biodata_mahasiswa.NIM_MHS}.jpg" width="180" height="230"/></td>
  </tr>
  <tr>
    <td>NAMA</td>
    <td>{$biodata_mahasiswa.NM_C_MHS}</td>
  </tr>
  <tr>
    <td>NIM</td>
    <td>{$biodata_mahasiswa.NIM_MHS}</td>
  </tr>
  <tr>
    <td>JENJANG</td>
    <td>{$biodata_mahasiswa.NM_JENJANG}</td>
  </tr>
  <tr>
    <td>FAKULTAS</td>
    <td>{$biodata_mahasiswa.NM_FAKULTAS|upper}</td>
  </tr>
  <tr>
    <td>PROGRAM STUDI</td>
    <td>{$biodata_mahasiswa.NM_PROGRAM_STUDI}</td>
  </tr>
  <tr>
    <td>TEMPAT/TANGGAL LAHIR</td>
    <td>{$biodata_mahasiswa.NM_KOTA} / {$biodata_mahasiswa.TGL_LAHIR_PENGGUNA}</td>
  </tr>
  <tr>
    <td>TELP</td>
    <td>{$biodata_mahasiswa.TELP} / {$biodata_mahasiswa.MOBILE_MHS}</td>
  </tr>
  <tr>
    <td>KELAMIN</td>
    <td> {if $biodata_mahasiswa.KELAMIN_PENGGUNA==1}
      Laki-laki
      {else}
      Perempuan
      {/if} </td>
  </tr>
  <tr>
    <td>ALAMAT MAHASISWA</td>
    <td>{$biodata_mahasiswa.ALAMAT_MHS}</td>
  </tr>
  <tr>
    <td>SEKOLAH ASAL MAHASISWA</td>
    <td>{$biodata_mahasiswa.SEKOLAH_ASAL_MHS}</td>
  </tr>
  <tr>
    <td>TAHUN LULUS MAHASISWA</td>
    <td>{$biodata_mahasiswa.THN_LULUS_MHS}</td>
  </tr>
  <tr>
    <td>PRODI S1</td>
    <td>{$biodata_mahasiswa.PRODI_S1}</td>
  </tr>
  <tr>
    <td>TGL MASUK S1</td>
    <td>{$biodata_mahasiswa.TGL_MASUK_S1}</td>
  </tr>
  <tr>
    <td>TGL LULUS S1</td>
    <td>{$biodata_mahasiswa.TGL_LULUS_S1}</td>
  </tr>
  <tr>
    <td>PRODI S2</td>
    <td>{$biodata_mahasiswa.PRODI_S2}</td>
  </tr>
  <tr>
    <td>TGL MASUK S2</td>
    <td>{$biodata_mahasiswa.TGL_MASUK_S2}</td>
  </tr>
  <tr>
    <td>TGL LULUS S2</td>
    <td>{$biodata_mahasiswa.TGL_LULUS_S2}</td>
  </tr>
</table>
