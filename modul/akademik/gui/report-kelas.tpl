{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
            9: { sorter: false }
		}
		}
	); 
} 
);

$('form').validate();
</script>
{/literal}

<div class="center_title_bar">Rekapitulasi Presensi Perkuliahan {foreach item="list1" from=$NM_PRODI}{$list1.PRODI} {/foreach} {foreach item="list2" from=$THNSMT}{$list2.THN_SMT} {/foreach}</div>
<form action="report-kelas.php" method="post" name="id_daftarmk" id="id_daftarmk">
<input type="hidden" name="action" value="view">
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td>
			 	Prodi : 
               <select name="kdprodi"  class="required" >
    		   <option value=''>------</option>
			   <option value='all-{$FAK}' style="font-weight: bold; color: #fff; background-color: #233d0e;">LIHAT SEMUA</option>
	 		   {foreach item="list" from=$T_PRODI}
               		<option value="{$list.ID_PROGRAM_STUDI}" {if $list.ID_PROGRAM_STUDI == $smarty.request.kdprodi} selected="selected"{/if}>{$list.PRODI}</option>
    		   
               
	 		   {/foreach}
			   </select>
		     </td>
			 <td>
			 	Tahun/Semester : 
               <select name="kdthnsmt"  class="required" >
    		   <option value=''>------</option>
	 		   {foreach item="list_smt" from=$T_THNSMT}
               <option value="{$list_smt.ID_SEMESTER}" {if $list_smt.ID_SEMESTER == $smarty.request.kdthnsmt} selected="selected"{/if}>{$list_smt.THN_SMT}</option>
    		   
	 		   {/foreach}
			   </select>
		     </td>
			 <td>			   
			   <input type="submit" name="View" value="View">
		     </td> 
           </tr>
</table>
</form>
{if {$list1.PRODI} == '' && $list2.THNSMT == ''}
{else}
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
             <th width="5%">Kode</th>
             <th width="34%">Nama Mata Ajar</th>
             <th width="5%">SKS</th>
			 <th width="5%">KLS</th>
			 <th width="15%">PRODI ASAL</th>
			 <th width="8%">PST</th>
             <th width="5%">REN</th>
			 <th width="8%">TM</th>
			 <th width="8%">%</th>
           </tr>
		</thead>
		<tbody>
           {foreach name=test item="list" from=$T_MK}
		   <tr>
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td><center>{$list.KREDIT_SEMESTER}</center></td>
			 <td><center>{$list.NAMA_KELAS}</center></td>
			 <td><center>{$list.PRODI}</center></td>
			 <td><center>{$list.PESERTA}</center></td>
             <td><center>{$list.REN}</center></td>
			 <td><center>{$list.TM}</center></td>
			 <td><center>{$list.PERSEN}</center></td>
           </tr>
		   {foreachelse}
        <tr><td colspan="9"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
   	 </table>
{/if}