{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[1,0],[3,0]],
		}
	); 
} 
);
</script>
{/literal}
<div class="center_title_bar">Data Kelas Mata Ajar dan Team Teaching {$NM_PRODI}, Tahun Akademik {$THN_AKAD} </div>
<form action="pengampu-mk-psikologi.php" method="post" name="id_pengampu" id="id_pengampu">
<input type="hidden" name="action" value="view" >
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td>
			 	Prodi : 
                <select name="kdprodi">
    		   <option value=''>-- PRODI --</option>
	 		   {foreach item="list" from=$T_PRODI}
    		   {html_options  values=$list.ID_PROGRAM_STUDI output=$list.PRODI}
	 		   {/foreach}
			   </select>
		     </td>
			 <td>
				Tahun/Semester : 
               <select name="kdthnsmt">
    		   <option value=''>------</option>
	 		   {foreach item="list_smt" from=$T_THNSMT}
    		   {html_options values=$list_smt.ID_SEMESTER output=$list_smt.THN_SMT}
	 		   {/foreach}
			   </select>
			   <input type="submit" name="View" value="View">	
			 </td>
           </tr>
</table>
</form>
<div id="tabs">
	<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Per Matakuliah</div>
	<div id="tab2" class="tab" align="center" onclick="javascript: displayPanel('2');">Per Dosen</div>
</div>
<div class="panel" id="panel1" style="display: {$disp1} ">
<p></p>
<p>
<input type="button" name="cetak" value="Ekspor Data ke MS. Excel" onclick="window.open('proses/pengampu-mk-xls.php?prodi={$PRODI}&smt={$SMT_AKAD}&nm_prodi={$NM_PRODI}','baru2');">&nbsp;
<input type="button" name="cetak" value="Ekspor Data ke MS. Word" onclick="window.open('proses/pengampu-mk-doc.php?prodi={$PRODI}&smt={$SMT_AKAD}&nm_prodi={$NM_PRODI}','baru2');">
</p>
<div>Format Kolom Team:NIP,NIDN,NAMA dengan pemisah "-" dan yg pertama adalah PJMA</div>  
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
             <th width="8%">Kode MA</th>
             <th width="25%">Nama MA</th>
             <th width="4%">SKS</th>
			 <th width="4%">Kls</th>
             <th width="55%">Team (NIP-NIDN-NAMA DOSEN)</th>
           </tr>
		</thead>
		<tbody>
		   {foreach name=test item="list" from=$T_MK}
           <tr>
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td><center>{$list.KREDIT_SEMESTER}</center></td>
             <td><center>{$list.NAMA_KELAS}</center></td>
             <td>{$list.TEAM}</td>
           </tr>
            {foreachelse}
        <tr><td colspan="5"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
</table>
</div>

{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable1").tablesorter(
		{
		sortList: [[0,0]],
		}
	); 
} 
);
</script>
{/literal}
<div class="panel" id="panel2" style="display: {$disp2} ">
<p></p>
<table id="myTable1" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
             <th><center>Nama Dosen</center></th>
             <th><center>Nama Mata Kuliah (jumlah kelas)</center></th>
           </tr>
		</thead>
		<tbody>
		   {foreach name=dsn item="dos" from=$T_DOSEN}
           <tr>
             <td>{$dos.NAMA_DSN}</td>
             <td>{$dos.JML}</td>
           </tr>
            {foreachelse}
        <tr><td colspan="5"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
</table>
</div>