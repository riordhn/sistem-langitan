{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		}
	); 
} 
);
</script>
{/literal}

<div class="center_title_bar">Cek Persetujuan KRS/KPRS {foreach item="list1" from=$NM_PRODI}{$list1.PRODI} {/foreach}</div>
<form action="cek-apv.php" method="post" name="id_daftarmk" id="id_daftarmk">
<input type="hidden" name="action" value="view">
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td>
			 	Prodi : 
                <select name="kdprodi">
    		   <option value=''>------</option>
			   <option value='all-{$FAK}' style="font-weight: bold; color: #fff; background-color: #233d0e;">LIHAT SEMUA</option>
	 		   {foreach item="list" from=$T_PRODI}
    		   {html_options values=$list.ID_PROGRAM_STUDI output=$list.PRODI selected=$smarty.post.kdprodi}
	 		   {/foreach}
			   </select>
			   <input type="submit" name="View" value="View">
		     </td> 
           </tr>
</table>
</form>
{if {$list1.PRODI} == ''}
{else}
<p>Berikut daftar Mahasiswa yang telah melakukan KRS/KPRS namun belum di-approve oleh Dosen Wali</p>
		<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
             <th>NIP/NIK Dosen</th>
             <th>Nama Dosen</th>
             <th>NIM Mahasiswa</th>
           </tr>
		</thead>
		<tbody>
           {foreach name=test item="list" from=$T_MK}
		   <tr>
             <td>{$list.NIP_DOSEN}</td>
             <td>{$list.NM_PENGGUNA}</td>
             <td>{$list.NIM_MHS}</td>
           </tr>
		   {foreachelse}
        <tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
   	 </table>
{/if}