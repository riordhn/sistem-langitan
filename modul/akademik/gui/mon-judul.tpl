
<div class="center_title_bar">Monitoring Judul TA/SKRIPSI/TESIS - {$NAMA} - {$T_PW1}</div>
<form action="mon-judul.php" method="post">
<table class="tb_frame" width="80%" border="0" cellpadding="0" cellspacing="0">
   <tr>
	<td>Prodi :
	   <select name="kdprodi">
	   {foreach item="list" from=$T_PRODI}
	   {html_options  values=$list.ID_PROGRAM_STUDI output=$list.PRODI selected=$kodepro}
	   {/foreach}
	   </select></td>
	<td>Periode Wisuda :
	   <select name="periode">
	   {foreach item="list" from=$T_PW}
	   {html_options  values=$list.NM_PERIODE_WISUDA output=$list.NM_PERIODE_WISUDA selected=$T_PW1}
	   {/foreach}
	   </select></td>  
	<td>   <input type="submit" name="Proses" value="Proses"></td>
   </tr>
</table>
</form>

<div id="tabs">
        <div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
        <div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Edit Judul</div>
</div>
<div class="tab_bdr"></div>

<div class="panel" id="panel1" style="display: {$disp1} ">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<th>No</th>
		<th>Nim</th>
		<th>Nama Mhs</th>
		<th>Judul</th>
		<th>Pembimbing 1</th>
		<th>Pembimbing 2</th>
		<th>Bln Awal Bimbingan</th>
		<th>Bln Akhir Bimbingan</th>
		<th>Aksi</th>
	</tr>
	</thead>
	<tbody>
  {foreach name=test item="list" from=$T_TA}
	<tr>
    <td>{$smarty.foreach.test.iteration}</td>
    <td>{$list.NIM_MHS}</td>
    <td>{$list.NM_PENGGUNA}</td>
	<td>{$list.JUDUL_TA}</td>
	<td>{$list.DOSBING_1}</td>
	<td>{$list.DOSBING_2}</td>
	<td>{$list.BULAN_AWAL_BIMBINGAN}</td>
	<td>{$list.BULAN_AKHIR_BIMBINGAN}</td>
    <td align="center"><a href="mon-judul.php?action=edit&id_mhs={$list.ID_MHS}&id_prodi={$list.ID_PROGRAM_STUDI}&pws={$list.NM_TARIF_WISUDA}">Edit</a></td>
	</tr>
  {foreachelse}
	<tr><td colspan="9"><em>Data tidak ditemukan</em></td></tr>
  {/foreach}
  </tbody>
</table>
</div>

<div class="panel" id="panel2" style="display: {$disp2} ">
<form action="mon-judul.php" method="post">
<input type="hidden" name="action" value="update" >

<table class="tb_frame" width="80%" border="0" cellpadding="0" cellspacing="0">
	{foreach item="list" from=$T_DATAMHS}
	<tr>
		<td>Nim </td>
		<td>:</td>
		<td>{$list.NIM_MHS}</td>
	</tr>
	<tr>
		<td>Nama Mhs</td>
		<td>:</td>
		<td>{$list.NM_PENGGUNA}</td>
	</tr>
	
	<tr>
		<td>Judul </td>
		<td>:</td>
		<td><textarea name="judul" cols="60" rows="6">{$list.JUDUL_TA}</textarea></td>
	</tr>
	<tr>
		<td>Judul [English] </td>
		<td>:</td>
		<td><textarea name="judul_en" cols="60" rows="6">{$list.JUDUL_TA_EN}</textarea></td>
	</tr>
	<tr>
		<td>Dosen Pembimbing 1 </td>
		<td>:</td>
		<td>
				<select name="dosbing_1" >
					<option value="">-- Pilih Dosen --</option>
					{foreach item="dosen" from=$DOSEN}
					{html_options values=$dosen.ID_DOSEN output=$dosen.NAMA_DOSEN selected=$list.ID_PEMBIMBING_1}
					{/foreach}
				</select>
		</td>
	</tr>
	<tr>
		<td>Dosen Pembimbing 2 </td>
		<td>:</td>
		<td>
				<select name="dosbing_2" >
					<option value="">-- Pilih Dosen --</option>
					{foreach item="dosen" from=$DOSEN}
					{html_options values=$dosen.ID_DOSEN output=$dosen.NAMA_DOSEN selected=$list.ID_PEMBIMBING_2}
					{/foreach}
				</select>
		</td>
	</tr>
	<tr>
		<td>Bulan Awal Bimbingan </td>
		<td>:</td>
		<td><input type="text"  name="bulan_awal_bimbingan" value="{$list.BULAN_AWAL_BIMBINGAN}" /> * Format Bulan dan Tahun Saja, Contoh: 08-2016</td>
	</tr>
	<tr>
		<td>Bulan Akhir Bimbingan </td>
		<td>:</td>
		<td><input type="text"  name="bulan_akhir_bimbingan" value="{$list.BULAN_AKHIR_BIMBINGAN}" /> * Format Bulan dan Tahun Saja, Contoh: 08-2016</td>
	</tr>
	<tr>
	<td colspan="3"><div align="right"><input type="submit" name="Update" value="Update"></div></td> 
	</tr>
		<input type="hidden" name="id_mhs" value="{$list.ID_MHS}" >
		<input type="hidden" name="kdprodi" value="{$list.ID_PROGRAM_STUDI}" >
		<input type="hidden" name="idperiode" value="{$list.ID_PERIODE_WISUDA}" >
		<input type="hidden" name="periode" value="{$list.NM_TARIF_WISUDA}" >
	{/foreach}
	
</table>
</from>
</div>


{literal}
 <script>$('form').validate();</script>
{/literal}
