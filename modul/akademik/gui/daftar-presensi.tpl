{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript" src="includes/sortable/addons/pager/jquery.tablesorter.pager.js"></script>

<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[9,0],[1,0],[3,0]],
		}
	); 
} 
);
</script>
{/literal}

<div class="center_title_bar">ENTRY PRESENSI</div>
<div>
<form action="daftar-presensi.php" method="post">
<input type="hidden" name="action" value="tampil" >  
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
           <tr>
           	<td colspan="4">
			 	Program Studi : 
                <select name="prodi">
                <option value="">Semua</option>
	 		   {foreach item="data" from=$prodi}
    		   {html_options values=$data.ID_PROGRAM_STUDI output=$data.NM_PROD selected=$smarty.request.prodi}
	 		   {/foreach}
			   </select>
		     </td>
           </tr>
           
           <tr>
             <td>
			 	Tahun Akademik : 
                <select name="smt">
	 		   {foreach item="smt" from=$T_ST}
    		   {html_options values=$smt.ID_SEMESTER output=$smt.SMT selected=$SMTGET}
	 		   {/foreach}
			   </select>
		     </td>
             <td>
			 	Hari : 
                <select name="hari">
	 		   {foreach item="hari" from=$T_HR}
    		   {html_options values=$hari.ID_JADWAL_HARI output=$hari.NM_JADWAL_HARI selected=$HRGET}
	 		   {/foreach}
			   </select>
		     </td>
			 <td> <input type="submit" name="View" value="View"> </td>
           </tr>
</table>
</form>	    
		<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
			<tr>
             <th>Kode</th>
             <th>Nama Mata Ajar</th>
             <th>SKS</th>
			 <th>KLS</th>
             <th>STS</th>
             <th>SMT</th>
             <th>Hari</th>
			 <th>Jam</th>
			 <th>Pst</th>
			 <th>Total TM</th>
			 <th>Jml Pertemuan</th>
			 <th>Prodi MA</th>
           </tr>
		</thead>
		<tbody>
           {foreach name=test item="list" from=$T_MK}
		   <tr>
		   {if $FAK == 11}
             <td><a href="entry-presensi-psikologi.php?action=entry&id_kelas_mk={$list.ID_KELAS_MK}&smt={$SMTGET}">{$list.KD_MATA_KULIAH}</a></td>
		   {else}
             <td><a href="entry-presensi.php?action=entry&id_kelas_mk={$list.ID_KELAS_MK}&smt={$SMTGET}">{$list.KD_MATA_KULIAH}</a></td>
		   {/if}
             <td>{$list.NM_MATA_KULIAH}</td>
             <td><center>{$list.KREDIT_SEMESTER}</center></td>
			 <td><center>{$list.NAMA_KELAS}</center></td>
             <td><center>{$list.STATUS}</center></td>
             <td><center>{$list.TINGKAT_SEMESTER}</center></td>
             <td><center>{$list.NM_JADWAL_HARI}</center></td>
			 <td><center>{$list.JAM}</center></td>
			 <td><center>{$list.KLS_TERISI}</center></td>
			 <td><center>{$list.TM}</center></td>
			 <td><center>{$list.JUMLAH_PERTEMUAN_KELAS_MK}</center></td>
			 <td>{$list.PRODIASAL}</td>
            </td>
           </tr>
		   {foreachelse}
			<tr><td colspan="10"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
   	 </table>
</div>