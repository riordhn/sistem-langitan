<div class="center_title_bar">Presensi Susulan</div>
<div id="tabs">
<div id="tab1" {if $smarty.get.action=='entry'}class="tab_sel"{else}class="tab"{/if} align="center" onclick="javascript: displayPanel('1');">Rincian</div>
<div id="tab2" {if $smarty.get.action=='presensi'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Entry Susulan</div>
</div>
<div class="panel" id="panel1" style="display: {$disp1} ">
<p> </p>
	<table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	{foreach item="dtkelas" from=$DT_KELAS}
	  <tr>
		<td>Kode Mata Ajar</td>
		<td>:</td>
		<td>{$dtkelas.KD_MATA_KULIAH}</td>
	  </tr>
	  <tr>
		<td>Nama Mata Ajar</td>
		<td>:</td>
		<td>{$dtkelas.NM_MATA_KULIAH}</td>
	  </tr>
	  <tr>
		<td>SKS</td>
		<td>:</td>
		<td>{$dtkelas.KREDIT_SEMESTER}</td>
	  </tr>
	  <tr>
		<td>Kelas</td>
		<td>:</td>
		<td>{$dtkelas.NAMA_KELAS}</td>
	  </tr>
	  <tr>
		<td>Ruang</td>
		<td>:</td>
		<td>{$dtkelas.NM_RUANGAN}</td>
	  </tr>
	 {/foreach}
	</table>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr><td colspan="8"><em>Daftar Pertemuan Yang Tdk di Ikuti oleh: {$nim} - {$namamhs} </em></td></tr>
	<tr bgcolor="#000">
	 <td width="4%"><font color="#FFFFFF">Kuliah ke</font></td>
	 <td width="8%"><font color="#FFFFFF">Tgl</font></td>
	 <td width="8%"><font color="#FFFFFF">Jam</font></td>
	 <td width="25%"><font color="#FFFFFF">Materi</font></td>
	 <td width="26%"><font color="#FFFFFF">Dosen</font></td>
	 <td width="4%"><font color="#FFFFFF">MHS Msk</font></td>
	 <td width="4%"><font color="#FFFFFF">Absen</font></td>
	 <td width="6%"><font color="#FFFFFF">Aksi</font></td>
	</tr>
		{foreach name=test item="presensi" from=$DATA_PRESENSI}
	<tr>
	 <td>{$smarty.foreach.test.iteration}</td>
	 <td>{$presensi.TGL_PRESENSI_KELAS}</td>
	 <td>{$presensi.JAM}</td>
	 <td>{$presensi.ISI_MATERI_MK}</td>
	 <td><ol>{$presensi.NMDOS}</ol></td>
	 <td>{$presensi.HADIR}</td>
	 <td>{$presensi.ABSEN}</td>
	 <td><a href="entry-presensi-susulan.php?action=presensi&id_presensi={$presensi.IDPRESENSI}&id_kelas={$smarty.get.id_kelas_mk}&nim={$nim}" onclick="displayPanel('2')">Entry Susulan</a></td>
	</tr>
	 {foreachelse}
		<tr><td colspan="9"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
	</table>
</div>
<div class="panel" id="panel2" style="display: {$disp2}">
<p> </p>
<table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	 {foreach item="list" from=$DT_SUSULAN}
	  <tr>
		<td width="20%">Tgl Kuliah</td>
		<td width="1%">:</td>
		<td width="79%">{$list.TGL_PRESENSI_KELAS}</td>
	  </tr>
	  <tr>
		<td>Jam Mulai Kuliah</td>
		<td>:</td>
		<td>{$list.WAKTU_MULAI} - {$list.WAKTU_SELESAI}</td>
	  </tr>
	  <tr>
		<td>Materi</td>
		<td>:</td>
		<td>{$list.ISI_MATERI_MK}</td>
	  </tr>
	  {/foreach}
</table>
<form action="entry-presensi-susulan.php" method="post">
<input type="hidden" name="action" value="susulan">
<input type="hidden" name="id_kelas" value="{$smarty.get.id_kelas}">
<input type="hidden" name="id_presensi" value="{$smarty.get.id_presensi}">
NIM	: <input type="text" name="nim" value="{$nim}">
&nbsp;
Presensi : 
<select name="ket">
    <option value='0'>Tidak hadir</option>
    <option value='1'>Hadir</option>
    <option value='2'>Ijin</option>
    <option value='3'>Sakit</option>
</select>
&nbsp;
<input type="submit" name="simpan" value="Simpan">
</form>
<p> </p>
</div>