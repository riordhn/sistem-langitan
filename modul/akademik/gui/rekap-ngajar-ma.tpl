{literal}
<script language="javascript" type="text/javascript" src="js/datetimepicker.js"></script>
{/literal}
<div class="center_title_bar">Rekapitulasi Mengajar per Mata Ajar</div>
<form action="rekap-ngajar-ma.php" method="post" name="rekap_ngajar" id="rekap_ngajar">
		 <input type="hidden" name="action" value="rekap" >
<table border="0" cellspacing="0" cellpadding="0">
           <tr>
					  <td>Periode Rekap</td>
					  <td><input type="Text" name="calawal" value="{if !empty($smarty.post.calawal)}{$smarty.post.calawal}{/if}" id="calawal" maxlength="25" size="25" onClick="NewCssCal('calawal','ddmmmyyyy');"></td>
							{*<input type="button" value="" style="background-image:url(includes/cal/cal.gif); background-repeat:no-repeat; background-position:center; border:none;" onClick="NewCssCal('calawal','ddmmmyyyy');" /></td>*}
			</tr>
			<tr>
					 <td>Sampai Dgn</td>
					  <td><input type="Text" name="calakhir" value="{if !empty($smarty.post.calakhir)}{$smarty.post.calakhir}{/if}" id="calakhir" maxlength="25" size="25" onClick="NewCssCal('calakhir','ddmmmyyyy');"></td>
							{*<input type="button" value="" style="background-image:url(includes/cal/cal.gif); background-repeat:no-repeat; background-position:center; border:none;" onClick="NewCssCal('calakhir','ddmmmyyyy');" /></td>*}
					  
		   </tr>
		   <tr>
					 <td>Program Studi</td>
					  <td><select name="prodi">
    		   			<option value='ALL'>-- ALL --</option>
				   		{foreach item="prd" from=$T_PRODI}
    		   			{html_options  values=$prd.ID_PROGRAM_STUDI output=$prd.NM_PRODI selected=$smarty.post.prodi}
	 		  			 {/foreach}
			   		 </select></td>
		   </tr>
		   <tr>
             <td> Tahun Akademik </td>
             <td> <select name="smt">
    		   <option value=''>-- PILIH THN AKD --</option>
	 		   {foreach item="smt" from=$T_ST}
    		   {html_options  values=$smt.ID_SEMESTER output=$smt.SMT selected=$SMT}
	 		   {/foreach}
			   </select></td>
           </tr>	
</table>
<td><input type="submit" name="cari1" value="Proses"></td>
</form>

<br>
</br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr bgcolor="#000">
	 <td width="4%"><font color="#FFFFFF">No</font></td>
	 <td width="6%"><font color="#FFFFFF">Kode</font></td>
	 <td width="20%"><font color="#FFFFFF">Mata Ajar</font></td>
	 <td width="4%"><font color="#FFFFFF">Kelas</font></td>
	 <td width="4%"><font color="#FFFFFF">SKS</font></td>
	 <td width="20%"><font color="#FFFFFF">Dosen</font></td>
	 <td width="4%"><font color="#FFFFFF">Hadir</font></td>
	 <td width="4%"><font color="#FFFFFF">Total</font></td>
	 </tr>
	
	<tr>
	{foreach name=presen item="sp" from=$T_SPAN}	
	 <td rowspan="$sp.SPAN">{$smarty.foreach.presen.iteration}</td>
	 <td rowspan="$sp.SPAN">{$sp.KD_MATA_KULIAH}</td>
	 <td rowspan="$sp.SPAN">{$sp.NM_MATA_KULIAH}</td>
	 <td rowspan="$sp.SPAN">{$sp.NAMA_KELAS}</td>
	 <td rowspan="$sp.SPAN">{$sp.KREDIT_SEMESTER}</td>

	 {foreach name=presen1 item="rkp" from=$T_REKAP}
		{if $sp.ID_KELAS_MK==$rkp.ID_KELAS_MK}
			<td >{$rkp.DOSEN}</td>
			<td >{$rkp.MASUK}</td>
			<td >{$rkp.TOTAL}</td>
			<tr>
			</tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		{/if}		
	{/foreach}
	</tr>
	{/foreach}

</table>
{*	
{literal}
    <script>
            $("#calawal").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
			$("#calakhir").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
            $('form').validate();
    </script>
{/literal}
*}