<div class="center_title_bar">Buka Tutup KRS </div>  
{* Khusus Fakultas Psikologi *}
{if $FAK == 11}
	  <table border="0" cellspacing="0" cellpadding="0" style="width:800px">
        <tr class="left_menu">
          <td width="60%" bgcolor="#333333"><font color="#FFFFFF">Program Studi </font></td>
          <td width="15%" bgcolor="#333333"><font color="#FFFFFF">Status</font></td>
          <td width="25%" bgcolor="#333333"><font color="#FFFFFF">Aksi</font> </td>
        </tr>
		{foreach item="butu" from=$BUTU}
        <tr>
          <td >{$butu.PRODI}</td>
          <td >{$butu.STATUS}</td>
          <td ><a href="buka-tutup.php?action=update&idprodi={$butu.ID_PROGRAM_STUDI}&status=1">Buka</a> | <a href="buka-tutup.php?action=update&idprodi={$butu.ID_PROGRAM_STUDI}&status=2">Tutup</a> </td>
        </tr>
         {foreachelse}
        <tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
      </table>
{else}
	  <table border="0" cellspacing="0" cellpadding="0" style="width:800px">
        <tr class="left_menu">
          <td width="50%" bgcolor="#333333"><font color="#FFFFFF">Program Studi </font></td>
          <td width="10%" bgcolor="#333333"><font color="#FFFFFF">KRS Reguler</font></td>
          <td width="15%" bgcolor="#333333"><font color="#FFFFFF">KRS Reguler</font> </td>
          <td width="10%" bgcolor="#333333"><font color="#FFFFFF">KRS SP</font></td>
          <td width="15%" bgcolor="#333333"><font color="#FFFFFF">KRS SP</font> </td>
        </tr>
		{foreach item="butu" from=$BUTU}
        <tr>
          <td >{$butu.PRODI}</td>
          <td >{$butu.STATUS}</td>
          <td ><a href="buka-tutup.php?action=update&idprodi={$butu.ID_PROGRAM_STUDI}&status=1">Buka</a> | <a href="buka-tutup.php?action=update&idprodi={$butu.ID_PROGRAM_STUDI}&status=2">Tutup</a> </td>
          <td >{$butu.STATUS2}</td>
          <td ><a href="buka-tutup.php?action=update&idprodi={$butu.ID_PROGRAM_STUDI}&status=5">Buka</a> | <a href="buka-tutup.php?action=update&idprodi={$butu.ID_PROGRAM_STUDI}&status=6">Tutup</a> </td>
        </tr>
         {foreachelse}
        <tr><td colspan="5"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
      </table>
{/if}