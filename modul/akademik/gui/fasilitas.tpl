	<div class="center_title_bar">Master Fasilitas</div>

	<div id="tabs">
		<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
		<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Input</div>
		<div id="tab3" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Update</div>
   	</div>
    <div class="tab_bdr"></div>
    <div class="panel" id="panel1" style="display: {$disp1}">    			
			<table width="90%" border="0" cellspacing="0" cellpadding="0">
        <tr class="left_menu">
          <td width="10%" bgcolor="#333333"><font color="#FFFFFF">Kode </font></td>
          <td width="20%" bgcolor="#333333"><font color="#FFFFFF">No Inventaris </font></td>
          <td width="25%" bgcolor="#333333"><font color="#FFFFFF">Nama Fasilitas</font></td>
          <td width="30%" bgcolor="#333333"><font color="#FFFFFF">Deskripsi</font></td>
          <td width="15%" bgcolor="#333333"><font color="#FFFFFF"> Aksi</font></td>
        </tr>
		{foreach item="datafasilitas" from=$T_FASILITAS}
        <tr>
          <td >{$datafasilitas.ID_FASILITAS}</td>
          <td >{$datafasilitas.NO_INVENTARIS}</td>
          <td >{$datafasilitas.NM_FASILITAS}</td>
          <td >{$datafasilitas.DESKRIPSI}</td>
          <td ><a href="fasilitas.php?action=tampil&id_fas={$datafasilitas.ID_FASILITAS}" onclick="displayPanel('3')">Update</a> | <a href="fasilitas.php?action=del&id_fas={$datafasilitas.ID_FASILITAS}">Delete</a> </td>
        </tr>
         {foreachelse}
        <tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
      </table>
</div>
		
    <div class="panel" id="panel2" style="display: {$disp2}">
	 
	  <form action="fasilitas.php" method="post" >
	  <input type="hidden" name="action" value="add" />
	  
				  <table class="tb_frame" width="60%" border="0" cellspacing="0" cellpadding="0">
					<tr>
					  <td width="100">Nama Fasilitas</td>
					  <td width="23">:</td>
					  <td width="331"><input type="text" name="nmfas"></td>
					</tr>
					<tr>
					  <td>No Inventaris</td>
					  <td>:</td>
					  <td><input type="text" name="noinven"></td>
					</tr>
					<tr>
					  <td>Deskripsi</td>
					  <td>:</td>
					  <td><input type="text" name="deskripsi"></td>
					</tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td><input type="submit" name="Submit" value="Tambah"></td>
					</tr>
				  </table>
       </form>           
	  </div>	
	  
	  <div class="panel" id="panel3" style="display: {$disp3}">
	  
	  <form action="fasilitas.php" method="post" >
	  <input type="hidden" name="action" value="update" />
				  <table class="tb_frame" width="60%" border="0" cellspacing="0" cellpadding="0">
					{foreach item="datafasilitas" from=$T_FASILITAS}
					{if $datafasilitas.ID_FASILITAS==$idfasilitas}
					<input type="hidden" name="id_fasilitas" value="{$datafasilitas.ID_FASILITAS}" />
					<tr>
					  <td width="100">Nama Fasilitas</td>
					  <td width="23">:</td>
					  <td width="331"><input type="text" name="nmfas" class="required" value="{$datafasilitas.NM_FASILITAS}"></td>
					</tr>
					<tr>
					  <td>No Inventaris</td>
					  <td>:</td>
					  <td><input type="text" name="noinven" class="required" value="{$datafasilitas.NO_INVENTARIS}"></td>
					</tr>
					<tr>
					  <td>Deskripsi</td>
					  <td>:</td>
					  <td><input type="text" name="deskripsi" class="required" value="{$datafasilitas.DESKRIPSI}"></td>
					</tr>
					{/if}

					{/foreach}
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td><input type="submit" name="Submit" value="Tambah"></td>
					</tr>
				  </table>
       </form>           
	  </div>					
 <script>$('form').validate();</script>
