{* Update by Yudi Sulistya on Nov 23, 2011  *}
<div class="center_title_bar"> Ganti Password</div>
<td><font color="red"><strong> {$display} </strong></font></td>
<form action="ganti-pwd.php" method="post">
<input type="hidden" name="action" value="gantipwd" >  
<table class="tb_frame" border="1" cellspacing="0" cellpadding="0" id="tableText">
	<tr>
		<th colspan="2">Form Ganti Password</th>
	</tr>
	<tr>
		<td width="149">Password Lama</td>
		<td width="754"><input name="pwdlama" type="password" /></td>
	</tr>
	<tr>
		<td width="149">Password Baru</td>
		<td><input name="pwdbaru" type="password" /></td>
	</tr>
	<tr>
		<td width="149">Password Baru (ulangi)</td>
		<td><input name="pwdbaru2" type="password" /></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" name="Submit" value="Ganti"></td>
	</tr>
</table>
 </form>
 
<table>
	<tr>
		<th colspan="4">Multi Role</th>
	</tr>
	<tr>
		<td>Role</td>
		<td>
			<input type="hidden" name="id_pengguna" value="{$id_pengguna}" />
            <select name="id_role">
            {foreach $role_set as $r}
                <option value="{$r.ID_ROLE}" {if $r.ID_ROLE == $id_role}selected="selected"{/if}>{$r.NM_ROLE}</option>
            {/foreach}
            </select>
		</td>
		<td>Pindah Prodi</td>
		<td>
            <select name="id_unit_kerja">
			{if $kdfak == 11}
			{foreach $prodi_pindah as $p}
                <option value="{$p.ID_UNIT_KERJA}" {if $p.ID_PROGRAM_STUDI == $id_prodi}selected="selected"{/if}>{$p.NM_JENJANG} - {$p.NM_UNIT_KERJA}</option>
            {/foreach}
			{else}
            {foreach $prodi_pindah as $p}
                <option value="{$p.ID_UNIT_KERJA}" {if $p.ID_PROGRAM_STUDI == $id_prodi}selected="selected"{/if}>{$p.NM_JENJANG} - {$p.NM_UNIT_KERJA}</option>
            {/foreach}
			{/if}
            </select>
		</td>
	</tr>
	<tr>
		<td colspan="4"><center><button onclick="GantiRole(); return false;">Ganti Role</button></center></td>
	</tr>
</table>
 
{literal}
 <script>
 $('#tableText').validate();
function GantiRole()
{
	if (confirm('Apakah Anda yakin akan ganti role ?') == true)
	{
		var id_role = $('select[name="id_role"]').val();
		var id_unit_kerja = $('select[name="id_unit_kerja"]').val();
		var id_pengguna = $('input[name="id_pengguna"]').val();
		var path = (id_fakultas == 8) ? '../' : '';
		
		$.ajax({
			type: 'POST',
			url: path + 'ganti-pwd.php',
			data: 'mode=change-role&id_pengguna='+id_pengguna+'&id_role='+id_role+'&id_unit_kerja='+id_unit_kerja,
			success: function(data) {				
				if (data != '')
				{
					window.location = path + '../../logout.php';
				}
				else
				{
					alert('Gagal ganti role');
				}
			}
		});
	}
}
 </script>
{/literal}