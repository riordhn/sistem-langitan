<div class="center_title_bar">PENGAJUAN BEBAS SOP</div>
<form name="f" action="wisuda-sop.php" method="post">
<table>
	<tr>
    	<th>NO</th>
        <th>NIM</th>
        <th>NAMA</th>
        <th>PROGRAM STUDI</th>
        <th>MATA KULIAH</th>
        <th>NILAI</th>
        <th>BIAYA KULIAH</th>
        <th>AKSI</th>
    </tr>
    {$no = 1}
    {foreach $view_pengajuan as $data}
    	<tr>
        	<td>{$no++}</td>
            <td>{$data.NIM_MHS}</td>
            <td>{$data.NM_PENGGUNA}</td>
            <td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
            <td>{$data.NM_MATA_KULIAH}</td>
            <td>{$data.NILAI_HURUF}</td>
            <td style="text-align:right">{$data.BESAR_BIAYA|number_format:0:",":"."}</td>
            <td>
            <input type="checkbox" value="1" name="sop{$no-1}" />
            <input type="hidden" name="id_mhs{$no-1}" value="{$data.ID_MHS}" />
            </td>
        </tr>
    {/foreach}
    <input type="hidden" name="no" value="{$no}" />
    <tr>
    	<td colspan="8" style="text-align:center"><input type="submit"  value="Simpan" name="simpan" /></td>
    </tr>
</table>
</form>