   <div class="center_title_bar">Transkrip Mahasiswa </div>  
		<table width="70%" border="1">
         <tr>
           <td><table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td width="135">NIM </td>
               <td width="10">:</td>
               <td width="239"><input name="textfield" type="text" value="0903123456"></td>
             </tr>
             <tr>
               <td>Nama Mahasiswa</td>
               <td>:</td>
               <td>Sugiono</td>
             </tr>
             <tr>
               <td>Jurusan / Prodi</td>
               <td>:</td>
               <td><select name="select">
                 <option>Matematika</option>
               </select>
      		</td>
             </tr>
           </table>
             <p>
               <input type="submit" name="Submit" value="Tampilkan">
             </p></td>
         </tr>
       </table>       
       
       <p>&nbsp;</p>
       <table width="95%" border="0" cellspacing="0" cellpadding="0" class="tb_frame">
         <tr>
           <td >Nama</td>
           <td ><strong>: Sugiono</strong></td>
           <td >&nbsp;</td>
           <td >Nomor Ijazah</td>
           <td ><strong>: 3215/2212/2122</strong></td>
         </tr>
         <tr>
           <td >NIM</td>
           <td ><strong>: 0903123456</strong></td>
           <td >&nbsp;</td>
           <td >Jumlah SKS</td>
           <td ><strong>: 11</strong></td>
         </tr>
         <tr>
           <td >Program Studi</td>
           <td ><strong>: Matematika</strong></td>
           <td >&nbsp;</td>
           <td >IP Komulatif</td>
           <td ><strong>: 3,75</strong></td>
         </tr>
         <tr>
           <td width="32%" >Tanggal Terdaftar Pertama Kali</td>
           <td width="25%" ><strong>: 1 September 2005</strong></td>
           <td width="7%" >&nbsp;</td>
           <td width="16%" >Tanggal Lulus</td>
           <td width="20%" ><strong>: 3 Maret 2010</strong></td>
         </tr>
       </table>
       <p>&nbsp;</p>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
           <tr class="left_menu">
             <td width="5%" bgcolor="#333333"><font color="#FFFFFF">No.</font></td>
             <td width="21%" bgcolor="#333333"><font color="#FFFFFF">Semester</font></td>
             <td width="21%" bgcolor="#333333"><font color="#FFFFFF">Kode MK</font></td>
             <td width="41%" bgcolor="#333333"><font color="#FFFFFF">Nama Mata Kuliah </font></td>
             <td width="11%" bgcolor="#333333"><font color="#FFFFFF">SKS</font> </td>
             <td width="10%" bgcolor="#333333"><font color="#FFFFFF">Nilai</font></td>
             <td width="12%" bgcolor="#333333"><font color="#FFFFFF">Bobot</font></td>
           </tr>
           <tr>
             <td >1.</td>
             <td rowspan="3" >I / 2005-2006</td>
             <td >MK-0012</td>
             <td >Jaringan</td>
             <td >2</td>
             <td >A</td>
             <td >8</td>
           </tr>
           <tr>
             <td >2.</td>
             <td >MK-0034</td>
             <td >Analisa Pasar Keuangan</td>
             <td >2</td>
             <td >AB</td>
             <td >7</td>
           </tr>
           <tr>
             <td >3.</td>
             <td >MK-0032</td>
             <td >Matematika Dasar</td>
             <td >2</td>
             <td >AB</td>
             <td >7</td>
           </tr>
           <tr>
             <td >&nbsp;</td>
             <td >&nbsp;</td>
             <td >&nbsp;</td>
             <td >&nbsp;</td>
             <td >&nbsp;</td>
             <td >&nbsp;</td>
             <td >&nbsp;</td>
           </tr>
           <tr>
             <td >4.</td>
             <td rowspan="3" >II / 2005-2006</td>
             <td >MK-0021</td>
             <td >Bahasa Inggris 2</td>
             <td >2</td>
             <td >A</td>
             <td >8</td>
           </tr>
           <tr>
             <td >5.</td>
             <td >MK-0046</td>
             <td >Praktikum Pemrograman</td>
             <td >1</td>
             <td >AB</td>
             <td >7</td>
           </tr>
           <tr>
             <td >6.</td>
             <td >MK-0024</td>
             <td >Kewirausahaan</td>
             <td >2</td>
             <td >AB</td>
             <td >7</td>
           </tr>
           <tr>
             <td colspan="3" rowspan="3" >&nbsp;</td>
             <td ><div align="left">Jumlah SKS dan Bobot</div></td>
             <td >11</td>
             <td >&nbsp;</td>
             <td >44</td>
           </tr>
           <tr>
             <td >IP komulatif</td>
             <td colspan="3" ><div align="center">3,75</div></td>
           </tr>
           <tr>
             <td >Predikat Kelulusan</td>
             <td colspan="3" ><div align="center">Dengan Pujian</div></td>
           </tr>
         </table>
<input type="submit" name="button" id="button" value="Cetak" />
</div>