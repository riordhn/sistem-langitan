{literal}
<script language="javascript" type="text/javascript" src="js/datetimepicker.js"></script>
{/literal}
<div class="center_title_bar">Rekapitulasi Mengajar </div>
<form action="rekap-ngajar.php" method="post" name="rekap_ngajar" id="rekap_ngajar">
		 <input type="hidden" name="action" value="rekap" >
<table border="0" cellspacing="0" cellpadding="0">
           <tr>
					  <td>Periode Rekap</td>
					  <td>
						  <input type="Text" name="calawal" value="{if !empty($smarty.post.calawal)}{$smarty.post.calawal}{/if}" id="calawal" maxlength="25" size="25" onClick="NewCssCal('calawal','ddmmyyyy');">
						  &nbsp;Sampai Dgn :
						  <input type="Text" name="calakhir" value="{if !empty($smarty.post.calakhir)}{$smarty.post.calakhir}{/if}" id="calakhir" maxlength="25" size="25" onClick="NewCssCal('calakhir','ddmmyyyy');">
					  </td>
		   </tr>
		   <tr>
					 <td>Program Studi</td>
					  <td><select name="prodi">
    		   			<option value='ALL'>-- ALL --</option>
				   		{foreach item="prd" from=$T_PRODI}
    		   			{html_options  values=$prd.ID_PROGRAM_STUDI output=$prd.NM_PRODI selected=$smarty.post.prodi}
	 		  			 {/foreach}
			   		 </select></td>
		   </tr>
		   <tr>
             <td> Tahun Akademik </td>
             <td> <select name="smt">
    		   <option value=''>-- PILIH THN AKD --</option>
	 		   {foreach item="smt" from=$T_ST}
    		   {html_options  values=$smt.ID_SEMESTER output=$smt.SMT selected=$SMT}
	 		   {/foreach}
			   </select></td>
           </tr>	
</table>
<td><input type="submit" name="cari1" value="Proses"></td>
</form>

<br>
{* edit by Yudi Sulistya, 2013-12-10 *}
{* adding SKS real mengajar *}
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
<thead>
	<tr>
	 <th><center>No</center></th>
	 <th><center>Dosen</center></th>
	 <th><center>Kode</center></th>
	 <th><center>Mata Ajar</center></th>
	 <th><center>Kelas</center></th>
	 <th><center>SKS</center></th>
	 <th><center>Hadir</center></th>
	 <th><center>TTM</center></th>
	 <th><center>SKS Ajar</center></th>
	 </tr>
</thead>
<tbody>
	<tr>
	{foreach name=presen item="sp" from=$T_SPAN}	
	 <td rowspan="$sp.SPAN">{$smarty.foreach.presen.iteration}</td>
	 <td rowspan="$sp.SPAN">{$sp.DOSEN}</td>
	
	 {foreach name=presen1 item="rkp" from=$T_REKAP}
		{if $sp.DOSEN==$rkp.DOSEN}
			<td>{$rkp.KD_MATA_KULIAH}</td>
			<td>{$rkp.NM_MATA_KULIAH}</td>
			<td><center>{$rkp.NAMA_KELAS}</center></td>
			<td><center>{$rkp.KREDIT_SEMESTER}</center></td>
			<td><center>{$rkp.MASUK}</center></td>
			<td><center>{$rkp.TOTAL_PERTEMUAN}</center></td>
			<td><center>{math equation="((x / y) * z)" x=$rkp.MASUK y=$rkp.TOTAL_PERTEMUAN z=$rkp.KREDIT_SEMESTER format="%.2f"}</center></td>
			<tr>
			</tr>
			<td></td>
			<td></td>
		{/if}		
	{/foreach}
	</tr>
	{/foreach}
</tbody>
</table>
{*
{literal}
    <script>
            $("#calawal").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
			$("#calakhir").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
            $('form').validate();
    </script>
{/literal}
*}