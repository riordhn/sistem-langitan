{literal}
<link href="js/jquery.dataTables.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('table.display').dataTable({
		"sDom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>',
		"sPaginationType": "full_numbers",
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Semua"]]
	});
});
</script>
{/literal}

<div class="center_title_bar">PECAH / GABUNG KELAS </div>
<div id="tabs">
	<div id="tab1" {if $smarty.get.action == ''}class="tab_sel"{else}class="tab"{/if} align="center" onclick="javascript: displayPanel('1');"><a href="pecah-gabung.php" style="color:black;text-decoration:none;">Rincian</a></div>
    <div id="tab2" {if $smarty.get.action == 'pegab'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Update</div>
	<div id="tab3" {if $smarty.get.action == 'detail'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Peserta MA</div>
</div>
<div class="panel" id="panel1" style="display: {$disp1} ">
<p> </p>
	<table id="" class="display" cellspacing="0" width="100%">
		<thead>
           <tr>
            <th>Kode</th>
            <th>Nama Mata Ajar</th>
			<th>SKS</th>
			<th>KLS</th>
            <th>STS</th>
            <th>Kpst</th>
			<th>Terisi</th>
			<th>Prodi MA</th>
            <th>Aksi</th>
           </tr>
		</thead>
		<tbody>
           {foreach name=test item="list" from=$T_MK}
		   {if $list.KAPASITAS_KELAS_MK - $list.KLS_TERISI == 0}
		   <tr bgcolor="#ff828e">
   		   {elseif $list.KAPASITAS_KELAS_MK - $list.KLS_TERISI <= 5}
		   <tr bgcolor="#f5f2a3">
		   {else}
		   <tr>
		   {/if}
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td><center>{$list.KREDIT_SEMESTER}</center></td>
			 <td><center>{$list.NAMA_KELAS}</center></td>
             <td><center>{$list.STATUS}</center></td>
			 <td><center>{$list.KAPASITAS_KELAS_MK}</center></td>
			 <td>
				<center>
					<button style="font-weight:bold;background:transparent;border:none;" onclick="window.location.href = '{$base_url}modul/akademik/#aktivitas-pgk!pecah-gabung.php?action=detail&id_kelas_mk={$list.ID_KELAS_MK|base64_encode}'" alt="Daftar peserta" title="Daftar peserta">{$list.KLS_TERISI}</button>
				</center>
			</td>
			 <td>{$list.PRODIMK}</td>
             <td>
				<center>
					<img src="includes/img/import_export.png" border="0" alt="Pecah-Gabung Kelas" title="Pecah-Gabung Kelas" style="cursor:pointer;" onclick="window.location.href = '{$base_url}modul/akademik/#aktivitas-pgk!pecah-gabung.php?action=pegab&id_kelas_mk={$list.ID_KELAS_MK|base64_encode}&kdmk={$list.KD_MATA_KULIAH|base64_encode}'" />
				</center>
			</td>
           </tr>
        {/foreach}
		</tbody>
		<tfoot>
           <tr>
            <th>Kode</th>
            <th>Nama Mata Ajar</th>
			<th>SKS</th>
			<th>KLS</th>
            <th>STS</th>
            <th>Kpst</th>
			<th>Terisi</th>
			<th>Prodi MA</th>
            <th>Aksi</th>
           </tr>
		</tfoot>
   	 </table>
<p><br>&nbsp;</p>
</div>

<div class="panel" id="panel2" style="display: {$disp2}">
{if $smarty.get.action == 'pegab'}
<p> </p>
<form action="pecah-gabung.php" method="post" name="pg" id="pg">
<input type="hidden" name="action" value="gantikelas" >
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>KELAS ASAL</td>
			<td><center>:</center></td>
			<td>
				<select name="kelas_mk_asal" id="kelas_mk_asal" disabled="disabled">
                    {foreach item="kelas_tujuan" from=$T_KT}
    		   		{html_options values=$kelas_tujuan.ID_KELAS_MK output=$kelas_tujuan.KELAS_TUJUAN selected=$kls_asal}
	 		   		{/foreach}
                 </select>
			</td>
		</tr>
		<tr>
			<td>KELAS TUJUAN</td>
			<td><center>:</center></td>
			<td>
				<select name="kelas_mk_tujuan" id="kelas_mk_tujuan">
                    {foreach item="kelas_tujuan" from=$T_KT}
    		   		{html_options values=$kelas_tujuan.ID_KELAS_MK output=$kelas_tujuan.KELAS_TUJUAN}
	 		   		{/foreach}
                 </select>
			</td>
		</tr>
		<tr>
			<td></td><td></td>
			<td><input name="Proses" type="submit" value="Proses" /></td>
		</tr>
	</table>
	<table id="" class="display" cellspacing="0" width="100%">
		<thead>
           <tr>
             <th>NIM</th>
             <th>Nama Mahasiswa</font> </td>
			 <th>Prodi</th>
			 <th>Pilih</th>
		   </tr>
		</thead>
		<tbody>
           {foreach name=test item="list" from=$T_DETAIL1}
		   <tr>
             <td>{$list.NIM_MHS}</td>
             <td>{$list.NM_PENGGUNA}</td>
             <td>{$list.NM_PROGRAM_STUDI}</td>
			 <td><center><input name="pil{$smarty.foreach.test.iteration}" type="checkbox" value="{$list.ID_PENGAMBILAN_MK}" /><center></td>
           </tr>
		   {/foreach}
		<input type="hidden" name="counter" value="{$smarty.foreach.test.iteration}" >
		</tbody>
		<tfoot>
           <tr>
             <th>NIM</th>
             <th>Nama Mahasiswa</font> </td>
			 <th>Prodi</th>
			 <th>Pilih</th>
		   </tr>
		</tfoot>
	</table>
<p><br>&nbsp;</p>
</form>
{else}
<p> </p>
{/if}
</div>

<div class="panel" id="panel3" style="display: {$disp3} ">
{if $smarty.get.action == 'detail'}
<p> </p>
<table>
	{foreach item="info" from=$T_INFO}
	<tr>
		<td width="29%">Kode Mata Ajar / SKS / Kelas</td>
		<td width="2%">:</td>
		<td width="69%">{$info.KD_MATA_KULIAH} / {$info.KREDIT_SEMESTER} / {$info.NAMA_KELAS}</td>
	</tr>
	<tr>
		<td width="29%">Nama Mata Ajar</td>
		<td width="2%">:</td>
		<td width="69%">{$info.NM_MATA_KULIAH}</td>
	</tr>
	<tr>
		<td width="29%">Tim Pengajar</td>
		<td width="2%">:</td>
		<td width="69%" style="padding-left:10px;"><br><ol>{$info.TIM}</li></ol></td>
	</tr>
		{/foreach}
</table>
	<table id="" class="display" cellspacing="0" width="100%">
		<thead>
           <tr>
			 <th>NIM</th>
			 <th>Nama Mahasiswa</th>
			 <th>Prodi</th>
			 <th>Status</th>
           </tr>
		</thead>
		<tbody>
			{foreach name=test item="list" from=$T_DETAIL}
			{if $list.STATUS == "Belum Disetujui"}
			<tr bgcolor="#f5f2a3">
			{else}
			<tr>
			{/if}
				<td>{$list.NIM_MHS}</td>
				<td>{$list.NM_PENGGUNA}</td>
				<td>{$list.NM_PROGRAM_STUDI}</td>
				{if $list.STATUS == "Belum Disetujui"}
				<td><center><img src="includes/img/forbidden.png" border="0" alt="{$list.STATUS}" title="{$list.STATUS}"/><center></td>
				{else}
				<td><center><img src="includes/img/success.png" border="0" alt="{$list.STATUS}" title="{$list.STATUS}"/><center></td>
				{/if}
			</tr>
			{/foreach}
		</tbody>
		<tfoot>
           <tr>
			 <th>NIM</th>
			 <th>Nama Mahasiswa</th>
			 <th>Prodi</th>
			 <th>Status</th>
           </tr>
		</tfoot>
	</table>
<p><br>&nbsp;</p>
{else}
<p> </p>
{/if}
</div>