{*
{literal}
<script type="text/javascript">
function validate(){
if (document.forms.id_transkrip.mode.selectedIndex == "" || document.forms.id_transkrip.mode.selectedIndex == null){
	alert ("Pilih kriteria.");
	return false;
}
return true;
}
function reloadpage()
{
            $.ajax({
                type:'post',
                url:'/modul/akademik/fst/rekap_nilai.php',
                data:'action=view&nim='+$('#nimx').val(),
                success:function(data){
                    $('#content').html(data);
                }    
            });
}
</script>
{/literal}
*}
{literal}
<script type="text/javascript">
function reloadpage()
{
            $.ajax({
                type:'post',
                url:'/modul/akademik/fst/rekap_nilai.php',
                data:'action=view&nim='+$('#nimx').val(),
                success:function(data){
                    $('#content').html(data);
                }    
            });
}
</script>
{/literal}
<div class="center_title_bar">Rekapitulasi Nilai {foreach item="list1" from=$MHS}{$list1.NM_MHS} {/foreach} </div>  
<form action="/modul/akademik/fst/rekap_nilai.php" method="post" name="id_transkrip" id="id_transkrip">
<input type="hidden" name="action" value="view" >
<table width="100%" border="0" cellpadding="0" cellspacing="0">
           <tr>
			 <td>NIM : <input id="nim_mhs" type="text" name="nim" value="{$NIMX}"></td>
			 <td><input type="submit" name="View" value="View"></td>
           </tr>
</table>
</form>
<hr/>
<a class="disable-ajax" href="/modulx/datadx.php?idfak={$FAK}" target="_new">DAFTAR MAHASISWA DENGAN NILAI D LEBIH DARI 20%</a><br />
<a class="disable-ajax" href="/modulx/sebaranip.php?idfak={$FAK}" target="_new">DAFTAR IPK,SKS KUMULATIF,IPS DAN SKS SEMESTER MAHASISWA (BAYAR SPP)</a>	<br />
<a class="disable-ajax" href="/modulx/sebaranipx.php?idfak={$FAK}" target="_new">DAFTAR IPK,SKS KUMULATIF,IPS DAN SKS SEMESTER MAHASISWA (TIDAK BAYAR SPP)</a>	
{if {$list1.NM_MHS} == ''}
{else}
{foreach item="cek" from=$T_MHS}

{if {$cek.STATUS_AKADEMIK_MHS} == 40}
<p><b>Maaf, {$cek.NM_PENGGUNA} dengan NIM:{$cek.NIM_MHS} sudah dinyatakan lulus, mohon gunakan cetak Transkrip.</b></p>
{elseif {$cek.STATUS_AKADEMIK_MHS} > 40 && {$cek.STATUS_AKADEMIK_MHS} < 160}
<p><b>Maaf, {$cek.NM_PENGGUNA} dengan NIM:{$cek.NIM_MHS} sedang dalam status tidak aktif.</b></p>
{else}

{foreach item="list0" from=$T_MHS}
<div class="panel" id="panel1" style="display: block">
<br>
<p>
</p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr>
             <td>NIM</td>
			 <td><center>:</center></td>
			 <td>{$list0.NIM_MHS}<input type="hidden" id="nimx" name="nimx" value="{$list0.NIM_MHS}" /></td>
           <tr>			 
             <td>Nama Mahasiswa</td>
			 <td><center>:</center></td>
			 <td>{$list0.NM_PENGGUNA}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
           </tr>
           <tr>
             <td>Program Studi</td>
			 <td><center>:</center></td>
			 <td>{$list0.NM_JENJANG} {$list0.NM_PROGRAM_STUDI}</td>
           </tr>
</table>
{/foreach}
<table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr class="left_menu">
             <td width="2%" bgcolor="#333333"><font color="#FFFFFF">No</font></td>
             <td width="5%" bgcolor="#333333"><font color="#FFFFFF">Smtr</font></td>
			 <td width="5%" bgcolor="#333333"><font color="#FFFFFF">Kode MA</font></td>
			 <td width="9%" bgcolor="#333333"><font color="#FFFFFF">Nama MA</font></td>
             <td width="5%" bgcolor="#333333"><font color="#FFFFFF">SKS</font></td>
			 <td width="5%" bgcolor="#333333"><font color="#FFFFFF">Nilai</font></td>
			 <td width="2%" bgcolor="#333333"><font color="#FFFFFF">Bobot</font></td>
			 <td width="9%" bgcolor="#333333"><font color="#FFFFFF">P1</font></td>
			 <td width="9%" bgcolor="#333333"><font color="#FFFFFF">P2</font></td>
			 <td width="9%" bgcolor="#333333"><font color="#FFFFFF">P3</font></td>
			 <td width="9%" bgcolor="#333333"><font color="#FFFFFF">P4</font></td>
			 <td width="9%" bgcolor="#333333"><font color="#FFFFFF">P5</font></td>
			 <td width="9%" bgcolor="#333333"><font color="#FFFFFF">P6</font></td>			 
			 <td width="9%" bgcolor="#333333"><font color="#FFFFFF">Masuk Transkrip</font></td>
           </tr>
		   {foreach name=test item="list" from=$T_MK}
		{if $list.NILAI=='D' or $list.NILAI=='E'}
			{if $list.STATUS_HAPUS=='0'}
			<tr bgcolor="yellow">
			{else}
			<tr bgcolor="#772244">
			{/if}
		{else}
		{if $smarty.foreach.test.iteration%2==0}
			{if $list.STATUS_HAPUS=='0'}
           <tr bgcolor="#ffffff">
			{else}
			<tr bgcolor="#772244">
			{/if}		
		{else}
			{if $list.STATUS_HAPUS=='0'}
           <tr bgcolor="#cccccc">
			{else}		
			<tr bgcolor="#772244">
			{/if}		
	   {/if}
		{/if}
             <td >{$smarty.foreach.test.iteration}</td>
		<td >{$list.SMTR}</td>
             <td >{$list.KODE}</td>
             <td >{$list.NAMA}</td>
             <td >{$list.SKS}</td>
             <td >{$list.NILAI}</td>
			 <td >{($list.BOBOT*$list.SKS)|number_format:2:".":","}</td>
		{if $list.P1 eq '- Nilai:'}
		<td >-</td>
		{else}
		<td ><a class="disable-ajax" style="text-decoration:underline;" onclick="window.open('/modul/akademik/fst/updt_nilai.php?idpmk={$list.IPM1}');">{$list.P1}</a>
		{if $list.W1 eq ''}
		-
		{else}<br/>
		<a class="disable-ajax" style="text-decoration:underline;color:red" onclick="window.open('/modul/akademik/fst/log_updt_nilai.php?idpmk={$list.IPM1}');">[pernah diubah]</a>
		{/if}
		</td>
		{/if}
		{if $list.P2 eq '- Nilai:'}
		<td >-</td>
		{else}
		<td ><a class="disable-ajax" style="text-decoration:underline;" onclick="window.open('/modul/akademik/fst/updt_nilai.php?idpmk={$list.IPM2}');">{$list.P2}</a>
				{if $list.W2 eq ''}
		-
		{else}<br/>
		<a class="disable-ajax" style="text-decoration:underline;color:red" onclick="window.open('/modul/akademik/fst/log_updt_nilai.php?idpmk={$list.IPM2}');">[pernah diubah]</a>
		{/if}
		</td>
		{/if}
		{if $list.P3 eq '- Nilai:'}
		<td >-</td>
		{else}
		<td ><a class="disable-ajax" style="text-decoration:underline;" onclick="window.open('/modul/akademik/fst/updt_nilai.php?idpmk={$list.IPM3}');">{$list.P3}</a>
				{if $list.W3 eq ''}
		-
		{else}<br/>
		<a class="disable-ajax" style="text-decoration:underline;color:red" onclick="window.open('/modul/akademik/fst/log_updt_nilai.php?idpmk={$list.IPM3}');">[pernah diubah]</a>
		{/if}
		</td>
		{/if}
		{if $list.P4 eq '- Nilai:'}
		<td >-</td>
		{else}
		<td ><a class="disable-ajax" style="text-decoration:underline;" onclick="window.open('/modul/akademik/fst/updt_nilai.php?idpmk={$list.IPM4}');">{$list.P4}</a>
				{if $list.W4 eq ''}
		-
		{else}<br/>
		<a class="disable-ajax" style="text-decoration:underline;color:red" onclick="window.open('/modul/akademik/fst/log_updt_nilai.php?idpmk={$list.IPM4}');">[pernah diubah]</a>
		{/if}
		</td>
		{/if}
		{if $list.P5 eq '- Nilai:'}
		<td >-</td>
		{else}
		<td ><a class="disable-ajax" style="text-decoration:underline;" onclick="window.open('/modul/akademik/fst/updt_nilai.php?idpmk={$list.IPM5}');">{$list.P5}</a>
				{if $list.W5 eq ''}
		-
		{else}<br/>
		<a class="disable-ajax" style="text-decoration:underline;color:red" onclick="window.open('/modul/akademik/fst/log_updt_nilai.php?idpmk={$list.IPM5}');">[pernah diubah]</a>
		{/if}
		</td>
		{/if}
		{if $list.P6 eq '- Nilai:'}
		<td >-</td>
		{else}
		<td ><a class="disable-ajax" style="text-decoration:underline;" onclick="window.open('/modul/akademik/fst/updt_nilai.php?idpmk={$list.IPM6}');">{$list.P6}</a>
				{if $list.W6 eq ''}
		-
		{else}<br/>
		<a class="disable-ajax" style="text-decoration:underline;color:red" onclick="window.open('/modul/akademik/fst/log_updt_nilai.php?idpmk={$list.IPM6}');">[pernah diubah]</a>
		{/if}
		</td>
		{/if}
		
                <td class="center">
                    <input class="status_hapus" type="checkbox" name="id_pengambilan_mk" value="{$list.IDS_PENGAMBILAN_MK}" {if $list.STATUS_HAPUS==0}checked="true"{/if}/>
                </td>
           </tr>
            {foreachelse}
        <tr><td colspan="14"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		{foreach item="footer" from=$T_IPK}
           <tr>
             <td colspan="4"><b>Total SKS dan Bobot</b></td>
             <td><b>{$footer.SKS_TOTAL}</b></td>
			 <td></td>
			 <td><b>{$footer.BOBOT_TOTAL}</b></td>
           </tr>
           <tr>
			 <td colspan="4"><b>Indeks Prestasi Kumulatif</b></td>
             <td colspan="3"><b>{$footer.IPK}</b></td>
           </tr>
        {/foreach}
</table>
</div>
{/if}
{/foreach}
{/if}

{literal}
    <script type="text/javascript">
        $('.status_hapus').click(function(){
            $.ajax({
                type:'post',
                url:'/modul/akademik/fst/rekap_nilai.php',
                data:'action=view&id_pengambilan_mk='+$(this).val()+'&nim='+$('#nimx').val(),
                success:function(data){
                    $('#content').html(data);
                }    
            });
        });
    </script>
{/literal}