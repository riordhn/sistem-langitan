<div class="center_title_bar">Pembayaran Detail Mahasiswa</div>  
        
<form method="post" action="pembayaran-detail.php">
    <table>
        <tr>
        	<td>Bayar</td>
            <td>
            <select name="bayar">
           	  <option value="NOT NULL" {if $bayar == 'NOT NULL'}selected="selected"{/if}>Sudah</option>
              <option value="NULL" {if $bayar == 'NULL'}selected="selected"{/if}>Belum</option>
            </select>
          </td>
            <td>Prodi</td>
            <td>
            <select name="prodi">
            	{foreach $prodi as $data}
                	<option value="{$data['ID_PROGRAM_STUDI']}" {if $id_prodi == $data['ID_PROGRAM_STUDI']}
                    selected="selected" {/if}>{$data['NM_JENJANG']} - {$data['NM_PROGRAM_STUDI']}</option>
                {/foreach}
            </select>
            </td>
            <td>Semester</td>
            <td>
            <select name="semester">
            	{foreach $semester as $data}
                	<option value="{$data['ID_SEMESTER']}" {if $id_semester == $data['ID_SEMESTER']}
                    selected="selected" {/if}>{$data['THN_AKADEMIK_SEMESTER']} - {$data['NM_SEMESTER']}</option>
                {/foreach}
            </select>
            </td>
            <td><input type="submit" value="Tampil"/></td>
        </tr>
    </table>
</form>
{if isset($data_pembayaran)}

<div style="overflow: scroll;width: 900px;height: 600px;">
    <table width="100%">
        <tr>
            <th style="text-align: center;">No</th>
            <th style="text-align: center;">NIM</th>
            <th style="text-align: center;">Nama</th>
			<th style="text-align: center;">Jalur</th>
            {foreach $biaya as $data}
            <th style="text-align: center;">{$data.NM_BIAYA}</th>
            {/foreach}
	     	<th style="text-align: center;">Total</th>
        </tr>
        
        {foreach $data_pembayaran as $data}
        <tr>
            <td>{$data@index+1}</td>
            <td>{$data['NIM_MHS']}</td>
            <td>{$data['NM_PENGGUNA']}</td>
			<td>{$data['NM_JALUR']}</td>
            {$total = 0}
            {foreach $biaya as $data2}
          		<td style="text-align: right;">
                	{foreach $detail_data_pembayaran as $x}
                    	{if $data2.ID_BIAYA == $x.ID_BIAYA and $data.ID_MHS == $x.ID_MHS}
                        	{$x.BESAR_BIAYA|number_format:2:",":"."}
                            {$total = $total + $x.BESAR_BIAYA}
                            {break}
                        {/if}
                    {/foreach}
                </td> 
            {/foreach}
		<td style="text-align: right;">{$total|number_format:2:",":"."}</td>       
        </tr>
        {/foreach}

        <tr style="font-weight:bold">
        	<td colspan="4">Total</td>
            {$total_all = 0}
            {foreach $biaya as $data2}
            	<td style="text-align: right;">
                {$total = 0}
                {$nilai = false}
                {foreach $detail_data_pembayaran as $x}
                    	{if $data2.ID_BIAYA == $x.ID_BIAYA}
                        	{$total = $total + $x.BESAR_BIAYA}
                            {$nilai = true}
                        {/if}
                {/foreach}
                {if $nilai == false}
                	0
                {else}
                	{$total|number_format:2:",":"."}
                    {$total_all = $total_all + $total}
                {/if}
                </td>
            {/foreach}
            <td style="text-align: right;">{$total_all|number_format:2:",":"."}</td>
            <td></td>
        </tr>
    </table>
</div>
{/if}