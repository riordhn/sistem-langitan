<div class="center_title_bar">Rekapitulasi Prodi</div>
<input type="button" name="excel" value="Cetak Excel" onclick="get_link_report('excel')" />
<script type="text/javascript">
        function get_link_report(type){
            link=document.location.hash.replace('#pembayaran-pembayaran_prodi!', '');
            if(type=='excel'){
                window.location.href='excel-'+link;
            }else
                window.open('cetak-'+link);
        }
</script>
<div style="overflow: scroll;width: 900px;height: 600px;">
<table width="200" border="1">
  <tr>
    <th rowspan="2">Prodi</th>
    {foreach $semester_bayar as $data}
    <th colspan="{count($biaya)}" style="text-align:center">
    	{$data.THN_AKADEMIK_SEMESTER}
    </th>
    {/foreach}
    <th rowspan="2">Σ Per Prodi</th>
  </tr>
  <tr>
  {foreach $semester_bayar as $data}
  		{foreach $biaya as $data1}
   			 <th>{$data1.NM_BIAYA}</th>
    	{/foreach}
   {/foreach}  
   </tr>
 
 {foreach $prodi as $data}
  <tr> 
    <td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
    {$total = 0}
    {foreach $semester_bayar as $data1}
  		{foreach $biaya as $data2}
   			 <td style="text-align:right">
             	{$nilai = false}
                {$jumlah = 0}
             	{foreach $pembayaran_prodi as $x}
                	{if $x.THN_AKADEMIK_SEMESTER == $data1.THN_AKADEMIK_SEMESTER and $x.ID_PROGRAM_STUDI == $data.ID_PROGRAM_STUDI	
                    	and $x.ID_BIAYA == $data2.ID_BIAYA}
                        {$jumlah = $jumlah + $x.JUMLAH}
                        {$nilai = true}                        
                    {/if}
                {/foreach}
                {if $nilai == true}
                	{$jumlah|number_format:2:",":"."}
                    {$total = $total + $jumlah}
                {else}
                	0
                {/if}
             </td>
         {/foreach}
   {/foreach}
    <td style="text-align:right">{$total|number_format:2:",":"."}</td>
  </tr>
 {/foreach}
  <tr>
    <td>Total</td>
    {$total_all = 0}
    {foreach $semester_bayar as $data}
  		{foreach $biaya as $data1}
             <td style="text-align:right"> 
             {$nilai = false}
             {$total = 0}
       		 {foreach $pembayaran_prodi as $x}
                	{if $x.THN_AKADEMIK_SEMESTER == $data.THN_AKADEMIK_SEMESTER and $x.ID_BIAYA == $data1.ID_BIAYA}
                        {$total = $total + $x.JUMLAH}
                        {$nilai = true}
                    {/if}
              {/foreach}
             	{if $nilai == false}
                	0
                {else}
                	{$total|number_format:2:",":"."}
                    {$total_all = $total_all + $total}
                {/if}
             </td>
         {/foreach}
    {/foreach}
    <td style="text-align:right">{$total_all|number_format:2:",":"."}</td>
  </tr>
</table>
</div>