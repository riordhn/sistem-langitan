
<div class="center_title_bar">Konversi Nilai : {$nim}</div> 
<form action="konversi-nilai.php" method="post">
<input type="hidden" name="action" value="viewdetail" >
     <table width="100%" border="1">
        <tr>
               <td>NIM</td>
               <td>:</td>
               <td><input name="nim" type="text" value="{if !empty($smarty.post.nim)}{$smarty.post.nim}{/if}"></td>
			   <td><input type="submit" name="Submit" value="Tampil"></td>
        </tr>
	</table>
</form>	

<div id="tabs">
	<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Konversi</div>
</div>

<div class="panel" id="panel1" style="display: {$disp1}">
<p> </p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
			 <td bgcolor="#FFCC33"><center>No</center></td>
             <td bgcolor="#FFCC33"><center>Kode MA</center></td>
			 <td bgcolor="#FFCC33"><center>Nama MA</center></td>
             <td bgcolor="#FFCC33"><center>SKS</center></td>
             <td bgcolor="#FFCC33"><center>Smt</center></td>
			 <td bgcolor="#FFCC33"><center>Nilai</center></td>
			 <td bgcolor="#FFCC33"><center>Aksi</center></td>
		  </tr>
		  {foreach name=test item="list" from=$T_NILAI}
			<tr>
			 <td>{$smarty.foreach.test.iteration}</td>
             <td>{$list.KD_MATA_KULIAH}</td>
			 <td>{$list.NM_MATA_KULIAH}</td>
             <td>{$list.KREDIT_SEMESTER}</td>
             <td>{$list.SMT}</td>
			 <td style="text-align: center">{$list.NILAI_HURUF}</td>
			 <td height="0" align="center"><input name="Button" type="button" class="button" onClick="location.href='#aktivitas-kn!konversi-nilai.php?action=viewproses&id_pmk={$list.ID_PENGAMBILAN_MK}&id_prodi={$list.ID_PROGRAM_STUDI}&nim={$nim}';disableButtons()" onMouseOver="window.status='Click untuk Insert';return true" onMouseOut="window.status='Konversi Nilai'" value="KONVERSI" /> </td> 
			</tr>
		  {/foreach}
</table>
</div>


<div>

<div class="panel" id="panel2" style="display: {$disp2} ">
<p> </p>
<form action="konversi-nilai.php" method="post" name="id_konversi_nilai" id="id_konversi_nilai">
<input type="hidden" name="action" value="proses" >
<input type="hidden" name="id_pengambilan_mk" value="{$datamk.ID_PENGAMBILAN_MK}" >

	<table>
        <tr>
        	<td>Kode MA</td>
            <td>{$datamk.KD_MATA_KULIAH}</td>
        </tr>
		<tr>
        	<td>Nama MA</td>
            <td>{$datamk.NM_MATA_KULIAH}</td>
        </tr>
		<tr>
        	<td>SKS</td>
            <td>{$datamk.KREDIT_SEMESTER}</td>
        </tr>
		<tr>
        	<td>Nilai</td>
            <td>{$datamk.NILAI_HURUF}</td>
        </tr>
		<tr>
        	<td>Semester</td>
            <td>{$datamk.SMT}</td>
        </tr>
        <tr>
        	<td>Konversi ke MA</td>
            <td>
            	<select name="kd_konversi" class="required">
                	<option value=""> ---------- </option>
                    {foreach $kd_konversi as $data}
                    	<option value="{$data.ID_KURIKULUM_MK}">{$data.TAHUN} - {$data.KD_MATA_KULIAH} - {$data.NM_MATA_KULIAH} - {$data.KREDIT_SEMESTER}</option>
                    {/foreach}
                </select><br />
				* Mata ajar yang ditawarkan prodi
            </td>
        </tr>

        <tr>
        	<td>Nilai Huruf Konversi</td>
            <td>
			   
            	<input type="hidden" name="nilai" maxlength="2" size="2" value="{$datamk.NILAI_HURUF}" />
				
				{$datamk.NILAI_HURUF}
            </td>
        </tr>
        <tr>
        	<td colspan="2" style="text-align:center">
            	<input type="hidden" value="{$nim}" name="nim" />
            	<input type="submit" name="Submit" value="Proses">
            </td>
        </tr>
    </table>
</form>	
</div>


	
{literal}
    <script>
			$("#id_konversi_nilai").validate();
    </script>
{/literal}
    