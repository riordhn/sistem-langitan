{literal}
<link href="js/jquery.dataTables.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script language="javascript" type="text/javascript" src="js/datetimepicker.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('table.display').dataTable({
		"sDom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>',
		"sPaginationType": "full_numbers",
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Semua"]]
	});
});

function hapus(hapus_jadwal){
	$.ajax({
	type: "POST",
	url: "skripsi-thesis-psikologi.php",
	data: "action=del&hapus="+hapus_jadwal,
	cache: false,
	success: function(data){
		if (data == ' ')
		{
			window.location.reload(true);
		}
		else
		{
			alert('gagal hapus');
		}
	}
	});
}

function status_penguji(ganti_status){
	$.ajax({
	type: "POST",
	url: "skripsi-thesis-psikologi.php",
	data: "action=status_tim&tim="+ganti_status+"&sts="+$("#sts"+ganti_status).val(),
	cache: false,
	success: function(data){
		if (data == ' ')
		{
			window.location.reload(true);
		}
		else
		{
			alert('gagal update');
		}
	}
	});
}

function periode(periode){
	$.ajax({
	type: "POST",
	url: "skripsi-thesis-psikologi.php",
	data: "action=periode&ujian_mk="+periode+"&periode="+$("#periode"+periode).val(),
	cache: false,
	success: function(data){
		if (data == ' ')
		{

		}
		else
		{
			alert('gagal update');
		}
	}
	});
}
</script>
{/literal}

<div class="center_title_bar">JADWAL TA/SKRIPSI/THESIS/DESERTASI</div>
<div id="tabs">
	<div id="tab1" {if $smarty.get.action == 'penjadwalan' || $smarty.get.action == 'updateview' || $smarty.get.action == 'penguji' || $smarty.get.action == 'cari_penguji' || $smarty.get.action == 'ruang' || $smarty.get.action == 'tim'}class="tab"{else}class="tab_sel"{/if} align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" {if $smarty.get.action == 'penjadwalan'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Penjadwalan</div>
	<div id="tab3" {if $smarty.get.action == 'updateview'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Update</div>
	<div id="tab4" {if $smarty.get.action == 'penguji' || $smarty.get.action == 'cari_penguji'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('4');">Penguji</div>
	<div id="tab5" {if $smarty.get.action == 'ruang'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('5');">Cek Ruang</div>
	<div id="tab6" {if $smarty.get.action == 'tim'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('6');">Cek Penguji</div>
</div>
<div class="panel" id="panel1" style="display: {$disp1} ">
<p>
<form name="cetak_periode" method="get" action="proses/jadwal-skripsi-thesis-cetak-psi.php" target="_blank">
<input type="button" name="cetak" value="Cetak Next Jadwal TA/SKRIPSI/THESIS/DESERTASI" onClick="window.open('proses/jadwal-skripsi-thesis-cetak-psikologi.php?cetak=71&smt={$SMT}','baru');">
&nbsp;<input type="button" name="cetak_all" value="Cetak All Jadwal TA/SKRIPSI/THESIS/DESERTASI" onClick="window.open('proses/jadwal-skripsi-thesis-cetak-psi.php?cetak=71&smt={$SMT}','baru');">
&nbsp;Periode:
<input type="hidden" name="cetak" value="71">
<input type="hidden" name="smt" value="{$SMT}">
<select name="periode" onchange='this.form.submit()'>
	<option value=""></option>
	<option value="Periode I">Periode I</option>
	<option value="Periode II">Periode II</option>
	<option value="Periode III">Periode III</option>
	<option value="Periode IV">Periode IV</option>
</select>
</form>
</p>
	<table id="" class="display" cellspacing="0" width="100%">
		<thead>
           <tr>
             <th>NIM</th>
             <th>Nama Mahasiswa</td>
             <th>Prodi</th>
             <th>Peminatan</th>
             <th>Jenis</th>
             <th>Periode</th>
             <th>Tanggal</th>
             <th>Jam</th>
			 <th>Ruang</th>
			 <th>Judul</th>
			 <th>Tim Penguji</th>
             <th>Aksi</th>
             <th>Status</th>
           </tr>
		</thead>
		<tbody>
           {foreach item="list" from=$T_MK}
		   {if $list.TGL_UJIAN == '' || $list.NM_RUANGAN == '' || $list.JAM|strlen < 13 || $list.JML < 3}
		   <tr bgcolor="#ff828e">
		   {elseif $list.TIM == '<li><font color="black"> , </font>'}
		   <tr bgcolor="#f2f3a2">
		   {else}
		   <tr>
		   {/if}
             <td>{$list.NIM_MHS}</td>
             <td>{$list.NM_PENGGUNA}</td>
             <td>{$list.PRODI}</td>
			 {if $list.NM_PRODI_MINAT == ''}
			 <td bgcolor="#ff828e">
			 {else}
             <td>
			 {/if}
			 {$list.NM_PRODI_MINAT}</td>
			 <td>{$list.NM_UJIAN_MK}</td>
			 {if $list.KETERANGAN == ''}
			 <td bgcolor="#ff828e">
			 {else}
			 <td>
			 {/if}
				<select id="periode{$list.ID_UJIAN_MK}" onchange="periode({$list.ID_UJIAN_MK});">
					<option value="" {if $list.KETERANGAN == ""}selected="selected"{/if}></option>
					<option value="Periode I" {if $list.KETERANGAN == "Periode I"}selected="selected"{/if}>Periode I</option>
					<option value="Periode II" {if $list.KETERANGAN == "Periode II"}selected="selected"{/if}>Periode II</option>
					<option value="Periode III" {if $list.KETERANGAN == "Periode III"}selected="selected"{/if}>Periode III</option>
					<option value="Periode IV" {if $list.KETERANGAN == "Periode IV"}selected="selected"{/if}>Periode IV</option>
				</select>
			 </td>
			 <td><center>{$list.TGL_UJIAN}</center></td>
			 <td><center>{$list.JAM}</center></td>
			 <td><center>{$list.NM_RUANGAN}</center></td>
			 <td>{$list.JUDUL}</td>
			 <td>{$list.TIM}</td>
			 <td>
				<center>
				<a href="skripsi-thesis-psikologi.php?action=updateview&ujian={$list.ID_UJIAN_MK}&jad={$list.ID_JADWAL_UJIAN_MK}" alt="Update jadwal" title="Update jadwal">Update</a><br/>
				<a href="skripsi-thesis-psikologi.php" name="hapus_jadwal" id="hapus_jadwal{$list.ID_UJIAN_MK}" alt="Hapus jadwal" title="Hapus jadwal" onclick="javascript:if(confirm('Anda yakin untuk menghapus jadwal?'))hapus({$list.ID_UJIAN_MK});">Hapus</a><br/>
				<a href="skripsi-thesis-psikologi.php?action=cari_penguji&ujian={$list.ID_UJIAN_MK}&jad={$list.ID_JADWAL_UJIAN_MK}" alt="Ploting penguji" title="Ploting penguji">Penguji</a><br/>
				</center>
			</td>
			{if $list.JML < 3}</td>
				{if $list.PENGUJI >= 3}
			<td><center><a href="skripsi-thesis-psikologi.php?action=publish&jad={$list.ID_JADWAL_UJIAN_MK}" alt="Publish" title="Publish">Unpublished</a></center></td>
				{else}
			<td><center>Unpublished</center></td>
				{/if}
			{else}
			<td><center>Published</center></td>
			{/if}
           </tr>
		   {foreachelse}
        <tr><td colspan="11"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
		<tfoot>
           <tr>
             <th>NIM</th>
             <th>Nama Mahasiswa</td>
             <th>Prodi</th>
             <th>Peminatan</th>
             <th>Jenis</th>
             <th>Periode</th>
             <th>Tanggal</th>
             <th>Jam</th>
			 <th>Ruang</th>
			 <th>Judul</th>
			 <th>Tim Penguji</th>
             <th>Aksi</th>
             <th>Status</th>
           </tr>
		</tfoot>
   	</table>
<p><br>&nbsp;</p>
</div>

<div class="panel" id="panel2" style="display: {$disp2} ">
<p><br/><input type="button" name="import" value="Impor Draft TA/SKRIPSI/THESIS/DESERTASI" onClick="window.open('proses/jadwal-skripsi-thesis-draft-psikologi.php?fak=11&smt={$SMT}','baru');"></p>
	<table id="" class="display" cellspacing="0" width="100%">
		<thead>
           <tr>
             <th>NIM</th>
             <th>Nama Mahasiswa</td>
             <th>Prodi</th>
             <th>Peminatan</th>
			 <th>Judul</th>
			 <th>Progres</th>
             <th>Aksi</th>
           </tr>
		</thead>
		<tbody>
           {foreach item="list" from=$TAWAR}
		   <tr>
             <td>{$list.NIM_MHS}</td>
             <td>{$list.NM_PENGGUNA}</td>
             <td>{$list.PRODI}</td>
             <td>{$list.NM_PRODI_MINAT}</td>
             <td>{$list.JUDUL}</td>
             <td style="text-align:right;">{$list.PROGRESS}&nbsp;&nbsp;&nbsp;</td>
			 {if $list.PEMBIMBING == null}
			 <td><center><i>Pembimbing belum diset</i></center></td>
			 {else}
			 <td><center><a href="skripsi-thesis-psikologi.php?action=add&kls={$list.ID_KELAS_MK}&mhs={$list.ID_MHS}&nm={$list.NM_UJIAN}&jad={$list.ID_JADWAL_KELAS}">Jadwalkan</a></center></td>
			 {/if}
           </tr>
		   {foreachelse}
        <tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
		<tfoot>
           <tr>
             <th>NIM</th>
             <th>Nama Mahasiswa</td>
             <th>Prodi</th>
             <th>Peminatan</th>
			 <th>Judul</th>
			 <th>Progres</th>
             <th>Aksi</th>
           </tr>
		</tfoot>
   	</table>
</p><br>&nbsp;</p>
</div>

<div class="panel" id="panel3" style="display: {$disp3}">
{if $smarty.get.action == '' || $smarty.get.jad == ''}
<p> </p>
{else}
<p> </p>
<form action="skripsi-thesis-psikologi.php" method="post">
<input type="hidden" name="action" value="update">
<table width="100%" class="tablesorter" cellspacing="0" cellpadding="0">
  <tr><th colspan="3"><strong> Update Jadwal TA/SKRIPSI/THESIS/DESERTASI </strong></th></tr>
	{foreach item="ubah" from=$UMK}
	<input type="hidden" name="ujian" value="{$ubah.ID_UJIAN_MK}" />
	<input type="hidden" name="jad" value="{$ubah.ID_JADWAL_UJIAN_MK}" />
	<tr>
	  <td>NIM</td>
	  <td>:</td>
	  <td>{$ubah.NIM_MHS}</td>
	</tr>
	<tr>
	  <td>Nama Mahasiswa</td>
	  <td>:</td>
	  <td>{$ubah.NM_PENGGUNA}</td>
	</tr>
	<tr>
	  <td>Prodi</td>
	  <td>:</td>
	  <td>{$ubah.PRODI} (<font color="blue">{$ubah.NM_PRODI_MINAT}</font>)</td>
	</tr>
	<tr>
	  <td>Jenis</td>
	  <td>:</td>
	  <td>{$ubah.NM_UJIAN_MK}</td>
	</tr>
	<tr>
	  <td>Judul</td>
	  <td>:</td>
	  <td>{$ubah.JUDUL}</td>
	</tr>
	<tr>
	  <td>Tanggal Ujian</td>
	  <td>:</td>
	  <td><input type="text" name="tgl_ujian" id="tgl_ujian" maxlength="25" value="{$ubah.TGL_UJIAN}" onClick="NewCssCal('tgl_ujian','ddmmmyyyy');" /></td>
	</tr>
	<tr>
	  <td>Jam Mulai</td>
	  <td>:</td>
	  <td><input type="text" name="jam_mulai" value="{$ubah.JAM_MULAI}" maxlength="5" />&nbsp;<font color="grey"><em>Format jam: 08:00</em></font></td>
	</tr>
	<tr>
	  <td>Jam Selesai</td>
	  <td>:</td>
	  <td><input type="text" name="jam_selesai" value="{$ubah.JAM_SELESAI}" maxlength="5" />&nbsp;<font color="grey"><em>Format jam: 14:00</em></font></td>
	</tr>
	<tr>
	  <td>Ruangan</td>
	  <td>:</td>
	  <td>
		<select name="ruangan" id="ruangan">
			{foreach item="ruangan" from=$T_RUANG}
			{html_options values=$ruangan.ID_RUANGAN output=$ruangan.RUANG selected=$ubah.ID_RUANGAN}
			{/foreach}
		  </select>
	  </td>
	</tr>
	<tr>
		<td colspan="2"></td>
		<td><input type="button" name="kembali" value="Batal" onClick="window.location.href='#ujian-thesis!skripsi-thesis-psikologi.php'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="Simpan1" value="Simpan"></td>
	</tr>
	{/foreach}
</table>
</form>
<table width="100%" class="tablesorter" cellspacing="0" cellpadding="0">
	<tr>
		<th><strong> Tim Penguji Ujian </strong></th>
		<th><strong> Status </strong></th>
	</tr>
	{foreach item="dsn" from=$DOSEN}
	<tr>
	  <td>{$dsn.PENGUJI}</td>
	  <td>{$dsn.STATUS_PENGUJI}</td>
	</tr>
	{foreachelse}
    <tr>
	<td colspan="2"><em>Data tidak ditemukan</em></td></tr>
    {/foreach}
	<tr>
	  <td colspan="2"><center><input type="button" name="add" value="Tambah/Edit Penguji" onClick="window.location.href='#ujian-thesis!skripsi-thesis-psikologi.php?action=cari_penguji&ujian={$ubah.ID_UJIAN_MK}&jad={$ubah.ID_JADWAL_UJIAN_MK}'"></center></td>
	</tr>
</table>
{/if}
</div>

<div class="panel" id="panel4" style="display: {$disp4}">
{if $smarty.get.action == '' || $smarty.get.jad == ''}
<p> </p>
{else}
<p> </p>
<table width="100%" class="tablesorter" cellspacing="0" cellpadding="0">
  <tr><th colspan="3"><strong> Penguji TA/SKRIPSI/THESIS/DESERTASI </strong></th></tr>
	{foreach item="ubah" from=$UMK}
	<tr>
	  <td>NIM / Nama</td>
	  <td>:</td>
	  <td>{$ubah.NIM_MHS} / {$ubah.NM_PENGGUNA}</td>
	</tr>
	<tr>
	  <td>Prodi</td>
	  <td>:</td>
	  <td>{$ubah.PRODI} (<font color="blue">{$ubah.NM_PRODI_MINAT}</font>)</td>
	</tr>
	<tr>
	  <td>Jenis</td>
	  <td>:</td>
	  <td>{$ubah.NM_UJIAN_MK}</td>
	</tr>
	<tr>
	  <td>Judul</td>
	  <td>:</td>
	  <td>{$ubah.JUDUL}</td>
	</tr>
	<tr>
	  <td>Tempat & Waktu Ujian</td>
	  <td>:</td>
	  <td>{$ruangan.RUANG}; tanggal: {$ubah.TGL_UJIAN}, pukul: {$ubah.JAM_MULAI} - {$ubah.JAM_SELESAI}</td>
	</tr>
	{/foreach}
</table>
<table width="100%" class="tablesorter" cellspacing="0" cellpadding="0">
	<tr>
		<th><strong> Tim Penguji Ujian </strong></th>
		<th><strong> Status </strong></th>
		<th><strong> Aksi </strong></th>
	</tr>
	{foreach item="dsn" from=$DOSEN}
	<tr>
	  <td>{$dsn.PENGUJI}</td>
	  <td>
		<select id="sts{$dsn.ID_TIM_PENGAWAS_UJIAN}" onchange="status_penguji({$dsn.ID_TIM_PENGAWAS_UJIAN});">
			<option value="1" {if $dsn.STATUS == "1"}selected="selected"{/if}>KETUA</option>
			<option value="2" {if $dsn.STATUS == "2"}selected="selected"{/if}>SEKRETARIS</option>
			<option value="0" {if $dsn.STATUS == "0"}selected="selected"{/if}>PEMBIMBING</option>
		</select>
	  </td>
	  <td><center><input type="button" name="del" value="Hapus" onClick="window.location.href='#ujian-thesis!skripsi-thesis-psikologi.php?action=del_tim&tim={$dsn.ID_TIM_PENGAWAS_UJIAN}&ujian={$smarty.get.ujian}&jad={$smarty.get.jad}'"></center></td>
	</tr>
	{foreachelse}
    <tr>
	<td colspan="3"><em>Data tidak ditemukan</em></td></tr>
    {/foreach}
</table>
<form action="skripsi-thesis-psikologi.php?action=cari_penguji&ujian={$smarty.get.ujian}&jad={$smarty.get.jad}" method="post" id="cari1">
<table width="100%" class="tablesorter" cellspacing="0" cellpadding="0">
<tr><th><strong> Tambah Tim Penguji Ujian </strong></th></tr>
	<tr>
		<td>Nama Penguji: <input name="namadosen" type="text" style="width:300px;" /> <input name="getdosen" type="hidden" value="1" /> <input type="submit" name="cari" value="Cari"></td>
	</tr>
</table>
</form>
<table width="100%" class="tablesorter" cellspacing="0" cellpadding="0">
	<tr>
	 <th>No</th>
	 <th>Photo</th>
	 <th>NIP/NIK</th>
	 <th>Nama</th>
	 <th>Prodi</th>
	 <th>Aksi</th>
	</tr>
	<tr>
	{foreach name=test item="list" from=$CARI_DOSEN}
	 <td>{$smarty.foreach.test.iteration}</td>
	 <td><img src="{$list.FOTO_PENGGUNA}/{$list.NIP_DOSEN}.JPG" width="160px"/></td>
	 <td>{$list.NIP_DOSEN}</td>
	 <td>{$list.NM_PENGGUNA}</td>
	 <td>{$list.NM_PROGRAM_STUDI}</td>
	 <td>
		<a href="skripsi-thesis-psikologi.php?action=add_tim&pgg={$list.ID_PENGGUNA}&ujian={$smarty.get.ujian}&jad={$smarty.get.jad}&sts=1">KETUA</a><br />
		<a href="skripsi-thesis-psikologi.php?action=add_tim&pgg={$list.ID_PENGGUNA}&ujian={$smarty.get.ujian}&jad={$smarty.get.jad}&sts=2">SEKRETARIS</a><br />
		<a href="skripsi-thesis-psikologi.php?action=add_tim&pgg={$list.ID_PENGGUNA}&ujian={$smarty.get.ujian}&jad={$smarty.get.jad}&sts=0">PEMBIMBING</a>
	</td>
	</tr>
	{/foreach}
</table>
{/if}
</div>

<div class="panel" id="panel5" style="display: {$disp5}">
<p> </p>
<form action="skripsi-thesis-psikologi.php?action=ruang" method="post">
<input type="hidden" name="action" value="ruang">
Pilih tanggal Ujian :
<select name="tgl">
	{foreach item="tgl" from=$T_TANGGAL}
	{html_options values=$tgl.TGL output=$tgl.TGL selected=$TGLGET}
	{/foreach}
</select>
&nbsp;<input type="submit" name="submit" value="Preview">
&nbsp;&nbsp;&nbsp;<input type=button name="cetak" value="Cetak" onclick="window.open('proses/plot-ruang-ujian-psikologi-cetak.php?tgl={$TGLGET}&id_smt={$SMT}','baru2');">
</form>
<br/>
<table class="tablesorter" cellspacing="1" cellpadding="0" border="0" width="1000">
	<thead>
	   <tr>
			<th rowspan="2" style="vertical-align: middle;"><center>Ruang</center></th>
			<th colspan="12"><center>Jam Mulai Ujian</center></th>
	   </tr>
	   <tr>
			<th width="50"><center>08</center></th>
			<th width="50"><center>09</center></th>
			<th width="50"><center>10</center></th>
			<th width="50"><center>11</center></th>
			<th width="50"><center>12</center></th>
			<th width="50"><center>13</center></th>
			<th width="50"><center>14</center></th>
			<th width="50"><center>15</center></th>
			<th width="50"><center>16</center></th>
			<th width="50"><center>17</center></th>
			<th width="50"><center>18</center></th>
			<th width="50"><center>19</center></th>
	   </tr>
	</thead>
	<tbody>
		  {foreach item="list" from=$RUANG}
		  <tr>
			<td><center>{$list.NM_RUANGAN}</center></td>
			{if $list.JAM0|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM0}</td>
			{else}
			<td>{$list.JAM0}</td>
			{/if}
			{if $list.JAM1|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM1}</td>
			{else}
			<td>{$list.JAM1}</td>
			{/if}
			{if $list.JAM2|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM2}</td>
			{else}
			<td>{$list.JAM2}</td>
			{/if}
			{if $list.JAM3|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM3}</td>
			{else}
			<td>{$list.JAM3}</td>
			{/if}
			{if $list.JAM4|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM4}</td>
			{else}
			<td>{$list.JAM4}</td>
			{/if}
			{if $list.JAM5|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM5}</td>
			{else}
			<td>{$list.JAM5}</td>
			{/if}
			{if $list.JAM6|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM6}</td>
			{else}
			<td>{$list.JAM6}</td>
			{/if}
			{if $list.JAM7|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM7}</td>
			{else}
			<td>{$list.JAM7}</td>
			{/if}
			{if $list.JAM8|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM8}</td>
			{else}
			<td>{$list.JAM8}</td>
			{/if}
			{if $list.JAM9|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM9}</td>
			{else}
			<td>{$list.JAM9}</td>
			{/if}
			{if $list.JAM10|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM10}</td>
			{else}
			<td>{$list.JAM10}</td>
			{/if}
			{if $list.JAM11|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM11}</td>
			{else}
			<td>{$list.JAM11}</td>
			{/if}
		  </tr>
		  {foreachelse}
		  <tr><td colspan="13"><em>Data tidak ditemukan</em></td></tr>
		  {/foreach}
	</tbody>
</table>
</div>

<div class="panel" id="panel6" style="display: {$disp6}">
<p> </p>
<form action="skripsi-thesis-psikologi.php?action=tim" method="post">
<input type="hidden" name="action" value="tim">
Pilih tanggal Ujian :
<select name="tgl_ujian">
	{foreach item="tgl" from=$T_TANGGAL}
	{html_options values=$tgl.TGL output=$tgl.TGL selected=$TGLGET}
	{/foreach}
</select>
&nbsp;<input type="submit" name="submit" value="Preview">
&nbsp;&nbsp;&nbsp;<input type=button name="cetak" value="Cetak" onclick="window.open('proses/plot-penguji-ta-psikologi-cetak.php?tgl={$TGLGET}&id_smt={$SMT}','baru2');">
</form>
<br/>
<table class="tablesorter" cellspacing="1" cellpadding="0" border="0" width="1000">
	<thead>
	   <tr>
			<th rowspan="2" style="vertical-align: middle;"><center>Penguji</center></th>
			<th colspan="12"><center>Jam Mulai Ujian</center></th>
	   </tr>
	   <tr>
			<th width="50"><center>08</center></th>
			<th width="50"><center>09</center></th>
			<th width="50"><center>10</center></th>
			<th width="50"><center>11</center></th>
			<th width="50"><center>12</center></th>
			<th width="50"><center>13</center></th>
			<th width="50"><center>14</center></th>
			<th width="50"><center>15</center></th>
			<th width="50"><center>16</center></th>
			<th width="50"><center>17</center></th>
			<th width="50"><center>18</center></th>
			<th width="50"><center>19</center></th>
	   </tr>
	</thead>
	<tbody>
		  {foreach item="tim" from=$TIM_PENGAWAS}
		  <tr>
			<td><center>{$tim.NM_PENGGUNA}</center></td>
			{if $tim.JAM0|strstr:", "}
			<td bgcolor="#ff828e">{$tim.JAM0}</td>
			{else}
			<td>{$tim.JAM0}</td>
			{/if}
			{if $tim.JAM1|strstr:", "}
			<td bgcolor="#ff828e">{$tim.JAM1}</td>
			{else}
			<td>{$tim.JAM1}</td>
			{/if}
			{if $tim.JAM2|strstr:", "}
			<td bgcolor="#ff828e">{$tim.JAM2}</td>
			{else}
			<td>{$tim.JAM2}</td>
			{/if}
			{if $tim.JAM3|strstr:", "}
			<td bgcolor="#ff828e">{$tim.JAM3}</td>
			{else}
			<td>{$tim.JAM3}</td>
			{/if}
			{if $tim.JAM4|strstr:", "}
			<td bgcolor="#ff828e">{$tim.JAM4}</td>
			{else}
			<td>{$tim.JAM4}</td>
			{/if}
			{if $tim.JAM5|strstr:", "}
			<td bgcolor="#ff828e">{$tim.JAM5}</td>
			{else}
			<td>{$tim.JAM5}</td>
			{/if}
			{if $tim.JAM6|strstr:", "}
			<td bgcolor="#ff828e">{$tim.JAM6}</td>
			{else}
			<td>{$tim.JAM6}</td>
			{/if}
			{if $tim.JAM7|strstr:", "}
			<td bgcolor="#ff828e">{$tim.JAM7}</td>
			{else}
			<td>{$tim.JAM7}</td>
			{/if}
			{if $tim.JAM8|strstr:", "}
			<td bgcolor="#ff828e">{$tim.JAM8}</td>
			{else}
			<td>{$tim.JAM8}</td>
			{/if}
			{if $tim.JAM9|strstr:", "}
			<td bgcolor="#ff828e">{$tim.JAM9}</td>
			{else}
			<td>{$tim.JAM9}</td>
			{/if}
			{if $tim.JAM10|strstr:", "}
			<td bgcolor="#ff828e">{$tim.JAM10}</td>
			{else}
			<td>{$tim.JAM10}</td>
			{/if}
			{if $tim.JAM11|strstr:", "}
			<td bgcolor="#ff828e">{$tim.JAM11}</td>
			{else}
			<td>{$tim.JAM11}</td>
			{/if}
		  </tr>
		  {foreachelse}
		  <tr><td colspan="13"><em>Data tidak ditemukan</em></td></tr>
		  {/foreach}
	</tbody>
</table>
</div>