<div>
<div class="center_title_bar">Jadwal Ruang {foreach item="list" from=$NM_SMT}({$list.NM_SMT}){/foreach}</div>
	<div id="tab1" {if $smarty.get.action == 'view' || $smarty.get.action == 'smt' || $smarty.get.action == 'grup'}class="tab"{else}class="tab_sel"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('1');">Plot Ruang</div>
	<div id="tab2" {if $smarty.get.action == 'plot' || $smarty.get.action == 'smt' || $smarty.get.action == 'grup'}class="tab"{else}class="tab_sel"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Per Hari</div>
	<div id="tab3" {if $smarty.get.action == 'plot' || $smarty.get.action == 'view' || $smarty.get.action == 'grup'}class="tab"{else}class="tab_sel"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Per Semester</div>
	<div id="tab4" {if $smarty.get.action == 'plot' || $smarty.get.action == 'smt' || $smarty.get.action == 'view'}class="tab"{else}class="tab_sel"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('4');">Per Kel Smt</div>
</div>

<div class="panel" id="panel1" style="display: {$disp1}">
<p> </p>
<form action="jadwal-ruang-psikologi.php?action=plot" method="post">
<input type="hidden" name="action" value="plot">
Tahun Akademik :
<select name="smt">
	{foreach item="smt" from=$T_ST}
	{html_options values=$smt.ID_SEMESTER output=$smt.SMT selected=$SMT}
	{/foreach}
</select>
Hari :
<select name="hari">
	{foreach item="list" from=$T_HARI}
	{html_options values=$list.ID_JADWAL_HARI output=$list.NM_JADWAL_HARI selected=$HRGET}
	{/foreach}
</select>
&nbsp;<input type="submit" name="submit" value="Preview">
&nbsp;&nbsp;&nbsp;<input type=button name="cetak" value="Cetak" onclick="window.open('proses/plot-ruang-psikologi-cetak.php?id_hari={$HRGET}&id_smt={$SMT}','baru2');">
</form>
<br/>
<table class="tablesorter" cellspacing="1" cellpadding="0" border="0" width="1000">
	<thead>
	   <tr>
			<th rowspan="2" style="vertical-align: middle;"><center>Ruang</center></th>
			<th colspan="12"><center>Jam Mulai Kuliah</center></th>
	   </tr>
	   <tr>
			<th width="50"><center>08</center></th>
			<th width="50"><center>09</center></th>
			<th width="50"><center>10</center></th>
			<th width="50"><center>11</center></th>
			<th width="50"><center>12</center></th>
			<th width="50"><center>13</center></th>
			<th width="50"><center>14</center></th>
			<th width="50"><center>15</center></th>
			<th width="50"><center>16</center></th>
			<th width="50"><center>17</center></th>
			<th width="50"><center>18</center></th>
			<th width="50"><center>19</center></th>
	   </tr>
	</thead>
	<tbody>
		  {foreach item="list" from=$T_PLOTRUANG}
		  <tr>
			<td><center>{$list.NM_RUANGAN}</center></td>
			{if $list.JAM0|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM0}</td>
			{else}
			<td>{$list.JAM0}</td>
			{/if}
			{if $list.JAM1|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM1}</td>
			{else}
			<td>{$list.JAM1}</td>
			{/if}
			{if $list.JAM2|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM2}</td>
			{else}
			<td>{$list.JAM2}</td>
			{/if}
			{if $list.JAM3|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM3}</td>
			{else}
			<td>{$list.JAM3}</td>
			{/if}
			{if $list.JAM4|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM4}</td>
			{else}
			<td>{$list.JAM4}</td>
			{/if}
			{if $list.JAM5|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM5}</td>
			{else}
			<td>{$list.JAM5}</td>
			{/if}
			{if $list.JAM6|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM6}</td>
			{else}
			<td>{$list.JAM6}</td>
			{/if}
			{if $list.JAM7|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM7}</td>
			{else}
			<td>{$list.JAM7}</td>
			{/if}
			{if $list.JAM8|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM8}</td>
			{else}
			<td>{$list.JAM8}</td>
			{/if}
			{if $list.JAM9|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM9}</td>
			{else}
			<td>{$list.JAM9}</td>
			{/if}
			{if $list.JAM10|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM10}</td>
			{else}
			<td>{$list.JAM10}</td>
			{/if}
			{if $list.JAM11|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM11}</td>
			{else}
			<td>{$list.JAM11}</td>
			{/if}
		  </tr>
		  {foreachelse}
		  <tr><td colspan="13"><em>Data tidak ditemukan</em></td></tr>
		  {/foreach}
	</tbody>
</table>
</div>

<div class="panel" id="panel2" style="display: {$disp2}">
<p> </p>
<form action="jadwal-ruang-psikologi.php?action=view" method="post">
<input type="hidden" name="action" value="view">
<input type="hidden" name="smt" value="{$SMT}">
Hari :
<select name="hari0">
	{foreach item="list" from=$T_HARI}
	{html_options values=$list.ID_JADWAL_HARI output=$list.NM_JADWAL_HARI selected=$HRGET0}
	{/foreach}
</select>
&nbsp;<input type="submit" name="submit" value="Preview">
&nbsp;&nbsp;&nbsp;<input type=button name="cetak" value="Cetak" onclick="window.open('proses/jadwal-ruang-psikologi-cetak.php?id_hari={$HRGET0}','baru2');">
</form>
<br/>
<table class="tablesorter" cellspacing="1" cellpadding="0" border="0" width="1000">
	<thead>
	   <tr>
			<th rowspan="2" style="vertical-align: middle;"><center>Ruang</center></th>
			<th colspan="12"><center>Jam Mulai Kuliah</center></th>
	   </tr>
	   <tr>
			<th width="50"><center>08</center></th>
			<th width="50"><center>09</center></th>
			<th width="50"><center>10</center></th>
			<th width="50"><center>11</center></th>
			<th width="50"><center>12</center></th>
			<th width="50"><center>13</center></th>
			<th width="50"><center>14</center></th>
			<th width="50"><center>15</center></th>
			<th width="50"><center>16</center></th>
			<th width="50"><center>17</center></th>
			<th width="50"><center>18</center></th>
			<th width="50"><center>19</center></th>
	   </tr>
	</thead>
	<tbody>
		  {foreach item="list" from=$T_JADRUANG}
		  <tr>
			<td><center>{$list.NM_RUANGAN}</center></td>
			{if $list.JAM0|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM0}</td>
			{else}
			<td>{$list.JAM0}</td>
			{/if}
			{if $list.JAM1|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM1}</td>
			{else}
			<td>{$list.JAM1}</td>
			{/if}
			{if $list.JAM2|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM2}</td>
			{else}
			<td>{$list.JAM2}</td>
			{/if}
			{if $list.JAM3|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM3}</td>
			{else}
			<td>{$list.JAM3}</td>
			{/if}
			{if $list.JAM4|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM4}</td>
			{else}
			<td>{$list.JAM4}</td>
			{/if}
			{if $list.JAM5|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM5}</td>
			{else}
			<td>{$list.JAM5}</td>
			{/if}
			{if $list.JAM6|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM6}</td>
			{else}
			<td>{$list.JAM6}</td>
			{/if}
			{if $list.JAM7|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM7}</td>
			{else}
			<td>{$list.JAM7}</td>
			{/if}
			{if $list.JAM8|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM8}</td>
			{else}
			<td>{$list.JAM8}</td>
			{/if}
			{if $list.JAM9|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM9}</td>
			{else}
			<td>{$list.JAM9}</td>
			{/if}
			{if $list.JAM10|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM10}</td>
			{else}
			<td>{$list.JAM10}</td>
			{/if}
			{if $list.JAM11|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM11}</td>
			{else}
			<td>{$list.JAM11}</td>
			{/if}
		  </tr>
		  {foreachelse}
		  <tr><td colspan="13"><em>Data tidak ditemukan</em></td></tr>
		  {/foreach}
	</tbody>
</table>
</div>

<div class="panel" id="panel3" style="display: {$disp3}">
<p> </p>
<form action="jadwal-ruang-psikologi.php?action=smt" method="post">
<input type="hidden" name="action" value="smt">
<input type="hidden" name="smt" value="{$SMT}">
Hari :
<select name="hari1">
	<option value=""></option>
	{foreach item="list" from=$T_HARI}
	{html_options values=$list.ID_JADWAL_HARI output=$list.NM_JADWAL_HARI selected=$HRGET1}
	{/foreach}
</select>
&nbsp;
Semester :
<select name="smt2">
	<option value=""></option>
	{foreach item="list" from=$T_SMT}
	{html_options values=$list.TINGKAT_SEMESTER output=$list.NM_SMT selected=$SMTGET}
	{/foreach}
</select>
&nbsp;
Prodi : 
<select name="kdprodi">
	<option value=""></option>
	{foreach item="list" from=$T_PRODI}
	{html_options values=$list.ID_PROGRAM_STUDI output=$list.PRODI selected=$PRODIGET}
	{/foreach}
</select>
&nbsp;<input type="submit" name="Submit" value="Preview">
</form>
<br/>
<table class="tablesorter" cellspacing="1" cellpadding="0" border="0" width="1000">
	<thead>
	   <tr>
			<th rowspan="2" style="vertical-align: middle;"><center>Ruang</center></th>
			<th colspan="12"><center>Jam Mulai Kuliah</center></th>
	   </tr>
	   <tr>
			<th width="50"><center>08</center></th>
			<th width="50"><center>09</center></th>
			<th width="50"><center>10</center></th>
			<th width="50"><center>11</center></th>
			<th width="50"><center>12</center></th>
			<th width="50"><center>13</center></th>
			<th width="50"><center>14</center></th>
			<th width="50"><center>15</center></th>
			<th width="50"><center>16</center></th>
			<th width="50"><center>17</center></th>
			<th width="50"><center>18</center></th>
			<th width="50"><center>19</center></th>
	   </tr>
	</thead>
	<tbody>
		  {foreach item="list" from=$T_JADRUANG1}
		  <tr>
			<td><center>{$list.NM_RUANGAN}</center></td>
			{if $list.JAM0|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM0}</td>
			{else}
			<td>{$list.JAM0}</td>
			{/if}
			{if $list.JAM1|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM1}</td>
			{else}
			<td>{$list.JAM1}</td>
			{/if}
			{if $list.JAM2|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM2}</td>
			{else}
			<td>{$list.JAM2}</td>
			{/if}
			{if $list.JAM3|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM3}</td>
			{else}
			<td>{$list.JAM3}</td>
			{/if}
			{if $list.JAM4|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM4}</td>
			{else}
			<td>{$list.JAM4}</td>
			{/if}
			{if $list.JAM5|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM5}</td>
			{else}
			<td>{$list.JAM5}</td>
			{/if}
			{if $list.JAM6|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM6}</td>
			{else}
			<td>{$list.JAM6}</td>
			{/if}
			{if $list.JAM7|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM7}</td>
			{else}
			<td>{$list.JAM7}</td>
			{/if}
			{if $list.JAM8|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM8}</td>
			{else}
			<td>{$list.JAM8}</td>
			{/if}
			{if $list.JAM10|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM10}</td>
			{else}
			<td>{$list.JAM9}</td>
			{/if}
			{if $list.JAM10|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM10}</td>
			{else}
			<td>{$list.JAM10}</td>
			{/if}
			{if $list.JAM11|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM11}</td>
			{else}
			<td>{$list.JAM11}</td>
			{/if}
		  </tr>
		  {foreachelse}
		  <tr><td colspan="13"><em>Data tidak ditemukan</em></td></tr>
		  {/foreach}
	</tbody>
</table>
</div>

<div class="panel" id="panel4" style="display: {$disp4}">
<p> </p>
<form action="jadwal-ruang-psikologi.php?action=grup" method="post">
<input type="hidden" name="action" value="grup">
<input type="hidden" name="smt" value="{$SMT}">
<input type="hidden" name="smt2" value="{$SMT2}">
Hari :
<select name="hari1">
	<option value=""></option>
	{foreach item="list" from=$T_HARI}
	{html_options values=$list.ID_JADWAL_HARI output=$list.NM_JADWAL_HARI selected=$HRGET1}
	{/foreach}
</select>
&nbsp;
Semester :
<select name="grup">
	<option value=""></option>
	{foreach item="list" from=$NM_GRUP}
	{html_options values=$list.GRUP output=$list.GRUP selected=$GRUPGET}
	{/foreach}
</select>
&nbsp;
Prodi : 
<select name="kdprodi1">
	<option value=""></option>
	{foreach item="list" from=$T_PRODI}
	{html_options values=$list.ID_PROGRAM_STUDI output=$list.PRODI selected=$PRODIGET}
	{/foreach}
</select>
&nbsp;<input type="submit" name="Submit" value="Preview">
</form>
<br/>
<table class="tablesorter" cellspacing="1" cellpadding="0" border="0" width="1000">
	<thead>
	   <tr>
			<th rowspan="2" style="vertical-align: middle;"><center>Ruang</center></th>
			<th colspan="12"><center>Jam Mulai Kuliah</center></th>
	   </tr>
	   <tr>
			<th width="50"><center>08</center></th>
			<th width="50"><center>09</center></th>
			<th width="50"><center>10</center></th>
			<th width="50"><center>11</center></th>
			<th width="50"><center>12</center></th>
			<th width="50"><center>13</center></th>
			<th width="50"><center>14</center></th>
			<th width="50"><center>15</center></th>
			<th width="50"><center>16</center></th>
			<th width="50"><center>17</center></th>
			<th width="50"><center>18</center></th>
			<th width="50"><center>19</center></th>
	   </tr>
	</thead>
	<tbody>
		  {foreach item="list" from=$T_JADRUANG2}
		  <tr>
			<td><center>{$list.NM_RUANGAN}</center></td>
			{if $list.JAM0|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM0}</td>
			{else}
			<td>{$list.JAM0}</td>
			{/if}
			{if $list.JAM1|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM1}</td>
			{else}
			<td>{$list.JAM1}</td>
			{/if}
			{if $list.JAM2|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM2}</td>
			{else}
			<td>{$list.JAM2}</td>
			{/if}
			{if $list.JAM3|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM3}</td>
			{else}
			<td>{$list.JAM3}</td>
			{/if}
			{if $list.JAM4|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM4}</td>
			{else}
			<td>{$list.JAM4}</td>
			{/if}
			{if $list.JAM5|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM5}</td>
			{else}
			<td>{$list.JAM5}</td>
			{/if}
			{if $list.JAM6|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM6}</td>
			{else}
			<td>{$list.JAM6}</td>
			{/if}
			{if $list.JAM7|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM7}</td>
			{else}
			<td>{$list.JAM7}</td>
			{/if}
			{if $list.JAM8|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM8}</td>
			{else}
			<td>{$list.JAM8}</td>
			{/if}
			{if $list.JAM10|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM10}</td>
			{else}
			<td>{$list.JAM9}</td>
			{/if}
			{if $list.JAM10|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM10}</td>
			{else}
			<td>{$list.JAM10}</td>
			{/if}
			{if $list.JAM11|strstr:", "}
			<td bgcolor="#ff828e">{$list.JAM11}</td>
			{else}
			<td>{$list.JAM11}</td>
			{/if}
		  </tr>
		  {foreachelse}
		  <tr><td colspan="13"><em>Data tidak ditemukan</em></td></tr>
		  {/foreach}
	</tbody>
</table>
</div>