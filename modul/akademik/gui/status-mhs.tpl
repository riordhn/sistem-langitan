  
   <div class="center_title_bar">Status Mahasiswa </div>  
     <table width="80%" border="1">
       <tr>
         <td><table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td width="135">Prodi</td>
               <td width="10">:</td>
               <td width="239"><select name="kdprodi">
    		   <option value=''>-- PRODI --</option>
	 		   {foreach item="list" from=$T_PRODI}
    		   {html_options  values=$list.ID_PROGRAM_STUDI output=$list.PRODI}
	 		   {/foreach}
			   </select></td>
             </tr>
             <tr>
               <td>Tahun Akademik</td>
               <td>:</td>
               <td><select name="smt">
    		   <option value=''>-- PILIH THN AKD --</option>
	 		   {foreach item="smt" from=$T_ST}
    		   {html_options  values=$smt.ID_SEMESTER output=$smt.SMT}
	 		   {/foreach}
			   </select>               </td>
             </tr>
			 <tr>
               <td>NIM</td>
               <td>:</td>
               <td><input name="nim" type="text"></td>
             </tr>
           </table>
             <p>
               <input type="submit" name="Submit" value="Preview">
           </p></td>
       </tr> 
</table> 
 <br />
  <div id="tabs">
        <div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
        <div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Input</div>
		<div id="tab3" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Update</div>
    	</div>
        <div class="tab_bdr"></div>
<div class="panel" id="panel1" style="display: {$disp1} ">
        <table width="100%" border="1">
          <tr>
            <td>Perubahan Status Mahasiswa<br>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr class="left_menu">
                <td width="4%" align="center" valign="middle" bgcolor="#333333"><font color="#FFFFFF">No</font></td>
                <td width="13%" align="center" valign="middle" bgcolor="#333333"><font color="#FFFFFF">NIM</font></td>
                <td width="27%" align="center" valign="middle" bgcolor="#333333"><font color="#FFFFFF">Nama Mahasiswa</font></td>
                <td align="center" bgcolor="#333333"><font color="#FFFFFF">Status Admisi </font></td>
                <td align="center" bgcolor="#333333"><font color="#FFFFFF">Thn Akademik </font></td>
                <td width="15%" align="center" valign="middle" bgcolor="#333333"><font color="#FFFFFF">Aksi</font> </td>
              </tr>
              
              <tr>
                <td >2</td>
                <td >080611912</td>
                <td >Arifin Ilham</td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
                <td ><a href="jadwal-akademik.php?action=tampil&id_jsf={$jaf.ID_JSF}&id_smt={$jaf.ID_SEMESTER}" onclick="displayPanel('3')">Update</a> | <a href="jadwal-akademik.php?action=del&id_jsf={$jaf.ID_JSF}&id_smt={$jaf.ID_SEMESTER}">Input</a></td>
              </tr>
            </table>
            <br></td>
          </tr>
  </table>
</div>

<div class="panel" id="panel2" style="display: {$disp2}">
			<form action="jadwal-akademik.php" method="post" name="id_jad_akd1" id="id_jad_akd1">
		    <input type="hidden" name="action" value="add" >
			 <input type="hidden" name="smt1" value="{$idsmt}" >	
	      <table width="99%" border="1">
            <tr>
			 
              <td><table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td>Kegiatan</td>
                    <td>:</td>
					<td><select name="nm_kgt">
                      {foreach item="nm_kgt" from=$T_KGT}
    		   {html_options  values=$nm_kgt.ID_KEGIATAN output=$nm_kgt.NM_KEGIATAN}
	 		   {/foreach}
                    </select></td>
                  </tr>
                  <tr>
                    <td>Tanggal Mulai </td>
					
					 <td>:</td>
                    <td><input type="Text" name="calawal" value="Isikan Tanggal" id="calawal" maxlength="25" size="25">
					<input type="button" value="" style="background-image:url(includes/cal/cal.gif)
; background-repeat:no-repeat; background-position:center; border:none;" onClick="NewCal('calawal','ddmmmyyyy');" /></td>
                  </tr>
				  <tr>
                    <td>Tanggal Selesai </td>
                    <td>:</td>
                    <td> <input type="Text" name="calakhir" value="Isikan Tanggal" id="calakhir" maxlength="25" size="25">
					<input type="button" value="" style="background-image:url(includes/cal/cal.gif)
; background-repeat:no-repeat; background-position:center; border:none;" onClick="NewCal('calakhir','ddmmmyyyy');" /></td>
                  </tr>
            
                </table>
                  <p>
                    <input type="submit" name="Submit" value="Tambah">
                </p></td>
            </tr>
          </table>
</form>		    
	      <br>
        </div>
		
		<div class="panel" id="panel3" style="display: {$disp3}">
		<form action="jadwal-akademik.php" method="post" >
		 <input type="hidden" name="action" value="update" >		
	      <table width="99%" border="1">
            <tr>
			
		  
              <td>
			  <table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
				  {foreach item="datajsf" from=$T_JAF}
					{if $datajsf.ID_JSF==$id_jsf}
					<input type="hidden" name="id_jsf" value="{$datajsf.ID_JSF}" >
					<input type="hidden" name="id_smt" value="{$datajsf.ID_SEMESTER}" >
					
                    <td>Kegiatan</td>
                    <td>:</td>
                    <td>{$datajsf.NM_KEGIATAN}</td>
                  </tr>
                  <tr>
                    <td>Tanggal Mulai </td>
                    <td>&nbsp;</td>
                    <td><input type="text" name="calawal1" value="{$datajsf.TGL_MULAI_JSF}" id="calawal1" maxlength="25" size="25" />
                    <input name="button" type="button" style="background-image:url(includes/cal/cal.gif)
; background-repeat:no-repeat; background-position:center; border:none;" onclick="NewCal('calawal1','ddmmmyyyy');" value="" /></td>
                  </tr>
                  <tr>
                    <td>Tanggal Selesai </td>
                    <td>&nbsp;</td>
                    <td><input type="text" name="calakhir1" value="{$datajsf.TGL_SELESAI_JSF}" id="calakhir1" maxlength="25" size="25" />
                    <input name="button2" type="button" style="background-image:url(includes/cal/cal.gif)
; background-repeat:no-repeat; background-position:center; border:none;" onclick="NewCal('calakhir1','ddmmmyyyy');" value="" /></td>
                  </tr>
			

					
            
                </table>
				{/if}
				{/foreach}
                  <p>
                    <input type="submit" name="Submit" value="Update">
                </p></td>
            </tr>
          </table>
 </form>		    
	      <br>
        </div> 

    