<div class="center_title_bar">Pembayaran Mahasiswa</div>  
        
<form method="post" action="pembayaran-mhs.php">
    <table>
        <tr>
        	<td>Bayar</td>
            <td>
            <select name="bayar">
           	  <option value="NOT NULL" {if $bayar == 'NOT NULL'}selected="selected"{/if}>Sudah</option>
              <option value="NULL" {if $bayar == 'NULL'}selected="selected"{/if}>Belum</option>
            </select>
          </td>
            <td>Prodi</td>
            <td>
            <select name="prodi">
            	{foreach $prodi as $data}
                	<option value="{$data['ID_PROGRAM_STUDI']}" {if $id_prodi == $data['ID_PROGRAM_STUDI']}
                    selected="selected" {/if}>{$data['NM_JENJANG']} - {$data['NM_PROGRAM_STUDI']}</option>
                {/foreach}
            </select>
            </td>
            <td>Semester</td>
            <td>
            <select name="semester">
            	{foreach $semester as $data}
                	<option value="{$data['ID_SEMESTER']}" {if $id_semester == $data['ID_SEMESTER']}
                    selected="selected" {/if}>{$data['THN_AKADEMIK_SEMESTER']} - {$data['NM_SEMESTER']}</option>
                {/foreach}
            </select>
            </td>
            <td><input type="submit" value="Tampil"/></td>
        </tr>
    </table>
</form>
{if isset($data_pembayaran)}
	<form action="cetak-pembayaran-mhs.php" method="post" name="cetak">
    	<input type="hidden" name="prodi" value="{$id_prodi}" />
        <input type="hidden" name="semester" value="{$id_semester}" />
        <input type="hidden" name="bayar" value="{$bayar}" />
    </form>
    <br />
    <table width="100%">
        <tr>
            <th style="text-align: center;">No</th>
            <th style="text-align: center;">NIM</th>
            <th style="text-align: center;">Nama</th>
			<th style="text-align: center;">Jalur</th>
            <th style="text-align: center;">Besar Biaya</th>
	     <th style="text-align: center;">Keterangan</th>
        </tr>
        {$total = 0}
        {foreach $data_pembayaran as $data}
        <tr>
            <td>{$data@index+1}</td>
            <td>{$data['NIM_MHS']}</td>
            <td>{$data['NM_PENGGUNA']}</td>
			<td>{$data['NM_JALUR']}</td>
          <td style="text-align: right;">{$data['BESAR_BIAYA']|number_format:2:",":"."}</td> 
		<td>{$data['KETERANGAN']}</td>       
        </tr>
        {$total = $total + $data['BESAR_BIAYA']}
        {/foreach}
        <tr style="font-weight:bold">
        	<td colspan="4">Total</td>
            <td style="text-align: right;">{$total|number_format:2:",":"."}</td>
            <td></td>
        </tr>
    </table>
{/if}