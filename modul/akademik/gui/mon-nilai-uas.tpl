{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[0,0],[2,0]],
		}
	);
}
);
</script>
{/literal}
<div class="center_title_bar"> Monitoring Entry Nilai UAS</div>
<form action="mon-nilai-uas.php" method="post" name="id_monup" id="id_monup">
<input type="hidden" name="action" value="view" >
<p>
			   Tahun/Semester : 
               <select name="kdthnsmt">
    		   <option value=''>------</option>
	 		   {foreach item="list_smt" from=$T_THNSMT}
    		   {html_options values=$list_smt.ID_SEMESTER output=$list_smt.THN_SMT selected=$SMT}
	 		   {/foreach}
			   </select>
			   <input type="submit" name="View" value="View">
</p>
</form>	
<div id="tabs">
	<div id="tab1" class="tab_sel" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('1');">UAS Sdh Masuk</div>
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">UAS Blm Masuk</div>
</div>
<div class="panel" id="panel1" style="display: {$disp1}">
<p></p>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
		<th>No</th>
		<th>Kode</th>
		<th>Nama Mata Ajar</th>
		<th>SKS</th>
		<th>KLS</th>
		<th>PJMA</th>
		<th>PST</th>
		<th>UTS</th>
		<th>UAS</th>
	</tr>
	</thead>
	<tbody>
	{foreach name=test item="list" from=$NIL_MSK}
	<tr>
		<td><center>{$smarty.foreach.test.iteration}</center></td>
		<td>{$list.KD_MATA_KULIAH}</td>
		<td>{$list.NM_MATA_KULIAH}</td>
		<td><center>{$list.KREDIT_SEMESTER}</center></td>
		<td><center>{$list.NAMA_KELAS}</center></td>
		<td>{$list.PJMA}</td>
		<td><center>{$list.PST}</center></td>
		<td><center>{$list.TTLUTS}</center></td>
		<td><center>{$list.TTLUAS}</center></td>
	</tr>
	{foreachelse}
	<tr><td colspan="9"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>
{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable1").tablesorter(
		{
		sortList: [[0,0],[2,0]],
		}
	);
}
);
</script>
{/literal}
<div class="panel" id="panel2" style="display: {$disp2}">
<p></p>
<table id="myTable1" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
		<th>No</th>
		<th>Kode</th>
		<th>Nama Mata Ajar</th>
		<th>SKS</th>
		<th>KLS</th>
		<th>PJMA</th>
		<th>PST</th>
		<th>UTS</th>
		<th>UAS</th>
	</tr>
	</thead>
	<tbody>
	{foreach name=test item="list" from=$UAS_BLM_MSK}
	<tr>
		<td><center>{$smarty.foreach.test.iteration}</center></td>
		<td>{$list.KD_MATA_KULIAH}</td>
		<td>{$list.NM_MATA_KULIAH}</td>
		<td><center>{$list.KREDIT_SEMESTER}</center></td>
		<td><center>{$list.NAMA_KELAS}</center></td>
		<td>{$list.PJMA}</td>
		<td><center>{$list.PST}</center></td>
		<td><center>{$list.TTLUTS}</center></td>
		<td><center>{$list.TTLUAS}</center></td>
	</tr>
	{foreachelse}
	<tr><td colspan="9"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>
