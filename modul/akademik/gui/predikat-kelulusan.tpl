{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[0,0]]
		}
	); 
} 
);
</script>
{/literal}

<div class="center_title_bar">Master Predikat Kelulusan</div>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
		<th>Jenjang</th>
		<th>IPK Minimal</th>
		<th>IPK Maksimal</th>
		<th colspan="2">Predikat Kelulusan</th>
	</tr>
	</thead>
	<tbody>
	{foreach name=test item="list" from=$PREDIKAT}
	{if $list.KODE % 2 == 0}
	<tr bgcolor="lightgray">
	{else}
	<tr>
	{/if}
		<td><center>{$list.NM_JENJANG}</center></td>
		<td><center>{$list.IPK_MIN_MHS_PREDIKAT}</center></td>
		<td><center>{$list.IPK_MAX_MHS_PREDIKAT}</center></td>
		<td>{$list.NM_MHS_PREDIKAT}</td>
		<td>{$list.NM_MHS_PREDIKAT_EN}</td>
	</tr>
	{foreachelse}
	<tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>				
	