<div class="center_title_bar">KODE PRODI IJASAH</div>
{if isset($prodi)}
<table>
	<tr>
    	<th>No</th>
        <th>Program Studi</th>
        <th>Kode Ijasah Prodi</th>
		<th>Kode Ijasah Jenjang</th>
        <th>Aksi</th>
    </tr>
    {$no = 1}
    {foreach $prodi as $data}
    <tr>
    	<td>{$no++}</td>
        <td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
        <td style="text-align:center">{$data.KODE_IJASAH}</td>
		<td style="text-align:center">{$data.JENJANG_IJASAH}</td>
        <td><a href="wisuda-prodi.php?id={$data.ID_PROGRAM_STUDI}">Edit</a></td>
    </tr>
    {/foreach}
</table>
{else}
<form method="post" action="wisuda-prodi.php">
	<table>
    	<tr>
        	<td>Nama Prodi</td>
            <td>{$id[0]['NM_JENJANG']} - {$id[0]['NM_PROGRAM_STUDI']}</td>
        </tr>
        <tr>
        	<td>Kode Ijasah Prodi</td>
            <td><input type="text" name="kode" value="{$id[0]['KODE_IJASAH']}" /></td>
        </tr>
		<tr>
        	<td>Kode Ijasah Jenjang</td>
            <td><input type="text" name="jenjang_ijasah" value="{$id[0]['JENJANG_IJASAH']}" /></td>
        </tr>
        <tr>
        	<td colspan="2" style="text-align:center">
            	<input type="hidden" value="{$id[0]['ID_PROGRAM_STUDI']}" name="id_prodi" />
            	<input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
{/if}