{literal}
<script language="javascript" type="text/javascript" src="js/datetimepicker.js"></script>
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[0,0]]
		}
	); 
} 
);

$(document).ready(function() 
{ 
    $("#myTable1").tablesorter(
		{
		sortList: [[0,0]]
		}
	); 
} 
);
</script>
{/literal}
<div class="center_title_bar">Detail Presensi Mata Ajar </div>
	<div id="tabs">
	<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Materi-Dos</div>
	<div id="tab3" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Presensi Mhs</div>
	<div id="tab4" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('4');">Edit Materi-Dos</div>
</div>
<div class="panel" id="panel1" style="display: {$disp1} ">
<p> </p>
	<table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	{foreach item="dtkelas" from=$DT_KELAS}
	  <tr>
		<td>Kode Mata Ajar</td>
		<td>:</td>
		<td>{$dtkelas.KD_MATA_KULIAH}</td>
	  </tr>
	  <tr>
		<td>Nama Mata Ajar</td>
		<td>:</td>
		<td>{$dtkelas.NM_MATA_KULIAH}</td>
	  </tr>
	  <tr>
		<td>SKS</td>
		<td>:</td>
		<td>{$dtkelas.KREDIT_SEMESTER}</td>
	  </tr>
	  <tr>
		<td>Kelas</td>
		<td>:</td>
		<td>{$dtkelas.NAMA_KELAS}</td>
	  </tr>
	  <tr>
		<td>Ruang</td>
		<td>:</td>
		<td>{$dtkelas.NM_RUANGAN}</td>
	  </tr>
	 {/foreach}
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
	 <th><center>Kuliah ke</center></th>
	 <th><center>Tgl</center></th>
	 <th><center>Jam</center></th>
	 <th><center>Materi</center></th>
	 <th><center>Dosen</center></th>
	 <th><center>Hadir</center></th>
	 <th><center>Absen</center></th>
	 <th><center>Presensi</center></th>
	 <th><center>Aksi</center></th>
	</tr>
	</thead>
	<tbody>
		{foreach name=test item="presensi" from=$DATA_PRESENSI}
	<tr>
	 <td ><center>{$smarty.foreach.test.iteration}</center></td>
	 <td ><center><a href="entry-presensi.php?action=edit&id_presensi={$presensi.IDPRESENSI}&idkelas={$idkelas}&ed=1">{$presensi.TGL_PRESENSI_KELAS}</a></center></td>
	 <td ><center>{$presensi.JAM}</center></td>
	 <td >{$presensi.ISI_MATERI_MK}</td>
	 <td ><ol>{$presensi.NMDOS}</ol></td>
	 <td ><center>{$presensi.HADIR}</center></td>
	 <td ><center>{$presensi.ABSEN}</center></td>
	 {if $presensi.HADIR==0 && $presensi.ABSEN==0}
	 <td ><center><a href="entry-presensi.php?action=presensi&id_presensi={$presensi.IDPRESENSI}&idkelas={$idkelas}&id=1" onclick="displayPanel('3')">Entry</a></center></td>
	 {else}
	 <td ><center><a href="entry-presensi.php?action=presensi&id_presensi={$presensi.IDPRESENSI}&idkelas={$idkelas}&id=2" onclick="displayPanel('3')">Edit</a></center></td>
	 {/if}
	  <td ><center><a href="entry-presensi.php?action=hapus&id_presensi={$presensi.IDPRESENSI}&idkelas={$idkelas}&id_materi={$presensi.ID_MATERI_MK}">Hapus</a></center></td>
	</tr>
	 {foreachelse}
		<tr><td colspan="9"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
	</tbody>
	</table>
</div>

<div class="panel" id="panel2" style="display: {$disp2}">
<p></p>
{if $DT_KELAS[0]['TM'] <= $DT_KELAS[0]['JUMLAH_PERTEMUAN_KELAS_MK']}
<form action="entry-presensi.php" method="post" id="f1" >
<input type="hidden" name="action" value="add" >
<input type="hidden" name="idkelas" value={$idkelas} >
	<table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="20%">Tgl Kuliah</td>
                    <td width="1%">:</td>
					<td width="79%"><input type="Text" name="calawal" id="calawal" maxlength="25" size="25"  class="required" onClick="NewCssCal('calawal','ddmmmyyyy');"/></td>
                  </tr>
				  <tr>
                    <td>Jam Mulai Kuliah</td>
                    <td>:</td>
					<td><input type="text" name="jam_mulai" id="jam_mulai" size="7" class="required" /> Contoh: 07:00</td>
                  </tr>
				  <tr>
                    <td>Jam Selesai Kuliah</td>
                    <td>:</td>
					<td><input type="text" name="jam_selesai" id="jam_selesai" size="7" class="required" /> Contoh: 10:00</td>
                  </tr>
				  {foreach name=nomer item="dosen" from=$T_DOSEN}
				  <tr>
                    <td>Dosen Pengajar-{$smarty.foreach.nomer.iteration}</td>
                    <td>:</td>
					<td><input name="dosen{$smarty.foreach.nomer.iteration}" type="checkbox" value="{$dosen.ID_DOSEN}" />{$dosen.NAMA}</td>

                  </tr>
				  {/foreach}
				  <input type="hidden" name="counter1" value="{$smarty.foreach.nomer.iteration}" >
				  <tr>
                    <td>Materi</td>
                    <td>:</td>
					<td><textarea name="materi" cols="60" class="required"></textarea></td>
                  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td><input type="submit" name="simpan" value="Simpan"></td>
					</tr>
        </table>
</form>
{else}
Maaf, jumlah tatap muka tidak boleh lebih dari jumlah pertemuan kelas mata kuliah.
{/if}
</div>
{literal}

<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable1").tablesorter(
		{
		sortList: [[1,0]]
		}
	); 
} 
);

$(document).ready(function() 
{ 
    $("#myTable1").tablesorter(
		{
		sortList: [[1,0]]
		}
	); 
} 
);
</script>
{/literal}
<div class="panel" id="panel3" style="display: {$disp3}">
<p> </p>
<form action="entry-presensi.php" method="post" >
<input type="hidden" name="action" value="masuk" >
<input type="hidden" name="id_presensi" value={$id_presensi} >
<input type="hidden" name="idkelas" value={$idkelas} >
<table id="myTable1" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
			     <th><center>No</center></th>
				 <th><center>NIM</center></th>
				 <th><center>Nama</center></th>
				 <th><center>Status</center></th>
				 <th><center>Absen</center></th>
			  </tr>
	</thead>
	<tbody>
			  {foreach name=presen item="pst" from=$PESERTA}
			  {if $smarty.foreach.presen.iteration % 2 == 0}
			  <tr bgcolor="lightgray">
			  {else}
			  <tr>
			  {/if}
			     <td><center>{$smarty.foreach.presen.iteration}</center></td>
				 <td>{$pst.NIM_MHS}</td>
				 <td>{$pst.NM_PENGGUNA}</td>
				 <td><center>{$pst.STKRS}</center></td>
				 <td><center><input name="mhs{$smarty.foreach.presen.iteration}" type="checkbox" value="{$pst.ID_MHS}" {$pst.TANDA} /></center></td>
			   </tr>
			     {foreachelse}
        			<tr><td colspan="5"><em>Data tidak ditemukan</em></td></tr>
        			{/foreach}
				<input type="hidden" name="counter" value="{$smarty.foreach.presen.iteration}" >
	</tbody>
  </table>
		{if $stt==1}
		<input type="hidden" name="pilihan" value="1" >
		<tr>
		<td><div align="right"><input type="submit" name="proses" value="Proses"></div></td>
		</tr>
		{else}
		<input type="hidden" name="pilihan" value="2" >
		<tr>
		<td><div align="right"><input type="submit" name="proses" value="Update"></div></td>
		</tr>
		{/if}
</form>
</div>

<div class="panel" id="panel4" style="display: {$disp4}">
<p> </p>
<form action="entry-presensi.php" method="post" >
<input type="hidden" name="action" value="update" >
<input type="hidden" name="idkelas" value={$idkelas} >
<input type="hidden" name="id_presensi" value={$id_presensi} >
	<table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
				 {foreach item="list" from=$DT_EDIT}
                  <tr>
                    <td width="20%">Tgl Kuliah</td>
                    <td width="1%">:</td>
					<td width="79%"><input type="Text" name="calawal1" value="{$list.TGL_PRESENSI_KELAS}" id="calawal1"  maxlength="25" size="25" onClick="NewCssCal('calawal1','ddmmmyyyy');"></td>
					
                  </tr>
				  <tr>
                    <td>Jam Mulai Kuliah</td>
                    <td>:</td>
					<td><input type="text" name="jam_mulai" id="jam_mulai" value="{$list.WAKTU_MULAI}" > Contoh: 07:00</td>
                  </tr>
				  <tr>
                    <td>Jam Selesai Kuliah</td>
                    <td>:</td>
					<td><input type="text" name="jam_selesai" id="jam_selesai" value="{$list.WAKTU_SELESAI}"> Contoh: 10:00</td>
                  </tr>

				  <tr>
                    <td>Materi</td>
                    <td>:</td>
					<td><textarea name="materi" cols="60">{$list.ISI_MATERI_MK}</textarea></td>
                  </tr>
				  {/foreach}
				  {foreach name=nomer item="dosen" from=$DOS_MASUK}
				  <tr>
                    <td>Dosen Pengajar-{$smarty.foreach.nomer.iteration}</td>
                    <td>:</td>
					<td><input name="dosen{$smarty.foreach.nomer.iteration}" type="checkbox" value="{$dosen.ID_DOSEN}" {$dosen.TANDA} />{$dosen.NAMA}</td>

                  </tr>
				  {/foreach}
				  <input type="hidden" name="counter1" value="{$smarty.foreach.nomer.iteration}" >
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td><input type="submit" name="update" value="Update"></td>
					</tr>
        </table>

</form>
</div>
{*
{literal}
    <script>
	    $("#jam_mulai").timepicker({timeFormat: 'hh:mm'});
	    $("#jam_selesai").timepicker({timeFormat: 'hh:mm'});
            $("#calawal").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
			 $("#calawal1").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
            $('#f1').validate();
    </script>
{/literal}
*}
