<div class="center_title_bar">Rekapitulasi Mengajar per Mata Ajar</div>

<form action="rekap-ngajar-ma-psikologi.php" method="post" name="rekap_ngajar" id="rekap_ngajar">
		 <input type="hidden" name="action" value="rekap" >
<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>Periode Rekap :</td>
		<td>
			<input type="Text" name="calawal" value="" id="calawal" maxlength="25" size="25" onClick="NewCssCal('calawal','ddmmmyyyy');">
			&nbsp;Sampai Dgn :
			<input type="Text" name="calakhir" value="" id="calakhir" maxlength="25" size="25" onClick="NewCssCal('calakhir','ddmmmyyyy');">
		</td>
	</tr>
	<tr>
		<td>Program Studi :</td>
		<td>
			<select name="prodi">
				<option value='ALL'>-- ALL --</option>
				{foreach item="prd" from=$T_PRODI}
				{html_options  values=$prd.ID_PROGRAM_STUDI output=$prd.NM_PRODI}
				{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Tahun Akademik :</td>
		<td>
			<select name="smt">
				<option value=''>-- PILIH THN AKD --</option>
				{foreach item="smt" from=$T_ST}
				{html_options  values=$smt.ID_SEMESTER output=$smt.SMT selected=$SMT}
				{/foreach}
			</select>
			<input type="submit" name="cari1" value="Proses">
		</td>
	</tr>
</table>
</form>
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
<thead>
	<tr>
	 <th><center>No</center></th>
	 <th><center>Kode</center></th>
	 <th><center>Mata Ajar</center></th>
	 <th><center>Kelas</center></th>
	 <th><center>SKS MK</center></th>
	 <th><center>Dosen</center></th>
	 <th><center>Hadir</center></th>
	 <th><center>TTM</center></th>
	 <th><center>SKS Ajar</center></th>
	 </tr>
</thead>
<tbody>
	{foreach name=presen item="sp" from=$T_SPAN}
	<tr>
	 <td><center>{$smarty.foreach.presen.iteration}</center></td>
	 <td><center>{$sp.KD_MATA_KULIAH}</center></td>
	 <td>{$sp.NM_MATA_KULIAH}</td>
	 <td><center>{$sp.NAMA_KELAS}</center></td>
	 <td><center>{$sp.KREDIT_SEMESTER}</center></td>
	 {foreach name=presen1 item="rkp" from=$T_REKAP}
		{if $sp.ID_KELAS_MK==$rkp.ID_KELAS_MK}
			<td>{$rkp.DOSEN}</td>
			<td><center>{$rkp.MASUK}</center></td>
			<td><center>{$rkp.TOTAL}</center></td>
			<td><center>{math equation="((x / y) * z)" x=$rkp.MASUK y=$rkp.TOTAL z=$sp.KREDIT_SEMESTER format="%.2f"}</center></td>
			<tr></tr>
			<td colspan="5"></td>
		{/if}
	{/foreach}
	</tr>
	{/foreach}
</tbody>
</table>
