{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable2").tablesorter(
		{
		sortList: [[1,0]], widgets: ["zebra"]
		}
	); 
} 
);

$(document).ready(function() 
{ 
    $("#myTable1").tablesorter(
		{
		sortList: [[1,0],[4,0]], widgets: ["zebra"]
		}
	); 
} 
);

$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]], widgets: ["zebra"]
		}
	); 
} 
);
</script>
{/literal}

<div class="center_title_bar">PROSES KRS MAHASISWA BARU</div>
{*<p><center><b><font color="red">DALAM PROSES PERBAIKAN, MOHON JANGAN GUNAKAN FASILITAS INI UNTUK SEMENTARA. (Yudi Sulistya)</font></b></center></p>*}
<div id="tabs">
<div id="tab1" {if $smarty.get.action=='view'} class="tab" {else} class="tab_sel" {/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
<div id="tab2" {if $smarty.get.action=='view'} class="tab_sel" {else} class="tab" {/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">KRS-MABA</div>
</div>

<div class="panel" id="panel1" style="display: {$disp1}">
<p> </p>
<table id="myTable2" class="tablesorter" width="800px" border="0" cellspacing="0" cellpadding="0" border="0">
	<thead>
          <tr>
			 <th><center>No</center></th>
             <th><center>Prodi</center></th>
             <th><center>Total Mhs</center></th>
             <th><center>Total KRS</center></th>
			 <th colspan="2"><center>Aksi</center></th>
		  </tr>
	</thead>
	<tbody>
		  {foreach name=test item="list" from=$T_MABA}
			<tr>
			 <td><center>{$smarty.foreach.test.iteration}</center></td>
             <td>{$list.PRODI}</td>
             <td><center>{$list.MABA}</center></td>
             <td><center>{$list.KRS}</center></td>
		  {if $smtaktif == '' || $thnaktif == ''}
			 <td colspan="2"></td>
			</tr>
		  {else}
			 <td height="0" align="center"><center><input name="Button" type="button" class="button" onClick="javascript:if(confirm('PERHATIAN!!!\n\nSemua KRS MABA akan diHAPUS!\nProses penghapusan adalah PERMANEN.\n\nLANJUTKAN?'))location.href='#utility-krs_paket!krs_maba_psikologi.php?action=reset&prodi={$list.ID_PROGRAM_STUDI}&smt={$smtaktif}&thn={$thnaktif}';disableButtons()" onMouseOver="window.status='Click untuk Reset';return true" onMouseOut="window.status='Reset KRS_MABA'" value="RESET KRS MABA" /></center></td>
			 <td height="0" align="center"><center><input name="Button" type="button" class="button" onClick="location.href='#utility-krs_paket!krs_maba_psikologi.php?action=view&prodi={$list.ID_PROGRAM_STUDI}';disableButtons()" onMouseOver="window.status='Click untuk Insert';return true" onMouseOut="window.status='Insert KRS_MABA'" value="PROSES KRS MABA" /></center></td>
			</tr>
		  {/if}
		  {/foreach}
	</tbody>
</table>
</div>

<div class="panel" id="panel2" style="display: {$disp2} ">
<p> </p>
<form action="krs_maba_psikologi.php" method="post" >
<input type="hidden" name="action" value="proses" >
<input type="hidden" name="prodi" value="{$ID_PROGRAM_STUDI}">
				<p><b><center>PILIH MATA AJAR DAN KELAS</center></b></p>
				<p><b>*Jika MATA AJAR tdk TAMPIL, periksalah Penawaran KRS untuk Prodi tersebut
				<br>*Jika MATA AJAR masih tdk TAMPIL, periksalah posisi semester MATA AJAR (semester 1) via menu PRODI DAFTAR MA</b></p>
		<table id="myTable1" width="800px" class="tablesorter" cellspacing="0" cellpadding="0" border="0">
			<thead>
				<tr>
			     <th><center>No</center></th>
				 <th><center>Kode MA</center></th>
				 <th><center>Nama MA</center></th>
				 <th><center>SKS</center></th>
				 <th><center>Kelas MA</center></th>
				 <th><center>Prodi</center></th>
				 <th><center>Pilih</center></th>
				</tr>
			</thead>
			<tbody>
				{foreach name=test1 item="krs" from=$MA_PILIH}
				<tr>
				 <td><center>{$smarty.foreach.test1.iteration}</center></td>
				 <td>{$krs.KD_MATA_KULIAH}</td>
				 <td>{$krs.NM_MATA_KULIAH}</td>
				 <td><center>{$krs.KREDIT_SEMESTER}</center></td>
				 <td><center>{$krs.NAMA_KELAS}</center></td>
				 <td><center>{$krs.PRODI}</center></td>
				 <td><center><input name="mk{$smarty.foreach.test1.iteration}" type="checkbox" value="{$krs.ID_KELAS_MK}" {$krs.TANDA} /></center></td>
				</tr>
			    {foreachelse}
        		<tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
        		{/foreach}
				<input type="hidden" name="counter" value="{$smarty.foreach.test1.iteration}" >
			</tbody>
		</table>
		<p><center><b><br>DAFTAR MAHASISWA</b></center></p>
		<table id="myTable" width="800px" class="tablesorter" cellspacing="0" cellpadding="0" border="0">
			<thead>
				<tr>
			     <th><center>No</center></th>
				 <th><center>NIM</center></th>
				 <th><center>Nama Mhs</center></th>
				 <th><center>Prodi</center></th>
				 <th><center>Peminatan</center></th>
				 <th><center>Agama</center></th>
				 <th><center>Bayar</center></th>
				 <th><center>Pilih</center></th>
				</tr>
			</thead>
			<tbody>
				{foreach name=test3 item="mhs" from=$MHS_PILIH}
				<tr>
				 <td><center>{$smarty.foreach.test3.iteration}</center></td>
				 <td>{$mhs.NIM_MHS}</td>
				 <td>{$mhs.NM_PENGGUNA}</td>
				 <td><center>{$mhs.PRODI}</center></td>
				 <td>{$mhs.MINAT}</td>
				 <td>{$mhs.NM_AGAMA}</td>
				 <td><center>{$mhs.STATUSBAYAR}</center></td>
				{if $mhs.STATUSBAYAR=='BELUM'}
				 <td></td>
				{else}
				 <td><center><input name="mhs{$smarty.foreach.test3.iteration}" type="checkbox" value="{$mhs.ID_MHS}" {$mhs.TANDA} /></center></td>
				{/if}
				</tr>
			    {foreachelse}
        		<tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
        		{/foreach}
				<input type="hidden" name="counter1" value="{$smarty.foreach.test3.iteration}" >
			</tbody>
				<tr>
				<td colspan="6"></td><td colspan="2"><center><input type="submit" name="PROSES" value="PROSES"></center></td>
				</tr>
		</table>
</form>
</div>