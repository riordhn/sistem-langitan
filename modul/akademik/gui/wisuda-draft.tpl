<div class="center_title_bar">DRAFT IJASAH</div>
<form id="form1" name="form1" method="post" action="wisuda-draft.php">
<table>
<tr>
	<td>NIM</td>
	<td> : </td>
	<td><input type="text" name="nim" /></td>
	<td><input type="submit" value="Tampil" /></td>
</tr>
</table>
</form>
<h2><font color="#FF0000">{$pesan}</font></h2>
{if isset($draft)}
<form method="post" action="wisuda-draft.php" id="draft">
<table width="500">
  <tr>
    <th colspan="2" style="text-align:center">Biodata</th>
  </tr>
  <tr>
    <td><b>NIM</b></td>
    <td>{$draft[0]['NIM_MHS']}</td>
  </tr>
  <tr>
    <td><b>Nama Lengkap</b></td>
    <td><input name="nm_pengguna" type="text" class="required" value="{$draft[0]['NM_PENGGUNA']}" size="50" /> * Tanpa Gelar</td>
  </tr>
  <tr>
    <td><b>Gelar Depan</b></td>
    <td><input type="text"  name="gelar_depan" value="{$draft[0]['GELAR_DEPAN']}" />
    &nbsp; Contoh : Prof. Dr. </td>
  </tr>
  <tr>
    <td><b>Gelar Belakang</b></td>
    <td><input type="text" name="gelar_belakang" value="{$draft[0]['GELAR_BELAKANG']}"  />
    &nbsp; Contoh : M.Kes., Drh</td>
  </tr>
    <tr>
      <td><b>Tempat, Tanggal Lahir</b></td>
      <td><input type="text" name="ttl" id="ttl" value="{$draft[0]['LAHIR_IJAZAH']}" size="30" /><br /><br />
		<b><i>* Diisi jika, tempat dan tanggal lahir tidak sesuai dengan ijasah yang terakhir</i></b><br />
        <b><i>* Contoh : Semarang, 20 Nopember 2012</i></b>
        </td>
    </tr>
  <tr>
    <td><b>Program Studi</b></td>
    <td>{$draft[0]['NM_JENJANG']} - {$draft[0]['NM_PROGRAM_STUDI']}</td>
  </tr>
  <tr>
  	<td colspan="2" style="text-align:center">
        	<input type="hidden" value="{$draft[0]['NIM_MHS']}" name="nim" />
        	<input type="hidden" value="{$draft[0]['ID_MHS']}" name="id_mhs" />
        	<input type="submit" value="Update" />
            {$nim = $draft[0]['NIM_MHS']}
            <input type="button" value="Cetak Draft" onclick="window.open('proses/wisuda-draft-cetak.php?nim={$nim}');">
    </td>
  </tr>
</table>
</form>
{literal}
<script language="javascript">
	$('#draft').validate();
</script>
{/literal}
{/if}
