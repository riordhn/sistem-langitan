{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
            3: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}

<div class="center_title_bar">Master Nama Kelas</div>
<div id="tabs">
	<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Input</div>
	{*<div id="tab3" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Update</div>*}
</div>

<div class="panel" id="panel1" style="display:{$disp1} ">
<p> </p>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
		{*<th>ID KELAS </th>*}
		<th>NAMA KELAS </th>
		<th>KETERANGAN </th>
		<th class="noheader">Aksi</th>
	</tr>
	</thead>
	<tbody>
	{foreach name=list item="datakelas" from=$T_KELAS}
	{if $smarty.foreach.list.iteration % 2 == 0}
	<tr bgcolor="lightgray">
	{else}
	<tr>
	{/if}
		{*<td>{$datakelas.ID_NAMA_KELAS}</td>*}
		<td><center>{$datakelas.NAMA_KELAS}</center></td>
		<td><center>{$datakelas.KETERANGAN_NM_KLS}</center></td>
		<td>
			<center>
			<a href="nama_kelas.php?action=tampil&id_nama_kelas={$datakelas.ID_NAMA_KELAS}">Update</a>
			{*| <a href="nama_kelas.php?action=del&id_nama_kelas={$datakelas.ID_NAMA_KELAS}">Delete</a>*}
			</center>
		</td>
	</tr>
	{foreachelse}
	<tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>

<div class="panel" id="panel2" style="display:{$disp2} ">
<p><br><b>Input Master Nama Kelas</b></p>
<form action="nama_kelas.php" method="post" >
<input type="hidden" name="action" value="add" >
<table class="tb_frame" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>Nama Kelas</td>
		<td><center>:</center></td>
		<td><input type="text" name="nama_kelas" ></td>
	</tr>
	<tr>
		<td>Keterangan</td>
		<td><center>:</center></td>
		<td><input type="text" name="keterangan_nm_kls"></td>
	</tr>
	
</table>
<p><input type="button" name="kembali" value="Kembali" onClick="javascript: displayPanel('1');">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="tambah" value="Tambah"></p>
</form>
</div>

<div class="panel" id="panel3" style="display: {$disp3} ">
<p><br><b>Update master Nama Kelas</b></p>
<form action="nama_kelas.php" method="post" >
<input type="hidden" name="action" value="update" >
<table class="tb_frame" border="0" cellspacing="0" cellpadding="0">

	{foreach item="upkelas" from=$T_KELAS}

	<input type="hidden" name="id_nama_kelas" value="{$upkelas.ID_NAMA_KELAS}" >
	
	<tr>
		<td>Nama Kelas</td>
		<td><center>:</center></td>
		<td><input type="text" name="nama_kelas" class="required" value="{$upkelas.NAMA_KELAS}"></td>
	</tr>
	<tr>
		<td>Keterangan</td>
		<td><center>:</center></td>
		<td><input type="text" name="keterangan_nm_kls" class="required" value="{$upkelas.KETERANGAN_NM_KLS}"></td>
	</tr>
	

	{/foreach}
</table>
<p><input type="button" name="kembali" value="Kembali" onClick="window.location.href='#perkuliahan-namakelas!nama_kelas.php'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="tambah" value="Update"></p>
</form>
</div>

<script>$('form').validate();</script>