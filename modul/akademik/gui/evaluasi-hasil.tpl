<div class="center_title_bar">Hasil Evaluasi</div>
<form action="evaluasi-hasil.php" method="post" name="f2" id="f2">
	<table width="700">
    	<tr>
			<td>Jenjang</td>
			<td>
				<select id="jenjang" name="jenjang">
                    <option value="">Semua</option> 
                   {foreach $jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $smarty.request.jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}                    
                </select>
			</td>
			<td>Program Studi</td>
            <td>
                <select id="program_studi" name="program_studi">
                    <option value="">Semua</option>
					{foreach $program_studi as $data}
						<option value="{$data.ID_PROGRAM_STUDI}" {if $smarty.request.program_studi==$data.ID_PROGRAM_STUDI}selected="true"{/if}>{$data.NM_PROGRAM_STUDI}</option>
					{/foreach}
                </select>
            </td>
            <td>Semester</td>
            <td>
            	<select name="semester" class="required">
                	<option value="">Pilih Semester</option>
                	{foreach $semester as $data}
                	<option value="{$data.ID_SEMESTER}" {if $smarty.request.semester==$data.ID_SEMESTER or $data.ID_SEMESTER==$semester_aktif} selected="selected" {/if}>{$data.TAHUN_AJARAN} ({$data.NM_SEMESTER})</option>
					{/foreach}
            	</select>
            </td>
        </tr>
        <tr>
        	<td colspan="6" style="text-align:center">
				<input type="submit" value="Tampil" />
				<input type="hidden" value="{$fakultas}" name="fakultas" id="fakultas" />
			</td>
        </tr>
    </table>
</form>

{if isset($evaluasi)}
<form action="evaluasi-hasil.php" name="f1" id="f1" method="post">
<table style="font-size:11px">
	<tr>
		<th style="text-align:center">NO</th>
		<th style="text-align:center">NIM</th>
		<th style="text-align:center">NAMA</th>
        <th style="text-align:center">JENJANG</th>
		<th style="text-align:center">PRODI</th>
		<th style="text-align:center">STATUS TERKINI</th>
		<th style="text-align:center">EVALUASI BWS</th>
        <th style="text-align:center">EVALUASI AKADEMIK</th>
        <th style="text-align:center">EVALUASI ADMINISTRASI</th>
        <th style="text-align:center">HASIL PENETAPAN</th>
		<th style="text-align:center">KETERANGAN</th>
        
	</tr>
	{$no = 1}
	{foreach $evaluasi as $data}
	<tr {if $data.PENGAJUAN_EVALUASI == 1}bgcolor="#E0EBFF"{/if}>
		<td>{$no++}</td>
		<td><a href="evaluasi-status-mhs.php?nim={$data.NIM_MHS}" target="_blank">{$data.NIM_MHS}</a></td>
		<td>{$data.NM_PENGGUNA}</td>
        <td>{$data.NM_JENJANG}</td>
		<td>{$data.NM_PROGRAM_STUDI}</td>
		<td>{$data.STATUS_TERKINI}</td>
		<td>{$data.REKOMENDASI_STATUS_BWS}</td>
        <td>{$data.REKOMENDASI_STATUS_AKADEMIK}</td>
        <td>{$data.REKOMENDASI_ADMINISTRASI}</td>
        <td>
			{$data.PENETAPAN}

        </td>
		<td>{$data.KETERANGAN}</td>
    </tr>
	{/foreach}
</table>
</form>
{/if}

{literal}
    <script>
			$("#f2").validate();

			
			$('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        	});
    </script>
{/literal}
