<div class="center_title_bar">ENTRY NILAI MAHASISWA ASING</div>
<form action="nilai-asing.php" method="post" name="mhs_krs" id="mhs_krs">
<input type="hidden" name="action" value="krs" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr>
	  <td>Nim Mahasiswa </td>
	  <td>:</td>
	  <td><input type="text" name="nim"></td>
	  <td><input type="submit" name="cari1" value="Proses"></td>
   </tr>
</table>
</form>

{if isset($smarty.request.nim)}

<table>
	<tr>
    	<th colspan="2">BIODATA</th>
    </tr>
	<tr>
    	<td>NIM</td>
        <td>{$krs[0]['NIM_MHS_ASING']}</td>
    </tr>
    <tr>
    	<td>NAMA</td>
        <td>{$krs[0]['NM_PENGGUNA']}</td>
    </tr>
</table>
<form action="nilai-asing.php" method="post" name="f" id="f">
<table>
	<tr>
    	<th colspan="10" style="text-align:center">ENTRY NILAI</th>
    </tr>
    	<tr>
        	<th>No</th>
            <th>Kode Mata Kuliah</th>
            <th>Mata Kuliah</th>
            <th>SKS</th>
            <th>Kelas</th>
            <th>Hari</th>
            <th>Jam</th>
            <th>Semester</th>
            <th>Nilai Angka</th>
            <th>Nilai Huruf</th>
        </tr>
       {$no = 1}
	{foreach $krs as $data}
    	<tr>
        	<td>{$no++}</td>
            <td>{$data.KD_MATA_KULIAH}</td>
            <td>{$data.NM_MATA_KULIAH}</td>
            <td>{$data.KREDIT_SEMESTER}</td>
            <td>{$data.NAMA_KELAS}</td>
            <td>{$data.NM_JADWAL_HARI}</td>
            <td>{$data.JAM}</td>
            <td>{$data.NM_SEMESTER}</td>
            <td>
            	<input type="text" name="nilai{$no}" value="{$data.NILAI_ANGKA}" size="3"/>
            	<input type="hidden" value="{$data.ID_PENGAMBILAN_MK}" name="id{$no}" />
            </td>
            <td>{$data.NILAI_HURUF}</td>
        </tr>
    {/foreach}
    <tr>
    	<td colspan="10" style="text-align:center">
        	<input type="submit" value="Simpan" name="simpan" />
        	<input type="hidden" value="{$smarty.request.nim}" name="nim" />
            <input type="hidden" value="{$no}" name="no" />
        </td>
    </tr>
</table>    
</form>
{/if}