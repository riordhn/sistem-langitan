{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[0,0]]
		}
	); 
} 
);
</script>
{/literal}

<div class="center_title_bar">Rentang Nilai </div>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
      <th>Nilai Huruf</th>
      <th>Nilai Mutu</th>
      <th>Nilai Min</th>
      <th>Nilai Maks</th>
    </tr>
	</thead>
	<tbody>
	{foreach name=list item="list" from=$T_NILAI}
	{if $smarty.foreach.list.iteration % 2 == 0}
	<tr bgcolor="lightgray">
	{else}
	<tr>
	{/if}
      <td><center>{$list.NM_STANDAR_NILAI}</center></td>
      <td><center>{$list.NILAI_STANDAR_NILAI}</center></td>
      <td><center>{$list.NILAI_MIN_PERATURAN_NILAI}</center></td>
      <td><center>{$list.NILAI_MAX_PERATURAN_NILAI}</center></td>
    </tr>
    {foreachelse}
    <tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
    {/foreach}
	</tbody>
</table>



    