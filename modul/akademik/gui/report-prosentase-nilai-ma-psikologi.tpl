{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[0,0]]
		}
	); 
} 
);
</script>
{/literal}

<div class="center_title_bar">Distribusi Nilai Mata Ajar {foreach item="list1" from=$NM_PRODI}{$list1.PRODI}{/foreach} {foreach item="list2" from=$THNSMT}{$list2.THN_SMT}{/foreach}</div>
<form action="report-prosentase-nilai-ma-psikologi.php" method="post" name="id_daftarmk" id="id_daftarmk">
<input type="hidden" name="action" value="view">
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td>
			 	Prodi : 
               <select name="kdprodi">
    		   <option value=''>------</option>
			   <option value='all-{$FAK}' style="font-weight: bold; color: #fff; background-color: #233d0e;">LIHAT SEMUA</option>
	 		   {foreach item="list" from=$T_PRODI}
    		   {html_options values=$list.ID_PROGRAM_STUDI output=$list.PRODI}
	 		   {/foreach}
			   </select>
		     </td>
			 <td>
			 	Tahun/Semester : 
               <select name="kdthnsmt">
    		   <option value=''>------</option>
	 		   {foreach item="list_smt" from=$T_THNSMT}
    		   {html_options values=$list_smt.ID_SEMESTER output=$list_smt.THN_SMT}
	 		   {/foreach}
			   </select>
		     </td>
			 <td>			   
			   <input type="submit" name="View" value="View">
		     </td> 
           </tr>
</table>
</form>
{if {$list1.PRODI} == '' && $list2.THNSMT == ''}
{else}
<p>
<input type=button name="cetak" value="Ekspor ke Excel" onclick="window.open('proses/prosentase-nilai-ma-xls.php?smt={$KD_SMT}&thn={$KD_THN}&fak={$KD_FAK}&prodi={$KD_PRODI}','baru2');">&nbsp;
</p>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
             <th width="8%">Kode</th>
             <th width="26%">Nama Mata Ajar</th>
			 <th width="5%">SKS</th>
             <th width="8%">A</th>
			 <th width="8%">AB</th>
             <th width="8%">B</th>
			 <th width="8%">BC</th>
			 <th width="8%">C</th>
			 <th width="8%">D</th>
			 <th width="8%">E</th>
			 <th class="noheader" width="5%">Detail</th>
	</tr>
		</thead>
		<tbody>
           {foreach name=test item="list" from=$T_MK}
		   <tr>
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
			 <td><center>{$list.KREDIT_SEMESTER}</center></td>
             <td><center>{$list.JML_A}<br><font color="blue"><b>({$list.NILAI_A}%)</b></font></center></td>
			 <td><center>{$list.JML_AB}<br><font color="blue"><b>({$list.NILAI_AB}%)</b></font></center></td>
             <td><center>{$list.JML_B}<br><font color="blue"><b>({$list.NILAI_B}%)</b></font></center></td>
			 <td><center>{$list.JML_BC}<br><font color="blue"><b>({$list.NILAI_BC}%)</b></font></center></td>
			 <td><center>{$list.JML_C}<br><font color="blue"><b>({$list.NILAI_C}%)</b></font></center></td>
			 <td><center>{$list.JML_D}<br><font color="blue"><b>({$list.NILAI_D}%)</b></font></center></td>
			 <td><center>{$list.JML_E}<br><font color="blue"><b>({$list.NILAI_E}%)</b></font></center></td>
			 <td><center><input type=button name="detail" value="Detail" onclick="window.open('#report-dn!report-nilai-detail.php?action=detail&kd={$list.KD_MATA_KULIAH}&smt={$KD_SMT}','baru2');"></center></td>
           </tr>
		   {foreachelse}
        <tr><td colspan="11"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
   	 </table>
{/if}