{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
            8: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}

<div class="center_title_bar">Transkrip Nilai {foreach item="list1" from=$MHS}{$list1.NM_MHS} {/foreach} </div>
<p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
<form action="transkrip.php" method="post" name="id_transkrip" id="id_transkrip">
<input type="hidden" name="action" value="view" >
NIM : <input type="text" name="nim" value="{if !empty($smarty.post.nim)}{$smarty.post.nim}{/if}">&nbsp;&nbsp;&nbsp;<input type="submit" name="View" value="View">
{if {$list1.NM_MHS} == ''}
{else}
{foreach item="cek" from=$T_MHS}
{if {$cek.STATUS_AKADEMIK_MHS} != 4}
<font color="red"><br><br><b>Maaf, {$cek.NM_PENGGUNA} dengan NIM:{$cek.NIM_MHS} belum dinyatakan lulus, mohon gunakan cetak Rekap nilai.<br><br></b></font>
{else}

{if $cek.NM_JENJANG == 'S2' || $cek.NM_JENJANG == 'S3' || $cek.NM_JENJANG == 'Profesi'}
&nbsp;&nbsp;&nbsp;<input type="button" name="cetak" value="Cetak" onclick="window.open('{$LINK_PASCA}','baru');">
&nbsp;&nbsp;&nbsp;<input type="button" name="cetak" value="Cetak (English)" onclick="window.open('{$LINK_PASCA_ENG}','baru');">
{else}
&nbsp;&nbsp;&nbsp;<input type="button" name="cetak" value="Cetak" onclick="window.open('{$LINK_D3S1}','baru');">
&nbsp;&nbsp;&nbsp;<input type="button" name="cetak" value="Cetak (English)" onclick="window.open('{$LINK_D3S1_ENG}','baru');">
{/if}

</form>
		</td>
	</tr>
</table>
</p>
{foreach item="list0" from=$T_MHS}
<table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr>
             <td>NIM</td>
			 <td>:</td>
			 <td>{$list0.NIM_MHS}</td>
			 <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
             <td>Nomor Ijasah</td>
			 <td>:</td>
			 <td>{$list0.NO_IJASAH}</td>
           <tr>
             <td>Nama Mahasiswa</td>
			 <td>:</td>
			 <td>{$list0.NM_PENGGUNA}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			 <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
             <td>Tanggal Lulus</td>
			 <td>:</td>
			 <td>{$list0.TGL_LULUS}</td>
           </tr>
           <tr>
             <td>Program Studi</td>
			 <td>:</td>
			 <td>{$list0.NM_JENJANG} {$list0.NM_PROGRAM_STUDI}</td>
			 <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
             <td>Status</td>
			 <td>:</td>
			 <td>{$list0.NM_STATUS_PENGGUNA}</td>
           </tr>
</table>
{/foreach}

<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
			 <th width="15%">Kode</th>
			 <th width="50%">Nama MA</th>
             <th width="10%">SKS</th>
			 <th width="10%">Nilai</th>
			 <th width="15%">Bobot</th>
           </tr>
		</thead>
		<tbody>
		   {foreach name=test item="list" from=$T_MK}
           <tr>
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td><center>{$list.KREDIT_SEMESTER}</center></td>
             <td><center>{$list.NILAI_HURUF}</center></td>
			 <td><center>{($list.BOBOT_TOTAL)|number_format:2:".":","}</center></td>
           </tr>
            {foreachelse}
        <tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		<tbody>
		{$footer = $T_IPK}
           <tr>
             <td colspan="2" style="text-align:right;"><b>Total SKS dan Bobot</b>&nbsp;&nbsp;&nbsp;</td>
             <td><center><b>{$footer.TOTAL_SKS}</b></center></td>
			 <td></td>
			 <td><center><b>{$footer.TOTAL_BOBOT}</b></center></td>
           </tr>
           <tr>
			 <td colspan="2" style="text-align:right;"><b>Indeks Prestasi Kumulatif</b>&nbsp;&nbsp;&nbsp;</td>
             <td colspan="3"><center><b>{$footer.IPK}</b></center></td>
           </tr>
</table>
{/if}
{/foreach}
{/if}
