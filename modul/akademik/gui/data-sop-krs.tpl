{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[3,0],[4,0],[1,0]],
		}
	); 
} 
);
</script>
{/literal}
<div class="center_title_bar">Data Mahasiswa Aktif, SOP, KRS</div>
<form action="data-sop-krs.php" method="post">
<table class="tb_frame" width="80%" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td>
			 	Prodi : 
                <select name="kdprodi">
    		   <option value=''>-- PRODI --</option>
	 		   {foreach $prodi as $list}
    		   		<option value="{$list.ID_PROGRAM_STUDI}" {if $id_prodi == $list.ID_PROGRAM_STUDI} selected="selected"{/if}>{$list.NM_JENJANG} - {$list.NM_PROGRAM_STUDI}</option>
	 		   {/foreach}
			   </select>
		     </td> 
             <td>
			 	Semester : 
                <select name="semester">
    		   <option value=''>-- SEMESTER --</option>
	 		   {foreach $semester as $list}
    		   		<option value="{$list.ID_SEMESTER}" {if $id_semester == $list.ID_SEMESTER} selected="selected"{/if}>{$list.THN_AKADEMIK_SEMESTER}  ({$list.NM_SEMESTER})</option>
	 		   {/foreach}
			   </select>
			   <input type="submit" name="View" value="View">
		     </td> 
           </tr>
</table>
</form>	

{if isset($krs_sop)}
<p>
<input type=button name="cetak" value="Ekspor Data ke MS. Excel" onclick="window.open('proses/data-sop-krs-xls.php?prodi={$id_prodi}&sem={$id_semester}&nm_prodi={$nm_prodi}','baru2');"><br />
</p>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
        	<th>No</th>
            <th>NIM</th>
            <th>NAMA</th>
            <th>PRODI</th>
            <th>STATUS AKADEMIK</th>
            <th>STATUS BAYAR</th>
            <th>STATUS KRS</th>
            <th>BIAYA KULIAH</th>
        </tr>
	</thead>
	<tbody>
        {$no=1}
        {foreach $krs_sop as $data}
        <tr>
        	<td>{$no++}</td>
            <td>{$data.NIM_MHS}</td>
            <td>{$data.NM_PENGGUNA}</td>
            <td>{$data.NM_PROGRAM_STUDI}</td>
            <td>{$data.NM_STATUS_PENGGUNA}</td>
            <td>{$data.NAMA_STATUS}</td>
            <td>{if $data.SUDAH_KRS == 1}Sudah{else}Belum{/if}</td>
            <td style="text-align:right">{$data.BESAR_BIAYA|number_format:2:".":","}</td>
        </tr>
        {/foreach}
	</tbody>
    </table>
{/if}
