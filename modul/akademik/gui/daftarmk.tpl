{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[4,0],[2,0]],
		}
	);
}
);

	function ambil(ID_PROGRAM_STUDI){
		$.ajax({
		type: "POST",
        url: "daftarmk.php",
        data: "kdprodi="+$("#id_program_studi").val(),
        cache: false,
        success: function(data){
			$('#content').html(data);
        }
        });
	}
</script>
{/literal}

<div class="center_title_bar">Daftar Mata Ajar </div>
<form action="daftarmk.php?kirim=view" method="post" name="id_daftarmk" id="id_daftarmk">
<table class="tb_frame" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td>
			 	Prodi :
                <select name="kdprodi" id="id_program_studi" onChange="ambil()">
    		   <option value=''>-- PRODI --</option>
	 		   {foreach item="list" from=$T_PRODI}
    		   {html_options values=$list.ID_PROGRAM_STUDI output=$list.PRODI selected=$smarty.request.kdprodi}
	 		   {/foreach}
			   </select>
			 	Tahun Kurikulum :
                <select name="thkur">
    		   <option value=''>-- THN KUR --</option>
	 		   {foreach item="list" from=$T_KUR}
    		   {html_options values=$list.ID_KURIKULUM_PRODI output=$list.NAMA selected=$smarty.request.thkur}
	 		   {/foreach}
			   </select>
			   <input type="submit" name="View" value="View">
		     </td>
           </tr>
</table>
</form>

<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
		<th>Kurikulum</th>
		<th>Kode</th>
		<th width="35%">Nama Mata Ajar</th>
		<th>SKS</th>
		<th>SMT</th>
		<th>Status LP3</th>
		<th>Prasyarat</th>
	</tr>
	</thead>
	<tbody>
	{foreach name=test item="list" from=$T_MK}
	<tr>
		<td>{$list.TAHUN_KURIKULUM}</td>
		<td>{$list.KD_MATA_KULIAH}</td>
		<td>{$list.NM_MATA_KULIAH}</td>
		<td><center>{$list.KREDIT_SEMESTER}</center></td>
		<td><center>{$list.TINGKAT_SEMESTER}</center></td>
		<td>{$list.STATUS_LP3}</td>
		<td>{$list.MKSYARAT}</td>
	</tr>
	{foreachelse}
	<tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
