{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]]
		}
	); 
} 
);
</script>
{/literal}

<div class="center_title_bar">MONITORING CEKAL UTS</div>  
 <div id="tabs">
        <div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
        {*<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Cekal</div>*}
</div>
<div class="panel" id="panel1" style="display: {$disp1} ">
<p> </p>
 <form action="monitor-cekal-uts.php" method="post">
		 <input type="hidden" name="action" value="tampil" >  
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td>
			 	Tahun Akademik : 
                <select name="smt">
    		   <option value=''>-- PILIH THN AKD --</option>
	 		   {foreach item="smt" from=$T_ST}
    		   {html_options  values=$smt.ID_SEMESTER output=$smt.SMT selected=$SMT}
	 		   {/foreach}
			   </select>
		     </td>
			 <td> <input type="submit" name="View" value="View"></td>
           </tr>		    
</table>
</form>	    
<!-- set id_pt pada config.php global -->
{if $id_pt==1 }
<p><a href="jadwal-akademik.php">* Pastikan jadwal kegiatan akademik sudah terisi (Perkuliahan dan Ujian Akhir Semester)</a><br /></p> 
{else}
<p>* Proses cekal mengikuti kebijakan Perguruan Tinggi masing-masing<br /></p>
{/if}
		<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
			<tr>
             <th width="10%">Kode</th>
             <th width="30%">Nama Mata Ajar</th>
             <th width="10%">Kelas</th>
			 <th width="15%">Prodi</th>
			 <th width="10%">TM</th>
			 <th width="10%">Mhs Cekal</th>
			 <th width="10%">Aksi</th>
			</tr>
		</thead>
		<tbody>
           {foreach name=test item="list" from=$T_MK}
		   <tr>
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td><center>{$list.NAMA_KELAS}</center></td>
			 <td><center>{$list.PRODI}</center></td>
			 <td><center>{$list.TM}</center></td>
			 <td><center><a href="monitor-cekal-uts.php?action=cekal&id_kelas_mk={$list.ID_KELAS_MK}&kd_mk={$list.KD_MATA_KULIAH}&smt={$SMT}">{$list.TTL}</a></center></td>
             <td><center><input type="button" name="cetak" value="Cetak" onclick="window.open('proses/cetak-cekal-uts.php?cetak={$list.ID_KELAS_MK}&idsem={$SMT}','baru');"></center></td>
		   </tr>
		   {foreachelse}
        <tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
   	 </table>
</div>
{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable1").tablesorter(
		{
		sortList: [[3,0]],
		headers: {
            9: { sorter: false }
		}
		}
	); 
} 
);
</script>
{/literal}
<div class="panel" id="panel2" style="display: {$disp2} ">
<p> </p>
<form action="monitor-cekal-uts.php" method="post" >
<input type="hidden" name="action" value="proses" >	
<input type="hidden" name="id_kelas_mk" value="{$ID_KELAS}" >
<input type="hidden" name="smt" value="{$SMT}" >	
		<table id="myTable1" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
				<tr>
			     <th width="12%">Kode</th>
				 <th width="35%">Nama Mata Ajar</th>
				 <th width="5%">Kelas</th>
				 <th width="10%">Nim</th>
				 <th width="20%">Nama Mhs</th>
				 <th width="3%">TM</th>
				 <th width="4%">Hadir</th>
				 <th width="4%">Prosen</th>
				 <th width="4%">Status</th>
				 <th class="noheader" width="3%">Buka</th>
				</tr>
			</thead>
			<tbody>
				{foreach name=test item="cekal" from=$DATA_CEKAL}
				<tr>
				 <td>{$cekal.KD_MATA_KULIAH}</td>
				 <td>{$cekal.NM_MATA_KULIAH}</td>
				 <td><center>{$cekal.NAMA_KELAS}</center></td>
				 <td>{$cekal.NIM_MHS}</td>
				 <td>{$cekal.NM_PENGGUNA}</td>
				 <td><center>{$cekal.TM}</center></td>
				 <td><center>{$cekal.HADIR}</center></td>
				 <td><center>{$cekal.PROSEN}%</center></td>
				 <td><center>{$cekal.STATUS}</center></td>
				 <td><center><input name="cekalmhs{$smarty.foreach.test.iteration}" type="checkbox" value="{$cekal.ID_MHS}" {$cekal.TANDA} /></center></td>
				</tr>
			    {foreachelse}
        		<tr><td colspan="10"><em>Data tidak ditemukan</em></td></tr>
        		{/foreach}
				<input type="hidden" name="counter" value="{$smarty.foreach.test.iteration}" >

			</tbody>
		</table>
		<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
                    <td>NO MEMO WADEK I</td>
                    <td>:</td>
					<td><input type="text" name="no_memo">&nbsp;[WAJIB DI-ISI]</td>
        </tr>
		<tr>
                    <td>Catatan</td>
                    <td>:</td>
					<td><textarea name="catatan" cols="60"></textarea>&nbsp;[WAJIB DI-ISI]</td>
        </tr>
		</table>
				<p><div align="right"><input type="button" value="Kembali" onClick="javascript:history.go(-1)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="proses" value="Proses"></div></p>
</form>
</div>
