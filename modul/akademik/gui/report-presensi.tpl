{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
            10: { sorter: false }
		}
		}
	); 
} 
);
</script>
{/literal}

<div class="center_title_bar">Rekapitulasi Presensi Perkuliahan {foreach item="list1" from=$NM_PRODI}{$list1.PRODI} {/foreach} {foreach item="list2" from=$THNSMT}{$list2.THN_SMT} {/foreach}</div>
<form action="report-presensi.php" method="post" name="id_daftarmk" id="id_daftarmk">
<input type="hidden" name="action" value="view">
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td>
			 	Prodi : 
               <select name="kdprodi">
    		   <option value=''>------</option>
			   <option value='all-{$FAK}' style="font-weight: bold; color: #fff; background-color: #233d0e;">LIHAT SEMUA</option>
	 		   {foreach item="list" from=$T_PRODI}
    		   {html_options values=$list.ID_PROGRAM_STUDI output=$list.PRODI selected=$smarty.post.kdprodi}
	 		   {/foreach}
			   </select>
		     </td>
			 <td>
			 	Tahun/Semester : 
               <select name="kdthnsmt">
    		   <option value=''>------</option>
	 		   {foreach item="list_smt" from=$T_THNSMT}
    		   {html_options values=$list_smt.ID_SEMESTER output=$list_smt.THN_SMT selected=$smarty.post.kdthnsmt}
	 		   {/foreach}
			   </select>
		     </td>
			 <td>			   
			   <input type="submit" name="View" value="View">
		     </td> 
           </tr>
</table>
</form>
{if {$list1.PRODI} == '' && $list2.THNSMT == ''}
{else}
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
             <th width="5%">Kode</th>
             <th width="34%">Nama Mata Ajar</th>
             <th width="5%">SKS</th>
			 <th width="5%">KLS</th>
			 <th width="8%">Ruang</th>
             <th width="5%">Hari</th>
			 <th width="8%">Jam</th>
			 <th width="17%">PJMA</th>
			 <th width="5%">Pst</th>
			 <th width="5%">Rerata<br/>Kehadiran</th>
             <th class="noheader" width="8%">Cetak Detil</th>
           </tr>
		</thead>
		<tbody>
           {foreach name=test item="list" from=$T_MK}
		   <tr>
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td><center>{$list.KREDIT_SEMESTER}</center></td>
			 <td><center>{$list.NAMA_KELAS}</center></td>
			 <td><center>{$list.NM_RUANGAN}</center></td>
             <td><center>{$list.NM_JADWAL_HARI}</center></td>
			 <td><center>{$list.JAM}</center></td>
			 <td>{$list.PJMA}</td>
			 <td><center>{$list.PESERTA}</center></td>
			 <td><center>{$list.RERATA}</center></td>

			{if $list.PESERTA == 0}
			<td>&nbsp;</td>
			{else}
            <td>
			<center>
			<input type=button name="cetak5" value="Mhs" onclick="window.open('proses/rekap-presensi-mhs-cetak.php?cetak={$list.ID_KELAS_MK}&prodi={$list.ID_PROGRAM_STUDI}&smt={$list.NM_SEMESTER}&thn={$list.TAHUN_AJARAN}','baru2');"><br>
			<input type=button name="cetak5" value="Rekap" onclick="window.open('proses/rekapall-presensi-mhs-cetak.php?cetak={$list.ID_KELAS_MK}&prodi={$list.ID_PROGRAM_STUDI}&smt={$list.NM_SEMESTER}&idsmt={$list.ID_SEMESTER}&thn={$list.TAHUN_AJARAN}','baru2');"><br>
			<input type=button name="cetak5" value="Dsn" onclick="window.open('proses/rekap-presensi-dsn-cetak.php?cetak={$list.ID_KELAS_MK}&prodi={$list.ID_PROGRAM_STUDI}&smt={$list.NM_SEMESTER}&thn={$list.TAHUN_AJARAN}','baru2');">
            </center>
			</td>
			{/if}

           </tr>
		   {foreachelse}
        <tr><td colspan="11"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
   	 </table>
{/if}