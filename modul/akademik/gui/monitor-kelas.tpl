{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
            10: { sorter: false }
		}
		}
	); 
} 
);
</script>
{/literal}

<div class="center_title_bar">MONITORING KAPASITAS KELAS </div>
<div id="tabs">
	<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');"><a href="monitor-kelas.php">Rincian </a></div>
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Update</div>
	<div id="tab3" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Peserta MA</div>
</div>
<div class="panel" id="panel1" style="display: {$disp1} ">
<p> </p>
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
             <th width="9%">Kode</th>
             <th width="25%">Nama Mata Ajar</font> </td>
			 <th width="5%">SKS</th>
			 <th width="5%">KLS</th>
             <th width="5%">STS</th>
             <th width="5%">PRODI</th>
             <th width="6%">Kpst</th>
			 <th width="5%">Terisi</th>
			 <th width="5%">KRS</th>
			 <th width="5%">KPRS</th>
             <th class="noheader" width="15%">Aksi</th>
           </tr>
		</thead>
		<tbody>
           {foreach name=test item="list" from=$T_MK}
		   {if $list.KAPASITAS_KELAS_MK - $list.KLS_TERISI == 0}
		   <tr bgcolor="red">
   		   {elseif $list.KAPASITAS_KELAS_MK - $list.KLS_TERISI <= 5}
		   <tr bgcolor="yellow">
		   {else}
		   <tr>
		   {/if}
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td><center>{$list.KREDIT_SEMESTER}</center></td>
			 <td><center>{$list.NAMA_KELAS}</center></td>
             <td><center>{$list.STATUS}</center></td>
             <td>{$list.NM_JENJANG} - {$list.NM_PROGRAM_STUDI}</center></td>
			 <td><center>{$list.KAPASITAS_KELAS_MK}</center></td>
			 <td><center><a href="monitor-kelas.php?action=detail&id_kelas_mk={$list.ID_KELAS_MK}">{$list.KLS_TERISI}</a></td>
			 <td><center>{$list.KRS}</center></td>
			 <td><center>{$list.KPRS}</center></td>
             <td>
			 <a href="monitor-kelas.php?action=kapasitas&id_kelas_mk={$list.ID_KELAS_MK}">Ubah Kpst</a> | <br>
			 <a href="monitor-kelas.php?action=del&id_kelas_mk={$list.ID_KELAS_MK}">Hapus Kls</a>
             </td>
           </tr>
		   {foreachelse}
        <tr><td colspan="9"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
   	</table>
</div>
		
<div class="panel" id="panel2" style="display: {$disp2}">
<p> </p>
<form action="monitor-kelas.php" method="post">
<input type="hidden" name="action" value="add" >
	<table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><td colspan="3"><strong> Tambah Kapasitas Kelas </strong></td></tr>
		{foreach item="ubah" from=$TJAF1}
		<input type="hidden" name="id_kelas_mk" value="{$ubah.ID_KELAS_MK}" />
		<tr>
			  <td width="29%" >Kode Mata Ajar</td>
			  <td width="2%" >:</td>
			  <td width="69%" >{$ubah.KD_MATA_KULIAH}</td>
		</tr>
		<tr>
			  <td>Nama Mata Ajar</td>
			  <td>:</td>
			  <td>{$ubah.NM_MATA_KULIAH}</td>
		</tr>
		<tr>
			  <td>Kelas</td>
			  <td>:</td>
			  <td>{$ubah.NAMA_KELAS}</td>
		</tr>					
		<tr bgcolor="yellow">
			  <td>KAPASITAS KELAS</td>
			  <td>:</td>
			  <td><input name="kap_kelas" id="kap_kelas" type="text" class="required" size="3" maxlength="3" class="required" value="{$ubah.KAPASITAS_KELAS_MK}"/></td>
		</tr>
		{/foreach}
	</table>
<p><input type="submit" name="Simpan1" value="Simpan"></p>
</form>			    
</div>

{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable1").tablesorter(
		{
		sortList: [[0,0]],
		}
	); 
} 
);
</script>
{/literal}
<div class="panel" id="panel3" style="display: {$disp3} ">
<p> </p>
<table>
	{foreach item="info" from=$T_INFO}
	<tr>
		<td width="29%" >Kode Mata Ajar / SKS / Kelas</td>
		<td width="2%" >:</td>
		<td width="69%" >{$info.KD_MATA_KULIAH} / {$info.KREDIT_SEMESTER} / {$info.NAMA_KELAS}</td>
	</tr>
	<tr>
		<td width="29%" >Nama Mata Ajar</td>
		<td width="2%" >:</td>
		<td width="69%" >{$info.NM_MATA_KULIAH}</td>
	</tr>
	<tr>
		<td width="29%" >Tim Pengajar</td>
		<td width="2%" >:</td>
		<td width="69%" ><br><ol>{$info.TIM}</li></ol></td>
	</tr>
		{/foreach}
</table>
	<table id="myTable1" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
				<th width="15%">NIM</th>
				<th width="35%">Nama Mahasiswa</th>
				<th width="35%">Prodi</th>
				<th width="15%">Status</th>
			</tr>
		</thead>
		<tbody>
				{foreach name=test item="list" from=$T_DETAIL}
				{if $list.STATUS == "Blm Disetujui"}
			<tr bgcolor="red">
				{else}
			<tr>
				{/if}
				<td>{$list.NIM_MHS}</td>
				<td>{$list.NM_PENGGUNA}</td>
				<td>{$list.NM_PROGRAM_STUDI}</td>
				<td>{$list.STATUS}</td>
			</tr>
				{foreachelse}
			<tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
				{/foreach}
		</tbody>
	</table>
</div>

{literal}
 <script>$('form').validate();</script>
{/literal}