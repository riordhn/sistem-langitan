{literal}
<script language="javascript" type="text/javascript" src="js/datetimepicker.js"></script>
{/literal}
<div class="center_title_bar">Jadwal Kegiatan Akademik </div>  
<form action="jadwal-akademik.php" method="post" name="id_jad_akd" id="id_jad_akd">
<input type="hidden" name="action" value="tampil" >  
<table>
           <tr>
             <td>
			 	Tahun Akademik : 
                <select name="smt">
    		   <option value=''>-- PILIH THN AKD --</option>
	 		   {foreach item="smt" from=$T_ST}
    		   {html_options  values=$smt.ID_SEMESTER output=$smt.SMT selected=$smt1}
	 		   {/foreach}
			   </select>
			   <input type="submit" name="View" value="View">
		     </td> 
           </tr>
</table>
</form>	
		 <div id="tabs">
        <div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
        <div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Input</div>
		<div id="tab3" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Update</div>
    	</div>
<div class="panel" id="panel1" style="display: {$disp1} ">
<p> </p>    
			<table class="tablesorter">
			<thead>
			   <tr>
			     <th><center>No</center></th>
				 <th><center>Kegiatan</center></th>
				 <th><center>Tanggal mulai</center></th>
				 <th><center>Tanggal Selesai</center></th>
				 <th><center>Aksi</center></th>
			  </tr>
			</thead>
			<tbody>
			  {foreach name="jaf" item="jaf" from=$T_JAF}
			   {if $smarty.foreach.jaf.iteration % 2 == 0}
				<tr bgcolor="lightgray">
				{else}
				<tr>
				{/if}
			     <td><center>{$smarty.foreach.jaf.iteration}</center></td>
				 <td>{$jaf.NM_KEGIATAN} </td>
				 <td>{$jaf.TGL_MULAI}</td>
				 <td>{$jaf.TGL_SELESAI}</td>
				 <td><center><a href="jadwal-akademik.php?action=tampil&id_jsf={$jaf.ID_JSF}&id_smt={$jaf.ID_SEMESTER}" onclick="displayPanel('3')">Update</a> | <a href="jadwal-akademik.php?action=del&id_jsf={$jaf.ID_JSF}&id_smt={$jaf.ID_SEMESTER}">Delete</a></center></td>
			   </tr>
			     {foreachelse}
        		<tr><td colspan="5"><em>Data tidak ditemukan</em></td></tr>
				{/foreach}
			</tbody>
  </table>		 
	</div>
		
        <div class="panel" id="panel2" style="display: {$disp2}">
		<p> </p>
			<form action="jadwal-akademik.php" method="post" name="id_jad_akd1" id="id_jad_akd1">
		    <input type="hidden" name="action" value="add" >
			 <input type="hidden" name="smt1" value="{$idsmt}" >	
	      <table>
                  <tr>
                    <td>Kegiatan</td>
                    <td>:</td>
					<td><select name="nm_kgt">
                      {foreach item="nm_kgt" from=$T_KGT}
    		   {html_options  values=$nm_kgt.ID_KEGIATAN output=$nm_kgt.NM_KEGIATAN}
	 		   {/foreach}
                    </select></td>
                  </tr>
                  <tr>
                    <td>Tanggal Mulai </td>
					
					 <td>:</td>
                    <td><input type="Text" name="calawal" value="" id="calawal" size="25" onClick="NewCssCal('calawal','ddMMMyyyy','dropdown',true,'24');"></td>
					{*<input type="button" value="" style="background-image:url(includes/cal/cal.gif); background-repeat:no-repeat; background-position:center; border:none;" onClick="NewCssCal('calawal','ddMMMyyyy');" /></td>*}
                  </tr>
				  <tr>
                    <td>Tanggal Selesai </td>
                    <td>:</td>
                    <td> <input type="Text" name="calakhir" value="" id="calakhir" size="25" onClick="NewCssCal('calakhir','ddMMMyyyy','dropdown',true,'24');">
					{*<input type="button" value="" style="background-image:url(includes/cal/cal.gif); background-repeat:no-repeat; background-position:center; border:none;" onClick="NewCssCal('calakhir','ddMMMyyyy');" /></td>*}
                  </tr>
            
                </table>
                  <p>
                    <input type="submit" name="Submit" value="Tambah">
					<input type="hidden" name="smt" value="{$smt1}" >
                </p>
</form>	

		
	<br />	
<table>
<tr>
	<th colspan="8" style="text-align:center">Kalender Akademik Universitas</th>
</tr>
<tr>
			<th>No</th>
			<th>Nama Kegiatan</th>
			<th>Deskripsi Keigatan</th>
			<th>Tanggal Mulai</th>
			<th>Tanggal Selesai</th>
		</tr>
		{$no = 1}
		{foreach $jadwal as $data}
		<tr>
			<td>{$no++}</td>
			<td>{$data.NM_KEGIATAN}</td>
			<td>{$data.DESKRIPSI_KEGIATAN}</td>
			<td>{$data.TGL_MULAI_JKS}</td>
			<td>{$data.TGL_SELESAI_JKS}</td>
		</tr>
		{/foreach}
</table>
	    
        </div>
        
		<div class="panel" id="panel3" style="display: {$disp3}">
		<p> </p>
		<form action="jadwal-akademik.php" method="post" >
		 <input type="hidden" name="action" value="update" >		
	      <table width="80%" border="1">
            <tr>
              <td>
			  <table>
                  <tr>
				  {foreach item="datajsf" from=$T_JAF}
					
					<input type="hidden" name="id_jsf" value="{$datajsf.ID_JSF}" >
					<input type="hidden" name="id_smt" value="{$datajsf.ID_SEMESTER}" >
					
                    <td>Kegiatan</td>
                    <td>:</td>
                    <td>{$datajsf.NM_KEGIATAN}</td>
                  </tr>
                  <tr>
                    <td>Tanggal Mulai </td>
                    <td>&nbsp;</td>
                    <td><input type="text" name="calawal1" value="{$datajsf.TGL_MULAI}" id="calawal1" size="25" onclick="NewCssCal('calawal1','ddMMMyyyy','dropdown',true,'24');" /></td>
                  </tr>
                  <tr>
                    <td>Tanggal Selesai </td>
                    <td>&nbsp;</td>
                    <td><input type="text" name="calakhir1" value="{$datajsf.TGL_SELESAI}" id="calakhir1" size="25" onclick="NewCssCal('calakhir1','ddMMMyyyy','dropdown',true,'24');" /></td>
                  </tr>
                </table>
				{/foreach}
                  <p>
                    <input type="submit" name="Submit" value="Update">
                </p></td>
            </tr>
          </table>
 </form>		    
	   
       
       		
	<br />	
<table>
<tr>
	<th colspan="8" style="text-align:center">Kalender Akademik Universitas</th>
</tr>
<tr>
			<th>No</th>
			<th>Nama Kegiatan</th>
			<th>Deskripsi Keigatan</th>
			<th>Tanggal Mulai</th>
			<th>Tanggal Selesai</th>
		</tr>
		{$no = 1}
		{foreach $jadwal as $data}
		<tr>
			<td>{$no++}</td>
			<td>{$data.NM_KEGIATAN}</td>
			<td>{$data.DESKRIPSI_KEGIATAN}</td>
			<td>{$data.TGL_MULAI_JKS}</td>
			<td>{$data.TGL_SELESAI_JKS}</td>
		</tr>
		{/foreach}
</table>
       
        </div>


{*
{literal}
    <script>
            $("#calawal").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
			$("#calakhir").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
			$("#calawal1").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
			$("#calakhir1").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
            $('form').validate();
    </script>
{/literal}
*}