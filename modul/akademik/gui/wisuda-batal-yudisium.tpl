<div class="center_title_bar">UNTUK MEMBATALKAN YUDISIUM</div>
<form action="wisuda-batal-yudisium.php" method="post">
	<table width="700">
    	<tr>
        	<td>NIM :</td>
            <td>
            	<input type="text" name="nim">
            </td>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>

{if isset($pesan)}
	{$pesan}
{/if}

{if isset($ijasah)}

<form action="wisuda-batal-yudisium.php" method="post" id="f2">
<table width="600">
  <tr>
    <th colspan="2" style="text-align:center">Status Yudisium</th>
  </tr>
  <tr>
    <td>NIM</td>
    <td>{$ijasah['NIM_MHS']}</td>
  </tr>
  <tr>
    <td>Nama Lengkap</td>
    <td>{$ijasah['NM_PENGGUNA']}</td>
  </tr>
  <tr>
    <td>Program Studi</td>
    <td>{$ijasah['NM_JENJANG']} - {$ijasah['NM_PROGRAM_STUDI']}</td>
  </tr>
  <tr>
    <td>Periode Yudisium</td>
    <td>{$ijasah['NM_TARIF_WISUDA']}</td>
  </tr>
  <tr>
    <td>Yudisium</td>
    <td>
        {if $ijasah['YUDISIUM'] != 0} Sudah Yudisium {else} Batal Yudisium{/if}
	</td>
  </tr>
    <tr>
    	<td colspan="2" style="text-align:center">
			<input type="hidden" name="nim" value="{$ijasah['NIM_MHS']}" />
            <input type="hidden" name="mode" value="update" />
        	<input type="submit" value="Batalkan" />
        </td>
    </tr>
</table>
</form>

{/if}