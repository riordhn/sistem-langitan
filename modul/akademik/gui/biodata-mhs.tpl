{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]]
		}
	); 
} 
);
</script>
{/literal}
<div class="center_title_bar">Biodata Mahasiswa {foreach item="list1" from=$NM_PRODI}{$list1.PRODI} {/foreach} </div>  
<form action="biodata-mhs.php" method="post" name="id_daftarmk" id="id_daftarmk">
<input type="hidden" name="action" value="view" >
<table class="tb_frame" width="80%" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td>
			 	Prodi : 
                <select name="kdprodi">
    		   <option value=''>-- PRODI --</option>
	 		   {foreach item="list" from=$T_PRODI}
    		   {html_options  values=$list.ID_PROGRAM_STUDI output=$list.PRODI selected=$smarty.post.kdprodi}
	 		   {/foreach}
			   </select>
			   <input type="submit" name="View" value="View">
		     </td> 
           </tr>
</table>
</form>	

{if {$list1.PRODI} == ''}
{else}
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
             <th width="3%">No</th>
             <th width="8%">Nim</th>
             <th width="15%">Nama Mhs</th>
             <th width="8%">Detail </th>
           </tr>
	</thead>
	<tbody>
		   {foreach name=test item="list" from=$T_MK}
           <tr>
             <td >{$smarty.foreach.test.iteration}</td>
             <td >{$list.NIM_MHS}</td>
             <td >{$list.NM_PENGGUNA}</td>
             <td ><a href="biodata-detail.php?action=detail&id={$list.ID_MHS}">detail</a></td>
           </tr>
            {foreachelse}
        <tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
	</tbody>
</table>
{/if}
		
   