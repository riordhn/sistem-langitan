{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		}
	); 
} 
);
</script>
{/literal}
<div class="center_title_bar">Monitoring Bidik Misi {$NM_PRODI}, Tahun Akademik {$THN_AKAD}</div>  
<form action="mon_bidik_misi.php" method="post" name="id_daftarmk" id="id_daftarmk">
<input type="hidden" name="action" value="view" >
<p>
			   Prodi : 
               <select name="kdprodi">
    		   <option value='ALL'>-- ALL --</option>
	 		   {foreach item="list" from=$T_PRODI}
    		   {html_options  values=$list.ID_PROGRAM_STUDI output=$list.PRODI}
	 		   {/foreach}
			   </select>
			   Tahun/Semester akademik : 
               <select name="kdthnsmt">
    		   <option value=''>------</option>
	 		   {foreach item="list_smt" from=$T_THNSMT}
    		   {html_options values=$list_smt.ID_SEMESTER output=$list_smt.THN_SMT}
	 		   {/foreach}
			   </select>
			   <input type="submit" name="View" value="View">
</p>
</form>	

<p>
<input type=button name="cetak" value="Ekspor ke Excel" onclick="window.open('proses/mon_bidik_misi-xls.php?prodi={$PRODI}&smt={$SMT_AKAD}&nm_prodi={$NM_PRODI}&tahun={$TAHUN_SMT}&thn_hitung={$THN_HITUNG}','baru2');">&nbsp;
</p>

<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
             <th>No</th>
             <th>Nim</th>
             <th>Nama Mahasiswa</th>
			 <th>Prodi</th>
			 <th>Status</th>
			 <th>Dosen Wali</th>
             <th>SKS SMT</th>
			 <th>IPS</th>
			 <th>TTL SKS</th>
			 <th>IPK</th>
           </tr>
		</thead>
		<tbody>
		   {foreach name=test item="list" from=$T_MK}
           <tr>
			 <td><center>{$smarty.foreach.test.iteration}</center></td>
             <td>{$list.NIM_MHS}</td>
             <td>{$list.NM_MHS}</td>
			 <td>{$list.PRODI}</td>
			 <td>{$list.STATUS}</td>
			 <td>{$list.DOLI}</td>
             <td><center>{$list.SKS_SEM}</center></td>
			 <td><center>{$list.IPS}</center></td>
			 <td><center>{$list.SKS_TOTAL_MHS}</center></td>
			 <td><center>{$list.IPK_MHS}</center></td>
           </tr>
            {foreachelse}
        <tr><td colspan="10"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
</table>
