{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[1,0],[3,0]],
		}
	);
}
);
</script>
{/literal}

<div class="center_title_bar">ENTRY PRESENSI SUSULAN</div>
<div >
<form action="susulan-psikologi.php" method="post">
<input type="hidden" name="action" value="tampil" >
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td>
			 	Tahun Akademik :
                <select name="smt">
    		   <option value=''>-- PILIH THN AKD --</option>
	 		   {foreach item="smt" from=$T_ST}
    		   {html_options  values=$smt.ID_SEMESTER output=$smt.SMT selected=$smtaktif}
	 		   {/foreach}
			   </select>
		     </td>
			 <td> <input type="submit" name="View" value="View"> </td>
           </tr>
</table>
</form>
		<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
			<tr>
             <th width="5%">Kode</th>
             <th width="38%">Nama Mata Ajar</th>
             <th width="4%">SKS</th>
			 <th width="4%">KLS</th>
             <th width="4%">STS</th>
             <th width="7%">Hari</th>
			 <th width="7%">Jam</th>
			 <th width="4%">Pst</th>
			 <th width="12%">TTL TM</th>
			 <th width="15%">Prodi MA</th>
           </tr>
		</thead>
		<tbody>
           {foreach name=test item="list" from=$T_MK}
		   <tr>
             <td><a href="entry-presensi-susulan-psikologi.php?action=entry&id_kelas_mk={$list.ID_KELAS_MK}">{$list.KD_MATA_KULIAH}</a></td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td><center>{$list.KREDIT_SEMESTER}</center></td>
			 <td><center>{$list.NAMA_KELAS}</center></td>
             <td><center>{$list.STATUS}</center></td>
             <td><center>{$list.NM_JADWAL_HARI}</center></td>
			 <td><center>{$list.JAM}</center></td>
			 <td><center>{$list.KLS_TERISI}</center></td>
			 <td><center>{$list.TM}</center></td>
			 <td>{$list.PRODIASAL}</td>
            </td>
           </tr>
		   {foreachelse}
			<tr><td colspan="10"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
   	 </table>
</div>