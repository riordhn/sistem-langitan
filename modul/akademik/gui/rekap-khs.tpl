{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		}
	); 
} 
);
</script>
{/literal}
<div class="center_title_bar">Data KHS Mahasiswa {$NM_PRODI}, Tahun Akademik {$THN_AKAD}</div>  
<form action="rekap-khs.php" method="post" name="id_daftarmk" id="id_daftarmk">
<input type="hidden" name="action" value="view" >
<p>
			   Prodi : 
               <select name="kdprodi">
    		   <option value=''>-- PRODI --</option>
	 		   {foreach item="list" from=$T_PRODI}
    		   {html_options  values=$list.ID_PROGRAM_STUDI output=$list.PRODI selected=$smarty.post.kdprodi}
	 		   {/foreach}
			   </select>
			   Tahun/Semester : 
               <select name="kdthnsmt">
    		   <option value=''>------</option>
	 		   {foreach item="list_smt" from=$T_THNSMT}
    		   {html_options values=$list_smt.ID_SEMESTER output=$list_smt.THN_SMT selected=$smarty.post.kdthnsmt}
	 		   {/foreach}
			   </select>
			   <input type="submit" name="View" value="View">
</p>
</form>	
{if {$NM_PRODI} == ''}
{else}
<p>
<input type=button name="cetak" value="Ekspor ke Excel" onclick="window.open('proses/rekap-khs-xls.php?prodi={$PRODI}&smt={$SMT_AKAD}&nm_prodi={$NM_PRODI}&tahun={$THN_SMT}&tahunakd={$THN_AKD}','baru2');">&nbsp;
</p>

	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
             <th width="3%">No</th>
             <th width="8%">Nim</th>
             <th width="15%">Nama Mhs </th>
             <th width="8%">Kode MA</font> </td>
			 <th width="36%">Nama MA</font> </td>
             <th width="4%">SKS</th>
             <th width="4%">Kls </th>
			 <th width="4%">Smt </th>
			 <th width="4%">Nilai </th>
           </tr>
		</thead>
		<tbody>
		   {foreach name=test item="list" from=$T_MK}
           <tr>
             <td><center>{$smarty.foreach.test.iteration}</center></td>
             <td>{$list.NIM_MHS}</td>
             <td>{$list.NM_PENGGUNA}</td>
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td><center>{$list.KREDIT_SEMESTER}</center></td>
             <td><center>{$list.NAMA_KELAS}</center></td>
			 <td><center>{$list.TIPE_SEMESTER}</center></td>
			 <td><center>{$list.NILAI_HURUF}</center></td>
           </tr>
            {foreachelse}
        <tr><td colspan="9"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
   	</table>
{/if}