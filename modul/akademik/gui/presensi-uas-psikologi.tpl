{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[4,0],[5,0],[1,0]],
		headers: {
            8: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}

<div class="center_title_bar">Cetak Presensi UAS {foreach item="list1" from=$NM_PRODI}{$list1.PRODI} {/foreach}</div>
<form action="presensi-uas.php" method="post" name="id_daftarmk" id="id_daftarmk">
<input type="hidden" name="action" value="view">
<table class="tb_frame" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td>
			 	Prodi :
                <select name="kdprodi">
    		   <option value=''>------</option>
			   <option value='all-{$FAK}' style="font-weight: bold; color: #fff; background-color: #233d0e;">LIHAT SEMUA</option>
	 		   {foreach item="list" from=$T_PRODI}
    		   {html_options values=$list.ID_PROGRAM_STUDI output=$list.PRODI}
	 		   {/foreach}
			   </select>
			   <input type="submit" name="View" value="View">
		     </td>
           </tr>
</table>
</form>
{if {$list1.PRODI} == ''}
{else}
		<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
             <th>Kode</th>
             <th>Nama Mata Ajar</th>
             <th>SKS</th>
             <th>Jenis Ujian</th>
             <th>Tanggal</th>
			 <th>Jam</th>
			 <th>Ruang</th>
			 <th>Peserta</th>
             <th class="noheader">Aksi</th>
           </tr>
		</thead>
		<tbody>
           {foreach name=test item="list" from=$T_MK}
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td><center>{$list.KREDIT_SEMESTER}</center></td>
             <td><center>{$list.NM_UJIAN_MK}</center></td>
             <td><center>{$list.TGL_UJIAN}</center></td>
			 <td><center>{$list.JAM}</center></td>
			 <td><center>{$list.NM_RUANGAN}</center></td>
			 <td><center>{$list.PESERTA}</center></td>
			{if $list.PESERTA == 0}
			<td>&nbsp;</td>
			{else}
             <td>
			 <center>
			<input type=button name="cetak5" value="Cetak" onclick="window.open('proses/presensi-uas-psikologi.php?cetak={$list.ID_KELAS_MK}&ujian={$list.ID_UJIAN_MK}&prodi={$list.ID_PROGRAM_STUDI}&smt={$list.NM_SEMESTER}&thn={$list.TAHUN_AJARAN}','baru2');">
             </center>
			 </td>
			{/if}
           </tr>
		   {foreachelse}
		{if $FAK == 11}
		<tr><td colspan="9"><em>Jadwal ujian belum di seting. Hubungi petugas akademik untuk menjadwalkan ujian.</em></td></tr>
		{else}
        <tr><td colspan="9"><em>Jadwal ujian belum di seting. <a href="uas-reguler.php">Klik di sini</a> untuk seting jadwal ujian.</em></td></tr>
		{/if}
        {/foreach}
		</tbody>
   	 </table>
{/if}