<div class="center_title_bar">Sebaran KKN Per-fakultas</div>  
{if $download == '' || $download =='0'}
<!-- 
<form name="formSebaran" id="formSebaran" action="mon-kkn.php" method="get">
<select name="fakultas" id="fakultas">
	
	<option value="0">
		PILIH FAKULTAS
	</option>
	{foreach $fakultas as $data}
	<option value="{$data.ID_FAKULTAS}">
		{$data.NM_FAKULTAS}
	</option>
	{/foreach}
</select>
<input type="submit" value="OK" />
</form>
<br />
-->
{/if}

{if $download == '' || $download =='0'}
<a class="disable-ajax" href="mon-kkn.php?fakultas={$getfak}&download"> Download Excel </a>
{/if}

{if $getfak}

	<table>
		<th>
			NO
		</th>
		<th>
			NIM
		</th>
		<th>
			NAMA
		</th>
		<th>
			KELOMPOK
		</th>
		<th>
			KELURAHAN
		</th>
		<th>
			PRODI
		</th>
		<th>
			FAKULTAS
		</th>
		
		{foreach $sebaran as $data}
		<tr>
			<td>
				{$data@index + 1}
			</td>
			<td>
				{$data.NIM_MHS}
			</td>
			<td>
				{$data.NM_PENGGUNA}
			</td>
			<td>
				{$data.NAMA_KELOMPOK}
			</td>
			<td>
				{$data.NM_KELURAHAN}
			</td>
			<td>
				{$data.NM_PROGRAM_STUDI}
			</td>
			<td>
				{$data.NM_FAKULTAS}
			</td>
		</tr>
		{/foreach}
	</table>
{/if}