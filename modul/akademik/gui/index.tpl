<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>
        <title>AKADEMIK - {$nama_pt}</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="../../css/reset.css" />
        <link rel="stylesheet" type="text/css" href="../../css/text.css" />
        <link rel="stylesheet" type="text/css" href="../../css/akademik_edit.css" />
        <link rel="stylesheet" type="text/css" href="../../css/jquery-ui-1.8.11.custom.css" />
        <link rel="stylesheet" type="text/css" href="includes/sortable/themes/green/style.css" />
        <script type="text/javascript">
            var defaultRel = 'perkuliahan';
            var defaultPage = 'master-perkuliahan.php';
            var id_fakultas = null;
        </script>
    </head>
    <body>
        <table class="clear-margin-bottom">
            <colgroup>
                <col />
                <col class="main-width"/>
                <col />
            </colgroup>
            <thead>
                <tr>
                    <td class="header-left"></td>
                    <td class="header-center" style="background-image: url('../../img/header/akademik-{$nama_singkat}.png')"> 
                        <div align="right">
                            {if $kdfak==0 }
                                <span style="font-size:32px">OPERATOR MKWU</span>	
                                <br/>
                                <span style="font-size:16px">Pendidikan</span>
                            {else}		

                                <span style="font-size:32px">{foreach $NAMAFAK as $prodi} {$prodi.NM_FAKULTAS} {/foreach}</span>
                                <br/>
                                <span style="font-size:16px">AKADEMIK - FAKULTAS</span>
                            {/if} 

                        </div>
                    </td>
                    <td class="header-right"></td>
                </tr>
                <tr>
                    <td class="tab-left"></td>
                    <td class="tab-center">   
                        <ul>
                            {foreach $modul_set as $m}
                                {if $m.AKSES == 1}
                                    <li><a href="#{$m.NM_MODUL}!{$m.PAGE}">{$m.TITLE}</a></li>
                                    <li class="divider"></li>
                                    {/if}
                                {/foreach}
                            <li><a class="disable-ajax" href="../../logout.php">Logout</a></li>
                        </ul>
                    </td>
                    <td class="tab-right"></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="body-left">&nbsp;</td>
                    <td class="body-center">
                        <table class="content-table">
                            <colgroup>
                                <col />
                                <col />
                            </colgroup>
                            <tr>
                                <td colspan="2" id="breadcrumbs" class="breadcrumbs">Navigation : <a href="#">Tab</a> / <a href="#">Menu</a></td>
                            </tr>
                            <tr>
                                <td id="menu" class="menu">

                                </td>
                                <td id="content" class="content">Content</td>
                            </tr>
                        </table>
                    </td>
                    <td class="body-right">&nbsp;</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td class="foot-left">&nbsp;</td>
                    <td class="foot-center">
                        <div class="footer-nav">
                            <a href="#">Home</a> | <a href="#">About</a> | <a href="#">Sitemap</a> | <a href="#">RSS</a> | <a href="#">Contact Us</a>
                        </div>
                        <div class="footer">Copyright &copy; 2015 - {$nama_pt} <br />Sistem Langitan NU</div>
                    </td>
                    <td class="foot-right">&nbsp;</td>
                </tr>
            </tfoot>
        </table>
        <script type="text/javascript" src="../../js/jquery-1.5.1.js"></script>
        <script type="text/javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
        <script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="../../js/cybercampus.ajax-1.0.js"></script>
        <script type="text/javascript" src="gui/tab.js"></script>
        <script type="text/javascript" src="includes/sortable/jquery.tablesorter.min.js"></script>
        <script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
        <script type="text/javascript" src="../../js/jquery-ui-timepicker-addon.js"></script>
        <script type="text/javascript" src="gui/datetimepicker.js"></script>
    </body>
</html>