<div class="center_title_bar">Distribusi IPK Mahasiswa Aktif {foreach item="list1" from=$NM_PRODI}{$list1.PRODI} {/foreach} {foreach item="list2" from=$THNSMT}{$list2.THN_SMT} {/foreach}</div>
<form action="report-prosentase-ipk.php" method="post" name="id_daftarmk" id="id_daftarmk">
<input type="hidden" name="action" value="view">
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td>
			 	Prodi : 
               <select name="kdprodi">
    		   <option value=''>------</option>
			   <option value='all-{$FAK}' style="font-weight: bold; color: #fff; background-color: #233d0e;">LIHAT SEMUA</option>
	 		   {foreach item="list" from=$T_PRODI}
    		   {html_options values=$list.ID_PROGRAM_STUDI output=$list.PRODI selected=$smarty.post.kdprodi}
	 		   {/foreach}
			   </select>
		     </td>
			 <td>
			 	Tahun/Semester : 
               <select name="kdthnsmt">
    		   <option value=''>------</option>
	 		   {foreach item="list_smt" from=$T_THNSMT}
    		   {html_options values=$list_smt.ID_SEMESTER output=$list_smt.THN_SMT selected=$smarty.post.kdthnsmt}
	 		   {/foreach}
			   </select>
		     </td>
			 <td>			   
			   <input type="submit" name="View" value="View">
		     </td> 
           </tr>
</table>
</form>
{if {$list1.PRODI} == '' && $list2.THNSMT == ''}
{else}
<div class="panel" id="panel1" style="display: block">
<br/>
{*
<p>
<input type=button name="cetak" value="Ekspor ke Excel" onclick="window.open('proses/dist-ipk-mhs-xls.php?smt={$KD_SMT}&thn={$KD_THN}&fak={$KD_FAK}&prodi={$KD_PRODI}','baru2');">&nbsp;
</p>
*}
		<table class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
             <th><center>No</center></th>
             <th><center>Jenjang</center></th>
             <th><center>Program Studi</center></th>
             <th><center>IPK<br/><2.00</center></th>
			 <th><center>2.00<=<br/>IPK<br/><2.50</center></th>
             <th><center>2.50<=<br/>IPK<br/><2.75</center></th>
			 <th><center>2.75<=<br/>IPK<br/><3.00</center></th>
			 <th><center>3.00<=<br/>IPK<br/><3.25</center></th>
			 <th><center>3.25<=<br/>IPK<br/><3.50</center></th>
			 <th><center>3.50<=<br/>IPK<br/><3.75</center></th>
			 <th><center>IPK<br/>>=3.75</center></th>
        </tr>
		</thead>
		   <tbody>
           {foreach name=test item="list" from=$T_MK}
		   <tr>
             <td>{$smarty.foreach.test.iteration}</td>
             <td>{$list.NM_JENJANG}</td>
             <td>{$list.NM_PROGRAM_STUDI}</td>
             <td><center>{$list.JUMLAH_A}<br/><font color="blue"><b>({$list.PERSEN_A}%)</b></font></center></td>
			 <td><center>{$list.JUMLAH_AB}<br/><font color="blue"><b>({$list.PERSEN_AB}%)</b></font></center></td>
             <td><center>{$list.JUMLAH_B}<br/><font color="blue"><b>({$list.PERSEN_B}%)</b></font></center></td>
			 <td><center>{$list.JUMLAH_BC}<br/><font color="blue"><b>({$list.PERSEN_BC}%)</b></font></center></td>
			 <td><center>{$list.JUMLAH_C}<br/><font color="blue"><b>({$list.PERSEN_C}%)</b></font></center></td>
			 <td><center>{$list.JUMLAH_D}<br/><font color="blue"><b>({$list.PERSEN_D}%)</b></font></center></td>
			 <td><center>{$list.JUMLAH_E}<br/><font color="blue"><b>({$list.PERSEN_E}%)</b></font></center></td>
			 <td><center>{$list.JUMLAH_F}<br/><font color="blue"><b>({$list.PERSEN_F}%)</b></font></center></td>
           </tr>
		   {foreachelse}
        <tr><td colspan="11"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
	</tbody>
   	</table>

	<table class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
           <tr>
             <th><center>No</center></th>
             <th><center>Tahun Angkatan</center></th>
             <th><center>IPK<br/><2.00</center></th>
			 <th><center>2.00<=<br/>IPK<br/><2.50</center></th>
             <th><center>2.50<=<br/>IPK<br/><2.75</center></th>
			 <th><center>2.75<=<br/>IPK<br/><3.00</center></th>
			 <th><center>3.00<=<br/>IPK<br/><3.25</center></th>
			 <th><center>3.25<=<br/>IPK<br/><3.50</center></th>
			 <th><center>3.50<=<br/>IPK<br/><3.75</center></th>
			 <th><center>IPK<br/>>=3.75</center></th>
           </tr>
	   </thead>
	   <tbody>
           {foreach name=data item="agk" from=$T_AGK}
		   <tr>
             <td>{$smarty.foreach.data.iteration}</td>
             <td>{$agk.THN_ANGKATAN_MHS}</td>
             <td><center>{$agk.JUMLAH_A}<br/><font color="blue"><b>({$agk.PERSEN_A}%)</b></font></center></td>
			 <td><center>{$agk.JUMLAH_AB}<br/><font color="blue"><b>({$agk.PERSEN_AB}%)</b></font></center></td>
             <td><center>{$agk.JUMLAH_B}<br/><font color="blue"><b>({$agk.PERSEN_B}%)</b></font></center></td>
			 <td><center>{$agk.JUMLAH_BC}<br/><font color="blue"><b>({$agk.PERSEN_BC}%)</b></font></center></td>
			 <td><center>{$agk.JUMLAH_C}<br/><font color="blue"><b>({$agk.PERSEN_C}%)</b></font></center></td>
			 <td><center>{$agk.JUMLAH_D}<br/><font color="blue"><b>({$agk.PERSEN_D}%)</b></font></center></td>
			 <td><center>{$agk.JUMLAH_E}<br/><font color="blue"><b>({$agk.PERSEN_E}%)</b></font></center></td>
			 <td><center>{$agk.JUMLAH_F}<br/><font color="blue"><b>({$agk.PERSEN_F}%)</b></font></center></td>
           </tr>
		   {foreachelse}
        <tr><td colspan="10"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
	</tbody>
   	</table>
</div>
{/if}