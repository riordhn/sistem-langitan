<div class="center_title_bar">TRANSAKSI KAPASISTAS</div>
<form action="ebsbed-trkap.php" method="post">
<table>
	<tr>
		<th colspan="6">PARAMETER</th>
	</tr>
	<tr>
			<td>Jenjang</td>
			<td>
				<select id="jenjang" name="jenjang">
                    <option value="">Semua</option> 
                   {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $smarty.post.jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}                    
                </select>
			</td>
			<td>Program Studi</td>
            <td>
                <select id="program_studi" name="program_studi">
                    <option value="">Semua</option>
					{foreach $prodi as $data}
						<option value="{$data.ID_PROGRAM_STUDI}" {if $smarty.post.program_studi==$data.ID_PROGRAM_STUDI}selected="true"{/if}>{$data.NM_PROGRAM_STUDI}</option>
					{/foreach}
                </select>
            </td>
		</tr>
		<tr>
			<td colspan="6" style="text-align:center">
				<input type="submit" value="Tampil" />
				<input type="hidden" value="show" name="mode" />
                <input type="hidden" value="{$fakultas}" name="fakultas" id="fakultas" />
			</td>
		</tr>
</table>
</form>

{if isset($ebsbed)}
<table>
	<tr>
		<th>PRODI</th>
		<th>THSMSTRKAP</th>
		<th>KDPTITRKAP</th>
		<th>KDJENTRKAP</th>
		<th>KDPSTTRKAP</th>
		<th>JMGETTRKAP</th>
		<th>JMCALTRKAP</th>
		<th>JMTERTRKAP</th>
		<th>JMDAFTRKAP</th>
		<th>JMMUNTRKAP</th>		
		<th>JMPINTRKAP</th>
		<th>TGAW1TRKAP</th>
		<th>TGAK1TRKAP</th>
		<th>TMRE1TRKAP</th>
		<th>TGAW2TRKAP</th>
		<th>TGAK2TRKAP</th>
		<th>TMRE2TRKAP</th>
		<th>MTKLHTRKAP</th>
		<th>KDEKSTRKAP</th>		
		<th>MTKLETRKAP</th>
		<th>SMPDKTRKAP</th>
		<th>JMPDKTRKAP</th>
		<th>MTPDKTRKAP</th>
		<th>JMHLKTRKAP</th>
		<th>JMHPRTRKAP</th>
	</tr>
	{foreach $ebsbed as $data}
	<tr>
		<td>{$data['NM_PROGRAM_STUDI']}</td>
		<td>{$data['TAHUN_SEMESTER']}</td>
		<td>001004</td>
		<td>{$data.KDJENMSPST}</td>
		<td>{$data.KDPSTMSPST}</td>
		<td>{$data.TOTAL}</td>
		<td>{$data.JML_PEMINAT}</td>
		<td>{$data.JML_DITERIMA}</td>
		<td>{$data.JML_DU}</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	{/foreach}
</table>
{/if}

{literal}
    <script type="text/javascript">
		$('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
    </script>
{/literal}