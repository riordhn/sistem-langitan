<div class="center_title_bar">TRANSAKSI MAHASISWA CUTI/ LULUS/ DO</div>
<form action="ebsbed-trlsm.php" method="post">
<table>
	<tr>
		<th colspan="9">PARAMETER</th>
	</tr>
	<tr>
			<td>Jenjang</td>
			<td>
				<select id="jenjang" name="jenjang">
                    <option value="">Semua</option> 
                   {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $smarty.post.jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}                    
                </select>
			</td>
			<td>Program Studi</td>
            <td>
                <select id="program_studi" name="program_studi">
                    <option value="">Semua</option>
					{foreach $prodi as $data}
						<option value="{$data.ID_PROGRAM_STUDI}" {if $smarty.post.program_studi==$data.ID_PROGRAM_STUDI}selected="true"{/if}>{$data.NM_PROGRAM_STUDI}</option>
					{/foreach}
                </select>
            </td>
            <td>Periode Wisuda</td>
            <td>
                <select id="periode_wisuda" name="periode_wisuda">
                    <option value="">Semua</option>
					{foreach $periode_wisuda as $data}
						<option value="{$data.ID_TARIF_WISUDA}" {if $smarty.post.periode_wisuda==$data.ID_TARIF_WISUDA}selected="true"{/if}>{$data.NM_TARIF_WISUDA}</option>
					{/foreach}
                </select>
            </td>
		</tr>
		<tr>
			<td colspan="8" style="text-align:center">
				<input type="submit" value="Tampil" />
				<input type="hidden" value="show" name="mode" />
				<input type="hidden" value="{$fakultas}" name="fakultas" id="fakultas" />
			</td>
		</tr>
</table>
</form>

{if isset($ebsbed)}

<table>
	<tr>
		<th>THSMSTRLSM</th>
		<th>KDPTITRLSM</th>
		<th>KDJENTRLSM</th>
		<th>KDPSTTRLSM</th>
		<th>NIMHSTRLSM</th>
   		<th>STMHSTRLSM</th>
		<th>TGLLSTRLSM</th>
		<th>SKSTTTRLSM</th>
		<th>NLIPKTRLSM</th>
		<th>NOSKRTRLSM</th>
		<th>TGLRETRLSM</th>
		<th>NOIJATRLSM</th>
		<th>STLLSTRLSM</th>
		<th>JNLLSTRLSM</th>
   		<th>BLAWLTRLSM</th>
		<th>BLAKHTRLSM</th>
		<th>NODS1TRLSM</th>
		<th>NODS2TRLSM</th>
		<th>NODS3TRLSM</th>
		<th>NODS4TRLSM</th>
		<th>NODS5TRLSM</th>
	</tr>
    {foreach $ebsbed as $data}
    <tr>
    	<td>{$data.THN_SEMESTER}</td>
        <td>001004</td>
        <td>{$data.KDJENMSPST}</td>
        <td>{$data.KDPSTMSPST}</td>
        <td>{$data.NIM_MHS}</td>
        <td>{$data.STATUS_MSMHS}</td>
        <td>{$data.TGL_LULUS}</td>
        <td>{$data.SKS}</td>
        <td>{$data.IPK}</td>
        <td>{$data.SK_YUDISIUM}</td>
        <td>{$data.TGL_SK_YUDISIUM}</td>
        <td>{$data.NO_IJASAH}</td>
        <td>{if $data.STATUS_MSMHS == 'L'}S{/if}</td>
        <td>{if $data.STATUS_MSMHS == 'L'}I{/if}</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    {/foreach}
</table>
{/if}


{literal}
    <script type="text/javascript">
        $('#fakultas').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		$('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
    </script>
{/literal}