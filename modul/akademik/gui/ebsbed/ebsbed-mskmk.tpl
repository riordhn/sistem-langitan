<div class="center_title_bar">MASTER MATA KULIAH/ KURIKULUM</div>
<form action="ebsbed-mskmk.php" method="post">
<table>
	<tr>
		<th colspan="6">PARAMETER</th>
	</tr>
	<tr>            
			<td>Jenjang</td>
			<td>
				<select id="jenjang" name="jenjang">
                    <option value="">Semua</option> 
                   {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $smarty.post.jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}                    
                </select>
			</td>
			<td>Program Studi</td>
            <td>
                <select id="program_studi" name="program_studi">
                    <option value="">Semua</option>
					{foreach $prodi as $data}
						<option value="{$data.ID_PROGRAM_STUDI}" {if $smarty.post.program_studi==$data.ID_PROGRAM_STUDI}selected="true"{/if}>{$data.NM_PROGRAM_STUDI}</option>
					{/foreach}
                </select>
            </td>
		
		<td>Jenis Mhs</td>
            <td>
                <select id="jenis_mhs" name="jenis_mhs">
                    <option value="">Semua</option>
					<option value="R" {if $smarty.post.jenis_mhs=="R"}selected="true"{/if}>Reguler</option>
					<option value="I" {if $smarty.post.jenis_mhs=="I"}selected="true"{/if}>Internasional</option>
					<option value="AJ" {if $smarty.post.jenis_mhs=="AJ"}selected="true"{/if}>Alih Jenis</option>
                </select>
            </td>
		</tr>
		<tr>
			<td colspan="8" style="text-align:center">
				<input type="submit" value="Tampil" />
				<input type="hidden" value="show" name="mode" />
                 <input type="hidden" value="{$fakultas}" name="fakultas" id="fakultas" />
			</td>
		</tr>
</table>
</form>

{if isset($ebsbed)}
<input type="button" value="Cetak" onclick="window.open('ebsbed-mskmk-excel.php?fakultas={$smarty.request.fakultas}&jenjang={$smarty.request.jenjang}&program_studi={$smarty.request.program_studi}');"/>
<table>
	<tr>
		<th>THSMSTBKMK</th>
		<th>KDPTITBKMK</th>
		<th>KDJENTBKMK</th>
		<th>KDPSTTBKMK</th>
		<th>KDKMKTBKMK</th>
		<th>NAKMKTBKMK</th>
		<th>SKSMKTBKMK</th>
		<th>SKSTMTBKMK</th>
		<th>SKSPRTBKMK</th>
		<th>SKSLPTBKMK</th>
		<th>SEMESTBKMK</th>
		<th>KDWPLTBKMK</th>
		<th>KDKURTBKMK</th>
		<th>KDKELTBKMK</th>
		<th>NODOSTBKMK</th>
		<th>JENJATBKMK</th>
		<th>PRODITBKMK</th>
		<th>STKMKTBKMK</th>
		<th>SLBUSTBKMK</th>
		<th>SAPPPTBKMK</th>
		<th>BHNAJTBKMK</th>
		<th>DIKTTTBKMK</th>
		<th>KDUTATBKMK</th>
		<th>KDKUGTBKMK</th>
		<th>KDLAITBKMK</th>
		<th>KDMPATBKMK</th>
		<th>KDMPBTBKMK</th>
		<th>KDMPCTBKMK</th>
		<th>KDMPDTBKMK</th>
		<th>KDMPETBKMK</th>
		<th>KDMPFTBKMK</th>
		<th>KDMPGTBKMK</th>
		<th>KDMPHTBKMK</th>
		<th>KDMPITBKMK</th>
		<th>KDMPJTBKMK</th>
		<th>CRMKLTBKMK</th>
		<th>PRSTDTBKMK</th>
		<th>SMGDSTBKMK</th>
		<th>RPSIMTBKMK</th>
		<th>CSSTUTBKMK</th>
		<th>DISLNTBKMK</th>
		<th>SDILNTBKMK</th>
		<th>CODLNTBKMK</th>
		<th>COLLNTBKMK</th>
		<th>CTXINTBKMK</th>
		<th>PJBLNTBKMK</th>
		<th>PBBLNTBKMK</th>
		<th>UJTLSTBKMK</th>
		<th>TGMKLTBKMK</th>
		<th>TGMODTBKMK</th>
		<th>PSTSITBKMK</th>
		<th>SIMULTBKMK</th>
		<th>LAINNTBKMK</th>
		<th>UJTL1TBKMK</th>
		<th>TGMK1TBKMK</th>
		<th>TGMO1TBKMK</th>
		<th>PSTS1TBKMK</th>
		<th>SIMU1TBKMK</th>
		<th>LAIN1TBKMK</th>
	</tr>
	{foreach $ebsbed as $data}
	<tr>
				<td>{$data.TAHUN_KURIKULUM}</td>
		<td>001004</td>
		<td>{$data.KDJENMSPST}</td>
		<td>{$data.KDPSTMSPST}</td>
		<td>{$data.KD_MATA_KULIAH}</td>
		<td>{$data.NM_MATA_KULIAH}</td>
		<td>{$data.KREDIT_SEMESTER}</td>
		<td>{$data.KREDIT_SEMESTER - $data.KREDIT_PRAKTIKUM}</td>
		<td>{if $data.KREDIT_PRAKTIKUM == ''}0{else}{$data.KREDIT_PRAKTIKUM}{/if}</td>
		<td>0</td>
		<td>{$data.TINGKAT_SEMESTER}</td>
		<td>A</td>
		<td>A</td>
		<td>A</td>
		<td>{$data.NIDN_DOSEN}</td>
		<td>{$data.KDJENMSPST}</td>
		<td>{$data.KDPSTMSPST}</td>
		<td>A</td>
		<td>Y</td>
		<td>Y</td>
		<td>Y</td>
		<td>Y</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td>0</td>
		<td>0</td>
		<td>0</td>
		<td>0</td>
		<td>0</td>
		<td>0</td>
		<td>0</td>
		<td>0</td>
		<td>0</td>
		<td>0</td>
		<td>0</td>
		<td>0</td>
	</tr>
	{/foreach}
</table>

{/if}


{literal}
    <script type="text/javascript">
		
		$('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
    </script>
{/literal}