<div class="center_title_bar">TRANSAKSI NILAI MAHASISWA</div>
<form action="ebsbed-trnlm.php" method="post">
<table>
	<tr>
		<th colspan="9">PARAMETER</th>
	</tr>
	<tr>
			<td>Semester</td>
            <td>
            	<select name="semester">
                	{foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $smarty.post.semester==$data.ID_SEMESTER}selected="true"{/if}>{$data.THN_AKADEMIK_SEMESTER} - {$data.NM_SEMESTER|upper}</option>
                    {/foreach}
                </select>
            </td>
			<td>Jenjang</td>
			<td>
				<select id="jenjang" name="jenjang">
                    <option value="">Semua</option> 
                   {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $smarty.post.jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}                    
                </select>
			</td>
			<td>Program Studi</td>
            <td>
                <select id="program_studi" name="program_studi">
                    <option value="">Semua</option>
					{foreach $prodi as $data}
						<option value="{$data.ID_PROGRAM_STUDI}" {if $smarty.post.program_studi==$data.ID_PROGRAM_STUDI}selected="true"{/if}>{$data.NM_PROGRAM_STUDI}</option>
					{/foreach}
                </select>
            </td>
			<td>Jenis Mhs</td>
            <td>
                <select id="jenis_mhs" name="jenis_mhs">
                    <option value="">Semua</option>
					<option value="R" {if $smarty.post.jenis_mhs=="R"}selected="true"{/if}>Reguler</option>
					<option value="I" {if $smarty.post.jenis_mhs=="I"}selected="true"{/if}>Internasional</option>
					<option value="AJ" {if $smarty.post.jenis_mhs=="AJ"}selected="true"{/if}>Alih Jenis</option>
                </select>
            </td>
		</tr>
		<tr>
			<td colspan="8" style="text-align:center">
				<input type="submit" value="Tampil" />
				<input type="hidden" value="show" name="mode" />
				<input type="hidden" value="{$fakultas}" name="fakultas" id="fakultas" />
			</td>
		</tr>
</table>
</form>


{if isset($ebsbed)}
<table>
	<tr>
		<th>THSMSTRNLM</th>
		<th>KDPTITRNLM</th>
		<th>KDJENTRNLM</th>
		<th>KDPSTTRNLM</th>
		<th>NIMHSTRNLM</th>
   		<th>KDKMKTRNLM</th>
		<th>NLAKHTRNLM</th>
		<th>BOBOTTRNLM</th>
		<th>KELASTRNLM</th>
	</tr>
	{foreach $ebsbed as $data}
	<tr>
		<td>{$data_smt['TAHUN']}</td>
		<td>001004</td>
		<td>{$data.KDJENMSPST}</td>
		<td>{$data.KDPSTMSPST}</td>
		<td>{$data.NIM_MHS}</td>
		<td>{$data.KD_MATA_KULIAH}</td>
		<td>{$data.NILAI_HURUF}</td>
		<td>{$data.NILAI_STANDAR_NILAI}</td>
		<td>{$data.NAMA_KELAS}</td>
	</tr>
	{/foreach}
</table>

{/if}


{literal}
    <script type="text/javascript">
		
		$('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
    </script>
{/literal}