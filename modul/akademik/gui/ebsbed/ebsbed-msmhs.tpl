<div class="center_title_bar">MASTER MAHASISWA</div>
<form action="ebsbed-msmhs.php" method="post">
<table>
	<tr>
		<th colspan="9">PARAMETER</th>
	</tr>
	<tr>
			<td>Jenjang</td>
			<td>
				<select id="jenjang" name="jenjang">
                    <option value="">Semua</option> 
                   {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $smarty.post.jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}                    
                </select>
			</td>
			<td>Program Studi</td>
            <td>
                <select id="program_studi" name="program_studi">
                    <option value="">Semua</option>
					{foreach $prodi as $data}
						<option value="{$data.ID_PROGRAM_STUDI}" {if $smarty.post.program_studi==$data.ID_PROGRAM_STUDI}selected="true"{/if}>{$data.NM_PROGRAM_STUDI}</option>
					{/foreach}
                </select>
            </td>
			<td>Jenis Mhs</td>
            <td>
                <select id="jenis_mhs" name="jenis_mhs">
                    <option value="">Semua</option>
					<option value="R" {if $smarty.post.jenis_mhs=="R"}selected="true"{/if}>Reguler</option>
					<option value="I" {if $smarty.post.jenis_mhs=="I"}selected="true"{/if}>Internasional</option>
					<option value="AJ" {if $smarty.post.jenis_mhs=="AJ"}selected="true"{/if}>Alih Jenis</option>
                </select>
            </td>
		</tr>
		<tr>
			<td colspan="8" style="text-align:center">
				<input type="submit" value="Tampil" />
				<input type="hidden" value="show" name="mode" />
                <input type="hidden" value="{$fakultas}" name="fakultas" id="fakultas" />
			</td>
		</tr>
</table>
</form>

{if isset($ebsbed)}
<table>
	<tr>
		<th>KDPTIMSMHS</th>
		<th>KDJENMSMHS</th>
		<th>KDPSTMSMHS</th>
		<th>NIMHSMSMHS</th>
		<th>NMMHSMSMHS</th>
		<th>SHIFTMSMHS</th>
		<th>TPLHRMSMHS</th>
		<th>TGLHRMSMHS</th>
		<th>KDJEKMSMHS</th>
		<th>TAHUNMSMHS</th>
		<th>SMAWLMSMHS</th>
		<th>BTSTUMSMHS</th>
		<th>ASSMAMSMHS</th>
		<th>TGMSKMSMHS</th>
		<th>TGLLSMSMHS</th>
		<th>STMHSMSMHS</th>
		<th>STPIDMSMHS</th>
		<th>SKSDIMSMHS</th>
		<th>ASNIMMSMHS</th>
		<th>ASPTIMSMHS</th>
		<th>ASJENMSMHS</th>
		<th>ASPSTMSMHS</th>
		<th>BISTUMSMHS</th>
		<th>PEKSBMSMHS</th>
		<th>NMPEKMSMHS</th>
		<th>PTPEKMSMHS</th>
		<th>PSPEKMSMHS</th>
		<th>NOPRMMSMHS</th>
		<th>NOKP1MSMHS</th>
		<th>NOKP2MSMHS</th>
		<th>NOKP3MSMHS</th>
		<th>NOKP4MSMHS</th>
	</tr>
	{foreach $ebsbed as $data}
	<tr>
		<td>001004</td>
		<td>{$data.KD_JENJANG}</td>
		<td>{$data.KDPSTMSPST}</td>
		<td>{$data.NIM_MHS}</td>
		<td>{$data.NM_PENGGUNA}</td>
		<td>R</td>
		<td>{$data.TMLAHIR}</td>
		<td>{$data.TGL_LAHIR_PENGGUNA}</td>
		<td>{$data.KELAMIN_PENGGUNA}</td>
		<td>{$data.THN_ANGKATAN_MHS}</td>
		<td>{$data.THN_SEMESTER}</td>
		<td>{$data.BWS}</td>
		<td></td>
		<td>{$data.TGL_TERDAFTAR_MHS}</td>
		<td>{$data.TGL_LULUS}</td>
		<td></td>
		<td>{$data.KD_JENJANG}</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	{/foreach}
</table>
{/if}

{literal}
    <script type="text/javascript">
		$('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
    </script>
{/literal}