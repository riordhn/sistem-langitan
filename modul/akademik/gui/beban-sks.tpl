{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[2,0],[0,0]]
		}
	); 
} 
);
</script>
{/literal}

<div class="center_title_bar">Beban SKS Maksimum </div>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
	  <th>IP Semester Minimum</th>
	  <th>Beban SKS Maksimum</th>
	  <th>Progam Studi</th>
	</tr>
	</thead>
	<tbody>
	{foreach name=list item="list" from=$T_BEBAN}
	{if $smarty.foreach.list.iteration % 2 == 0}
	<tr bgcolor="lightgray">
	{else}
	<tr>
	{/if}
	  <td><center>{$list.IPK_MINIMUM}</center></td>
	  <td><center>{$list.SKS_MAKSIMAL}</center></td>
	  <td>{$list.KETERANGAN}</td>
	</tr>
	{foreachelse}
	<tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>