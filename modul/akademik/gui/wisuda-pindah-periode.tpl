<div class="center_title_bar">PINDAH PERIODE WISUDA MAHASISWA</div>  
<form id="form1" name="form1" method="get" action="wisuda-pindah-periode.php">
<table>
<tr>
	<td>NIM</td>
	<td> : </td>
	<td><input type="text" name="nim" /></td>
	<td><input type="submit" value="Tampil" /></td>
</tr>
</table>
</form>

<h2><font color="#FF0000">{$pesan}</font></h2>

{if isset($ijasah)}
<form action="wisuda-pindah-periode.php" method="get" id="f2">
<table width="600">
  <tr>
    <th colspan="2" style="text-align:center">Biodata</th>
  </tr>
  <tr>
    <td>NIM</td>
    <td>{$ijasah[0]['NIM_MHS']}</td>
  </tr>
  <tr>
    <td>Nama Lengkap</td>
    <td>{$ijasah[0]['NM_PENGGUNA']}</td>
  </tr>
  <tr>
    <td>Program Studi</td>
    <td>{$ijasah[0]['NM_JENJANG']} - {$ijasah[0]['NM_PROGRAM_STUDI']}</td>
  </tr>
  <tr>
    <td>Periode Yudisium</td>
    <td>{$ijasah[0]['NM_TARIF_WISUDA']}</td>
  </tr>
  <tr>
    <td>Pindah Ke Periode Yudisium</td>
    <td>
		<select name="periode">
			{foreach $periode as $data}
				<option value="{$data.ID_TARIF_WISUDA}">{$data.NM_TARIF_WISUDA}</option>
			{/foreach}
		</select>
	</td>
  </tr>
    <tr>
    	<td colspan="2" style="text-align:center">
			<input type="hidden" name="nim" value="{$ijasah[0]['NIM_MHS']}" />
        	<input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>

{literal}
<script language="text/javascript">
            $('#f2').validate();
</script>
{/literal}

{/if}
