<div class="center_title_bar">Daftar Nilai MA {foreach item="list1" from=$NM_MK}{$list1.NM_MK} {/foreach} <br/>{foreach item="list2" from=$THNSMT}{$list2.THN_SMT} {/foreach}</div>
<form action="report-nilai.php" method="post" name="id_daftarmk" id="id_daftarmk">
<input type="hidden" name="action" value="view">
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
             <td>
			 	Semester : 
               <select name="smt" id="smt">
    		   <option value=''>------</option>
	 		   {foreach $DSMT as $data}
    		  		<option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER == $smt or $data.ID_SEMESTER == $smarty.request.smt} selected="selected" {/if}>{$data.THN_SMT}</option>
	 		   {/foreach}
			   </select>
		     </td>
           </tr>
           <tr>
             <td>
			 	Mata Ajar : 
               <select name="id_kelas_mk" id="id_kelas_mk">
    		   <option value=''>------</option>
	 		   {foreach $T_LIST_MK as $data}
               <option value="{$data.ID_KELAS_MK}" {if $data.ID_KELAS_MK == $smarty.request.id_kelas_mk} selected="selected" {/if}>{$data.NM_MATA_KULIAH} ({$data.NAMA_KELAS}) - {$data.NM_DOSEN}</option>
	 		   {/foreach}
			   </select>
		     </td> 
           </tr>
           <tr>
            <td>			   
			   <input type="submit" name="View" value="View">
               <input type="hidden" value="{$kdfak}" name="id_fakultas" id="id_fakultas" />
		     </td> 
           </tr>
</table>
</form>
{if {$list1.NM_MK} == '' && $list2.THNSMT == ''}
{else}
<div class="panel" id="panel1" style="display: block">
<br/>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr class="left_menu" align="center">
             <td width="5%" bgcolor="#333333"><font color="#FFFFFF">No</font></td>
             <td width="15%" bgcolor="#333333"><font color="#FFFFFF">NIM</font></td>
             <td width="50%" bgcolor="#333333"><font color="#FFFFFF">Nama Mahasiswa</font> </td>
             <td width="15%" bgcolor="#333333"><font color="#FFFFFF">Nilai Angka</font></td>
			 <td width="15%" bgcolor="#333333"><font color="#FFFFFF">Nilai Huruf</font></td>
           </tr>
           {foreach name=test item="list" from=$T_MK}
		   <tr>
             <td>{$smarty.foreach.test.iteration}</td>
             <td>{$list.NIM_MHS}</td>
             <td>{$list.NM_PENGGUNA}</td>
             <td>{$list.NILAI_ANGKA}</td>
			 <td>{$list.NILAI_HURUF}</td>
           </tr>
		   {foreachelse}
        <tr><td colspan="5"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
   	 </table>
</div>
{/if}

{literal}
    <script type="text/javascript">

		$('#smt').change(function(){
            $.ajax({
                type:'post',
                url:'getMataAjar.php',
                data:'id_fakultas='+$('#id_fakultas').val()+'&smt='+$('#smt').val(),
                success:function(data){
                    $('#id_kelas_mk').html(data);
                }                    
            })
        });
    </script>
{/literal}