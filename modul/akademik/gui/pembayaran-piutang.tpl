<div class="center_title_bar">Piutang Mahasiswa</div> 
{if isset($tampil)}
<table>
<tr>
	<th>No</th>
	<th>NIM</th>
	<th>Nama</th>
	<th>Fakultas</th>
	<th>Program Studi</th>
	<th>Status Akademik</th>
    <th>Piutang Semester</th>
</tr>
{$tampil}
<tr>
	<td colspan="7" style="text-align:center">
	<form name="f3" action="excel-pembayaran-piutang.php" method="get">
		<input type="hidden" value="{$fakultas}" name="fakultas" />
		<input type="button" value="Cetak" onclick="get_link_report()"/>
	</form>
	</td>
</tr>
</table>
{elseif isset($tampil_mhs)}
<table>
	<tr>
    	<th colspan="2">BIODATA</th>
    </tr>
    <tr>
    	<td>NIM</td>
        <td>{$cek_data['NIM_MHS']}</td>
    </tr>
    <tr>
    	<td>Nama</td>
        <td>{$cek_data.NM_PENGGUNA}</td>
    </tr>
    <tr>
    	<td>Prodi</td>
        <td>{$cek_data.NM_JENJANG} - {$cek_data.NM_PROGRAM_STUDI}</td>
    </tr>
    <tr>
    	<td>Jalur</td>
        <td>{$cek_data.NM_JALUR}</td>
    </tr>
</table>
<table bgcolor="#ffffff" cellspacing="1" cellpadding="1" border="0" width="760" style="font-size:9px">
	<tr bgcolor="Silver">
		<td>Thn Akademik</td>
		<td>Bank</td>
		<td>Tgl Bayar</td>
		{foreach $biaya_mhs as $data}
		<td>{$data.NM_BIAYA}</td>
		{/foreach}
		<td>Denda</td>
	</tr>
	{$tampil_mhs}
</table>
{/if}

{literal}
    <script type="text/javascript">
        function get_link_report(){
			if(document.location.hash == '#pembayaran-piutang_pembayaran!pembayaran-piutang.php'){
			link=document.location.hash.replace('#pembayaran-piutang_pembayaran!', '');
			}else{
            link=document.location.hash.replace('#admisi-evaluasi_pembayaran!', '');
			}
			 window.location.href='excel-'+link;
        }
    </script>
{/literal}