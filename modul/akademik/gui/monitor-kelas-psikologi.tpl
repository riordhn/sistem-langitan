{literal}
<link href="js/jquery.dataTables.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('table.display').dataTable({
		"sDom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>',
		"sPaginationType": "full_numbers",
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Semua"]]
	});
});
</script>
{/literal}


<div class="center_title_bar">MONITORING KAPASITAS KELAS </div>
<div id="tabs">
	<div id="tab1" {if $smarty.get.action == ''}class="tab_sel"{else}class="tab"{/if} align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" {if $smarty.get.action == 'updateview'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Update</div>
	<div id="tab3" {if $smarty.get.action == 'detail'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Peserta MA</div>
</div>
<div class="panel" id="panel1" style="display: {$disp1} ">
<p> </p>
	<table id="" class="display" cellspacing="0" width="100%">
		<thead>
           <tr>
             <th>Kode</th>
             <th>Nama Mata Ajar</td>
			 <th>SKS</th>
			 <th>KLS</th>
             <th>Prodi</th>
             <th>Hari/Ruang</th>
             <th>Kpst</th>
			 <th>Terisi</th>
			 <th>KRS</th>
			 <th>KPRS</th>
             <th>Aksi</th>
             <th>Keterangan</th>
           </tr>
		</thead>
		<tbody>
           {foreach name=test item="list" from=$T_MK}
		   {if $list.KAPASITAS_KELAS_MK - $list.KLS_TERISI == 0}
		   <tr bgcolor="#ff828e">
   		   {elseif $list.KAPASITAS_KELAS_MK - $list.KLS_TERISI <= 5}
		   <tr bgcolor="#f5f2a3">
		   {else}
		   <tr>
		   {/if}
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td><center>{$list.KREDIT_SEMESTER}</center></td>
			 <td><center>{$list.NAMA_KELAS}</center></td>
             <td>{$list.PRODI}</center></td>
			 <td><center>{$list.NM_JADWAL_HARI}<br>{$list.KPST_RUANG}</center></td>
			 <td><center>{$list.KAPASITAS_KELAS_MK}</center></td>
			 <td>
				<center>
					<button style="font-weight:bold;background:transparent;border:none;" onclick="window.location.href = '/modul/akademik/#aktivitas-monitor!monitor-kelas-psikologi.php?action=detail&id_kelas_mk={$list.ID_KELAS_MK|base64_encode}'" alt="Daftar peserta" title="Daftar peserta">{$list.KLS_TERISI}</button>
				</center>
			</td>
			 <td><center>{$list.KRS}</center></td>
			 <td><center>{$list.KPRS}</center></td>
             <td>
				<center>
					<img src="includes/img/edit.png" border="0" alt="Edit" title="Edit" style="cursor:pointer;" onclick="window.location.href = '/modul/akademik/#aktivitas-monitor!monitor-kelas-psikologi.php?action=updateview&id_klsmk={$list.ID_KELAS_MK|base64_encode}&id_jk={$list.ID_JADWAL_KELAS|base64_encode}'" />
				</center>
			 </td>
			 <td>{$list.KETERANGAN}</td>
           </tr>
        {/foreach}
		</tbody>
		<tfoot>
           <tr>
             <th>Kode</th>
             <th>Nama Mata Ajar</td>
			 <th>SKS</th>
			 <th>KLS</th>
             <th>Prodi</th>
             <th>Hari/Ruang</th>
             <th>Kpst</th>
			 <th>Terisi</th>
			 <th>KRS</th>
			 <th>KPRS</th>
             <th>Aksi</th>
             <th>Keterangan</th>
           </tr>
		</tfoot>
   	</table>
<p><br>&nbsp;</p>
</div>

<div class="panel" id="panel2" style="display: {$disp2}">
{if $smarty.get.action == 'updateview'}
<p> </p>
		<form name="update_kelas" action="monitor-kelas-psikologi.php" method="post" onSubmit="return cek_kapasitas();">
			 <input type="hidden" name="action" value="update1" >
			  <table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="3"><strong> Update Kelas </strong></td></tr>
					{foreach item="ubah" from=$TJAF}
					<input type="hidden" name="id_kelas_mk" value="{$ubah.ID_KELAS_MK}" />
					<input type="hidden" name="id_jk" value="{$ubah.ID_JADWAL_KELAS}" />
					<tr>
					  <td width="29%" >Kode Mata Kuliah</td>
					  <td width="2%" >:</td>
					  <td width="69%" >{$ubah.KD_MATA_KULIAH}</td>
					</tr>
					<tr>
					  <td>Nama Mata Kuliah</td>
					  <td>:</td>
					  <td>{$ubah.NM_MATA_KULIAH}</td>
					</tr>
					<tr>
					  <td>Kelas</td>
					  <td>:</td>
					  <td>{$ubah.NAMA_KELAS}</td>
				    </tr>
					<tr>
					  <td>KAPASITAS KELAS</td>
					  <td>:</td>
					  <td>
					   {if $ubah.KAPASITAS_KELAS_MK >= $ubah.KAPASITAS_RUANGAN}
					   {$ubah.KAPASITAS_KELAS_MK} <font color="red">[Kapasitas kelas penuh]</font> <input name="kap_ruang" id="kap_ruang" type="hidden" value="{$ubah.KAPASITAS_RUANGAN}"/>
					   {else}
						<input name="kap_kelas" id="kap_kelas" type="text" size="3" maxlength="3" class="required" value="{$ubah.KAPASITAS_KELAS_MK}"/> [WAJIB DI-ISI]<input name="kap_ruang" id="kap_ruang" type="hidden" value="{$ubah.KAPASITAS_RUANGAN}"/>
					   {/if}
					   </td>
				    </tr>
					<tr>
					  <td>Rencana Perkuliahan</td>
					  <td>:</td>
					  <td><input name="ren_kul" id="ren_kul" type="text" class="required" size="3" maxlength="3" class="required" value="{$ubah.JUMLAH_PERTEMUAN_KELAS_MK}"/> [WAJIB DI-ISI]</td>
				    </tr>
					<tr>
					  <td>Hari</td>
					  <td>:</td>
					  <td>{$ubah.NM_JADWAL_HARI}</td>
				    </tr>
					<tr>
					  <td>Jam</td>
					  <td>:</td>
					  <td>{$ubah.NM_JADWAL_JAM}</td>
				    </tr>
					<tr>
					  <td>Ruangan</td>
					  <td>:</td>
					  <td>
                      	<select name="ruangan" id="ruangan">
                            {foreach item="ruangan" from=$T_RUANG}
    		   				{html_options values=$ruangan.ID_RUANGAN output=$ruangan.RUANG selected=$ubah.ID_RUANGAN}
	 		   				{/foreach}
                          </select>
                      </td>
				    </tr>
					{/foreach}
          </table>
<p><input type="submit" name="Simpan1" value="Simpan"></p>
</form>
{else}
<p> </p>
{/if}
</div>

<div class="panel" id="panel3" style="display: {$disp3} ">
{if $smarty.get.action == 'detail'}
<p> </p>
<table>
	{foreach item="info" from=$T_INFO}
	<tr>
		<td width="29%" >Kode Mata Ajar / SKS / Kelas</td>
		<td width="2%" >:</td>
		<td width="69%" >{$info.KD_MATA_KULIAH} / {$info.KREDIT_SEMESTER} / {$info.NAMA_KELAS}</td>
	</tr>
	<tr>
		<td width="29%" >Nama Mata Ajar</td>
		<td width="2%" >:</td>
		<td width="69%" >{$info.NM_MATA_KULIAH}</td>
	</tr>
	<tr>
		<td width="29%" >Tim Pengajar</td>
		<td width="2%" >:</td>
		<td width="69%" style="padding-left:10px;"><br><ol>{$info.TIM}</li></ol></td>
	</tr>
		{/foreach}
</table>
	<table id="" class="display" cellspacing="0" width="100%">
		<thead>
           <tr>
				<th>NIM</th>
				<th>Nama Mahasiswa</th>
				<th>Prodi</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
				{foreach name=test item="list" from=$T_DETAIL}
				{if $list.STATUS == "Belum Disetujui"}
			<tr bgcolor="#f5f2a3">
				{else}
			<tr>
				{/if}
				<td>{$list.NIM_MHS}</td>
				<td>{$list.NM_PENGGUNA}</td>
				<td>{$list.NM_PROGRAM_STUDI}</td>
				{if $list.STATUS == "Belum Disetujui"}
				<td><center><img src="includes/img/forbidden.png" border="0" alt="{$list.STATUS}" title="{$list.STATUS}"/><center></td>
				{else}
				<td><center><img src="includes/img/success.png" border="0" alt="{$list.STATUS}" title="{$list.STATUS}"/><center></td>
				{/if}
			</tr>
				{/foreach}
		</tbody>
		<tfoot>
           <tr>
				<th>NIM</th>
				<th>Nama Mahasiswa</th>
				<th>Prodi</th>
				<th>Status</th>
			</tr>
		</tfoot>
	</table>
{else}
<p> </p>
{/if}
</div>