{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
            10: { sorter: false }
		}
		}
	); 
} 
);
</script>
{/literal}

<div class="center_title_bar">MONITORING PESERTA REMIDI </div>
<div id="tabs">
	<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');"><a href="mon-pst-rmd.php">Rincian </a></div>
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Peserta MA</div>
</div>
<div class="panel" id="panel1" style="display: {$disp1} ">
<p> </p>
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
             <th width="9%">Kode</th>
             <th width="25%">Nama Mata Ajar</font> </td>
			 <th width="5%">SKS</th>
			 <th width="5%">KLS</th>
             <th width="5%">STS</th>
             <th width="5%">PRODI</th>
             <th width="6%">Peserta</th>
			 <th class="noheader" width="15%">Aksi</th>
           </tr>
		</thead>
		<tbody>
           {foreach name=test item="list" from=$T_MK}
		   {if $list.KAPASITAS_KELAS_MK - $list.KLS_TERISI == 0}
		   <tr bgcolor="red">
   		   {elseif $list.KAPASITAS_KELAS_MK - $list.KLS_TERISI <= 5}
		   <tr bgcolor="yellow">
		   {else}
		   <tr>
		   {/if}
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td><center>{$list.KREDIT_SEMESTER}</center></td>
			 <td><center>{$list.NAMA_KELAS}</center></td>
             <td><center>{$list.STATUS}</center></td>
             <td>{$list.NM_JENJANG} - {$list.NM_PROGRAM_STUDI}</center></td>
			 <td><center><a href="mon-pst-rmd.php?action=detail&id_kelas_mk={$list.ID_KELAS_MK}&kodemk={$list.KD_MATA_KULIAH}">{$list.KLS_TERISI}</a></td>
             <td><center>CETAK</center></td>
           </tr>
		   {foreachelse}
        <tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
   	</table>
</div>
		
{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable1").tablesorter(
		{
		sortList: [[0,0]],
		}
	); 
} 
);
</script>
{/literal}
<div class="panel" id="panel2" style="display: {$disp2} ">
<p> </p>
<table>
	{foreach item="info" from=$T_INFO}
	<tr>
		<td width="29%" >Kode Mata Ajar / SKS / Kelas</td>
		<td width="2%" >:</td>
		<td width="69%" >{$info.KD_MATA_KULIAH} / {$info.KREDIT_SEMESTER} / {$info.NAMA_KELAS}</td>
	</tr>
	<tr>
		<td width="29%" >Nama Mata Ajar</td>
		<td width="2%" >:</td>
		<td width="69%" >{$info.NM_MATA_KULIAH}</td>
	</tr>
	<tr>
		<td width="29%" >Tim Pengajar</td>
		<td width="2%" >:</td>
		<td width="69%" ><br><ol>{$info.TIM}</li></ol></td>
	</tr>
		{/foreach}
</table>
	<table id="myTable1" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
				<th width="10%">NIM</th>
				<th width="35%">Nama Mahasiswa</th>
				<th width="20%">Prodi</th>
				<th width="10%">Status</th>
				<th width="10%">Baru-Ulang</th>
			</tr>
		</thead>
		<tbody>
				{foreach name=test item="list" from=$T_DETAIL}
				{if $list.STATUS == "Blm Disetujui"}
			<tr bgcolor="red">
				{else}
			<tr>
				{/if}
				<td>{$list.NIM_MHS}</td>
				<td>{$list.NM_PENGGUNA}</td>
				<td>{$list.NM_PROGRAM_STUDI}</td>
				<td>{$list.STATUS}</td>
				<td>{$list.STATUS1}</td>
			</tr>
				{foreachelse}
			<tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
				{/foreach}
		</tbody>
	</table>
</div>

{literal}
 <script>$('form').validate();</script>
{/literal}