{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[0,0]],
		}
	); 
} 
);
</script>
{/literal}

<div class="center_title_bar">Data KHS Mahasiswa {foreach item="list1" from=$NM_PRODI}{$list1.PRODI}{/foreach}, Tahun Akademik {foreach item="list2" from=$THN_SMT}{$list2.THN_SMT}{/foreach}</div>  
<form action="rekap-khs-psikologi.php" method="post" name="id_daftarmk" id="id_daftarmk">
<input type="hidden" name="action" value="view" >
<p>
			   Prodi : 
               <select name="kdprodi">
    		   <option value=''>-- PRODI --</option>
	 		   {foreach item="list" from=$T_PRODI}
    		   {html_options  values=$list.ID_PROGRAM_STUDI output=$list.PRODI}
	 		   {/foreach}
			   </select>
			   Tahun/Semester : 
               <select name="kdthnsmt">
    		   <option value=''>------</option>
	 		   {foreach item="list_smt" from=$T_THNSMT}
    		   {html_options values=$list_smt.ID_SEMESTER output=$list_smt.THN_SMT}
	 		   {/foreach}
			   </select>
			   <input type="submit" name="View" value="View">
</p>
</form>	

{if {$list1.PRODI} == ''}
{else}
<p>
<input type=button name="cetak" value="Ekspor Data ke MS. Excel" onclick="window.open('proses/rekap-khs-psikologi-xls.php?prodi={$PRODI}&smt={$SMT}&nm_prodi={$list1.PRODI}','baru2');">&nbsp;
<input type=button name="cetak" value="Ekspor Data ke MS. Word" onclick="window.open('proses/rekap-ipk-psikologi-doc.php?prodi={$PRODI}&smt={$SMT}&nm_prodi={$list1.PRODI}','baru2');">&nbsp;
</p>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
             <th>Nim</th>
             <th>Nama Mahasiswa</th>
             <th>IPK</th>
           </tr>
		</thead>
		<tbody>
		   {foreach name=test item="list" from=$T_MK}
           <tr>
             <td>{$list.NIM_MHS}</td>
             <td>{$list.NM_PENGGUNA}</td>
             <td><center>{$list.IPK}</center></td>
           </tr>
            {foreachelse}
        <tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
</table>
{/if}