{literal}
<script language="javascript" type="text/javascript" src="js/datetimepicker.js"></script>
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[0,0]]
		}
	); 
} 
);

$(document).ready(function() 
{ 
    $("#myTable1").tablesorter(
		{
		sortList: [[1,0]]
		}
	); 
} 
);
</script>
{/literal}
<div class="center_title_bar">Detail Presensi Mata Ajar </div>
	<div id="tabs">
	<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Materi-Dos</div>
	<div id="tab3" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Presensi Mhs</div>
	<div id="tab4" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('4');">Edit Materi-Dos</div>
</div>
<div class="panel" id="panel1" style="display: {$disp1} ">
<p> </p>
	<table>
	{foreach item="dtkelas" from=$DT_KELAS}
	  <tr>
		<td>Kode Mata Ajar</td>
		<td>:</td>
		<td>{$dtkelas.KD_MATA_KULIAH}</td>
	  </tr>
	  <tr>
		<td>Nama Mata Ajar</td>
		<td>:</td>
		<td>{$dtkelas.NM_MATA_KULIAH}</td>
	  </tr>
	  <tr>
		<td>SKS</td>
		<td>:</td>
		<td>{$dtkelas.KREDIT_SEMESTER}</td>
	  </tr>
	  <tr>
		<td>Kelas</td>
		<td>:</td>
		<td>{$dtkelas.NAMA_KELAS}</td>
	  </tr>
	  <tr>
		<td>Ruang</td>
		<td>:</td>
		<td>{$dtkelas.NM_RUANGAN}</td>
	  </tr>
	 {/foreach}
	</table>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
	 <th><center>Kuliah ke</center></th>
	 <th><center>Tgl</center></th>
	 <th><center>Jam</center></th>
	 <th><center>Materi</center></th>
	 <th><center>Dosen</center></th>
	 <th><center>Hadir</center></th>
	 <th><center>Absen</center></th>
	 <th><center>Presensi</center></th>
	 <th><center>Aksi</center></th>
	</tr>
	</thead>
	<tbody>
		{foreach name=test item="presensi" from=$DATA_PRESENSI}
	<tr>
	 <td><center>{$smarty.foreach.test.iteration}</center></td>
	 <td><center><a href="entry-presensi-psikologi.php?action=edit&id_presensi={$presensi.IDPRESENSI}&idkelas={$idkelas}&ed=1">{$presensi.TGL_PRESENSI_KELAS}</a></center></td>
	 <td><center>{$presensi.JAM}</center></td>
	 <td>{$presensi.ISI_MATERI_MK}</td>
	 <td><ol>{$presensi.NMDOS}</ol></td>
	 <td><center>{$presensi.HADIR}</center></td>
	 <td><center>{$presensi.ABSEN}</center></td>
	 {if $presensi.HADIR==0 && $presensi.ABSEN==0}
	 <td><center><a href="entry-presensi-psikologi.php?action=presensi&id_presensi={$presensi.IDPRESENSI}&idkelas={$idkelas}&id=1" onclick="displayPanel('3')">Entry</a></center></td>
	 {else}
	 <td><center><a href="entry-presensi-psikologi.php?action=presensi&id_presensi={$presensi.IDPRESENSI}&idkelas={$idkelas}&id=2" onclick="displayPanel('3')">Edit</a></center></td>
	 {/if}
	  <td><center><a href="entry-presensi-psikologi.php?action=hapus&id_presensi={$presensi.IDPRESENSI}&idkelas={$idkelas}&id_materi={$presensi.ID_MATERI_MK}">Hapus</a></center></td>
	</tr>
	 {foreachelse}
		<tr><td colspan="9"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
	</tbody>
	</table>
</div>

<div class="panel" id="panel2" style="display: {$disp2}">
<p> </p>
<form action="entry-presensi-psikologi.php" method="post" >
<input type="hidden" name="action" value="add" >
<input type="hidden" name="idkelas" value={$idkelas} >
	<table>
                  <tr>
                    <td width="20%">Tgl Kuliah</td>
                    <td width="1%">:</td>
					<td width="79%"><input type="Text" name="calawal" value="" id="calawal" maxlength="25" size="25" onClick="NewCssCal('calawal','ddmmmyyyy');"></td>
					{*<input type="button" value="" style="background-image:url(includes/cal/cal.gif); background-repeat:no-repeat; background-position:center; border:none;" onClick="NewCssCal('calawal','ddmmmyyyy');" /></td>*}
                  </tr>
				  <tr>
                    <td>Jam Mulai Kuliah</td>
                    <td>:</td>
					<td>
					<select name="jam_mulai">
					{foreach item="jam" from=$DT_JAM}
					{html_options values=$jam.JAM_MULAI output=$jam.JAM_MULAI}
					{/foreach}
					</select>&nbsp;
					<input type="text" name="menit_mulai" value="00" maxlength="2" style="width:30px;">
					</td>
                  </tr>
				  <tr>
                    <td>Jam Selesai Kuliah</td>
                    <td>:</td>
					<td>
					<select name="jam_selesai">
					{foreach item="jam1" from=$DT_JAM1}
					{html_options values=$jam1.JAM_SELESAI output=$jam1.JAM_SELESAI}
					{/foreach}
					</select>&nbsp;
					<input type="text" name="menit_selesai" value="50" maxlength="2" style="width:30px;">
					</td>
                  </tr>
				  {foreach name=nomer item="dosen" from=$T_DOSEN}
				  <tr>
                    <td>Dosen Pengajar-{$smarty.foreach.nomer.iteration}</td>
                    <td>:</td>
					<td><input name="dosen{$smarty.foreach.nomer.iteration}" type="checkbox" value="{$dosen.ID_DOSEN}" />{$dosen.NAMA}</td>

                  </tr>
				  {/foreach}
				  <input type="hidden" name="counter1" value="{$smarty.foreach.nomer.iteration}" >
				  <tr>
                    <td>Materi</td>
                    <td>:</td>
					<td><textarea name="materi" cols="60"></textarea></td>
                  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td><input type="submit" name="simpan" value="Simpan"></td>
					</tr>
        </table>
</form>
</div>
{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable1").tablesorter(
		{
		sortList: [[1,0]]
		}
	); 
} 
);

$(document).ready(function() 
{ 
    $("#myTable1").tablesorter(
		{
		sortList: [[1,0]]
		}
	); 
} 
);
</script>
{/literal}
<div class="panel" id="panel3" style="display: {$disp3}">
<p> </p>
<form action="entry-presensi-psikologi.php" method="post" >
<input type="hidden" name="action" value="masuk" >
<input type="hidden" name="id_presensi" value={$id_presensi} >
<input type="hidden" name="idkelas" value={$idkelas} >
<table id="myTable1" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
			     <th><center>No</center></th>
				 <th><center>NIM</center></th>
				 <th><center>Nama</center></th>
				 <th><center>Status</center></th>
				 <th><center>Absen</center></th>
			  </tr>
	</thead>
	<tbody>
			  {foreach name=presen item="pst" from=$PESERTA}
			  {if $smarty.foreach.presen.iteration % 2 == 0}
			  <tr bgcolor="lightgray">
			  {else}
			  <tr>
			  {/if}
			     <td><center>{$smarty.foreach.presen.iteration}</center></td>
				 <td>{$pst.NIM_MHS}</td>
				 <td>{$pst.NM_PENGGUNA}</td>
				 <td><center>{$pst.STKRS}</center></td>
				 <td><center><input name="mhs{$smarty.foreach.presen.iteration}" type="checkbox" value="{$pst.ID_MHS}" {$pst.TANDA} /></center></td>
			   </tr>
			     {foreachelse}
        			<tr><td colspan="5"><em>Data tidak ditemukan</em></td></tr>
        			{/foreach}
				<input type="hidden" name="counter" value="{$smarty.foreach.presen.iteration}" >
	</tbody>
  </table>
		{if $stt==1}
		<input type="hidden" name="pilihan" value="1" >
		<tr>
		<td><div align="left"><input type="submit" name="proses" value="Proses"></div></td>
		</tr>
		{else}
		<input type="hidden" name="pilihan" value="2" >
		<tr>
		<td><div align="left"><input type="submit" name="proses" value="Update"></div></td>
		</tr>
		{/if}
</form>
</div>

<div class="panel" id="panel4" style="display: {$disp4}">
<p> </p>
<form action="entry-presensi-psikologi.php" method="post" >
<input type="hidden" name="action" value="update" >
<input type="hidden" name="idkelas" value={$idkelas} >
<input type="hidden" name="id_presensi" value={$id_presensi} >
	<table>
				 {foreach item="list" from=$DT_EDIT}
                  <tr>
                    <td width="20%">Tgl Kuliah</td>
                    <td width="1%">:</td>
					<td width="79%"><input type="Text" name="calawal1" value="{$list.TGL_PRESENSI_KELAS}" id="calawal1"  maxlength="25" size="25" onClick="NewCssCal('calawal1','ddmmmyyyy');"></td>
					{*<input type="button" value="" style="background-image:url(includes/cal/cal.gif); background-repeat:no-repeat; background-position:center; border:none;" onClick="NewCal('calawal1','ddmmmyyyy');" />*}
                  </tr>
				  <tr>
                    <td>Jam Mulai Kuliah</td>
                    <td>:</td>
					<td>
					<select name="jam_mulai">
					{foreach item="jam" from=$DT_JAM}
					{html_options values=$jam.JAM_MULAI output=$jam.JAM_MULAI selected=$list.JAM_MULAI}
					{/foreach}
					</select>&nbsp;
					<input type="text" name="menit_mulai" value="{$list.MENIT_MULAI}" maxlength="2" style="width:30px;">
					</td>
                  </tr>
				  <tr>
                    <td>Jam Selesai Kuliah</td>
                    <td>:</td>
					<td>
					<select name="jam_selesai">
					{foreach item="jam1" from=$DT_JAM1}
					{html_options values=$jam1.JAM_SELESAI output=$jam1.JAM_SELESAI selected=$list.JAM_SELESAI}
					{/foreach}
					</select>&nbsp;
					<input type="text" name="menit_selesai" value="{$list.MENIT_SELESAI}" maxlength="2" style="width:30px;">
					</td>
                  </tr>

				  <tr>
                    <td>Materi</td>
                    <td>:</td>
					<td><textarea name="materi" cols="60">{$list.ISI_MATERI_MK}</textarea></td>
                  </tr>
				  {/foreach}
				  {foreach name=nomer item="dosen" from=$DOS_MASUK}
				  <tr>
                    <td>Dosen Pengajar-{$smarty.foreach.nomer.iteration}</td>
                    <td>:</td>
					<td><input name="dosen{$smarty.foreach.nomer.iteration}" type="checkbox" value="{$dosen.ID_DOSEN}" {$dosen.TANDA} />{$dosen.NAMA}</td>

                  </tr>
				  {/foreach}
				  <input type="hidden" name="counter1" value="{$smarty.foreach.nomer.iteration}" >
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td><input type="submit" name="update" value="Update"></td>
					</tr>
        </table>

</form>
</div>

