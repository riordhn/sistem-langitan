{literal}
<script language="javascript" type="text/javascript" src="js/datetimepicker.js"></script>
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[1,0],[3,0]],
		headers: {
            12: { sorter: false }
		}
		}
	);
}
);

function hapus(hapus_jadwal){
	$.ajax({
	type: "POST",
	url: "uas-sp-fkg.php",
	data: "action=del&hapus="+hapus_jadwal,
	cache: false,
	success: function(data){
		if (data == ' ')
		{
			window.location.reload(true);
		}
		else
		{
			alert('gagal hapus');
		}
	}
	});
}

function reset(reset_pst){
	$.ajax({
	type: "POST",
	url: "uas-sp-fkg.php",
	data: "action=reset_pst&reset="+reset_pst,
	cache: false,
	success: function(data){
		if (data == ' ')
		{
			window.location.reload(true);
		}
		else
		{
			alert('gagal hapus');
		}
	}
	});
}
</script>
{/literal}

<div class="center_title_bar">JADWAL UAS SP</div>
<div id="tabs">
	<div id="tab1" {if $smarty.get.action == 'penjadwalan' || $smarty.get.action == 'updateview' || $smarty.get.action == 'pengawas' || $smarty.get.action == 'peserta' || $smarty.get.action == 'list'}class="tab"{else}class="tab_sel"{/if} align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" {if $smarty.get.action == 'penjadwalan'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Penjadwalan</div>
	<div id="tab3" {if $smarty.get.action == 'updateview'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Update</div>
	<div id="tab4" {if $smarty.get.action == 'pengawas'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('4');">Pengawas</div>
	<div id="tab5" {if $smarty.get.action == 'peserta' || $smarty.get.action == 'list'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('5');">Peserta</div>
</div>

<div class="panel" id="panel1" style="display: {$disp1} ">
<p><br/><input type="button" name="cetak" value="Cetak Jadwal UAS SP" onClick="window.open('proses/jadwal-ujian-cetak.php?cetak=84&smt={$SMT}','baru');"></p>
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
             <th>Kode</th>
             <th>Nama Mata Ajar</td>
			 <th>SKS</th>
			 <th>KLS</th>
             <th>Prodi</th>
             <th>Jenis</th>
             <th>Tanggal</th>
             <th>Jam</th>
			 <th>Ruang</th>
             <th>Kpst</th>
			 <th>Pst</th>
			 <th>Tim Pengawas</th>
             <th class="noheader">Aksi</th>
           </tr>
		</thead>
		<tbody>
           {foreach item="list" from=$T_MK}
		   {if $list.TGL_UJIAN == '' || $list.NM_RUANGAN == '' || $list.KLS_TERISI == '' || $list.TIM == '<li><font color="black"> , </font>'}
		   <tr bgcolor="#ff828e">
		   {else}
		   <tr>
		   {/if}
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td><center>{$list.KREDIT_SEMESTER}</center></td>
			 <td><center>{$list.NAMA_KELAS}</center></td>
             <td>{$list.PRODI}</center></td>
			 <td>{$list.NM_UJIAN_MK}</td>
			 <td><center>{$list.TGL_UJIAN}</center></td>
			 <td><center>{$list.JAM}</center></td>
			 <td><center>{$list.NM_RUANGAN}</center></td>
			 <td><center>{$list.KAPASITAS_UJIAN}</center></td>
			 {if $list.KLS_TERISI == 0}
			 <td><center><a href="uas-sp-fkg.php?action=list&kelas={$list.ID_KELAS_MK}&ujian={$list.ID_UJIAN_MK}" alt="Tambah peserta manual" title="Tambah peserta manual">Plot manual</a></center></td>
			 {else}
			 <td><center><a href="uas-sp-fkg.php?action=list&kelas={$list.ID_KELAS_MK}&ujian={$list.ID_UJIAN_MK}" alt="Detail peserta" title="Detail peserta">{$list.KLS_TERISI}</a></center></td>
			 {/if}
			 <td>{$list.TIM}</td>
			 <td>
				<ul>
				<li><a href="uas-sp-fkg.php?action=updateview&ujian={$list.ID_UJIAN_MK}" alt="Update jadwal" title="Update jadwal">Update</a></li>
				<li><a href="uas-sp-fkg.php" name="hapus_jadwal" id="hapus_jadwal{$list.ID_UJIAN_MK}" alt="Hapus jadwal" title="Hapus jadwal" onclick="javascript:if(confirm('Anda yakin untuk menghapus jadwal?'))hapus({$list.ID_UJIAN_MK});">Hapus</a></li>
				<li><a href="uas-sp-fkg.php?action=peserta" alt="Ploting peserta" title="Ploting peserta">Plot otomatis</a></li>
				<li><a href="uas-sp-fkg.php?action=addjadwal&kls={$list.ID_KELAS_MK}&ujian={$list.ID_UJIAN_MK}" alt="Tambah ruang" title="Tambah ruang">Tambah</a></li>
				</ul>
			</td>
           </tr>
		   {foreachelse}
        <tr><td colspan="13"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
   	</table>
</div>

{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable1").tablesorter(
		{
		sortList: [[1,0],[3,0]],
		headers: {
            9: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
<div class="panel" id="panel2" style="display: {$disp2} ">
<p> </p>
	<table id="myTable1" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
             <th>Kode</th>
             <th>Nama Mata Ajar</td>
			 <th>SKS</th>
			 <th>KLS</th>
             <th>Prodi</th>
			 <th>Hari</th>
			 <th>Ruang</th>
             <th>Kpst</th>
			 <th>Pst</th>
             <th class="noheader">Aksi</th>
           </tr>
		</thead>
		<tbody>
           {foreach item="list" from=$TAWAR}
		   <tr>
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td><center>{$list.KREDIT_SEMESTER}</center></td>
			 <td><center>{$list.NAMA_KELAS}</center></td>
             <td>{$list.PRODI}</center></td>
			 <td><center>{$list.NM_JADWAL_HARI}</center></td>
			 <td><center>{$list.NM_RUANGAN}</center></td>
			 <td><center>{$list.KAPASITAS_UJIAN}</center></td>
			 <td><center>{$list.KLS_TERISI}</center></td>
			 <td><center><a href="uas-sp-fkg.php?action=add&kls={$list.ID_KELAS_MK}&jad={$list.ID_JADWAL_KELAS}">Jadwalkan</a></center></td>
           </tr>
		   {foreachelse}
        <tr><td colspan="10"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
   	</table>
</div>

<div class="panel" id="panel3" style="display: {$disp3}">
{if $smarty.get.action == ''}
<p> </p>
{else}
<p> </p>
<form action="uas-sp-fkg.php" method="post">
<input type="hidden" name="action" value="update">
<table width="100%" class="tablesorter" cellspacing="0" cellpadding="0">
  <tr><th colspan="3"><strong> Update Jadwal UAS SP </strong></th></tr>
	{foreach item="ubah" from=$UMK}
	<input type="hidden" name="ujian" value="{$ubah.ID_UJIAN_MK}" />
	<input type="hidden" name="jad" value="{$ubah.ID_JADWAL_UJIAN_MK}" />
	<input type="hidden" name="kls" value="{$ubah.ID_KELAS_MK}" />
	<tr>
	  <td>Mata Kuliah</td>
	  <td>:</td>
	  <td>{$ubah.NM_MATA_KULIAH}</td>
	</tr>
	<tr>
	  <td>Kelas</td>
	  <td>:</td>
	  <td>{$ubah.NAMA_KELAS}</td>
	</tr>
	<tr>
	  <td>Prodi</td>
	  <td>:</td>
	  <td>{$ubah.PRODI}</td>
	</tr>
	<tr>
	  <td>Jenis Ujian</td>
	  <td>:</td>
	  <td><input type="text" name="nm_ujian_mk" value="{$ubah.NM_UJIAN_MK}" maxlength="64" />&nbsp;<font color="grey"><em>Contoh: Ujian Tulis, Take Home, dsb</em></font></td>
	</tr>
	<tr>
	  <td>Tanggal Ujian</td>
	  <td>:</td>
	  <td><input type="text" name="tgl_ujian" id="tgl_ujian" maxlength="25" value="{$ubah.TGL_UJIAN}" onClick="NewCssCal('tgl_ujian','ddmmmyyyy');" /></td>
	</tr>
	<tr>
	  <td>Jam Mulai</td>
	  <td>:</td>
	  <td><input type="text" name="jam_mulai" value="{$ubah.JAM_MULAI}" maxlength="5" />&nbsp;<font color="grey"><em>Format jam: 08:00</em></font></td>
	</tr>
	<tr>
	  <td>Jam Selesai</td>
	  <td>:</td>
	  <td><input type="text" name="jam_selesai" value="{$ubah.JAM_SELESAI}" maxlength="5" />&nbsp;<font color="grey"><em>Format jam: 14:00</em></font></td>
	</tr>
	<tr>
	  <td>Ruangan - Kpst Ujian</td>
	  <td>:</td>
	  <td>
		<select name="ruangan" id="ruangan">
			{foreach item="ruangan" from=$T_RUANG}
			{html_options values=$ruangan.ID_RUANGAN output=$ruangan.RUANG selected=$ubah.ID_RUANGAN}
			{/foreach}
		  </select>
	  </td>
	</tr>
	{/foreach}
</table>
<table width="100%" class="tablesorter" cellspacing="0" cellpadding="0">
	<tr><th colspan="4"><strong> Tim Pengawas Ujian </strong></th></tr>
	{foreach item="dsn" from=$DOSEN}
	<tr>
	  <td>{$dsn.PENGAWAS}</td>
	  <td>{$dsn.STATUS}</td>
	  <td>{$dsn.TIPE}</td>
	  <td><center><input type="button" name="del" value="Hapus" onClick="window.location.href='#ujian-ujianspuas!uas-sp-fkg.php?action=del_tim&tim={$dsn.ID_TIM_PENGAWAS_UJIAN}&ujian={$smarty.get.ujian}'"></center></td>
	</tr>
	{foreachelse}
    <tr>
	<td colspan="4"><em>Data tidak ditemukan</em></td></tr>
    {/foreach}
	<tr>
	  <td colspan="3">&nbsp;</td>
	  <td><center><input type="button" name="add" value="Tambah" onClick="window.location.href='#ujian-ujianspuas!uas-sp-fkg.php?action=pengawas&ujian={$ubah.ID_UJIAN_MK}&jad={$ubah.ID_JADWAL_UJIAN_MK}'"></center></td>
	</tr>
</table>
<p><input type="button" name="kembali" value="Batal" onClick="window.location.href='#ujian-ujianspuas!uas-sp-fkg.php'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="Simpan1" value="Simpan"></p>
</form>
{/if}
</div>

<div class="panel" id="panel4" style="display: {$disp4}">
{if $smarty.get.action == ''}
<p> </p>
{else}
<p> </p>
<table width="100%" class="tablesorter" cellspacing="0" cellpadding="0">
  <tr><th colspan="3"><strong> Update Tim Pengawas Ujian </strong></th></tr>
	{foreach item="ubah" from=$UMK}
	<tr>
	  <td>Mata Kuliah</td>
	  <td>:</td>
	  <td>{$ubah.NM_MATA_KULIAH}</td>
	</tr>
	<tr>
	  <td>Kelas</td>
	  <td>:</td>
	  <td>{$ubah.NAMA_KELAS}</td>
	</tr>
	<tr>
	  <td>Prodi</td>
	  <td>:</td>
	  <td>{$ubah.PRODI}</td>
	</tr>
	<tr>
	  <td>Jenis Ujian</td>
	  <td>:</td>
	  <td>{$ubah.NM_UJIAN_MK}</td>
	</tr>
	<tr>
	  <td>Jadwal Ujian</td>
	  <td>:</td>
	  <td>{$ubah.TGL_UJIAN}; {$ubah.JAM_MULAI} - {$ubah.JAM_SELESAI}</td>
	</tr>
	<tr>
	  <td>Ruangan - Kpst Ujian</td>
	  <td>:</td>
	  <td>{$ubah.NM_RUANGAN} - {$ubah.KAPASITAS_UJIAN}</td>
	</tr>
	{/foreach}
</table>
<table width="100%" class="tablesorter" cellspacing="0" cellpadding="0">
  <tr><th colspan="3"><strong> Update Tim Pengawas Ujian </strong></th></tr>
  	<tr>
	  <th>Nama</th>
	  <th>Status</th>
	  <th>Aksi</th>
	</tr>
	{foreach item="tim" from=$TIM}
	<tr>
	  <td>{$tim.PENGAWAS}</td>
	  <td>{$tim.NM_ROLE}</td>
	  <td><center><input type="button" name="add" value="Pilih" onClick="window.location.href='#ujian-ujianspuas!uas-sp-fkg.php?action=add_tim&ujian={$smarty.get.ujian}&jad={$smarty.get.jad}&tim={$tim.ID_PENGGUNA}&dosen={$tim.ID_ROLE}'"></center></td>
	</tr>
	{/foreach}
</table>
{/if}
</div>

{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable2").tablesorter(
		{
		sortList: [[1,0],[3,0]],
		headers: {
            9: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
<div class="panel" id="panel5" style="display: {$disp5}">
<p> </p>
{if $smarty.get.action == 'list'}
<table width="100%" class="tablesorter" cellspacing="0" cellpadding="0">
  <tr><th colspan="3"><strong> Daftar Peserta Ujian </strong></th></tr>
  	{foreach item="ubah" from=$UMK}
	<tr>
	  <td>Mata Kuliah</td>
	  <td>:</td>
	  <td>{$ubah.NM_MATA_KULIAH}</td>
	</tr>
	<tr>
	  <td>Kelas</td>
	  <td>:</td>
	  <td>{$ubah.NAMA_KELAS}</td>
	</tr>
	<tr>
	  <td>Prodi</td>
	  <td>:</td>
	  <td>{$ubah.PRODI}</td>
	</tr>
	<tr>
	  <td>Jenis Ujian</td>
	  <td>:</td>
	  <td>{$ubah.NM_UJIAN_MK}</td>
	</tr>
	<tr>
	  <td>Jadwal Ujian</td>
	  <td>:</td>
	  <td>{$ubah.TGL_UJIAN}; {$ubah.JAM_MULAI} - {$ubah.JAM_SELESAI}</td>
	</tr>
	<tr>
	  <td>Ruangan - Kpst Ujian</td>
	  <td>:</td>
	  <td>{$ubah.NM_RUANGAN} - {$ubah.KAPASITAS_UJIAN}</td>
	</tr>
	{/foreach}
</table>
<form action="uas-sp-fkg.php" method="post">
<input type="hidden" name="action" value="manual">
<input type="hidden" name="ujian" value="{$smarty.get.ujian}">
<table width="100%" class="tablesorter" cellspacing="0" cellpadding="0">
	<thead>
	   <tr>
		 <th>No</th>
		 <th>NIM</th>
		 <th>Nama Mahasiswa</td>
		 <th>Peserta</td>
	   </tr>
	</thead>
	<tbody>
	   {foreach name=list item="list" from=$DAFTAR}
	   {if $smarty.foreach.list.iteration % 2 == 0}
	 <tr bgcolor="lightgray">
	   {else}
	 <tr>
	  {/if}
		 <td>{$smarty.foreach.list.iteration}</td>
		 <td>{$list.NIM_MHS}</td>
		 <td>{$list.NM_PENGGUNA}</td>
		 <td><center><input name="mhs[]" type="checkbox" value="{$list.ID_MHS}" {$list.TANDA} /></center></td>
	   </tr>
	   {foreachelse}
	 <tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
	   {/foreach}
	 <tr>
		<td colspan="3"></td>
		<td><center><input type="submit" name="PROSES" value="PROSES"></center></td>
	 </tr>
	</tbody>
</table>
</form>
{else}
<table width="100%" class="tablesorter" cellspacing="0" cellpadding="0">
  <tr><th><strong> Ploting Peserta Ujian </strong></th></tr>
</table>
<table id="myTable2" width="100%" class="tablesorter" cellspacing="0" cellpadding="0">
	<thead>
	   <tr>
		 <th>Kode</th>
		 <th>Nama Mata Ajar</td>
		 <th>SKS</th>
		 <th>Prodi</th>
		 <th>Jadwal</th>
		 <th>Kelas - Ruang</th>
		 <th>Tot. Kpst</th>
		 <th>Tot. Pst</th>
		 <th>Pst/ruang</th>
		 <th class="noheader">Aksi</th>
	   </tr>
	</thead>
	<tbody>
	   {foreach name=test item="list" from=$DATA}
	   {if $list.KAPASITAS_UJIAN < $list.KLS_TERISI}
	   <tr bgcolor="#ff828e">
	   {else}
	   <tr>
	   {/if}
		 <td>{$list.KD_MATA_KULIAH}</td>
		 <td>{$list.NM_MATA_KULIAH}</td>
		 <td><center>{$list.KREDIT_SEMESTER}</center></td>
		 <td>{$list.PRODI}</center></td>
		 <td><center>{$list.TGL_UJIAN}<br/>{$list.JAM}</center></td>
		 <td>{$list.NAMA_KELAS}</td>
		 <td><center>{$list.KAPASITAS_UJIAN}</center></td>
		 <td><center>{$list.KLS_TERISI}</center></td>
		 <td><center>{$list.BAGI}</center></td>
		 {if $list.KAPASITAS_UJIAN < $list.KLS_TERISI}
		 <td><center>Kapasitas ruang<br/>tidak mencukupi</center></td>
		 {elseif $list.ADA == 0}
		 <td><center><input type="button" name="plot" value="Plot peserta" onClick="window.location.href='#ujian-ujianspuas!uas-sp-fkg.php?action=add_pst&kur={$list.ID_KURIKULUM_MK}'"></center></td>
		 {else}
		 <td><center><input type="button" name="reset" value="Reset" onclick="javascript:if(confirm('Anda yakin untuk menghapus semua peserta ujian?'))reset({$list.ID_KURIKULUM_MK});"></center></td>
		 {/if}
	   </tr>
	   {foreachelse}
	<tr><td colspan="10"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
{/if}
</div>
