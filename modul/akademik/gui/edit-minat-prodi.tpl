
<div class="center_title_bar">Edit Peminatan Prodi</div>
<form action="edit-minat-prodi.php?action=view" method="post" name="id_prodi" id="id_prodi">
<table class="tb_frame" width="80%" border="0" cellpadding="0" cellspacing="0">
   <tr>
	<td>Prodi :
	   <select name="kdprodi">
	   <option value='ALL'>-- ALL --</option>
	   {foreach item="list" from=$T_PRODI}
	   {html_options  values=$list.ID_PROGRAM_STUDI output=$list.PRODI selected=$smarty.post.kdprodi}
	   {/foreach}
	   </select></td>
	<td>Periode Wisuda :
	   <select name="periode">
	   {foreach item="list" from=$T_PW}
	   {html_options  values=$list.NM_PERIODE_WISUDA output=$list.NM_PERIODE_WISUDA selected=$smarty.post.periode}
	   {/foreach}
	   </select></td>  
	<td>   <input type="submit" name="Proses" value="Proses"></td>
   </tr>
</table>
</form>

<div id="tabs">
        <div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
        <div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Edit Minat Mhs</div>
</div>
<div class="tab_bdr"></div>

<div class="panel" id="panel1" style="display: {$disp1} ">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<th>No</th>
		<th>Nim</th>
		<th>Nama Mhs</th>
		<th>Program Studi</th>
		<th>Minat</th>
		<th>Aksi</th>
	</tr>
	</thead>
	<tbody>
  {foreach name=test item="list" from=$T_MINAT}
	<tr>
    <td>{$smarty.foreach.test.iteration}</td>
    <td>{$list.NIM_MHS}</td>
    <td>{$list.NM_PENGGUNA}</td>
	<td>{$list.PRODI}</td>
    <td>{$list.NM_PRODI_MINAT}</td>
    <td align="center"><a href="edit-minat-prodi.php?action=edit&id_mhs={$list.ID_MHS}&id_prodi={$list.ID_PROGRAM_STUDI}&p_ws={$list.NM_PERIODE_WISUDA}">Edit</a></td>
	</tr>
  {foreachelse}
	<tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
  {/foreach}
  </tbody>
</table>
</div>

<div class="panel" id="panel2" style="display: {$disp2} ">
<form action="edit-minat-prodi.php" method="post">
<input type="hidden" name="action" value="update" >

<table class="tb_frame" width="80%" border="0" cellpadding="0" cellspacing="0">
	{foreach item="list" from=$T_DATAMHS}
	<tr>
		<td>Nim </td>
		<td>:</td>
		<td>{$list.NIM_MHS}</td>
	</tr>
	<tr>
		<td>Nama Mhs</td>
		<td>:</td>
		<td>{$list.NM_PENGGUNA}</td>
	</tr>
	<tr>
		<td>Prodi </td>
		<td>:</td>
		<td>{$list.PRODI}</td>
	</tr>
	
	<tr>
		<td>Peminatan </td>
		<td>:</td>
		<td><select name="id_minat">
	   {foreach item="list1" from=$DATA_MINAT}
	   {html_options  values=$list1.ID_PRODI_MINAT output=$list1.NM_PRODI_MINAT selected=$list.ID_PRODI_MINAT}
	   {/foreach}
	   </select><input type="submit" name="Update" value="Update"></td> 
	</tr>
		<input type="hidden" name="id_mhs" value="{$list.ID_MHS}" >
		<input type="hidden" name="kdprodi" value="{$list.ID_PROGRAM_STUDI}" >
		<input type="hidden" name="periode" value="{$p_ws}" >
	{/foreach}
	
</table>


</from>
</div>


{literal}
 <script>$('form').validate();</script>
{/literal}
