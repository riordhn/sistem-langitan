   <div class="center_title_bar">Informasi Dosen </div>  
	   <form name="report_info_dosen" action="report-info-dosen-xls.php" target="myNewWin" method="POST" onSubmit="return validate_post();">
	   <table width="70%" border="1">
         <tr>
           <td>
			<table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td width="135">Prodi</td>
               <td width="10">:</td>
               <td width="239"><select name="prodi">
                     <option value="">----</option>
		       {foreach item="pro" from=$T_PRO}
		       {html_options values=$pro.ID_PROGRAM_STUDI output=$pro.PRODI}
		       {/foreach}
		     </select>
                </td>
             </tr>
             <tr>
               <td>Tahun Akademik</td>
               <td>:</td>
               <td><select name="semester">
                     <option value="">----</option>
		       {foreach item="smt" from=$T_SMT}
		       {html_options values=$smt.ID_SEMESTER output=$smt.THNSMT}
		       {/foreach}
		     </select>
                </td>
             </tr>
		<tr>
		  <td></td>
		  <td></td>
		  <td><input type="submit" name="Submit" value="Preview" onClick="sendme();"></td>
             </tr>
       </table>       
	   </form>
	</div>
{literal}
<script type="text/javascript">
function validate_post(){
if ((document.forms.report_info_dosen.prodi.selectedIndex == "" || document.forms.report_info_dosen.prodi.selectedIndex == null) && (document.forms.report_info_dosen.semester.selectedIndex == "" || document.forms.report_info_dosen.semester.selectedIndex == null))
{
	alert ("Prodi dan Tahun akademik belum dipilih");
	return false;
}
else if (document.forms.report_info_dosen.semester.selectedIndex == "" || document.forms.report_info_dosen.semester.selectedIndex == null)
{
	alert ("Tahun akademik belum dipilih");
	return false;
} 
else if (document.forms.report_info_dosen.prodi.selectedIndex == "" || document.forms.report_info_dosen.prodi.selectedIndex == null)
{
	alert ("Prodi belum dipilih");
	return false;
}
return true;
}

function sendme() 
{ 
    window.open("report-info-dosen-xls.php?prodi='+document.forms.report_info_dosen.prodi.selectedIndex+'&semester='+document.forms.report_info_dosen.semester.selectedIndex+'","myNewWin","width=100,height=100,toolbar=0"); 
    var a = window.setTimeout("document.form1.submit();",100); 
}
</script>
{/literal}