{literal}
<link href="js/jquery.dataTables.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('table.display').dataTable({
		"sDom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>',
		"sPaginationType": "full_numbers",
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Semua"]]
	});
});
</script>
{/literal}

<div class="center_title_bar">KRS - Tawar : {$disprodi}</div>  
	<div id="tabs">
	<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Penawaran</div>
</div>
		
<div class="panel" id="panel1" style="display: {$disp1} ">
<p> </p>
<form action="krs-tawar.php" method="post">
<input type="hidden" name="action" value="tampil" >  
<input type="hidden" name="idprodi" value="{$kodeprodi|base64_encode}" > 
<table class="tb_frame" width="80%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
		Tahun Akademik : 
			<select name="smt">
			{foreach item="smt" from=$T_ST}
			{if $smarty.request.smt == ''}
			{html_options  values=$smt.ID_SEMESTER output=$smt.SMT selected=$smtaktif}
			{else}
			{html_options  values=$smt.ID_SEMESTER output=$smt.SMT selected=$smarty.request.smt}
			{/if}
			{/foreach}
			</select>
		</td> 
		<td> <input type="submit" name="View" value="View"> </td>
	</tr>
</table>
</form>	    
<table id="" class="display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>Kode</th>
			<th>Nama Mata Kuliah</th>
			<th>SKS</th>
			<th>KLS</th>
			<th>STS</th>
			<th>Hari</th>
			<th>Jam</th>
			<th>Ruang</th>
			<th>Prodi Asal</th>
			<th>Aksi</th>
        </tr>
	</thead>
	<tbody>
	{foreach name=test item="list" from=$T_MK}
		<tr>
			<td>{$list.KD_MATA_KULIAH}</td>
			<td>{$list.NM_MATA_KULIAH}</td>
			<td><center>{$list.KREDIT_SEMESTER}</center></td>
			<td><center>{$list.NAMA_KELAS}</center></td>
			<td><center>{$list.STATUS}</center></td>
			<td><center>{$list.NM_JADWAL_HARI}</center></td>
			<td><center>{$list.JAM}</center></td>
			<td>{$list.NM_RUANGAN}</td>
			<td>{$list.PRODIASAL}</td>
			<td><center><a href="krs-tawar.php?action=del&id_krs_prodi={$list.ID_KRS_PRODI|base64_encode}&idprodi={$kodeprodi|base64_encode}"><img src="includes/img/error_delete.png" border="0" alt="Hapus" title="Hapus"/></a></center></td>
		</tr>
	{/foreach}
	</tbody>
	<tfoot>
		<tr>
			<th>Kode</th>
			<th>Nama Mata Kuliah</th>
			<th>SKS</th>
			<th>KLS</th>
			<th>STS</th>
			<th>Hari</th>
			<th>Jam</th>
			<th>Ruang</th>
			<th>Prodi Asal</th>
			<th>Aksi</th>
        </tr>
	</tfoot>
</table>
<p><br>&nbsp;</p>
</div>
		
<div class="panel" id="panel2" style="display: {$disp2}">
<p> </p>
<form action="krs-tawar.php" method="post" >
<input type="hidden" name="action" value="penawaran" >  
<input type="hidden" name="idprodi" value="{$kodeprodi|base64_encode}" > 
<table class="tb_frame" width="80%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
		Tahun Akademik : 
			<select name="smt">
			{foreach item="smt" from=$T_ST}
			{html_options values=$smt.ID_SEMESTER output=$smt.SMT selected=$smtaktif}
			{/foreach}
			</select>
		</td> 
		<td>
		Tahun Kurikulum : 
			<select name="thkur">
			{foreach item="kur" from=$T_KUR}
			{html_options values=$kur.ID_KURIKULUM_PRODI output=$kur.NAMA selected=$idkur}
			{/foreach}
			</select>
		</td> 
		<td> <input type="submit" name="View" value="View"> </td>
	</tr>
</table>
</form>	    
<table id="" class="display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>Kode MK</th>
			<th>Nama Mata Kuliah</th>
			<th>SKS</th>
			<th>KLS</th>
			<th>STS</th>
			<th>Prodi Asal</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
	{foreach name=test item="list" from=$TAWAR}
		<tr>
		<td>{$list.KD_MATA_KULIAH}</td>
		<td>{$list.NM_MATA_KULIAH}</td>
		<td><center>{$list.KREDIT_SEMESTER}</center></td>
		<td><center>{$list.NAMA_KELAS}</center></td>
		<td><center>{$list.STATUS}</center></td>
		<td>{$list.PRODIASAL}</td>
		<td><center><a href="krs-tawar.php?action=add&id_mk={$list.ID_KELAS_MK|base64_encode}&status={$list.STATUS|base64_encode}&smt={$smtaktif|base64_encode}&idprodi={$kodeprodi|base64_encode}"><img src="includes/img/add.png" border="0" alt="Tawarkan" title="Tawarkan"/></a></center></td>
		</tr>
	{/foreach}
	</tbody>
	<tfoot>
		<tr>
			<th>Kode MK</th>
			<th>Nama Mata Kuliah</th>
			<th>SKS</th>
			<th>KLS</th>
			<th>STS</th>
			<th>Prodi Asal</th>
			<th>Aksi</th>
		</tr>
	</tfoot>
</table>
<p><br>&nbsp;</p>
</div>