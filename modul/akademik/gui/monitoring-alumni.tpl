<div class="center_title_bar">Monitoring Alumni</div>
<table style="font-size:12px">
<tr>
	<th>NO</th>
	<th>NIM</th>
    <th>NAMA</th>
    <th>TTL</th>
	<th>L/P</th>
    <th>PROGRAM STUDI</th>
	<th>TGL LULUS</th>
	<th>NO IJASAH</th>
    <th>IPK</th>
    <th>SKS</th>
	<th>Alamat</th>
	<th>Phone</th>
</tr>
	{$no = 1}
	{foreach $pengajuan_bayar as $data}
    	{if ($no % 2) == 0}
		 	{$warna = "#CCCCCC"}
		{else}
		 	{$warna = ""}
		{/if}
    	<tr bgcolor="{$warna}">
        	<td>{$no++}</td>
            <td>{$data.NIM_MHS}</td>
            <td>{$data.NM_PENGGUNA}</td>
            <td>{$data.LAHIR_IJAZAH}</td>
			<td>{$data.KELAMIN_PENGGUNA}</td>
            <td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
			<td>{$data.TGL_LULUS_PENGAJUAN}</td>
			<td>{$data.NO_IJASAH}</td>
            <td>{$data.IPK}</td>
            <td>{$data.SKS}</td>
			<td>{$data.ALAMAT}</td>
			<td>{$data.PHONE}</td>
        </tr>
    {/foreach}
</table>
