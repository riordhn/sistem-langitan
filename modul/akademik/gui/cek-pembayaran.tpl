<div class="center_title_bar">Cek Pembayaran</div> 
<form method="post" action="cek-pembayaran.php">
    <table>
        <tr>
            <td>NIM Mahasiswa</td>
            <td><input type="text" name="nim" value="{$smarty.post.nim}" /></td>
            <td><input type="submit" value="Cari"/></td>
        </tr>
    </table>
</form>
{if isset($data_pembayaran)}
    <table class="detail">
        <tr>
            <th colspan="2" style="text-align: center;">Biodata</th>
        </tr>
        <tr>
            <td>NIM</td>
            <td>{$data_pembayaran[0]['NIM_MHS']}</td>            
        </tr>
        <tr>
            <td>NIM BANK</td>
            <td>{$data_pembayaran[0]['NIM_BANK']}</td>            
        </tr>
		<tr>
            <td>NIM LAMA</td>
            <td>{$data_pembayaran[0]['NIM_LAMA']}</td>            
        </tr>
        <tr>
            <td>Nama</td>
            <td>{$data_pembayaran[0]['NM_PENGGUNA']}</td>            
        </tr>
        <tr>
            <td>Program Studi</td>
            <td>{$data_pembayaran[0]['NM_PROGRAM_STUDI']}</td>            
        </tr>
        <tr>
            <td>Jenjang</td>
            <td>{$data_pembayaran[0]['NM_JENJANG']}</td>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>{$data_pembayaran[0]['NM_FAKULTAS']}</td>            
        </tr>
        <tr>
            <td>Status Cekal</td>
            <td>{$data_pembayaran[0]['STATUS_CEKAL']}</td>            
        </tr>
        <tr>
            <td>Kelompok Biaya</td>
            <td>{$data_pembayaran[0]['NM_KELOMPOK_BIAYA']}</td>            
        </tr>
    </table>
    <p></p>
    <table>
        <tr>
            <th colspan="2" style="text-align: center;">Detail Mahasiswa</th>
        </tr>
        <tr>
            <td>Jalur</td>
            <td>{$data_pembayaran[0]['NM_JALUR']}</td>            
        </tr>
        <tr>
            <td>Tahun Akademik</td>
            <td>{$data_pembayaran[0]['THN_AKADEMIK_SEMESTER']}</td>            
        </tr>

    </table>
    <p></p>
    <table class="detail">
        <tr>
            <th colspan="10" style="text-align: center;">Status Pembayaran</th>
        </tr>
        <tr>
            <th>Nama Biaya</th>
            <th>Besar Biaya</th>
            <th>Denda Biaya</th>
            <th>Status Tagihan Biaya</th>
            <th>Tanggal Bayar</th>
            <th>Nama Bank</th>
            <th>Via Bank</th>
            <th>Status Pembayaran</th>
            <th>Keterangan</th>
            <th>Semester Pembayaran</th>
        </tr>
        {foreach $data_pembayaran as $data}
            {if $data['NM_BIAYA']}
                <tr {if $data.NM_SEMESTER == 'Ganjil'} bgcolor="#CCCCFF"{/if}>
                    <td>{$data['NM_BIAYA']}</td>
                    <td style="text-align:right">{$data['BESAR_BIAYA']|number_format:0:",":"."}</td>
                    <td style="text-align:right">{$data['DENDA_BIAYA']|number_format:0:",":"."}</td>
                    <td>{$data['IS_TAGIH']}</td>
                    <td>{$data['TGL_BAYAR']}</td>
                    <td>{$data['NM_BANK']}</td>
                    <td>{$data['NAMA_BANK_VIA']}</td>
                    <td>{$data['NAMA_STATUS']}</td>
                    <td>{$data['KETERANGAN']}</td>
                    <td>{$data['NM_SEMESTER']} ({$data['TAHUN_AJARAN']}) </td>
                </tr>
            {else}
                <tr>
                    <td colspan="10" style="text-align: center;">Data Pembayaran Kosong</td>
                </tr>
            {/if}

        {/foreach}
        <tr>
            <td colspan="10" style="text-align: center;">
				{$total = $data_pembayaran[0]['TOTAL_SPP']+$data_pembayaran[0]['TOTAL_DENDA']}
                <p>
                    <span style="color: #007000;font-weight: bold;">TOTAL BIAYA </span><br/><strong>{$total|number_format:0:",":"."}</strong><br/>
                    <span style="color: #ff3300;font-weight: bold;">DAFTAR TAGIHAN</span><br/>Biaya Kuliah : <strong>{$data_pembayaran[0]['TOTAL_SPP_TAGIH']}</strong> + Denda Biaya :<strong> {$data_pembayaran[0]['TOTAL_DENDA']|number_format:0:",":"."}</strong><br/>
                    <strong>Total Biaya : {$data_pembayaran[0]['TOTAL_SPP_TAGIH']+$data_pembayaran[0]['TOTAL_DENDA_BIAYA']|number_format:0:",":"."}</strong>
                </p>
            </td>
        </tr>
    </table>
{else if isset($data_kosong)}
    <span>{$data_kosong}</span>
{/if}