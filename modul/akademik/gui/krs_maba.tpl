<div class="center_title_bar">KRS MABA</div>  

<div id="tabs">
    <div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
    <div id="tab2" class="tab" align="center" onclick="javascript: displayPanel('2');">KRS Maba</div>
    <div id="tab3" class="tab" align="center" onclick="javascript: displayPanel('3');">KRS Custom</div>
    <div id="tab4" class="tab" align="center" onclick="javascript: displayPanel('4');">Monitor KRS</div>
</div>

<div class="panel" id="panel1" style="display: {$disp1}">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="5%" bgcolor="#FFCC33"><center>No</center></td>
            <td width="30%" bgcolor="#FFCC33"><center>Prodi</center></td>
            <td width="10%" bgcolor="#FFCC33"><center>Total Mhs</center></td>
            <td width="10%" bgcolor="#FFCC33"><center>Total KRS</center></td>
            <td colspan="2" width="11%" bgcolor="#FFCC33"><center>Aksi</center></td>
        </tr>
        {foreach name=test item="list" from=$T_MABA}
            <tr>
                <td>{$smarty.foreach.test.iteration}</td>
                <td>{$list.PRODI}</td>
                <td>{$list.MABA}</td>
                <td><a href="krs_maba.php?action=monitor&prodi={$list.ID_PROGRAM_STUDI}">{$list.KRS}</a></td>
                <td height="0" align="center">
                    <input name="Button" type="button" class="button" onClick="javascript: location.href = '#utility-krs_paket!krs_maba.php?action=view&prodi={$list.ID_PROGRAM_STUDI}';disableButtons();" onMouseOver="window.status = 'Click untuk Insert';return true;" value="KRS MABA" /> </td> 
                <td height="0" align="center">
                    <input name="Button" type="button" class="button" onClick="javascript: location.href = '#utility-krs_paket!krs_maba.php?action=viewcustem&prodi={$list.ID_PROGRAM_STUDI}';disableButtons();" onMouseOver="window.status = 'Click untuk Insert'; return true;" value="CUSTOMIZE" /> </td> 
            </tr>
        {/foreach}
    </table>
</div>

<div class="panel" id="panel2" style="display: {$disp2} ">
    <p> </p>
    <form action="krs_maba.php" method="post" >
        <input type="hidden" name="action" value="prosesall" >	
        <input type="hidden" name="prodi" value="{$ID_PROGRAM_STUDI}" >
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td colspan="8" bgcolor="#FFCC33"><center>PILIH MATA AJAR DAN KELAS</center></td>
            </tr>
            <tr>
                <td colspan="8" bgcolor="#FFCC33"><center>*Jika MATA AJAR tdk TAMPIL, periksalah Penawaran KRS untuk Prodi tersebut</center></td>
            </tr>
            <tr>
                <td colspan="8" bgcolor="#FFCC33"><center>*Jika MATA AJAR masih tdk TAMPIL, periksalah posisi semester MATA AJAR via menu PRODI DAFTAR MA</center></td>
            </tr>
            <tr>
                <td width="3%" bgcolor="#FFCC33">No</td>
                <td width="5%" bgcolor="#FFCC33">Kode MA</td>
                <td width="20%" bgcolor="#FFCC33">Nama MA</td>
                <td width="5%" bgcolor="#FFCC33">SKS</td>
                <td width="5%" bgcolor="#FFCC33">Kelas MA</td>
                <td width="5%" bgcolor="#FFCC33">Status</td>
                <td width="10%" bgcolor="#FFCC33">Prodi</td>
                <td width="5%" bgcolor="#FFCC33">Pilih</td>
            </tr>
            {foreach name=test1 item="krs" from=$MA_PILIH}
                <tr>
                    <td>{$smarty.foreach.test1.iteration}</td>
                    <td>{$krs.KD_MATA_KULIAH}</td>
                    <td>{$krs.NM_MATA_KULIAH}</td>
                    <td>{$krs.KREDIT_SEMESTER}</td>
                    <td>{$krs.NAMA_KELAS}</td>
                    <td>{$krs.STATUS}</td>
                    <td>{$krs.PRODI}</td>
                    <td><center><input name="mk{$smarty.foreach.test1.iteration}" type="checkbox" value="{$krs.ID_KELAS_MK}" {$krs.TANDA} /></center></td>
                </tr>

            {foreachelse}
                <tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
            {/foreach}
            <input type="hidden" name="counter" value="{$smarty.foreach.test1.iteration}" >
            <tr>
                <td colspan="7"></td><td><input type="submit" name="PROSES" value="PROSES"></td>
            </tr>
        </table>

    </form>
</div>

<div class="panel" id="panel3" style="display: {$disp3} ">
    <p> </p>
    <form action="krs_maba.php" method="post" >
        <input type="hidden" name="action" value="prosescustem" >	
        <input type="hidden" name="prodi" value="{$ID_PROGRAM_STUDI}" >	
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td colspan="8" bgcolor="#FFCC33"><center>PILIH MATA AJAR DAN KELAS</center></td>
            </tr>
            <tr>
                <td colspan="8" bgcolor="#FFCC33"><center>*Jika MATA AJAR tdk TAMPIL, periksalah Penawaran KRS untuk Prodi tersebut</center></td>
            </tr>
            <tr>
                <td colspan="8" bgcolor="#FFCC33"><center>*Jika MATA AJAR masih tdk TAMPIL, periksalah posisi semester MATA AJAR via menu PRODI DAFTAR MA</center></td>
            </tr>
            <tr>
                <td width="3%" bgcolor="#FFCC33">No</td>
                <td width="5%" bgcolor="#FFCC33">Kode MA</td>
                <td width="20%" bgcolor="#FFCC33">Nama MA</td>
                <td width="5%" bgcolor="#FFCC33">SKS</td>
                <td width="5%" bgcolor="#FFCC33">Kelas MA</td>
                <td width="5%" bgcolor="#FFCC33">Status</td>
                <td width="10%" bgcolor="#FFCC33">Prodi</td>
                <td width="5%" bgcolor="#FFCC33">Pilih</td>
            </tr>
            {foreach name=test1 item="krs" from=$MA_PILIH}
                <tr>
                    <td>{$smarty.foreach.test1.iteration}</td>
                    <td>{$krs.KD_MATA_KULIAH}</td>
                    <td>{$krs.NM_MATA_KULIAH}</td>
                    <td>{$krs.KREDIT_SEMESTER}</td>
                    <td>{$krs.NAMA_KELAS}</td>
                    <td>{$krs.STATUS}</td>
                    <td>{$krs.PRODI}</td>
                    <td><center><input name="mk{$smarty.foreach.test1.iteration}" type="checkbox" value="{$krs.ID_KELAS_MK}" {$krs.TANDA} /></center></td>
                </tr>
            {foreachelse}
                <tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
            {/foreach}
            <input type="hidden" name="counter" value="{$smarty.foreach.test1.iteration}" >
        </table>
        <p></p>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td colspan="8" bgcolor="#FFCC33"><center>DAFTAR MAHASISWA</center></td>
            </tr>
            <tr>
                <td width="3%" bgcolor="#FFCC33">No</td>
                <td width="5%" bgcolor="#FFCC33">NIM</td>
                <td width="20%" bgcolor="#FFCC33">Nama Mhs</td>
                <td width="5%" bgcolor="#FFCC33">Prodi</td>
                <td width="5%" bgcolor="#FFCC33">Status</td>
                <td width="5%" bgcolor="#FFCC33">Agama</td>
                <td width="5%" bgcolor="#FFCC33">Bayar</td>
                <td width="5%" bgcolor="#FFCC33">Pilih</td>
            </tr>
            {foreach name=test3 item="mhs" from=$MHS_PILIH}
                {if $mhs.STATUSBAYAR=='BELUM'}
                    <tr bgcolor="red">
                    {/if}
                    <td>{$smarty.foreach.test3.iteration}</td>
                    <td>{$mhs.NIM_MHS}</td>
                    <td>{$mhs.NM_PENGGUNA}</td>
                    <td>{$mhs.PRODI}</td>
                    <td>{$mhs.STATUS}</td>
                    <td>{$mhs.NM_AGAMA}</td>
                    <td>{$mhs.STATUSBAYAR}</td>
                    <td><center><input name="mhs{$smarty.foreach.test3.iteration}" type="checkbox" value="{$mhs.ID_MHS}" {$mhs.TANDA} /></center></td>		 	
                </tr>
            {foreachelse}
                <tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
            {/foreach}
            <input type="hidden" name="counter1" value="{$smarty.foreach.test3.iteration}" >
            <tr>
                <td colspan="6"></td><td><input type="submit" name="PROSES" value="PROSES"></td>
            </tr>
        </table>
    </form>
</div>
<div class="panel" id="panel4" style="display: {$disp4} ">
    <p> </p>
    <form action="krs_maba.php" method="post" >
        <input type="hidden" name="action" value="batal" >	
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="3%" bgcolor="#FFCC33">No</td>
                <td width="5%" bgcolor="#FFCC33">NIM</td>
                <td width="20%" bgcolor="#FFCC33">Nama Mhs</td>
                <td width="15%" bgcolor="#FFCC33">Prodi</td>
                <td width="5%" bgcolor="#FFCC33">Status</td>
                <td width="5%" bgcolor="#FFCC33">Ttl MK</td>
                <td width="5%" bgcolor="#FFCC33">Ttl SKS</td>
                <td width="5%" bgcolor="#FFCC33">Pilih</td>
            </tr>
            {foreach name=test2 item="krsmaba" from=$MA_KRS}
                <tr>
                    <td>{$smarty.foreach.test2.iteration}</td>
                    <td>{$krsmaba.NIM_MHS}</td>
                    <td>{$krsmaba.NM_PENGGUNA}</td>
                    <td>{$krsmaba.PRODI}</td>
                    <td>{$krsmaba.STATUS}</td>
                    <td>{$krsmaba.TTL_MK}</td>
                    <td>{$krsmaba.TTL_SKS}</td>
                    <td><center><input name="maba{$smarty.foreach.test2.iteration}" type="checkbox" value="{$krsmaba.ID_MHS}" {$krsmaba.TANDA} /></center></td>
                </tr>

            {foreachelse}
                <tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
            {/foreach}
            <input type="hidden" name="counter4" value="{$smarty.foreach.test2.iteration}" >
            <tr>
                <td colspan="7"></td><td><input type="submit" name="PROSES" value="PROSES"></td>
            </tr>
        </table>

    </form>
</div>
<script>$('form').validate();</script>