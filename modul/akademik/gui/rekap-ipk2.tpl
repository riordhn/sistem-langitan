{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[0,0]],
		}
	); 
} 
);
</script>
{/literal}

<div class="center_title_bar">Data IPK/IPS/STATUS Mahasiswa {$NM_PRODI}, Tahun Akademik {$THN_AKAD}</div>  
<form action="rekap-ipk.php" method="post" name="id_daftarmk" id="id_daftarmk">
<input type="hidden" name="action" value="view" >
<p>
			   Prodi : 
               <select name="kdprodi">
    		   <option value=''>-- PRODI --</option>
	 		   {foreach item="list" from=$T_PRODI}
    		   {html_options  values=$list.ID_PROGRAM_STUDI output=$list.PRODI}
	 		   {/foreach}
			   </select>
			   Tahun/Semester : 
               <select name="kdthnsmt">
    		   <option value=''>------</option>
	 		   {foreach item="list_smt" from=$T_THNSMT}
    		   {html_options values=$list_smt.ID_SEMESTER output=$list_smt.THN_SMT}
	 		   {/foreach}
			   </select>
			   <input type="submit" name="View" value="View">
</p>
</form>	

{* khusus fakultas psikologi *}
{if $FAK == '11'}
{if {$list1.PRODI} == ''}
{else}
{*
<p>
<input type=button name="cetak" value="Ekspor Data ke MS. Excel" onclick="window.open('proses/rekap-ipk-xls.php?prodi={$PRODI}&smt={$SMT_AKAD}&nm_prodi={$NM_PRODI}&tahun={$TAHUN_SMT}&thn_hitung={$THN_HITUNG}','baru2');">&nbsp;
<input type=button name="cetak" value="Ekspor Data ke MS. Word" onclick="window.open('proses/rekap-ipk-doc.php?prodi={$PRODI}&smt={$SMT_AKAD}&nm_prodi={$NM_PRODI}&tahun={$TAHUN_SMT}&thn_hitung={$THN_HITUNG}','baru2');">&nbsp;
Jumlah Mahasiswa yang melakukan KRS: {$JML_MHS} mahasiswa
</p>
*}
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
             <th>Nim</th>
             <th>Nama Mahasiswa</th>
			 <th>STATUS</th>
             <th>SKS SMT</th>
			 <th>IPS</th>
			 <th>TTL SKS</th>
			 <th>IPK</th>
           </tr>
		</thead>
		<tbody>
		   {foreach name=test item="list" from=$T_MK}
           <tr>
             <td>{$list.NIM_MHS}</td>
             <td>{$list.NM_PENGGUNA}</td>
			 <td><center>{$list.STATUS}</center></td>
             <td><center>{$list.SKS_SEM}</center></td>
			 <td><center>{$list.IPS}</center></td>
			 <td><center>{$list.SKS_TOTAL_MHS}</center></td>
			 <td><center>{$list.IPK_MHS}</center></td>
           </tr>
            {foreachelse}
        <tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
</table>
{/if}

{* fakultas selain psikologi *}
{else}

<p>
<input type=button name="cetak" value="Ekspor ke Excel" onclick="window.open('proses/rekap-ipk-xls.php?prodi={$PRODI}&smt={$SMT_AKAD}&nm_prodi={$NM_PRODI}&tahun={$TAHUN_SMT}&thn_hitung={$THN_HITUNG}','baru2');">&nbsp;
</p>

<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
             <th>Nim</th>
             <th>Nama Mahasiswa</th>
			 <th>STATUS</th>
             <th>SKS SMT</th>
			 <th>IPS</th>
			 <th>TTL SKS</th>
			 <th>IPK</th>
           </tr>
		</thead>
		<tbody>
		   {foreach name=test item="list" from=$T_MK}
           <tr>
             <td>{$list.NIM_MHS}</td>
             <td>{$list.NM_PENGGUNA}</td>
			 <td><center>{$list.STATUS}</center></td>
             <td><center>{$list.SKS_SEM}</center></td>
			 <td><center>{$list.IPS}</center></td>
			 <td><center>{$list.SKS_TOTAL_MHS}</center></td>
			 <td><center>{$list.IPK_MHS}</center></td>
           </tr>
            {foreachelse}
        <tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
</table>
{/if}
   