{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[1,0],[2,0]],
		}
	); 
} 
);
</script>
{/literal}
<div class="center_title_bar">Data Kelas Mata Ajar dan Team Teaching {$NM_PRODI}, Tahun Akademik {$THN_AKAD} </div>
<form action="pengampu-mk.php" method="post" name="id_pengampu" id="id_pengampu">
<input type="hidden" name="action" value="view" >
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td>
			 	Prodi : 
                <select name="kdprodi">
    		   <option value=''>-- PRODI --</option>
	 		   {foreach item="list" from=$T_PRODI}
    		   {html_options  values=$list.ID_PROGRAM_STUDI output=$list.PRODI selected=$smarty.post.kdprodi}
	 		   {/foreach}
			   </select>
		     </td>
			 <td>
				Tahun/Semester : 
               <select name="kdthnsmt">
    		   <option value=''>------</option>
	 		   {foreach item="list_smt" from=$T_THNSMT}
    		   {html_options values=$list_smt.ID_SEMESTER output=$list_smt.THN_SMT selected=$smarty.post.kdthnsmt}
	 		   {/foreach}
			   </select>
			   <input type="submit" name="View" value="View">	
			 </td>
           </tr>
</table>
</form>	
<p>
<input type=button name="cetak" value="Ekspor ke Excel" onclick="window.open('proses/pengampu-mk-xls.php?prodi={$PRODI}&smt={$SMT_AKAD}&nm_prodi={$NM_PRODI}','baru2');">&nbsp;
</p>
<div>Format Kolom Team:NIP,NIDN,NAMA dengan pemisah "-" dan yg pertama adalah PJMA</div>  
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
             <th width="3%">No</th>
             <th width="8%">Kode MA</th>
             <th width="25%">Nama MA</th>
             <th width="4%">SKS</th>
			 <th width="4%">Kls</th>
             <th width="55%">Team (NIP-NIDN-NAMA DOSEN)</th>
           </tr>
		</thead>
		<tbody>
		   {foreach name=test item="list" from=$T_MK}
           <tr>
             <td><center>{$smarty.foreach.test.iteration}</center></td>
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td><center>{$list.KREDIT_SEMESTER}</center></td>
             <td><center>{$list.NAMA_KELAS}</center></td>
             <td>{$list.TEAM}</td>
           </tr>
            {foreachelse}
        <tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
</table>