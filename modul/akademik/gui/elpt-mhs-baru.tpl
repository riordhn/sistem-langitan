{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function() 
{ 
     $("#myTable").tablesorter(
		{
		sortList: [[0,0]],
		headers: {
            0: { sorter: false }
		}
		}
	);
} 
);
</script>
{/literal}

<div class="center_title_bar">ELPT MAHASISWA BARU</div>

<form action="elpt-mhs-baru.php" method="get">
    <table>
        <tr>
            <th colspan="2">
                Parameter
            </th>
        </tr>
        <tr>
            <td>Penerimaan</td>
            <td>
                <select name="id_penerimaan">
                    <option value="">Pilih Penerimaan</option>
                    {foreach $penerimaan_set as $p}
                        <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                            {foreach $p.p_set as $p2}
                                <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center"><input type="submit" value="Tampil"></td>
        </tr>
    </table>
</form>

 {if isset($view_kesehatan)}

   
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
             <th>No</th>
             <th>No Ujian</th>
             <th>NIM</th>
			 <th>Nama</th>
             <th>Program Studi</th>
             <th>Hasil Tes ELPT</th>
             <th>Tanggal Tes ELPT</th>
         </tr>
	</thead>
	<tbody>
			{$no = 1}
         {foreach $view_kesehatan as $data}
         <tr>
             <td>{$no++}</td>
             <td>{$data.NO_UJIAN}</td>
			 <td>{$data.NIM_MHS}</td>
             <td>{$data.NM_C_MHS}</td>
             <td>{$data.NM_PROGRAM_STUDI}</td>
             <td>{if $data.TGL_VERIFIKASI_ELPT != '' and $data.ELPT_SCORE != 0}{$data.ELPT_SCORE}{else}Belum Tes{/if}</td>
             <td>{$data.TGL_TES_TOEFL}</td>
         </tr>
         {/foreach}
	</tbody>
</table>

 {/if} 