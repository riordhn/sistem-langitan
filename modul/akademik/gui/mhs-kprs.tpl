{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable2").tablesorter(
		{
		sortList: [[0,0]],
		}
	); 
} 
);
</script>
{/literal}

<div class="center_title_bar">Daftar Mahasiswa KPRS</div>
<table>
	{foreach item="info" from=$T_INFO}
	<tr>
		<td width="29%" >Kode Mata Ajar / SKS / Kelas</td>
		<td width="2%" >:</td>
		<td width="69%" >{$info.KD_MATA_KULIAH} / {$info.KREDIT_SEMESTER} / {$info.NAMA_KELAS}</td>
	</tr>
	<tr>
		<td width="29%" >Nama Mata Ajar</td>
		<td width="2%" >:</td>
		<td width="69%" >{$info.NM_MATA_KULIAH}</td>
	</tr>
	<tr>
		<td width="29%" >Tim Pengajar</td>
		<td width="2%" >:</td>
		<td width="69%" ><br><ol>{$info.TIM}</li></ol></td>
	</tr>
		{/foreach}
</table>
<table id="myTable2" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	   <tr>
		 <th>NIM</th>
		 <th>Nama Mahasiswa</th>
		 <th>Prodi</th>
		 <th>Status</th>
	   </tr>
	</thead>
	<tbody>
			{foreach name=test item="list" from=$T_DETAIL}
			{if $list.STATUS == "Blm Disetujui"}
		<tr bgcolor="red">
			{else}
		<tr>
			{/if}
			<td>{$list.NIM_MHS}</td>
			<td>{$list.NM_PENGGUNA}</td>
			<td>{$list.NM_PROGRAM_STUDI}</td>
			<td>{$list.STATUS}</td>
		</tr>
			{foreachelse}
		<tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
			{/foreach}
	</tbody>
</table>
<p><input type="button" name="kembali" value="Kembali" onClick="javascript:history.go(-1)" ></p>