<div class="center_title_bar">Minat Prodi Lulusan</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
<form action="minat-mhs-lulus.php" method="post" name="minat" id="minat">
<input type="hidden" name="action" value="view" >
NIM : <input type="text" name="nim" value="{if !empty($smarty.post.nim)}{$smarty.post.nim}{/if}">&nbsp;&nbsp;&nbsp;<input type="submit" name="View" value="View">
</form>
		</td>
	</tr>
</table>

{if isset($T_MHS)}
<form action="minat-mhs-lulus.php" method="post">
<input type="hidden" name="action" value="view" >
<input type="hidden" name="nim" value="{$smarty.request.nim}" >
<table>
	<tr>
    	<td>NIM</td>
        <td>:</td>
        <td>{$T_MHS.NIM_MHS}</td>
    </tr>
    <tr>
    	<td>Nama</td>
        <td>:</td>
        <td>{$T_MHS.NM_PENGGUNA}</td>
    </tr>
    <tr>
    	<td>Prodi</td>
        <td>:</td>
        <td>{$T_MHS.NM_JENJANG} - {$T_MHS.NM_PROGRAM_STUDI}</td>
    </tr>
    <tr>
    	<td>Minat</td>
        <td>:</td>
        <td>
        	<select name="minat">
            	{foreach $minat as $data}
            		<option value="{$data.ID_PRODI_MINAT}" {if $data.ID_PRODI_MINAT == $T_MHS.ID_PRODI_MINAT} selected="selected" {/if}>{$data.NM_PRODI_MINAT}</option>
                {/foreach}
        	</select>
        </td>
    </tr>
    <tr>
    	<td colspan="3" style="text-align:center"><input type="submit" value="Simpan" /></td>
    </tr>
</table>
</form>
{/if}