<div class="center_title_bar">Entri Hasil Yudisium</div>  
<form id="form1" name="form1" method="post" action="wisuda-kelengkapan.php">
<table>
<tr>
	<td>NIM</td>
	<td> : </td>
	<td><input type="text" name="nim" value="{$smarty.post.nim}" /></td>
	<td><input type="submit" value="Tampil" /></td>
</tr>
</table>
</form>

<h2><font color="#FF0000">{$pesan}</font></h2>
{if isset($pengajuan_wisuda)}
<div id="alert" class="ui-widget" style="display:none;margin-left:15px;">
        <div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;"> 
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                    Nomor SK Yudisium, Tanggal SK Yudisium, Judul Skripsi, Bulan Awal Bimbingan, Bulan Akhir Bimbingan Harus Diisi.</p>
        </div>
</div>
<form method="post" name="lengkap" id="lengkap" action="wisuda-kelengkapan.php">
  <table width="500">
    <tr>
      <th colspan="2" style="text-align:center">Biodata</th>
    </tr>
    <tr>
      <td>NIM</td>
      <td>{$pengajuan_wisuda[0]['NIM_MHS']}</td>
    </tr>
    <tr>
      <td>Nama</td>
      <td>{$pengajuan_wisuda[0]['NM_PENGGUNA']}</td>
    </tr>
    <tr>
      <td>Program Studi</td>
      <td>{$pengajuan_wisuda[0]['NM_JENJANG']} - {$pengajuan_wisuda[0]['NM_PROGRAM_STUDI']}</td>
    </tr>
    <tr>
      <td>Periode</td>
      <td>{if $pengajuan_wisuda[0]['NM_TARIF_WISUDA'] == ' - '}{$periode['NM_TARIF_WISUDA']}{else}{$pengajuan_wisuda[0]['NM_TARIF_WISUDA']}{/if}</td>
    </tr>
    <tr>
      <td>Tanggal Keluar</td>
      <td>
          <input type="text" id="tgl_keluar" class="datepicker"  name="tgl_keluar" {if isset($tgl_keluar)} value="{$tgl_keluar}"{/if} />        
      </td>
    </tr>
    <tr>
      <td>Nomor SK Yudisium</td>
      <td><input type="text" id="sk_yudisium" name="sk_yudisium" value="{$pengajuan_wisuda[0]['SK_YUDISIUM']}" /></td>
    </tr>
    <tr>
      <td>Tanggal SK Yudisium</td>
      <td>
          <input type="text" id="tgl_sk_yudisium" class="datepicker"  name="tgl_sk_yudisium" {if isset($tgl_sk_yudisium)} value="{$tgl_sk_yudisium}"{/if} />        
      </td>
    </tr>
    <tr>
      <td>IPK</td>
      <td><input type="text" id="ipk" name="ipk" value="{$pengajuan_wisuda[0]['IPK']}" /></td>
    </tr>
    <tr>
      <td>No Seri Ijazah</td>
      <td><input type="text" name="no_ijasah" value="{$pengajuan_wisuda[0]['NO_IJASAH']}" /></td>
    </tr>
    <tr>
      <td>Judul Skripsi</td>
      <td><textarea cols="50" rows="3" name="judul_ta" id="judul_ta">{$pengajuan_wisuda[0]['JUDUL_TA']}</textarea></td>
    </tr>
    <tr>
      <td>Judul Skripsi (English)</td>
      <td><textarea cols="50" rows="3" name="judul_ta_en">{$pengajuan_wisuda[0]['JUDUL_TA_EN']}</textarea></td>
    </tr>
    <tr>
      <td>Bulan Awal Bimbingan</td>
      <td>
          <input type="text" id="bulan_awal_bimbingan" class="datepicker"  name="bulan_awal_bimbingan" {if isset($bulan_awal_bimbingan)} value="{$bulan_awal_bimbingan}"{/if} />        
      </td>
    </tr>
    <tr>
      <td>Bulan Akhir Bimbingan</td>
      <td>
          <input type="text" id="bulan_akhir_bimbingan" class="datepicker"  name="bulan_akhir_bimbingan" {if isset($bulan_akhir_bimbingan)} value="{$bulan_akhir_bimbingan}"{/if} />        
      </td>
    </tr>
  <!--  <tr>
      <td>Mata Ajar</td>
      <td>{$pengajuan_wisuda[0]['NM_MATA_KULIAH']}</td>
    </tr>
    <tr>
      <td>Nilai</td>
      <td>{$pengajuan_wisuda[0]['NILAI_HURUF']}</td>
    </tr>-->
    <tr>
    	<td>Status Pengajuan</td>
      <td>{if $pengajuan_wisuda[0]['YUDISIUM'] == ''}BELUM{else}SUDAH{/if}</td>
    </tr>
    <tr>
    	<td colspan="2" style="text-align:center">
          	<input type="hidden" value="{$pengajuan_wisuda[0]['NIM_MHS']}" name="nim" />
          	<input type="hidden" value="{$pengajuan_wisuda[0]['ID_MHS']}" name="id_mhs" />
            <input type="hidden" value="{$pengajuan_wisuda[0]['ID_PENGAJUAN_WISUDA']}" name="id_pengajuan_wisuda" />
            <input type="hidden" value="update" name="mode" />
          	{if $pengajuan_wisuda[0]['YUDISIUM'] == '2'}
              	<input type="submit" value="Update" />
            {else}
               Silakan Cek Status Yudisium Mahasiswa
            {/if}
      </td>
    </tr>
  </table>
</form>
{elseif isset($cek)}
 <table width="500">
  <tr>
    <th colspan="2" style="text-align:center">Biodata</th>
  </tr>
  <tr>
    <td>NIM</td>
    <td>{$cek[0]['NIM_MHS']}</td>
  </tr>
  <tr>
    <td>Nama</td>
    <td>{$cek[0]['NM_PENGGUNA']}</td>
  </tr>
  <tr>
    <td>Program Studi</td>
    <td>{$cek[0]['NM_JENJANG']} - {$cek[0]['NM_PROGRAM_STUDI']}</td>
  </tr>
  <tr>
  	<td>Status Akademik</td>
    <td>{$cek[0]['NM_STATUS_PENGGUNA']}</td>
  </tr>
  </table>
{/if}


{literal}
    <script>
            $('#lengkap').submit(function() {
                    
                    if($('#tgl_sk_yudisium').val()==''||$('#bulan_awal_bimbingan').val()==''||$('#bulan_akhir_bimbingan').val()==''||$('#sk_yudisium').val()==''||$('#judul_ta').val()==''){
                            $('#alert').fadeIn();
                    return false;
                    }  
            });
            $('#lengkap').validate();
            $( ".datepicker" ).datepicker({dateFormat:'dd-mm-yy',changeMonth: true,
      changeYear: true});
    </script>
{/literal}