<div class="center_title_bar">Nilai Mahasiswa Yang Belum Ada/ Belum Masuk</div>
{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]]
		}
	); 
} 
);
</script>
{/literal}
<table class="tablesorter" id="myTable">
	<thead>
	<tr>
    	<th style="text-align:center">NO</th>
        <th style="text-align:center">NAMA</th>
        <th style="text-align:center">NIM</th>
        <th style="text-align:center">PROGRAM STUDI</th>
        <th style="text-align:center">JENJANG</th>
        <th style="text-align:center">STATUS AKADEMIK</th>
    </tr>
    </thead>
    <tbody>
    {$no = 1}
    {foreach $mhs as $data}
    <tr>
    	<td>{$no++}</td>
        <td>{$data.NM_PENGGUNA}</td>
        <td>{$data.NIM_MHS}</td>
        <td>{$data.NM_PROGRAM_STUDI}</td>
        <td>{$data.NM_JENJANG}</td>
        <td>{$data.NM_STATUS_PENGGUNA}</td>
    </tr>
    {/foreach}
    </tbody>
</table>