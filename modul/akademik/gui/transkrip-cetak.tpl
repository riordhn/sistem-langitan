<style>
table.transkrip{
	border:#999 1px solid;
	font-size:9px;
	border-collapse:collapse;}
</style>
<p>&nbsp;</p>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:8x;">
  <tr>
    <td colspan="7" align="center"><p><strong>TRANSKRIP AKADEMIK</strong></p>
    <p><strong>NO : ...</strong></p></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
           <td width="17%">Nama</td>
           <td width="1%"><strong>: </strong></td>
           <td width="24%"><strong>Sugiono</strong></td>
           <td width="2%">&nbsp;</td>
           <td width="23%">Nomor Ijazah</td>
           <td width="1%"><strong>:</strong></td>
           <td width="32%"><strong>3215/2212/2122</strong></td>
  </tr>
         <tr>
           <td width="17%">NIM</td>
           <td width="1%"><strong>: </strong></td>
           <td width="24%"><strong>0903123456</strong></td>
           <td width="2%">&nbsp;</td>
           <td >Tanggal Terdaftar Pertama Kali</td>
           <td ><strong>: </strong></td>
           <td ><strong>1 September 2005</strong></td>
         </tr>
         <tr>
           <td width="17%">Program Studi</td>
           <td width="1%"><strong>: </strong></td>
           <td width="24%"><strong>Kedokteran Gigi</strong></td>
           <td width="2%">&nbsp;</td>
           <td >Tanggal Lulus</td>
           <td ><strong>: </strong></td>
           <td ><strong>3 Maret 2010</strong></td>
         </tr>
         <tr>
           <td >Tempat, Tanggal Lahir</td>
           <td ><strong>: </strong></td>
           <td ><strong>Surabaya,6 September 1984</strong></td>
           <td >&nbsp;</td>
           <td >Judul Skripsi/ Tesis/ Disertasi</td>
           <td ><strong>: </strong></td>
           <td ><strong>Emmbedding Komplemen Graph Sikel</strong></td>
         </tr>
</table>
<p>&nbsp;</p>
<table border="0" width="100%">
<tr>
<td width="49%">
<table cellspacing="0" cellpadding="1" border="1" width="100%" style="font-size:25px;">
  <thead>
    <tr bgcolor="#000000" style="color:#FFF;">
      <td width="20%" align="center" bgcolor="#333333"><font color="#FFFFFF">Kode MK</font></td>
      <td width="42%" align="center" bgcolor="#333333"><font color="#FFFFFF">Nama Mata Kuliah </font></td>
      <td width="19%" align="center" bgcolor="#333333"><font color="#FFFFFF">SKS</font></td>
      <td width="19%" align="center" bgcolor="#333333"><font color="#FFFFFF">Nilai</font></td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td width="20%">MK-0001</td>
      <td width="42%">Agama</td>
      <td width="19%" align="center" >2</td>
      <td width="19%" align="center" >AB</td>
    </tr>
    <tr>
      <td>MK-0002</td>
      <td >Bahasa Inggris</td>
      <td align="center" >2</td>
      <td align="center" >A</td>
    </tr>
    <tr>
      <td>MK-0003</td>
      <td >Biologi</td>
      <td align="center" >4</td>
      <td align="center" >BC</td>
    </tr>
    <tr>
      <td>MK-0004</td>
      <td >Ilmu Fisika</td>
      <td align="center" >3</td>
      <td align="center" >BC</td>
    </tr>
    <tr>
      <td>MK-0005</td>
      <td >Ilmu Kimia</td>
      <td align="center" >3</td>
      <td align="center" >C</td>
    </tr>
    <tr>
      <td>MK-0006</td>
      <td >Ilmu Anatomi I</td>
      <td align="center" >3</td>
      <td align="center" >BC</td>
    </tr>
    <tr>
      <td>MK-0007</td>
      <td >Ilmu Material</td>
      <td align="center" >1</td>
      <td align="center" >C</td>
    </tr>
    <tr>
      <td>MK-0008</td>
      <td >Praktikum Ilmu Material dan Teknik Kedokteran Gigi</td>
      <td align="center" >1</td>
      <td align="center" >A</td>
    </tr>
    <tr>
      <td>MK-0009</td>
      <td >Ilmu Komunikasi</td>
      <td align="center" >1</td>
      <td align="center" >A</td>
    </tr>
    <tr>
      <td>MK-0010</td>
      <td >Ilmu Sosial</td>
      <td align="center" >&nbsp;</td>
      <td align="center" >&nbsp;</td>
    </tr>
    <tr>
      <td>MK-0011</td>
      <td >Pancasila</td>
      <td align="center" >2</td>
      <td align="center" >AB</td>
    </tr>
    <tr>
      <td>MK-0012</td>
      <td >Histologi</td>
      <td align="center" >3</td>
      <td align="center" >C</td>
    </tr>
    <tr>
      <td>MK-0013</td>
      <td >Antropologi</td>
      <td align="center" >1</td>
      <td align="center" >B</td>
    </tr>
    <tr>
      <td>MK-0014</td>
      <td >Ilmu Anatomi II</td>
      <td align="center" >2</td>
      <td align="center" >BC</td>
    </tr>
    <tr>
      <td width="20%">MK-0001</td>
      <td width="42%">Agama</td>
      <td width="19%" align="center" >2</td>
      <td width="19%" align="center" >AB</td>
    </tr>
    <tr>
      <td>MK-0016</td>
      <td >Bahasa Inggris</td>
      <td align="center" >2</td>
      <td align="center" >A</td>
    </tr>
    <tr>
      <td>MK-0017</td>
      <td >Biologi</td>
      <td align="center" >4</td>
      <td align="center" >BC</td>
    </tr>
    <tr>
      <td>MK-0018</td>
      <td >Ilmu Fisika</td>
      <td align="center" >3</td>
      <td align="center" >BC</td>
    </tr>
    <tr>
      <td>MK-0019</td>
      <td >Ilmu Kimia</td>
      <td align="center" >3</td>
      <td align="center" >C</td>
    </tr>
    <tr>
      <td>MK-0020</td>
      <td >Ilmu Anatomi I</td>
      <td align="center" >3</td>
      <td align="center" >BC</td>
    </tr>
    <tr>
      <td>MK-0021</td>
      <td >Ilmu Material</td>
      <td align="center" >1</td>
      <td align="center" >C</td>
    </tr>
    <tr>
      <td>MK-0022</td>
      <td >Praktikum Ilmu Material dan Teknik Kedokteran Gigi</td>
      <td align="center" >1</td>
      <td align="center" >A</td>
    </tr>
    <tr>
      <td>MK-0023</td>
      <td >Ilmu Komunikasi</td>
      <td align="center" >1</td>
      <td align="center" >A</td>
    </tr>
    <tr>
      <td>MK-0024</td>
      <td >Ilmu Sosial</td>
      <td align="center" >&nbsp;</td>
      <td align="center" >&nbsp;</td>
    </tr>
    <tr>
      <td>MK-0025</td>
      <td >Pancasila</td>
      <td align="center" >2</td>
      <td align="center" >AB</td>
    </tr>
    <tr>
      <td>MK-0026</td>
      <td >Histologi</td>
      <td align="center" >3</td>
      <td align="center" >C</td>
    </tr>
    <tr>
      <td>MK-0027</td>
      <td >Antropologi</td>
      <td align="center" >1</td>
      <td align="center" >B</td>
    </tr>
    <tr>
      <td>MK-0028</td>
      <td >Ilmu Anatomi II</td>
      <td align="center" >2</td>
      <td align="center" >BC</td>
    </tr>
    <tr>
      <td>MK-0029</td>
      <td >Pancasila</td>
      <td align="center" >2</td>
      <td align="center" >AB</td>
    </tr>
    <tr>
      <td>MK-0030</td>
      <td >Histologi</td>
      <td align="center" >3</td>
      <td align="center" >C</td>
    </tr>
    <tr>
      <td>MK-0031</td>
      <td >Antropologi</td>
      <td align="center" >1</td>
      <td align="center" >B</td>
    </tr>
    <tr>
      <td>MK-0032</td>
      <td >Ilmu Anatomi II</td>
      <td align="center" >2</td>
      <td align="center" >BC</td>
    </tr>
    <tr>
      <td>MK-0033</td>
      <td >Pancasila</td>
      <td align="center" >2</td>
      <td align="center" >AB</td>
    </tr>
    <tr>
      <td>MK-0034</td>
      <td >Histologi</td>
      <td align="center" >3</td>
      <td align="center" >C</td>
    </tr>
    <tr>
      <td>MK-0035</td>
      <td >Antropologi</td>
      <td align="center" >1</td>
      <td align="center" >B</td>
    </tr>
    <tr>
      <td>MK-0036</td>
      <td >Ilmu Anatomi II</td>
      <td align="center" >2</td>
      <td align="center" >BC</td>
    </tr>
    <tr>
      <td>MK-0037</td>
      <td >Histologi</td>
      <td align="center" >2</td>
      <td align="center" >AB</td>
    </tr>
    <tr>
      <td>MK-0038</td>
      <td >Antropologi</td>
      <td align="center" >3</td>
      <td align="center" >C</td>
    </tr>
    <tr>
      <td>MK-0039</td>
      <td >Ilmu Anatomi II</td>
      <td align="center" >1</td>
      <td align="center" >B</td>
    </tr>
    <tr>
      <td>MK-0040</td>
      <td >Ilmu Material 2</td>
      <td align="center" >2</td>
      <td align="center" >BC</td>
    </tr>
  </tbody>
</table>
</td>
<td width="1%">&nbsp;</td>
<td width="49%">
<table cellspacing="0" cellpadding="1" border="1" width="100%" style="font-size:25px;">
  <thead>
    <tr bgcolor="#000000" style="color:#FFF;">
      <td width="20%" align="center" bgcolor="#333333"><font color="#FFFFFF">Kode MK</font></td>
      <td width="42%" align="center" bgcolor="#333333"><font color="#FFFFFF">Nama Mata Kuliah </font></td>
      <td width="19%" align="center" bgcolor="#333333"><font color="#FFFFFF">SKS</font></td>
      <td width="19%" align="center" bgcolor="#333333"><font color="#FFFFFF">Nilai</font></td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td width="20%">MK-0041</td>
      <td width="42%" >Agama</td>
      <td width="19%" align="center" >2</td>
      <td  width="19%" align="center" >AB</td>
    </tr>
    <tr>
      <td>MK-0042</td>
      <td >Bahasa Inggris</td>
      <td align="center" >2</td>
      <td align="center" >A</td>
    </tr>
    <tr>
      <td>MK-0043</td>
      <td >Biologi</td>
      <td align="center" >4</td>
      <td align="center" >BC</td>
    </tr>
    <tr>
      <td>MK-0044</td>
      <td >Ilmu Fisika</td>
      <td align="center" >3</td>
      <td align="center" >BC</td>
    </tr>
    <tr>
      <td>MK-0045</td>
      <td >Ilmu Kimia</td>
      <td align="center" >3</td>
      <td align="center" >C</td>
    </tr>
    <tr>
      <td>MK-0046</td>
      <td >Ilmu Anatomi I</td>
      <td align="center" >3</td>
      <td align="center" >BC</td>
    </tr>
    <tr>
      <td>MK-0047</td>
      <td >Ilmu Material</td>
      <td align="center" >1</td>
      <td align="center" >C</td>
    </tr>
    <tr>
      <td>MK-0048</td>
      <td >Praktikum Ilmu Material dan Teknik Kedokteran Gigi</td>
      <td align="center" >1</td>
      <td align="center" >A</td>
    </tr>
    <tr>
      <td>MK-0049</td>
      <td >Ilmu Komunikasi</td>
      <td align="center" >1</td>
      <td align="center" >A</td>
    </tr>
    <tr>
      <td>MK-0050</td>
      <td >Ilmu Sosial</td>
      <td align="center" >&nbsp;</td>
      <td align="center" >&nbsp;</td>
    </tr>
    <tr>
      <td>MK-0051</td>
      <td >Pancasila</td>
      <td align="center" >2</td>
      <td align="center" >AB</td>
    </tr>
    <tr>
      <td>MK-0052</td>
      <td >Histologi</td>
      <td align="center" >3</td>
      <td align="center" >C</td>
    </tr>
    <tr>
      <td>MK-0053</td>
      <td >Antropologi</td>
      <td align="center" >1</td>
      <td align="center" >B</td>
    </tr>
    <tr>
      <td>MK-0054</td>
      <td >Ilmu Anatomi II</td>
      <td align="center" >2</td>
      <td align="center" >BC</td>
    </tr>
    <tr>
      <td>MK-0055</td>
      <td width="42%" >Agama</td>
      <td width="19%" align="center" >2</td>
      <td  width="19%" align="center" >AB</td>
    </tr>
    <tr>
      <td>MK-0056</td>
      <td >Bahasa Inggris</td>
      <td align="center" >2</td>
      <td align="center" >A</td>
    </tr>
    <tr>
      <td>MK-0057</td>
      <td >Biologi</td>
      <td align="center" >4</td>
      <td align="center" >BC</td>
    </tr>
    <tr>
      <td>MK-0058</td>
      <td >Ilmu Fisika</td>
      <td align="center" >3</td>
      <td align="center" >BC</td>
    </tr>
    <tr>
      <td>MK-0059</td>
      <td >Ilmu Kimia</td>
      <td align="center" >3</td>
      <td align="center" >C</td>
    </tr>
    <tr>
      <td>MK-0060</td>
      <td >Ilmu Anatomi I</td>
      <td align="center" >3</td>
      <td align="center" >BC</td>
    </tr>
    <tr>
      <td>MK-0061</td>
      <td >Ilmu Material</td>
      <td align="center" >1</td>
      <td align="center" >C</td>
    </tr>
    <tr>
      <td>MK-0062</td>
      <td >Praktikum Ilmu Material dan Teknik Kedokteran Gigi</td>
      <td align="center" >1</td>
      <td align="center" >A</td>
    </tr>
    <tr>
      <td>MK-0063</td>
      <td >Ilmu Komunikasi</td>
      <td align="center" >1</td>
      <td align="center" >A</td>
    </tr>
    <tr>
      <td>MK-0064</td>
      <td >Ilmu Sosial</td>
      <td align="center" >&nbsp;</td>
      <td align="center" >&nbsp;</td>
    </tr>
    <tr>
      <td>MK-0065</td>
      <td >Pancasila</td>
      <td align="center" >2</td>
      <td align="center" >AB</td>
    </tr>
    <tr>
      <td>MK-0066</td>
      <td >Histologi</td>
      <td align="center" >3</td>
      <td align="center" >C</td>
    </tr>
    <tr>
      <td>MK-0067</td>
      <td >Antropologi</td>
      <td align="center" >1</td>
      <td align="center" >B</td>
    </tr>
    <tr>
      <td>MK-0068</td>
      <td >Ilmu Anatomi II</td>
      <td align="center" >2</td>
      <td align="center" >BC</td>
    </tr>
    <tr>
      <td>MK-0069</td>
      <td >Pancasila</td>
      <td align="center" >2</td>
      <td align="center" >AB</td>
    </tr>
    <tr>
      <td>MK-0070</td>
      <td >Histologi</td>
      <td align="center" >3</td>
      <td align="center" >C</td>
    </tr>
    <tr>
      <td>MK-0071</td>
      <td >Antropologi</td>
      <td align="center" >1</td>
      <td align="center" >B</td>
    </tr>
    <tr>
      <td>MK-0072</td>
      <td >Ilmu Anatomi II</td>
      <td align="center" >2</td>
      <td align="center" >BC</td>
    </tr>
    <tr>
      <td>MK-0073</td>
      <td >Pancasila</td>
      <td align="center" >2</td>
      <td align="center" >AB</td>
    </tr>
    <tr>
      <td>MK-0074</td>
      <td >Histologi</td>
      <td align="center" >3</td>
      <td align="center" >C</td>
    </tr>
    <tr>
      <td>MK-0075</td>
      <td >Antropologi</td>
      <td align="center" >1</td>
      <td align="center" >B</td>
    </tr>
    <tr>
      <td>MK-0076</td>
      <td >Ilmu Anatomi II</td>
      <td align="center" >2</td>
      <td align="center" >BC</td>
    </tr>
    <tr>
      <td>MK-0077</td>
      <td >Pancasila</td>
      <td align="center" >2</td>
      <td align="center" >AB</td>
    </tr>
    <tr>
      <td>MK-0078</td>
      <td >Histologi</td>
      <td align="center" >3</td>
      <td align="center" >C</td>
    </tr>
    <tr>
      <td>MK-0079</td>
      <td >Antropologi</td>
      <td align="center" >1</td>
      <td align="center" >B</td>
    </tr>
    <tr>
      <td>MK-0080</td>
      <td >Ilmu Anatomi II</td>
      <td align="center" >2</td>
      <td align="center" >BC</td>
    </tr>
  </tbody>
</table>
</td>
</tr>
</table>
<p>&nbsp;</p>
<table border="0" width="100%" style="font-size:30px;;">
  <tr valign="top">
    <td width="50%">
    <table border="0" width="100%" style="font-size:24px;">
      <tr>
        <td width="46%"><div align="left">Kredit Yang Ditempuh</div></td>
        <td width="6%"><div align="left">:</div></td>
        <td width="48%"><div align="left">144 SKS</div></td>
      </tr>
      <tr>
        <td><div align="left">Index Prestasi Kumulatif</div></td>
        <td><div align="left">:</div></td>
        <td><div align="left">3.57</div></td>
      </tr>
      <tr>
        <td><div align="left">Predikat Kelulusan</div></td>
        <td><div align="left">:</div></td>
        <td><div align="left">Dengan Pujian</div></td>
      </tr>
    </table></td>
    <td width="50%" rowspan="3"><p align="center">Surabaya, 04 April 2011</p>
      <p align="center">Dekan,</p>
      <p align="center">&nbsp;</p>
      <p align="center">&nbsp;</p>
      <p align="center">Prof. Dr. Muslich Anshori, SE., MSc.,Ak</p>
      <p align="center">NIP. 131570339</p>      <div align="center"></div>      <div align="center"></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table border="1" width="100%" style="font-size:22px;">
      <tr>
        <td width="30%"><div align="center">Nilai</div></td>
        <td width="20%"><div align="center">Bobot</div></td>
        </tr>
      <tr>
        <td><div align="center">A</div></td>
        <td><div align="center">4</div></td>
        </tr>
      <tr>
        <td><div align="center">AB</div></td>
        <td><div align="center">3,5</div></td>
        </tr>
      <tr>
        <td><div align="center">B</div></td>
        <td><div align="center">3</div></td>
        </tr>
      <tr>
        <td><div align="center">BC</div></td>
        <td><div align="center">2,5</div></td>
        </tr>
      <tr>
        <td><div align="center">C</div></td>
        <td><div align="center">2</div></td>
        </tr>
      <tr>
        <td><div align="center">D</div></td>
        <td><div align="center">1</div></td>
        </tr>
      <tr>
        <td><div align="center">E</div></td>
        <td><div align="center">0</div></td>
        </tr>
    </table></td>
  </tr>
</table>
