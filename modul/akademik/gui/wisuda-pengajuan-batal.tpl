<div class="center_title_bar">Pembatalan Yudisium</div>  

<input type="button" value="<< Kembali" onclick="history.back(-1)" />

<h2><font color="#FF0000">{$pesan}</font></h2>
{if isset($pengajuan_wisuda)}
<form method="post" action="wisuda-pengajuan.php">

            <input type="hidden" value="{$pengajuan_wisuda[0]['NIM_MHS']}" name="nim" />
            <input type="hidden" value="{$pengajuan_wisuda[0]['ID_MHS']}" name="id_mhs" />
            <input type="hidden" value="{$pengajuan_wisuda[0]['ID_PENGAJUAN_WISUDA']}" name="id_pengajuan_wisuda" />
            <input type="hidden" value="batal" name="mode" />
            
<table width="500">
  <tr>
    <th colspan="2" style="text-align:center">Biodata</th>
  </tr>
  <tr>
    <td>NIM</td>
    <td>{$pengajuan_wisuda[0]['NIM_MHS']}</td>
  </tr>
  <tr>
    <td>Nama</td>
    <td>{$pengajuan_wisuda[0]['NM_PENGGUNA']}</td>
  </tr>
  <tr>
    <td>Program Studi</td>
    <td>{$pengajuan_wisuda[0]['NM_JENJANG']} - {$pengajuan_wisuda[0]['NM_PROGRAM_STUDI']}</td>
  </tr>
  <tr>
    <td>Periode</td>
    <td>{if $pengajuan_wisuda[0]['NM_TARIF_WISUDA'] == ' - '}{$periode['NM_TARIF_WISUDA']}{else}{$pengajuan_wisuda[0]['NM_TARIF_WISUDA']}{/if}</td>
  </tr>
<!--  <tr>
    <td>Mata Ajar</td>
    <td>{$pengajuan_wisuda[0]['NM_MATA_KULIAH']}</td>
  </tr>
  <tr>
    <td>Nilai</td>
    <td>{$pengajuan_wisuda[0]['NILAI_HURUF']}</td>
  </tr>-->
  <tr>
  	<td>Status Pengajuan</td>
    <td>{if $pengajuan_wisuda[0]['YUDISIUM'] == ''}BELUM{else}SUDAH{/if}</td>
  </tr>
    <tr>
      <td>Keterangan Batal</td>
      <td><textarea cols="50" rows="3" name="keterangan_batal"></textarea></td>
    </tr>
    <tr>
    	<td colspan="2" style="text-align:center">
            <input type="submit" value="Simpan Pembatalan" />
      </td>
    </tr>
</table>
</form>
{/if}
