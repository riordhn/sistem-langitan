<div class="center_title_bar">TRANSKRIP MAHASISWA ASING</div>
{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
            8: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}

<p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
<form action="transkrip-asing.php" method="post" name="id_transkrip" id="id_transkrip">
<input type="hidden" name="action" value="view" >
NIM : <input type="text" name="nim" value="">&nbsp;&nbsp;&nbsp;
Semester : <select name="id_smt">
			{foreach $smt as $data}
            	<option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER == $smarty.request.id_smt or $data.ID_SEMESTER == $smt_aktif} selected="selected"{/if}>{$data.THN_AKADEMIK_SEMESTER} / {$data.NM_SEMESTER}</option>
            {/foreach}
            </select>&nbsp;&nbsp;&nbsp;<input type="submit" name="View" value="View">
{if isset($smarty.request.nim)}
<input type="button" name="cetak" value="Cetak" onclick="window.open('{$LINK_ASING}','baru');">
{/if}
</form>
		</td>
	</tr>
</table>
</p>
{if isset($smarty.request.nim)}
{foreach item="list0" from=$T_MHS}
<table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr>
             <td>NIM</td>
			 <td>:</td>
			 <td>{$list0.NIM_MHS_ASING}</td>
           </tr>
	   <tr>
             <td>Nama Mahasiswa</td>
			 <td>:</td>
			 <td>{$list0.NM_PENGGUNA}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
             <td>Program Studi</td>
			 <td>:</td>
			 <td>{$list0.NM_JENJANG} {$list0.NM_PROGRAM_STUDI}</td>
           </tr>
</table>
{/foreach}

<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
			 <th width="15%">Kode</th>
			 <th width="50%">Nama MA</th>
             <th width="10%">SKS</th>
			 <th width="10%">Nilai</th>
			 <th width="15%">Bobot</th>
           </tr>
		</thead>
		<tbody>
		   {foreach name=test item="list" from=$T_MK}
           <tr>
             <td>{$list.KODE}</td>
             <td>{$list.NAMA}</td>
             <td><center>{$list.SKS}</center></td>
             <td><center>{$list.NILAI}</center></td>
			 <td><center>{($list.BOBOT*$list.SKS)|number_format:2:".":","}</center></td>
           </tr>
            {foreachelse}
        <tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		<tbody>
		{foreach item="footer" from=$T_IPK}
           <tr>
             <td colspan="2" style="text-align:right;"><b>Total SKS dan Bobot</b>&nbsp;&nbsp;&nbsp;</td>
             <td><center><b>{$footer.SKS_TOTAL}</b></center></td>
			 <td></td>
			 <td><center><b>{$footer.BOBOT_TOTAL}</b></center></td>
           </tr>
           <tr>
			 <td colspan="2" style="text-align:right;"><b>Indeks Prestasi Kumulatif</b>&nbsp;&nbsp;&nbsp;</td>
             <td colspan="3"><center><b>{$footer.IPK}</b></center></td>
           </tr>
        {/foreach}
</table>
{/if}
