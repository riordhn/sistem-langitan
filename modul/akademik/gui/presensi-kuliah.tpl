{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
            8: { sorter: false }
		}
		}
	); 
} 
);
</script>
{/literal}

<div class="center_title_bar">Cetak Presensi Perkuliahan {foreach item="list1" from=$NM_PRODI}{$list1.PRODI} {/foreach}</div>
<form action="presensi-kuliah.php" method="post" name="id_daftarmk" id="id_daftarmk">
<input type="hidden" name="action" value="view">
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td>
			 	Prodi : 
                <select name="kdprodi">
    		   <option value=''>------</option>
			   <option value='all-{$FAK}' style="font-weight: bold; color: #fff; background-color: #233d0e;">LIHAT SEMUA</option>
	 		   {foreach item="list" from=$T_PRODI}
    		   {html_options values=$list.ID_PROGRAM_STUDI output=$list.PRODI selected=$smarty.post.kdprodi}
	 		   {/foreach}
			   </select>
			   <input type="submit" name="View" value="View">
		     </td> 
           </tr>
</table>
</form>
{if {$list1.PRODI} == ''}
{else}
		<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
             <th>Kode</th>
             <th>Nama Mata Ajar</th>
             <th>SKS</th>
			 <th>Kelas</th>
             <th>Hari</th>
			 <th>Jam</th>
			 <th>Ruangan</th>
			 <th>Peserta</th>
             <th class="noheader">Aksi</th>
           </tr>
		</thead>
		<tbody>
           {foreach name=test item="list" from=$T_MK}
		   <tr>
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td><center>{$list.KREDIT_SEMESTER}</center></td>
			 <td><center>{$list.NAMA_KELAS}</center></td>
             <td><center>{$list.NM_JADWAL_HARI}</center></td>
			 <td><center>{$list.JAM}</center></td>
			 <td><center>{$list.NM_RUANGAN}</center></td>
			 <td><center>{$list.PESERTA}</center></td>
		{$FAK = 0.5}{* Modifikasi $FAK agar tidak masuk bentuk FEB UNAIR *}
		{if $FAK == 4}
			{if $list.PESERTA == 0}
			<td>&nbsp;</td>
			{else}
            <td>
			<center>
			<input type=button name="cetak5" value="Cetak" onclick="window.open('proses/presensi-kuliah-cetak-feb.php?cetak={$list.ID_KELAS_MK}&hari={$list.ID_JADWAL_HARI}&prodi={$list.ID_PROGRAM_STUDI}&smt={$list.NM_SEMESTER}&thn={$list.TAHUN_AJARAN}','baru2');">
            <input type=button name="cetak5" value="Excel" onclick="window.open('proses/presensi-kuliah-xls-feb.php?xls={$list.ID_KELAS_MK}&hari={$list.ID_JADWAL_HARI}&prodi={$list.ID_PROGRAM_STUDI}&smt={$list.NM_SEMESTER}&thn={$list.TAHUN_AJARAN}','baru2');">
			</center>
			</td>
			{/if}
		{else}
			{if $list.PESERTA == 0}
			<td>&nbsp;</td>
			{elseif $list.NM_JADWAL_HARI == '' or $list.NM_JADWAL_HARI == ':-:'}
			<td>Setting belum lengkap</td>
			{else}
            <td>
			<input type=button name="cetak5" value="Cetak" onclick="window.open('proses/presensi-kuliah-cetak.php?cetak={$list.ID_KELAS_MK}&hari={$list.ID_JADWAL_HARI}&jam={$list.JAM}&prodi={$list.ID_PROGRAM_STUDI}&smt={$list.NM_SEMESTER}&thn={$list.TAHUN_AJARAN}','baru2');">
			<input type=button name="cetak5" value="Excel" onclick="window.open('proses/presensi-kuliah-xls.php?xls={$list.ID_KELAS_MK}&hari={$list.ID_JADWAL_HARI}&prodi={$list.ID_PROGRAM_STUDI}&smt={$list.NM_SEMESTER}&thn={$list.TAHUN_AJARAN}','baru2');">
            </td>
			{/if}
		{/if}
           </tr>
		   {foreachelse}
        <tr><td colspan="9"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
   	 </table>
{/if}