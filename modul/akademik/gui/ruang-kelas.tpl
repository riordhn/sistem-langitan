{literal}
<script type="text/javascript">
$(document).ready(function()
{
$("#myTable").tablesorter(
		{
		sortList: [[0,0]],
		headers: {
6: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}

<div class="center_title_bar">Master Ruang (Fakultas) </div>
<div id="tabs">
	<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Input</div>
	{*<div id="tab3" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Update</div>*}
</div>

<div class="panel" id="panel1" style="display: {$disp1}">
<p> </p>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
		<th>Nama Ruang</th>
		<th>Nama Gedung</th>
		<th>Kapasitas Ruang</th>
		<th>Kapasitas Ujian</th>
		<th>Petugas</th>
		<th>Type</th>
		<th class="noheader">Aksi</th>
	</tr>
	</thead>
	<tbody>
	{foreach name=list item="ruangkelas" from=$T_RK}
	{if $smarty.foreach.list.iteration % 2 == 0}
	<tr bgcolor="lightgray">
	{else}
	<tr>
	{/if}
		<td>{$ruangkelas.NM_RUANGAN}</td>
		<td>{$ruangkelas.NM_GEDUNG}</td>
		<td><center>{$ruangkelas.KAPASITAS_RUANGAN}</center></td>
		<td><center>{$ruangkelas.KAPASITAS_UJIAN}</center></td>
		<td>{$ruangkelas.PENANGGUNG_JAWAB}</td>
		<td>{$ruangkelas.TIPE_RUANGAN}</td>
		<td>
		<center>
		<a href="ruang-kelas.php?action=tampil&id_ruang={$ruangkelas.ID_RUANGAN}">Update</a>
		{*| <a href="ruang-kelas.php?action=del&ruang={$ruangkelas.ID_RUANGAN}">Delete</a>*}
		</center>
		</td>
	</tr>
	{foreachelse}
	<tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>

<div class="panel" id="panel2" style="display: {$disp2}">
<p><br><b>Input master ruangan</b></p>
<form action="ruang-kelas.php" method="post" >
<input type="hidden" name="action" value="add" >
<table class="tb_frame" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>Nama ruang</td>
		<td><center>:</center></td>
		<td><input type="text" name="nm_ruang"></td>
	</tr>
	<tr>
		<td>Kapasitas ruang</td>
		<td><center>:</center></td>
		<td><input type="text" name="kapasitas"></td>
	</tr>
	<tr>
		<td>Kapasitas ujian</td>
		<td><center>:</center></td>
		<td><input type="text" name="kap_ujian"></td>
	</tr>
	<tr>
		<td>Gedung</td>
		<td><center>:</center></td>
		<td>
			<select name="gedung">
			{foreach item="gdg" from=$TGEDUNG}
			{html_options values=$gdg.ID_GEDUNG output=$gdg.KODE_GEDUNG selected=$gedpil}
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Type</td>
		<td><center>:</center></td>
		<td>
			<select name="type_ruang">
				<option>Kuliah</option>
				<option>Praktikum</option>
				<option>Rapat</option>
			</select>
		</td>
	</tr>
</table>
<p><input type="button" name="kembali" value="Kembali" onClick="javascript: displayPanel('1');">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="Submit" value="Tambah"></p>
</form>
</div>

<div class="panel" id="panel3" style="display: {$disp3}">
<p><br><b>Update master ruangan</b></p>
<form action="ruang-kelas.php" method="post" >
<input type="hidden" name="action" value="update" >
{foreach item="ruangkelas1" from=$T_RK1}
<input type="hidden" name="id_ruangan" value="{$ruangkelas1.ID_RUANGAN}" >
<table class="tb_frame" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>Nama ruang </td>
		<td><center>:</center></td>
		<td><input type="text" name="nm_ruang" class="required" value="{$ruangkelas1.NM_RUANGAN}"></td>
	</tr>
	<tr>
		<td>Kapasitas ruang</td>
		<td><center>:</center></td>
		<td><input type="text" name="kapasitas" class="required" value="{$ruangkelas1.KAPASITAS_RUANGAN}"></td>
	</tr>
	<tr>
		<td>Kapasitas ujian</td>
		<td><center>:</center></td>
		<td><input type="text" name="kap_ujian" class="required" value="{$ruangkelas1.KAPASITAS_UJIAN}"></td>
	</tr>
	<tr>
	<td>Gedung</td>
		<td><center>:</center></td>
		<td>
			<select name="gedung">
			{foreach item="gdg" from=$TGEDUNG}
			{html_options values=$gdg.ID_GEDUNG output=$gdg.KODE_GEDUNG selected=$ruangkelas1.ID_GEDUNG}
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Type</td>
		<td><center>:</center></td>
		<td>
			<select name="type_ruang">
				<option>Kuliah</option>
				<option>Praktikum</option>
				<option>Rapat</option>
			</select>
		</td>
	</tr>
{/foreach}
</table>
<p><input type="button" name="kembali" value="Kembali" onClick="window.location.href='#perkuliahan-rk!ruang-kelas.php'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="Submit" value="Update"></p>
</form>
</div>

<script>$('form').validate();</script>