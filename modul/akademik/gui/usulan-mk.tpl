{literal}
	<script type="text/javascript">
		$(document).ready(function () {
			$("#myTable").tablesorter({
				sortList: [[1, 0], [3, 0]],
				headers: {
					4: {sorter: false}
				}
			});
		});
	</script>
{/literal}

<div class="center_title_bar">Usulan / Penawaran Mata Ajar</div>
<form action="usulan-mk.php" method="post" >
	<input type="hidden" name="action" value="view" >
	<table class="tb_frame" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				Prodi : 
				<select name="kdprodi">
					{foreach item="list" from=$T_PRODI}
						{html_options values=$list.ID_PROGRAM_STUDI output=$list.PRODI selected=$PRODI}
					{/foreach}
				</select>
				Tahun ajaran : 
				<select name="thakd">
					{foreach item="list" from=$T_SMT}
						{html_options values=$list.ID_SEMESTER output=$list.SMT selected=$SMT_AKTIF}
					{/foreach}
				</select>
				<input type="submit" name="Submit" value="Preview">
			</td>
		</tr>
	</table>
</form>
<p><b>Mata kuliah yang diusulkan :</b></p>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>Kode</th>
			<th>Nama Mata Ajar</th>
			<th>SKS</th>
			<th>Kelas</th>
			<th class="noheader" width="25%">Tim Pengajar</th>
			<th>Ruang</th>
			<th>Hari</th>
			<th>Jam</th>
		</tr>
	</thead>
	<tbody>
		{foreach name=test item="list" from=$T_USULMK}
			<tr>
				<td>{$list.KD_MATA_KULIAH}</td>
				<td>{$list.NM_MATA_KULIAH}</td>
				<td>{$list.KREDIT_SEMESTER}</td>
				<td>{$list.NAMA_KELAS}</td>
				<td><ol style="margin-bottom: 0">{$list.PENGAJAR}</ol></td>
				<td>{$list.NM_RUANGAN}</td>
				<td>{$list.NM_JADWAL_HARI}</td>
				<td>{$list.NM_JADWAL_JAM}</td>
			</tr>
		{foreachelse}
			<tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
	</tbody>
</table>