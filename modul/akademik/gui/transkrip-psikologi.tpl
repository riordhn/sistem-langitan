{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
            8: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}

<div class="center_title_bar">Transkrip Nilai {foreach item="list1" from=$MHS}{$list1.NM_MHS} {/foreach} </div>
<p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
<form action="transkrip-psikologi.php" method="post" name="id_transkrip" id="id_transkrip">
<input type="hidden" name="action" value="view" >
NIM : <input type="text" name="nim" value="">&nbsp;&nbsp;&nbsp;<input type="submit" name="View" value="View">
{if $list1.NM_MHS == ''}
{else}
{foreach item="cek" from=$T_MHS}
{if $cek.STATUS_AKADEMIK_MHS != 4}
<font color="red"><br><br><b>Maaf, {$cek.NM_PENGGUNA} dengan NIM:{$cek.NIM_MHS} belum dinyatakan lulus, mohon gunakan cetak Rekap nilai.<br><br></b></font>
{else}
{if $cek.NM_JENJANG == 'S1'}
&nbsp;&nbsp;&nbsp;<input type="button" name="cetak" value="Cetak" onclick="window.open('{$LINK_S1}','baru');">
&nbsp;&nbsp;&nbsp;<input type="button" name="print" value="Print" onclick="window.open('{$LINK_S1_EN}','baru');">
&nbsp;&nbsp;&nbsp;<input type="button" name="xls" value="Excel" onclick="window.open('{$XLS}','baru');">
{elseif $cek.NM_JENJANG == 'S2'}
&nbsp;&nbsp;&nbsp;<input type="button" name="cetak" value="Cetak" onclick="window.open('{$LINK_S2}','baru');">
&nbsp;&nbsp;&nbsp;<input type="button" name="print" value="Print" onclick="window.open('{$LINK_S2_EN}','baru');">
&nbsp;&nbsp;&nbsp;<input type="button" name="xls" value="Excel" onclick="window.open('{$XLS}','baru');">
{else}
&nbsp;&nbsp;&nbsp;<input type="button" name="cetak" value="Cetak" onclick="window.open('{$LINK_S3}','baru');">
&nbsp;&nbsp;&nbsp;<input type="button" name="print" value="Print" onclick="window.open('{$LINK_S3_EN}','baru');">
&nbsp;&nbsp;&nbsp;<input type="button" name="xls" value="Excel" onclick="window.open('{$XLS}','baru');">
{/if}
</form>
		</td>
	</tr>
</table>
</p>
{foreach item="list0" from=$T_MHS}
<table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr>
             <td>NIM</td>
			 <td>:</td>
			 <td>{$list0.NIM_MHS}</td>
			 <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
             <td>Nomor Ijasah</td>
			 <td>:</td>
			 <td>{$list0.NO_IJASAH}</td>
           <tr>
             <td>Nama Mahasiswa</td>
			 <td>:</td>
			 <td>{$list0.NM_PENGGUNA}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			 <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
             <td>Tanggal Lulus</td>
			 <td>:</td>
			 <td>{$list0.TGL_LULUS}</td>
           </tr>
           <tr>
             <td>Program Studi</td>
			 <td>:</td>
			 <td>{$list0.NM_JENJANG} {$list0.NM_PROGRAM_STUDI}</td>
			 <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
             <td>Status</td>
			 <td>:</td>
			 <td>{$list0.NM_STATUS_PENGGUNA}</td>
           </tr>
</table>
{/foreach}

<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
			 <th width="15%">Kode</th>
			 <th width="50%">Nama MA</th>
             <th width="10%">SKS</th>
			 <th width="10%">Nilai</th>
			 <th width="15%">Bobot</th>
           </tr>
		</thead>
		<tbody>
		   {foreach name=test item="list" from=$T_MK}
           <tr>
             <td>{$list.KODE}</td>
             <td>{$list.NAMA}</td>
             <td><center>{$list.SKS}</center></td>
             <td><center>{$list.NILAI}</center></td>
			 <td><center>{($list.BOBOT*$list.SKS)|number_format:2:".":","}</center></td>
           </tr>
            {foreachelse}
        <tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		<tbody>
		{foreach item="footer" from=$T_IPK}
           <tr>
             <td colspan="2" style="text-align:right;"><b>Total SKS dan Bobot</b>&nbsp;&nbsp;&nbsp;</td>
             <td><center><b>{$footer.SKS_TOTAL}</b></center></td>
			 <td></td>
			 <td><center><b>{$footer.BOBOT_TOTAL}</b></center></td>
           </tr>
           <tr>
			 <td colspan="2" style="text-align:right;"><b>Indeks Prestasi Kumulatif</b>&nbsp;&nbsp;&nbsp;</td>
             <td colspan="3"><center><b>{$footer.IPK}</b></center></td>
           </tr>
        {/foreach}
</table>
{/if}
{/foreach}
{/if}
