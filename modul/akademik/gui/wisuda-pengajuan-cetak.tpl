
<div class="center_title_bar">PENGAJUAN CETAK IJASAH</div>
<form action="wisuda-pengajuan-cetak.php" method="post">
	<table width="700">
    	<tr>
            <td>Periode Wisuda</td>
            <td>
            	<select name="periode">
                	{foreach $periode as $data}
                	<option value="{$data.ID_TARIF_WISUDA}" {if $smarty.request.periode==$data.ID_TARIF_WISUDA} selected="selected" {/if}>{$data.NM_TARIF_WISUDA}</option>
					{/foreach}
            	</select>
            </td>
        </tr>
        <tr>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>

{if isset($smarty.request.periode)}
<table>
	<tr>
		<td>Tanggal</td>
		<td>
			<select name="tgl_cetak" id="tgl_cetak">
				{foreach $tgl_cetak as $data}
				<option value="{$data.PENGAJUAN_CETAK_IJASAH}">{$data.PENGAJUAN_CETAK_IJASAH}</option>
				{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="text-align:center">
			<input type="button" value="Cetak" id="cetak" onclick="window.open('wisuda-yudisium-cetak.php?fakultas={$fakultas}&status_bayar=3&periode={$smarty.request.periode}&tgl_cetak='+document.getElementById('tgl_cetak').value);">
		</td>
	</tr>
</table>
{/if}

{if isset($pengajuan)}
<form action="wisuda-pengajuan-cetak.php" method="post">
<table>
<tr>
	<th>NO</th>
	<th>NIM</th>
    <th>NAMA</th>
    <th>PROGRAM STUDI</th>	
	<th>No Ijasah</th>
	<th>Tgl Lulus</th>
	<th>TTL</th>
    <th>Foto</th>
	<th>Tgl Pengajuan</th>
	<th><input type="checkbox" id="cek_all" name="cek_all" /></th>
</tr>
	{$no = 1}
	{foreach $pengajuan as $data}
    	<tr>
        	<td>{$no++}</td>
            <td>{$data.NIM_MHS}</td>
            <td>{$data.NM_PENGGUNA}</td>
            <td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>			
			<td>{$data.NO_IJASAH}</td>
			<td>{$data.TGL_LULUS_PENGAJUAN}</td>
			<td>{$data.LAHIR_IJAZAH}</td>
            <td>
            	{capture assign='foto'}../../foto_wisuda/{$data.NIM_MHS}.jpg{/capture}
                {if $foto|file_exists eq ''} 
  					Belum
                {else}
                	Sudah 
				{/if}
            </td>
			<td>{$data.PENGAJUAN_CETAK_IJASAH}</td>
			<td style="text-align:center">
				{if $data.JUMLAH_CETAK == ''}
					<input type="checkbox" class="cek" name="cek{$no}" {if $data.PENGAJUAN_CETAK_IJASAH != ''}checked="checked"{/if} value="1" />
					<input type="hidden" value="{$data.ID_PENGAJUAN_WISUDA}" name="id{$no}" />
					<input type="hidden" value="{$data.PENGAJUAN_CETAK_IJASAH}" name="tgl_pengajuan{$no}" />
				{else}
				Sudah Dicetak
				{/if}
			</td>
        </tr>
    {/foreach}
	<tr>
		<td colspan="10" style="text-align:center">
			<input type="hidden" value="{$no}" name="no" />
			<input type="hidden" value="{$smarty.post.periode}" name="periode" />
    		<input type="submit" value="Simpan" />
			
        </td>
    </tr>
	
</table>
</form>
{/if}


{literal}
    <script>
		$("#cek_all").click(function()				
			{
				var checked_status = this.checked;
				$(".cek").each(function()
				{
					this.checked = checked_status;
				});
			});
						 
		/*document.getElementById("cetak").onclick = function(){
			window.open('wisuda-pengajuan-cetak-pdf.php?tgl_cetak='+document.getElementById("tgl_cetak").value);
		}*/
			
    </script>
{/literal}