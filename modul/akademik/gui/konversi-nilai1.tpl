  
<div class="center_title_bar">Konversi Nilai</div> 
<form action="konversi-nilai.php" method="post">
<input type="hidden" name="action" value="view" >
     <table width="100%" border="1">
        <tr>
               <td>NIM</td>
               <td>:</td>
               <td><input name="nim" type="text"></td>
			   <td><input type="submit" name="Submit" value="Tampil"></td>
        </tr>
	</table>
</form>	

{if isset($mhs)}
<form action="konversi-nilai.php" method="post" name="id_konversi_nilai" id="id_konversi_nilai">
	<table>
    	<tr>
        	<th colspan="4">BIODATA</th>
        </tr>
        <tr>
        	<td>NIM</td>
            <td>{$mhs.NIM_MHS}</td>
        </tr>
        <tr>
        	<td>NAMA</td>
            <td>{$mhs.NM_PENGGUNA}</td>
        </tr>
        <tr>
        	<td>PRODI</td>
            <td>{$mhs.NM_JENJANG} {$mhs.NM_PROGRAM_STUDI}</td>
        </tr>
        <tr>
        	<td>Kode MA</td>
            <td>
            	<select name="kd_ma" class="required">
                	<option value=""> ---------- </option>
                    {foreach $kd_ma as $data}
                    	<option value="{$data.ID_KURIKULUM_MK}">{$data.KD_MATA_KULIAH} - {$data.NM_MATA_KULIAH}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
        	<td>Konversi ke MA</td>
            <td>
            	<select name="kd_konversi" class="required">
                	<option value=""> ---------- </option>
                    {foreach $kd_konversi as $data}
                    	<option value="{$data.ID_KURIKULUM_MK}">{$data.KD_MATA_KULIAH} - {$data.NM_MATA_KULIAH}</option>
                    {/foreach}
                </select><br />
				* Mata ajar yang ditawarkan prodi
            </td>
        </tr>
        <tr>
        	<td>Semester Konversi</td>
            <td>
            	<select name="semester" class="required">
                	<option value=""> ---------- </option>
                    {foreach $T_ST as $data}
                    	<option value="{$data.ID_SEMESTER}">{$data.SMT}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
        	<td>Nilai Huruf Konversi</td>
            <td>
            	<input type="text" name="nilai" maxlength="2" size="2" class="required"/>
            </td>
        </tr>
        <tr>
        	<td colspan="2" style="text-align:center">
            	<input type="hidden" value="{$smarty.post.nim}" name="nim" />
            	<input type="submit" name="Submit" value="Proses">
            </td>
        </tr>
    </table>
    </form>	
{/if}
		
{literal}
    <script>
			$("#id_konversi_nilai").validate();
    </script>
{/literal}
    