<div class="center_title_bar">CEKAL PEMBAYARAN MAHASISWA</div>
<form action="evaluasi-cekal-bayar.php" method="post">
	<table>
		<tr>
			<td>NIM</td>
			<td>:</td>
			<td><input type="text" name="nim" value="{if !empty($smarty.post.nim)}{$smarty.post.nim}{/if}" /></td>
			<td><input type="submit" value="Tampil" /></td>
		</tr>
	</table>
</form>

{if isset($mhs)}
<form action="evaluasi-cekal-bayar.php" method="post" id="f1">
	<table>
		<tr>
			<th colspan="3">Setting Cekal Pembayaran</th>
		</tr>
		<tr>
			<td>NIM</td>
			<td>:</td>
			<td>{$mhs[0]['NIM_MHS']}</td>
		</tr>
		<tr>
			<td>Nama</td>
			<td>:</td>
			<td>{$mhs[0]['NM_PENGGUNA']}</td>
		</tr>
		<tr>
			<td>Program Studi</td>
			<td>:</td>
			<td>{$mhs[0]['NM_PROGRAM_STUDI']}</td>
		</tr>
		<tr>
			<td>Status Akademik</td>
			<td>:</td>
			<td>{$mhs[0]['NM_STATUS_PENGGUNA']}</td>
		</tr>
		<tr>
			<td>Status Cekal Pembayaran</td>
			<td>:</td>
			<td>{if $mhs[0]['STATUS_CEKAL'] == 0}Boleh Bayar{else}<font color="#FF0000"><b>Tidak Boleh Bayar</b></font>{/if}</td>
		</tr>
        <tr>
			<td>Semester Aktif</td>
			<td>:</td>
			<td>{$semester_aktif['TAHUN_AJARAN']} ({$semester_aktif['NM_SEMESTER']})</td>
		</tr>
		<tr>
			<td>History</td>
			<td>:</td>
			<td>
				{if $mhs[0]['ID_MHS'] == ''}
					Belum pernah tercekal pembayaran
				{else}
				<table>
					<tr>
						<th>Status Cekal</th>
						<th>Semester</th>
						<th>Keterangan</th>
					</tr>
					{foreach $mhs as $data}
					<tr>
						<td>{if $data.CEKAL_UNIVERSITAS == 0}Boleh Bayar{else}Tidak Boleh Bayar{/if}</td>
						<td>{$data.TAHUN_AJARAN} ({$data.NM_SEMESTER})</td>
						<td>{$data.KETERANGAN}</td>
					</tr>
					{/foreach}
				</table>
				{/if}
			</td>
		</tr>
		<tr>
			<td>
				Keterangan
			</td>
			<td>:</td>
			<td><textarea cols="60" rows="5" name="keterangan" id="keterangan" class="required"></textarea></td>
		</tr>
        <tr>
			<td>
				Semester Cekal
			</td>
			<td>:</td>
			<td>
            	<select name="semester">
                	{foreach $semester as $data}
                	<option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER==$semester_aktif['ID_SEMESTER']}selected="selected"{/if}>{$data.TAHUN_AJARAN} ({$data.NM_SEMESTER})</option>
					{/foreach}
            	</select>
            </td>
		</tr>
		<tr>
			<td style="text-align:center" colspan="3">
				<input type="submit" value="Proses Cekal" />
				<input type="hidden" name="nim" value="{$smarty.post.nim}" />
				<input type="hidden" name="id_mhs" value="{$mhs[0]['MHS_ID']}" />
				<input type="hidden" name="mode" value="proses_cekal" />
			</td>
		</tr>
	</table>
</form>
{/if}


<script>$('#f1').validate();</script>
