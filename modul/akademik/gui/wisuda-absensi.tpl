<div class="center_title_bar">ABSENSI WISUDA</div>
<form action="wisuda-absensi.php" method="post" name="f2">
	<table width="700">
    	<tr>
            <td>Periode Wisuda</td>
            <td>
            	<select name="periode">
                	{foreach $periode as $data}
                	<option value="{$data.ID_TARIF_WISUDA}" {if $id_periode==$data.ID_TARIF_WISUDA} selected="selected" {/if}>{$data.NM_TARIF_WISUDA}
					</option>
					{/foreach}
            	</select>
            </td>
            <td style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>

{if isset($absensi)}
<b>Hilangkan centangnya dan isi keterangannya untuk mahasiswa yang TIDAK ikut wisuda, kemudian Simpan</b>
<br /><br />

<form action="wisuda-absensi.php" name="f1" method="post">
<table style="font-size:12px">
	<tr>
		<th>NO</th>
		<th>NIM</th>
		<th>NAMA</th>
		<th>PROGRAM STUDI</th>
		<th>KETERANGAN TIDAK IKUT</th>
		<th>ABSENSI <input type="checkbox" id="cek_all" name="cek_all" checked="checked" /></th>
	</tr>
	{$no=1}
	{foreach $absensi as $data}
		{if ($no % 2) == 0}
		 	{$warna = "#CCCCCC"}
		{else}
		 	{$warna = ""}
		{/if}
    	<tr bgcolor="{$warna}">
			<td>{$no++}</td>
			<td>{$data.NIM_MHS}</td>
			<td>{$data.NM_PENGGUNA}</td>
			<td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
			<td><textarea name="keterangan{$no}">{$data.KETERANGAN_ABSENSI}</textarea></td>
			<td style="text-align:center">
				<input type="checkbox" class="cek" name="cek{$no}" {if $data.ABSENSI_WISUDA == 1}checked="checked"{/if} value="1" />
				<input type="hidden" value="{$data.ID_PEMBAYARAN_WISUDA}" name="id{$no}" />
			</td>
		</tr>
		
	{/foreach}
	<tr>
		<td colspan="6" style="text-align:center">
			<input type="hidden" value="{$no}" name="no" />
			<input type="hidden" value="{$id_periode}" name="periode" />
            <input type="hidden" value="{$id_fakultas}" name="id_fakultas" />
			<input type="submit" value="Simpan" />
			<input type="button" value="Cetak" onclick="window.open('excel-wisuda-absensi.php?id_periode={$id_periode}&id_fakultas={$id_fakultas}','baru2')" />
		</td>
	</tr>
</table>
</form>
{/if}


{literal}
    <script type="text/javascript">
		$("#cek_all").click(function()				
			{
				var checked_status = this.checked;
				$(".cek").each(function()
				{
					this.checked = checked_status;
				});
			});	
    </script>
{/literal}