<div class="center_title_bar">EDIT KRS MAHASISWA (PROGSUS) </div>
<form action="akademik-krs-ps.php" method="post" name="mhs_krs" id="mhs_krs">
<input type="hidden" name="action" value="krs" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr>
	  <td>Nim Mahasiswa </td>
	  <td>:</td>
	  <td><input type="text" name="nim"></td>
	  <td><input type="submit" name="cari1" value="Proses"></td>
   </tr>
</table>
</form>
{if $nim == ""}
{else}
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	  <td>NIM</td>
	  <td>:</td>
	  <td>{$nim}</td>
	</tr>
	<tr>
	  <td>NAMA </td>
	  <td>:</td>
	  <td>{$nama}</td>
	</tr>
	<tr>
	  <td>PRODI </td>
	  <td>:</td>
	  <td>{$prodi}</td>
	</tr>
	<tr>
	  <td>IPS </td>
	  <td>:</td>
	  <td>{$ips}</td>
	</tr>
	<tr>
	  <td>IPK </td>
	  <td>:</td>
	  <td>{$ipk}</td>
	</tr>
	<tr>
	  <td>SKS TTL </td>
	  <td>:</td>
	  <td>{$ttl_sks}</td>
	</tr>
	
	<tr>
	  <td>Jatah SKS (Maks.)</td>
	  <td>:</td>
	  <td>{$sks_maks}</td>
	</tr>

</table>

<div id="tabs">
<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">KRS[PROGSUS]</div>
</div>
<p> </p>
<div class="panel" id="panel1" style="display: {$disp1}">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr class="left_menu" align="center">
			<td width="4%" bgcolor="#333333"><font color="#FFFFFF">No</font></td>
			<td width="5%" bgcolor="#333333"><font color="#FFFFFF">Kode</font></td>
			<td width="33%" bgcolor="#333333"><font color="#FFFFFF">Nama Mata Kuliah</font></td>
			<td width="5%" bgcolor="#333333"><font color="#FFFFFF">SKS</font></td>
			<td width="5%" bgcolor="#333333"><font color="#FFFFFF">KLS</font></td>
			<td width="8%" bgcolor="#333333"><font color="#FFFFFF">Hari</font></td>
			<td width="5%" bgcolor="#333333"><font color="#FFFFFF">Jam</font></td>
			<td width="5%" bgcolor="#333333"><font color="#FFFFFF">Kaps</font></td>
			<td width="5%" bgcolor="#333333"><font color="#FFFFFF">Pst</font></td>
			<td width="20%" bgcolor="#333333"><font color="#FFFFFF">Aksi</font></td>
        </tr>
          {foreach name=test item="list" from=$T_KRS}
        <tr>
          <td>{$smarty.foreach.test.iteration}</td>
          <td>{$list.KD_MATA_KULIAH}</td>
		  <td>{$list.NM_MATA_KULIAH}</td>
          <td>{$list.KREDIT_SEMESTER}</td>
          <td>{$list.NAMA_KELAS}</td>
		  <td>{$list.NM_JADWAL_HARI}</td>
          <td>{$list.NM_JADWAL_JAM}</td>
		  <td>{$list.KAPASITAS_KELAS_MK}</td>
		  <td>{$list.PST}</td>
		  <td height="0" align="center"><input name="Button" type="button" class="button" onClick="location.href='#utility-ekrsps!akademik-krs-ps.php?action=add&nim={$nim}&id_kelas_mk={$list.ID_KELAS_MK}&sks={$list.KREDIT_SEMESTER}&kaps={$list.KAPASITAS_KELAS_MK}';disableButtons()" onMouseOver="window.status='Click untuk Insert';return true" onMouseOut="window.status='Insert KRS'" value="P R O G R A M" /> </td> 
        </tr>
         {foreachelse}
        <tr><td colspan="10"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
	</table>
</div>
		
<div class="panel" id="panel2" style="display: {$disp2}"><br>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr class="left_menu" align="center">
			<td width="4%" bgcolor="#333333"><font color="#FFFFFF">No</font></td>
			<td width="5%" bgcolor="#333333"><font color="#FFFFFF">Kode</font></td>
			<td width="33%" bgcolor="#333333"><font color="#FFFFFF">Nama Mata Kuliah</font></td>
			<td width="5%" bgcolor="#333333"><font color="#FFFFFF">SKS</font></td>
			<td width="5%" bgcolor="#333333"><font color="#FFFFFF">KLS</font></td>
			<td width="12%" bgcolor="#333333"><font color="#FFFFFF">Hari</font></td>
			<td width="12%" bgcolor="#333333"><font color="#FFFFFF">Jam</font></td>
			<td width="12%" bgcolor="#333333"><font color="#FFFFFF">Status</font></td>
			<td width="20%" bgcolor="#333333"><font color="#FFFFFF">Aksi</font></td>
		</tr>
         {foreach name=test item="list1" from=$T_AMBIL}
        <tr>
          <td>{$smarty.foreach.test.iteration}</td>
          <td>{$list1.KD_MATA_KULIAH}</td>
		  <td>{$list1.NM_MATA_KULIAH}</td>
          <td>{$list1.KREDIT_SEMESTER}</td>
          <td>{$list1.NAMA_KELAS}</td>
		  <td>{$list1.NM_JADWAL_HARI}</td>
          <td>{$list1.NM_JADWAL_JAM}</td>
		  <td>{$list1.STATUS}</td>
		  <td height="0" align="center"><input name="Button" type="button" class="button" onClick="location.href='#utility-ekrsps!akademik-krs-ps.php?action=del&idpmk={$list1.ID_PENGAMBILAN_MK}&nim={$nim}';disableButtons()" onMouseOver="window.status='Click untuk Delete';return true" onMouseOut="window.status='Delete KRS'" value="Delete" /> </td>  
        </tr>
         {foreachelse}
        <tr><td colspan="9"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		<tr>
		<td colspan="3"><em>Total SKS Disetujui</em></td>
          <td >{$sksdisetujui}</td>
		  <td colspan="7"><em></em></td>
		</tr>
      </table>
</div>
{/if}
<script>$('form').validate();</script>
