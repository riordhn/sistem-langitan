<div class="center_title_bar">Distribusi Nilai Total {foreach item="list1" from=$NM_PRODI}{$list1.PRODI} {/foreach} {foreach item="list2" from=$THNSMT}{$list2.THN_SMT} {/foreach}</div>
<form action="report-prosentase-nilai.php" method="post" name="id_daftarmk" id="id_daftarmk">
<input type="hidden" name="action" value="view">
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td>
			 	Prodi : 
               <select name="kdprodi">
    		   <option value=''>------</option>
			   <option value='all-{$FAK}' style="font-weight: bold; color: #fff; background-color: #233d0e;">LIHAT SEMUA</option>
	 		   {foreach item="list" from=$T_PRODI}
    		   {html_options values=$list.ID_PROGRAM_STUDI output=$list.PRODI selected=$smarty.post.kdprodi}
	 		   {/foreach}
			   </select>
		     </td>
			 <td>
			 	Tahun/Semester : 
               <select name="kdthnsmt">
    		   <option value=''>------</option>
	 		   {foreach item="list_smt" from=$T_THNSMT}
    		   {html_options values=$list_smt.ID_SEMESTER output=$list_smt.THN_SMT selected=$smarty.post.kdthnsmt}
	 		   {/foreach}
			   </select>
		     </td>
			 <td>			   
			   <input type="submit" name="View" value="View">
		     </td> 
           </tr>
</table>
</form>
{if {$list1.PRODI} == '' && $list2.THNSMT == ''}
{else}
<div class="panel" id="panel1" style="display: block">
<br/>
<p>
<input type=button name="cetak" value="Ekspor ke Excel" onclick="window.open('proses/prosentase-nilai-xls.php?smt={$KD_SMT}&thn={$KD_THN}&fak={$KD_FAK}&prodi={$KD_PRODI}','baru2');">&nbsp;
</p>
<table class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
             <th><center>No</center></th>
             <th><center>Jenjang</center></th>
             <th><center>Program Studi</center></th>
             <th><center>A</center></th>
             <th><center>AB</center></th>
             <th><center>B</center></th>
             <th><center>BC</center></th>
             <th><center>C</center></th>
             <th><center>D</center></th>
             <th><center>E</center></th>
           </tr>
	</thead>
	<tbody>
           {foreach name=test item="list" from=$T_MK}
		   <tr>
             <td><center>{$smarty.foreach.test.iteration}</center></td>
             <td><center>{$list.NM_JENJANG}</center></td>
             <td>{$list.NM_PROGRAM_STUDI}</td>
             <td><center>{$list.JML_A}<br><font color="blue"><b>({$list.NILAI_A}%)</b></font></center></td>
			 <td><center>{$list.JML_AB}<br><font color="blue"><b>({$list.NILAI_AB}%)</b></font></center></td>
             <td><center>{$list.JML_B}<br><font color="blue"><b>({$list.NILAI_B}%)</b></font></center></td>
			 <td><center>{$list.JML_BC}<br><font color="blue"><b>({$list.NILAI_BC}%)</b></font></center></td>
			 <td><center>{$list.JML_C}<br><font color="blue"><b>({$list.NILAI_C}%)</b></font></center></td>
			 <td><center>{$list.JML_D}<br><font color="blue"><b>({$list.NILAI_D}%)</b></font></center></td>
			 <td><center>{$list.JML_E}<br><font color="blue"><b>({$list.NILAI_E}%)</b></font></center></td>
           </tr>
		   {foreachelse}
        <tr><td colspan="10"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
	</tbody>
   	 </table>

<table class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
             <th><center>No</center></th>
             <th><center>Tahun Angkatan</center></th>
             <th><center>A</center></th>
             <th><center>AB</center></th>
             <th><center>B</center></th>
             <th><center>BC</center></th>
             <th><center>C</center></th>
             <th><center>D</center></th>
             <th><center>E</center></th>
           </tr>
	</thead>
	<tbody>
           {foreach name=data item="agk" from=$T_AGK}
		   <tr>
             <td><center>{$smarty.foreach.data.iteration}</center></td>
             <td><center>{$agk.THN_ANGKATAN_MHS}</center></td>
             <td><center>{$agk.JML_A}<br><font color="blue"><b>({$agk.NILAI_A}%)</b></font></center></td>
			 <td><center>{$agk.JML_AB}<br><font color="blue"><b>({$agk.NILAI_AB}%)</b></font></center></td>
             <td><center>{$agk.JML_B}<br><font color="blue"><b>({$agk.NILAI_B}%)</b></font></center></td>
			 <td><center>{$agk.JML_BC}<br><font color="blue"><b>({$agk.NILAI_BC}%)</b></font></center></td>
			 <td><center>{$agk.JML_C}<br><font color="blue"><b>({$agk.NILAI_C}%)</b></font></center></td>
			 <td><center>{$agk.JML_D}<br><font color="blue"><b>({$agk.NILAI_D}%)</b></font></center></td>
			 <td><center>{$agk.JML_E}<br><font color="blue"><b>({$agk.NILAI_E}%)</b></font></center></td>
           </tr>
		   {foreachelse}
        <tr><td colspan="9"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
	</tbody>
   	 </table>
</div>
{/if}