{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
		{
		sortList: [[3,0],[1,0]],
		}
	); 
} 
);
</script>
{/literal}
<div class="center_title_bar">Monitoring Peserta KKN</div> 
<input type="button" value="Cetak" onclick="window.open('proses/cetak-monkkn-mhs.php?fak={$id_fakultas}&smt={$id_smt}');"/>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
			 <th width="5%"><center>No</center></th>
             <th width="5%"><center>NIM</center></th>
			 <th width="20%"><center>Nama Mhs</center></th>
             <th width="10%"><center>Prodi</center></th>
             <th width="5%"><center>L/P</center></th>
			 <th width="5%"><center>Agama</center></th>
			 <th width="5%"><center>STATUS</center></th>
			 <th width="5%"><center>SKS</center></th>
			 <th width="5%"><center>SKS SMT</center></th>
			 <th width="5%"><center>TTL SKS</center></th>
			 <th width="10%"><center>AKSI</center></th>
		  </tr>
	</thead>
	<tbody>
		  {foreach name=test item="list" from=$T_KKN}
			{if ($list.STATUS !='AJ' and $list.TTL<110)}
			<tr bgcolor="red">
			{else}
			<tr>
			{/if}
			 <td>{$smarty.foreach.test.iteration}</td>
             <td>{$list.NIM_MHS}</td>
			 <td>{$list.NM_PENGGUNA}</td>
             <td>{$list.PRODI}</td>
             <td>{$list.KEL}</td>
			 <td>{$list.AGAMA}</td>
			 <td>{$list.STATUS}</td>
			 <td>{$list.SKS_DIPEROLEH}</td>
			 <td>{$list.SKS_SEM}</td>
			 <td>{$list.TTL}</td>
			 {if ($list.STATUS !='AJ' && $list.TTL<110)}
			 <td align="center"><input name="Button" type="button" class="button" onClick="location.href='#utility-mon_kkn!mon-kkn.php?action=drop&id_pmk={$list.ID_PENGAMBILAN_MK}';disableButtons()" onMouseOver="window.status='Click untuk Insert';return true" onMouseOut="window.status='Konversi Nilai'" value="DROP KKN" /> </td> 
			{else}
			<td></td>
			 {/if}
			</tr>
		  {/foreach}
	</tbody>
</table>


    