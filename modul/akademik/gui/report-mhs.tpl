 
<div class="center_title_bar">Mahasiswa {$nama_pt}</div>
<form action="report-mhs.php" method="post">
    <table style="width: 100%">
        <tr>
            <td>Tahun Akademik Mahasiswa</td>
            <td>
                <select name="tahun_akademik">
                    <option value="">Semua</option>
                    {foreach $data_tahun_akademik as $data}
                        <option value="{$data.THN_AKADEMIK_SEMESTER}" {if $smarty.request.tahun_akademik==$data.THN_AKADEMIK_SEMESTER}selected="true"{/if}>{$data.THN_AKADEMIK_SEMESTER}</option>
                    {/foreach}
                </select>
            </td>
			<td>Status Mahasiswa</td>
            <td>
                <select id="status" name="status">
                    <option value="">Semua</option> 
                   {foreach $data_status as $data}
                        <option value="{$data.ID_STATUS_PENGGUNA}" {if $smarty.request.status==$data.ID_STATUS_PENGGUNA}selected="true"{/if}>{$data.NM_STATUS_PENGGUNA}</option>
                    {/foreach}                    
                </select>
            </td>
        </tr>
        <tr>
			<td>Jenjang</td>
			<td>
				<select id="jenjang" name="jenjang">
                    <option value="">Semua</option> 
                   {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $smarty.request.jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}                    
                </select>
			</td>
            <td>Jalur</td>
			<td><select id="jalur" name="jalur">
                    <option value="">Semua</option> 
                   {foreach $data_jalur as $data}
                        <option value="{$data.ID_JALUR}" {if $smarty.request.jalur==$data.ID_JALUR}selected="true"{/if}>{$data.NM_JALUR}</option>
                    {/foreach}                    
                </select>
			</td>
        </tr>
		<tr>
			<td>Program Studi</td>
            <td colspan="4" >
                <select id="program_studi" name="program_studi">
                    <option value="">Semua</option>
					{foreach $program_studi as $data}
                        <option value="{$data.ID_PROGRAM_STUDI}" {if $smarty.request.program_studi==$data.ID_PROGRAM_STUDI}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
                    {/foreach} 
                </select>
            </td>

			
		</tr>
        <tr>
            <td colspan="4" class="center"><input type="submit" value="Tampilkan"/></td>
        </tr>
        <input type="hidden" name="tampil" value="ok"/>
		<input type="hidden" value="{$fakultas}" name="fakultas" id="fakultas" />
    </table>
</form>
{if isset($tampil)}
    <table>
        <tr>
            <th>NO</th>
            <th>NIM</th>
            <th>NAMA</th>
            <th>ANGKATAN</th>
            <th>JENJANG</th>
            <th>PROGRAM STUDI</th>
            <th>FAKULTAS</th>
            <th>STATUS</th>
            <th>ALAMAT</th>
        </tr>
        {$nomer=1}
        {foreach $data_mahasiswa as $data}
            <tr>
                <td>{$nomer++}</td>
                <td>{$data.NIM_MHS}</td>
                <td>{$data.NM_PENGGUNA}</td>
                <td>{$data.THN_ANGKATAN_MHS}</td>
                <td>{$data.NM_JENJANG}</td>
                <td>{$data.NM_PROGRAM_STUDI}</td>
                <td>{$data.NM_FAKULTAS}</td>
                <td>{$data.NM_STATUS_PENGGUNA}</td>
                <td>{$data.ALAMAT_MHS}</td>
            </tr>
        {/foreach}
    </table>
{/if}
{literal}
    <script type="text/javascript">		
		$('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
        function get_link_report(type){
            link=document.location.hash.replace('#report-mhs!', '');
            if(type=='excel'){
                window.location.href='excel-'+link;
            }else
                window.open('cetak-'+link);
        }
    </script>
{/literal}