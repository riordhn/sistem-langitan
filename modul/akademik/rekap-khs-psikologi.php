<?php
/*
YAH 06/06/2011
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$kdprodi=$user->ID_PROGRAM_STUDI;

$id_pengguna= $user->ID_PENGGUNA;
/*
$fak=getvar("select fakultas.id_fakultas from pegawai left join unit_kerja on pegawai.id_unit_kerja=unit_kerja.id_unit_kerja
left join program_studi on unit_kerja.id_program_studi=program_studi.id_program_studi
left join fakultas on program_studi.id_fakultas=fakultas.id_fakultas
where pegawai.id_pengguna=$id_pengguna");
*/
//$kdfak=$fak['ID_FAKULTAS'];
$kdfak = $user->ID_FAKULTAS;
$smarty->assign('FAK', $kdfak);

//Fakultas Lain
if($kdfak!=11){
	echo 'Anda tidak memiliki hak akses';
} else {

$thnsmt=getData("select id_semester as id_semester, tahun_ajaran||' - '||nm_semester as thn_smt from semester where thn_akademik_semester in (
select * from (select distinct thn_akademik_semester
from semester where thn_akademik_semester<=EXTRACT(YEAR FROM sysdate) order by thn_akademik_semester desc)
where rownum<=10) and nm_semester in ('Ganjil', 'Genap')
order by thn_akademik_semester desc, nm_semester desc");
$smarty->assign('T_THNSMT', $thnsmt);

$dataprodi=getData("select id_program_studi,nm_jenjang||'-' || nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_fakultas=$kdfak");
$smarty->assign('T_PRODI', $dataprodi);

if (isset($_REQUEST['action'])=='view') {
$smt = $_POST['kdthnsmt'];
//$smarty->assign('SMT_AKAD', $smt);
$krs_smt=getData("select tahun_ajaran||' - '||nm_semester as thn_smt from semester where id_semester=$smt");
$smarty->assign('THN_SMT', $krs_smt);

$kdprodi = $_POST['kdprodi'];
//$smarty->assign('PRODI', $kdprodi);

$dataprodi1=getData("select nm_jenjang||'-'||nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_program_studi=$kdprodi");
$smarty->assign('NM_PRODI', $dataprodi1);


// Yudi Sulistya, 2012/05/10
/*
$jml_mhs=getvar("
select count(distinct nim_mhs) as jml_mhs from
(
select MAHASISWA.nim_mhs,pengguna.nm_pengguna,mahasiswa.id_program_studi,nm_program_studi,
MATA_KULIAH.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,KURIKULUM_MK.kredit_semester from PENGAMBILAN_MK
left join mahasiswa on pengambilan_mk.id_mhs=MAHASISWA.ID_MHS
left join pengguna on MAHASISWA.id_pengguna=PENGGUNA.id_pengguna
left join program_studi on MAHASISWA.id_program_studi=PROGRAM_STUDI.id_program_studi
left join kelas_mk on PENGAMBILAN_MK.id_kelas_mk=KELAS_MK.id_kelas_mk
left join NAMA_KELAS on KELAS_MK.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk
left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
where PENGAMBILAN_MK.id_semester=$smt and PROGRAM_STUDI.id_fakultas=$kdfak and status_apv_pengambilan_mk=1 and mahasiswa.id_program_studi=$kdprodi
)");
$smarty->assign('JML_MHS', $jml_mhs['JML_MHS']);
*/

// Yudi Sulistya, 2012/07/17
$mk=getData("
select nim_mhs, nm_pengguna, round(sum(sks*bobot)/sum(sks),2) as ipk from 
( 
select m.nim_mhs, p.nm_pengguna, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join pengguna p on p.id_pengguna=m.id_pengguna
where rangking=1 and m.id_program_studi=$kdprodi and a.id_semester=$smt
)
group by nim_mhs, nm_pengguna
");
$smarty->assign('T_MK', $mk);

}

$smarty->display('rekap-khs-psikologi.tpl');

}
?>
