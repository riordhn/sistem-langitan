<?php
require('common.php');
require_once ('ociFunction.php');

$id_pengguna= $user->ID_PENGGUNA; 
$fak=getvar("select fakultas.id_fakultas from pegawai left join unit_kerja on pegawai.id_unit_kerja=unit_kerja.id_unit_kerja 
left join program_studi on unit_kerja.id_program_studi=program_studi.id_program_studi 
left join fakultas on program_studi.id_fakultas=fakultas.id_fakultas 
where pegawai.id_pengguna=$id_pengguna");
$kdfak=$fak['ID_FAKULTAS'];


$mhs=getData("SELECT ID_MHS, NIM_MHS, NM_PENGGUNA, NM_STATUS_PENGGUNA, NM_FAKULTAS, NM_PROGRAM_STUDI, NM_JENJANG
				FROM MAHASISWA
				LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
				LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
				LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
				LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
				LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
				WHERE (STATUS_AKADEMIK_MHS = 1 OR STATUS_AKADEMIK_MHS = 2 OR STATUS_AKADEMIK_MHS = 3 OR STATUS_AKADEMIK_MHS = 8
				OR STATUS_AKADEMIK_MHS = 9 OR STATUS_AKADEMIK_MHS = 10 OR STATUS_AKADEMIK_MHS = 11 OR STATUS_AKADEMIK_MHS = 14 
				OR STATUS_AKADEMIK_MHS = 15 OR STATUS_AKADEMIK_MHS = 17 OR STATUS_AKADEMIK_MHS = 18 OR STATUS_AKADEMIK_MHS = 19
				OR STATUS_AKADEMIK_MHS = 21)
				AND PROGRAM_STUDI.ID_FAKULTAS = '$kdfak'
				ORDER BY FAKULTAS.ID_FAKULTAS, NM_JENJANG, NM_PROGRAM_STUDI, NIM_MHS");

$no = 1;
foreach($mhs as $data){
	$id_semester_piutang = getData("SELECT ID_SEMESTER, TAHUN_AJARAN, NM_SEMESTER 
							FROM SEMESTER WHERE (NM_SEMESTER = 'Ganjil' or NM_SEMESTER = 'Genap') 
							AND THN_AKADEMIK_SEMESTER BETWEEN (SELECT THN_ANGKATAN_MHS FROM MAHASISWA WHERE NIM_MHS = '$data[NIM_MHS]') AND 
							(SELECT THN_AKADEMIK_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True') 
							AND ID_SEMESTER <> (SELECT ID_SEMESTER FROM SEMESTER WHERE NM_SEMESTER = 'Genap' AND THN_AKADEMIK_SEMESTER = 
							(SELECT THN_AKADEMIK_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True'))
							ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC");
	$semester = 0;
	foreach($id_semester_piutang as $sp){
		
		$piutang = getvar("SELECT ID_MHS, ID_SEMESTER, TGL_BAYAR,  SUM(BESAR_BIAYA) BESAR_BIAYA FROM PEMBAYARAN 
										WHERE ID_SEMESTER = '$sp[ID_SEMESTER]' AND ID_MHS ='$data[ID_MHS]'
										GROUP BY ID_MHS, ID_SEMESTER, TGL_BAYAR
										UNION
										SELECT ID_MHS, ID_SEMESTER, TGL_BAYAR, SUM(BESAR_BIAYA) BESAR_BIAYA 
										FROM PEMBAYARAN_CMHS 
										LEFT JOIN MAHASISWA ON MAHASISWA.ID_C_MHS = PEMBAYARAN_CMHS.ID_C_MHS
										WHERE ID_SEMESTER = '$sp[ID_SEMESTER]' AND ID_MHS = '$data[ID_MHS]'
										GROUP BY ID_MHS, ID_SEMESTER, TGL_BAYAR
										ORDER BY TGL_BAYAR DESC");
		
		if($piutang['TGL_BAYAR'] == ''){
			$semester = $semester + 1;
		}
	}
			if($semester > 0){
			$tampil = $tampil. "<tr>
						<td>$no</td>
						<td><a href='pembayaran-piutang.php?nim=$data[NIM_MHS]'>$data[NIM_MHS]</a></td>
						<td>$data[NM_PENGGUNA]</td>
						<td>$data[NM_FAKULTAS]</td>
						<td>$data[NM_JENJANG] - $data[NM_PROGRAM_STUDI]</td>
						<td>$data[NM_STATUS_PENGGUNA]</td>
						<td style='text-align:center'>$semester</td>
					</tr>";
			$no++;
			}
}

ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #1d5700;
    }
    td{
        text-align: left;
    }
</style>
<table cellpadding="3" cellspacing="3" border="1">
    <thead>
        <tr>
			<th>No</th>
			<th>NIM</th>
			<th>Nama</th>
			<th>Fakultas</th>
			<th>Program Studi</th>
			<th>Status Akademik</th>
			<th>Piutang Semester</th>
        </tr>
    </thead>
    <tbody>
        <?php
			echo $tampil;
        ?>
    </tbody>
</table>

<?php
$filename = 'Excel-piutang-mahasiswa' . date('Y-m-d');
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=$filename");
header("Pragma: ");
header("Cache-Control: ");
ob_flush();
?>
