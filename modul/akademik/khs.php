<?php
require('common.php');
require_once ('ociFunction.php');
include 'includes/function.php';

$kdprodi = $user->ID_PROGRAM_STUDI;
$id_pengguna = $user->ID_PENGGUNA;

$fak = getvar("select fakultas.id_fakultas, upper(fakultas.nm_fakultas) as nm_fakultas from pegawai left join unit_kerja on pegawai.id_unit_kerja=unit_kerja.id_unit_kerja 
left join fakultas on unit_kerja.id_fakultas=fakultas.id_fakultas 
where pegawai.id_pengguna=$id_pengguna");
$kdfak = $fak['ID_FAKULTAS'];
$nmfak = $fak['NM_FAKULTAS'];
$smarty->assign('FAK', $kdfak);
$smarty->assign('NM_FAK', $nmfak);

$thnsmt = getData(
	"SELECT id_semester, nm_semester, tipe_semester, tahun_ajaran || ' ' || nm_semester AS semester, tahun_ajaran,
		case tipe_semester when 'REG' then 1 when 'UP' then 2 when 'SP' then 3 end as urutan_tipe
	FROM semester
	WHERE id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}'
	ORDER BY thn_akademik_semester DESC, group_semester DESC, 5 DESC /* urutan_tipe */");
$smarty->assign('T_THNSMT', $thnsmt);

$prodinya = getData("select id_program_studi,b.nm_jenjang||' - '||a.nm_program_studi nm_program_studi from program_studi a left join jenjang b on (a.id_jenjang=b.id_jenjang) where id_fakultas='{$kdfak}' and status_aktif_prodi = 1 order by id_program_studi");
$smarty->assign('prodinya', $prodinya);

$depan = time();
$belakang = strrev(time());

if (isset($_POST['action']) == 'view')
{
	$nim = $_POST['nim'];
	$id_smt = $_POST['smt'];
	$tgl_cetak_krs = $_POST['tgl_cetak_krs'];
	$tgl_cetak_khs = $_POST['tgl_cetak_khs'];
	$smarty->assign('IDSMT', $id_smt);

//Yudi Sulistya 21/07/2012
//Enkrip url untuk cetak
	$link = 'proses/khs-cetak.php?' . paramEncrypt($depan . '=' . $belakang . $depan . '&nim=' . $nim . '&smt=' . $id_smt . '&tgl_cetak=' . $tgl_cetak_khs . '&' . $belakang . '=' . $depan . $belakang) . '';
	$smarty->assign('LINK', $link);

	$link_krs = 'proses/krs-cetak.php?' . paramEncrypt($depan . '=' . $belakang . $depan . '&nim=' . $nim . '&smt=' . $id_smt . '&tgl_cetak=' . $tgl_cetak_krs . '&' . $belakang . '=' . $depan . $belakang) . '';
	$smarty->assign('LINK_KRS', $link_krs);

	$datasmt = getvar("select 'KARTU HASIL STUDI TAHUN AJARAN '||tahun_ajaran||' SEMESTER '||upper(nm_semester) as nm_smt from semester where id_semester='" . $id_smt . "'");
	$namasmt = $datasmt['NM_SMT'];
	$smarty->assign('NM_SMT', $namasmt);

	$datamhs = getData("select ': '||nim_mhs||' - '||upper(nm_pengguna) as nm_mhs from mahasiswa left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna where nim_mhs='" . $nim . "' and id_program_studi in (select id_program_studi from program_studi where id_fakultas='" . $kdfak . "')");
	$smarty->assign('MHS', $datamhs);

	$biomhs = getData("select mhs.id_mhs, mhs.nim_mhs, upper(p.nm_pengguna) as nm_pengguna, j.nm_jenjang, ps.nm_program_studi, mhs.status_akademik_mhs
				from mahasiswa mhs 
				left join pengguna p on mhs.id_pengguna=p.id_pengguna 
				left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
				left join jenjang j on ps.id_jenjang=j.id_jenjang
				where mhs.nim_mhs='" . $nim . "' and mhs.id_program_studi in (select id_program_studi from program_studi where id_fakultas='" . $kdfak . "')");
	$smarty->assign('T_MHS', $biomhs);

//Yudi Sulistya 21/07/2012
//Untuk membedakan KHS SP & REG
	$tipe_smt = getvar("select upper(nm_semester) as nm_semester from semester where id_semester='" . $id_smt . "'");
	$pendek = $tipe_smt['NM_SEMESTER'];

	$smt = getvar("select fd_id_smt from semester where id_semester='" . $id_smt . "'");
	$fd_id_smt = $smt['FD_ID_SMT'];

	$mhs = getvar("select id_mhs from mahasiswa m 
					join pengguna p on p.id_pengguna = m.id_pengguna 
					where m.nim_mhs = '" . $nim . "' and p.id_perguruan_tinggi='" . $id_pt_user . "'");
	$id_mhs = $mhs['ID_MHS'];

	if ($pendek == 'PENDEK')
	{
		/*$jaf = getData(
			"select kd_mata_kuliah as kode,upper(nm_mata_kuliah) as nama,nama_kelas,tipe_semester,
			kredit_semester as sks,nilai_huruf as nilai,bobot,bobot_total,UTS,UAS,nilai_angka
			from(
			select pengambilan_mk.id_mhs,MATA_KULIAH.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,tipe_semester,
			KURIKULUM_MK.kredit_semester,case when kurikulum_mk.status_mkta='1' 
			and (nilai_huruf='E' or nilai_huruf is null) and id_fakultas=7 then 'T' 
			when nilai_huruf is null then 'E' else nilai_huruf end as nilai_huruf,
			case when standar_nilai.nilai_standar_nilai is null then 0 else standar_nilai.nilai_standar_nilai end as bobot,
			coalesce(standar_nilai.nilai_standar_nilai*kurikulum_mk.kredit_semester,0) as bobot_total,
			nilai_angka,a.besar_nilai_mk as UTS,b.besar_nilai_mk as UAS,
			row_number() over(partition by pengambilan_mk.id_mhs,kd_mata_kuliah order by nilai_huruf) rangking
			from PENGAMBILAN_MK
			left join kelas_mk on PENGAMBILAN_MK.id_kelas_mk=KELAS_MK.id_kelas_mk
			left join NAMA_KELAS on KELAS_MK.no_kelas_mk=nama_kelas.id_nama_kelas
			left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk
			left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
			left join semester on pengambilan_mk.id_semester=semester.id_semester
			left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
			left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
			left join standar_nilai on pengambilan_mk.nilai_huruf=standar_nilai.nm_standar_nilai
			left join nilai_mk a on pengambilan_mk.id_pengambilan_mk=a.id_pengambilan_mk and a.id_komponen_mk in (select id_komponen_mk from komponen_mk where nm_komponen_mk='UTS')
			left join nilai_mk b on pengambilan_mk.id_pengambilan_mk=b.id_pengambilan_mk and b.id_komponen_mk in (select id_komponen_mk from komponen_mk where nm_komponen_mk='UAS')
			where id_semester='" . $id_smt . "' and status_apv_pengambilan_mk=1 and PENGAMBILAN_MK.status_hapus=0 and PENGAMBILAN_MK.status_pengambilan_mk !=0
			and mahasiswa.nim_mhs='" . $nim . "') 
			where rangking=1");
		$smarty->assign('T_MK', $jaf);

		$ipk = getData(
			"select sum(kredit_semester) as sks_total, sum((bobot*kredit_semester)) as bobot_total, 
			case when sum(kredit_semester)=0 then 0 else round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ipk
			from 
			(
			select kredit_semester,bobot
			from(
			select pengambilan_mk.id_mhs,MATA_KULIAH.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,tipe_semester,
			KURIKULUM_MK.kredit_semester,case when kurikulum_mk.status_mkta='1' 
			and (nilai_huruf='E' or nilai_huruf is null) and id_fakultas=7 then 'T' 
			when nilai_huruf is null then 'E' else nilai_huruf end as nilai_huruf,
			case when standar_nilai.nilai_standar_nilai is null then 0 else standar_nilai.nilai_standar_nilai end as bobot,
			coalesce(standar_nilai.nilai_standar_nilai*kurikulum_mk.kredit_semester,0) as bobot_total,
			nilai_angka,a.besar_nilai_mk as UTS,b.besar_nilai_mk as UAS,
			row_number() over(partition by pengambilan_mk.id_mhs,kd_mata_kuliah order by nilai_huruf) rangking
			from PENGAMBILAN_MK
			left join kelas_mk on PENGAMBILAN_MK.id_kelas_mk=KELAS_MK.id_kelas_mk
			left join NAMA_KELAS on KELAS_MK.no_kelas_mk=nama_kelas.id_nama_kelas
			left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk
			left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
			left join semester on pengambilan_mk.id_semester=semester.id_semester
			left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
			left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
			left join standar_nilai on pengambilan_mk.nilai_huruf=standar_nilai.nm_standar_nilai
			left join nilai_mk a on pengambilan_mk.id_pengambilan_mk=a.id_pengambilan_mk and a.id_komponen_mk in (select id_komponen_mk from komponen_mk where nm_komponen_mk='UTS')
			left join nilai_mk b on pengambilan_mk.id_pengambilan_mk=b.id_pengambilan_mk and b.id_komponen_mk in (select id_komponen_mk from komponen_mk where nm_komponen_mk='UAS')
			where id_semester='" . $id_smt . "' and status_apv_pengambilan_mk=1 and PENGAMBILAN_MK.status_hapus=0 and PENGAMBILAN_MK.status_pengambilan_mk !=0
			and mahasiswa.nim_mhs='" . $nim . "') 
			where rangking=1
			)");
		$smarty->assign('T_IPK', $ipk);*/

		$jaf = getData(
			"SELECT 
					kd_mata_kuliah AS kode,
					upper(nm_mata_kuliah) AS nama,
					nama_kelas,
					tipe_semester,
					kredit_semester AS sks,
					CASE WHEN flagnilai = 1 THEN nilai_huruf ELSE '-' END AS nilai,
					nilai_angka AS akhir,
					bobot,
					bobot_total,
					id_pengambilan_mk
				FROM (
					SELECT 
						pengambilan_mk.id_mhs,
						/* Kode MK */
				        COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
				        /* Nama MK */
				        COALESCE(mk1.nm_mata_kuliah, mk2.nm_mata_kuliah) AS nm_mata_kuliah,
				        /* SKS */
				        COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS kredit_semester,
						nama_kelas,
						tipe_semester,

						/* Nilai Huruf, cek status Tugas Akhir */
						CASE 
						  WHEN kurikulum_mk.status_mkta = '1' AND (nilai_huruf = 'E' OR nilai_huruf IS NULL) /* AND id_fakultas = 7 */ THEN 'T'
						  WHEN nilai_huruf IS NULL THEN 'E' 
						  ELSE nilai_huruf
						END AS nilai_huruf,

						pengambilan_mk.nilai_angka,

						/* Bobot = Standar nilai */
						CASE
						  WHEN standar_nilai.nilai_standar_nilai IS NULL THEN 0
						  ELSE standar_nilai.nilai_standar_nilai
						END AS bobot,

						/* Bobot total = Standar Nilai x SKS */
						COALESCE(standar_nilai.nilai_standar_nilai * kurikulum_mk.kredit_semester, 0) AS bobot_total,

						/* Ranking group by id_mhs-kode mata kuliah */
						row_number() OVER(PARTITION BY pengambilan_mk.id_mhs, kd_mata_kuliah ORDER BY nilai_huruf) AS rangking,

						pengambilan_mk.id_pengambilan_mk,
						pengambilan_mk.flagnilai
					FROM pengambilan_mk
					JOIN mahasiswa			ON pengambilan_mk.id_mhs = mahasiswa.id_mhs
					JOIN program_studi		ON mahasiswa.id_program_studi = program_studi.id_program_studi
					JOIN semester			ON pengambilan_mk.id_semester = semester.id_semester
					-- Via Kelas
					LEFT JOIN kelas_mk		ON pengambilan_mk.id_kelas_mk = kelas_mk.id_kelas_mk
		    	LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kelas_mk.id_mata_kuliah
					LEFT JOIN nama_kelas	ON kelas_mk.no_kelas_mk = nama_kelas.id_nama_kelas
					-- Via Kurikulum
					LEFT JOIN kurikulum_mk	ON pengambilan_mk.id_kurikulum_mk = kurikulum_mk.id_kurikulum_mk
					LEFT JOIN mata_kuliah mk2 ON kurikulum_mk.id_mata_kuliah = mk2.id_mata_kuliah
					-- Penyetaraan
				    LEFT JOIN mk_setara mks ON mks.id_mata_kuliah = COALESCE(mk1.id_mata_kuliah, mk2.id_mata_kuliah) AND mks.is_deleted = 0
				    LEFT JOIN mk_penyetaraan mkp ON mkp.id_mk_penyetaraan = mks.id_mk_penyetaraan and mkp.is_deleted = 0

					LEFT JOIN standar_nilai	ON pengambilan_mk.nilai_huruf = standar_nilai.nm_standar_nilai
					WHERE 
						group_semester || thn_akademik_semester IN (SELECT group_semester || thn_akademik_semester FROM semester WHERE id_semester = '" . $id_smt . "') AND 
						tipe_semester = 'SP' AND
						status_apv_pengambilan_mk = 1 AND
						pengambilan_mk.status_hapus = 0 AND
						pengambilan_mk.status_pengambilan_mk != 0 AND
						mahasiswa.id_mhs = '" . $id_mhs . "')
				WHERE rangking = 1");

		$smarty->assign('T_MK', $jaf);

		$ips = getData(
			"SELECT SUM(sks) AS sks_total, SUM((bobot*sks)) as bobot_total, round(SUM(nilai_mutu) / sum(sks), 2) as ips from (
					    SELECT
					        mhs.id_mhs,
					        /* Kode MK utk grouping total mutu */
					        COALESCE(mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
					        /* SKS */
					        COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS sks,
					        /* Bobot */
					        sn.nilai_standar_nilai AS bobot,
					        /* Nilai Mutu = SKS * Bobot */
					        COALESCE(mk1.kredit_semester, mk2.kredit_semester) * sn.nilai_standar_nilai AS nilai_mutu
					    FROM pengambilan_mk pmk
					    JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
					    JOIN semester S ON S.id_semester = pmk.id_semester
					    -- Via Kelas
					    LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
					    LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
					    -- Via Kurikulum
					    LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
					    LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
					    -- nilai bobot
					    JOIN standar_nilai sn ON sn.nm_standar_nilai = pmk.nilai_huruf
					    WHERE 
					        pmk.status_apv_pengambilan_mk = 1 AND 
					        mhs.id_mhs = '".$id_mhs."' AND 
					        S.id_semester = '".$id_smt."'
					)
					group by id_mhs");

		$smarty->assign('T_IPS', $ips);

		$ipk = getData(
			"SELECT id_mhs,
                        SUM(sks) AS total_sks, 
                        round(SUM(nilai_mutu) / SUM(sks), 2) AS ipk 
                    FROM (
                        SELECT
                            mhs.id_mhs,
                            /* Kode MK utk grouping total mutu */
                            COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
                            /* SKS */
                            COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS sks,
                            /* Nilai Mutu = SKS * Bobot */
                            COALESCE(mk1.kredit_semester, mk2.kredit_semester) * sn.nilai_standar_nilai AS nilai_mutu,
                            /* Urutan Nilai Terbaik */
                            row_number() OVER (PARTITION BY mhs.id_mhs, COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) ORDER BY sn.nilai_standar_nilai DESC) urut
                        FROM pengambilan_mk pmk
                        JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
                        JOIN semester S ON S.id_semester = pmk.id_semester
                        -- Via Kelas
                        LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
                        LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
                        -- Via Kurikulum
                        LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
                        LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
                        -- Penyetaraan
                        LEFT JOIN mk_setara mks ON mks.id_mata_kuliah = COALESCE(mk1.id_mata_kuliah, mk2.id_mata_kuliah) AND mks.is_deleted = 0
                        LEFT JOIN mk_penyetaraan mkp ON mkp.id_mk_penyetaraan = mks.id_mk_penyetaraan and mkp.is_deleted = 0
                        -- nilai bobot
                        JOIN standar_nilai sn ON sn.nm_standar_nilai = pmk.nilai_huruf
                        WHERE 
                            pmk.status_apv_pengambilan_mk = 1 AND 
                            mhs.id_mhs = '".$id_mhs."' AND 
                            S.fd_id_smt <= '".$fd_id_smt."'
                    )
                    GROUP BY id_mhs");

		$smarty->assign('T_IPK', $ipk);
	}
	else
	{
		/*$jaf = getData(
			"SELECT
				pmk.id_pengambilan_mk,
				mk.kd_mata_kuliah as kode, mk.nm_mata_kuliah as nama, COALESCE(kmk.kredit_semester, mk.kredit_semester) AS sks,
				'' AS uts, '' AS uas, pmk.nilai_angka AS akhir,
				NVL(pmk.nilai_huruf, 'E') AS nilai, 
				sn.nilai_standar_nilai * COALESCE(kmk.kredit_semester, mk.kredit_semester) AS bobot  Bobot = Nilai Standar x SKS ,
				kur.nm_kurikulum as kurikulum
			FROM pengambilan_mk pmk
			JOIN semester s ON s.id_semester = pmk.id_semester
			JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
			JOIN kurikulum_prodi kp ON kp.id_kurikulum_prodi = kmk.id_kurikulum_prodi
			JOIN kurikulum kur ON kur.id_kurikulum = kp.id_kurikulum
			JOIN mata_kuliah mk ON mk.id_mata_kuliah = kmk.id_mata_kuliah
			LEFT JOIN standar_nilai sn ON sn.nm_standar_nilai = nvl(pmk.nilai_huruf, 'E')
			WHERE 
				pmk.status_apv_pengambilan_mk = 1 AND
				pmk.status_transfer = 0 AND 
				pmk.id_mhs = {$biomhs[0]['ID_MHS']} AND 
				pmk.id_semester = {$id_smt}
			ORDER BY kd_mata_kuliah");
		$smarty->assign('T_MK', $jaf);

		$ipk = getData(
			"select sum(kredit_semester) as sks_total, sum((bobot*kredit_semester)) as bobot_total, 
			case when sum(kredit_semester)=0 then 0 else round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ipk
			from 
			(
			select kredit_semester,bobot
			from(
			select pengambilan_mk.id_mhs,MATA_KULIAH.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,tipe_semester,
			KURIKULUM_MK.kredit_semester,case when kurikulum_mk.status_mkta='1' 
			and (nilai_huruf='E' or nilai_huruf is null) and id_fakultas=7 then 'T' 
			when nilai_huruf is null then 'E' else nilai_huruf end as nilai_huruf,
			case when standar_nilai.nilai_standar_nilai is null then 0 else standar_nilai.nilai_standar_nilai end as bobot,
			coalesce(standar_nilai.nilai_standar_nilai*kurikulum_mk.kredit_semester,0) as bobot_total,
			nilai_angka,a.besar_nilai_mk as UTS,b.besar_nilai_mk as UAS,
			row_number() over(partition by pengambilan_mk.id_mhs,kd_mata_kuliah order by nilai_huruf) rangking
			from PENGAMBILAN_MK
			left join kelas_mk on PENGAMBILAN_MK.id_kelas_mk=KELAS_MK.id_kelas_mk
			left join NAMA_KELAS on KELAS_MK.no_kelas_mk=nama_kelas.id_nama_kelas
			left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk
			left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
			left join semester on pengambilan_mk.id_semester=semester.id_semester
			left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
			left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
			left join standar_nilai on pengambilan_mk.nilai_huruf=standar_nilai.nm_standar_nilai
			left join nilai_mk a on pengambilan_mk.id_pengambilan_mk=a.id_pengambilan_mk and a.id_komponen_mk in (select id_komponen_mk from komponen_mk where nm_komponen_mk='UTS')
			left join nilai_mk b on pengambilan_mk.id_pengambilan_mk=b.id_pengambilan_mk and b.id_komponen_mk in (select id_komponen_mk from komponen_mk where nm_komponen_mk='UAS')
			where  group_semester||thn_akademik_semester in
			(select group_semester||thn_akademik_semester from semester where id_semester='" . $id_smt . "')
			and tipe_semester in ('UP','REG','RD')
			and status_apv_pengambilan_mk=1 and PENGAMBILAN_MK.status_hapus=0 and PENGAMBILAN_MK.status_pengambilan_mk !=0
			and mahasiswa.nim_mhs='" . $nim . "') 
			where rangking=1
			)");
		$smarty->assign('T_IPK', $ipk);*/

		$jaf = getData(
			"SELECT 
					kd_mata_kuliah AS kode,
					upper(nm_mata_kuliah) AS nama,
					nama_kelas,
					tipe_semester,
					kredit_semester AS sks,
					/* Status Belum Tampil */
					CASE WHEN flagnilai = 1 THEN nilai_huruf ELSE '*BT' END AS nilai,
					nilai_angka AS akhir,
					bobot,
					bobot_total,
					id_pengambilan_mk
				FROM (
					SELECT 
						pengambilan_mk.id_mhs,
						/* Kode MK */
				        COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
				        /* Nama MK */
				        COALESCE(mk1.nm_mata_kuliah, mk2.nm_mata_kuliah) AS nm_mata_kuliah,
				        /* SKS */
				        COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS kredit_semester,
						nama_kelas,
						tipe_semester,

						CASE 
						  WHEN pengambilan_mk.nilai_huruf IS NULL THEN '*K' 
						  ELSE pengambilan_mk.nilai_huruf
						END AS nilai_huruf,

						pengambilan_mk.nilai_angka,

						/* Bobot */
						CASE
						  WHEN standar_nilai.nilai_standar_nilai IS NULL THEN 0
						  ELSE standar_nilai.nilai_standar_nilai
						END AS bobot,

						/* Bobot Total = Standar Nilai x SKS */
						COALESCE(standar_nilai.nilai_standar_nilai * kurikulum_mk.kredit_semester, 0) AS bobot_total,

						/* Ranking berdasar kode */
						row_number() OVER(PARTITION BY pengambilan_mk.id_mhs, COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) ORDER BY nilai_huruf) AS rangking,

						pengambilan_mk.id_pengambilan_mk,
						pengambilan_mk.flagnilai
					FROM pengambilan_mk
					JOIN mahasiswa			ON pengambilan_mk.id_mhs = mahasiswa.id_mhs
					JOIN semester			ON pengambilan_mk.id_semester = semester.id_semester
					JOIN program_studi		ON mahasiswa.id_program_studi = program_studi.id_program_studi
					-- Via Kelas
					LEFT JOIN kelas_mk		ON pengambilan_mk.id_kelas_mk = kelas_mk.id_kelas_mk
		    		LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kelas_mk.id_mata_kuliah
					LEFT JOIN nama_kelas	ON kelas_mk.no_kelas_mk = nama_kelas.id_nama_kelas
					-- Via Kurikulum
					LEFT JOIN kurikulum_mk	ON pengambilan_mk.id_kurikulum_mk = kurikulum_mk.id_kurikulum_mk
					LEFT JOIN mata_kuliah mk2 ON kurikulum_mk.id_mata_kuliah = mk2.id_mata_kuliah
					-- Penyetaraan
					LEFT JOIN mk_setara mks ON mks.id_mata_kuliah = COALESCE(mk1.id_mata_kuliah, mk2.id_mata_kuliah) AND mks.is_deleted = 0
					LEFT JOIN mk_penyetaraan mkp ON mkp.id_mk_penyetaraan = mks.id_mk_penyetaraan and mkp.is_deleted = 0
					-- Bobot
					LEFT JOIN standar_nilai	ON pengambilan_mk.nilai_huruf = standar_nilai.nm_standar_nilai
					WHERE
						group_semester||thn_akademik_semester IN (SELECT group_semester||thn_akademik_semester FROM semester WHERE id_semester = '" . $id_smt . "') AND
						tipe_semester IN ('UP','REG') AND
						status_apv_pengambilan_mk = 1 AND 
						pengambilan_mk.status_hapus = 0 AND 
						pengambilan_mk.status_pengambilan_mk != 0 AND
						mahasiswa.id_mhs = '" . $id_mhs . "'
				)
				WHERE rangking = 1");

		$smarty->assign('T_MK', $jaf);

		$ips = getData(
			"SELECT SUM(sks) AS sks_total, SUM((bobot*sks)) as bobot_total, round(SUM(nilai_mutu) / sum(sks), 2) as ips from (
					    SELECT
					        mhs.id_mhs,
					        /* Kode MK utk grouping total mutu */
					        COALESCE(mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
					        /* SKS */
					        COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS sks,
					        /* Bobot */
					        sn.nilai_standar_nilai AS bobot,
					        /* Nilai Mutu = SKS * Bobot */
					        COALESCE(mk1.kredit_semester, mk2.kredit_semester) * sn.nilai_standar_nilai AS nilai_mutu
					    FROM pengambilan_mk pmk
					    JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
					    JOIN semester S ON S.id_semester = pmk.id_semester
					    -- Via Kelas
					    LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
					    LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
					    -- Via Kurikulum
					    LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
					    LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
					    -- nilai bobot
					    JOIN standar_nilai sn ON sn.nm_standar_nilai = pmk.nilai_huruf
					    WHERE 
					        pmk.status_apv_pengambilan_mk = 1 AND 
					        mhs.id_mhs = '".$id_mhs."' AND 
					        S.id_semester = '".$id_smt."'
					)
					group by id_mhs");

		$smarty->assign('T_IPS', $ips);

		$ipk = getData(
			"SELECT id_mhs,
                        SUM(sks) AS total_sks, 
                        round(SUM(nilai_mutu) / SUM(sks), 2) AS ipk 
                    FROM (
                        SELECT
                            mhs.id_mhs,
                            /* Kode MK utk grouping total mutu */
                            COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
                            /* SKS */
                            COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS sks,
                            /* Nilai Mutu = SKS * Bobot */
                            COALESCE(mk1.kredit_semester, mk2.kredit_semester) * sn.nilai_standar_nilai AS nilai_mutu,
                            /* Urutan Nilai Terbaik */
                            row_number() OVER (PARTITION BY mhs.id_mhs, COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) ORDER BY sn.nilai_standar_nilai DESC) urut
                        FROM pengambilan_mk pmk
                        JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
                        JOIN semester S ON S.id_semester = pmk.id_semester
                        -- Via Kelas
                        LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
                        LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
                        -- Via Kurikulum
                        LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
                        LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
                        -- Penyetaraan
                        LEFT JOIN mk_setara mks ON mks.id_mata_kuliah = COALESCE(mk1.id_mata_kuliah, mk2.id_mata_kuliah) AND mks.is_deleted = 0
                        LEFT JOIN mk_penyetaraan mkp ON mkp.id_mk_penyetaraan = mks.id_mk_penyetaraan and mkp.is_deleted = 0
                        -- nilai bobot
                        JOIN standar_nilai sn ON sn.nm_standar_nilai = pmk.nilai_huruf
                        WHERE 
                            pmk.status_apv_pengambilan_mk = 1 AND 
                            mhs.id_mhs = '".$id_mhs."' AND 
                            S.fd_id_smt <= '".$fd_id_smt."'
                    )
                    GROUP BY id_mhs");

		$smarty->assign('T_IPK', $ipk);
	}
}

$smarty->display('khs.tpl');
?>