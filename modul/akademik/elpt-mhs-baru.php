<?php
include('config.php');
include('../ppmb/class/Penerimaan.class.php');
$kdfak=$user->ID_FAKULTAS; 

$penerimaan = new Penerimaan($db);
$smarty->assign('penerimaan_set', $penerimaan->GetAllForView());


if(isset($_GET['id_penerimaan'])){

	
$view_kesehatan = $db->QueryToArray("SELECT NO_UJIAN, NM_C_MHS, KESEHATAN_KESIMPULAN_AKHIR, NM_PROGRAM_STUDI, 
                                    TO_CHAR(JADWAL_KESEHATAN.TGL_TEST, 'DD-MM-YYYY') AS TGL_TES_KESEHATAN, TGL_VERIFIKASI_KESEHATAN,
                                    TO_CHAR(JADWAL_TOEFL.TGL_TEST, 'DD-MM-YYYY') AS TGL_TES_TOEFL, TGL_VERIFIKASI_ELPT, 
                                    ELPT_SCORE, ELPT_LISTENING, ELPT_STRUCTURE, ELPT_READING, MAHASISWA.NIM_MHS
                                    FROM CALON_MAHASISWA_BARU
                                    LEFT JOIN CALON_MAHASISWA_DATA ON CALON_MAHASISWA_DATA.ID_C_MHS = CALON_MAHASISWA_BARU.ID_C_MHS
                                    LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = CALON_MAHASISWA_BARU.ID_PROGRAM_STUDI
                                    LEFT JOIN JADWAL_KESEHATAN ON JADWAL_KESEHATAN.ID_JADWAL_KESEHATAN = CALON_MAHASISWA_DATA.ID_JADWAL_KESEHATAN
                                    LEFT JOIN JADWAL_TOEFL ON JADWAL_TOEFL.ID_JADWAL_TOEFL = CALON_MAHASISWA_DATA.ID_JADWAL_TOEFL
									JOIN MAHASISWA ON MAHASISWA.ID_C_MHS = CALON_MAHASISWA_BARU.ID_C_MHS
                                    WHERE CALON_MAHASISWA_BARU.ID_PENERIMAAN = '$_GET[id_penerimaan]' AND TGL_DITERIMA IS NOT NULL
									AND PROGRAM_STUDI.ID_FAKULTAS = '$kdfak'
                                    ORDER BY ID_FAKULTAS, PROGRAM_STUDI.ID_PROGRAM_STUDI, MAHASISWA.NIM_MHS");
$smarty->assign('view_kesehatan', $view_kesehatan);
}



$smarty->display("elpt-mhs-baru.tpl");
?>