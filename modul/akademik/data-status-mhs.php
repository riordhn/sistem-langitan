<?php
include 'config.php';
$db2 = new MyOracle();


$id_pengguna= $user->ID_PENGGUNA; 
$id_fakultas = $user->ID_FAKULTAS;
$query= "";
if($id_fakultas != '0'){
	$query = " AND FAKULTAS.ID_FAKULTAS = '$id_fakultas'";	
}

if(isset($_GET['nim'])){
	
$db->Query("SELECT COUNT(*) AS CEK
				FROM MAHASISWA
				LEFT JOIN PENGGUNA ON MAHASISWA.ID_PENGGUNA = PENGGUNA.ID_PENGGUNA AND PENGGUNA.ID_PERGURUAN_TINGGI = {$id_pt_user}
				LEFT JOIN PROGRAM_STUDI ON MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
				LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS 
				LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
				LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
				WHERE (NIM_MHS = '$_GET[nim]' OR UPPER(PENGGUNA.NM_PENGGUNA) LIKE UPPER('{$_GET['nim']}')) $query");
$cek_mhs = $db->FetchAssoc();

if($cek_mhs['CEK'] > 0){

	$mhs = $db->QueryToArray("SELECT MAHASISWA.ID_MHS, NIM_MHS, a.NM_PENGGUNA, NM_PROGRAM_STUDI, NM_FAKULTAS, NM_STATUS_PENGGUNA, NM_JENJANG
				, STATUS_CEKAL, FAKULTAS.ID_FAKULTAS,
				b.GELAR_DEPAN||' '||b.NM_PENGGUNA||', '||b.GELAR_BELAKANG as DOSEN_WALI
				FROM MAHASISWA
				LEFT JOIN PENGGUNA a ON MAHASISWA.ID_PENGGUNA = a.ID_PENGGUNA AND a.ID_PERGURUAN_TINGGI = {$id_pt_user}
				LEFT JOIN PROGRAM_STUDI ON MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
				LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
				LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
				LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
				LEFT JOIN DOSEN_WALI ON MAHASISWA.ID_MHS=DOSEN_WALI.ID_MHS and DOSEN_WALI.STATUS_DOSEN_WALI=1
				LEFT JOIN DOSEN ON DOSEN_WALI.ID_DOSEN=DOSEN.ID_DOSEN
				LEFT JOIN PENGGUNA b ON DOSEN.ID_PENGGUNA=b.ID_PENGGUNA
				WHERE (NIM_MHS = '$_GET[nim]' OR UPPER(a.NM_PENGGUNA) LIKE UPPER('{$_GET['nim']}'))");
	
	$smarty->assign('mhs', $mhs);
	$id_mhs = $mhs[0]['ID_MHS'];
	$fakultas = $mhs[0]['ID_FAKULTAS'];
	
		//admisi
	$admisi = $db->QueryToArray("SELECT MAHASISWA.ID_MHS, NIM_MHS, a.NM_PENGGUNA, NM_PROGRAM_STUDI, NM_FAKULTAS, NM_STATUS_PENGGUNA, NM_JENJANG
				,ADMISI.NO_SK, ADMISI.TGL_SK, ADMISI.ALASAN, NM_SEMESTER, THN_AKADEMIK_SEMESTER, KURUN_WAKTU, NO_IJASAH, TGL_LULUS, STATUS_CEKAL
				FROM MAHASISWA
				LEFT JOIN PENGGUNA a ON MAHASISWA.ID_PENGGUNA = a.ID_PENGGUNA AND a.ID_PERGURUAN_TINGGI = {$id_pt_user}
				LEFT JOIN ADMISI ON ADMISI.ID_MHS = MAHASISWA.ID_MHS
				LEFT JOIN PROGRAM_STUDI ON MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
				LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
				LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = ADMISI.STATUS_AKD_MHS
				LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
				LEFT JOIN SEMESTER ON SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
				WHERE NIM_MHS = '$_GET[nim]'
				ORDER BY ADMISI.ID_ADMISI DESC");
		
	
		$smarty->assign('admisi', $admisi);
		
		//Pembayaran
		$db->Query("SELECT COUNT(ID_MHS) AS MHS FROM PEMBAYARAN WHERE ID_MHS IN (SELECT m.ID_MHS 
																					FROM MAHASISWA m 
																					JOIN PENGGUNA p ON p.ID_PENGGUNA = m.ID_PENGGUNA and p.ID_PERGURUAN_TINGGI = {$id_pt_user}
																					WHERE m.NIM_MHS = '$_GET[nim]')");
		$row = $db->FetchAssoc();
		
		if ($row['MHS'] >= 1){
			$pembayaran_mhs = $db->QueryToArray("SELECT TAHUN_AJARAN, NM_SEMESTER, TGL_BAYAR, STATUS_PEMBAYARAN.NAMA_STATUS, PEMBAYARAN.KETERANGAN, 
									NM_BANK, NAMA_BANK_VIA, IS_TAGIH, STATUS_PEMBAYARAN.ID_STATUS_PEMBAYARAN,
									SUM(PEMBAYARAN.BESAR_BIAYA) BESAR_BIAYA, SUM(DENDA_BIAYA) AS DENDA_BIAYA
									FROM PEMBAYARAN
									LEFT JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBAYARAN.ID_MHS
									LEFT JOIN STATUS_PEMBAYARAN ON STATUS_PEMBAYARAN.ID_STATUS_PEMBAYARAN = PEMBAYARAN.ID_STATUS_PEMBAYARAN
									LEFT JOIN SEMESTER ON SEMESTER.ID_SEMESTER = PEMBAYARAN.ID_SEMESTER AND SEMESTER.ID_PERGURUAN_TINGGI = {$id_pt_user}
									LEFT JOIN BANK ON BANK.ID_BANK = PEMBAYARAN.ID_BANK
									LEFT JOIN BANK_VIA ON BANK_VIA.ID_BANK_VIA = PEMBAYARAN.ID_BANK_VIA
									WHERE NIM_MHS = '$_GET[nim]' 
									GROUP BY TAHUN_AJARAN, NM_SEMESTER, TGL_BAYAR, STATUS_PEMBAYARAN.NAMA_STATUS, PEMBAYARAN.KETERANGAN, 
									NM_BANK, NAMA_BANK_VIA, IS_TAGIH, STATUS_PEMBAYARAN.ID_STATUS_PEMBAYARAN
									ORDER BY TAHUN_AJARAN DESC, NM_SEMESTER DESC");
			

			$smarty->assign('pembayaran_mhs', $pembayaran_mhs);
		}
		
/*		$kueri = $db->QueryToArray("
			select b.tahun_ajaran, b.nm_semester, round(sum(e.kredit_semester*nilai_standar_nilai)/sum(e.kredit_semester), 2) as ips, sum(e.kredit_semester) as sks
		from mata_kuliah a, semester b,pengambilan_mk d, kurikulum_mk e, mahasiswa f, standar_nilai g
		where  a.id_mata_kuliah=e.id_mata_kuliah and d.id_semester=b.id_semester and d.id_semester=b.id_semester and e.id_kurikulum_mk=d.id_kurikulum_mk 
		and d.STATUS_APV_PENGAMBILAN_MK='1' and (d.STATUS_PENGAMBILAN_MK!='4' or d.STATUS_PENGAMBILAN_MK is null)
		and d.flagnilai='1' and f.id_mhs = d.id_mhs and nilai_huruf = nm_standar_nilai 
		and nim_mhs = '$_GET[nim]' 
		GROUP BY b.tahun_ajaran, b.nm_semester
		order by b.tahun_ajaran asc, b.nm_semester asc");*/
		
		
		$kueri = $db->QueryToArray("select id_mhs,nim_mhs,nm_pengguna,tahun_ajaran, group_semester, thn_akademik_semester,case when sum(bobot*kredit_semester)=0 then 0 else 
round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips,
case when id_fakultas=7 then sum(sksreal) else sum(kredit_semester) end as sks_sem, 
case when group_semester = 'Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun
from 
(select id_mhs,nim_mhs,nm_pengguna,id_kurikulum_mk,id_fakultas,kredit_semester, sksreal,min(nilai_huruf) as nilai, max(bobot) as bobot, 
tahun_ajaran, group_semester, thn_akademik_semester
from
(select a.id_mhs,m.nim_mhs,pg.nm_pengguna,ps.id_fakultas,a.id_kurikulum_mk, tahun_ajaran,group_semester,thn_akademik_semester,
case when (a.nilai_huruf = 'E' or a.nilai_huruf is null) 
and d.status_mkta in (1,2) then 0
else d.kredit_semester end as kredit_semester,d.kredit_semester as sksreal,
case when a.nilai_huruf is null then 'E' else a.nilai_huruf end as nilai_huruf,
case when e.nilai_standar_nilai is null then 0 else e.nilai_standar_nilai end as bobot
from pengambilan_mk a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join pengguna pg on m.id_pengguna=pg.id_pengguna 
left join program_studi ps on m.id_program_studi=ps.id_program_studi
left join semester s on a.id_semester=s.id_semester
where tipe_semester in ('UP','REG','RD') 
and a.status_apv_pengambilan_mk='1' and m.nim_mhs='$_GET[nim]' and a.status_hapus=0 
and a.status_pengambilan_mk !=0)
group by id_mhs, nim_mhs,nm_pengguna,id_kurikulum_mk, id_fakultas, kredit_semester, sksreal, tahun_ajaran, group_semester,thn_akademik_semester
)
group by id_mhs,nim_mhs,nm_pengguna,id_fakultas,tahun_ajaran, group_semester,thn_akademik_semester
order by tahun_ajaran desc, group_semester desc");
		
		$nomer=1;
		foreach($kueri as $data)
		{
			$db->Query("select id_mhs, sum(kredit_semester) as SKS_TOTAL_MHS, 
						round((sum((kredit_semester * nilai_standar_nilai)) / sum(kredit_semester)), 2) as IPK_MHS
						from 
						(select a.id_mhs, c.kd_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai,
						case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,
						row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by nilai_huruf) rangking
						from pengambilan_mk a
						join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
						join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
						join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
						join semester smt on a.id_semester=smt.id_semester and smt.id_perguruan_tinggi = {$id_pt_user}
						join mahasiswa m on a.id_mhs=m.id_mhs
						where a.status_apv_pengambilan_mk = 1 and (a.nilai_huruf<>'E' or a.nilai_huruf<>null) 
						and m.nim_mhs='$_GET[nim]' and a.status_hapus=0 
						and a.status_pengambilan_mk !=0 
						and (case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end)<='$data[TAHUN]'
						)
						where rangking=1
						group by id_mhs");
						
			$ipk_sem = $db->FetchAssoc();			
			$ips .= "<tr>
                <td style='text-align:center'>".$nomer++."</td>
                <td>".$data['TAHUN_AJARAN']." (".$data['GROUP_SEMESTER'].")</td>
                <td style='text-align:center'>".$data['IPS']."</td>				
                <td style='text-align:center'>".$data['SKS_SEM']."</td>
				<td style='text-align:center'>".$ipk_sem['IPK_MHS']."</td>
				<td style='text-align:center'>".$ipk_sem['SKS_TOTAL_MHS']."</td>
           		</tr>";
		}
		$smarty->assign('ips', $ips);
		
		
/*	$tes = $db->QueryToArray("
	select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks from (
	select e.tahun_ajaran, e.nm_semester, c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, nilai_standar_nilai,
	row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,count(*) over(partition by c.nm_mata_kuliah) terulang
	from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai g
	where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester and a.id_mhs='".$id_mhs."' and a.STATUS_APV_PENGAMBILAN_MK='1' and nilai_huruf = nm_standar_nilai
	) 
	where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
	group by tahun_ajaran, nm_semester
	order by tahun_ajaran asc, nm_semester asc
	");

if($fakultas==10 or $fakultas==11){
	$tes = $db->QueryToArray("
	select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks from (
	select e.tahun_ajaran, e.nm_semester, c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, nilai_standar_nilai,
	row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by e.thn_akademik_semester desc,e.nm_semester desc) rangking,count(*) over(partition by c.nm_mata_kuliah) terulang
	from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai g
	where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester and a.id_mhs='".$id_mhs."' and a.STATUS_APV_PENGAMBILAN_MK='1' and nilai_huruf = nm_standar_nilai
	)
	where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
	group by tahun_ajaran, nm_semester
	order by tahun_ajaran asc, nm_semester asc
	");
}
		
		$smarty->assign('ipk', $tes);
*/
	$isi_transkrip = '
		<table cellspacing="0" cellpadding="0" border="0" width="95%"  id="myTable" class="tablesorter" >
		<thead>
		<tr class="left_menu">
			<th>Semester</th>
			<th>Kode MA</th>
			<th>Nama Mata Ajar</th>
			<th>SKS</th>
			<th>Nilai</th>
			<th>Bobot</th>
		</tr>
		</thead>
	';
	$jum_sks=0; $jum_bobot=0; $ipk=0;
	// data lama
	$kueri = "
	select e.tahun_ajaran, e.nm_semester, c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,count(*) over(partition by c.nm_mata_kuliah) terulang
	from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e
	where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester and a.id_mhs='".$id_mhs."' and a.STATUS_APV_PENGAMBILAN_MK='1'
	order by e.tahun_ajaran desc, e.nm_semester desc, c.kd_mata_kuliah
	";

/*if($fakultas==10 or $fakultas==11){
	$kueri = "
	select e.tahun_ajaran, e.nm_semester, c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by e.thn_akademik_semester desc,e.nm_semester desc) rangking,count(*) over(partition by c.nm_mata_kuliah) terulang
	from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e
	where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester and a.id_mhs='".$id_mhs."' and a.STATUS_APV_PENGAMBILAN_MK='1'
	order by e.tahun_ajaran desc, e.nm_semester desc, c.kd_mata_kuliah
	";
}*/


	$result = $db->Query($kueri)or die("salah kueri1 : ");
	$isi_transkrip .= '<tbody>';
	while($r = $db->FetchRow()) {
		if($r[6]=='1') { // filter FLAGNILAI
			$semester = $r[0].' '.$r[1];
		
			// ambil bobot nilai huruf
			$bobot=0;
			$kueri2 = "select NILAI_STANDAR_NILAI from standar_nilai where NM_STANDAR_NILAI='".trim($r[5])."'";
			$result2 = $db2->Query($kueri2)or die("salah kueri : ");
			while($r2 = $db2->FetchRow()) {
				$bobot = $r2[0];
			}
			if($r[7]==1 and $r[8]>1)$warna='#00FF00';
			else {
				if($r[7]>1)$warna='#FF0000';
				else {
					if($r[5]=='-')$warna='yellow';
					$warna='#FFFFFF';
				}
			}
			
			$isi_transkrip .= '
				<tr bgcolor="'.$warna.'">
					<td>'.$semester.'</td>
					<td>'.$r[2].'</td>
					<td>'.$r[3].'</td>
					<td align="center">'.$r[4].'</td>
					<td align="center">'.$r[5].'</td>
					<td align="center">'.($bobot*$r[4]).'</td>
				</tr>
			';
			if($r[5]<'E' and $r[5]!='' and $r[5]!='-' and $r[7]==1 ){
				$jum_sks += $r[4];
				$jum_bobot += ($bobot*$r[4]);
			}
		}else{
			$semester = $r[0].' '.$r[1];
			$isi_transkrip .= '
				<tr bgcolor="yellow">
					<td> '.$semester.'</td>
					<td>'.$r[2].'</td>
					<td>'.$r[3].'</td>
					<td align="center">'.$r[4].'</td>
					<td align="center">-</td>
					<td align="center">-</td>
				</tr>
			';
		}
		

	}
	
	$isi_transkrip .= '</tbody>';

	if($jum_sks==0) {
		$ipk='0.00';
	}else{
		$ipk = number_format(($jum_bobot/$jum_sks),2);
	}
	
	$isi_transkrip .= '
		<tr>
			<td rowspan="3" colspan="2"></td>
			<td><div align="left">Jumlah SKS dan Bobot</div></td>
			<td align="center">'.$jum_sks.'</td>
			<td align="center">&nbsp;</td>
			<td align="center">'.$jum_bobot.'</td>
		</tr>
		<tr>
			<td>IP komulatif</td>
			<td colspan="3"><div align="center">'.$ipk.'</div></td>
		</tr>
		</tbody>
		</table>
		Keterangan: <br>
		<span>Putih</span> : Normal, masuk transkrip dan perhitungan IP kumulatif dan sks kumulatif<br>
		<span style="background-color:green">Hijau</span> : Ulangan, masuk transkrip dan perhitungan IP kumulatif dan sks kumulatif<br>
		<span style="background-color:yellow">Kuning</span> : Nilai belum dikeluarkan, tidak masuk transkrip dan tidak masuk perhitungan IP kumulatif dan sks kumulatif<br>
		<span style="background-color:red">Merah</span> : Sudah diulang, tidak masuk transkrip dan tidak masuk perhitungan IP kumulatif dan sks kumulatif<br>
	';

	$smarty->assign('isitranskrip', $isi_transkrip);

}

}


if(isset($_POST['nim'])){
$data_mahasiswa = $db->QueryToArray("SELECT *
				FROM MAHASISWA
				JOIN PENGGUNA ON MAHASISWA.ID_PENGGUNA = PENGGUNA.ID_PENGGUNA AND PENGGUNA.ID_PERGURUAN_TINGGI = {$id_pt_user}
				JOIN PROGRAM_STUDI ON MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
				JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
				JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
				JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
				WHERE (MAHASISWA.NIM_MHS LIKE '%$_POST[nim]%' OR UPPER(PENGGUNA.NM_PENGGUNA) LIKE UPPER('%{$_POST['nim']}%')) $query
				ORDER BY PROGRAM_STUDI.ID_PROGRAM_STUDI, NIM_MHS");
$smarty->assign('data_mahasiswa', $data_mahasiswa);

}

$smarty->display("data-status-mhs.tpl");
?>