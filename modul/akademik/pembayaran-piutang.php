<?php
require('common.php');
require_once ('ociFunction.php');

$id_pengguna= $user->ID_PENGGUNA; 
$fak=getvar("select fakultas.id_fakultas from pegawai left join unit_kerja on pegawai.id_unit_kerja=unit_kerja.id_unit_kerja 
left join program_studi on unit_kerja.id_program_studi=program_studi.id_program_studi 
left join fakultas on program_studi.id_fakultas=fakultas.id_fakultas 
where pegawai.id_pengguna=$id_pengguna");
$kdfak=$fak['ID_FAKULTAS'];


$mhs=getData("SELECT ID_MHS, NIM_MHS, NM_PENGGUNA, NM_STATUS_PENGGUNA, NM_FAKULTAS, NM_PROGRAM_STUDI, NM_JENJANG
				FROM MAHASISWA
				LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
				LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
				LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
				LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
				LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
				WHERE (STATUS_AKADEMIK_MHS = 1 OR STATUS_AKADEMIK_MHS = 2 OR STATUS_AKADEMIK_MHS = 3 OR STATUS_AKADEMIK_MHS = 8
				OR STATUS_AKADEMIK_MHS = 9 OR STATUS_AKADEMIK_MHS = 10 OR STATUS_AKADEMIK_MHS = 11 OR STATUS_AKADEMIK_MHS = 14 
				OR STATUS_AKADEMIK_MHS = 15 OR STATUS_AKADEMIK_MHS = 17 OR STATUS_AKADEMIK_MHS = 18 OR STATUS_AKADEMIK_MHS = 19
				OR STATUS_AKADEMIK_MHS = 21)
				AND PROGRAM_STUDI.ID_FAKULTAS = '$kdfak'
				ORDER BY FAKULTAS.ID_FAKULTAS, NM_JENJANG, NM_PROGRAM_STUDI, NIM_MHS");

$no = 1;
foreach($mhs as $data){
	$id_semester_piutang = getData("SELECT ID_SEMESTER, TAHUN_AJARAN, NM_SEMESTER 
							FROM SEMESTER WHERE (NM_SEMESTER = 'Ganjil' or NM_SEMESTER = 'Genap') 
							AND THN_AKADEMIK_SEMESTER BETWEEN (SELECT THN_ANGKATAN_MHS FROM MAHASISWA WHERE NIM_MHS = '$data[NIM_MHS]') AND 
							(SELECT THN_AKADEMIK_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True') 
							AND ID_SEMESTER <> (SELECT ID_SEMESTER FROM SEMESTER WHERE NM_SEMESTER = 'Genap' AND THN_AKADEMIK_SEMESTER = 
							(SELECT THN_AKADEMIK_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True'))
							ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC");
	$semester = 0;
	foreach($id_semester_piutang as $sp){
		
		$piutang = getvar("SELECT ID_MHS, ID_SEMESTER, TGL_BAYAR,  SUM(BESAR_BIAYA) BESAR_BIAYA FROM PEMBAYARAN 
										WHERE ID_SEMESTER = '$sp[ID_SEMESTER]' AND ID_MHS ='$data[ID_MHS]'
										GROUP BY ID_MHS, ID_SEMESTER, TGL_BAYAR
										UNION
										SELECT ID_MHS, ID_SEMESTER, TGL_BAYAR, SUM(BESAR_BIAYA) BESAR_BIAYA 
										FROM PEMBAYARAN_CMHS 
										LEFT JOIN MAHASISWA ON MAHASISWA.ID_C_MHS = PEMBAYARAN_CMHS.ID_C_MHS
										WHERE ID_SEMESTER = '$sp[ID_SEMESTER]' AND ID_MHS = '$data[ID_MHS]'
										GROUP BY ID_MHS, ID_SEMESTER, TGL_BAYAR
										ORDER BY TGL_BAYAR DESC");
		
		if($piutang['TGL_BAYAR'] == ''){
			$semester = $semester + 1;
		}
	}
			if($semester > 0){
			$tampil = $tampil. "<tr>
						<td>$no</td>
						<td><a href='pembayaran-piutang.php?nim=$data[NIM_MHS]'>$data[NIM_MHS]</a></td>
						<td>$data[NM_PENGGUNA]</td>
						<td>$data[NM_FAKULTAS]</td>
						<td>$data[NM_JENJANG] - $data[NM_PROGRAM_STUDI]</td>
						<td>$data[NM_STATUS_PENGGUNA]</td>
						<td style='text-align:center'>$semester</td>
					</tr>";
			$no++;
			}
}


if(isset($_GET['nim'])){
	$nim = $_GET['nim'];

	$cek_data = getvar("SELECT M.NIM_MHS,P.NM_PENGGUNA,J.ID_JENJANG,J.NM_JENJANG,
		S.ID_SEMESTER,S.THN_AKADEMIK_SEMESTER,PS.ID_PROGRAM_STUDI,
		PS.NM_PROGRAM_STUDI,J.ID_JALUR,J.NM_JALUR,
        KB.ID_KELOMPOK_BIAYA,KB.NM_KELOMPOK_BIAYA 
            FROM AUCC.PENGGUNA P
            LEFT JOIN AUCC.MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA 
            LEFT JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            LEFT JOIN AUCC.JENJANG J ON PS.ID_JENJANG = J.ID_JENJANG
            LEFT JOIN AUCC.KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = M.ID_KELOMPOK_BIAYA
            LEFT JOIN AUCC.JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS
            LEFT JOIN AUCC.SEMESTER S ON JM.ID_SEMESTER = S.ID_SEMESTER 
            LEFT JOIN AUCC.JALUR J ON JM.ID_JALUR = J.ID_JALUR
        WHERE M.NIM_MHS='$nim'");
	$smarty->assign('cek_data', $cek_data);
		
	$tampil_mhs="";	
	$semester = getData("SELECT ID_SEMESTER, TAHUN_AJARAN, NM_SEMESTER 
							FROM SEMESTER WHERE (NM_SEMESTER = 'Ganjil' or NM_SEMESTER = 'Genap') 
							AND THN_AKADEMIK_SEMESTER BETWEEN (SELECT THN_ANGKATAN_MHS FROM MAHASISWA WHERE NIM_MHS = '$nim') AND 
							(SELECT THN_AKADEMIK_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True') 
							AND ID_SEMESTER <> (SELECT ID_SEMESTER FROM SEMESTER WHERE NM_SEMESTER = 'Genap' AND THN_AKADEMIK_SEMESTER = 
							(SELECT THN_AKADEMIK_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True'))
							ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC");
							
	$biaya = getData("SELECT NM_BIAYA, BIAYA.ID_BIAYA FROM BIAYA
							LEFT JOIN DETAIL_BIAYA ON DETAIL_BIAYA.ID_BIAYA = BIAYA.ID_BIAYA
							LEFT JOIN PEMBAYARAN ON PEMBAYARAN.ID_DETAIL_BIAYA = DETAIL_BIAYA.ID_DETAIL_BIAYA
							LEFT JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBAYARAN.ID_MHS
							WHERE NIM_MHS = '$nim'
							GROUP BY NM_BIAYA, BIAYA.ID_BIAYA 
							ORDER BY NM_BIAYA");
	$smarty->assign('biaya_mhs', $biaya);
	$x = 0;
	foreach($semester as $s){
			$bayar =	getData("SELECT PEMBAYARAN.TGL_BAYAR, NM_BANK, NM_BIAYA, T.ID_BIAYA,
								SUM(CASE WHEN T.ID_BIAYA = DETAIL_BIAYA.ID_BIAYA THEN PEMBAYARAN.BESAR_BIAYA ELSE 0 END) AS BESAR_BIAYA
							FROM (SELECT NM_BIAYA, BIAYA.ID_BIAYA FROM BIAYA
							LEFT JOIN DETAIL_BIAYA ON DETAIL_BIAYA.ID_BIAYA = BIAYA.ID_BIAYA
							LEFT JOIN PEMBAYARAN ON PEMBAYARAN.ID_DETAIL_BIAYA = DETAIL_BIAYA.ID_DETAIL_BIAYA
							LEFT JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBAYARAN.ID_MHS
							WHERE NIM_MHS = '$nim'
							GROUP BY NM_BIAYA, BIAYA.ID_BIAYA) T, PEMBAYARAN
								LEFT JOIN MAHASISWA ON PEMBAYARAN.ID_MHS = MAHASISWA.ID_MHS
								LEFT JOIN DETAIL_BIAYA ON DETAIL_BIAYA.ID_DETAIL_BIAYA = PEMBAYARAN.ID_DETAIL_BIAYA
								LEFT JOIN BANK ON BANK.ID_BANK = PEMBAYARAN.ID_BANK
								WHERE NIM_MHS = '$nim' AND PEMBAYARAN.ID_SEMESTER = '$s[ID_SEMESTER]'
								GROUP BY PEMBAYARAN.TGL_BAYAR, NM_BANK, NM_BIAYA, T.ID_BIAYA
								ORDER BY PEMBAYARAN.TGL_BAYAR DESC, NM_BIAYA ASC");
		
		if($bayar[0]['TGL_BAYAR'] == ''){$pembayaran = 'Belum Bayar'; }else{$pembayaran = $bayar[0]['TGL_BAYAR'];}		
		$tampil_mhs = $tampil_mhs . "<tr>
				<td>" . $s['NM_SEMESTER'] . "<br>" . $s['TAHUN_AJARAN'] . "</td>
				<td>".$bayar[0]['NM_BANK']."</td>
				<td>".$pembayaran."</td>";
		$i = 0;
		$x++;
		$denda = 0;
		foreach($biaya as $b){
			$tampil_mhs = $tampil_mhs . "<td style='text-align:right'>";
			if($b['ID_BIAYA'] == $bayar[$i]['ID_BIAYA']){
			$tampil_mhs = $tampil_mhs . $bayar[$i]['BESAR_BIAYA'];
			}
			$tampil_mhs = $tampil_mhs . "</td>";
			$denda = $bayar[$i]['DENDA_BIAYA'] + $denda;
			$i++;
		}
		$tampil_mhs = $tampil_mhs .	"<td style='text-align:right'>$denda</td></tr>";
	}
	$smarty->assign('tampil_mhs', $tampil_mhs);
}else{
$smarty->assign('tampil', $tampil);
}
$smarty->display('pembayaran-piutang.tpl');
?>
