<?php
/*
YAH 06/06/2011
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$kdprodi=$user->ID_PROGRAM_STUDI;

$id_pengguna= $user->ID_PENGGUNA; 

$kdfak=$user->ID_FAKULTAS; 
$smarty->assign('FAK', $kdfak);

/*//Khusus Fakultas Psikologi
if($kdfak==11){

	echo '
		<script>
		location.href="#akademik-dk!pengampu-mk-psikologi.php";
		</script>
	';

//Fakultas Lain
} else {*/

$dataprodi=getData("select id_program_studi,nm_jenjang||'-' || nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_fakultas=$kdfak and status_aktif_prodi = 1");
$smarty->assign('T_PRODI', $dataprodi);

$thnsmt=getData("select id_semester, tahun_ajaran||' - '||nm_semester as thn_smt from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester in (
select * from (select distinct thn_akademik_semester
from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester<=EXTRACT(YEAR FROM sysdate) order by thn_akademik_semester desc)
where rownum<=10) and nm_semester in ('Ganjil', 'Genap')
order by thn_akademik_semester desc, nm_semester desc");
$smarty->assign('T_THNSMT', $thnsmt);

if (isset($_REQUEST['action'])=='view') {
$kdprodi= $_POST['kdprodi'];
$smarty->assign('PRODI', $kdprodi);		

$smt = $_POST['kdthnsmt'];
$smarty->assign('SMT_AKAD', $smt);
$kelas_smt=getvar("select tahun_ajaran||' - '||nm_semester as thn_smt,thn_akademik_semester from semester where id_semester=$smt");
$smarty->assign('THN_AKAD', $kelas_smt['THN_SMT']);

$dataprodi1=getvar("select nm_jenjang||'-' ||nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_program_studi=$kdprodi and status_aktif_prodi = 1");
$smarty->assign('NM_PRODI', $dataprodi1['PRODI']);
		
$mk=getData("
select id_program_studi,nm_program_studi,kd_mata_kuliah,
nm_mata_kuliah,nama_kelas,nm_ruangan,nm_jadwal_hari,nm_jadwal_jam,kredit_semester,
wm_concat(' '||nip_dosen||'-'||nidn_dosen||'-'||nama_dsn) as team 
from 
(select distinct mahasiswa.id_program_studi,nm_program_studi,mata_kuliah.kd_mata_kuliah,
mata_kuliah.nm_mata_kuliah,nama_kelas,nm_ruangan,nm_jadwal_hari,nm_jadwal_jam,
kurikulum_mk.kredit_semester,dosen.nip_dosen,dosen.nidn_dosen,pjmk_pengampu_mk,
case when trim(pengguna.gelar_depan||' '||pengguna.nm_pengguna||', '||pengguna.gelar_belakang)=',' then 'PJMA BELUM DITENTUKAN' 
when pengguna.gelar_belakang is null then trim(pengguna.gelar_depan||' '||pengguna.nm_pengguna) 
else trim(pengguna.gelar_depan||' '||pengguna.nm_pengguna||', '||pengguna.gelar_belakang) end as nama_dsn
from pengambilan_mk
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk
left join nama_kelas on kelas_mk.no_kelas_mk=id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
where pengambilan_mk.id_semester=$smt and program_studi.id_program_studi=$kdprodi and status_apv_pengambilan_mk=1
order by id_program_studi,kd_mata_kuliah,nama_kelas,pjmk_pengampu_mk desc)
group by id_program_studi,nm_program_studi,kd_mata_kuliah,
nm_mata_kuliah,nama_kelas,nm_ruangan,nm_jadwal_hari,nm_jadwal_jam,
kredit_semester

");
$smarty->assign('T_MK', $mk);
}

$smarty->display('pengampu-mk.tpl');  
/*} */ 
?>
