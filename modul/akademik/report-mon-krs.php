<?php
require('common.php');
require_once ('ociFunction.php');
$kd_fak= $user->ID_FAKULTAS;

$sem_lalu = $user->SEMESTER->ID_SEMESTER_LALU;
$smarty->assign('id_fakultas',$kd_fak);
$id_pengguna= $user->ID_PENGGUNA; 

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True' and id_perguruan_tinggi = '{$id_pt_user}'");
$smarty->assign('id_smt',$smtaktif['ID_SEMESTER']);

if(isset($_REQUEST['nim'])){
	$detail=getData("select s1.nim_mhs,nm_pengguna,status,prodi,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kredit_semester
				from
				(select nim_mhs,nm_pengguna,nm_jenjang,nm_jenjang||'-'||coalesce(nm_singkat_prodi,nm_program_studi) as prodi,
				mahasiswa.status,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester
				from pengambilan_mk
				left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
				left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
				left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
				left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
				left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
				LEFT JOIN MATA_KULIAH ON MATA_KULIAH.ID_MATA_KULIAH = kurikulum_mk.id_mata_kuliah
				left join kelas_mk on kelas_mk.id_kelas_mk = pengambilan_mk.id_kelas_mk
				left join nama_kelas on nama_kelas.id_nama_kelas = kelas_mk.no_kelas_mk
				where mahasiswa.nim_mhs = '$_REQUEST[nim]' and program_studi.id_fakultas=$kd_fak and pengambilan_mk.id_semester='$smtaktif[ID_SEMESTER]'		
				)s1				
				order by prodi,nim_mhs");

$smarty->assign('detail', $detail);
}
else if(isset($_REQUEST['prodi'])){

$report=getData("select s1.nim_mhs,nm_pengguna,status,prodi,SKS_diperoleh,ipk_mhs,nm_jenjang,s3.sks_sem ,
				case when sks_maks is null then (select min(sks_maksimal) as sks_maksimal from beban_sks where id_program_studi = $_REQUEST[prodi]) else sks_maks end as sks_maks
				from
				(select nim_mhs,nm_pengguna,nm_jenjang,nm_jenjang||'-'||coalesce(nm_singkat_prodi,nm_program_studi) as prodi,
				mahasiswa.status
				from pengambilan_mk
				left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
				left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
				left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
				left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
				left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
				LEFT JOIN MATA_KULIAH ON MATA_KULIAH.ID_MATA_KULIAH = kurikulum_mk.id_mata_kuliah
				where program_studi.id_program_studi=$_REQUEST[prodi] and pengambilan_mk.id_semester='$smtaktif[ID_SEMESTER]'
				group by nim_mhs,nm_pengguna,nm_jenjang,nm_jenjang||'-'||coalesce(nm_singkat_prodi,nm_program_studi),
				mahasiswa.status			
				)s1
				left join
				(select nim_mhs, sum(kredit_semester) as SKS_diperoleh, 
				round((sum((kredit_semester * nilai_standar_nilai)) / sum(kredit_semester)), 2) as IPK_MHS
				from 
				(select m.nim_mhs, c.kd_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai,
				row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by nilai_huruf) rangking
				from pengambilan_mk a
				join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
				join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
				join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
				join mahasiswa m on a.id_mhs=m.id_mhs
				join program_studi ps on m.id_program_studi=ps.id_program_studi
				where a.status_apv_pengambilan_mk = 1 and (a.nilai_huruf<>'E' or a.nilai_huruf<>null) and ps.id_fakultas=$kd_fak and a.status_hapus=0 
				and a.status_pengambilan_mk !=0 and a.id_semester !='$smtaktif[ID_SEMESTER]')
				where rangking=1
				group by nim_mhs) s2 on s1.nim_mhs=s2.nim_mhs
				left join 
				(select m.nim_mhs,sum(d.kredit_semester) as sks_sem
				from pengambilan_mk a
				left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
				left join mahasiswa m on a.id_mhs=m.id_mhs
				left join program_studi ps on m.id_program_studi=ps.id_program_studi
				left join semester s on a.id_semester=s.id_semester
				where a.id_Semester='$smtaktif[ID_SEMESTER]'
				and ps.id_program_studi=$_REQUEST[prodi] and a.status_hapus=0 
				and a.status_pengambilan_mk !=0
				group by m.nim_mhs
				)s3 on s1.nim_mhs=s3.nim_mhs
				left join (
					select id_mhs,nim_mhs, ips, sks_sem as sks_sem_kemarin, 
								(select max(sks_maksimal) as sks_maksimal from beban_sks where id_program_studi = $_REQUEST[prodi] and ipk_minimum <= ips ) as sks_maks
							from (
							select id_mhs,nim_mhs,case when sum(bobot*kredit_semester)=0 then 0 else 
												round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips,
												case when id_fakultas=7 then sum(sksreal) else sum(kredit_semester) end as sks_sem	
												from 
												(select id_mhs,nim_mhs,nm_pengguna,id_kurikulum_mk,id_fakultas,kredit_semester, sksreal,min(nilai_huruf) as nilai, 
												max(bobot) as bobot, 
												tahun_ajaran, group_semester, thn_akademik_semester
												from
												(select a.id_mhs,m.nim_mhs,pg.nm_pengguna,ps.id_fakultas,a.id_kurikulum_mk, tahun_ajaran,group_semester,thn_akademik_semester,
												case when (a.nilai_huruf = 'E' or a.nilai_huruf is null) 
												and d.status_mkta in (1,2) then 0
												else d.kredit_semester end as kredit_semester,d.kredit_semester as sksreal,
												case when a.nilai_huruf is null then 'E' else a.nilai_huruf end as nilai_huruf,
												case when e.nilai_standar_nilai is null then 0 else e.nilai_standar_nilai end as bobot
												from pengambilan_mk a
												left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
												left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
												left join mahasiswa m on a.id_mhs=m.id_mhs
												left join pengguna pg on m.id_pengguna=pg.id_pengguna 
												left join program_studi ps on m.id_program_studi=ps.id_program_studi
												left join semester s on a.id_semester=s.id_semester
												where  a.id_semester ='$sem_lalu' 
												and a.status_apv_pengambilan_mk='1' and  ps.id_program_studi=$_REQUEST[prodi] and a.status_hapus=0)
												group by id_mhs, nim_mhs,nm_pengguna,id_kurikulum_mk, id_fakultas, kredit_semester, sksreal, 
												tahun_ajaran, group_semester,thn_akademik_semester
												)
												group by id_mhs,nim_mhs,id_fakultas
							)
				) s4 on s4.nim_mhs = s1.nim_mhs
				order by prodi,nim_mhs");

$smarty->assign('report', $report);
}else{
	
	$prodi=getData("select id_program_studi, nm_jenjang,nm_jenjang||'-'||nm_program_studi as prodi
					 from program_studi 
					 join jenjang on jenjang.id_jenjang=program_studi.id_jenjang
					 where status_aktif_prodi = 1 and id_fakultas = $kd_fak
					 order by nm_jenjang, nm_program_studi");
	$smarty->assign('prodi', $prodi);
}

$smarty->display('report-mon-krs.tpl');
?>
