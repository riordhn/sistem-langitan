<?php
include ('config.php');
include "../pendidikan/class/epsbed.class.php";
$aucc = new epsbed($db);
$kdfak=$user->ID_FAKULTAS; 

$smarty->assign('fakultas', $kdfak);
$smarty->assign('data_semester', $aucc->semester());
$smarty->assign('data_jenjang', $aucc->jenjang());

if(isset($_POST['mode'])){
	
	$smarty->assign('data_smt', $aucc->get_condition_trakm($_POST['semester']));
	$smarty->assign('prodi', $aucc->prodi_condition($_POST['fakultas'], $_POST['jenjang']));
	$smarty->assign('ebsbed', $aucc->trnlm($_POST['fakultas'], $_POST['jenjang'], $_POST['program_studi'], $_POST['semester'], $_POST['jenis_mhs']));
}


$smarty->display('ebsbed/ebsbed-trnlm.tpl');
?>