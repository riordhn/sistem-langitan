<?php
/*
YAH 06/06/2011
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$id_pengguna= $user->ID_PENGGUNA; 

$kdfak=$user->ID_FAKULTAS; 
$smarty->assign('FAK', $kdfak);

//Fakultas Lain
if($kdfak!=11){
	echo 'Anda tidak memiliki hak akses';
} else {

$dataprodi=getData("select id_program_studi,nm_jenjang||'-' || nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_fakultas=$kdfak");
$smarty->assign('T_PRODI', $dataprodi);

$thnsmt=getData("select id_semester, tahun_ajaran||' - '||nm_semester as thn_smt from semester where thn_akademik_semester in (
select * from (select distinct thn_akademik_semester
from semester where thn_akademik_semester<=EXTRACT(YEAR FROM sysdate) order by thn_akademik_semester desc)
where rownum<=10) and nm_semester in ('Ganjil', 'Genap')
order by thn_akademik_semester desc, nm_semester desc");
$smarty->assign('T_THNSMT', $thnsmt);

if (isset($_REQUEST['action'])=='view') {
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$kdprodi= $_POST['kdprodi'];
$smarty->assign('PRODI', $kdprodi);		

$smt = $_POST['kdthnsmt'];
$smarty->assign('SMT_AKAD', $smt);
$kelas_smt=getvar("select tahun_ajaran||' - '||nm_semester as thn_smt,thn_akademik_semester from semester where id_semester=$smt");
$smarty->assign('THN_AKAD', $kelas_smt['THN_SMT']);

$dataprodi1=getvar("select nm_jenjang||'-' ||nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_program_studi=$kdprodi");
$smarty->assign('NM_PRODI', $dataprodi1['PRODI']);
		
$mk=getData("
select id_program_studi,nm_program_studi,kd_mata_kuliah,
nm_mata_kuliah,nama_kelas,kredit_semester,
wm_concat(' '||nip_dosen||'-'||nidn_dosen||'-'||nama_dsn) as team 
from 
(select distinct kelas_mk.id_program_studi,nm_program_studi,mata_kuliah.kd_mata_kuliah,
mata_kuliah.nm_mata_kuliah,nama_kelas, kurikulum_mk.kredit_semester,dosen.nip_dosen,dosen.nidn_dosen,pjmk_pengampu_mk,
case when trim(pengguna.gelar_depan||' '||pengguna.nm_pengguna||', '||pengguna.gelar_belakang)=',' then 'PJMA BELUM DITENTUKAN' 
when pengguna.gelar_belakang is null then trim(pengguna.gelar_depan||' '||pengguna.nm_pengguna) 
else trim(pengguna.gelar_depan||' '||pengguna.nm_pengguna||', '||pengguna.gelar_belakang) end as nama_dsn
from kelas_mk
left join nama_kelas on kelas_mk.no_kelas_mk=id_nama_kelas
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
where kelas_mk.id_semester=$smt and kelas_mk.id_program_studi=$kdprodi
order by kelas_mk.id_program_studi,kd_mata_kuliah,nama_kelas,pjmk_pengampu_mk desc)
group by id_program_studi,nm_program_studi,kd_mata_kuliah,
nm_mata_kuliah,nama_kelas,kredit_semester
");
$smarty->assign('T_MK', $mk);
/*
$rekap=getData("
select trim(pengguna.gelar_depan||' '||pengguna.nm_pengguna||', '||pengguna.gelar_belakang) as nama_dsn,
trim(wm_concat(distinct '<li>'||mata_kuliah.kd_mata_kuliah||' '||mata_kuliah.nm_mata_kuliah||' '||count(distinct kelas_mk.id_kurikulum_mk))) as nama_mk
from pengampu_mk
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
left join kelas_mk on pengampu_mk.id_kelas_mk=kelas_mk.id_kelas_mk
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
where kelas_mk.id_semester=$smt and kelas_mk.id_program_studi=$kdprodi
group by pengguna.gelar_depan, pengguna.nm_pengguna, pengguna.gelar_belakang, kelas_mk.id_kurikulum_mk
");
*/
$rekap=getData("
select nama_dsn, wm_concat(distinct '<li>'||daftar) as jml
from
(
select a.nama_dsn, a.mk||b.jml_kls as daftar from
(
select mata_kuliah.id_mata_kuliah, trim(pengguna.gelar_depan||' '||pengguna.nm_pengguna||', '||pengguna.gelar_belakang) as nama_dsn,
mata_kuliah.kd_mata_kuliah||' '||mata_kuliah.nm_mata_kuliah as mk
from pengampu_mk
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
left join kelas_mk on pengampu_mk.id_kelas_mk=kelas_mk.id_kelas_mk
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
where kelas_mk.id_semester=$smt and kelas_mk.id_program_studi=$kdprodi
group by mata_kuliah.id_mata_kuliah, pengguna.gelar_depan, pengguna.nm_pengguna, pengguna.gelar_belakang, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah
) a join
(
select mata_kuliah.id_mata_kuliah, ' <font color=\"red\">('||count(kelas_mk.id_kelas_mk)||')</font>' as jml_kls
from kelas_mk
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
where kelas_mk.id_semester=$smt and kelas_mk.id_program_studi=$kdprodi
group by mata_kuliah.id_mata_kuliah
) b on a.id_mata_kuliah = b.id_mata_kuliah
)
group by nama_dsn
");
$smarty->assign('T_DOSEN', $rekap);
/*
$span=getData("
select gelar_depan||' '||nm_pengguna||', '||gelar_belakang as dosen,
count(distinct kd_mata_kuliah) as span
from kelas_mk
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join presensi_kelas on kelas_mk.id_kelas_mk=presensi_kelas.id_kelas_mk
left join presensi_mkdos on presensi_kelas.id_presensi_kelas=presensi_mkdos.id_presensi_kelas and dosen.id_dosen=presensi_mkdos.id_dosen
where kelas_mk.id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak) and kelas_mk.id_semester=$smt1 and
tgl_presensi_kelas between to_date ('$awal', 'dd-mm-yy') AND to_date ('$akhir', 'dd-mm-yy')
group by gelar_depan,nm_pengguna,gelar_belakang
");
*/
}

$smarty->display('pengampu-mk-psikologi.tpl');    

}
?>
