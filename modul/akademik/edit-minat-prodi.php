<?php

error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');


$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$id_pengguna= $user->ID_PENGGUNA; 
$kdfak=$user->ID_FAKULTAS; 

$smtaktif=getvar("select * from semester where status_aktif_semester='True'");


$dataprodi=getData("select id_program_studi,nm_jenjang||'-' || nm_program_studi as prodi from program_studi 
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_fakultas=$kdfak and status_aktif_prodi = 1 order by program_studi.id_jenjang");
$smarty->assign('T_PRODI', $dataprodi);

$periode=getData("SELECT NM_TARIF_WISUDA as NM_PERIODE_WISUDA FROM TARIF_WISUDA ORDER BY ID_TARIF_WISUDA DESC");
$smarty->assign('T_PW', $periode);

$periode_wisuda=$_POST['periode'];

$id_program_studi=$_POST['kdprodi'];
//$smarty->assign('kdprodi',$id_program_studi);


if ($_GET['action']=='edit') {
$smarty->assign('disp1','none');
$smarty->assign('disp2','block');

$id_mhs=$_GET['id_mhs'];
$id_program_studi=$_GET['id_prodi'];
$p_ws=$_GET['p_ws'];
$smarty->assign('p_ws',$p_ws);

$det_mhs=getData("select mhs.nim_mhs,mhs.id_mhs,p.nm_pengguna,j.nm_jenjang||'-'||ps.nm_program_studi as prodi,id_prodi_minat,mhs.id_program_studi
from mahasiswa mhs
left join pengguna p on mhs.id_pengguna=p.id_pengguna
left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
left join jenjang j on ps.id_jenjang=j.id_jenjang
where mhs.id_mhs=$id_mhs");
$smarty->assign('T_DATAMHS', $det_mhs);

$minat=getData("select id_prodi_minat,nm_prodi_minat from prodi_minat where id_program_studi=$id_program_studi");

$smarty->assign('DATA_MINAT', $minat);
}

if ($_POST['action']=='update') {
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$id_mhs=$_POST['id_mhs'];
$id_prodi_minat=$_POST['id_minat'];
$periode_wisuda=$_POST['periode'];
//$periode_wisuda=$_POST['kdprodi'];

UpdateData("update mahasiswa set id_prodi_minat=$id_prodi_minat where id_mhs=$id_mhs");
echo "update mahasiswa set id_prodi_minat=$id_prodi_minat where id_mhs=$id_mhs";
}

if ($id_program_studi=='ALL' or $id_program_studi=="") {
$det_minat_mhs=getData("select mhs.nim_mhs,mhs.id_mhs,p.nm_pengguna,j.nm_jenjang||'-'||ps.nm_program_studi as prodi,mhs.id_program_studi,pm.nm_prodi_minat
from mahasiswa mhs
left join pengajuan_wisuda pw on pw.id_mhs=mhs.id_mhs
left join periode_wisuda pew on pw.id_periode_wisuda=pew.id_periode_wisuda
left join tarif_wisuda tw on pew.id_tarif_wisuda=tw.id_tarif_wisuda
left join pengguna p on mhs.id_pengguna=p.id_pengguna
left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
left join jenjang j on ps.id_jenjang=j.id_jenjang
left join prodi_minat pm on mhs.id_prodi_minat=pm.id_prodi_minat
where tw.NM_TARIF_WISUDA='$periode_wisuda' and ps.id_fakultas=$kdfak
order by mhs.id_program_studi,mhs.nim_mhs");



} else {
$det_minat_mhs=getData("select mhs.nim_mhs,mhs.id_mhs,p.nm_pengguna,j.nm_jenjang||'-'||ps.nm_program_studi as prodi,mhs.id_program_studi,pm.nm_prodi_minat
from mahasiswa mhs
left join pengajuan_wisuda pw on pw.id_mhs=mhs.id_mhs
left join periode_wisuda pew on pw.id_periode_wisuda=pew.id_periode_wisuda
left join tarif_wisuda tw on pew.id_tarif_wisuda=tw.id_tarif_wisuda
left join pengguna p on mhs.id_pengguna=p.id_pengguna
left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
left join jenjang j on ps.id_jenjang=j.id_jenjang
left join prodi_minat pm on mhs.id_prodi_minat=pm.id_prodi_minat
where tw.NM_TARIF_WISUDA='$periode_wisuda' and ps.id_fakultas=$kdfak and mhs.id_program_studi=$id_program_studi
order by mhs.id_program_studi,mhs.nim_mhs");


}
$smarty->assign('T_MINAT', $det_minat_mhs);


$smarty->display('edit-minat-prodi.tpl');

?>



