<?php
include 'config.php';
include "class/epsbed.class.php";
$aucc = new epsbed($db);

$data_excel = $aucc->mskmk($_POST['fakultas'], $_POST['jenjang'], $_POST['program_studi']);
ob_start();
?>
<style>
    th{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #1d5700;
    }
    td{
        text-align: left;
    }
</style>
<table cellpadding="3" cellspacing="3" border="1">
    <thead>
        <tr>
		<th>THSMSTBKMK</th>
		<th>KDPTITBKMK</th>
		<th>KDJENTBKMK</th>
		<th>KDPSTTBKMK</th>
		<th>KDKMKTBKMK</th>
		<th>NAKMKTBKMK</th>
		<th>SKSMKTBKMK</th>
		<th>SKSTMTBKMK</th>
		<th>SKSPRTBKMK</th>
		<th>SKSLPTBKMK</th>
		<th>SEMESTBKMK</th>
		<th>KDWPLTBKMK</th>
		<th>KDKURTBKMK</th>
		<th>KDKELTBKMK</th>
		<th>NODOSTBKMK</th>
		<th>JENJATBKMK</th>
		<th>PRODITBKMK</th>
		<th>STKMKTBKMK</th>
		<th>SLBUSTBKMK</th>
		<th>SAPPPTBKMK</th>
		<th>BHNAJTBKMK</th>
		<th>DIKTTTBKMK</th>
		<th>KDUTATBKMK</th>
		<th>KDKUGTBKMK</th>
		<th>KDLAITBKMK</th>
		<th>KDMPATBKMK</th>
		<th>KDMPBTBKMK</th>
		<th>KDMPCTBKMK</th>
		<th>KDMPDTBKMK</th>
		<th>KDMPETBKMK</th>
		<th>KDMPFTBKMK</th>
		<th>KDMPGTBKMK</th>
		<th>KDMPHTBKMK</th>
		<th>KDMPITBKMK</th>
		<th>KDMPJTBKMK</th>
		<th>CRMKLTBKMK</th>
		<th>PRSTDTBKMK</th>
		<th>SMGDSTBKMK</th>
		<th>RPSIMTBKMK</th>
		<th>CSSTUTBKMK</th>
		<th>DISLNTBKMK</th>
		<th>SDILNTBKMK</th>
		<th>CODLNTBKMK</th>
		<th>COLLNTBKMK</th>
		<th>CTXINTBKMK</th>
		<th>PJBLNTBKMK</th>
		<th>PBBLNTBKMK</th>
		<th>UJTLSTBKMK</th>
		<th>TGMKLTBKMK</th>
		<th>TGMODTBKMK</th>
		<th>PSTSITBKMK</th>
		<th>SIMULTBKMK</th>
		<th>LAINNTBKMK</th>
		<th>UJTL1TBKMK</th>
		<th>TGMK1TBKMK</th>
		<th>TGMO1TBKMK</th>
		<th>PSTS1TBKMK</th>
		<th>SIMU1TBKMK</th>
		<th>LAIN1TBKMK</th>
	</tr>
    </thead>
    <tbody>
        <?php
        foreach ($data_excel as $data) {
			
            echo
            "<tr>
		<td>".$data['TAHUN_KURIKULUM']."/td>
		<td>001004</td>
		<td>".$data['KDJENMSPST']."/td>
		<td>".$data['KDPSTMSPST']."/td>
		<td>".$data['KD_MATA_KULIAH']."/td>
		<td>".$data['NM_MATA_KULIAH']."/td>
		<td>".$data['KREDIT_SEMESTER']."/td>
		<td>".$data['KREDIT_TATAP_MUKA']."/td>
		<td>".$data['KREDIT_PRAKTIKUM']."/td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>";
        }
        ?>
    </tbody>
</table>

<?php
$filename = 'mskmk' . date('Y-m-d');
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=$filename");
header("Pragma: ");
header("Cache-Control: ");
ob_flush();
?>
