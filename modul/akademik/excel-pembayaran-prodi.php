<?php
ob_start();
require('common.php');
include "class/aucc.class.php";
$aucc = new AUCC_Akademik($db);
$id_pengguna = $user->ID_PENGGUNA; 
$kdfak = $aucc->cek_fakultas_pengguna($id_pengguna);

$prodi = $aucc->get_prodi_fak($kdfak['ID_FAKULTAS']);
$semester_bayar = $aucc->get_semester_bayar();
$biaya = $aucc->get_biaya();
$pembayaran_prodi = $aucc->data_pembayaran_prodi($kdfak['ID_FAKULTAS']);

echo "
<table width='200' border='1'>
  <tr>
    <th rowspan='2'>Prodi</th>";
    foreach ($semester_bayar as $data){
	$panjang = count($biaya);
    echo "<th colspan='$panjang' style='text-align:center'>
    	$data[THN_AKADEMIK_SEMESTER]
    </th>";
    }
	echo "
    <th rowspan='2'>S Per Prodi</th>
  </tr>
  <tr>";
  foreach ($semester_bayar as $data){
  		foreach ($biaya as $data1){
   			 echo "<th>$data1[NM_BIAYA]</th>";
    	}
  }  
  echo "</tr>";
 
 foreach ($prodi as $data){
  echo "<tr> 
    <td>$data[NM_JENJANG] - $data[NM_PROGRAM_STUDI]</td>";
    $total = 0;
    foreach ($semester_bayar as $data1){
  		foreach ($biaya as $data2){
   			echo "<td style='text-align:right'>";
             	$nilai = false;
                $jumlah = 0;
             	foreach ($pembayaran_prodi as $x){
                	if ($x['THN_AKADEMIK_SEMESTER'] == $data1['THN_AKADEMIK_SEMESTER'] and $x['ID_PROGRAM_STUDI'] == $data['ID_PROGRAM_STUDI']	
                    	and $x['ID_BIAYA'] == $data2['ID_BIAYA']){
                        $jumlah = $jumlah + $x['JUMLAH'];
                        $nilai = true;
                        $total = $total + $jumlah;
                    }
                }
                if ($nilai == true){
                	echo $jumlah;
                }else{
                	echo "0";
                }
             echo "</td>";
         }
   }
    echo "<td style='text-align:right'>$total</td>
  </tr>";
 }
 echo "<tr>
    <td>Total</td>";
    $total_all = 0;
    foreach ($semester_bayar as $data){
  		foreach ($biaya as $data1){
             echo "<td style='text-align:right'>"; 
             $nilai = false;
             $total = 0;
       		 foreach ($pembayaran_prodi as $x){
                	if ($x['THN_AKADEMIK_SEMESTER'] == $data['THN_AKADEMIK_SEMESTER'] and $x['ID_BIAYA'] == $data1['ID_BIAYA']){
                        $total = $total + $x['JUMLAH'];
                        $nilai = true;
                    }
              }
             	if ($nilai == false){
                	echo "0";
                }else{
                	echo $total;
                    $total_all = $total_all + $total;
                }
             echo "</td>";
         }
    }
    echo "<td style='text-align:right'>$total_all</td>
  </tr>
</table>";


$filename = 'Rekapitulasi Pembayaran '. date('Y-m-d');
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=$filename");
header("Pragma: ");
header("Cache-Control: ");
ob_flush();
?>
