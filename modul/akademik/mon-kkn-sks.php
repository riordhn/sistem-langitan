<?php
require('common.php');
require_once ('ociFunction.php');
$kd_fak= $user->ID_FAKULTAS;
$smarty->assign('id_fakultas',$kd_fak);
$id_pengguna= $user->ID_PENGGUNA; 

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True'");
$smarty->assign('id_smt',$smtaktif['ID_SEMESTER']);

if (isset($_GET['action'])=='drop') {
	$id_pengambilan_mk =$_GET['id_pmk'];

	UpdateData("update pengambilan_mk set status_apv_pengambilan_mk=0,keterangan = 'DROP KRS KKN'
				where id_pengambilan_mk=$id_pengambilan_mk") or die('gagal 2');
}


$mon_kkn=getData("select id_pengambilan_mk,s1.nim_mhs,nm_pengguna,kel,agama,status,prodi,SKS_diperoleh,sks_sem, (SKS_diperoleh+sks_sem) as ttl from
				(select id_pengambilan_mk,nim_mhs,nm_pengguna,nm_jenjang||'-'||coalesce(nm_singkat_prodi,nm_program_studi) as prodi,
				mahasiswa.status,case when pengguna.kelamin_pengguna='1' then 'L' else 'P' end as kel,upper(nm_agama) as agama
				from pengambilan_mk
				left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
				left join semester on pengambilan_mk.id_semester=semester.id_semester
				left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
				left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
				left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
				left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
				left join agama on pengguna.id_agama=agama.id_agama
				where kurikulum_mk.status_mkta='2' and id_fakultas=$kd_fak and pengambilan_mk.id_semester='$smtaktif[ID_SEMESTER]'
				order by nm_jenjang,nm_program_studi,nim_mhs)s1
				left join
				(select nim_mhs, sum(kredit_semester) as SKS_diperoleh, 
				round((sum((kredit_semester * nilai_standar_nilai)) / sum(kredit_semester)), 2) as IPK_MHS
				from 
				(select m.nim_mhs, c.kd_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai,
				row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by nilai_huruf) rangking
				from pengambilan_mk a
				join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
				join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
				join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
				join mahasiswa m on a.id_mhs=m.id_mhs
				join program_studi ps on m.id_program_studi=ps.id_program_studi
				where a.status_apv_pengambilan_mk = 1 and (a.nilai_huruf<>'E' or a.nilai_huruf<>null) and ps.id_fakultas=$kd_fak and a.status_hapus=0 
				and a.status_pengambilan_mk !=0 and a.id_semester !='$smtaktif[ID_SEMESTER]')
				where rangking=1
				group by nim_mhs) s2 on s1.nim_mhs=s2.nim_mhs
				left join 
				(select m.nim_mhs,sum(d.kredit_semester) as sks_sem
				from pengambilan_mk a
				left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
				left join mahasiswa m on a.id_mhs=m.id_mhs
				left join program_studi ps on m.id_program_studi=ps.id_program_studi
				left join semester s on a.id_semester=s.id_semester
				where a.id_Semester='$smtaktif[ID_SEMESTER]'
				and a.status_apv_pengambilan_mk='1' and ps.id_fakultas=$kd_fak and a.status_hapus=0 
				and a.status_pengambilan_mk !=0
				group by m.nim_mhs
				)s3 on s1.nim_mhs=s3.nim_mhs");
	
$smarty->assign('T_KKN', $mon_kkn);

$smarty->display('mon-kkn.tpl');

?>
