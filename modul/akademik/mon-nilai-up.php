<?php
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');


$kdfak= $user->ID_FAKULTAS; 

//$smtaktif=getvar("select id_semester from semester where trim(thn_akademik_semester||group_semester) in 
//(select thn_akademik_semester||group_semester from semester where status_aktif_semester='True')
//and nm_semester='Ujian Perbaikan'");

//$smtaktif=getvar("select ID_SEMESTER from semester where status_aktif_semester='True'");

$smtaktif=getvar("select id_semester from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND
nm_semester in ('Ujian Perbaikan') 
and group_semester||thn_akademik_semester in (
select group_semester||thn_akademik_semester from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND status_aktif_semester='True')");

$smt1 = isset($_POST['kdthnsmt']) ? $_POST['kdthnsmt'] : $smtaktif['ID_SEMESTER'];

$smarty->assign('SMT',$smt1);

$thnsmt=getData("select id_semester as id_semester, tahun_ajaran||'-'||nm_semester||'-'||group_semester as thn_smt from semester 
where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester in (select distinct thn_akademik_semester
from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester>=EXTRACT(YEAR FROM sysdate)-2)
order by thn_akademik_semester desc, group_semester desc, nm_semester desc");
$smarty->assign('T_THNSMT', $thnsmt);

if($smt1 != ''){
if($kdfak=='8'){
$nilai_masuk=getData("select kelas_mk.ID_KELAS_MK,kd_mata_kuliah,nm_mata_kuliah, kurikulum_mk.kredit_semester,
nmkelas nama_kelas, gelar_depan||nm_pengguna||', '||gelar_belakang as pjma,
sum(case when id_mhs is not null then 1 else 0 end) as pst,
sum(case when nilai_angka is not null or nilai_angka>=0 then 1 else 0 end) as ttlup,
case when sum(case when flagnilai='1' then 1 else 0 end)>0 then 'PUBLIS' else 'BELUM' end as status 
from pengambilan_mk 
left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk 
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk and pjmk_pengampu_mk=1 
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join fst_kelas on kelas_mk.tipe_kelas_mk=fst_kelas.kdkelas
where pengambilan_mk.id_semester=$smt1 and program_studi.id_fakultas=$kdfak 
group by kelas_mk.ID_KELAS_MK,kd_mata_kuliah,nm_mata_kuliah, kurikulum_mk.kredit_semester,
nama_kelas, gelar_depan,nm_pengguna,gelar_belakang
having sum(case when nilai_angka is not null or nilai_angka>=0 then 1 else 0 end)>0
");
}
else{
	
$nilai_masuk=getData("select kelas_mk.ID_KELAS_MK,kd_mata_kuliah,nm_mata_kuliah, kurikulum_mk.kredit_semester,
nama_kelas, gelar_depan||nm_pengguna||', '||gelar_belakang as pjma,
sum(case when id_mhs is not null then 1 else 0 end) as pst,
sum(case when nilai_angka is not null or nilai_angka>=0 then 1 else 0 end) as ttlup,
case when sum(case when flagnilai='1' then 1 else 0 end)>0 then 'PUBLIS' else 'BELUM' end as status 
from pengambilan_mk 
left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk 
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk and pjmk_pengampu_mk=1 
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
where pengambilan_mk.id_semester=$smt1 and program_studi.id_fakultas=$kdfak 
group by kelas_mk.ID_KELAS_MK,kd_mata_kuliah,nm_mata_kuliah, kurikulum_mk.kredit_semester,
nama_kelas, gelar_depan,nm_pengguna,gelar_belakang
having sum(case when nilai_angka is not null or nilai_angka>=0 then 1 else 0 end)>0
");

}


$smarty->assign('NIL_MSK',$nilai_masuk);

if($kdfak=='8'){
$nilai_blmmasuk=getData("select kelas_mk.ID_KELAS_MK,kd_mata_kuliah,nm_mata_kuliah, kurikulum_mk.kredit_semester,
nmkelas nama_kelas, gelar_depan||nm_pengguna||', '||gelar_belakang as pjma,
sum(case when id_mhs is not null then 1 else 0 end) as pst,
sum(case when nilai_angka is not null or nilai_angka>=0 then 1 else 0 end) as ttlup,
case when sum(case when flagnilai='1' then 1 else 0 end)>0 then 'PUBLIS' else 'BELUM' end as status 
from pengambilan_mk 
left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk 
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk and pjmk_pengampu_mk=1 
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join fst_kelas on kelas_mk.tipe_kelas_mk=fst_kelas.kdkelas
where pengambilan_mk.id_semester=$smt1 and program_studi.id_fakultas=$kdfak 
group by kelas_mk.ID_KELAS_MK,kd_mata_kuliah,nm_mata_kuliah, kurikulum_mk.kredit_semester,
nama_kelas, gelar_depan,nm_pengguna,gelar_belakang
having sum(case when nilai_angka is not null or nilai_angka>=0 then 1 else 0 end)=0
");


}
else{
$nilai_blmmasuk=getData("select kelas_mk.ID_KELAS_MK,kd_mata_kuliah,nm_mata_kuliah, kurikulum_mk.kredit_semester,
nama_kelas, gelar_depan||nm_pengguna||', '||gelar_belakang as pjma,
sum(case when id_mhs is not null then 1 else 0 end) as pst,
sum(case when nilai_angka is not null or nilai_angka>=0 then 1 else 0 end) as ttlup,
case when sum(case when flagnilai='1' then 1 else 0 end)>0 then 'PUBLIS' else 'BELUM' end as status 
from pengambilan_mk 
left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk 
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk and pjmk_pengampu_mk=1 
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
where pengambilan_mk.id_semester=$smt1 and program_studi.id_fakultas=$kdfak 
group by kelas_mk.ID_KELAS_MK,kd_mata_kuliah,nm_mata_kuliah, kurikulum_mk.kredit_semester,
nama_kelas, gelar_depan,nm_pengguna,gelar_belakang
having sum(case when nilai_angka is not null or nilai_angka>=0 then 1 else 0 end)=0
");
}

$smarty->assign('UTS_BLM_MSK',$nilai_blmmasuk);
}

$smarty->display('mon-nilai-up.tpl');

?>
