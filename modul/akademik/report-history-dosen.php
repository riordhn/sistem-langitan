<?php
/*
Yudi Sulistya 02/02/2012
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$kdprodi=$user->ID_PROGRAM_STUDI;

$id_pengguna= $user->ID_PENGGUNA;

$kdfak=$user->ID_FAKULTAS;
$smarty->assign('FAK', $kdfak);

$thnsmt=getData("select id_semester, tahun_ajaran||' - '||nm_semester as thn_smt from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester in (
select * from (select distinct thn_akademik_semester
from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester<=EXTRACT(YEAR FROM sysdate) order by thn_akademik_semester desc)
where rownum<=10) and nm_semester in ('Ganjil', 'Genap')
order by thn_akademik_semester desc, nm_semester desc");
$smarty->assign('T_THNSMT', $thnsmt);

$listdsn=getData("select dosen.id_dosen, upper(pengguna.nm_pengguna) as nm_pengguna
					from kelas_mk
					left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
					left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
					left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
					where pengampu_mk.id_dosen is not null
					and kelas_mk.id_program_studi in (select id_program_studi from program_studi where id_fakultas='".$kdfak."')
					group by dosen.id_dosen, pengguna.nm_pengguna
					order by pengguna.nm_pengguna");
$smarty->assign('T_DSN', $listdsn);

$dataprodi=getData("select id_program_studi||'-'||id_fakultas as id_program_studi,nm_jenjang||'-'||nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_fakultas=$kdfak and status_aktif_prodi = 1");
$smarty->assign('T_PRODI', $dataprodi);

if (isset($_POST['action'])=='view') {
$var = explode("-", $_POST['kdprodi']);
$smt = $_POST['kdthnsmt'];
$nip = $_POST['id_dosen'];

$datathnsmt=getData("select '(TAHUN '||tahun_ajaran||' - SEMESTER '||nm_semester||')' as thn_smt from semester where id_semester=$smt");
$smarty->assign('THNSMT', $datathnsmt);

if ($var[0]=='all') {
$datadsn=getData("select ': '||nip_dosen||' - '||trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nm_dosen from dosen left join pengguna on dosen.id_pengguna=pengguna.id_pengguna where id_dosen=".$nip."");
$smarty->assign('DSN', $datadsn);

$jaf=getData("select mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, jenjang.nm_jenjang,
				program_studi.nm_program_studi, semester.tahun_ajaran, semester.nm_semester
				from kelas_mk
				left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk = kelas_mk.id_kurikulum_mk
				left join mata_kuliah on mata_kuliah.id_mata_kuliah = kurikulum_mk.id_mata_kuliah
				left join pengampu_mk on kelas_mk.id_kelas_mk = pengampu_mk.id_kelas_mk
				left join program_studi on program_studi.id_program_studi = kelas_mk.id_program_studi
				left join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
				left join semester on semester.id_semester = kelas_mk.id_semester
				where kelas_mk.id_semester = '".$smt."' and pengampu_mk.id_dosen = '".$nip."'
				group by mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, jenjang.nm_jenjang,
				program_studi.nm_program_studi, semester.tahun_ajaran, semester.nm_semester
				order by semester.tahun_ajaran desc, semester.nm_semester desc, mata_kuliah.kd_mata_kuliah asc");
$smarty->assign('T_MK', $jaf);

} else {
$datadsn=getData("select ': '||nip_dosen||' - '||trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nm_dosen from dosen left join pengguna on dosen.id_pengguna=pengguna.id_pengguna where id_dosen=".$nip."");
$smarty->assign('DSN', $datadsn);

$jaf=getData("select mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, jenjang.nm_jenjang,
				program_studi.nm_program_studi, semester.tahun_ajaran, semester.nm_semester
				from kelas_mk
				left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk = kelas_mk.id_kurikulum_mk
				left join mata_kuliah on mata_kuliah.id_mata_kuliah = kurikulum_mk.id_mata_kuliah
				left join pengampu_mk on kelas_mk.id_kelas_mk = pengampu_mk.id_kelas_mk
				left join program_studi on program_studi.id_program_studi = kelas_mk.id_program_studi
				left join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
				left join semester on semester.id_semester = kelas_mk.id_semester
				where kelas_mk.id_program_studi = '".$var[0]."' and kelas_mk.id_semester = '".$smt."' and pengampu_mk.id_dosen = '".$nip."'
				group by mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, jenjang.nm_jenjang,
				program_studi.nm_program_studi, semester.tahun_ajaran, semester.nm_semester
				order by mata_kuliah.kd_mata_kuliah asc");
$smarty->assign('T_MK', $jaf);
}
}
$smarty->display('report-history-dosen.tpl');
?>