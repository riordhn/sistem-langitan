<?php
require('common.php');
require_once ('ociFunction.php');
require('class/date.class.php');

$kdprodi= $user->ID_PROGRAM_STUDI;
$id_pengguna= $user->ID_PENGGUNA; 
$id_fakultas = $user->ID_FAKULTAS;
$nama_singkat_pt = $nama_singkat;

$pesan = "";
if(isset($_POST['no_ijasah']) or isset($_POST['tgl_lulus'])){
	
	$kd_ijasah = getvar("SELECT KODE_IJASAH FROM PROGRAM_STUDI WHERE ID_PROGRAM_STUDI = '$_POST[id_prodi]'");
	if($kd_ijasah['KODE_IJASAH'] == ''){
		$pesan = "Isi kode Ijasah Prodi di menu Prodi Ijasah";
	}
	
			$kd_jenjang = getvar("SELECT JENJANG_IJASAH FROM PROGRAM_STUDI WHERE ID_PROGRAM_STUDI = '$_POST[id_prodi]'");
			if($kd_jenjang['JENJANG_IJASAH'] == ''){
				$pesan = "Isi kode jenjang di menu Prodi Ijasah";
			}
			
			
	
			if($pesan == ''){
				$tgl = "''";
				if($_POST['tgl_sk_yudisium'] <> ''){
					$tgl = "to_date('".$_POST['tgl_sk_yudisium']."', 'DD-MM-YYYY')";
				}


				$thn = getvar("SELECT TO_CHAR(TGL_LULUS, 'YYYY') AS TAHUN FROM ADMISI
								WHERE ID_MHS = '$_POST[id_mhs]' AND STATUS_AKD_MHS = 4
								ORDER BY ID_ADMISI DESC");
				$tahun = $thn['TAHUN'];


				if($tahun == ''){
					$tahun = substr($_POST['tgl_lulus'], 6, 4);
				}

				$db->Query(
		            "SELECT ps.kode_ijasah, ps.jenjang_ijasah, f.kode_ijazah_fakultas, pt.kode_ijazah_pt, f.format_ijazah_fakultas, pt.format_no_ijazah_pt 
		            from program_studi ps 
		            join fakultas f on f.id_fakultas = ps.id_fakultas 
		            join perguruan_tinggi pt on pt.id_perguruan_tinggi = f.id_perguruan_tinggi
		            where ps.id_program_studi = '$_POST[id_prodi]'");
		        $kode_ijazah = $db->FetchAssoc();

		        $format_ijazah = $kode_ijazah['FORMAT_IJAZAH_FAKULTAS'];

		        if(empty($format_ijazah)){
		            $format_ijazah = $kode_ijazah['FORMAT_NO_IJAZAH_PT'];
		        }


		        $thn_ijazah = $tahun;

		        $kata_ganti = array( 
		        			'[Seri]' => $_POST['no_ijasah'],
		        			'[Kode PT]' => $kode_ijazah['KODE_IJAZAH_PT'],
		                    '[F]' => $kode_ijazah['KODE_IJAZAH_FAKULTAS'], 
		                    '[PS]' => $kode_ijazah['KODE_IJASAH'], 
		                    '[J]' => $kode_ijazah['JENJANG_IJASAH'], 
		                    '[Tahun]' => $thn_ijazah
		                    ); 

		        $hasil_ijazah = strtr($format_ijazah,$kata_ganti);

		        $no_ijasah = $hasil_ijazah;
				


/*
				if(strlen($id_fakultas) == 1){
					$id_fakultas = "0".$id_fakultas;
				}

				
				
				
				if($prodi == ''){
					$no_ijasah = $_POST['no_ijasah']."/0113/". $id_fakultas . "/". $jenjang . "/" . $tahun;
				}else{
					$no_ijasah = $_POST['no_ijasah']."/0113/". $id_fakultas . "." . $prodi . "/". $jenjang . "/" . $tahun;
				}*/
					
				
				$db->Query("SELECT COUNT(*) AS ADA FROM PENGAJUAN_WISUDA 
					JOIN MAHASISWA ON MAHASISWA.ID_MHS = PENGAJUAN_WISUDA.ID_MHS
					JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
					WHERE NO_IJASAH = '$no_ijasah' AND PROGRAM_STUDI.ID_FAKULTAS = '$id_fakultas' AND PROGRAM_STUDI.ID_JENJANG = '$_POST[id_jenjang]'");
				$cek = $db->FetchAssoc();
				if($cek['ADA'] >= 1 and $_POST['ijasah_admisi'] == ''){					
						
					$pesan = "No ijasah sudah digunakan";
					
					/*$db->Query("SELECT COUNT(*) AS ADA 
					FROM MAHASISWA
					JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
					WHERE PROGRAM_STUDI.ID_FAKULTAS = '1' AND PROGRAM_STUDI.ID_JENJANG = '9' AND ID_MHS = '$_POST[id_mhs]'");
					$cek = $db->FetchAssoc();
					
					if($cek['ADA'] > 0){					
						$pesan = "No ijasah sudah digunakan";
					}*/
					
				}
				
				
				if($pesan == ''){
				
				
						$filename = '../../../foto_wisuda/'.$nama_singkat_pt.'/'.$_POST['nim'].'.jpg';

						/*if (file_exists($filename) and filesize($filename)>0) {
							
						}else{
							$namafile = $_POST['nim'].'.jpg';
							//original file
							$filenya = "http://10.0.110.13/foto/fotoijasah/".$namafile;
							//directory to copy to (must be CHMOD to 777)
							$copydir = "../../../foto_wisuda/";
							$data = file_get_contents($filenya);
							$file = fopen($copydir . $namafile, "w+");
				
							fputs($file, $data);
							fclose($file);
						}
						*/

					
					//UPDATE LULUS
					$nama =  str_replace("'", "''", $_POST['nm_pengguna']);
					
					UpdateData("UPDATE MAHASISWA SET STATUS_AKADEMIK_MHS = '4', STATUS_CEKAL = 1 WHERE ID_MHS = '$_POST[id_mhs]'");
					
					UpdateData("UPDATE PENGGUNA SET NM_PENGGUNA = '$nama', GELAR_DEPAN = '$_POST[gelar_depan]', 
								GELAR_BELAKANG = '$_POST[gelar_belakang]', ID_ROLE = '28' WHERE USERNAME = '$_POST[nim]' AND ID_PERGURUAN_TINGGI = '{$id_pt_user}'");


					$db->Query("SELECT ID_PENGGUNA FROM MAHASISWA WHERE ID_MHS = '$_POST[id_mhs]'");
					$pengguna_role = $db->FetchAssoc();

					$id_pengguna_mahasiswa = $pengguna_role['ID_PENGGUNA'];

					$db->Query("SELECT ID_ROLE_PENGGUNA FROM ROLE_PENGGUNA WHERE ID_PENGGUNA = '$id_pengguna_mahasiswa' AND ID_ROLE = '28'");
					$pengguna_role_id = $db->FetchAssoc();

					$id_role_pengguna = $pengguna_role_id['ID_ROLE_PENGGUNA'];

					if(empty($id_role_pengguna)){

						// insert role pengguna alumni
						InsertData("INSERT INTO ROLE_PENGGUNA (ID_ROLE, ID_PENGGUNA)
									VALUES ('28', '$id_pengguna_mahasiswa')") or die ('error role pengguna');
					}
								
					
						$tgl_lulus = $_POST['tgl_lulus'];
						if($tgl_lulus <> ''){
							$tgl_lulus = "to_date('".$tgl_lulus."', 'DD-MM-YYYY')";
						}else{$tgl_lulus = "";}
					
					//edit yunus krn $judul_ta me-replace null
					//$judul_ta = str_replace("</p>","</br>",$_POST['judul_ta']);
					
					//UpdateData("UPDATE PENGAJUAN_WISUDA SET NO_IJASAH = '$no_ijasah', SK_YUDISIUM = '$_POST[sk_yudisium]', 
					//			TGL_SK_YUDISIUM = $tgl, SKS_TOTAL = '$_POST[sks_total]', ELPT = '$_POST[elpt]', IPK = '$_POST[ipk]',
					//			LAHIR_IJAZAH = '$_POST[ttl]', TGL_LULUS_PENGAJUAN = $tgl_lulus, JUDUL_TA = '$judul_ta'
					//			WHERE ID_MHS = '$_POST[id_mhs]'");
					
					$row = getvar("SELECT MAHASISWA.ID_PROGRAM_STUDI, ID_JENJANG 
								FROM MAHASISWA 
								JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI 
								WHERE ID_MHS = '$_POST[id_mhs]'");	
					
					$id_jenjang = $row['ID_JENJANG'];
					
					$where = "";
					if($id_jenjang == 3 or $id_jenjang == 2){
						$judul_ta = str_replace("</p>","</br>",$_POST['judul_ta']);
						$where = ", JUDUL_TA = '$judul_ta'";
					}
					
					$ttl =  str_replace("'", "''", $_POST['ttl']);
					//echo $ttl;
					
					/*UpdateData("UPDATE PENGAJUAN_WISUDA SET NO_IJASAH = '$no_ijasah', SK_YUDISIUM = '$_POST[sk_yudisium]', 
								TGL_SK_YUDISIUM = $tgl, SKS_TOTAL = '$_POST[sks_total]', ELPT = '$_POST[elpt]', IPK = '$_POST[ipk]',
								LAHIR_IJAZAH = '$ttl', TGL_LULUS_PENGAJUAN = $tgl_lulus $where
								WHERE ID_MHS = '$_POST[id_mhs]'");*/
					UpdateData("UPDATE PENGAJUAN_WISUDA SET NO_IJASAH = '$no_ijasah', SK_YUDISIUM = '$_POST[sk_yudisium]', 
								TGL_SK_YUDISIUM = $tgl, SKS_TOTAL = '$_POST[sks_total]', IPK = '$_POST[ipk]',
								LAHIR_IJAZAH = '$ttl', TGL_LULUS_PENGAJUAN = $tgl_lulus $where
								WHERE ID_MHS = '$_POST[id_mhs]'");
					
					$row = getvar("SELECT ID_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True' AND ID_PERGURUAN_TINGGI = '{$id_pt_user}'");	
					//INSERT ADMISI
					
					if($_POST['ijasah_admisi'] == ''){
						$tgl = date('Y-m-d');		
						InsertData("INSERT INTO ADMISI (ID_MHS, STATUS_AKD_MHS, TGL_USULAN, TGL_APV, ID_SEMESTER, STATUS_APV, ID_PENGGUNA, 
								NO_IJASAH, NO_URUT_IJASAH, TGL_LULUS)
								VALUES ('$_POST[id_mhs]', '4', to_date('$tgl', 'YYYY-MM-DD'), to_date('$tgl', 'YYYY-MM-DD'), '$_POST[semester]'
								,'1', $id_pengguna, '$no_ijasah', '$_POST[no_ijasah]', $tgl_lulus)") or die ('error admisi');
					}else{
					
						UpdateData("UPDATE ADMISI SET TGL_LULUS = $tgl_lulus, NO_IJASAH = '$no_ijasah', NO_URUT_IJASAH = '$_POST[no_ijasah]', ID_SEMESTER = '$_POST[semester]'
									WHERE ID_ADMISI = '$_POST[id_admisi]'");
					}
				
				}
			}
}



if($_POST['nim']){


	$semester = getData("SELECT * FROM SEMESTER WHERE NM_SEMESTER IN ('Ganjil', 'Genap') AND TIPE_SEMESTER = 'REG' AND ID_PERGURUAN_TINGGI = '{$id_pt_user}' ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER ASC");
	$smarty->assign('semester', $semester);

	$cek = getvar("SELECT COUNT(*) AS JML FROM MAHASISWA 
					JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
					WHERE NIM_MHS = '$_POST[nim]' AND ID_FAKULTAS = '$id_fakultas'
					");

	if($cek['JML'] == 1){	
		$nim = $_POST['nim'];
		$pw = getvar("SELECT COUNT(*) AS JML 
				FROM MAHASISWA
				LEFT JOIN PENGAJUAN_WISUDA ON PENGAJUAN_WISUDA.ID_MHS = MAHASISWA.ID_MHS
				JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
				WHERE NIM_MHS = '$nim' AND PENGGUNA.ID_PERGURUAN_TINGGI = '{$id_pt_user}' AND (YUDISIUM = 1 OR YUDISIUM = 2)");
				
		if($pw['JML'] >= 1){
			
			$ijasah = getData("SELECT MAHASISWA.ID_MHS, NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, NM_JENJANG,
				PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_JENJANG, ADMISI.NO_IJASAH, ADMISI.NO_URUT_IJASAH, SK_YUDISIUM, 
				TO_CHAR(TGL_SK_YUDISIUM, 'DD-MM-YYYY') AS TGL_SK_YUDISIUM, TO_CHAR(TGL_LULUS, 'DD-MM-YYYY') AS TGL_LULUS, TO_CHAR(TGL_LULUS, 'YYYY') AS TAHUN_LULUS, SKS_TOTAL, ELPT, IPK,
				GELAR_DEPAN, GELAR_BELAKANG, LAHIR_IJAZAH, JUDUL_TA, ADMISI.ID_ADMISI,
				TO_CHAR(TGL_LAHIR_PENGGUNA, 'YYYY-MM-DD') AS TGL_LAHIR_PENGGUNA, initcap(NM_KOTA) as NM_KOTA, ADMISI.ID_SEMESTER, JUMLAH_CETAK, TGL_PENGAJUAN_WISUDA
				FROM MAHASISWA
				LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
				LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
				LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
				LEFT JOIN PENGAJUAN_WISUDA ON PENGAJUAN_WISUDA.ID_MHS = MAHASISWA.ID_MHS
				LEFT JOIN ADMISI ON ADMISI.ID_MHS = MAHASISWA.ID_MHS AND TGL_LULUS IS NOT NULL AND STATUS_APV = 1
				LEFT JOIN KOTA ON KOTA.ID_KOTA = LAHIR_KOTA_MHS
				WHERE NIM_MHS = '$nim' AND PENGGUNA.ID_PERGURUAN_TINGGI = '{$id_pt_user}'
				ORDER BY ID_ADMISI DESC
				");
			$smarty->assign('ijasah', $ijasah);
					$tgl_lahir = tgl_indo($ijasah[0]['TGL_LAHIR_PENGGUNA']);
					$smarty->assign('tgl_lahir', $tgl_lahir);

			$db->Query(
	            "SELECT ps.kode_ijasah, ps.jenjang_ijasah, f.kode_ijazah_fakultas, pt.kode_ijazah_pt, f.format_ijazah_fakultas, pt.format_no_ijazah_pt 
	            from program_studi ps 
	            join fakultas f on f.id_fakultas = ps.id_fakultas 
	            join perguruan_tinggi pt on pt.id_perguruan_tinggi = f.id_perguruan_tinggi
	            where ps.id_program_studi = {$ijasah[0]['ID_PROGRAM_STUDI']}");
	        $kode_ijazah = $db->FetchAssoc();

	        $format_ijazah = $kode_ijazah['FORMAT_IJAZAH_FAKULTAS'];

	        if(empty($format_ijazah)){
	            $format_ijazah = $kode_ijazah['FORMAT_NO_IJAZAH_PT'];
	        }


	        /*$thn_ijazah = $ijasah[0]['TAHUN_LULUS'];*/
	        $thn_ijazah = date('Y');

	        $kata_ganti = array( 
	        			'[Kode PT]' => $kode_ijazah['KODE_IJAZAH_PT'],
	                    '[F]' => $kode_ijazah['KODE_IJAZAH_FAKULTAS'], 
	                    '[PS]' => $kode_ijazah['KODE_IJASAH'], 
	                    '[J]' => $kode_ijazah['JENJANG_IJASAH'], 
	                    '[Tahun]' => $thn_ijazah
	                    ); 

	        $hasil_ijazah = strtr($format_ijazah,$kata_ganti);

	        $smarty->assign('format_ijazah', $format_ijazah);
	        $smarty->assign('hasil_ijazah', $hasil_ijazah);

					$kode_ijasah =  $ijasah[0]['NO_URUT_IJASAH'];
					$smarty->assign('kode_ijasah', $kode_ijasah);
					$smarty->assign('semester_ijasah', $ijasah[0]['ID_SEMESTER']);
					
					if($ijasah[0]['ID_SEMESTER']==''){
						$row = getvar("SELECT ID_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True' AND ID_PERGURUAN_TINGGI = '{$id_pt_user}'");
						$smarty->assign('semester_aktif', $row['ID_SEMESTER']);
					}
		}else{
			$pesan = "Mahasiswa Belum Yudisium";
		}
	}

}

$smarty->assign('pesan', $pesan);
$smarty->display("wisuda-no-draft.tpl");
?>
