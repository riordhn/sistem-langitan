<?php
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
include "class/aucc.class.php";
$aucc = new AUCC_Akademik($db);
$id_pengguna = $user->ID_PENGGUNA; 

if (post('nim')) {
	
	$nim = trim(post('nim'));
    $nim = str_replace("'", "''", $nim);

    if (strlen($nim) >= 8) {
		if ($aucc->cek_pengguna($nim)){
			$semester = $aucc->get_semester_aktif();
			$cek_bayar_cmhs = $aucc->cek_pembayaran_cmhs($nim, $semester['ID_SEMESTER']);
			$kdfak = $aucc->cek_fakultas_pengguna($id_pengguna);
			$fak_mhs = substr($nim, 0, 2);
			if($kdfak['ID_FAKULTAS'] == $fak_mhs){
				if($cek_bayar_cmhs['CEK_BAYAR'] > 0){
					$smarty->assign('data_pembayaran', $aucc->detail_pembayaran_cmhs($nim, $semester['ID_SEMESTER']));
				}else{
					$smarty->assign('data_pembayaran', $aucc->detail_pembayaran($nim, $semester['ID_SEMESTER']));
				}
			}else{
					$smarty->assign('data_kosong', 'Hanya untuk MAHASISWA fakultas '.$kdfak[NM_FAKULTAS]);
			}
		}else{
             $smarty->assign('data_kosong', 'Data Mahasiswa Masih Belum ada dalam database kami');
		}
	}
}

$smarty->display('pembayaran-cek.tpl');
?>
