<?php
/*
Yudi Sulistya 04/01/2012
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');
include 'includes/function.php';

$akademik = new Akademik($db);  // lokasi /includes/Akademik.class.php

$kdprodi=$user->ID_PROGRAM_STUDI;

$id_pengguna= $user->ID_PENGGUNA; 
$fak=getvar("select fakultas.id_fakultas, upper(fakultas.nm_fakultas) as nm_fakultas from pegawai left join unit_kerja on pegawai.id_unit_kerja=unit_kerja.id_unit_kerja 
left join fakultas on unit_kerja.id_fakultas=fakultas.id_fakultas 
where pegawai.id_pengguna=$id_pengguna");
$kdfak=$fak['ID_FAKULTAS'];
$nmfak=$fak['NM_FAKULTAS'];
$smarty->assign('FAK', $kdfak);
$smarty->assign('NM_FAK', $nmfak);
$smarty->assign('NIMX', '');

/*if($kdfak==11){

	echo '
		<script>
		location.href="#transkrip-tn!transkrip-psikologi.php";
		</script>
	';

//Fakultas Lain
} else {*/

$depan=time();
$belakang=strrev(time());

if (isset($_POST['action'])=='view') {
$nim = $_POST['nim'];
//$mode = $_POST['mode'];

//Yudi Sulistya 21/07/2012
//Enkrip url untuk cetak
$id=getvar("select id_mhs from mahasiswa where nim_mhs='".$nim."'");
$cetak=$id['ID_MHS'];
$id_mhs=$id['ID_MHS'];

//UMUM
$link_fisip = 'proses/transkrip-cetak-D3S1.php?'.paramEncrypt($depan.'='.$belakang.$depan.'&cetak='.$cetak.'&'.$belakang.'='.$depan.$belakang).'';
$smarty->assign('LINK_D3S1', $link_fisip);
$link_sidip_pasca = 'proses/transkrip-cetak-pasca.php?'.paramEncrypt($depan.'='.$belakang.$depan.'&cetak='.$cetak.'&'.$belakang.'='.$depan.$belakang).'';
$smarty->assign('LINK_PASCA', $link_sidip_pasca);

$link_fisip_eng = 'proses/transkrip-cetak-D3S1-en.php?'.paramEncrypt($depan.'='.$belakang.$depan.'&cetak='.$cetak.'&'.$belakang.'='.$depan.$belakang).'';
$smarty->assign('LINK_D3S1_ENG', $link_fisip_eng);
$link_sidip_pasca_eng = 'proses/transkrip-cetak-pasca-en.php?'.paramEncrypt($depan.'='.$belakang.$depan.'&cetak='.$cetak.'&'.$belakang.'='.$depan.$belakang).'';
$smarty->assign('LINK_PASCA_ENG', $link_sidip_pasca_eng);


$datamhs=getData("select ': '||nim_mhs||' - '||upper(nm_pengguna) as nm_mhs from mahasiswa left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna where nim_mhs='".$nim."' and id_program_studi in (select id_program_studi from program_studi where id_fakultas='".$kdfak."')");
$smarty->assign('MHS', $datamhs);
$smarty->assign('NIMX', $nim);			
$biomhs=getData("select mhs.id_mhs, mhs.nim_mhs, upper(p.nm_pengguna) as nm_pengguna, j.nm_jenjang, ps.nm_program_studi, mhs.status_akademik_mhs,
				TO_CHAR(pw.tgl_lulus_pengajuan, 'DD-MM-YYYY') as tgl_lulus, pw.no_ijasah, sp.nm_status_pengguna
				from mahasiswa mhs 
				left join pengajuan_wisuda pw on pw.id_mhs=mhs.id_mhs
				left join pengguna p on mhs.id_pengguna=p.id_pengguna 
				left join status_pengguna sp on sp.id_status_pengguna=mhs.status_akademik_mhs
				left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
				left join jenjang j on ps.id_jenjang=j.id_jenjang
				where mhs.nim_mhs='".$nim."' and mhs.id_program_studi in (select id_program_studi from program_studi where id_fakultas='".$kdfak."')");
$smarty->assign('T_MHS', $biomhs);

// Pilih nilai terakhir
/*
if($mode == 2){
$jaf=getData("select max(thn) as thn, max(smt) as smt, kd_mata_kuliah as kode, nm_mata_kuliah as nama, kredit_semester as sks, min(nilai_huruf) as nilai, max(nilai_standar_nilai) as bobot
from 
(select f.thn_akademik_semester as thn,case when f.nm_semester='Ganjil' then 1 else 2 end as smt,c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf,e.nilai_standar_nilai
from pengambilan_mk a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
left join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join semester f on a.id_semester=f.id_semester
where a.nilai_huruf<>'E' and a.nilai_huruf is not null  and m.nim_mhs='".$nim."' and a.status_apv_pengambilan_mk=1)
group by kd_mata_kuliah, nm_mata_kuliah, kredit_semester
order by nm_mata_kuliah");
$smarty->assign('T_MK', $jaf);

$ipk=getData("select sum(sks) as sks_total, sum((bobot*sks)) as bobot_total, round((sum((bobot*sks))/sum(sks)),2) as ipk
from 
(select max(thn) as thn, max(smt) as smt, kd_mata_kuliah as kode, nm_mata_kuliah as nama, kredit_semester as sks, min(nilai_huruf) as nilai, max(nilai_standar_nilai) as bobot
from 
(select f.thn_akademik_semester as thn,case when f.nm_semester='Ganjil' then 1 else 2 end as smt,c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf,e.nilai_standar_nilai
from pengambilan_mk a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
left join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join semester f on a.id_semester=f.id_semester
where a.nilai_huruf<>'E' and a.nilai_huruf is not null  and m.nim_mhs='".$nim."' and a.status_apv_pengambilan_mk=1)
group by kd_mata_kuliah, nm_mata_kuliah, kredit_semester)");
$smarty->assign('T_IPK', $ipk);

// Pilih nilai terbaik
} else if($mode == 1){
$jaf=getData("select kd_mata_kuliah as kode, nm_mata_kuliah as nama, kredit_semester as sks, min(nilai_huruf) as nilai, max(nilai_standar_nilai) as bobot
from 
(select c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai
from pengambilan_mk a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
left join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where a.nilai_huruf<>'E' and a.nilai_huruf is not null  and m.nim_mhs='".$nim."' and a.status_apv_pengambilan_mk=1)
group by kd_mata_kuliah, nm_mata_kuliah, kredit_semester
order by nm_mata_kuliah");
$smarty->assign('T_MK', $jaf);

$ipk=getData("select sum(sks) as sks_total, sum((bobot*sks)) as bobot_total, round((sum((bobot*sks))/sum(sks)),2) as ipk
from 
(select kd_mata_kuliah as kode, nm_mata_kuliah as nama, kredit_semester as sks, min(nilai_huruf) as nilai, max(nilai_standar_nilai) as bobot
from 
(select c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai
from pengambilan_mk a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
left join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where a.nilai_huruf<>'E' and a.nilai_huruf is not null and m.nim_mhs='".$nim."' and a.status_apv_pengambilan_mk=1)
group by kd_mata_kuliah, nm_mata_kuliah, kredit_semester)");
$smarty->assign('T_IPK', $ipk);
*/

// Pengambilan IPK
$ipk = $akademik->get_ipk_mhs($id_mhs);
$smarty->assign('T_IPK', $ipk);

// Pengambilan
$pengambilan_mk_set = $akademik->list_pengambilan_mk_mhs($id_mhs); 
$smarty->assign('T_MK', $pengambilan_mk_set);

/*$jaf=getData("select e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where rangking=1 and m.nim_mhs='{$nim}'");
$smarty->assign('T_MK', $jaf);*/
/*
$ipk=getData("select sum(sks) as sks_total, sum((bobot*sks)) as bobot_total, round((sum((bobot*sks))/sum(sks)),2) as ipk
from 
(select kd_mata_kuliah as kode, nm_mata_kuliah as nama, kredit_semester as sks, min(nilai_huruf) as nilai, max(nilai_standar_nilai) as bobot
from 
(select c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai
from pengambilan_mk a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
left join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where a.nilai_huruf<>'E' and a.nilai_huruf is not null and m.nim_mhs='".$nim."' and a.status_apv_pengambilan_mk=1)
group by kd_mata_kuliah, nm_mata_kuliah, kredit_semester)");
*/
/*$ipk=getData("SELECT sum(sks) as sks_total, sum((bobot*sks)) as bobot_total, round((sum((bobot*sks))/sum(sks)),2) as ipk
from 
(
select e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where rangking=1 and m.nim_mhs='{$nim}'
)
");
$smarty->assign('T_IPK', $ipk);*/

} else {
}

$smarty->display('transkrip.tpl');  
/*}*/
?>