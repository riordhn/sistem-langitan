<?php
//Yudi Sulistya, 30/04/2012

error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$id_pengguna= $user->ID_PENGGUNA;
$kdfak=$user->ID_FAKULTAS;
$idkelas= $_GET['id_kelas_mk'];
$id_mhs= $_GET['id_mhs'];

$datamhs=getvar("select nim_mhs,nm_pengguna from mahasiswa join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna where id_mhs=$id_mhs");

$smarty->assign('id_mhs',$id_mhs);
$smarty->assign('nim',$datamhs['NIM_MHS']);
$smarty->assign('namamhs',$datamhs['NM_PENGGUNA']);

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True'");
//$smtaktif=getvar("select id_semester from semester where id_semester=21");
$smt_aktif=$smtaktif['ID_SEMESTER'];

if ($_GET['action']=='entry') {
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$datakelas=getData("select kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nm_ruangan,nama_kelas from kelas_mk
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
where kelas_mk.id_kelas_mk=$idkelas");
$smarty->assign('DT_KELAS', $datakelas);

$datapresensi=getData("select tgl_presensi_kelas,jam,isi_materi_mk,
wm_concat('<li>'||nmdos1) as nmdos,id_materi_mk,hadir,absen,idpresensi from (
select tgl_presensi_kelas,waktu_mulai||'-'||waktu_selesai as jam,isi_materi_mk,
' '||gelar_depan||' '||nm_pengguna||','||gelar_belakang as nmdos1,presensi_kelas.id_materi_mk,
sum(case when presensi_mkmhs.kehadiran=1 then 1 else 0 end ) as hadir,
sum(case when presensi_mkmhs.kehadiran=0 then 1 else 0 end ) as absen,presensi_kelas.id_presensi_kelas as idpresensi
from presensi_kelas
left join materi_mk on presensi_kelas.id_materi_mk=materi_mk.id_materi_mk
left join presensi_mkdos on presensi_kelas.id_presensi_kelas=presensi_mkdos.id_presensi_kelas
left join dosen on presensi_mkdos.id_dosen=dosen.id_dosen
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
left join presensi_mkmhs on presensi_kelas.id_presensi_kelas=presensi_mkmhs.id_presensi_kelas
where presensi_kelas.id_kelas_mk=$idkelas and PRESENSI_KELAS.ID_PRESENSI_KELAS not in 
(select id_presensi_kelas from presensi_mkmhs where presensi_mkmhs.id_mhs=$id_mhs and id_presensi_kelas in (
select id_presensi_kelas from presensi_kelas where presensi_kelas.id_kelas_mk=$idkelas) and kehadiran=1)
group by tgl_presensi_kelas, waktu_mulai||'-'||waktu_selesai, isi_materi_mk,
presensi_kelas.id_presensi_kelas,presensi_kelas.id_materi_mk,' '||gelar_depan||' '||nm_pengguna||','||gelar_belakang
order by tgl_presensi_kelas)
group by tgl_presensi_kelas,jam,isi_materi_mk,id_materi_mk,hadir,absen,idpresensi order by tgl_presensi_kelas");


$smarty->assign('DATA_PRESENSI', $datapresensi);
}

if ($_GET['action']=='presensi') {
$smarty->assign('disp1','none');
$smarty->assign('disp2','block');

$nim=$_GET['nim'];
$smarty->assign('nim',$nim);

$id_presensi_kelas=$_GET['id_presensi'];
$dt_susul=getData("select id_presensi_kelas,tgl_presensi_kelas,waktu_mulai,waktu_selesai,
presensi_kelas.id_materi_mk,isi_materi_mk from presensi_kelas
left join materi_mk on presensi_kelas.id_materi_mk=materi_mk.id_materi_mk
where id_presensi_kelas=$id_presensi_kelas");
$smarty->assign('DT_SUSULAN', $dt_susul);
}

if ($_POST['action']=='susulan') {
$id_presensi_kelas=$_POST['id_presensi'];
$id_kelas=$_POST['id_kelas'];
$nim=$_POST['nim'];
$ket=$_POST['ket'];

$mhs=getvar("select mahasiswa.id_mhs from pengambilan_mk 
left join mahasiswa on mahasiswa.id_mhs=pengambilan_mk.id_mhs 
left join kelas_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk
where mahasiswa.nim_mhs='".$nim."' and pengambilan_mk.id_kelas_mk=$id_kelas and pengambilan_mk.id_semester=$smt_aktif and pengambilan_mk.status_apv_pengambilan_mk=1");
$id_mhs=$mhs['ID_MHS'];

if($id_mhs != null){
$cek=getvar("select count(*) as cek from presensi_mkmhs where id_presensi_kelas=$id_presensi_kelas and id_mhs=$id_mhs");
$cek_data=$cek['CEK'];

if($cek_data == 0){
	InsertData("insert into presensi_mkmhs (id_presensi_kelas, id_mhs, kehadiran) values ($id_presensi_kelas, $id_mhs, $ket)");
	echo '
	<script>location.href="javascript:history.go(-2)";</script>
	<script>alert("Data berhasil disimpan")</script>';
} else {
	gantidata("update presensi_mkmhs set kehadiran=$ket where id_presensi_kelas=$id_presensi_kelas and id_mhs=$id_mhs");
	echo '
	<script>location.href="javascript:history.go(-2)";</script>
	<script>alert("Data berhasil disimpan")</script>';
}
} else {
echo '
<script>alert("NIM tersebut tidak terdaftar pada MA")</script>
<script>location.href="#presensi-susulan!susulan.php";</script>
';
}
}

$smarty->display('entry-presensi-susulan.tpl');

?>
