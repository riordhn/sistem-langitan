<?php
/*
Yudi Sulistya 07/03/2012
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

/*
$kdprodi=$user->ID_PROGRAM_STUDI;

$id_pengguna= $user->ID_PENGGUNA; 
$fak=getvar("select fakultas.id_fakultas from pegawai left join unit_kerja on pegawai.id_unit_kerja=unit_kerja.id_unit_kerja 
left join program_studi on unit_kerja.id_program_studi=program_studi.id_program_studi 
left join fakultas on program_studi.id_fakultas=fakultas.id_fakultas 
where pegawai.id_pengguna=$id_pengguna");
$kdfak=$fak['ID_FAKULTAS'];
*/
$kdfak=$user->ID_FAKULTAS;
$smarty->assign('FAK', $kdfak);

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True'");
$smt=$smtaktif['ID_SEMESTER'];

$dataprodi=getData("select id_program_studi||'-'||id_fakultas as id_program_studi,nm_jenjang||'-'||nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_fakultas=$kdfak and status_aktif_prodi = 1");
$smarty->assign('T_PRODI', $dataprodi);


if (isset($_POST['action'])=='view') {
$var = explode("-", $_POST['kdprodi']);

if ($var[0]=='all') {
$dataprodi1=getData("select (': FAKULTAS '||nm_fakultas) as prodi from fakultas where id_fakultas=".$var[1]."");
$smarty->assign('NM_PRODI', $dataprodi1);

$jaf=getData("select distinct mahasiswa.nim_mhs, dosen.nip_dosen, pengguna.nm_pengguna
from pengambilan_mk
left join mahasiswa on mahasiswa.id_mhs=pengambilan_mk.id_mhs
left join dosen_wali on mahasiswa.id_mhs=dosen_wali.id_mhs
left join dosen on dosen.id_dosen=dosen_wali.id_dosen
left join pengguna on pengguna.id_pengguna=dosen.id_pengguna
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
where program_studi.id_fakultas=".$var[1]." and pengambilan_mk.status_apv_pengambilan_mk=0 and pengambilan_mk.id_semester=".$smt."
and dosen_wali.status_dosen_wali=1
group by mahasiswa.nim_mhs, dosen.nip_dosen, pengguna.nm_pengguna
");
$smarty->assign('T_MK', $jaf);

} else {
$dataprodi1=getData("select (': PRODI '||nm_jenjang||'-'||nm_program_studi) as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_program_studi=".$var[0]." and status_aktif_prodi = 1");
$smarty->assign('NM_PRODI', $dataprodi1);

$jaf=getData("select distinct mahasiswa.nim_mhs, dosen.nip_dosen, pengguna.nm_pengguna
from pengambilan_mk
left join mahasiswa on mahasiswa.id_mhs=pengambilan_mk.id_mhs
left join dosen_wali on mahasiswa.id_mhs=dosen_wali.id_mhs
left join dosen on dosen.id_dosen=dosen_wali.id_dosen
left join pengguna on pengguna.id_pengguna=dosen.id_pengguna
where mahasiswa.id_program_studi=".$var[0]." and pengambilan_mk.status_apv_pengambilan_mk=0 and pengambilan_mk.id_semester=".$smt."
and dosen_wali.status_dosen_wali=1
group by mahasiswa.nim_mhs, dosen.nip_dosen, pengguna.nm_pengguna

");
$smarty->assign('T_MK', $jaf);
}
}
$smarty->display('cek-apv.tpl');  
?>