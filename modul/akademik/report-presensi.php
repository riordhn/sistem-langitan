<?php
/*
Yudi Sulistya 19/12/2011
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$kdprodi=$user->ID_PROGRAM_STUDI;


$kdfak=$user->ID_FAKULTAS;
$smarty->assign('FAK', $kdfak);


$thnsmt=getData("select id_semester, tahun_ajaran||' - '||nm_semester as thn_smt from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester in (
select * from (select distinct thn_akademik_semester 
from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester<=EXTRACT(YEAR FROM sysdate) order by thn_akademik_semester desc) 
where rownum<=10) 
and (nm_semester ='Ganjil' or nm_semester ='Genap')
order by thn_akademik_semester desc, nm_semester desc");
$smarty->assign('T_THNSMT', $thnsmt);

$dataprodi=getData("select id_program_studi||'-'||id_fakultas as id_program_studi,nm_jenjang||'-'||nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_fakultas=$kdfak and status_aktif_prodi = 1");
$smarty->assign('T_PRODI', $dataprodi);



if (isset($_POST['action'])=='view') {
$var = explode("-", $_POST['kdprodi']);
$smt = $_POST['kdthnsmt'];

$datathnsmt=getData("select '(TAHUN '||tahun_ajaran||' - SEMESTER '||nm_semester||')' as thn_smt from semester where id_semester=$smt");
$smarty->assign('THNSMT', $datathnsmt);

if ($var[0]=='all') {
$dataprodi1=getData("select (': FAKULTAS '||nm_fakultas) as prodi from fakultas where id_fakultas=".$var[1]."");
$smarty->assign('NM_PRODI', $dataprodi1);

$jaf=getData("
select * from 
(
(select kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah,
kurikulum_mk.kredit_semester, nama_kelas.nama_kelas, jadwal_hari.nm_jadwal_hari, ruangan.nm_ruangan,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam, sum(case when pengambilan_mk.status_apv_pengambilan_mk=1 then 1 else 0 end) as peserta,
semester.nm_semester, semester.tahun_ajaran,semester.id_semester, program_studi.id_fakultas, kelas_mk.id_program_studi,
trim(pengguna.gelar_depan||' '||pengguna.nm_pengguna||' '||gelar_belakang) as pjma
from kelas_mk
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk
left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk=kelas_mk.id_kurikulum_mk
left join mata_kuliah on mata_kuliah.id_mata_kuliah=kurikulum_mk.id_mata_kuliah
left join nama_kelas on nama_kelas.id_nama_kelas=kelas_mk.no_kelas_mk
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_hari on jadwal_hari.id_jadwal_hari=jadwal_kelas.id_jadwal_hari
left join jadwal_jam on jadwal_jam.id_jadwal_jam=jadwal_kelas.id_jadwal_jam
left join ruangan on ruangan.id_ruangan=jadwal_kelas.id_ruangan
left join semester on kelas_mk.id_semester=semester.id_semester
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk 
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
where kelas_mk.id_semester=$smt and pengambilan_mk.status_apv_pengambilan_mk=1
and program_studi.id_fakultas=".$var[1]." and pengampu_mk.pjmk_pengampu_mk=1
group by kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah,
kurikulum_mk.kredit_semester, nama_kelas.nama_kelas, jadwal_hari.nm_jadwal_hari, ruangan.nm_ruangan,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai, semester.nm_semester, semester.tahun_ajaran,semester.id_semester, program_studi.id_fakultas, kelas_mk.id_program_studi,
trim(pengguna.gelar_depan||' '||pengguna.nm_pengguna||' '||gelar_belakang)
) a
left join 
(select id_kelas_mk, round(avg(persen),2)||'%' as rerata
from 
(
select pengambilan_mk.id_kelas_mk, mahasiswa.nim_mhs,
round((sum(presensi_mkmhs.kehadiran)/count(presensi_kelas.id_presensi_kelas))*100,2) as persen
from mahasiswa
left join pengambilan_mk on mahasiswa.id_mhs=pengambilan_mk.id_mhs
left join kelas_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk
left join presensi_kelas on presensi_kelas.id_kelas_mk=pengambilan_mk.id_kelas_mk 
left join presensi_mkmhs on presensi_mkmhs.id_mhs=pengambilan_mk.id_mhs and presensi_mkmhs.id_presensi_kelas=presensi_kelas.id_presensi_kelas 
where kelas_mk.id_program_studi in (select id_program_studi from program_studi where id_fakultas=".$var[1].") and pengambilan_mk.status_apv_pengambilan_mk=1
group by pengambilan_mk.id_kelas_mk, mahasiswa.nim_mhs
)
group by id_kelas_mk
) b on a.id_kelas_mk=b.id_kelas_mk
)
");
$smarty->assign('T_MK', $jaf);

} else {
$dataprodi1=getData("select (': PRODI '||nm_jenjang||'-'||nm_program_studi) as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_program_studi=".$var[0]."");
$smarty->assign('NM_PRODI', $dataprodi1);


$jaf=getData("
select * from 
(
(select kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah,
kurikulum_mk.kredit_semester, nama_kelas.nama_kelas, jadwal_hari.nm_jadwal_hari, ruangan.nm_ruangan,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam, sum(case when pengambilan_mk.status_apv_pengambilan_mk=1 then 1 else 0 end) as peserta,
semester.nm_semester, semester.tahun_ajaran,semester.id_semester, program_studi.id_fakultas, kelas_mk.id_program_studi,
trim(pengguna.gelar_depan||' '||pengguna.nm_pengguna||' '||gelar_belakang) as pjma
from kelas_mk
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk
left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk=kelas_mk.id_kurikulum_mk
left join mata_kuliah on mata_kuliah.id_mata_kuliah=kurikulum_mk.id_mata_kuliah
left join nama_kelas on nama_kelas.id_nama_kelas=kelas_mk.no_kelas_mk
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_hari on jadwal_hari.id_jadwal_hari=jadwal_kelas.id_jadwal_hari
left join jadwal_jam on jadwal_jam.id_jadwal_jam=jadwal_kelas.id_jadwal_jam
left join ruangan on ruangan.id_ruangan=jadwal_kelas.id_ruangan
left join semester on kelas_mk.id_semester=semester.id_semester
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk 
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
where kelas_mk.id_semester=$smt and pengambilan_mk.status_apv_pengambilan_mk=1
and kelas_mk.id_program_studi=".$var[0]." and pengampu_mk.pjmk_pengampu_mk=1
group by kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah,
kurikulum_mk.kredit_semester, nama_kelas.nama_kelas, jadwal_hari.nm_jadwal_hari, ruangan.nm_ruangan,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai, semester.nm_semester, semester.tahun_ajaran,semester.id_semester, program_studi.id_fakultas, kelas_mk.id_program_studi,
trim(pengguna.gelar_depan||' '||pengguna.nm_pengguna||' '||gelar_belakang)
) a
left join 
(select id_kelas_mk, round(avg(persen),2)||'%' as rerata
from 
(
select pengambilan_mk.id_kelas_mk, mahasiswa.nim_mhs,
round((sum(presensi_mkmhs.kehadiran)/count(presensi_kelas.id_presensi_kelas))*100,2) as persen
from mahasiswa
left join pengambilan_mk on mahasiswa.id_mhs=pengambilan_mk.id_mhs
left join kelas_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk
left join presensi_kelas on presensi_kelas.id_kelas_mk=pengambilan_mk.id_kelas_mk 
left join presensi_mkmhs on presensi_mkmhs.id_mhs=pengambilan_mk.id_mhs and presensi_mkmhs.id_presensi_kelas=presensi_kelas.id_presensi_kelas 
where kelas_mk.id_program_studi=".$var[0]." and pengambilan_mk.status_apv_pengambilan_mk=1
group by pengambilan_mk.id_kelas_mk, mahasiswa.nim_mhs
)
group by id_kelas_mk
) b on a.id_kelas_mk=b.id_kelas_mk
)
");
$smarty->assign('T_MK', $jaf);
}
}


$smarty->display('report-presensi.tpl');  
?>