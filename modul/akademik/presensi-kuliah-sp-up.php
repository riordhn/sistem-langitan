<?php
/*
Yudi Sulistya 30/05/2012
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$kdprodi=$user->ID_PROGRAM_STUDI;

$id_pengguna= $user->ID_PENGGUNA; 

$kdfak=$user->ID_FAKULTAS; 
$smarty->assign('FAK', $kdfak);

$smtaktif=getData("select id_semester, upper(nm_semester||'-'||THN_AKADEMIK_SEMESTER||'-'||group_semester) as semester 
from semester 
where -- trim(thn_akademik_semester||group_semester) in (select trim(thn_akademik_semester||group_semester) 
		-- from semester where status_aktif_semester='True') and 
tipe_semester !='REG'
order by thn_akademik_semester desc, nm_semester");

$smarty->assign('T_SMT', $smtaktif);

$dataprodi=getData("select id_program_studi||'-'||id_fakultas as id_program_studi,nm_jenjang||'-'||nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_fakultas=$kdfak and status_aktif_prodi = 1");
$smarty->assign('T_PRODI', $dataprodi);


if (isset($_POST['action'])=='view') {
$var = explode("-", $_POST['kdprodi']);
$smt = $_POST['smt'];
$jenis = $_POST['jenis'];
$smarty->assign('JENIS', $jenis);

if ($var[0]=='all') {
$dataprodi1=getData("select (': FAKULTAS '||nm_fakultas) as prodi from fakultas where id_fakultas=".$var[1]."");
$smarty->assign('NM_PRODI', $dataprodi1);

$jaf=getData("select kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah,
kurikulum_mk.kredit_semester, nama_kelas.nama_kelas, jadwal_hari.id_jadwal_hari, jadwal_hari.nm_jadwal_hari, ruangan.nm_ruangan,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam, sum(case when pengambilan_mk.status_apv_pengambilan_mk=1 then 1 else 0 end) as peserta,
semester.tipe_semester,semester.id_semester, semester.tahun_ajaran||'-'||semester.group_semester as tahun_ajaran, program_studi.id_fakultas, kelas_mk.id_program_studi
from kelas_mk
inner join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk
left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk=kelas_mk.id_kurikulum_mk
left join mata_kuliah on mata_kuliah.id_mata_kuliah=kurikulum_mk.id_mata_kuliah
left join nama_kelas on nama_kelas.id_nama_kelas=kelas_mk.no_kelas_mk
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_hari on jadwal_hari.id_jadwal_hari=jadwal_kelas.id_jadwal_hari
left join jadwal_jam on jadwal_jam.id_jadwal_jam=jadwal_kelas.id_jadwal_jam
left join ruangan on ruangan.id_ruangan=jadwal_kelas.id_ruangan
left join semester on pengambilan_mk.id_semester=semester.id_semester
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
where pengambilan_mk.id_semester=$smt and pengambilan_mk.status_apv_pengambilan_mk=1
and program_studi.id_fakultas=".$var[1]."
group by kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah,semester.id_semester,semester.group_semester,
kurikulum_mk.kredit_semester, nama_kelas.nama_kelas, jadwal_hari.id_jadwal_hari, jadwal_hari.nm_jadwal_hari, ruangan.nm_ruangan,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai, semester.tipe_semester, semester.tahun_ajaran, program_studi.id_fakultas, kelas_mk.id_program_studi
");
$smarty->assign('T_MK', $jaf);

} else {
$dataprodi1=getData("select (': PRODI '||nm_jenjang||'-'||nm_program_studi) as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_program_studi=".$var[0]."");
$smarty->assign('NM_PRODI', $dataprodi1);

$jaf=getData("select kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah,
kurikulum_mk.kredit_semester, nama_kelas.nama_kelas, jadwal_hari.id_jadwal_hari, jadwal_hari.nm_jadwal_hari, ruangan.nm_ruangan,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam, sum(case when pengambilan_mk.status_apv_pengambilan_mk=1 then 1 else 0 end) as peserta,
semester.tipe_semester,semester.id_semester, semester.tahun_ajaran||'-'||semester.group_semester as tahun_ajaran, program_studi.id_fakultas, kelas_mk.id_program_studi
from kelas_mk
inner join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk
left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk=kelas_mk.id_kurikulum_mk
left join mata_kuliah on mata_kuliah.id_mata_kuliah=kurikulum_mk.id_mata_kuliah
left join nama_kelas on nama_kelas.id_nama_kelas=kelas_mk.no_kelas_mk
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_hari on jadwal_hari.id_jadwal_hari=jadwal_kelas.id_jadwal_hari
left join jadwal_jam on jadwal_jam.id_jadwal_jam=jadwal_kelas.id_jadwal_jam
left join ruangan on ruangan.id_ruangan=jadwal_kelas.id_ruangan
left join semester on pengambilan_mk.id_semester=semester.id_semester
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
where pengambilan_mk.id_semester=$smt and pengambilan_mk.status_apv_pengambilan_mk=1
and kelas_mk.id_program_studi=".$var[0]."
group by kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah,semester.id_semester,semester.group_semester,
kurikulum_mk.kredit_semester, nama_kelas.nama_kelas, jadwal_hari.id_jadwal_hari, jadwal_hari.nm_jadwal_hari, ruangan.nm_ruangan,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai, semester.tipe_semester, semester.tahun_ajaran, program_studi.id_fakultas, kelas_mk.id_program_studi
");

$smarty->assign('T_MK', $jaf);
}
}
$smarty->display('presensi-kuliah-sp-up.tpl');  
?>