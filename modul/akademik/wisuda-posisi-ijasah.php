<?php
require 'config.php';

$id_pengguna= $user->ID_PENGGUNA; 
$id_fakultas = $user->ID_FAKULTAS;

$periode = $db->QueryToArray("SELECT * FROM TARIF_WISUDA WHERE id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI} ORDER BY NM_TARIF_WISUDA ASC");
$smarty->assign('periode', $periode);

if(isset($_POST['no'])){
	$no = $_POST['no'];
	$tgl = date('Y-m-d');
	for($i=1;$i<=$no;$i++){
		$id_mhs = $_POST['id_mhs'.$i];
		$posisi_ijasah = $_POST['cek'.$i];
		if($posisi_ijasah <> ''){
			if($posisi_ijasah == 1){
				$set = ", TGL_POSISI_IJASAH = to_date('$tgl', 'YYYY-MM-DD')";
			}elseif($posisi_ijasah == 2){
				$set = ", TGL_POSISI_IJASAH2 = to_date('$tgl', 'YYYY-MM-DD')";
			}elseif($posisi_ijasah == 3){
				$set = ", TGL_POSISI_IJASAH3 = to_date('$tgl', 'YYYY-MM-DD')";
			}elseif($posisi_ijasah == 4){
				$set = ", TGL_POSISI_IJASAH4 = to_date('$tgl', 'YYYY-MM-DD')";
			}elseif($posisi_ijasah == 5){
				$set = ", TGL_POSISI_IJASAH5 = to_date('$tgl', 'YYYY-MM-DD')";
			}elseif($posisi_ijasah == 6){
				$set = ", TGL_POSISI_IJASAH6 = to_date('$tgl', 'YYYY-MM-DD')";
			}elseif($posisi_ijasah == 7){
				$set = ", TGL_POSISI_IJASAH7 = to_date('$tgl', 'YYYY-MM-DD')";
			}else{
				$set = "";
			}
			
			
			$db->Query("SELECT POSISI_IJASAH FROM PENGAJUAN_WISUDA
						WHERE ID_MHS = '$id_mhs'");
			$row = $db->FetchAssoc();
			
			if($row['POSISI_IJASAH'] != $posisi_ijasah){
			$db->Query("UPDATE PENGAJUAN_WISUDA SET POSISI_IJASAH = '$posisi_ijasah'
						$set
						WHERE ID_MHS = '$id_mhs'");
			}
		}
	}
	
	
}


if(isset($_POST['periode'])){
			$where  = " AND FAKULTAS.ID_FAKULTAS = '$id_fakultas'";
		
		$pengajuan = $db->QueryToArray("SELECT NIM_MHS, NM_PENGGUNA, NM_JENJANG, NM_PROGRAM_STUDI, NM_FAKULTAS, PENGAJUAN_WISUDA.NO_IJASAH, 
								ABSTRAK_TA_CLOB, JUDUL_TA, TGL_LULUS_PENGAJUAN, PENGAJUAN_WISUDA.ID_MHS, POSISI_IJASAH, NO_SERI_KERTAS
								,TO_CHAR(TGL_LULUS_PENGAJUAN, 'DD-MM-YYYY') AS TGL_LULUS_PENGAJUAN, LAHIR_IJAZAH,
								TO_CHAR(TGL_POSISI_IJASAH, 'DD-MM-YYYY') AS TGL_POSISI_IJASAH, 
								TO_CHAR(TGL_POSISI_IJASAH2, 'DD-MM-YYYY') AS TGL_POSISI_IJASAH2,
								TO_CHAR(TGL_POSISI_IJASAH3, 'DD-MM-YYYY') AS TGL_POSISI_IJASAH3,
								TO_CHAR(TGL_POSISI_IJASAH4, 'DD-MM-YYYY') AS TGL_POSISI_IJASAH4,
								TO_CHAR(TGL_POSISI_IJASAH5, 'DD-MM-YYYY') AS TGL_POSISI_IJASAH5,
								TO_CHAR(TGL_POSISI_IJASAH6, 'DD-MM-YYYY') AS TGL_POSISI_IJASAH6,
								TO_CHAR(TGL_POSISI_IJASAH6, 'DD-MM-YYYY') AS TGL_POSISI_IJASAH7
								FROM PENGAJUAN_WISUDA
								LEFT JOIN MAHASISWA ON MAHASISWA.ID_MHS = PENGAJUAN_WISUDA.ID_MHS
								LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
								LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
								LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
								LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
								LEFT JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PENGAJUAN_WISUDA.ID_PERIODE_WISUDA
								WHERE PERIODE_WISUDA.ID_TARIF_WISUDA = '$_POST[periode]'
								$where
								ORDER BY NM_JENJANG, NM_PROGRAM_STUDI, NIM_MHS");
		$smarty->assign('pengajuan', $pengajuan);		
	

}

$smarty->assign('id_periode', $_POST['periode']);
$smarty->assign('id_fakultas', $_POST['fakultas']);
$smarty->display("wisuda-posisi-ijasah.tpl");
?>
