<?PHP
require('common.php');
require_once ('ociFunction.php');
$id_pengguna= $user->ID_PENGGUNA; 
$kdfak=$user->ID_FAKULTAS; 

if(isset($_POST['no'])){

	$no = $_POST['no'];
	for($i=2; $i<=$no; $i++){
		$id = $_POST['id'.$i];
		$nilai = $_POST['nilai'.$i];
		
		if($nilai != ''){

		$db->Query("SELECT * FROM (			
						SELECT SN.NM_STANDAR_NILAI,PN.NILAI_MAX_PERATURAN_NILAI AS NILAI_MAX,PN.NILAI_MIN_PERATURAN_NILAI AS NILAI_MIN 
									FROM PERATURAN_NILAI PN
									JOIN STANDAR_NILAI SN ON PN.ID_STANDAR_NILAI = SN.ID_STANDAR_NILAI
						) WHERE '$nilai' >=  NILAI_MIN  AND '$nilai' <= NILAI_MAX");
		$nilai_huruf = $db->FetchArray();

			if($nilai_huruf['NM_STANDAR_NILAI'] != ''){
			UpdateData("update pengambilan_mk_asing set 
						nilai_angka = '$nilai', 
						nilai_huruf = '$nilai_huruf[NM_STANDAR_NILAI]', 
						flagnilai = 1,
						id_pengguna = '$id_pengguna'
						where id_pengambilan_mk_asing = '$id'");
			}
		}
	}
}

if(isset($_POST['nim'])){

$nim_mhs_asing = $_POST['nim'];

$krs= $db->QueryToArray("select id_pengambilan_mk_asing as id_pengambilan_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,nm_jadwal_hari,nm_jadwal_jam, 
status_pengambilan_mk,case when (status_apv_pengambilan_mk=1) then 'DiSetujui' else 'Blm Disetujui' end as status, (jam_mulai||':'||menit_mulai||' - '||jam_selesai||':'||menit_selesai) as jam, nm_pengguna, nim_mhs_asing, (thn_akademik_semester||' / '||nm_semester)  as nm_semester, nilai_angka, nilai_huruf
 from pengambilan_mk_asing
 join mahasiswa_asing on pengambilan_mk_asing.id_mhs=mahasiswa_asing.id_mhs_asing
 join pengguna on pengguna.id_pengguna = mahasiswa_asing.id_pengguna
 join program_studi on program_studi.id_program_studi = mahasiswa_asing.id_program_studi
left join kelas_mk on pengambilan_mk_asing.id_kelas_mk=kelas_mk.id_kelas_mk
left join nama_kelas on kelas_mk.no_kelas_mk=id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join semester on semester.id_semester = pengambilan_mk_asing.id_semester
where pengambilan_mk_asing.id_mhs in (select id_mhs_asing from mahasiswa_asing where nim_mhs_asing='".$nim_mhs_asing."') 
and id_kelas_mk is not null and program_studi.id_fakultas = '".$kdfak."'
order by kd_mata_kuliah,nama_kelas");

$smarty->assign('krs', $krs);
}

$smarty->display('nilai-asing.tpl');
?>