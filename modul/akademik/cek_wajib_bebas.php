<?php
/*
Yudi Sulistya 04/01/2012
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$kdprodi=$user->ID_PROGRAM_STUDI;

$id_pengguna= $user->ID_PENGGUNA;
$kdfak=$user->ID_FAKULTAS; 
 
$fak=getvar("select upper(fakultas.nm_fakultas) as nm_fakultas from fakultas where id_fakultas=$kdfak");



$nmfak=$fak['NM_FAKULTAS'];
$smarty->assign('FAK', $kdfak);
$smarty->assign('NM_FAK', $nmfak);
$smarty->assign('NIMX', '');



if (isset($_POST['action'])=='view') {
$nim = $_POST['nim'];
//$mode = $_POST['mode'];



$datamhs=getData("select ': '||nim_mhs||' - '||upper(nm_pengguna) as nm_mhs from mahasiswa left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna where nim_mhs='".$nim."' and id_program_studi in (select id_program_studi from program_studi where id_fakultas='".$kdfak."')");
$smarty->assign('MHS', $datamhs);
$smarty->assign('NIMX', $nim);			
$biomhs=getData("select id_mhs, mhs.nim_mhs, upper(p.nm_pengguna) as nm_pengguna, j.nm_jenjang, ps.nm_program_studi, mhs.status_akademik_mhs
				from mahasiswa mhs 
				left join pengguna p on mhs.id_pengguna=p.id_pengguna 
				left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
				left join jenjang j on ps.id_jenjang=j.id_jenjang
				where mhs.nim_mhs='".$nim."' and mhs.id_program_studi in (select id_program_studi from program_studi where id_fakultas='".$kdfak."')");
$smarty->assign('T_MHS', $biomhs);

$cek=getvar("select id_jenjang from program_studi where id_program_studi in (select id_program_studi from mahasiswa where nim_mhs='".$nim."')");
$id_jenjang=$cek['ID_JENJANG'];


if ($id_jenjang== 2 || $id_jenjang== 3)
{
$rekap=getData("select distinct m.nm_status_mk from kurikulum_minat k
left join status_mk m on k.id_status_mk=m.id_status_mk 
left join kurikulum_prodi l on k.id_kurikulum_prodi=l.id_kurikulum_prodi
left join kurikulum_mk d on k.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where l.status_aktif=1 and l.id_program_studi in (select id_program_studi from mahasiswa where nim_mhs='".$nim."')
order by m.nm_status_mk");

} else {

/*
$rekap=getData("select nm_status_mk,sum(sks) as ttlsks from (
select nim_mhs,a.id_semester,d.id_kurikulum_mk,e.kd_mata_kuliah as kode,e.nm_mata_kuliah as nama,
d.kredit_semester as sks,nilai_huruf as nilai,
case when s.group_semester='Ganjil' then s.thn_akademik_semester||'-'||'1' else s.thn_akademik_semester||'-'||'2' end as thn, 
s.group_semester,l.tahun_kurikulum,m.nm_status_mk, 
row_number() over(partition by a.id_mhs,e.kd_mata_kuliah order by nilai_huruf,thn_akademik_semester||group_semester desc) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join mahasiswa m on a.id_mhs=m.id_mhs
left join semester s on a.id_semester=s.id_semester
left join kurikulum_prodi l on d.id_kurikulum_prodi=l.id_kurikulum_prodi 
left join kurikulum_minat k on d.id_kurikulum_mk=k.id_kurikulum_mk 
left join status_mk m on k.id_status_mk=m.id_status_mk 
where a.nilai_huruf<'E' and a.nilai_huruf is not null and nim_mhs='".$nim."' and a.status_hapus=0 and a.status_apv_pengambilan_mk=1)
where rangking=1 
group by nm_status_mk 
order by nm_status_mk");

*/
$rekap=getData("select distinct m.nm_status_mk from kurikulum_minat k
left join status_mk m on k.id_status_mk=m.id_status_mk 
left join kurikulum_prodi l on k.id_kurikulum_prodi=l.id_kurikulum_prodi
left join kurikulum_mk d on k.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where l.status_aktif=1 and l.id_program_studi in (select id_program_studi from mahasiswa where nim_mhs='".$nim."')
order by m.nm_status_mk");
}
$smarty->assign('T_REKAP', $rekap);

$jaf=getData("select kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nilai,nm_status_mk,tahun_kurikulum from 
(select kd_mata_kuliah,nm_mata_kuliah,d.kredit_semester,m.nm_status_mk,l.tahun_kurikulum from kurikulum_minat k
left join status_mk m on k.id_status_mk=m.id_status_mk 
left join kurikulum_prodi l on k.id_kurikulum_prodi=l.id_kurikulum_prodi
left join kurikulum_mk d on k.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where l.status_aktif=1 and l.id_program_studi in (select id_program_studi from mahasiswa where nim_mhs='".$nim."')
order by m.nm_status_mk)s1
left join
(select nim_mhs,kode,nama,nilai from (
select nim_mhs,e.kd_mata_kuliah as kode,e.nm_mata_kuliah as nama,
nilai_huruf as nilai,
row_number() over(partition by a.id_mhs,e.kd_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join mahasiswa m on a.id_mhs=m.id_mhs
where a.nilai_huruf<'E' and a.nilai_huruf is not null and nim_mhs='".$nim."' and a.status_hapus=0 and a.status_apv_pengambilan_mk=1)
where rangking=1) s2 on s1.kd_mata_kuliah=s2.kode
order by nm_status_mk");

$smarty->assign('T_MK', $jaf);

$ipk=getData("select sum(sks) as sks_total, sum((bobot*sks)) as bobot_total, round((sum((bobot*sks))/sum(sks)),2) as ipk
from 
(
select e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where rangking=1 and m.nim_mhs='{$nim}'
)
");
$smarty->assign('T_IPK', $ipk);

} 

$smarty->display('cek_wajib_bebas.tpl');  
?>