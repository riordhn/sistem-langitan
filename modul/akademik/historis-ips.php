<?php
require('common.php');
require_once ('ociFunction.php');
$kd_fak= $user->ID_FAKULTAS;
$id_pengguna= $user->ID_PENGGUNA; 

$nim =$_POST['nim'];
$smarty->assign('nim', $nim);
$datamhs=getvar("select nim_mhs,nm_pengguna,nm_jenjang||'-'||nm_program_studi as prodi,thn_angkatan_mhs from mahasiswa
				 join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
				 join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
				 join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
				 where nim_mhs='$nim' and id_fakultas=$kd_fak");
$smarty->assign('datamhs', $datamhs);

$var="select distinct * from (
				select group_semester||thn_akademik_semester as smt,
				case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun 
				from semester where thn_akademik_semester >= '$datamhs[THN_ANGKATAN_MHS]')
				order by tahun";

$result = $db->Query($var)or die("salah kueri ipsipk");
$index=1;
	while($r = $db->FetchRow()) {
			
			$dataipsipk =getData("select s1.id_mhs,s1.nim_mhs,upper(s1.nm_pengguna) as NM_PENGGUNA,tahun,
								coalesce(sks_sem,0) as sks_sem,round(coalesce(ips,0),2)as ips,
								coalesce(SKS_TOTAL_MHS,0) as SKS_TOTAL_MHS,round(coalesce(IPK_MHS,0),2)as IPK_MHS
								from
								(select id_mhs,nim_mhs,nm_pengguna,tahun,case when sum(bobot*kredit_semester)=0 then 0 else 
								round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips,
								sum(kredit_semester) as sks_sem 
								from 
								(select id_mhs,nim_mhs,nm_pengguna,id_kurikulum_mk,id_fakultas,kredit_semester,tahun, sksreal,min(nilai_huruf) as nilai, max(bobot) as bobot from
								(select a.id_mhs,m.nim_mhs,pg.nm_pengguna,ps.id_fakultas,a.id_kurikulum_mk,thn_akademik_semester||'-'||group_semester as tahun, 
								case when (a.nilai_huruf = 'E' or a.nilai_huruf is null) 
								and d.status_mkta in (1,2) then 0
								else d.kredit_semester end as kredit_semester,d.kredit_semester as sksreal,
								case when a.nilai_huruf is null then 'E' else a.nilai_huruf end as nilai_huruf,
								case when e.nilai_standar_nilai is null then 0 else e.nilai_standar_nilai end as bobot
								from pengambilan_mk a
								left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
								left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
								left join mahasiswa m on a.id_mhs=m.id_mhs
								left join pengguna pg on m.id_pengguna=pg.id_pengguna 
								left join program_studi ps on m.id_program_studi=ps.id_program_studi
								left join semester s on a.id_semester=s.id_semester
								where group_semester||thn_akademik_semester in ('".$r[0]."')
								and a.status_apv_pengambilan_mk='1' and m.nim_mhs='$nim' and a.status_hapus=0 
								and a.status_pengambilan_mk !=0)
								group by id_mhs, nim_mhs, nm_pengguna, id_kurikulum_mk, id_fakultas, kredit_semester, tahun, sksreal
								)
								group by id_mhs, nim_mhs, nm_pengguna, tahun)s1
								left join
								(select id_mhs, sum(kredit_semester) as SKS_TOTAL_MHS, 
								round((sum((kredit_semester * nilai_standar_nilai)) / sum(kredit_semester)), 2) as IPK_MHS
								from 
								(select a.id_mhs, c.kd_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai,
								case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,
								row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by nilai_huruf) rangking
								from pengambilan_mk a
								join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
								join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
								join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
								join semester smt on a.id_semester=smt.id_semester
								join mahasiswa m on a.id_mhs=m.id_mhs
								where a.status_apv_pengambilan_mk = 1 and (a.nilai_huruf<>'E' or a.nilai_huruf<>null) 
								and m.nim_mhs='$nim' and a.status_hapus=0 
								and a.status_pengambilan_mk !=0)
								where tahun<='".$r[1]."' and rangking=1
								group by id_mhs ) s2 on s1.id_mhs=s2.id_mhs");
		
		
		$smarty->assign('NILAI'.$index, $dataipsipk);
			
		$index=$index+1;
		
	}
	$smarty->assign('index', $index);

/*	
$nilai=getData("select id_mhs,tahun,case when sum(bobot*kredit_semester)=0 then 0 else 
				round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips,
				sum(kredit_semester)as sks_sem 
				from 
				(select id_mhs,tahun,id_kurikulum_mk,kredit_semester, sksreal,min(nilai_huruf) as nilai, max(bobot) as bobot from
				(select a.id_mhs,ps.id_fakultas,a.id_kurikulum_mk,
				case when (group_semester='Ganjil' and tipe_semester in ('UP','REG','RD'))then thn_akademik_semester||'-'||'Ganjil' 
				when (group_semester='Genap' and tipe_semester in ('UP','REG','RD')) then thn_akademik_semester||'-'||'Genap'
				when (group_semester='Ganjil' and tipe_semester in ('SP')) then thn_akademik_semester||'-Ganjil-'||'Pendek'
				when (group_semester='Genap' and tipe_semester in ('SP')) then thn_akademik_semester||'-Genap-'||'Pendek' else
				thn_akademik_semester||'-NON SMT' end as tahun,
				case when (a.nilai_huruf = 'E' or a.nilai_huruf is null) 
				and d.status_mkta in (1,2) then 0
				else d.kredit_semester end as kredit_semester,d.kredit_semester as sksreal,
				case when a.nilai_huruf is null then 'E' else a.nilai_huruf end as nilai_huruf,
				case when e.nilai_standar_nilai is null then 0 else e.nilai_standar_nilai end as bobot
				from pengambilan_mk a
				left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
				left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
				left join mahasiswa m on a.id_mhs=m.id_mhs
				left join program_studi ps on m.id_program_studi=ps.id_program_studi
				left join semester s on a.id_semester=s.id_semester
				where a.status_apv_pengambilan_mk='1' and a.status_hapus=0 and m.nim_mhs='$nim' and ps.id_fakultas=$kd_fak
				and a.status_pengambilan_mk !=0 
				)group by id_mhs, tahun, id_kurikulum_mk, kredit_semester, sksreal
				)
				group by id_mhs,tahun
				order by tahun ");
	
$smarty->assign('NILAI', $nilai);
*/
$smarty->display('historis-ips.tpl');

?>
