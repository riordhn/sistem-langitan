<?php
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');

$id_pengguna= $user->ID_PENGGUNA;
$username= $user->USERNAME;
$waktu= date('Y-m-d H:i:s');

$kdfak=$user->ID_FAKULTAS;

//Fakultas Lain
if($kdfak!=11){
	echo 'Anda tidak memiliki hak akses';
} else {

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True'");

$status = isset($_REQUEST['action'])? $_REQUEST['action'] : 'tampil';

switch($status) {

case 'updateview':
 // pilih
$smarty->assign('disp1','none');
$smarty->assign('disp2','block');
$smarty->assign('disp3','none');

//Yudi Sulistya, encrypting
$id_klsmk = base64_decode($_GET['id_klsmk']);
$id_jk = base64_decode($_GET['id_jk']);

$detail='';
$smarty->assign('T_DETAIL', $detail);

$ruang=getData("select id_ruangan,nm_ruangan||' - '||kapasitas_ruangan as ruang from ruangan
left join gedung on gedung.id_gedung=ruangan.id_gedung
left join gedung_kuliah on gedung.id_gedung=gedung_kuliah.id_gedung
where gedung.id_fakultas='".$kdfak."' order by nm_ruangan");
$smarty->assign('T_RUANG', $ruang);

$jaf1=getData("select kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nm_jadwal_hari,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as nm_jadwal_jam,
ruangan.id_ruangan,nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kapasitas_ruangan
from kelas_mk
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join semester on kelas_mk.id_semester=semester.id_semester
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
where program_studi.id_fakultas=$kdfak and kelas_mk.id_kelas_mk=$id_klsmk and jadwal_kelas.id_jadwal_kelas=$id_jk");
$smarty->assign('TJAF', $jaf1);

$jaf=getData("select distinct jadwal_kelas.id_jadwal_kelas,kelas_mk.id_kelas_mk,kelas_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
nama_kelas,kapasitas_kelas_mk,terisi_kelas_mk,nm_ruangan||' - '||kapasitas_ruangan as kpst_ruang,nm_jadwal_hari,kapasitas_ruangan,
sum(case when status_pengambilan_mk>=1 then 1 else 0 end) as kls_terisi,
sum(case when status_pengambilan_mk=2 then 1 else 0 end) as kprs,
sum(case when status_pengambilan_mk=1 or status_pengambilan_mk>2 then 1 else 0 end) as krs,
kelas_mk.status,nm_jenjang||'-'||nm_singkat_prodi as prodi,nm_kelas_mk as keterangan
from kelas_mk
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and kelas_mk.id_semester=pengambilan_mk.id_semester
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
where id_fakultas=$kdfak and kelas_mk.id_semester='".$smtaktif['ID_SEMESTER']."'
group by jadwal_kelas.id_jadwal_kelas, kelas_mk.id_kelas_mk, kelas_mk.id_kurikulum_mk, kd_mata_kuliah,
nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas, kapasitas_kelas_mk, nm_jadwal_hari,
terisi_kelas_mk, kelas_mk.status, nm_singkat_prodi, nm_jenjang, nm_ruangan, kapasitas_ruangan,nm_kelas_mk
");
$smarty->assign('T_MK', $jaf);
break;

case 'update1':

$kap_kelas = $_POST['kap_kelas'];
$kelas_mk = $_POST['kelas_mk'];
$ren_kul = $_POST['ren_kul'];
$id_kelas_mk = $_POST['id_kelas_mk'];
$id_jk = $_POST['id_jk'];
$idruang = $_POST['ruangan'];
//$kap_ruang = $_POST['kap_ruang'];

$kap_ruangan=getvar("select kapasitas_ruangan from ruangan where id_ruangan='".$idruang."'");

if ($kap_kelas > $kap_ruangan['KAPASITAS_RUANGAN']) {
	echo '<script>alert ("Kelas tidak cukup");</script>';
} else {
	UpdateData("update kelas_mk set kapasitas_kelas_mk=$kap_kelas, jumlah_pertemuan_kelas_mk=$ren_kul, nm_kelas_mk='Updated by: ".$username." on ".$waktu."' where id_kelas_mk=$id_kelas_mk");
	UpdateData("update jadwal_kelas set id_ruangan=$idruang where id_kelas_mk=$id_kelas_mk and id_jadwal_kelas=$id_jk");
	
	echo '<script>location.href="#aktivitas-monitor!monitor-kelas-psikologi.php";</script>';
}
break;

case 'detail':
 // pilih
$smarty->assign('disp1','none');
$smarty->assign('disp2','none');
$smarty->assign('disp3','block');

//Yudi Sulistya, encrypting
$id_mk = base64_decode($_GET['id_kelas_mk']);

$info=getData("select kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas.nama_kelas,
trim(wm_concat('<li>'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||' '||(case when pengampu_mk.pjmk_pengampu_mk=1 then '(PJMA)' when pengampu_mk.pjmk_pengampu_mk=0 then '(ANGGOTA)' else '' end))) as tim
from kelas_mk
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
where kelas_mk.id_semester='".$smtaktif['ID_SEMESTER']."' and kelas_mk.id_kelas_mk=$id_mk
group by kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas.nama_kelas");
$smarty->assign('T_INFO', $info);

$detail=getData("select distinct pengambilan_mk.id_mhs,nim_mhs,nm_pengguna,nm_program_studi,
case when status_apv_pengambilan_mk=1 then 'Disetujui' else 'Belum Disetujui' end as status
from pengambilan_mk
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
where pengambilan_mk.id_semester='".$smtaktif['ID_SEMESTER']."' and id_kelas_mk=$id_mk
");
$smarty->assign('T_DETAIL', $detail);

$jaf=getData("select distinct jadwal_kelas.id_jadwal_kelas,kelas_mk.id_kelas_mk,kelas_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
nama_kelas,kapasitas_kelas_mk,terisi_kelas_mk,nm_ruangan||' - '||kapasitas_ruangan as kpst_ruang,nm_jadwal_hari,kapasitas_ruangan,
sum(case when status_pengambilan_mk>=1 then 1 else 0 end) as kls_terisi,
sum(case when status_pengambilan_mk=2 then 1 else 0 end) as kprs,
sum(case when status_pengambilan_mk=1 or status_pengambilan_mk>2 then 1 else 0 end) as krs,
kelas_mk.status,nm_jenjang||'-'||nm_singkat_prodi as prodi,nm_kelas_mk as keterangan
from kelas_mk
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and kelas_mk.id_semester=pengambilan_mk.id_semester
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
where id_fakultas=$kdfak and kelas_mk.id_semester='".$smtaktif['ID_SEMESTER']."'
group by jadwal_kelas.id_jadwal_kelas, kelas_mk.id_kelas_mk, kelas_mk.id_kurikulum_mk, kd_mata_kuliah,
nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas, kapasitas_kelas_mk, nm_jadwal_hari,
terisi_kelas_mk, kelas_mk.status, nm_singkat_prodi, nm_jenjang, nm_ruangan, kapasitas_ruangan,nm_kelas_mk
");
$smarty->assign('T_MK', $jaf);
break;

case 'tampil':

$detail='';
$smarty->assign('T_DETAIL', $detail);

$jaf=getData("select distinct jadwal_kelas.id_jadwal_kelas,kelas_mk.id_kelas_mk,kelas_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
nama_kelas,kapasitas_kelas_mk,terisi_kelas_mk,nm_ruangan||' - '||kapasitas_ruangan as kpst_ruang,nm_jadwal_hari,kapasitas_ruangan,
sum(case when status_pengambilan_mk>=1 then 1 else 0 end) as kls_terisi,
sum(case when status_pengambilan_mk=2 then 1 else 0 end) as kprs,
sum(case when status_pengambilan_mk=1 or status_pengambilan_mk>2 then 1 else 0 end) as krs,
kelas_mk.status,nm_jenjang||'-'||nm_singkat_prodi as prodi,nm_kelas_mk as keterangan
from kelas_mk
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and kelas_mk.id_semester=pengambilan_mk.id_semester
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
where id_fakultas=$kdfak and kelas_mk.id_semester='".$smtaktif['ID_SEMESTER']."'
group by jadwal_kelas.id_jadwal_kelas, kelas_mk.id_kelas_mk, kelas_mk.id_kurikulum_mk, kd_mata_kuliah,
nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas, kapasitas_kelas_mk, nm_jadwal_hari,
terisi_kelas_mk, kelas_mk.status, nm_singkat_prodi, nm_jenjang, nm_ruangan, kapasitas_ruangan,nm_kelas_mk
");
$smarty->assign('T_MK', $jaf);
break;

}

$smarty->display('monitor-kelas-psikologi.tpl');

}
?>
