<?php
//Yudi Sulistya, 30/04/2012

require('common.php');
require_once ('ociFunction.php');

error_reporting (E_ALL & ~E_NOTICE);

$id_pengguna= $user->ID_PENGGUNA;
$kdfak=$user->ID_FAKULTAS;

//Fakultas Lain
if($kdfak!=11){
	echo 'Anda tidak memiliki hak akses';
} else {

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True'");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);

$smt=getData("select id_semester, tahun_ajaran||'-'||nm_semester as smt from semester where nm_semester in ('Ganjil', 'Genap')order by thn_akademik_semester desc,nm_semester desc");
$smarty->assign('T_ST', $smt);

$smt1 = isSet($_POST['smt']) ? $_POST['smt'] : $smtaktif['ID_SEMESTER'];

if ($smt1!='') {
$smarty->assign('SMT',$smt);

$jaf=getData("select kelas_mk.id_kelas_mk,kelas_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
nama_kelas,kapasitas_kelas_mk,terisi_kelas_mk,(select count(status_apv_pengambilan_mk) from pengambilan_mk
where kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and status_apv_pengambilan_mk=1)as kls_terisi,kelas_mk.status,nm_jenjang||'-'||nm_singkat_prodi as prodiasal,
jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam,
(select count(id_presensi_kelas) from presensi_kelas where kelas_mk.id_kelas_mk=presensi_kelas.id_kelas_mk) as tm
from kelas_mk
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and kelas_mk.id_semester=pengambilan_mk.id_semester
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join presensi_kelas on kelas_mk.id_kelas_mk=presensi_kelas.id_kelas_mk
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester=$smt1
group by kelas_mk.id_kelas_mk,kelas_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
nama_kelas,kapasitas_kelas_mk,terisi_kelas_mk,kelas_mk.status,
jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai,nm_jenjang||'-'||nm_singkat_prodi
");
}
$smarty->assign('T_MK', $jaf);

$smarty->display('susulan-psikologi.tpl');

}
?>
