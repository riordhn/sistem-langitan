<?php
require('common.php');
require_once ('ociFunction.php');

$smarty->assign('disp1', 'block');
$smarty->assign('disp2', 'none');

$kdfak = $user->ID_FAKULTAS;

//Yudi Sulistya, encrypting
$kdprodi = base64_decode($_REQUEST['idprodi']);

// Yudi Sulistya, 25-07-2014
// Hanya prodi di fakultas user saja yang bisa di eksekusi
$idprodi = getvar("select count(*) as boleh from program_studi where id_program_studi=$kdprodi and id_fakultas=$kdfak");

if ($idprodi['BOLEH'] == 0)
{
	echo 'Anda tidak memiliki hak akses di prodi ini';
}
else
{

	$smarty->assign('kodeprodi', $kdprodi);

	$nmprodi = getvar(
		"select nm_jenjang||' - '||nm_program_studi as prodi from program_studi
		left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
		where id_program_studi=$kdprodi");
	$smarty->assign('disprodi', $nmprodi['PRODI']);

	if ($kdfak == 11)  // Psikologi
	{
		$smtaktif = getvar("select id_semester from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND status_aktif_semester='True'");
		$smarty->assign('smtaktif', $smtaktif['ID_SEMESTER']);

		$idkur = getvar(
			"select id_kurikulum_prodi from kurikulum_prodi
			left join kurikulum on kurikulum_prodi.id_kurikulum=kurikulum.id_kurikulum
			where kurikulum_prodi.id_program_studi=$kdprodi");
		$smarty->assign('idkur', $idkur['ID_KURIKULUM_PRODI']);

		$smt = getData(
			"select id_semester, tahun_ajaran||' - '||nm_semester as smt from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester in (
			select * from (select distinct thn_akademik_semester
			from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester<=EXTRACT(YEAR FROM sysdate) order by thn_akademik_semester desc)
			where rownum<=3) and nm_semester in ('Ganjil', 'Genap')
			order by thn_akademik_semester desc, nm_semester desc");
		$smarty->assign('T_ST', $smt);

		$datakur = getData("select id_kurikulum_prodi,nm_kurikulum||'-'||tahun_kurikulum as nama from kurikulum_prodi
			left join kurikulum on kurikulum_prodi.id_kurikulum=kurikulum.id_kurikulum
			where kurikulum_prodi.id_program_studi=$kdprodi
			order by id_kurikulum_prodi desc");
		$smarty->assign('T_KUR', $datakur);

	}
	else  // Fakultas Lain
	{
		$smtaktif = getvar("select id_semester from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND status_aktif_semester='True'");
		$smarty->assign('smtaktif', $smtaktif['ID_SEMESTER']);

		$idkur = getvar(
			"select id_kurikulum_prodi from kurikulum_prodi
			left join kurikulum on kurikulum_prodi.id_kurikulum=kurikulum.id_kurikulum
			where kurikulum_prodi.id_program_studi=$kdprodi");
		$smarty->assign('idkur', $idkur['ID_KURIKULUM_PRODI']);

		$smt = getData("select id_semester, tahun_ajaran||' - '||nm_semester as smt from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' order by thn_akademik_semester desc,nm_semester");
		$smarty->assign('T_ST', $smt);

		$datakur = getData(
			"select id_kurikulum_prodi,nm_kurikulum||'-'||tahun_kurikulum as nama  from kurikulum_prodi
			left join kurikulum on kurikulum_prodi.id_kurikulum=kurikulum.id_kurikulum
			where kurikulum_prodi.id_program_studi=$kdprodi");
		$smarty->assign('T_KUR', $datakur);
	}

	$status = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'tampil';
	//echo $status;

	switch ($status)
	{
		case 'add':
			// pilih
			$smarty->assign('disp1', 'none');
			$smarty->assign('disp2', 'block');

			//$smt1= $_GET['smt'];
			//Yudi Sulistya, encrypting
			$kdprodi = base64_decode($_GET['idprodi']);
			$id_mk = base64_decode($_GET['id_mk']);
			$status = base64_decode($_GET['status']);

			tambahdata("krs_prodi", "id_kelas_mk,id_semester,id_program_studi,status", "'$id_mk','" . $smtaktif['ID_SEMESTER'] . "','$kdprodi','$status'");
			//if (!isFindData("jadwal_kelas","id_kelas_mk=$id_mk")) {tambahdata("jadwal_kelas","id_kelas_mk","$id_mk");}

			$smt1 = isset($_POST['smt']) ? $_POST['smt'] : $smtaktif['ID_SEMESTER'];
			$jaf = getData(
				"select distinct id_krs_prodi,kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah, 
				kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester, 
				jadwal_kelas.id_jadwal_hari,nm_jadwal_hari, jadwal_kelas.id_jadwal_jam, nm_jadwal_jam, 
				jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam, ruangan.id_ruangan,nm_ruangan, 
				nm_jenjang||'-'||nm_program_studi as prodiasal, 
				nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status 
				from krs_prodi 
				left join kelas_mk on krs_prodi.id_kelas_mk=kelas_mk.id_kelas_mk
				left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
				left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
				left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
				left join semester on kelas_mk.id_semester=semester.id_semester 
				left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk 
				left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
				left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
				left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam 
				left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
				left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
				where krs_prodi.id_program_studi=$kdprodi and krs_prodi.id_semester=$smt1");
			$smarty->assign('T_MK', $jaf);

			break;

		case 'del':
			// pilih
			$smarty->assign('disp1', 'block');
			$smarty->assign('disp2', 'none');

			//Yudi Sulistya, encrypting
			$id_krs_prodi = base64_decode($_GET['id_krs_prodi']);
			$kdprodi = base64_decode($_GET['idprodi']);
			//$smt1= $_GET['smt'];

			deleteData("delete from krs_prodi where id_krs_prodi='$id_krs_prodi'");

			$smt1 = isset($_POST['smt']) ? $_POST['smt'] : $smtaktif['ID_SEMESTER'];
			$jaf = getData(
				"select distinct id_krs_prodi,kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah, 
				kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester, 
				jadwal_kelas.id_jadwal_hari,nm_jadwal_hari, jadwal_kelas.id_jadwal_jam, nm_jadwal_jam, 
				jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam, ruangan.id_ruangan,nm_ruangan, 
				nm_jenjang||'-'||nm_program_studi as prodiasal, 
				nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status 
				from krs_prodi 
				left join kelas_mk on krs_prodi.id_kelas_mk=kelas_mk.id_kelas_mk
				left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
				left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
				left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
				left join semester on kelas_mk.id_semester=semester.id_semester 
				left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk 
				left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
				left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
				left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam 
				left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
				left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
				where krs_prodi.id_program_studi=$kdprodi and krs_prodi.id_semester=$smt1");
			$smarty->assign('T_MK', $jaf);

			break;

		case 'penawaran':
			$smt = $_POST['smt'];
			$id_kur = $_POST['thkur'];

			$smarty->assign('disp1', 'none');
			$smarty->assign('disp2', 'block');

			$smarty->assign('id_smt', $smt);


			//$smarty->display('usulan-mk.tpl');
			break;

		case 'tampil':
			//echo "aa";
			//echo $smtaktif['ID_SEMESTER'];
			//$smt1= $_POST['smt'];
			$smt1 = isset($_POST['smt']) ? $_POST['smt'] : $smtaktif['ID_SEMESTER'];
			// if ($smt1=='') { $smt1=$smtaktif['ID_SEMESTER'];}
			//echo $smt1;
			//$id_kur1= $_POST['thkur'];
			$id_kur1 = isset($_POST['thkur']) ? $_POST['thkur'] : '0';
			if ($smt1 != '')
			{
				$smarty->assign('SMT', $smt);
				//$smarty->assign('KUR', $id_kur);

				$jaf = getData(
					"select distinct id_krs_prodi,kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah, 
					kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester, 
					jadwal_kelas.id_jadwal_hari,nm_jadwal_hari, jadwal_kelas.id_jadwal_jam, nm_jadwal_jam, 
					jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam, ruangan.id_ruangan,nm_ruangan, 
					nm_jenjang||'-'||nm_program_studi as prodiasal, 
					nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status 
					from krs_prodi 
					left join kelas_mk on krs_prodi.id_kelas_mk=kelas_mk.id_kelas_mk
					left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
					left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
					left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
					left join semester on kelas_mk.id_semester=semester.id_semester 
					left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk 
					left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
					left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
					left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam 
					left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
					left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
					where krs_prodi.id_program_studi=$kdprodi and krs_prodi.id_semester=$smt1");
				$smarty->assign('T_MK', $jaf);
				//$smarty->assign('T_JAF', $jaf); 
				//$smarty->display('usulan-mk.tpl');
			}

			break;
	}


	if ($kdfak == 11)  // Khusus Fakultas Psikologi
	{
		$tawar = getData(
			"select kelas_mk.id_kelas_mk,kelas_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nama_kelas,
			nm_jenjang||'-'||nm_program_studi as prodiasal,program_studi.id_program_studi,kelas_mk.status from kelas_mk
			left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
			left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
			left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
			left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
			left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
			where id_fakultas=$kdfak and id_semester='" . $smtaktif['ID_SEMESTER'] . "' and mata_kuliah.kd_mata_kuliah in (select kd_mata_kuliah from kurikulum_mk 
			left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
			where kurikulum_mk.id_program_studi=$kdprodi)
			and id_kelas_mk not in (select id_kelas_mk from krs_prodi where id_program_studi=$kdprodi and id_semester='" . $smtaktif['ID_SEMESTER'] . "')");
		$smarty->assign('TAWAR', $tawar);


	}
	else  //Fakultas Lain
	{
		$smt1 = isset($_POST['smt']) ? $_POST['smt'] : $smtaktif['ID_SEMESTER'];

		$tawar = getData(
			"select kelas_mk.id_kelas_mk,kelas_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nama_kelas,
			nm_jenjang||'-'||nm_program_studi as prodiasal,program_studi.id_program_studi,kelas_mk.status from kelas_mk
			left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
			left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
			left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
			left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
			left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
			where id_fakultas=$kdfak and id_semester='" . $smt1 . "' and mata_kuliah.kd_mata_kuliah in (select kd_mata_kuliah from kurikulum_mk 
			left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
			where kurikulum_mk.id_program_studi=$kdprodi)
			and id_kelas_mk not in (select id_kelas_mk from krs_prodi where id_program_studi=$kdprodi and id_semester='" . $smt1 . "')");
		$smarty->assign('TAWAR', $tawar);
	}


	if ($kdfak == 11)  //Khusus Fakultas Psikologi
	{
		$smarty->display('krs-tawar-psikologi.tpl');
		//$smarty->display('krs-tawar.tpl');
	}
	else  //Fakultas Lain
	{
		$smarty->display('krs-tawar.tpl');
	}
}