<?php
/*
Yudi Sulistya 28/12/2011
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$kdprodi=$user->ID_PROGRAM_STUDI;

$id_pengguna= $user->ID_PENGGUNA;

$kdfak=$user->ID_FAKULTAS;
$smarty->assign('FAK', $kdfak);

$thnsmt=getData("select case when group_semester='Ganjil' then id_semester||'-'||thn_akademik_semester||1 else id_semester||'-'||thn_akademik_semester||2 end as id_semester, tahun_ajaran||' - '||nm_semester as thn_smt from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester in (
select * from (select distinct thn_akademik_semester
from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester<=EXTRACT(YEAR FROM sysdate) order by thn_akademik_semester desc)
where rownum<=10) and nm_semester in ('Ganjil', 'Genap')
order by thn_akademik_semester desc, nm_semester desc");
$smarty->assign('T_THNSMT', $thnsmt);

$dataprodi=getData("select id_program_studi||'-'||id_fakultas as id_program_studi,nm_jenjang||'-'||nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_fakultas=$kdfak and status_aktif_prodi = 1");
$smarty->assign('T_PRODI', $dataprodi);


if (isset($_POST['action'])=='view') {
$var = explode("-", $_POST['kdprodi']);
$var1 = explode("-", $_POST['kdthnsmt']);

$smt = $var1[0];
$smarty->assign('KD_SMT', $smt);

$thn = $var1[1];
$smarty->assign('KD_THN', $thn);

$kd_prodi=$var[0];
$smarty->assign('KD_PRODI', $kd_prodi);

$kd_fak=$var[1];
$smarty->assign('KD_FAK', $kd_fak);

$datathnsmt=getData("select '(TAHUN '||tahun_ajaran||' - SEMESTER '||nm_semester||')' as thn_smt from semester where id_semester=$smt");
$smarty->assign('THNSMT', $datathnsmt);

if ($var[0]=='all') {
$dataprodi1=getData("select (': FAKULTAS '||nm_fakultas) as prodi from fakultas where id_fakultas=".$var[1]."");
$smarty->assign('NM_PRODI', $dataprodi1);

$jaf=getData("
select nm_jenjang, nm_program_studi,
sum(A) as jumlah_A,
round(sum(A)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_A,
sum(AB) as jumlah_AB,
round(sum(AB)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_AB,
sum(B) as jumlah_B,
round(sum(B)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_B,
sum(BC) as jumlah_BC,
round(sum(BC)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_BC,
sum(C) as jumlah_C,
round(sum(C)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_C,
sum(D) as jumlah_D,
round(sum(D)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_D,
sum(E) as jumlah_E,
round(sum(E)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_E,
sum(F) as jumlah_F,
round(sum(F)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_F
from
(
	select nm_jenjang, nm_program_studi,
	case when ipk_mhs_status<2.00 then 1 else 0 end as A,
	case when ipk_mhs_status between 2.00 and 2.49 then 1 else 0 end as AB,
	case when ipk_mhs_status between 2.50 and 2.74 then 1 else 0 end as B,
	case when ipk_mhs_status between 2.75 and 2.99 then 1 else 0 end as BC,
	case when ipk_mhs_status between 3.00 and 3.24 then 1 else 0 end as C,
	case when ipk_mhs_status between 3.25 and 3.49 then 1 else 0 end as D,
	case when ipk_mhs_status between 3.50 and 3.74 then 1 else 0 end as E,
	case when ipk_mhs_status>=3.75 then 1 else 0 end as F
	from 
	(
		select nm_jenjang, nm_program_studi, id_mhs, round((sum((kredit_semester * nilai_standar_nilai)) / sum(kredit_semester)), 2) as ipk_mhs_status
		from 
		(
			select j.nm_jenjang,ps.nm_program_studi,a.id_mhs,d.kredit_semester, e.nilai_standar_nilai,
			case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,
			row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by nilai_huruf) rangking
			from pengambilan_mk a
			join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
			join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
			join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
			join semester smt on a.id_semester=smt.id_semester
			join mahasiswa m on a.id_mhs=m.id_mhs
			left join program_studi ps on ps.id_program_studi=m.id_program_studi
			left join jenjang j on j.id_jenjang=ps.id_jenjang
			where a.status_apv_pengambilan_mk = 1 and (a.nilai_huruf<>'E' or a.nilai_huruf<>null) 
			and ps.id_fakultas = ".$var[1]." and a.status_hapus = 0 
			and a.status_pengambilan_mk != 0 and m.status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role = 3 and status_aktif = 1)
		)
		where tahun<='".$thn."' and rangking = 1
		group by nm_jenjang, nm_program_studi, id_mhs
	)
)
group by nm_jenjang, nm_program_studi
order by nm_jenjang, nm_program_studi
");
$smarty->assign('T_MK', $jaf);

$agk=getdata("
select thn_angkatan_mhs,
sum(A) as jumlah_A,
round(sum(A)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_A,
sum(AB) as jumlah_AB,
round(sum(AB)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_AB,
sum(B) as jumlah_B,
round(sum(B)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_B,
sum(BC) as jumlah_BC,
round(sum(BC)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_BC,
sum(C) as jumlah_C,
round(sum(C)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_C,
sum(D) as jumlah_D,
round(sum(D)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_D,
sum(E) as jumlah_E,
round(sum(E)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_E,
sum(F) as jumlah_F,
round(sum(F)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_F
from
(
	select thn_angkatan_mhs,
	case when ipk_mhs_status<2.00 then 1 else 0 end as A,
	case when ipk_mhs_status between 2.00 and 2.49 then 1 else 0 end as AB,
	case when ipk_mhs_status between 2.50 and 2.74 then 1 else 0 end as B,
	case when ipk_mhs_status between 2.75 and 2.99 then 1 else 0 end as BC,
	case when ipk_mhs_status between 3.00 and 3.24 then 1 else 0 end as C,
	case when ipk_mhs_status between 3.25 and 3.49 then 1 else 0 end as D,
	case when ipk_mhs_status between 3.50 and 3.74 then 1 else 0 end as E,
	case when ipk_mhs_status>=3.75 then 1 else 0 end as F
	from 
	(
		select thn_angkatan_mhs, id_mhs, round((sum((kredit_semester * nilai_standar_nilai)) / sum(kredit_semester)), 2) as ipk_mhs_status
		from 
		(
			select m.thn_angkatan_mhs,a.id_mhs,d.kredit_semester, e.nilai_standar_nilai,
			case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,
			row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by nilai_huruf) rangking
			from pengambilan_mk a
			join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
			join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
			join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
			join semester smt on a.id_semester=smt.id_semester
			join mahasiswa m on a.id_mhs=m.id_mhs
			left join program_studi ps on ps.id_program_studi=m.id_program_studi
			left join jenjang j on j.id_jenjang=ps.id_jenjang
			where a.status_apv_pengambilan_mk = 1 and (a.nilai_huruf<>'E' or a.nilai_huruf<>null) 
			and ps.id_fakultas = ".$var[1]." and a.status_hapus = 0 
			and a.status_pengambilan_mk != 0 and m.status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role = 3 and status_aktif = 1)
		)
		where tahun<='".$thn."' and rangking = 1
		group by thn_angkatan_mhs, id_mhs
	)
)
group by thn_angkatan_mhs
order by thn_angkatan_mhs desc
");
$smarty->assign('T_AGK', $agk);

} else {
$dataprodi1=getData("select (': PRODI '||nm_jenjang||'-'||nm_program_studi) as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_program_studi=".$var[0]."");
$smarty->assign('NM_PRODI', $dataprodi1);

$jaf=getData("
select nm_jenjang, nm_program_studi,
sum(A) as jumlah_A,
round(sum(A)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_A,
sum(AB) as jumlah_AB,
round(sum(AB)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_AB,
sum(B) as jumlah_B,
round(sum(B)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_B,
sum(BC) as jumlah_BC,
round(sum(BC)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_BC,
sum(C) as jumlah_C,
round(sum(C)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_C,
sum(D) as jumlah_D,
round(sum(D)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_D,
sum(E) as jumlah_E,
round(sum(E)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_E,
sum(F) as jumlah_F,
round(sum(F)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_F
from
(
	select nm_jenjang, nm_program_studi,
	case when ipk_mhs_status<2.00 then 1 else 0 end as A,
	case when ipk_mhs_status between 2.00 and 2.49 then 1 else 0 end as AB,
	case when ipk_mhs_status between 2.50 and 2.74 then 1 else 0 end as B,
	case when ipk_mhs_status between 2.75 and 2.99 then 1 else 0 end as BC,
	case when ipk_mhs_status between 3.00 and 3.24 then 1 else 0 end as C,
	case when ipk_mhs_status between 3.25 and 3.49 then 1 else 0 end as D,
	case when ipk_mhs_status between 3.50 and 3.74 then 1 else 0 end as E,
	case when ipk_mhs_status>=3.75 then 1 else 0 end as F
	from 
	(
		select nm_jenjang, nm_program_studi, id_mhs, round((sum((kredit_semester * nilai_standar_nilai)) / sum(kredit_semester)), 2) as ipk_mhs_status
		from 
		(
			select j.nm_jenjang,ps.nm_program_studi,a.id_mhs,d.kredit_semester, e.nilai_standar_nilai,
			case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,
			row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by nilai_huruf) rangking
			from pengambilan_mk a
			join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
			join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
			join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
			join semester smt on a.id_semester=smt.id_semester
			join mahasiswa m on a.id_mhs=m.id_mhs
			left join program_studi ps on ps.id_program_studi=m.id_program_studi
			left join jenjang j on j.id_jenjang=ps.id_jenjang
			where a.status_apv_pengambilan_mk = 1 and (a.nilai_huruf<>'E' or a.nilai_huruf<>null) 
			and ps.id_program_studi = ".$var[0]." and a.status_hapus = 0 
			and a.status_pengambilan_mk != 0 and m.status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role = 3 and status_aktif = 1)
		)
		where tahun<='".$thn."' and rangking = 1
		group by nm_jenjang, nm_program_studi, id_mhs
	)
)
group by nm_jenjang, nm_program_studi
order by nm_jenjang, nm_program_studi
");
$smarty->assign('T_MK', $jaf);

$agk=getdata("
select thn_angkatan_mhs,
sum(A) as jumlah_A,
round(sum(A)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_A,
sum(AB) as jumlah_AB,
round(sum(AB)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_AB,
sum(B) as jumlah_B,
round(sum(B)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_B,
sum(BC) as jumlah_BC,
round(sum(BC)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_BC,
sum(C) as jumlah_C,
round(sum(C)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_C,
sum(D) as jumlah_D,
round(sum(D)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_D,
sum(E) as jumlah_E,
round(sum(E)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_E,
sum(F) as jumlah_F,
round(sum(F)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_F
from
(
	select thn_angkatan_mhs,
	case when ipk_mhs_status<2.00 then 1 else 0 end as A,
	case when ipk_mhs_status between 2.00 and 2.49 then 1 else 0 end as AB,
	case when ipk_mhs_status between 2.50 and 2.74 then 1 else 0 end as B,
	case when ipk_mhs_status between 2.75 and 2.99 then 1 else 0 end as BC,
	case when ipk_mhs_status between 3.00 and 3.24 then 1 else 0 end as C,
	case when ipk_mhs_status between 3.25 and 3.49 then 1 else 0 end as D,
	case when ipk_mhs_status between 3.50 and 3.74 then 1 else 0 end as E,
	case when ipk_mhs_status>=3.75 then 1 else 0 end as F
	from 
	(
		select thn_angkatan_mhs, id_mhs, round((sum((kredit_semester * nilai_standar_nilai)) / sum(kredit_semester)), 2) as ipk_mhs_status
		from 
		(
			select m.thn_angkatan_mhs,a.id_mhs,d.kredit_semester, e.nilai_standar_nilai,
			case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,
			row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by nilai_huruf) rangking
			from pengambilan_mk a
			join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
			join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
			join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
			join semester smt on a.id_semester=smt.id_semester
			join mahasiswa m on a.id_mhs=m.id_mhs
			left join program_studi ps on ps.id_program_studi=m.id_program_studi
			left join jenjang j on j.id_jenjang=ps.id_jenjang
			where a.status_apv_pengambilan_mk = 1 and (a.nilai_huruf<>'E' or a.nilai_huruf<>null) 
			and ps.id_program_studi = ".$var[0]." and a.status_hapus = 0 
			and a.status_pengambilan_mk != 0 and m.status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role = 3 and status_aktif = 1)
		)
		where tahun<='".$thn."' and rangking = 1
		group by thn_angkatan_mhs, id_mhs
	)
)
group by thn_angkatan_mhs
order by thn_angkatan_mhs desc
");
$smarty->assign('T_AGK', $agk);
}
}
$smarty->display('report-prosentase-ipk.tpl');
?>