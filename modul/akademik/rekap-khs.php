<?php
/*
YAH 06/06/2011
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$kdprodi=$user->ID_PROGRAM_STUDI;

$id_pengguna= $user->ID_PENGGUNA;
$kdfak=$user->ID_FAKULTAS;

$smarty->assign('FAK', $kdfak);

//Khusus Fakultas Psikologi
/*
if($kdfak==11){

	echo '
		<script>
		location.href="#akademik-dkhs!rekap-khs-psikologi.php";
		</script>
	';

//Fakultas Lain
} else {
*/

$thnsmt=getData("select id_semester as id_semester, tahun_ajaran||' - '||nm_semester as thn_smt from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester in (
select * from (select distinct thn_akademik_semester
from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester<=EXTRACT(YEAR FROM sysdate) order by thn_akademik_semester desc)
where rownum<=10) and nm_semester in ('Ganjil', 'Genap')
order by thn_akademik_semester desc, nm_semester desc");
$smarty->assign('T_THNSMT', $thnsmt);

$dataprodi=getData("select id_program_studi,nm_jenjang||'-' || nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_fakultas=$kdfak and status_aktif_prodi = 1");
$smarty->assign('T_PRODI', $dataprodi);

if (isset($_REQUEST['action'])=='view') {
$smt = $_POST['kdthnsmt'];
$smarty->assign('SMT_AKAD', $smt);
$krs_smt=getvar("select tahun_ajaran||' - '||nm_semester as thn_smt,thn_akademik_semester, 
				case when nm_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun
				from semester where id_semester=$smt");
$smarty->assign('THN_AKAD', $krs_smt['THN_SMT']);
$smarty->assign('THN_SMT', $krs_smt['THN_AKADEMIK_SEMESTER']);
$smarty->assign('THN_AKD', $krs_smt['TAHUN']);

$kdprodi = $_POST['kdprodi'];
$smarty->assign('PRODI', $kdprodi);

$dataprodi1=getvar("select nm_jenjang||'-'||nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_program_studi=$kdprodi and status_aktif_prodi = 1");
$smarty->assign('NM_PRODI', $dataprodi1['PRODI']);


// Yudi Sulistya, 2012/05/10
$jml_mhs=getvar("
select count(distinct nim_mhs) as jml_mhs from
(
select MAHASISWA.nim_mhs,pengguna.nm_pengguna,mahasiswa.id_program_studi,nm_program_studi,
MATA_KULIAH.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,KURIKULUM_MK.kredit_semester from PENGAMBILAN_MK
left join mahasiswa on pengambilan_mk.id_mhs=MAHASISWA.ID_MHS
left join pengguna on MAHASISWA.id_pengguna=PENGGUNA.id_pengguna
left join program_studi on MAHASISWA.id_program_studi=PROGRAM_STUDI.id_program_studi
left join kelas_mk on PENGAMBILAN_MK.id_kelas_mk=KELAS_MK.id_kelas_mk
left join NAMA_KELAS on KELAS_MK.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk
left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
where PENGAMBILAN_MK.id_semester=$smt and PROGRAM_STUDI.id_fakultas=$kdfak and status_apv_pengambilan_mk=1 and mahasiswa.id_program_studi=$kdprodi
)");
$smarty->assign('JML_MHS', $jml_mhs['JML_MHS']);

/*
$mk=getData("select s1.id_mhs,s1.nim_mhs,s1.nm_pengguna,coalesce(s1.nm_status_pengguna,'AKTIF') as nm_status_pengguna,
s2.kd_mata_kuliah,s2.nm_mata_kuliah,s2.nama_kelas,s2.kredit_semester,s2.nilai_huruf,
s2.tipe_semester from 
(select mahasiswa.id_mhs,nim_mhs,nm_pengguna,nm_status_pengguna from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join admisi on mahasiswa.id_mhs=admisi.id_mhs and admisi.id_semester=$smt
left join status_pengguna on admisi.status_akd_mhs=status_pengguna.id_status_pengguna
where id_program_studi=$kdprodi and thn_angkatan_mhs<='$krs_smt[THN_AKADEMIK_SEMESTER]'
and mahasiswa.id_mhs not in
(select id_mhs from 
(select id_mhs,case when nm_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,status_akd_mhs
from admisi
left join semester on admisi.id_semester=semester.id_semester)
where tahun<='$krs_smt[TAHUN]' and status_akd_mhs not in (1,3))
)s1
left join 
(select id_mhs,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,tipe_semester,
kredit_semester,nilai_huruf
from(
select pengambilan_mk.id_mhs,MATA_KULIAH.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,tipe_semester,
KURIKULUM_MK.kredit_semester,case when kurikulum_mk.status_mkta='1' 
and (nilai_huruf='E' or nilai_huruf is null) and id_fakultas=7 then 'T' 
when nilai_huruf is null then 'E' else nilai_huruf end as nilai_huruf,
row_number() over(partition by pengambilan_mk.id_mhs,nm_mata_kuliah order by nilai_huruf) rangking
from PENGAMBILAN_MK
left join kelas_mk on PENGAMBILAN_MK.id_kelas_mk=KELAS_MK.id_kelas_mk
left join NAMA_KELAS on KELAS_MK.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk
left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
left join semester on pengambilan_mk.id_semester=semester.id_semester
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
where  group_semester||thn_akademik_semester in
(select group_semester||thn_akademik_semester from semester where id_Semester=$smt)
and tipe_semester in ('UP','REG','RD')
and status_apv_pengambilan_mk=1 and PENGAMBILAN_MK.status_hapus=0 and PENGAMBILAN_MK.status_pengambilan_mk !=0
and mahasiswa.id_program_studi=$kdprodi) 
where rangking=1
)s2 on s1.id_mhs=s2.id_mhs
where s1.nm_status_pengguna is not null or (s1.nm_status_pengguna is null and kd_mata_kuliah is not null)
order by s1.nim_mhs,s2.kd_mata_kuliah");
*/

$mk=getData("select id_mhs,nim_mhs,nm_pengguna,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,tipe_semester,
kredit_semester,nilai_huruf
from(
select pengambilan_mk.id_mhs,nim_mhs,nm_pengguna,MATA_KULIAH.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,tipe_semester,
KURIKULUM_MK.kredit_semester,case when kurikulum_mk.status_mkta='1' 
and (nilai_huruf='E' or nilai_huruf is null) and id_fakultas=7 then 'T' 
when nilai_huruf is null then 'E' else nilai_huruf end as nilai_huruf,
row_number() over(partition by pengambilan_mk.id_mhs,nm_mata_kuliah order by nilai_huruf) rangking
from PENGAMBILAN_MK
left join kelas_mk on PENGAMBILAN_MK.id_kelas_mk=KELAS_MK.id_kelas_mk
left join NAMA_KELAS on KELAS_MK.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk
left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
left join semester on pengambilan_mk.id_semester=semester.id_semester
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
where  group_semester||thn_akademik_semester in
(select group_semester||thn_akademik_semester from semester where id_Semester=$smt)
and tipe_semester in ('UP','REG','RD')
and status_apv_pengambilan_mk=1 and PENGAMBILAN_MK.status_hapus=0 and PENGAMBILAN_MK.status_pengambilan_mk !=0
and mahasiswa.id_program_studi=$kdprodi) 
where rangking=1
");

$smarty->assign('T_MK', $mk);
}
//}

$smarty->display('rekap-khs.tpl');
?>
