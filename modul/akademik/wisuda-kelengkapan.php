<?php
require 'common.php';

$kdprodi = $user->ID_PROGRAM_STUDI;
$id_pengguna = $user->ID_PENGGUNA;
$id_fakultas = $user->ID_FAKULTAS;

// Prepare
$_POST['mode']		= isset($_POST['mode']) ? $_POST['mode'] : '';
$_POST['nim']		= isset($_POST['nim']) ? $_POST['nim'] : '';
$_POST['id_mhs']	= isset($_POST['id_mhs']) ? $_POST['id_mhs'] : '';

if ($_POST['nim'])
{
	$nim = $_POST['nim'];

	// Cek ID Mahasiswa
	$get_id = getvar(
		"SELECT ID_MHS FROM mahasiswa m
		JOIN program_studi ps ON ps.id_program_studi = m.id_program_studi
		JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
		WHERE nim_mhs = '{$nim}' AND f.id_perguruan_tinggi = '{$id_pt_user}'");
	$id_mhs = $get_id['ID_MHS'];


	// Cek Mahasiswa exist
	$cek = getvar(
		"SELECT count(*) as JML FROM mahasiswa m
		JOIN program_studi ps ON ps.id_program_studi = m.id_program_studi
		WHERE nim_mhs = '{$nim}' AND ps.id_fakultas = '{$id_fakultas}'");

	// Jika mahasiswa ada
	if ($cek['JML'] == 1)
	{	

		// Cek status Calon Lulus
		$cek = getvar(
			"SELECT COUNT(*) AS JML FROM MAHASISWA m
			JOIN STATUS_PENGGUNA sp ON sp.ID_STATUS_PENGGUNA = m.STATUS_AKADEMIK_MHS
			JOIN program_studi ps ON ps.id_program_studi = m.id_program_studi
			WHERE m.nim_mhs = '{$nim}' AND ps.id_fakultas = '{$id_fakultas}' AND m.STATUS_AKADEMIK_MHS = 16 AND sp.AKTIF_STATUS_PENGGUNA = 1");
		
		// Jika mahasiswa calon lulus
		if ($cek['JML'] == 1)
		{
				$tgl = date('Y-m-d');
				
				// cek periode wisuda sudah disetting / belum
				$periode_ws = getvar(
					"SELECT COUNT(*) AS CEK
					FROM PERIODE_WISUDA
					LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_JENJANG = PERIODE_WISUDA.ID_JENJANG
					LEFT JOIN MAHASISWA ON MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
					WHERE NIM_MHS = '{$nim}' AND STATUS_AKTIF = 1 AND TGL_BAYAR_SELESAI >= SYSDATE");

				if($periode_ws['CEK'] == 0)
				{
					$pesan = 'Periode Wisuda belum disetting, harap hubungi bagian akademik pusat';
				}
				else 
				{ 

							// Ambil biodata
							$pw = getData(
								"SELECT MAHASISWA.ID_MHS, NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, JENJANG.NM_JENJANG, YUDISIUM, PROGRAM_STUDI.ID_JENJANG, NM_TARIF_WISUDA || ' - ' || J2.NM_JENJANG AS NM_TARIF_WISUDA, TO_CHAR(PERIODE_WISUDA.TGL_BAYAR_MULAI, 'DD-MM-YYYY') AS TGL_BAYAR_MULAI, 
									PENGAJUAN_WISUDA.ID_PENGAJUAN_WISUDA,
									PENGAJUAN_WISUDA.TGL_LULUS_PENGAJUAN AS TGL_KELUAR, PENGAJUAN_WISUDA.SK_YUDISIUM, 
									PENGAJUAN_WISUDA.TGL_SK_YUDISIUM, PENGAJUAN_WISUDA.IPK, 
									PENGAJUAN_WISUDA.NO_IJASAH, PENGAJUAN_WISUDA.JUDUL_TA, PENGAJUAN_WISUDA.JUDUL_TA_EN,
									PENGAJUAN_WISUDA.BULAN_AWAL_BIMBINGAN, PENGAJUAN_WISUDA.BULAN_AKHIR_BIMBINGAN
								FROM MAHASISWA
								LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
								LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
								LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
								LEFT JOIN PENGAJUAN_WISUDA ON PENGAJUAN_WISUDA.ID_MHS = MAHASISWA.ID_MHS
								LEFT JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PENGAJUAN_WISUDA.ID_PERIODE_WISUDA
								LEFT JOIN JENJANG J2 ON J2.ID_JENJANG = PERIODE_WISUDA.ID_JENJANG
								LEFT JOIN TARIF_WISUDA ON PERIODE_WISUDA.ID_TARIF_WISUDA = TARIF_WISUDA.ID_TARIF_WISUDA
								WHERE NIM_MHS = '{$nim}' AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}'");
							$smarty->assign('pengajuan_wisuda', $pw);

							$tgl_keluar = $pw[0]['TGL_KELUAR'];
							if(!empty($tgl_keluar)){
								$smarty->assign('tgl_keluar', date_format(date_create($tgl_keluar), 'd-m-Y'));
							}
							else{
								$smarty->assign('tgl_keluar', '');
							}

							$tgl_sk_yudisium = $pw[0]['TGL_SK_YUDISIUM'];
							if(!empty($tgl_sk_yudisium)){
								$smarty->assign('tgl_sk_yudisium', date_format(date_create($tgl_sk_yudisium), 'd-m-Y'));
							}
							else{
								$smarty->assign('tgl_sk_yudisium', '');
							}

							$bulan_awal_bimbingan = $pw[0]['BULAN_AWAL_BIMBINGAN'];
							if(!empty($bulan_awal_bimbingan)){
								$smarty->assign('bulan_awal_bimbingan', date_format(date_create($bulan_awal_bimbingan), 'd-m-Y'));
							}
							else{
								$smarty->assign('bulan_awal_bimbingan', '');
							}

							$bulan_akhir_bimbingan = $pw[0]['BULAN_AKHIR_BIMBINGAN'];
							if(!empty($bulan_akhir_bimbingan)){
								$smarty->assign('bulan_akhir_bimbingan', date_format(date_create($bulan_akhir_bimbingan), 'd-m-Y'));
							}
							else{
								$smarty->assign('bulan_akhir_bimbingan', '');
							}

				}
		}
		else
		{
			$cek = getData("SELECT NM_STATUS_PENGGUNA, NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, NM_JENJANG FROM MAHASISWA 
							JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
							JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
							JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
							JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
							WHERE ID_MHS = '{$id_mhs}'");
			$pesan = "Bukan Mahasiswa Calon Lulus / Belum Yudisium (" . $cek[0]['NM_STATUS_PENGGUNA'] . ")";
			$smarty->assign('cek', $cek);
		}
	}
	else
	{

		$cek = getvar("SELECT NM_FAKULTAS FROM MAHASISWA 
						JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
						JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
						WHERE ID_MHS = '{$id_mhs}'");

		$pesan = "Mahasiswa tidak terdaftar, mahasiswa terdaftar di Fakultas " . $cek['NM_FAKULTAS'];
	}
}

// Jika posting ada id_mhs dan mode update
if ($_POST['id_mhs'] <> '' and $_POST['mode'] == 'update')
{

	$id_pengajuan_wisuda = $_POST['id_pengajuan_wisuda'];

	$tgl_lulus_pengajuan = $_POST['tgl_keluar'];
	$sk_yudisium = $_POST['sk_yudisium'];
	$tgl_sk_yudisium = $_POST['tgl_sk_yudisium'];
	$ipk = $_POST['ipk'];
	$no_ijasah = $_POST['no_ijasah'];
	$judul_ta = $_POST['judul_ta'];
	$judul_ta_en = $_POST['judul_ta_en'];
	$bulan_awal_bimbingan = $_POST['bulan_awal_bimbingan'];
	$bulan_akhir_bimbingan = $_POST['bulan_akhir_bimbingan'];

	$tgl = date('Y-m-d');

	// Mengambil semester aktif
	$row = getvar("SELECT ID_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True' AND ID_PERGURUAN_TINGGI = '{$id_pt_user}'");

	// UPdate isian PENGAJUAN_WISUDA
	UpdateData("UPDATE PENGAJUAN_WISUDA 
					SET TGL_LULUS_PENGAJUAN = to_date('$tgl_lulus_pengajuan', 'DD-MM-YYYY'), 
						SK_YUDISIUM = '{$sk_yudisium}', TGL_SK_YUDISIUM = to_date('$tgl_sk_yudisium', 'DD-MM-YYYY'), 
						IPK = '{$ipk}', NO_IJASAH = '{$no_ijasah}', JUDUL_TA = '{$judul_ta}', 
						JUDUL_TA_EN = '{$judul_ta_en}', 
						BULAN_AWAL_BIMBINGAN = to_date('$bulan_awal_bimbingan', 'DD-MM-YYYY'), 
						BULAN_AKHIR_BIMBINGAN = to_date('$bulan_akhir_bimbingan', 'DD-MM-YYYY'),
						UPDATED_BY = {$id_pengguna}, UPDATED_ON = to_date('$tgl', 'YYYY-MM-DD')
					WHERE ID_PENGAJUAN_WISUDA = '{$id_pengajuan_wisuda}'");

	
							// Tampilkan biodata (lagi)
							$pw = getData(
								"SELECT MAHASISWA.ID_MHS, NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, JENJANG.NM_JENJANG, YUDISIUM, PROGRAM_STUDI.ID_JENJANG, NM_TARIF_WISUDA || ' - ' || J2.NM_JENJANG AS NM_TARIF_WISUDA, TO_CHAR(PERIODE_WISUDA.TGL_BAYAR_MULAI, 'DD-MM-YYYY') AS TGL_BAYAR_MULAI, 
									PENGAJUAN_WISUDA.ID_PENGAJUAN_WISUDA,
									PENGAJUAN_WISUDA.TGL_LULUS_PENGAJUAN AS TGL_KELUAR, PENGAJUAN_WISUDA.SK_YUDISIUM, 
									PENGAJUAN_WISUDA.TGL_SK_YUDISIUM, PENGAJUAN_WISUDA.IPK, 
									PENGAJUAN_WISUDA.NO_IJASAH, PENGAJUAN_WISUDA.JUDUL_TA, PENGAJUAN_WISUDA.JUDUL_TA_EN,
									PENGAJUAN_WISUDA.BULAN_AWAL_BIMBINGAN, PENGAJUAN_WISUDA.BULAN_AKHIR_BIMBINGAN
								FROM MAHASISWA
								LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
								LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
								LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
								LEFT JOIN PENGAJUAN_WISUDA ON PENGAJUAN_WISUDA.ID_MHS = MAHASISWA.ID_MHS
								LEFT JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PENGAJUAN_WISUDA.ID_PERIODE_WISUDA
								LEFT JOIN JENJANG J2 ON J2.ID_JENJANG = PERIODE_WISUDA.ID_JENJANG
								LEFT JOIN TARIF_WISUDA ON PERIODE_WISUDA.ID_TARIF_WISUDA = TARIF_WISUDA.ID_TARIF_WISUDA
								WHERE NIM_MHS = '{$nim}' AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}'");
							$smarty->assign('pengajuan_wisuda', $pw);

							$tgl_keluar = $pw[0]['TGL_KELUAR'];
							if(!empty($tgl_keluar)){
								$smarty->assign('tgl_keluar', date_format(date_create($tgl_keluar), 'd-m-Y'));
							}
							else{
								$smarty->assign('tgl_keluar', '');
							}

							$tgl_sk_yudisium = $pw[0]['TGL_SK_YUDISIUM'];
							if(!empty($tgl_sk_yudisium)){
								$smarty->assign('tgl_sk_yudisium', date_format(date_create($tgl_sk_yudisium), 'd-m-Y'));
							}
							else{
								$smarty->assign('tgl_sk_yudisium', '');
							}

							$bulan_awal_bimbingan = $pw[0]['BULAN_AWAL_BIMBINGAN'];
							if(!empty($bulan_awal_bimbingan)){
								$smarty->assign('bulan_awal_bimbingan', date_format(date_create($bulan_awal_bimbingan), 'd-m-Y'));
							}
							else{
								$smarty->assign('bulan_awal_bimbingan', '');
							}

							$bulan_akhir_bimbingan = $pw[0]['BULAN_AKHIR_BIMBINGAN'];
							if(!empty($bulan_akhir_bimbingan)){
								$smarty->assign('bulan_akhir_bimbingan', date_format(date_create($bulan_akhir_bimbingan), 'd-m-Y'));
							}
							else{
								$smarty->assign('bulan_akhir_bimbingan', '');
							}
	
	$pesan = "Update Data Yudisium Berhasil";
}

// Ambil periode wisuda
$pr = getvar(
	"SELECT NM_TARIF_WISUDA || ' - ' || JENJANG.NM_JENJANG AS NM_TARIF_WISUDA 
	FROM PERIODE_WISUDA
	JOIN TARIF_WISUDA ON TARIF_WISUDA.ID_TARIF_WISUDA = PERIODE_WISUDA.ID_TARIF_WISUDA
	JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_JENJANG = PERIODE_WISUDA.ID_JENJANG
	JOIN JENJANG ON JENJANG.ID_JENJANG = PERIODE_WISUDA.ID_JENJANG
	JOIN MAHASISWA ON MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
	WHERE NIM_MHS = '$nim' AND PROGRAM_STUDI.ID_FAKULTAS = '$id_fakultas' AND PERIODE_WISUDA.STATUS_AKTIF = 1");
$smarty->assign('periode', $pr);

$smarty->assign('pesan', $pesan);
$smarty->display('wisuda-kelengkapan.tpl');