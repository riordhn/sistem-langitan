<?php
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');

$id_pengguna= $user->ID_PENGGUNA; 


$kdfak=$user->ID_FAKULTAS; 

$smtaktif=getvar("select id_semester from semester where status_aktif_sp='True'");

$status = isset($_REQUEST['action'])? $_REQUEST['action'] : 'tampil';

switch($status) {
case 'kapasitas':
		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','blok');
		$smarty->assign('disp3','none');
	
		
		$id_mk=$_GET['id_kelas_mk'];

		$jaf1=getData("select kelas_mk.id_kelas_mk,kelas_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
nama_kelas,kapasitas_kelas_mk
from kelas_mk
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi 
where id_kelas_mk=$id_mk");
		$smarty->assign('TJAF1', $jaf1);
        break;

case 'detail':
		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','block');
	
		
		$id_mk=$_GET['id_kelas_mk'];
		$kodemk=$_GET['kodemk'];
/*		
		$info=getData("select kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nama_kelas from kelas_mk
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
where id_semester='".$smtaktif['ID_SEMESTER']."' and kelas_mk.id_kelas_mk=$id_mk");
		$smarty->assign('T_INFO', $info);
*/

		$info=getData("select kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas.nama_kelas,
trim(wm_concat('<li>'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||' '||(case when pengampu_mk.pjmk_pengampu_mk=1 then '(PJMA)' when pengampu_mk.pjmk_pengampu_mk=0 then '(ANGGOTA)' else '' end))) as tim
from kelas_mk 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk 
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
where kelas_mk.id_semester='".$smtaktif['ID_SEMESTER']."' and kelas_mk.id_kelas_mk=$id_mk
group by kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas.nama_kelas");
		$smarty->assign('T_INFO', $info);

/*
		$detail=getData("select distinct pengambilan_mk.id_mhs,nim_mhs,nm_pengguna,nm_program_studi,
case when status_apv_pengambilan_mk=1 then 'DiSetujui' else 'Blm Disetujui' end as status
from pengambilan_mk
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
where pengambilan_mk.id_semester='".$smtaktif['ID_SEMESTER']."' and id_kelas_mk=$id_mk
");
*/

		$detail=getData("select distinct pengambilan_mk.id_mhs,nim_mhs,nm_pengguna,nm_program_studi,
case when status_apv_pengambilan_mk=1 then 'DiSetujui' else 'Blm Disetujui' end as status,
case when pengambilan_mk.id_mhs in (select distinct id_mhs from pengambilan_mk where id_kurikulum_mk in 
(select id_kurikulum_mk from kurikulum_mk where id_mata_kuliah in 
(select id_mata_kuliah from mata_kuliah where kd_mata_kuliah='$kodemk')) and id_semester !='".$smtaktif['ID_SEMESTER']."') then 'Ulang' else 'Baru' end as status1
from pengambilan_mk
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
where pengambilan_mk.id_semester='".$smtaktif['ID_SEMESTER']."' and id_kelas_mk=$id_mk");

		$smarty->assign('T_DETAIL', $detail);
        break; 
 

case 'add':
		 // pilih
		$smarty->assign('disp1','blok');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
	
		
		$id_mk=$_POST['id_kelas_mk'];
		$kap_kelas=$_POST['kap_kelas'];
		
		
		UpdateData("update kelas_mk set kapasitas_kelas_mk=$kap_kelas where id_kelas_mk=$id_mk");

		$jaf=getData("select kelas_mk.id_kelas_mk,kelas_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
nama_kelas,kapasitas_kelas_mk,terisi_kelas_mk,sum(case when status_pengambilan_mk>=1 then 1 else 0 end) as kls_terisi 
from kelas_mk
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and kelas_mk.id_semester=pengambilan_mk.id_semester
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi 
where id_fakultas=$kdfak and kelas_mk.id_semester='".$smtaktif['ID_SEMESTER']."'
group by kelas_mk.id_kelas_mk,kelas_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
nama_kelas,kapasitas_kelas_mk,terisi_kelas_mk
");
		$smarty->assign('T_MK', $jaf);
        break;

case 'del':
		 // pilih
		$smarty->assign('disp1','blok');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
	
		
		$id_mk=$_GET['id_kelas_mk'];
		//$kap_kelas=$_POST['kap_kelas'];
		
		
		deleteData("delete from  krs_prodi where id_kelas_mk=$id_mk");
		deleteData("delete from  pengampu_mk where id_kelas_mk=$id_mk");
		deleteData("delete from  pengambilan_mk_temp where id_kelas_mk=$id_mk");
		deleteData("delete from  jadwal_kelas where id_kelas_mk=$id_mk");
		deleteData("delete from  kelas_mk where id_kelas_mk=$id_mk and id_semester='".$smtaktif['ID_SEMESTER']."'");
		deleteData("delete from  pengambilan_mk where id_kelas_mk=$id_mk and id_semester='".$smtaktif['ID_SEMESTER']."'");

		$jaf=getData("select kelas_mk.id_kelas_mk,kelas_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
nama_kelas,kapasitas_kelas_mk,terisi_kelas_mk,sum(case when status_pengambilan_mk>=1 then 1 else 0 end) as kls_terisi 
from kelas_mk
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and kelas_mk.id_semester=pengambilan_mk.id_semester
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi 
where id_fakultas=$kdfak and kelas_mk.id_semester='".$smtaktif['ID_SEMESTER']."'
group by kelas_mk.id_kelas_mk,kelas_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
nama_kelas,kapasitas_kelas_mk,terisi_kelas_mk
");
		$smarty->assign('T_MK', $jaf);
        break;          
		

case 'tampil':

if ($kdfak==7 || $kdfak==4)
{
$jaf=getData("select kelas_mk.id_kelas_mk,kelas_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
nama_kelas,kapasitas_kelas_mk,terisi_kelas_mk,
sum(case when status_pengambilan_mk>=1 then 1 else 0 end) as kls_terisi,
sum(case when status_pengambilan_mk=2 then 1 else 0 end) as kprs,
sum(case when status_pengambilan_mk=1 or status_pengambilan_mk>2 then 1 else 0 end) as krs,
kelas_mk.status,nm_singkat_prodi as nm_program_studi,nm_jenjang
from kelas_mk
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and kelas_mk.id_semester=pengambilan_mk.id_semester
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on jenjang.id_jenjang = program_studi.id_jenjang 
where id_fakultas=$kdfak and kelas_mk.id_semester='".$smtaktif['ID_SEMESTER']."'
group by kelas_mk.id_kelas_mk, kelas_mk.id_kurikulum_mk, kd_mata_kuliah, 
nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas, kapasitas_kelas_mk, 
terisi_kelas_mk, kelas_mk.status, nm_singkat_prodi, nm_jenjang
");
}
else
{		
$jaf=getData("select kelas_mk.id_kelas_mk,kelas_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
nama_kelas,kapasitas_kelas_mk,terisi_kelas_mk,
sum(case when status_pengambilan_mk>=1 then 1 else 0 end) as kls_terisi,
sum(case when status_pengambilan_mk=2 then 1 else 0 end) as kprs,
sum(case when status_pengambilan_mk=1 or status_pengambilan_mk>2 then 1 else 0 end) as krs,
kelas_mk.status,nm_program_studi,nm_jenjang
from kelas_mk
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and kelas_mk.id_semester=pengambilan_mk.id_semester
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on jenjang.id_jenjang = program_studi.id_jenjang 
where id_fakultas=$kdfak and kelas_mk.id_semester='".$smtaktif['ID_SEMESTER']."'
group by kelas_mk.id_kelas_mk, kelas_mk.id_kurikulum_mk, kd_mata_kuliah, 
nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas, kapasitas_kelas_mk, 
terisi_kelas_mk, kelas_mk.status, nm_program_studi, nm_jenjang
");
}

		$smarty->assign('T_MK', $jaf);
		//$smarty->assign('T_JAF', $jaf); 
		//$smarty->display('usulan-mk.tpl');
        break; 
			 
}

$smarty->display('mon-pst-sp.tpl');

?>
