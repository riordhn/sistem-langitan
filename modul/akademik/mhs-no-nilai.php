<?php
error_reporting (E_ALL & ~E_NOTICE);
include 'config.php';
$kdprodi= $user->ID_PROGRAM_STUDI;
$id_pengguna= $user->ID_PENGGUNA; 
$id_fakultas = $user->ID_FAKULTAS;

// 2013/10/30
// Added THN_ANGKATAN_MHS != to_char(sysdate, 'YYYY') by Yudi Sulistya
// Agar MaBa tidak ditampilkan dalam daftar
$mhs = $db->QueryToArray("SELECT NIM_MHS, NM_PENGGUNA, NM_STATUS_PENGGUNA, NM_PROGRAM_STUDI, NM_JENJANG
								FROM MAHASISWA
								LEFT JOIN 
									(select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, 
									sum(kredit_semester) as sks, id_mhs
									from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, e.id_semester, 
									c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
									row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
									count(*) over(partition by c.nm_mata_kuliah) terulang
										from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g
										where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah 
										and a.id_semester=e.id_semester
										and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs
									) x
									where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
									group by id_mhs) C ON C.ID_MHS = MAHASISWA.ID_MHS
								LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
								LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
								LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
								LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
								WHERE ID_FAKULTAS = '$id_fakultas' AND IPK IS NULL AND THN_ANGKATAN_MHS != to_char(sysdate, 'YYYY')
								ORDER BY NM_JENJANG, NM_PROGRAM_STUDI, NIM_MHS");
$smarty->assign('mhs', $mhs);

$smarty->display("mhs-no-nilai.tpl");
?>