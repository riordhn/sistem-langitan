<?php
require('common.php');
require_once ('ociFunction.php');
//require('../../../config.php');
$db = new MyOracle();

error_reporting (E_ALL & ~E_NOTICE);
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$id_pengguna= $user->ID_PENGGUNA;

$kdfak=$user->ID_FAKULTAS;


$smtaktif=getvar("select id_semester from semester where status_aktif_sp='True'");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);

$smt1 = isSet($_POST['smt']) ? $_POST['smt'] : $smtaktif['ID_SEMESTER'];


if ($_GET['action']=='detail')
{
	$id_prodi=$_GET['idprodi'];
	$st=$_GET['st'];
	//echo $id_prodi;
	//echo $st;
	
	if ($st==1)
	{
	 $detailprodi=getData("select * from 
					(select pengambilan_mk.id_mhs,mahasiswa.nim_mhs,nm_pengguna,
					nm_jenjang||'-'||nm_program_studi as prodi,mahasiswa.id_program_studi,
					sum(kredit_semester) as sks_ambil, besar_biaya_sp, (besar_biaya_sp * sum(kredit_semester)) as sp_biaya,
					besar_biaya,id_status_pembayaran,'lunas' as status,nm_jalur as jalur
					from pengambilan_mk
					left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
					left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
					left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
					left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
					left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
					left join jalur_mahasiswa on jalur_mahasiswa.id_mhs = mahasiswa.id_mhs
					left join jalur on jalur_mahasiswa.id_jalur=jalur.id_jalur
					left join biaya_sp on biaya_sp.id_jalur = jalur_mahasiswa.id_jalur and biaya_sp.id_program_studi = mahasiswa.id_program_studi
					and biaya_sp.id_semester = pengambilan_mk.id_semester
					left join pembayaran_sp on pengambilan_mk.id_mhs=pembayaran_sp.id_mhs and pengambilan_mk.id_semester=pembayaran_sp.id_semester
					where pengambilan_mk.id_semester=$smt1 and id_fakultas=$kdfak and id_program_studi=$id_prodi 
					and jalur_mahasiswa.id_jalur_aktif = 1 and id_status_pembayaran=1 and status_apv_pengambilan_mk=1
					group by pengambilan_mk.id_mhs, mahasiswa.nim_mhs, nm_pengguna,nm_jalur, 
					nm_jenjang||'-'||nm_program_studi, mahasiswa.id_program_studi, besar_biaya_sp,besar_biaya,id_status_pembayaran)
					where besar_biaya=sp_biaya");
	}
	if ($st==2)
	{
	 $detailprodi=getData("select * from 
					(select pengambilan_mk.id_mhs,mahasiswa.nim_mhs,nm_pengguna,
					nm_jenjang||'-'||nm_program_studi as prodi,mahasiswa.id_program_studi,
					sum(kredit_semester) as sks_ambil, besar_biaya_sp, (besar_biaya_sp * sum(kredit_semester)) as sp_biaya,
					besar_biaya,id_status_pembayaran,'belum bayar' as status,nm_jalur as jalur
					from pengambilan_mk
					left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
					left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
					left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
					left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
					left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
					left join jalur_mahasiswa on jalur_mahasiswa.id_mhs = mahasiswa.id_mhs
					left join jalur on jalur_mahasiswa.id_jalur=jalur.id_jalur
					left join biaya_sp on biaya_sp.id_jalur = jalur_mahasiswa.id_jalur and biaya_sp.id_program_studi = mahasiswa.id_program_studi
					and biaya_sp.id_semester = pengambilan_mk.id_semester
					left join pembayaran_sp on pengambilan_mk.id_mhs=pembayaran_sp.id_mhs and pengambilan_mk.id_semester=pembayaran_sp.id_semester
					where pengambilan_mk.id_semester=$smt1 and id_fakultas=$kdfak and id_program_studi=$id_prodi 
					and jalur_mahasiswa.id_jalur_aktif = 1 and id_status_pembayaran!=1 and status_apv_pengambilan_mk=1
					group by pengambilan_mk.id_mhs, mahasiswa.nim_mhs, nm_pengguna, nm_jalur,
					nm_jenjang||'-'||nm_program_studi, mahasiswa.id_program_studi, besar_biaya_sp,besar_biaya,id_status_pembayaran)
					");
	}
	if ($st==3)
	{
	 $detailprodi=getData("select * from 
					(select pengambilan_mk.id_mhs,mahasiswa.nim_mhs,nm_pengguna,
					nm_jenjang||'-'||nm_program_studi as prodi,mahasiswa.id_program_studi,
					sum(kredit_semester) as sks_ambil, besar_biaya_sp, (besar_biaya_sp * sum(kredit_semester)) as sp_biaya,
					besar_biaya,id_status_pembayaran,'kurang' as status,nm_jalur as jalur
					from pengambilan_mk
					left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
					left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
					left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
					left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
					left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
					left join jalur_mahasiswa on jalur_mahasiswa.id_mhs = mahasiswa.id_mhs
					left join jalur on jalur_mahasiswa.id_jalur=jalur.id_jalur
					left join biaya_sp on biaya_sp.id_jalur = jalur_mahasiswa.id_jalur and biaya_sp.id_program_studi = mahasiswa.id_program_studi
					and biaya_sp.id_semester = pengambilan_mk.id_semester
					left join pembayaran_sp on pengambilan_mk.id_mhs=pembayaran_sp.id_mhs and pengambilan_mk.id_semester=pembayaran_sp.id_semester
					where pengambilan_mk.id_semester=$smt1 and id_fakultas=$kdfak and id_program_studi=$id_prodi 
					and jalur_mahasiswa.id_jalur_aktif = 1 and id_status_pembayaran=1 and status_apv_pengambilan_mk=1
					group by pengambilan_mk.id_mhs, mahasiswa.nim_mhs, nm_pengguna,nm_jalur, 
					nm_jenjang||'-'||nm_program_studi, mahasiswa.id_program_studi, besar_biaya_sp,besar_biaya,id_status_pembayaran)
					where besar_biaya<sp_biaya");
	}
	if ($st==4)
	{
	 $detailprodi=getData("select * from 
					(select pengambilan_mk.id_mhs,mahasiswa.nim_mhs,nm_pengguna,
					nm_jenjang||'-'||nm_program_studi as prodi,mahasiswa.id_program_studi,
					sum(kredit_semester) as sks_ambil, besar_biaya_sp, (besar_biaya_sp * sum(kredit_semester)) as sp_biaya,
					besar_biaya,id_status_pembayaran,'lebih' as status,nm_jalur as jalur
					from pengambilan_mk
					left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
					left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
					left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
					left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
					left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
					left join jalur_mahasiswa on jalur_mahasiswa.id_mhs = mahasiswa.id_mhs
					left join jalur on jalur_mahasiswa.id_jalur=jalur.id_jalur
					left join biaya_sp on biaya_sp.id_jalur = jalur_mahasiswa.id_jalur and biaya_sp.id_program_studi = mahasiswa.id_program_studi
					and biaya_sp.id_semester = pengambilan_mk.id_semester
					left join pembayaran_sp on pengambilan_mk.id_mhs=pembayaran_sp.id_mhs and pengambilan_mk.id_semester=pembayaran_sp.id_semester
					where pengambilan_mk.id_semester=$smt1 and id_fakultas=$kdfak and id_program_studi=$id_prodi 
					and jalur_mahasiswa.id_jalur_aktif = 1 and id_status_pembayaran=1 and status_apv_pengambilan_mk=1
					group by pengambilan_mk.id_mhs, mahasiswa.nim_mhs, nm_pengguna,nm_jalur, 
					nm_jenjang||'-'||nm_program_studi, mahasiswa.id_program_studi, besar_biaya_sp,besar_biaya,id_status_pembayaran)
					where besar_biaya>sp_biaya");
	}
	
	
	$smarty->assign('T_DETPRODI', $detailprodi);
	
	$smarty->assign('disp1','none');
	$smarty->assign('disp2','block');
} 
if ($_GET['action']=='drop')
{
	$id_prodi=$_GET['idprodi'];
	
	gantidata("update pengambilan_mk set status_pengambilan_mk=0,status_apv_pengambilan_mk=0,status_hapus=1,flagnilai='0',keterangan='DROP KRS_SP BELUM BAYAR' where id_semester=$smt1
	           and id_mhs in (select id_mhs from pembayaran_sp where id_semester=$smt1 and id_status_pembayaran !=1) 
			   and id_mhs in (select id_mhs from mahasiswa where id_program_studi=$id_prodi)");
	//echo "update pengambilan_mk set status_pengambilan_mk='0',status_apv_pengambilan_mk='0',status_hapus=1,flagnilai='0',keterangan='DROP KRS_SP BELUM BAYAR' where id_semester=$smt1
	  //         and id_mhs in (select id_mhs from pembayaran_sp where id_semester=$smt1 and id_status_pembayaran !=1) 
		//	   and id_mhs in (select id_mhs from mahasiswa where id_program_studi=$id_prodi";
	
	echo '<script>alert("Proses DROP KRS-SP.. Sudah SELESAI...")</script>';
	echo '<script>location.href="javascript:history.go(-1)";</script>';
	
} 
else {
$prodi1=getData("select prodi,id_program_studi,sum(case when (besar_biaya=sp_biaya and id_status_pembayaran=1) then 1 else 0 end)as ttlbayar, 
sum(case when (besar_biaya<sp_biaya and id_status_pembayaran=1) then 1 else 0 end)as krgbayar,
sum(case when (id_status_pembayaran !=1)  then 1 else 0 end)as blmbayar,
sum(case when (besar_biaya>sp_biaya and id_status_pembayaran =1) then 1 else 0 end)as lbhbayar from 
(select pengambilan_mk.id_mhs,mahasiswa.nim_mhs,nm_pengguna,
nm_jenjang||'-'||nm_program_studi as prodi,mahasiswa.id_program_studi,
sum(kredit_semester) as sks_ambil, besar_biaya_sp, (besar_biaya_sp * sum(kredit_semester)) as sp_biaya,
besar_biaya,id_status_pembayaran
from pengambilan_mk
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join jalur_mahasiswa on jalur_mahasiswa.id_mhs = mahasiswa.id_mhs
left join biaya_sp on biaya_sp.id_jalur = jalur_mahasiswa.id_jalur and biaya_sp.id_program_studi = mahasiswa.id_program_studi
and biaya_sp.id_semester = pengambilan_mk.id_semester
left join pembayaran_sp on pengambilan_mk.id_mhs=pembayaran_sp.id_mhs and pengambilan_mk.id_semester=pembayaran_sp.id_semester
where pengambilan_mk.id_semester=$smt1 and id_fakultas=$kdfak and jalur_mahasiswa.id_jalur_aktif = 1 and status_apv_pengambilan_mk=1
group by pengambilan_mk.id_mhs, mahasiswa.nim_mhs, nm_pengguna, 
nm_jenjang||'-'||nm_program_studi, mahasiswa.id_program_studi, besar_biaya_sp,besar_biaya,id_status_pembayaran)
group by prodi,id_program_studi
order by prodi");


$smarty->assign('T_PRODI1', $prodi1);
}

$smarty->display('mon-pemb-sp.tpl');

?>
