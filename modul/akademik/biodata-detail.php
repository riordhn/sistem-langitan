<?php
/*
YAH 06/06/2011
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

//** Yudi Sulistya untuk Psikologi **//
$kdprodi=$user->ID_PROGRAM_STUDI;

$id_pengguna= $user->ID_PENGGUNA; 

$kdfak=$user->ID_FAKULTAS;
$smarty->assign('FAK', $kdfak);
//********//

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True'");
$id_semester=$smtaktif['ID_SEMESTER'];

if (isset($_GET['action'])=='detail') {
   		$id_mhs= $_GET['id'];
		
		$biodata_mahasiswa=getvar("SELECT M.NIM_MHS, NM_PENGGUNA, THN_ANGKATAN_MHS, NM_JENJANG, NM_FAKULTAS, NM_PROGRAM_STUDI,
			NM_KOTA, TGL_LAHIR_PENGGUNA, TELP, MOBILE_MHS, KELAMIN_PENGGUNA, EMAIL_PENGGUNA, ALAMAT_MHS, BLOG_PENGGUNA,
			SEKOLAH_ASAL_MHS, THN_LULUS_MHS, PRODI_S1, PRODI_S2, TGL_LULUS_S1, TGL_LULUS_S2, TGL_MASUK_S1, TGL_MASUK_S2, PTN_S1, PTN_S2
			FROM AUCC.PENGGUNA P 
            LEFT JOIN AUCC.KOTA K ON K.ID_KOTA LIKE ''||P.TEMPAT_LAHIR||''
            LEFT JOIN AUCC.MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA
            LEFT JOIN AUCC.CALON_MAHASISWA_BARU CM ON CM.NIM_MHS = M.NIM_MHS       
            LEFT JOIN AUCC.PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            LEFT JOIN AUCC.JENJANG J ON PS.ID_JENJANG = J.ID_JENJANG
            LEFT JOIN AUCC.FAKULTAS F ON PS.ID_FAKULTAS = F.ID_FAKULTAS
			LEFT JOIN AUCC.CALON_MAHASISWA_PASCA ON CALON_MAHASISWA_PASCA.ID_C_MHS = CM.ID_C_MHS
            WHERE M.ID_MHS=$id_mhs");

$smarty->assign('biodata_mahasiswa', $biodata_mahasiswa);
		
}

$smarty->display('biodata-detail.tpl');    
?>
