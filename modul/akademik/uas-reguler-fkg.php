<?php
//Yudi Sulistya, 27/09/2012
require('common.php');
require_once ('ociFunction.php');

error_reporting (E_ALL & ~E_NOTICE);

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');
$smarty->assign('disp5','none');

$id_pengguna = $user->ID_PENGGUNA;
$kdfak = $user->ID_FAKULTAS;

$smtaktif = getvar("select id_semester from semester where status_aktif_semester='True'");
$id_smt = $smtaktif['ID_SEMESTER'];
$smarty->assign('SMT', $id_smt);

$status = isset($_REQUEST['action'])? $_REQUEST['action'] : 'tampil';

switch($status) {

// Penambahan jadwal Ujian MK
case 'add':
$id_kelas_mk = $_GET['kls'];
$jadwal = getvar("
select kelas_mk.id_kelas_mk,jam_mulai||':'||menit_mulai as jam_mulai, jam_selesai||':'||menit_selesai as jam_selesai
from kelas_mk
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
where kelas_mk.id_kelas_mk=$id_kelas_mk and rownum=1
");
$jam_mulai = $jadwal['JAM_MULAI'];
$jam_selesai = $jadwal['JAM_SELESAI'];

$cek = getvar("select count(*) as ada from ujian_mk where id_kelas_mk=$id_kelas_mk and id_kegiatan=47");
$ada = $cek['ADA'];

if($ada == 0) {
tambahdata("ujian_mk","id_kegiatan,id_kelas_mk,jam_mulai,jam_selesai,id_fakultas,id_semester","47,$id_kelas_mk,'$jam_mulai','$jam_selesai',$kdfak,$id_smt");

$id_jadwal_kelas = $_GET['jad'];
$ruang = getvar("select id_ruangan from jadwal_kelas where id_jadwal_kelas=$id_jadwal_kelas and rownum=1");
$id_ruangan = $ruang['ID_RUANGAN'];

$id_ujian = getvar("
select max(id_ujian_mk) as id_ujian_mk
from ujian_mk
where id_kelas_mk=$id_kelas_mk and id_kegiatan=47
and id_fakultas=$kdfak and id_semester=$id_smt and rownum=1
order by id_ujian_mk desc");
$id_ujian_mk = $id_ujian['ID_UJIAN_MK'];
tambahdata("jadwal_ujian_mk","id_ujian_mk,id_ruangan","$id_ujian_mk,$id_ruangan");
}

echo '<script>location.href="#ujian-ujianreguas!uas-reguler-fkg.php?action=penjadwalan";</script>';
break;

// Penambahan jadwal Ujian MK (pecah ruang)
case 'addjadwal':
$id_kelas_mk = $_GET['kls'];
$id_umk = $_GET['ujian'];

$jadwal = getvar("select nm_ujian_mk,tgl_ujian,jam_mulai,jam_selesai from ujian_mk where id_ujian_mk=$id_umk");
$nm_ujian_mk = $jadwal['NM_UJIAN_MK'];
$tgl_ujian = $jadwal['TGL_UJIAN'];
$jam_mulai = $jadwal['JAM_MULAI'];
$jam_selesai = $jadwal['JAM_SELESAI'];
tambahdata("ujian_mk","id_kegiatan,id_kelas_mk,nm_ujian_mk,tgl_ujian,jam_mulai,jam_selesai,id_fakultas,id_semester","47,$id_kelas_mk,'$nm_ujian_mk','$tgl_ujian','$jam_mulai','$jam_selesai',$kdfak,$id_smt");

echo '<script>location.href="#ujian-ujianreguas!uas-reguler-fkg.php?action=tampil";</script>';
break;

// Tampilan form update jadwal Ujian MK
case 'updateview':
$smarty->assign('disp1','none');
$smarty->assign('disp2','none');
$smarty->assign('disp3','block');
$smarty->assign('disp4','none');
$smarty->assign('disp5','none');

$id_ujian_mk = $_GET['ujian'];

$ruang = getData("
select id_ruangan,nm_ruangan||' - '||kapasitas_ujian as ruang from ruangan
left join gedung on gedung.id_gedung=ruangan.id_gedung
left join gedung_kuliah on gedung.id_gedung=gedung_kuliah.id_gedung
where gedung.id_fakultas=$kdfak and ruangan.kapasitas_ujian > 0 order by nm_ruangan
");
$smarty->assign('T_RUANG', $ruang);

$hari = getData("select id_jadwal_hari,nm_jadwal_hari from jadwal_hari");
$smarty->assign('T_HARI', $hari);

$dosen = getData("
select tim.id_tim_pengawas_ujian,case when tim.status=1 then 'Penguji' else 'Pengawas' end as tipe,
case when (pgg.gelar_belakang is null or pgg.gelar_belakang = '') then trim(pgg.gelar_depan||' '||upper(nm_pengguna))
else trim(pgg.gelar_depan||' '||upper(nm_pengguna)||', '||pgg.gelar_belakang) end as pengawas,
case when pgg.id_role=4 then 'Dosen' else 'Pegawai' end as status
from tim_pengawas_ujian tim
left join jadwal_ujian_mk jad on jad.id_jadwal_ujian_mk=tim.id_jadwal_ujian_mk
left join pengguna pgg on tim.id_pengguna=pgg.id_pengguna
where jad.id_ujian_mk=$id_ujian_mk
");
$smarty->assign('DOSEN', $dosen);

$ujian_mk = getData("
select umk.id_ujian_mk,jad.id_jadwal_ujian_mk,umk.id_kelas_mk,umk.nm_ujian_mk,umk.tgl_ujian,umk.jam_mulai,umk.jam_selesai,jad.id_ruangan,
mk.kd_mata_kuliah||' - '||mk.nm_mata_kuliah as nm_mata_kuliah,nmk.nama_kelas,nm_jenjang||'-'||nm_singkat_prodi as prodi, rg.nm_ruangan, rg.kapasitas_ujian
from ujian_mk umk
left join jadwal_ujian_mk jad on umk.id_ujian_mk=jad.id_ujian_mk
left join ruangan rg on jad.id_ruangan=rg.id_ruangan
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join nama_kelas nmk on nmk.id_nama_kelas=kmk.no_kelas_mk
left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
where umk.id_ujian_mk=$id_ujian_mk
");
$smarty->assign('UMK', $ujian_mk);

$data = getData("
select a.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nm_ujian_mk,tgl_ujian,jam,prodi,round(b.kls_terisi/count(a.id_ujian_mk)) as bagi,ada,
wm_concat('<li>'||nama_kelas||' - '||nm_ruangan||' ('||kapasitas_ujian||')') as nama_kelas,sum(kapasitas_ujian) as kapasitas_ujian, b.kls_terisi from
(
(select kmk.id_kurikulum_mk,umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,to_char(umk.tgl_ujian,'DD-MM-YYYY') as tgl_ujian,
umk.jam_mulai||' - '||umk.jam_selesai as jam,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang||'-'||ps.nm_singkat_prodi as prodi,nmk.nama_kelas
from ujian_mk umk
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join nama_kelas nmk on nmk.id_nama_kelas=kmk.no_kelas_mk
left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
left join jadwal_ujian_mk jad on jad.id_ujian_mk=umk.id_ujian_mk
left join ruangan rg on rg.id_ruangan=jad.id_ruangan
where umk.id_kegiatan=47 and umk.id_fakultas=$kdfak and umk.id_semester=$id_smt
group by kmk.id_kurikulum_mk,umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,umk.tgl_ujian,
umk.jam_mulai,umk.jam_selesai,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang,ps.nm_singkat_prodi,nmk.nama_kelas) a
left join
(select pmk.id_kurikulum_mk, sum(case when pmk.status_apv_pengambilan_mk=1 then 1 else 0 end) as kls_terisi
from pengambilan_mk pmk
where pmk.id_semester=$id_smt and pmk.id_kelas_mk in (select id_kelas_mk from kelas_mk where id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak)) and pmk.status_apv_pengambilan_mk=1
group by pmk.id_kurikulum_mk) b
on a.id_kurikulum_mk=b.id_kurikulum_mk
left join
(select kelas_mk.id_kurikulum_mk, count(distinct ujian_mk_peserta.id_mhs) as ada from ujian_mk 
left join ujian_mk_peserta on ujian_mk.id_ujian_mk=ujian_mk_peserta.id_ujian_mk
left join kelas_mk on kelas_mk.id_kelas_mk=ujian_mk.id_kelas_mk
where kelas_mk.id_semester=$id_smt and kelas_mk.id_kelas_mk in
(select id_kelas_mk from kelas_mk where id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak))
and ujian_mk.id_kegiatan=47
group by kelas_mk.id_kurikulum_mk) c
on a.id_kurikulum_mk=c.id_kurikulum_mk
)
group by a.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nm_ujian_mk,tgl_ujian,jam,prodi,kls_terisi,ada
");
$smarty->assign('DATA', $data);

$jaf = getData("
select * from
(
(select umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,to_char(umk.tgl_ujian,'DD-MM-YYYY') as tgl_ujian,
umk.jam_mulai||' - '||umk.jam_selesai as jam,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang||'-'||ps.nm_singkat_prodi as prodi,
wm_concat('<li>'||
case when tim.status='1' then trim('<font color=\"blue\"><b>'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||'</b></font>')
else trim('<font color=\"black\">'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||'</font>') end) as tim,
nmk.nama_kelas
from ujian_mk umk
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join nama_kelas nmk on nmk.id_nama_kelas=kmk.no_kelas_mk
left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
left join jadwal_ujian_mk jad on jad.id_ujian_mk=umk.id_ujian_mk
left join ruangan rg on rg.id_ruangan=jad.id_ruangan
left join tim_pengawas_ujian tim on jad.id_jadwal_ujian_mk=tim.id_jadwal_ujian_mk
left join pengguna pgg on pgg.id_pengguna=tim.id_pengguna
where umk.id_kegiatan=47 and umk.id_fakultas=$kdfak and umk.id_semester=$id_smt
group by umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,umk.tgl_ujian,
umk.jam_mulai,umk.jam_selesai,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang,ps.nm_singkat_prodi,nmk.nama_kelas) a
left join
(select id_ujian_mk, count(*) as kls_terisi from ujian_mk_peserta group by id_ujian_mk) b
on a.id_ujian_mk=b.id_ujian_mk
)
");
$smarty->assign('T_MK', $jaf);

$tawar = getData("
select distinct jadwal_kelas.id_jadwal_kelas,kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
nama_kelas,kapasitas_kelas_mk,nm_ruangan,kapasitas_ujian,nm_jadwal_hari,
sum(case when status_apv_pengambilan_mk=1 then 1 else 0 end) as kls_terisi,kelas_mk.status,nm_jenjang||'-'||nm_singkat_prodi as prodi
from kelas_mk
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and pengambilan_mk.id_semester=$id_smt
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
where id_fakultas=$kdfak and kelas_mk.id_semester=$id_smt and kurikulum_mk.status_mkta not in (1,2) and
kelas_mk.id_kelas_mk not in (select id_kelas_mk from ujian_mk where id_kegiatan=47 and id_fakultas=$kdfak and id_semester=$id_smt)
group by jadwal_kelas.id_jadwal_kelas,kelas_mk.id_kelas_mk, kd_mata_kuliah, nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas, kapasitas_kelas_mk,
nm_jadwal_hari, kelas_mk.status, nm_singkat_prodi, nm_jenjang, nm_ruangan, kapasitas_ujian
");
$smarty->assign('TAWAR', $tawar);
break;

// Update jadwal Ujian MK
case 'update':
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');
$smarty->assign('disp5','none');

$id_ujian_mk = $_POST['ujian'];
$id_jadwal_ujian_mk = $_POST['jad'];
$nm_ujian_mk = $_POST['nm_ujian_mk'];
$tgl_ujian = $_POST['tgl_ujian'];
$mulai = $_POST['jam_mulai'];
$selesai = $_POST['jam_selesai'];
$id_ruangan = $_POST['ruangan'];

$jam_mulai = substr($mulai, 0, 2).':'.substr($mulai, -2, 2);
$jam_selesai = substr($selesai, 0, 2).':'.substr($selesai, -2, 2);

$cek = getvar("select count(*) as ada from jadwal_ujian_mk where id_ujian_mk=$id_ujian_mk");
$ada = $cek['ADA'];

if($ada == 0) {
	gantidata("update ujian_mk set nm_ujian_mk=upper('".$nm_ujian_mk."'), tgl_ujian='$tgl_ujian', jam_mulai='$jam_mulai', jam_selesai='$jam_selesai' where id_ujian_mk=$id_ujian_mk");
	tambahdata("jadwal_ujian_mk","id_ujian_mk,id_ruangan","$id_ujian_mk,$id_ruangan");
} else {
	gantidata("update ujian_mk set nm_ujian_mk=upper('".$nm_ujian_mk."'), tgl_ujian='$tgl_ujian', jam_mulai='$jam_mulai', jam_selesai='$jam_selesai' where id_ujian_mk=$id_ujian_mk");
	gantidata("update jadwal_ujian_mk set id_ruangan=$id_ruangan where id_jadwal_ujian_mk=$id_jadwal_ujian_mk");
}

$data = getData("
select a.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nm_ujian_mk,tgl_ujian,jam,prodi,round(b.kls_terisi/count(a.id_ujian_mk)) as bagi,ada,
wm_concat('<li>'||nama_kelas||' - '||nm_ruangan||' ('||kapasitas_ujian||')') as nama_kelas,sum(kapasitas_ujian) as kapasitas_ujian, b.kls_terisi from
(
(select kmk.id_kurikulum_mk,umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,to_char(umk.tgl_ujian,'DD-MM-YYYY') as tgl_ujian,
umk.jam_mulai||' - '||umk.jam_selesai as jam,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang||'-'||ps.nm_singkat_prodi as prodi,nmk.nama_kelas
from ujian_mk umk
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join nama_kelas nmk on nmk.id_nama_kelas=kmk.no_kelas_mk
left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
left join jadwal_ujian_mk jad on jad.id_ujian_mk=umk.id_ujian_mk
left join ruangan rg on rg.id_ruangan=jad.id_ruangan
where umk.id_kegiatan=47 and umk.id_fakultas=$kdfak and umk.id_semester=$id_smt
group by kmk.id_kurikulum_mk,umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,umk.tgl_ujian,
umk.jam_mulai,umk.jam_selesai,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang,ps.nm_singkat_prodi,nmk.nama_kelas) a
left join
(select pmk.id_kurikulum_mk, sum(case when pmk.status_apv_pengambilan_mk=1 then 1 else 0 end) as kls_terisi
from pengambilan_mk pmk
where pmk.id_semester=$id_smt and pmk.id_kelas_mk in (select id_kelas_mk from kelas_mk where id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak)) and pmk.status_apv_pengambilan_mk=1
group by pmk.id_kurikulum_mk) b
on a.id_kurikulum_mk=b.id_kurikulum_mk
left join
(select kelas_mk.id_kurikulum_mk, count(distinct ujian_mk_peserta.id_mhs) as ada from ujian_mk 
left join ujian_mk_peserta on ujian_mk.id_ujian_mk=ujian_mk_peserta.id_ujian_mk
left join kelas_mk on kelas_mk.id_kelas_mk=ujian_mk.id_kelas_mk
where kelas_mk.id_semester=$id_smt and kelas_mk.id_kelas_mk in
(select id_kelas_mk from kelas_mk where id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak))
and ujian_mk.id_kegiatan=47
group by kelas_mk.id_kurikulum_mk) c
on a.id_kurikulum_mk=c.id_kurikulum_mk
)
group by a.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nm_ujian_mk,tgl_ujian,jam,prodi,kls_terisi,ada
");
$smarty->assign('DATA', $data);

$jaf = getData("
select * from
(
(select umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,to_char(umk.tgl_ujian,'DD-MM-YYYY') as tgl_ujian,
umk.jam_mulai||' - '||umk.jam_selesai as jam,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang||'-'||ps.nm_singkat_prodi as prodi,
wm_concat('<li>'||
case when tim.status='1' then trim('<font color=\"blue\"><b>'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||'</b></font>')
else trim('<font color=\"black\">'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||'</font>') end) as tim,
nmk.nama_kelas
from ujian_mk umk
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join nama_kelas nmk on nmk.id_nama_kelas=kmk.no_kelas_mk
left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
left join jadwal_ujian_mk jad on jad.id_ujian_mk=umk.id_ujian_mk
left join ruangan rg on rg.id_ruangan=jad.id_ruangan
left join tim_pengawas_ujian tim on jad.id_jadwal_ujian_mk=tim.id_jadwal_ujian_mk
left join pengguna pgg on pgg.id_pengguna=tim.id_pengguna
where umk.id_kegiatan=47 and umk.id_fakultas=$kdfak and umk.id_semester=$id_smt
group by umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,umk.tgl_ujian,
umk.jam_mulai,umk.jam_selesai,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang,ps.nm_singkat_prodi,nmk.nama_kelas) a
left join
(select id_ujian_mk, count(*) as kls_terisi from ujian_mk_peserta group by id_ujian_mk) b
on a.id_ujian_mk=b.id_ujian_mk
)
");
$smarty->assign('T_MK', $jaf);

$tawar = getData("
select distinct jadwal_kelas.id_jadwal_kelas,kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
nama_kelas,kapasitas_kelas_mk,nm_ruangan,kapasitas_ujian,nm_jadwal_hari,
sum(case when status_apv_pengambilan_mk=1 then 1 else 0 end) as kls_terisi,kelas_mk.status,nm_jenjang||'-'||nm_singkat_prodi as prodi
from kelas_mk
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and pengambilan_mk.id_semester=$id_smt
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
where id_fakultas=$kdfak and kelas_mk.id_semester=$id_smt and kurikulum_mk.status_mkta not in (1,2) and
kelas_mk.id_kelas_mk not in (select id_kelas_mk from ujian_mk where id_kegiatan=47 and id_fakultas=$kdfak and id_semester=$id_smt)
group by jadwal_kelas.id_jadwal_kelas,kelas_mk.id_kelas_mk, kd_mata_kuliah, nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas, kapasitas_kelas_mk,
nm_jadwal_hari, kelas_mk.status, nm_singkat_prodi, nm_jenjang, nm_ruangan, kapasitas_ujian
");
$smarty->assign('TAWAR', $tawar);
break;

// Penentuan pengawas Ujian MK
case 'pengawas':
$smarty->assign('disp1','none');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','block');
$smarty->assign('disp5','none');

$id_jadwal_ujian_mk = $_GET['jad'];
$id_ujian_mk = $_GET['ujian'];

$ruang = getData("
select id_ruangan,nm_ruangan||' - '||kapasitas_ujian as ruang from ruangan
left join gedung on gedung.id_gedung=ruangan.id_gedung
left join gedung_kuliah on gedung.id_gedung=gedung_kuliah.id_gedung
where gedung.id_fakultas=$kdfak and ruangan.kapasitas_ujian > 0 order by nm_ruangan
");
$smarty->assign('T_RUANG', $ruang);

$tim = getData("
select id_pengguna,pengawas,id_role,nm_role from
(
(select pgg.id_pengguna, case when (pgg.gelar_belakang is null or pgg.gelar_belakang = '') then trim(pgg.gelar_depan||' '||upper(nm_pengguna))
else trim(pgg.gelar_depan||' '||upper(nm_pengguna)||', '||pgg.gelar_belakang) end as pengawas, pgg.id_role, 'Dosen' as nm_role
from dosen dsn left join pengguna pgg on dsn.id_pengguna=pgg.id_pengguna
where dsn.id_program_studi_sd in (select id_program_studi from program_studi where id_fakultas=$kdfak))
union
(select pgg.id_pengguna, case when (pgg.gelar_belakang is null or pgg.gelar_belakang = '') then trim(pgg.gelar_depan||' '||upper(nm_pengguna))
else trim(pgg.gelar_depan||' '||upper(nm_pengguna)||', '||pgg.gelar_belakang) end as pengawas, pgg.id_role, 'Pegawai' as nm_role
from pegawai peg
left join pengguna pgg on peg.id_pengguna=pgg.id_pengguna
where peg.id_unit_kerja in (select id_unit_kerja from unit_kerja where id_fakultas=$kdfak) and peg.id_pengguna not in (select id_pengguna from dosen))
)
where id_pengguna not in (select id_pengguna from tim_pengawas_ujian where id_jadwal_ujian_mk=$id_jadwal_ujian_mk)
order by nm_role, pengawas
");
$smarty->assign('TIM', $tim);

$data = getData("
select a.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nm_ujian_mk,tgl_ujian,jam,prodi,round(b.kls_terisi/count(a.id_ujian_mk)) as bagi,ada,
wm_concat('<li>'||nama_kelas||' - '||nm_ruangan||' ('||kapasitas_ujian||')') as nama_kelas,sum(kapasitas_ujian) as kapasitas_ujian, b.kls_terisi from
(
(select kmk.id_kurikulum_mk,umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,to_char(umk.tgl_ujian,'DD-MM-YYYY') as tgl_ujian,
umk.jam_mulai||' - '||umk.jam_selesai as jam,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang||'-'||ps.nm_singkat_prodi as prodi,nmk.nama_kelas
from ujian_mk umk
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join nama_kelas nmk on nmk.id_nama_kelas=kmk.no_kelas_mk
left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
left join jadwal_ujian_mk jad on jad.id_ujian_mk=umk.id_ujian_mk
left join ruangan rg on rg.id_ruangan=jad.id_ruangan
where umk.id_kegiatan=47 and umk.id_fakultas=$kdfak and umk.id_semester=$id_smt
group by kmk.id_kurikulum_mk,umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,umk.tgl_ujian,
umk.jam_mulai,umk.jam_selesai,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang,ps.nm_singkat_prodi,nmk.nama_kelas) a
left join
(select pmk.id_kurikulum_mk, sum(case when pmk.status_apv_pengambilan_mk=1 then 1 else 0 end) as kls_terisi
from pengambilan_mk pmk
where pmk.id_semester=$id_smt and pmk.id_kelas_mk in (select id_kelas_mk from kelas_mk where id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak)) and pmk.status_apv_pengambilan_mk=1
group by pmk.id_kurikulum_mk) b
on a.id_kurikulum_mk=b.id_kurikulum_mk
left join
(select kelas_mk.id_kurikulum_mk, count(distinct ujian_mk_peserta.id_mhs) as ada from ujian_mk 
left join ujian_mk_peserta on ujian_mk.id_ujian_mk=ujian_mk_peserta.id_ujian_mk
left join kelas_mk on kelas_mk.id_kelas_mk=ujian_mk.id_kelas_mk
where kelas_mk.id_semester=$id_smt and kelas_mk.id_kelas_mk in
(select id_kelas_mk from kelas_mk where id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak))
and ujian_mk.id_kegiatan=47
group by kelas_mk.id_kurikulum_mk) c
on a.id_kurikulum_mk=c.id_kurikulum_mk
)
group by a.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nm_ujian_mk,tgl_ujian,jam,prodi,kls_terisi,ada
");
$smarty->assign('DATA', $data);

$jaf = getData("
select * from
(
(select umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,to_char(umk.tgl_ujian,'DD-MM-YYYY') as tgl_ujian,
umk.jam_mulai||' - '||umk.jam_selesai as jam,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang||'-'||ps.nm_singkat_prodi as prodi,
wm_concat('<li>'||
case when tim.status='1' then trim('<font color=\"blue\"><b>'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||'</b></font>')
else trim('<font color=\"black\">'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||'</font>') end) as tim,
nmk.nama_kelas
from ujian_mk umk
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join nama_kelas nmk on nmk.id_nama_kelas=kmk.no_kelas_mk
left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
left join jadwal_ujian_mk jad on jad.id_ujian_mk=umk.id_ujian_mk
left join ruangan rg on rg.id_ruangan=jad.id_ruangan
left join tim_pengawas_ujian tim on jad.id_jadwal_ujian_mk=tim.id_jadwal_ujian_mk
left join pengguna pgg on pgg.id_pengguna=tim.id_pengguna
where umk.id_kegiatan=47 and umk.id_fakultas=$kdfak and umk.id_semester=$id_smt
group by umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,umk.tgl_ujian,
umk.jam_mulai,umk.jam_selesai,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang,ps.nm_singkat_prodi,nmk.nama_kelas) a
left join
(select id_ujian_mk, count(*) as kls_terisi from ujian_mk_peserta group by id_ujian_mk) b
on a.id_ujian_mk=b.id_ujian_mk
)
");
$smarty->assign('T_MK', $jaf);

$tawar = getData("
select distinct jadwal_kelas.id_jadwal_kelas,kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
nama_kelas,kapasitas_kelas_mk,nm_ruangan,kapasitas_ujian,nm_jadwal_hari,
sum(case when status_apv_pengambilan_mk=1 then 1 else 0 end) as kls_terisi,kelas_mk.status,nm_jenjang||'-'||nm_singkat_prodi as prodi
from kelas_mk
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and pengambilan_mk.id_semester=$id_smt
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
where id_fakultas=$kdfak and kelas_mk.id_semester=$id_smt and kurikulum_mk.status_mkta not in (1,2) and
kelas_mk.id_kelas_mk not in (select id_kelas_mk from ujian_mk where id_kegiatan=47 and id_fakultas=$kdfak and id_semester=$id_smt)
group by jadwal_kelas.id_jadwal_kelas,kelas_mk.id_kelas_mk, kd_mata_kuliah, nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas, kapasitas_kelas_mk,
nm_jadwal_hari, kelas_mk.status, nm_singkat_prodi, nm_jenjang, nm_ruangan, kapasitas_ujian
");
$smarty->assign('TAWAR', $tawar);

$ujian_mk = getData("
select umk.id_ujian_mk,jad.id_jadwal_ujian_mk,umk.id_kelas_mk,umk.nm_ujian_mk,umk.tgl_ujian,umk.jam_mulai,umk.jam_selesai,jad.id_ruangan,
mk.kd_mata_kuliah||' - '||mk.nm_mata_kuliah as nm_mata_kuliah,nmk.nama_kelas,nm_jenjang||'-'||nm_singkat_prodi as prodi, rg.nm_ruangan, rg.kapasitas_ujian
from ujian_mk umk
left join jadwal_ujian_mk jad on umk.id_ujian_mk=jad.id_ujian_mk
left join ruangan rg on jad.id_ruangan=rg.id_ruangan
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join nama_kelas nmk on nmk.id_nama_kelas=kmk.no_kelas_mk
left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
where umk.id_ujian_mk=$id_ujian_mk
");
$smarty->assign('UMK', $ujian_mk);

$dosen = getData("
select * from
(
select tim.id_tim_pengawas_ujian,case when tim.status=1 then 'Penguji' else 'Pengawas' end as tipe,
case when (pgg.gelar_belakang is null or pgg.gelar_belakang = '') then trim(pgg.gelar_depan||' '||upper(nm_pengguna))
else trim(pgg.gelar_depan||' '||upper(nm_pengguna)||', '||pgg.gelar_belakang) end as pengawas,
case when pgg.id_role=4 then 'Dosen' else 'Pegawai' end as status
from tim_pengawas_ujian tim
left join jadwal_ujian_mk jad on jad.id_jadwal_ujian_mk=tim.id_jadwal_ujian_mk
left join pengguna pgg on tim.id_pengguna=pgg.id_pengguna
where jad.id_ujian_mk=$id_ujian_mk
)
order by tipe desc, status, pengawas
");
$smarty->assign('DOSEN', $dosen);
break;

// Penambahan pengawas Ujian MK
case 'add_tim':
$smarty->assign('disp1','none');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','block');
$smarty->assign('disp5','none');

$id_ujian_mk = $_GET['ujian'];
$id_jadwal_ujian_mk = $_GET['jad'];
$id_pengguna = $_GET['tim'];
$dosen = $_GET['dosen'];

$cek = getvar("select count(*) as ada from tim_pengawas_ujian where id_jadwal_ujian_mk=$id_jadwal_ujian_mk and status='1'");
$ada = $cek['ADA'];
if ($ada >= 1) {
	tambahdata("tim_pengawas_ujian","id_jadwal_ujian_mk,id_pengguna,status","$id_jadwal_ujian_mk,$id_pengguna,'0'");
	echo '<script>location.href="#ujian-ujianreguas!uas-reguler-fkg.php?action=pengawas&ujian='.$id_ujian_mk.'&jad='.$id_jadwal_ujian_mk.'";</script>';
} else {
	if ($dosen == 4) {
		tambahdata("tim_pengawas_ujian","id_jadwal_ujian_mk,id_pengguna,status","$id_jadwal_ujian_mk,$id_pengguna,'1'");
		echo '<script>location.href="#ujian-ujianreguas!uas-reguler-fkg.php?action=pengawas&ujian='.$id_ujian_mk.'&jad='.$id_jadwal_ujian_mk.'";</script>';
	} else {
		tambahdata("tim_pengawas_ujian","id_jadwal_ujian_mk,id_pengguna,status","$id_jadwal_ujian_mk,$id_pengguna,'0'");
		echo '<script>location.href="#ujian-ujianreguas!uas-reguler-fkg.php?action=pengawas&ujian='.$id_ujian_mk.'&jad='.$id_jadwal_ujian_mk.'";</script>';
	}
}
break;

// Hapus pengawas Ujian MK
case 'del_tim':
$smarty->assign('disp1','none');
$smarty->assign('disp2','none');
$smarty->assign('disp3','block');
$smarty->assign('disp4','none');
$smarty->assign('disp5','none');

$id_ujian_mk = $_GET['ujian'];
$id_tim_pengawas_ujian = $_GET['tim'];
deleteData("delete from tim_pengawas_ujian where id_tim_pengawas_ujian=$id_tim_pengawas_ujian");

echo '<script>location.href="#ujian-ujianreguas!uas-reguler-fkg.php?action=updateview&ujian='.$id_ujian_mk.'";</script>';
break;

case 'del':
$id_ujian_mk = $_POST['hapus'];
$del = getvar("select id_jadwal_ujian_mk from jadwal_ujian_mk where id_ujian_mk=$id_ujian_mk");
$id_jadwal_ujian_mk = $del['ID_JADWAL_UJIAN_MK'];
deleteData("delete from ujian_mk_peserta where id_ujian_mk=$id_ujian_mk");
deleteData("delete from tim_pengawas_ujian where id_jadwal_ujian_mk=$id_jadwal_ujian_mk");
deleteData("delete from jadwal_ujian_mk where id_ujian_mk=$id_ujian_mk");
deleteData("delete from ujian_mk where id_ujian_mk=$id_ujian_mk");

echo ' ';
exit();
break;

// Daftar MK yang sudah dijadwalkan Ujian MK
case 'tampil':
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');
$smarty->assign('disp5','none');

$data = getData("
select a.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nm_ujian_mk,tgl_ujian,jam,prodi,round(b.kls_terisi/count(a.id_ujian_mk)) as bagi,ada,
wm_concat('<li>'||nama_kelas||' - '||nm_ruangan||' ('||kapasitas_ujian||')') as nama_kelas,sum(kapasitas_ujian) as kapasitas_ujian, b.kls_terisi from
(
(select kmk.id_kurikulum_mk,umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,to_char(umk.tgl_ujian,'DD-MM-YYYY') as tgl_ujian,
umk.jam_mulai||' - '||umk.jam_selesai as jam,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang||'-'||ps.nm_singkat_prodi as prodi,nmk.nama_kelas
from ujian_mk umk
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join nama_kelas nmk on nmk.id_nama_kelas=kmk.no_kelas_mk
left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
left join jadwal_ujian_mk jad on jad.id_ujian_mk=umk.id_ujian_mk
left join ruangan rg on rg.id_ruangan=jad.id_ruangan
where umk.id_kegiatan=47 and umk.id_fakultas=$kdfak and umk.id_semester=$id_smt
group by kmk.id_kurikulum_mk,umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,umk.tgl_ujian,
umk.jam_mulai,umk.jam_selesai,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang,ps.nm_singkat_prodi,nmk.nama_kelas) a
left join
(select pmk.id_kurikulum_mk, sum(case when pmk.status_apv_pengambilan_mk=1 then 1 else 0 end) as kls_terisi
from pengambilan_mk pmk
where pmk.id_semester=$id_smt and pmk.id_kelas_mk in (select id_kelas_mk from kelas_mk where id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak)) and pmk.status_apv_pengambilan_mk=1
group by pmk.id_kurikulum_mk) b
on a.id_kurikulum_mk=b.id_kurikulum_mk
left join
(select kelas_mk.id_kurikulum_mk, count(distinct ujian_mk_peserta.id_mhs) as ada from ujian_mk 
left join ujian_mk_peserta on ujian_mk.id_ujian_mk=ujian_mk_peserta.id_ujian_mk
left join kelas_mk on kelas_mk.id_kelas_mk=ujian_mk.id_kelas_mk
where kelas_mk.id_semester=$id_smt and kelas_mk.id_kelas_mk in
(select id_kelas_mk from kelas_mk where id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak))
and ujian_mk.id_kegiatan=47
group by kelas_mk.id_kurikulum_mk) c
on a.id_kurikulum_mk=c.id_kurikulum_mk
)
group by a.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nm_ujian_mk,tgl_ujian,jam,prodi,kls_terisi,ada
");
$smarty->assign('DATA', $data);

$jaf = getData("
select * from
(
(select umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,to_char(umk.tgl_ujian,'DD-MM-YYYY') as tgl_ujian,
umk.jam_mulai||' - '||umk.jam_selesai as jam,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang||'-'||ps.nm_singkat_prodi as prodi,
wm_concat('<li>'||
case when tim.status='1' then trim('<font color=\"blue\"><b>'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||'</b></font>')
else trim('<font color=\"black\">'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||'</font>') end) as tim,
nmk.nama_kelas
from ujian_mk umk
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join nama_kelas nmk on nmk.id_nama_kelas=kmk.no_kelas_mk
left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
left join jadwal_ujian_mk jad on jad.id_ujian_mk=umk.id_ujian_mk
left join ruangan rg on rg.id_ruangan=jad.id_ruangan
left join tim_pengawas_ujian tim on jad.id_jadwal_ujian_mk=tim.id_jadwal_ujian_mk
left join pengguna pgg on pgg.id_pengguna=tim.id_pengguna
where umk.id_kegiatan=47 and umk.id_fakultas=$kdfak and umk.id_semester=$id_smt
group by umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,umk.tgl_ujian,
umk.jam_mulai,umk.jam_selesai,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang,ps.nm_singkat_prodi,nmk.nama_kelas) a
left join
(select id_ujian_mk, count(*) as kls_terisi from ujian_mk_peserta group by id_ujian_mk) b
on a.id_ujian_mk=b.id_ujian_mk
)
");
$smarty->assign('T_MK', $jaf);

$tawar = getData("
select distinct jadwal_kelas.id_jadwal_kelas,kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
nama_kelas,kapasitas_kelas_mk,nm_ruangan,kapasitas_ujian,nm_jadwal_hari,
sum(case when status_apv_pengambilan_mk=1 then 1 else 0 end) as kls_terisi,kelas_mk.status,nm_jenjang||'-'||nm_singkat_prodi as prodi
from kelas_mk
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and pengambilan_mk.id_semester=$id_smt
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
where id_fakultas=$kdfak and kelas_mk.id_semester=$id_smt and kurikulum_mk.status_mkta not in (1,2) and
kelas_mk.id_kelas_mk not in (select id_kelas_mk from ujian_mk where id_kegiatan=47 and id_fakultas=$kdfak and id_semester=$id_smt)
group by jadwal_kelas.id_jadwal_kelas,kelas_mk.id_kelas_mk, kd_mata_kuliah, nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas, kapasitas_kelas_mk,
nm_jadwal_hari, kelas_mk.status, nm_singkat_prodi, nm_jenjang, nm_ruangan, kapasitas_ujian
");
$smarty->assign('TAWAR', $tawar);
break;

// Daftar MK yang belum dijadwalkan Ujian MK
case 'penjadwalan':
$smarty->assign('disp1','none');
$smarty->assign('disp2','block');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');
$smarty->assign('disp5','none');

$data = getData("
select a.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nm_ujian_mk,tgl_ujian,jam,prodi,round(b.kls_terisi/count(a.id_ujian_mk)) as bagi,ada,
wm_concat('<li>'||nama_kelas||' - '||nm_ruangan||' ('||kapasitas_ujian||')') as nama_kelas,sum(kapasitas_ujian) as kapasitas_ujian, b.kls_terisi from
(
(select kmk.id_kurikulum_mk,umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,to_char(umk.tgl_ujian,'DD-MM-YYYY') as tgl_ujian,
umk.jam_mulai||' - '||umk.jam_selesai as jam,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang||'-'||ps.nm_singkat_prodi as prodi,nmk.nama_kelas
from ujian_mk umk
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join nama_kelas nmk on nmk.id_nama_kelas=kmk.no_kelas_mk
left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
left join jadwal_ujian_mk jad on jad.id_ujian_mk=umk.id_ujian_mk
left join ruangan rg on rg.id_ruangan=jad.id_ruangan
where umk.id_kegiatan=47 and umk.id_fakultas=$kdfak and umk.id_semester=$id_smt
group by kmk.id_kurikulum_mk,umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,umk.tgl_ujian,
umk.jam_mulai,umk.jam_selesai,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang,ps.nm_singkat_prodi,nmk.nama_kelas) a
left join
(select pmk.id_kurikulum_mk, sum(case when pmk.status_apv_pengambilan_mk=1 then 1 else 0 end) as kls_terisi
from pengambilan_mk pmk
where pmk.id_semester=$id_smt and pmk.id_kelas_mk in (select id_kelas_mk from kelas_mk where id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak)) and pmk.status_apv_pengambilan_mk=1
group by pmk.id_kurikulum_mk) b
on a.id_kurikulum_mk=b.id_kurikulum_mk
left join
(select kelas_mk.id_kurikulum_mk, count(distinct ujian_mk_peserta.id_mhs) as ada from ujian_mk 
left join ujian_mk_peserta on ujian_mk.id_ujian_mk=ujian_mk_peserta.id_ujian_mk
left join kelas_mk on kelas_mk.id_kelas_mk=ujian_mk.id_kelas_mk
where kelas_mk.id_semester=$id_smt and kelas_mk.id_kelas_mk in
(select id_kelas_mk from kelas_mk where id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak))
and ujian_mk.id_kegiatan=47
group by kelas_mk.id_kurikulum_mk) c
on a.id_kurikulum_mk=c.id_kurikulum_mk
)
group by a.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nm_ujian_mk,tgl_ujian,jam,prodi,kls_terisi,ada
");
$smarty->assign('DATA', $data);

$jaf = getData("
select * from
(
(select umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,to_char(umk.tgl_ujian,'DD-MM-YYYY') as tgl_ujian,
umk.jam_mulai||' - '||umk.jam_selesai as jam,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang||'-'||ps.nm_singkat_prodi as prodi,
wm_concat('<li>'||
case when tim.status='1' then trim('<font color=\"blue\"><b>'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||'</b></font>')
else trim('<font color=\"black\">'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||'</font>') end) as tim,
nmk.nama_kelas
from ujian_mk umk
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join nama_kelas nmk on nmk.id_nama_kelas=kmk.no_kelas_mk
left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
left join jadwal_ujian_mk jad on jad.id_ujian_mk=umk.id_ujian_mk
left join ruangan rg on rg.id_ruangan=jad.id_ruangan
left join tim_pengawas_ujian tim on jad.id_jadwal_ujian_mk=tim.id_jadwal_ujian_mk
left join pengguna pgg on pgg.id_pengguna=tim.id_pengguna
where umk.id_kegiatan=47 and umk.id_fakultas=$kdfak and umk.id_semester=$id_smt
group by umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,umk.tgl_ujian,
umk.jam_mulai,umk.jam_selesai,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang,ps.nm_singkat_prodi,nmk.nama_kelas) a
left join
(select id_ujian_mk, count(*) as kls_terisi from ujian_mk_peserta group by id_ujian_mk) b
on a.id_ujian_mk=b.id_ujian_mk
)
");
$smarty->assign('T_MK', $jaf);

$tawar = getData("
select distinct jadwal_kelas.id_jadwal_kelas,kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
nama_kelas,kapasitas_kelas_mk,nm_ruangan,kapasitas_ujian,nm_jadwal_hari,
sum(case when status_apv_pengambilan_mk=1 then 1 else 0 end) as kls_terisi,kelas_mk.status,nm_jenjang||'-'||nm_singkat_prodi as prodi
from kelas_mk
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and pengambilan_mk.id_semester=$id_smt
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
where id_fakultas=$kdfak and kelas_mk.id_semester=$id_smt and kurikulum_mk.status_mkta not in (1,2) and
kelas_mk.id_kelas_mk not in (select id_kelas_mk from ujian_mk where id_kegiatan=47 and id_fakultas=$kdfak and id_semester=$id_smt)
group by jadwal_kelas.id_jadwal_kelas,kelas_mk.id_kelas_mk, kd_mata_kuliah, nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas, kapasitas_kelas_mk,
nm_jadwal_hari, kelas_mk.status, nm_singkat_prodi, nm_jenjang, nm_ruangan, kapasitas_ujian
");
$smarty->assign('TAWAR', $tawar);
break;

// Peserta Ujian MK
case 'peserta':
$smarty->assign('disp1','none');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');
$smarty->assign('disp5','block');

$ruang = getData("
select id_ruangan,nm_ruangan||' - '||kapasitas_ujian as ruang from ruangan
left join gedung on gedung.id_gedung=ruangan.id_gedung
left join gedung_kuliah on gedung.id_gedung=gedung_kuliah.id_gedung
where gedung.id_fakultas=$kdfak and ruangan.kapasitas_ujian > 0 order by nm_ruangan
");
$smarty->assign('T_RUANG', $ruang);

$data = getData("
select a.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nm_ujian_mk,tgl_ujian,jam,prodi,round(b.kls_terisi/count(a.id_ujian_mk)) as bagi,ada,
wm_concat('<li>'||nama_kelas||' - '||nm_ruangan||' ('||kapasitas_ujian||')') as nama_kelas,sum(kapasitas_ujian) as kapasitas_ujian, b.kls_terisi from
(
(select kmk.id_kurikulum_mk,umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,to_char(umk.tgl_ujian,'DD-MM-YYYY') as tgl_ujian,
umk.jam_mulai||' - '||umk.jam_selesai as jam,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang||'-'||ps.nm_singkat_prodi as prodi,nmk.nama_kelas
from ujian_mk umk
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join nama_kelas nmk on nmk.id_nama_kelas=kmk.no_kelas_mk
left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
left join jadwal_ujian_mk jad on jad.id_ujian_mk=umk.id_ujian_mk
left join ruangan rg on rg.id_ruangan=jad.id_ruangan
where umk.id_kegiatan=47 and umk.id_fakultas=$kdfak and umk.id_semester=$id_smt
group by kmk.id_kurikulum_mk,umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,umk.tgl_ujian,
umk.jam_mulai,umk.jam_selesai,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang,ps.nm_singkat_prodi,nmk.nama_kelas) a
left join
(select pmk.id_kurikulum_mk, sum(case when pmk.status_apv_pengambilan_mk=1 then 1 else 0 end) as kls_terisi
from pengambilan_mk pmk
where pmk.id_semester=$id_smt and pmk.id_kelas_mk in (select id_kelas_mk from kelas_mk where id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak)) and pmk.status_apv_pengambilan_mk=1
group by pmk.id_kurikulum_mk) b
on a.id_kurikulum_mk=b.id_kurikulum_mk
left join
(select kelas_mk.id_kurikulum_mk, count(distinct ujian_mk_peserta.id_mhs) as ada from ujian_mk 
left join ujian_mk_peserta on ujian_mk.id_ujian_mk=ujian_mk_peserta.id_ujian_mk
left join kelas_mk on kelas_mk.id_kelas_mk=ujian_mk.id_kelas_mk
where kelas_mk.id_semester=$id_smt and kelas_mk.id_kelas_mk in
(select id_kelas_mk from kelas_mk where id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak))
and ujian_mk.id_kegiatan=47
group by kelas_mk.id_kurikulum_mk) c
on a.id_kurikulum_mk=c.id_kurikulum_mk
)
group by a.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nm_ujian_mk,tgl_ujian,jam,prodi,kls_terisi,ada
");
$smarty->assign('DATA', $data);

$jaf = getData("
select * from
(
(select umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,to_char(umk.tgl_ujian,'DD-MM-YYYY') as tgl_ujian,
umk.jam_mulai||' - '||umk.jam_selesai as jam,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang||'-'||ps.nm_singkat_prodi as prodi,
wm_concat('<li>'||
case when tim.status='1' then trim('<font color=\"blue\"><b>'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||'</b></font>')
else trim('<font color=\"black\">'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||'</font>') end) as tim,
nmk.nama_kelas
from ujian_mk umk
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join nama_kelas nmk on nmk.id_nama_kelas=kmk.no_kelas_mk
left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
left join jadwal_ujian_mk jad on jad.id_ujian_mk=umk.id_ujian_mk
left join ruangan rg on rg.id_ruangan=jad.id_ruangan
left join tim_pengawas_ujian tim on jad.id_jadwal_ujian_mk=tim.id_jadwal_ujian_mk
left join pengguna pgg on pgg.id_pengguna=tim.id_pengguna
where umk.id_kegiatan=47 and umk.id_fakultas=$kdfak and umk.id_semester=$id_smt
group by umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,umk.tgl_ujian,
umk.jam_mulai,umk.jam_selesai,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang,ps.nm_singkat_prodi,nmk.nama_kelas) a
left join
(select id_ujian_mk, count(*) as kls_terisi from ujian_mk_peserta group by id_ujian_mk) b
on a.id_ujian_mk=b.id_ujian_mk
)
");
$smarty->assign('T_MK', $jaf);

$tawar = getData("
select distinct jadwal_kelas.id_jadwal_kelas,kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
nama_kelas,kapasitas_kelas_mk,nm_ruangan,kapasitas_ujian,nm_jadwal_hari,
sum(case when status_apv_pengambilan_mk=1 then 1 else 0 end) as kls_terisi,kelas_mk.status,nm_jenjang||'-'||nm_singkat_prodi as prodi
from kelas_mk
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and pengambilan_mk.id_semester=$id_smt
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
where id_fakultas=$kdfak and kelas_mk.id_semester=$id_smt and kurikulum_mk.status_mkta not in (1,2) and
kelas_mk.id_kelas_mk not in (select id_kelas_mk from ujian_mk where id_kegiatan=47 and id_fakultas=$kdfak and id_semester=$id_smt)
group by jadwal_kelas.id_jadwal_kelas,kelas_mk.id_kelas_mk, kd_mata_kuliah, nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas, kapasitas_kelas_mk,
nm_jadwal_hari, kelas_mk.status, nm_singkat_prodi, nm_jenjang, nm_ruangan, kapasitas_ujian
");
$smarty->assign('TAWAR', $tawar);

$ujian_mk = getData("
select umk.id_ujian_mk,jad.id_jadwal_ujian_mk,umk.id_kelas_mk,umk.nm_ujian_mk,umk.tgl_ujian,umk.jam_mulai,umk.jam_selesai,jad.id_ruangan,
mk.kd_mata_kuliah||' - '||mk.nm_mata_kuliah as nm_mata_kuliah,nmk.nama_kelas,nm_jenjang||'-'||nm_singkat_prodi as prodi, rg.nm_ruangan, rg.kapasitas_ujian
from ujian_mk umk
left join jadwal_ujian_mk jad on umk.id_ujian_mk=jad.id_ujian_mk
left join ruangan rg on jad.id_ruangan=rg.id_ruangan
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join nama_kelas nmk on nmk.id_nama_kelas=kmk.no_kelas_mk
left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
where umk.id_ujian_mk=$id_ujian_mk
");
$smarty->assign('UMK', $ujian_mk);

$dosen = getData("
select * from
(
select tim.id_tim_pengawas_ujian,case when tim.status=1 then 'Penguji' else 'Pengawas' end as tipe,
case when (pgg.gelar_belakang is null or pgg.gelar_belakang = '') then trim(pgg.gelar_depan||' '||upper(nm_pengguna))
else trim(pgg.gelar_depan||' '||upper(nm_pengguna)||', '||pgg.gelar_belakang) end as pengawas,
case when pgg.id_role=4 then 'Dosen' else 'Pegawai' end as status
from tim_pengawas_ujian tim
left join jadwal_ujian_mk jad on jad.id_jadwal_ujian_mk=tim.id_jadwal_ujian_mk
left join pengguna pgg on tim.id_pengguna=pgg.id_pengguna
where jad.id_ujian_mk=$id_ujian_mk
)
order by tipe desc, status, pengawas
");
$smarty->assign('DOSEN', $dosen);
break;

// Hapus peserta Ujian MK
case 'reset_pst':
$id_kurikulum_mk = $_POST['reset'];

deleteData("
delete from ujian_mk_peserta where id_ujian_mk in
(select distinct umk.id_ujian_mk
from ujian_mk_peserta pst
left join ujian_mk umk on umk.id_ujian_mk=pst.id_ujian_mk
where umk.id_kegiatan=47 and
umk.id_kelas_mk in 
(select id_kelas_mk from kelas_mk where id_semester=$id_smt and id_kurikulum_mk=$id_kurikulum_mk and id_program_studi in
(select id_program_studi from program_studi where id_fakultas=$kdfak))
group by umk.id_ujian_mk)
");

echo ' ';
exit();
break;

// Ploting peserta Ujian MK
case 'add_pst':
$smarty->assign('disp1','none');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');
$smarty->assign('disp5','block');

$id_kurikulum_mk = $_GET['kur'];

$cek = getvar("
select masuk, kls_terisi from
(
(
select pmk.id_kurikulum_mk, sum(case when pmk.status_apv_pengambilan_mk=1 then 1 else 0 end) as kls_terisi
from pengambilan_mk pmk
where pmk.id_semester=$id_smt and pmk.id_kurikulum_mk=$id_kurikulum_mk and pmk.status_apv_pengambilan_mk=1
group by pmk.id_kurikulum_mk
) a
left join
(
select kelas_mk.id_kurikulum_mk, count(ujian_mk_peserta.id_mhs) as masuk from ujian_mk_peserta
left join ujian_mk on ujian_mk.id_ujian_mk=ujian_mk_peserta.id_ujian_mk
left join kelas_mk on kelas_mk.id_kelas_mk=ujian_mk.id_kelas_mk
where kelas_mk.id_kurikulum_mk=$id_kurikulum_mk and ujian_mk.id_kegiatan=47 and ujian_mk.id_semester=$id_smt
group by kelas_mk.id_kurikulum_mk
) b
on a.id_kurikulum_mk=b.id_kurikulum_mk
)
");

if ($cek['MASUK'] == $cek['KLS_TERISI']) {
	echo '<script>location.href="#ujian-ujianreguas!uas-reguler-fkg.php?action=peserta";</script>';
} else {
$umk = "
select distinct umk.id_ujian_mk,kls.nama_kelas
from ujian_mk umk
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join nama_kelas kls on kmk.no_kelas_mk=kls.id_nama_kelas
where umk.id_kegiatan=47 and kmk.id_semester=$id_smt and kmk.id_kurikulum_mk=$id_kurikulum_mk
and umk.id_ujian_mk not in (select id_ujian_mk from ujian_mk_peserta)
group by umk.id_ujian_mk,kls.nama_kelas
order by kls.nama_kelas
";
$result = $db->Query($umk)or die("salah kueri umk ");
while($r = $db->FetchRow()) {
	$id_ujian_mk = $r[0];

	$kpst = "select rg.kapasitas_ujian from jadwal_ujian_mk jad left join ruangan rg on rg.id_ruangan=jad.id_ruangan where jad.id_ujian_mk=$id_ujian_mk";
	$result = $db->Query($kpst)or die("salah kueri kpst ");
	while($r = $db->FetchRow()) {
		$limit = $r[0];

		$add_pst = "
		select pmk.id_mhs from pengambilan_mk pmk 
		left join mahasiswa mhs on mhs.id_mhs=pmk.id_mhs
		left join kelas_mk kmk on kmk.id_kelas_mk=pmk.id_kelas_mk
		left join nama_kelas kls on kmk.no_kelas_mk=kls.id_nama_kelas
		where pmk.id_semester=$id_smt and pmk.id_kurikulum_mk=$id_kurikulum_mk and pmk.id_mhs not in 
		(select pst.id_mhs from ujian_mk_peserta pst
		left join ujian_mk umk on umk.id_ujian_mk=pst.id_ujian_mk
		left join kelas_mk kls on kls.id_kelas_mk=umk.id_kelas_mk
		where kls.id_kurikulum_mk=$id_kurikulum_mk and umk.id_kegiatan=47 and umk.id_semester=$id_smt)
		and rownum<=$limit
		order by kls.nama_kelas, mhs.nim_mhs
		";
		$result1 = $db->Query($add_pst)or die("salah kueri proses add_pst ");
		while($r1 = $db->FetchRow()) {
			$id_mhs = $r1[0];

			tambahdata("ujian_mk_peserta","id_ujian_mk,id_mhs","$id_ujian_mk,$id_mhs");
		}
	}
	}
}
	echo '<script>location.href="javascript:window.location.reload()";</script>';
break;

// Detail peserta Ujian MK
case 'list':
$smarty->assign('disp1','none');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');
$smarty->assign('disp5','block');

$id_kelas_mk = $_GET['kelas'];
$id_ujian_mk = $_GET['ujian'];

$cek = getvar("
select count(pst.id_ujian_mk_peserta) as ada
from ujian_mk_peserta pst
left join ujian_mk umk on umk.id_ujian_mk=pst.id_ujian_mk
where umk.id_ujian_mk=$id_ujian_mk and umk.id_kegiatan=47
");
$ada = $cek['ADA'];

if ($ada >= 1) {
$list = getData("
select a.id_mhs, a.nim_mhs, a.nm_pengguna, case when b.id_mhs is not null then 'checked' else '' end as tanda from
(
select pmk.id_mhs, mhs.nim_mhs, upper(nm_pengguna) as nm_pengguna
from pengambilan_mk pmk
left join mahasiswa mhs on mhs.id_mhs=pmk.id_mhs
left join pengguna pgg on pgg.id_pengguna=mhs.id_pengguna
where pmk.id_kelas_mk=$id_kelas_mk and pmk.status_apv_pengambilan_mk=1
) a left join
(
select pst.id_mhs
from ujian_mk_peserta pst
left join ujian_mk umk on umk.id_ujian_mk=pst.id_ujian_mk
where umk.id_kelas_mk=$id_kelas_mk and umk.id_kegiatan=47
) b on a.id_mhs=b.id_mhs
order by nim_mhs
");
} else {
$list = getData("
select a.id_mhs, a.nim_mhs, a.nm_pengguna, 'checked' as tanda from
(
select pmk.id_mhs, mhs.nim_mhs, upper(nm_pengguna) as nm_pengguna
from pengambilan_mk pmk
left join mahasiswa mhs on mhs.id_mhs=pmk.id_mhs
left join pengguna pgg on pgg.id_pengguna=mhs.id_pengguna
where pmk.id_kelas_mk=$id_kelas_mk and pmk.status_apv_pengambilan_mk=1
) a left join
(
select pst.id_mhs
from ujian_mk_peserta pst
left join ujian_mk umk on umk.id_ujian_mk=pst.id_ujian_mk
where umk.id_kelas_mk=$id_kelas_mk and umk.id_kegiatan=47
) b on a.id_mhs=b.id_mhs
order by nim_mhs
");
}
$smarty->assign('DAFTAR', $list);

$ruang = getData("
select id_ruangan,nm_ruangan||' - '||kapasitas_ujian as ruang from ruangan
left join gedung on gedung.id_gedung=ruangan.id_gedung
left join gedung_kuliah on gedung.id_gedung=gedung_kuliah.id_gedung
where gedung.id_fakultas=$kdfak and ruangan.kapasitas_ujian > 0 order by nm_ruangan
");
$smarty->assign('T_RUANG', $ruang);

$data = getData("
select a.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nm_ujian_mk,tgl_ujian,jam,prodi,round(b.kls_terisi/count(a.id_ujian_mk)) as bagi,ada,
wm_concat('<li>'||nama_kelas||' - '||nm_ruangan||' ('||kapasitas_ujian||')') as nama_kelas,sum(kapasitas_ujian) as kapasitas_ujian, b.kls_terisi from
(
(select kmk.id_kurikulum_mk,umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,to_char(umk.tgl_ujian,'DD-MM-YYYY') as tgl_ujian,
umk.jam_mulai||' - '||umk.jam_selesai as jam,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang||'-'||ps.nm_singkat_prodi as prodi,nmk.nama_kelas
from ujian_mk umk
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join nama_kelas nmk on nmk.id_nama_kelas=kmk.no_kelas_mk
left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
left join jadwal_ujian_mk jad on jad.id_ujian_mk=umk.id_ujian_mk
left join ruangan rg on rg.id_ruangan=jad.id_ruangan
where umk.id_kegiatan=47 and umk.id_fakultas=$kdfak and umk.id_semester=$id_smt
group by kmk.id_kurikulum_mk,umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,umk.tgl_ujian,
umk.jam_mulai,umk.jam_selesai,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang,ps.nm_singkat_prodi,nmk.nama_kelas) a
left join
(select pmk.id_kurikulum_mk, sum(case when pmk.status_apv_pengambilan_mk=1 then 1 else 0 end) as kls_terisi
from pengambilan_mk pmk
where pmk.id_semester=$id_smt and pmk.id_kelas_mk in (select id_kelas_mk from kelas_mk where id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak)) and pmk.status_apv_pengambilan_mk=1
group by pmk.id_kurikulum_mk) b
on a.id_kurikulum_mk=b.id_kurikulum_mk
left join
(select kelas_mk.id_kurikulum_mk, count(distinct ujian_mk_peserta.id_mhs) as ada from ujian_mk 
left join ujian_mk_peserta on ujian_mk.id_ujian_mk=ujian_mk_peserta.id_ujian_mk
left join kelas_mk on kelas_mk.id_kelas_mk=ujian_mk.id_kelas_mk
where kelas_mk.id_semester=$id_smt and kelas_mk.id_kelas_mk in
(select id_kelas_mk from kelas_mk where id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak))
and ujian_mk.id_kegiatan=47
group by kelas_mk.id_kurikulum_mk) c
on a.id_kurikulum_mk=c.id_kurikulum_mk
)
group by a.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nm_ujian_mk,tgl_ujian,jam,prodi,kls_terisi,ada
");
$smarty->assign('DATA', $data);

$jaf = getData("
select * from
(
(select umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,to_char(umk.tgl_ujian,'DD-MM-YYYY') as tgl_ujian,
umk.jam_mulai||' - '||umk.jam_selesai as jam,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang||'-'||ps.nm_singkat_prodi as prodi,
wm_concat('<li>'||
case when tim.status='1' then trim('<font color=\"blue\"><b>'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||'</b></font>')
else trim('<font color=\"black\">'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||'</font>') end) as tim,
nmk.nama_kelas
from ujian_mk umk
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join nama_kelas nmk on nmk.id_nama_kelas=kmk.no_kelas_mk
left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
left join jadwal_ujian_mk jad on jad.id_ujian_mk=umk.id_ujian_mk
left join ruangan rg on rg.id_ruangan=jad.id_ruangan
left join tim_pengawas_ujian tim on jad.id_jadwal_ujian_mk=tim.id_jadwal_ujian_mk
left join pengguna pgg on pgg.id_pengguna=tim.id_pengguna
where umk.id_kegiatan=47 and umk.id_fakultas=$kdfak and umk.id_semester=$id_smt
group by umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kur.kredit_semester,umk.nm_ujian_mk,umk.tgl_ujian,
umk.jam_mulai,umk.jam_selesai,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang,ps.nm_singkat_prodi,nmk.nama_kelas) a
left join
(select id_ujian_mk, count(*) as kls_terisi from ujian_mk_peserta group by id_ujian_mk) b
on a.id_ujian_mk=b.id_ujian_mk
)
");
$smarty->assign('T_MK', $jaf);

$tawar = getData("
select distinct jadwal_kelas.id_jadwal_kelas,kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
nama_kelas,kapasitas_kelas_mk,nm_ruangan,kapasitas_ujian,nm_jadwal_hari,
sum(case when status_apv_pengambilan_mk=1 then 1 else 0 end) as kls_terisi,kelas_mk.status,nm_jenjang||'-'||nm_singkat_prodi as prodi
from kelas_mk
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and pengambilan_mk.id_semester=$id_smt
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
where id_fakultas=$kdfak and kelas_mk.id_semester=$id_smt and kurikulum_mk.status_mkta not in (1,2) and
kelas_mk.id_kelas_mk not in (select id_kelas_mk from ujian_mk where id_kegiatan=47 and id_fakultas=$kdfak and id_semester=$id_smt)
group by jadwal_kelas.id_jadwal_kelas,kelas_mk.id_kelas_mk, kd_mata_kuliah, nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas, kapasitas_kelas_mk,
nm_jadwal_hari, kelas_mk.status, nm_singkat_prodi, nm_jenjang, nm_ruangan, kapasitas_ujian
");
$smarty->assign('TAWAR', $tawar);

$ujian_mk = getData("
select umk.id_ujian_mk,jad.id_jadwal_ujian_mk,umk.id_kelas_mk,umk.nm_ujian_mk,umk.tgl_ujian,umk.jam_mulai,umk.jam_selesai,jad.id_ruangan,
mk.kd_mata_kuliah||' - '||mk.nm_mata_kuliah as nm_mata_kuliah,nmk.nama_kelas,nm_jenjang||'-'||nm_singkat_prodi as prodi, rg.nm_ruangan, rg.kapasitas_ujian
from ujian_mk umk
left join jadwal_ujian_mk jad on umk.id_ujian_mk=jad.id_ujian_mk
left join ruangan rg on jad.id_ruangan=rg.id_ruangan
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join nama_kelas nmk on nmk.id_nama_kelas=kmk.no_kelas_mk
left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
where umk.id_ujian_mk=$id_ujian_mk
");
$smarty->assign('UMK', $ujian_mk);

$dosen = getData("
select * from
(
select tim.id_tim_pengawas_ujian,case when tim.status=1 then 'Penguji' else 'Pengawas' end as tipe,
case when (pgg.gelar_belakang is null or pgg.gelar_belakang = '') then trim(pgg.gelar_depan||' '||upper(nm_pengguna))
else trim(pgg.gelar_depan||' '||upper(nm_pengguna)||', '||pgg.gelar_belakang) end as pengawas,
case when pgg.id_role=4 then 'Dosen' else 'Pegawai' end as status
from tim_pengawas_ujian tim
left join jadwal_ujian_mk jad on jad.id_jadwal_ujian_mk=tim.id_jadwal_ujian_mk
left join pengguna pgg on tim.id_pengguna=pgg.id_pengguna
where jad.id_ujian_mk=$id_ujian_mk
)
order by tipe desc, status, pengawas
");
$smarty->assign('DOSEN', $dosen);
break;

// Ploting peserta Ujian MK manual
case 'manual':
$smarty->assign('disp1','none');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');
$smarty->assign('disp5','block');

$id_ujian_mk = $_POST['ujian'];
$mhs = $_POST['mhs'];

deleteData("delete from ujian_mk_peserta where id_ujian_mk=$id_ujian_mk");

foreach ($mhs as $id_mhs){
	tambahdata("ujian_mk_peserta","id_ujian_mk,id_mhs","$id_ujian_mk,$id_mhs");
}

echo '<script>location.href="#ujian-ujianreguas!uas-reguler-fkg.php?action=tampil";</script>';
break;
}

$smarty->display('uas-reguler-fkg.tpl');

?>
