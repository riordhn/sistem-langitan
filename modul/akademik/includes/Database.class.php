<?php
/**
 * Class untuk menangani segala yang mengenai database
 *
 * @author UUAC23
 */
class database
{
    private $connection;
    private $statement;
    
	// Db Configuration
    private $username = 'fisip';
    private $password = 'fisip';
    private $server   = 'localhost';
    private $port     = '1521';
    private $db_name  = 'XE';
    
    /**
     * Perintah untuk menjalankan koneksi ke database oracle
     */
    public function connect()
    {
        // Connection Settings
        $oci_tnsc = "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST={$this->server})(PORT={$this->port}))(CONNECT_DATA=(SERVER = DEDICATED)(SERVICE_NAME = {$this->db_name})))";
        $this->connection = oci_connect($this->username, $this->password, $oci_tnsc);
    }

    /**
     * Mengeksekusi query untuk dilakukan fetch row
     * @param type $query 
     */
    public function execute($query)
    {
        if (!is_null($this->statement))
        {
            oci_free_statement($this->statement);
        }
        
        $this->statement = oci_parse($this->connection, $query);
        return oci_execute($this->statement);
    }
    
    /**
     * Fetch row dari query yang terakhir di eksekusi
     */
    public function fetch()
    {
        if (!is_null($this->statement))
        {
            return oci_fetch_assoc($this->statement);
        }
        else
        {
            return null;
        }
    }
    
    public function single()
    {
        $row = oci_fetch_row($this->statement);
        return $row[0];
    }
    
    /**
     * Menutup koneksi database
     */
    public function close()
    {
        if (!is_null($this->statement))
        {
            oci_free_statement($this->statement);
        }
        
        if (!is_null($this->connection))
        {
            return oci_close($this->connection);
        }
    }
}
?>
