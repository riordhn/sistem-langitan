<?php
include ('config.php');
$kdfak=$user->ID_FAKULTAS; 

$ebsbed = $db->QueryToArray("SELECT * FROM PROGRAM_STUDI
								RIGHT JOIN EPSBED_MSPST ON EPSBED_MSPST.ID_EPSBED_MSPST = PROGRAM_STUDI.ID_EPSBED_MSPST
								WHERE PROGRAM_STUDI.ID_FAKULTAS = {$kdfak}
								AND PROGRAM_STUDI.STATUS_AKTIF_PRODI = 1
							ORDER BY KDPSTMSPST ASC");

$smarty->assign('ebsbed', $ebsbed);
$smarty->display('ebsbed/ebsbed-mspst.tpl');
?>