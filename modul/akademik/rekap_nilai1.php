<?php
/*
Yudi Sulistya 06/01/2012
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$kdprodi=$user->ID_PROGRAM_STUDI;

$id_pengguna= $user->ID_PENGGUNA; 
if ($user->ID_FAKULTAS == 8) { // FST
header('Location: /modul/akademik/fst/rekap_nilai.php');
exit;
}

$fak=getvar("select fakultas.id_fakultas, upper(fakultas.nm_fakultas) as nm_fakultas from pegawai 
left join unit_kerja on pegawai.id_unit_kerja=unit_kerja.id_unit_kerja 
left join fakultas on unit_kerja.id_fakultas=fakultas.id_fakultas 
where pegawai.id_pengguna=$id_pengguna");
$kdfak=$fak['ID_FAKULTAS'];
$nmfak=$fak['NM_FAKULTAS'];
$smarty->assign('FAK', $kdfak);
$smarty->assign('NM_FAK', $nmfak);
$smarty->assign('NIMX', '');
//print_r($fak);
if (isset($_POST['action'])=='view') {
$nim = $_POST['nim'];
if(isset($_POST['id_pengambilan_mk'])){
$db->Query("update pengambilan_mk set status_hapus=mod(1-status_hapus,2) where id_pengambilan_mk in {$_POST['id_pengambilan_mk']}");
}
$smarty->assign('NIMX', $nim);
//$mode = $_POST['mode'];
//$smarty->assign('MOD', $mode);

$datamhs=getData("select ': '||nim_mhs||' - '||upper(nm_pengguna) as nm_mhs from mahasiswa left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna where nim_mhs='".$nim."' and id_program_studi in (select id_program_studi from program_studi where id_fakultas='".$kdfak."')");
$smarty->assign('MHS', $datamhs);
//print_r("select ': '||nim_mhs||' - '||upper(nm_pengguna) as nm_mhs from mahasiswa left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna where nim_mhs='".$nim."' and id_program_studi in (select id_program_studi from program_studi where id_fakultas='".$kdfak."')");			
$biomhs=getData("select mhs.nim_mhs,mhs.id_mhs upper(p.nm_pengguna) as nm_pengguna, j.nm_jenjang, ps.nm_program_studi, mhs.status_akademik_mhs
				from mahasiswa mhs 
				left join pengguna p on mhs.id_pengguna=p.id_pengguna 
				left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
				left join jenjang j on ps.id_jenjang=j.id_jenjang
				where mhs.nim_mhs='".$nim."' and mhs.id_program_studi in (select id_program_studi from program_studi where id_fakultas='".$kdfak."')");
$smarty->assign('T_MHS', $biomhs);

// Pilih nilai terakhir
/*
if($mode == 2){
$jaf=getData("select max(thn) as thn, max(smt) as smt, kd_mata_kuliah as kode, nm_mata_kuliah as nama, kredit_semester as sks, min(nilai_huruf) as nilai, max(nilai_standar_nilai) as bobot
from 
(select f.thn_akademik_semester as thn,case when f.nm_semester='Ganjil' then 1 else 2 end as smt,c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf,e.nilai_standar_nilai
from pengambilan_mk a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
left join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join semester f on a.id_semester=f.id_semester
where a.nilai_huruf<>'E' and a.nilai_huruf is not null and m.nim_mhs='".$nim."' and a.status_apv_pengambilan_mk=1)
group by kd_mata_kuliah, nm_mata_kuliah, kredit_semester
order by nm_mata_kuliah");
$smarty->assign('T_MK', $jaf);

$ipk=getData("select sum(sks) as sks_total, sum((bobot*sks)) as bobot_total, round((sum((bobot*sks))/sum(sks)),2) as ipk
from 
(select max(thn) as thn, max(smt) as smt, kd_mata_kuliah as kode, nm_mata_kuliah as nama, kredit_semester as sks, min(nilai_huruf) as nilai, max(nilai_standar_nilai) as bobot
from 
(select f.thn_akademik_semester as thn,case when f.nm_semester='Ganjil' then 1 else 2 end as smt,c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf,e.nilai_standar_nilai
from pengambilan_mk a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
left join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join semester f on a.id_semester=f.id_semester
where a.nilai_huruf<>'E' and a.nilai_huruf is not null and m.nim_mhs='".$nim."' and a.status_apv_pengambilan_mk=1)
group by kd_mata_kuliah, nm_mata_kuliah, kredit_semester)");
$smarty->assign('T_IPK', $ipk);

// Pilih nilai terbaik
} else if($mode == 1){

$jaf=getData("select kd_mata_kuliah as kode, nm_mata_kuliah as nama, kredit_semester as sks, min(nilai_huruf) as nilai, max(nilai_standar_nilai) as bobot
from 
(select c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai
from pengambilan_mk a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
left join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where a.nilai_huruf<>'E' and a.nilai_huruf is not null and m.nim_mhs='".$nim."' and a.status_apv_pengambilan_mk=1)
group by kd_mata_kuliah, nm_mata_kuliah, kredit_semester
order by nm_mata_kuliah");
*/
// versi lukman, untuk mengatasi bug mata kuliah yg sudah diulang tetap muncul semua gara2 kuliah yg diulang kodenya berbeda

$jaf=getData("
select
a.*,
b.thn_akademik_semester||'-'||b.nm_semester||' Nilai:'||b.nilai p1,
c.thn_akademik_semester||'-'||c.nm_semester||' Nilai:'||c.nilai p2,
d.thn_akademik_semester||'-'||d.nm_semester||' Nilai:'||d.nilai p3,
e.thn_akademik_semester||'-'||e.nm_semester||' Nilai:'||e.nilai p4,
f.thn_akademik_semester||'-'||f.nm_semester||' Nilai:'||f.nilai p5,
g.thn_akademik_semester||'-'||g.nm_semester||' Nilai:'||g.nilai p6,
'('''||b.id_pengambilan_mk||''','''||c.id_pengambilan_mk||''','''||d.id_pengambilan_mk||''','''||e.id_pengambilan_mk||''','''||f.id_pengambilan_mk||''','''||g.id_pengambilan_mk||''')' ids_pengambilan_mk
from(
select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p,a.status_hapus,a.id_pengambilan_mk
 from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
thn_akademik_semester,nm_semester
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join semester sm on sm.id_semester=a.id_semester
where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where  m.nim_mhs='{$nim}' and rangking=1) a
left join
(
select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p,a.id_pengambilan_mk
 from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
thn_akademik_semester,nm_semester
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join semester sm on sm.id_semester=a.id_semester
where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where  m.nim_mhs='{$nim}' and p=1
) b on a.nama=b.nama
left join
(
select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p,a.id_pengambilan_mk
 from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
thn_akademik_semester,nm_semester
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join semester sm on sm.id_semester=a.id_semester
where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where  m.nim_mhs='{$nim}' and p=2
) c on a.nama=c.nama
left join
(
select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p,a.id_pengambilan_mk
 from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
thn_akademik_semester,nm_semester
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join semester sm on sm.id_semester=a.id_semester
where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where  m.nim_mhs='{$nim}' and p=3
) d on a.nama=d.nama
left join
(
select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p,a.id_pengambilan_mk
 from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
thn_akademik_semester,nm_semester
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join semester sm on sm.id_semester=a.id_semester
where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where  m.nim_mhs='{$nim}' and p=4
) e on a.nama=e.nama
left join
(
select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p,a.id_pengambilan_mk
 from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
thn_akademik_semester,nm_semester
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join semester sm on sm.id_semester=a.id_semester
where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where  m.nim_mhs='{$nim}' and p=5
) f on a.nama=f.nama
left join
(
select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p,a.id_pengambilan_mk
 from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
thn_akademik_semester,nm_semester
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join semester sm on sm.id_semester=a.id_semester
where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where  m.nim_mhs='{$nim}' and p=6
) g on a.nama=g.nama

order by a.smtr
");


$smarty->assign('T_MK', $jaf);
//print_r($jaf);exit;
$ipk=getData("select sum(sks) as sks_total, sum((bobot*sks)) as bobot_total, round((sum((bobot*sks))/sum(sks)),2) as ipk
from 
(

select e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where rangking=1 and m.nim_mhs='{$nim}'
) uu
");
 
$smarty->assign('T_IPK', $ipk);
} else{
}


$smarty->display('rekap_nilai.tpl');  
?>