<?php
/*
Yudi Sulistya 10/05/2012
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$kdprodi=$user->ID_PROGRAM_STUDI;

$id_pengguna= $user->ID_PENGGUNA;
$kdfak=$user->ID_FAKULTAS;
$smarty->assign('FAK', $kdfak);

/*
$mk=getData("select s1.id_mhs,s1.nim_mhs,upper(s1.nm_pengguna) as nm_pengguna,coalesce(s1.nm_status_pengguna,'AKTIF') as nm_status_pengguna,
s1.tgl_lahir,s1.kelamin,s1.thn_angkatan_mhs,
coalesce(s3.sks_sem,0) as sks_sem,round(coalesce(s3.ips,0),2)as ips,
coalesce(s2.sks_total_mhs,0) as sks_total,round(coalesce(s2.ipk_mhs,0),2) as ipk
from
(select mahasiswa.id_mhs,nim_mhs,nm_pengguna,nm_status_pengguna,
case when tgl_lahir_pengguna is not null then TO_CHAR(tgl_lahir_pengguna,'DD-MM-YYYY') else '-' end as tgl_lahir,mahasiswa.thn_angkatan_mhs,
case when kelamin_pengguna=1 then 'L' when kelamin_pengguna=2 then 'P' else '-' end as kelamin
from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join admisi on mahasiswa.id_mhs=admisi.id_mhs and admisi.id_semester=5
left join status_pengguna on admisi.status_akd_mhs=status_pengguna.id_status_pengguna
where id_program_studi=7 and thn_angkatan_mhs<=2011
and mahasiswa.id_mhs not in
(select id_mhs from
(select id_mhs,case when nm_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,status_akd_mhs
from admisi
left join semester on admisi.id_semester=semester.id_semester)
where tahun<=20111 and status_akd_mhs not in (1,3))
)s1
left join
(select id_mhs, sum(kredit_semester) as sks_total_mhs,
round((sum((kredit_semester * nilai_standar_nilai)) / sum(kredit_semester)), 2) as IPK_MHS
from
(select a.id_mhs, c.kd_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai,
case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,
row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a
join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah
join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
join semester smt on a.id_semester=smt.id_semester
join mahasiswa m on a.id_mhs=m.id_mhs
where a.status_apv_pengambilan_mk = 1 and (a.nilai_huruf<>'E' or a.nilai_huruf<>null) and m.id_program_studi=7 and a.status_hapus=0
and a.status_pengambilan_mk > 0)
where tahun<=20111 and rangking=1
group by id_mhs) s2 on s1.id_mhs=s2.id_mhs
left join
(select id_mhs,case when sum(bobot*kredit_semester)=0 then 0 else
round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips,
case when id_fakultas=7 then sum(sksreal) else sum(kredit_semester) end as sks_sem
from
(select id_mhs,id_kurikulum_mk,id_fakultas,kredit_semester, sksreal,min(nilai_huruf) as nilai, max(bobot) as bobot from
(select a.id_mhs,ps.id_fakultas,a.id_kurikulum_mk,
case when (a.nilai_huruf = 'E' or a.nilai_huruf is null)
and d.status_mkta in (1,2) then 0
else d.kredit_semester end as kredit_semester,d.kredit_semester as sksreal,
case when a.nilai_huruf is null then 'E' else a.nilai_huruf end as nilai_huruf,
case when e.nilai_standar_nilai is null then 0 else e.nilai_standar_nilai end as bobot
from pengambilan_mk a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join program_studi ps on m.id_program_studi=ps.id_program_studi
left join semester s on a.id_semester=s.id_semester
where group_semester||thn_akademik_semester in
(select group_semester||thn_akademik_semester from semester where id_semester=5)
and tipe_semester in ('UP','REG','RD')
and a.status_apv_pengambilan_mk='1' and m.id_program_studi=7 and a.status_hapus=0
and a.status_pengambilan_mk > 0
)
group by id_mhs, id_kurikulum_mk, id_fakultas, kredit_semester, sksreal
)
group by id_mhs,id_fakultas)s3 on s1.id_mhs=s3.id_mhs
where s1.nm_status_pengguna is not null or (s1.nm_status_pengguna is null and s2.IPK_MHS is not null)
order by s1.nim_mhs");
*/

$mk=getData("
select x.id_mhs,y.nim_mhs,x.thn_angkatan_mhs,y.nm_pengguna,x.kelamin,x.tgl_lahir,
case when x.nem_pelajaran_mhs > 0 then x.nem_pelajaran_mhs else 0 end as nem_pelajaran,
y.sks_sem,y.ips,y.sks_total,y.ipk,x.smt
from
(
select mahasiswa.id_mhs,mahasiswa.nem_pelajaran_mhs,case when kelamin_pengguna='1' then 'L' else 'P' end as kelamin,
TO_CHAR(tgl_lahir_pengguna, 'YYYY-MM-DD') as tgl_lahir, thn_angkatan_mhs,case when thn_angkatan_mhs = 2011 then 1 else 3 end as smt
from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join jalur_mahasiswa on mahasiswa.id_mhs=jalur_mahasiswa.id_mhs
where jalur_mahasiswa.id_jalur=1 and thn_angkatan_mhs in (2010,2011) and mahasiswa.id_program_studi=7
) x
left join
(
select s1.id_mhs,s1.nim_mhs,upper(s1.nm_pengguna) as nm_pengguna,
coalesce(s3.sks_sem,0) as sks_sem,round(coalesce(s3.ips,0),2)as ips, coalesce(s2.sks_total_mhs,0) as sks_total,
round(coalesce(s2.ipk_mhs,0),2)as ipk
from
(
select mahasiswa.id_mhs,nim_mhs,nm_pengguna
from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
where mahasiswa.id_program_studi=7 and mahasiswa.thn_angkatan_mhs in (2010,2011)
) s1
left join
(
select id_mhs, sum(kredit_semester) as sks_total_mhs, round((sum((kredit_semester * nilai_standar_nilai))/sum(kredit_semester)), 2) as ipk_mhs
from
(
select a.id_mhs, c.kd_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai,
case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,
row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a
join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah
join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
join semester smt on a.id_semester=smt.id_semester
join mahasiswa m on a.id_mhs=m.id_mhs
where a.status_apv_pengambilan_mk >= 1 and (a.nilai_huruf<>'E' or a.nilai_huruf<>null) and m.id_program_studi=7 and a.status_hapus=0
and a.status_pengambilan_mk > 0 and m.thn_angkatan_mhs in (2010,2011)
)
where tahun<=20111 and rangking=1 group by id_mhs
) s2 on s1.id_mhs=s2.id_mhs
left join
(
select id_mhs,case when sum(bobot*kredit_semester)=0 then 0 else round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips,
sum(kredit_semester) as sks_sem
from
(
select id_mhs,id_kurikulum_mk,id_fakultas,kredit_semester,sksreal,min(nilai_huruf) as nilai,max(bobot) as bobot
from
(
select a.id_mhs,ps.id_fakultas,a.id_kurikulum_mk,
case when (a.nilai_huruf = 'E' or a.nilai_huruf is null) and d.status_mkta in (1,2) then 0 else d.kredit_semester end as kredit_semester,
d.kredit_semester as sksreal,
case when a.nilai_huruf is null then 'E' else a.nilai_huruf end as nilai_huruf,
case when e.nilai_standar_nilai is null then 0 else e.nilai_standar_nilai end as bobot
from pengambilan_mk a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join program_studi ps on m.id_program_studi=ps.id_program_studi
left join semester s on a.id_semester=s.id_semester
where group_semester||thn_akademik_semester in
(select group_semester||thn_akademik_semester from semester where id_semester=5) and tipe_semester in ('UP','REG','RD')
and a.status_apv_pengambilan_mk >= 1 and m.id_program_studi=7 and a.status_hapus=0 and a.status_pengambilan_mk > 0
and m.thn_angkatan_mhs in (2010,2011)
)
group by id_mhs, id_kurikulum_mk, id_fakultas, kredit_semester, sksreal
)
group by id_mhs,id_fakultas
) s3 on s1.id_mhs=s3.id_mhs
) y on x.id_mhs=y.id_mhs
order by nim_mhs
");


$smarty->assign('T_MK', $mk);

$smarty->display('rekap-ipk3.tpl');
?>
