<?php
require '../../config.php';
require_once '../../tcpdf/config/lang/ind.php';
require_once '../../tcpdf/tcpdf.php';	

$kdprodi= $user->ID_PROGRAM_STUDI;
$id_pengguna= $user->ID_PENGGUNA; 
$id_fakultas = $user->ID_FAKULTAS;


$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetMargins(18, 5, 4, false);

$ipk = "LEFT JOIN (select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
				from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
				c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
				row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
				count(*) over(partition by c.nm_mata_kuliah) terulang
					from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
					where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
					and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs 
					and h.id_program_studi = g.id_program_studi and h.id_fakultas = '$id_fakultas'
				) x
				where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
				group by id_mhs) C ON C.ID_MHS = MAHASISWA.ID_MHS ";
	
		$db->Query("SELECT MAHASISWA.NIM_MHS, NM_PENGGUNA, NM_JENJANG, NM_PROGRAM_STUDI, NM_FAKULTAS, 
								NO_IJASAH, TGL_BAYAR, PEMBAYARAN_WISUDA.BESAR_BIAYA, (SELECT SUM(PEMBAYARAN.BESAR_BIAYA) AS JML FROM PEMBAYARAN
									LEFT JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBAYARAN.ID_MHS
									LEFT JOIN DETAIL_BIAYA ON DETAIL_BIAYA.ID_DETAIL_BIAYA = PEMBAYARAN.ID_DETAIL_BIAYA
									WHERE (TGL_BAYAR IS NOT NULL OR PEMBAYARAN.ID_STATUS_PEMBAYARAN = 1) 
									AND ID_BIAYA = 115 AND PEMBAYARAN.ID_MHS = PEMBAYARAN_WISUDA.ID_MHS) AS CICILAN,
								LAHIR_IJAZAH, TO_CHAR(TGL_LULUS_PENGAJUAN, 'dd-mm-YYYY') as TGL_LULUS_PENGAJUAN, PERIODE_WISUDA.BESAR_BIAYA AS TARIF,
								(CASE WHEN KELAMIN_PENGGUNA = 1 THEN 'L' WHEN KELAMIN_PENGGUNA = 2 THEN 'P' ELSE '' END) AS KELAMIN_PENGGUNA,
								(CASE WHEN C.IPK IS NULL THEN PENGAJUAN_WISUDA.IPK ELSE C.IPK END) AS IPK, 
								(CASE WHEN C.SKS IS NULL THEN PENGAJUAN_WISUDA.SKS_TOTAL ELSE C.SKS END) AS SKS,TOTAL_SKP, PENGAJUAN_WISUDA.ELPT, MAHASISWA.STATUS, NM_JALUR,
								ALAMAT_ASAL_MHS, MOBILE_MHS, EMAIL_PENGGUNA, NM_TARIF_WISUDA, COALESCE(TO_CHAR(TGL_WISUDA, 'dd-mm-YYYY'), 'Belum Wisuda') as TGL_WISUDA
								 FROM MAHASISWA
								 LEFT JOIN PEMBAYARAN_WISUDA ON MAHASISWA.ID_MHS = PEMBAYARAN_WISUDA.ID_MHS
								 JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
								 JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
								 JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
								 JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG 
								 LEFT JOIN JALUR_MAHASISWA ON JALUR_MAHASISWA.ID_MHS = MAHASISWA.ID_MHS AND ID_JALUR_AKTIF = 1
								 LEFT JOIN JALUR ON JALUR.ID_JALUR = JALUR_MAHASISWA.ID_JALUR
								 LEFT JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PEMBAYARAN_WISUDA.ID_PERIODE_WISUDA
								 LEFT JOIN TARIF_WISUDA ON TARIF_WISUDA.ID_TARIF_WISUDA = PERIODE_WISUDA.ID_TARIF_WISUDA
								 LEFT JOIN PENGAJUAN_WISUDA ON PENGAJUAN_WISUDA.ID_MHS = PEMBAYARAN_WISUDA.ID_MHS 
								 LEFT JOIN (SELECT SUM(SKOR_KRP_KHP) AS TOTAL_SKP, ID_MHS FROM KRP_KHP GROUP BY ID_MHS)  KRP_KHP ON KRP_KHP.ID_MHS = MAHASISWA.ID_MHS 
								 $ipk
								WHERE MAHASISWA.NIM_MHS = '$_REQUEST[nim]' AND FAKULTAS.ID_FAKULTAS = '$id_fakultas'
								ORDER BY FAKULTAS.ID_FAKULTAS, NM_JENJANG, NM_PROGRAM_STUDI, MAHASISWA.NIM_MHS, PEMBAYARAN_WISUDA.TGL_BAYAR");

		$data_mhs = $db->FetchAssoc();
								
	
	$pdf->AddPage();
	$pdf->setPageMark();
	
	
$html = '
<table style="font-size:26px" width="80%" cellpadding="2">
<tr>
	<td colspan="3" style="text-align:center"><img src="../../foto_wisuda/'.$data_mhs['NIM_MHS'].'.jpg" width="105" height="150" vspace="1" hspace="2"></td>
</tr>
<tr>
	<td width="38%">NIM</td>
	<td width="2%">:</td>
	<td width="60%">'.$data_mhs['NIM_MHS'].'</td>
</tr>
<tr>
	<td>Nama</td>
	<td>:</td>
	<td>'.$data_mhs['NM_PENGGUNA'].'</td>
</tr>
<tr>
	<td>Fakultas</td>
	<td>:</td>
	<td>'.$data_mhs['NM_FAKULTAS'].'</td>
</tr>
<tr>
	<td>Program Studi</td>
	<td>:</td>
	<td>'.$data_mhs['NM_JENJANG'].' - '.$data_mhs['NM_PROGRAM_STUDI'].'</td>
</tr>
<tr>
	<td>Jalur</td>
	<td>:</td>
	<td>'.$data_mhs['NM_JALUR'].'</td>
</tr>
<tr>
	<td>Tempat/ Tanggal Lahir</td>
	<td>:</td>
	<td>'.$data_mhs['LAHIR_IJAZAH'].'</td>
</tr>
<tr>
	<td>Tanggal Lulus</td>
	<td>:</td>
	<td>'.$data_mhs['TGL_LULUS_PENGAJUAN'].'</td>
</tr>
<tr>
	<td>Nomor Ijasah</td>
	<td>:</td>
	<td>'.$data_mhs['NO_IJASAH'].'</td>
</tr>
<tr>
	<td>IPK</td>
	<td>:</td>
	<td>'.$data_mhs['IPK'].'</td>
</tr>
<tr>
	<td>SKS</td>
	<td>:</td>
	<td>'.$data_mhs['SKS'].'</td>
</tr>
<tr>
	<td>SKP</td>
	<td>:</td>
	<td>'.$data_mhs['TOTAL_SKP'].'</td>
</tr>
<tr>
	<td>TOEFL</td>
	<td>:</td>
	<td>'.$data_mhs['ELPT'].'</td>
</tr>
<tr>
	<td>Alamat Terakhir</td>
	<td>:</td>
	<td>'.$data_mhs['ALAMAT_ASAL_MHS'].'</td>
</tr>
<tr>
	<td>Telp/ HP</td>
	<td>:</td>
	<td>'.$data_mhs['MOBILE_MHS'].'</td>
</tr>
<tr>
	<td>Email</td>
	<td>:</td>
	<td>'.$data_mhs['EMAIL_PENGGUNA'].'</td>
</tr>
<tr>
	<td>Periode Wisuda</td>
	<td>:</td>
	<td>'.$data_mhs['NM_TARIF_WISUDA'].'</td>
</tr>
<tr>
	<td>Tanggal Wisuda</td>
	<td>:</td>
	<td>'.$data_mhs['TGL_WISUDA'].'</td>
</tr>
</table>';

$pdf->writeHTML($html);
$pdf->Output('info-wisuda-'.$_REQUEST['nim'].'.pdf', 'I');
?>
