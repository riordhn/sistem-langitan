<?php
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
include "class/aucc.class.php";
$aucc = new AUCC_Akademik($db);
$id_pengguna = $user->ID_PENGGUNA; 
$kdfak = $aucc->cek_fakultas_pengguna($id_pengguna);

$smarty->assign('prodi', $aucc->get_prodi_fak($kdfak['ID_FAKULTAS']));
$smarty->assign('semester_bayar', $aucc->get_semester_bayar());
$smarty->assign('biaya', $aucc->get_biaya());
$smarty->assign('pembayaran_prodi', $aucc->data_pembayaran_prodi($kdfak['ID_FAKULTAS']));

$smarty->display('pembayaran-prodi.tpl');
?>
