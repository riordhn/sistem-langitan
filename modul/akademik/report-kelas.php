<?php
/*
Yudi Sulistya 19/12/2011
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$kdprodi=$user->ID_PROGRAM_STUDI;

$id_pengguna= $user->ID_PENGGUNA; 

$kdfak=$user->ID_FAKULTAS;
$smarty->assign('FAK', $kdfak);

$thnsmt=getData("select id_semester, tahun_ajaran||' - '||nm_semester as thn_smt from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester in (
select * from (select distinct thn_akademik_semester 
from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester<=EXTRACT(YEAR FROM sysdate) order by thn_akademik_semester desc) 
where rownum<=10) 
and (nm_semester ='Ganjil' or nm_semester ='Genap')
order by thn_akademik_semester desc, nm_semester desc");
$smarty->assign('T_THNSMT', $thnsmt);

$dataprodi=getData("select id_program_studi||'-'||id_fakultas as id_program_studi,nm_jenjang||'-'||nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_fakultas=$kdfak and status_aktif_prodi = 1");
$smarty->assign('T_PRODI', $dataprodi);


if (isset($_POST['action'])=='view') {
$var = explode("-", $_POST['kdprodi']);
$smt = $_POST['kdthnsmt'];

$datathnsmt=getData("select '(TAHUN '||tahun_ajaran||' - SEMESTER '||nm_semester||')' as thn_smt from semester where id_semester=$smt");
$smarty->assign('THNSMT', $datathnsmt);

if ($var[0]=='all') {
$dataprodi1=getData("select (': FAKULTAS '||nm_fakultas) as prodi from fakultas where id_fakultas=".$var[1]."");
$smarty->assign('NM_PRODI', $dataprodi1);

$jaf=getData("
select distinct kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah,
kurikulum_mk.kredit_semester, nama_kelas.nama_kelas,
nm_jenjang||'-'||coalesce(nm_singkat_prodi,nm_program_studi) as prodi,
jumlah_pertemuan_kelas_mk as ren,count(distinct id_presensi_kelas) as tm,
round(count(distinct id_presensi_kelas)/jumlah_pertemuan_kelas_mk,2)*100 as persen,
sum(case when pengambilan_mk.status_apv_pengambilan_mk=1 then 1 else 0 end) as peserta
from kelas_mk
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk
left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk=kelas_mk.id_kurikulum_mk
left join mata_kuliah on mata_kuliah.id_mata_kuliah=kurikulum_mk.id_mata_kuliah
left join nama_kelas on nama_kelas.id_nama_kelas=kelas_mk.no_kelas_mk
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk 
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
left join presensi_kelas on kelaS_mk.id_kelas_mk=presensi_kelas.id_kelas_mk
where kelas_mk.id_semester=$smt and pengambilan_mk.status_apv_pengambilan_mk=1
and program_studi.id_fakultas=".$var[1]." and pengampu_mk.pjmk_pengampu_mk=1
group by kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, 
kurikulum_mk.kredit_semester, nama_kelas.nama_kelas, nm_jenjang||'-'||coalesce(nm_singkat_prodi,nm_program_studi), 
jumlah_pertemuan_kelas_mk ");
$smarty->assign('T_MK', $jaf);

} else {
$dataprodi1=getData("select (': PRODI '||nm_jenjang||'-'||nm_program_studi) as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_program_studi=".$var[0]."");
$smarty->assign('NM_PRODI', $dataprodi1);

$jaf=getData("
--select * from (
select distinct kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah,
kurikulum_mk.kredit_semester, nama_kelas.nama_kelas,
nm_jenjang||'-'||coalesce(nm_singkat_prodi,nm_program_studi) as prodi,
jumlah_pertemuan_kelas_mk as ren,count(distinct id_presensi_kelas) as tm,
round(count(distinct id_presensi_kelas)/jumlah_pertemuan_kelas_mk,2)*100 as persen,
sum(case when pengambilan_mk.status_apv_pengambilan_mk=1 then 1 else 0 end) as peserta
from kelas_mk
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk
left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk=kelas_mk.id_kurikulum_mk
left join mata_kuliah on mata_kuliah.id_mata_kuliah=kurikulum_mk.id_mata_kuliah
left join nama_kelas on nama_kelas.id_nama_kelas=kelas_mk.no_kelas_mk
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk 
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
left join presensi_kelas on kelaS_mk.id_kelas_mk=presensi_kelas.id_kelas_mk
where kelas_mk.id_semester=$smt and pengambilan_mk.status_apv_pengambilan_mk=1
and kelas_mk.id_program_studi=".$var[0]." and pengampu_mk.pjmk_pengampu_mk=1
group by kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, 
kurikulum_mk.kredit_semester, nama_kelas.nama_kelas, nm_jenjang||'-'||coalesce(nm_singkat_prodi,nm_program_studi), 
jumlah_pertemuan_kelas_mk
--)
--where tm != 0 ");
$smarty->assign('T_MK', $jaf);
}
}
$smarty->display('report-kelas.tpl');  
?>