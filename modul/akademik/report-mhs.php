<?php

include 'config.php';
include '../pendidikan/class/report_mahasiswa.class.php';
include '../pendidikan/class/evaluasi.class.php';

$rm = new report_mahasiswa($db);
$eval = new evaluasi($db);
$id_fakultas = $user->ID_FAKULTAS;

$id_pt = $id_pt_user;

if (isset($_POST['tampil'])) {
	
	$smarty->assign('program_studi', $eval->program_studi($id_fakultas, $_POST['jenjang'], $_POST['program_studi']));
    $smarty->assign('data_mahasiswa',$rm->get_mahasiswa_dayat($id_fakultas, $_POST['program_studi'], $_POST['tahun_akademik'], $_POST['status'], $_POST['jenjang'], $_POST['jalur'], $id_pt));

   	$smarty->assign('tampil', $_POST['tampil']);
}

$smarty->assign('fakultas', $id_fakultas);
$smarty->assign('data_jalur', $rm->get_jalur($id_pt));
$smarty->assign('data_jenjang', $rm->get_jenjang());
$smarty->assign('data_tahun_akademik', $rm->get_tahun_akademik());
$smarty->assign('data_status', $rm->get_status());

$smarty->display("report-mhs.tpl");
?>
