<?php
//error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$id_pengguna= $user->ID_PENGGUNA;
$username= $user->USERNAME;
$kdfak=$user->ID_FAKULTAS;
$smarty->assign('FAK',$kdfak);

//Fakultas Lain
if($kdfak!=11){
	echo 'Anda tidak memiliki hak akses';
} else {

//smt aktif
$smtaktif=getvar("select id_semester,thn_akademik_semester,nm_semester from semester where status_aktif_semester='True'");
//$smtaktif=getvar("select id_semester,thn_akademik_semester,nm_semester from semester where id_semester=21");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);
$id_semester=$smtaktif['ID_SEMESTER'];

//last smt
if($smtaktif['NM_SEMESTER']=="Ganjil") {
	$last_smt=getvar("select id_semester from semester where thn_akademik_semester=$smtaktif[THN_AKADEMIK_SEMESTER]-1 and nm_semester='Genap'");
}elseif($smtaktif['NM_SEMESTER']=="Genap") {
	$last_smt=getvar("select id_semester from semester where thn_akademik_semester=$smtaktif[THN_AKADEMIK_SEMESTER] and nm_semester='Ganjil'");
}

$nim_mhs= $_REQUEST['nim'];
$smarty->assign('nim',$nim_mhs);

$detailmhs= getvar("select mahasiswa.id_pengguna,mahasiswa.id_mhs,mahasiswa.id_program_studi,nm_pengguna,
nm_jenjang||'-'||nm_program_studi as nm_program_studi,nim_mhs,jenjang.id_jenjang, thn_angkatan_mhs, thn_angkatan_mhs||1 as maba
from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where nim_mhs = '".$nim_mhs."' and program_studi.id_fakultas=$kdfak");

//cek
$cek= getvar("select status_krs from program_studi where id_program_studi='$detailmhs[ID_PROGRAM_STUDI]'");
if (($cek['STATUS_KRS']<> 1) && ($cek['STATUS_KRS']<> 3)){

$smarty->assign('nim', $detailmhs['NIM_MHS']);
$smarty->assign('nama', $detailmhs['NM_PENGGUNA']);
$smarty->assign('prodi', $detailmhs['NM_PROGRAM_STUDI']);
$smarty->assign('maba', $detailmhs['MABA']);

//get IPS
$datamhs= getvar("select id_mhs, sum(kredit_semester) as sks_total, sum((bobot*kredit_semester)) as bobot_total,
case when sum(bobot*kredit_semester)=0 then 0 else
round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips
from
(select m.id_mhs,
case when (a.nilai_angka <= 0 or a.nilai_huruf = 'E' or a.nilai_angka is null or a.nilai_huruf is null) and d.status_mkta in (1,2) then 0
else d.kredit_semester end as kredit_semester,
case when a.nilai_huruf is null then 'E' else a.nilai_huruf end as nilai_huruf,
case when e.nilai_standar_nilai is null then 0 else e.nilai_standar_nilai end as bobot
from pengambilan_mk a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah
left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where a.id_semester=$last_smt[ID_SEMESTER] and a.status_apv_pengambilan_mk='1' and m.nim_mhs='".$nim_mhs."'
)
group by id_mhs");
$smarty->assign('ips',$datamhs['IPS']);

//get IPK
$dataipk= getvar("select id_mhs, sum(sks) as ttl_sks, round((sum((sks * bobot)) / sum(sks)), 2) as ipk
from
(select id_mhs, kd_mata_kuliah as kode, nm_mata_kuliah as nama, kredit_semester as sks, min(nilai_huruf) as nilai, max(nilai_standar_nilai) as bobot
from
(select m.id_mhs, c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai
from pengambilan_mk a
join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah
join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
join mahasiswa m on a.id_mhs=m.id_mhs
where a.status_apv_pengambilan_mk = 1 and a.status_hapus=0 and a.nilai_huruf<>'E' and a.nilai_huruf is not null and a.id_semester<>$id_semester and m.nim_mhs='".$nim_mhs."')
group by id_mhs, kd_mata_kuliah, nm_mata_kuliah, kredit_semester)
group by id_mhs");
$smarty->assign('ttl_sks',$dataipk['TTL_SKS']);
$smarty->assign('ipk',$dataipk['IPK']);

$angkatan=getvar("select trim(thn_akademik_semester) as thn from semester where status_aktif_semester='True'");

//get jatah SKS
if ($detailmhs['THN_ANGKATAN_MHS'] == $angkatan['THN']) {
		$jatahsks= getvar("select max(sks_maksimal) as sks_max from beban_sks where id_program_studi = '".$detailmhs['ID_PROGRAM_STUDI']."'");
		$smarty->assign('sks_maks',$jatahsks['SKS_MAX']);
} else {
		if ($datamhs['IPS']>0) {
		$jatahsks= getvar("select max(sks_maksimal) as sks_max from beban_sks where id_program_studi = '".$detailmhs['ID_PROGRAM_STUDI']."' and ipk_minimum <= '".$datamhs['IPS']."'");
		} else {
		$jatahsks= getvar("select max(sks_maksimal) as sks_max from beban_sks where id_program_studi = '".$detailmhs['ID_PROGRAM_STUDI']."' and ipk_minimum <=0");
		}
$smarty->assign('sks_maks',$jatahsks['SKS_MAX']);
}

//sks max
$sksambil=getvar("select sum(kurikulum_mk.kredit_semester) as ttlsks from pengambilan_mk
left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
where pengambilan_mk.id_mhs='$detailmhs[ID_MHS]' and pengambilan_mk.id_semester=$id_semester and status_apv_pengambilan_mk=1");
$smarty->assign('sksdisetujui', $sksambil['TTLSKS']);

$status = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'krs';

switch($status) {

case 'add':
		 // pilih

		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block');

		$id_kelas_mk= $_GET['id_kelas_mk'];
		$sks= $_GET['sks'];
		$kaps= $_GET['kaps'];

		//get jumlah pst
		$cekpst= getvar("select count (distinct id_mhs) as pst from pengambilan_mk where id_kelas_mk=$id_kelas_mk and status_apv_pengambilan_mk=1 and id_semester=$id_semester");

//tambahan dayat
//cek pembayaran

$cek_pembayaran = getvar("select count(*) as jumlah_bayar from pembayaran where id_mhs='$detailmhs[ID_MHS]'
							and id_status_pembayaran in (1, 3) and id_semester=$id_semester");

$cek_pembayaran_cmhs = getvar("select count(*) as jumlah_bayar
								from pembayaran_cmhs where id_c_mhs in (select id_c_mhs from mahasiswa where id_mhs='$detailmhs[ID_MHS]')
								and tgl_bayar is not null and id_semester=$id_semester");

if ($cek_pembayaran['JUMLAH_BAYAR'] > 0 || $cek_pembayaran_cmhs['JUMLAH_BAYAR'] > 0){

//cek sidik jari
$cek_sidik = getvar("SELECT count(*) as JARI FROM FINGERPRINT_MAHASISWA 
					WHERE FINGERPRINT_MAHASISWA.NIM_MHS = '".$nim_mhs."'");

if ($cek_sidik['JARI'] <> "" || $cek_sidik['JARI'] > 0) {

//cek cekal akademik
$cekal = getvar("SELECT STATUS_CEKAL
				FROM MAHASISWA
				LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
				WHERE STATUS_PENGGUNA.STATUS_AKTIF = 1 AND NIM_MHS = '".$nim_mhs."'");

if($cekal['STATUS_CEKAL'] <> '' || $cekal['STATUS_CEKAL'] <> '0'){
/*
// Jika Mahasiswa Sudah Belajar 1 tahun
if ($maba == $agk_maba) {
	$eval_maba = 1;
} else {
	$tahun_sekarang = date('Y');
	$eval_maba1 = getvar("SELECT COUNT(*) as jml FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=4 AND ID_MHS='$detailmhs[ID_MHS]'");
	$eval_maba = $eval_maba1['JML'];
}

if ($eval_maba > 0) {

// Cek Pengisian Evaluasi Perwalian
if ($maba == $agk_maba) {
	$eval_wali = 1;
} else {
	$doli=getvar("select id_dosen from dosen_wali where id_mhs='$detailmhs[ID_MHS]' and status_dosen_wali='1'");
	$eval_wali1 = getvar("SELECT COUNT(*) as jml FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=3 AND ID_MHS='$detailmhs[ID_MHS]' AND ID_DOSEN='$doli[ID_DOSEN]' AND ID_SEMESTER='$last_smt[ID_SEMESTER]'");
	$eval_wali = $eval_wali1['JML'];
}

if ($eval_wali > 0) {

// Cek Pengisian Evaluasi perkuliahan
if ($maba == $agk_maba) {
	$eval_kuliah = 1;
} else {
	$datamk= getvar("select count(*) as tot from pengambilan_mk 
	where id_semester=$last_smt[ID_SEMESTER] and status_apv_pengambilan_mk='1' and id_mhs='$detailmhs[ID_MHS]'");
	$eval_kuliah1 = getvar("SELECT COUNT(*) as jml FROM
				(
					SELECT ID_KELAS_MK,ID_DOSEN,ID_MHS,ID_SEMESTER FROM AUCC.EVALUASI_HASIL 
					WHERE ID_MHS='$detailmhs[ID_MHS]' AND ID_SEMESTER='$last_smt[ID_SEMESTER]' AND (ID_EVAL_INSTRUMEN=1 OR ID_EVAL_INSTRUMEN=2)
					GROUP BY ID_KELAS_MK,ID_DOSEN,ID_MHS,ID_SEMESTER
				)");
	$eval_kuliah = $eval_kuliah1['JML'];
}

if (($eval_kuliah >= $datamk['TOT'] && $datamk['TOT'] != 0) || (($eval_kuliah == 0 || $eval_kuliah >= 0) && $datamk['TOT'] == 0)) {

// Cek Pengisian Evaluasi Administrasi Fakultas
if ($maba == $agk_maba) {
	$eval_adm = 1;
} else {
	$eval_adm1 = getvar("SELECT COUNT(*) as jml FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=6 AND ID_MHS='$detailmhs[ID_MHS]' AND ID_SEMESTER='$last_smt[ID_SEMESTER]'");
	$eval_adm = $eval_adm1['JML'];
}
if ($eval_adm > 0) {
*/
		if (($cekpst['PST']+1) <= $kaps) {
		if (($sksambil['TTLSKS']+$sks) <= $jatahsks['SKS_MAX']) {
				if($jatahsks['SKS_MAX'] <= 24){
					$id_kur=getvar("select id_kurikulum_mk,id_mata_kuliah from kurikulum_mk kmk1
									where kmk1.id_program_studi='$detailmhs[ID_PROGRAM_STUDI]' and kmk1.id_mata_kuliah in
									(select id_mata_kuliah from mata_kuliah where kd_mata_kuliah in
									(select c.kd_mata_kuliah from kelas_mk a, kurikulum_mk b, mata_kuliah c
									where a.id_kurikulum_mk=b.id_kurikulum_mk and a.id_kelas_mk='".$id_kelas_mk."'
									and c.id_mata_kuliah=b.id_mata_kuliah))");

					InsertData("insert into pengambilan_mk (id_mhs,id_semester,status_pengambilan_mk,status_apv_pengambilan_mk,id_kelas_mk,id_kurikulum_mk,keterangan)
					values ('$detailmhs[ID_MHS]','$id_semester','9','1','$id_kelas_mk','".$id_kur['ID_KURIKULUM_MK']."','KRS AKADEMIK OLEH ".$username."')");

					//Update status_cekal=1 untuk status_mkta>0
					$mkta = "select id_pengambilan_mk from pengambilan_mk where id_semester='$id_semester' and id_kurikulum_mk in 
					(select id_kurikulum_mk from kurikulum_mk where status_mkta>0) and id_mhs='$detailmhs[ID_MHS]'";
					$result1 = $db->Query($mkta)or die("salah kueri proses mkta ");
					while($r1 = $db->FetchRow()) {
						$id_pengambilan_mk = $r1[0];

						gantidata("update pengambilan_mk set status_cekal=1, status_cekal_uts=1, persen_presensi=0 where id_pengambilan_mk=$id_pengambilan_mk");
					}

				}else{
					echo '<script>alert("Jumlah SKS maksimum sudah lebih dari sama dengan 24 sks")</script>';
				}
		} else {
			 echo '<script>alert("Jumlah SKS Melebihi Batas Maksimum")</script>';
		}
		} else
		{
			 echo '<script>alert("Kapasitas Kelas Tidak Cukup")</script>';
		}
/*
	 }else{
	 	echo '<script>alert("Belum mengisi Evaluasi Administrasi Fakultas secara benar/lengkap...")</script>';
	 }
	 }else{
	 	echo '<script>alert("Belum mengisi Evaluasi Perkuliahan secara benar/lengkap...")</script>';
	 }
	 }else{
	 	echo '<script>alert("Belum mengisi Evaluasi Perwalian secara benar/lengkap...")</script>';
	 }
	 }else{
	 	echo '<script>alert("Belum mengisi Evaluasi Mahasiswa Baru secara benar/lengkap...")</script>';
	 }
*/
	 }else{
	 	echo '<script>alert("Cekal Akademik")</script>';
	 }
	}
	else{
		 echo '<script>alert("Belum melakukan sidik jari")</script>';
	}
}else{
	 echo '<script>alert("Data pembayaran semester ini masih kosong")</script>';
}

	break;

case 'krs':
		 // pilih

        break;

case 'del':
		$smarty->assign('disp1','block');
		$smarty->assign('disp2','none');


	   $id_pengambilan_mk= $_GET['idpmk'];

       deletedata("delete from nilai_mk where id_pengambilan_mk=$id_pengambilan_mk");
	   deletedata("delete from pengambilan_mk where id_pengambilan_mk=$id_pengambilan_mk");

        break;
}

//dayat =  and status_apv_pengambilan_mk=1 -> saya hapus
$mhs_status1=getData("select krs_prodi.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,nm_jadwal_hari,
jam_mulai||':'||menit_mulai||' - '||jam_selesai||':'||menit_selesai as nm_jadwal_jam,
sum(case when status_pengambilan_mk>=1 then 1 else 0 end) as pst, kapasitas_kelas_mk from krs_prodi
left join pengambilan_mk on krs_prodi.id_kelas_mk=pengambilan_mk.id_kelas_mk and krs_prodi.id_semester=pengambilan_mk.id_semester
left join kelas_mk on krs_prodi.id_kelas_mk=kelas_mk.id_kelas_mk and krs_prodi.id_semester=kelas_mk.id_semester
left join nama_kelas on kelas_mk.no_kelas_mk=id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
where krs_prodi.id_program_studi='$detailmhs[ID_PROGRAM_STUDI]' and krs_prodi.id_semester=$id_semester and
kurikulum_mk.id_kurikulum_mk not in (select id_kurikulum_mk from pengambilan_mk where id_mhs='$datamhs[ID_MHS]' and id_semester=$id_semester)
group by krs_prodi.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,nm_jadwal_hari,
jam_mulai,menit_mulai,jam_selesai,menit_selesai,kapasitas_kelas_mk
order by kd_mata_kuliah,nama_kelas");
$smarty->assign('T_KRS', $mhs_status1);

$data_mhs_krs=getData("select id_pengambilan_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,nm_jadwal_hari,
jam_mulai||':'||menit_mulai||' - '||jam_selesai||':'||menit_selesai as nm_jadwal_jam,
case when status_apv_pengambilan_mk=1 then 'Approved' else 'Not Approved' end as status
from pengambilan_mk
left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk
left join nama_kelas on kelas_mk.no_kelas_mk=id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
where pengambilan_mk.id_mhs in (select id_mhs from mahasiswa where nim_mhs='".$nim_mhs."') and pengambilan_mk.id_semester=$id_semester order by kd_mata_kuliah,nama_kelas");
$smarty->assign('T_AMBIL', $data_mhs_krs);

$sksambil=getvar("select sum(kurikulum_mk.kredit_semester) as ttlsks from pengambilan_mk
left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
where pengambilan_mk.id_mhs='$datamhs[ID_MHS]' and pengambilan_mk.id_semester=$id_semester and status_apv_pengambilan_mk=1");
$smarty->assign('sksdisetujui', $sksambil['TTLSKS']);

$smarty->display('akademik-krs-psikologi.tpl');

} else
{
	 echo '<script>alert("Saat ini sedang berlangsung KRS / KPRS, Fitur Ditutup Sementara")</script>';
	 echo '<script>window.document.location.reload();</script>';
}

}
?>
