<?PHP
include('config.php');
require_once ('ociFunction.php');
include 'includes/function.php';

$kdprodi=$user->ID_PROGRAM_STUDI;

$id_pengguna= $user->ID_PENGGUNA; 
$fak=getvar("select fakultas.id_fakultas, upper(fakultas.nm_fakultas) as nm_fakultas from pegawai left join unit_kerja on pegawai.id_unit_kerja=unit_kerja.id_unit_kerja 
left join fakultas on unit_kerja.id_fakultas=fakultas.id_fakultas 
where pegawai.id_pengguna=$id_pengguna");
$kdfak=$fak['ID_FAKULTAS'];
$nmfak=$fak['NM_FAKULTAS'];
$smarty->assign('FAK', $kdfak);
$smarty->assign('NM_FAK', $nmfak);
$smarty->assign('NIMX', '');


$depan=time();
$belakang=strrev(time());

$smt=getData("select * from semester where nm_semester in ('Ganjil', 'Genap') order by thn_akademik_semester desc, nm_semester asc");
$smarty->assign('smt', $smt);
$smt_aktif=getvar("select * from semester where status_aktif_semester='True'");
$smarty->assign('smt_aktif', $smt_aktif['ID_SEMESTER']);

if (isset($_POST['action'])=='view') {
$nim = $_POST['nim'];
//$mode = $_POST['mode'];

//Yudi Sulistya 21/07/2012
//Enkrip url untuk cetak

$id=getvar("select id_mhs_asing from mahasiswa_asing where nim_mhs_asing='".$nim."'");
$cetak=$id['ID_MHS_ASING'];

//UMUM
$link_asing = 'proses/transkrip-asing-cetak.php?'.paramEncrypt($depan.'='.$belakang.$depan.'&cetak='.$cetak.'&'.$belakang.'='.$depan.$belakang.'&id_smt='.$_POST['id_smt']).'';
$smarty->assign('LINK_ASING', $link_asing);;

$datamhs=getData("select ': '||nim_mhs_asing||' - '||upper(nm_pengguna) as nm_mhs from mahasiswa_asing left join pengguna on mahasiswa_asing.id_pengguna=pengguna.id_pengguna where nim_mhs_asing='".$nim."' and id_program_studi in (select id_program_studi from program_studi where id_fakultas='".$kdfak."')");

$smarty->assign('MHS', $datamhs);
$smarty->assign('NIMX', $nim);			

$biomhs=getData("select mhs.id_mhs_asing, mhs.nim_mhs_asing, upper(p.nm_pengguna) as nm_pengguna, j.nm_jenjang, ps.nm_program_studi
				from mahasiswa_asing mhs 
				left join pengajuan_wisuda pw on pw.id_mhs=mhs.id_mhs_asing
				left join pengguna p on mhs.id_pengguna=p.id_pengguna 
				left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
				left join jenjang j on ps.id_jenjang=j.id_jenjang
				where mhs.nim_mhs_asing='".$nim."' and mhs.id_program_studi in (select id_program_studi from program_studi where id_fakultas='".$kdfak."')");
$smarty->assign('T_MHS', $biomhs);

$jaf=getData("select e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk_asing a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa_asing m on a.id_mhs=m.id_mhs_asing
where rangking=1 and m.nim_mhs_asing='{$nim}'");
$smarty->assign('T_MK', $jaf);


$ipk=getData("select sum(sks) as sks_total, sum((bobot*sks)) as bobot_total, round((sum((bobot*sks))/sum(sks)),2) as ipk
from 
(
select e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk_asing a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa_asing m on a.id_mhs=m.id_mhs_asing
where rangking=1 and m.nim_mhs_asing='{$nim}'
) uu
");
$smarty->assign('T_IPK', $ipk);

}
$smarty->display('transkrip-asing.tpl');
?>