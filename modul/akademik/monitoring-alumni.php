<?php
include('config.php');
$kdprodi= $user->ID_PROGRAM_STUDI;
$id_pengguna= $user->ID_PENGGUNA; 
$id_fakultas = $user->ID_FAKULTAS;

$periode = $db->QueryToArray("SELECT * FROM TARIF_WISUDA WHERE ID_PERGURUAN_TINGGI = '{$id_pt_user}' ORDER BY ID_TARIF_WISUDA DESC");
$smarty->assign('periode', $periode);

	
$pengajuan = $db->QueryToArray("SELECT NIM_MHS, NM_PENGGUNA, NM_JENJANG, NM_PROGRAM_STUDI, NM_FAKULTAS, 
								NO_IJASAH,LAHIR_IJAZAH, TGL_LULUS_PENGAJUAN,mahasiswa.alamat_mhs as alamat,mobile_mhs as phone,
								(CASE WHEN KELAMIN_PENGGUNA = 1 THEN 'L' WHEN KELAMIN_PENGGUNA = 2 THEN 'P' ELSE '' END) AS KELAMIN_PENGGUNA,
								(CASE WHEN C.IPK IS NULL THEN PENGAJUAN_WISUDA.IPK ELSE C.IPK END) AS IPK, 
								(CASE WHEN C.SKS IS NULL THEN PENGAJUAN_WISUDA.SKS_TOTAL ELSE C.SKS END) AS SKS
								FROM mahasiswa
								JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
								JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
								JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
								JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
								LEFT JOIN PENGAJUAN_WISUDA ON PENGAJUAN_WISUDA.ID_MHS = mahasiswa.ID_MHS
								LEFT JOIN (select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
								from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
								c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
								row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
								count(*) over(partition by c.nm_mata_kuliah) terulang
								from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
								where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
								and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs 
								and h.id_program_studi = g.id_program_studi and h.id_fakultas = $id_fakultas
								) x
								where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
								GROUP BY id_mhs) C ON C.ID_MHS = MAHASISWA.ID_MHS
										
								WHERE pengguna.id_role=28
								AND PROGRAM_STUDI.ID_FAKULTAS =$id_fakultas
								ORDER BY FAKULTAS.ID_FAKULTAS, NM_JENJANG, NM_PROGRAM_STUDI, MAHASISWA.NIM_MHS");


$smarty->assign('pengajuan_bayar', $pengajuan);

$smarty->display("monitoring-alumni.tpl");
?>
