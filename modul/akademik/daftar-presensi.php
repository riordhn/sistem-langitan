<?php
require('common.php');
require_once ('ociFunction.php');

error_reporting (E_ALL & ~E_NOTICE);

$id_pengguna= $user->ID_PENGGUNA;
$kdfak=$user->ID_FAKULTAS;

$smarty->assign('FAK',$kdfak);

$smtaktif=getvar("select id_semester from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND status_aktif_semester='True'");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);

//mkwu
$unit_kerja=getvar("select * from pegawai where id_pengguna=$id_pengguna");

if($unit_kerja['ID_UNIT_KERJA'] != 2){
$prodi=getData("select id_program_studi, (nm_jenjang|| ' - ' ||nm_program_studi)  as nm_prod
				from program_studi 
				join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
				where id_fakultas='$kdfak' and status_aktif_prodi = 1
				order by jenjang.nm_jenjang, nm_program_studi");
$smarty->assign('prodi',$prodi);
}

//Khusus Fakultas Psikologi
if($kdfak==11){
$smt=getData("select id_semester, tahun_ajaran||' - '||nm_semester as smt from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND nm_semester in ('Ganjil', 'Genap') order by thn_akademik_semester desc,nm_semester desc");
$smarty->assign('T_ST', $smt);

//Fakultas Lain
} else {
$smt=getData("select id_semester, tahun_ajaran||' - '||nm_semester||'-'||group_semester as smt from semester 
              where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester > (EXTRACT(YEAR FROM sysdate)-2)
			  order by thn_akademik_semester desc,group_semester desc, nm_semester desc");
$smarty->assign('T_ST', $smt);
}

$smt1 = isSet($_POST['smt']) ? $_POST['smt'] : $smtaktif['ID_SEMESTER'];
$smarty->assign('SMTGET',$smt1);

// Detect current day (Yudi Sulistya, 26/07/2012)
$today = date("w")+1;
$hariini=getvar("select id_jadwal_hari from jadwal_hari where id_jadwal_hari=$today");
$smarty->assign('hariini',$hariini['ID_JADWAL_HARI']);

$hari=getData("select id_jadwal_hari, nm_jadwal_hari from jadwal_hari");
$smarty->assign('T_HR', $hari);

$hr1 = isSet($_POST['hari']) ? $_POST['hari'] : $hariini['ID_JADWAL_HARI'];
$smarty->assign('HRGET',$hr1);

if ($smt1!='') {

if(isset($_POST['prodi']) and $_POST['prodi'] != ''){
$query = " and program_studi.id_program_studi=$_POST[prodi]";
}else{
$query = "";
}

if($unit_kerja['ID_UNIT_KERJA'] == 2){
	$query .= " and program_studi.id_program_studi=228 ";
}else{
	$query .= " and program_studi.id_fakultas=$kdfak ";
}
	
$jaf=getData("select kelas_mk.id_kelas_mk,kelas_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
nama_kelas,kapasitas_kelas_mk,terisi_kelas_mk,(select count(status_apv_pengambilan_mk) from pengambilan_mk
where kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and status_apv_pengambilan_mk=1)as kls_terisi,kelas_mk.status,nm_jenjang||'-'||coalesce(nm_singkat_prodi,nm_program_studi) as prodiasal,
jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam,
(select count(id_presensi_kelas) from presensi_kelas where kelas_mk.id_kelas_mk=presensi_kelas.id_kelas_mk) as tm, tingkat_semester, jumlah_pertemuan_kelas_mk
from kelas_mk
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and kelas_mk.id_semester=pengambilan_mk.id_semester
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join presensi_kelas on kelas_mk.id_kelas_mk=presensi_kelas.id_kelas_mk
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where kelas_mk.id_semester=$smt1 and jadwal_hari.id_jadwal_hari=$hr1
$query 
group by kelas_mk.id_kelas_mk,kelas_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
nama_kelas,kapasitas_kelas_mk,terisi_kelas_mk,kelas_mk.status,
jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
jam_mulai,menit_mulai,jam_selesai,menit_selesai,nm_jenjang,nm_singkat_prodi,nm_program_studi, tingkat_semester, jumlah_pertemuan_kelas_mk
");}
$smarty->assign('T_MK', $jaf);

$smarty->display('daftar-presensi.tpl');

?>
