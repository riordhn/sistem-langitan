<?php

error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');


$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$id_pengguna= $user->ID_PENGGUNA; 
$kdfak=$user->ID_FAKULTAS; 

$smtaktif=getvar("select * from semester where status_aktif_semester='True' AND id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI}");

$dataprodi=getData("select id_program_studi,nm_jenjang||'-' || nm_program_studi as prodi from program_studi 
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_fakultas=$kdfak  and status_aktif_prodi = 1 order by program_studi.id_jenjang");
$smarty->assign('T_PRODI', $dataprodi);

$periode=getData("SELECT NM_TARIF_WISUDA as NM_PERIODE_WISUDA FROM TARIF_WISUDA WHERE id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI} ORDER BY NM_TARIF_WISUDA DESC");
$smarty->assign('T_PW', $periode);

$periode_wisuda=$_POST['periode'];
$smarty->assign('T_PW1', $periode_wisuda);

$id_program_studi=$_POST['kdprodi'];
$smarty->assign('kodepro', $id_program_studi);
//$smarty->assign('kdprodi',$id_program_studi);



if ($_GET['action']=='edit') {
$smarty->assign('disp1','none');
$smarty->assign('disp2','block');

$id_mhs=$_GET['id_mhs'];
$id_program_studi=$_GET['id_prodi'];
$p_ws=$_GET['pws'];
$smarty->assign('p_ws',$p_ws);

$det_mhs=getData("select mhs.nim_mhs,mhs.id_mhs,
case when ps.id_jenjang=2 or ps.id_jenjang=3 then p.nm_pengguna||', '||p.gelar_belakang else p.nm_pengguna end as nm_pengguna,
mhs.id_program_studi,tw.NM_TARIF_WISUDA,pw.judul_ta,pw.judul_ta_en,pew.id_periode_wisuda, pw.id_pembimbing_1, pw.id_pembimbing_2, 
to_char(pw.bulan_awal_bimbingan, 'MM-YYYY') as bulan_awal_bimbingan, to_char(pw.bulan_akhir_bimbingan, 'MM-YYYY') as bulan_akhir_bimbingan
from mahasiswa mhs
left join pengajuan_wisuda pw on pw.id_mhs=mhs.id_mhs
left join periode_wisuda pew on pw.id_periode_wisuda=pew.id_periode_wisuda
left join tarif_wisuda tw on pew.id_tarif_wisuda=tw.id_tarif_wisuda
left join pengguna p on mhs.id_pengguna=p.id_pengguna
left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
where tw.NM_TARIF_WISUDA='$p_ws' 
and mhs.id_mhs=$id_mhs");


$smarty->assign('T_DATAMHS', $det_mhs);

$dosen=getData("select id_dosen,nip_dosen,trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nama_dosen 
	from dosen
	left join program_studi on dosen.id_program_studi=program_studi.id_program_studi
	left join pengguna b on dosen.id_pengguna=b.id_pengguna
	where 
	--program_studi.id_fakultas=$kdfak and 
	b.id_perguruan_tinggi = '{$id_pt_user}' 
	-- and dosen.status_dosen = 'PNS' and dosen.nidn_dosen is not null 
	order by nm_pengguna");
/*}*/


$smarty->assign('DOSEN', $dosen);

}

if ($_POST['action']=='update') {
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$id_mhs=$_POST['id_mhs'];
$id_periode_wisuda=$_POST['idperiode'];
$judul_ta=$_POST['judul'];
$judul_ta_en=$_POST['judul_en'];
$dosbing_1 = $_POST['dosbing_1'];
$dosbing_2 = $_POST['dosbing_2'];
$bulan_awal_bimbingan = $_POST['bulan_awal_bimbingan'];
$bulan_akhir_bimbingan = $_POST['bulan_akhir_bimbingan'];
//$periode_wisuda=$_POST['kdprodi'];

$judul_ta =  str_replace("'", "''", $judul_ta);
$judul_ta_en =  str_replace("'", "''", $judul_ta_en);

UpdateData("update pengajuan_wisuda set judul_ta='$judul_ta',judul_ta_en='$judul_ta_en', id_pembimbing_1 = '$dosbing_1', id_pembimbing_2 = '$dosbing_2', 
				bulan_awal_bimbingan = to_date('{$bulan_awal_bimbingan}', 'MM-YYYY'), bulan_akhir_bimbingan = to_date('{$bulan_akhir_bimbingan}', 'MM-YYYY') where id_mhs=$id_mhs and id_periode_wisuda=$id_periode_wisuda");
//echo "update pengajuan_wisuda set judul_ta='$judul_ta' where id_mhs=$id_mhs and id_periode_wisuda=$id_periode_wisuda";
}



if ($id_program_studi != "")
{
$det_ta_mhs=getData("select mhs.nim_mhs,mhs.id_mhs,
case when ps.id_jenjang=2 or ps.id_jenjang=3 then p.nm_pengguna||', '||p.gelar_belakang else p.nm_pengguna end as nm_pengguna,
mhs.id_program_studi,tw.NM_TARIF_WISUDA,pw.judul_ta,to_char(pw.bulan_awal_bimbingan, 'MM-YYYY') as bulan_awal_bimbingan, to_char(pw.bulan_akhir_bimbingan, 'MM-YYYY') as bulan_akhir_bimbingan,(select gelar_depan||' '||nm_pengguna||', '||gelar_belakang from pengguna join dosen on dosen.id_pengguna = pengguna.id_pengguna where dosen.id_dosen = pw.id_pembimbing_1) as dosbing_1, (select gelar_depan||' '||nm_pengguna||', '||gelar_belakang from pengguna join dosen on dosen.id_pengguna = pengguna.id_pengguna where dosen.id_dosen = pw.id_pembimbing_2) as dosbing_2
from mahasiswa mhs
left join pengajuan_wisuda pw on pw.id_mhs=mhs.id_mhs
left join periode_wisuda pew on pw.id_periode_wisuda=pew.id_periode_wisuda
left join tarif_wisuda tw on pew.id_tarif_wisuda=tw.id_tarif_wisuda
left join pengguna p on mhs.id_pengguna=p.id_pengguna
left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
where tw.NM_TARIF_WISUDA='$periode_wisuda' and ps.id_program_studi=$id_program_studi
order by mhs.nim_mhs");
}

$nmprodi=getvar("select nm_jenjang||'-'||nm_program_studi as prodi from program_studi
				 left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
				 where id_program_studi=$id_program_studi");
$smarty->assign('NAMA', $nmprodi['PRODI']);				 


$smarty->assign('T_TA', $det_ta_mhs);


$smarty->display('mon-judul.tpl');

?>



