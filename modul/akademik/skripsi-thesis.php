<?php
//Yudi Sulistya, 17/12/2013
require('common.php');
require_once ('ociFunction.php');

error_reporting (E_ALL & ~E_NOTICE);

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');

$id_pengguna = $user->ID_PENGGUNA;
$kdfak = $user->ID_FAKULTAS;

$id_pt = $id_pt_user;

//Khusus Fakultas Psikologi
/*if($kdfak==11){

	echo '
		<script>
		location.href="#ujian-thesis!skripsi-thesis-psikologi.php";
		</script>
	';

//Fakultas Lain
} else {*/

$smtaktif = getvar("select id_semester from semester where status_aktif_semester='True' and id_perguruan_tinggi = '{$id_pt}'");
$id_smt = $smtaktif['ID_SEMESTER'];
$smarty->assign('SMT', $id_smt);

$status = isset($_REQUEST['action'])? $_REQUEST['action'] : 'tampil';

switch($status) {

// Penambahan jadwal Ujian
case 'add':
$id_kelas_mk = $_GET['kls'];
$id_mhs = $_GET['mhs'];
$nm_ujian_mk = $_GET['nm'];

tambahdata("ujian_mk","id_kegiatan,id_kelas_mk,nm_ujian_mk,jam_mulai,jam_selesai,id_fakultas,id_semester","71,$id_kelas_mk,'$nm_ujian_mk','08:00','10:00',$kdfak,$id_smt");

$id_jadwal_kelas = $_GET['jad'];
$ruang = getvar("select id_ruangan from jadwal_kelas where id_jadwal_kelas=$id_jadwal_kelas and rownum=1");
$id_ruangan = $ruang['ID_RUANGAN'];

$id_ujian = getvar("
select id_ujian_mk from ujian_mk
where id_kelas_mk=$id_kelas_mk and id_kegiatan=71
and id_fakultas=$kdfak and id_semester=$id_smt
and id_ujian_mk not in (select id_ujian_mk from jadwal_ujian_mk)
");
$id_ujian_mk = $id_ujian['ID_UJIAN_MK'];
tambahdata("jadwal_ujian_mk","id_ujian_mk,id_ruangan","$id_ujian_mk,$id_ruangan");
tambahdata("ujian_mk_peserta","id_ujian_mk,id_mhs","$id_ujian_mk,$id_mhs");

$jadwal = getvar("select id_jadwal_ujian_mk from jadwal_ujian_mk where id_ujian_mk=$id_ujian_mk");
$id_jadwal_ujian_mk = $jadwal['ID_JADWAL_UJIAN_MK'];

$penguji = "
select b.id_pengguna 
from pembimbing_ta a left join dosen b on a.id_dosen=b.id_dosen 
where a.id_mhs=$id_mhs and a.status_dosen=1
";
$result = $db->Query($penguji)or die("salah kueri penguji ");
while($r = $db->FetchRow()) {
	$id_pengguna = $r[0];
	tambahdata("tim_pengawas_ujian","id_jadwal_ujian_mk,id_pengguna,status","$id_jadwal_ujian_mk,$id_pengguna,'1'");
}

echo '<script>location.href="#ujian-thesis!skripsi-thesis.php?action=penjadwalan";</script>';
break;

// Hapus jadwal
case 'del':
$id_ujian_mk = $_POST['hapus'];
$del = getvar("select id_jadwal_ujian_mk from jadwal_ujian_mk where id_ujian_mk=$id_ujian_mk");
$id_jadwal_ujian_mk = $del['ID_JADWAL_UJIAN_MK'];
deleteData("delete from ujian_mk_peserta where id_ujian_mk=$id_ujian_mk");
deleteData("delete from tim_pengawas_ujian where id_jadwal_ujian_mk=$id_jadwal_ujian_mk");
deleteData("delete from jadwal_ujian_mk where id_ujian_mk=$id_ujian_mk");
deleteData("delete from ujian_mk where id_ujian_mk=$id_ujian_mk");

echo ' ';
exit();
break;

// Tampilan form update jadwal Ujian
case 'updateview':
$smarty->assign('disp1','none');
$smarty->assign('disp2','none');
$smarty->assign('disp3','block');
$smarty->assign('disp4','none');

$id_ujian_mk = $_GET['ujian'];

$ruang = getData("
select id_ruangan,nm_ruangan as ruang from ruangan
left join gedung on gedung.id_gedung=ruangan.id_gedung
left join gedung_kuliah on gedung.id_gedung=gedung_kuliah.id_gedung
where gedung.id_fakultas=$kdfak and ruangan.kapasitas_ujian > 0 order by nm_ruangan
");
$smarty->assign('T_RUANG', $ruang);

$hari = getData("select id_jadwal_hari,nm_jadwal_hari from jadwal_hari");
$smarty->assign('T_HARI', $hari);

$dosen = getData("
select tim.id_tim_pengawas_ujian,case when tim.status=1 then 'Penguji' else 'Pengawas' end as tipe,
case when (pgg.gelar_belakang is null or pgg.gelar_belakang = '') then trim(pgg.gelar_depan||' '||upper(nm_pengguna))
else trim(pgg.gelar_depan||' '||upper(nm_pengguna)||', '||pgg.gelar_belakang) end as penguji
from tim_pengawas_ujian tim
left join jadwal_ujian_mk jad on jad.id_jadwal_ujian_mk=tim.id_jadwal_ujian_mk
left join pengguna pgg on tim.id_pengguna=pgg.id_pengguna
where jad.id_ujian_mk=$id_ujian_mk
");
$smarty->assign('DOSEN', $dosen);

$ujian_mk = getData("
select umk.id_ujian_mk,jad.id_jadwal_ujian_mk,umk.nm_ujian_mk,umk.tgl_ujian,umk.jam_mulai,umk.jam_selesai,jad.id_ruangan,
nm_jenjang||'-'||nm_singkat_prodi as prodi, upper(ta.judul_tugas_akhir) as judul, umk.nm_ujian_mk, mhs.nim_mhs, pgg.nm_pengguna
from ujian_mk umk
left join jadwal_ujian_mk jad on umk.id_ujian_mk=jad.id_ujian_mk
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
left join ujian_mk_peserta pst on umk.id_ujian_mk=pst.id_ujian_mk
left join tugas_akhir ta on pst.id_mhs=ta.id_mhs and ta.status=1
left join mahasiswa mhs on ta.id_mhs=mhs.id_mhs
left join pengguna pgg on mhs.id_pengguna=pgg.id_pengguna
where umk.id_ujian_mk=$id_ujian_mk
");
$smarty->assign('UMK', $ujian_mk);

$jaf = getData("
select * from
(select pst.id_mhs,kmk.id_kelas_mk,umk.id_ujian_mk,umk.nm_ujian_mk,to_char(umk.tgl_ujian,'DD-MM-YYYY') as tgl_ujian,
umk.jam_mulai||' - '||umk.jam_selesai as jam,rg.nm_ruangan,jjg.nm_jenjang||'-'||ps.nm_singkat_prodi as prodi, jad.id_jadwal_ujian_mk,
wm_concat('<li>'||
trim('<font color=\"black\">'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||'</font>')) as tim
from ujian_mk umk
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
left join jadwal_ujian_mk jad on jad.id_ujian_mk=umk.id_ujian_mk
left join ruangan rg on rg.id_ruangan=jad.id_ruangan
left join tim_pengawas_ujian tim on jad.id_jadwal_ujian_mk=tim.id_jadwal_ujian_mk
left join ujian_mk_peserta pst on umk.id_ujian_mk=pst.id_ujian_mk
left join pengguna pgg on pgg.id_pengguna=tim.id_pengguna
where umk.id_kegiatan=71 and umk.id_fakultas=$kdfak and umk.id_semester=$id_smt
group by pst.id_mhs, kmk.id_kelas_mk,umk.id_ujian_mk,umk.nm_ujian_mk,umk.tgl_ujian,
umk.jam_mulai,umk.jam_selesai,rg.nm_ruangan,jjg.nm_jenjang,ps.nm_singkat_prodi,jad.id_jadwal_ujian_mk) a
join
(select a.id_mhs, upper(a.judul_tugas_akhir) as judul, c.nim_mhs, d.nm_pengguna, e.id_kelas_mk
from tugas_akhir a 
left join tipe_ta b on a.id_tipe_ta = b.id_tipe_ta
left join mahasiswa c on a.id_mhs = c.id_mhs
left join pengguna d on c.id_pengguna = d.id_pengguna
left join program_studi g on c.id_program_studi = g.id_program_studi
left join kelas_mk e on g.id_program_studi = e.id_program_studi
left join kurikulum_mk f on e.id_kurikulum_mk = f.id_kurikulum_mk
where a.status = 1 and f.status_mkta = 1
and e.id_semester = $id_smt and g.id_fakultas = $kdfak) b
on a.id_mhs=b.id_mhs and a.id_kelas_mk=b.id_kelas_mk
");
$smarty->assign('T_MK', $jaf);

$tawar = getData("
select a.id_tugas_akhir, a.id_mhs, upper(a.judul_tugas_akhir) as judul, c.nim_mhs, d.nm_pengguna, e.id_kelas_mk,
h.nm_jenjang||' - '||g.nm_singkat_prodi as prodi, 'UJIAN '||upper(b.nm_tipe_ta) as nm_ujian, z.id_jadwal_kelas,
wm_concat(x.id_dosen) as pembimbing, case when a.progress is null then '0 %' else a.progress||' %' end as progress
from tugas_akhir a 
left join tipe_ta b on a.id_tipe_ta = b.id_tipe_ta
left join mahasiswa c on a.id_mhs = c.id_mhs
left join pengguna d on c.id_pengguna = d.id_pengguna
left join program_studi g on c.id_program_studi = g.id_program_studi
left join jenjang h on g.id_jenjang = h.id_jenjang
left join kelas_mk e on g.id_program_studi = e.id_program_studi
left join pembimbing_ta x on a.id_mhs = x.id_mhs and x.status_dosen = 1
left join jadwal_kelas z on z.id_kelas_mk = e.id_kelas_mk
left join kurikulum_mk f on e.id_kurikulum_mk = f.id_kurikulum_mk
where a.status = 1 and f.status_mkta = 1
and e.id_semester = $id_smt and g.id_fakultas = $kdfak
and a.id_mhs not in (select id_mhs from ujian_mk_peserta where id_ujian_mk in (select id_ujian_mk from ujian_mk where id_kegiatan = 71 and id_fakultas = $kdfak and id_semester = $id_smt))
group by a.id_tugas_akhir, a.id_mhs, a.judul_tugas_akhir, c.nim_mhs, d.nm_pengguna, e.id_kelas_mk,
h.nm_jenjang, g.nm_singkat_prodi, b.nm_tipe_ta, z.id_jadwal_kelas, a.progress
");
$smarty->assign('TAWAR', $tawar);
break;

// Update jadwal Ujian MK
case 'update':
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');

$id_ujian_mk = $_POST['ujian'];
$id_jadwal_ujian_mk = $_POST['jad'];
$tgl_ujian = $_POST['tgl_ujian'];
$mulai = $_POST['jam_mulai'];
$selesai = $_POST['jam_selesai'];
$id_ruangan = $_POST['ruangan'];

$jam_mulai = substr($mulai, 0, 2).':'.substr($mulai, -2, 2);
$jam_selesai = substr($selesai, 0, 2).':'.substr($selesai, -2, 2);

gantidata("update ujian_mk set tgl_ujian='$tgl_ujian', jam_mulai='$jam_mulai', jam_selesai='$jam_selesai' where id_ujian_mk=$id_ujian_mk");
gantidata("update jadwal_ujian_mk set id_ruangan=$id_ruangan where id_jadwal_ujian_mk=$id_jadwal_ujian_mk");

$jaf = getData("
select * from
(select pst.id_mhs,kmk.id_kelas_mk,umk.id_ujian_mk,umk.nm_ujian_mk,to_char(umk.tgl_ujian,'DD-MM-YYYY') as tgl_ujian,
umk.jam_mulai||' - '||umk.jam_selesai as jam,rg.nm_ruangan,jjg.nm_jenjang||'-'||ps.nm_singkat_prodi as prodi, jad.id_jadwal_ujian_mk,
wm_concat('<li>'||
trim('<font color=\"black\">'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||'</font>')) as tim
from ujian_mk umk
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
left join jadwal_ujian_mk jad on jad.id_ujian_mk=umk.id_ujian_mk
left join ruangan rg on rg.id_ruangan=jad.id_ruangan
left join tim_pengawas_ujian tim on jad.id_jadwal_ujian_mk=tim.id_jadwal_ujian_mk
left join ujian_mk_peserta pst on umk.id_ujian_mk=pst.id_ujian_mk
left join pengguna pgg on pgg.id_pengguna=tim.id_pengguna
where umk.id_kegiatan=71 and umk.id_fakultas=$kdfak and umk.id_semester=$id_smt
group by pst.id_mhs, kmk.id_kelas_mk,umk.id_ujian_mk,umk.nm_ujian_mk,umk.tgl_ujian,
umk.jam_mulai,umk.jam_selesai,rg.nm_ruangan,jjg.nm_jenjang,ps.nm_singkat_prodi,jad.id_jadwal_ujian_mk) a
join
(select a.id_mhs, upper(a.judul_tugas_akhir) as judul, c.nim_mhs, d.nm_pengguna, e.id_kelas_mk
from tugas_akhir a 
left join tipe_ta b on a.id_tipe_ta = b.id_tipe_ta
left join mahasiswa c on a.id_mhs = c.id_mhs
left join pengguna d on c.id_pengguna = d.id_pengguna
left join program_studi g on c.id_program_studi = g.id_program_studi
left join kelas_mk e on g.id_program_studi = e.id_program_studi
left join kurikulum_mk f on e.id_kurikulum_mk = f.id_kurikulum_mk
where a.status = 1 and f.status_mkta = 1
and e.id_semester = $id_smt and g.id_fakultas = $kdfak) b
on a.id_mhs=b.id_mhs and a.id_kelas_mk=b.id_kelas_mk
");
$smarty->assign('T_MK', $jaf);

$tawar = getData("
select a.id_tugas_akhir, a.id_mhs, upper(a.judul_tugas_akhir) as judul, c.nim_mhs, d.nm_pengguna, e.id_kelas_mk,
h.nm_jenjang||' - '||g.nm_singkat_prodi as prodi, 'UJIAN '||upper(b.nm_tipe_ta) as nm_ujian, z.id_jadwal_kelas,
wm_concat(x.id_dosen) as pembimbing, case when a.progress is null then '0 %' else a.progress||' %' end as progress
from tugas_akhir a 
left join tipe_ta b on a.id_tipe_ta = b.id_tipe_ta
left join mahasiswa c on a.id_mhs = c.id_mhs
left join pengguna d on c.id_pengguna = d.id_pengguna
left join program_studi g on c.id_program_studi = g.id_program_studi
left join jenjang h on g.id_jenjang = h.id_jenjang
left join kelas_mk e on g.id_program_studi = e.id_program_studi
left join pembimbing_ta x on a.id_mhs = x.id_mhs and x.status_dosen = 1
left join jadwal_kelas z on z.id_kelas_mk = e.id_kelas_mk
left join kurikulum_mk f on e.id_kurikulum_mk = f.id_kurikulum_mk
where a.status = 1 and f.status_mkta = 1
and e.id_semester = $id_smt and g.id_fakultas = $kdfak
and a.id_mhs not in (select id_mhs from ujian_mk_peserta where id_ujian_mk in (select id_ujian_mk from ujian_mk where id_kegiatan = 71 and id_fakultas = $kdfak and id_semester = $id_smt))
group by a.id_tugas_akhir, a.id_mhs, a.judul_tugas_akhir, c.nim_mhs, d.nm_pengguna, e.id_kelas_mk,
h.nm_jenjang, g.nm_singkat_prodi, b.nm_tipe_ta, z.id_jadwal_kelas, a.progress
");
$smarty->assign('TAWAR', $tawar);
break;

// Daftar Skripsi/TA/Thesis/Desertasi yang sudah dijadwalkan Ujian
case 'tampil':
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');

$jaf = getData("
select * from
(select pst.id_mhs,kmk.id_kelas_mk,umk.id_ujian_mk,umk.nm_ujian_mk,to_char(umk.tgl_ujian,'DD-MM-YYYY') as tgl_ujian,
umk.jam_mulai||' - '||umk.jam_selesai as jam,rg.nm_ruangan,jjg.nm_jenjang||'-'||ps.nm_singkat_prodi as prodi, jad.id_jadwal_ujian_mk,
wm_concat('<li>'||
trim('<font color=\"black\">'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||'</font>')) as tim
from ujian_mk umk
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
left join jadwal_ujian_mk jad on jad.id_ujian_mk=umk.id_ujian_mk
left join ruangan rg on rg.id_ruangan=jad.id_ruangan
left join tim_pengawas_ujian tim on jad.id_jadwal_ujian_mk=tim.id_jadwal_ujian_mk
left join ujian_mk_peserta pst on umk.id_ujian_mk=pst.id_ujian_mk
left join pengguna pgg on pgg.id_pengguna=tim.id_pengguna
where umk.id_kegiatan=71 and umk.id_fakultas=$kdfak and umk.id_semester=$id_smt
group by pst.id_mhs, kmk.id_kelas_mk,umk.id_ujian_mk,umk.nm_ujian_mk,umk.tgl_ujian,
umk.jam_mulai,umk.jam_selesai,rg.nm_ruangan,jjg.nm_jenjang,ps.nm_singkat_prodi,jad.id_jadwal_ujian_mk) a
join
(select a.id_mhs, upper(a.judul_tugas_akhir) as judul, c.nim_mhs, d.nm_pengguna, e.id_kelas_mk
from tugas_akhir a 
left join tipe_ta b on a.id_tipe_ta = b.id_tipe_ta
left join mahasiswa c on a.id_mhs = c.id_mhs
left join pengguna d on c.id_pengguna = d.id_pengguna
left join program_studi g on c.id_program_studi = g.id_program_studi
left join kelas_mk e on g.id_program_studi = e.id_program_studi
left join kurikulum_mk f on e.id_kurikulum_mk = f.id_kurikulum_mk
where a.status = 1 and f.status_mkta = 1
and e.id_semester = $id_smt and g.id_fakultas = $kdfak) b
on a.id_mhs=b.id_mhs and a.id_kelas_mk=b.id_kelas_mk
");
$smarty->assign('T_MK', $jaf);

$tawar = getData("
select a.id_tugas_akhir, a.id_mhs, upper(a.judul_tugas_akhir) as judul, c.nim_mhs, d.nm_pengguna, e.id_kelas_mk,
h.nm_jenjang||' - '||g.nm_singkat_prodi as prodi, 'UJIAN '||upper(b.nm_tipe_ta) as nm_ujian, z.id_jadwal_kelas,
wm_concat(x.id_dosen) as pembimbing, case when a.progress is null then '0 %' else a.progress||' %' end as progress
from tugas_akhir a 
left join tipe_ta b on a.id_tipe_ta = b.id_tipe_ta
left join mahasiswa c on a.id_mhs = c.id_mhs
left join pengguna d on c.id_pengguna = d.id_pengguna
left join program_studi g on c.id_program_studi = g.id_program_studi
left join jenjang h on g.id_jenjang = h.id_jenjang
left join kelas_mk e on g.id_program_studi = e.id_program_studi
left join pembimbing_ta x on a.id_mhs = x.id_mhs and x.status_dosen = 1
left join jadwal_kelas z on z.id_kelas_mk = e.id_kelas_mk
left join kurikulum_mk f on e.id_kurikulum_mk = f.id_kurikulum_mk
where a.status = 1 and f.status_mkta = 1
and e.id_semester = $id_smt and g.id_fakultas = $kdfak
and a.id_mhs not in (select id_mhs from ujian_mk_peserta where id_ujian_mk in (select id_ujian_mk from ujian_mk where id_kegiatan = 71 and id_fakultas = $kdfak and id_semester = $id_smt))
group by a.id_tugas_akhir, a.id_mhs, a.judul_tugas_akhir, c.nim_mhs, d.nm_pengguna, e.id_kelas_mk,
h.nm_jenjang, g.nm_singkat_prodi, b.nm_tipe_ta, z.id_jadwal_kelas, a.progress
");
$smarty->assign('TAWAR', $tawar);
break;

// Daftar Skripsi/TA/Thesis/Desertasi yang belum dijadwalkan Ujian
case 'penjadwalan':
$smarty->assign('disp1','none');
$smarty->assign('disp2','block');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');

$jaf = getData("
select * from
(select pst.id_mhs,kmk.id_kelas_mk,umk.id_ujian_mk,umk.nm_ujian_mk,to_char(umk.tgl_ujian,'DD-MM-YYYY') as tgl_ujian,
umk.jam_mulai||' - '||umk.jam_selesai as jam,rg.nm_ruangan,jjg.nm_jenjang||'-'||ps.nm_singkat_prodi as prodi, jad.id_jadwal_ujian_mk,
wm_concat('<li>'||
trim('<font color=\"black\">'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||'</font>')) as tim
from ujian_mk umk
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
left join jadwal_ujian_mk jad on jad.id_ujian_mk=umk.id_ujian_mk
left join ruangan rg on rg.id_ruangan=jad.id_ruangan
left join tim_pengawas_ujian tim on jad.id_jadwal_ujian_mk=tim.id_jadwal_ujian_mk
left join ujian_mk_peserta pst on umk.id_ujian_mk=pst.id_ujian_mk
left join pengguna pgg on pgg.id_pengguna=tim.id_pengguna
where umk.id_kegiatan=71 and umk.id_fakultas=$kdfak and umk.id_semester=$id_smt
group by pst.id_mhs, kmk.id_kelas_mk,umk.id_ujian_mk,umk.nm_ujian_mk,umk.tgl_ujian,
umk.jam_mulai,umk.jam_selesai,rg.nm_ruangan,jjg.nm_jenjang,ps.nm_singkat_prodi,jad.id_jadwal_ujian_mk) a
join
(select a.id_mhs, upper(a.judul_tugas_akhir) as judul, c.nim_mhs, d.nm_pengguna, e.id_kelas_mk
from tugas_akhir a 
left join tipe_ta b on a.id_tipe_ta = b.id_tipe_ta
left join mahasiswa c on a.id_mhs = c.id_mhs
left join pengguna d on c.id_pengguna = d.id_pengguna
left join program_studi g on c.id_program_studi = g.id_program_studi
left join kelas_mk e on g.id_program_studi = e.id_program_studi
left join kurikulum_mk f on e.id_kurikulum_mk = f.id_kurikulum_mk
where a.status = 1 and f.status_mkta = 1
and e.id_semester = $id_smt and g.id_fakultas = $kdfak) b
on a.id_mhs=b.id_mhs and a.id_kelas_mk=b.id_kelas_mk
");
$smarty->assign('T_MK', $jaf);

$tawar = getData("
select a.id_tugas_akhir, a.id_mhs, upper(a.judul_tugas_akhir) as judul, c.nim_mhs, d.nm_pengguna, e.id_kelas_mk,
h.nm_jenjang||' - '||g.nm_singkat_prodi as prodi, 'UJIAN '||upper(b.nm_tipe_ta) as nm_ujian, z.id_jadwal_kelas,
wm_concat(x.id_dosen) as pembimbing, case when a.progress is null then '0 %' else a.progress||' %' end as progress
from tugas_akhir a 
left join tipe_ta b on a.id_tipe_ta = b.id_tipe_ta
left join mahasiswa c on a.id_mhs = c.id_mhs
left join pengguna d on c.id_pengguna = d.id_pengguna
left join program_studi g on c.id_program_studi = g.id_program_studi
left join jenjang h on g.id_jenjang = h.id_jenjang
left join kelas_mk e on g.id_program_studi = e.id_program_studi
left join pembimbing_ta x on a.id_mhs = x.id_mhs and x.status_dosen = 1
left join jadwal_kelas z on z.id_kelas_mk = e.id_kelas_mk
left join kurikulum_mk f on e.id_kurikulum_mk = f.id_kurikulum_mk
where a.status = 1 and f.status_mkta = 1
and e.id_semester = $id_smt and g.id_fakultas = $kdfak
and a.id_mhs not in (select id_mhs from ujian_mk_peserta where id_ujian_mk in (select id_ujian_mk from ujian_mk where id_kegiatan = 71 and id_fakultas = $kdfak and id_semester = $id_smt))
group by a.id_tugas_akhir, a.id_mhs, a.judul_tugas_akhir, c.nim_mhs, d.nm_pengguna, e.id_kelas_mk,
h.nm_jenjang, g.nm_singkat_prodi, b.nm_tipe_ta, z.id_jadwal_kelas, a.progress
");
$smarty->assign('TAWAR', $tawar);
break;

case 'cari_penguji':
$smarty->assign('disp1','none');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','block');

$id_jadwal_ujian_mk=$_GET['jad'];
$id_ujian_mk = $_GET['ujian'];

$ruang = getData("
select id_ruangan,nm_ruangan as ruang from ruangan
left join gedung on gedung.id_gedung=ruangan.id_gedung
left join gedung_kuliah on gedung.id_gedung=gedung_kuliah.id_gedung
where gedung.id_fakultas=$kdfak and ruangan.kapasitas_ujian > 0 order by nm_ruangan
");
$smarty->assign('T_RUANG', $ruang);

$hari = getData("select id_jadwal_hari,nm_jadwal_hari from jadwal_hari");
$smarty->assign('T_HARI', $hari);

$dosen = getData("
select tim.id_tim_pengawas_ujian,case when tim.status=1 then 'Penguji' else 'Pengawas' end as tipe,
case when (pgg.gelar_belakang is null or pgg.gelar_belakang = '') then trim(pgg.gelar_depan||' '||upper(nm_pengguna))
else trim(pgg.gelar_depan||' '||upper(nm_pengguna)||', '||pgg.gelar_belakang) end as penguji
from tim_pengawas_ujian tim
left join jadwal_ujian_mk jad on jad.id_jadwal_ujian_mk=tim.id_jadwal_ujian_mk
left join pengguna pgg on tim.id_pengguna=pgg.id_pengguna
where jad.id_ujian_mk=$id_ujian_mk
");
$smarty->assign('DOSEN', $dosen);

$ujian_mk = getData("
select umk.id_ujian_mk,jad.id_jadwal_ujian_mk,umk.nm_ujian_mk,umk.tgl_ujian,umk.jam_mulai,umk.jam_selesai,jad.id_ruangan,
nm_jenjang||'-'||nm_singkat_prodi as prodi, upper(ta.judul_tugas_akhir) as judul, umk.nm_ujian_mk, mhs.nim_mhs, pgg.nm_pengguna
from ujian_mk umk
left join jadwal_ujian_mk jad on umk.id_ujian_mk=jad.id_ujian_mk
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
left join ujian_mk_peserta pst on umk.id_ujian_mk=pst.id_ujian_mk
left join tugas_akhir ta on pst.id_mhs=ta.id_mhs and ta.status=1
left join mahasiswa mhs on ta.id_mhs=mhs.id_mhs
left join pengguna pgg on mhs.id_pengguna=pgg.id_pengguna
where umk.id_ujian_mk=$id_ujian_mk
");
$smarty->assign('UMK', $ujian_mk);

$jaf = getData("
select * from
(select pst.id_mhs,kmk.id_kelas_mk,umk.id_ujian_mk,umk.nm_ujian_mk,to_char(umk.tgl_ujian,'DD-MM-YYYY') as tgl_ujian,
umk.jam_mulai||' - '||umk.jam_selesai as jam,rg.nm_ruangan,jjg.nm_jenjang||'-'||ps.nm_singkat_prodi as prodi, jad.id_jadwal_ujian_mk,
wm_concat('<li>'||
trim('<font color=\"black\">'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||'</font>')) as tim
from ujian_mk umk
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
left join jadwal_ujian_mk jad on jad.id_ujian_mk=umk.id_ujian_mk
left join ruangan rg on rg.id_ruangan=jad.id_ruangan
left join tim_pengawas_ujian tim on jad.id_jadwal_ujian_mk=tim.id_jadwal_ujian_mk
left join ujian_mk_peserta pst on umk.id_ujian_mk=pst.id_ujian_mk
left join pengguna pgg on pgg.id_pengguna=tim.id_pengguna
where umk.id_kegiatan=71 and umk.id_fakultas=$kdfak and umk.id_semester=$id_smt
group by pst.id_mhs, kmk.id_kelas_mk,umk.id_ujian_mk,umk.nm_ujian_mk,umk.tgl_ujian,
umk.jam_mulai,umk.jam_selesai,rg.nm_ruangan,jjg.nm_jenjang,ps.nm_singkat_prodi,jad.id_jadwal_ujian_mk) a
join
(select a.id_mhs, upper(a.judul_tugas_akhir) as judul, c.nim_mhs, d.nm_pengguna, e.id_kelas_mk
from tugas_akhir a 
left join tipe_ta b on a.id_tipe_ta = b.id_tipe_ta
left join mahasiswa c on a.id_mhs = c.id_mhs
left join pengguna d on c.id_pengguna = d.id_pengguna
left join program_studi g on c.id_program_studi = g.id_program_studi
left join kelas_mk e on g.id_program_studi = e.id_program_studi
left join kurikulum_mk f on e.id_kurikulum_mk = f.id_kurikulum_mk
where a.status = 1 and f.status_mkta = 1
and e.id_semester = $id_smt and g.id_fakultas = $kdfak) b
on a.id_mhs=b.id_mhs and a.id_kelas_mk=b.id_kelas_mk
");
$smarty->assign('T_MK', $jaf);

$tawar = getData("
select a.id_tugas_akhir, a.id_mhs, upper(a.judul_tugas_akhir) as judul, c.nim_mhs, d.nm_pengguna, e.id_kelas_mk,
h.nm_jenjang||' - '||g.nm_singkat_prodi as prodi, 'UJIAN '||upper(b.nm_tipe_ta) as nm_ujian, z.id_jadwal_kelas,
wm_concat(x.id_dosen) as pembimbing, case when a.progress is null then '0 %' else a.progress||' %' end as progress
from tugas_akhir a 
left join tipe_ta b on a.id_tipe_ta = b.id_tipe_ta
left join mahasiswa c on a.id_mhs = c.id_mhs
left join pengguna d on c.id_pengguna = d.id_pengguna
left join program_studi g on c.id_program_studi = g.id_program_studi
left join jenjang h on g.id_jenjang = h.id_jenjang
left join kelas_mk e on g.id_program_studi = e.id_program_studi
left join pembimbing_ta x on a.id_mhs = x.id_mhs and x.status_dosen = 1
left join jadwal_kelas z on z.id_kelas_mk = e.id_kelas_mk
left join kurikulum_mk f on e.id_kurikulum_mk = f.id_kurikulum_mk
where a.status = 1 and f.status_mkta = 1
and e.id_semester = $id_smt and g.id_fakultas = $kdfak
and a.id_mhs not in (select id_mhs from ujian_mk_peserta where id_ujian_mk in (select id_ujian_mk from ujian_mk where id_kegiatan = 71 and id_fakultas = $kdfak and id_semester = $id_smt))
group by a.id_tugas_akhir, a.id_mhs, a.judul_tugas_akhir, c.nim_mhs, d.nm_pengguna, e.id_kelas_mk,
h.nm_jenjang, g.nm_singkat_prodi, b.nm_tipe_ta, z.id_jadwal_kelas, a.progress
");
$smarty->assign('TAWAR', $tawar);

// Cari Dosen
if ($_POST['getdosen']=='1'){
$namacari=$_POST['namadosen'];
$upper = strtoupper($namacari);
$hasil=getData("select dosen.id_pengguna,nip_dosen,trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nm_pengguna, 
foto_pengguna, upper(nm_program_studi) as nm_program_studi
from dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
left join program_studi on dosen.id_program_studi=program_studi.id_program_studi
where nm_pengguna like '%$upper%' and dosen.id_status_pengguna=22 and
dosen.id_pengguna not in (select id_pengguna from tim_pengawas_ujian where id_jadwal_ujian_mk = $id_jadwal_ujian_mk)
order by trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang)");
$smarty->assign('CARI_DOSEN',$hasil);
}
break;

// Penambahan penguji
case 'add_tim':
$smarty->assign('disp1','none');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','block');

$id_ujian_mk = $_GET['ujian'];
$id_jadwal_ujian_mk = $_GET['jad'];
$id_pengguna = $_GET['pgg'];

tambahdata("tim_pengawas_ujian","id_jadwal_ujian_mk,id_pengguna,status","$id_jadwal_ujian_mk,$id_pengguna,'1'");
echo '<script>location.href="#ujian-thesis!skripsi-thesis.php?action=cari_penguji&ujian='.$id_ujian_mk.'&jad='.$id_jadwal_ujian_mk.'";</script>';

break;

// Hapus penguji
case 'del_tim':
$smarty->assign('disp1','none');
$smarty->assign('disp2','none');
$smarty->assign('disp3','block');
$smarty->assign('disp4','none');

$id_ujian_mk = $_GET['ujian'];
$id_jadwal_ujian_mk = $_GET['jad'];
$id_tim_pengawas_ujian = $_GET['tim'];
deleteData("delete from tim_pengawas_ujian where id_tim_pengawas_ujian=$id_tim_pengawas_ujian");

echo '<script>location.href="#ujian-thesis!skripsi-thesis.php?action=cari_penguji&ujian='.$id_ujian_mk.'&jad='.$id_jadwal_ujian_mk.'";</script>';
break;
}

$smarty->display('skripsi-thesis.tpl');
/*}*/
?>
