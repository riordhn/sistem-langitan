<?php
/*
YAH 06/06/2011
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

//$kdfak=get_fak($user->ID_PENGGUNA);
//$kdfak=$fak['ID_FAKULTAS'];
//echo $kdfak;
$kdprodi=$user->ID_PROGRAM_STUDI;

$id_pengguna= $user->ID_PENGGUNA; 
$kdfak=$user->ID_FAKULTAS; 
$smarty->assign('FAK', $kdfak);

$dataprodi=getData("select id_program_studi,nm_jenjang||'-' || nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_fakultas=$kdfak and status_aktif_prodi = 1");
$smarty->assign('T_PRODI', $dataprodi);

$dataagk=getData("select distinct thn_angkatan_mhs from mahasiswa where thn_angkatan_mhs is not null order by thn_angkatan_mhs desc");
$smarty->assign('T_AGK', $dataagk);

if (isset($_REQUEST['action'])=='view') {
$kdprodi= $_POST['kdprodi'];
$smarty->assign('PRODI', $kdprodi);

$thn_angkatan=$_POST['th_agk'];
$smarty->assign('ANGK', $thn_angkatan);
	
if($kdprodi != '' && $thn_angkatan != ''){

	$dataprodi1=getData("select (': '||nm_jenjang||'-'||nm_program_studi) as prodi, (nm_jenjang||'-'||nm_program_studi) as nama_prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_program_studi=$kdprodi and status_aktif_prodi = 1");
	$smarty->assign('NM_PRODI', $dataprodi1);

	$mk=getData("select mahasiswa.nim_mhs,upper(nm_pengguna) as nm_pengguna,mahasiswa.id_program_studi,nm_program_studi,upper(alamat_asal_mhs) as alamat_asal_mhs,sekolah_asal_mhs,
	nem_pelajaran_mhs,upper(nm_agama) as nm_agama,case when kelamin_pengguna='1' then 'L' else 'P' end as kelamin,
	lahir_kota_mhs,nm_kota,TO_CHAR(tgl_lahir_pengguna, 'DD-MM-YYYY') as tgl_lahir_pengguna,nm_jalur,mahasiswa.mobile_mhs
	from mahasiswa
	left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
	left join agama on pengguna.id_agama=agama.id_agama
	left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
	left join kota on mahasiswa.lahir_kota_mhs=kota.id_kota
	left join jalur_mahasiswa on mahasiswa.id_mhs=jalur_mahasiswa.id_mhs and mahasiswa.thn_angkatan_mhs=$thn_angkatan 
	left join jalur on jalur_mahasiswa.id_jalur=jalur.id_jalur
	where thn_angkatan_mhs=$thn_angkatan and program_studi.id_fakultas=$kdfak and mahasiswa.id_program_studi=$kdprodi and mahasiswa.status_akademik_mhs in (1,2,3)
	");

} else{
	$mk = NULL;
	echo "<script>alert('Prodi dan Angkatan belum dipilih')</script>";		
}

$smarty->assign('T_MK', $mk);
		
}

$smarty->display('data-mhs-baru.tpl');    
?>
