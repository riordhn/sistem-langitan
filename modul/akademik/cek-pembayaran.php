<?php

require('common.php');
include "class/aucc.class.php";
$id_fakultas = $user->ID_FAKULTAS;
$aucc = new AUCC_Akademik($db);
if (post('nim')) {

    $nim = str_replace("'", "''", post('nim'));
    if (strlen($nim) >= 8) {
		
			if($aucc->cek_mahasiswa_fakultas($nim, $id_fakultas)){
            	$smarty->assign('data_pembayaran', $aucc->detail_pembayaran($nim));
			}else{
				$smarty->assign('data_kosong', 'Data Mahasiswa Masih Belum ada dalam database kami');
			}
    }
}

$smarty->display('cek-pembayaran.tpl');
?>
