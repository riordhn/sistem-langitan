<?php
require '../../config.php';
require '../pendidikan/class/wisuda.class.php';
require '../pendidikan/class/date.class.php';
require_once '../../tcpdf/config/lang/ind.php';
require_once '../../tcpdf/tcpdf.php';

$tgl = tgl_indo(date('Y-m-d'));

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$wd = new wisuda($db);

$data_pdf = $wd->wisuda_yudisium($_GET['fakultas'], $_GET['status_bayar'], $_GET['periode'], $_GET['tgl_cetak']);

$db->Query("SELECT UPPER(NM_TARIF_WISUDA) AS NM_TARIF_WISUDA FROM TARIF_WISUDA WHERE ID_TARIF_WISUDA = '$_GET[periode]'");
$periode = $db->FetchAssoc();


$db->Query("SELECT * FROM FAKULTAS
			JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = FAKULTAS.ID_WADEK1
			WHERE ID_FAKULTAS = $_GET[fakultas]");
$wadek1 = $db->FetchAssoc();
$wadek = ucwords(strtolower($wadek1['NM_PENGGUNA']));
$tgl = strtoupper(tgl_indo(date("Y-m-d", strtotime($_GET['tgl_cetak']))));

$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');


$pdf->SetMargins(10, 3, 10);

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->SetAutoPageBreak(TRUE, 0);

$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

$pdf->AddPage('L', 'A4');

$pdf->setPageMark();
if($_GET['tgl_cetak'] != ''){
$html = <<<EOF
<style>
    div { margin-top: 0pt; }
    .header { font-size: 18pt; font-family: times; font-weight: bold; text-align:center;}
    .address { font-size: 14pt; font-family: serif; margin-top: 0px ;text-align:center; }
    th { 
        text-align: center;
        color: #ffffff;
        background-color: #006600;
        padding:4px;
		font-size: 10pt;
    }
    td { font-size: 9pt; }
</style>
<table width="100%" border="0">
    <tr>
        <td width="100%" align="center">
            <span class="header">MAHASISWA YANG DIAJUKAN CETAK IJASAH TANGGAL {$tgl}</span>
        </td>
    </tr>
</table>
<hr/>
<p><p/>
<table width="100%" cellpadding="3" border="0.5">
        <tr>
            <th width="3%">NO</th>
            <th width="10%">NIM</th>
            <th width="20%">NAMA</th>
            <th width="15%">FAKULTAS</th>
			<th width="8%">JENJANG</th>
            <th width="20%">PROGRAM STUDI</th>
			<th width="7%">TGL LULUS</th>
			<th width="17%">NO IJASAH</th>
        </tr>
    {data_mahasiswa}
</table>

<table border="0">
<tr><td></td></tr>
</table>

<table border="0" cellpadding="3">
	<tr>
		<td width="70%"></td>
		<td>Surabaya, </td>
	</tr>
	<tr>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>{$wadek1['GELAR_DEPAN']} {$wadek}, {$wadek1['GELAR_BELAKANG']}</td>
	</tr>
	<tr>
		<td></td>
		<td>NIP. {$wadek1['USERNAME']}</td>
	</tr>
</table>
EOF;

$index = 1;
foreach ($data_pdf as $data) {
    $data_mahasiswa .=
            "<tr>
                <td>{$index}</td>
                <td>{$data['NIM_MHS']}</td>
                <td>{$data['NM_PENGGUNA']}</td>               
                <td>{$data['NM_FAKULTAS']}</td>
				<td>{$data['NM_JENJANG']}</td>
				<td>{$data['NM_PROGRAM_STUDI']}</td>
				<td>{$data['TGL_LULUS_PENGAJUAN']}</td>
				<td>{$data['NO_IJASAH']}</td>
            </tr>";
    $index++;
}

$html = str_replace('{data_mahasiswa}', $data_mahasiswa, $html);
}else{
$html = <<<EOF
<p>Belum ada pengajuan cetak ijasah<p/>
EOF;
}
$pdf->writeHTML($html);

$pdf->Output();
?>
