<?php
include 'config.php';
include '../dosen/proses/rekap_nilai.class.php';
include '../dosen/proses/penilaian.class.php';

$penilaian = new penilaian($db);
$id_fakultas = $user->ID_FAKULTAS;

require_once('../../tcpdf/config/lang/eng.php');
require_once('../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sistem Langitan');
$pdf->SetTitle('DRAFT');
$pdf->SetSubject('DRAFT');


$pdf->SetMargins(10, 3, 10);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default footer
//$pdf->setPrintFooter(false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 0);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('times', '', 13);

// add a page
$pdf->AddPage('L', 'A4');

// draw jpeg image


// set the starting point for the page content
$pdf->setPageMark();
//$pdf->Image('includes/draft.png', 30, 70, 140, '40', '', '', '', false, 72);


		$id_kelas_mk = get('id');
        $detail_mk = $penilaian->get_kelas_mk($id_kelas_mk);
        $data_mahasiswa = $penilaian->load_data_mahasiswa($id_kelas_mk);
        $count_data_komponen_mk = count($penilaian->load_komponen_mk($id_kelas_mk));
        $data_pengajar_mk = $penilaian->load_data_pengajar_kelas_mk($id_kelas_mk);
        $data_kelas_mk_detail = $detail_mk;
        $id_fakultas_kelas = $penilaian->get_id_fakultas_kelas_mk($id_kelas_mk);
        $data_komponen_mk = $penilaian->load_komponen_mk($id_kelas_mk);
		
		
$html = '
<table width="100%" cellpadding="2" cellspacing="2" border="0.5">
             <tr bgcolor="green" style="color:#fff;">
                <th class="header-coloumn" colspan="3">Daftar Pengajar Mata Kuliah '.$data_kelas_mk_detail['NM_MATA_KULIAH']. ' '.$data_kelas_mk_detail['NM_KELAS'].'</th>
            </tr>
            <tr bgcolor="green" style="color:#fff;">
                <th width="5%">No</th>
                <th width="60%">Pengajar</th>
                <th width="35%">Status</th>
            </tr>';
			$i=1;
			foreach($data_pengajar_mk as $data){
               $html .= '<tr>
                    <td>'.$i++.'</td>
                    <td>'.$data['NM_PENGGUNA'].'</td>
                    <td>';
                        if($data['PJMK_PENGAMPU_MK']==1){
                           $html .= ' PJMK';
						}else{
                           $html .= ' Anggota';
                        }
               $html .= '</td>
                </tr>';
            }
$html .= '</table>
<br />
<br />
<table width="100%" cellpadding="2" cellspacing="2" border="0.5">
            <tr bgcolor="green" style="color:#fff;">
                <th colspan="'.($count_data_komponen_mk+6).'">Data Nilai Mahasiswa '.$data_kelas_mk_detail['NM_MATA_KULIAH'].' '. $data_kelas_mk_detail['NM_KELAS'].'</th>
            </tr>
            <tr>
                <th width="5%">
                    No
                </th>
                <th class="center" width="15%">
                    NIM
                </th>
                <th class="center" width="25%">
                    Nama
                </th>';
                foreach($data_komponen_mk as $data){
                	$html .= '<th class="center" width="5%">
                        '.$data['NM_KOMPONEN_MK'].'<br/>('.$data['PERSENTASE_KOMPONEN_MK'].'%)
                    </th>';
                }
                $html .= '<th class="center" width="10%">
                    Nilai Angka Akhir
                </th>
                <th class="center" width="10%">
                    Nilai Huruf Akhir
                </th>
                <th class="center" width="10%">
                    Tampilkan Ke Mahasiswa
                </th>
            </tr>';
            $index_nilai=1;
			$i=1;
            foreach($data_mahasiswa as $mhs){
             $html .= '<tr> 
                    <td>'.$i++.'</td>
                    <td>'.$mhs['nim'].'</td>
                    <td>'.$mhs['nama'].'</td>';
                    foreach($mhs['data_nilai'] as $nilai){
                        $html .= '<td ';
							if(($mhs['status_cekal']==0||$mhs['status_cekal']==2)&&preg_match("/[U|u][A|a][S|s]/",$nilai['NM_KOMPONEN_MK'])){
							$html .= 'style="background-color: lightcoral"';
							} 
						$html .= 'class="center">'.$nilai['BESAR_NILAI_MK'].'</td>';
                    }
                   $html .= '<td class="center">';
				   				if($mhs['nilai_angka_akhir']!=''){
									$html .= $mhs['nilai_angka_akhir'];
								}else{
									$html .= 'Kosong';
								}
					$html .= '</td>
                    <td class="center">';
					if($mhs['nilai_huruf_akhir']!=''){
					$html .= $mhs['nilai_huruf_akhir'];
					}else{
					$html .= 'Kosong';
					}
					$html .= '</td>
                    <td class="center">';
					if($mhs['flagnilai']==0){
					 $html .= 'Belum Tampil';
					 }else{
					 $html .= 'Tampil';
					 }
					 $html .= '</td>
                </tr>';
            }
$html .= ' </table>';


// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('Detail-nilai-'.$_REQUEST['id'].'.pdf', 'I');
?>
