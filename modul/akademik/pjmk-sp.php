<?php
/*
update by Yudi Sulistya on 29/12/2011
*/

error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');

$kdprodi= $user->ID_PROGRAM_STUDI; 
$kdfak= $user->ID_FAKULTAS; 

$smtaktifup=getvar("select * from semester where status_aktif_sp='True'");
$smarty->assign('smtaktif',$smtaktifup['ID_SEMESTER']);

$smt=getData("select id_semester, tahun_ajaran||' - '||nm_semester||'/'||group_semester as smt from semester order by thn_akademik_semester desc,nm_semester");
$smarty->assign('T_ST', $smt);

if ($_GET['action']=='searchdosen'){

		$id_mk=$_GET['id_klsmk'];
		$pjmk=$_GET['pjmk'];
		$id_pengampu=$_GET['id_pengampu'];
		$status1=$_GET['status1'];
			
		if ($status1=='ganti') {
		$namacari=$_POST['namadosen1'];
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','block');
		$smarty->assign('STATUS1',$status1);
		} else {
		$namacari=$_POST['namadosen'];
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','block');
		$smarty->assign('disp4','none');}
		
		if ($namacari !='') {
		// Added by Yudi Sulistya on Dec 02, 2011
		$upper = strtoupper($namacari);
		$lower = strtolower($namacari);
		$proper = ucwords($namacari);
		$hasil=getData("select id_dosen,nip_dosen,trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nm_pengguna, foto_pengguna , upper(nm_program_studi) as nm_program_studi
		from dosen 
		left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
		left join program_studi on dosen.id_program_studi=program_studi.id_program_studi
		where (nm_pengguna like '%$upper%' or nm_pengguna like '%$lower%' or nm_pengguna like '%$proper%') and dosen.id_status_pengguna=22");}
		$smarty->assign('DOSEN',$hasil);
		$smarty->assign('IDKEL',$id_mk);
		$smarty->assign('ST_PJMK',$pjmk);
		$smarty->assign('ID_PENGAMPU',$id_pengampu);
		

}
if ($_GET['action']=='addview'){

		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
		
		$id_mk=$_GET['id_klsmk'];
		$smarty->assign('IDKEL',$id_mk);
		$st=$_GET['st'];
		$pjma=$_GET['pjma'];
		$smarty->assign('PJ',$pjma);
		
		//insert dosen pengampu
		$pil=$_GET['pil'];
		//$kelasmk=$_GET['kelasmk'];
		$id_dosen=$_GET['id_dosen'];
		
		if ($pil==1) {
			if (!isFindData("pengampu_mk","pjmk_pengampu_mk=1 and id_kelas_mk=$id_mk")) {
			tambahdata("pengampu_mk","id_kelas_mk,id_dosen,pjmk_pengampu_mk","$id_mk,$id_dosen,1");
			$st=1;
			} 
			}
		if ($pil==2) {
			if (!isFindData("pengampu_mk","pjmk_pengampu_mk=0 and id_kelas_mk=$id_mk and id_dosen=$id_dosen")) {
			tambahdata("pengampu_mk","id_kelas_mk,id_dosen,pjmk_pengampu_mk","$id_mk,$id_dosen,0");
			$st=1;
			} 
			}
		//update dosen pengampu
		$id_pengampu=$_GET['id_pengampu'];
		$status1=$_GET['status1'];
		if ($status1=='ganti'){
			UpdateData("update pengampu_mk set id_dosen=$id_dosen where id_pengampu_mk=$id_pengampu");
			$st=1;
			}	
		
				
		$jaf=getData("select distinct kelas_mk.id_kelas_mk,KD_MATA_KULIAH,nm_mata_kuliah,kurikulum_mk.KREDIT_SEMESTER,nama_kelas 
from kelas_mk 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
left join semester on kelas_mk.id_semester=semester.id_semester 
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk 
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
where kelas_mk.id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak)  and kelas_mk.id_kelas_mk=$id_mk");
		$smarty->assign('T_PJMK', $jaf);
		
		$pengampu=getData("select pengampu_mk.id_dosen,pjmk_pengampu_mk,nip_dosen,trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nm_pengguna,id_pengampu_mk from pengampu_mk
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
where id_kelas_mk=$id_mk order by pjmk_pengampu_mk desc ");
		$smarty->assign('PGP', $pengampu);
		$smarty->assign('ST', $st);
		
		//$smarty->display('usulan-mk.tpl');
} 
// Added by Yudi Sulistya on Dec 28, 2011
if ($_GET['action']=='hapusdosen'){

		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');

		$id_mk=$_GET['id_klsmk'];
		$smarty->assign('IDKEL',$id_mk);
		$st=$_GET['st'];
		
		//hapus dosen pengampu
		$id_pengampu=$_GET['id_pengampu'];
		$status2=$_GET['status2'];
		if ($status2=='hapus'){
			UpdateData("delete from pengampu_mk where id_pengampu_mk=$id_pengampu");
			$st=1;
			}	
		
		echo '<script> $("#pjmk").load("pjmk-up.php"); </script>';
}			
if ($_GET['action']=='del'){	

		 // pilih
		$id_klsmk= $_GET['id_klsmk'];
				
		deleteData("delete from pengampu_mk where id_kelas_mk='$id_klsmk'");
	
		//if ($smt!=''){
		$jaf=getData("select distinct kelas_mk.id_kelas_mk,KD_MATA_KULIAH,nm_mata_kuliah,kurikulum_mk.KREDIT_SEMESTER,nama_kelas,
case when dosen.id_dosen is not null then 'sdh terisi' else 'belum terisi' end as status_pjmk, 
case when dosen.id_dosen is not null then '1' else '0' end as id_status 
from kelas_mk 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
left join semester on kelas_mk.id_semester=semester.id_semester 
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk 
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
where kelas_mk.id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak) and kelas_mk.id_semester='".$smtaktifup['ID_SEMESTER']."' order by kelas_mk.id_kelas_mk");
		$smarty->assign('PJMK', $jaf);
		//$smarty->display('usulan-mk.tpl');

}
		
		

		//echo "aa";
		//echo $smtaktif['ID_SEMESTER'];
		$smt1 = isSet($_POST['smt']) ? $_POST['smt'] : $smtaktifup['ID_SEMESTER'];
		//$smt1= $_POST['smt'];
		// if ($smt1=='') { $smt1=$smtaktif['ID_SEMESTER'];}
		//echo $smt1;
		
		if ($smt1!='') {
		$smarty->assign('SMT',$smt);
		//$smarty->assign('KUR', $id_kur);
		
		$jaf=getData("select kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas.nama_kelas,
case when wm_concat(pengguna.nm_pengguna) is not null then '1' else '0' end as id_status,
trim(wm_concat('<li>'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang)) as tim,
case when sum(pengampu_mk.pjmk_pengampu_mk)=1 then 'ada' else 'kosong' end as ada
from kelas_mk 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk 
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
where kelas_mk.id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak) and kelas_mk.id_semester=$smt1 
group by kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas.nama_kelas
order by mata_kuliah.kd_mata_kuliah, nama_kelas.nama_kelas");
}
		$smarty->assign('PJMK', $jaf);
		/*
		$pengampu=getData("select case when pjmk_pengampu_mk=1 then nm_pengguna||'(PJMA)' else nm_pengguna||'(Anggota)' end as dosen_pgp
from pengampu_mk
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
where id_kelas_mk=$id_mk");
		$smarty->assign('PGP1', $pengampu);
		*/
		

			 



$smarty->display('pjmk-sp.tpl');

?>
