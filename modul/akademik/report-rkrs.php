<?php

include 'config.php';
include '../aucc/class/utility.class.php';
include '../aucc/class/laporan.class.php';
include '../aucc/class/master.class.php';
include '../aucc/class/list_data.class.php';

$laporan = new laporan($db);
$master = new master($db);
$list = new list_data($db);

if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        $smarty->assign('data_rekap_pembayaran', $laporan->load_rekap_pembayaran(get('fakultas'), get('semester')));
    } else if (get('mode') == 'load_mhs') {
        $smarty->assign('data_status_bayar', $laporan->load_data_status_pembayaran(get('fakultas'), get('prodi'), get('semester'), get('status'), get('jalur')));
        $smarty->assign('data_fakultas_one', $list->get_fakultas(get('fakultas')));
        $smarty->assign('data_prodi_one', $list->get_prodi(get('prodi')));
        $smarty->assign('data_status_one', $list->get_status(get('status')));
    }
}

$smarty->assign('count_data_status', count($master->load_status_pembayaran()));
$smarty->assign('data_semester', $list->load_list_semester());
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->assign('data_status', $master->load_status_pembayaran());
$smarty->display('report-rkrs.tpl');
?>
