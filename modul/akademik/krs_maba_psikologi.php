<?php
// Yudi Sulistya, 06 Sep 2012
require('common.php');
require_once ('ociFunction.php');

error_reporting (E_ALL & ~E_NOTICE);

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$id_pengguna= $user->ID_PENGGUNA;
$kdfak=$user->ID_FAKULTAS;
$smarty->assign('fak',$kdfak);

//Fakultas Lain
if($kdfak!=11){
	echo 'Anda tidak memiliki hak akses';
} else {

$smtaktif=getvar("select id_semester, trim(thn_akademik_semester) as thn from semester where status_aktif_semester='True' and nm_semester='Ganjil'");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);
$smarty->assign('thnaktif',$smtaktif['THN']);

if ($_GET['action']=='reset')
{
	$id_program_studi=$_GET['prodi'];
	$id_semester=$_GET['smt'];
	$thn_agk=$_GET['thn'];

	if ($thn_agk == '' || $id_semester == '') {
	echo '<script>alert("PROSES RESET KRS MABA TIDAK DAPAT DILANJUTKAN")</script>';
	} else {

	deleteData("delete from pengambilan_mk where id_semester=$id_semester and id_mhs in
				(select id_mhs from mahasiswa where thn_angkatan_mhs=$thn_agk and id_program_studi=$id_program_studi)");

	echo '<script>alert("PROSES RESET KRS MABA ...SUDAH SELESAI...")</script>';
	}
}

if ($_POST['action']=='proses')
{
	$id_program_studi=$_POST['prodi'];
	$smarty->assign('ID_PROGRAM_STUDI',$id_program_studi);
	$counter=$_POST['counter'];
	$counter1=$_POST['counter1'];

	for ($i=1; $i<=$counter1; $i++) {
		if ($_POST['mhs'.$i]<>'' || $_POST['mhs'.$i]<>null) {
			for ($j=1; $j<=$counter; $j++) {
				if ($_POST['mk'.$j]<>'' || $_POST['mk'.$j]<>null) {
				
					$id_kur=getvar("select id_kurikulum_mk from kelas_mk where id_kelas_mk='".$_POST['mk'.$j]."'");
						
				
					gantidata("insert into pengambilan_mk (id_mhs,id_semester,id_kelas_mk,id_kurikulum_mk,status_pengambilan_mk,status_apv_pengambilan_mk,keterangan)
							  values ('".$_POST['mhs'.$i]."','$smtaktif[ID_SEMESTER]','".$_POST['mk'.$j]."','$id_kur[ID_KURIKULUM_MK]','9','1','PAKET MABA OLEH ".$id_pengguna."')");
					
					/*
					echo "insert into pengambilan_mk (id_mhs,id_semester,id_kelas_mk,id_kurikulum_mk,status_pengambilan_mk,status_apv_pengambilan_mk,keterangan)
							values ('".$_POST['mhs'.$i]."','$smtaktif[ID_SEMESTER]','".$_POST['mk'.$j]."','$id_kur[ID_KURIKULUM_MK]','9','1','PAKET MABA OLEH ".$id_pengguna."')";
					*/
				}
			}
		}
	}
	echo '<script>alert("PROSES PAKET KRS MABA ...SUDAH SELESAI...")</script>';
}

if ($_GET['action']=='view')
{
	$smarty->assign('disp1','none');
	$smarty->assign('disp2','block');

	$id_program_studi=$_GET['prodi'];
	$smarty->assign('ID_PROGRAM_STUDI',$id_program_studi);
	$detail_tawar=getData("select kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
							nama_kelas,kelas_mk.status,'0' as tanda,nm_jenjang||'-'||coalesce(nm_singkat_prodi,nm_program_studi) as prodi
							from krs_prodi
							left join program_studi on krs_prodi.id_program_studi=program_studi.id_program_studi
							left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
							left join kelas_mk on krs_prodi.id_kelas_mk=kelas_mk.id_kelas_mk and krs_prodi.id_semester=kelas_mk.id_semester
							left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
							left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
							left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
							where krs_prodi.id_semester='$smtaktif[ID_SEMESTER]' and krs_prodi.id_program_studi=$id_program_studi
							and kd_mata_kuliah in
              						(select kd_mata_kuliah from kurikulum_mk
               						join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
               						where kurikulum_mk.id_program_studi=$id_program_studi and tingkat_semester=1)
							order by kd_mata_kuliah ");
	$smarty->assign('MA_PILIH', $detail_tawar);

	$detail_mhs=getData("select nim_mhs,nm_pengguna,nm_jenjang||'-'||coalesce(nm_singkat_prodi,nm_program_studi) as prodi,
						mahasiswa.status,'nonchecked' as tanda,id_mhs,nm_agama,
						case when mahasiswa.id_mhs in
						(select id_mhs from pembayaran where id_mhs in (select id_mhs from mahasiswa where id_program_studi=$id_program_studi
						and trim(mahasiswa.thn_angkatan_mhs) in (select trim(thn_akademik_semester) from semester where status_aktif_semester='True'))
						and (tgl_bayar is not null or (id_status_pembayaran <> 2 and id_status_pembayaran is not null)) and id_semester='$smtaktif[ID_SEMESTER]'
						having count(*)>0
						group by id_mhs) then 'SUDAH' else 'BELUM' end as statusbayar,
						coalesce(nm_prodi_minat,'-') as minat
						from mahasiswa
						left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
						left join agama on pengguna.id_agama=agama.id_agama
						left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
						left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
						left join prodi_minat on prodi_minat.id_prodi_minat=mahasiswa.id_prodi_minat
						where mahasiswa.id_program_studi=$id_program_studi
						and trim(mahasiswa.thn_angkatan_mhs) in (select trim(thn_akademik_semester) from semester where status_aktif_semester='True')
						and id_mhs not in (select id_mhs from pengambilan_mk where id_semester='$smtaktif[ID_SEMESTER]'
						and id_mhs in (select id_mhs from mahasiswa where id_program_studi=$id_program_studi))
						order by nim_mhs");
	$smarty->assign('MHS_PILIH', $detail_mhs);
}

$detailkrs= getData("select prodi,s1.id_program_studi,coalesce(maba,0) as maba,coalesce(krs,0) as krs from
(select nm_jenjang||'-'||nm_program_studi as prodi,program_studi.id_program_studi,count(id_mhs) as maba from program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
join mahasiswa on program_studi.id_program_studi=mahasiswa.id_program_studi
where id_fakultas=$kdfak
and trim(mahasiswa.thn_angkatan_mhs) in (select trim(thn_akademik_semester) from semester where status_aktif_semester='True')
group by nm_jenjang||'-'||nm_program_studi, program_studi.id_program_studi,program_studi.id_jenjang
order by program_studi.id_jenjang,program_studi.id_program_studi)s1
left join
(select id_program_studi,count(distinct pengambilan_mk.id_mhs) as krs from pengambilan_mk
join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
where id_semester='$smtaktif[ID_SEMESTER]'
and trim(mahasiswa.thn_angkatan_mhs) in (select trim(thn_akademik_semester) from semester where status_aktif_semester='True')
group by id_program_studi)s2 on s1.id_program_studi=s2.id_program_studi
order by prodi");

$smarty->assign('T_MABA', $detailkrs);
$smarty->display('krs_maba_psikologi.tpl');

}
?>
