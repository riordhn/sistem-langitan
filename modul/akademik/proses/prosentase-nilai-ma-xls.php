<?php
require('../../../config.php');
$db = new MyOracle();
$db1 = new MyOracle();
$db2 = new MyOracle();

$smt = $_GET['smt'];
$thn = $_GET['thn'];
$fak = $_GET['fak'];
$prodi = $_GET['prodi'];

$nm_smt="select lower(nm_semester) as sem from semester where id_semester=".$smt."";
$result1 = $db1->Query($nm_smt)or die("salah kueri 0 ");
while($r1 = $db1->FetchRow()) {
	$nm_file_smt = $r1[0];
}

if ($prodi=='all') {
$dataprodi1="select lower(nm_fakultas) as fak from fakultas where id_fakultas=".$fak."";
$result = $db->Query($dataprodi1)or die("salah kueri 1 ");
while($r = $db->FetchRow()) {
	$nm_file = $r[0];
}

header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=dist-nilai-ma_fakultas-".$nm_file."-".$nm_file_smt."-".$thn.".xls");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data";

echo '
		<table border="1" width="100%" cellspacing="0" cellpadding="0">
           <tr>
             <td width="8%" bgcolor="lightgrey" align="center"><b>Kode</b></td>
             <td width="31%" bgcolor="lightgrey" align="center"><b>Nama Mata Ajar</b></td>
			 <td width="5%" bgcolor="lightgrey" align="center"><b>SKS</b></td>
             <td width="8%" bgcolor="lightgrey" align="center"><b>A</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>AB</b></td>
             <td width="8%" bgcolor="lightgrey" align="center"><b>B</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>BC</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>C</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>D</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>E</b></td>
           </tr>';

$all="select kd_mata_kuliah, nm_mata_kuliah, kredit_semester,
			round(sum(A)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_A,
			round(sum(AB)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_AB,
			round(sum(B)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_B,
			round(sum(BC)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_BC,
			round(sum(C)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_C,
			round(sum(D)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_D,
			round(sum(E)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_E
			from
			(select mk.kd_mata_kuliah, mk.nm_mata_kuliah, kmk.kredit_semester,
			case when nilai_huruf='A' then 1 else 0 end as A,
			case when nilai_huruf='AB' then 1 else 0 end as AB,
			case when nilai_huruf='B' then 1 else 0 end as B,
			case when nilai_huruf='BC' then 1 else 0 end as BC,
			case when nilai_huruf='C' then 1 else 0 end as C,
			case when nilai_huruf='D' then 1 else 0 end as D,
			case when (nilai_huruf='E' or nilai_huruf='T' or nilai_huruf is null) then 1 else 0 end as E
			from pengambilan_mk pmk
			left join mahasiswa mhs on pmk.id_mhs = mhs.id_mhs
			left join program_studi ps on ps.id_program_studi = mhs.id_program_studi
			left join jenjang j on j.id_jenjang = ps.id_jenjang
			left join fakultas f on f.id_fakultas = ps.id_fakultas
			left join kurikulum_mk kmk on kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
			left join mata_kuliah mk on mk.id_mata_kuliah = kmk.id_mata_kuliah
			where pmk.id_semester=".$smt." and ps.id_fakultas=".$fak." and pmk.status_apv_pengambilan_mk=1) group by kd_mata_kuliah, nm_mata_kuliah, kredit_semester
			order by nm_mata_kuliah";
$result2 = $db2->Query($all)or die("salah kueri 2 ");
while($r2 = $db2->FetchRow()) {
	$kd_ma = $r2[0];
	$nm_ma = $r2[1];
	$sks = $r2[2];
	$nilai_a = $r2[3];
	$nilai_ab = $r2[4];
	$nilai_b = $r2[5];
	$nilai_bc = $r2[6];
	$nilai_c = $r2[7];
	$nilai_d = $r2[8];
	$nilai_e = $r2[9];

echo '
			<tr>
             <td>'.$kd_ma.'</td>
             <td>'.$nm_ma.'</td>
			 <td>'.$sks.'</td>
             <td>'.$nilai_a.'%</td>
			 <td>'.$nilai_ab.'%</td>
             <td>'.$nilai_b.'%</td>
			 <td>'.$nilai_bc.'%</td>
			 <td>'.$nilai_c.'%</td>
			 <td>'.$nilai_d.'%</td>
			 <td>'.$nilai_e.'%</td>
           </tr>';
}
echo '
   	 </table>';

} else {
$dataprodi1="select lower(nm_jenjang)||'-'||lower(nm_program_studi) as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_program_studi=".$prodi."";
$result = $db->Query($dataprodi1)or die("salah kueri 1 ");
while($r = $db->FetchRow()) {
	$nm_file = $r[0];
}

header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=dist-nilai-ma-prodi-".$nm_file."-".$nm_file_smt."-".$thn.".xls");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data";

echo '
		<table border="1" width="100%" cellspacing="0" cellpadding="0">
           <tr>
             <td width="8%" bgcolor="lightgrey" align="center"><b>Kode</b></td>
             <td width="31%" bgcolor="lightgrey" align="center"><b>Nama Mata Ajar</b></td>
			 <td width="5%" bgcolor="lightgrey" align="center"><b>SKS</b></td>
             <td width="8%" bgcolor="lightgrey" align="center"><b>A</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>AB</b></td>
             <td width="8%" bgcolor="lightgrey" align="center"><b>B</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>BC</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>C</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>D</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>E</b></td>
           </tr>';

$all="select kd_mata_kuliah, nm_mata_kuliah, kredit_semester,
			round(sum(A)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_A,
			round(sum(AB)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_AB,
			round(sum(B)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_B,
			round(sum(BC)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_BC,
			round(sum(C)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_C,
			round(sum(D)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_D,
			round(sum(E)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_E
			from
			(select mk.kd_mata_kuliah, mk.nm_mata_kuliah, kmk.kredit_semester,
			case when nilai_huruf='A' then 1 else 0 end as A,
			case when nilai_huruf='AB' then 1 else 0 end as AB,
			case when nilai_huruf='B' then 1 else 0 end as B,
			case when nilai_huruf='BC' then 1 else 0 end as BC,
			case when nilai_huruf='C' then 1 else 0 end as C,
			case when nilai_huruf='D' then 1 else 0 end as D,
			case when (nilai_huruf='E' or nilai_huruf='T' or nilai_huruf is null) then 1 else 0 end as E
			from pengambilan_mk pmk
			left join mahasiswa mhs on pmk.id_mhs = mhs.id_mhs
			left join program_studi ps on ps.id_program_studi = mhs.id_program_studi
			left join jenjang j on j.id_jenjang = ps.id_jenjang
			left join fakultas f on f.id_fakultas = ps.id_fakultas
			left join kurikulum_mk kmk on kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
			left join mata_kuliah mk on mk.id_mata_kuliah = kmk.id_mata_kuliah
			where pmk.id_semester=".$smt." and ps.id_program_studi=".$prodi." and pmk.status_apv_pengambilan_mk=1) group by kd_mata_kuliah, nm_mata_kuliah, kredit_semester
			order by nm_mata_kuliah";
$result2 = $db2->Query($all)or die("salah kueri 2 ");
while($r2 = $db2->FetchRow()) {
	$kd_ma = $r2[0];
	$nm_ma = $r2[1];
	$sks = $r2[2];
	$nilai_a = $r2[3];
	$nilai_ab = $r2[4];
	$nilai_b = $r2[5];
	$nilai_bc = $r2[6];
	$nilai_c = $r2[7];
	$nilai_d = $r2[8];
	$nilai_e = $r2[9];

echo '
			<tr>
             <td>'.$kd_ma.'</td>
             <td>'.$nm_ma.'</td>
			 <td>'.$sks.'</td>
             <td>'.$nilai_a.'%</td>
			 <td>'.$nilai_ab.'%</td>
             <td>'.$nilai_b.'%</td>
			 <td>'.$nilai_bc.'%</td>
			 <td>'.$nilai_c.'%</td>
			 <td>'.$nilai_d.'%</td>
			 <td>'.$nilai_e.'%</td>
           </tr>';
}
echo '
   	 </table>';

}
?>