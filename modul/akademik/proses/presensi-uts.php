<?php
require('../../../config.php');
$db = new MyOracle();

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sistem Langitan');
$pdf->SetTitle('Presensi UTS');
$pdf->SetSubject('Presensi UTS');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

$id_pt = $id_pt_user;

//parameter header
$logo = "../../../img/akademik_images/logo-".$nama_singkat.".gif";
$logo_size = "25";
$title = strtoupper($nama_pt);

$smt = $_GET['smt'];
$thn = $_GET['thn'];

$kdprodi = $_GET['prodi'];
$kueri = "select a.nm_jenjang, b.nm_program_studi, f.nm_fakultas, f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas
			from jenjang a, program_studi b, fakultas f where a.id_jenjang=b.id_jenjang and f.id_fakultas=b.id_fakultas and b.id_program_studi=$kdprodi";
$result = $db->Query($kueri)or die("salah kueri 22 ");
while($r = $db->FetchRow()) {
	$jenjang = $r[0];
	$prodi = $r[1];
	$fak = $r[2];
	$alm_fak = $r[3];
	$pos_fak = $r[4];
	$tel_fak = $r[5];
	$fax_fak = $r[6];
	$web_fak = $r[7];
	$eml_fak = $r[8];
}

$id_ujian_mk = $_GET['ujian'];
$kueri = "select umk.nm_ujian_mk,to_char(umk.tgl_ujian,'DD-MM-YYYY'),umk.jam_mulai,umk.jam_selesai,rg.nm_ruangan,
mk.kd_mata_kuliah,mk.nm_mata_kuliah,nmk.nama_kelas,kur.kredit_semester, kur.tingkat_semester
from ujian_mk umk
left join jadwal_ujian_mk jad on umk.id_ujian_mk=jad.id_ujian_mk
left join ruangan rg on jad.id_ruangan=rg.id_ruangan
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join nama_kelas nmk on nmk.id_nama_kelas=kmk.no_kelas_mk
left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
where umk.id_kegiatan=(select id_kegiatan from kegiatan where id_perguruan_tinggi = '{$id_pt}' and kode_kegiatan = 'UTS') and umk.id_ujian_mk=$id_ujian_mk";
$result = $db->Query($kueri)or die("salah kueri 25 ");
while($r = $db->FetchRow()) {
	$tipe_umk = $r[0];
	$tgl_umk = $r[1];
	$mulai_umk = $r[2];
	$selesai_umk = $r[3];
	$ruang_umk = $r[4];
	$kode_ma = $r[5];
	$nama_ma = $r[6];
	$nama_kls = $r[7];
	$sks_ma = $r[8];
  $tingkat_smt = $r[9];
}

$id_kelas_mk = $_GET['cetak'];
$kueri1 = "select case when pengguna.gelar_belakang is null then trim(pengguna.gelar_depan||' '||upper(pengguna.nm_pengguna)) 
else trim(pengguna.gelar_depan||' '||upper(pengguna.nm_pengguna)||', '||pengguna.gelar_belakang) end as pjmk, dosen.nip_dosen as nip
from kelas_mk 
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk 
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
where kelas_mk.id_kelas_mk=$id_kelas_mk and pengampu_mk.pjmk_pengampu_mk=1";
$result = $db->Query($kueri1)or die("salah kueri 26 ");
while($r = $db->FetchRow()) {
	$pjmk = $r[0];
	$nip = $r[1];
}

$content = "FAKULTAS ".strtoupper($fak)."\n".strtoupper($jenjang)." - ".strtoupper($prodi)."\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
//$pdf->SetHeaderData($logo, $logo_size, $title, $content);
$pdf->SetPrintHeader(false);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 15, 10, 15);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 14);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 8);

// add a page
$pdf->AddPage('P', 'A4');

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="20%" align="left"> <img src="'.$logo.'" alt="" height="80" width="80"> </td>
    <td width="60%" align="center"><span style="font-size: xx-large;"><b>PRESENSI UJIAN TENGAH SEMESTER (UTS)</b></span><br/><span style="font-size: xx-large;"><b>SEMESTER '.strtoupper($smt).' '.$thn.'</b></span><br/><span style="font-size: xx-large;"><b>FAKULTAS '.strtoupper($fak).'</b></span></td>
    <td width="20%">&nbsp;</td>
  </tr>
</table>

<hr>
<hr>
';

$html .= ' 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="17">&nbsp;</td>
  </tr>
</table>

<table width="100%" cellspacing="0" cellpadding="5">
  <tr>
    <td width="12%">JURUSAN</td>
    <td width="38%"> : '.strtoupper($jenjang).' - '.strtoupper($prodi).'</td>
    <td width="15%"> MATA KULIAH</td>
    <td width="35%"> : '.$kode_ma.' - '.strtoupper($nama_ma).' ('.$sks_ma.' SKS)</td>
  </tr>
  <tr>
    <td>KELAS </td>
    <td> : '.strtoupper($nama_kls).'</td>
    <td> DOSEN</td>';

  if($pjmk != ',') {
$html .= '
  <td> : '.strtoupper($pjmk).'</td>';
} else {
$html .= '
  <td> : PJMA BELUM DITENTUKAN</td>';
}


 $html.=' 
 <!--<tr>
    <td colspan="3" width="65%">'.$kode_ma.' - '.strtoupper($nama_ma).' ('.$sks_ma.' SKS)</td>
	<td colspan="2" width="35%">RUANG / KELAS : '.$ruang_umk.' / '.$nama_kls.'</td>
  </tr>
  <tr>
	<td colspan="3" width="65%">JENIS UJIAN : '.$tipe_umk.'</td>
	<td colspan="2" width="35%">WAKTU : '.$tgl_umk.', '.$mulai_umk.' - '.$selesai_umk.'</td>
  </tr>-->
  </tr>
  <tr>
    <td>SEMESTER </td>
    <td> : '.$tingkat_smt.'</td>
    <td> RUANG </td>
    <td> : '.strtoupper($ruang_umk).'</td>
  </tr>
  <tr>
    <td>JENIS UJIAN </td>
    <td> : '.$tipe_umk.'</td>
    <td> WAKTU </td>
    <td> : '.$tgl_umk.', '.$mulai_umk.' - '.$selesai_umk.'</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
<thead>
  <tr bgcolor="lightgrey" style="color:#000;">
      <td border="1" width="5%" align="center"><strong><br/>NO.<br/>&nbsp;</strong></td>
	  <td border="1" width="15%" align="center"><strong><br/>NIM<br/>&nbsp;</strong></td>
      <td border="1" width="45%" align="center"><strong><br/>NAMA MAHASISWA<br/>&nbsp;</strong></td>
	  <td border="1" width="10%" align="center" valign="middle"><strong><br/>NILAI UTS<br/>&nbsp;</strong></td>
      <td border="1" width="25%" align="center"><strong><br/>TANDA TANGAN<br/>&nbsp;</strong></td>
    </tr>
</thead>';

$id_ujian_mk = $_GET['ujian'];
$nomor=1;
$kueri = "select mhs.nim_mhs, upper(nm_pengguna) as nm_pengguna
from ujian_mk_peserta pst
left join mahasiswa mhs on mhs.id_mhs=pst.id_mhs
left join pengguna pgg on pgg.id_pengguna=mhs.id_pengguna
where pst.id_ujian_mk=$id_ujian_mk
order by mhs.nim_mhs asc";
$result = $db->Query($kueri)or die("salah kueri 27 ");
$i=0;
while($r = $db->FetchRow()) {
	$nim_mhs = $r[0];
	$nama_mhs = $r[1];
$i++;   
if (($i % 2)==0) $tpt='align="right"';
else $tpt='align="left"';
$html .= '
    <tr nobr="true" bgcolor="#FFF" style="color:#000;">
      <td border="1" width="5%" align="center">'.$nomor.'</td>
	  <td border="1" width="15%" align="center">'.$nim_mhs.'</td>
      <td border="1" width="45%" align="left">'.strtoupper($nama_mhs).'</td>
	  <td border="1" width="10%" align="center">...............</td>
      <td border="1" width="25%" '.$tpt.'>'.$nomor.'. ...............</td>
    </tr>';
$nomor++;
}
if($pjmk != null) {
$html .= '
  <tr>
    <td colspan="5" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
	<td colspan="4">'.ucwords($kota_pt).', '.$tgl_umk.'<br/>PJMK,<br/><br/><br/><u>'.strtoupper($pjmk).'</u><br />'.$nip.'</td>
  </tr>
</table>';
} else {
$html .= '
  <tr>
    <td colspan="5" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
	<td colspan="4">'.ucwords($kota_pt).', '.$tgl_umk.'<br/><br/><br/><br/>PJMK BELUM DITENTUKAN</td>
  </tr>
</table>
';
}

$html .= '
<table width="100%" cellspacing="0" cellpadding="5">
  <tr>
    <td colspan="4" align="center"><br/><br/>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" align="center"><b><u>PENGAWAS UJIAN TENGAH SEMESTER '.strtoupper($smt).' '.$thn.'</u></b></td>
  </tr>
  <tr bgcolor="lightgrey" style="color:#000;">
      <td border="1" width="5%" align="center"><strong>NO.</strong></td>
	  <td border="1" width="20%" align="center"><strong>NIP/NIK</strong></td>
      <td border="1" width="50%" align="center"><strong>NAMA PENGAWAS</strong></td>
      <td border="1" width="25%" align="center"><strong>TANDA TANGAN</strong></td>
    </tr>';

$nomor=1;
$pengawas = "select tim.id_tim_pengawas_ujian,pgg.username,case when tim.status=1 then 'Penguji' else 'Pengawas' end as tipe,
case when (pgg.gelar_belakang is null or pgg.gelar_belakang = '') then trim(pgg.gelar_depan||' '||upper(nm_pengguna))
else trim(pgg.gelar_depan||' '||upper(nm_pengguna)||', '||pgg.gelar_belakang) end as pengawas,
case when pgg.id_role=4 then 'Dosen' else 'Pegawai' end as status
from tim_pengawas_ujian tim
left join jadwal_ujian_mk jad on jad.id_jadwal_ujian_mk=tim.id_jadwal_ujian_mk
left join pengguna pgg on tim.id_pengguna=pgg.id_pengguna
where jad.id_ujian_mk=$id_ujian_mk
order by status, pengawas";
$result = $db->Query($pengawas)or die("salah kueri 99 ");
$i=0;
while($r = $db->FetchRow()) {
	$nip = $r[1];
	$nama = $r[3];
	$status = $r[4];
$i++;   
if (($i % 2)==0) $tpt='align="right"';
else $tpt='align="left"';
$html .= '
    <tr nobr="true" bgcolor="#FFF" style="color:#000;">
      <td border="1" width="5%" align="center">'.$nomor.'</td>
	  <td border="1" width="20%" align="left">'.$nip.'</td>
      <td border="1" width="50%" align="left">'.$nama.'</td>
      <td border="1" width="25%" '.$tpt.'>'.$nomor.'. ...............</td>
    </tr>';
$nomor++;
}
$html .= '
</table>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('PRESENSI UTS '.strtoupper($nama_ma).' '.strtoupper($ruang_umk).'.pdf', 'I');

?>