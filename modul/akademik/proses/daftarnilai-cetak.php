<?php
include '../../../config.php';

$akademik = new Akademik($db); // includes/Akademik.class.php

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sistem Langitan');
$pdf->SetTitle('Daftar Nilai');
$pdf->SetSubject('Daftar Nilai');

include '../includes/function.php';
$var = decode($_SERVER['REQUEST_URI']);
$id_mhs = $var['cetak'];

if ($id_mhs == NULL) die('Error in line '.__LINE__); // id_mhs tidak ditemukan

$biomhs = 
		"SELECT
		mhs.nim_mhs,
		CASE
			WHEN j.id_jenjang = 2 THEN p.nm_pengguna || ', ' || p.gelar_belakang
			ELSE p.nm_pengguna
		END ,
		j.nm_jenjang,
		ps.nm_program_studi,
		mhs.status_akademik_mhs,
		f.nm_fakultas,
		f.alamat_fakultas,
		f.kodepos_fakultas,
		f.telpon_fakultas,
		f.faksimili_fakultas,
		f.website_fakultas,
		f.email_fakultas,
		f.id_fakultas,
		kt.nm_kota,
		TO_CHAR(p.tgl_lahir_pengguna, 'DD-MM-YYYY'),
		TO_CHAR(mhs.tgl_lulus_mhs, 'DD-MM-YYYY'),
		pw.no_ijasah,
		TO_CHAR(mhs.tgl_terdaftar_mhs, 'DD-MM-YYYY'),
		mhs.id_mhs,
		pw.judul_ta,
		pw.lahir_ijazah,
		j.id_jenjang,
		mhs.id_program_studi,
		pm.nm_prodi_minat,
		mhs.id_mhs
	FROM mahasiswa mhs
	LEFT JOIN kota kt ON kt.id_kota = mhs.lahir_kota_mhs
	LEFT JOIN pengajuan_wisuda pw ON pw.id_mhs = mhs.id_mhs
	LEFT JOIN pengguna p ON mhs.id_pengguna = p.id_pengguna
	LEFT JOIN program_studi ps ON mhs.id_program_studi = ps.id_program_studi
	LEFT JOIN fakultas f ON ps.id_fakultas = f.id_fakultas
	LEFT JOIN jenjang j ON ps.id_jenjang = j.id_jenjang
	LEFT JOIN prodi_minat pm ON mhs.id_prodi_minat = pm.id_prodi_minat
	WHERE mhs.id_mhs = {$id_mhs}";
$result1 = $db->Query($biomhs) or die("salah kueri biodata ");
while($r1 = $db->FetchRow()) {
	$nim_mhs = $r1[0];
	$nim = $r1[0];
	$nm_mhs = $r1[1];
	$jenjang = $r1[2];
	$prodi = $r1[3];
	$status = $r1[4];
	$fak = $r1[5];
	$alm_fak = $r1[6];
	$pos_fak = $r1[7];
	$tel_fak = $r1[8];
	$fax_fak = $r1[9];
	$web_fak = $r1[10];
	$eml_fak = $r1[11];
	$kd_fak = $r1[12];
	$tpt_lhr = $r1[13];
	$tgl_lhr = $r1[14];
	$tgl_lls = $r1[15];
	$no_ijazah = $r1[16];
	$tgl_dtr = $r1[17];
	$id_mhs = $r1[18];
	$jdl_ta = $r1[19];
	$ttl = $r1[20];
	$jjg1 = $r1[21];
	$idprodi = $r1[22];
	$minatmhs = $r1[23];
	$id_mhs = $r1[24];
}

// Pengambilan IPK
$ipk = $akademik->get_ipk_mhs($id_mhs);

// Pengambilan
$pengambilan_mk_set = $akademik->list_pengambilan_mk_mhs($id_mhs); 

$dekan="select d.nip_dosen ,case when p.gelar_belakang is not null then 
trim(p.gelar_depan||' '||upper(p.nm_pengguna)||', '||p.gelar_belakang) else 
trim(p.gelar_depan||' '||upper(p.nm_pengguna)) end as nm_dosen, 
'Dekan' as dekan,
'Wakil Dekan 1' as wadek
from fakultas f, pengguna p, dosen d
where p.id_pengguna=f.id_kabag_akademik and f.id_fakultas=".$kd_fak." and p.id_pengguna=d.id_pengguna";
$resultdekan = $db->Query($dekan)or die("salah kueri dekan ");
while($dkn = $db->FetchRow()) {
	$nm_baak = $dkn[1];
	$nip_baak = $dkn[0];
	/*$ttd_dekan=$dkn[2];
	$ttd_wadek=$dkn[3];*/
}

$bulan['01'] = "Januari"; $bulan['02'] = "Pebruari"; $bulan['03'] = "Maret"; $bulan['04'] = "April"; $bulan['05'] = "Mei"; $bulan['06'] = "Juni"; $bulan['07'] = "Juli"; $bulan['08'] = "Agustus"; $bulan['09'] = "September"; $bulan['10'] = "Oktober";  $bulan['11'] = "Nopember";  $bulan['12'] = "Desember";


// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 10, 10);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 10);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 8);

// add a page
$pdf->AddPage('P', 'Legal');

// draw jpeg image
//$pdf->Image('../includes/logo_unair.png', 75, 25, 150, '', '', '', '', false, 72);
//$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
// set the starting point for the page content
$pdf->setPageMark();

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td width="15%" align="left" valign="middle"><img src="../../../img/akademik_images/logo-'.$nama_singkat.'.gif" width="65" border="0"></td>
    <td width="85%" align="left" valign="middle"><font size="10"><b>'.strtoupper($nama_pt).'<br>'.strtoupper($fak).'</b></font><br>'.strtoupper($alm_fak).', '.$pos_fak.'<br>Telp. '.$tel_fak.', Fax. '.$fax_fak.'<br>'.strtolower($web_fak).', '.strtolower($eml_fak).'</td>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2">
		<table style="border-bottom:2px solid black; width:98%">
			<tr>
				<td colspan="2"><br>&nbsp;</td>
			</tr>
		</table>
	</td>
  </tr>
  <tr>
    <td colspan="2" align="center"><font size="11"><br><b>DAFTAR NILAI</b></font></td>
  </tr>
  <tr>
    <td colspan="2"><br>&nbsp;</td>
  </tr>
  <tr>
    <td width="15%"><font size="9">NIM</font></td>
	<td width="30%"><font size="9"> : '.$nim_mhs.'</font></td>
	<td width="10%"></td>
    <td width="15%"><font size="9">Program Studi</font></td>
	<td width="30%"><font size="9"> : '.$prodi.'</font></td>
	</tr>
  <tr>
    <td><font size="9">Nama</font></td>
	<td><font size="9"> : '.$nm_mhs.'</font></td>
	<td></td>
	<td><font size="9">Total SKS / Bobot</font></td>
	<td><font size="9"> : '.$ipk['TOTAL_SKS'].' / '.$ipk['TOTAL_BOBOT'].'</font></td>
  </tr>
  <tr>
    <td><font size="9">Tempat, Tgl Lahir</font></td>
	<td><font size="9"> : '.$tpt_lhr.', '.date("d", strtotime($tgl_lhr)).' '.$bulan[date("m", strtotime($tgl_lhr))].' '.date("Y", strtotime($tgl_lhr)).'</font></td>
	<td></td>
	<td><font size="9">IPK Sementara</font></td>
	<td><font size="9"> : '.$ipk['IPK'].'</font></td>
  </tr>
  <tr>
  <td colspan="2"><br>&nbsp;</td>
  </tr>
</table>

<table width="100%" border="1" cellpadding="2">
<tr>
	<td width="5%" align="center"><font size="7"><b>THN</b></font></td>
	<td width="8%" align="center"><font size="7"><b>KODE</b></font></td>
	<td width="29%" align="center"><font size="7"><b>NAMA MATA AJAR</b></font></td>
	<td width="4%" align="center"><font size="7"><b>SKS</b></font></td>
	<td width="4%" align="center"><font size="7"><b>NILAI</b></font></td>
	<td width="5%" align="center"><font size="7"><b>THN</b></font></td>
	<td width="8%" align="center"><font size="7"><b>KODE</b></font></td>
	<td width="29%" align="center"><font size="7"><b>NAMA MATA AJAR</b></font></td>
	<td width="4%" align="center"><font size="7"><b>SKS</b></font></td>
	<td width="4%" align="center"><font size="7"><b>NILAI</b></font></td>
</tr>';

$jumlah_baris_max = ceil(count($pengambilan_mk_set) / 2.0);

for ($i = 0; $i < $jumlah_baris_max; $i++) {

	$kode_semester = 
		($pengambilan_mk_set[$i]['NM_SEMESTER'] == 'Ganjil') ? '1' :
		($pengambilan_mk_set[$i]['NM_SEMESTER'] == 'Genap') ? '2' : '3';

	$html .= 
		'<tr style="color:#000;">
		<td align="center"><font size="7">'.$pengambilan_mk_set[$i]['THN_AKADEMIK_SEMESTER'].'-'.$kode_semester.'</font></td>
		<td align="left"><font size="7">'.$pengambilan_mk_set[$i]['KD_MATA_KULIAH'].'</font></td>
		<td align="left"><font size="7">'.$pengambilan_mk_set[$i]['NM_MATA_KULIAH'].'</font></td>
		<td align="center"><font size="7">'.$pengambilan_mk_set[$i]['KREDIT_SEMESTER'].'</font></td>
		<td align="center"><font size="7">'.$pengambilan_mk_set[$i]['NILAI_HURUF'].'</font></td>';
	
	// Kolom Sebelah Kanan
	if (isset($pengambilan_mk_set[$i + $jumlah_baris_max])) {
		$html .=
		'<td align="center"><font size="7">'.$pengambilan_mk_set[$i + $jumlah_baris_max]['THN_AKADEMIK_SEMESTER'].'-'.$kode_semester.'</font></td>
		<td align="left"><font size="7">'.$pengambilan_mk_set[$i + $jumlah_baris_max]['KD_MATA_KULIAH'].'</font></td>
		<td align="left"><font size="7">'.$pengambilan_mk_set[$i + $jumlah_baris_max]['NM_MATA_KULIAH'].'</font></td>
		<td align="center"><font size="7">'.$pengambilan_mk_set[$i + $jumlah_baris_max]['KREDIT_SEMESTER'].'</font></td>
		<td align="center"><font size="7">'.$pengambilan_mk_set[$i + $jumlah_baris_max]['NILAI_HURUF'].'</font></td>
		</tr>';
	}
	else {
		$html .=
		'<td align="center"><font size="7"></font></td>
		<td align="left"><font size="7"></font></td>
		<td align="left"><font size="7"></font></td>
		<td align="center"><font size="7"></font></td>
		<td align="center"><font size="7"></font></td>
		</tr>';
	}
	
}

$list2="SELECT SYSDATE FROM DUAL";
$result22 = $db->Query($list2)or die("salah kueri list2 ");
while($r22 = $db->FetchRow()) {
	$tglcetak=$r22[0];
}

// UMAHA
if ($user->ID_PERGURUAN_TINGGI == 1)
{
	$kota_ttd = 'Sidoarjo';
	$jabatan_ttd = 'Direktur Akademik, SDM, & Sisfor';
	$nama_ttd = 'Wiji Lestariningsih, S.Pd., M.Pd.';
	$nip_ttd = 'NIDN 0707058501';
}
// UNU LAMPUNG
else if ($user->ID_PERGURUAN_TINGGI == 5)
{
	$kota_ttd = 'Lampung Timur';
	$jabatan_ttd = 'Kepala BAAKSI';
	$nama_ttd = 'Nur Syamsiah, S.Pd., M.Pd.';
	$nip_ttd = 'NIP 88203012';
}
else 
{
	$kota_ttd = '';
	$jabatan_ttd = '';
	$nama_ttd = '';
	$nip_ttd = '';
}

$html .= '
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td colspan="10"><br>&nbsp;</td>
</tr>
<tr>
    <td colspan="10"><br>&nbsp;</td>
</tr>

<tr>
	<td width="55%"><font size="9">&nbsp;</font></td>
    <td width="45%"><font size="9">'.ucwords($kota_pt).',&nbsp;'.date("d", strtotime($tglcetak)).' '.$bulan[date("m", strtotime($tglcetak))].' '.date("Y", strtotime($tglcetak)).'<br>'.$jabatan_ttd.'<br><br><br><br><br>'.$nama_ttd.'<br>'.$nip_ttd.'</font></td>
  </tr>
</table>

';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('TRANSKRIP AKADEMIK '.strtoupper($nim_mhs).'.pdf', 'I');

?>