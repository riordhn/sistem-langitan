<?php
require('../../../config.php');
$db = new MyOracle();
$db1 = new MyOracle();


require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('Cek Wajib Bebas');
$pdf->SetSubject('Cek Wajib Bebas');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
//$logo = "../../img/akademik_images/logounair.gif";
//$logo_size = "25";
//$title = "UNIVERSITAS AIRLANGGA";

include '../includes/function.php';
//$var = decode($_SERVER['REQUEST_URI']);
$nim = $_GET['cetak'];
//echo $nim;
$biomhs="select mhs.nim_mhs, case when j.id_jenjang=2 then p.nm_pengguna||', '||p.gelar_belakang else p.nm_pengguna end , j.nm_jenjang, ps.nm_program_studi, mhs.status_akademik_mhs, f.nm_fakultas,
			f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas, f.id_fakultas,
			kt.nm_kota, TO_CHAR(p.tgl_lahir_pengguna, 'DD-MM-YYYY'), TO_CHAR(mhs.tgl_lulus_mhs, 'DD-MM-YYYY'), pw.no_ijasah, TO_CHAR(mhs.tgl_terdaftar_mhs, 'DD-MM-YYYY'), mhs.id_mhs, pw.judul_ta, pw.lahir_ijazah,
			j.id_jenjang,mhs.id_program_studi,pm.nm_prodi_minat,mhs.id_mhs
			from mahasiswa mhs
			left join kota kt on kt.id_kota=mhs.lahir_kota_mhs
			left join pengajuan_wisuda pw on pw.id_mhs=mhs.id_mhs
			left join pengguna p on mhs.id_pengguna=p.id_pengguna
			left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
			left join fakultas f on ps.id_fakultas=f.id_fakultas
			left join jenjang j on ps.id_jenjang=j.id_jenjang
			left join prodi_minat pm on mhs.id_prodi_minat=pm.id_prodi_minat
			where mhs.nim_mhs='".$nim."'";
$result1 = $db->Query($biomhs)or die("salah kueri biodata ");
while($r1 = $db->FetchRow()) {
	$nim_mhs = $r1[0];
	$nm_mhs = $r1[1];
	$jenjang = $r1[2];
	$prodi = $r1[3];
	$status = $r1[4];
	$fak = $r1[5];
	$alm_fak = $r1[6];
	$pos_fak = $r1[7];
	$tel_fak = $r1[8];
	$fax_fak = $r1[9];
	$web_fak = $r1[10];
	$eml_fak = $r1[11];
	$kd_fak = $r1[12];
	$tpt_lhr = $r1[13];
	$tgl_lhr = $r1[14];
	$tgl_lls = $r1[15];
	$no_ijazah = $r1[16];
	$tgl_dtr = $r1[17];
	$id_mhs = $r1[18];
	$jdl_ta = $r1[19];
	$ttl = $r1[20];
	$jjg1 = $r1[21];
	$idprodi = $r1[22];
	$minatmhs = $r1[23];
	$id_mhs = $r1[24];
}


$nilai="select id_jenjang, sum(sks) as sks_total, round((sum((bobot*sks))/sum(sks)),2) as ipk,sum(bobot*sks) as bobot 
from
(
select ps.id_jenjang, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah,d.kredit_semester order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join program_studi ps on m.id_program_studi=ps.id_program_studi
where rangking=1 and m.nim_mhs='".$nim."'
)
group by id_jenjang";
$result2 = $db->Query($nilai)or die("salah kueri nilai ");
while($r2 = $db->FetchRow()) {
	$jjg = $r2[0];
	$jum_sks = $r2[1];
	$ipk = number_format($r2[2],2,'.',',');
	$bobot = number_format($r2[3],2,'.',',');
}

$pre="select nm_mhs_predikat 
from mhs_predikat left join jenjang on mhs_predikat.id_jenjang=jenjang.id_jenjang 
where jenjang.id_jenjang=$jjg and ipk_min_mhs_predikat<=$ipk and ipk_max_mhs_predikat>=$ipk";
$result_pre = $db->Query($pre)or die("salah kueri predikat ");
while($r_pre = $db->FetchRow()) {
	$predikat = $r_pre[0];
}

$jml2 = "select count(*) as jml from (
select e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where rangking=1 and m.nim_mhs='".$nim."')";
$resultjml2 = $db->Query($jml2)or die("salah kueri jml2 ");
while($jml2 = $db->FetchRow()) {
	$jml_2 = $jml2[0];

	$total = $jml_2 / 2;

	$limit = round($total, 0, PHP_ROUND_HALF_UP);
}

$bulan['01'] = "Januari"; $bulan['02'] = "Pebruari"; $bulan['03'] = "Maret"; $bulan['04'] = "April"; $bulan['05'] = "Mei"; $bulan['06'] = "Juni"; $bulan['07'] = "Juli"; $bulan['08'] = "Agustus"; $bulan['09'] = "September"; $bulan['10'] = "Oktober";  $bulan['11'] = "Nopember";  $bulan['12'] = "Desember";


// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(2, 2, 2);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 0);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 9);

// add a page
$pdf->AddPage('P', 'Legal');

// draw jpeg image
//$pdf->Image('../includes/logo_unair.png', 75, 25, 150, '', '', '', '', false, 72);
//$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
// set the starting point for the page content
$pdf->setPageMark();

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td width="15%" align="left" valign="middle"><img src="../includes/logounair.gif" width="65" border="0"></td>
    <td width="85%" align="left" valign="middle"><font size="10"><b>UNIVERSITAS AIRLANGGA<br>FAKULTAS '.strtoupper($fak).'</b></font><br>'.strtoupper($alm_fak).', '.$pos_fak.'<br>Telp. '.$tel_fak.', Fax. '.$fax_fak.'<br>'.strtolower($web_fak).', '.strtolower($eml_fak).'</td>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2">
		<table style="border-bottom:2px solid black; width:98%">
			<tr>
				<td colspan="2"><br>&nbsp;</td>
			</tr>
		</table>
	</td>
  </tr>
  <tr>
    <td colspan="2" align="center"><font size="11"><br><b>CEK WAJIB BEBAS</b></font></td>
  </tr>
  <tr>
    <td colspan="2"><br>&nbsp;</td>
  </tr>
  <tr>
    <td width="15%"><font size="10">NIM</font></td>
	<td width="30%"><font size="10"> : '.$nim_mhs.'</font></td>
	<td width="10%"></td>
    <td width="15%"><font size="10">Program Studi</font></td>
	<td width="30%"><font size="10"> : '.ucwords(strtolower($prodi)).'</font></td>
  </tr>
  <tr>
    <td><font size="10">Nama</font></td>
	<td><font size="10"> : '.ucwords(strtolower($nm_mhs)).'</font></td>
	<td></td>
	<td><font size="10">Total SKS / Bobot</font></td>
	<td><font size="10"> : '.$jum_sks.' / '.$bobot.'</font></td>
  </tr>
  <tr>
    <td><font size="10">Tempat, Tgl Lahir</font></td>
	<td><font size="10"> : '.ucwords(strtolower($tpt_lhr)).', '.date("d", strtotime($tgl_lhr)).' '.$bulan[date("m", strtotime($tgl_lhr))].' '.date("Y", strtotime($tgl_lhr)).'</font></td>
	<td></td>
	<td><font size="10">IPK Sementara</font></td>
	<td><font size="10"> : '.$ipk.'</font></td>
  </tr>
  <tr>
  <td colspan="2"><br>&nbsp;</td>
  </tr>
</table>
	
<table width="100%" cellspacing="0" cellpadding="3" border="1">
	<tr>
		 <td width="8%" align="center"><b>Kode</b></td>
		 <td width="43%" align="center"><b>Nama MA</b></td>
         <td width="5%" align="center"><b>SKS</b></td>
		 <td width="6%" align="center"><b>Nilai</b></td>
		 <td width="7%" align="center"><b>Thn Kur</b></td>
		 <td width="29%" align="center"><b>Status_MA</b></td>
    </tr>';

/*
$rekap="select nm_status_mk,sum(sks) as ttlsks from (
select nim_mhs,a.id_semester,d.id_kurikulum_mk,e.kd_mata_kuliah as kode,e.nm_mata_kuliah as nama,
d.kredit_semester as sks,nilai_huruf as nilai,
case when s.group_semester='Ganjil' then s.thn_akademik_semester||'-'||'1' else s.thn_akademik_semester||'-'||'2' end as thn, 
s.group_semester,l.tahun_kurikulum,m.nm_status_mk, 
row_number() over(partition by a.id_mhs,e.kd_mata_kuliah order by nilai_huruf,thn_akademik_semester||group_semester desc) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join mahasiswa m on a.id_mhs=m.id_mhs
left join semester s on a.id_semester=s.id_semester
left join kurikulum_prodi l on d.id_kurikulum_prodi=l.id_kurikulum_prodi 
left join kurikulum_minat k on d.id_kurikulum_mk=k.id_kurikulum_mk 
left join status_mk m on k.id_status_mk=m.id_status_mk 
where a.nilai_huruf<'E' and a.nilai_huruf is not null and nim_mhs='".$nim."' and a.status_hapus=0 and a.status_apv_pengambilan_mk=1)
where rangking=1 
group by nm_status_mk 
order by nm_status_mk";
*/

$rekap=getData("select distinct m.nm_status_mk from kurikulum_minat k
left join status_mk m on k.id_status_mk=m.id_status_mk 
left join kurikulum_prodi l on k.id_kurikulum_prodi=l.id_kurikulum_prodi
left join kurikulum_mk d on k.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where l.status_aktif=1 and l.id_program_studi in (select id_program_studi from mahasiswa where nim_mhs='".$nim."')
order by m.nm_status_mk");

$jaf="select kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nilai,nm_status_mk,tahun_kurikulum from 
(select kd_mata_kuliah,nm_mata_kuliah,d.kredit_semester,m.nm_status_mk,l.tahun_kurikulum from kurikulum_minat k
left join status_mk m on k.id_status_mk=m.id_status_mk 
left join kurikulum_prodi l on k.id_kurikulum_prodi=l.id_kurikulum_prodi
left join kurikulum_mk d on k.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where l.status_aktif=1 and l.id_program_studi in (select id_program_studi from mahasiswa where nim_mhs='".$nim."')
order by m.nm_status_mk)s1
left join
(select nim_mhs,kode,nama,nilai from (
select nim_mhs,e.kd_mata_kuliah as kode,e.nm_mata_kuliah as nama,
nilai_huruf as nilai,
row_number() over(partition by a.id_mhs,e.kd_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join mahasiswa m on a.id_mhs=m.id_mhs
where a.nilai_huruf<'E' and a.nilai_huruf is not null and nim_mhs='".$nim."' and a.status_hapus=0 and a.status_apv_pengambilan_mk=1)
where rangking=1) s2 on s1.kd_mata_kuliah=s2.kode
order by nm_status_mk,kd_mata_kuliah";

$result30 = $db->Query($rekap)or die("salah kueri rekap ");
$result31 = $db1->Query($jaf)or die("salah kueri jaf ");
//while($r30 = $db->FetchRow()) {
//	$a1=$r30[0];
//	$b2=$r30[1];

	
	while($r31 = $db1->FetchRow()) {
	$a=$r31[0];
	$b=$r31[1];
    	$c=$r31[2];
	$d=$r31[3];
	$e=$r31[5];
	$f=$r31[4];
	//echo $r31[0];
	
	//if ($a1==$f) {
	
	
		$html .= '<tr>
			 <td>'.$a.'</td>
			 <td>'.$b.'</td>
             <td align="center">'.$c.'</td>
			 <td align="center">'.$d.'</td>
			 <td>'.$e.'</td>
			 <td>'.$f.'</td>
         </tr>';
	   
	
	//}
			
	}
	
//}
$html .= '
</table>';
$list2="SELECT SYSDATE FROM DUAL";
$result22 = $db->Query($list2)or die("salah kueri list2 ");
while($r22 = $db->FetchRow()) {
	$tglcetak=$r22[0];
}

$list3="select nip_dosen,case when gelar_belakang is not null then 
trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) else 
trim(gelar_depan||' '||upper(nm_pengguna)) end from dosen
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
left join program_studi on dosen.id_program_studi=program_studi.id_program_studi
where id_jabatan_pegawai=6 and id_fakultas='".$kd_fak."'";
$result23 = $db->Query($list3)or die("salah kueri list3 ");
while($r23 = $db->FetchRow()) {
	$nipwadek=$r23[0];
	$nmwadek=$r23[1];
}


// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('Cek Wajib Bebas '.strtoupper($nim_mhs).'.pdf', 'I');

?>