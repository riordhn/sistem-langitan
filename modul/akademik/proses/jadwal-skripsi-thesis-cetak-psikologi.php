<?php
require('../../../config.php');
$db = new MyOracle();

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages().' - '.date("Y-m-d H:i:s", time()), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('Daftar Ujian');
$pdf->SetSubject('Daftar Ujian');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logounair.gif";
$logo_size = "25";
$title = "UNIVERSITAS AIRLANGGA";

$kdfak = $user->ID_FAKULTAS;
$ujian = $_GET['cetak'];
$id_smt = $_GET['smt'];

$thn_smt = "select nm_semester, tahun_ajaran from semester where id_semester=$id_smt";
$result = $db->Query($thn_smt)or die("salah kueri thn_smt ");
while($r = $db->FetchRow()) {
	$smt = $r[0];
	$thn = $r[1];
}

$fak = "select a.nm_jenjang, b.nm_program_studi, f.nm_fakultas, f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas
			from jenjang a, program_studi b, fakultas f where a.id_jenjang=b.id_jenjang and f.id_fakultas=b.id_fakultas and b.id_fakultas=$kdfak";
$result = $db->Query($fak)or die("salah kueri fak ");
while($r = $db->FetchRow()) {
	$jenjang = $r[0];
	$prodi = $r[1];
	$fak = $r[2];
	$alm_fak = $r[3];
	$pos_fak = $r[4];
	$tel_fak = $r[5];
	$fax_fak = $r[6];
	$web_fak = $r[7];
	$eml_fak = $r[8];
}

$content = "FAKULTAS ".strtoupper($fak)."\n\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(5, 35, 5);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 15);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage('L', 'A4');

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="5" align="center"><b><u>DAFTAR UJIAN SKRIPSI/THESIS/DESERTASI SEMESTER '.strtoupper($smt).' '.$thn.'</u></b></td>
  </tr>
  <tr>
    <td colspan="5">&nbsp;</td>
  </tr>
</table>';

$html .= '
<table width="100%" cellspacing="0" cellpadding="5" border="1">
<thead>
  <tr bgcolor="black" style="color:white;">
      <td border="1" width="5%" align="center"><strong>NO.</strong></td>
	  <td border="1" width="13%" align="center"><strong>NIM<br>NAMA MAHASISWA</strong></td>
	  <td border="1" width="20%" align="center"><strong>JUDUL</strong></td>
      <td border="1" width="15%" align="center"><strong>PRODI<br>PEMINATAN</strong></td>
      <td border="1" width="7%" align="center"><strong>JENIS</strong></td>
      <td border="1" width="10%" align="center"><strong>RUANG<br>WAKTU</strong></td>
      <td border="1" width="30%" align="center"><strong>PENGUJI</strong></td>
    </tr>
</thead>';

$nomor=1;
$list = "select * from
(select pst.id_mhs,kmk.id_kelas_mk,umk.nm_ujian_mk,case when umk.tgl_ujian is null then '<i>Tanggal belum diset</i>' else to_char(umk.tgl_ujian,'DD-MM-YYYY') end as tgl_ujian,
umk.jam_mulai||' - '||umk.jam_selesai as jam,rg.nm_ruangan,jjg.nm_jenjang||'-'||ps.nm_singkat_prodi as prodi,
wm_concat('<br>'||
case when tim.status=1 then '<font color=\"blue\">'||trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang)||' (KETUA)</font>'
when tim.status=2 then '<font color=\"green\">'||trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang)||' (SEKRETARIS)</font>'
else '<font color=\"black\">'||trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang)||' (PEMBIMBING)</font>' end) as tim,
to_char(umk.tgl_ujian,'YYYYMMDD') as urut
from ujian_mk umk
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
left join jadwal_ujian_mk jad on jad.id_ujian_mk=umk.id_ujian_mk
left join ruangan rg on rg.id_ruangan=jad.id_ruangan
left join tim_pengawas_ujian tim on jad.id_jadwal_ujian_mk=tim.id_jadwal_ujian_mk
left join ujian_mk_peserta pst on umk.id_ujian_mk=pst.id_ujian_mk
left join pengguna pgg on pgg.id_pengguna=tim.id_pengguna
where umk.id_kegiatan=71 and umk.id_fakultas=$kdfak and umk.id_semester=$id_smt and umk.tgl_ujian>=to_date(to_char(SYSDATE, 'YYYY-MM-DD'), 'YYYY-MM-DD')
group by pst.id_mhs, kmk.id_kelas_mk,umk.id_ujian_mk,umk.nm_ujian_mk,umk.tgl_ujian,
umk.jam_mulai,umk.jam_selesai,rg.nm_ruangan,jjg.nm_jenjang,ps.nm_singkat_prodi,jad.id_jadwal_ujian_mk) a
join
(select a.id_mhs, upper(a.judul_tugas_akhir) as judul, c.nim_mhs, d.nm_pengguna, e.id_kelas_mk, i.nm_prodi_minat
from tugas_akhir a 
left join tipe_ta b on a.id_tipe_ta = b.id_tipe_ta
left join mahasiswa c on a.id_mhs = c.id_mhs
left join prodi_minat i on c.id_prodi_minat = i.id_prodi_minat
left join pengguna d on c.id_pengguna = d.id_pengguna
left join program_studi g on c.id_program_studi = g.id_program_studi
left join kelas_mk e on g.id_program_studi = e.id_program_studi
left join kurikulum_mk f on e.id_kurikulum_mk = f.id_kurikulum_mk
where a.status = 1 and f.status_mkta = 1
and e.id_semester = $id_smt and g.id_fakultas = $kdfak) b
on a.id_mhs=b.id_mhs and a.id_kelas_mk=b.id_kelas_mk
order by urut,jam,nim_mhs";
$result = $db->Query($list)or die("salah kueri list ");
$i=0;
while($r = $db->FetchRow()) {
	$jenis = $r[2];
	$tgl = $r[3];
	$jam = $r[4];
	$ruang = $r[5];
	$prodi = $r[6];
	$tim = $r[7];
	$judul = $r[10];
	$nim = $r[11];
	$nama = $r[12];
$i++;   
if (($i % 2)==0) $bgcolor='lightgrey';
else $bgcolor='white';
$html .= '
    <tr bgcolor="'.$bgcolor.'" style="color:black;">
      <td border="1" width="5%" align="center" valign="middle">'.$nomor.'</td>
	  <td border="1" width="13%" align="center" valign="middle">'.$nim.'<br>'.strtoupper($nama).'</td>
      <td border="1" width="20%" align="left" valign="middle">'.strtoupper($judul).'</td>
      <td border="1" width="15%" align="center" valign="middle">'.strtoupper($prodi).'<br><br>'.strtoupper($minat).'</td>
      <td border="1" width="7%" align="center" valign="middle">'.strtoupper($jenis).'</td>
      <td border="1" width="10%" align="center" valign="middle">'.strtoupper($ruang).'<br><br>'.$tgl.'<br/><br>'.$jam.'</td>
      <td border="1" width="30%" align="left" valign="middle">'.$tim.'</td>
    </tr>';
$nomor++;
}
$html .= '
</table>';


// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('DAFTAR UJIAN.pdf', 'I');

?>