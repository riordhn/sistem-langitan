<?php
require('../../../config.php');
$db = new MyOracle();

$smt = $_GET['smt'];
$prodi = $_GET['prodi'];
$nm_prodi = $_GET['nm_prodi'];

header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=rekap-krs-".$nm_prodi.".xls");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data";

echo '
<table border="1" cellspacing="0" cellpadding="0">
           <tr>
             <td bgcolor="lightgrey" align="center"><b>No</b></td>
             <td bgcolor="lightgrey" align="center"><b>Nim</b></td>
             <td bgcolor="lightgrey" align="center"><b>Nama Mahasiswa</b></td>
             <td bgcolor="lightgrey" align="center"><b>Kode MA</b></td>
			 <td bgcolor="lightgrey" align="center"><b>Nama MA</b></td>
             <td bgcolor="lightgrey" align="center"><b>Kls</b></td>
             <td bgcolor="lightgrey" align="center"><b>SKS</b></td>
			 <td bgcolor="lightgrey" align="center"><b>Smt</b></td>
           </tr>';

$kueri="select mahasiswa.nim_mhs, pengguna.nm_pengguna, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, nama_kelas.nama_kelas, 
kurikulum_mk.kredit_semester, thn_akademik_semester, nm_semester from pengambilan_mk
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join semester on semester.id_semester = pengambilan_mk.id_semester
where pengambilan_mk.id_semester=$smt and pengambilan_mk.status_apv_pengambilan_mk=1 and mahasiswa.id_program_studi=$prodi
order by mahasiswa.nim_mhs, mata_kuliah.kd_mata_kuliah";
$result = $db->Query($kueri)or die("salah kueri 01 ");
$i=1;
while($r = $db->FetchRow()) {
echo '
           <tr>
             <td>'.$i.'</td>
             <td>&nbsp;'.$r[0].'</td>
             <td>'.$r[1].'</td>
             <td>'.$r[2].'</td>
             <td>'.$r[3].'</td>
             <td align="center">'.$r[4].'</td>
             <td align="center">'.$r[5].'</td>
			 <td align="center">'.$r[7].' '.$r[6].'</td>
           </tr>';
$i++;
}
echo '
</table>';

?>