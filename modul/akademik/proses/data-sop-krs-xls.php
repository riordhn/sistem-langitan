<?php
require('../../../config.php');
$db = new MyOracle();

$smt = $_GET['sem'];
$prodi = $_GET['prodi'];
$nm_prodi = $_GET['nm_prodi'];

header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=sop-krs-".$nm_prodi.".xls");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data";

echo '
<table border="1" cellspacing="0" cellpadding="0">
              	<tr>
        	<th>NO</th>
            <th>NIM</th>
            <th>NAMA</th>
            <th>JENJANG</th>
            <th>PRODI</th>
            <th>STATUS AKADEMIK</th>
            <th>STATUS BAYAR</th>
            <th>STATUS KRS</th>
            <th>BIAYA KULIAH</th>
        </tr>';

$kueri="SELECT NIM_MHS, NM_PENGGUNA, NM_JENJANG, NM_PROGRAM_STUDI, NM_STATUS_PENGGUNA, NAMA_STATUS,
								(CASE WHEN B.SUDAH_KRS IS NULL THEN 0 ELSE B.SUDAH_KRS END) AS SUDAH_KRS, BESAR_BIAYA
								FROM MAHASISWA
								LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
								LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
								LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
								LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
								LEFT JOIN (
									SELECT MAHASISWA.ID_MHS, STATUS_PEMBAYARAN.NAMA_STATUS, SUM(BESAR_BIAYA) AS BESAR_BIAYA
									FROM PEMBAYARAN
									LEFT JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBAYARAN.ID_MHS
									LEFT JOIN STATUS_PEMBAYARAN ON STATUS_PEMBAYARAN.ID_STATUS_PEMBAYARAN = PEMBAYARAN.ID_STATUS_PEMBAYARAN
									WHERE ID_SEMESTER = '$smt' AND MAHASISWA.ID_PROGRAM_STUDI = '$prodi'
									GROUP BY MAHASISWA.ID_MHS, STATUS_PEMBAYARAN.NAMA_STATUS
								) A ON A.ID_MHS = MAHASISWA.ID_MHS
								LEFT JOIN (
									SELECT COUNT(DISTINCT PENGAMBILAN_MK.ID_MHS) AS SUDAH_KRS, PENGAMBILAN_MK.ID_MHS 
									FROM PENGAMBILAN_MK
									LEFT JOIN SEMESTER ON SEMESTER.ID_SEMESTER = PENGAMBILAN_MK.ID_SEMESTER
									LEFT JOIN MAHASISWA ON PENGAMBILAN_MK.ID_MHS = MAHASISWA.ID_MHS
									LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
									WHERE SEMESTER.ID_SEMESTER = '$smt' AND PROGRAM_STUDI.ID_PROGRAM_STUDI = '$prodi'
									GROUP BY PENGAMBILAN_MK.ID_MHS) B ON B.ID_MHS = MAHASISWA.ID_MHS
								WHERE (STATUS_AKADEMIK_MHS <> 4 AND STATUS_AKADEMIK_MHS <> 5 AND
								STATUS_AKADEMIK_MHS <> 6 AND STATUS_AKADEMIK_MHS <> 7 AND STATUS_AKADEMIK_MHS <> 20) 
								AND MAHASISWA.ID_PROGRAM_STUDI = '$prodi'
								ORDER BY NIM_MHS";
$result = $db->Query($kueri)or die("salah kueri 01 ");
$i=1;
while($r = $db->FetchRow()) {
echo '
           <tr>
             <td>'.$i.'</td>
             <td>&nbsp;'.$r[0].'</td>
             <td>'.$r[1].'</td>
             <td>'.$r[2].'</td>
             <td>'.$r[3].'</td>
			 <td>'.$r[4].'</td>
			 <td>'.$r[5].'</td>
             <td>';
			 if($r[6] == 1){
				echo 'Sudah KRS';
			 }else{
			 	echo 'Belum KRS';
			 }
			echo '</td>
             <td align="right">'.$r[7].'</td>
           </tr>';
$i++;
}
echo '
</table>';

?>