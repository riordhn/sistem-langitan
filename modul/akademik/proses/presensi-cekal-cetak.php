<?php
require('../../../config.php');
$db = new MyOracle();

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('Presensi UAS');
$pdf->SetSubject('Presensi UAS');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logounair.gif";
$logo_size = "25";
$title = "UNIVERSITAS AIRLANGGA";

$kdsmt = $_GET['smt'];
$kdfak = $_GET['fak'];

$kueri = "select f.nm_fakultas, f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas from fakultas f where f.id_fakultas=$kdfak";
$result = $db->Query($kueri)or die("salah kueri 22 ");
while($r = $db->FetchRow()) {
	$fak = $r[0];
	$alm_fak = $r[1];
	$pos_fak = $r[2];
	$tel_fak = $r[3];
	$fax_fak = $r[4];
	$web_fak = $r[5];
	$eml_fak = $r[6];
}

$kueri1 = "select tahun_ajaran, nm_semester from semester where id_semester=$kdsmt";
$result = $db->Query($kueri1)or die("salah kueri 26 ");
while($r = $db->FetchRow()) {
	$thn = $r[0];
	$smt = $r[1];
}

$content = "FAKULTAS ".strtoupper($fak)."\n".strtoupper($jenjang)." - ".strtoupper($prodi)."\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 40, 5);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 14);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage('P', 'A4');

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="6" align="center"><b><u>DAFTAR CEKAL UJIAN AKHIR SEMESTER '.strtoupper($smt).' '.$thn.'</u></b></td>
  </tr>
  <tr>
    <td colspan="6">&nbsp;</td>
  </tr>
</table>
<table width="100%" cellspacing="0" cellpadding="5">
<thead>
  <tr bgcolor="black" style="color:#fff;">
      <td border="1" width="5%" align="center"><strong>NO.</strong></td>
	  <td border="1" width="12%" align="center"><strong>NIM</strong></td>
      <td border="1" width="30%" align="center"><strong>NAMA MAHASISWA</strong></td>
	  <td border="1" width="10%" align="center"><strong>KODE MA</strong></td>
      <td border="1" width="30%" align="center"><strong>NAMA MATA AJAR</strong></td>
	  <td border="1" width="13%" align="center"><strong>KEHADIRAN</strong></td>
    </tr>
</thead>';

$nomor=1;
$kueri = "select mhs.nim_mhs, upper(pg.nm_pengguna) as nm_pengguna, mk.kd_mata_kuliah, upper(mk.nm_mata_kuliah) as nm_mata_kuliah, pma.persen_presensi from 
mahasiswa mhs 
left join pengguna pg on mhs.id_pengguna=pg.id_pengguna
left join pengambilan_mk pma on mhs.id_mhs=pma.id_mhs 
left join kelas_mk kma on pma.id_kelas_mk=kma.id_kelas_mk 
left join kurikulum_mk kmk on kma.id_kurikulum_mk=kmk.id_kurikulum_mk 
left join mata_kuliah mk on kmk.id_mata_kuliah=mk.id_mata_kuliah 
left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
where pma.id_semester='".$kdsmt."' and ps.id_fakultas='".$kdfak."' and pma.status_cekal=0 
and pma.status_apv_pengambilan_mk=1 
order by mhs.nim_mhs, mk.nm_mata_kuliah";
$result = $db->Query($kueri)or die("salah kueri 27 ");
$i=0;
while($r = $db->FetchRow()) {
	$nim_mhs = $r[0];
	$nama_mhs = $r[1];
	$kd_mk = $r[2];
	$nama_mk = $r[3];
	$persen = $r[4];
$i++;   
if (($i % 2)==0) $bg='lightgrey';
else $bg='white';
$html .= '
    <tr bgcolor="'.$bg.'" style="color:#000;">
      <td border="1" width="5%" align="center">'.$nomor.'</td>
	  <td border="1" width="12%" align="center">'.$nim_mhs.'</td>
      <td border="1" width="30%" align="left">'.$nama_mhs.'</td>
	  <td border="1" width="10%" align="center">'.$kd_mk.'</td>
      <td border="1" width="30%" align="left">'.$nama_mk.'</td>
	  <td border="1" width="13%" align="center">'.$persen.'%</td>
    </tr>';
$nomor++;
}
$html .= '</table>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('DAFTAR CEKAL UAS SEMESTER '.strtoupper($smt).' '.$thn.'.pdf', 'I');

?>