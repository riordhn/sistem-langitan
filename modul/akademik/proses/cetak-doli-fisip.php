<?php
require('../../../config.php');
$db = new MyOracle();

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('Dosen Wali');
$pdf->SetSubject('Dosen Wali');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logounair.gif";
$logo_size = "25";
$title = "UNIVERSITAS AIRLANGGA";

$bulan['01'] = "Januari"; $bulan['02'] = "Pebruari"; $bulan['03'] = "Maret"; $bulan['04'] = "April"; $bulan['05'] = "Mei"; $bulan['06'] = "Juni"; $bulan['07'] = "Juli"; $bulan['08'] = "Agustus"; $bulan['09'] = "September"; $bulan['10'] = "Oktober";  $bulan['11'] = "Nopember";  $bulan['12'] = "Desember";


$kueri = "select a.nm_jenjang, b.nm_program_studi, f.nm_fakultas, f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas,f.id_fakultas
			from jenjang a, program_studi b, fakultas f where a.id_jenjang=b.id_jenjang and f.id_fakultas=b.id_fakultas and b.id_fakultas = '$_REQUEST[fak]'";
$result = $db->Query($kueri)or die("salah kueri 22 ");
while($r = $db->FetchRow()) {
	$jenjang = $r[0];
	$prodi = $r[1];
	$fak = $r[2];
	$alm_fak = $r[3];
	$pos_fak = $r[4];
	$tel_fak = $r[5];
	$fax_fak = $r[6];
	$web_fak = $r[7];
	$eml_fak = $r[8];
	$kd_fak = $r[9];
}
$kueri = "select nm_semester,tahun_ajaran from semester where id_semester='$_REQUEST[smt]'";
//echo $kueri;
$result = $db->Query($kueri)or die("salah kueri 20 ");
while($r = $db->FetchRow()) {
	$thn_ajaran = $r[1];
	$nm_semester = $r[0];
}

$dat="select nip_dosen, trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as dosen
					from dosen left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
					where dosen.id_dosen='$_REQUEST[cetak]'";
$result = $db->Query($dat)or die("salah kueri 20 ");
while($r = $db->FetchRow()) {
	$nm_dosen = $r[1];
	$nip_dosen = $r[0];
}




$content = "FAKULTAS ".strtoupper($fak)."\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 35, 10);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 10);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 9);

// add a page
$pdf->AddPage('P', 'Legal');

//tgl cetak
$list2="SELECT SYSDATE FROM DUAL";
$result22 = $db->Query($list2)or die("salah kueri list2 ");
while($r22 = $db->FetchRow()) {
	$tglcetak=$r22[0];
}

//get wadek I
$list3="select nip_dosen,case when gelar_belakang is not null then 
trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) else 
trim(gelar_depan||' '||upper(nm_pengguna)) end from dosen
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
left join program_studi on dosen.id_program_studi=program_studi.id_program_studi
where id_jabatan_pegawai=6 and id_fakultas='$_REQUEST[fak]'";
$result23 = $db->Query($list3)or die("salah kueri list3 ");
while($r23 = $db->FetchRow()) {
	$nipwadek=$r23[0];
	$nmwadek=$r23[1];
}
//hitung wali
$list4="select count(*) as ttl
					from mahasiswa
					left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
					left join dosen_wali on dosen_wali.id_mhs=mahasiswa.id_mhs
					left join program_studi on program_studi.id_program_studi=mahasiswa.id_program_studi
					left join jenjang on jenjang.id_jenjang=program_studi.id_jenjang
					where dosen_wali.id_dosen='$_REQUEST[cetak]' and mahasiswa.status_akademik_mhs in 
					(select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
					and dosen_wali.id_mhs in (select id_mhs from pengambilan_mk where id_semester='$_REQUEST[smt]')
					and dosen_wali.status_dosen_wali='1'";
$result24 = $db->Query($list4)or die("salah kueri list4 ");
while($r24 = $db->FetchRow()) {
	$ttl=$r24[0];
}

$det="select s1.nim_mhs,mhs,prodi,coalesce(s4.sks_skrg,0)as sks_skrg,coalesce(SKS_TOTAL_MHS,0) as skstotal,
					coalesce(IPK_MHS,0)as ipk,coalesce(sks_lalu,0) as sks_lalu,coalesce(ips,0) as ips
					from
					(select mahasiswa.id_mhs,mahasiswa.nim_mhs,upper(pengguna.nm_pengguna) as mhs,
					upper(nm_jenjang)||' - '||upper(nm_singkat_prodi) as prodi
					from mahasiswa
					left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
					left join dosen_wali on dosen_wali.id_mhs=mahasiswa.id_mhs
					left join program_studi on program_studi.id_program_studi=mahasiswa.id_program_studi
					left join jenjang on jenjang.id_jenjang=program_studi.id_jenjang
					where dosen_wali.id_dosen='$_REQUEST[cetak]' and mahasiswa.status_akademik_mhs in 
					(select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
					and dosen_wali.id_mhs in (select id_mhs from pengambilan_mk where id_semester='$_REQUEST[smt]')
					and dosen_wali.status_dosen_wali='1')s1
					left join (
					select id_mhs, sum(kredit_semester) as SKS_TOTAL_MHS, 
					round((sum((kredit_semester * nilai_standar_nilai)) / sum(kredit_semester)), 2) as IPK_MHS
					from 
					(select a.id_mhs, c.kd_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai,
					case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,
					row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by nilai_huruf) rangking
					from pengambilan_mk a
					join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
					join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
					join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
					join semester smt on a.id_semester=smt.id_semester
					join mahasiswa m on a.id_mhs=m.id_mhs
					join program_studi ps on m.id_program_studi=ps.id_program_studi
					where a.status_apv_pengambilan_mk = 1 and (a.nilai_huruf<>'E' or a.nilai_huruf<>null) 
					and ps.id_fakultas='$_REQUEST[fak]' and a.status_hapus=0 
					and a.status_pengambilan_mk !=0)
					where tahun<='$_REQUEST[smtpiltahun]' and rangking=1
					group by id_mhs)s2 on s1.id_mhs=s2.id_mhs
					left join 
					(select id_mhs,case when sum(bobot*kredit_semester)=0 then 0 else 
					round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips,
					case when id_fakultas='$_REQUEST[fak]' then sum(sksreal) else sum(kredit_semester) end as sks_lalu 
					from 
					(select id_mhs,nim_mhs,id_kurikulum_mk,id_fakultas,kredit_semester, sksreal,min(nilai_huruf) as nilai, max(bobot) as bobot from
					(select a.id_mhs,m.nim_mhs,ps.id_fakultas,a.id_kurikulum_mk, 
					case when (a.nilai_huruf = 'E' or a.nilai_huruf is null) 
					and d.status_mkta in (1,2) then 0
					else d.kredit_semester end as kredit_semester,d.kredit_semester as sksreal,
					case when a.nilai_huruf is null then 'E' else a.nilai_huruf end as nilai_huruf,
					case when e.nilai_standar_nilai is null then 0 else e.nilai_standar_nilai end as bobot
					from pengambilan_mk a
					left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
					left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
					left join mahasiswa m on a.id_mhs=m.id_mhs
					left join program_studi ps on m.id_program_studi=ps.id_program_studi
					left join semester s on a.id_semester=s.id_semester
					where group_semester||thn_akademik_semester in
					(select group_semester||thn_akademik_semester from semester where id_Semester='$_REQUEST[last_smt]')
					and tipe_semester in ('UP','REG','RD') 
					and a.status_apv_pengambilan_mk='1' and ps.id_fakultas='$_REQUEST[fak]' and a.status_hapus=0 
					and a.status_pengambilan_mk !=0)
					group by id_mhs, nim_mhs, id_kurikulum_mk, id_fakultas, kredit_semester, sksreal
					)
					group by id_mhs,id_fakultas)s3 on s1.id_mhs=s3.id_mhs
					left join 
					(select a.id_mhs,sum(d.kredit_semester) as sks_skrg
					from pengambilan_mk a
					left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
					left join mahasiswa m on a.id_mhs=m.id_mhs
					left join program_studi ps on m.id_program_studi=ps.id_program_studi
					where id_semester='$_REQUEST[smt]' and id_fakultas='$_REQUEST[fak]' and status_apv_pengambilan_mk=1
					group by a.id_mhs)s4 on s1.id_mhs=s4.id_mhs
					order by prodi,nim_mhs";
$result = $db->Query($det)or die("salah kueri 20 ");


if ($ttl <= 45 or $ttl >56){

$html = '
<table width="80%" border="0" cellspacing="0" cellpadding="0"> 
  <tr>
	<td width="15%">Nama Dosen </td>
	<td width="60%">: '.$nm_dosen.'</td>
  </tr>
  <tr>
	<td  >N I P </td>
	<td>: '.$nip_dosen.'</td>
  </tr>
  <tr>
	<td >Semester </td>
	<td>: '.strtoupper($nm_semester).' '.$thn_ajaran.'</td>
  </tr>
  <tr>
	<td></td>
	<td></td>
  </tr>
</table>
<table width="100%" border="1" cellspacing="0" cellpadding="2">
  <tr>
    <th width="4%" align="center"><b>No</b></th>
	<th width="12%" align="center"><b>NIM</b></th>
	<th width="30%" align="center"><b>Nama</b></th>
	<th width="10%" align="center"><b>Prodi</b></th>
	<th width="10%" align="center"><b>SKS Ambil</b></th>
	<th width="6%" align="center"><b>IPS</b></th>
	<th width="10%" align="center"><b>SKS TTL</b></th>
	<th width="6%" align="center"><b>IPK</b></th>
  </tr>';
 } else {
 $html = '
 <table width="80%" border="0" cellspacing="0" cellpadding="0"> 
  <tr>
	<td width="15%">Nama Dosen </td>
	<td width="60%">: '.$nm_dosen.'</td>
  </tr>
  <tr>
	<td  >N I P </td>
	<td>: '.$nip_dosen.'</td>
  </tr>
  <tr>
	<td >Semester </td>
	<td>: '.strtoupper($nm_semester).' '.$thn_ajaran.'</td>
  </tr>
  <tr>
	<td></td>
	<td></td>
  </tr>
</table>
<table width="100%" border="1" cellspacing="0" cellpadding="1">
  <tr>
    <th width="4%" align="center"><b>No</b></th>
	<th width="12%" align="center"><b>NIM</b></th>
	<th width="30%" align="center"><b>Nama</b></th>
	<th width="10%" align="center"><b>Prodi</b></th>
	<th width="10%" align="center"><b>SKS Ambil</b></th>
	<th width="6%" align="center"><b>IPS</b></th>
	<th width="10%" align="center"><b>SKS TTL</b></th>
	<th width="6%" align="center"><b>IPK</b></th>
  </tr>';
 
 }
 
  $no = 1;
while($r = $db->FetchRow()) {
	$html .= '<tr>
				<td>'.$no++.'</td>
				<td>'.$r[0].'</td>
				<td>'.$r[1].'</td>
				<td>'.$r[2].'</td>
				<td>'.$r[3].'</td>
				<td>'.$r[7].'</td>
				<td>'.$r[4].'</td>
				<td>'.$r[5].'</td>
			</tr>';
}

$html .= '
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr>
    <td colspan="8"><br>&nbsp;</td>
</tr>

<tr>
	<td width="55%"><font size="9">&nbsp;</font></td>
    <td width="45%"><font size="9">Surabaya,&nbsp;'.date("d", strtotime($tglcetak)).' '.$bulan[date("m", strtotime($tglcetak))].' '.date("Y", strtotime($tglcetak)).'<br>a.n Dekan<br>Wakil Dekan I<br><br><br><br>'.$nmwadek.'<br>NIP. '.$nipwadek.'</font></td>
  </tr>
</table>
';


// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('dosen-wali.pdf', 'I');

?>