<?php
require('../../../config.php');
$db = new MyOracle();
$db2 = new MyOracle();

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('KRS');
$pdf->SetSubject('KRS Mahasiswa');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logounair.gif";
$logo_size = "20";
$title = "UNIVERSITAS AIRLANGGA";

$kdprodi = $_GET['prodi'];
$angk =  $_GET['cetak'];

$kueri = "select a.nm_jenjang, b.nm_program_studi, c.nm_fakultas from jenjang a, program_studi b, fakultas c 
where a.id_jenjang=b.id_jenjang and c.id_fakultas=b.id_fakultas and b.id_program_studi=$kdprodi";
$result = $db->Query($kueri)or die("salah kueri 22 ");
while($r = $db->FetchRow()) {
	$jenjang = $r[0];
	$prodi = $r[1];
	$fak = $r[2];
}

$content = "FAKULTAS ".strtoupper($fak)."\n".strtoupper($jenjang)." - ".strtoupper($prodi)."\n";

$kueri = "select * from semester where STATUS_AKTIF_SEMESTER='True' order by id_semester desc";
$result = $db->Query($kueri)or die ("salah kueri 48 ");
while($r = $db->FetchRow()) {
	$sem_aktif = $r[0];
	$nm_sem = $r[1];
	$thn_sem = $r[2];
}

$kueri = "select distinct pengambilan_mk.id_mhs, mahasiswa.nim_mhs, pengguna.nm_pengguna, mhs_status.ips_mhs_status, mhs_status.ipk_mhs_status, mhs_status.sks_max
from mahasiswa, pengguna, mhs_status, pengambilan_mk
where mahasiswa.id_pengguna=pengguna.id_pengguna and mhs_status.id_mhs=mahasiswa.id_mhs
and pengambilan_mk.id_mhs=mahasiswa.id_mhs and mhs_status.id_semester=$sem_aktif
and mahasiswa.id_program_studi=$kdprodi and mahasiswa.thn_angkatan_mhs=$angk
order by mahasiswa.nim_mhs asc";
$result = $db->Query($kueri)or die("salah kueri 24 ");
while($r = $db->FetchRow()) {
	$id_mhs = $r[0];
	$mhs_nim = $r[1];
	$mhs_nama = $r[2];
	$mhs_ips = $r[3];
	$mhs_ipk = $r[4];
	$mhs_sks_max = $r[5];

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, PDF_MARGIN_TOP, 5);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 14);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
//$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage('P', 'A4');

$html = '
<p>&nbsp;</p>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:8x;">
  <tr>
    <td colspan="7" align="center"><p><strong>KRS AKADEMIK SEMESTER '.strtoupper($nm_sem).' '.$thn_sem.'</strong></p></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
           <td width="12%">Nama</td>
           <td width="1%"><strong>: </strong></td>
           <td width="33%"><strong>'.strtoupper($mhs_nama).'</strong></td>
           <td width="18%">&nbsp;</td>
           <td width="15%">IPK</td>
           <td width="1%"><strong>:</strong></td>
           <td width="20%"><strong>'.$mhs_ipk.'</strong></td>
  </tr>
         <tr>
           <td>NIM</td>
           <td><strong>: </strong></td>
           <td><strong>'.$mhs_nim.'</strong></td>
           <td>&nbsp;</td>
           <td>IPS</td>
           <td><strong>: </strong></td>
           <td><strong>'.$mhs_ips.'</strong></td>
         </tr>
         <tr>
           <td>Program Studi</td>
           <td><strong>: </strong></td>
           <td><strong>'.$jenjang.' '.$prodi.'</strong></td>
           <td>&nbsp;</td>
           <td>SKS MAKS.</td>
           <td><strong>: </strong></td>
           <td><strong>'.$mhs_sks_max.'</strong></td>
         </tr>
</table>
<p>&nbsp;</p>
<table border="0" width="100%">
<tr>
<td width="100%">
<table cellspacing="0" cellpadding="3" border="1" width="100%" style="font-size:25px;">
  <thead>
    <tr bgcolor="#000000" style="color:#FFF;">
      <td width="20%" align="center" bgcolor="#333333"><font color="#FFFFFF">Kode MA</font></td>
      <td width="42%" align="center" bgcolor="#333333"><font color="#FFFFFF">Nama Mata Ajar</font></td>
      <td width="19%" align="center" bgcolor="#333333"><font color="#FFFFFF">SKS</font></td>
      <td width="19%" align="center" bgcolor="#333333"><font color="#FFFFFF">Kelas</font></td>
    </tr>
  </thead>
';
}

$kueri = "
select a.kd_mata_kuliah,a.nm_mata_kuliah,e.kredit_semester,d.id_pengambilan_mk,c.no_kelas_mk,d.STATUS_APV_PENGAMBILAN_MK
from mata_kuliah a, kelas_mk c, pengambilan_mk d, kurikulum_mk e
where c.id_kurikulum_mk=e.id_kurikulum_mk and a.id_mata_kuliah=e.id_mata_kuliah and c.id_semester=d.id_semester and c.id_kelas_mk=d.id_kelas_mk
and c.id_semester='".$sem_aktif."' and d.id_mhs='".$id_mhs."' and d.STATUS_APV_PENGAMBILAN_MK='1'
order by a.kd_mata_kuliah
";
$hit=0; $tot_sks=0;
$result = $db->Query($kueri)or die("salah kueri 31 ");
while($r = $db->FetchRow()) {
	$hit++;
	$tot_sks += $r[2];
	$nm_kelas = "";
	$result2 = $db2->Query("select nama_kelas from nama_kelas where id_nama_kelas='".$r[4]."'")or die("salah kueri 42 ");
	while($r2 = $db2->FetchRow()) {
		$nm_kelas = $r2[0];
	}
	$html .= '
		<tr>
			<td width="20%">'.$r[0].'</td>
			<td width="42%">'.$r[1].'</td>
			<td width="19%" align="center" >'.$r[2].'</td>
			<td width="19%" align="center" >'.$nm_kelas.'</td>
		</tr>
	';
}
$html .= '
	<tr>
		<td colspan="2" align="center">Total SKS</td>
		<td width="19%" align="center">'.$tot_sks.'</td>
		<td width="19%" align="center"></td>
	</tr>
</table>
</td>
</tr>
</table>
<p>&nbsp;</p>
';
// ambil dosen wali
$sem_aktif="";
$kueri = "select b.nip_dosen,c.nm_pengguna from dosen_wali a, dosen b, pengguna c
where a.id_dosen=b.id_dosen and b.id_pengguna=c.id_pengguna and a.id_mhs='".$id_mhs."'
";
$result = $db->Query($kueri)or die ("salah kueri 48 ");
while($r = $db->FetchRow()) {
	$wali_nip = $r[0];
	$wali_nama = $r[1];
}
$bulan['01'] = "Januari"; $bulan['02'] = "Februari"; $bulan['03'] = "Maret"; $bulan['04'] = "April"; $bulan['05'] = "Mei"; $bulan['06'] = "juni"; $bulan['07'] = "Juli"; $bulan['08'] = "Agustus"; $bulan['09'] = "September"; $bulan['10'] = "Oktober";  $bulan['11'] = "November";  $bulan['12'] = "Desember";
$html .= '
<table border="0" width="100%" style="font-size:30px;">
  <tr valign="top">
    <td width="50%">&nbsp;</td>
    <td width="50%" rowspan="3"><p align="center">Surabaya, '.date("d").' '.$bulan[date("m")].' '.date("Y").'</p>
      <p align="center">Dosen Wali,</p>
      <p align="center">&nbsp;</p>
      <p align="center">&nbsp;</p>
      <p align="center">'.$wali_nama.'</p>
      <p align="center">NIP. '.$wali_nip.'</p>      <div align="center"></div>      <div align="center"></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// reset pointer to the last page
//$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('krs.pdf', 'I');

//============================================================+
// END OF FILE                                                
//============================================================+
?>