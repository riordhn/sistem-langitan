<?php
require('../../../config.php');
$db = new MyOracle();

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('Presensi UAS');
$pdf->SetSubject('Presensi UAS');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logounair.gif";
$logo_size = "25";
$title = "UNIVERSITAS AIRLANGGA";

$bulan['01'] = "Januari"; $bulan['02'] = "Pebruari"; $bulan['03'] = "Maret"; $bulan['04'] = "April"; $bulan['05'] = "Mei"; $bulan['06'] = "Juni"; $bulan['07'] = "Juli"; $bulan['08'] = "Agustus"; $bulan['09'] = "September"; $bulan['10'] = "Oktober";  $bulan['11'] = "Nopember";  $bulan['12'] = "Desember";

$id_kelas_mk = $_GET['cetak'];
$id_semester = $_GET['idsem'];
$kueri = "select a.nm_jenjang, b.nm_program_studi, f.nm_fakultas, f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas,f.id_fakultas
			from jenjang a, program_studi b, fakultas f where a.id_jenjang=b.id_jenjang and f.id_fakultas=b.id_fakultas and b.id_program_studi in (select id_program_studi from kelas_mk where id_kelas_mk=$id_kelas_mk)";
$result = $db->Query($kueri)or die("salah kueri 22 ");
while($r = $db->FetchRow()) {
	$jenjang = $r[0];
	$prodi = $r[1];
	$fak = $r[2];
	$alm_fak = $r[3];
	$pos_fak = $r[4];
	$tel_fak = $r[5];
	$fax_fak = $r[6];
	$web_fak = $r[7];
	$eml_fak = $r[8];
	$kd_fak = $r[9];
}
$kueri = "select nm_semester,tahun_ajaran from semester where id_semester=$id_semester";
//echo $kueri;
$result = $db->Query($kueri)or die("salah kueri 20 ");
while($r = $db->FetchRow()) {
	$thn_ajaran = $r[1];
	$nm_semester = $r[0];
}

//Edited by Yudi Sulistya, 13/07/2012
$kueri = "SELECT TGL_MULAI_JSF,TGL_SELESAI_JSF FROM JADWAL_SEMESTER_FAKULTAS WHERE ID_SEMESTER=$id_semester and ID_FAKULTAS=$kd_fak and ID_KEGIATAN=43";
//echo $kueri;
$result = $db->Query($kueri)or die("salah kueri kuliah ");
while($r = $db->FetchRow()) {
	$kul_mulai = $r[0];
	$kul_akhir = $r[1];
}

$kueri = "SELECT TGL_MULAI_JSF,TGL_SELESAI_JSF FROM JADWAL_SEMESTER_FAKULTAS WHERE ID_SEMESTER=$id_semester and ID_FAKULTAS=$kd_fak and ID_KEGIATAN=44";
//echo $kueri;
$result = $db->Query($kueri)or die("salah kueri uts ");
while($r = $db->FetchRow()) {
	$uts_mulai = $r[0];
	$uts_akhir = $r[1];
}

$kueri = "SELECT TGL_MULAI_JSF,TGL_SELESAI_JSF FROM JADWAL_SEMESTER_FAKULTAS WHERE ID_SEMESTER=$id_semester and ID_FAKULTAS=$kd_fak and ID_KEGIATAN=47";
//echo $kueri;
$result = $db->Query($kueri)or die("salah kueri uts ");
while($r = $db->FetchRow()) {
	$uas_mulai = $r[0];
	$uas_akhir = $r[1];
}



$kueri = "select mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, 
nama_kelas.nama_kelas,case when pengguna.gelar_belakang is null then trim(pengguna.gelar_depan||' '||upper(pengguna.nm_pengguna)) 
else trim(pengguna.gelar_depan||' '||upper(pengguna.nm_pengguna)||', '||pengguna.gelar_belakang) end from kelas_mk
left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk=kelas_mk.id_kurikulum_mk
left join mata_kuliah on mata_kuliah.id_mata_kuliah=kurikulum_mk.id_mata_kuliah
left join nama_kelas on nama_kelas.id_nama_kelas=kelas_mk.no_kelas_mk
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk and pjmk_pengampu_mk='1'
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
where kelas_mk.id_kelas_mk=$id_kelas_mk";
$result = $db->Query($kueri)or die("salah kueri 25 ");
while($r = $db->FetchRow()) {
	$kode_ma = $r[0];
	$nama_ma = $r[1];
	$sks_ma = $r[2];
	$kelas_ma = $r[3];
	$dosen_ma = $r[4];
}


$kueri1 = "select case when pengguna.gelar_belakang is null then trim(pengguna.gelar_depan||' '||upper(pengguna.nm_pengguna)) 
else trim(pengguna.gelar_depan||' '||upper(pengguna.nm_pengguna)||', '||pengguna.gelar_belakang) end as pjmk, dosen.nip_dosen as nip
from kelas_mk 
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk 
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
where kelas_mk.id_kelas_mk=$id_kelas_mk and pengampu_mk.pjmk_pengampu_mk=1";
$result = $db->Query($kueri1)or die("salah kueri 26 ");
while($r = $db->FetchRow()) {
	$pjmk = $r[0];
	$nip = $r[1];
}

$content = "FAKULTAS ".strtoupper($fak)."\n".strtoupper($jenjang)." - ".strtoupper($prodi)."\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 40, 5);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 14);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage('P', 'A4');

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="5" align="center"><b><u>DAFTAR CEKAL SEMESTER '.strtoupper($nm_semester).' '.$thn_ajaran.'</u></b></td>
  </tr>
  <tr>
    <td colspan="5">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="15%">KODE MA</td>
	<td width="2%">:</td>
	<td width="63%">'.$kode_ma.'</td>
  </tr>
   <tr>
    <td width="15%">NAMA MA</td>
	<td width="2%">:</td>
	<td width="63%">'.strtoupper($nama_ma).'</td>
  </tr>
   <tr>
    <td width="15%">SKS</td>
	<td width="2%">:</td>
	<td width="63%">'.$sks_ma.'</td>
  </tr>
   <tr>
    <td width="15%">KELAS</td>
	<td width="2%">:</td>
	<td width="63%">'.$kelas_ma.'</td>
  </tr>
   <tr>
    <td width="15%">DOSEN</td>
	<td width="2%">:</td>
	<td width="63%">'.$dosen_ma.'</td>
  </tr>
  <tr>
    <td colspan="5">&nbsp;</td>
  </tr>
</table>
<table width="100%" cellspacing="0" cellpadding="5">
  
<thead>
  <tr bgcolor="lightgrey" style="color:#000;">
      <td border="1" width="5%" align="center"><strong><br/>NO.<br/>&nbsp;</strong></td>
	  <td border="1" width="14%" align="center"><strong><br/>NIM<br/>&nbsp;</strong></td>
      <td border="1" width="48%" align="center"><strong><br/>NAMA MAHASISWA<br/>&nbsp;</strong></td>
	  <td border="1" width="10%" align="center"><strong><br/>TM<br/>&nbsp;</strong></td>
      <td border="1" width="10%" align="center"><strong><br/>HADIR<br/>&nbsp;</strong></td>
	  <td border="1" width="10%" align="center"><strong><br/>PROSEN<br/>&nbsp;</strong></td>
    </tr>
</thead>';


$nomor=1;
if ($kd_fak==4) {
$kueri = "select s1.id_pengambilan_mk,s1.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,prodi,
nm_pengguna,nim_mhs,count(presensi_kelas.id_kelas_mk) as tm,s1.status,s1.id_mhs,
sum(case when presensi_mkmhs.kehadiran>0 then 1 else 0 end) as hadir,
round(sum(case when presensi_mkmhs.kehadiran>0 then 1 else 0 end)/count(presensi_kelas.id_kelas_mk)*100,2) as prosen
from (select id_pengambilan_mk,nm_pengguna,nim_mhs,pengambilan_mk.id_mhs,pengambilan_mk.id_kelas_mk,
kd_mata_kuliah,nm_mata_kuliah,nama_kelas,nm_jenjang||'-'||nm_program_studi as prodi,
case when pengambilan_mk.status_ulangke > 0 then 'ULANG' else 'BARU' end as status
from pengambilan_mk
left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk and kelas_mk.id_semester=$id_semester 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
where pengambilan_mk.status_cekal='0' and pengambilan_mk.id_semester=$id_semester  and kelas_mk.id_kelas_mk=$id_kelas_mk and status_apv_pengambilan_mk='1')s1
left join presensi_kelas on s1.id_kelas_mk=presensi_kelas.id_kelas_mk
left join presensi_mkmhs on presensi_kelas.id_presensi_kelas=presensi_mkmhs.id_presensi_kelas and s1.id_mhs=presensi_mkmhs.id_mhs
where tgl_presensi_kelas between to_date ('$uts_akhir', 'dd-mm-yy') AND to_date ('$uas_mulai', 'dd-mm-yy') 
group by s1.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,prodi,nm_pengguna,nim_mhs,s1.id_pengambilan_mk,s1.status,s1.id_mhs
order by nim_mhs";
} else {
$kueri = "select s1.id_pengambilan_mk,s1.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,prodi,
nm_pengguna,nim_mhs,count(presensi_kelas.id_kelas_mk) as tm,s1.status,s1.id_mhs,
sum(case when presensi_mkmhs.kehadiran>0 then 1 else 0 end) as hadir,
round(sum(case when presensi_mkmhs.kehadiran>0 then 1 else 0 end)/count(presensi_kelas.id_kelas_mk)*100,2) as prosen
from (select id_pengambilan_mk,nm_pengguna,nim_mhs,pengambilan_mk.id_mhs,pengambilan_mk.id_kelas_mk,
kd_mata_kuliah,nm_mata_kuliah,nama_kelas,nm_jenjang||'-'||nm_program_studi as prodi,
case when pengambilan_mk.status_ulangke > 0 then 'ULANG' else 'BARU' end as status
from pengambilan_mk
left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk and kelas_mk.id_semester=$id_semester 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
where pengambilan_mk.status_cekal='0' and pengambilan_mk.id_semester=$id_semester  and kelas_mk.id_kelas_mk=$id_kelas_mk and status_apv_pengambilan_mk='1')s1
left join presensi_kelas on s1.id_kelas_mk=presensi_kelas.id_kelas_mk
left join presensi_mkmhs on presensi_kelas.id_presensi_kelas=presensi_mkmhs.id_presensi_kelas and s1.id_mhs=presensi_mkmhs.id_mhs
where tgl_presensi_kelas between to_date ('$kul_mulai', 'dd-mm-yy') AND to_date ('$uas_mulai', 'dd-mm-yy') 
group by s1.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,prodi,nm_pengguna,nim_mhs,s1.id_pengambilan_mk,s1.status,s1.id_mhs
order by nim_mhs";
}
//echo "$kueri";
$result = $db->Query($kueri)or die("salah kueri 27 ");
$i=0;
while($r = $db->FetchRow()) {
	$nim_mhs = $r[7];
	$nama_mhs = $r[6];
	$tm = $r[8];
	$hadir = $r[11];
	$prosen = $r[12];
$i++;   

$html .= '
    <tr bgcolor="#FFF" style="color:#000;">
      <td border="1" width="5%" align="center">'.$nomor.'</td>
	  <td border="1" width="14%" align="center">'.$nim_mhs.'</td>
      <td border="1" width="48%" align="left">'.strtoupper($nama_mhs).'</td>
	  <td border="1" width="10%" align="center">'.$tm.'</td>
      <td border="1" width="10%" align="center">'.$hadir.'</td>
	  <td border="1" width="10%" align="right">'.$prosen.' %</td>
    </tr>';
$nomor++;
}

$list2="SELECT SYSDATE FROM DUAL";
$result22 = $db->Query($list2)or die("salah kueri list2 ");
while($r22 = $db->FetchRow()) {
	$tglcetak=$r22[0];
}

$list3="select nip_dosen,gelar_depan||' '||nm_pengguna||', '||gelar_belakang from aucc.dosen
left join aucc.pengguna on dosen.id_pengguna=pengguna.id_pengguna
left join aucc.program_studi on dosen.id_program_studi=program_studi.id_program_studi
where id_jabatan_pegawai=6 and id_fakultas='".$kd_fak."'";
$result23 = $db->Query($list3)or die("salah kueri list3 ");
while($r23 = $db->FetchRow()) {
	$nipwadek=$r23[0];
	$nmwadek=$r23[1];
}

$html .= '
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td colspan="10"><br>&nbsp;</td>
</tr>
<tr>
    <td colspan="10"><br>&nbsp;</td>
</tr>
<tr>
    <td colspan="10"><br>&nbsp;</td>
</tr>
<tr>
	<td width="55%"><font size="9">&nbsp;</font></td>
    <td width="45%"><font size="9">Surabaya,'.date("d", strtotime($tglcetak)).' '.$bulan[date("m", strtotime($tglcetak))].' '.date("Y", strtotime($tglcetak)).'<br>a.n Dekan<br>Wakil Dekan I<br><br><br><br>'.$nmwadek.'<br>NIP. '.$nipwadek.'</font></td>
  </tr>
</table>

';


// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('PRESENSI UAS '.strtoupper($nama_ma).' '.strtoupper($kelas_ma).'.pdf', 'I');

?>