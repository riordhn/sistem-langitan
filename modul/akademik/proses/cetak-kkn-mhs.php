<?php
require('../../../config.php');
$db = new MyOracle();

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('KKN MHS');
$pdf->SetSubject('KKN MHS');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
$logo = "../../img/akademik_images/logounair.gif";
$logo_size = "25";
$title = "UNIVERSITAS AIRLANGGA";

$id_fakultas = $_GET['fak'];
$id_pengguna= $user->ID_PENGGUNA; 
$kdfak=$user->ID_FAKULTAS; 


		$where  = "and program_studi.id_fakultas = '$kdfak'";

$mhs = $db->QueryToArray("select nim_mhs, nm_pengguna, nm_program_studi, IPK, SKS, NM_SEMESTER, THN_AKADEMIK_SEMESTER, TAHUN_AJARAN
									from mahasiswa 
									left join pengguna on pengguna.id_pengguna = mahasiswa.id_pengguna
									left join program_studi on mahasiswa.id_program_Studi = program_studi.id_program_studi
									left join pengambilan_mk on pengambilan_mk.id_mhs = mahasiswa.id_mhs
									left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk = pengambilan_mk.id_kurikulum_mk
									left join kelas_mk on kelas_mk.id_kelas_mk = pengambilan_mk.id_kelas_mk
									left join mata_kuliah on mata_kuliah.id_mata_kuliah = kurikulum_mk.id_mata_kuliah
									left join semester on semester.id_semester = pengambilan_mk.id_semester
									LEFT JOIN (select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, 
												sum(kredit_semester) as sks, id_mhs
										from (select a.id_mhs,nilai_standar_nilai,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
										row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
										count(*) over(partition by c.nm_mata_kuliah) terulang
											from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g
											where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah 
											and a.id_semester=e.id_semester
											and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs
										) x
										where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null 
										and nilai_huruf != '-'
										group by id_mhs) C ON C.ID_MHS = MAHASISWA.ID_MHS
									where status_aktif_semester = 'True' and status_mkta = 2 and STATUS_APV_PENGAMBILAN_MK='1'
									$where
							order by program_studi.id_fakultas, nm_program_studi, nim_mhs
							");


$biomhs="select j.nm_jenjang, f.id_fakultas, f.alamat_fakultas, ps.nm_program_studi, f.nm_fakultas, 
			f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas
			from program_studi ps
			left join fakultas f on ps.id_fakultas=f.id_fakultas
			left join jenjang j on ps.id_jenjang=j.id_jenjang
			where f.id_fakultas='".$id_fakultas."'";
$result1 = $db->Query($biomhs)or die("salah kueri 2 ");
while($r1 = $db->FetchRow()) {
	$nmjenjang = $r1[0];
	$fak = $r1[4];
	$alm_fak = $r1[2];
	$pos_fak = $r1[5];
	$tel_fak = $r1[6];
	$fax_fak = $r1[7];
	$web_fak = $r1[8];
	$eml_fak = $r1[9];
}

$content = "FAKULTAS ".strtoupper($fak)."\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

//set margins
$pdf->SetMargins(10, 40, 10);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default footer
//$pdf->setPrintFooter(false);
//$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 0);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('times', '', 9);

// add a page
$pdf->AddPage('L', 'A4');

$pdf->Image('../includes/logo_unair.png', 100, 50, 100, '', '', '', '', false, 72);

// set the starting point for the page content
$pdf->setPageMark();



$html = '
<table width="110%" cellpadding="2" border="0.5">
	<tr bgcolor="green" style="color:#fff;">
		<th style="text-align:center" width="3%">NO</th>
		<th style="text-align:center" width="10%">NIM</th>
		<th style="text-align:center" width="25%">NAMA</th>
		<th style="text-align:center" width="25%">PRODI</th>
        <th style="text-align:center" width="5%">IPK</th>
        <th style="text-align:center" width="5%">SKS</th>
        <th style="text-align:center" width="17%">SEMESTER</th>
	</tr>';
	$no = 1;
	foreach($mhs as $data){
	
	
$html .= '<tr>
			<td>'.$no++.'</td>
			<td>'.$data['NIM_MHS'].'</td>
			<td>'.$data['NM_PENGGUNA'].'</td>
			<td>'.$data['NM_PROGRAM_STUDI'].'</td>
            <td>'.$data['IPK'].'</td>
            <td>'.$data['SKS'].'</td>
            <td>'.$data['TAHUN_AJARAN'] . $data['NM_SEMESTER'].'</td>
	</tr>';
	}
$html .= '</table>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('kkn-mhs.pdf', 'I');

?>