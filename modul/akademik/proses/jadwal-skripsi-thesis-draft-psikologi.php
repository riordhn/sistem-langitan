<?php
require('../../../config.php');
$db = new MyOracle();

$smt = $_GET['smt'];
$fak = $_GET['fak'];

header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=draft-ujian-ta.xls");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data";

echo '
<table border="1" cellspacing="0" cellpadding="0">
           <tr>
             <td bgcolor="lightgrey" align="center"><b>NO.</b></td>
             <td bgcolor="lightgrey" align="center"><b>NIM MHS</b></td>
			 <td bgcolor="lightgrey" align="center"><b>Nama MHS</b></td>
             <td bgcolor="lightgrey" align="center"><b>NO. HP MHS</b></td>
             <td bgcolor="lightgrey" align="center"><b>JUDUL</b></td>
             <td bgcolor="lightgrey" align="center"><b>PRODI</b></td>
			 <td bgcolor="lightgrey" align="center"><b>PEMINATAN</b></td>
			 <td bgcolor="lightgrey" align="center"><b>NIP PEMBIMBING</b></td>
			 <td bgcolor="lightgrey" align="center"><b>NAMA PEMBIMBING</b></td>
			 <td bgcolor="lightgrey" align="center"><b>KETUA</b></td>
			 <td bgcolor="lightgrey" align="center"><b>SEKRETARIS</b></td>
			 <td bgcolor="lightgrey" align="center"><b>HARI/TANGGAL</b></td>
			 <td bgcolor="lightgrey" align="center"><b>PUKUL</b></td>
           </tr>';

$kueri="
select c.nim_mhs, upper(d.nm_pengguna) as nm_mhs, c.mobile_mhs, upper(a.judul_tugas_akhir) as judul,
h.nm_jenjang||' - '||g.nm_singkat_prodi as prodi, b.nm_prodi_minat, p.nip_dosen as nip_pembimbing,
trim(q.gelar_depan||' '||upper(q.nm_pengguna)||', '||q.gelar_belakang) as nm_pembimbing  
from tugas_akhir a 
left join mahasiswa c on a.id_mhs = c.id_mhs
left join pengguna d on c.id_pengguna = d.id_pengguna
left join prodi_minat b on c.id_prodi_minat = b.id_prodi_minat
left join program_studi g on c.id_program_studi = g.id_program_studi
left join jenjang h on g.id_jenjang = h.id_jenjang
left join kelas_mk e on g.id_program_studi = e.id_program_studi
left join pembimbing_ta x on a.id_mhs = x.id_mhs and x.status_dosen = 1
left join dosen p on x.id_dosen = p.id_dosen
left join pengguna q on p.id_pengguna = q.id_pengguna
left join jadwal_kelas z on z.id_kelas_mk = e.id_kelas_mk
left join kurikulum_mk f on e.id_kurikulum_mk = f.id_kurikulum_mk
where a.status = 1 and f.status_mkta = 1
and e.id_semester = 115 and g.id_fakultas = 11
and a.id_mhs not in (select id_mhs from ujian_mk_peserta where id_ujian_mk in (select id_ujian_mk from ujian_mk where id_kegiatan = 71 and id_fakultas = 11 and id_semester = 115))
group by a.judul_tugas_akhir, c.nim_mhs, d.nm_pengguna, c.mobile_mhs, p.nip_dosen, q.gelar_depan, q.nm_pengguna, q.gelar_belakang,
h.nm_jenjang, g.nm_singkat_prodi, b.nm_prodi_minat
order by h.nm_jenjang||' - '||g.nm_singkat_prodi, c.nim_mhs
";
$result = $db->Query($kueri)or die("salah kueri 01 ");
$i=1;
while($r = $db->FetchRow()) {
echo '
           <tr>
             <td>'.$i.'</td>
             <td>&nbsp;'.$r[0].'</td>
             <td>'.$r[1].'</td>
             <td>&nbsp;'.$r[2].'</td>
             <td>'.$r[3].'</td>
             <td>'.$r[4].'</td>
             <td>'.$r[5].'</td>
             <td>&nbsp;'.$r[6].'</td>
             <td>'.$r[7].'</td>
             <td></td>
             <td></td>
             <td></td>
             <td></td>
           </tr>';
$i++;
}
echo '
</table>';

?>';

?>