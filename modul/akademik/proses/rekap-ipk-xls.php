<?php
require('../../../config.php');
$db = new MyOracle();

$smt = $_GET['smt'];
$prodi = $_GET['prodi'];
$nm_prodi = $_GET['nm_prodi'];
$tahun = $_GET['tahun'];
$thn_hitung= $_GET['thn_hitung'];

header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=rekap-IPK-IPS.xls");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data";

echo '
<table border="1" cellspacing="0" cellpadding="0">
           <tr>
             <td bgcolor="lightgrey" align="center"><b>No</b></td>
             <td bgcolor="lightgrey" align="center"><b>Nim</b></td>
             <td bgcolor="lightgrey" align="center"><b>Nama Mahasiswa</b></td>
             <td bgcolor="lightgrey" align="center"><b>Status</b></td>
			 <td bgcolor="lightgrey" align="center"><b>SKS SEM</b></td>
             <td bgcolor="lightgrey" align="center"><b>IPS</b></td>
             <td bgcolor="lightgrey" align="center"><b>TTL SKS</b></td>
			 <td bgcolor="lightgrey" align="center"><b>IPK</b></td>
			 <td bgcolor="lightgrey" align="center"><b>DOSEN</b></td>
			 <td bgcolor="lightgrey" align="center"><b>NIP</b></td>
			 <td bgcolor="lightgrey" align="center"><b>NIDN</b></td>
           </tr>';
/*
$kueri="select s1.nim_mhs,upper(s1.nm_pengguna) as NM_PENGGUNA,coalesce(s1.nm_status_pengguna,'AKTIF') as nm_status_pengguna,
coalesce(s3.sks_sem,0) as sks_sem,round(coalesce(s3.ips,0),2)as ips,
coalesce(s2.SKS_TOTAL_MHS,0) as SKS_TOTAL_MHS,round(coalesce(s2.IPK_MHS,0),2)as IPK_MHS
from
(select mahasiswa.id_mhs,nim_mhs,nm_pengguna,nm_status_pengguna from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join admisi on mahasiswa.id_mhs=admisi.id_mhs and admisi.id_semester=$smt
left join status_pengguna on admisi.status_akd_mhs=status_pengguna.id_status_pengguna
where id_program_studi=$prodi and thn_angkatan_mhs<=$tahun
and mahasiswa.id_mhs not in
(select id_mhs from 
(select id_mhs,case when nm_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,status_akd_mhs
from admisi
left join semester on admisi.id_semester=semester.id_semester)
where tahun<=$thn_hitung and status_akd_mhs not in (1,3))
)s1
left join
(select id_mhs, sum(sks) as SKS_TOTAL_MHS, round((sum((sks * bobot)) / sum(sks)), 2) as IPK_MHS
from
(select id_mhs, kd_mata_kuliah as kode,kredit_semester as sks, min(nilai_huruf) as nilai, max(nilai_standar_nilai) as bobot,tahun
from 
(select a.id_mhs, c.kd_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai,
case when nm_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun
from pengambilan_mk a
join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
join semester smt on a.id_semester=smt.id_semester
left join mahasiswa m on a.id_mhs=m.id_mhs
where a.status_apv_pengambilan_mk = 1 and a.nilai_huruf<>'E' and m.id_program_studi=$prodi and a.status_hapus=0)
where tahun<=$thn_hitung
group by id_mhs, kd_mata_kuliah, kredit_semester, tahun)
group by id_mhs) s2 on s1.id_mhs=s2.id_mhs
left join 
(select id_mhs,case when sum(bobot*kredit_semester)=0 then 0 else 
round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips,
sum(sksreal)as sks_sem 
from 
(select a.id_mhs, 
case when (a.nilai_huruf = 'E' or a.nilai_huruf is null) 
and d.status_mkta in (1,2) then 0
else d.kredit_semester end as kredit_semester,d.kredit_semester as sksreal,
case when a.nilai_huruf is null then 'E' else a.nilai_huruf end as nilai_huruf,
case when e.nilai_standar_nilai is null then 0 else e.nilai_standar_nilai end as bobot
from pengambilan_mk a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where a.id_semester=$smt and a.status_apv_pengambilan_mk='1' and m.id_program_studi=$prodi and a.status_hapus=0
)group by id_mhs)s3 on s1.id_mhs=s3.id_mhs
where s1.nm_status_pengguna is not null or (s1.nm_status_pengguna is null and s2.IPK_MHS is not null)
order by nim_mhs";
*/

$kueri="select s1.id_mhs,s1.nim_mhs,upper(s1.nm_pengguna) as NM_PENGGUNA,
coalesce(sks_sem,0) as sks_sem,round(coalesce(ips,0),2)as ips,
coalesce(SKS_TOTAL_MHS,0) as SKS_TOTAL_MHS,round(coalesce(IPK_MHS,0),2)as IPK_MHS,
dosen,nip_dosen,nidn_dosen
from
(select id_mhs,nim_mhs,nm_pengguna,case when sum(bobot*kredit_semester)=0 then 0 else 
round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips,
case when id_fakultas=7 then sum(sksreal) else sum(kredit_semester) end as sks_sem 
from 
(select id_mhs,nim_mhs,nm_pengguna,id_kurikulum_mk,id_fakultas,kredit_semester, sksreal,min(nilai_huruf) as nilai, max(bobot) as bobot from
(select a.id_mhs,m.nim_mhs,pg.nm_pengguna,ps.id_fakultas,a.id_kurikulum_mk, 
case when (a.nilai_huruf = 'E' or a.nilai_huruf is null) 
and d.status_mkta in (1,2) then 0
else d.kredit_semester end as kredit_semester,d.kredit_semester as sksreal,
case when a.nilai_huruf is null then 'E' else a.nilai_huruf end as nilai_huruf,
case when e.nilai_standar_nilai is null then 0 else e.nilai_standar_nilai end as bobot
from pengambilan_mk a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join pengguna pg on m.id_pengguna=pg.id_pengguna 
left join program_studi ps on m.id_program_studi=ps.id_program_studi
left join semester s on a.id_semester=s.id_semester
where group_semester||thn_akademik_semester in
(select group_semester||thn_akademik_semester from semester where id_Semester=$smt)
and tipe_semester in ('UP','REG','RD') 
and a.status_apv_pengambilan_mk='1' and m.id_program_studi=$prodi and a.status_hapus=0 
and a.status_pengambilan_mk !=0)
group by id_mhs, nim_mhs,nm_pengguna,id_kurikulum_mk, id_fakultas, kredit_semester, sksreal
)
group by id_mhs,nim_mhs,nm_pengguna,id_fakultas)s1
left join
(select id_mhs, sum(kredit_semester) as SKS_TOTAL_MHS, 
round((sum((kredit_semester * nilai_standar_nilai)) / sum(kredit_semester)), 2) as IPK_MHS
from 
(select a.id_mhs, c.kd_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai,
case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,
row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a
join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
join semester smt on a.id_semester=smt.id_semester
join mahasiswa m on a.id_mhs=m.id_mhs
where a.status_apv_pengambilan_mk = 1 and (a.nilai_huruf<>'E' or a.nilai_huruf<>null) 
and m.id_program_studi=$prodi and a.status_hapus=0 
and a.status_pengambilan_mk !=0)
where tahun<='$thn_hitung' and rangking=1
group by id_mhs) s2 on s1.id_mhs=s2.id_mhs
left join 
(select distinct id_mhs,gelar_depan||' '||nm_pengguna||', '||gelar_belakang as dosen,nip_dosen,nidn_dosen from dosen_wali
join dosen on DOSEN_WALI.id_dosen=DOSEN.ID_DOSEN
join pengguna on DOSEN.id_pengguna=PENGGUNA.id_pengguna
where status_dosen_wali=1
and id_mhs in (select id_mhs from mahasiswa where id_program_studi=$prodi))s3 on s1.id_mhs=s3.id_mhs
order by nim_mhs";

$result = $db->Query($kueri)or die("salah kueri 01 ");
$i=1;
while($r = $db->FetchRow()) {
echo '
           <tr>
             <td>'.$i.'</td>
             <td>&nbsp;'.$r[1].'</td>
             <td>'.$r[2].'</td>
             <td></td>
             <td>'.$r[3].'</td>
             <td>'.$r[4].'</td>
             <td>'.$r[5].'</td>
             <td>'.$r[6].'</td>
			 <td>'.$r[7].'</td>
			 <td>&nbsp;'.$r[8].'</td>
			 <td>&nbsp;'.$r[9].'</td>
           </tr>';
$i++;
}
echo '
</table>';

?>