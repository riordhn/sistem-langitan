<?php
require('../../../config.php');
$db = new MyOracle();
$db2 = new MyOracle();

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sistem Langitan');
$pdf->SetTitle('Rekapitulasi Presensi Kuliah');
$pdf->SetSubject('Rekapitulasi Presensi Kuliah');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logo-".$nama_singkat.".gif";
$logo_size = "20";
$title = strtoupper($nama_pt);

$smt = $_GET['smt'];
$thn = $_GET['thn'];

$kdprodi = $_GET['prodi'];
$kueri = "select a.nm_jenjang, b.nm_program_studi, c.nm_fakultas from jenjang a, program_studi b, fakultas c 
where a.id_jenjang=b.id_jenjang and c.id_fakultas=b.id_fakultas and b.id_program_studi=$kdprodi";
$result = $db->Query($kueri)or die("salah kueri 22 ");
while($r = $db->FetchRow()) {
	$jenjang = $r[0];
	$prodi = $r[1];
	$fak = $r[2];
}

$id_kelas_mk = $_GET['cetak'];
$kueri = "select mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas.nama_kelas, jadwal_hari.nm_jadwal_hari, 
ruangan.nm_ruangan, jadwal_jam.jam_mulai||':'||jadwal_jam.menit_mulai||' - '||jadwal_jam.jam_selesai||':'||jadwal_jam.menit_selesai as jam
from kelas_mk
left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk=kelas_mk.id_kurikulum_mk
left join mata_kuliah on mata_kuliah.id_mata_kuliah=kurikulum_mk.id_mata_kuliah
left join nama_kelas on nama_kelas.id_nama_kelas=kelas_mk.no_kelas_mk
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_hari on jadwal_hari.id_jadwal_hari=jadwal_kelas.id_jadwal_hari
left join jadwal_jam on jadwal_jam.id_jadwal_jam=jadwal_kelas.id_jadwal_jam
left join ruangan on ruangan.id_ruangan=jadwal_kelas.id_ruangan
where kelas_mk.id_kelas_mk=$id_kelas_mk";
$result = $db->Query($kueri)or die("salah kueri 25 ");
while($r = $db->FetchRow()) {
	$kode_ma = $r[0];
	$nama_ma = $r[1];
	$sks_ma = $r[2];
	$kelas_ma = $r[3];
	$hari_ma = $r[4];
	$ruang_ma = $r[5];
	$jam_ma = $r[6];
}

$content = "FAKULTAS ".strtoupper($fak)."\n".strtoupper($jenjang)." - ".strtoupper($prodi)."\nPRESENSI PERKULIAHAN MAHASISWA SEMESTER ".strtoupper($smt)." ".$thn."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(5, PDF_MARGIN_TOP, 5);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 15);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage('L', 'F4');

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="17">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="12">'.$kode_ma.' - '.strtoupper($nama_ma).' ('.$sks_ma.' SKS) <br/>KELAS '.strtoupper($kelas_ma).'</td>
	<td colspan="5" rowspan="2">
	<table border="0" cellspacing="0" cellpadding="0">';

$kueri = "select case when pengguna.gelar_belakang is null then trim(pengguna.gelar_depan||' '||upper(pengguna.nm_pengguna)) 
else trim(pengguna.gelar_depan||' '||upper(pengguna.nm_pengguna)||', '||pengguna.gelar_belakang) end as nama_dsn, 
case when pengampu_mk.pjmk_pengampu_mk=1 then 'PJMA' else 'TIM' end as pjma
from kelas_mk
left join pengampu_mk on kelas_mk.id_kelas_mk = pengampu_mk.id_kelas_mk
left join dosen on pengampu_mk.id_dosen = dosen.id_dosen
left join pengguna on dosen.id_pengguna = pengguna.id_pengguna
where kelas_mk.id_kelas_mk=$id_kelas_mk
order by pengampu_mk.pjmk_pengampu_mk desc";
$result = $db->Query($kueri)or die("salah kueri 26 ");
while($r = $db->FetchRow()) {
	$nama_dsn = $r[0];
	$pjma = $r[1];
if($nama_dsn != ',') {
$html .= '
<tr>
	<td>'.$nama_dsn.' ('.$pjma.')</td>
</tr>';
} else {
$html .= '
<tr>
	<td>PJMA BELUM DITENTUKAN</td>
</tr>';
}
}
$html .= '
	</table>
	</td>
  </tr>
  <tr>
    <td colspan="12">'.strtoupper($hari_ma).' : '.$jam_ma.' <br/>'.strtoupper($ruang_ma).'</td>
  </tr>
  <tr>
    <td colspan="17">&nbsp;</td>
  </tr>
</table>
<table cellspacing="0" cellpadding="4" border="1" width="100%">
<thead>
    <tr bgcolor="lightgray" style="color:#000;">
      <td width="5%" align="center"><strong>NO.</strong></td>
	  <td width="10%" align="center"><strong>NIM</strong></td>
      <td width="30%" align="center"><strong>NAMA MAHASISWA</strong></td>
      <td width="10%" align="center"><strong>KEHADIRAN</strong></td>
	  <td width="20%" align="center"><strong>PERSENTASE KEHADIRAN</strong></td>
	  <td width="15%" align="center"><strong>TOTAL TATAP MUKA</strong></td>
	  <td width="10%" align="center"><strong>STATUS CEKAL</strong></td>
    </tr>
</thead>';

$nomor=1;
$kueri = "select mahasiswa.nim_mhs, pengguna.nm_pengguna, sum(presensi_mkmhs.kehadiran) as hadir, 
round((sum(presensi_mkmhs.kehadiran)/count(presensi_kelas.id_presensi_kelas))*100,2)||'%' as persen, count(presensi_kelas.id_presensi_kelas) as tm,
case when pengambilan_mk.status_cekal=0 then 'CEKAL' 
when pengambilan_mk.status_cekal=1 then '' else 'BLM DISET' end as cekal
from mahasiswa
left join pengambilan_mk on mahasiswa.id_mhs=pengambilan_mk.id_mhs
left join pengguna on pengguna.id_pengguna=mahasiswa.id_pengguna
left join presensi_kelas on presensi_kelas.id_kelas_mk=pengambilan_mk.id_kelas_mk 
left join presensi_mkmhs on presensi_mkmhs.id_mhs=pengambilan_mk.id_mhs and presensi_mkmhs.id_presensi_kelas=presensi_kelas.id_presensi_kelas 
where pengambilan_mk.id_kelas_mk=$id_kelas_mk and pengambilan_mk.status_apv_pengambilan_mk=1
group by mahasiswa.nim_mhs, pengguna.nm_pengguna, pengambilan_mk.status_cekal 
order by mahasiswa.nim_mhs asc";
$result = $db->Query($kueri)or die("salah kueri 27 ");
while($r = $db->FetchRow()) {
	$nim_mhs = $r[0];
	$nama_mhs = $r[1];
	$hadir_mhs = $r[2];
	$persen_mhs = $r[3];
	$tm_ma = $r[4];
	$cekal_uas = $r[5];

$style = "";
switch ($cekal_uas){
case  'CEKAL' : $style = 'bgcolor="red" style="color:#FFF;"';
		break;
case  'BLM DISET' : $style = 'bgcolor="yellow" style="color:#000;"';
		break;
case  '' : $style = 'bgcolor="white" style="color:#000;"';
		break;
}

		$html .= '
    <tr nobr="true" '.$style.'>
      <td width="5%" align="center">'.$nomor.'</td>
	  <td width="10%" align="center">'.$nim_mhs.'</td>
      <td width="30%" align="left">'.strtoupper($nama_mhs).'</td>
	  <td width="10%" align="center">'.$hadir_mhs.'</td>
	  <td width="20%" align="center">'.$persen_mhs.'</td>
	  <td width="15%" align="center">'.$tm_ma.'</td>
	  <td width="10%" align="center">'.$cekal_uas.'</td>
    </tr>';
$nomor++;
}
$html .= '</table>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('REKAP PRESENSI MHS KULIAH '.strtoupper($nama_ma).' '.strtoupper($kelas_ma).'.pdf', 'I');

?>