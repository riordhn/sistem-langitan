<?php
require('../../../config.php');
$db = new MyOracle();

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('Transkrip Akademik');
$pdf->SetSubject('Transkrip Akademik');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
//$logo = "../../img/akademik_images/logounair.gif";
//$logo_size = "25";
//$title = "UNIVERSITAS AIRLANGGA";

include '../includes/function.php';
$var = decode($_SERVER['REQUEST_URI']);
$id = $var['cetak'];


$idmhs="select nim_mhs from mahasiswa where id_mhs='".$id."'";
$result0 = $db->Query($idmhs)or die("salah kueri get ");
while($r0 = $db->FetchRow()) {
	$nim = $r0[0];
}

$biomhs="select mhs.nim_mhs, p.nm_pengguna , j.nm_jenjang, ps.nm_program_studi_eng, mhs.status_akademik_mhs, f.nm_fakultas_eng,
			f.alamat_fakultas_eng, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas, f.id_fakultas,
			kt.nm_kota, TO_CHAR(p.tgl_lahir_pengguna, 'DD-MM-YYYY'), TO_CHAR(pw.TGL_LULUS_PENGAJUAN, 'DD-MM-YYYY'), pw.no_ijasah, TO_CHAR(mhs.tgl_terdaftar_mhs, 'DD-MM-YYYY'), mhs.id_mhs, pw.judul_ta_en, pw.lahir_ijazah,
			j.id_jenjang,mhs.id_program_studi,pm.nm_prodi_minat,mhs.status,f.id_fakultas,trim('AGUSTUS'||mhs.thn_angkatan_mhs) as tgldaftar,mhs.thn_angkatan_mhs as agk,
			', '||p.gelar_belakang
			from mahasiswa mhs
			left join kota kt on kt.id_kota=mhs.lahir_kota_mhs
			left join pengajuan_wisuda pw on pw.id_mhs=mhs.id_mhs
			left join pengguna p on mhs.id_pengguna=p.id_pengguna
			left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
			left join fakultas f on ps.id_fakultas=f.id_fakultas
			left join jenjang j on ps.id_jenjang=j.id_jenjang
			left join prodi_minat pm on mhs.id_prodi_minat=pm.id_prodi_minat
			where mhs.nim_mhs='".$nim."'";
//echo $biomhs;
$result1 = $db->Query($biomhs)or die("salah kueri biodata ");
while($r1 = $db->FetchRow()) {
	$nim_mhs = $r1[0];
	$nm_mhs = $r1[1];
	$jenjang = $r1[2];
	$prodi = $r1[3];
	$status = $r1[4];
	$fak = $r1[5];
	$alm_fak = $r1[6];
	$pos_fak = $r1[7];
	$tel_fak = $r1[8];
	$fax_fak = $r1[9];
	$web_fak = $r1[10];
	$eml_fak = $r1[11];
	$kd_fak = $r1[12];
	$tpt_lhr = $r1[13];
	$tgl_lhr = $r1[14];
	$tgl_lls = $r1[15];
	$no_ijazah = $r1[16];
	$tgl_dtr = $r1[17];
	$id_mhs = $r1[18];
	$jdl_ta = $r1[19];
	$ttl = $r1[20];
	$jjg1 = $r1[21];
	$idprodi = $r1[22];
	$minatmhs = $r1[23];
	$aj = $r1[24];
	$idfak = $r1[25];
	$tgl_dtr1 = $r1[26];
	$agk = $r1[27];
	$gelar = $r1[28];
	$foto = "../../../foto_wisuda/$r1[0].jpg";
}


$nilai="select id_jenjang, sum(sks) as sks_total, round((sum((bobot*sks))/sum(sks)),2) as ipk
from 
(
select ps.id_jenjang, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join program_studi ps on m.id_program_studi=ps.id_program_studi
where rangking=1 and m.nim_mhs='".$nim_mhs."'
)
group by id_jenjang";
$result2 = $db->Query($nilai)or die("salah kueri nilai ");
while($r2 = $db->FetchRow()) {
	$jjg = $r2[0];
	$jum_sks = $r2[1];
	$ipk = number_format($r2[2],2,'.',',');
}

$pre="select nm_mhs_predikat_en 
from mhs_predikat left join jenjang on mhs_predikat.id_jenjang=jenjang.id_jenjang 
where jenjang.id_jenjang=$jjg and ipk_min_mhs_predikat<=$ipk and ipk_max_mhs_predikat>=$ipk";
$result_pre = $db->Query($pre)or die("salah kueri predikat ");
while($r_pre = $db->FetchRow()) {
	$predikat = $r_pre[0];
}

$jml2 = "select count(*) as jml from (
select e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where rangking=1 and m.nim_mhs='".$nim_mhs."')";
$resultjml2 = $db->Query($jml2)or die("salah kueri jml2 ");
while($jml2 = $db->FetchRow()) {
	$jml_2 = $jml2[0];

	$total = $jml_2 / 2;

	$limit = round($total, 0, PHP_ROUND_HALF_UP);
}

$bulan['01'] = "January"; $bulan['02'] = "Pebruary"; $bulan['03'] = "March"; $bulan['04'] = "April"; $bulan['05'] = "May"; $bulan['06'] = "June"; $bulan['07'] = "July"; $bulan['08'] = "August"; $bulan['09'] = "September"; $bulan['10'] = "October";  $bulan['11'] = "November";  $bulan['12'] = "December";

$dekan="select case when p.gelar_belakang is not null then 
trim(p.gelar_depan||' '||upper(p.nm_pengguna)||', '||p.gelar_belakang) else 
trim(p.gelar_depan||' '||upper(p.nm_pengguna)) end as nm_dosen, d.nip_dosen 
from pengguna p, dosen d, program_studi ps
where p.id_pengguna=d.id_pengguna and d.id_program_studi=ps.id_program_studi
and ps.id_fakultas=".$kd_fak." and d.id_jabatan_pegawai=5";
$resultdekan = $db->Query($dekan)or die("salah kueri dekan ");
while($dkn = $db->FetchRow()) {
	$nm_dkn = $dkn[0];
	$nip_dkn = $dkn[1];
}

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 10, 10);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 10);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 8,'', 'false');

// add a page
$pdf->AddPage('L', 'A4');

// draw jpeg image
$pdf->Image('../includes/logo_unair.png', 75, 25, 150, '', '', '', '', false, 72);
//$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
// set the starting point for the page content
$pdf->setPageMark();


$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td width="40%">
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td width="25%" align="center" valign="middle"><img src="../includes/logounair.gif" width="65" border="0"></td>
    <td width="75%" align="left" valign="middle"><font size="10"><b>AIRLANGGA UNIVERSITY<br>FACULTY OF '.strtoupper($fak).'</b></font><br>'.strtoupper($alm_fak).', '.$pos_fak.'<br>Phone. '.$tel_fak.', Fax. '.$fax_fak.'<br>'.strtolower($web_fak).', '.strtolower($eml_fak).'</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2">
		<table style="border-bottom:2px solid black; width:98%">
			<tr>
				<td colspan="2"><br>&nbsp;</td>
			</tr>
		</table>
	</td>
  </tr>
  <tr>
    <td colspan="2" align="center"><font size="10"><br><b>ACADEMIC TRANSCRIPT</b></font></td>
  </tr>
  <tr>
    <td colspan="2"><br><br><br>&nbsp;</td>
  </tr>
  <tr>
    <td width="32%"><font size="8">Major In</font></td>
	<td width="68%"><font size="8"> : '.ucwords(strtolower($prodi)).'</font></td>
  </tr>
  <tr>
    <td colspan="2"></td>
  </tr>
  <tr>
    <td><font size="8">Name of Student</font></td>
	<td><font size="8"> : '.ucwords(strtolower($nm_mhs)).''.$gelar.'</font></td>
  </tr>
  <tr>
    <td colspan="2"></td>
  </tr>
  <tr>
    <td><font size="8">Registered Number</font></td>
	<td><font size="8"> : '.$nim_mhs.'</font></td>
  </tr>
  <tr>
    <td colspan="2"></td>
  </tr>

	<tr>
    <td><font size="8">Place, Date of Birth</font></td>
	<td><font size="8"> : '.ucwords(strtolower($tpt_lhr)).', '.$bulan[date("m", strtotime($tgl_lhr))].' '.date("d", strtotime($tgl_lhr)).', '.date("Y", strtotime($tgl_lhr)).'</font></td>
	</tr>
	
  <tr>
    <td colspan="2"></td>
  </tr>
  <tr>
    <td><font size="8">Date of Graduate</font></td>
	<td><font size="8"> : '.$bulan[date("m", strtotime($tgl_lls))].' '.date("d", strtotime($tgl_lls)).', '.date("Y", strtotime($tgl_lls)).'</font></td>
  </tr>
  <tr>
    <td colspan="2"></td>
  </tr>
  <tr>
    <td><font size="8">Sertificate Number</font></td>
	<td><font size="8"> : '.$no_ijazah.'</font></td>
  </tr>
  <tr>
    <td colspan="2"></td>
  </tr>
  <tr>
    <td><font size="8">Total Number of Credit Taken</font></td>
	<td><font size="8"> : '.$jum_sks.'</font></td>
  <tr>
    <td colspan="2"></td>
  </tr>
  </tr>
  <tr>
    <td colspan="2"></td>
  </tr>
  <tr>
    <td><font size="8">Cumulative Graduate Point Average</font></td>
	<td><font size="8"> : '.$ipk.'</font></td>
  </tr>
  <tr>
    <td colspan="2"></td>
  </tr>
  <tr>
    <td><font size="8">Graduation Grade</font></td>
	<td><font size="8"> : '.ucwords(strtolower($predikat)).'</font></td>
  </tr>
  <tr>
    <td colspan="2"></td>
  </tr>';

  if  ($jjg==2)
  {
  $html .= '
  <tr>
	<td><font size="8">Title of Thesis</font></td>
	<td><font size="8"> : </font></td>
  </tr>
  <tr>
	<td colspan="2">
		<table>
			<tr>
				<td width="5%"></td>
				<td colspan="5"><font size="8">'.strtoupper($jdl_ta).'</font></td>
			</tr>
		</table>
	</td>
  </tr>';
  }

  if  ($jjg==3)
  {
  $html .= '
  <tr>
	<td><font size="8">Title of Dissertation</font></td>
	<td><font size="8"> : </font></td>
  </tr>
  <tr>
	<td colspan="2">
		<table>
			<tr>
				<td width="5%"></td>
				<td colspan="5"><font size="8">'.strtoupper($jdl_ta).'</font></td>
			</tr>
		</table>
	</td>
  </tr>';
  }

  $html .= '
  <tr>
	<td colspan="2">
		<table>
			<tr>
				<td width="32%"><br><br><br><br><br><img src="'.$foto.'" width="65" border="0"></td>
				<td colspan="5"><font size="8"><br><br><br><br><br>Surabaya, '.$bulan[date("m", strtotime($tgl_lls))].' '.date("d", strtotime($tgl_lls)).', '.date("Y", strtotime($tgl_lls)).'<br>Dean<br><br><br><br>'.$nm_dkn.'<br>NIP. '.$nip_dkn.'</font></td>
			</tr>
		</table>
	</td>
  </tr>
</table>
	</td>
	<td width="60%">
<table width="100%">
	<tr>
		<td valign="top">
		<table style="border-left:2px solid black; border-top:2px solid black; border-bottom:2px solid black;" cellpadding="2">
			<tr bgcolor="gray" style="color:#fff; border:2px solid black;">
				<td width="14%" align="center"><font size="6"><b>CODE</b></font></td>
				<td width="64%" align="center"><font size="6"><b>COURSE TITLE</b></font></td>
				<td width="11%" align="center"><font size="6"><b>CREDIT</b></font></td>
				<td width="11%" align="center"><font size="6"><b>GRADE</b></font></td>
			</tr>';
			
$list1="select * from 
(select k.*, rownum rnum from 
(select e.kd_mata_kuliah kode,e.nm_mata_kuliah_en nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where rangking=1 and m.nim_mhs='".$nim_mhs."'
order by e.nm_mata_kuliah) k
)";
$result21 = $db->Query($list1)or die("salah kueri list1 ");
while($r21 = $db->FetchRow()) {
	$kode_ma21 = $r21[0];
	$nama_ma21 = $r21[1];
	$sks_ma21 = $r21[3];
	$nilai_ma21 = $r21[4];
	$bobot_ma21 = $r21[5];

$html .= '
    <tr style="color:#000;">
	  <td align="left"><font size="6">&nbsp;'.$kode_ma21.'</font></td>
      <td align="left" style="border-left:1px solid black;"><font size="6">'.strtoupper($nama_ma21).'</font></td>
      <td align="center" style="border-left:1px solid black;"><font size="6">'.$sks_ma21.'</font></td>
	  <td align="center" style="border-left:1px solid black; border-right:1px solid black;"><font size="6">'.$nilai_ma21.'</font></td>
	</tr>';
}

$html .='
		</table>
		</td>
	</tr>
  <tr>
	<td colspan="4">
		<table width="60%">
			<tr>
				<td></td>
				<td colspan="2"><br><br><br><br>Information</td>
			</tr>
			<tr>
				<td></td>
				<td align="center" style="border:1px solid black;">Grade</td>
				<td align="center" style="border:1px solid black;">Score</td>
			</tr>
			<tr>
				<td></td>
				<td align="center" style="border:1px solid black;"><em>A</em></td>
				<td align="center" style="border:1px solid black;"><em>4</em></td>
			</tr>
			<tr>
				<td></td>
				<td align="center" style="border:1px solid black;"><em>AB</em></td>
				<td align="center" style="border:1px solid black;"><em>3.5</em></td>
			</tr>
			<tr>
				<td></td>
				<td align="center" style="border:1px solid black;"><em>B</em></td>
				<td align="center" style="border:1px solid black;"><em>3</em></td>
			</tr>
			<tr>
				<td></td>
				<td align="center" style="border:1px solid black;"><em>BC</em></td>
				<td align="center" style="border:1px solid black;"><em>2.5</em></td>
			</tr>
			<tr>
				<td></td>
				<td align="center" style="border:1px solid black;"><em>C</em></td>
				<td align="center" style="border:1px solid black;"><em>2</em></td>
			</tr>
			<tr>
				<td></td>
				<td align="center" style="border:1px solid black;"><em>D</em></td>
				<td align="center" style="border:1px solid black;"><em>1</em></td>
			</tr>
			<tr>
				<td></td>
				<td align="center" style="border:1px solid black;"><em>E</em></td>
				<td align="center" style="border:1px solid black;"><em>0</em></td>
			</tr>
		</table>
	</td>
  </tr>
</table>
	</td>
  </tr>
</table>
';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('TRANSKRIP AKADEMIK '.strtoupper($nim_mhs).'.pdf', 'I');

?>