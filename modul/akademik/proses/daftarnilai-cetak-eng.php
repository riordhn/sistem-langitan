<?php
require('../../../config.php');
$db = new MyOracle();

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('Daftar Nilai');
$pdf->SetSubject('Daftar Nilai');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
//$logo = "../../img/akademik_images/logounair.gif";
//$logo_size = "25";
//$title = "UNIVERSITAS AIRLANGGA";

include '../includes/function.php';
$var = decode($_SERVER['REQUEST_URI']);
$nim = $var['cetak'];

$biomhs="select mhs.nim_mhs, case when j.id_jenjang=2 then p.nm_pengguna||', '||p.gelar_belakang else p.nm_pengguna end , j.nm_jenjang, ps.nm_program_studi_eng, mhs.status_akademik_mhs, f.nm_fakultas_eng,
			f.alamat_fakultas_eng, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas, f.id_fakultas,
			kt.nm_kota, TO_CHAR(p.tgl_lahir_pengguna, 'DD-MM-YYYY'), TO_CHAR(pw.TGL_LULUS_PENGAJUAN, 'DD-MM-YYYY'), pw.no_ijasah, TO_CHAR(mhs.tgl_terdaftar_mhs, 'DD-MM-YYYY'), mhs.id_mhs, pw.judul_ta_en, pw.lahir_ijazah,
			j.id_jenjang,mhs.id_program_studi,pm.nm_prodi_minat,mhs.id_mhs
			from mahasiswa mhs
			left join kota kt on kt.id_kota=mhs.lahir_kota_mhs
			left join pengajuan_wisuda pw on pw.id_mhs=mhs.id_mhs
			left join pengguna p on mhs.id_pengguna=p.id_pengguna
			left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
			left join fakultas f on ps.id_fakultas=f.id_fakultas
			left join jenjang j on ps.id_jenjang=j.id_jenjang
			left join prodi_minat pm on mhs.id_prodi_minat=pm.id_prodi_minat
			where mhs.nim_mhs='".$nim."'";
$result1 = $db->Query($biomhs)or die("salah kueri biodata ");
while($r1 = $db->FetchRow()) {
	$nim_mhs = $r1[0];
	$nm_mhs = $r1[1];
	$jenjang = $r1[2];
	$prodi = $r1[3];
	$status = $r1[4];
	$fak = $r1[5];
	$alm_fak = $r1[6];
	$pos_fak = $r1[7];
	$tel_fak = $r1[8];
	$fax_fak = $r1[9];
	$web_fak = $r1[10];
	$eml_fak = $r1[11];
	$kd_fak = $r1[12];
	$tpt_lhr = $r1[13];
	$tgl_lhr = $r1[14];
	$tgl_lls = $r1[15];
	$no_ijazah = $r1[16];
	$tgl_dtr = $r1[17];
	$id_mhs = $r1[18];
	$jdl_ta = $r1[19];
	$ttl = $r1[20];
	$jjg1 = $r1[21];
	$idprodi = $r1[22];
	$minatmhs = $r1[23];
	$id_mhs = $r1[24];
}

$nilai="select id_jenjang, sum(sks) as sks_total, round((sum((bobot*sks))/sum(sks)),2) as ipk,sum(bobot*sks) as bobot 
from
(
select ps.id_jenjang, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join program_studi ps on m.id_program_studi=ps.id_program_studi
where rangking=1 and m.nim_mhs='".$nim."'
)
group by id_jenjang";
$result2 = $db->Query($nilai)or die("salah kueri nilai ");
while($r2 = $db->FetchRow()) {
	$jjg = $r2[0];
	$jum_sks = $r2[1];
	$ipk = number_format($r2[2],2,'.',',');
	$bobot = number_format($r2[3],2,'.',',');
}

$pre="select nm_mhs_predikat 
from mhs_predikat left join jenjang on mhs_predikat.id_jenjang=jenjang.id_jenjang 
where jenjang.id_jenjang=$jjg and ipk_min_mhs_predikat<=$ipk and ipk_max_mhs_predikat>=$ipk";
$result_pre = $db->Query($pre)or die("salah kueri predikat ");
while($r_pre = $db->FetchRow()) {
	$predikat = $r_pre[0];
}

$jml2 = "select count(*) as jml from (
select e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where rangking=1 and m.nim_mhs='".$nim."')";
$resultjml2 = $db->Query($jml2)or die("salah kueri jml2 ");
while($jml2 = $db->FetchRow()) {
	$jml_2 = $jml2[0];

	$total = $jml_2 / 2;

	$limit = round($total, 0, PHP_ROUND_HALF_UP);
}

$bulan['01'] = "January"; $bulan['02'] = "Pebruary"; $bulan['03'] = "March"; $bulan['04'] = "April"; $bulan['05'] = "May"; $bulan['06'] = "June"; $bulan['07'] = "July"; $bulan['08'] = "August"; $bulan['09'] = "September"; $bulan['10'] = "October";  $bulan['11'] = "November";  $bulan['12'] = "December";


// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 10, 10);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 10);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 8);

// add a page
$pdf->AddPage('P', 'Legal');

// draw jpeg image
//$pdf->Image('../includes/logo_unair.png', 75, 25, 150, '', '', '', '', false, 72);
//$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
// set the starting point for the page content
$pdf->setPageMark();

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td width="15%" align="left" valign="middle"><img src="../includes/logounair.gif" width="65" border="0"></td>
    <td width="85%" align="left" valign="middle"><font size="10"><b>AIRLANGGA UNIVERSITY<br>FACULTY OF '.strtoupper($fak).'</b></font><br>'.strtoupper($alm_fak).', '.$pos_fak.'<br>Phone. '.$tel_fak.', Fax. '.$fax_fak.'<br>'.strtolower($web_fak).', '.strtolower($eml_fak).'</td>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2">
		<table style="border-bottom:2px solid black; width:98%">
			<tr>
				<td colspan="2"><br>&nbsp;</td>
			</tr>
		</table>
	</td>
  </tr>
  <tr>
    <td colspan="2" align="center"><font size="11"><br><b>SCORE SHEET</b></font></td>
  </tr>
  <tr>
    <td colspan="2"><br>&nbsp;</td>
  </tr>
  <tr>
    <td width="15%"><font size="9">Registered Number</font></td>
	<td width="30%"><font size="9"> : '.$nim_mhs.'</font></td>
	<td width="8%"></td>
    <td width="22%"><font size="9">Major In</font></td>
	<td width="28%"><font size="9"> : '.ucwords(strtolower($prodi)).'</font></td>
  </tr>
  <tr>
    <td><font size="9">Name of Student</font></td>
	<td><font size="9"> : '.ucwords(strtolower($nm_mhs)).'</font></td>
	<td></td>
	<td><font size="9">Total Number of Credit Taken</font></td>
	<td><font size="9"> : '.$jum_sks.' </font></td>
  </tr>
  <tr>
    <td><font size="9">Date of Birth Place</font></td>
	<td><font size="9"> : '.ucwords(strtolower($tpt_lhr)).','.$bulan[date("m", strtotime($tgl_lhr))].'  '.date("d", strtotime($tgl_lhr)).', '.date("Y", strtotime($tgl_lhr)).'</font></td>
	<td></td>
	<td><font size="9">GPA</font></td>
	<td><font size="9"> : '.$ipk.'</font></td>
  </tr>
  <tr>
  <td colspan="2"><br>&nbsp;</td>
  </tr>
</table>

<table width="100%" border="1" cellpadding="2">
<tr>
	<td width="5%" align="center"><font size="7"><b>YEAR</b></font></td>
	<td width="7%" align="center"><font size="7"><b>CODE</b></font></td>
	<td width="28%" align="center"><font size="7"><b>COURSE TITLE</b></font></td>
	<td width="6%" align="center"><font size="7"><b>CREDIT</b></font></td>
	<td width="6%" align="center"><font size="7"><b>GRADE</b></font></td>
	<td width="5%" align="center"><font size="7"><b>YEAR</b></font></td>
	<td width="7%" align="center"><font size="7"><b>CODE</b></font></td>
	<td width="28%" align="center"><font size="7"><b>COURSE TITLE</b></font></td>
	<td width="6%" align="center"><font size="7"><b>CREDIT</b></font></td>
	<td width="6%" align="center"><font size="7"><b>GRADE</b></font></td>
</tr>';

$list1="select * from (
select rownum as b1,  thn,thn_akademik_semester ,thn_angkatan_mhs,
kode,nama,id_mhs,sks,nilai,bobot from (
select rownum as C1,thn,thn_akademik_semester,thn_angkatan_mhs,
kode,nama,id_mhs,sks,nilai,bobot from 
(select case when s.group_semester='Ganjil' and  (s.nm_semester='Ujian Perbaikan' or s.nm_semester='Ganjil') then thn_akademik_semester||'-'||'1' 
else case when s.group_semester='Ganjil' and  (s.nm_semester='Ujian Perbaikan' or s.nm_semester='Genap') then thn_akademik_semester||'-'||'2'  
else case when s.group_semester='Genap' and  (s.nm_semester='Pendek') then thn_akademik_semester||'-'||'4'    
else thn_akademik_semester||'-'||'2' end end end as thn,
thn_akademik_semester,thn_angkatan_mhs,
e.kd_mata_kuliah kode,e.nm_mata_kuliah_en nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join semester s on a.id_semester=s.id_semester
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where rangking=1 and m.id_mhs='".$id_mhs."' and rownum<= $jml_2
order by thn_akademik_semester,group_semester,kode)) 
where c1 <= $limit)s1
FULL OUTER join
(select rownum as b2,  thn1,thn_akademik_semester1 ,thn_angkatan_mhs1,
kode1,nama1,id_mhs1,sks,nilai,bobot from (
select rownum as c2,thn as thn1,thn_akademik_semester as thn_akademik_semester1 ,thn_angkatan_mhs as thn_angkatan_mhs1,
kode as kode1,nama as nama1,id_mhs as id_mhs1,sks as sks,nilai as nilai,bobot as bobot from 
(select case when s.group_semester='Ganjil' and  (s.nm_semester='Ujian Perbaikan' or s.nm_semester='Ganjil') then thn_akademik_semester||'-'||'1' 
else case when s.group_semester='Ganjil' and  (s.nm_semester='Ujian Perbaikan' or s.nm_semester='Genap') then thn_akademik_semester||'-'||'2'  
else case when s.group_semester='Genap' and  (s.nm_semester='Pendek') then thn_akademik_semester||'-'||'4'    
else thn_akademik_semester||'-'||'2' end end end as thn,
thn_akademik_semester,thn_angkatan_mhs,
e.kd_mata_kuliah kode,e.nm_mata_kuliah_en nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join semester s on a.id_semester=s.id_semester
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where rangking=1 and m.id_mhs='".$id_mhs."' and rownum<= $jml_2
order by thn_akademik_semester,group_semester,kode))
where c2 > $limit
) s2 on s1.b1=s2.b2
order by b1";

$result21 = $db->Query($list1)or die("salah kueri list1 ");
while($r21 = $db->FetchRow()) {
	$tsmt=$r21[1];
	$kode_ma21 = $r21[4];
	$nama_ma21 = $r21[5];
	$sks_ma21 = $r21[7];
	$nilai_ma21 = $r21[8];
	$bobot_ma21 = $r21[9];

	$tsmt1=$r21[11];
	$kode_ma22 = $r21[14];
	$nama_ma22 = $r21[15];
	$sks_ma22 = $r21[17];
	$nilai_ma22 = $r21[18];
	$bobot_ma22 = $r21[19];
$html .= '
   	<tr style="color:#000;">
	<td width="5%" align="center"><font size="7">'.$tsmt.'</font></td>
	<td width="7%" align="left"><font size="7">'.$kode_ma21.'</font></td>
	<td width="28%" align="left"><font size="7">'.$nama_ma21.'</font></td>
	<td width="6%" align="center"><font size="7">'.$sks_ma21.'</font></td>
	<td width="6%" align="center"><font size="7">'.$nilai_ma21.'</font></td>
	<td width="5%" align="center"><font size="7">'.$tsmt1.'</font></td>
	<td width="7%" align="left"><font size="7">'.$kode_ma22.'</font></td>
	<td width="28%" align="left"><font size="7">'.$nama_ma22.'</font></td>
	<td width="6%" align="center"><font size="7">'.$sks_ma22.'</font></td>
	<td width="6%" align="center"><font size="7">'.$nilai_ma22.'</font></td>
	</tr>';


}
$list2="SELECT SYSDATE FROM DUAL";
$result22 = $db->Query($list2)or die("salah kueri list2 ");
while($r22 = $db->FetchRow()) {
	$tglcetak=$r22[0];
}

$list3="select nip_dosen,case when gelar_belakang is not null then 
trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) else 
trim(gelar_depan||' '||upper(nm_pengguna)) end from dosen
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
left join program_studi on dosen.id_program_studi=program_studi.id_program_studi
where id_jabatan_pegawai=6 and id_fakultas='".$kd_fak."'";
$result23 = $db->Query($list3)or die("salah kueri list3 ");
while($r23 = $db->FetchRow()) {
	$nipwadek=$r23[0];
	$nmwadek=$r23[1];
}

$html .= '
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td colspan="10"><br>&nbsp;</td>
</tr>
<tr>
    <td colspan="10"><br>&nbsp;</td>
</tr>

<tr>
	<td width="55%"><font size="9">&nbsp;</font></td>
    <td width="45%"><font size="9">Surabaya,&nbsp;'.$bulan[date("m", strtotime($tglcetak))].' '.date("d", strtotime($tglcetak)).', '.date("Y", strtotime($tglcetak)).'<br>for the Dean<br>Vice Dean for Academic Affairs<br><br><br><br><br>'.$nmwadek.'<br>NIP. '.$nipwadek.'</font></td>
    </tr>
</table>

';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('TRANSKRIP AKADEMIK '.strtoupper($nim_mhs).'.pdf', 'I');

?>