<?php
require('../../../config.php');
$db = new MyOracle();

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages().' - '.date("Y-m-d H:i:s", time()), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('Plot Ruang');
$pdf->SetSubject('Plot Ruang');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logounair.gif";
$logo_size = "25";
$title = "UNIVERSITAS AIRLANGGA";

$kdfak=$user->ID_FAKULTAS;
$hari = $_GET['id_hari'];
$smt = $_GET['id_smt'];

$datahari="select nm_jadwal_hari from jadwal_hari where id_jadwal_hari=$hari";
$result = $db->Query($datahari)or die("salah kueri datahari");
while($r = $db->FetchRow()) {
	$nm_hari = $r[0];
}

$smt="select id_semester, ', SEMESTER '||upper(nm_semester)||' '||tahun_ajaran as nm_semester from semester where id_semester=$smt";
$result = $db->Query($smt)or die("salah kueri smt ");
while($r = $db->FetchRow()) {
	$smtaktif = $r[0];
	$nm_semester = $r[1];
}

$header = "select a.nm_jenjang, b.nm_program_studi, f.nm_fakultas, f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, 
			f.website_fakultas, f.email_fakultas
			from jenjang a, program_studi b, fakultas f where a.id_jenjang=b.id_jenjang and f.id_fakultas=b.id_fakultas and f.id_fakultas=$kdfak";
$result = $db->Query($header)or die("salah kueri header");
while($r = $db->FetchRow()) {
	$jenjang = $r[0];
	$prodi = $r[1];
	$fak = $r[2];
	$alm_fak = $r[3];
	$pos_fak = $r[4];
	$tel_fak = $r[5];
	$fax_fak = $r[6];
	$web_fak = $r[7];
	$eml_fak = $r[8];
}

$content = "FAKULTAS ".strtoupper($fak)."\n\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 35, 5);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 15);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 8);

// add a page
$pdf->AddPage('L', 'A4');

$html = '
<p align="center"><strong>PLOTING RUANG HARI '.strtoupper($nm_hari).''.$nm_semester.'</strong></p>
<table border="1" cellspacing="0" cellpadding="1">
	<thead>
	   <tr>	
			<th width="50" rowspan="2" align="center" bgcolor="#bcbcbc"><strong>Nama Ruang</strong></th>
			<th width="936" colspan="12" align="center" bgcolor="#bcbcbc"><strong>Jam Mulai Kuliah</strong></th>
	   </tr>
	   <tr>
			<th width="78" align="center" bgcolor="#bcbcbc"><strong>08:00</strong></th>
			<th width="78" align="center" bgcolor="#bcbcbc"><strong>09:00</strong></th>
			<th width="78" align="center" bgcolor="#bcbcbc"><strong>10:00</strong></th>
			<th width="78" align="center" bgcolor="#bcbcbc"><strong>11:00</strong></th>
			<th width="78" align="center" bgcolor="#bcbcbc"><strong>12:00</strong></th>
			<th width="78" align="center" bgcolor="#bcbcbc"><strong>13:00</strong></th>
			<th width="78" align="center" bgcolor="#bcbcbc"><strong>14:00</strong></th>
			<th width="78" align="center" bgcolor="#bcbcbc"><strong>15:00</strong></th>
			<th width="78" align="center" bgcolor="#bcbcbc"><strong>16:00</strong></th>
			<th width="78" align="center" bgcolor="#bcbcbc"><strong>17:00</strong></th>
			<th width="78" align="center" bgcolor="#bcbcbc"><strong>18:00</strong></th>
			<th width="78" align="center" bgcolor="#bcbcbc"><strong>19:00</strong></th>
	   </tr>
	</thead>
	<tbody>';

$datajadruang="SELECT nm_ruangan,
MAX(DECODE ( kdjam , 08, trim(nm_mk) )) jam0,
MAX(DECODE ( kdjam , 09, trim(nm_mk) )) jam1,
MAX(DECODE ( kdjam , 10, trim(nm_mk) )) jam2,
MAX(DECODE ( kdjam , 11, trim(nm_mk) )) jam3,
MAX(DECODE ( kdjam , 12, trim(nm_mk) )) jam4,
MAX(DECODE ( kdjam , 13, trim(nm_mk) )) jam5,
MAX(DECODE ( kdjam , 14, trim(nm_mk) )) jam6,
MAX(DECODE ( kdjam , 15, trim(nm_mk) )) jam7,
MAX(DECODE ( kdjam , 16, trim(nm_mk) )) jam8,
MAX(DECODE ( kdjam , 17, trim(nm_mk) )) jam9,
MAX(DECODE ( kdjam , 18, trim(nm_mk) )) jam10,
MAX(DECODE ( kdjam , 19, trim(nm_mk) )) jam11
FROM
(
select kdjam,nm_ruangan||'<br>(<font color=\"blue\"><b>'||kapasitas_ruangan||'</b></font>)' as nm_ruangan,
wm_concat(' '||kd_mata_kuliah||' - '||nm_mata_kuliah||' ('||nama_kelas||') '||kredit_semester||' SKS') as nm_mk
from
(
select jam_mulai as kdjam,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,kapasitas_ruangan
from kelas_mk left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester=$smtaktif and jadwal_kelas.id_jadwal_hari=$hari
group by jam_mulai,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,kapasitas_ruangan
)
group by kdjam,nm_ruangan,kapasitas_ruangan
)
GROUP BY nm_ruangan";
$result = $db->Query($datajadruang)or die("salah kueri datajadruang ");
while($r = $db->FetchRow()) {
	$nm_ruangan = $r[0];
	$jam0 = $r[1];
	$jam1 = $r[2];
	$jam2= $r[3];
	$jam3 = $r[4];
	$jam4 = $r[5];
	$jam5 = $r[6];
	$jam6 = $r[7];
	$jam7 = $r[8];
	$jam8 = $r[9];
	$jam9 = $r[10];
	$jam10 = $r[11];
	$jam11 = $r[12];

$html .= '
		  <tr>
			<td width="50" align="center"><strong>'.$nm_ruangan.'</strong></td>';
			if(strstr($jam0, ", ")){
			$html .= '
			<td width="78" bgcolor="#ff828e">'.$jam0.'</td>';
			} else {
			$html .= '
			<td width="78">'.$jam0.'</td>';
			}
			if(strstr($jam1, ", ")){
			$html .= '
			<td width="78" bgcolor="#ff828e">'.$jam1.'</td>';
			} else {
			$html .= '
			<td width="78">'.$jam1.'</td>';
			}
			if(strstr($jam2, ", ")){
			$html .= '
			<td width="78" bgcolor="#ff828e">'.$jam2.'</td>';
			} else {
			$html .= '
			<td width="78">'.$jam2.'</td>';
			}
			if(strstr($jam3, ", ")){
			$html .= '
			<td width="78" bgcolor="#ff828e">'.$jam3.'</td>';
			} else {
			$html .= '
			<td width="78">'.$jam3.'</td>';
			}
			if(strstr($jam4, ", ")){
			$html .= '
			<td width="78" bgcolor="#ff828e">'.$jam4.'</td>';
			} else {
			$html .= '
			<td width="78">'.$jam4.'</td>';
			}
			if(strstr($jam5, ", ")){
			$html .= '
			<td width="78" bgcolor="#ff828e">'.$jam5.'</td>';
			} else {
			$html .= '
			<td width="78">'.$jam5.'</td>';
			}
			if(strstr($jam6, ", ")){
			$html .= '
			<td width="78" bgcolor="#ff828e">'.$jam6.'</td>';
			} else {
			$html .= '
			<td width="78">'.$jam6.'</td>';
			}
			if(strstr($jam7, ", ")){
			$html .= '
			<td width="78" bgcolor="#ff828e">'.$jam7.'</td>';
			} else {
			$html .= '
			<td width="78">'.$jam7.'</td>';
			}
			if(strstr($jam8, ", ")){
			$html .= '
			<td width="78" bgcolor="#ff828e">'.$jam8.'</td>';
			} else {
			$html .= '
			<td width="78">'.$jam8.'</td>';
			}
			if(strstr($jam9, ", ")){
			$html .= '
			<td width="78" bgcolor="#ff828e">'.$jam9.'</td>';
			} else {
			$html .= '
			<td width="78">'.$jam9.'</td>';
			}
			if(strstr($jam10, ", ")){
			$html .= '
			<td width="78" bgcolor="#ff828e">'.$jam10.'</td>';
			} else {
			$html .= '
			<td width="78">'.$jam10.'</td>';
			}
			if(strstr($jam11, ", ")){
			$html .= '
			<td width="78" bgcolor="#ff828e">'.$jam11.'</td>';
			} else {
			$html .= '
			<td width="78">'.$jam11.'</td>';
			}
$html .= '
		  </tr>';
}
$html .= '
	</tbody>
</table>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('PLOTING RUANG HARI '.strtoupper($nm_hari).'.pdf', 'I');

?>
