<?php
require '../../../config.php';
require '../../../tcpdf/config/lang/eng.php';
require '../../../tcpdf/tcpdf.php';

$akademik = new Akademik($db); // includes/Akademik.class.php

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sistem Langitan');
$pdf->SetTitle('Daftar Nilai');
$pdf->SetSubject('Daftar Nilai');

include '../includes/function.php';
$var = decode($_SERVER['REQUEST_URI']);
$nim = $var['cetak'];

$sql_biodata = "
    SELECT
        mhs.nim_mhs,
        p.nm_pengguna,
        j.nm_jenjang,
        ps.nm_program_studi,
        mhs.status_akademik_mhs,
        f.nm_fakultas,
        f.alamat_fakultas,
        f.kodepos_fakultas,
        f.telpon_fakultas,
        f.faksimili_fakultas,
        f.website_fakultas,
        f.email_fakultas,
        f.id_fakultas,
        kt.nm_kota,
        TO_CHAR(p.tgl_lahir_pengguna, 'DD-MM-YYYY'),
        TO_CHAR(mhs.tgl_lulus_mhs, 'DD-MM-YYYY'),
        pw.no_ijasah,
        TO_CHAR(mhs.tgl_terdaftar_mhs, 'DD-MM-YYYY'),
        mhs.id_mhs,
        pw.judul_ta,
        pw.lahir_ijazah,
        j.id_jenjang,
        mhs.id_program_studi,
        pm.nm_prodi_minat,
        mhs.id_mhs,
        thn_angkatan_mhs
    FROM mahasiswa mhs
    LEFT JOIN kota kt ON kt.id_kota = mhs.lahir_kota_mhs
    LEFT JOIN pengajuan_wisuda pw ON pw.id_mhs = mhs.id_mhs
    JOIN pengguna p ON mhs.id_pengguna = p.id_pengguna
    JOIN program_studi ps ON mhs.id_program_studi = ps.id_program_studi
    JOIN fakultas f ON ps.id_fakultas = f.id_fakultas
    JOIN jenjang j ON ps.id_jenjang = j.id_jenjang
    LEFT JOIN prodi_minat pm ON mhs.id_prodi_minat = pm.id_prodi_minat
    WHERE p.ID_PERGURUAN_TINGGI = {$user->ID_PERGURUAN_TINGGI} AND  mhs.nim_mhs = '{$nim}'";
$db->Query($sql_biodata);
$r1 = $db->FetchRow();
$nim_mhs = $r1[0];
$nm_mhs = $r1[1];
$jenjang = $r1[2];
$prodi = $r1[3];
$status = $r1[4];
$fak = 'Fakultas ' . $r1[5];
$alm_fak = $r1[6];
$pos_fak = $r1[7];
$tel_fak = $r1[8];
$fax_fak = $r1[9];
$web_fak = $r1[10];
$eml_fak = $r1[11];
$kd_fak = $r1[12];
$tpt_lhr = $r1[13];
$tgl_lhr = $r1[14];
$tgl_lls = $r1[15];
$no_ijazah = $r1[16];
$tgl_dtr = $r1[17];
$id_mhs = $r1[18];
$jdl_ta = $r1[19];
$ttl = $r1[20];
$jjg1 = $r1[21];
$idprodi = $r1[22];
$minatmhs = $r1[23];

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 10, 10);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 10);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 8);

// add a page
$pdf->AddPage('P', 'Legal');

// set the starting point for the page content
$pdf->setPageMark();

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td width="15%" align="left" valign="middle"><img src="../../../img/akademik_images/logo-'.$nama_singkat.'.gif" width="65" border="0"></td>
    <td width="85%" align="left" valign="middle"><font size="10"><b>'.$nama_pt.'<br>'.$fak.'</b></font><br>'.$alm_fak.', '.$pos_fak.'<br>Telp. '.$tel_fak.', Fax. '.$fax_fak.'<br>'.strtolower($web_fak).', '.strtolower($eml_fak).'</td>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2">
		<table style="border-bottom:2px solid black; width:98%">
			<tr>
				<td colspan="2"><br>&nbsp;</td>
			</tr>
		</table>
	</td>
  </tr>
  <tr>
    <td colspan="2" align="center"><font size="11"><br><b>DAFTAR NILAI KUMULATIF</b></font></td>
  </tr>
  <tr>
    <td colspan="2"><br>&nbsp;</td>
  </tr>
  <tr>
    <td width="15%"><font size="9">NIM</font></td>
	<td width="30%"><font size="9"> : '.$nim_mhs.'</font></td>
	<td width="10%"></td>
    <td width="15%"></td>
	<td width="30%"></td>
  </tr>
  <tr>
    <td><font size="9">Nama</font></td>
	<td><font size="9"> : '.$nm_mhs.'</font></td>
	<td></td>
	<td></td>
	<td></td>
  </tr>
  <tr>
    <td width="15%"><font size="9">Program Studi</font></td>
	<td width="30%"><font size="9"> : '.$jenjang.' '.$prodi.'</font></td>
	<td></td>
	<td></td>
	<td></td>
  </tr>
  <tr>
  <td colspan="2"><br>&nbsp;</td>
  </tr>
</table>

<table width="100%" border="1" cellpadding="4">
<tr>
    <th rowspan="2" align="center" width="5%">No</th>
    <th rowspan="2" align="center" width="10%">KODE</th>
    <th rowspan="2" align="center" width="40%">NAMA MATA KULIAH</th>
    <th rowspan="2" align="center" width="5%">SKS</th>
    <th colspan="6" align="center" width="40%">Nilai</th>
</tr>
<tr>
         <th align="center">1</th>
         <th align="center">2</th>
         <th align="center">3</th>
         <th align="center">4</th>
         <th align="center">5</th>
         <th align="center">6</th>
</tr>
';

$jaf=$db->QueryToArray(
"
select
a.*,
b.nilai p1,
c.nilai p2,
d.nilai p3,
e.nilai p4,
f.nilai p5,
g.nilai p6,
'('''||b.id_pengambilan_mk||''','''||c.id_pengambilan_mk||''','''||d.id_pengambilan_mk||''','''||e.id_pengambilan_mk||''','''||f.id_pengambilan_mk||''','''||g.id_pengambilan_mk||''')' ids_pengambilan_mk,
b.id_pengambilan_mk ipm1,
c.id_pengambilan_mk ipm2,
d.id_pengambilan_mk ipm3,
e.id_pengambilan_mk ipm4,
f.id_pengambilan_mk ipm5,
g.id_pengambilan_mk ipm6,
b.waktu w1,
c.waktu w2,
d.waktu w3,
e.waktu w4,
f.waktu w5,
g.waktu w6
from(
select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p,a.status_hapus,a.id_pengambilan_mk
 from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
thn_akademik_semester,nm_semester
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join semester sm on sm.id_semester=a.id_semester
where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where  m.nim_mhs='{$nim}' and rangking=1) a
left join
(
select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p,a.id_pengambilan_mk,n.id_pengambilan_mk waktu
 from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
thn_akademik_semester,nm_semester
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join semester sm on sm.id_semester=a.id_semester
where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join (select distinct id_pengambilan_mk from log_update_nilai) n on n.id_pengambilan_mk=a.id_pengambilan_mk
where  m.nim_mhs='{$nim}' and p=1
) b on a.nama=b.nama
left join
(
select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p,a.id_pengambilan_mk,n.id_pengambilan_mk waktu
 from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
thn_akademik_semester,nm_semester
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join semester sm on sm.id_semester=a.id_semester
where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join (select distinct id_pengambilan_mk from log_update_nilai) n on n.id_pengambilan_mk=a.id_pengambilan_mk
where  m.nim_mhs='{$nim}' and p=2
) c on a.nama=c.nama
left join
(
select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p,a.id_pengambilan_mk,n.id_pengambilan_mk waktu
 from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
thn_akademik_semester,nm_semester
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join semester sm on sm.id_semester=a.id_semester
where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join (select distinct id_pengambilan_mk from log_update_nilai) n on n.id_pengambilan_mk=a.id_pengambilan_mk
where  m.nim_mhs='{$nim}' and p=3
) d on a.nama=d.nama
left join
(
select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p,a.id_pengambilan_mk,n.id_pengambilan_mk waktu
 from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
thn_akademik_semester,nm_semester
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join semester sm on sm.id_semester=a.id_semester
where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join (select distinct id_pengambilan_mk from log_update_nilai) n on n.id_pengambilan_mk=a.id_pengambilan_mk
where  m.nim_mhs='{$nim}' and p=4
) e on a.nama=e.nama
left join
(
select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p,a.id_pengambilan_mk,n.id_pengambilan_mk waktu
 from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
thn_akademik_semester,nm_semester
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join semester sm on sm.id_semester=a.id_semester
where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join (select distinct id_pengambilan_mk from log_update_nilai) n on n.id_pengambilan_mk=a.id_pengambilan_mk
where  m.nim_mhs='{$nim}' and p=5
) f on a.nama=f.nama
left join
(
select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p,a.id_pengambilan_mk,n.id_pengambilan_mk waktu
 from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
thn_akademik_semester,nm_semester
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join semester sm on sm.id_semester=a.id_semester
where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join (select distinct id_pengambilan_mk from log_update_nilai) n on n.id_pengambilan_mk=a.id_pengambilan_mk
where  m.nim_mhs='{$nim}' and p=6
) g on a.nama=g.nama
where a.status_hapus = 0
order by a.nama asc
"
);	

$i=1;
foreach($jaf as $data){
$html .='
	<tr nobr="true">
		<td align="center">'.$i++.'</td>
		<td align="center">'.$data['KODE'].'</td>
		<td>'.$data['NAMA'].'</td>
		<td align="center">'.$data['SKS'].'</td>
		<td align="center">'.$data['P1'].'</td>
		<td align="center">'.$data['P2'].'</td>
		<td align="center">'.$data['P3'].'</td>
		<td align="center">'.$data['P4'].'</td>
		<td align="center">'.$data['P5'].'</td>
		<td align="center">'.$data['P6'].'</td>
	</tr>';
}

// Pengambilan IPK BARU
$ipk = $akademik->get_ipk_mhs($id_mhs);

if ($user->ID_PERGURUAN_TINGGI == PT_UMAHA) {
	$kota_ttd = 'Sidoarjo';
	$jabatan_ttd = 'Direktur Akademik, SDM, & Sisfor';
	$nama_ttd = 'Wiji Lestariningsih, S.Pd., M.Pd.';
	$nip_ttd = 'NIDN 0707058501';
} else if ($user->ID_PERGURUAN_TINGGI == PT_UNU_LAMPUNG) {
	$kota_ttd = 'Lampung Timur';
	$jabatan_ttd = 'Kepala BAAKSI';
	$nama_ttd = 'Nur Syamsiah, S.Pd., M.Pd.';
	$nip_ttd = 'NIP 88203012';
} else  {
	$kota_ttd = '';
	$jabatan_ttd = '';
	$nama_ttd = '';
	$nip_ttd = '';
}

$html .= '
</table>
<p></p>
<table>
		   <tr>
             <td width="35%"><b>Total SKS yang Telah diambil</b></td>
			 <td width="3%">:</td>
             <td width="62%"><b>'.$ipk['TOTAL_SKS'].'</b></td>
           </tr>
		   <tr>
             <td><b>Indeks Prestasi Kumulatif</b></td>
			 <td>:</td>
             <td><b>'.$ipk['IPK'].'</b></td>
           </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td colspan="10"><br>&nbsp;</td>
</tr>
<tr>
    <td colspan="10"><br>&nbsp;</td>
</tr>

<tr>
	<td width="55%"><font size="9">&nbsp;</font></td>
    <td width="45%"><font size="9">'.$kota_ttd.',&nbsp;'.strftime('%d %B %Y').'<br><br>'.$jabatan_ttd.'<br><br><br><br><br>'.$nama_ttd.'<br>'.$nip_ttd.'</font></td>
  </tr>
</table>
';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('DAFTAR NILAI KUMULATIF '.strtoupper($nim_mhs).'.pdf', 'I');