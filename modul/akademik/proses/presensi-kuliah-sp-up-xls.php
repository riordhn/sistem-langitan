<?php
require('../../../config.php');
$db = new MyOracle();

$smt = $_GET['smt'];
switch ($smt){
case  'SP' : $str_smt = 'SEMESTER PENDEK';
		break;
case  'UP' : $str_smt = 'UJIAN PERBAIKAN';
		break;
}

$thn = $_GET['thn'];
$hari = $_GET['hari'];

$kdprodi = $_GET['prodi'];
$kueri = "select a.nm_jenjang, b.nm_program_studi, c.nm_fakultas from jenjang a, program_studi b, fakultas c 
where a.id_jenjang=b.id_jenjang and c.id_fakultas=b.id_fakultas and b.id_program_studi=$kdprodi";
$result = $db->Query($kueri)or die("salah kueri 22 ");
while($r = $db->FetchRow()) {
	$jenjang = $r[0];
	$prodi = $r[1];
	$fak = $r[2];
}

$id_kelas_mk = $_GET['xls'];
$kueri = "select mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas.nama_kelas, jadwal_hari.nm_jadwal_hari, 
ruangan.nm_ruangan, jadwal_jam.jam_mulai||':'||jadwal_jam.menit_mulai||' - '||jadwal_jam.jam_selesai||':'||jadwal_jam.menit_selesai as jam
from kelas_mk
left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk=kelas_mk.id_kurikulum_mk
left join mata_kuliah on mata_kuliah.id_mata_kuliah=kurikulum_mk.id_mata_kuliah
left join nama_kelas on nama_kelas.id_nama_kelas=kelas_mk.no_kelas_mk
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_hari on jadwal_hari.id_jadwal_hari=jadwal_kelas.id_jadwal_hari
left join jadwal_jam on jadwal_jam.id_jadwal_jam=jadwal_kelas.id_jadwal_jam
left join ruangan on ruangan.id_ruangan=jadwal_kelas.id_ruangan
where kelas_mk.id_kelas_mk=$id_kelas_mk and jadwal_hari.id_jadwal_hari=$hari";
$result = $db->Query($kueri)or die("salah kueri 25 ");
while($r = $db->FetchRow()) {
	$kode_ma = $r[0];
	$nama_ma = $r[1];
	$sks_ma = $r[2];
	$kelas_ma = $r[3];
	$hari_ma = $r[4];
	$ruang_ma = $r[5];
	$jam_ma = $r[6];
}

header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=PRESENSI KULIAH ".$str_smt." ".strtoupper($nama_ma)." ".strtoupper($kelas_ma)."".$nm_prodi.".xls");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data";

echo '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="17" align="center"><b>FAKULTAS '.strtoupper($fak).'</b></td>
  </tr>
  <tr>
    <td colspan="17" align="center"><b>'.strtoupper($jenjang)." - ".strtoupper($prodi).'</b></td>
  </tr>
  <tr>
    <td colspan="17" align="center"><b>PRESENSI PERKULIAHAN MAHASISWA '.strtoupper($str_smt).' '.$thn.'</b></td>
  </tr>
  <tr>
    <td colspan="17">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="12">'.$kode_ma.' - '.strtoupper($nama_ma).' ('.$sks_ma.' SKS) <br/>KELAS '.strtoupper($kelas_ma).'</td>
	<td colspan="5" rowspan="3">
	<table border="0" cellspacing="0" cellpadding="0">';

$kueri = "select case when pengguna.gelar_belakang is null then trim(pengguna.gelar_depan||' '||upper(pengguna.nm_pengguna)) 
else trim(pengguna.gelar_depan||' '||upper(pengguna.nm_pengguna)||', '||pengguna.gelar_belakang) end as nama_dsn, 
case when pengampu_mk.pjmk_pengampu_mk=1 then 'PJMA' else 'TIM' end as pjma
from kelas_mk
left join pengampu_mk on kelas_mk.id_kelas_mk = pengampu_mk.id_kelas_mk
left join dosen on pengampu_mk.id_dosen = dosen.id_dosen
left join pengguna on dosen.id_pengguna = pengguna.id_pengguna
where kelas_mk.id_kelas_mk=$id_kelas_mk
order by pengampu_mk.pjmk_pengampu_mk desc";
$result = $db->Query($kueri)or die("salah kueri 26 ");
while($r = $db->FetchRow()) {
	$nama_dsn = $r[0];
	$pjma = $r[1];
if($nama_dsn != ',') {
echo '
<tr>
	<td colspan="5">'.$nama_dsn.' ('.$pjma.')</td>
</tr>';
} else {
echo '
<tr>
	<td colspan="5">PJMA BELUM DITENTUKAN</td>
</tr>';
}
}
echo '
	</table>
	</td>
  </tr>
  <tr>
    <td colspan="12">'.strtoupper($hari_ma).' : '.$jam_ma.' <br/>RUANG : '.strtoupper($ruang_ma).'</td>
  </tr>
  <tr>
    <td colspan="17">&nbsp;</td>
  </tr>
</table>
<table cellspacing="0" cellpadding="4" border="1" width="100%">
<thead>
    <tr bgcolor="lightgrey">
      <td width="5%" align="center"><strong>NO.</strong></td>
	  <td width="7%" align="center"><strong>NIM</strong></td>
      <td width="18%" align="center"><strong>NAMA MAHASISWA</strong></td>
      <td width="5%" align="center"><strong>&nbsp;</strong></td>
	  <td width="5%" align="center"><strong>&nbsp;</strong></td>
	  <td width="5%" align="center"><strong>&nbsp;</strong></td>
	  <td width="5%" align="center"><strong>&nbsp;</strong></td>
	  <td width="5%" align="center"><strong>&nbsp;</strong></td>
	  <td width="5%" align="center"><strong>&nbsp;</strong></td>
	  <td width="5%" align="center"><strong>&nbsp;</strong></td>
	  <td width="5%" align="center"><strong>&nbsp;</strong></td>
	  <td width="5%" align="center"><strong>&nbsp;</strong></td>
	  <td width="5%" align="center"><strong>&nbsp;</strong></td>
	  <td width="5%" align="center"><strong>&nbsp;</strong></td>
	  <td width="5%" align="center"><strong>&nbsp;</strong></td>
	  <td width="5%" align="center"><strong>&nbsp;</strong></td>
	  <td width="5%" align="center"><strong>&nbsp;</strong></td>
    </tr>
</thead>';

$nomor=1;

$kueri = "select mahasiswa.nim_mhs, pengguna.nm_pengguna, cek_bayar
from mahasiswa
left join pengambilan_mk on mahasiswa.id_mhs=pengambilan_mk.id_mhs
left join pengguna on pengguna.id_pengguna=mahasiswa.id_pengguna
left join (
	select count(*) as cek_bayar, id_mhs 
	from pembayaran_sp where id_semester = $_REQUEST[idsmt] and id_status_pembayaran = 2
	group by id_mhs
) a on a.id_mhs = mahasiswa.id_mhs
where pengambilan_mk.id_kelas_mk=$id_kelas_mk and pengambilan_mk.status_apv_pengambilan_mk=1
and pengambilan_mk.id_semester=$_REQUEST[idsmt]
order by mahasiswa.nim_mhs asc";

$result = $db->Query($kueri)or die("salah kueri 27 ");
while($r = $db->FetchRow()) {
	$nim_mhs = $r[0];
	$nama_mhs = $r[1];
	$cek_bayar = $r[2];
	

	if($cek_bayar >= 1){
		$bgcolor = '  style="background-color:#000"';	
	}else{
		$bgcolor = ' style="background-color:#FFF"';
	}

echo '
    <tr>
      <td width="5%" align="center">'.$nomor.'</td>
	  <td width="7%" align="center">&nbsp;'.$nim_mhs.'</td>
      <td width="18%" align="left">'.strtoupper($nama_mhs).'</td>
      <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
    </tr>';
$nomor++;
}
echo '</table>';

?>