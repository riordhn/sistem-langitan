<?php
require('../../../config.php');
$db = new MyOracle();

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('Dosen Wali');
$pdf->SetSubject('Dosen Wali');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logo-".$nama_singkat.".gif";
$logo_size = "25";
$title = strtoupper($nama_pt);

$bulan['01'] = "Januari"; $bulan['02'] = "Pebruari"; $bulan['03'] = "Maret"; $bulan['04'] = "April"; $bulan['05'] = "Mei"; $bulan['06'] = "Juni"; $bulan['07'] = "Juli"; $bulan['08'] = "Agustus"; $bulan['09'] = "September"; $bulan['10'] = "Oktober";  $bulan['11'] = "Nopember";  $bulan['12'] = "Desember";


$kueri = "select a.nm_jenjang, b.nm_program_studi, f.nm_fakultas, f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas,f.id_fakultas
			from jenjang a, program_studi b, fakultas f where a.id_jenjang=b.id_jenjang and f.id_fakultas=b.id_fakultas and b.id_fakultas = '$_REQUEST[fak]'";
$result = $db->Query($kueri)or die("salah kueri 22 ");
while($r = $db->FetchRow()) {
	$jenjang = $r[0];
	$prodi = $r[1];
	$fak = $r[2];
	$alm_fak = $r[3];
	$pos_fak = $r[4];
	$tel_fak = $r[5];
	$fax_fak = $r[6];
	$web_fak = $r[7];
	$eml_fak = $r[8];
	$kd_fak = $r[9];
}
$kueri = "select nm_semester,tahun_ajaran from semester where id_semester='$_REQUEST[smt]'";
//echo $kueri;
$result = $db->Query($kueri)or die("salah kueri 20 ");
while($r = $db->FetchRow()) {
	$thn_ajaran = $r[1];
	$nm_semester = $r[0];
}

$dat="select nip_dosen, trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as dosen
					from dosen left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
					where dosen.id_dosen='$_REQUEST[cetak]'";
$result = $db->Query($dat)or die("salah kueri 20 ");
while($r = $db->FetchRow()) {
	$nm_dosen = $r[1];
	$nip_dosen = $r[0];
}




$content = "FAKULTAS ".strtoupper($fak)."\n".strtoupper($jenjang)." - ".strtoupper($prodi)."\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 40, 5);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 14);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage('P', 'A4');



$det="SELECT * FROM (select s1.nim_mhs,mhs,prodi,coalesce(s4.sks_skrg,0)as sks_skrg
					from
					(select mahasiswa.id_mhs,mahasiswa.nim_mhs,upper(pengguna.nm_pengguna) as mhs,
					upper(nm_jenjang)||' - '||upper(nm_program_studi) as prodi
					from mahasiswa
					left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
					left join dosen_wali on dosen_wali.id_mhs=mahasiswa.id_mhs
					left join program_studi on program_studi.id_program_studi=mahasiswa.id_program_studi
					left join jenjang on jenjang.id_jenjang=program_studi.id_jenjang
					where dosen_wali.id_dosen='$_REQUEST[cetak]' and mahasiswa.status_akademik_mhs in 
					(select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
					and dosen_wali.id_mhs in (select id_mhs from pengambilan_mk where id_semester='$_REQUEST[smt]')
					and dosen_wali.status_dosen_wali='1')s1
					
					left join 
					(select a.id_mhs,sum(d.kredit_semester) as sks_skrg
					from pengambilan_mk a
					left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
					left join mahasiswa m on a.id_mhs=m.id_mhs
					left join program_studi ps on m.id_program_studi=ps.id_program_studi
					where id_semester='$_REQUEST[smt]' and id_fakultas='$_REQUEST[fak]' and status_apv_pengambilan_mk=1
					group by a.id_mhs)s4 on s1.id_mhs=s4.id_mhs
					)
					GROUP BY NIM_MHS, MHS, PRODI, SKS_SKRG
					order by prodi,nim_mhs";
$result = $db->Query($det)or die("salah kueri 20 ");

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="5" align="center"><b><u>DAFTAR ANAK WALI DOSEN '.$nm_dosen.'('.$nip_dosen.') <br />Semester '.strtoupper($nm_semester).' '.$thn_ajaran.'</u></b></td>
  </tr>
  <tr>
    <td colspan="5">&nbsp;</td>
  </tr>
</table>


<table width="100%" border="1" cellspacing="0" cellpadding="4">
  <tr>
    <th width="5%">No</th>
	<th width="15%">NIM</th>
	<th width="40%">Nama</th>
	<th width="40%">Program Studi</th>
  </tr>';
  $no = 1;
while($r = $db->FetchRow()) {
	$html .= '<tr>
				<td>'.$no++.'</td>
				<td>'.$r[0].'</td>
				<td>'.$r[1].'</td>
				<td>'.$r[2].'</td>
			</tr>';
}

$html .= '
</table>';


// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('dosen-wali.pdf', 'I');

?>