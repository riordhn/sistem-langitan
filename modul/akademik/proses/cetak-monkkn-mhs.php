<?php
require('../../../config.php');
$db = new MyOracle();

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('KKN MHS');
$pdf->SetSubject('KKN MHS');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
$logo = "../../img/akademik_images/logo-".$nama_singkat.".gif";
$logo_size = "25";
$title = strtoupper($nama_pt);

$kd_fak = $_GET['fak'];
$id_semester = $_GET['smt'];

$mhs = $db->QueryToArray("select id_pengambilan_mk,s1.nim_mhs,nm_pengguna,kel,agama,status,prodi,SKS_diperoleh,sks_sem, (SKS_diperoleh+sks_sem) as ttl,s1.smt,s2.IPK_MHS from
				(select id_pengambilan_mk,nim_mhs,nm_pengguna,nm_jenjang||'-'||coalesce(nm_singkat_prodi,nm_program_studi) as prodi,
				mahasiswa.status,case when pengguna.kelamin_pengguna='1' then 'L' else 'P' end as kel,upper(nm_agama) as agama,thn_akademik_semester||'-'||nm_semester as smt
				from pengambilan_mk
				left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
				left join semester on pengambilan_mk.id_semester=semester.id_semester
				left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
				left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
				left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
				left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
				left join agama on pengguna.id_agama=agama.id_agama
				where kurikulum_mk.status_mkta='2' and id_fakultas=$kd_fak and pengambilan_mk.id_semester=$id_semester
				order by nm_jenjang,nm_program_studi,nim_mhs)s1
				left join
				(select nim_mhs, sum(kredit_semester) as SKS_diperoleh, 
				round((sum((kredit_semester * nilai_standar_nilai)) / sum(kredit_semester)), 2) as IPK_MHS
				from 
				(select m.nim_mhs, c.kd_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai,
				row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by nilai_huruf) rangking
				from pengambilan_mk a
				join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
				join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
				join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
				join mahasiswa m on a.id_mhs=m.id_mhs
				join program_studi ps on m.id_program_studi=ps.id_program_studi
				where a.status_apv_pengambilan_mk = 1 and (a.nilai_huruf<>'E' or a.nilai_huruf<>null) and ps.id_fakultas=$kd_fak and a.status_hapus=0 
				and a.status_pengambilan_mk !=0 and a.id_semester !=$id_semester)
				where rangking=1
				group by nim_mhs) s2 on s1.nim_mhs=s2.nim_mhs
				left join 
				(select m.nim_mhs,sum(d.kredit_semester) as sks_sem
				from pengambilan_mk a
				left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
				left join mahasiswa m on a.id_mhs=m.id_mhs
				left join program_studi ps on m.id_program_studi=ps.id_program_studi
				left join semester s on a.id_semester=s.id_semester
				where a.id_Semester=$id_semester
				and a.status_apv_pengambilan_mk='1' and ps.id_fakultas=$kd_fak and a.status_hapus=0 
				and a.status_pengambilan_mk !=0
				group by m.nim_mhs
				)s3 on s1.nim_mhs=s3.nim_mhs");




$biomhs="select j.nm_jenjang, f.id_fakultas, f.alamat_fakultas, ps.nm_program_studi, f.nm_fakultas, 
			f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas
			from program_studi ps
			left join fakultas f on ps.id_fakultas=f.id_fakultas
			left join jenjang j on ps.id_jenjang=j.id_jenjang
			where f.id_fakultas='".$kd_fak."'";
$result1 = $db->Query($biomhs)or die("salah kueri 2 ");
while($r1 = $db->FetchRow()) {
	$nmjenjang = $r1[0];
	$fak = $r1[4];
	$alm_fak = $r1[2];
	$pos_fak = $r1[5];
	$tel_fak = $r1[6];
	$fax_fak = $r1[7];
	$web_fak = $r1[8];
	$eml_fak = $r1[9];
}

$content = "FAKULTAS ".strtoupper($fak)."\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

//set margins
$pdf->SetMargins(5, 35, 5);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default footer
//$pdf->setPrintFooter(false);
//$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 18);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('times', '', 8);

// add a page
$pdf->AddPage('P', 'A4');

//$pdf->Image('../includes/logo_unair.png', 100, 50, 100, '', '', '', '', false, 72);

// set the starting point for the page content
//$pdf->setPageMark();



$html = '
<table width="110%" cellpadding="2" border="0.5">
	<tr bgcolor="green" style="color:#fff;">
		<th style="text-align:center" width="3%">NO</th>
		<th style="text-align:center" width="8%">NIM</th>
		<th style="text-align:center" width="25%">NAMA</th>
		<th style="text-align:center" width="15%">PRODI</th>
        <th style="text-align:center" width="3%">L/P</th>
		<th style="text-align:center" width="9%">Agama</th>
        <th style="text-align:center" width="5%">Status</th>
		<th style="text-align:center" width="4%">SKS</th>
		<th style="text-align:center" width="4%">SKS SMT</th>
		<th style="text-align:center" width="4%">TTL SKS</th>
		<th style="text-align:center" width="4%">IPK</th>
        <th style="text-align:center" width="8%">THN SMT</th>
	</tr>';
	$no = 1;

foreach($mhs as $data){
$html .= '<tr>
			<td>'.$no++.'</td>
			<td>'.$data['NIM_MHS'].'</td>
			<td>'.$data['NM_PENGGUNA'].'</td>
			<td>'.$data['PRODI'].'</td>
			<td>'.$data['KEL'].'</td>
			<td>'.$data['AGAMA'].'</td>
			<td>'.$data['STATUS'].'</td>
			<td>'.$data['SKS_DIPEROLEH'].'</td>
            <td>'.$data['SKS_SEM'].'</td>
            <td>'.$data['TTL'].'</td>
			<td>'.$data['IPK_MHS'].'</td>
            <td>'.$data['SMT'].'</td>
	</tr>';
	}
$html .= '</table>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('daftar-kkn-mhs.pdf', 'I');

?>