<?php
require('../../../config.php');
$db = new MyOracle();

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

$id_pt = $id_pt_user;


// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sistem Langitan');
$pdf->SetTitle('KHS');
$pdf->SetSubject('KHS');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logo-".$nama_singkat.".gif";
$logo_size = "25";
$title = strtoupper($nama_pt);

include '../includes/function.php';
$var = decode($_SERVER['REQUEST_URI']);
$nim = $var['nim'];
$id_smt = $var['smt'];
$tgl_cetak = $var['tgl_cetak'];

$tipe_smt="select upper(nm_semester) as nm_semester,fd_id_smt,case when 
(group_semester='Ganjil' and tipe_semester in ('UP','REG','RD')) then thn_akademik_semester||1 else
case when (group_semester='Genap' and tipe_semester in ('UP','REG','RD')) then thn_akademik_semester||2 else
thn_akademik_semester||4 end end
from semester where id_semester='".$id_smt."'";
$result = $db->Query($tipe_smt)or die("salah kueri 0 ");
while($r = $db->FetchRow()) {
	$pendek = $r[0];
	$fd_id_smt = $r[1];
	$tahun_hitung = $r[2];
}

$thnsmt="select 'KARTU HASIL STUDI TAHUN AJARAN '||tahun_ajaran||' SEMESTER '||upper(nm_semester) as nm_smt from semester where id_semester='".$id_smt."'";
$result = $db->Query($thnsmt)or die("salah kueri 1 ");
while($r = $db->FetchRow()) {
	$nm_smt = $r[0];
}

$biomhs="select mhs.nim_mhs, upper(p.nm_pengguna) as nm_pengguna, j.nm_jenjang, ps.nm_program_studi, mhs.status_akademik_mhs, 
			 'FAKULTAS '||f.nm_fakultas as nm_fakultas, 
			f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas, f.id_fakultas,
			mhs.id_program_studi,mhs.id_mhs
			from mahasiswa mhs 
			left join pengguna p on mhs.id_pengguna=p.id_pengguna and p.id_perguruan_tinggi = '{$id_pt}'
			left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
			left join fakultas f on ps.id_fakultas=f.id_fakultas
			left join jenjang j on ps.id_jenjang=j.id_jenjang
			where mhs.nim_mhs='".$nim."'";
$result1 = $db->Query($biomhs)or die("salah kueri 2 ");
while($r1 = $db->FetchRow()) {
	$nim_mhs = $r1[0];
	$nm_mhs = $r1[1];
	$jenjang = $r1[2];
	$prodi = $r1[3];
	$status = $r1[4];
	$fak = $r1[5];
	$alm_fak = $r1[6];
	$pos_fak = $r1[7];
	$tel_fak = $r1[8];
	$fax_fak = $r1[9];
	$web_fak = $r1[10];
	$eml_fak = $r1[11];
	$kd_fak = $r1[12];
	$kd_prodi = $r1[13];
	$id_mhs=$r1[14];
}

$nm_doli1="select case when p.gelar_belakang is not null then 
trim(p.gelar_depan||' '||upper(p.nm_pengguna)||', '||p.gelar_belakang) else 
trim(p.gelar_depan||' '||upper(p.nm_pengguna)) end as nm_dosen, d.nip_dosen
from pengguna p
left join dosen d on p.id_pengguna=d.id_pengguna 
left join dosen_wali dw on d.id_dosen=dw.id_dosen
left join mahasiswa m on dw.id_mhs=m.id_mhs 
where m.nim_mhs='".$nim."' and dw.status_dosen_wali=1 and p.id_perguruan_tinggi = '{$id_pt}' and rownum=1
order by dw.id_dosen_wali desc";
$result7 = $db->Query($nm_doli1)or die("salah kueri 8 ");
while($r7 = $db->FetchRow()) {
	$nm_doli2 = $r7[0];
	$nip_doli2 = $r7[1];
}

$content = strtoupper($fak)."\n".strtoupper($jenjang)." - ".strtoupper($prodi)."\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 40, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default footer
$pdf->setPrintFooter(false);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage('P', 'A4');


// set the starting point for the page content
$pdf->setPageMark();

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="6" align="center"><b><u>'.$nm_smt.'</u></b></td>
  </tr>
  <tr>
    <td colspan="6">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="17%">NIM</td>
	<td width="3%"><center>:</center></td>
	<td width="80%">'.$nim_mhs.'</td>
  </tr>
  <tr>
    <td>Nama Mahasiswa</td>
	<td><center>:</center></td>
	<td>'.strtoupper($nm_mhs).'</td>
  </tr>
  <tr>
    <td>Dosen Wali</td>
	<td><center>:</center></td>
	<td>'.$nm_doli2.'</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="6">&nbsp;</td>
  </tr>
</table>
<table cellspacing="0" cellpadding="0" border="1" width="100%">
<thead>
    <tr bgcolor="black" style="color:#fff;">
      <td width="5%" align="center"><strong>NO.</strong></td>
	  <td width="10%" align="center"><strong>KODE MA</strong></td>
      <td width="64%" align="center"><strong>NAMA MATA AJAR</strong></td>
      <td width="5%" align="center"><strong>SKS</strong></td>
	  <td width="8%" align="center"><strong>NILAI</strong></td>
	  <td width="8%" align="center"><strong>BOBOT</strong></td>
    </tr>
</thead>';

if($pendek == 'PENDEK'){
/*$kueri = "select kd_mata_kuliah as kode,upper(nm_mata_kuliah) as nama,nama_kelas,tipe_semester,
kredit_semester as sks,nilai_huruf as nilai,bobot,bobot_total
from(
select pengambilan_mk.id_mhs,MATA_KULIAH.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,tipe_semester,
KURIKULUM_MK.kredit_semester,case when kurikulum_mk.status_mkta='1' 
and (nilai_huruf='E' or nilai_huruf is null) and id_fakultas=7 then 'T' 
when nilai_huruf is null then 'E' else nilai_huruf end as nilai_huruf,
case when standar_nilai.nilai_standar_nilai is null then 0 else standar_nilai.nilai_standar_nilai end as bobot,
coalesce(standar_nilai.nilai_standar_nilai*kurikulum_mk.kredit_semester,0) as bobot_total,
row_number() over(partition by pengambilan_mk.id_mhs,nm_mata_kuliah order by nilai_huruf) rangking
from PENGAMBILAN_MK
left join kelas_mk on PENGAMBILAN_MK.id_kelas_mk=KELAS_MK.id_kelas_mk
left join NAMA_KELAS on KELAS_MK.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk
left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
left join semester on pengambilan_mk.id_semester=semester.id_semester
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
left join standar_nilai on pengambilan_mk.nilai_huruf=standar_nilai.nm_standar_nilai
where id_Semester='".$id_smt."' and status_apv_pengambilan_mk=1 and PENGAMBILAN_MK.status_hapus=0 and PENGAMBILAN_MK.status_pengambilan_mk !=0
and mahasiswa.nim_mhs='".$nim."') 
where rangking=1";*/

$kueri = "SELECT 
					kd_mata_kuliah AS kode,
					upper(nm_mata_kuliah) AS nama,
					nama_kelas,
					tipe_semester,
					kredit_semester AS sks,
					CASE WHEN flagnilai = 1 THEN nilai_huruf ELSE '-' END AS nilai,
					bobot,
					bobot_total,
					nilai_angka AS akhir,
					id_pengambilan_mk
				FROM (
					SELECT 
						pengambilan_mk.id_mhs,
						/* Kode MK */
				        COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
				        /* Nama MK */
				        COALESCE(mk1.nm_mata_kuliah, mk2.nm_mata_kuliah) AS nm_mata_kuliah,
				        /* SKS */
				        COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS kredit_semester,
						nama_kelas,
						tipe_semester,

						/* Nilai Huruf, cek status Tugas Akhir */
						CASE 
						  WHEN kurikulum_mk.status_mkta = '1' AND (nilai_huruf = 'E' OR nilai_huruf IS NULL) /* AND id_fakultas = 7 */ THEN 'T'
						  WHEN nilai_huruf IS NULL THEN 'E' 
						  ELSE nilai_huruf
						END AS nilai_huruf,

						pengambilan_mk.nilai_angka,

						/* Bobot = Standar nilai */
						CASE
						  WHEN standar_nilai.nilai_standar_nilai IS NULL THEN 0
						  ELSE standar_nilai.nilai_standar_nilai
						END AS bobot,

						/* Bobot total = Standar Nilai x SKS */
						COALESCE(standar_nilai.nilai_standar_nilai * kurikulum_mk.kredit_semester, 0) AS bobot_total,

						/* Ranking group by id_mhs-kode mata kuliah */
						row_number() OVER(PARTITION BY pengambilan_mk.id_mhs, kd_mata_kuliah ORDER BY nilai_huruf) AS rangking,

						pengambilan_mk.id_pengambilan_mk,
						pengambilan_mk.flagnilai
					FROM pengambilan_mk
					JOIN mahasiswa			ON pengambilan_mk.id_mhs = mahasiswa.id_mhs
					JOIN program_studi		ON mahasiswa.id_program_studi = program_studi.id_program_studi
					JOIN semester			ON pengambilan_mk.id_semester = semester.id_semester
					-- Via Kelas
					LEFT JOIN kelas_mk		ON pengambilan_mk.id_kelas_mk = kelas_mk.id_kelas_mk
		    	LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kelas_mk.id_mata_kuliah
					LEFT JOIN nama_kelas	ON kelas_mk.no_kelas_mk = nama_kelas.id_nama_kelas
					-- Via Kurikulum
					LEFT JOIN kurikulum_mk	ON pengambilan_mk.id_kurikulum_mk = kurikulum_mk.id_kurikulum_mk
					LEFT JOIN mata_kuliah mk2 ON kurikulum_mk.id_mata_kuliah = mk2.id_mata_kuliah
					-- Penyetaraan
				    LEFT JOIN mk_setara mks ON mks.id_mata_kuliah = COALESCE(mk1.id_mata_kuliah, mk2.id_mata_kuliah) AND mks.is_deleted = 0
				    LEFT JOIN mk_penyetaraan mkp ON mkp.id_mk_penyetaraan = mks.id_mk_penyetaraan and mkp.is_deleted = 0

					LEFT JOIN standar_nilai	ON pengambilan_mk.nilai_huruf = standar_nilai.nm_standar_nilai
					WHERE 
						group_semester || thn_akademik_semester IN (SELECT group_semester || thn_akademik_semester FROM semester WHERE id_semester = '" . $id_smt . "') AND 
						tipe_semester = 'SP' AND
						status_apv_pengambilan_mk = 1 AND
						pengambilan_mk.status_hapus = 0 AND
						pengambilan_mk.status_pengambilan_mk != 0 AND
						mahasiswa.id_mhs = '" . $id_mhs . "')
				WHERE rangking = 1";

} else {
/*$kueri = "select kd_mata_kuliah as kode,upper(nm_mata_kuliah) as nama,nama_kelas,tipe_semester,
kredit_semester as sks,nilai_huruf as nilai,bobot,bobot_total
from(
select pengambilan_mk.id_mhs,MATA_KULIAH.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,tipe_semester,
KURIKULUM_MK.kredit_semester,case when kurikulum_mk.status_mkta='1' 
and (nilai_huruf='E' or nilai_huruf is null) and id_fakultas=7 then 'T' 
when nilai_huruf is null then 'E' else nilai_huruf end as nilai_huruf,
case when standar_nilai.nilai_standar_nilai is null then 0 else standar_nilai.nilai_standar_nilai end as bobot,
coalesce(standar_nilai.nilai_standar_nilai*kurikulum_mk.kredit_semester,0) as bobot_total,
row_number() over(partition by pengambilan_mk.id_mhs,nm_mata_kuliah order by nilai_huruf) rangking
from PENGAMBILAN_MK
left join kelas_mk on PENGAMBILAN_MK.id_kelas_mk=KELAS_MK.id_kelas_mk
left join NAMA_KELAS on KELAS_MK.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk
left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
left join semester on pengambilan_mk.id_semester=semester.id_semester
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
left join standar_nilai on pengambilan_mk.nilai_huruf=standar_nilai.nm_standar_nilai
where  group_semester||thn_akademik_semester in
(select group_semester||thn_akademik_semester from semester where id_Semester='".$id_smt."')
and tipe_semester in ('UP','REG','RD')
and status_apv_pengambilan_mk=1 and PENGAMBILAN_MK.status_hapus=0 and PENGAMBILAN_MK.status_pengambilan_mk !=0
and mahasiswa.nim_mhs='".$nim."') 
where rangking=1";*/

$kueri = "SELECT 
					kd_mata_kuliah AS kode,
					upper(nm_mata_kuliah) AS nama,
					nama_kelas,
					tipe_semester,
					kredit_semester AS sks,
					/* Status Belum Tampil */
					CASE WHEN flagnilai = 1 THEN nilai_huruf ELSE '*BT' END AS nilai,
					bobot,
					bobot_total,
					nilai_angka AS akhir,
					id_pengambilan_mk
				FROM (
					SELECT 
						pengambilan_mk.id_mhs,
						/* Kode MK */
				        COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
				        /* Nama MK */
				        COALESCE(mk1.nm_mata_kuliah, mk2.nm_mata_kuliah) AS nm_mata_kuliah,
				        /* SKS */
				        COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS kredit_semester,
						nama_kelas,
						tipe_semester,

						CASE 
						  WHEN pengambilan_mk.nilai_huruf IS NULL THEN '*K' 
						  ELSE pengambilan_mk.nilai_huruf
						END AS nilai_huruf,

						pengambilan_mk.nilai_angka,

						/* Bobot */
						CASE
						  WHEN standar_nilai.nilai_standar_nilai IS NULL THEN 0
						  ELSE standar_nilai.nilai_standar_nilai
						END AS bobot,

						/* Bobot Total = Standar Nilai x SKS */
						COALESCE(standar_nilai.nilai_standar_nilai * kurikulum_mk.kredit_semester, 0) AS bobot_total,

						/* Ranking berdasar kode */
						row_number() OVER(PARTITION BY pengambilan_mk.id_mhs, COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) ORDER BY nilai_huruf) AS rangking,

						pengambilan_mk.id_pengambilan_mk,
						pengambilan_mk.flagnilai
					FROM pengambilan_mk
					JOIN mahasiswa			ON pengambilan_mk.id_mhs = mahasiswa.id_mhs
					JOIN semester			ON pengambilan_mk.id_semester = semester.id_semester
					JOIN program_studi		ON mahasiswa.id_program_studi = program_studi.id_program_studi
					-- Via Kelas
					LEFT JOIN kelas_mk		ON pengambilan_mk.id_kelas_mk = kelas_mk.id_kelas_mk
		    		LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kelas_mk.id_mata_kuliah
					LEFT JOIN nama_kelas	ON kelas_mk.no_kelas_mk = nama_kelas.id_nama_kelas
					-- Via Kurikulum
					LEFT JOIN kurikulum_mk	ON pengambilan_mk.id_kurikulum_mk = kurikulum_mk.id_kurikulum_mk
					LEFT JOIN mata_kuliah mk2 ON kurikulum_mk.id_mata_kuliah = mk2.id_mata_kuliah
					-- Penyetaraan
					LEFT JOIN mk_setara mks ON mks.id_mata_kuliah = COALESCE(mk1.id_mata_kuliah, mk2.id_mata_kuliah) AND mks.is_deleted = 0
					LEFT JOIN mk_penyetaraan mkp ON mkp.id_mk_penyetaraan = mks.id_mk_penyetaraan and mkp.is_deleted = 0
					-- Bobot
					LEFT JOIN standar_nilai	ON pengambilan_mk.nilai_huruf = standar_nilai.nm_standar_nilai
					WHERE
						group_semester||thn_akademik_semester IN (SELECT group_semester||thn_akademik_semester FROM semester WHERE id_semester = '" . $id_smt . "') AND
						tipe_semester IN ('UP','REG') AND
						status_apv_pengambilan_mk = 1 AND 
						pengambilan_mk.status_hapus = 0 AND 
						pengambilan_mk.status_pengambilan_mk != 0 AND
						mahasiswa.id_mhs = '" . $id_mhs . "'
				)
				WHERE rangking = 1";

}

$nomor=1;
$result2 = $db->Query($kueri)or die("salah kueri 3 ");
while($r2 = $db->FetchRow()) {
	$kode_ma = $r2[0];
	$nama_ma = $r2[1];
	$sks_ma = $r2[4];
	$nilai_ma = $r2[5];
	$bobot_ma = $r2[6];
	$bobot_ttl = $r2[7];

$html .= '
    <tr style="color:#000;">
      <td width="5%" align="center">'.$nomor.'</td>
	  <td width="10%" align="center">'.$kode_ma.'</td>
      <td width="64%" align="left">&nbsp;&nbsp;'.strtoupper($nama_ma).'</td>
      <td width="5%" align="center">'.$sks_ma.'</td>
	  <td width="8%" align="center">'.$nilai_ma.'</td>
	  <td width="8%" align="center">'.$bobot_ttl.'</td>
    </tr>';
$nomor++;
}

if($pendek == 'PENDEK'){
/*$ipk="select sum(kredit_semester) as sks_total, sum((bobot*kredit_semester)) as bobot_total, 
case when sum(kredit_semester)=0 then 0 else round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ipk
from 
(
select kredit_semester, bobot
from(
select mata_kuliah.kd_mata_kuliah,case when (pengambilan_mk.nilai_angka <= 0 or pengambilan_mk.nilai_huruf = 'E' or pengambilan_mk.nilai_angka is null or pengambilan_mk.nilai_huruf is null) 
and kurikulum_mk.status_mkta in (1,2) then 0 else kurikulum_mk.kredit_semester end as kredit_semester,
case when standar_nilai.nilai_standar_nilai is null then 0 else standar_nilai.nilai_standar_nilai end as bobot,
row_number() over(partition by pengambilan_mk.id_mhs,kd_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk
left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join semester on pengambilan_mk.id_semester=semester.id_semester
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join standar_nilai on pengambilan_mk.nilai_huruf=standar_nilai.nm_standar_nilai
where id_semester='".$id_smt."' and status_apv_pengambilan_mk=1 and pengambilan_mk.status_hapus=0 and pengambilan_mk.status_pengambilan_mk !=0
and mahasiswa.nim_mhs='".$nim."') 
where rangking=1
)";*/
	$ips = "SELECT SUM(sks) AS sks_total, SUM((bobot*sks)) as bobot_total, round(SUM(nilai_mutu) / sum(sks), 2) as ips from (
					    SELECT
					        mhs.id_mhs,
					        /* Kode MK utk grouping total mutu */
					        COALESCE(mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
					        /* SKS */
					        COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS sks,
					        /* Bobot */
					        sn.nilai_standar_nilai AS bobot,
					        /* Nilai Mutu = SKS * Bobot */
					        COALESCE(mk1.kredit_semester, mk2.kredit_semester) * sn.nilai_standar_nilai AS nilai_mutu
					    FROM pengambilan_mk pmk
					    JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
					    JOIN semester S ON S.id_semester = pmk.id_semester
					    -- Via Kelas
					    LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
					    LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
					    -- Via Kurikulum
					    LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
					    LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
					    -- nilai bobot
					    JOIN standar_nilai sn ON sn.nm_standar_nilai = pmk.nilai_huruf
					    WHERE 
					        pmk.status_apv_pengambilan_mk = 1 AND 
					        mhs.id_mhs = '".$id_mhs."' AND 
					        S.id_semester = '".$id_smt."'
					)
					group by id_mhs";
} else {
/*$ipk="select sum(kredit_semester) as sks_total, sum((bobot*kredit_semester)) as bobot_total, 
case when sum(kredit_semester)=0 then 0 else round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ipk
from 
(
select kredit_semester, bobot
from(
select mata_kuliah.kd_mata_kuliah,case when ((pengambilan_mk.nilai_huruf in ('E') or pengambilan_mk.nilai_huruf is null) and kurikulum_mk.status_mkta in (1,2)) then 0 else kurikulum_mk.kredit_semester end as kredit_semester,
case when standar_nilai.nilai_standar_nilai is null then 0 else standar_nilai.nilai_standar_nilai end as bobot,
row_number() over(partition by pengambilan_mk.id_mhs,kd_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk
left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join semester on pengambilan_mk.id_semester=semester.id_semester
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join standar_nilai on pengambilan_mk.nilai_huruf=standar_nilai.nm_standar_nilai
where group_semester||thn_akademik_semester in
(select group_semester||thn_akademik_semester from semester where id_semester='".$id_smt."')
and tipe_semester in ('UP','REG','RD')
and status_apv_pengambilan_mk=1 and pengambilan_mk.status_hapus=0 and pengambilan_mk.status_pengambilan_mk !=0
and mahasiswa.nim_mhs='".$nim."') 
where rangking=1
)";*/
	$ips = 	"SELECT SUM(sks) AS sks_total, SUM((bobot*sks)) as bobot_total, round(SUM(nilai_mutu) / sum(sks), 2) as ips from (
					    SELECT
					        mhs.id_mhs,
					        /* Kode MK utk grouping total mutu */
					        COALESCE(mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
					        /* SKS */
					        COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS sks,
					        /* Bobot */
					        sn.nilai_standar_nilai AS bobot,
					        /* Nilai Mutu = SKS * Bobot */
					        COALESCE(mk1.kredit_semester, mk2.kredit_semester) * sn.nilai_standar_nilai AS nilai_mutu
					    FROM pengambilan_mk pmk
					    JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
					    JOIN semester S ON S.id_semester = pmk.id_semester
					    -- Via Kelas
					    LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
					    LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
					    -- Via Kurikulum
					    LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
					    LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
					    -- nilai bobot
					    JOIN standar_nilai sn ON sn.nm_standar_nilai = pmk.nilai_huruf
					    WHERE 
					        pmk.status_apv_pengambilan_mk = 1 AND 
					        mhs.id_mhs = '".$id_mhs."' AND 
					        S.id_semester = '".$id_smt."'
					)
					group by id_mhs";
}

$result3 = $db->Query($ips)or die("salah kueri 4 ");
while($r3 = $db->FetchRow()) {
	$sks_tot = $r3[0];
	$bobot_tot = $r3[1];
	$ips_tot = $r3[2];

	if(empty($ips_tot)){
		echo "Data ips belum terisi";
		exit();
	}

	$sks="select max(sks_maksimal) as sks_maksimal from beban_sks where id_program_studi = ".$kd_prodi." and ipk_minimum <= ".$ips_tot."";
$result5 = $db->Query($sks)or die("salah kueri 6 ");
while($r5 = $db->FetchRow()) {
	$sks_max = $r5[0];
}	
$html .= '
           <tr>
             <td colspan="3" align="right"><b>Total SKS dan Bobot&nbsp;&nbsp;</b></td>
             <td align="center"><b>'.$sks_tot.'</b></td>
			 <td></td>
			 <td align="center"><b>'.$bobot_tot.'</b></td>
           </tr>
           <tr>
			 <td colspan="3" align="right"><b>Indeks Prestasi Semester&nbsp;&nbsp;</b></td>
             <td colspan="3" align="center"><b>'.$ips_tot.'</b></td>
           </tr>
           <tr>
			 <td colspan="3" align="right"><b>SKS maksimal yang boleh diambil semester depan&nbsp;&nbsp;</b></td>
             <td colspan="3" align="center"><b>'.$sks_max.' SKS</b></td>
           </tr>';
}

$html .= '</table>';

/*
$info="select sum(sks) as jml_sks, round((sum((sks * bobot)) / sum(sks)), 2) as ipk
from
(select nim_mhs, kd_mata_kuliah as kode, nm_mata_kuliah as nama, kredit_semester as sks, min(nilai_huruf) as nilai, max(nilai_standar_nilai) as bobot
from 
(select m.nim_mhs, c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai
from pengambilan_mk a
join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
join mahasiswa m on a.id_mhs=m.id_mhs
where m.nim_mhs='".$nim."' and a.status_apv_pengambilan_mk = 1 and e.nilai_standar_nilai>0)
group by nim_mhs, kd_mata_kuliah, nm_mata_kuliah, kredit_semester)
group by nim_mhs";
*/


/*$info="select id_mhs, sum(kredit_semester) as jml_sks, 
round((sum((kredit_semester * nilai_standar_nilai)) / sum(kredit_semester)), 2) as ipk
from 
(select a.id_mhs, c.kd_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai,
case when 
(group_semester='Ganjil' and tipe_semester in ('UP','REG','RD')) then thn_akademik_semester||1 else
case when (group_semester='Genap' and tipe_semester in ('UP','REG','RD')) then thn_akademik_semester||2 else
thn_akademik_semester||4 end end as tahun,row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a
join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
join semester smt on a.id_semester=smt.id_semester
join mahasiswa m on a.id_mhs=m.id_mhs
where a.status_apv_pengambilan_mk = 1 and (a.nilai_huruf<>'E' or a.nilai_huruf<>null) and m.nim_mhs='".$nim."' and a.status_hapus=0 
and a.status_pengambilan_mk !=0)
where tahun<=$tahun_hitung and rangking=1
group by id_mhs";*/

$info = "SELECT id_mhs,
                        SUM(sks) AS total_sks, 
                        round(SUM(nilai_mutu) / SUM(sks), 2) AS ipk 
                    FROM (
                        SELECT
                            mhs.id_mhs,
                            /* Kode MK utk grouping total mutu */
                            COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
                            /* SKS */
                            COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS sks,
                            /* Nilai Mutu = SKS * Bobot */
                            COALESCE(mk1.kredit_semester, mk2.kredit_semester) * sn.nilai_standar_nilai AS nilai_mutu,
                            /* Urutan Nilai Terbaik */
                            row_number() OVER (PARTITION BY mhs.id_mhs, COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) ORDER BY sn.nilai_standar_nilai DESC) urut
                        FROM pengambilan_mk pmk
                        JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
                        JOIN semester S ON S.id_semester = pmk.id_semester
                        -- Via Kelas
                        LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
                        LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
                        -- Via Kurikulum
                        LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
                        LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
                        -- Penyetaraan
                        LEFT JOIN mk_setara mks ON mks.id_mata_kuliah = COALESCE(mk1.id_mata_kuliah, mk2.id_mata_kuliah) AND mks.is_deleted = 0
                        LEFT JOIN mk_penyetaraan mkp ON mkp.id_mk_penyetaraan = mks.id_mk_penyetaraan and mkp.is_deleted = 0
                        -- nilai bobot
                        JOIN standar_nilai sn ON sn.nm_standar_nilai = pmk.nilai_huruf
                        WHERE 
                            pmk.status_apv_pengambilan_mk = 1 AND 
                            mhs.id_mhs = '".$id_mhs."' AND 
                            S.fd_id_smt <= '".$fd_id_smt."'
                    )
                    GROUP BY id_mhs";

$result6 = $db->Query($info)or die("salah kueri 7 ");
while($r6 = $db->FetchRow()) {
	$sks_total = $r6[1];
	$ipk_total = $r6[2];
}
$html .= '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="6">&nbsp;<br/>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6">Tanpa mata ajar dengan nilai E, hasil studi sampai semester ini adalah :</td>
  </tr>
  <tr>
    <td colspan="6">JUMLAH SKS YANG TELAH DITEMPUH = '.$sks_total.', dengan IPK = '.$ipk_total.'</td>
  </tr>
</table>';


 
/*$doli="select case when p.gelar_belakang is not null then 
trim(p.gelar_depan||' '||upper(p.nm_pengguna)||', '||p.gelar_belakang) else 
trim(p.gelar_depan||' '||upper(p.nm_pengguna)) end as nm_dosen, d.nip_dosen
from pengguna p
left join dosen d on p.id_pengguna=d.id_pengguna 
left join program_studi ps on d.id_program_studi=ps.id_program_studi
left join fakultas f on ps.id_fakultas=f.id_fakultas 
where f.id_fakultas='".$kd_fak."' and d.id_jabatan_pegawai='6'";

$result4 = $db->Query($doli)or die("salah kueri 5 ");
while($r4 = $db->FetchRow()) {
	$nm_doli = $r4[0];
	$nip_doli = $r4[1];
}*/


$ttd_khs = 1;

// Request UNU Lampung pejabat khusus
if ($id_pt == 5) {
	$ttd_khs = 4;
}
// Request Umaha Kaprodi
elseif($id_pt == 1){
	$ttd_khs = 2;
}


if($ttd_khs == 1){
	$dosen_wali = "select b.nip_dosen,c.gelar_depan || ' ' || c.nm_pengguna || ', ' || c.gelar_belakang as nm_pengguna
					from dosen_wali a, dosen b, pengguna c, semester d
					where a.id_dosen=b.id_dosen and b.id_pengguna=c.id_pengguna 
					and a.id_semester=d.id_semester and a.id_mhs='".$id_mhs."' and a.status_dosen_wali=1
					order by d.thn_akademik_semester desc, d.nm_semester desc
					";
	$result = $db->Query($dosen_wali)or die ("salah kueri 48 ");

	$r = $db->FetchRow();
	$jabatan_ttd = "Dosen Wali";
	$nip_ttd = $r[0];
	$nama_ttd = $r[1];
}
elseif($ttd_khs == 2){
	$kaprodi="select d.nip_dosen, case when p.gelar_belakang is not null then 
				trim(p.gelar_depan||' '||upper(p.nm_pengguna)||', '||p.gelar_belakang) else 
				trim(p.gelar_depan||' '||upper(p.nm_pengguna)) end as nm_dosen
				from program_studi ps, pengguna p, dosen d
				where d.id_dosen=ps.id_kaprodi and ps.id_program_studi=".$kd_prodi." and p.id_pengguna=d.id_pengguna";
	$resultkaprodi = $db->Query($kaprodi)or die("salah kueri kaprodi ");

	$r = $db->FetchRow();
	$jabatan_ttd = "Kaprodi";
	$nip_ttd = $r[0];
	$nama_ttd = $r[1];
}
elseif($ttd_khs == 3){
	/*$dekan="select d.nip_dosen ,case when p.gelar_belakang is not null then 
				trim(p.gelar_depan||' '||upper(p.nm_pengguna)||', '||p.gelar_belakang) else 
				trim(p.gelar_depan||' '||upper(p.nm_pengguna)) end as nm_dosen, 
				case when f.id_fakultas=9 then 'Direktur' else 'Dekan' end as dekan,
				case when f.id_fakultas=9 then 'Wakil Direktur 1' else 'Wakil Dekan 1' end as wadek
				from fakultas f, pengguna p, dosen d
				where p.id_pengguna=f.id_wadek1 and f.id_fakultas=".$kd_fak." and p.id_pengguna=d.id_pengguna";
	$resultdekan = $db->Query($dekan)or die("salah kueri dekan ");*/

	$dekan="select d.nip_dosen ,case when p.gelar_belakang is not null then 
				trim(p.gelar_depan||' '||upper(p.nm_pengguna)||', '||p.gelar_belakang) else 
				trim(p.gelar_depan||' '||upper(p.nm_pengguna)) end as nm_dosen
				from fakultas f, pengguna p, dosen d
				where p.id_pengguna=f.id_wadek1 and f.id_fakultas=".$kd_fak." and p.id_pengguna=d.id_pengguna";
	$resultdekan = $db->Query($dekan)or die("salah kueri dekan ");
	
	$r = $db->FetchRow();
	$jabatan_ttd = "Wakil Dekan 1";
	$nip_ttd = $r[0];
	$nama_ttd = $r[1];
}
elseif($ttd_khs == 4){
	$baak = "select nvl(b.nidn_dosen,b.nip_dosen) nip,
				a.gelar_depan || ' ' || a.nm_pengguna || ', ' || a.gelar_belakang as nm_pengguna
				from pengguna a, dosen b, fakultas c
				where a.id_pengguna=b.id_pengguna and a.id_pengguna=c.id_kabag_akademik and c.id_fakultas='".$id_fakultas."'
				";
	$result = $db->Query($baak)or die ("salah kueri 48 ");

	$r = $db->FetchRow();
	$jabatan_ttd = "Kabag. Akademik";
	if($id_pt == 5){
		$jabatan_ttd = 'Kepala Biro Administrasi Akademik,<br/>Kemahasiswaan dan Sistem Informasi';
	}
	$nip_ttd = $r[0];
	$nama_ttd = $r[1];
}
elseif($ttd_khs == 5){
	// Pejabat yang tanda tangan KHS, masih manual, perlu di databasekan.
	$jabatan_ttd		= 'Kepala Biro Administrasi Akademik,<br/>Kemahasiswaan dan Sistem Informasi';
	$nip_ttd			= '';
	$nama_ttd			= 'Dr. Nur Syamsiah, S.Pd, M.Pd';
}



$bulan['01'] = "Januari"; $bulan['02'] = "Februari"; $bulan['03'] = "Maret"; $bulan['04'] = "April"; $bulan['05'] = "Mei"; $bulan['06'] = "juni"; $bulan['07'] = "Juli"; $bulan['08'] = "Agustus"; $bulan['09'] = "September"; $bulan['10'] = "Oktober";  $bulan['11'] = "November";  $bulan['12'] = "Desember";


/** Kode Fakultas 8 dimatikan. Fathoni 01-09-2018 */
/**
if($kd_fak==8){
$html .= '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="6">&nbsp;<br/><br/><br/>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><br><br><br>Lembar :<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. untuk mahasiswa<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2. untuk dosen wali<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3. untuk Program Studi</td>
	<td colspan="4" align="center">'.$kota_pt.', '.date("d").' '.$bulan[date("m")].' '.date("Y").'<br/>Dosen Wali,<br/><br/><br/><br/><br/><br/><u>'.$nm_doli2.'</u><br />'.$nip_doli2.'</td>
  </tr>
</table>';
}
else{
*/

	$html .= '
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="10">&nbsp;<br/><br/><br/>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3"><br><br><br>Lembar :<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. untuk mahasiswa<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2. untuk dosen wali<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3. untuk Program Studi</td>
					<td colspan="3" align="center"></td>
				<td colspan="4" align="center">'.$kota_pt.', '.$tgl_cetak.'<br/>'.$jabatan_ttd.',<br/><br/><br/><br/><br/><br/><u>'.$nama_ttd.'</u><br />'.$nip_ttd.'</td>
				</tr>
			</table>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('KHS-'.strtoupper($nim_mhs).'.pdf', 'I');

?>