<?php
require('../../../config.php');
$db = new MyOracle();

$smt = $_GET['smt'];
$prodi = $_GET['prodi'];
$nm_prodi = $_GET['nm_prodi'];

header("Content-type: application/vnd.msword");
header("Content-disposition: attachment; filename=pengampu-mk-".$nm_prodi.".doc");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data";

echo '
<table border="1" cellspacing="0" cellpadding="0">
           <tr>
             <td align="center"><b>No</b></td>
             <td align="center"><b>Kode MA</b></td>
			 <td align="center"><b>Nama MA</b></td>
             <td align="center"><b>SKS</b></td>
             <td align="center"><b>Kls</b></td>
			 <td align="center"><b>Tim Pengajar</b></td>
           </tr>';

$kueri="
select mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas.nama_kelas,
wm_concat(' '||nip_dosen||'-'||nidn_dosen||'-'||nm_pengguna) as tim
from kelas_mk 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk 
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
where kelas_mk.id_program_studi=$prodi and kelas_mk.id_semester=$smt 
group by mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas.nama_kelas
order by mata_kuliah.kd_mata_kuliah, nama_kelas.nama_kelas
";
$result = $db->Query($kueri)or die("salah kueri 01 ");
$i=1;
while($r = $db->FetchRow()) {
echo '
           <tr>
             <td>'.$i.'</td>
             <td>'.$r[0].'</td>
             <td>'.$r[1].'</td>
             <td align="center">'.$r[2].'</td>
             <td align="center">'.$r[3].'</td>
             <td>'.$r[4].'</td>
           </tr>';
$i++;
}
echo '
</table>';

?>