<?php 
//require('config.php');
require('../../../config.php');
$db = new MyOracle();

//$BOBOT=array();
//$BOBOT["E"]="0";
//$BOBOT["D"]="1";
//$BOBOT["C"]="2";
//$BOBOT["BC"]="2.5";
//$BOBOT["B"]="3";
//$BOBOT["AB"]="3.5";
//$BOBOT["A"]="4";


$idprodi=$_POST['prodi'];
$id_semester=$_POST['smt'];;
$fakultas = "FAKULTAS ".strtoupper($user->NM_FAKULTAS);
//echo $user->NM_FAKULTAS;exit;
//$idprodi=108;
//get semester
$sql="select NM_SEMESTER, TAHUN_AJARAN from semester where id_semester=$id_semester";
$db->Query($sql);
while($r=$db->FetchRow())
{
	$nm_semester = $r[0];
	$thn_semester = $r[1];
}

$sql="select id_fakultas, nm_jenjang from program_studi left join jenjang on jenjang.id_jenjang = program_studi.id_jenjang where id_program_studi={$idprodi}";
$db->Query($sql);
while($r=$db->FetchRow())
{
$idfakultas = $r[0];
$nm_jenjang = $r[1];
}
if($idfakultas !=8 ){
$sql="select c.username,c.nm_asli from fakultas a left join pengguna c on (c.id_pengguna=a.id_wadek1)
where a.id_fakultas='$idfakultas'";
$db->Query($sql);
while($r=$db->FetchRow())
{
$wadek1_nip = $r[0];
$wadek1_nama = $r[1];
}
}


require  "libs/tcetak1.php";

function getSKSMAX($ips)
{
	if($ips>=3)
	return 24;		
	elseif($ips>=2.5)
	return 22;
	elseif($ips>=2)
	return 20;
	elseif($ips>=1.5)
	return 18;
	else
	return 15;			
}



$sql="
select a.*,case when c.gelar_belakang is not null then 
trim(c.gelar_depan||' '||upper(c.nm_pengguna)||', '||c.gelar_belakang) else 
trim(c.gelar_depan||' '||upper(c.nm_pengguna)) end as nm_dosen,c.username from
(
select h.username,h.nm_pengguna,ipk.skstotal,ipk.ipk,ips.sksmtk,ips.ips,f.nm_program_studi,i.nm_jenjang,g.id_dosen,e.nm_mata_kuliah,e.kd_mata_kuliah,d.kredit_semester,a.nilai_huruf from
pengambilan_mk a 
left join mahasiswa b on a.id_mhs=b.id_mhs
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join program_studi f on b.id_program_studi=f.id_program_studi
left join (
select * from dosen_wali where status_dosen_wali=1 and id_mhs in (select id_mhs from mahasiswa where id_program_studi={$idprodi})
)
g on b.id_mhs=g.id_mhs
left join pengguna h on b.id_pengguna=h.id_pengguna
left join jenjang i on f.id_jenjang=i.id_jenjang
left join
(
select a.id_mhs,sum(d.kredit_semester) sksmtk,
round(sum(d.kredit_semester*(case a.nilai_huruf 
when 'A' then 4 
when 'AB' then 3.5
when 'B' then 3
when 'BC' then 2.5
when 'C' then 2
when 'D' then 1
when 'E' then 0
else 0
end))/sum(d.kredit_semester),2) IPS
from
pengambilan_mk a 
left join mahasiswa b on a.id_mhs=b.id_mhs
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join program_studi f on b.id_program_studi=f.id_program_studi
where a.ID_SEMESTER={$id_semester} and f.id_program_studi={$idprodi}
and a.status_apv_pengambilan_mk=1 and a.status_hapus=0 and a.status_pengambilan_mk !=0
group by a.id_mhs order by a.id_mhs
) ips on a.id_mhs=ips.id_mhs
left join
(
select a.id_mhs,sum(a.kredit_semester) skstotal,
round(sum(a.kredit_semester*(case a.nilai_huruf 
when 'A' then 4 
when 'AB' then 3.5
when 'B' then 3
when 'BC' then 2.5
when 'C' then 2
when 'D' then 1

end))/sum(a.kredit_semester),2) IPK
from
(

select a.id_mhs,e.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join mahasiswa b on a.id_mhs=b.id_mhs
left join program_studi f on b.id_program_studi=f.id_program_studi
where a.nilai_huruf<'E' and a.nilai_huruf is not null  
and a.status_apv_pengambilan_mk=1 and a.status_hapus=0 and a.status_pengambilan_mk !=0
and a.id_semester is not null and f.id_program_studi={$idprodi}
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where rangking=1 

) a 
left join mahasiswa b on a.id_mhs=b.id_mhs
left join program_studi f on b.id_program_studi=f.id_program_studi
where f.id_program_studi={$idprodi}
group by a.id_mhs order by a.id_mhs

) ipk on a.id_mhs=ipk.id_mhs
where f.id_program_studi={$idprodi} and a.id_semester={$id_semester}
group by a.id_mhs,h.username,h.nm_pengguna,ipk.skstotal,ipk.ipk,ips.sksmtk,ips.ips,f.nm_program_studi,i.nm_jenjang,g.id_dosen,e.nm_mata_kuliah,e.kd_mata_kuliah,d.kredit_semester,a.nilai_huruf
) a
left join dosen b on a.id_dosen=b.id_dosen
left join pengguna c on b.id_pengguna=c.id_pengguna
order by  a.nm_program_studi,a.username,a.kd_mata_kuliah
";


$db->Query($sql);
$i=0;
 	while($r=$db->FetchRow())
  	{
$i++;
$kdep=$r[6];

$dosenwali=$r[13];
$nim=$r[0];
$nmmhs=$r[1];
$nmprodi=$r[6];
$kdmtk=$r[10];
$nmmtk=$r[9];
$sksmtk=$r[11];
$nipdosen=$r[14];
$nilai=$r[12];
$ipk=$r[3];
$ips=$r[5];
$sksips=$r[4];

$jumsks=$r[2];
$CT[$kdep][$nim]['nama']=$nmmhs;
$CT[$kdep][$nim]['ips']=$ips;
$CT[$kdep][$nim]['sksips']=$sksips;
$CT[$kdep][$nim]['ipk']=$ipk;
$CT[$kdep][$nim]['jumsks']=$jumsks;
$CT[$kdep][$nim]['prodi']=$nmprodi;
$CT[$kdep][$nim]['nipdosen']=$nipdosen;
$CT[$kdep][$nim]['nmdosen']=$dosenwali;
$CT[$kdep][$nim]['khs'][$kdmtk]['nama']=$nmmtk;
$CT[$kdep][$nim]['khs'][$kdmtk]['sks']=$sksmtk;
$CT[$kdep][$nim]['khs'][$kdmtk]['nilai']=$nilai;

}
//var_export($CT);
//var_dump($CT);exit;
//$t->SetProtection(array('print'));

if($idfakultas =7 )
{
	$t = new tcetak1("P","mm",array(216,280));
	//$t = new tcetak1("L","mm",array(225,365));
	$t->AliasNbPages();
	$rek=0;
	//$tahun=2011;
	//$nmsem="GENAP";
	//$tahun1=2012;
	//print_r($CT);
	if($CT)
	foreach($CT as $kdep => $MHS)
	{

	$nmdep=$kdep;
	$t->AddPage();

	$t->Bookmark($nmdep);
	$t->Ln(15);

	$t->SetFont('Times','B',24);
	$t->Cell(0,15,"KARTU HASIL STUDI SEMESTER $nm_semester $thn_semester",0,1,'C');
	$t->Ln(80);

	$t->SetFont('Times','B',36);
	$t->Cell(0,15,$nmdep,0,1,'C');
	$t->Ln(80);
	$t->SetFont('Times','B',16);
	$t->Cell(0,10,$fakultas,0,1,'C');
	$t->SetFont('Times','B',18);
	$t->Cell(0,10,'UNIVERSITAS AIRLANGGA',0,1,'C');
	$t->Cell(0,10,date('Y'),0,1,'C');


	foreach($MHS as $nim => $datamhs)
	{
	$KHS=$datamhs['khs'];
	$nmmhs=$datamhs['nama'];
	$prodi=$datamhs['prodi'];
	$ipk=$datamhs['ipk'];
	$jumsks=$datamhs['jumsks'];
	$ips=$datamhs['ips'];
	$sksips=$datamhs['sksips'];
	$dosenwali=$datamhs['nmdosen'];
	$nipdosen=$datamhs['nipdosen'];
	$t->AddPage();
	$t->Bookmark($nim." ".$nmmhs,2,-1);
	$t->SetFont('Times','B',12);
	$t->Cell(0,6,'UNIVERSITAS AIRLANGGA',0,1,'L');
	$t->Ln(2);
	$t->SetFont('Times','',12);
	$t->Cell(0,6,$fakultas,'0',1,'L');
	$t->Ln(2);
	$t->Cell(0,6,$nm_jenjang.' - '.$prodi,'B',1,'L');
	$t->Ln(2);
	$t->SetFont('Times','BU',12);
	$t->Cell(0,6,"KARTU HASIL STUDI TAHUN AJARAN $thn_semester SEMESTER ".strtoupper($nm_semester)." ",0,1,'C');
	$t->Ln(5);
	$t->SetFont('Times','',11);
	$t->Cell(40,6," N I M",0,0,'L');
	$t->Cell(80,6,": $nim",0,0,'L');
	$t->Ln(2);
	$t->Cell(40,6," Nama Mahasiswa",0,0,'L');
	$t->Cell(80,6,": $nmmhs",0,0,'L');
	$t->Ln(2);
	$t->Cell(40,6," Dosen Wali",0,0,'L');
	$t->Cell(0,6,": $dosenwali",0,1,'L');
	$t->Ln(2);
	$t->Cell(30,10,"KODE MK",1,0,'L');
	$t->Cell(115,10,"MATA KULIAH",'TBR',0,'L');
	$t->Cell(25,10,"SKS",'TBR',0,'C');
	$t->Cell(25,10,"NILAI",'TBR',1,'C');


	$totalsks=0;
	$totalbobot=0;
	if($KHS)
	foreach($KHS as $kdmtk => $data)
	{

	$t->Cell(30,6,$kdmtk,'LR',0,'L');
	$t->Cell(115,6,$data['nama'],'R',0,'L');
	 $t->Cell(25,6,$data['sks'],'R',0,'C');
	$t->Cell(25,6,$data['nilai'],'R',1,'C');
	$totalsks+=$data['sks'];
	$totalbobot+=$data['sks']*$BOBOT[$data['nilai']];
	$t->Ln(2);
	 //

	}


	$sksmax=getSKSMAX($ips);
	$t->Cell(145,10,"IP SEMESTER INI : ".number_format($ips,2),1,0,'C');
	$t->Cell(25,10,$sksips,1,0,'C');
	$t->Cell(25,10,"",1,1,'C');
	$t->Cell(195,5,"",'LR',1,'C');
	$t->Cell(195,6,"Tanpa mata kuliah dengan nilai E, hasil studi sampai dengan semester ini ialah:",'LR',1,'L');

	$t->Cell(195,6,"JUMLAH SKS YANG TELAH DITEMPUH = $jumsks dengan IPK : ".number_format($ipk,2),'LR',1,'L');
	$t->Cell(195,6,"",'LR',1,'C');
	$t->Cell(115,6,"Catatan dosen wali :",'L',0,'L');
	$BULAN[1]="Januari";
	$BULAN[2]="Februari";
	$BULAN[3]="Maret";
	$BULAN[4]="April";
	$BULAN[5]="Mei";
	$BULAN[6]="Juni";
	$BULAN[7]="Juli";
	$BULAN[8]="Agustus";
	$BULAN[9]="September";
	$BULAN[10]="Oktober";
	$BULAN[11]="November";
	$BULAN[12]="Desember";

	$tanggal=date('j')." ".$BULAN[date('n')]." ".date('Y');
	$t->Cell(80,6,"Surabaya, $tanggal",'R',1,'C');
	$t->Cell(195,6,"Mengingat prestasi IPS Saudara = ".number_format($ips,2)." maka,",'LR',1,'L');
	$t->Cell(195,6,"beban SKS maksimum yang dapat diambil = $sksmax SKS",'LR',1,'L');
	$t->Cell(195,10,"",'LR',1,'L');
	$t->Cell(195,6,"Lembar: 1. untuk mahasiswa",'LR',1,'L');
	$t->Cell(195,6,"Lembar: 2. untuk dosen wali",'LR',1,'L');
	$t->Cell(115,6,"Lembar: 3. untuk prodi",'L',0,'L');
	$t->SetFont('Times','U',12);
	if($idfakultas !=8 )
	{	
	$t->Cell(80,6,"(  $wadek1_nama  )",'R',1,'C');
	$t->SetFont('Times','',12);
	$t->Cell(115,10,"",'L',0,'C');
	$t->Cell(80,10,"NIP.$wadek1_nip",'R',1,'C');
	}
	else{
	$t->Cell(80,6,"(  $dosenwali   )",'R',1,'C');
	$t->SetFont('Times','',12);
	$t->Cell(115,10,"",'L',0,'C');
	$t->Cell(80,10,"NIP.$nipdosen",'R',1,'C');

	}
	$t->Cell(195,10,"",'LBR',1,'C');
	$t->SetFont('Times','I',8);
	$rek++;
	$t->Cell(195,6,"cybercampus: rec no. $rek",0,1,'R');
	}
	}
} else
{

	$t = new tcetak1("P","mm",array(216,280));
	//$t = new tcetak1("L","mm",array(225,365));
	$t->AliasNbPages();
	$rek=0;
	//$tahun=2011;
	//$nmsem="GENAP";
	//$tahun1=2012;
	//print_r($CT);
	if($CT)
	foreach($CT as $kdep => $MHS)
	{

	$nmdep=$kdep;
	$t->AddPage();

	$t->Bookmark($nmdep);
	$t->Ln(15);

	$t->SetFont('Times','B',24);
	$t->Cell(0,15,"KARTU HASIL STUDI SEMESTER $nm_semester $thn_semester",0,1,'C');
	$t->Ln(80);

	$t->SetFont('Times','B',36);
	$t->Cell(0,15,$nmdep,0,1,'C');
	$t->Ln(80);
	$t->SetFont('Times','B',16);
	$t->Cell(0,10,$fakultas,0,1,'C');
	$t->SetFont('Times','B',18);
	$t->Cell(0,10,'UNIVERSITAS AIRLANGGA',0,1,'C');
	$t->Cell(0,10,date('Y'),0,1,'C');


	foreach($MHS as $nim => $datamhs)
	{
	$KHS=$datamhs['khs'];
	$nmmhs=$datamhs['nama'];
	$prodi=$datamhs['prodi'];
	$ipk=$datamhs['ipk'];
	$jumsks=$datamhs['jumsks'];
	$ips=$datamhs['ips'];
	$sksips=$datamhs['sksips'];
	$dosenwali=$datamhs['nmdosen'];
	$nipdosen=$datamhs['nipdosen'];
	$t->AddPage();
	$t->Bookmark($nim." ".$nmmhs,2,-1);
	$t->SetFont('Times','B',12);
	$t->Cell(0,6,'UNIVERSITAS AIRLANGGA',0,1,'L');
	$t->SetFont('Times','',12);
	$t->Cell(0,6,$fakultas,'0',1,'L');
	$t->Cell(0,6,$nm_jenjang.' - '.$prodi,'B',1,'L');
	$t->Ln(5);
	$t->SetFont('Times','BU',12);
	$t->Cell(0,6,"KARTU HASIL STUDI TAHUN AJARAN $thn_semester SEMESTER ".strtoupper($nm_semester)." ",0,1,'C');
	$t->Ln(10);
	$t->SetFont('Times','',11);
	$t->Cell(40,6," N I M",0,0,'L');
	$t->Cell(80,6,": $nim",0,0,'L');
	$t->Ln();
	$t->Cell(40,6," Nama Mahasiswa",0,0,'L');
	$t->Cell(80,6,": $nmmhs",0,0,'L');
	$t->Ln();
	$t->Cell(40,6," Dosen Wali",0,0,'L');
	$t->Cell(0,6,": $dosenwali",0,1,'L');
	$t->Ln(2);
	$t->Cell(30,10,"KODE MK",1,0,'L');
	$t->Cell(115,10,"MATA KULIAH",'TBR',0,'L');
	$t->Cell(25,10,"SKS",'TBR',0,'C');
	$t->Cell(25,10,"NILAI",'TBR',1,'C');


	$totalsks=0;
	$totalbobot=0;
	if($KHS)
	foreach($KHS as $kdmtk => $data)
	{

	$t->Cell(30,6,$kdmtk,'LR',0,'L');
	$t->Cell(115,6,$data['nama'],'R',0,'L');
	 $t->Cell(25,6,$data['sks'],'R',0,'C');
	$t->Cell(25,6,$data['nilai'],'R',1,'C');
	$totalsks+=$data['sks'];
	$totalbobot+=$data['sks']*$BOBOT[$data['nilai']];

	 //

	}


	$sksmax=getSKSMAX($ips);
	$t->Cell(145,10,"IP SEMESTER INI : ".number_format($ips,2),1,0,'C');
	$t->Cell(25,10,$sksips,1,0,'C');
	$t->Cell(25,10,"",1,1,'C');
	$t->Cell(195,5,"",'LR',1,'C');
	$t->Cell(195,6,"Tanpa mata kuliah dengan nilai E, hasil studi sampai dengan semester ini ialah:",'LR',1,'L');

	$t->Cell(195,6,"JUMLAH SKS YANG TELAH DITEMPUH = $jumsks dengan IPK : ".number_format($ipk,2),'LR',1,'L');
	$t->Cell(195,6,"",'LR',1,'C');
	$t->Cell(115,6,"Catatan dosen wali :",'L',0,'L');
	$BULAN[1]="Januari";
	$BULAN[2]="Februari";
	$BULAN[3]="Maret";
	$BULAN[4]="April";
	$BULAN[5]="Mei";
	$BULAN[6]="Juni";
	$BULAN[7]="Juli";
	$BULAN[8]="Agustus";
	$BULAN[9]="September";
	$BULAN[10]="Oktober";
	$BULAN[11]="November";
	$BULAN[12]="Desember";

	$tanggal=date('j')." ".$BULAN[date('n')]." ".date('Y');
	$t->Cell(80,6,"Surabaya, $tanggal",'R',1,'C');
	$t->Cell(195,6,"Mengingat prestasi IPS Saudara = ".number_format($ips,2)." maka,",'LR',1,'L');
	$t->Cell(195,6,"beban SKS maksimum yang dapat diambil = $sksmax SKS",'LR',1,'L');
	$t->Cell(195,10,"",'LR',1,'L');
	$t->Cell(195,6,"Lembar: 1. untuk mahasiswa",'LR',1,'L');
	$t->Cell(195,6,"Lembar: 2. untuk dosen wali",'LR',1,'L');
	$t->Cell(115,6,"Lembar: 3. untuk prodi",'L',0,'L');
	$t->SetFont('Times','U',12);
	if($idfakultas !=8 )
	{	
	$t->Cell(80,6,"(  $wadek1_nama  )",'R',1,'C');
	$t->SetFont('Times','',12);
	$t->Cell(115,10,"",'L',0,'C');
	$t->Cell(80,10,"NIP.$wadek1_nip",'R',1,'C');
	}
	else{
	$t->Cell(80,6,"(  $dosenwali   )",'R',1,'C');
	$t->SetFont('Times','',12);
	$t->Cell(115,10,"",'L',0,'C');
	$t->Cell(80,10,"NIP.$nipdosen",'R',1,'C');

	}
	$t->Cell(195,10,"",'LBR',1,'C');
	$t->SetFont('Times','I',8);
	$rek++;
	$t->Cell(195,6,"cybercampus: rec no. $rek",0,1,'R');
	}
	}
}

$t->SetDisplayMode(50);
$t->Output();

?>







