<?php
require('../../../config.php');
$db = new MyOracle();

$smt = $_GET['smt'];
$prodi = $_GET['prodi'];
$nm_prodi = $_GET['nm_prodi'];
$thn = $_GET['tahun'];
$thnakd = $_GET['tahunakd'];

header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=REKAP-KHS-".$thnakd."-".$nm_prodi.".xls");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data";

echo '
<table border="1" cellspacing="0" cellpadding="0">
           <tr>
             <td bgcolor="lightgrey" align="center"><b>No</b></td>
             <td bgcolor="lightgrey" align="center"><b>Nim</b></td>
             <td bgcolor="lightgrey" align="center"><b>Nama Mahasiswa</b></td>
             <td bgcolor="lightgrey" align="center"><b>Kode MA</b></td>
			 <td bgcolor="lightgrey" align="center"><b>Nama MA</b></td>
             <td bgcolor="lightgrey" align="center"><b>Kls</b></td>
             <td bgcolor="lightgrey" align="center"><b>Sks</b></td>
			 <td bgcolor="lightgrey" align="center"><b>Smt</b></td>
			 <td bgcolor="lightgrey" align="center"><b>Nilai</b></td>
           </tr>';

/*
$kueri="select s1.nim_mhs,s1.nm_pengguna,coalesce(s1.nm_status_pengguna,'AKTIF') as nm_status_pengguna,
s2.kd_mata_kuliah,s2.nm_mata_kuliah,s2.nama_kelas,s2.kredit_semester,s2.nilai_huruf from 
(select mahasiswa.id_mhs,nim_mhs,nm_pengguna,nm_status_pengguna from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join admisi on mahasiswa.id_mhs=admisi.id_mhs and admisi.id_semester=$smt
left join status_pengguna on admisi.status_akd_mhs=status_pengguna.id_status_pengguna
where id_program_studi=$prodi and thn_angkatan_mhs<=$thn
and mahasiswa.id_mhs not in
(select id_mhs from 
(select id_mhs,case when nm_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,status_akd_mhs
from admisi
left join semester on admisi.id_semester=semester.id_semester)
where tahun<=$thnakd  and status_akd_mhs not in (1,3))
)s1
left join 
(select pengambilan_mk.id_mhs,MATA_KULIAH.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,
KURIKULUM_MK.kredit_semester,case when kurikulum_mk.status_mkta='1' and (nilai_huruf='E' or nilai_huruf is null) then 'T' else nilai_huruf end as nilai_huruf
from PENGAMBILAN_MK
left join kelas_mk on PENGAMBILAN_MK.id_kelas_mk=KELAS_MK.id_kelas_mk
left join NAMA_KELAS on KELAS_MK.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk
left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
where PENGAMBILAN_MK.id_semester=$smt and status_apv_pengambilan_mk=1 and PENGAMBILAN_MK.status_hapus=0
and id_mhs in (select id_mhs from mahasiswa where id_program_studi=$prodi)
)s2 on s1.id_mhs=s2.id_mhs
where s1.nm_status_pengguna is not null or (s1.nm_status_pengguna is null and kd_mata_kuliah is not null)
order by s1.nim_mhs";
*/

/*
$kueri="select s1.nim_mhs,s1.nm_pengguna,coalesce(s1.nm_status_pengguna,'AKTIF') as nm_status_pengguna,
s2.kd_mata_kuliah,s2.nm_mata_kuliah,s2.nama_kelas,s2.kredit_semester,s2.tipe_semester,s2.nilai_huruf
from 
(select mahasiswa.id_mhs,nim_mhs,nm_pengguna,nm_status_pengguna from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join admisi on mahasiswa.id_mhs=admisi.id_mhs and admisi.id_semester=$smt
left join status_pengguna on admisi.status_akd_mhs=status_pengguna.id_status_pengguna
where id_program_studi=$prodi and thn_angkatan_mhs<=$thn
and mahasiswa.id_mhs not in
(select id_mhs from 
(select id_mhs,case when nm_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,status_akd_mhs
from admisi
left join semester on admisi.id_semester=semester.id_semester)
where tahun<=$thnakd and status_akd_mhs not in (1,3))
)s1
left join 
(select id_mhs,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,tipe_semester,
kredit_semester,nilai_huruf
from(
select pengambilan_mk.id_mhs,MATA_KULIAH.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,tipe_semester,
KURIKULUM_MK.kredit_semester,case when kurikulum_mk.status_mkta='1' 
and (nilai_huruf='E' or nilai_huruf is null) and id_fakultas=7 then 'T' 
when nilai_huruf is null then 'E' else nilai_huruf end as nilai_huruf,
row_number() over(partition by pengambilan_mk.id_mhs,nm_mata_kuliah order by nilai_huruf) rangking
from PENGAMBILAN_MK
left join kelas_mk on PENGAMBILAN_MK.id_kelas_mk=KELAS_MK.id_kelas_mk
left join NAMA_KELAS on KELAS_MK.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk
left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
left join semester on pengambilan_mk.id_semester=semester.id_semester
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
where  group_semester||thn_akademik_semester in
(select group_semester||thn_akademik_semester from semester where id_Semester=$smt)
and tipe_semester in ('UP','REG','RD')
and status_apv_pengambilan_mk=1 and PENGAMBILAN_MK.status_hapus=0 and PENGAMBILAN_MK.status_pengambilan_mk !=0
and mahasiswa.id_program_studi=$prodi) 
where rangking=1
)s2 on s1.id_mhs=s2.id_mhs
where s1.nm_status_pengguna is not null or (s1.nm_status_pengguna is null and kd_mata_kuliah is not null)
order by s1.nim_mhs,s2.kd_mata_kuliah";
*/

$kueri="select nim_mhs,nm_pengguna,'',kd_mata_kuliah,nm_mata_kuliah,nama_kelas,tipe_semester,
kredit_semester,nilai_huruf
from(
select pengambilan_mk.id_mhs,nim_mhs,nm_pengguna,MATA_KULIAH.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,tipe_semester,
KURIKULUM_MK.kredit_semester,case when kurikulum_mk.status_mkta='1' 
and (nilai_huruf='E' or nilai_huruf is null) and id_fakultas=7 then 'T' 
when nilai_huruf is null then 'E' else nilai_huruf end as nilai_huruf,
row_number() over(partition by pengambilan_mk.id_mhs,nm_mata_kuliah order by nilai_huruf) rangking
from PENGAMBILAN_MK
left join kelas_mk on PENGAMBILAN_MK.id_kelas_mk=KELAS_MK.id_kelas_mk
left join NAMA_KELAS on KELAS_MK.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk
left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
left join semester on pengambilan_mk.id_semester=semester.id_semester
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
where  group_semester||thn_akademik_semester in
(select group_semester||thn_akademik_semester from semester where id_Semester=$smt)
and tipe_semester in ('UP','REG','RD')
and status_apv_pengambilan_mk=1 and PENGAMBILAN_MK.status_hapus=0 and PENGAMBILAN_MK.status_pengambilan_mk !=0
and mahasiswa.id_program_studi=$prodi) 
where rangking=1
order by nim_mhs,kd_mata_kuliah";

//echo $kueri;

$result = $db->Query($kueri)or die("salah kueri 01 ");
$i=1;

while($r = $db->FetchRow()) {
echo '
           <tr>
             <td align="center">'.$i.'</td>
             <td>&nbsp;'.$r[0].'</td>
             <td>'.$r[1].'</td>
             <td>'.$r[3].'</td>
             <td>'.$r[4].'</td>
             <td align="center">'.$r[5].'</td>
			 <td align="center">'.$r[7].'</td>
			 <td align="center">'.$r[6].'</td>
			 <td align="center">'.$r[8].'</td>
           </tr>';
$i++;
}
echo '
</table>';

?>