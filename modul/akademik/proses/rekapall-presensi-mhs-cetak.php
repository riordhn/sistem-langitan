<?php
require('../../../config.php');
$db = new MyOracle();
$db2 = new MyOracle();
$db3 = new MyOracle();
$db4 = new MyOracle();
$db5 = new MyOracle();

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sistem Langitan');
$pdf->SetTitle('Rekapitulasi Presensi Kuliah');
$pdf->SetSubject('Rekapitulasi Presensi Kuliah');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logo-".$nama_singkat.".gif";
$logo_size = "20";
$title = strtoupper($nama_pt);

$smt = $_GET['smt'];
$id_smt = $_GET['idsmt'];
$thn = $_GET['thn'];

$kdprodi = $_GET['prodi'];
$kueri = "select a.nm_jenjang, b.nm_program_studi, c.nm_fakultas,c.id_fakultas from jenjang a, program_studi b, fakultas c 
where a.id_jenjang=b.id_jenjang and c.id_fakultas=b.id_fakultas and b.id_program_studi=$kdprodi";
$result = $db->Query($kueri)or die("salah kueri 22 ");
while($r = $db->FetchRow()) {
	$jenjang = $r[0];
	$prodi = $r[1];
	$fak = $r[2];
	$kdfak = $r[3];
}

$id_kelas_mk = $_GET['cetak'];
$kueri = "select mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas.nama_kelas, jadwal_hari.nm_jadwal_hari, 
ruangan.nm_ruangan, jadwal_jam.jam_mulai||':'||jadwal_jam.menit_mulai||' - '||jadwal_jam.jam_selesai||':'||jadwal_jam.menit_selesai as jam
from kelas_mk
left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk=kelas_mk.id_kurikulum_mk
left join mata_kuliah on mata_kuliah.id_mata_kuliah=kurikulum_mk.id_mata_kuliah
left join nama_kelas on nama_kelas.id_nama_kelas=kelas_mk.no_kelas_mk
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_hari on jadwal_hari.id_jadwal_hari=jadwal_kelas.id_jadwal_hari
left join jadwal_jam on jadwal_jam.id_jadwal_jam=jadwal_kelas.id_jadwal_jam
left join ruangan on ruangan.id_ruangan=jadwal_kelas.id_ruangan
where kelas_mk.id_kelas_mk=$id_kelas_mk";
$result = $db->Query($kueri)or die("salah kueri 25 ");
while($r = $db->FetchRow()) {
	$kode_ma = $r[0];
	$nama_ma = $r[1];
	$sks_ma = $r[2];
	$kelas_ma = $r[3];
	$hari_ma = $r[4];
	$ruang_ma = $r[5];
	$jam_ma = $r[6];
}

$content = "FAKULTAS ".strtoupper($fak)."\n".strtoupper($jenjang)." - ".strtoupper($prodi)."\nPRESENSI PERKULIAHAN MAHASISWA SEMESTER ".strtoupper($smt)." ".$thn."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(5, PDF_MARGIN_TOP, 5);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 15);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage('L', 'F4');

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="17">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="12">'.$kode_ma.' - '.strtoupper($nama_ma).' ('.$sks_ma.' SKS) <br/>KELAS '.strtoupper($kelas_ma).'</td>
	<td colspan="5" rowspan="2">
	<table border="0" cellspacing="0" cellpadding="0">';

$kueri = "select case when pengguna.gelar_belakang is null then trim(pengguna.gelar_depan||' '||upper(pengguna.nm_pengguna)) 
else trim(pengguna.gelar_depan||' '||upper(pengguna.nm_pengguna)||', '||pengguna.gelar_belakang) end as nama_dsn, 
case when pengampu_mk.pjmk_pengampu_mk=1 then 'PJMA' else 'TIM' end as pjma
from kelas_mk
left join pengampu_mk on kelas_mk.id_kelas_mk = pengampu_mk.id_kelas_mk
left join dosen on pengampu_mk.id_dosen = dosen.id_dosen
left join pengguna on dosen.id_pengguna = pengguna.id_pengguna
where kelas_mk.id_kelas_mk=$id_kelas_mk
order by pengampu_mk.pjmk_pengampu_mk desc";
$result = $db->Query($kueri)or die("salah kueri 26 ");
while($r = $db->FetchRow()) {
	$nama_dsn = $r[0];
	$pjma = $r[1];
if($nama_dsn != ',') {
$html .= '
<tr>
	<td>'.$nama_dsn.' ('.$pjma.')</td>
</tr>';
} else {
$html .= '
<tr>
	<td>PJMA BELUM DITENTUKAN</td>
</tr>';
}
}
$html .= '
	</table>
	</td>
  </tr>
  <tr>
    <td colspan="12">'.strtoupper($hari_ma).' : '.$jam_ma.' <br/>'.strtoupper($ruang_ma).'</td>
  </tr>
  <tr>
    <td colspan="17">&nbsp;</td>
  </tr>
</table>
<table cellspacing="0" cellpadding="4" border="1" width="100%">
<thead>
    <tr bgcolor="lightgray" style="color:#000;">
      <td width="3%" align="center"><strong>NO.</strong></td>
	  <td width="9%" align="center"><strong>NIM</strong></td>
      <td width="15%" align="center"><strong>NAMA MAHASISWA</strong></td>';


$uts = "select  to_char(tgl_presensi_kelas,'DD/MM') from presensi_kelas 
where id_kelas_mk=$id_kelas_mk
and tgl_presensi_kelas <= (select tgl_mulai_jsf as mulai_uts from jadwal_semester_fakultas 
where id_kegiatan=44 and id_fakultas=$kdfak and id_semester=$id_smt)
order by tgl_presensi_kelas";

$result = $db->Query($uts)or die("salah kueri uts ");
while($r = $db->FetchRow()) {
	 $tgl_uts=$r[0];	
	  
	  $html .= '
      <td width="4%" align="center"><strong>'.$tgl_uts.'</strong></td>';
	  
}  
	  $html .= '
	  <td width="4%" align="center"><strong>UTS</strong></td>';

$uas = "select  to_char(tgl_presensi_kelas,'DD/MM') from presensi_kelas 
where id_kelas_mk=$id_kelas_mk
and tgl_presensi_kelas > (select tgl_mulai_jsf as mulai_uts from jadwal_semester_fakultas 
where id_kegiatan=44 and id_fakultas=$kdfak and id_semester=$id_smt)
and tgl_presensi_kelas <= (select tgl_mulai_jsf as mulai_uts from jadwal_semester_fakultas 
where id_kegiatan=47 and id_fakultas=$kdfak and id_semester=$id_smt)
order by tgl_presensi_kelas";

$result = $db->Query($uas)or die("salah kueri uas");
	  
while($r = $db->FetchRow()) {
	 $tgl_uas=$r[0];		  
	  $html .= '
	  <td width="4%" align="center"><strong>'.$tgl_uas.'</strong></td>';
}
	  $html .= '
	  <td width="4%" align="center"><strong>UAS</strong></td>
    </tr>
</thead>';

$nomor=1;
$kueri = "select kelas_mk.id_kelaS_mk,mahasiswa.nim_mhs,substr(nm_pengguna,1,20),mahasiswa.id_mhs
from kelas_mk
join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelaS_mk
join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
where kelas_mk.id_kelas_mk=$id_kelas_mk
order by mahasiswa.nim_mhs";

//echo $kueri;

$result1 = $db->Query($kueri)or die("salah kueri 27 ");
while($r = $db->FetchRow()) {
	$nim_mhs = $r[1];
	$nama_mhs = $r[2];
	$id_mhs=$r[3];
$style = "";
switch ($cekal_uas){
case  'CEKAL' : $style = 'bgcolor="red" style="color:#FFF;"';
		break;
case  'BLM DISET' : $style = 'bgcolor="yellow" style="color:#000;"';
		break;
case  '' : $style = 'bgcolor="white" style="color:#000;"';
		break;
}

		$html .= '
    <tr nobr="true" '.$style.'>
      <td width="3%" align="center">'.$nomor.'</td>
	  <td width="9%" align="center">'.$nim_mhs.'</td>
      <td width="15%" align="left">'.strtoupper($nama_mhs).'</td>';

		$totuts=0;
		$putar_uts="select  tgl_presensi_kelas,waktu_mulai from presensi_kelas 
					where id_kelas_mk=$id_kelas_mk
					and tgl_presensi_kelas < (select tgl_mulai_jsf as mulai_uts from jadwal_semester_fakultas 
					where id_kegiatan=44 and id_fakultas=$kdfak and id_semester=$id_smt)
					order by tgl_presensi_kelas";
		//echo $putar_uts;
		$result2 = $db2->Query($putar_uts)or die("salah kueri putar_uts ");
		while($r_uts = $db2->FetchRow()) {
				$putar_uts=$r_uts[0];
				$waktu_uts=$r_uts[1];
			
				$hadiruts="select coalesce(kehadiran,0) from presensi_mkmhs
						where id_presensi_kelas in (select id_presensi_kelas from presensi_kelas
						where id_kelas_mk=$id_kelas_mk and tgl_presensi_kelas='$putar_uts' and waktu_mulai='$waktu_uts')
						and id_mhs=$id_mhs";
				//echo $hadiruts;
				$cek=0;
				$result21 = $db3->Query($hadiruts)or die("salah kueri hadiruts ");
				while($r21 = $db3->FetchRow()) {
						$cek=$r21[0];
				}
						if ($cek==null) {$cek=0;}
						$html .= '
						<td width="4%" align="center"><strong>'.$cek.'</strong></td>';
						$totuts=$totuts+$cek;
				//}
		}	
		
	  $html .= '
	  <td width="4%" align="center"><strong>'.$totuts.'</strong></td>';
	  	
		$totuas=0;
		$putar_uas="select  tgl_presensi_kelas,waktu_mulai from presensi_kelas 
					where id_kelas_mk=$id_kelas_mk
					and tgl_presensi_kelas > (select tgl_mulai_jsf as mulai_uts from jadwal_semester_fakultas 
					where id_kegiatan=44 and id_fakultas=$kdfak and id_semester=$id_smt)
					and tgl_presensi_kelas < (select tgl_mulai_jsf as mulai_uts from jadwal_semester_fakultas 
					where id_kegiatan=47 and id_fakultas=$kdfak and id_semester=$id_smt)
					order by tgl_presensi_kelas";

		$result3 = $db4->Query($putar_uas)or die("salah kueri putar_uas ");
		while($r_uas = $db4->FetchRow()) {
				$putar_uas=$r_uas[0];
				$waktu_uas=$r_uas[1];
				
				$hadiruas="select coalesce(kehadiran,0) from presensi_mkmhs
						where id_presensi_kelas in (select id_presensi_kelas from presensi_kelas
						where id_kelas_mk=$id_kelas_mk  and tgl_presensi_kelas='$putar_uas' and waktu_mulai='$waktu_uas')
						and id_mhs=$id_mhs";
				//echo $hadiruas;
				
				$result31 = $db5->Query($hadiruas)or die("salah kueri hadiruas ");
				$cek1=0;
				while($r_hadiruas = $db5->FetchRow()) {
						$cek1=$r_hadiruas[0];
				}
						//echo $cek1;
						if ($cek1==null) {$cek1=0;}
						$html .= '
						<td width="4%" align="center"><strong>'.$cek1.'</strong></td>';
						$totuas=$totuas+$cek1;
				//}
		}
	 $html .= '
	  <td width="4%" align="center"><strong>'.$totuas.'</strong></td>
    </tr>';
$nomor++;
}
$html .= '</table>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('REKAP PRESENSI MHS KULIAH '.strtoupper($nama_ma).' '.strtoupper($kelas_ma).'.pdf', 'I');

?>