<?php
require('../../../config.php');
$db = new MyOracle();

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('KHS');
$pdf->SetSubject('KHS');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logounair.gif";
$logo_size = "25";
$title = "UNIVERSITAS AIRLANGGA";

include '../includes/function.php';
$var = decode($_SERVER['REQUEST_URI']);

$id = $var['cetak'];
$id_smt = $var['id_smt'];
$idmhs="select nim_mhs_asing from mahasiswa_asing where id_mhs_asing='".$id."'";
$result0 = $db->Query($idmhs)or die("salah kueri get");
while($r0 = $db->FetchRow()) {
	$nim = $r0[0];
}

$thnsmt="select 'KARTU HASIL STUDI TAHUN AJARAN '||tahun_ajaran||' SEMESTER '||upper(nm_semester) as nm_smt from semester where id_semester='".$id_smt."'";
$result = $db->Query($thnsmt)or die("salah kueri 1 ");
while($r = $db->FetchRow()) {
	$nm_smt = $r[0];
}

$biomhs="select mhs.nim_mhs_asing, upper(p.nm_pengguna) as nm_pengguna, j.nm_jenjang, ps.nm_program_studi, mhs.id_status_pengguna, f.nm_fakultas, 
			f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas, f.id_fakultas,
			mhs.id_program_studi,mhs.id_mhs_asing
			from mahasiswa_asing mhs 
			left join pengguna p on mhs.id_pengguna=p.id_pengguna 			
			left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
			left join fakultas f on ps.id_fakultas=f.id_fakultas
			left join jenjang j on ps.id_jenjang=j.id_jenjang
			where mhs.nim_mhs_asing='".$nim."'";
$result1 = $db->Query($biomhs)or die("salah kueri 2 ");
while($r1 = $db->FetchRow()) {
	$nim_mhs = $r1[0];
	$nm_mhs = $r1[1];
	$jenjang = $r1[2];
	$prodi = $r1[3];
	$status = $r1[4];
	$fak = $r1[5];
	$alm_fak = $r1[6];
	$pos_fak = $r1[7];
	$tel_fak = $r1[8];
	$fax_fak = $r1[9];
	$web_fak = $r1[10];
	$eml_fak = $r1[11];
	$kd_fak = $r1[12];
	$kd_prodi = $r1[13];
	$id_mhs=$r1[14];
}

$content = "FAKULTAS ".strtoupper($fak)."\n".strtoupper($jenjang)." - ".strtoupper($prodi)."\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 40, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default footer
$pdf->setPrintFooter(false);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage('P', 'A4');

// draw jpeg image
$pdf->Image('../includes/logo_unair.png', 50, 50, 100, '', '', '', '', false, 72);

// set the starting point for the page content
$pdf->setPageMark();

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="6" align="center"><b><u>'.$nm_smt.'</u></b></td>
  </tr>
  <tr>
    <td colspan="6">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="17%">NIM</td>
	<td width="3%"><center>:</center></td>
	<td width="80%">'.$nim_mhs.'</td>
  </tr>
  <tr>
    <td>Nama Mahasiswa</td>
	<td><center>:</center></td>
	<td>'.strtoupper($nm_mhs).'</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="6">&nbsp;</td>
  </tr>
</table>
<table cellspacing="0" cellpadding="0" border="1" width="100%">
<thead>
    <tr bgcolor="black" style="color:#fff;">
      <td width="5%" align="center"><strong>NO.</strong></td>
	  <td width="10%" align="center"><strong>KODE MA</strong></td>
      <td width="64%" align="center"><strong>NAMA MATA AJAR</strong></td>
      <td width="5%" align="center"><strong>SKS</strong></td>
	  <td width="8%" align="center"><strong>NILAI</strong></td>
	  <td width="8%" align="center"><strong>BOBOT</strong></td>
    </tr>
</thead>';

/*$kueri = "select e.kd_mata_kuliah kode,e.nm_mata_kuliah_en nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,
coalesce(f.nilai_standar_nilai*d.kredit_semester,0) as bobot_total 
from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk_asing a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa_asing m on a.id_mhs=m.id_mhs_asing
where rangking=1 and m.nim_mhs_asing='".$nim."'
order by e.nm_mata_kuliah";*/

$kueri = "select e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk_asing a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa_asing m on a.id_mhs=m.id_mhs_asing
where rangking=1 and m.nim_mhs_asing='{$nim}'
order by e.nm_mata_kuliah";

$nomor=1;
$bobot_tot = 0;
$result2 = $db->Query($kueri)or die("salah kueri 3 ");
while($r2 = $db->FetchRow()) {
	$kode_ma = $r2[0];
	$nama_ma = $r2[1];
	$sks_ma = $r2[3];
	$nilai_ma = $r2[4];
	$bobot_ma = $r2[5];
	$bobot_ttl = $bobot_ma*$sks_ma;
	$bobot_tot = $bobot_tot + $bobot_ttl;

$html .= '
    <tr style="color:#000;">
      <td width="5%" align="center">'.$nomor.'</td>
	  <td width="10%" align="center">'.$kode_ma.'</td>
      <td width="64%" align="left">&nbsp;&nbsp;'.strtoupper($nama_ma).'</td>
      <td width="5%" align="center">'.$sks_ma.'</td>
	  <td width="8%" align="center">'.$nilai_ma.'</td>
	  <td width="8%" align="center">'.$bobot_ttl.'</td>
    </tr>';
$nomor++;
}


$ipk="select sum(sks) as sks_total, sum((bobot*sks)) as bobot_total, round((sum((bobot*sks))/sum(sks)),2) as ipk
from 
(
select e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk_asing a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa_asing m on a.id_mhs=m.id_mhs_asing
where rangking=1 and m.nim_mhs_asing='{$nim}'
) uu";
$result2 = $db->Query($ipk)or die("salah kueri nilai ");
while($r2 = $db->FetchRow()) {
	$sks_tot = $r2[0];
	$ipk_tot = number_format($r2[2],2,'.',',');

$html .= '
           <tr>
             <td colspan="3" align="right"><b>Total SKS dan Bobot&nbsp;&nbsp;</b></td>
             <td align="center"><b>'.$sks_tot.'</b></td>
			 <td></td>
			 <td align="center"><b>'.$bobot_tot.'</b></td>
           </tr>
           <tr>
			 <td colspan="3" align="right"><b>Indeks Prestasi Kumulatif&nbsp;&nbsp;</b></td>
             <td colspan="3" align="center"><b>'.$ipk_tot.'</b></td>
           </tr>';
}

$html .= '</table>';


 
$doli="select case when p.gelar_belakang is not null then 
trim(p.gelar_depan||' '||upper(p.nm_pengguna)||', '||p.gelar_belakang) else 
trim(p.gelar_depan||' '||upper(p.nm_pengguna)) end as nm_dosen, d.nip_dosen
from pengguna p
left join dosen d on p.id_pengguna=d.id_pengguna 
left join program_studi ps on d.id_program_studi=ps.id_program_studi
left join fakultas f on ps.id_fakultas=f.id_fakultas 
where f.id_fakultas='".$kd_fak."' and d.id_jabatan_pegawai='6'";

$result4 = $db->Query($doli)or die("salah kueri 5 ");
while($r4 = $db->FetchRow()) {
	$nm_doli = $r4[0];
	$nip_doli = $r4[1];
}

$bulan['01'] = "Januari"; $bulan['02'] = "Februari"; $bulan['03'] = "Maret"; $bulan['04'] = "April"; $bulan['05'] = "Mei"; $bulan['06'] = "juni"; $bulan['07'] = "Juli"; $bulan['08'] = "Agustus"; $bulan['09'] = "September"; $bulan['10'] = "Oktober";  $bulan['11'] = "November";  $bulan['12'] = "Desember";


if($kd_fak==8){
$html .= '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="6">&nbsp;<br/><br/><br/>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><br><br><br>Lembar :<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. untuk mahasiswa_asing<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2. untuk dosen wali<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3. untuk departemen</td>
	<td colspan="4" align="center">Surabaya, '.date("d").' '.$bulan[date("m")].' '.date("Y").'<br/>Dosen Wali,<br/><br/><br/><br/><br/><br/><u>'.$nm_doli2.'</u><br />'.$nip_doli2.'</td>
  </tr>
</table>';
}
else{
$html .= '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="6">&nbsp;<br/><br/><br/>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><br><br><br>Lembar :<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. untuk mahasiswa<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2. untuk dosen wali<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3. untuk departemen</td>
	<td colspan="4" align="center">Surabaya, '.date("d").' '.$bulan[date("m")].' '.date("Y").'<br/>Wakil Dekan I,<br/><br/><br/><br/><br/><br/><u>'.$nm_doli.'</u><br />'.$nip_doli.'</td>
  </tr>
</table>';

}

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('KHS-'.strtoupper($nim_mhs).'.pdf', 'I');

?>