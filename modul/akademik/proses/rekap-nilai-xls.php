<?php
require('../../../config.php');
$db = new MyOracle();

include '../includes/function.php';
$var = decode($_SERVER['REQUEST_URI']);
$id = $var['cetak'];

$idmhs="select nim_mhs from mahasiswa where id_mhs='".$id."'";
$result0 = $db->Query($idmhs)or die("salah kueri get ");
while($r0 = $db->FetchRow()) {
	$nim = $r0[0];
}

header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=rekap-nilai-".$nim.".xls");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data";

$biomhs="select mhs.nim_mhs, case when j.id_jenjang=2 then p.nm_pengguna||', '||p.gelar_belakang else p.nm_pengguna end , j.nm_jenjang, ps.nm_program_studi, mhs.status_akademik_mhs, f.nm_fakultas,
			f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas, f.id_fakultas,
			kt.nm_kota, TO_CHAR(p.tgl_lahir_pengguna, 'DD-MM-YYYY'), TO_CHAR(mhs.tgl_lulus_mhs, 'DD-MM-YYYY'), pw.no_ijasah, TO_CHAR(mhs.tgl_terdaftar_mhs, 'DD-MM-YYYY'), mhs.id_mhs, pw.judul_ta, pw.lahir_ijazah,
			j.id_jenjang,mhs.id_program_studi,pm.nm_prodi_minat,jal.nm_jalur
			from mahasiswa mhs
			left join kota kt on kt.id_kota=mhs.lahir_kota_mhs
			left join pengajuan_wisuda pw on pw.id_mhs=mhs.id_mhs
			left join pengguna p on mhs.id_pengguna=p.id_pengguna
			left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
			left join fakultas f on ps.id_fakultas=f.id_fakultas
			left join jenjang j on ps.id_jenjang=j.id_jenjang
			left join prodi_minat pm on mhs.id_prodi_minat=pm.id_prodi_minat
			left join jalur_mahasiswa jm on mhs.id_mhs=jm.id_mhs 
			left join jalur jal on jm.id_jalur=jal.id_jalur
			where mhs.nim_mhs='".$nim."'";
$result1 = $db->Query($biomhs)or die("salah kueri biodata ");
while($r1 = $db->FetchRow()) {
	$nim_mhs = $r1[0];
	$nm_mhs = $r1[1];
	$jenjang = $r1[2];
	$prodi = $r1[3];
	$status = $r1[4];
	$fak = $r1[5];
	$alm_fak = $r1[6];
	$pos_fak = $r1[7];
	$tel_fak = $r1[8];
	$fax_fak = $r1[9];
	$web_fak = $r1[10];
	$eml_fak = $r1[11];
	$kd_fak = $r1[12];
	$tpt_lhr = $r1[13];
	$tgl_lhr = $r1[14];
	$tgl_lls = $r1[15];
	$no_ijazah = $r1[16];
	$tgl_dtr = $r1[17];
	$id_mhs = $r1[18];
	$jdl_ta = $r1[19];
	$ttl = $r1[20];
	$jjg1 = $r1[21];
	$idprodi = $r1[22];
	$minatmhs = $r1[23];
	$jalur = $r1[24];
}

$nilai="select id_jenjang, sum(sks) as sks_total, round((sum((bobot*sks))/sum(sks)),2) as ipk,sum(bobot*sks) as bobot 
from
(
select ps.id_jenjang, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah,d.kredit_semester order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join program_studi ps on m.id_program_studi=ps.id_program_studi
where rangking=1 and m.nim_mhs='".$nim."'
)
group by id_jenjang";
$result2 = $db->Query($nilai)or die("salah kueri nilai ");
while($r2 = $db->FetchRow()) {
	$jjg = $r2[0];
	$jum_sks = $r2[1];
	$ipk = number_format($r2[2],2,'.',',');
	$bobot = number_format($r2[3],2,'.',',');
}

$pre="select nm_mhs_predikat 
from mhs_predikat left join jenjang on mhs_predikat.id_jenjang=jenjang.id_jenjang 
where jenjang.id_jenjang=$jjg and ipk_min_mhs_predikat<=$ipk and ipk_max_mhs_predikat>=$ipk";
$result_pre = $db->Query($pre)or die("salah kueri predikat ");
while($r_pre = $db->FetchRow()) {
	$predikat = $r_pre[0];
}

$jml2 = "select count(*) as jml from (
select e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where rangking=1 and m.nim_mhs='".$nim."')";
$resultjml2 = $db->Query($jml2)or die("salah kueri jml2 ");
while($jml2 = $db->FetchRow()) {
	$jml_2 = $jml2[0];

	$total = $jml_2 / 2;

	$limit = round($total, 0, PHP_ROUND_HALF_UP);
}

$bulan['01'] = "Januari"; $bulan['02'] = "Pebruari"; $bulan['03'] = "Maret"; $bulan['04'] = "April"; $bulan['05'] = "Mei"; $bulan['06'] = "Juni"; $bulan['07'] = "Juli"; $bulan['08'] = "Agustus"; $bulan['09'] = "September"; $bulan['10'] = "Oktober";  $bulan['11'] = "Nopember";  $bulan['12'] = "Desember";

echo '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2" align="center"><br><b>DAFTAR NILAI</b></td>
  </tr>
  <tr>
    <td colspan="2"><br>&nbsp;</td>
  </tr>
  <tr>
    <td width="15%">NIM</td>
	<td> : '.$nim_mhs.'</td>
	<td width="10%"></td>
    <td width="15%">Program Studi</td>
	<td> : '.strtoupper($prodi).'</td>
  </tr>
  <tr>
    <td>Nama</td>
	<td> : '.strtoupper($nm_mhs).'</td>
	<td></td>
	<td>Total SKS / Bobot</td>
	<td> : '.$jum_sks.' / '.$bobot.'</td>
  </tr>
  <tr>
    <td>Tempat, Tgl Lahir</td>
	<td> : '.strtoupper($tpt_lhr).', '.date("d", strtotime($tgl_lhr)).' '.$bulan[date("m", strtotime($tgl_lhr))].' '.date("Y", strtotime($tgl_lhr)).'</td>
	<td></td>
	<td>IPK</td>
	<td> : '.$ipk.'</td>
  </tr>
  <tr>
    <td>Jalur</td>
	<td> : '.strtoupper($jalur).'</td>
	<td></td>
	<td>No. Ijasah</td>
	<td> : '.$no_ijazah.'</td>
  </tr>
  <tr>
  <td colspan="2"><br>&nbsp;</td>
  </tr>
</table>

<table width="100%" border="1" cellpadding="2">
<tr>
	<td align="center"><b>THN</b></td>
	<td align="center"><b>KODE</b></td>
	<td align="center"><b>NAMA MATA AJAR</b></td>
	<td align="center"><b>ENGLISH</b></td>
	<td align="center"><b>SKS</b></td>
	<td align="center"><b>NILAI</b></td>
	<td align="center"><b>BOBOT</b></td>
	<td align="center"><b>THN</b></td>
	<td align="center"><b>KODE</b></td>
	<td align="center"><b>NAMA MATA AJAR</b></td>
	<td align="center"><b>ENGLISH</b></td>
	<td align="center"><b>SKS</b></td>
	<td align="center"><b>NILAI</b></td>
	<td align="center"><b>BOBOT</b></td>
</tr>';

$list1="select * from (
select rownum as b1,thn,thn_akademik_semester,thn_angkatan_mhs,
kode,nama,name,id_mhs,sks,nilai,bobot from 
(select case when s.group_semester='Ganjil' and  (s.nm_semester='Ujian Perbaikan' or s.nm_semester='Ganjil') then thn_akademik_semester||' '||'Ganjil' 
else case when s.group_semester='Ganjil' and  (s.nm_semester='Ujian Perbaikan' or s.nm_semester='Genap') then thn_akademik_semester||' '||'Genap'  
else thn_akademik_semester||' '||'Genap' end end as thn,
thn_akademik_semester,thn_angkatan_mhs,
e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,e.nm_mata_kuliah_en name,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join semester s on a.id_semester=s.id_semester
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where rangking=1 and m.id_mhs='".$id_mhs."' and s.thn_akademik_semester < thn_angkatan_mhs+2
order by thn_akademik_semester,group_semester,kode)) s1
FULL OUTER join
(select rownum as b2,thn as thn1,thn_akademik_semester as thn_akademik_semester1 ,thn_angkatan_mhs as thn_angkatan_mhs1,
kode as kode1,nama as nama1,name as name1,id_mhs as id_mhs1,sks as sks,nilai as nilai,bobot as bobot from 
(select case when s.group_semester='Ganjil' and  (s.nm_semester='Ujian Perbaikan' or s.nm_semester='Ganjil') then thn_akademik_semester||' '||'Ganjil' 
else case when s.group_semester='Ganjil' and  (s.nm_semester='Ujian Perbaikan' or s.nm_semester='Genap') then thn_akademik_semester||' '||'Genap'  
else thn_akademik_semester||' '||'Genap' end end as thn,
thn_akademik_semester,thn_angkatan_mhs,
e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,e.nm_mata_kuliah_en name,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join semester s on a.id_semester=s.id_semester
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where rangking=1 and m.id_mhs='".$id_mhs."' and s.thn_akademik_semester >= thn_angkatan_mhs+2
order by thn_akademik_semester,group_semester,kode)) s2 on s1.b1=s2.b2
order by b1";


$result21 = $db->Query($list1)or die("salah kueri list1 ");
while($r21 = $db->FetchRow()) {
	$tsmt=$r21[1];
	$kode_ma21 = $r21[4];
	$nama_ma21 = $r21[5];
	$name_ma21 = $r21[6];
	$sks_ma21 = $r21[8];
	$nilai_ma21 = $r21[9];
	$bobot_ma21 = number_format(($r21[8]*$r21[10]),2,'.',',');

	$tsmt1=$r21[12];
	$kode_ma22 = $r21[15];
	$nama_ma22 = $r21[16];
	$name_ma22 = $r21[17];
	$sks_ma22 = $r21[19];
	$nilai_ma22 = $r21[20];
	$bobot_ma22 = number_format(($r21[19]*$r21[21]),2,'.',',');;
echo '
   	<tr style="color:#000;">
	<td align="center">'.$tsmt.'</td>
	<td align="left">'.$kode_ma21.'</td>
	<td align="left">'.$nama_ma21.'</td>
	<td align="left">'.$name_ma21.'</td>
	<td align="center">'.$sks_ma21.'</td>
	<td align="center">'.$nilai_ma21.'</td>
	<td align="center">'.$bobot_ma21.'</td>
	<td align="center">'.$tsmt1.'</td>
	<td align="left">'.$kode_ma22.'</td>
	<td align="left">'.$nama_ma22.'</td>
	<td align="left">'.$name_ma22.'</td>
	<td align="center">'.$sks_ma22.'</td>
	<td align="center">'.$nilai_ma22.'</td>
	<td align="center">'.$bobot_ma22.'</td>
	</tr>';


}

echo '
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td colspan="10">&nbsp;</td>
</tr>
<tr>
    <td colspan="10">JUDUL SKRIPSI</td>
</tr>
<tr>
    <td colspan="10">'.strtoupper($jdl_ta).'</td>
  </tr>
</table>
';
?>