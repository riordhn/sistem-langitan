<?php
require('../../../config.php');
$db = new MyOracle();

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// fungsi untuk menyingkat nama yg karakternya lebih dari 20
 /* FIKRIE 16-04-2016 */
function singkat_nama($str) {
	if(strlen($str) > 20 ){
		// mengambil jumlah kata
		$jumlah_kata = str_word_count($str);

		// jika jumlah kata > 2
		if($jumlah_kata > 2 ){
			// memisahkan per kata
			$arr = explode(' ',trim($str));

			$kata_potong = '';
			// mengambil mulai potongan kata ke 3
			if(!empty($arr[2])){
				$kata_potong = $arr[2];
			}
			if(!empty($arr[3])){
				$kata_potong .= ' '.$arr[3];
			}
			if(!empty($arr[4])){
				$kata_potong .= ' '.$arr[4];
			}
			if(!empty($arr[5])){
				$kata_potong .= ' '.$arr[5];
			}

			// mengambil singkatan
			$ret = '';
		    foreach (explode(' ', $kata_potong) as $word){
		        $ret .= ' '.strtoupper($word[0]).'.';
		    }

		    // menggabungkan 2 kata awal dengan hasil singkatan
		    $hasil = $arr[0].' '.$arr[1].$ret;
		    return $hasil;
		}
		// jika jumlah kata = 2
		else{
			// memisahkan per kata
			$arr = explode(' ',trim($str));

			$kata_potong = '';
			// mengambil potongan kata ke 2
			if(!empty($arr[1])){
				$kata_potong = $arr[1];
			}

			// mengambil singkatan
			$ret = '';
		    foreach (explode(' ', $kata_potong) as $word){
		        $ret .= ' '.strtoupper($word[0]).'.';
		    }

		    // menggabungkan 1 kata awal dengan hasil singkatan di kata kedua
		    $hasil = $arr[0].' '.$ret;
		    return $hasil;
		}
	}
	else{
		return $str;
	}
}


// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sistem Langitan');
$pdf->SetTitle('Presensi Kuliah');
$pdf->SetSubject('Presensi Kuliah');
$pdf->SetKeywords('');

//parameter header
$logo = "../../../img/akademik_images/logo-".$nama_singkat.".gif";
$logo_size = "25";
$title = strtoupper($nama_pt);

$smt = $_GET['smt'];
$thn = $_GET['thn'];
$hari = $_GET['hari'];
$jam = $_GET['jam'];

$kdprodi = $_GET['prodi'];
$kueri = "select a.nm_jenjang, b.nm_program_studi, f.nm_fakultas, f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas
			from jenjang a, program_studi b, fakultas f where a.id_jenjang=b.id_jenjang and f.id_fakultas=b.id_fakultas and b.id_program_studi=$kdprodi";
$result = $db->Query($kueri)or die("salah kueri 22 ");
while($r = $db->FetchRow()) {
	$jenjang = $r[0];
	$prodi = $r[1];
	$fak = $r[2];
	$alm_fak = $r[3];
	$pos_fak = $r[4];
	$tel_fak = $r[5];
	$fax_fak = $r[6];
	$web_fak = $r[7];
	$eml_fak = $r[8];
}

$id_kelas_mk = $_GET['cetak'];
$kueri = "select mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, 
nama_kelas.nama_kelas, jadwal_hari.nm_jadwal_hari,ruangan.nm_ruangan
from kelas_mk
left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk=kelas_mk.id_kurikulum_mk
left join mata_kuliah on mata_kuliah.id_mata_kuliah=kurikulum_mk.id_mata_kuliah
left join nama_kelas on nama_kelas.id_nama_kelas=kelas_mk.no_kelas_mk
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_hari on jadwal_hari.id_jadwal_hari=jadwal_kelas.id_jadwal_hari
left join jadwal_jam on jadwal_jam.id_jadwal_jam=jadwal_kelas.id_jadwal_jam
left join ruangan on ruangan.id_ruangan=jadwal_kelas.id_ruangan
where kelas_mk.id_kelas_mk=$id_kelas_mk and jadwal_hari.id_jadwal_hari=$hari";
$result = $db->Query($kueri)or die("salah kueri 25 ");
while($r = $db->FetchRow()) {
	$kode_ma = $r[0];
	$nama_ma = $r[1];
	$sks_ma = $r[2];
	$kelas_ma = $r[3];
	$hari_ma = $r[4];
	$ruang_ma = $r[5];
	
}

$content = "FAKULTAS ".strtoupper($fak)."\n".strtoupper($jenjang)." - ".strtoupper($prodi)."\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
//$pdf->SetHeaderData($logo, $logo_size, $title, $content);
$pdf->SetPrintHeader(false);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 15, 10, 15);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 15);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 9);

// add a page
$pdf->AddPage('L', 'F4');

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  	<td width="10%"></td>
    <td width="20%" align="center"> <img src="'.$logo.'" alt="" height="80" width="80"> </td>
    <td width="40%" align="center"><span style="font-size: xx-large;"><b>PRESENSI MAHASISWA</b></span><br/><span style="font-size: xx-large;"><b>PERKULIAHAN SEMESTER '.strtoupper($smt).' '.$thn.'</b></span><br/><span style="font-size: xx-large;"><b>FAKULTAS '.strtoupper($fak).'</b></span></td>
    <td width="30%"></td>
  </tr>
</table>

<div style="color:#020202;height:170px;">_______________________________________________________________________________________________________________________________________________________________________________</div>

';

$html .= '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="17">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  	<td width="10%">PROGRAM STUDI</td>
  	<td width="40%"> : '.strtoupper($jenjang).' - '.strtoupper($prodi).'</td>
  	<td width="10%"> MATA KULIAH</td>
  	<td width="40%"> : '.$kode_ma.' - '.strtoupper($nama_ma).' ('.$sks_ma.' SKS)</td>

	<!--<td> KELAS :  '.strtoupper($kelas_ma).'</td>-->
    <!--<td colspan="12">'.$kode_ma.' - '.strtoupper($nama_ma).' ('.$sks_ma.' SKS) <br/>KELAS '.strtoupper($kelas_ma).'</td>
	<td colspan="5" rowspan="2">
	<table border="0" cellspacing="0" cellpadding="0">-->';

$kueri = "select case when pengguna.gelar_belakang is null then trim(pengguna.gelar_depan||' '||upper(pengguna.nm_pengguna)) 
else trim(pengguna.gelar_depan||' '||upper(pengguna.nm_pengguna)||', '||pengguna.gelar_belakang) end as nama_dsn, 
case when pengampu_mk.pjmk_pengampu_mk=1 then 'PJMA' else 'TIM' end as pjma, kurikulum_mk.tingkat_semester
from kelas_mk
left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk = kelas_mk.id_kurikulum_mk
left join pengampu_mk on kelas_mk.id_kelas_mk = pengampu_mk.id_kelas_mk
left join dosen on pengampu_mk.id_dosen = dosen.id_dosen
left join pengguna on dosen.id_pengguna = pengguna.id_pengguna
where kelas_mk.id_kelas_mk=$id_kelas_mk
order by pengampu_mk.pjmk_pengampu_mk desc";
$result = $db->Query($kueri)or die("salah kueri 26 ");
while($r = $db->FetchRow()) {
	$nama_dsn = $r[0];
	$pjma = $r[1];
	$tingkat_smt = $r[2];
if($nama_dsn != ',') {
$html .= '
<!--<tr>
	<td>'.strtoupper($nama_dsn).' ('.$pjma.')</td>
</tr>-->';
} else {
$html .= '
<!--<tr>
	<td>PJMA BELUM DITENTUKAN</td>
</tr>-->';
}
}
$html .= '
	<!--</table>
	</td>-->
  </tr>
  <tr>
  	<td>KELAS </td>
  	<td> : '.strtoupper($kelas_ma).'</td>
    <!--<td colspan="12">'.strtoupper($hari_ma).' : '.$jam.' <br/>RUANG : '.strtoupper($ruang_ma).'</td>-->
    <td> DOSEN</td>';

if($nama_dsn != ',') {
$html .= '
	<td> : '.strtoupper($nama_dsn).' ('.$pjma.')</td>';
} else {
$html .= '
	<td> : PJMA BELUM DITENTUKAN</td>';
}



$html .='
  </tr>
  <tr>
    <td>SEMESTER / HARI </td>
  	<td> : '.$tingkat_smt.' / '.strtoupper($hari_ma).' : '.$jam.'</td>
  	<td> RUANG </td>
  	<td> : '.strtoupper($ruang_ma).'</td>
  </tr>
  <tr>
    <td></td>
  	<td></td>
  	<td></td>
  	<td></td>
  </tr>
</table>
<table cellspacing="0" cellpadding="4" border="1" width="100%">
<thead>
	<tr bgcolor="lightgray" style="color:#000;">
      <td width="5%" rowspan="3" align="center"><strong><br/>NO.<br/>&nbsp;</strong></td>
	  <td width="9%" rowspan="3" align="center"><strong><br/>NIM<br/>&nbsp;</strong></td>
      <td width="16%" rowspan="3" align="center"><strong><br/>NAMA MAHASISWA<br/>&nbsp;</strong></td>
      <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
    </tr>
    <tr bgcolor="white" style="color:#000;">
      <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
    </tr>
    <tr bgcolor="lightgray" style="color:#000;">
      <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
    </tr>
</thead>';

$nomor=1;
$kueri = "select mahasiswa.nim_mhs, pengguna.nm_pengguna
from mahasiswa
left join pengambilan_mk on mahasiswa.id_mhs=pengambilan_mk.id_mhs
left join pengguna on pengguna.id_pengguna=mahasiswa.id_pengguna
where pengambilan_mk.id_kelas_mk=$id_kelas_mk and pengambilan_mk.status_apv_pengambilan_mk=1
order by mahasiswa.nim_mhs asc";
$result = $db->Query($kueri)or die("salah kueri 27 ");
while($r = $db->FetchRow()) {
	$nim_mhs = $r[0];
	$nama_mhs = $r[1];

$html .= '
    <tr nobr="true" bgcolor="#FFF" style="color:#000;">
      <td width="5%" align="center">'.$nomor.'</td>
	  <td width="9%" align="center">'.$nim_mhs.'</td>
      <td width="16%" align="left">'.singkat_nama(strtoupper($nama_mhs)).'</td>
      <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
    </tr>';
$nomor++;
}

for ($i=0; $i < 4; $i++) { 
	$html .= '
    <tr nobr="true" bgcolor="#FFF" style="color:#000;">
      <td width="5%" align="center">&nbsp;</td>
	  <td width="9%" align="center">&nbsp;</td>
      <td width="16%" align="center">&nbsp;</td>
      <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
    </tr>';
}

$html .= '</table>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('PRESENSI KULIAH '.strtoupper($nama_ma).' '.strtoupper($kelas_ma).'.pdf', 'I');

?>