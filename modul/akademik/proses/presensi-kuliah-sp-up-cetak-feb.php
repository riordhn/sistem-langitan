<?php
require('../../../config.php');
$db = new MyOracle();

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('Presensi Kuliah SP/UP');
$pdf->SetSubject('Presensi Kuliah SP/UP');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logounair.gif";
$logo_size = "25";
$title = "UNIVERSITAS AIRLANGGA";

$smt = $_GET['smt'];
switch ($smt){
case  'SP' : $str_smt = 'SEMESTER PENDEK';
		break;
case  'UP' : $str_smt = 'UJIAN PERBAIKAN';
		break;
}

$thn = $_GET['thn'];
$hari = $_GET['hari'];

$kdprodi = $_GET['prodi'];
$kueri = "select a.nm_jenjang, b.nm_program_studi, c.nm_fakultas from jenjang a, program_studi b, fakultas c 
where a.id_jenjang=b.id_jenjang and c.id_fakultas=b.id_fakultas and b.id_program_studi=$kdprodi";
$result = $db->Query($kueri)or die("salah kueri 22 ");
while($r = $db->FetchRow()) {
	$jenjang = $r[0];
	$prodi = $r[1];
	$fak = $r[2];
}

$id_kelas_mk = $_GET['cetak'];
$kueri = "select mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas.nama_kelas, jadwal_hari.nm_jadwal_hari, ruangan.nm_ruangan, jadwal_jam.nm_jadwal_jam
from kelas_mk
left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk=kelas_mk.id_kurikulum_mk
left join mata_kuliah on mata_kuliah.id_mata_kuliah=kurikulum_mk.id_mata_kuliah
left join nama_kelas on nama_kelas.id_nama_kelas=kelas_mk.no_kelas_mk
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_hari on jadwal_hari.id_jadwal_hari=jadwal_kelas.id_jadwal_hari
left join jadwal_jam on jadwal_jam.id_jadwal_jam=jadwal_kelas.id_jadwal_jam
left join ruangan on ruangan.id_ruangan=jadwal_kelas.id_ruangan
where kelas_mk.id_kelas_mk=$id_kelas_mk and jadwal_hari.id_jadwal_hari=$hari";
$result = $db->Query($kueri)or die("salah kueri 25 ");
while($r = $db->FetchRow()) {
	$kode_ma = $r[0];
	$nama_ma = $r[1];
	$sks_ma = $r[2];
	$kelas_ma = $r[3];
	$hari_ma = $r[4];
	$ruang_ma = $r[5];
	$jam_ma = $r[6];
}

$content = "FAKULTAS ".strtoupper($fak)."\n".strtoupper($jenjang)." - ".strtoupper($prodi)."\nPRESENSI PERKULIAHAN MAHASISWA SEMESTER ".strtoupper($smt)." ".$thn."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, PDF_MARGIN_TOP, 5);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 14);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage('P', 'A4');

$html = '
<table width="100%" cellspacing="0" cellpadding="5">
<thead>
  <tr>
    <td colspan="9" align="center"><b><u>PRESENSI PERKULIAHAN MAHASISWA '.strtoupper($str_smt).' '.$thn.'</u></b></td>
  </tr>
  <tr>
    <td colspan="9">'.$kode_ma.' - '.strtoupper($nama_ma).' ('.$sks_ma.' SKS) <br/>KELAS '.$kelas_ma.' <br/>'.strtoupper($hari_ma).' : '.$jam_ma.' ('.strtoupper($ruang_ma).')</td>
  </tr>
    <tr bgcolor="#FFF" style="color:#000;">
      <td border="1" width="5%" align="center"><strong><br/>NO.<br/>&nbsp;</strong></td>
	  <td border="1" width="20%" align="center"><strong><br/>NIM<br/>&nbsp;</strong></td>
      <td border="1" width="35%" align="center"><strong><br/>NAMA MAHASISWA<br/>&nbsp;</strong></td>
      <td border="1" width="7%" align="center"><strong>&nbsp;</strong></td>
	  <td border="1" width="7%" align="center"><strong>&nbsp;</strong></td>
	  <td border="1" width="7%" align="center"><strong>&nbsp;</strong></td>
	  <td border="1" width="7%" align="center"><strong>&nbsp;</strong></td>
	  <td border="1" width="7%" align="center"><strong>&nbsp;</strong></td>
	  <td border="1" width="7%" align="center"><strong>&nbsp;</strong></td>
    </tr>
</thead>';

$nomor=1;
$kueri = "select mahasiswa.nim_mhs, pengguna.nm_pengguna, cek_bayar
from mahasiswa
left join pengambilan_mk on mahasiswa.id_mhs=pengambilan_mk.id_mhs
left join pengguna on pengguna.id_pengguna=mahasiswa.id_pengguna
left join (
	select count(*) as cek_bayar, id_mhs 
	from pembayaran_sp where id_semester = $_REQUEST[idsmt] and id_status_pembayaran = 2
	group by id_mhs
) a on a.id_mhs = mahasiswa.id_mhs
where pengambilan_mk.id_kelas_mk=$id_kelas_mk and pengambilan_mk.status_apv_pengambilan_mk=1
and pengambilan_mk.id_semester=$_REQUEST[idsmt]
order by mahasiswa.nim_mhs asc";
$result = $db->Query($kueri)or die("salah kueri 27 ");
while($r = $db->FetchRow()) {
	$nim_mhs = $r[0];
	$nama_mhs = $r[1];
	$cek_bayar = $r[2];
	

	if($cek_bayar >= 1){
		$bgcolor = '  style="background-color:#000"';	
	}else{
		$bgcolor = ' style="background-color:#FFF"';
	}
	
$html .= '
    <tr  style="color:#000;">
      <td border="1" width="5%" align="center">'.$nomor.'</td>
	  <td border="1" width="20%" align="center">'.$nim_mhs.'</td>
      <td border="1" width="35%" align="left">'.$nama_mhs.'</td>
      <td '.$bgcolor.' border="1" width="7%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' border="1" width="7%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' border="1" width="7%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' border="1" width="7%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' border="1" width="7%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' border="1" width="7%" align="center">&nbsp;</td>
    </tr>';
$nomor++;
}
$html .= '</table>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('PRESENSI KULIAH '.$str_smt.' '.strtoupper($nama_ma).' '.strtoupper($kelas_ma).'.pdf', 'I');

?>