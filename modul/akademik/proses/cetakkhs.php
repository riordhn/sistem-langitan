<?php
require '../../../config.php';

$id_pt = $user->ID_PERGURUAN_TINGGI;
$nama_pt = $PT->nama_pt;
$kota_pt = $PT->kota_pt;

$idprodi = $_POST['prodi'];
$id_semester = $_POST['id_smt'];
$fakultas = "FAKULTAS ".strtoupper($user->NM_FAKULTAS);

$sql = "select NM_SEMESTER, TAHUN_AJARAN from semester where id_semester=$id_semester and id_perguruan_tinggi = '".$id_pt."'";
$db->Query($sql);
while ($r=$db->FetchRow()) {
    $nm_semester = $r[0];
    $thn_semester = $r[1];
}

$sql="select id_fakultas, nm_jenjang from program_studi left join jenjang on jenjang.id_jenjang = program_studi.id_jenjang 
		where id_program_studi={$idprodi}";
$db->Query($sql);
while ($r=$db->FetchRow()) {
    $idfakultas = $r[0];
    $nm_jenjang = $r[1];
}

function getSKSMAX($ips)
{
    if ($ips>=3) {
        return 24;
    } elseif ($ips>=2.5) {
        return 22;
    } elseif ($ips>=2) {
        return 20;
    } elseif ($ips>=1.5) {
        return 18;
    } else {
        return 15;
    }
}

$sql =
"SELECT a.*,
	CASE
		WHEN c.gelar_belakang IS NOT NULL THEN trim(c.gelar_depan || ' ' || upper(c.nm_pengguna)|| ', ' || c.gelar_belakang)
		ELSE trim(c.gelar_depan || ' ' || upper(c.nm_pengguna))
	END AS nm_dosen,
	c.username
FROM
 (SELECT h.username,
		 h.nm_pengguna,
		 ipk.skstotal,
		 ipk.ipk,
		 ips.sksmtk,
		 ips.ips,
		 f.nm_program_studi,
		 i.nm_jenjang,
		 g.id_dosen,
		 e.nm_mata_kuliah,
		 e.kd_mata_kuliah,
		 d.kredit_semester,
		 a.nilai_huruf
  FROM pengambilan_mk a
  LEFT JOIN mahasiswa b ON a.id_mhs = b.id_mhs
  LEFT JOIN kurikulum_mk d ON a.id_kurikulum_mk = d.id_kurikulum_mk
  LEFT JOIN mata_kuliah e ON d.id_mata_kuliah = e.id_mata_kuliah
  LEFT JOIN program_studi f ON b.id_program_studi = f.id_program_studi
  LEFT JOIN
	  (SELECT *
	   FROM dosen_wali
	   WHERE status_dosen_wali = 1
		   AND id_mhs IN
			   (SELECT id_mhs
				FROM mahasiswa
				WHERE id_program_studi = '{$idprodi}' ) ) g ON b.id_mhs = g.id_mhs
  LEFT JOIN pengguna h ON b.id_pengguna = h.id_pengguna
  LEFT JOIN jenjang i ON f.id_jenjang = i.id_jenjang
  LEFT JOIN
	  (SELECT a.id_mhs,
			  sum(d.kredit_semester) sksmtk,
			  round(sum(d.kredit_semester *(CASE a.nilai_huruf
												WHEN 'A' THEN 4
												WHEN 'AB' THEN 3.5
												WHEN 'B' THEN 3
												WHEN 'BC' THEN 2.5
												WHEN 'C' THEN 2
												WHEN 'D' THEN 1
												WHEN 'E' THEN 0
												ELSE 0
											END))/ sum(d.kredit_semester), 2) ips
	   FROM pengambilan_mk a
	   LEFT JOIN mahasiswa b ON a.id_mhs = b.id_mhs
	   LEFT JOIN kurikulum_mk d ON a.id_kurikulum_mk = d.id_kurikulum_mk
	   LEFT JOIN mata_kuliah e ON d.id_mata_kuliah = e.id_mata_kuliah
	   LEFT JOIN program_studi f ON b.id_program_studi = f.id_program_studi
	   WHERE a.id_semester = '{$id_semester}'
		   AND f.id_program_studi = '{$idprodi}'
		   AND a.status_apv_pengambilan_mk = 1
		   AND a.status_hapus = 0
		   AND a.status_pengambilan_mk != 0
	   GROUP BY a.id_mhs
	   ORDER BY a.id_mhs) ips ON a.id_mhs = ips.id_mhs
  LEFT JOIN
	  (SELECT a.id_mhs,
			  sum(a.kredit_semester) skstotal,
			  round(sum(a.kredit_semester *(CASE a.nilai_huruf
												WHEN 'A' THEN 4
												WHEN 'AB' THEN 3.5
												WHEN 'B' THEN 3
												WHEN 'BC' THEN 2.5
												WHEN 'C' THEN 2
												WHEN 'D' THEN 1
											END))/ sum(a.kredit_semester), 2) ipk
	   FROM
		   (SELECT a.id_mhs,
				   e.nm_mata_kuliah,
				   d.kredit_semester,
				   a.nilai_huruf
			FROM
				(SELECT a.*,
						row_number() over(PARTITION BY a.id_mhs, e.nm_mata_kuliah
										  ORDER BY nilai_huruf) rangking
				 FROM pengambilan_mk a
				 LEFT JOIN kurikulum_mk d ON a.id_kurikulum_mk = d.id_kurikulum_mk
				 LEFT JOIN mata_kuliah e ON d.id_mata_kuliah = e.id_mata_kuliah
				 LEFT JOIN mahasiswa b ON a.id_mhs = b.id_mhs
				 LEFT JOIN program_studi f ON b.id_program_studi = f.id_program_studi
				 WHERE a.nilai_huruf < 'E'
					 AND a.nilai_huruf IS NOT NULL
					 AND a.status_apv_pengambilan_mk = 1
					 AND a.status_hapus = 0
					 AND a.status_pengambilan_mk != 0
					 AND a.id_semester IS NOT NULL
					 AND f.id_program_studi = '{$idprodi}' ) a
			LEFT JOIN kurikulum_mk d ON a.id_kurikulum_mk = d.id_kurikulum_mk
			LEFT JOIN mata_kuliah e ON d.id_mata_kuliah = e.id_mata_kuliah
			WHERE rangking = 1 ) a
	   LEFT JOIN mahasiswa b ON a.id_mhs = b.id_mhs
	   LEFT JOIN program_studi f ON b.id_program_studi = f.id_program_studi
	   WHERE f.id_program_studi = '{$idprodi}'
	   GROUP BY a.id_mhs
	   ORDER BY a.id_mhs) ipk ON a.id_mhs = ipk.id_mhs
  WHERE f.id_program_studi = '{$idprodi}'
	  AND a.id_semester = '{$id_semester}'
  GROUP BY a.id_mhs,
		   h.username,
		   h.nm_pengguna,
		   ipk.skstotal,
		   ipk.ipk,
		   ips.sksmtk,
		   ips.ips,
		   f.nm_program_studi,
		   i.nm_jenjang,
		   g.id_dosen,
		   e.nm_mata_kuliah,
		   e.kd_mata_kuliah,
		   d.kredit_semester,
		   a.nilai_huruf) a
LEFT JOIN dosen b ON a.id_dosen = b.id_dosen
LEFT JOIN pengguna c ON b.id_pengguna = c.id_pengguna
ORDER BY a.nm_program_studi,
	  a.username,
	  a.kd_mata_kuliah";

$db->Query($sql);
$i=0;
while ($r=$db->FetchRow()) {
    $i++;
    $kdep=$r[6];

    $dosenwali=$r[13];
    $nim=$r[0];
    $nmmhs=$r[1];
    $nmprodi=$r[6];
    $kdmtk=$r[10];
    $nmmtk=$r[9];
    $sksmtk=$r[11];
    $nipdosen=$r[14];
    $nilai=$r[12];
    $ipk=$r[3];
    $ips=$r[5];
    $sksips=$r[4];

    $jumsks=$r[2];
    $CT[$kdep][$nim]['nama']=$nmmhs;
    $CT[$kdep][$nim]['ips']=$ips;
    $CT[$kdep][$nim]['sksips']=$sksips;
    $CT[$kdep][$nim]['ipk']=$ipk;
    $CT[$kdep][$nim]['jumsks']=$jumsks;
    $CT[$kdep][$nim]['prodi']=$nmprodi;
    $CT[$kdep][$nim]['nipdosen']=$nipdosen;
    $CT[$kdep][$nim]['nmdosen']=$dosenwali;
    $CT[$kdep][$nim]['khs'][$kdmtk]['nama']=$nmmtk;
    $CT[$kdep][$nim]['khs'][$kdmtk]['sks']=$sksmtk;
    $CT[$kdep][$nim]['khs'][$kdmtk]['nilai']=$nilai;
}

$t = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

/*$t->AliasNbPages();*/
$rek=0;
    
if ($CT) {
    foreach ($CT as $kdep => $MHS) {
        $nmdep=$kdep;
        $t->AddPage();
        $t->Bookmark($nmdep);
        $t->Ln(15);
        $t->SetFont('Times', 'B', 24);
        $t->Cell(0, 15, "KARTU HASIL STUDI SEMESTER $nm_semester $thn_semester", 0, 1, 'C');
        $t->Ln(80);
        $t->SetFont('Times', 'B', 36);
        $t->Cell(0, 15, $nmdep, 0, 1, 'C');
        $t->Ln(80);
        $t->SetFont('Times', 'B', 16);
        $t->Cell(0, 10, $fakultas, 0, 1, 'C');
        $t->SetFont('Times', 'B', 18);
        $t->Cell(0, 10, strtoupper($nama_pt), 0, 1, 'C');
        $t->Cell(0, 10, date('Y'), 0, 1, 'C');

        foreach ($MHS as $nim => $datamhs) {
            $KHS=$datamhs['khs'];
            $nmmhs=$datamhs['nama'];
            $prodi=$datamhs['prodi'];
            $ipk=$datamhs['ipk'];
            $jumsks=$datamhs['jumsks'];
            $ips=$datamhs['ips'];
            $sksips=$datamhs['sksips'];
            $dosenwali=$datamhs['nmdosen'];
            $nipdosen=$datamhs['nipdosen'];
            $t->AddPage();
            $t->Bookmark($nim." ".$nmmhs, 2, -1);
            $t->SetFont('Times', 'B', 12);
            $t->Cell(0, 6, strtoupper($nama_pt), 0, 1, 'L');
            $t->SetFont('Times', '', 12);
            $t->Cell(0, 6, $fakultas, '0', 1, 'L');
            $t->Cell(0, 6, $nm_jenjang.' - '.$prodi, 'B', 1, 'L');
            $t->Ln(5);
            $t->SetFont('Times', 'BU', 12);
            $t->Cell(0, 6, "KARTU HASIL STUDI TAHUN AJARAN $thn_semester SEMESTER ".strtoupper($nm_semester)." ", 0, 1, 'C');
            $t->Ln(10);
            $t->SetFont('Times', '', 11);
            $t->Cell(40, 6, " N I M", 0, 0, 'L');
            $t->Cell(80, 6, ": $nim", 0, 0, 'L');
            $t->Ln();
            $t->Cell(40, 6, " Nama Mahasiswa", 0, 0, 'L');
            $t->Cell(80, 6, ": $nmmhs", 0, 0, 'L');
            $t->Ln();
            $t->Cell(40, 6, " Dosen Wali", 0, 0, 'L');
            $t->Cell(0, 6, ": $dosenwali", 0, 1, 'L');
            $t->Ln(2);
            $t->Cell(30, 10, "KODE MK", 1, 0, 'L');
            $t->Cell(115, 10, "MATA KULIAH", 'TBR', 0, 'L');
            $t->Cell(25, 10, "SKS", 'TBR', 0, 'C');
            $t->Cell(25, 10, "NILAI", 'TBR', 1, 'C');

            $totalsks=0;
            //$totalbobot=0;
            if ($KHS) {
				$t->SetFont('Times', '', 10);
                foreach ($KHS as $kdmtk => $data) {
                    $t->Cell(30, 6, $kdmtk, 'LR', 0, 'L');
                    $t->Cell(115, 6, $data['nama'], 'R', 0, 'L');
                    $t->Cell(25, 6, $data['sks'], 'R', 0, 'C');
                    $t->Cell(25, 6, $data['nilai'], 'R', 1, 'C');
                    $totalsks += $data['sks'];
                    //$totalbobot += $data['sks']*$BOBOT[$data['nilai']];
				}
				$t->SetFont('Times', '', 11);
            }


            $sksmax=getSKSMAX($ips);
            $t->Cell(145, 10, "IP SEMESTER INI : ".number_format($ips, 2), 1, 0, 'C');
            $t->Cell(25, 10, $sksips, 1, 0, 'C');
            $t->Cell(25, 10, "", 1, 1, 'C');
            $t->Cell(195, 5, "", 'LR', 1, 'C');
            $t->Cell(195, 6, "Tanpa mata kuliah dengan nilai E, hasil studi sampai dengan semester ini ialah:", 'LR', 1, 'L');
            $t->Cell(195, 6, "JUMLAH SKS YANG TELAH DITEMPUH = $jumsks dengan IPK : ".number_format($ipk, 2), 'LR', 1, 'L');
            $t->Cell(195, 6, "", 'LR', 1, 'C');
            $t->Cell(115, 6, "Catatan dosen wali :", 'L', 0, 'L');

			$BULAN[1]="Januari";
            $BULAN[2]="Februari";
            $BULAN[3]="Maret";
            $BULAN[4]="April";
            $BULAN[5]="Mei";
            $BULAN[6]="Juni";
            $BULAN[7]="Juli";
            $BULAN[8]="Agustus";
            $BULAN[9]="September";
            $BULAN[10]="Oktober";
            $BULAN[11]="November";
            $BULAN[12]="Desember";

            $tanggal=date('j')." ".$BULAN[date('n')]." ".date('Y');
            $t->Cell(80, 6, "{$PT->kota_pt}, $tanggal", 'R', 1, 'C');
            $t->Cell(195, 6, "Mengingat prestasi IPS Saudara = ".number_format($ips, 2)." maka,", 'LR', 1, 'L');
            $t->Cell(195, 6, "beban SKS maksimum yang dapat diambil = $sksmax SKS", 'LR', 1, 'L');
            $t->Cell(195, 10, "", 'LR', 1, 'L');
            $t->Cell(195, 6, "Lembar: 1. untuk mahasiswa", 'LR', 1, 'L');
            $t->Cell(195, 6, "Lembar: 2. untuk dosen wali", 'LR', 1, 'L');
            $t->Cell(115, 6, "Lembar: 3. untuk prodi", 'L', 0, 'L');
            $t->SetFont('Times', 'U', 12);
            
			$t->Cell(80, 6, "(  $dosenwali   )", 'R', 1, 'C');
			$t->SetFont('Times', '', 12);
			$t->Cell(115, 10, "", 'L', 0, 'C');
			$t->Cell(80, 10, "NIP.$nipdosen", 'R', 1, 'C');
            
            $t->Cell(195, 10, "", 'LBR', 1, 'C');
            $t->SetFont('Times', 'I', 8);
            $rek++;
            $t->Cell(195, 6, 'Generated at ' . strftime('%D %r'), 0, 1, 'R');
        }
    }
}

$t->SetDisplayMode(50);
$t->Output();