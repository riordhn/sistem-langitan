<?php
require('../../../config.php');
$db = new MyOracle();

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages().' - '.date("Y-m-d H:i:s", time()), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('Plot Penguji Sidang');
$pdf->SetSubject('Plot Penguji Sidang');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logounair.gif";
$logo_size = "25";
$title = "UNIVERSITAS AIRLANGGA";

$kdfak=$user->ID_FAKULTAS;
$tgl = $_GET['tgl'];
$smt = $_GET['id_smt'];

$smt="select id_semester, ', SEMESTER '||upper(nm_semester)||' '||tahun_ajaran as nm_semester from semester where id_semester=$smt";
$result = $db->Query($smt)or die("salah kueri smt ");
while($r = $db->FetchRow()) {
	$smtaktif = $r[0];
	$nm_semester = $r[1];
}

$header = "select a.nm_jenjang, b.nm_program_studi, f.nm_fakultas, f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, 
			f.website_fakultas, f.email_fakultas
			from jenjang a, program_studi b, fakultas f where a.id_jenjang=b.id_jenjang and f.id_fakultas=b.id_fakultas and f.id_fakultas=$kdfak";
$result = $db->Query($header)or die("salah kueri header");
while($r = $db->FetchRow()) {
	$jenjang = $r[0];
	$prodi = $r[1];
	$fak = $r[2];
	$alm_fak = $r[3];
	$pos_fak = $r[4];
	$tel_fak = $r[5];
	$fax_fak = $r[6];
	$web_fak = $r[7];
	$eml_fak = $r[8];
}

$content = "FAKULTAS ".strtoupper($fak)."\n\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(5, 35, 5);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 15);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 8);

// add a page
$pdf->AddPage('L', 'A4');

$html = '
<p align="center"><strong>PLOTING PENGUJI SIDANG TA/SKRIPSI/THESIS/DESERTASI TANGGAL '.strtoupper($tgl).''.$nm_semester.'</strong></p>
<table border="1" cellspacing="0" cellpadding="1">
	<thead>
	   <tr>	
			<th width="80" rowspan="2" align="center" bgcolor="#bcbcbc"><strong>Penguji</strong></th>
			<th width="936" colspan="12" align="center" bgcolor="#bcbcbc"><strong>Jam Mulai Ujian</strong></th>
	   </tr>
	   <tr>
			<th width="78" align="center" bgcolor="#bcbcbc"><strong>08:00</strong></th>
			<th width="78" align="center" bgcolor="#bcbcbc"><strong>09:00</strong></th>
			<th width="78" align="center" bgcolor="#bcbcbc"><strong>10:00</strong></th>
			<th width="78" align="center" bgcolor="#bcbcbc"><strong>11:00</strong></th>
			<th width="78" align="center" bgcolor="#bcbcbc"><strong>12:00</strong></th>
			<th width="78" align="center" bgcolor="#bcbcbc"><strong>13:00</strong></th>
			<th width="78" align="center" bgcolor="#bcbcbc"><strong>14:00</strong></th>
			<th width="78" align="center" bgcolor="#bcbcbc"><strong>15:00</strong></th>
			<th width="78" align="center" bgcolor="#bcbcbc"><strong>16:00</strong></th>
			<th width="78" align="center" bgcolor="#bcbcbc"><strong>17:00</strong></th>
			<th width="78" align="center" bgcolor="#bcbcbc"><strong>18:00</strong></th>
			<th width="78" align="center" bgcolor="#bcbcbc"><strong>19:00</strong></th>
	   </tr>
	</thead>
	<tbody>';

$datapengawas="SELECT nm_pengguna,
MAX(DECODE ( kdjam , 08, tim )) jam0,
MAX(DECODE ( kdjam , 09, tim )) jam1,
MAX(DECODE ( kdjam , 10, tim )) jam2,
MAX(DECODE ( kdjam , 11, tim )) jam3,
MAX(DECODE ( kdjam , 12, tim )) jam4,
MAX(DECODE ( kdjam , 13, tim )) jam5,
MAX(DECODE ( kdjam , 14, tim )) jam6,
MAX(DECODE ( kdjam , 15, tim )) jam7,
MAX(DECODE ( kdjam , 16, tim )) jam8,
MAX(DECODE ( kdjam , 17, tim )) jam9,
MAX(DECODE ( kdjam , 18, tim )) jam10,
MAX(DECODE ( kdjam , 19, tim )) jam11
FROM
(
select kdjam,nm_pengguna,wm_concat(' '||tgl_ujian||'<br>('||nm_mata_kuliah||' - '||nm_ruangan||') <br><br>') as tim
from
(
select umk.tgl_ujian,substr(umk.jam_mulai,1,2) as kdjam,pgg.nm_pengguna,mk.nm_mata_kuliah,rg.nm_ruangan
from ujian_mk umk
left join jadwal_ujian_mk jad on umk.id_ujian_mk=jad.id_ujian_mk
left join ruangan rg on jad.id_ruangan=rg.id_ruangan
left join tim_pengawas_ujian tim on jad.id_jadwal_ujian_mk=tim.id_jadwal_ujian_mk
left join pengguna pgg on pgg.id_pengguna=tim.id_pengguna
left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
left join nama_kelas nmk on nmk.id_nama_kelas=kmk.no_kelas_mk
left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
where to_char(umk.tgl_ujian,'DD-MM-YYYY')='".$tgl."' and umk.id_kegiatan=71 and
umk.id_fakultas=$kdfak and umk.id_semester=$smtaktif and length(umk.jam_mulai) = 5
and umk.tgl_ujian is not null and tim.id_pengguna in (select id_pengguna from dosen)
)
group by kdjam,nm_pengguna
)
group by nm_pengguna";
$result = $db->Query($datapengawas)or die("salah kueri datapengawas ");
while($r = $db->FetchRow()) {
	$nm_pengguna = $r[0];
	$jam0 = $r[1];
	$jam1 = $r[2];
	$jam2= $r[3];
	$jam3 = $r[4];
	$jam4 = $r[5];
	$jam5 = $r[6];
	$jam6 = $r[7];
	$jam7 = $r[8];
	$jam8 = $r[9];
	$jam9 = $r[10];
	$jam10 = $r[11];
	$jam11 = $r[12];

$html .= '
		  <tr>
			<td width="80" align="center"><strong>'.$nm_pengguna.'</strong></td>';
			if(strstr($jam0, ", ")){
			$html .= '
			<td width="78" bgcolor="#ff828e">'.$jam0.'</td>';
			} else {
			$html .= '
			<td width="78">'.$jam0.'</td>';
			}
			if(strstr($jam1, ", ")){
			$html .= '
			<td width="78" bgcolor="#ff828e">'.$jam1.'</td>';
			} else {
			$html .= '
			<td width="78">'.$jam1.'</td>';
			}
			if(strstr($jam2, ", ")){
			$html .= '
			<td width="78" bgcolor="#ff828e">'.$jam2.'</td>';
			} else {
			$html .= '
			<td width="78">'.$jam2.'</td>';
			}
			if(strstr($jam3, ", ")){
			$html .= '
			<td width="78" bgcolor="#ff828e">'.$jam3.'</td>';
			} else {
			$html .= '
			<td width="78">'.$jam3.'</td>';
			}
			if(strstr($jam4, ", ")){
			$html .= '
			<td width="78" bgcolor="#ff828e">'.$jam4.'</td>';
			} else {
			$html .= '
			<td width="78">'.$jam4.'</td>';
			}
			if(strstr($jam5, ", ")){
			$html .= '
			<td width="78" bgcolor="#ff828e">'.$jam5.'</td>';
			} else {
			$html .= '
			<td width="78">'.$jam5.'</td>';
			}
			if(strstr($jam6, ", ")){
			$html .= '
			<td width="78" bgcolor="#ff828e">'.$jam6.'</td>';
			} else {
			$html .= '
			<td width="78">'.$jam6.'</td>';
			}
			if(strstr($jam7, ", ")){
			$html .= '
			<td width="78" bgcolor="#ff828e">'.$jam7.'</td>';
			} else {
			$html .= '
			<td width="78">'.$jam7.'</td>';
			}
			if(strstr($jam8, ", ")){
			$html .= '
			<td width="78" bgcolor="#ff828e">'.$jam8.'</td>';
			} else {
			$html .= '
			<td width="78">'.$jam8.'</td>';
			}
			if(strstr($jam9, ", ")){
			$html .= '
			<td width="78" bgcolor="#ff828e">'.$jam9.'</td>';
			} else {
			$html .= '
			<td width="78">'.$jam9.'</td>';
			}
			if(strstr($jam10, ", ")){
			$html .= '
			<td width="78" bgcolor="#ff828e">'.$jam10.'</td>';
			} else {
			$html .= '
			<td width="78">'.$jam10.'</td>';
			}
			if(strstr($jam11, ", ")){
			$html .= '
			<td width="78" bgcolor="#ff828e">'.$jam11.'</td>';
			} else {
			$html .= '
			<td width="78">'.$jam11.'</td>';
			}
$html .= '
		  </tr>';
}
$html .= '
	</tbody>
</table>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('PLOTING PENGUJI SIDANG '.strtoupper($tgl).'.pdf', 'I');

?>
