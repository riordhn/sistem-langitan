<?php
require('../../../config.php');
$db = new MyOracle();
$db1 = new MyOracle();
$db2 = new MyOracle();
$db3 = new MyOracle();

$smt = $_GET['smt'];
$thn = $_GET['thn'];
$fak = $_GET['fak'];
$prodi = $_GET['prodi'];

$nm_smt="select lower(nm_semester) as sem from semester where id_semester=".$smt."";
$result1 = $db1->Query($nm_smt)or die("salah kueri 0 ");
while($r1 = $db1->FetchRow()) {
	$nm_file_smt = $r1[0];
}

if ($prodi=='all') {
$dataprodi1="select lower(nm_fakultas) as fak from fakultas where id_fakultas=".$fak."";
$result = $db->Query($dataprodi1)or die("salah kueri 1 ");
while($r = $db->FetchRow()) {
	$nm_file = $r[0];
}

header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=dist-ipk-mhs-fakultas-".$nm_file."-".$nm_file_smt."-".$thn.".xls");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data";

echo '
		<table width="100%" border="1" cellspacing="0" cellpadding="0">
           <tr>
             <td width="10%" bgcolor="lightgrey" align="center"><b>Jenjang</b></td>
             <td width="25%" bgcolor="lightgrey" align="center"><b>Program Studi</b></td>
             <td width="8%" bgcolor="lightgrey" align="center"><b>IPK<2.00</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>2.00<=IPK<2.50</b></td>
             <td width="8%" bgcolor="lightgrey" align="center"><b>2.50<=IPK<2.75</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>2.75<=IPK<3.00</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>3.00<=IPK<3.25</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>3.25<=IPK<3.50</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>3.50<=IPK<3.75</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>IPK>=3.75</b></td>
           </tr>';

$all="select nm_jenjang, nm_program_studi,
			sum(A) as jumlah_A,
			round(sum(A)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_A,
			sum(AB) as jumlah_AB,
			round(sum(AB)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_AB,
			sum(B) as jumlah_B,
			round(sum(B)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_B,
			sum(BC) as jumlah_BC,
			round(sum(BC)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_BC,
			sum(C) as jumlah_C,
			round(sum(C)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_C,
			sum(D) as jumlah_D,
			round(sum(D)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_D,
			sum(E) as jumlah_E,
			round(sum(E)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_E,
			sum(F) as jumlah_F,
			round(sum(F)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_F
			from
			(select j.nm_jenjang, ps.nm_program_studi,
			case when ipk_mhs_status<2.00 then 1 else 0 end as A,
			case when ipk_mhs_status between 2.00 and 2.49 then 1 else 0 end as AB,
			case when ipk_mhs_status between 2.50 and 2.74 then 1 else 0 end as B,
			case when ipk_mhs_status between 2.75 and 2.99 then 1 else 0 end as BC,
			case when ipk_mhs_status between 3.00 and 3.24 then 1 else 0 end as C,
			case when ipk_mhs_status between 3.25 and 3.49 then 1 else 0 end as D,
			case when ipk_mhs_status between 3.50 and 3.74 then 1 else 0 end as E,
			case when ipk_mhs_status>=3.75 then 1 else 0 end as F
			from mhs_status mst
			left join mahasiswa mhs on mst.id_mhs=mhs.id_mhs
			left join program_studi ps on ps.id_program_studi=mhs.id_program_studi
			left join jenjang j on j.id_jenjang=ps.id_jenjang
			left join fakultas f on f.id_fakultas=ps.id_fakultas
			left join status_pengguna sp on sp.id_status_pengguna=mhs.status_akademik_mhs 
			where mst.id_semester=".$smt." and ps.id_fakultas=".$fak.") group by nm_jenjang, nm_program_studi order by nm_jenjang, nm_program_studi";
$result2 = $db2->Query($all)or die("salah kueri 2 ");
while($r2 = $db2->FetchRow()) {
	$jenjang = $r2[0];
	$nm_prodi = $r2[1];
	$nilai_a = $r2[2];
	$nilai_ab = $r2[3];
	$nilai_b = $r2[4];
	$nilai_bc = $r2[5];
	$nilai_c = $r2[6];
	$nilai_d = $r2[7];
	$nilai_e = $r2[8];
	$nilai_f = $r2[9];
}

echo '
			<tr>
             <td>'.$jenjang.'</td>
             <td>'.$nm_prodi.'</td>
             <td>'.$nilai_a.'%</td>
			 <td>'.$nilai_ab.'%</td>
             <td>'.$nilai_b.'%</td>
			 <td>'.$nilai_bc.'%</td>
			 <td>'.$nilai_c.'%</td>
			 <td>'.$nilai_d.'%</td>
			 <td>'.$nilai_e.'%</td>
			 <td>'.$nilai_f.'%</td>
           </tr>
		   <tr><td colspan="10"></td></tr>
           <tr>
             <td width="35%" bgcolor="lightgrey" align="center" colspan="2"><b>Tahun Angkatan</b></td>
             <td width="8%" bgcolor="lightgrey" align="center"><b>IPK<2.00</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>2.00<=IPK<2.50</b></td>
             <td width="8%" bgcolor="lightgrey" align="center"><b>2.50<=IPK<2.75</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>2.75<=IPK<3.00</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>3.00<=IPK<3.25</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>3.25<=IPK<3.50</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>3.50<=IPK<3.75</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>IPK>=3.75</b></td>
           </tr>';

$agk="select thn_angkatan_mhs,
			sum(A) as jumlah_A,
			round(sum(A)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_A,
			sum(AB) as jumlah_AB,
			round(sum(AB)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_AB,
			sum(B) as jumlah_B,
			round(sum(B)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_B,
			sum(BC) as jumlah_BC,
			round(sum(BC)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_BC,
			sum(C) as jumlah_C,
			round(sum(C)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_C,
			sum(D) as jumlah_D,
			round(sum(D)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_D,
			sum(E) as jumlah_E,
			round(sum(E)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_E,
			sum(F) as jumlah_F,
			round(sum(F)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_F
			from
			(select mhs.thn_angkatan_mhs,
			case when ipk_mhs_status<2.00 then 1 else 0 end as A,
			case when ipk_mhs_status between 2.00 and 2.49 then 1 else 0 end as AB,
			case when ipk_mhs_status between 2.50 and 2.74 then 1 else 0 end as B,
			case when ipk_mhs_status between 2.75 and 2.99 then 1 else 0 end as BC,
			case when ipk_mhs_status between 3.00 and 3.24 then 1 else 0 end as C,
			case when ipk_mhs_status between 3.25 and 3.49 then 1 else 0 end as D,
			case when ipk_mhs_status between 3.50 and 3.74 then 1 else 0 end as E,
			case when ipk_mhs_status>=3.75 then 1 else 0 end as F
			from mhs_status mst
			left join mahasiswa mhs on mst.id_mhs=mhs.id_mhs
			left join program_studi ps on ps.id_program_studi=mhs.id_program_studi
			left join jenjang j on j.id_jenjang=ps.id_jenjang
			left join fakultas f on f.id_fakultas=ps.id_fakultas
			left join status_pengguna sp on sp.id_status_pengguna=mhs.status_akademik_mhs 
			where mst.id_semester=".$smt." and ps.id_fakultas=".$fak.") group by thn_angkatan_mhs order by thn_angkatan_mhs desc";
$result3 = $db3->Query($agk)or die("salah kueri 3 ");
while($r3 = $db3->FetchRow()) {
	$thn_agk = $r3[0];
	$nilai_a = $r3[1];
	$nilai_ab = $r3[2];
	$nilai_b = $r3[3];
	$nilai_bc = $r3[4];
	$nilai_c = $r3[5];
	$nilai_d = $r3[6];
	$nilai_e = $r3[7];
	$nilai_f = $r3[8];

echo '
			<tr>
             <td colspan="2" align="center">'.$thn_agk.'</td>
             <td>'.$nilai_a.'%</td>
			 <td>'.$nilai_ab.'%</td>
             <td>'.$nilai_b.'%</td>
			 <td>'.$nilai_bc.'%</td>
			 <td>'.$nilai_c.'%</td>
			 <td>'.$nilai_d.'%</td>
			 <td>'.$nilai_e.'%</td>
			 <td>'.$nilai_f.'%</td>
           </tr>';
}
echo '
   	 </table>';

} else {
$dataprodi1="select lower(nm_jenjang)||'-'||lower(nm_program_studi) as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_program_studi=".$prodi."";
$result = $db->Query($dataprodi1)or die("salah kueri 1 ");
while($r = $db->FetchRow()) {
	$nm_file = $r[0];
}

header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=dist-ipk-mhs-prodi-".$nm_file."-".$nm_file_smt."-".$thn.".xls");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data";

echo '
		<table width="100%" border="1" cellspacing="0" cellpadding="0">
           <tr>
             <td width="10%" bgcolor="lightgrey" align="center"><b>Jenjang</b></td>
             <td width="25%" bgcolor="lightgrey" align="center"><b>Program Studi</b></td>
             <td width="8%" bgcolor="lightgrey" align="center"><b>IPK<2.00</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>2.00<=IPK<2.50</b></td>
             <td width="8%" bgcolor="lightgrey" align="center"><b>2.50<=IPK<2.75</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>2.75<=IPK<3.00</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>3.00<=IPK<3.25</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>3.25<=IPK<3.50</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>3.50<=IPK<3.75</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>IPK>=3.75</b></td>
           </tr>';

$all="select nm_jenjang, nm_program_studi,
			sum(A) as jumlah_A,
			round(sum(A)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_A,
			sum(AB) as jumlah_AB,
			round(sum(AB)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_AB,
			sum(B) as jumlah_B,
			round(sum(B)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_B,
			sum(BC) as jumlah_BC,
			round(sum(BC)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_BC,
			sum(C) as jumlah_C,
			round(sum(C)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_C,
			sum(D) as jumlah_D,
			round(sum(D)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_D,
			sum(E) as jumlah_E,
			round(sum(E)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_E,
			sum(F) as jumlah_F,
			round(sum(F)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_F
			from
			(select j.nm_jenjang, ps.nm_program_studi,
			case when ipk_mhs_status<2.00 then 1 else 0 end as A,
			case when ipk_mhs_status between 2.00 and 2.49 then 1 else 0 end as AB,
			case when ipk_mhs_status between 2.50 and 2.74 then 1 else 0 end as B,
			case when ipk_mhs_status between 2.75 and 2.99 then 1 else 0 end as BC,
			case when ipk_mhs_status between 3.00 and 3.24 then 1 else 0 end as C,
			case when ipk_mhs_status between 3.25 and 3.49 then 1 else 0 end as D,
			case when ipk_mhs_status between 3.50 and 3.74 then 1 else 0 end as E,
			case when ipk_mhs_status>=3.75 then 1 else 0 end as F
			from mhs_status mst
			left join mahasiswa mhs on mst.id_mhs=mhs.id_mhs
			left join program_studi ps on ps.id_program_studi=mhs.id_program_studi
			left join jenjang j on j.id_jenjang=ps.id_jenjang
			left join fakultas f on f.id_fakultas=ps.id_fakultas
			left join status_pengguna sp on sp.id_status_pengguna=mhs.status_akademik_mhs 
			where mst.id_semester=".$smt." and ps.id_program_studi=".$prodi.") group by nm_jenjang, nm_program_studi order by nm_jenjang, nm_program_studi";
$result2 = $db2->Query($all)or die("salah kueri 2 ");
while($r2 = $db2->FetchRow()) {
	$jenjang = $r2[0];
	$nm_prodi = $r2[1];
	$nilai_a = $r2[2];
	$nilai_ab = $r2[3];
	$nilai_b = $r2[4];
	$nilai_bc = $r2[5];
	$nilai_c = $r2[6];
	$nilai_d = $r2[7];
	$nilai_e = $r2[8];
	$nilai_f = $r2[9];
}

echo '
			<tr>
             <td>'.$jenjang.'</td>
             <td>'.$nm_prodi.'</td>
             <td>'.$nilai_a.'%</td>
			 <td>'.$nilai_ab.'%</td>
             <td>'.$nilai_b.'%</td>
			 <td>'.$nilai_bc.'%</td>
			 <td>'.$nilai_c.'%</td>
			 <td>'.$nilai_d.'%</td>
			 <td>'.$nilai_e.'%</td>
			 <td>'.$nilai_f.'%</td>
           </tr>
		   <tr><td colspan="10"></td></tr>
           <tr>
             <td width="35%" bgcolor="lightgrey" align="center" colspan="2"><b>Tahun Angkatan</b></td>
             <td width="8%" bgcolor="lightgrey" align="center"><b>IPK<2.00</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>2.00<=IPK<2.50</b></td>
             <td width="8%" bgcolor="lightgrey" align="center"><b>2.50<=IPK<2.75</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>2.75<=IPK<3.00</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>3.00<=IPK<3.25</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>3.25<=IPK<3.50</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>3.50<=IPK<3.75</b></td>
			 <td width="8%" bgcolor="lightgrey" align="center"><b>IPK>=3.75</b></td>
           </tr>';

$agk="select thn_angkatan_mhs,
			sum(A) as jumlah_A,
			round(sum(A)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_A,
			sum(AB) as jumlah_AB,
			round(sum(AB)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_AB,
			sum(B) as jumlah_B,
			round(sum(B)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_B,
			sum(BC) as jumlah_BC,
			round(sum(BC)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_BC,
			sum(C) as jumlah_C,
			round(sum(C)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_C,
			sum(D) as jumlah_D,
			round(sum(D)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_D,
			sum(E) as jumlah_E,
			round(sum(E)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_E,
			sum(F) as jumlah_F,
			round(sum(F)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)+sum(F)),2) as persen_F
			from
			(select mhs.thn_angkatan_mhs,
			case when ipk_mhs_status<2.00 then 1 else 0 end as A,
			case when ipk_mhs_status between 2.00 and 2.49 then 1 else 0 end as AB,
			case when ipk_mhs_status between 2.50 and 2.74 then 1 else 0 end as B,
			case when ipk_mhs_status between 2.75 and 2.99 then 1 else 0 end as BC,
			case when ipk_mhs_status between 3.00 and 3.24 then 1 else 0 end as C,
			case when ipk_mhs_status between 3.25 and 3.49 then 1 else 0 end as D,
			case when ipk_mhs_status between 3.50 and 3.74 then 1 else 0 end as E,
			case when ipk_mhs_status>=3.75 then 1 else 0 end as F
			from mhs_status mst
			left join mahasiswa mhs on mst.id_mhs=mhs.id_mhs
			left join program_studi ps on ps.id_program_studi=mhs.id_program_studi
			left join jenjang j on j.id_jenjang=ps.id_jenjang
			left join fakultas f on f.id_fakultas=ps.id_fakultas
			left join status_pengguna sp on sp.id_status_pengguna=mhs.status_akademik_mhs 
			where mst.id_semester=".$smt." and ps.id_program_studi=".$prodi.") group by thn_angkatan_mhs order by thn_angkatan_mhs desc";
$result3 = $db3->Query($agk)or die("salah kueri 3 ");
while($r3 = $db3->FetchRow()) {
	$thn_agk = $r3[0];
	$nilai_a = $r3[1];
	$nilai_ab = $r3[2];
	$nilai_b = $r3[3];
	$nilai_bc = $r3[4];
	$nilai_c = $r3[5];
	$nilai_d = $r3[6];
	$nilai_e = $r3[7];
	$nilai_f = $r3[8];

echo '
			<tr>
             <td colspan="2" align="center">'.$thn_agk.'</td>
             <td>'.$nilai_a.'%</td>
			 <td>'.$nilai_ab.'%</td>
             <td>'.$nilai_b.'%</td>
			 <td>'.$nilai_bc.'%</td>
			 <td>'.$nilai_c.'%</td>
			 <td>'.$nilai_d.'%</td>
			 <td>'.$nilai_e.'%</td>
			 <td>'.$nilai_f.'%</td>
           </tr>';
}
echo '
   	 </table>';

}
?>