<?php
require('../../../config.php');
$db = new MyOracle();
$db1 = new MyOracle();

$prodi = $_GET['prodi'];
$nm_prodi = $_GET['nm_prodi'];
$angk = $_GET['angk'];
$fak = "select id_fakultas from program_studi where id_program_studi = $prodi";
$hasil = $db1->Query($fak)or die("salah kueri 00 ");
$h = $db1->FetchRow();
$kdfak = $h[0];

header("Content-type: application/vnd.msword");
header("Content-disposition: attachment; filename=DATA-MHS-".$angk."-".$nm_prodi.".doc");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data";

if ($kdfak == 11) {
echo '
<table border="1" cellspacing="0" cellpadding="0">
           <tr>
             <td bgcolor="lightgrey" align="center"><b>No</b></td>
             <td bgcolor="lightgrey" align="center"><b>Nim</b></td>
             <td bgcolor="lightgrey" align="center"><b>Nama Mahasiswa</b></td>
             <td bgcolor="lightgrey" align="center"><b>Alamat</b></td>
             <td bgcolor="lightgrey" align="center"><b>Email</b></td>
             <td bgcolor="lightgrey" align="center"><b>Sekolah Asal</b></td>
             <td bgcolor="lightgrey" align="center"><b>No. Telepon</b></td>
			 <td bgcolor="lightgrey" align="center"><b>NEM/NUN</b></td>
             <td bgcolor="lightgrey" align="center"><b>Agama</b></td>
             <td bgcolor="lightgrey" align="center"><b>Jenis Kelamin</b></td>
			 <td bgcolor="lightgrey" align="center"><b>Tempat Lahir</b></td>
             <td bgcolor="lightgrey" align="center"><b>Tanggal Lahir</b></td>
             <td bgcolor="lightgrey" align="center"><b>Penerimaan</b></td>
           </tr>';

$kueri="select mahasiswa.nim_mhs,upper(nm_pengguna),upper(alamat_asal_mhs),sekolah_asal_mhs,
nem_pelajaran_mhs,upper(nm_agama),case when kelamin_pengguna='1' then 'L' else 'P' end as kelamin,
nm_kota,TO_CHAR(tgl_lahir_pengguna, 'DD-MM-YYYY'),nm_jalur,mahasiswa.mobile_mhs,lower(email_pengguna)
from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join agama on pengguna.id_agama=agama.id_agama
left join kota on mahasiswa.lahir_kota_mhs=kota.id_kota
left join jalur_mahasiswa on mahasiswa.id_mhs=jalur_mahasiswa.id_mhs and mahasiswa.thn_angkatan_mhs=$angk 
left join jalur on jalur_mahasiswa.id_jalur=jalur.id_jalur
where thn_angkatan_mhs=$angk and mahasiswa.id_program_studi=$prodi and mahasiswa.status_akademik_mhs in (1,2,3)
order by mahasiswa.nim_mhs";
$result = $db->Query($kueri)or die("salah kueri 01 ");
$i=1;	
while($r = $db->FetchRow()) {
echo '
           <tr>
             <td>'.$i.'</td>
             <td>&nbsp;'.$r[0].'</td>
             <td>'.$r[1].'</td>
             <td>'.$r[2].'</td>
             <td>'.$r[11].'</td>
             <td>'.$r[3].'</td>
			 <td>&nbsp;'.$r[10].'</td>
             <td align="center">'.$r[4].'</td>
             <td>'.$r[5].'</td>
             <td align="center">'.$r[6].'</td>
             <td>'.$r[7].'</td>
             <td>&nbsp;'.$r[8].'</td>
             <td>'.$r[9].'</td>
           </tr>';
$i++;
}
echo '
</table>';
} else {
echo '
<table border="1" cellspacing="0" cellpadding="0">
           <tr class="left_menu">
             <td align="center"><b>No</b></td>
             <td align="center"><b>Nim</b></td>
             <td align="center"><b>Nama Mahasiswa</b></td>
             <td align="center"><b>Alamat</b></td>
             <td align="center"><b>Sekolah Asal</b></td>
             <td align="center"><b>No. Telepon</b></td>
			 <td align="center"><b>NEM/NUN</b></td>
             <td align="center"><b>Agama</b></td>
             <td align="center"><b>Jenis Kelamin</b></td>
			 <td align="center"><b>Tempat Lahir</b></td>
             <td align="center"><b>Tanggal Lahir</b></td>
             <td align="center"><b>Penerimaan</b></td>
           </tr>';

$kueri="select mahasiswa.nim_mhs,upper(nm_pengguna),upper(alamat_asal_mhs),sekolah_asal_mhs,
nem_pelajaran_mhs,upper(nm_agama),case when kelamin_pengguna='1' then 'L' else 'P' end as kelamin,
nm_kota,TO_CHAR(tgl_lahir_pengguna, 'DD-MM-YYYY'),nm_jalur,mahasiswa.mobile_mhs
from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join agama on pengguna.id_agama=agama.id_agama
left join kota on mahasiswa.lahir_kota_mhs=kota.id_kota
left join jalur_mahasiswa on mahasiswa.id_mhs=jalur_mahasiswa.id_mhs and mahasiswa.thn_angkatan_mhs=$angk 
left join jalur on jalur_mahasiswa.id_jalur=jalur.id_jalur
where thn_angkatan_mhs=$angk and mahasiswa.id_program_studi=$prodi and mahasiswa.status_akademik_mhs in (1,2,3)
order by mahasiswa.nim_mhs";
$result = $db->Query($kueri)or die("salah kueri 01 ");
$i=1;	
while($r = $db->FetchRow()) {
echo '
           <tr>
             <td>'.$i.'</td>
             <td>&nbsp;'.$r[0].'</td>
             <td>'.$r[1].'</td>
             <td>'.$r[2].'</td>
             <td>'.$r[3].'</td>
			 <td>'.$r[10].'</td>
             <td align="center">'.$r[4].'</td>
             <td>'.$r[5].'</td>
             <td align="center">'.$r[6].'</td>
             <td>'.$r[7].'</td>
             <td>'.$r[8].'</td>
             <td>'.$r[9].'</td>
           </tr>';
$i++;
}
echo '
</table>';
}
?>