<?php
require('../../../config.php');
$db = new MyOracle();

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$smt = $_GET['smt'];
switch ($smt){
case  'SP' : $str_smt = 'SEMESTER PENDEK';
		break;
case  'UP' : $str_smt = 'UJIAN PERBAIKAN';
		break;
case  'RD' : $str_smt = 'REMIDI';
		break;
case  'MU-K' : $str_smt = 'MAKE-UP KRS';
		break;
case  'SK' : $str_smt = 'PROGRAM KHUSUS';
		break;
}

$jenis = $_GET['jenis'];
switch ($jenis){
case  'kuliah' : $str_jenis = 'PERKULIAHAN';
		break;
case  'uts' : $str_jenis = 'UTS';
		break;
case  'uas' : $str_jenis = 'UAS';
		break;
}

if ($smt=='SP' && $jenis=='kuliah')
{
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('Presensi SP/UP');
$pdf->SetSubject('Presensi SP/UP');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logounair.gif";
$logo_size = "25";
$title = "UNIVERSITAS AIRLANGGA";

$thn = $_GET['thn'];
$hari = $_GET['hari'];
$idsmt = $_GET['idsmt'];

$kdprodi = $_GET['prodi'];
$kueri = "select a.nm_jenjang, b.nm_program_studi, f.nm_fakultas, f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas
			from jenjang a, program_studi b, fakultas f where a.id_jenjang=b.id_jenjang and f.id_fakultas=b.id_fakultas and b.id_program_studi=$kdprodi";
$result = $db->Query($kueri)or die("salah kueri 22 ");
while($r = $db->FetchRow()) {
	$jenjang = $r[0];
	$prodi = $r[1];
	$fak = $r[2];
	$alm_fak = $r[3];
	$pos_fak = $r[4];
	$tel_fak = $r[5];
	$fax_fak = $r[6];
	$web_fak = $r[7];
	$eml_fak = $r[8];
}

$id_kelas_mk = $_GET['cetak'];
$kueri = "select mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas.nama_kelas, jadwal_hari.nm_jadwal_hari, 
ruangan.nm_ruangan, jadwal_jam.jam_mulai||':'||jadwal_jam.menit_mulai||' - '||jadwal_jam.jam_selesai||':'||jadwal_jam.menit_selesai as jam
from kelas_mk
left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk=kelas_mk.id_kurikulum_mk
left join mata_kuliah on mata_kuliah.id_mata_kuliah=kurikulum_mk.id_mata_kuliah
left join nama_kelas on nama_kelas.id_nama_kelas=kelas_mk.no_kelas_mk
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_hari on jadwal_hari.id_jadwal_hari=jadwal_kelas.id_jadwal_hari
left join jadwal_jam on jadwal_jam.id_jadwal_jam=jadwal_kelas.id_jadwal_jam
left join ruangan on ruangan.id_ruangan=jadwal_kelas.id_ruangan
where kelas_mk.id_kelas_mk=$id_kelas_mk and jadwal_hari.id_jadwal_hari=$hari";
$result = $db->Query($kueri)or die("salah kueri 25 ");
while($r = $db->FetchRow()) {
	$kode_ma = $r[0];
	$nama_ma = $r[1];
	$sks_ma = $r[2];
	$kelas_ma = $r[3];
	$hari_ma = $r[4];
	$ruang_ma = $r[5];
	$jam_ma = $r[6];
}

$content = "FAKULTAS ".strtoupper($fak)."\n".strtoupper($jenjang)." - ".strtoupper($prodi)."\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(5, 40, 5);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 15);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage('L', 'F4');

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="17" align="center"><b><u>PRESENSI '.$str_jenis.' MAHASISWA '.strtoupper($str_smt).' '.strtoupper($thn).'</u></b></td>
  </tr>
  <tr>
    <td colspan="17">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="12">'.$kode_ma.' - '.strtoupper($nama_ma).' ('.$sks_ma.' SKS) <br/>KELAS '.strtoupper($kelas_ma).'</td>
	<td colspan="5" rowspan="2">
	<table border="0" cellspacing="0" cellpadding="0">';

$kueri = "select case when pengguna.gelar_belakang is null then trim(pengguna.gelar_depan||' '||upper(pengguna.nm_pengguna)) 
else trim(pengguna.gelar_depan||' '||upper(pengguna.nm_pengguna)||', '||pengguna.gelar_belakang) end as nama_dsn, 
case when pengampu_mk.pjmk_pengampu_mk=1 then 'PJMA' else 'TIM' end as pjma
from kelas_mk
left join pengampu_mk on kelas_mk.id_kelas_mk = pengampu_mk.id_kelas_mk
left join dosen on pengampu_mk.id_dosen = dosen.id_dosen
left join pengguna on dosen.id_pengguna = pengguna.id_pengguna
where kelas_mk.id_kelas_mk=$id_kelas_mk
order by pengampu_mk.pjmk_pengampu_mk desc";
$result = $db->Query($kueri)or die("salah kueri 26 ");
while($r = $db->FetchRow()) {
	$nama_dsn = $r[0];
	$pjma = $r[1];
if($nama_dsn != ',') {
$html .= '
<tr>
	<td>'.strtoupper($nama_dsn).' ('.$pjma.')</td>
</tr>';
} else {
$html .= '
<tr>
	<td>PJMA BELUM DITENTUKAN</td>
</tr>';
}
}
$html .= '
	</table>
	</td>
  </tr>
  <tr>
    <td colspan="12">'.strtoupper($hari_ma).' : '.$jam_ma.' <br/>RUANG : '.strtoupper($ruang_ma).'</td>
  </tr>
  <tr>
    <td colspan="17">&nbsp;</td>
  </tr>
</table>
<table cellspacing="0" cellpadding="4" border="1" width="100%">
<thead>
    <tr bgcolor="lightgray" style="color:#000;">
      <td width="5%" align="center"><strong>NO.</strong></td>
	  <td width="15%" align="center"><strong>NIM</strong></td>
      <td width="20%" align="center"><strong>NAMA MAHASISWA</strong></td>
      <td width="5%" align="center"><strong>&nbsp;</strong></td>
	  <td width="5%" align="center"><strong>&nbsp;</strong></td>
	  <td width="5%" align="center"><strong>&nbsp;</strong></td>
	  <td width="5%" align="center"><strong>&nbsp;</strong></td>
	  <td width="5%" align="center"><strong>&nbsp;</strong></td>
	  <td width="5%" align="center"><strong>&nbsp;</strong></td>
	  <td width="5%" align="center"><strong>&nbsp;</strong></td>
	  <td width="5%" align="center"><strong>&nbsp;</strong></td>
	  <td width="5%" align="center"><strong>&nbsp;</strong></td>
	  <td width="5%" align="center"><strong>&nbsp;</strong></td>
	  <td width="5%" align="center"><strong>&nbsp;</strong></td>
	  <td width="5%" align="center"><strong>&nbsp;</strong></td>
    </tr>
</thead>';

$nomor=1;

$kueri = "select nim_mhs,nm_pengguna, cek_bayar from pengambilan_mk
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join (
	select count(*) as cek_bayar, id_mhs 
	from pembayaran_sp where id_semester = $idsmt and id_status_pembayaran = 2
	group by id_mhs
) a on a.id_mhs = mahasiswa.id_mhs
where pengambilan_mk.id_kelas_mk=$id_kelas_mk and pengambilan_mk.status_apv_pengambilan_mk=1
and pengambilan_mk.id_semester=$idsmt
order by nim_mhs";


$result = $db->Query($kueri)or die("salah kueri 27 ");
while($r = $db->FetchRow()) {
	$nim_mhs = $r[0];
	$nama_mhs = $r[1];
	$cek_bayar = $r[2];
	

	if($cek_bayar >= 1){
		$bgcolor = ' bgcolor="#000"';	
	}else{
		$bgcolor = ' bgcolor="#FFF"';
	}

	
$html .= '
    <tr style="color:#000;">
      <td width="5%" align="center">'.$nomor.'</td>
	  <td  width="15%" align="center">'.$nim_mhs.'</td>
      <td  width="20%" align="left">'.strtoupper($nama_mhs).'</td>
      <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
	  <td '.$bgcolor.' width="5%" align="center">&nbsp;</td>
    </tr>';
$nomor++;
}
$html .= '</table>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('PRESENSI '.$str_jenis.' '.$str_smt.' '.strtoupper($nama_ma).' '.strtoupper($kelas_ma).'.pdf', 'I');
}

if ($smt=='SP' && $jenis!='kuliah')
{
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('Presensi SP/UP');
$pdf->SetSubject('Presensi SP/UP');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logounair.gif";
$logo_size = "25";
$title = "UNIVERSITAS AIRLANGGA";

$thn = $_GET['thn'];
$hari = $_GET['hari'];
$idsmt = $_GET['idsmt'];

$kdprodi = $_GET['prodi'];
$kueri = "select a.nm_jenjang, b.nm_program_studi, f.nm_fakultas, f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas
			from jenjang a, program_studi b, fakultas f where a.id_jenjang=b.id_jenjang and f.id_fakultas=b.id_fakultas and b.id_program_studi=$kdprodi";
$result = $db->Query($kueri)or die("salah kueri 22 ");
while($r = $db->FetchRow()) {
	$jenjang = $r[0];
	$prodi = $r[1];
	$fak = $r[2];
	$alm_fak = $r[3];
	$pos_fak = $r[4];
	$tel_fak = $r[5];
	$fax_fak = $r[6];
	$web_fak = $r[7];
	$eml_fak = $r[8];
}

$id_kelas_mk = $_GET['cetak'];
$kueri = "select mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas.nama_kelas, jadwal_hari.nm_jadwal_hari, 
ruangan.nm_ruangan, jadwal_jam.jam_mulai||':'||jadwal_jam.menit_mulai||' - '||jadwal_jam.jam_selesai||':'||jadwal_jam.menit_selesai as jam
from kelas_mk
left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk=kelas_mk.id_kurikulum_mk
left join mata_kuliah on mata_kuliah.id_mata_kuliah=kurikulum_mk.id_mata_kuliah
left join nama_kelas on nama_kelas.id_nama_kelas=kelas_mk.no_kelas_mk
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_hari on jadwal_hari.id_jadwal_hari=jadwal_kelas.id_jadwal_hari
left join jadwal_jam on jadwal_jam.id_jadwal_jam=jadwal_kelas.id_jadwal_jam
left join ruangan on ruangan.id_ruangan=jadwal_kelas.id_ruangan
where kelas_mk.id_kelas_mk=$id_kelas_mk and jadwal_hari.id_jadwal_hari=$hari";
$result = $db->Query($kueri)or die("salah kueri 25 ");
while($r = $db->FetchRow()) {
	$kode_ma = $r[0];
	$nama_ma = $r[1];
	$sks_ma = $r[2];
	$kelas_ma = $r[3];
	$hari_ma = $r[4];
	$ruang_ma = $r[5];
	$jam_ma = $r[6];
}

$content = "FAKULTAS ".strtoupper($fak)."\n".strtoupper($jenjang)." - ".strtoupper($prodi)."\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(5, 40, 5);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 15);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage('P', 'F4');

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="17" align="center"><b><u>PRESENSI '.$str_jenis.' '.strtoupper($str_smt).' '.strtoupper($thn).'</u></b></td>
  </tr>
  <tr>
    <td colspan="17">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="12">'.$kode_ma.' - '.strtoupper($nama_ma).' ('.$sks_ma.' SKS) <br/>KELAS '.strtoupper($kelas_ma).'</td>
	<td colspan="5" rowspan="2">
	<table border="0" cellspacing="0" cellpadding="0">';

$kueri = "select case when pengguna.gelar_belakang is null then trim(pengguna.gelar_depan||' '||upper(pengguna.nm_pengguna)) 
else trim(pengguna.gelar_depan||' '||upper(pengguna.nm_pengguna)||', '||pengguna.gelar_belakang) end as nama_dsn, 
case when pengampu_mk.pjmk_pengampu_mk=1 then 'PJMA' else 'TIM' end as pjma
from kelas_mk
left join pengampu_mk on kelas_mk.id_kelas_mk = pengampu_mk.id_kelas_mk
left join dosen on pengampu_mk.id_dosen = dosen.id_dosen
left join pengguna on dosen.id_pengguna = pengguna.id_pengguna
where kelas_mk.id_kelas_mk=$id_kelas_mk
order by pengampu_mk.pjmk_pengampu_mk desc";
$result = $db->Query($kueri)or die("salah kueri 26 ");
while($r = $db->FetchRow()) {
	$nama_dsn = $r[0];
	$pjma = $r[1];
if($nama_dsn != ',') {
$html .= '
<tr>
	<td>'.strtoupper($nama_dsn).' ('.$pjma.')</td>
</tr>';
} else {
$html .= '
<tr>
	<td>PJMA BELUM DITENTUKAN</td>
</tr>';
}
}
$html .= '
	</table>
	</td>
  </tr>
  <tr>
    <td colspan="12">'.strtoupper($hari_ma).' : '.$jam_ma.' <br/>RUANG : '.strtoupper($ruang_ma).'</td>
  </tr>
  <tr>
    <td colspan="17">&nbsp;</td>
  </tr>
</table>
<table cellspacing="0" cellpadding="4" border="1" width="100%">
<thead>
    <tr bgcolor="lightgray" style="color:#000;">
      <td border="1" width="5%" align="center"><strong><br/>NO.<br/>&nbsp;</strong></td>
	  <td border="1" width="20%" align="center"><strong><br/>NIM<br/>&nbsp;</strong></td>
      <td border="1" width="40%" align="center"><strong><br/>NAMA MAHASISWA<br/>&nbsp;</strong></td>
	  <td border="1" width="10%" align="center" valign="middle"><strong>NILAI</strong></td>
      <td border="1" width="25%" align="center"><strong><br/>TANDA TANGAN<br/>&nbsp;</strong></td>
    </tr>
</thead>';

$nomor=1;

$kueri = "select nim_mhs,nm_pengguna from pengambilan_mk
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
where pengambilan_mk.id_kelas_mk=$id_kelas_mk and pengambilan_mk.status_apv_pengambilan_mk=1
and pengambilan_mk.id_semester=$idsmt
order by nim_mhs";

$result = $db->Query($kueri)or die("salah kueri 30 ");
$i=0;
while($r = $db->FetchRow()) {
	$nim_mhs = $r[0];
	$nama_mhs = $r[1];
$i++;   
if (($i % 2)==0) $tpt='align="right"';
else $tpt='align="left"';
$html .= '
    <tr bgcolor="#FFF" style="color:#000;">
      <td border="1" width="5%" align="center">'.$nomor.'</td>
	  <td border="1" width="20%" align="center">'.$nim_mhs.'</td>
      <td border="1" width="40%" align="left">'.strtoupper($nama_mhs).'</td>
	  <td border="1" width="10%" align="center">...............</td>
      <td border="1" width="25%" '.$tpt.'>'.$nomor.'. ...............</td>
    </tr>';
$nomor++;
}
$html .= '</table>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('PRESENSI '.$str_jenis.' '.$str_smt.' '.strtoupper($nama_ma).' '.strtoupper($kelas_ma).'.pdf', 'I');
}

if ($smt=='UP')
{
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('Presensi UP');
$pdf->SetSubject('Presensi UP');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logounair.gif";
$logo_size = "25";
$title = "UNIVERSITAS AIRLANGGA";

$idsmt = $_GET['idsmt'];
$thn = $_GET['thn'];

$kdprodi = $_GET['prodi'];
$kueri = "select a.nm_jenjang, b.nm_program_studi, f.nm_fakultas, f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas
			from jenjang a, program_studi b, fakultas f where a.id_jenjang=b.id_jenjang and f.id_fakultas=b.id_fakultas and b.id_program_studi=$kdprodi";
$result = $db->Query($kueri)or die("salah kueri 22 ");
while($r = $db->FetchRow()) {
	$jenjang = $r[0];
	$prodi = $r[1];
	$fak = $r[2];
	$alm_fak = $r[3];
	$pos_fak = $r[4];
	$tel_fak = $r[5];
	$fax_fak = $r[6];
	$web_fak = $r[7];
	$eml_fak = $r[8];
}

$id_kelas_mk = $_GET['cetak'];
$kueri = "select mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas.nama_kelas, jadwal_hari.nm_jadwal_hari, 
ruangan.nm_ruangan, jadwal_jam.jam_mulai||':'||jadwal_jam.menit_mulai||' - '||jadwal_jam.jam_selesai||':'||jadwal_jam.menit_selesai as jam
from kelas_mk
left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk=kelas_mk.id_kurikulum_mk
left join mata_kuliah on mata_kuliah.id_mata_kuliah=kurikulum_mk.id_mata_kuliah
left join nama_kelas on nama_kelas.id_nama_kelas=kelas_mk.no_kelas_mk
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_hari on jadwal_hari.id_jadwal_hari=jadwal_kelas.id_jadwal_hari
left join jadwal_jam on jadwal_jam.id_jadwal_jam=jadwal_kelas.id_jadwal_jam
left join ruangan on ruangan.id_ruangan=jadwal_kelas.id_ruangan
where kelas_mk.id_kelas_mk=$id_kelas_mk";
$result = $db->Query($kueri)or die("salah kueri 25 ");
while($r = $db->FetchRow()) {
	$kode_ma = $r[0];
	$nama_ma = $r[1];
	$sks_ma = $r[2];
	$kelas_ma = $r[3];
	$hari_ma = $r[4];
	$ruang_ma = $r[5];
	$jam_ma = $r[6];
}

$id_kelas_mk = $_GET['cetak'];
$kueri1 = "select case when pengguna.gelar_belakang is null then trim(pengguna.gelar_depan||' '||upper(pengguna.nm_pengguna)) 
else trim(pengguna.gelar_depan||' '||upper(pengguna.nm_pengguna)||', '||pengguna.gelar_belakang) end as pjmk, dosen.nip_dosen as nip
from kelas_mk 
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk 
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
where kelas_mk.id_kelas_mk=$id_kelas_mk and pengampu_mk.pjmk_pengampu_mk=1";
$result = $db->Query($kueri1)or die("salah kueri 26 ");
while($r = $db->FetchRow()) {
	$pjmk = $r[0];
	$nip = $r[1];
}

$content = "FAKULTAS ".strtoupper($fak)."\n".strtoupper($jenjang)." - ".strtoupper($prodi)."\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 40, 5);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 14);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage('P', 'A4');

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="5" align="center"><b><u>PRESENSI '.strtoupper($str_smt).' '.strtoupper($thn).'</u></b></td>
  </tr>
  <tr>
    <td colspan="5">&nbsp;</td>
  </tr>
</table>
<table width="100%" cellspacing="0" cellpadding="5">
  <tr>
    <td colspan="3" width="65%">'.$kode_ma.' - '.strtoupper($nama_ma).' ('.$sks_ma.' SKS)</td>
	<td colspan="2" width="35%">Hari/Tanggal&nbsp;:</td>
  </tr>
  <tr>
	<td colspan="3" width="65%">KELAS '.$kelas_ma.'</td>
	<td colspan="2" width="35%">Jam/Ruang&nbsp;&nbsp;&nbsp;:</td>
  </tr>
<thead>
  <tr bgcolor="lightgrey" style="color:#000;">
      <td border="1" width="5%" align="center"><strong><br/>NO.<br/>&nbsp;</strong></td>
	  <td border="1" width="20%" align="center"><strong><br/>NIM<br/>&nbsp;</strong></td>
      <td border="1" width="40%" align="center"><strong><br/>NAMA MAHASISWA<br/>&nbsp;</strong></td>
	  <td border="1" width="10%" align="center" valign="middle"><strong>NILAI UP</strong></td>
      <td border="1" width="25%" align="center"><strong><br/>TANDA TANGAN<br/>&nbsp;</strong></td>
    </tr>
</thead>';

$nomor=1;
$kueri = "select nim_mhs,nm_pengguna,1 as status_cekal from pengambilan_mk
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
where pengambilan_mk.id_kelas_mk=$id_kelas_mk and pengambilan_mk.status_apv_pengambilan_mk=1
and pengambilan_mk.id_semester=$idsmt 
order by nim_mhs";
$result = $db->Query($kueri)or die("salah kueri 27 ");
$i=0;
while($r = $db->FetchRow()) {
	$nim_mhs = $r[0];
	$nama_mhs = $r[1];
	$tilang = $r[2];
$i++;   
if (($i % 2)==0) $tpt='align="right"';
else $tpt='align="left"';
//if ($tilang == 1) $status='"#FFFFFF"';
//else $status='"#000000"';
if ($tilang == 0 || $tilang == 2) $status='"#000000"';
else $status='"#FFFFFF"';
$html .= '
    <tr bgcolor="#FFF" style="color:#000;">
      <td border="1" width="5%" align="center">'.$nomor.'</td>
	  <td border="1" width="20%" align="center">'.$nim_mhs.'</td>
      <td border="1" width="40%" align="left">'.strtoupper($nama_mhs).'</td>
	  <td border="1" width="10%" align="center" bgcolor='.$status.'>...............</td>
      <td border="1" width="25%" '.$tpt.' bgcolor='.$status.'>'.$nomor.'. ...............</td>
    </tr>';
$nomor++;
}
if($pjmk != null) {
$html .= '
  <tr>
    <td colspan="5" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
	<td colspan="4">Surabaya, .....................................<br/><br/><br/><br/><u>'.strtoupper($pjmk).'</u><br />'.$nip.'</td>
  </tr>
</table>';
} else {
$html .= '
  <tr>
    <td colspan="5" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
	<td colspan="4">Surabaya, .....................................<br/><br/><br/><br/>PJMA BELUM DITENTUKAN</td>
  </tr>
</table>';
}

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('PRESENSI UP '.strtoupper($nama_ma).' '.strtoupper($kelas_ma).'.pdf', 'I');

}

?>