<?php
include ('config.php');
include "../pendidikan/class/epsbed.class.php";
$aucc = new epsbed($db);

$kdfak=$user->ID_FAKULTAS; 

$smarty->assign('fakultas', $kdfak);
$smarty->assign('data_jenjang', $aucc->jenjang());
$smarty->assign('periode_wisuda', $db->QueryToArray("select * from tarif_wisuda order by id_tarif_wisuda desc"));

if(isset($_POST['mode'])){
	
	$smarty->assign('prodi', $aucc->prodi_condition($_POST['fakultas'], $_POST['jenjang']));
	$smarty->assign('ebsbed', $aucc->trlsm($_POST['fakultas'], $_POST['jenjang'], $_POST['program_studi'], $_POST['periode_wisuda']));
}

$smarty->display('ebsbed/ebsbed-trlsm.tpl');
?>