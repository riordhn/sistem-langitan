<?php
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');

$id_pengguna= $user->ID_PENGGUNA; 

$kdfak=$user->ID_FAKULTAS; 

$id_pt = $id_pt_user;

if(isset($_POST['smt'])){

$jadwal = $db->QueryToArray("SELECT NM_KEGIATAN, DESKRIPSI_KEGIATAN, THN_AKADEMIK_SEMESTER, NM_SEMESTER, 
											TGL_MULAI_JKS, TGL_SELESAI_JKS, 
											ID_JADWAL_KEGIATAN_SEMESTER, ID_SEMESTER, KEGIATAN.ID_KEGIATAN
											FROM KEGIATAN
											LEFT JOIN (
											SELECT THN_AKADEMIK_SEMESTER, NM_SEMESTER, 
											TO_CHAR(TGL_MULAI_JKS, 'DD-MM-YYYY') AS TGL_MULAI_JKS, 
											TO_CHAR(TGL_SELESAI_JKS, 'DD-MM-YYYY') AS TGL_SELESAI_JKS, 
											ID_JADWAL_KEGIATAN_SEMESTER, SEMESTER.ID_SEMESTER, KEGIATAN.ID_KEGIATAN 
											FROM KEGIATAN
											LEFT JOIN JADWAL_KEGIATAN_SEMESTER ON KEGIATAN.ID_KEGIATAN = JADWAL_KEGIATAN_SEMESTER.ID_KEGIATAN
											LEFT JOIN SEMESTER ON SEMESTER.ID_SEMESTER = JADWAL_KEGIATAN_SEMESTER.ID_SEMESTER
											WHERE THN_AKADEMIK_SEMESTER in 
											(SELECT THN_AKADEMIK_SEMESTER FROM SEMESTER WHERE ID_SEMESTER = '$_POST[smt]')
											AND GROUP_SEMESTER in (SELECT GROUP_SEMESTER FROM SEMESTER WHERE ID_SEMESTER = '$_POST[smt]')
											) A ON A.ID_KEGIATAN = KEGIATAN.ID_KEGIATAN
											ORDER BY TGL_MULAI_JKS, NM_KEGIATAN");
$smarty->assign('jadwal', $jadwal);
}else{
$jadwal = $db->QueryToArray("SELECT NM_KEGIATAN, DESKRIPSI_KEGIATAN, THN_AKADEMIK_SEMESTER, NM_SEMESTER, 
											TGL_MULAI_JKS, TGL_SELESAI_JKS, 
											ID_JADWAL_KEGIATAN_SEMESTER, ID_SEMESTER, KEGIATAN.ID_KEGIATAN
											FROM KEGIATAN
											LEFT JOIN (
											SELECT THN_AKADEMIK_SEMESTER, NM_SEMESTER, 
											TO_CHAR(TGL_MULAI_JKS, 'DD-MM-YYYY') AS TGL_MULAI_JKS, 
											TO_CHAR(TGL_SELESAI_JKS, 'DD-MM-YYYY') AS TGL_SELESAI_JKS, 
											ID_JADWAL_KEGIATAN_SEMESTER, SEMESTER.ID_SEMESTER, KEGIATAN.ID_KEGIATAN 
											FROM KEGIATAN
											LEFT JOIN JADWAL_KEGIATAN_SEMESTER ON KEGIATAN.ID_KEGIATAN = JADWAL_KEGIATAN_SEMESTER.ID_KEGIATAN
											LEFT JOIN SEMESTER ON SEMESTER.ID_SEMESTER = JADWAL_KEGIATAN_SEMESTER.ID_SEMESTER
											WHERE THN_AKADEMIK_SEMESTER in 
											(SELECT THN_AKADEMIK_SEMESTER FROM SEMESTER WHERE status_aktif_semester='True' AND id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}')
											AND GROUP_SEMESTER in (SELECT GROUP_SEMESTER FROM SEMESTER WHERE status_aktif_semester='True' AND  id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}')
											) A ON A.ID_KEGIATAN = KEGIATAN.ID_KEGIATAN
											WHERE KEGIATAN.ID_PERGURUAN_TINGGI = '{$user->ID_PERGURUAN_TINGGI}'
											ORDER BY TGL_MULAI_JKS, NM_KEGIATAN");
$smarty->assign('jadwal', $jadwal);

}

//Untuk Fakultas Psikologi
if($kdfak==11){
$smt=getData("select id_semester, tahun_ajaran||' - '||nm_semester as smt from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester in (
select * from (select distinct thn_akademik_semester
from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester<=EXTRACT(YEAR FROM sysdate) order by thn_akademik_semester desc)
where rownum<=3) and nm_semester in ('Ganjil', 'Genap')
order by thn_akademik_semester desc, nm_semester desc");
$smarty->assign('T_ST', $smt);
} else {
$smt=getData("select id_semester, tahun_ajaran||' - '||nm_semester||'-'||group_semester as smt from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester in (
select * from (select distinct thn_akademik_semester 
from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester<=EXTRACT(YEAR FROM sysdate) order by thn_akademik_semester desc) 
where rownum<=2) 
order by thn_akademik_semester desc, group_semester desc,nm_semester desc");
$smarty->assign('T_ST', $smt);
}

$smtaktif=getvar("select id_semester from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND status_aktif_semester='True'");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);

$kgt=getData("select * from kegiatan where id_perguruan_tinggi = '{$id_pt}' order by nm_kegiatan");
$smarty->assign('T_KGT', $kgt);

$smt = isSet($_POST['smt']) ? $_POST['smt'] : $smtaktif['ID_SEMESTER'];
$smarty->assign('smt1', $smt);

$status = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'tampil';

switch($status) {
case 'add':
		 // pilih

		$nm_kgt = $_POST['nm_kgt'];
		$awal = $_POST['calawal'];
		$akhir = $_POST['calakhir'];

		if (trim($nm_kgt) != '')
		{
		
		$jadwal_univ=getvar("SELECT COUNT(*) AS CEK FROM JADWAL_KEGIATAN_SEMESTER
					JOIN JADWAL_SEMESTER_FAKULTAS ON JADWAL_SEMESTER_FAKULTAS.ID_KEGIATAN = JADWAL_KEGIATAN_SEMESTER.ID_KEGIATAN 
					AND JADWAL_SEMESTER_FAKULTAS.ID_SEMESTER = JADWAL_KEGIATAN_SEMESTER.ID_SEMESTER
					WHERE ID_FAKULTAS = '$kdfak' AND JADWAL_KEGIATAN_SEMESTER.ID_SEMESTER = '$smt' AND JADWAL_KEGIATAN_SEMESTER.ID_KEGIATAN = '$nm_kgt'");
		
		if($jadwal_univ['CEK'] == 0){
			//$db->beginTransaction();
			//tambahdata("jadwal_semester_fakultas","id_kegiatan,id_semester,id_fakultas,tgl_mulai_jsf,tgl_selesai_jsf","'$nm_kgt','$smt','$kdfak','$awal','$akhir'");
			
			// Update by Yudi Sulistya, including HH24:MI:SS

			$tambah = $db->Query("INSERT INTO JADWAL_SEMESTER_FAKULTAS (id_kegiatan, id_semester, id_fakultas,tgl_mulai_jsf,tgl_selesai_jsf)
									VALUES ('$nm_kgt','$smt','$kdfak',to_date('".$awal."', 'DD-MM-YYYY HH24:MI:SS'),to_date('".$akhir."', 'DD-MM-YYYY HH24:MI:SS'))");
					
			$jadwal_univ=getvar("SELECT COUNT(*) AS CEK FROM JADWAL_KEGIATAN_SEMESTER
					JOIN JADWAL_SEMESTER_FAKULTAS ON JADWAL_SEMESTER_FAKULTAS.ID_KEGIATAN = JADWAL_KEGIATAN_SEMESTER.ID_KEGIATAN 
					AND JADWAL_SEMESTER_FAKULTAS.ID_SEMESTER = JADWAL_KEGIATAN_SEMESTER.ID_SEMESTER
					WHERE ID_FAKULTAS = '$kdfak' AND JADWAL_KEGIATAN_SEMESTER.ID_SEMESTER = '$smt' AND JADWAL_KEGIATAN_SEMESTER.ID_KEGIATAN = '$nm_kgt'
					AND TGL_MULAI_JSF >= TGL_MULAI_JKS AND TGL_SELESAI_JSF <= TGL_SELESAI_JKS");
			if($jadwal_univ['CEK'] == 1){
				//$db->commit();
				echo 'berhasil';
			}else{
				//$db->rollBack();

				deleteData("delete from jadwal_semester_fakultas where id_semester='$smt' and id_kegiatan = '$nm_kgt' and id_fakultas = '$kdfak' ");
				echo "Maaf, Jadwal Kegiatan Fakultas tidak sesuai dengan Kalender Universitas";
			}
		}else{
			echo "Maaf, Jadwal Kegiatan Fakultas tidak sesuai dengan Kalender Universitas";
		}
		
		}

		if ($smt!=''){
		$jaf=getData("SELECT ID_JSF,ID_FAKULTAS, JSF.ID_KEGIATAN,NM_KEGIATAN,JSF.ID_SEMESTER,STATUS_AKTIF_SEMESTER,
TO_CHAR(TGL_MULAI_JSF,'DD-MM-YYYY HH24:MI') AS TGL_MULAI, TO_CHAR(TGL_SELESAI_JSF,'DD-MM-YYYY HH24:MI') AS TGL_SELESAI FROM JADWAL_SEMESTER_FAKULTAS JSF 
LEFT JOIN KEGIATAN ON JSF.ID_KEGIATAN=KEGIATAN.ID_KEGIATAN 
LEFT JOIN SEMESTER ON JSF.ID_SEMESTER=SEMESTER.ID_SEMESTER  
WHERE JSF.ID_SEMESTER=$smt and ID_FAKULTAS=$kdfak  
ORDER BY TGL_MULAI_JSF, TGL_SELESAI_JSF ");}

		$smarty->assign('T_JAF', $jaf);
		$smarty->display('jadwal-akademik.tpl');
        break;   
		
case 'del':
		 // pilih
		$smt= $_GET['id_smt'];
		$id_jsf = $_GET['id_jsf'];
				
		deleteData("delete from jadwal_semester_fakultas where id_jsf='$id_jsf'");

		$jaf=getData("SELECT ID_JSF,ID_FAKULTAS, JSF.ID_KEGIATAN,NM_KEGIATAN,JSF.ID_SEMESTER,STATUS_AKTIF_SEMESTER,
TO_CHAR(TGL_MULAI_JSF,'DD-MM-YYYY HH24:MI') AS TGL_MULAI, TO_CHAR(TGL_SELESAI_JSF,'DD-MM-YYYY HH24:MI') AS TGL_SELESAI FROM JADWAL_SEMESTER_FAKULTAS JSF 
LEFT JOIN KEGIATAN ON JSF.ID_KEGIATAN=KEGIATAN.ID_KEGIATAN 
LEFT JOIN SEMESTER ON JSF.ID_SEMESTER=SEMESTER.ID_SEMESTER 
WHERE JSF.ID_SEMESTER=$smt and ID_FAKULTAS=$kdfak  
ORDER BY TGL_MULAI_JSF, TGL_SELESAI_JSF ");

		$smarty->assign('T_JAF', $jaf);
		$smarty->display('jadwal-akademik.tpl');

        break; 
		
case 'update':
		 // pilih
		$smt = $_POST['id_smt'];
		//echo "aa";
		//echo $smt;
		$id_jsf = $_POST['id_jsf'];
		$awal = $_POST['calawal1'];
		$selesai = $_POST['calakhir1'];	
		
		$jadwal_univ=getvar("SELECT COUNT(*) AS CEK FROM JADWAL_KEGIATAN_SEMESTER
					JOIN JADWAL_SEMESTER_FAKULTAS ON JADWAL_SEMESTER_FAKULTAS.ID_KEGIATAN = JADWAL_KEGIATAN_SEMESTER.ID_KEGIATAN 
					AND JADWAL_SEMESTER_FAKULTAS.ID_SEMESTER = JADWAL_KEGIATAN_SEMESTER.ID_SEMESTER
					WHERE ID_JSF = '$id_jsf'
					AND to_date('".$awal."', 'DD-MM-YYYY HH24:MI:SS') >= TGL_MULAI_JKS AND to_date('".$selesai."', 'DD-MM-YYYY HH24:MI:SS') <= TGL_SELESAI_JKS");
		
		if($jadwal_univ['CEK'] == 1){
		// Update by Yudi Sulistya, including HH24:MI:SS
		UpdateData("update jadwal_semester_fakultas set tgl_mulai_jsf=to_date('".$awal."', 'DD-MM-YYYY HH24:MI:SS'), tgl_selesai_jsf=to_date('".$selesai."', 'DD-MM-YYYY HH24:MI:SS') where id_jsf='$id_jsf'");
		}else{
			echo "Maaf, Jadwal Kegiatan Fakultas tidak sesuai dengan Kalender Universitas";
		}

		//if ($smt!=''){
		$jaf=getData("SELECT ID_JSF,ID_FAKULTAS, JSF.ID_KEGIATAN,NM_KEGIATAN,JSF.ID_SEMESTER,STATUS_AKTIF_SEMESTER,
TO_CHAR(TGL_MULAI_JSF,'DD-MM-YYYY HH24:MI') AS TGL_MULAI, TO_CHAR(TGL_SELESAI_JSF,'DD-MM-YYYY HH24:MI') AS TGL_SELESAI FROM JADWAL_SEMESTER_FAKULTAS JSF 
LEFT JOIN KEGIATAN ON JSF.ID_KEGIATAN=KEGIATAN.ID_KEGIATAN 
LEFT JOIN SEMESTER ON JSF.ID_SEMESTER=SEMESTER.ID_SEMESTER 
WHERE JSF.ID_SEMESTER=$smt and ID_FAKULTAS=$kdfak  
ORDER BY TGL_MULAI_JSF, TGL_SELESAI_JSF");

		$smarty->assign('T_JAF', $jaf);
		$smarty->display('jadwal-akademik.tpl');

        break; 
   
case 'tampil':
		
		//$smt1= $_GET['id_smt'];
		$id_jsf=$_GET['id_jsf'];
		
		if ($id_jsf !='') {
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','block');
		$jaf=getData("SELECT ID_JSF,ID_FAKULTAS, JSF.ID_KEGIATAN,NM_KEGIATAN,JSF.ID_SEMESTER,STATUS_AKTIF_SEMESTER,
TO_CHAR(TGL_MULAI_JSF,'DD-MM-YYYY HH24:MI') AS TGL_MULAI, TO_CHAR(TGL_SELESAI_JSF,'DD-MM-YYYY HH24:MI') AS TGL_SELESAI FROM JADWAL_SEMESTER_FAKULTAS JSF 
LEFT JOIN KEGIATAN ON JSF.ID_KEGIATAN=KEGIATAN.ID_KEGIATAN 
LEFT JOIN SEMESTER ON JSF.ID_SEMESTER=SEMESTER.ID_SEMESTER 
WHERE JSF.ID_SEMESTER=$smt and ID_JSF=$id_jsf  
ORDER BY TGL_MULAI_JSF, TGL_SELESAI_JSF");
/*
echo "SELECT ID_JSF,ID_FAKULTAS, JSF.ID_KEGIATAN,NM_KEGIATAN,JSF.ID_SEMESTER,STATUS_AKTIF_SEMESTER,TGL_MULAI_JSF, TGL_SELESAI_JSF FROM JADWAL_SEMESTER_FAKULTAS JSF 
LEFT JOIN KEGIATAN ON JSF.ID_KEGIATAN=KEGIATAN.ID_KEGIATAN 
LEFT JOIN SEMESTER ON JSF.ID_SEMESTER=SEMESTER.ID_SEMESTER 
WHERE JSF.ID_SEMESTER=$smt and ID_JSF=$id_jsf  
ORDER BY JSF.ID_KEGIATAN";
*/
		} else {

		$jaf=getData("SELECT ID_JSF,ID_FAKULTAS, JSF.ID_KEGIATAN,NM_KEGIATAN,JSF.ID_SEMESTER,STATUS_AKTIF_SEMESTER,
TO_CHAR(TGL_MULAI_JSF,'DD-MM-YYYY HH24:MI') AS TGL_MULAI, TO_CHAR(TGL_SELESAI_JSF,'DD-MM-YYYY HH24:MI') AS TGL_SELESAI FROM JADWAL_SEMESTER_FAKULTAS JSF 
LEFT JOIN KEGIATAN ON JSF.ID_KEGIATAN=KEGIATAN.ID_KEGIATAN 
LEFT JOIN SEMESTER ON JSF.ID_SEMESTER=SEMESTER.ID_SEMESTER  
WHERE JSF.ID_SEMESTER=$smt and ID_FAKULTAS=$kdfak  
ORDER BY TGL_MULAI_JSF, TGL_SELESAI_JSF");
		}
		$smarty->assign('T_JAF', $jaf);
		$smarty->display('jadwal-akademik.tpl');
        break;   

}
        
       
?>
