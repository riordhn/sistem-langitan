<?php
require 'common.php';

$kdprodi = $user->ID_PROGRAM_STUDI;
$id_pengguna = $user->ID_PENGGUNA;
$id_fakultas = $user->ID_FAKULTAS;

// Prepare
$_POST['mode']      = isset($_POST['mode']) ? $_POST['mode'] : '';
$_POST['nim']       = isset($_POST['nim']) ? $_POST['nim'] : '';
$_POST['id_mhs']    = isset($_POST['id_mhs']) ? $_POST['id_mhs'] : '';

if ($_GET['mode'] == 'batal') {
    $nim = $_GET['nim'];

    // Ambil biodata
    $pw = getData(
        "SELECT MAHASISWA.ID_MHS, NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, JENJANG.NM_JENJANG, YUDISIUM, PROGRAM_STUDI.ID_JENJANG, NM_TARIF_WISUDA || ' - ' || J2.NM_JENJANG AS NM_TARIF_WISUDA, TO_CHAR(PERIODE_WISUDA.TGL_BAYAR_MULAI, 'DD-MM-YYYY') AS TGL_BAYAR_MULAI, PENGAJUAN_WISUDA.ID_PENGAJUAN_WISUDA
        FROM MAHASISWA
        LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
        LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
        LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
        LEFT JOIN PENGAJUAN_WISUDA ON PENGAJUAN_WISUDA.ID_MHS = MAHASISWA.ID_MHS
        LEFT JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PENGAJUAN_WISUDA.ID_PERIODE_WISUDA
        LEFT JOIN JENJANG J2 ON J2.ID_JENJANG = PERIODE_WISUDA.ID_JENJANG
        LEFT JOIN TARIF_WISUDA ON PERIODE_WISUDA.ID_TARIF_WISUDA = TARIF_WISUDA.ID_TARIF_WISUDA
        WHERE NIM_MHS = '{$nim}' AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}' AND PENGAJUAN_WISUDA.YUDISIUM <> 0"
    );
    $smarty->assign('pengajuan_wisuda', $pw);
}

if ($_POST['nim']) {
    $nim = $_POST['nim'];

    if ($_POST['mode'] == 'hapus' and $_POST['id_mhs']) {
        deleteData("DELETE PENGAJUAN_WISUDA WHERE ID_MHS = '$_POST[id_mhs]'");
        deleteData("DELETE ADMISI WHERE ID_MHS = '$_POST[id_mhs]' AND STATUS_AKD_MHS = 16");
        UpdateData("UPDATE MAHASISWA SET STATUS_AKADEMIK_MHS = '1', STATUS_CEKAL = '0' WHERE ID_MHS = '$_POST[id_mhs]'");
        $pesan = "Hapus Pengajuan Yudisium";
    }

    if ($_POST['mode'] == 'batal' and $_POST['id_mhs']) {
        $id_pengajuan_wisuda = $_POST['id_pengajuan_wisuda'];
        $keterangan_batal = $_POST['keterangan_batal'];

        UpdateData("UPDATE PENGAJUAN_WISUDA SET YUDISIUM = 0, KETERANGAN_BATAL = '{$keterangan_batal}' WHERE ID_PENGAJUAN_WISUDA = '{$id_pengajuan_wisuda}'");
        UpdateData("UPDATE ADMISI SET KETERANGAN = 'Batal Yudisium' WHERE ID_PENGAJUAN_WISUDA = '{$id_pengajuan_wisuda}'");
        UpdateData("UPDATE MAHASISWA SET STATUS_CEKAL = '0' WHERE ID_MHS = '$_POST[id_mhs]'");
        $pesan = "Batalkan Pengajuan Yudisium";
    }

    // Cek ID Mahasiswa
    $get_id = getvar(
        "SELECT ID_MHS FROM mahasiswa m
        JOIN program_studi ps ON ps.id_program_studi = m.id_program_studi
        JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
        WHERE nim_mhs = '{$nim}' AND f.id_perguruan_tinggi = '{$id_pt_user}'"
    );
    $id_mhs = $get_id['ID_MHS'];

    // Cek Mahasiswa exist
    $cek = getvar(
        "SELECT count(*) as JML FROM mahasiswa m
        JOIN program_studi ps ON ps.id_program_studi = m.id_program_studi
        WHERE nim_mhs = '{$nim}' AND ps.id_fakultas = '{$id_fakultas}'"
    );

    // Jika mahasiswa ada
    if ($cek['JML'] == 1) {
        // Cek status Aktif
        $cek = getvar(
            "SELECT COUNT(*) AS JML FROM MAHASISWA m
            JOIN STATUS_PENGGUNA sp ON sp.ID_STATUS_PENGGUNA = m.STATUS_AKADEMIK_MHS
            JOIN program_studi ps ON ps.id_program_studi = m.id_program_studi
            WHERE m.nim_mhs = '{$nim}' AND ps.id_fakultas = '{$id_fakultas}' AND m.STATUS_AKADEMIK_MHS IN (1,16) AND sp.STATUS_AKTIF = 1"
        );
        
        // Jika mahasiswa aktif
        if ($cek['JML'] == 1) {
                $tgl = date('Y-m-d');
                
                // cek periode wisuda sudah disetting / belum
                $periode_ws = getvar(
                    "SELECT COUNT(*) AS CEK
                    FROM PERIODE_WISUDA
                    LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_JENJANG = PERIODE_WISUDA.ID_JENJANG
                    LEFT JOIN MAHASISWA ON MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
                    WHERE NIM_MHS = '{$nim}' AND STATUS_AKTIF = 1 AND TGL_BAYAR_SELESAI >= SYSDATE"
                );

            if ($periode_ws['CEK'] == 0) {
                $pesan = 'Periode Wisuda belum disetting, harap hubungi bagian akademik pusat';
            } else {
                        // Ambil biodata
                        $pw = getData(
                            "SELECT MAHASISWA.ID_MHS, NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, JENJANG.NM_JENJANG, YUDISIUM, PROGRAM_STUDI.ID_JENJANG, NM_TARIF_WISUDA || ' - ' || J2.NM_JENJANG AS NM_TARIF_WISUDA, TO_CHAR(PERIODE_WISUDA.TGL_BAYAR_MULAI, 'DD-MM-YYYY') AS TGL_BAYAR_MULAI
                                FROM MAHASISWA
                                LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
                                LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
                                LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
                                LEFT JOIN PENGAJUAN_WISUDA ON PENGAJUAN_WISUDA.ID_MHS = MAHASISWA.ID_MHS
                                LEFT JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PENGAJUAN_WISUDA.ID_PERIODE_WISUDA
                                LEFT JOIN JENJANG J2 ON J2.ID_JENJANG = PERIODE_WISUDA.ID_JENJANG
                                LEFT JOIN TARIF_WISUDA ON PERIODE_WISUDA.ID_TARIF_WISUDA = TARIF_WISUDA.ID_TARIF_WISUDA
                                WHERE NIM_MHS = '{$nim}' AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}'"
                        );
                        $smarty->assign('pengajuan_wisuda', $pw);
            }
        } else {
            $cek = getData("SELECT NM_STATUS_PENGGUNA, NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, NM_JENJANG FROM MAHASISWA 
                            JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
                            JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
                            JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
                            JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
                            WHERE ID_MHS = '{id_mhs}'");
            $pesan = "Bukan Mahasiswa Aktif atau Calon Lulus (" . $cek[0]['NM_STATUS_PENGGUNA'] . ")";
            $smarty->assign('cek', $cek);
        }
    } else {
        $cek = getvar("SELECT NM_FAKULTAS FROM MAHASISWA 
                        JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
                        JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
                        WHERE ID_MHS = '{$id_mhs}'");

        $pesan = "Mahasiswa tidak terdaftar, mahasiswa terdaftar di Fakultas " . $cek[0]['NM_FAKULTAS'];
    }
}

// Jika posting ada id_mhs dan bukan mode hapus
if ($_POST['id_mhs'] <> '' and $_POST['mode'] <> 'hapus' and $_POST['mode'] <> 'batal') {
    $tarif = getvar(
        "SELECT PERIODE_WISUDA.BESAR_BIAYA, ID_PERIODE_WISUDA 
        FROM PERIODE_WISUDA
        LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_JENJANG = PERIODE_WISUDA.ID_JENJANG
        LEFT JOIN MAHASISWA ON MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
        WHERE ID_MHS = '{$_POST['id_mhs']}' AND STATUS_AKTIF = 1"
    );
    
    // Mengambil semester aktif
    $row = getvar("SELECT ID_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True' AND ID_PERGURUAN_TINGGI = '{$id_pt_user}'");

    $db->BeginTransaction();

    // Update mahasiswa menjadi calon lulus & set cekal
    $hasil1 = UpdateData("UPDATE MAHASISWA SET STATUS_AKADEMIK_MHS = '16', STATUS_CEKAL = 1 WHERE ID_MHS = '{$_POST['id_mhs']}'");

    if ($hasil1) {
        $db->Commit();
    } else {
        $db->Rollback();
        die('gagal input1');
    }

    $tgl = date('Y-m-d');

    // cek periode wisuda sudah disetting / belum
    $cek_pengajuan = getvar(
        "SELECT COUNT(*) AS CEK
        FROM PENGAJUAN_WISUDA
        WHERE ID_MHS = '{$_POST['id_mhs']}'"
    );

    /*if($cek_pengajuan['CEK'] == 0){*/
        // ambil ID dari seq
        $pengajuan_wisuda = getvar("select PENGAJUAN_WISUDA_SEQ.nextval AS ID_PENGAJUAN_WISUDA from dual");

        $id_pengajuan_wisuda = $pengajuan_wisuda['ID_PENGAJUAN_WISUDA'];

        // Insert pengajuan wisuda
        $hasil2 = InsertData("INSERT INTO PENGAJUAN_WISUDA (ID_PENGAJUAN_WISUDA, ID_MHS, YUDISIUM, ID_PERIODE_WISUDA, TGL_PENGAJUAN_WISUDA, CREATED_BY) 
                        VALUES ('{$id_pengajuan_wisuda}','{$_POST['id_mhs']}', '2', '" . $tarif['ID_PERIODE_WISUDA'] . "', to_date('{$tgl}', 'YYYY-MM-DD'),'{$id_pengguna}')");

    if ($hasil2) {
        $db->Commit();
    } else {
        $db->Rollback();
        die('gagal input2');
    }

    // Insert admisi calon lulus
    $hasil3 = InsertData("INSERT INTO ADMISI (ID_MHS, STATUS_AKD_MHS, TGL_USULAN, TGL_APV, ID_SEMESTER, STATUS_APV, CREATED_BY, ID_PENGAJUAN_WISUDA)
                    VALUES ('{$_POST['id_mhs']}', '16', to_date('{$tgl}', 'YYYY-MM-DD'), to_date('{$tgl}', 'YYYY-MM-DD'), '{$row['ID_SEMESTER']}'
                    ,'1', '{$id_pengguna}', '{$id_pengajuan_wisuda}')");

    if ($hasil3) {
        $db->Commit();
    } else {
        $db->Rollback();
        die('gagal input3');
    }
    
    // Tampilkan biodata (lagi
    $pw = getData(
        "SELECT MAHASISWA.ID_MHS, NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, NM_JENJANG, YUDISIUM, PROGRAM_STUDI.ID_JENJANG, NM_TARIF_WISUDA
        FROM MAHASISWA
        LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
        LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
        LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
        LEFT JOIN PENGAJUAN_WISUDA ON PENGAJUAN_WISUDA.ID_MHS = MAHASISWA.ID_MHS				
        LEFT JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PENGAJUAN_WISUDA.ID_PERIODE_WISUDA
        LEFT JOIN TARIF_WISUDA ON PERIODE_WISUDA.ID_TARIF_WISUDA = TARIF_WISUDA.ID_TARIF_WISUDA
        WHERE NIM_MHS = '{$nim}' AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}'"
    );
    $smarty->assign('pengajuan_wisuda', $pw);
    
    $pesan = "Pengajuan Yudisium Berhasil";
}

// Ambil periode wisuda
$pr = getvar(
    "SELECT NM_TARIF_WISUDA || ' - ' || JENJANG.NM_JENJANG AS NM_TARIF_WISUDA 
    FROM PERIODE_WISUDA
    JOIN TARIF_WISUDA ON TARIF_WISUDA.ID_TARIF_WISUDA = PERIODE_WISUDA.ID_TARIF_WISUDA
    JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_JENJANG = PERIODE_WISUDA.ID_JENJANG
    JOIN JENJANG ON JENJANG.ID_JENJANG = PERIODE_WISUDA.ID_JENJANG
    JOIN MAHASISWA ON MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
    WHERE NIM_MHS = '$nim' AND PROGRAM_STUDI.ID_FAKULTAS = '$id_fakultas' AND PERIODE_WISUDA.STATUS_AKTIF = 1"
);
$smarty->assign('periode', $pr);

$smarty->assign('pesan', $pesan);

if ($_GET['mode'] == 'batal') {
    $smarty->display('wisuda-pengajuan-batal.tpl');
} else {
    $smarty->display('wisuda-pengajuan.tpl');
}
