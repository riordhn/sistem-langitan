<?php
//error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

//CEK
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');

$id_pengguna= $user->ID_PENGGUNA; 
$kdfak=$user->ID_FAKULTAS; 
$smarty->assign('FAK',$kdfak);
//smt aktif
$smtaktif=getvar("select id_semester,thn_akademik_semester,nm_semester from semester where status_aktif_semester='True'");	
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);
$id_semester=$smtaktif['ID_SEMESTER'];


//last smt
if($smtaktif['NM_SEMESTER']=="Ganjil") {
	$last_smt=getvar("select id_semester from semester where thn_akademik_semester=$smtaktif[THN_AKADEMIK_SEMESTER]-1 and nm_semester='Genap'");	
	
}else if($smtaktif['NM_SEMESTER']=="Genap") {
	$last_smt=getvar("select id_semester from semester where thn_akademik_semester=$smtaktif[THN_AKADEMIK_SEMESTER] and nm_semester='Ganjil'");	
	
}

$nim_mhs_asing = $_REQUEST['nim'];

$detailmhs= getvar("select mahasiswa_asing.id_pengguna, mahasiswa_asing.id_mhs_asing,mahasiswa_asing.id_program_studi,nm_pengguna,
nm_jenjang||'-'||nm_program_studi as nm_program_studi,nim_mhs_asing,jenjang.id_jenjang
from mahasiswa_asing
left join pengguna on mahasiswa_asing.id_pengguna=pengguna.id_pengguna
left join program_studi on mahasiswa_asing.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where nim_mhs_asing = '".$nim_mhs_asing."' and program_studi.id_fakultas=$kdfak");

$smarty->assign('nim', $detailmhs['NIM_MHS_ASING']);
$smarty->assign('nama', $detailmhs['NM_PENGGUNA']);
$smarty->assign('prodi', $detailmhs['NM_PROGRAM_STUDI']);

		
//get IPS
$datamhs= getvar("select id_mhs, sum(kredit_semester) as sks_total, sum((bobot*kredit_semester)) as bobot_total, 
case when sum(bobot*kredit_semester)=0 then 0 else 
round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips
from 
(select m.id_mhs_asing as id_mhs, 
case when (a.nilai_angka <= 0 or a.nilai_huruf = 'E' or a.nilai_angka is null or a.nilai_huruf is null) and d.status_mkta in (1,2) then 0
else d.kredit_semester end as kredit_semester,
case when a.nilai_huruf is null then 'E' else a.nilai_huruf end as nilai_huruf,
case when e.nilai_standar_nilai is null then 0 else e.nilai_standar_nilai end as bobot
from pengambilan_mk_asing a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
left join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
left join mahasiswa_asing m on a.id_mhs=m.id_mhs_asing
where a.id_semester=$last_smt[ID_SEMESTER] and a.status_apv_pengambilan_mk='1' and m.nim_mhs_asing='".$nim_mhs_asing."'
)
group by id_mhs");

$smarty->assign('ips',$datamhs['IPS']);

//get IPK
$dataipk= getvar("select id_mhs, sum(sks) as ttl_sks, round((sum((sks * bobot)) / sum(sks)), 2) as ipk
from
(select id_mhs, kd_mata_kuliah as kode, nm_mata_kuliah as nama, kredit_semester as sks, min(nilai_huruf) as nilai, max(nilai_standar_nilai) as bobot
from 
(select m.id_mhs_asing as id_mhs, c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai
from pengambilan_mk_asing a
join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
join mahasiswa_asing m on a.id_mhs=m.id_mhs_asing
where a.status_apv_pengambilan_mk = 1 and a.status_hapus=0 and a.nilai_huruf<>'E' and a.nilai_huruf is not null and a.id_semester<>$id_semester and m.nim_mhs_asing='".$nim_mhs_asing."')
group by id_mhs, kd_mata_kuliah, nm_mata_kuliah, kredit_semester)
group by id_mhs");

$smarty->assign('ttl_sks',$dataipk['TTL_SKS']);
$smarty->assign('ipk',$dataipk['IPK']);



$status = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'krs';

switch($status) {

case 'add':
		 // pilih
		
		$smarty->assign('disp1','block');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		
		$id_kelas_mk= $_GET['id_kelas_mk'];
		$sks= $_GET['sks'];
		$kaps= $_GET['kaps'];
		
		//get jumlah pst
		$cekpst= getvar("select count ( distinct id_mhs) as pst from pengambilan_mk_asing where id_kelas_mk=$id_kelas_mk and status_apv_pengambilan_mk=1 and id_semester=$id_semester");
	
		$batas=24;
		

		if (($cekpst['PST']+1) <= $kaps) {
				if (($sksambil['TTLSKS']+$sks) <= $batas) {
					
					if ($kdfak == 10 or $kdfak == 12)
					{
						
					$id_kur=getvar("select id_kurikulum_mk from kelas_mk where id_kelas_mk='".$id_kelas_mk."'");
					} else {

					$id_kur=getvar("select id_kurikulum_mk,id_mata_kuliah from kurikulum_mk kmk1
									where kmk1.id_program_studi='$detailmhs[ID_PROGRAM_STUDI]' and kmk1.id_mata_kuliah in
									(select id_mata_kuliah from mata_kuliah where kd_mata_kuliah in
									(select c.kd_mata_kuliah from kelas_mk a, kurikulum_mk b, mata_kuliah c
									where a.id_kurikulum_mk=b.id_kurikulum_mk and a.id_kelas_mk='".$id_kelas_mk."' 
									and c.id_mata_kuliah=b.id_mata_kuliah))");
					}
									
					InsertData("insert into pengambilan_mk_asing (id_mhs,id_semester,status_pengambilan_mk,status_apv_pengambilan_mk,id_kelas_mk,id_kurikulum_mk,keterangan)
					values ('$detailmhs[ID_MHS_ASING]','$id_semester','9','1','$id_kelas_mk','".$id_kur['ID_KURIKULUM_MK']."','KRS AKADEMIK OLEH ".$id_pengguna."')");
					
					
					//echo "insert into pengambilan_mk_asing (id_mhs,id_semester,status_pengambilan_mk,status_apv_pengambilan_mk,id_kelas_mk,id_kurikulum_mk)
					//values ('$detailmhs[ID_MHS]','$id_semester','9','1','$id_kelas_mk','".$id_kur['ID_KURIKULUM_MK']."')";
				}else{
					?>
					<script>alert("SKS maksimum anda sudah lebih dari <?=$batas?>sks")</script>	
					<?php
				}
			

		} else
		{
			 echo '<script>alert("Kapasitas Kelas Tidak Cukup")</script>';
		}



break;

   
case 'del':
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block');
		

	   $id_pengambilan_mk_asing= $_GET['idpmk'];
	
       //deletedata("delete from nilai_mk where id_pengambilan_mk_asing=$id_pengambilan_mk_asing");
	   deletedata("delete from pengambilan_mk_asing where id_pengambilan_mk_asing=$id_pengambilan_mk_asing");

        break;   
}



//dayat =  and status_apv_pengambilan_mk=1 -> saya hapus
$mhs_status1=getData("select krs_prodi.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,nm_jadwal_hari,nm_jadwal_jam, 
sum(case when status_pengambilan_mk>=1 then 1 else 0 end) as pst, kapasitas_kelas_mk 
from krs_prodi
left join pengambilan_mk_asing on krs_prodi.id_kelas_mk=pengambilan_mk_asing.id_kelas_mk and krs_prodi.id_semester=pengambilan_mk_asing.id_semester 
left join kelas_mk on krs_prodi.id_kelas_mk=kelas_mk.id_kelas_mk and krs_prodi.id_semester=kelas_mk.id_semester
left join nama_kelas on kelas_mk.no_kelas_mk=id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
where krs_prodi.id_program_studi='$detailmhs[ID_PROGRAM_STUDI]' and krs_prodi.id_semester=$id_semester  and 
krs_prodi.id_kelas_mk not in (select id_kelas_mk from pengambilan_mk_asing where id_mhs='$detailmhs[ID_MHS_ASING]' and id_semester=$id_semester and id_kelas_mk is not null)
GROUP BY krs_prodi.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,nm_jadwal_hari,nm_jadwal_jam,kapasitas_kelas_mk
order by kd_mata_kuliah,nama_kelas");


$smarty->assign('T_KRS', $mhs_status1);

$data_mhs_krs=getData("select id_pengambilan_mk_asing as id_pengambilan_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,nm_jadwal_hari,nm_jadwal_jam, 
status_pengambilan_mk,case when (status_apv_pengambilan_mk=1) then 'DiSetujui' else 'Blm Disetujui' end as status, (jam_mulai||':'||menit_mulai||' - '||jam_selesai||':'||menit_selesai) as jam
 from pengambilan_mk_asing
left join kelas_mk on pengambilan_mk_asing.id_kelas_mk=kelas_mk.id_kelas_mk
left join nama_kelas on kelas_mk.no_kelas_mk=id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
where pengambilan_mk_asing.id_mhs in (select id_mhs_asing from mahasiswa_asing where nim_mhs_asing='".$nim_mhs_asing."') 
and pengambilan_mk_asing.id_semester=$id_semester and id_kelas_mk is not null
order by kd_mata_kuliah,nama_kelas");

$smarty->assign('T_AMBIL', $data_mhs_krs);


$smarty->display('akademik-krs-asing.tpl');
?>
