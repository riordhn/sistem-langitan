<?php
require('common.php');
require_once ('ociFunction.php');

$id_pengguna= $user->ID_PENGGUNA; 
$fak=getvar("select fakultas.id_fakultas from pegawai left join unit_kerja on pegawai.id_unit_kerja=unit_kerja.id_unit_kerja 
left join program_studi on unit_kerja.id_program_studi=program_studi.id_program_studi 
left join fakultas on program_studi.id_fakultas=fakultas.id_fakultas 
where pegawai.id_pengguna=$id_pengguna");
$kdfak=$fak['ID_FAKULTAS'];

$kdp=getvar("select id_program_studi from 
(select id_program_studi from program_studi
where id_fakultas=$kdfak order by id_jenjang)
where rownum between 0 and 1");
$kdprodi=$kdp['ID_PROGRAM_STUDI'];
$smarty->assign('DFT', $kdprodi);

$prodi=getData("select id_program_studi, nm_jenjang||'-'||nm_program_studi as prodi from program_studi 
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_fakultas=$kdfak order by program_studi.id_jenjang");
$smarty->assign('T_PRO', $prodi);

$smt=getData("select id_semester, tahun_ajaran||' - '||nm_semester as thnsmt from semester order by thnsmt desc");
$smarty->assign('T_SMT', $smt);

$smarty->display('report-info-dosen.tpl');
?>
