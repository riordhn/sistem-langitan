<?php
// error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$id_pengguna= $user->ID_PENGGUNA; 

$kdfak=$user->ID_FAKULTAS; 

$dataprodi=getData("select id_program_studi,nm_jenjang||'-' || nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_fakultas=$kdfak");
$smarty->assign('T_PRODI', $dataprodi);

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True'");
$smarty->assign('SMT_AKTIF', $smtaktif['ID_SEMESTER']);

$datasmt=getData("select id_semester, tahun_ajaran||' - '||nm_semester as smt from semester where thn_akademik_semester in (
select * from (select distinct thn_akademik_semester
from semester where thn_akademik_semester<=EXTRACT(YEAR FROM sysdate) order by thn_akademik_semester desc)
where rownum<=3) and nm_semester in ('Ganjil', 'Genap')
order by thn_akademik_semester desc, nm_semester desc");
$smarty->assign('T_SMT', $datasmt);

if ($_POST['action']=='view') {
$kdprodi= $_POST['kdprodi'];
$smarty->assign('PRODI', $kdprodi);
$idsmt= $_POST['thakd'];

$datajenjang=getvar("select id_jenjang from program_studi where id_program_studi='".$kdprodi."'");
$idjenjang=$datajenjang['ID_JENJANG'];

$datausulmk=getData("select kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nama_kelas,
nm_jadwal_hari,jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as nm_jadwal_jam,nm_ruangan, 
trim(wm_concat('<li>'||(case when pengampu_mk.pjmk_pengampu_mk=1 then '<b><font color=\"blue\">PJMK : '||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||'</font></b>'
else 'TIM : ' ||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang end))) as pengajar
from kelas_mk
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk 
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam 
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk 
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
where kelas_mk.id_program_studi=$kdprodi and kelas_mk.id_semester=$idsmt
group by kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nama_kelas,
nm_jadwal_hari,jam_mulai,menit_mulai,jam_selesai,menit_selesai,nm_ruangan");
} else {
$datausulmk=0;
}

$smarty->assign('T_USULMK', $datausulmk);

$smarty->display('usulan-mk-psikologi.tpl'); 
?>
