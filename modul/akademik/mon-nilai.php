<?php
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$kdfak= $user->ID_FAKULTAS; 

$smtaktif=getvar("select ID_SEMESTER from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND status_aktif_semester='True'");

$smt1 = isSet($_POST['kdthnsmt']) ? $_POST['kdthnsmt'] : $smtaktif['ID_SEMESTER'];

$smarty->assign('SMT',$smt1);

$thnsmt=getData("select id_semester as id_semester, tahun_ajaran||'-'||nm_semester||'-'||group_semester as thn_smt from semester 
where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester in (select distinct thn_akademik_semester
from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester>=EXTRACT(YEAR FROM sysdate)-2)
order by thn_akademik_semester desc, group_semester desc, nm_semester desc");
$smarty->assign('T_THNSMT', $thnsmt);


/*if($kdfak=='8'){
$nilai_masuk=getData("select kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nmkelas nama_kelas,gelar_depan||nm_pengguna||', '||gelar_belakang as pjma,
count(id_mhs) as pst,sum(case when nilai_angka is not null and nilai_angka >0 then 1 else 0 end) as nilmasuk,
case when(sum(case when pengambilan_mk.flagnilai='1' then 1 else 0 end)=count(id_mhs)) then 'PUBLISH' else 'BELUM' end as terpublish
from kelas_mk
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk and pjmk_pengampu_mk=1
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and kelas_mk.id_semester=pengambilan_mk.id_semester
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join fst_kelas on kelas_mk.tipe_kelas_mk=fst_kelas.kdkelas
where kelas_mk.id_semester=$smt1 and program_studi.id_fakultas=$kdfak and status_apv_pengambilan_mk=1
group by kd_mata_kuliah, nm_mata_kuliah, kurikulum_mk.kredit_semester, nmkelas, gelar_depan||nm_pengguna||', '||gelar_belakang
having sum(case when nilai_angka is not null and nilai_angka >0 then 1 else 0 end)>=(count(id_mhs)/2) and count(id_mhs)<>0");
}
else{*/
$nilai_masuk=getData("select kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nama_kelas,gelar_depan||nm_pengguna||', '||gelar_belakang as pjma,
nm_jenjang||'-'||coalesce(nm_singkat_prodi,nm_program_studi) as prodi,
count(id_mhs) as pst,sum(case when nilai_angka is not null and nilai_angka >0 then 1 else 0 end) as nilmasuk,
case when(sum(case when pengambilan_mk.flagnilai='1' then 1 else 0 end)=count(id_mhs)) then 'PUBLISH' else 'BELUM' end as terpublish
from kelas_mk
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk and pjmk_pengampu_mk=1
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and kelas_mk.id_semester=pengambilan_mk.id_semester
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
where kelas_mk.id_semester=$smt1 and program_studi.id_fakultas=$kdfak and status_apv_pengambilan_mk=1
group by kd_mata_kuliah, nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas, gelar_depan||nm_pengguna||', '||gelar_belakang,
nm_jenjang,nm_program_studi,nm_singkat_prodi
having sum(case when nilai_angka is not null and nilai_angka >0 then 1 else 0 end)>=(count(id_mhs)/2) and count(id_mhs)<>0
");
/*}*/

$smarty->assign('NIL_MSK',$nilai_masuk);

/*if($kdfak=='8'){
$nilai_blm_masuk=getData("select kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nmkelas nama_kelas,gelar_depan||nm_pengguna||', '||gelar_belakang as pjma,
count(id_mhs) as pst,sum(case when nilai_angka is null or nilai_angka <=0 then 1 else 0 end) as nilmasuk,
case when(sum(case when pengambilan_mk.flagnilai='1' then 1 else 0 end)=count(id_mhs)) then 'PUBLISH' else 'BELUM' end as terpublish
from kelas_mk
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk and pjmk_pengampu_mk=1
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and kelas_mk.id_semester=pengambilan_mk.id_semester
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join fst_kelas on kelas_mk.tipe_kelas_mk=fst_kelas.kdkelas
where kelas_mk.id_semester=$smt1 and program_studi.id_fakultas=$kdfak and status_apv_pengambilan_mk=1
group by kd_mata_kuliah, nm_mata_kuliah, kurikulum_mk.kredit_semester, nmkelas, gelar_depan||nm_pengguna||', '||gelar_belakang
having sum(case when nilai_angka is null or nilai_angka <=0 then 1 else 0 end)>=(count(id_mhs)/2) and count(id_mhs)<>0");

}
else{*/
$nilai_blm_masuk=getData("select kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nama_kelas,gelar_depan||nm_pengguna||', '||gelar_belakang as pjma,
nm_jenjang||'-'||coalesce(nm_singkat_prodi,nm_program_studi) as prodi,
count(id_mhs) as pst,sum(case when nilai_angka is null or nilai_angka <=0 then 1 else 0 end) as nilmasuk,
case when(sum(case when pengambilan_mk.flagnilai='1' then 1 else 0 end)=count(id_mhs)) then 'PUBLISH' else 'BELUM' end as terpublish
from kelas_mk
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk and pjmk_pengampu_mk=1
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and kelas_mk.id_semester=pengambilan_mk.id_semester
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
where kelas_mk.id_semester=$smt1 and program_studi.id_fakultas=$kdfak and status_apv_pengambilan_mk=1
group by kd_mata_kuliah, nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas, gelar_depan||nm_pengguna||', '||gelar_belakang,
nm_jenjang,nm_program_studi,nm_singkat_prodi
having sum(case when nilai_angka is null or nilai_angka <=0 then 1 else 0 end)>=(count(id_mhs)/2) and count(id_mhs)<>0
");
/*}*/
$smarty->assign('NIL_BLM_MSK',$nilai_blm_masuk);

$smarty->display('mon-nilai.tpl');

?>
