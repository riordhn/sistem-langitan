<?php
require('common.php');
require_once ('ociFunction.php');

error_reporting (E_ALL & ~E_NOTICE);
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$id_pengguna= $user->ID_PENGGUNA; 
$kdfak=$user->ID_FAKULTAS; 

$smtaktif=getvar("select id_semester from semester where status_aktif_sp='True'");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);

$smt=getData("select id_semester, tahun_ajaran||' - '||nm_semester||'-'||group_semester as smt from semester where thn_akademik_semester in (
select * from (select distinct thn_akademik_semester
from semester where thn_akademik_semester<=EXTRACT(YEAR FROM sysdate) order by thn_akademik_semester desc)
where rownum<=3) and tipe_semester in ('SP')
order by thn_akademik_semester desc, nm_semester desc");
$smarty->assign('T_ST', $smt);

//Edited by Yudi Sulistya, 13/07/2012
$smt1 = isSet($_POST['smt']) ? $_POST['smt'] : $smtaktif['ID_SEMESTER'];
$smarty->assign('SMT',$smt1);

$kuliah=getvar("SELECT TGL_MULAI_JSF,TGL_SELESAI_JSF FROM JADWAL_SEMESTER_FAKULTAS WHERE ID_SEMESTER=94 and ID_FAKULTAS=$kdfak and ID_KEGIATAN=86");
$uts=getvar("SELECT TGL_MULAI_JSF,TGL_SELESAI_JSF FROM JADWAL_SEMESTER_FAKULTAS WHERE ID_SEMESTER=94 and ID_FAKULTAS=$kdfak and ID_KEGIATAN=83");
$uas=getvar("SELECT TGL_MULAI_JSF,TGL_SELESAI_JSF FROM JADWAL_SEMESTER_FAKULTAS WHERE ID_SEMESTER=94 and ID_FAKULTAS=$kdfak and ID_KEGIATAN=84");

if ($_GET['action']=='cekal') {
	$idkelas= $_GET['id_kelas_mk'];
	$smarty->assign('ID_KELAS',$idkelas);
	$kd_mata_kuliah= $_GET['kd_mk'];
	
	$smarty->assign('disp1','none');
	$smarty->assign('disp2','block');

if($kdfak==4)
{	
$datacekal=getData("select s1.id_pengambilan_mk,s1.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,prodi,
nm_pengguna,nim_mhs,count(presensi_kelas.id_kelas_mk) as tm,s1.status,s1.id_mhs,
sum(case when presensi_mkmhs.kehadiran>0 then 1 else 0 end) as hadir,
round(sum(case when presensi_mkmhs.kehadiran>0 then 1 else 0 end)/count(presensi_kelas.id_kelas_mk)*100,2) as prosen
from (select id_pengambilan_mk,nm_pengguna,nim_mhs,pengambilan_mk.id_mhs,pengambilan_mk.id_kelas_mk,
kd_mata_kuliah,nm_mata_kuliah,nama_kelas,nm_jenjang||'-'||nm_program_studi as prodi,
case when pengambilan_mk.status_ulangke > 0 then 'ULANG' else 'BARU' end as status
from pengambilan_mk
left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk and kelas_mk.id_semester=$smt1 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
where pengambilan_mk.status_cekal='0' and pengambilan_mk.id_semester=$smt1  and kelas_mk.id_kelas_mk=$idkelas and status_apv_pengambilan_mk='1')s1
left join presensi_kelas on s1.id_kelas_mk=presensi_kelas.id_kelas_mk
left join presensi_mkmhs on presensi_kelas.id_presensi_kelas=presensi_mkmhs.id_presensi_kelas and s1.id_mhs=presensi_mkmhs.id_mhs
where tgl_presensi_kelas between to_date ('$uts[TGL_SELESAI_JSF]', 'dd-mm-yy') AND to_date ('$uas[TGL_MULAI_JSF]', 'dd-mm-yy') 
group by s1.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,prodi,nm_pengguna,nim_mhs,s1.id_pengambilan_mk,s1.status,s1.id_mhs
");
} else
{
$datacekal=getData("select s1.id_pengambilan_mk,s1.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,prodi,
nm_pengguna,nim_mhs,count(presensi_kelas.id_kelas_mk) as tm,s1.status,s1.id_mhs,
sum(case when presensi_mkmhs.kehadiran>0 then 1 else 0 end) as hadir,
round(sum(case when presensi_mkmhs.kehadiran>0 then 1 else 0 end)/count(presensi_kelas.id_kelas_mk)*100,2) as prosen
from (select id_pengambilan_mk,nm_pengguna,nim_mhs,pengambilan_mk.id_mhs,pengambilan_mk.id_kelas_mk,
kd_mata_kuliah,nm_mata_kuliah,nama_kelas,nm_jenjang||'-'||nm_program_studi as prodi,
case when pengambilan_mk.status_ulangke > 0 then 'ULANG' else 'BARU' end as status
from pengambilan_mk
left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk and kelas_mk.id_semester=$smt1 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
where pengambilan_mk.status_cekal='0' and pengambilan_mk.id_semester=$smt1  and kelas_mk.id_kelas_mk=$idkelas and status_apv_pengambilan_mk='1')s1
left join presensi_kelas on s1.id_kelas_mk=presensi_kelas.id_kelas_mk
left join presensi_mkmhs on presensi_kelas.id_presensi_kelas=presensi_mkmhs.id_presensi_kelas and s1.id_mhs=presensi_mkmhs.id_mhs
where tgl_presensi_kelas between to_date ('$kuliah[TGL_MULAI_JSF]', 'dd-mm-yy') AND to_date ('$uas[TGL_MULAI_JSF]', 'dd-mm-yy') 
group by s1.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,prodi,nm_pengguna,nim_mhs,s1.id_pengambilan_mk,s1.status,s1.id_mhs
");


}
$smarty->assign('DATA_CEKAL', $datacekal);
}	

if ($_POST['action']=='proses') {
	$idkelas= $_POST['id_kelas_mk'];
	$counter=$_POST['counter'];
	$no_memo=$_POST['no_memo'];
	$catatan=$_POST['catatan'];
	//echo $no_memo;
//	echo $catatan;
	
if 	($no_memo==null ||trim($no_memo)==''||$catatan==null||trim($catatan)=='')
{
	echo '<script>alert("NOMER MEMO dan CATATAN WAJIB DI_ISI")</script>';
} else {

	$smarty->assign('disp1','block');
	$smarty->assign('disp2','none');
	
	//$mhs=getData("select id_mhs from pengambilan_mk where id_kelas_mk=$idkelas  and id_semester='$smtaktif[ID_SEMESTER]'");
	

for ($i=1; $i<=$counter; $i++)
	{
	//echo "masuk";echo $_POST['cekalmhs'.$i];
	//echo $mhs['ID_MHS'];
	if ($_POST['cekalmhs'.$i]<>'' || $_POST['cekalmhs'.$i]<>null) {
			   gantidata("update pengambilan_mk set status_cekal='1' where id_kelas_mk=$idkelas and id_mhs='".$_POST['cekalmhs'.$i]."' and id_semester='$smtaktif[ID_SEMESTER]'");
			   $id_peng_mk=getvar("select id_pengambilan_mk from pengambilan_mk where id_semester='$smtaktif[ID_SEMESTER]' and id_kelas_mk=$idkelas and id_mhs='".$_POST['cekalmhs'.$i]."'");
			   $today = date('d-M-y');
			   gantidata("insert into buka_cekal (id_mhs,id_pengambilan_mk,catatan,jenis_cekal,tgl_insert,id_pengguna,no_memo)
						  values ('".$_POST['cekalmhs'.$i]."','$id_peng_mk[ID_PENGAMBILAN_MK]','$catatan','BUKA CEKAL UAS-SP','$today',$id_pengguna,'$no_memo')");
			   //echo "update pengambilan_mk set status_cekal_uts='1' where id_kelas_mk=$idkelas and id_mhs='".$_POST['cekalmhs'.$i]."' and id_semester='$smtaktif[ID_SEMESTER]'";
			   //echo "insert into buka_cekal (id_mhs,id_pengambilan_mk,catatan,jenis_cekal,tgl_insert,id_pengguna,no_memo)
				//	  values ('".$_POST['cekalmhs'.$i]."','$id_peng_mk[ID_PENGAMBILAN_MK]','$catatan','BUKA CEKAL UTS','$today',$id_pengguna,'$no_memo')";
			} 
	}
	 //gantidata("update pengambilan_mk set status_cekal='1' where id_kelas_mk=$idkelas and (status_cekal='0' or status_cekal is null) and id_semester='$smtaktif[ID_SEMESTER]'");
	 //gantidata("update pengambilan_mk set status_cekal_uts='1' where id_kelas_mk=$idkelas and (status_cekal_uts='2' or status_cekal_uts is null or status_cekal_uts='3') and id_semester='$smtaktif[ID_SEMESTER]'");
	 //gantidata("update pengambilan_mk set status_cekal_uts='3' where id_kelas_mk=$idkelas and (status_cekal_uts='90') and id_semester='$smtaktif[ID_SEMESTER]'");

	 //echo "update pengambilan_mk set status_cekal=1 where id_kelas_mk=$idkelas and status_cekal=2  and id_semester='$smtaktif[ID_SEMESTER]'";
}
}

if($kdfak==4)
{
$jaf=getData("select s1.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,prodi,ttl,count(presensi_kelas.id_kelas_mk) as tm from
(select pengambilan_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,nm_jenjang||'-'||nm_program_studi as prodi,
count(id_mhs)as ttl from pengambilan_mk
left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk and kelas_mk.id_semester=$smt1 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where pengambilan_mk.status_cekal='0' and pengambilan_mk.id_semester=$smt1  and id_fakultas=$kdfak and status_apv_pengambilan_mk='1'
group by pengambilan_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,nm_jenjang||'-'||nm_program_studi)s1
left join presensi_kelas on s1.id_kelas_mk=presensi_kelas.id_kelas_mk
where tgl_presensi_kelas between to_date ('$uts[TGL_SELESAI_JSF]', 'dd-mm-yy') AND to_date ('$uas[TGL_MULAI_JSF]', 'dd-mm-yy')
group by s1.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,prodi,ttl
");
} else {
$jaf=getData("select s1.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,prodi,ttl,count(presensi_kelas.id_kelas_mk) as tm from
(select pengambilan_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,nm_jenjang||'-'||nm_program_studi as prodi,
count(id_mhs)as ttl from pengambilan_mk
left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk and kelas_mk.id_semester=$smt1 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where pengambilan_mk.status_cekal='0' and pengambilan_mk.id_semester=$smt1 and id_fakultas=$kdfak and status_apv_pengambilan_mk='1'
group by pengambilan_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,nm_jenjang||'-'||nm_program_studi)s1
left join presensi_kelas on s1.id_kelas_mk=presensi_kelas.id_kelas_mk
where tgl_presensi_kelas between to_date ('$kuliah[TGL_MULAI_JSF]', 'dd-mm-yy') AND to_date ('$uas[TGL_MULAI_JSF]', 'dd-mm-yy') 
group by s1.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,prodi,ttl
");
}
$smarty->assign('T_MK', $jaf);

$smarty->display('mon-cekal-uassp.tpl');

?>
