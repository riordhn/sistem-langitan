<?php
/** 
* @author Choirul Hidayat, http://www.choirulhidayat.wordpress.com [Freelance Programmer]
* @contributor all AUCC Developers, dibuat berdasarkan manual PHP5 dan OCI8
* @start Wednesday, March 09, 2011
* @lastchange Wednesday, March 09, 2011
* @copyright 2011 Universitas Airlangga
* @project Airlangga University - Cyber Campus
* @link http://www.unair.ac.id
/*===========================================================================*/
ob_start();
require('common.php');
include "class/aucc.class.php";
$aucc = new AUCC_Akademik($db);

$kdfak = $aucc->get_program_studi_by_id(post('prodi'));

if (post('prodi') and post('semester') and post('bayar')) {
	
	$pembayaran = $aucc->data_pembayaran(post('prodi'), post('semester'), post('bayar'));
	
echo "
 <table>
        <tr>
            <th style='text-align: center;'>No</th>
            <th style='text-align: center;'>NIM</th>
            <th style='text-align: center;'>Nama</th>
            <th style='text-align: center;'>Besar Biaya</th>
        </tr>
";
		$no = 1;
      foreach($pembayaran as $data){
       echo "<tr>
            <td>$no</td>
            <td>$data[NIM_MHS]</td>
            <td>$data[NM_PENGGUNA]</td>
            <td style='text-align: right;'>$data[BESAR_BIAYA]</td>         
        </tr>";
		$no++;
	  }
echo "</table>";

}

$nm_file = "pembayaran-mhs-".$kdfak['NM_JENJANG']."-".$kdfak['NM_PROGRAM_STUDI'];
header("Content-disposition: attachment; filename=$nm_file.xls");
header("Content-type: Application/exe");
header("Content-Transfer-Encoding: binary");
ob_flush();
?>