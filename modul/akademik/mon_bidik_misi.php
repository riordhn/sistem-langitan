<?php
/*
Yudi Sulistya 10/05/2012
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$kdprodi=$user->ID_PROGRAM_STUDI;

$id_pengguna= $user->ID_PENGGUNA;
$kdfak=$user->ID_FAKULTAS;
$smarty->assign('FAK', $kdfak);

$thnsmt=getData("select id_semester, tahun_ajaran||' - '||nm_semester as thn_smt from semester where thn_akademik_semester in (
select * from (select distinct thn_akademik_semester
from semester where thn_akademik_semester<=EXTRACT(YEAR FROM sysdate) order by thn_akademik_semester desc)
where rownum<=10) and nm_semester in ('Ganjil', 'Genap')
order by thn_akademik_semester desc, nm_semester desc");
$smarty->assign('T_THNSMT', $thnsmt);

$dataprodi=getData("select id_program_studi,nm_jenjang||'-' || nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_fakultas=$kdfak and status_aktif_prodi = 1");
$smarty->assign('T_PRODI', $dataprodi);

if (isset($_REQUEST['action'])=='view') {
$kdprodi = $_POST['kdprodi'];
$smarty->assign('PRODI', $kdprodi);

$smt = $_POST['kdthnsmt'];
//$smtbm = $_POST['kdthnsmtbm'];

$smarty->assign('SMT_AKAD', $smt);
$ipk_smt=getvar("select tahun_ajaran||' - '||nm_semester as thn_smt,thn_akademik_semester from semester where id_semester=$smt");
$smarty->assign('THN_AKAD', $ipk_smt['THN_SMT']);
$smarty->assign('TAHUN_SMT', $ipk_smt['THN_AKADEMIK_SEMESTER']);
//echo $smt;

$dataprodi1=getvar("select nm_jenjang||'-'||nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_program_studi=$kdprodi and status_aktif_prodi = 1");
$smarty->assign('NM_PRODI', $dataprodi1['PRODI']);

$smt_temp=getvar("select case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,thn_akademik_semester from semester where id_semester=$smt");
$smarty->assign('THN_HITUNG', $smt_temp['TAHUN']);


if ($kdprodi=='ALL')
{    
	$filter="program_studi.id_fakultas='$kdfak'";
} else {
	$filter="program_studi.id_fakultas='$kdfak' and program_studi.id_program_studi='$kdprodi'";
}

$mk=getData("select s1.id_mhs,nim_mhs,nm_mhs,status,prodi,doli,ips,sks_sem,SKS_TOTAL_MHS,IPK_MHS from (
select MAHASISWA.NIM_MHS,mahasiswa.id_mhs,  a.nm_pengguna as nm_mhs, status_akademik_mhs,nm_status_pengguna as status,
nm_jenjang||'-'||nm_program_studi as prodi,
b.gelar_depan||' '||b.nm_pengguna||', '||b.gelar_belakang as doli
from sejarah_beasiswa
join beasiswa on sejarah_beasiswa.id_beasiswa=beasiswa.id_beasiswa
join mahasiswa on sejarah_beasiswa.id_mhs = mahasiswa.id_mhs
join pengguna a on a.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
join status_pengguna on mahasiswa.status_akademik_mhs=STATUS_PENGGUNA.id_status_pengguna
join program_studi on MAHASISWA.id_program_studi=PROGRAM_STUDI.id_program_studi
join jenjang on PROGRAM_STUDI.id_jenjang=JENJANG.id_jenjang
join dosen_wali on DOSEN_WALI.id_mhs=MAHASISWA.id_mhs and status_dosen_wali=1
join dosen on DOSEN_WALI.id_dosen=DOSEN.id_dosen
join pengguna b on DOSEN.id_pengguna=b.id_pengguna 			
where id_group_beasiswa=16 and $filter
and sejarah_beasiswa.id_semester=$smt
order by PROGRAM_STUDI.id_program_studi,mahasiswa.nim_mhs)s1
left join
(select id_mhs,case when sum(bobot*kredit_semester)=0 then 0 else 
round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips,
case when id_fakultas=7 then sum(sksreal) else sum(kredit_semester) end as sks_sem 
from (
select id_mhs,id_fakultas,id_kurikulum_mk,kredit_semester, sksreal,min(nilai_huruf) as nilai, max(bobot) as bobot from
(select a.id_mhs,a.id_kurikulum_mk,id_fakultas, 
case when (a.nilai_huruf = 'E' or a.nilai_huruf is null) 
and d.status_mkta in (1,2) then 0
else d.kredit_semester end as kredit_semester,d.kredit_semester as sksreal,
case when a.nilai_huruf is null then 'E' else a.nilai_huruf end as nilai_huruf,
case when e.nilai_standar_nilai is null then 0 else e.nilai_standar_nilai end as bobot
from pengambilan_mk a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join program_studi on m.id_program_studi=program_studi.id_program_studi
left join semester s on a.id_semester=s.id_semester
where group_semester||thn_akademik_semester in
(select group_semester||thn_akademik_semester from semester where id_Semester=$smt)
and tipe_semester in ('UP','REG','RD') 
and a.status_apv_pengambilan_mk='1' and $filter and a.status_hapus=0 
and a.status_pengambilan_mk !=0)
group by id_mhs,id_kurikulum_mk,id_fakultas,kredit_semester, sksreal)
group by id_mhs,id_fakultas)s2 on s1.id_mhs=s2.id_mhs
left join
(select id_mhs, sum(kredit_semester) as SKS_TOTAL_MHS, 
round((sum((kredit_semester * nilai_standar_nilai)) / sum(kredit_semester)), 2) as IPK_MHS
from 
(select a.id_mhs, c.kd_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai,
case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,
row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a
join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
join semester smt on a.id_semester=smt.id_semester
join mahasiswa m on a.id_mhs=m.id_mhs
join program_studi on m.id_program_studi=program_studi.id_program_studi
where a.status_apv_pengambilan_mk = 1 and (a.nilai_huruf<>'E' or a.nilai_huruf<>null) 
and $filter and a.status_hapus=0 
and a.status_pengambilan_mk !=0)
where tahun<='$smt_temp[TAHUN]' and rangking=1
group by id_mhs) s3 on s1.id_mhs=s3.id_mhs
order by nim_mhs");

$smarty->assign('T_MK', $mk);

}

$smarty->display('mon_bidik_misi.tpl');
?>
