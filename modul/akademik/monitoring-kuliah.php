<?php
require('common.php');
require_once ('ociFunction.php');

error_reporting (E_ALL & ~E_NOTICE);

$id_pengguna= $user->ID_PENGGUNA; 


$kdfak=$user->ID_FAKULTAS; 

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True'");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);

$idkur=getvar("select id_kurikulum_prodi from kurikulum_prodi
left join kurikulum on kurikulum_prodi.id_kurikulum=kurikulum.id_kurikulum
where kurikulum_prodi.id_program_studi=$kdprodi");
$smarty->assign('idkur', $idkur['ID_KURIKULUM_PRODI']);

$datakur=getData("select id_kurikulum_prodi,nm_kurikulum||'-'||tahun_kurikulum as nama  from kurikulum_prodi
left join kurikulum on kurikulum_prodi.id_kurikulum=kurikulum.id_kurikulum
where kurikulum_prodi.id_program_studi=$kdprodi");
$smarty->assign('T_KUR', $datakur);

$prodi=getData("select id_program_studi, nm_jenjang||'-'||nm_program_studi as namaprodi from program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where id_fakultas=$kdfak order by nm_jenjang");
$smarty->assign('T_PRODI', $prodi);

$smarty->assign('statuskls', array('R','AJ'));




$status = isset($_REQUEST['action'])? $_REQUEST['action'] : 'tampil';
//echo $status;

switch($status) {
case 'add':
		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
		
		//$smt1= $_GET['smt'];
		$id_mk=$_GET['id_mk'];

		tambahdata("kelas_mk","id_kurikulum_mk,id_semester,id_program_studi","'$id_mk','".$smtaktif['ID_SEMESTER']."','$kdprodi'");
		//if (!isFindData("jadwal_kelas","id_kelas_mk=$id_mk")) {tambahdata("jadwal_kelas","id_kelas_mk","$id_mk");}
		$tawar=getData("select id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_tatap_muka,kredit_praktikum,
kmk.kredit_semester, nm_status_mk,nm_kelompok_mk, tahun_kurikulum,
tingkat_semester from kurikulum_mk kmk left join mata_kuliah on kmk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join status_mk on kmk.id_status_mk=status_mk.id_status_mk 
left join kelompok_mk on kmk.id_kelompok_mk=kelompok_mk.id_kelompok_mk 
left join kurikulum_prodi on kmk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
where kmk.id_program_studi=$kdprodi and id_kurikulum_mk not in (select id_kurikulum_mk from kelas_mk where id_program_studi=$kdprodi and id_semester='".$smtaktif['ID_SEMESTER']."')" );
		
		$smarty->assign('TAWAR', $tawar);
		//$smarty->display('usulan-mk.tpl');
        break;

case 'tambahkelas':
		 // pilih
		$smarty->assign('disp1','block');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
		
		//$smt1= $_GET['smt'];
		//$id_mk=$_GET['id_mk'];
		$kap_kelas = $_POST['kap_kelas'];
		$kelas_mk = $_POST['kelas_mk'];
		$ren_kul = $_POST['ren_kul'];
		$id_kur_mk= $_POST['id_kur_mk'];
		$id_hari = $_POST['hari'];
		$id_jam= $_POST['jam'];
		$smt1= $_POST['smtb'];
		$idruang= $_POST['ruangan'];
		$prodi= $_POST['kd_prodi']; 
		
		//echo $prodi;
		
		if (!isFindData("kelas_mk","id_kurikulum_mk=$id_kur_mk and no_kelas_mk='$kelas_mk' and id_program_studi=$prodi")){
		tambahdata("kelas_mk","id_kurikulum_mk,id_semester,id_program_studi,no_kelas_mk,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk","'$id_kur_mk','".$smtaktif['ID_SEMESTER']."','$prodi','$kelas_mk','$kap_kelas','$ren_kul'");
		
		$kelas=getvar("select id_kelas_mk from kelas_mk where id_kurikulum_mk='$id_kur_mk' and id_semester='".$smtaktif['ID_SEMESTER']."' and no_kelas_mk=$kelas_mk and id_program_studi=$prodi");
		//echo "aaa";
		//echo $kelas['ID_KELAS_MK'];
		//echo "aaa";
		tambahdata("jadwal_kelas","id_kelas_mk,id_ruangan,id_jadwal_jam,id_jadwal_hari","'".$kelas['ID_KELAS_MK']."','$idruang','$id_jam','$id_hari'");
		}
		//if (!isFindData("jadwal_kelas","id_kelas_mk=$id_mk")) {tambahdata("jadwal_kelas","id_kelas_mk","$id_mk");
		$jaf=getData("select distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester, 
jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam, 
ruangan.id_ruangan,nm_ruangan,case when pengampu_mk.id_dosen is not null then 'sdh terisi' else 'blm terisi' end as status_pjmk,
no_kelas_mk,nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status from kelas_mk
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas  
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join semester on kelas_mk.id_semester=semester.id_semester 
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk 
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester=$smt1 order by nm_mata_kuliah,nama_kelas.nama_kelas");
		$smarty->assign('T_MK', $jaf);
		//$smarty->display('usulan-mk.tpl');
        break;      
		
case 'del':
		 // pilih
		$id_klsmk= $_GET['id_klsmk'];
		$smt1= $_GET['smt'];
		
		deleteData("delete from jadwal_kelas where id_kelas_mk='$id_klsmk'");
		deleteData("delete from pengampu_mk where id_kelas_mk='$id_klsmk'");
		deleteData("delete from krs_prodi where id_kelas_mk='$id_klsmk'");		
		deleteData("delete from kelas_mk where id_kelas_mk='$id_klsmk'");
		//if ($smt!=''){
		$jaf=getData("select distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester, 
jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam, 
ruangan.id_ruangan,nm_ruangan,case when pengampu_mk.id_dosen is not null then 'sdh terisi' else 'blm terisi' end as status_pjmk,
no_kelas_mk,nama_kelas.nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status from kelas_mk 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join semester on kelas_mk.id_semester=semester.id_semester 
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk 
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester=$smt1 order by nm_mata_kuliah,nama_kelas.nama_kelas");
		
		$smarty->assign('T_MK', $jaf);
		//$smarty->display('usulan-mk.tpl');

        break; 
		
case 'updateview':
		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','block');
		$smarty->assign('disp4','none');
		
		$id_klsmk= $_GET['id_klsmk'];
		
		
		$ruang=getData("select id_ruangan,nm_ruangan||'-'||kapasisitas_ruangan as ruang from ruangan
left join gedung on gedung.id_gedung=ruangan.id_gedung
left join gedung_kuliah on gedung.id_gedung=gedung_kuliah.id_gedung
where gedung.id_fakultas='".$kdfak."'");
		$smarty->assign('T_RUANG', $ruang);
		
		$hari=getData("select id_jadwal_hari,nm_jadwal_hari from jadwal_hari");
		$smarty->assign('T_HARI', $hari);

		$jam=getData("select id_jadwal_jam,nm_jadwal_jam from jadwal_jam where id_fakultas='".$kdfak."' order by nm_jadwal_jam");
		$smarty->assign('T_JAM', $jam);
		
		$jaf=getData("select kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
		kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt,
jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,kelas_mk.id_semester,
ruangan.id_ruangan,nm_ruangan,no_kelas_mk,nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status from kelas_mk
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas  
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join semester on kelas_mk.id_semester=semester.id_semester 
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
where program_studi.id_fakultas=$kdfak and kelas_mk.id_kelas_mk=$id_klsmk ");
		
		$smarty->assign('TJAF1', $jaf);

		//$smarty->display('usulan-mk.tpl');

        break; 

case 'adkelview':
		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','block');
		
		$id_klsmk= $_GET['id_klsmk'];
		
		$ruang=getData("select id_ruangan,nm_ruangan||'-'||kapasisitas_ruangan as ruang from ruangan
left join gedung on gedung.id_gedung=ruangan.id_gedung
left join gedung_kuliah on gedung.id_gedung=gedung_kuliah.id_gedung
where gedung.id_fakultas='".$kdfak['ID_FAKULTAS']."'");
		$smarty->assign('T_RUANG', $ruang);
		
		$hari=getData("select id_jadwal_hari,nm_jadwal_hari from jadwal_hari");
		$smarty->assign('T_HARI', $hari);

		$jam=getData("select id_jadwal_jam,nm_jadwal_jam from jadwal_jam where id_fakultas='".$kdfak['ID_FAKULTAS']."'");
		$smarty->assign('T_JAM', $jam);
		
		$jaf=getData("select kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kelas_mk.id_kurikulum_mk,
		kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt,kelas_mk.id_program_studi,
jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,kelas_mk.id_semester,
ruangan.id_ruangan,nm_ruangan,no_kelas_mk,nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status from kelas_mk
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas  
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join semester on kelas_mk.id_semester=semester.id_semester 
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
where program_studi.id_fakultas=$kdfak and kelas_mk.id_kelas_mk=$id_klsmk ");
		
		$smarty->assign('TJAF1', $jaf);

		//$smarty->display('usulan-mk.tpl');

        break; 
   

case 'penawaran':
		$smt= $_POST['smt'];
		$id_kur= $_POST['thkur'];
		
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
		
		$smarty->assign('id_smt',$smt);
		/*
		$tawar=getData("select id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_tatap_muka,kredit_praktikum,
kmk.kredit_semester, nm_status_mk,nm_kelompok_mk, tahun_kurikulum,
tingkat_semester from kurikulum_mk kmk left join mata_kuliah on kmk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join status_mk on kmk.id_status_mk=status_mk.id_status_mk 
left join kelompok_mk on kmk.id_kelompok_mk=kelompok_mk.id_kelompok_mk 
left join kurikulum_prodi on kmk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
where kmk.id_program_studi=$kdprodi and id_kurikulum_mk not in (select id_kurikulum_mk from kelas_mk where id_program_studi=$kdprodi and id_semester='".$smtaktif['ID_SEMESTER']."')" );
		
		$smarty->assign('TAWAR', $tawar);
		//$smarty->display('usulan-mk.tpl');
		*/
		break;
		
case 'update1':
	//echo "aaa";
		
		$kap_kelas = $_POST['kap_kelas'];
		$kelas_mk = $_POST['kelas_mk'];
		$status_kls = $_POST['status_kls'];
		$ren_kul = $_POST['ren_kul'];
		$id_kelas_mk= $_POST['id_kelas_mk'];
		$id_hari = $_POST['hari'];
		$id_jam= $_POST['jam'];
		$smt1= $_POST['smtb'];
		$idruang= $_POST['ruangan'];
		
		//echo $id_hari;
		//echo $id_jam;
	//	echo "a";
		//echo $id_kur1;
		if (isFindData("jadwal_kelas","id_kelas_mk=$id_kelas_mk")) {
		//tambahdata("jadwal_kelas","id_kelas_mk","$id_kelas_mk");
		UpdateData("update kelas_mk set kapasitas_kelas_mk=$kap_kelas, jumlah_pertemuan_kelas_mk=$ren_kul,no_kelas_mk='$kelas_mk',status='$status_kls' where id_kelas_mk=$id_kelas_mk ");
		UpdateData("update jadwal_kelas set id_jadwal_jam=$id_jam, id_jadwal_hari=$id_hari,id_ruangan='$idruang' where id_kelas_mk=$id_kelas_mk ");
		UpdateData("update krs_prodi set status='$status_kls' where id_kelas_mk=$id_kelas_mk ");
		}
		//isFindData("jadwal_kelas","id_kelas_mk=$id_kelas_mk");
		//tambahdata("jadwal_kelas","id_kelas_mk,id_jadwal_hari,id_jadwal_jam","'$id_kelas_mk','$id_hari','$id_jam'");
		//exit;
		
		$jaf1=("select distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester, 
jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam, 
ruangan.id_ruangan,nm_ruangan,case when pengampu_mk.id_dosen is not null then 'sdh terisi' else 'blm terisi' end as status_pjmk,
no_kelas_mk,nama_kelas.nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status from kelas_mk 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join semester on kelas_mk.id_semester=semester.id_semester 
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk 
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester=$smt1 order by nm_mata_kuliah,nama_kelas.nama_kelas");
		$smarty->assign('T_MK', $jaf1);

		break;

case 'tampil':
		//echo "aa";
		//echo $smtaktif['ID_SEMESTER'];
		//$smt1= $_POST['smt'];
		$smt1 = isSet($_POST['smt']) ? $_POST['smt'] : $smtaktif['ID_SEMESTER'];
		// if ($smt1=='') { $smt1=$smtaktif['ID_SEMESTER'];}
		//echo $smt1;
		
		//$id_kur1= $_POST['thkur'];
		$id_kur1 = isSet($_POST['thkur']) ? $_POST['thkur'] : '0';
		if ($smt1!='') {
		$smarty->assign('SMT',$smt);
		//$smarty->assign('KUR', $id_kur);
		
		$jaf=getData("select distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester, 
jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam, 
ruangan.id_ruangan,nm_ruangan,case when pengampu_mk.id_dosen is not null then 'sdh terisi' else 'blm terisi' end as status_pjmk,
no_kelas_mk,nama_kelas.nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status from kelas_mk 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join semester on kelas_mk.id_semester=semester.id_semester 
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk 
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester=$smt1
order by kd_mata_kuliah,nama_kelas.nama_kelas ");}
		$smarty->assign('T_MK', $jaf);
		//$smarty->assign('T_JAF', $jaf); 
		//$smarty->display('usulan-mk.tpl');
        break; 
			 
}
$smarty->display('jadwal-kuliah.tpl');

?>
