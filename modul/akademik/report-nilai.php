<?php
require('common.php');
require_once ('ociFunction.php');

$kdprodi		= $user->ID_PROGRAM_STUDI;
$id_pengguna	= $user->ID_PENGGUNA;
$kdfak			= $user->ID_FAKULTAS;

$datathnsmt = getData("select tahun_ajaran||' - '||nm_semester as thn_smt, id_semester from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' order by thn_akademik_semester desc");
$smarty->assign('DSMT', $datathnsmt);

$smtaktif = getvar("select id_semester from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND status_aktif_semester='True'");
$smt = isset($_POST['smt']) ? $_POST['smt'] : $smtaktif['ID_SEMESTER'];
$smarty->assign('smt', $smt);
$smarty->assign('kdfak', $kdfak);

$datalist = getData(
	"SELECT pengambilan_mk.id_kelas_mk, mata_kuliah.nm_mata_kuliah, nama_kelas, nm_pengguna as nm_dosen
	FROM kelas_mk
	JOIN pengambilan_mk ON kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk
	JOIN kurikulum_mk ON kurikulum_mk.id_kurikulum_mk=kelas_mk.id_kurikulum_mk
	JOIN mata_kuliah ON mata_kuliah.id_mata_kuliah=kurikulum_mk.id_mata_kuliah
	LEFT JOIN nama_kelas ON nama_kelas.id_nama_kelas=kelas_mk.no_kelas_mk
	JOIN semester ON kelas_mk.id_semester=semester.id_semester
	JOIN mahasiswa ON mahasiswa.id_mhs = pengambilan_mk.id_mhs
	JOIN program_studi ON mahasiswa.id_program_studi=program_studi.id_program_studi
	LEFT JOIN pengampu_mk ON pengampu_mk.id_kelas_mk = kelas_mk.id_kelas_mk and pengampu_mk.pjmk_pengampu_mk = '1'
	LEFT JOIN dosen ON dosen.id_dosen = pengampu_mk.id_dosen 
	LEFT JOIN pengguna ON pengguna.id_pengguna = dosen.id_pengguna
	WHERE 
		pengambilan_mk.id_semester = {$smt} AND 
		pengambilan_mk.status_apv_pengambilan_mk = 1 AND 
		program_studi.id_fakultas = {$kdfak}
	GROUP BY pengambilan_mk.id_kelas_mk, mata_kuliah.nm_mata_kuliah, nama_kelas.nama_kelas, nm_pengguna
	ORDER BY mata_kuliah.nm_mata_kuliah, nama_kelas.nama_kelas");

$smarty->assign('T_LIST_MK', $datalist);

if (isset($_POST['action']) == 'view')
{
	$id_kelas_mk = $_POST['id_kelas_mk'];

	$datathnsmt = getData("select '(TAHUN '||tahun_ajaran||' - SEMESTER '||nm_semester||')' as thn_smt from semester where id_semester='" . $smt . "'");
	$smarty->assign('THNSMT', $datathnsmt);

	$datamk = getData("
		select ' : '||mata_kuliah.nm_mata_kuliah||' - KELAS '||nama_kelas.nama_kelas as nm_mk
		from kelas_mk
		left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk=kelas_mk.id_kurikulum_mk
		left join mata_kuliah on mata_kuliah.id_mata_kuliah=kurikulum_mk.id_mata_kuliah
		left join nama_kelas on nama_kelas.id_nama_kelas=kelas_mk.no_kelas_mk
		where kelas_mk.id_kelas_mk='" . $id_kelas_mk . "'");
	$smarty->assign('NM_MK', $datamk);

	$jaf = getData("
		select mahasiswa.nim_mhs, upper(pengguna.nm_pengguna) as nm_pengguna, pengambilan_mk.nilai_angka, pengambilan_mk.nilai_huruf
		from mahasiswa
		left join pengambilan_mk on mahasiswa.id_mhs=pengambilan_mk.id_mhs
		left join pengguna on pengguna.id_pengguna=mahasiswa.id_pengguna
		left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
		where pengambilan_mk.id_kelas_mk='" . $id_kelas_mk . "' and pengambilan_mk.status_apv_pengambilan_mk=1
		and program_studi.id_fakultas='" . $kdfak . "'
		order by mahasiswa.nim_mhs asc");
	$smarty->assign('T_MK', $jaf);
}

$smarty->display('report-nilai.tpl');