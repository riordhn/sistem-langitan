<?php
error_reporting (E_ALL & ~E_NOTICE);
include 'config.php';
include '../pendidikan/class/evaluasi.class.php';

$eval = new evaluasi($db);
$id_pengguna= $user->ID_PENGGUNA; 
$id_fakultas = $user->ID_FAKULTAS;

$smarty->assign('semester', $semester);
$smarty->assign('fakultas', $id_fakultas);
$smarty->assign('semester', $eval->semester());
$smarty->assign('jenjang', $eval->jenjang_penetapan());
$smarty->assign('status', $eval->status_rekomendasi());
$smarty->assign('status_penetapan', $eval->status_penetapan());


if(isset($_REQUEST['semester'])){

$smarty->assign('program_studi', $eval->program_studi($id_fakultas, $_REQUEST['jenjang']));
$smarty->assign('evaluasi', $eval->evaluasi_pengajuan($_REQUEST['fakultas'], $_REQUEST['jenjang'], $_REQUEST['prodi'], $_REQUEST['semester']));

}

$smarty->display("evaluasi-hasil.tpl");
?>