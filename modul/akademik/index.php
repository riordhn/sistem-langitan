<?php

require_once('common.php');
require_once('ociFunction.php');

$id_pengguna= $user->ID_PENGGUNA; 
$kdfak = $user->ID_FAKULTAS;
$smarty->assign('kdfak', $kdfak);

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_AKADEMIK) {

	// force password to ISO27001:2009
	if($_SESSION['standar_iso']=='no'){
        header("Location: ../../login.php?mode=ltp-null");
        exit();
    }
	
	if($kdfak != ''){
    $namafak=getData("select * from fakultas where id_fakultas=$kdfak");
    $smarty->assign('NAMAFAK', $namafak);
	}
	
	//unit kerja mkwu
	//$db->Query("select * from pegawai where id_pengguna=$id_pengguna");
    	//$unit_kerja=$db->FetchArray();
	//$smarty->assign('unit_kerja', $unit_kerja);

   
	

    /*$data_tab = array();

    foreach ($xml_menu->tab as $tab)
    {
        array_push($data_tab, array(
            'ID'    => $tab->id,
            'TITLE' => $tab->title,
            'MENU'  => $tab->menu,
            'PAGE'  => $tab->page
        ));
    }

    $smarty->assign('tabs', $data_tab);*/
    
    // data menu dari $user
    $smarty->assign('modul_set', $user->MODULs);

    $smarty->display('index.tpl');

} elseif($user->IsLogged()&& $user->Role() == AUCC_ROLE_KEUANGAN_FAKULTAS && $kdfak<>null && $kdfak<>""){
	
    $namafak=getData("select * from fakultas where id_fakultas=$kdfak");
    $smarty->assign('NAMAFAK', $namafak);

    $data_tab = array();

    foreach ($xml_menu->tab as $tab)
    {
        array_push($data_tab, array(
            'ID'    => $tab->id,
            'TITLE' => $tab->title,
            'MENU'  => $tab->menu,
            'PAGE'  => $tab->page
        ));
    }

    $smarty->assign('tabs', $data_tab);

    $smarty->display('index1.tpl');
}
else {
    header("Location: /logout.php");
	exit();
}
?>