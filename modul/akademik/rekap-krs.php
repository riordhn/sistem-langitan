<?php
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$kdprodi=$user->ID_PROGRAM_STUDI;

$id_pengguna= $user->ID_PENGGUNA;

$kdfak=$user->ID_FAKULTAS;
$smarty->assign('FAK', $kdfak);

$thnsmt=getData(
	"SELECT id_semester as id_semester, tahun_ajaran||' - '||nm_semester as thn_smt from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester in (
	select * from (select distinct thn_akademik_semester
	from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester<=EXTRACT(YEAR FROM sysdate) order by thn_akademik_semester desc)
	where rownum<=10) and nm_semester in ('Ganjil', 'Genap')
	order by thn_akademik_semester desc, nm_semester desc");
$smarty->assign('T_THNSMT', $thnsmt);

$dataprodi=getData("SELECT id_program_studi,nm_jenjang||'-' || nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_fakultas=$kdfak and status_aktif_prodi = 1");
$smarty->assign('T_PRODI', $dataprodi);

if (isset($_POST['action']) == 'view') {

	$smt = $_POST['kdthnsmt'];
	$smarty->assign('SMT_AKAD', $smt);
	$krs_smt=getData("SELECT tahun_ajaran||' - '||nm_semester as thn_smt from semester where id_semester=$smt");
	$smarty->assign('THN_AKAD', $krs_smt);

	$kdprodi = $_POST['kdprodi'];
	$smarty->assign('PRODI', $kdprodi);

	$dataprodi1=getData("SELECT (': '||nm_jenjang||'-'||nm_program_studi) as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_program_studi=$kdprodi and status_aktif_prodi = 1");
	$smarty->assign('NM_PRODI', $dataprodi1);

	// Yudi Sulistya, 2012/05/10
	$jml_mhs=getvar(
		"SELECT count(distinct nim_mhs) as jml_mhs from
		(
		select MAHASISWA.nim_mhs,pengguna.nm_pengguna,mahasiswa.id_program_studi,nm_program_studi,
		MATA_KULIAH.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,KURIKULUM_MK.kredit_semester from PENGAMBILAN_MK
		left join mahasiswa on pengambilan_mk.id_mhs=MAHASISWA.ID_MHS
		left join pengguna on MAHASISWA.id_pengguna=PENGGUNA.id_pengguna
		left join program_studi on MAHASISWA.id_program_studi=PROGRAM_STUDI.id_program_studi
		left join kelas_mk on PENGAMBILAN_MK.id_kelas_mk=KELAS_MK.id_kelas_mk
		left join NAMA_KELAS on KELAS_MK.no_kelas_mk=nama_kelas.id_nama_kelas
		left join kurikulum_mk on kelas_mk.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk
		left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
		where PENGAMBILAN_MK.id_semester=$smt and PROGRAM_STUDI.id_fakultas=$kdfak and mahasiswa.id_program_studi=$kdprodi)");
	$smarty->assign('JML_MHS', $jml_mhs['JML_MHS']);

	$mk=getData(
		"SELECT MAHASISWA.nim_mhs,pengguna.nm_pengguna,mahasiswa.id_program_studi,nm_program_studi,
		MATA_KULIAH.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,KURIKULUM_MK.kredit_semester, thn_akademik_semester, nm_semester,
		pengambilan_mk.keterangan, (case when status_apv_pengambilan_mk = 1 then 'Approved' else 'Not Approved' end) as status_krs
		from PENGAMBILAN_MK
		left join mahasiswa on pengambilan_mk.id_mhs=MAHASISWA.ID_MHS
		left join pengguna on MAHASISWA.id_pengguna=PENGGUNA.id_pengguna
		left join program_studi on MAHASISWA.id_program_studi=PROGRAM_STUDI.id_program_studi
		left join kelas_mk on PENGAMBILAN_MK.id_kelas_mk=KELAS_MK.id_kelas_mk
		left join NAMA_KELAS on KELAS_MK.no_kelas_mk=nama_kelas.id_nama_kelas
		left join kurikulum_mk on kelas_mk.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk
		left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
		left join semester on semester.id_semester = pengambilan_mk.id_semester
		where PENGAMBILAN_MK.id_semester=$smt and PROGRAM_STUDI.id_fakultas=$kdfak and mahasiswa.id_program_studi=$kdprodi");
	$smarty->assign('T_MK', $mk);

}

$smarty->display('rekap-krs.tpl');
?>