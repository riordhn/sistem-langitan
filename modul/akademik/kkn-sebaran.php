<?php
	include '../../config.php';
	
	//$fakultas = $db->QueryToArray("select * from fakultas");
	//$smarty->assign('fakultas', $fakultas);
	//if(isset($_GET['fakultas'])){
		//$fakultas = (int)$_GET['fakultas'];
		$fakultas = $user->ID_FAKULTAS;
		$sebaran = $db->QueryToArray("select b.nim_mhs, p.nm_pengguna, e.nama_kelompok, f.nm_kelurahan, c.nm_program_studi, d.nm_fakultas from kkn_kelompok_mhs a
										left join mahasiswa b on b.id_mhs = a.id_mhs
										left join pengguna p on p.id_pengguna = b.id_pengguna
										left join program_studi c on c.id_program_studi = b.id_program_studi
										left join fakultas d on d.id_fakultas = c.id_fakultas
										left join kkn_kelompok e on e.id_kkn_kelompok = a.id_kkn_kelompok
										left join kelurahan f on f.id_kelurahan = e.id_kelurahan
										left join semester g on g.id_semester = e.id_semester
										where d.id_fakultas = '$fakultas' and g.status_aktif_semester = 'True' order by d.id_fakultas, c.id_program_studi, p.nm_pengguna");
	
		$smarty->assign('sebaran', $sebaran);
		$smarty->assign('getfak', $fakultas);
		$smarty->assign('download', 0);
	
	//}
	

	if(isset($_GET['download'])){
	
echo '	<link rel="stylesheet" type="text/css" href="../../css/reset.css" />
	<link rel="stylesheet" type="text/css" href="../../css/text.css" />
	<link rel="stylesheet" type="text/css" href="../../css/akademik_edit.css" />
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #B24405;
        text-transform:capitalize;
    }
	th{
		background-color:#265226;
		color:#fff;
		border-color:#000;
		border-width:1px;
		border-style:solid;
		text-align:center;
	}
    td{
        text-align: left;
		border-color:#000;
		border-width:1px;
		border-style:solid;
		padding:2px;
    }
    .bold{
        font-weight: bold;
        font-size: 1.2em;
    }
</style>
	';
		//$date = date("Y-m-d");
		//$fak = $_GET['fak'];
		
		ob_start();
		$nm_file = "sebaran-kkn(" . date('d-m-Y') . ')';
		header("Content-disposition: attachment; filename={$nm_file}.xls");
		header("Content-type: application/vnd.ms-excel");
		header("Content-Transfer-Encoding: binary");
		ob_flush();
		
		$smarty->assign('download', 1);
	}	
	
	$smarty->display("kkn-sebaran.tpl");
?>