<?php
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$kdprodi=$_GET['prodi'];
$smarty->assign('kodeprodi',$kdprodi);

$nmfak=getvar("select id_fakultas from program_studi where id_program_studi=$kdprodi");
$smarty->assign('FAK',$nmfak['ID_FAKULTAS']);

$nmprodi=getvar("select nm_jenjang||' - '||nm_program_studi as prodi from program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where id_program_studi=$kdprodi");
$smarty->assign('disprodi',$nmprodi['PRODI']);

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True'");
$smt=$smtaktif['ID_SEMESTER'];

$jaf=getData("select distinct mahasiswa.thn_angkatan_mhs, mahasiswa.id_program_studi, count(distinct pengambilan_mk.id_mhs) as jml
from mahasiswa inner join pengambilan_mk on mahasiswa.id_mhs=pengambilan_mk.id_mhs
where mahasiswa.id_program_studi=$kdprodi and pengambilan_mk.status_apv_pengambilan_mk = 1
group by mahasiswa.thn_angkatan_mhs, mahasiswa.id_program_studi
order by mahasiswa.thn_angkatan_mhs desc");

$smarty->assign('T_MK', $jaf);
$smarty->display('krs-cetak.tpl');

?>
