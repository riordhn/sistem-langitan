<?php

require('common.php');
include "class/aucc.class.php";
$id_fakultas = $user->ID_FAKULTAS;
$aucc = new AUCC_Akademik($db);
if (post('no_ujian')) {
    if ($aucc->cek_cmhs_fakultas(post('no_ujian'), $id_fakultas))
        $smarty->assign('data_pembayaran', $aucc->detail_pembayaran_cmhs(post('no_ujian')));
    else
        $smarty->assign('data_kosong', 'Data Mahasiswa Masih Belum ada dalam database kami');
}

$smarty->display('cek-pembayaran-cmhs.tpl');
?>
