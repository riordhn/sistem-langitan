<?php
/*
Yudi Sulistya 23/12/2011
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$kdprodi=$user->ID_PROGRAM_STUDI;

$id_pengguna= $user->ID_PENGGUNA;

$kdfak=$user->ID_FAKULTAS;
$smarty->assign('FAK', $kdfak);

if($kdfak==11){

	echo '
		<script>
		location.href="#report-dnma!report-prosentase-nilai-ma-psikologi.php";
		</script>
	';

//Fakultas Lain
} else {
$thnsmt=getData("select id_semester as id_semester, tahun_ajaran||'-'||nm_semester||'-'||group_semester as thn_smt from semester 
where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester in (select distinct thn_akademik_semester
from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester>=EXTRACT(YEAR FROM sysdate)-2)
order by thn_akademik_semester desc, group_semester desc, nm_semester desc");
$smarty->assign('T_THNSMT', $thnsmt);

$dataprodi=getData("select id_program_studi||'-'||id_fakultas as id_program_studi,nm_jenjang||'-'||nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_fakultas=$kdfak and status_aktif_prodi = 1");
$smarty->assign('T_PRODI', $dataprodi);


if (isset($_POST['action'])=='view') {
$var = explode("-", $_POST['kdprodi']);
$var1 = explode("-", $_POST['kdthnsmt']);

$smt = $var1[0];
$smarty->assign('KD_SMT', $smt);

$thn = $var1[1];
$smarty->assign('KD_THN', $thn);

$kd_prodi=$var[0];
$smarty->assign('KD_PRODI', $kd_prodi);

$kd_fak=$var[1];
$smarty->assign('KD_FAK', $kd_fak);

$datathnsmt=getData("select '(TAHUN '||tahun_ajaran||' - SEMESTER '||nm_semester||')' as thn_smt from semester where id_semester=$smt");
$smarty->assign('THNSMT', $datathnsmt);

if ($var[0]=='all') {
$dataprodi1=getData("select (': FAKULTAS '||nm_fakultas) as prodi from fakultas where id_fakultas=".$var[1]."");
$smarty->assign('NM_PRODI', $dataprodi1);

if($thn>=2011){
$jaf=getData("select kd_mata_kuliah, nm_mata_kuliah, kredit_semester,
			sum(A) as jml_A,
			round(sum(A)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_A,
			sum(AB) as jml_AB,
			round(sum(AB)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_AB,
			sum(B) as jml_B,
			round(sum(B)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_B,
			sum(BC) as jml_BC,
			round(sum(BC)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_BC,
			sum(C) as jml_C,
			round(sum(C)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_C,
			sum(D) as jml_D,
			round(sum(D)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_D,
			sum(E) as jml_E,
			round(sum(E)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_E
			from
			(select mk.kd_mata_kuliah, mk.nm_mata_kuliah, kmk.kredit_semester,
			case when nilai_huruf='A' then 1 else 0 end as A,
			case when nilai_huruf='AB' then 1 else 0 end as AB,
			case when nilai_huruf='B' then 1 else 0 end as B,
			case when nilai_huruf='BC' then 1 else 0 end as BC,
			case when nilai_huruf='C' then 1 else 0 end as C,
			case when nilai_huruf='D' then 1 else 0 end as D,
			case when (nilai_huruf='E' or nilai_huruf='T' or nilai_huruf is null) then 1 else 0 end as E
			from pengambilan_mk pmk
			left join mahasiswa mhs on pmk.id_mhs = mhs.id_mhs
			left join program_studi ps on ps.id_program_studi = mhs.id_program_studi
			left join jenjang j on j.id_jenjang = ps.id_jenjang
			left join fakultas f on f.id_fakultas = ps.id_fakultas
			left join kelas_mk klm on klm.id_kelas_mk = pmk.id_kelas_mk
			left join kurikulum_mk kmk on kmk.id_kurikulum_mk = klm.id_kurikulum_mk
			left join mata_kuliah mk on mk.id_mata_kuliah = kmk.id_mata_kuliah
			where pmk.id_semester=$smt and ps.id_fakultas=".$var[1]." and pmk.status_apv_pengambilan_mk=1) group by kd_mata_kuliah, nm_mata_kuliah, kredit_semester
			");
$smarty->assign('T_MK', $jaf);
} else {
$jaf=getData("select kd_mata_kuliah, nm_mata_kuliah, kredit_semester,
			sum(A) as jml_A,
			round(sum(A)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_A,
			sum(AB) as jml_AB,
			round(sum(AB)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_AB,
			sum(B) as jml_B,
			round(sum(B)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_B,
			sum(BC) as jml_BC,
			round(sum(BC)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_BC,
			sum(C) as jml_C,
			round(sum(C)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_C,
			sum(D) as jml_D,
			round(sum(D)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_D,
			sum(E) as jml_E,
			round(sum(E)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_E
			from
			(select mk.kd_mata_kuliah, mk.nm_mata_kuliah, kmk.kredit_semester,
			case when nilai_huruf='A' then 1 else 0 end as A,
			case when nilai_huruf='AB' then 1 else 0 end as AB,
			case when nilai_huruf='B' then 1 else 0 end as B,
			case when nilai_huruf='BC' then 1 else 0 end as BC,
			case when nilai_huruf='C' then 1 else 0 end as C,
			case when nilai_huruf='D' then 1 else 0 end as D,
			case when (nilai_huruf='E' or nilai_huruf='T' or nilai_huruf is null) then 1 else 0 end as E
			from pengambilan_mk pmk
			left join mahasiswa mhs on pmk.id_mhs = mhs.id_mhs
			left join program_studi ps on ps.id_program_studi = mhs.id_program_studi
			left join jenjang j on j.id_jenjang = ps.id_jenjang
			left join fakultas f on f.id_fakultas = ps.id_fakultas
			left join kurikulum_mk kmk on kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
			left join mata_kuliah mk on mk.id_mata_kuliah = kmk.id_mata_kuliah
			where pmk.id_semester=$smt and ps.id_fakultas=".$var[1]." and pmk.status_apv_pengambilan_mk=1) group by kd_mata_kuliah, nm_mata_kuliah, kredit_semester
			");
$smarty->assign('T_MK', $jaf);
}

} else {
$dataprodi1=getData("select (': PRODI '||nm_jenjang||'-'||nm_program_studi) as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_program_studi=".$var[0]."");
$smarty->assign('NM_PRODI', $dataprodi1);

if($thn>=2011){
$jaf=getData("select kd_mata_kuliah, nm_mata_kuliah, kredit_semester,
			sum(A) as jml_A,
			round(sum(A)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_A,
			sum(AB) as jml_AB,
			round(sum(AB)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_AB,
			sum(B) as jml_B,
			round(sum(B)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_B,
			sum(BC) as jml_BC,
			round(sum(BC)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_BC,
			sum(C) as jml_C,
			round(sum(C)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_C,
			sum(D) as jml_D,
			round(sum(D)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_D,
			sum(E) as jml_E,
			round(sum(E)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_E
			from
			(select mk.kd_mata_kuliah, mk.nm_mata_kuliah, kmk.kredit_semester,
			case when nilai_huruf='A' then 1 else 0 end as A,
			case when nilai_huruf='AB' then 1 else 0 end as AB,
			case when nilai_huruf='B' then 1 else 0 end as B,
			case when nilai_huruf='BC' then 1 else 0 end as BC,
			case when nilai_huruf='C' then 1 else 0 end as C,
			case when nilai_huruf='D' then 1 else 0 end as D,
			case when (nilai_huruf='E' or nilai_huruf='T' or nilai_huruf is null) then 1 else 0 end as E
			from pengambilan_mk pmk
			left join mahasiswa mhs on pmk.id_mhs = mhs.id_mhs
			left join program_studi ps on ps.id_program_studi = mhs.id_program_studi
			left join jenjang j on j.id_jenjang = ps.id_jenjang
			left join fakultas f on f.id_fakultas = ps.id_fakultas
			left join kelas_mk klm on klm.id_kelas_mk = pmk.id_kelas_mk
			left join kurikulum_mk kmk on kmk.id_kurikulum_mk = klm.id_kurikulum_mk
			left join mata_kuliah mk on mk.id_mata_kuliah = kmk.id_mata_kuliah
			where pmk.id_semester=$smt and ps.id_program_studi=".$var[0]." and pmk.status_apv_pengambilan_mk=1) group by kd_mata_kuliah, nm_mata_kuliah, kredit_semester
			");
$smarty->assign('T_MK', $jaf);
} else {
$jaf=getData("select kd_mata_kuliah, nm_mata_kuliah, kredit_semester,
			sum(A) as jml_A,
			round(sum(A)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_A,
			sum(AB) as jml_AB,
			round(sum(AB)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_AB,
			sum(B) as jml_B,
			round(sum(B)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_B,
			sum(BC) as jml_BC,
			round(sum(BC)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_BC,
			sum(C) as jml_C,
			round(sum(C)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_C,
			sum(D) as jml_D,
			round(sum(D)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_D,
			sum(E) as jml_E,
			round(sum(E)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_E
			from
			(select mk.kd_mata_kuliah, mk.nm_mata_kuliah, kmk.kredit_semester,
			case when nilai_huruf='A' then 1 else 0 end as A,
			case when nilai_huruf='AB' then 1 else 0 end as AB,
			case when nilai_huruf='B' then 1 else 0 end as B,
			case when nilai_huruf='BC' then 1 else 0 end as BC,
			case when nilai_huruf='C' then 1 else 0 end as C,
			case when nilai_huruf='D' then 1 else 0 end as D,
			case when (nilai_huruf='E' or nilai_huruf='T' or nilai_huruf is null) then 1 else 0 end as E
			from pengambilan_mk pmk
			left join mahasiswa mhs on pmk.id_mhs = mhs.id_mhs
			left join program_studi ps on ps.id_program_studi = mhs.id_program_studi
			left join jenjang j on j.id_jenjang = ps.id_jenjang
			left join fakultas f on f.id_fakultas = ps.id_fakultas
			left join kurikulum_mk kmk on kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
			left join mata_kuliah mk on mk.id_mata_kuliah = kmk.id_mata_kuliah
			where pmk.id_semester=$smt and ps.id_program_studi=".$var[0]." and pmk.status_apv_pengambilan_mk=1) group by kd_mata_kuliah, nm_mata_kuliah, kredit_semester
			");
$smarty->assign('T_MK', $jaf);
}

}
}
$smarty->display('report-prosentase-nilai-ma.tpl');
}
?>