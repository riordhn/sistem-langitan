<?php
//error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$id_pengguna= $user->ID_PENGGUNA;
$kdfak=$user->ID_FAKULTAS;
$smarty->assign('FAK',$kdfak);

//Fakultas Lain
if($kdfak!=11){
	echo 'Anda tidak memiliki hak akses';
} else {

//smt aktif
$smtaktif=getvar("select id_semester,thn_akademik_semester,nm_semester from semester where status_aktif_semester='True'");
//$smtaktif=getvar("select id_semester,thn_akademik_semester,nm_semester from semester where id_semester=21");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);
$id_semester=$smtaktif['ID_SEMESTER'];

//last smt
if($smtaktif['NM_SEMESTER']=="Ganjil") {
		$last_smt=getvar("select id_semester from semester where thn_akademik_semester=$smtaktif[THN_AKADEMIK_SEMESTER]-1 and nm_semester='Genap'");

	}else if($smtaktif['NM_SEMESTER']=="Genap") {
		$last_smt=getvar("select id_semester from semester where thn_akademik_semester=$smtaktif[THN_AKADEMIK_SEMESTER] and nm_semester='Ganjil'");

	}
	
$nim_mhs= $_REQUEST['nim'];
$smarty->assign('nim',$nim_mhs);

$detailmhs= getvar("select mahasiswa.id_pengguna,mahasiswa.id_mhs,mahasiswa.id_program_studi,nm_pengguna,
nm_jenjang||'-'||nm_program_studi as nm_program_studi,nim_mhs,jenjang.id_jenjang, thn_angkatan_mhs
from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where nim_mhs = '".$nim_mhs."' and program_studi.id_fakultas=$kdfak");

$smarty->assign('nim', $detailmhs['NIM_MHS']);
$smarty->assign('nama', $detailmhs['NM_PENGGUNA']);
$smarty->assign('prodi', $detailmhs['NM_PROGRAM_STUDI']);

//get IPS
$datamhs= getvar("select id_mhs, sum(kredit_semester) as sks_total, sum((bobot*kredit_semester)) as bobot_total,
case when sum(bobot*kredit_semester)=0 then 0 else
round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips
from
(select m.id_mhs,
case when (a.nilai_angka <= 0 or a.nilai_huruf = 'E' or a.nilai_angka is null or a.nilai_huruf is null) and d.status_mkta in (1,2) then 0
else d.kredit_semester end as kredit_semester,
case when a.nilai_huruf is null then 'E' else a.nilai_huruf end as nilai_huruf,
case when e.nilai_standar_nilai is null then 0 else e.nilai_standar_nilai end as bobot
from pengambilan_mk a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah
left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where a.id_semester=$last_smt[ID_SEMESTER] and a.status_apv_pengambilan_mk='1' and m.nim_mhs='".$nim_mhs."'
)
group by id_mhs");
$smarty->assign('ips',$datamhs['IPS']);

//get IPK
$dataipk= getvar("select id_mhs, sum(sks) as ttl_sks, round((sum((sks * bobot)) / sum(sks)), 2) as ipk
from
(select id_mhs, kd_mata_kuliah as kode, nm_mata_kuliah as nama, kredit_semester as sks, min(nilai_huruf) as nilai, max(nilai_standar_nilai) as bobot
from
(select m.id_mhs, c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai
from pengambilan_mk a
join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah
join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
join mahasiswa m on a.id_mhs=m.id_mhs
where a.status_apv_pengambilan_mk = 1 and a.status_hapus=0 and a.nilai_huruf<>'E' and a.nilai_huruf is not null and a.id_semester<>$id_semester and m.nim_mhs='".$nim_mhs."')
group by id_mhs, kd_mata_kuliah, nm_mata_kuliah, kredit_semester)
group by id_mhs");
$smarty->assign('ttl_sks',$dataipk['TTL_SKS']);
$smarty->assign('ipk',$dataipk['IPK']);

$angkatan=getvar("select trim(thn_akademik_semester) as thn from semester where status_aktif_semester='True' and nm_semester='Ganjil'");

//get jatah SKS
if ($detailmhs['THN_ANGKATAN_MHS'] == $angkatan['THN']) {
		$jatahsks= getvar("select max(sks_maksimal) as sks_max from beban_sks where id_program_studi = '".$detailmhs['ID_PROGRAM_STUDI']."'");
		$smarty->assign('sks_maks',$jatahsks['SKS_MAX']);
} else {
		if ($datamhs['IPS']>0) {
		$jatahsks= getvar("select max(sks_maksimal) as sks_max from beban_sks where id_program_studi = '".$detailmhs['ID_PROGRAM_STUDI']."' and ipk_minimum <= '".$datamhs['IPS']."'");
		} else {
		$jatahsks= getvar("select max(sks_maksimal) as sks_max from beban_sks where id_program_studi = '".$detailmhs['ID_PROGRAM_STUDI']."' and ipk_minimum <=0");
		}
$smarty->assign('sks_maks',$jatahsks['SKS_MAX']);
}

if ($request_method == 'GET')
{
if (get('action')=='del')
{
	   $id_pengambilan_mk= $_GET['idpmk'];

       deletedata("delete from nilai_mk where id_pengambilan_mk=$id_pengambilan_mk");
	   deletedata("delete from pengambilan_mk where id_pengambilan_mk=$id_pengambilan_mk");


}
}

$mhs_status1=getData("select krs_prodi.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,nm_jadwal_hari,
jam_mulai||':'||menit_mulai||' - '||jam_selesai||':'||menit_selesai as nm_jadwal_jam,
sum(case when status_pengambilan_mk>=1 then 1 else 0 end) as pst, kapasitas_kelas_mk from krs_prodi
left join pengambilan_mk on krs_prodi.id_kelas_mk=pengambilan_mk.id_kelas_mk and krs_prodi.id_semester=pengambilan_mk.id_semester
left join kelas_mk on krs_prodi.id_kelas_mk=kelas_mk.id_kelas_mk and krs_prodi.id_semester=kelas_mk.id_semester
left join nama_kelas on kelas_mk.no_kelas_mk=id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
where krs_prodi.id_program_studi='$detailmhs[ID_PROGRAM_STUDI]' and krs_prodi.id_semester=$id_semester and
kurikulum_mk.id_kurikulum_mk not in (select id_kurikulum_mk from pengambilan_mk where id_mhs='$datamhs[ID_MHS]' and id_semester=$id_semester)
group by krs_prodi.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,nm_jadwal_hari,
jam_mulai,menit_mulai,jam_selesai,menit_selesai,kapasitas_kelas_mk
order by kd_mata_kuliah,nama_kelas");
$smarty->assign('T_KRS', $mhs_status1);

$data_mhs_krs=getData("select id_pengambilan_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,nm_jadwal_hari,
jam_mulai||':'||menit_mulai||' - '||jam_selesai||':'||menit_selesai as nm_jadwal_jam,
case when status_apv_pengambilan_mk=1 then 'Approved' else 'Not Approved' end as status
from pengambilan_mk
left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk
left join nama_kelas on kelas_mk.no_kelas_mk=id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
where pengambilan_mk.id_mhs in (select id_mhs from mahasiswa where nim_mhs='".$nim_mhs."') and pengambilan_mk.id_semester=$id_semester order by kd_mata_kuliah,nama_kelas");
$smarty->assign('T_AMBIL', $data_mhs_krs);

$sksambil=getvar("select sum(kurikulum_mk.kredit_semester) as ttlsks from pengambilan_mk
left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
where pengambilan_mk.id_mhs='$datamhs[ID_MHS]' and pengambilan_mk.id_semester=$id_semester and status_apv_pengambilan_mk=1");
$smarty->assign('sksdisetujui', $sksambil['TTLSKS']);

$smarty->display('drop-krs-psikologi.tpl');

}
?>
