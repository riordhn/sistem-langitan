<?php
/*
Yudi Sulistya 02/01/2012
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$kdprodi=$user->ID_PROGRAM_STUDI;

$id_pengguna= $user->ID_PENGGUNA; 

$kdfak=$user->ID_FAKULTAS;
$smarty->assign('FAK', $kdfak);

if (isset($_POST['action'])=='view') {
$nim = $_POST['nim'];

$datamhs=getData("select ': '||nim_mhs||' - '||upper(nm_pengguna) as nm_mhs from mahasiswa left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna where nim_mhs='".$nim."' and id_program_studi in (select id_program_studi from program_studi where id_fakultas='".$kdfak."')");
$smarty->assign('MHS', $datamhs);

$thnsmt=getvar("select * from (
				select mhs.nim_mhs, ms.id_semester from semester smt
				left join mhs_status ms on ms.id_semester=smt.id_semester
				left join mahasiswa mhs on ms.id_mhs=mhs.id_mhs
				order by smt.thn_akademik_semester desc, smt.nm_semester)
				where nim_mhs='".$nim."' and rownum=1");
$id_smt=$thnsmt['ID_SEMESTER'];
				
$biomhs=getData("select mhs.nim_mhs, upper(p.nm_pengguna) as nm_pengguna, j.nm_jenjang, ps.nm_program_studi, ms.ipk_mhs_status, ms.sks_total_mhs_status, sp.nm_status_pengguna
				from mahasiswa mhs 
				left join pengguna p on mhs.id_pengguna=p.id_pengguna 
				left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
				left join jenjang j on ps.id_jenjang=j.id_jenjang
				left join mhs_status ms on mhs.id_mhs=ms.id_mhs
				left join status_pengguna sp on mhs.status_akademik_mhs=sp.id_status_pengguna
				where mhs.nim_mhs='".$nim."' and ms.id_semester='".$id_smt."' and mhs.id_program_studi in (select id_program_studi from program_studi where id_fakultas='".$kdfak."')");
$smarty->assign('T_MHS', $biomhs);

$jaf=getData("select mk.kd_mata_kuliah, mk.nm_mata_kuliah, kmk.kredit_semester, smt.tahun_ajaran, smt.nm_semester, pmk.nilai_huruf 
			from pengambilan_mk pmk
			left join semester smt on pmk.id_semester=smt.id_semester
			left join mahasiswa mhs on pmk.id_mhs = mhs.id_mhs
			left join kurikulum_mk kmk on kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
			left join mata_kuliah mk on mk.id_mata_kuliah = kmk.id_mata_kuliah
			where mhs.id_program_studi in (select id_program_studi from program_studi where id_fakultas='".$kdfak."') and mhs.nim_mhs='".$nim."'
			order by smt.tahun_ajaran, smt.nm_semester, nm_mata_kuliah");
$smarty->assign('T_MK', $jaf);
}

$smarty->display('report-history-mhs.tpl');  
?>