<?php
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$id_pengguna= $user->ID_PENGGUNA; 
$fak=getvar("select fakultas.id_fakultas from pegawai left join unit_kerja on pegawai.id_unit_kerja=unit_kerja.id_unit_kerja 
left join program_studi on unit_kerja.id_program_studi=program_studi.id_program_studi 
left join fakultas on program_studi.id_fakultas=fakultas.id_fakultas 
where pegawai.id_pengguna=$id_pengguna");
$kdfak=$fak['ID_FAKULTAS'];
$error = "";


if(isset($_POST['nim']) and !isset($_POST['action'])){
	$nim_mhs = $_POST['nim'];
	
	$status_pengguna=getvar("select nm_status_pengguna, id_status_pengguna, nm_pengguna from mahasiswa 
					left join pengguna on pengguna.id_pengguna = mahasiswa.id_pengguna
					left join status_pengguna on id_status_pengguna = status_akademik_mhs
					left join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
					left join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
					where nim_mhs='$nim_mhs' and fakultas.id_fakultas = '$kdfak'");

	if($status_pengguna['NM_STATUS_PENGGUNA'] <> ''){
					
$stsakd=getData("select id_status_pengguna,nm_status_pengguna from status_pengguna where status_akademik=1 and id_role = 3");
$smarty->assign('T_STAKD', $stsakd);
$smt=getData("select id_semester, tahun_ajaran||' - '||nm_semester as smt from semester order by thn_akademik_semester desc,nm_semester");
$smarty->assign('T_ST', $smt);

$smarty->assign('nm_pengguna', $status_pengguna['NM_PENGGUNA']);
$smarty->assign('nim_mhs',$nim_mhs);
$smarty->assign('nm_status_pengguna', $status_pengguna['NM_STATUS_PENGGUNA']);
$smarty->assign('id_status_pengguna', $status_pengguna['ID_STATUS_PENGGUNA']);
	}else{
		$error = "Data tidak ditemukan";
	}

$smarty->assign('disp1','none');
$smarty->assign('disp2','block');
}

if ($_POST['action']=='add')
{ 
		
		$nim_mhs = $_POST['nim'];
		$sta = $_POST['sta'];
		$smt = $_POST['smt'];
		$kurun_waktu = $_POST['kurun_waktu'];
		$alasan = $_POST['alasan'];

		$id_mhs=getvar("select nm_status_pengguna, id_status_pengguna, ID_MHS, nm_pengguna from mahasiswa 
					left join pengguna on pengguna.id_pengguna = mahasiswa.id_pengguna
					left join status_pengguna on id_status_pengguna = status_akademik_mhs
					left join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
					left join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
					where nim_mhs='$nim_mhs' and fakultas.id_fakultas = '$kdfak'");		
		
		if ($id_mhs['ID_MHS'] !='' || $id_mhs['ID_MHS'] !=null ) {
			if($sta != "" and $smt != ""){
				if($sta == 2 or $sta == 3){
					if($kurun_waktu == ""){
						$error = "Untuk Cuti dan Perpanjangan, kurun waktu tidak boleh kosong";
					}else{
						InsertData("insert into admisi (id_mhs,id_semester,status_akd_mhs,kurun_waktu,tgl_usulan,alasan, STATUS_APV)
									values ('$id_mhs[ID_MHS]','$smt','$sta','$kurun_waktu',TO_CHAR(SYSDATE, 'DD-Mon-YYYY'),'$alasan', '0')");
					}
				}else{
						InsertData("insert into admisi (id_mhs,id_semester,status_akd_mhs,tgl_usulan,alasan, STATUS_APV)
									values ('$id_mhs[ID_MHS]','$smt','$sta',TO_CHAR(SYSDATE, 'DD-Mon-YYYY'),'$alasan', '0')");
				}
				
			}else{
				$error = "Status Pengajuan dan Semester harap diisi ";
			}
		}else{
			$error = "Data tidak ditemukan";
		}
		
		if($error <> ""){
			$stsakd=getData("select id_status_pengguna,nm_status_pengguna from status_pengguna where status_akademik=1 and id_role = 3");
			$smarty->assign('T_STAKD', $stsakd);
			$smt=getData("select id_semester, tahun_ajaran||' - '||nm_semester as smt from semester order by thn_akademik_semester desc,nm_semester");
			$smarty->assign('T_ST', $smt);
			
			$smarty->assign('nm_pengguna', $id_mhs['NM_PENGGUNA']);
			$smarty->assign('nm_status_pengguna', $id_mhs['NM_STATUS_PENGGUNA']);
			$smarty->assign('nim_mhs',$nim_mhs);
			$smarty->assign('disp1','none');
			$smarty->assign('disp2','block');
		}
		
}

if ($_GET['action']=='del')
{
		 // pilih
		$id_admisi = $_GET['id_admisi'];
		
		deleteData("delete from admisi where id_admisi=$id_admisi");
}   


	
$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True'");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);
$id_semester=$smtaktif['ID_SEMESTER'];

$mhs_status1=getData("select mahasiswa.nim_mhs,pengguna.nm_pengguna,admisi.status_akd_mhs,admisi.status_apv,admisi.no_sk,
admisi.tgl_usulan,admisi.tgl_apv,admisi.tgl_sk,admisi.no_ijasah,admisi.tgl_lulus,admisi.toefl, id_admisi,
nm_jenjang ||'-'||nm_program_studi as prodi,kurun_waktu||' - smt' as kurun_waktu,alasan,nm_status_pengguna from admisi
left join mahasiswa on admisi.id_mhs=mahasiswa.id_mhs
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
left join status_pengguna on admisi.status_akd_mhs=status_pengguna.id_status_pengguna
where  admisi.id_semester=$id_semester and program_studi.id_fakultas=$kdfak and admisi.status_akd_mhs<>1 and 
(status_apv is null or status_apv = 0)");
$smarty->assign('T_STATUS1', $mhs_status1);
$smarty->assign('error',$error);

$smarty->display('admisi-mhs.tpl');

?>
