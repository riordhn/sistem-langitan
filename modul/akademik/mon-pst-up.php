<?php
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');


$id_pengguna= $user->ID_PENGGUNA; 


$kdfak=$user->ID_FAKULTAS; 

$smtaktif=getvar("select ID_SEMESTER from semester where status_aktif_up='True'");
$smt1 = isSet($_REQUEST['kdthnsmt']) ? $_REQUEST['kdthnsmt'] : $smtaktif['ID_SEMESTER'];



$smarty->assign('SMT',$smt1);

$thnsmt=getData("select id_semester as id_semester, tahun_ajaran||'-'||nm_semester||'-'||group_semester as thn_smt from semester 
where thn_akademik_semester in (select distinct thn_akademik_semester
from semester where thn_akademik_semester>=EXTRACT(YEAR FROM sysdate)-2) and tipe_semester='UP'
order by thn_akademik_semester desc, group_semester desc, nm_semester desc");
$smarty->assign('T_THNSMT', $thnsmt);

$status = isset($_REQUEST['action'])? $_REQUEST['action'] : 'tampil';

switch($status) {

case 'detail':
		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block');
	
		
		$id_mk=$_GET['id_kelas_mk'];
		$kodemk=$_GET['kodemk'];


		$info=getData("select kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas.nama_kelas,
trim(wm_concat('<li>'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||' '||(case when pengampu_mk.pjmk_pengampu_mk=1 then '(PJMA)' when pengampu_mk.pjmk_pengampu_mk=0 then '(ANGGOTA)' else '' end))) as tim
from kelas_mk 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk 
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
where pengambilan_mk.id_semester=$smt1 and kelas_mk.id_kelas_mk=$id_mk
group by kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas.nama_kelas");
		$smarty->assign('T_INFO', $info);

		$detail=getData("select distinct pengambilan_mk.id_mhs,nim_mhs,nm_pengguna,nm_program_studi,
case when status_apv_pengambilan_mk=1 then 'DiSetujui' else 'Blm Disetujui' end as status,
case when pengambilan_mk.id_mhs in (select distinct id_mhs from pengambilan_mk where id_kurikulum_mk in 
(select id_kurikulum_mk from kurikulum_mk where id_mata_kuliah in 
(select id_mata_kuliah from mata_kuliah where kd_mata_kuliah='$kodemk')) and id_semester !=$smt1) then 'Ulang' else 'Baru' end as status1
from pengambilan_mk
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
where pengambilan_mk.id_semester=$smt1 and pengambilan_mk.id_kelas_mk=$id_mk");


		$smarty->assign('T_DETAIL', $detail);
        break; 
 


case 'tampil':

$jaf=getData("select kelas_mk.id_kelas_mk,kelas_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
nama_kelas,kapasitas_kelas_mk,terisi_kelas_mk,
sum(case when status_pengambilan_mk>=1 then 1 else 0 end) as kls_terisi,
sum(case when status_pengambilan_mk=2 then 1 else 0 end) as kprs,
sum(case when status_pengambilan_mk=1 or status_pengambilan_mk>2 then 1 else 0 end) as krs,
kelas_mk.status,coalesce(nm_singkat_prodi,nm_program_studi) as nm_program_studi,
nm_jenjang,program_studi.id_program_studi,semester.tipe_semester,semester.id_semester,
semester.tahun_ajaran||'-'||semester.group_semester as tahun_ajaran
from kelas_mk
left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
left join semester on pengambilan_mk.id_semester=semester.id_semester 
where id_fakultas=$kdfak and pengambilan_mk.id_semester=$smt1
group by kelas_mk.id_kelas_mk, kelas_mk.id_kurikulum_mk, kd_mata_kuliah, 
nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas, kapasitas_kelas_mk, 
terisi_kelas_mk, kelas_mk.status, nm_program_studi,nm_singkat_prodi, nm_jenjang,program_studi.id_program_studi,
semester.tipe_semester,semester.id_semester,semester.tahun_ajaran,semester.group_semester
");


		$smarty->assign('T_MK', $jaf);
		//$smarty->assign('T_JAF', $jaf); 
		//$smarty->display('usulan-mk.tpl');
        break; 
			 
}

$smarty->display('mon-pst-up.tpl');

?>
