<?php
require_once('config.php');

$data_tab = array();

foreach ($xml_menu->tab as $tab)
{
    array_push($data_tab, array(
        'ID'    => $tab->id,
        'TITLE' => $tab->title,
        'MENU'  => $tab->menu,
        'PAGE'  => $tab->page
    ));
}

$smarty->assign('tabs', $data_tab);

$smarty->display('index1.tpl');
?>