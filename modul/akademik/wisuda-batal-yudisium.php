<?php
require 'common.php';

$kdprodi= $user->ID_PROGRAM_STUDI;
$id_pengguna= $user->ID_PENGGUNA; 
$id_fakultas = $user->ID_FAKULTAS;

$mahasiswa = getvar("SELECT MAHASISWA.ID_MHS FROM MAHASISWA 
				JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
				WHERE NIM_MHS = '$_POST[nim]' AND ID_FAKULTAS = '$id_fakultas'
				");

if ( ! empty($mahasiswa['ID_MHS'])) {
	
	// Ketika Submit
	if(isset($_POST['mode'])) {
		
		// Ambil row pengambilan wisuda
		$pengajuan_wisuda = getvar("SELECT * FROM PENGAJUAN_WISUDA WHERE ID_MHS = {$mahasiswa['ID_MHS']}");

		// Jika pengajuan ditemukan,
		if ($pengajuan_wisuda != null) {

			// Ambil status admisi terakhir yg bukan pengajuan_wisuda
			$admisi = getvar("SELECT * from (
				SELECT a.* FROM admisi A
				join semester s on s.id_semester = a.id_semester
				where a.id_mhs = {$mahasiswa['ID_MHS']} and a.ID_PENGAJUAN_WISUDA is null
				order by s.thn_akademik_semester desc, s.nm_semester desc
			) where rownum = 1");

			// Delete pengajuan + Admisi + Update Status Mahasiswa ke admisi terakhir
			deleteData("DELETE FROM PENGAJUAN_WISUDA WHERE ID_PENGAJUAN_WISUDA = {$pengajuan_wisuda['ID_PENGAJUAN_WISUDA']}");
			deleteData("DELETE FROM ADMISI WHERE ID_PENGAJUAN_WISUDA = {$pengajuan_wisuda['ID_PENGAJUAN_WISUDA']}");
			UpdateData("UPDATE MAHASISWA SET STATUS_AKADEMIK_MHS = {$admisi['STATUS_AKD_MHS']} WHERE ID_MHS = {$mahasiswa['ID_MHS']}");
		}
		else {
			$smarty->assign('pesan', "Yudisium berhasil dibatalkan");
		}

		$smarty->display("wisuda-batal-yudisium.tpl");
		exit();
	}

	$pw = getvar("SELECT COUNT(*) AS JML 
				FROM MAHASISWA
				 JOIN PENGAJUAN_WISUDA ON PENGAJUAN_WISUDA.ID_MHS = MAHASISWA.ID_MHS
				WHERE NIM_MHS = '$_POST[nim]'");
				
		if ($pw['JML'] == 1) {

			$ijasah = getvar("SELECT MAHASISWA.ID_MHS, NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, NM_JENJANG,
				PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_JENJANG, NM_TARIF_WISUDA, TARIF_WISUDA.ID_TARIF_WISUDA, YUDISIUM
				FROM MAHASISWA
				LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
				LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
				LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
				LEFT JOIN PENGAJUAN_WISUDA ON PENGAJUAN_WISUDA.ID_MHS = MAHASISWA.ID_MHS
				LEFT JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PENGAJUAN_WISUDA.ID_PERIODE_WISUDA
				LEFT JOIN TARIF_WISUDA ON TARIF_WISUDA.ID_TARIF_WISUDA = PERIODE_WISUDA.ID_TARIF_WISUDA
				WHERE NIM_MHS = '$_POST[nim]' AND PROGRAM_STUDI.ID_FAKULTAS = '$id_fakultas'");
			$smarty->assign('ijasah', $ijasah);
			
		} else {
			$smarty->assign('pesan', "Mahasiswa Belum Yudisium");
		}

	
}

$smarty->display("wisuda-batal-yudisium.tpl");