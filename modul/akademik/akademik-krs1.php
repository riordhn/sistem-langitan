<?php
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');


$id_pengguna= $user->ID_PENGGUNA;
$fak=getvar("select fakultas.id_fakultas from pegawai left join unit_kerja on pegawai.id_unit_kerja=unit_kerja.id_unit_kerja
left join program_studi on unit_kerja.id_program_studi=program_studi.id_program_studi
left join fakultas on program_studi.id_fakultas=fakultas.id_fakultas
where pegawai.id_pengguna=$id_pengguna");
$kdfak=$fak['ID_FAKULTAS'];

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True'");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);
$id_semester=$smtaktif['ID_SEMESTER'];

$stsakd=getData("select id_status_pengguna,nm_status_pengguna from status_pengguna where status_akademik=1");
$smarty->assign('T_STAKD', $stsakd);

$smt=getData("select id_semester, tahun_ajaran||' - '||nm_semester as smt from semester order by thn_akademik_semester desc,nm_semester");
$smarty->assign('T_ST', $smt);



$status = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'cari';
//echo $status;

switch($status) {

case 'add':
		 // pilih
		$smarty->assign('disp1','block');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');

		$nim_mhs = $_POST['nim'];
		$sta = $_POST['sta'];
		$smt = $_POST['smt'];
		$no_sk = $_POST['no_sk'];
		$no_ijasah = $_POST['no_ijasah'];
		$tglsk = $_POST['tglsk'];
		$toefl = $_POST['toefl'];


		$id_mhs=getvar("select id_mhs from mahasiswa where nim_mhs='$nim_mhs'");

		if ($id_mhs['ID_MHS'] !='' || $id_mhs['ID_MHS'] !=null ) {
		InsertData("insert into admisi (id_mhs,id_semester,status_akd_mhs,no_sk,tgl_sk)
		values ('$id_mhs[ID_MHS]','$smt','$sta','$no_sk','$tgl_sk')");
		}

		$mhs_status1=getData("select mahasiswa.nim_mhs,pengguna.nm_pengguna,admisi.status_akd_mhs,admisi.status_apv,admisi.no_sk,
admisi.tgl_usulan,admisi.tgl_apv,admisi.tgl_sk,admisi.no_ijasah,admisi.tgl_lulus,admisi.toefl,
nm_jenjang ||'-'||nm_program_studi as prodi from admisi
left join mahasiswa on admisi.id_mhs=mahasiswa.id_mhs
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where  admisi.id_semester=$id_semester and program_studi.id_fakultas=$kdfak");
$smarty->assign('T_STATUS1', $mhs_status1);



        break;

case 'update':
		 // pilih
		$id_mhs = $_POST['id_mhs'];
		$ips = $_POST['ips'];
		$ipk = $_POST['ipk'];
		$sksttl = $_POST['sksttl'];
		$sksmax = $_POST['sksmax'];

		if (trim($id_mhs) != '') {
		UpdateData("update mhs_status set ips_mhs_status='$ips',ipk_mhs_status='$ipk', sks_total_mhs_status='$sksttl', sks_max='$sksmax' where id_mhs=$id_mhs");
		}
		$mhs_status1=getData("select mahasiswa.nim_mhs,pengguna.nm_pengguna,admisi.status_akd_mhs,admisi.status_apv,admisi.no_sk,
admisi.tgl_usulan,admisi.tgl_apv,admisi.tgl_sk,admisi.no_ijasah,admisi.tgl_lulus,admisi.toefl,
nm_jenjang ||'-'||nm_program_studi as prodi from admisi
left join mahasiswa on admisi.id_mhs=mahasiswa.id_mhs
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where  admisi.id_semester=$id_semester and program_studi.id_fakultas=$kdfak");
$smarty->assign('T_STATUS1', $mhs_status1);
   		$smarty->assign('disp1','block');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
        break;

case 'cari':
		 // pilih

$mhs_status1=getData("select mahasiswa.nim_mhs,pengguna.nm_pengguna,admisi.status_akd_mhs,admisi.status_apv,admisi.no_sk,
admisi.tgl_usulan,admisi.tgl_apv,admisi.tgl_sk,admisi.no_ijasah,admisi.tgl_lulus,admisi.toefl,
nm_jenjang ||'-'||nm_program_studi as prodi from admisi
left join mahasiswa on admisi.id_mhs=mahasiswa.id_mhs
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where  admisi.id_semester=$id_semester and program_studi.id_fakultas=$kdfak");
$smarty->assign('T_STATUS1', $mhs_status1);
        break;

case 'tampil':
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','block');

	   $id_mhs= $_GET['idmhs'];
       $mhs_status1=getData("select mhs_status.id_mhs,mahasiswa.nim_mhs,nm_pengguna,ips_mhs_status,ipk_mhs_status,sks_total_mhs_status,sks_max from mhs_status
left join mahasiswa on mhs_status.id_mhs=mahasiswa.id_mhs
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
where mhs_status.id_mhs='$id_mhs' and id_semester='".$smtaktif['ID_SEMESTER']."'
and mahasiswa.id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak)");
$smarty->assign('T_MHS', $mhs_status1);
        break;
}
$smarty->display('akademik-krs.tpl');
//if ($_REQUEST['action']=='krs') {
//
//	// ambil semester_aktif
//	$sem_aktif="";
//	$kueri = "select id_semester from semester where STATUS_AKTIF_SEMESTER='True' order by id_semester desc";
//	$result = $db->Query($kueri)or die ("salah kueri : ");
//	while($r = $db->FetchRow()) {
//		$sem_aktif = $r[0];
//	}
//
//	// ambil id_mhs
//	$id_mhs=""; $id_prodi = ""; $id_fakul = ""; $cekal_mhs="1"; $status_krs="0"; $nim_mhs = ""; $angkatan = ""; $id_c_mhs = "";
//	$nim_mhs = $_REQUEST['nim'];
//
//	$kueri = "select a.id_mhs, a.id_program_studi, b.id_fakultas, b.status_krs, a.nim_mhs, a.status_cekal, a.thn_angkatan_mhs, a.id_c_mhs from mahasiswa a, program_studi b where a.id_program_studi=b.id_program_studi and a.nim_mhs='$nim_mhs'";
//	echo $kueri;
//	$result = $db->Query($kueri)or die ("salah kueri : ");
//	while($r = $db->FetchRow()) {
//		$id_mhs = $r[0];
//		$id_prodi = $r[1];
//		$id_fakul = $r[2];
//		$status_krs = $r[3];
//		$nim_mhs = $r[4];
//		$cekal_mhs = $r[5];
//		$angkatan = $r[6];
//		$id_c_mhs = $r[7];
//	}
//
//
//	if($angkatan == '2011') {
//		// ambil data bayar
//		$bayar_ada = false;
//		$kueri = "select count(*) as jumlah_bayar from pembayaran_cmhs where id_c_mhs={$id_c_mhs} and tgl_bayar is not null and id_semester={$sem_aktif} ";  // --> Perbaikan Fathoni
//		$result = $db->Query($kueri)or die ("salah kueri 431 ");
//		$r = $db->FetchAssoc();
//		if ($r['JUMLAH_BAYAR'] > 0) { $bayar_ada = true; }
//
//		// ambil data Finger
//		$finger_ada = true;
//
//	}else{
//		if(strlen($id_c_mhs) > 0) {
//			// ambil data bayar
//			$bayar_ada = false;
//			$kueri = "select count(*) as jumlah_bayar from pembayaran_cmhs where id_c_mhs={$id_c_mhs} and tgl_bayar is not null and id_semester={$sem_aktif} ";  // --> Perbaikan Fathoni
//			$result = $db->Query($kueri)or die ("salah kueri 432 ");
//			$r = $db->FetchAssoc();
//			if ($r['JUMLAH_BAYAR'] > 0) { $bayar_ada = true; }
//
//			// ambil data Finger
//			$finger_ada = false;
//			$kueri = "select count(*) as jari from mahasiswa where nim_mhs='".$nim_mhs."' and sidik_jari_mhs is not null";
//			$result = $db->Query($kueri)or die ("salah kueri 50 ");
//			$r = $db->FetchAssoc();
//			if ($r['JARI'] > 0) { $finger_ada = true; }
//		}else{
//			// ambil data bayar
//
//			$bayar_ada = false;
//			$kueri = "select count(*) as jumlah_bayar from pembayaran where id_mhs={$id_mhs} and tgl_bayar is not null and id_semester={$sem_aktif} ";  // --> Perbaikan Fathoni
//			//echo $kueri;
//			$result = $db->Query($kueri)or die ("salah kueri 433 ");
//			$r = $db->FetchAssoc();
//			if ($r['JUMLAH_BAYAR'] > 0) { $bayar_ada = true; }
//
//			// ambil data Finger
//			$finger_ada = false;
//			$kueri = "select count(*) as jari from fingerprint_mahasiswa where nim_mhs='".$nim_mhs."' and finger_data is not null";
//			$result = $db->Query($kueri)or die ("salah kueri 50 ");
//			$r = $db->FetchAssoc();
//			if ($r['JARI'] > 0) { $finger_ada = true; }
//
//		}
//	}
//
//
//	if($bayar_ada == false){
//		$jkueri_penawaranmk="Maaf, anda belum bisa KRS, karena data pembayaran semester ini kosong<br>Harap menyerahkan copy bukti bayar ke bagian pendidikan rektorat pada tgl 7 september 2011<br>Terima kasih"; $jkueri_mkterambil=""; $jkueri_respondosen="";
//		//$jkueri_penawaranmk="    ."; $jkueri_mkterambil=""; $jkueri_respondosen="";
//		$smarty->assign('jkueri_penawaranmk', $jkueri_penawaranmk);
//		$smarty->assign('jkueri_mkterambil', $jkueri_mkterambil);
//		$smarty->assign('jkueri_respondosen', $jkueri_respondosen);
//	}else if($finger_ada == false){
//		$jkueri_penawaranmk="<br>Maaf, anda belum bisa KRS, karena belum melakukan sidik jari<br><br>"; $jkueri_mkterambil=""; $jkueri_respondosen="";
//		//$jkueri_penawaranmk="    .."; $jkueri_mkterambil=""; $jkueri_respondosen="";
//		$smarty->assign('jkueri_penawaranmk', $jkueri_penawaranmk);
//		$smarty->assign('jkueri_mkterambil', $jkueri_mkterambil);
//		$smarty->assign('jkueri_respondosen', $jkueri_respondosen);
//	}else if($cekal_mhs == '1'){
//		$jkueri_penawaranmk="<br>Maaf, anda belum bisa KRS, karena status anda adalah cekal, hubungi bagian akademik<br><br>"; $jkueri_mkterambil=""; $jkueri_respondosen="";
//		$smarty->assign('jkueri_penawaranmk', $jkueri_penawaranmk);
//		$smarty->assign('jkueri_mkterambil', $jkueri_mkterambil);
//		$smarty->assign('jkueri_respondosen', $jkueri_respondosen);
//	}else {
	
//
//		$krsinfo=getData("select * from mhs_status where id_mhs='".$id_mhs."' and id_semester='".$sem_aktif."'");
//        $smarty->assign('KRSINFO', $krsinfo);
//
//		$batassks=getvar("select sks_max from mhs_status where id_mhs='".$id_mhs."' and id_semester='".$sem_aktif."'")
//
//		if ($_REQUEST['status']=='masuk') {
//
//			$id_mhs = $_GET['id_mhs'];
//			$id_kelas_mk = $_GET['id_mk'];
//			$sks_mk = $_GET['sks'];
//
//			$sks=getvar("select sum(kurikulum_mk.kredit_semester) as ttl from pengambilan_mk
//left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk
//left join nama_kelas on kelas_mk.no_kelas_mk=id_nama_kelas
//left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
//where pengambilan_mk.id_mhs='".$id_mhs."' and pengambilan_mk.id_semester='".$sem_aktif."'")
//			$ttlsks=$sks['ttl']+$sks_mk;
//
//			if (! isFindData("pengambilan_mk","id_kelas_mk=$id_kelas_mk and id_mhs=$id_mhs") && ($ttlsks<=$batassks['sksmax'])){
//
//			tambahdata("pengambilan_mk","id_mhs,id_kelas_mk,id_semester,status_pengambilan_mk,status_apv_pengambilan_mk","'$id_mhs','$id_kelas_mk','".$smtaktif['ID_SEMESTER']."','1','1'");
//			}
//		}
//
//		if ($_REQUEST['status']=='del') {
//
//			$id_pengambilan_mk = $_GET['id_mk'];
//
//			deleteData("delete from pengambilan_mk where id_pengambilan_mk=$id_pengambilan_mk");
//		}
//
//	// KHAS FK
//         $krs=getData("select krs_prodi.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,nm_jadwal_hari,nm_jadwal_jam from krs_prodi
//left join kelas_mk on krs_prodi.id_kelas_mk=kelas_mk.id_kelas_mk
//left join nama_kelas on kelas_mk.no_kelas_mk=id_nama_kelas
//left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
//left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
//left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
//left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
//left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
//where krs_prodi.id_program_studi='".$id_prodi."' and
//krs_prodi.id_kelas_mk not in (select id_kelas_mk from pengambilan_mk where id_mhs='".$id_mhs."' and id_semester='".$sem_aktif."')
//order by kd_mata_kuliah,nama_kelas");
//        $smarty->assign('KRS', $krs);
//         $krsambil=getData("select id_pengambilan_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,nm_jadwal_hari,nm_jadwal_jam from pengambilan_mk
//left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk
//left join nama_kelas on kelas_mk.no_kelas_mk=id_nama_kelas
//left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
//left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
//left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
//left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
//left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
//where pengambilan_mk.id_mhs='".$id_mhs."' and pengambilan_mk.id_semester='".$sem_aktif."' order by kd_mata_kuliah,nama_kelas");
//         $smarty->assign('KRSAMBIL', $krsambil);
//		 $smarty->assign('IDMHS', $id_mhs);
//
//        }
//
//}



?>