<?php
//error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

//CEK
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$id_pengguna= $user->ID_PENGGUNA; 
$kdfak=$user->ID_FAKULTAS; 
$smarty->assign('FAK',$kdfak);

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True'");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);

$thnsmt=getData("select id_semester as id_semester, tahun_ajaran||'-'||nm_semester||'-'||group_semester as thn_smt from semester where thn_akademik_semester in (
select distinct thn_akademik_semester from semester where thn_akademik_semester>=EXTRACT(YEAR FROM sysdate)-2)
order by thn_akademik_semester desc, group_semester desc, nm_semester desc");
$smarty->assign('T_THNSMT', $thnsmt);

$id_semester= isset($_REQUEST['smt']) ? $_REQUEST['smt'] :$smtaktif['ID_SEMESTER'];
$smarty->assign('smt1', $id_semester);

$nim_mhs= $_REQUEST['nim'];
$smarty->assign('nim',$nim_mhs);

$ket_drop=$_REQUEST['ket_drop'];
$smarty->assign('ket_drop',$ket_drop);

//echo $ket_drop;

if ($ket_drop=='Pilih') {
	echo '<script>alert("Keterangan Wajib di Pilih alias di Isi!!!!")</script>';
}
else
{

if ($_GET['action']=='drop') {
			$id_pengambilan_mk=$_GET['id_peng_mk'];
			$ket_drop=$_GET['ket_drop'];
		
			if ($ket_drop=='Sanksi UTS-KERJASAMA' || $ket_drop=='Sanksi UTS-NGERPEK' || $ket_drop=='Sanksi UTS-Administrasi')
			{
			UpdateData("update pengambilan_mk set status_cekal_uts='4', keterangan='$ket_drop' where id_pengambilan_mk=$id_pengambilan_mk");
			UpdateData("insert into log_sanksi_ujian (id_pengambilan_mk,status_log,keterangan_log,id_user) values ($id_pengambilan_mk,'UTS','$ket_drop',$id_pengguna)");	
			//cek
			$ceknil=getvar("select count(*) as cek,besar_nilai_mk from NILAI_MK where id_komponen_mk in 
							(select id_komponen_mk from komponen_mk where UPPER(nm_komponen_mk) like '%UTS%') and id_pengambilan_mk=$id_pengambilan_mk
							and besar_nilai_mk is not null
							group by besar_nilai_mk");
			
			
				
			if ($ceknil['CEK'] ==1 )
			{
			UpdateData("update log_sanksi_ujian set nilai='$ceknil[BESAR_NILAI_MK]' where id_pengambilan_mk=$id_pengambilan_mk");
			
			UpdateData("update NILAI_MK set besar_nilai_mk=0 where id_komponen_mk in (select id_komponen_mk from komponen_mk where UPPER(nm_komponen_mk) like '%UTS%')
						and id_pengambilan_mk=$id_pengambilan_mk");
			
			//hitung kembali
			$nil_hitung=getvar("select round(sum(nilai),2) as nilai_angka from (
								select BESAR_NILAI_MK,persentase_komponen_mk,
								BESAR_NILAI_MK*persentase_komponen_mk/100 as nilai 
								from NILAI_MK
								left join komponen_mk on NILAI_MK.ID_KOMPONEN_MK=KOMPONEN_MK.ID_KOMPONEN_MK
								where id_pengambilan_mk=$id_pengambilan_mk)");
			
			
			$nil_huruf=getvar("select NM_STANDAR_NILAI from PERATURAN_NILAI
								join STANDAR_NILAI on PERATURAN_NILAI.ID_STANDAR_NILAI=STANDAR_NILAI.ID_STANDAR_NILAI 
								where '$nil_hitung[NILAI_ANGKA]' between NILAI_MIN_PERATURAN_NILAI and NILAI_MAX_PERATURAN_NILAI");
			
			UpdateData("update pengambilan_MK set nilai_angka='$nil_hitung[NILAI_ANGKA]',nilai_huruf='$nil_huruf[NM_STANDAR_NILAI]' 
						where id_pengambilan_mk=$id_pengambilan_mk");
			
			
			}
			}
			
			if ($ket_drop=='Sanksi UAS-KERJASAMA' ||$ket_drop=='Sanksi UAS-NGERPEK' || $ket_drop=='Sanksi UAS-Administrasi')
			{
			UpdateData("update pengambilan_mk set status_cekal='4', keterangan='$ket_drop' where id_pengambilan_mk=$id_pengambilan_mk");
			UpdateData("insert into log_sanksi_ujian (id_pengambilan_mk,status_log,keterangan_log,id_user) values ($id_pengambilan_mk,'UAS','$ket_drop',$id_pengguna)");	
			 
			//cek
			$ceknil=getvar("select count(*) as cek,besar_nilai_mk from NILAI_MK where id_komponen_mk in 
							(select id_komponen_mk from komponen_mk where UPPER(nm_komponen_mk) like '%UAS%') and id_pengambilan_mk=$id_pengambilan_mk
							and besar_nilai_mk is not null
							group by besar_nilai_mk");
			if ($ceknil['CEK'] ==1 )
			{
			
			UpdateData("update log_sanksi_ujian set nilai='$ceknil[BESAR_NILAI_MK]' where id_pengambilan_mk=$id_pengambilan_mk");
			UpdateData("update NILAI_MK set besar_nilai_mk=0 where id_komponen_mk in (select id_komponen_mk from komponen_mk where UPPER(nm_komponen_mk) like '%UAS%')
						and id_pengambilan_mk=$id_pengambilan_mk");
			
			//hitung kembali
			$nil_hitung=getvar("select round(sum(nilai),2) as nilai_angka from (
								select BESAR_NILAI_MK,persentase_komponen_mk,
								BESAR_NILAI_MK*persentase_komponen_mk/100 as nilai 
								from NILAI_MK
								left join komponen_mk on NILAI_MK.ID_KOMPONEN_MK=KOMPONEN_MK.ID_KOMPONEN_MK
								where id_pengambilan_mk=$id_pengambilan_mk)");
									
			$nil_huruf=getvar("select NM_STANDAR_NILAI from PERATURAN_NILAI
								join STANDAR_NILAI on PERATURAN_NILAI.ID_STANDAR_NILAI=STANDAR_NILAI.ID_STANDAR_NILAI 
								where '$nil_hitung[NILAI_ANGKA]' between NILAI_MIN_PERATURAN_NILAI and NILAI_MAX_PERATURAN_NILAI");
			
			UpdateData("update pengambilan_MK set nilai_angka='$nil_hitung[NILAI_ANGKA]',nilai_huruf='$nil_huruf[NM_STANDAR_NILAI]' 
						where id_pengambilan_mk=$id_pengambilan_mk");
				
			}	
			}
			
}

if ($_GET['action']=='batal') {
			$id_pengambilan_mk=$_GET['id_peng_mk'];
			$ket_drop=$_GET['ket_drop'];
			
			if ($ket_drop=='Sanksi UTS-KERJASAMA' || $ket_drop=='Sanksi UTS-NGERPEK' || $ket_drop=='Sanksi UTS-Administrasi')
			{
			UpdateData("update pengambilan_mk set status_cekal_uts='1',keterangan='Batal Drop' where id_pengambilan_mk=$id_pengambilan_mk");
			}
			if ($ket_drop=='Sanksi UAS-KERJASAMA' ||$ket_drop=='Sanksi UAS-NGERPEK' || $ket_drop=='Sanksi UAS-Administrasi')
			{
			UpdateData("update pengambilan_mk set status_cekal='1', keterangan='Batal Drop' where id_pengambilan_mk=$id_pengambilan_mk");
			}
			$smarty->assign('disp1','none');
			$smarty->assign('disp2','block');
			
}
$datamhs= getData("select a.id_mhs,b.nim_mhs,h.nm_pengguna,g.nm_jenjang||'-'||f.nm_program_studi as prodi,sum(a.kredit_semester) as skstotal,
		round(sum(a.kredit_semester*(case a.nilai_huruf 
		when 'A' then 4 
		when 'AB' then 3.5 
		when 'B' then 3
		when 'BC' then 2.5
		when 'C' then 2
		when 'D' then 1
		end))/sum(a.kredit_semester),2) as IPK
		from
		(
		select a.id_mhs,e.nm_mata_kuliah,nvl(e.kredit_semester,d.kredit_semester) kredit_semester,a.nilai_huruf from (
		select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
		from pengambilan_mk a 
		left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
		left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
		where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.id_semester is not null and a.status_hapus=0
		) a
		left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
		left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
		where rangking=1 and id_mhs in (select id_mhs from mahasiswa where nim_mhs='$nim_mhs')
		) a
		left join mahasiswa b on a.id_mhs=b.id_mhs
    left join pengguna h on b.id_pengguna=h.id_pengguna
		left join program_studi f on b.id_program_studi=f.id_program_studi
    left join jenjang g on f.id_jenjang=g.id_jenjang
		where a.id_mhs  in (select id_mhs from mahasiswa where nim_mhs='$nim_mhs') and f.id_fakultas=$kdfak
   group by a.id_mhs, b.nim_mhs, h.nm_pengguna, g.nm_jenjang||'-'||f.nm_program_studi");
  

   
$smarty->assign('T_DATAMHS', $datamhs);

$krsmhs= getData("select PENGAMBILAN_MK.keterangan,id_pengambilan_mk,thn_akademik_semester||'-'||nm_semester as smt,kd_mata_kuliah,nm_mata_kuliah, 
kurikulum_mk.kredit_semester,nilai_huruf from pengambilan_mk 
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join semester on pengambilan_mk.id_semester=semester.id_semester 
where id_mhs in (select id_mhs from mahasiswa where nim_mhs='$nim_mhs') 
and pengambilan_mk.id_semester=$id_semester and status_pengambilan_mk !=0 and status_apv_pengambilan_mk='1'
and id_pengambilan_mk not in (
select id_pengambilan_mk from pengambilan_mk where id_mhs in (select id_mhs from mahasiswa where nim_mhs='$nim_mhs')
and pengambilan_mk.id_semester=$id_semester and status_pengambilan_mk='1' and status_apv_pengambilan_mk='1'
and PENGAMBILAN_MK.keterangan in ('Sanksi UTS-KERJASAMA','Sanksi UTS-NGERPEK','Sanksi UAS-KERJASAMA','Sanksi UAS-NGERPEK','Sanksi UTS-Administrasi','Sanksi UAS-Administrasi'))
");



$smarty->assign('T_KRSMHS', $krsmhs);

$krsbatal= getData("select id_pengambilan_mk,thn_akademik_semester||'-'||nm_semester as smt,kd_mata_kuliah,nm_mata_kuliah,
kurikulum_mk.kredit_semester,nilai_huruf,pengambilan_mk.keterangan from pengambilan_mk
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join semester on pengambilan_mk.id_semester=semester.id_semester
where id_mhs in (select id_mhs from mahasiswa where nim_mhs='$nim_mhs') and pengambilan_mk.id_semester=$id_semester
and pengambilan_mk.keterangan in ('Sanksi UTS-KERJASAMA','Sanksi UTS-NGERPEK','Sanksi UAS-KERJASAMA','Sanksi UAS-NGERPEK','Sanksi UTS-Administrasi','Sanksi UAS-Administrasi')
");


$smarty->assign('T_DROP', $krsbatal);

}
$smarty->display('sanksi-ujian.tpl');

?>
