<?php
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
include "class/aucc.class.php";
$aucc = new AUCC_Akademik($db);
$id_pengguna = $user->ID_PENGGUNA; 

$kdfak = $aucc->cek_fakultas_pengguna($id_pengguna);
$smarty->assign('prodi', $aucc->get_prodi_fak($kdfak['ID_FAKULTAS']));
$smarty->assign('semester', $aucc->get_semester_all());

if (post('prodi') and post('semester') and post('bayar')) {
	$smarty->assign('id_prodi', post('prodi'));
	//$smarty->assign('maba_mala', post('maba_mala'));
	$smarty->assign('biaya', $aucc->get_biaya());
	$smarty->assign('id_semester', post('semester'));
	$smarty->assign('bayar', post('bayar'));
	
	$smarty->assign('data_pembayaran', $aucc->data_pembayaran(post('prodi'), post('semester'), post('bayar')));
	$smarty->assign('detail_data_pembayaran', $aucc->detail_data_pembayaran(post('prodi'), post('semester'), post('bayar')));	
		
}

$smarty->display('pembayaran-detail.tpl');
?>
