<?php
/*
Yudi Sulistya 04/01/2012
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');
include 'includes/function.php';

$kdprodi=$user->ID_PROGRAM_STUDI;
$kdfak=$user->ID_FAKULTAS;

//Fakultas Lain
if($kdfak!=11){
	echo 'Anda tidak memiliki hak akses';
} else {

$fak=getvar("select upper(nm_fakultas) as nm_fakultas from fakultas where id_fakultas=$kdfak");

$nmfak=$fak['NM_FAKULTAS'];
$smarty->assign('FAK', $kdfak);
$smarty->assign('NM_FAK', $nmfak);
$smarty->assign('NIMX', '');

$depan=time();
$belakang=strrev(time());

if (isset($_POST['action'])=='view') {
$nim = $_POST['nim'];
//$mode = $_POST['mode'];

//Yudi Sulistya 21/07/2012
//Enkrip url untuk cetak
$id=getvar("select id_mhs from mahasiswa where nim_mhs='".$nim."'");
$cetak=$id['ID_MHS'];

//PSIKOLOGI
$link_s1 = 'proses/transkrip-cetak-s1-psikologi.php?'.paramEncrypt($depan.'='.$belakang.$depan.'&cetak='.$cetak.'&'.$belakang.'='.$depan.$belakang).'';
$smarty->assign('LINK_S1', $link_s1);
$link_s2 = 'proses/transkrip-cetak-s2-psikologi.php?'.paramEncrypt($depan.'='.$belakang.$depan.'&cetak='.$cetak.'&'.$belakang.'='.$depan.$belakang).'';
$smarty->assign('LINK_S2', $link_s2);
$link_s3 = 'proses/transkrip-cetak-s3-psikologi.php?'.paramEncrypt($depan.'='.$belakang.$depan.'&cetak='.$cetak.'&'.$belakang.'='.$depan.$belakang).'';
$smarty->assign('LINK_S3', $link_s3);

$link_s1_en = 'proses/transkrip-cetak-s1-psikologi-en.php?'.paramEncrypt($depan.'='.$belakang.$depan.'&cetak='.$cetak.'&'.$belakang.'='.$depan.$belakang).'';
$smarty->assign('LINK_S1_EN', $link_s1_en);
$link_s2_en = 'proses/transkrip-cetak-s2-psikologi-en.php?'.paramEncrypt($depan.'='.$belakang.$depan.'&cetak='.$cetak.'&'.$belakang.'='.$depan.$belakang).'';
$smarty->assign('LINK_S2_EN', $link_s2_en);
$link_s3_en = 'proses/transkrip-cetak-s3-psikologi-en.php?'.paramEncrypt($depan.'='.$belakang.$depan.'&cetak='.$cetak.'&'.$belakang.'='.$depan.$belakang).'';
$smarty->assign('LINK_S3_EN', $link_s3_en);

$xls = 'proses/rekap-nilai-xls.php?'.paramEncrypt($depan.'='.$belakang.$depan.'&cetak='.$cetak.'&'.$belakang.'='.$depan.$belakang).'';
$smarty->assign('XLS', $xls);


$datamhs=getData("select ': '||nim_mhs||' - '||upper(nm_pengguna) as nm_mhs from mahasiswa left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna where nim_mhs='".$nim."' and id_program_studi in (select id_program_studi from program_studi where id_fakultas='".$kdfak."')");
$smarty->assign('MHS', $datamhs);
$smarty->assign('NIMX', $nim);			
$biomhs=getData("select mhs.id_mhs, mhs.nim_mhs, upper(p.nm_pengguna) as nm_pengguna, j.nm_jenjang, ps.nm_program_studi, mhs.status_akademik_mhs,
				TO_CHAR(pw.tgl_lulus_pengajuan, 'DD-MM-YYYY') as tgl_lulus, pw.no_ijasah, sp.nm_status_pengguna
				from mahasiswa mhs 
				left join pengajuan_wisuda pw on pw.id_mhs=mhs.id_mhs
				left join pengguna p on mhs.id_pengguna=p.id_pengguna 
				left join status_pengguna sp on sp.id_status_pengguna=mhs.status_akademik_mhs
				left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
				left join jenjang j on ps.id_jenjang=j.id_jenjang
				where mhs.nim_mhs='".$nim."' and mhs.id_program_studi in (select id_program_studi from program_studi where id_fakultas='".$kdfak."')");
$smarty->assign('T_MHS', $biomhs);

$jaf=getData("select e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where rangking=1 and m.nim_mhs='{$nim}'");
$smarty->assign('T_MK', $jaf);

$ipk=getData("select sum(sks) as sks_total, sum((bobot*sks)) as bobot_total, round((sum((bobot*sks))/sum(sks)),2) as ipk
from 
(
select e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where rangking=1 and m.nim_mhs='{$nim}'
) uu
");
$smarty->assign('T_IPK', $ipk);

} else {
}

$smarty->display('transkrip-psikologi.tpl');  

}
?>