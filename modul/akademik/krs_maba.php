<?php
require 'common.php';
require_once 'ociFunction.php';

$smarty->assign('disp1', 'block');
$smarty->assign('disp2', 'none');
$smarty->assign('disp3', 'none');
$smarty->assign('disp4', 'none');

$id_pt			= $user->ID_PERGURUAN_TINGGI;
$id_pengguna	= $user->ID_PENGGUNA;
$kdfak			= $user->ID_FAKULTAS;
$smarty->assign('fak', $kdfak);

$smtaktif = getvar("select id_semester from semester where status_aktif_semester='True' and id_perguruan_tinggi = {$id_pt}");
$smarty->assign('smtaktif', $smtaktif['ID_SEMESTER']);

if ($_POST['action'] == 'prosesall')
{
    $id_program_studi = $_POST['prodi'];
    $smarty->assign('ID_PROGRAM_STUDI', $id_program_studi);
    $counter = $_POST['counter'];

    // ID_MHS yang tidak ada tagihan pembayaran (dr unair)
    /* 
    $proses_krs_maba = 
        "select id_mhs from mahasiswa
        where mahasiswa.id_program_studi='{$id_program_studi}'
        and trim(mahasiswa.thn_angkatan_mhs) in (select trim(thn_akademik_semester) from semester where status_aktif_semester='True')
        and id_mhs not in (select id_mhs from pengambilan_mk where id_semester='{$smtaktif[ID_SEMESTER]}'
                           and id_mhs in (select id_mhs from mahasiswa where id_program_studi='{$id_program_studi}'))
        and id_mhs in (select id_mhs from pembayaran where id_mhs in (select id_mhs from mahasiswa where id_program_studi='{$id_program_studi}'
                       and trim(mahasiswa.thn_angkatan_mhs) in (select trim(thn_akademik_semester) from semester where status_aktif_semester='True'))
                       and (tgl_bayar is not null or (id_status_pembayaran <> 2 and id_status_pembayaran is not null)) and id_semester='{$smtaktif[ID_SEMESTER]}'
                       having count(*)>0
                       group by id_mhs)
        order by id_mhs"; */
    
    $proses_krs_maba = 
        "SELECT id_mhs, p.username, p.nm_pengguna
        FROM mahasiswa m
        JOIN pengguna p ON p.id_pengguna = m.id_pengguna AND p.id_perguruan_tinggi = '{$id_pt}'
        JOIN semester s ON thn_akademik_semester = m.thn_angkatan_mhs AND s.status_aktif_semester = 'True' AND s.id_perguruan_tinggi = '{$id_pt}'
        WHERE
            m.id_program_studi = '{$id_program_studi}' AND
            m.id_mhs NOT IN (SELECT id_mhs FROM pengambilan_mk WHERE id_semester = '{$smtaktif['ID_SEMESTER']}')
        ORDER BY id_mhs";

    $result = $db->Query($proses_krs_maba) or die("salah kueri krs_maba");

    while ($r = $db->FetchRow())
    {
        for ($i = 1; $i <= $counter; $i++)
        {
            //echo "masuk";echo $_POST['cekalmhs'.$i];
            //echo $mhs['ID_MHS'];
            if ($_POST['mk' . $i] <> '' || $_POST['mk' . $i] <> null)
            {

                if ($kdfak == 7 or $kdfak == 4)
                {
                    $id_kur = getvar("select id_kurikulum_mk,id_mata_kuliah from kurikulum_mk kmk1
                                                                        where kmk1.id_program_studi='{$id_program_studi}' and kmk1.id_mata_kuliah in
									(select id_mata_kuliah from mata_kuliah where kd_mata_kuliah in
									(select c.kd_mata_kuliah from kelas_mk a, kurikulum_mk b, mata_kuliah c
                                                                        where a.id_kurikulum_mk=b.id_kurikulum_mk and a.id_kelas_mk='{$_POST['mk' . $i]}'
									and c.id_mata_kuliah=b.id_mata_kuliah))");
                }
                else
                {
                    $id_kur = getvar("select id_kurikulum_mk from kelas_mk where id_kelas_mk='{$_POST['mk' . $i]}'");
                }

                gantidata("insert into pengambilan_mk (id_mhs,id_semester,id_kelas_mk,id_kurikulum_mk,status_pengambilan_mk,status_apv_pengambilan_mk,keterangan)
							  values ('" . $r[0] . "','$smtaktif[ID_SEMESTER]','" . $_POST['mk' . $i] . "','$id_kur[ID_KURIKULUM_MK]','9','1','PAKET MABA OLEH " . $id_pengguna . "')");
            }
        }
    }
    
    echo '<script>alert("Proses PAKET MABA.. Sudah SELESAI...")</script>';
}

if ($_POST['action'] == 'prosescustem')
{
    $id_program_studi = $_POST['prodi'];
    $smarty->assign('ID_PROGRAM_STUDI', $id_program_studi);
    $counter = $_POST['counter'];
    $counter1 = $_POST['counter1'];

    for ($i = 1; $i <= $counter1; $i++)
    {
        if ($_POST['mhs' . $i] <> '' || $_POST['mhs' . $i] <> null)
        {
            for ($j = 1; $j <= $counter; $j++)
            {
                //echo "masuk";echo $_POST['cekalmhs'.$i];
                //echo $mhs['ID_MHS'];
                if ($_POST['mk' . $j] <> '' || $_POST['mk' . $j] <> null)
                {
                    /*
                      if ($kdfak==6 or $kdfak==12)
                      {
                      $id_kur=getvar("select id_kurikulum_mk from kelas_mk
                      where id_kelas_mk='".$_POST['mk'.$j]."'");
                      } else {
                      $id_kur=getvar("select id_kurikulum_mk,id_mata_kuliah from kurikulum_mk kmk1
                      where kmk1.id_program_studi=$id_program_studi and kmk1.id_mata_kuliah in
                      (select id_mata_kuliah from mata_kuliah where kd_mata_kuliah in
                      (select c.kd_mata_kuliah from kelas_mk a, kurikulum_mk b, mata_kuliah c
                      where a.id_kurikulum_mk=b.id_kurikulum_mk and a.id_kelas_mk='".$_POST['mk'.$j]."'
                      and c.id_mata_kuliah=b.id_mata_kuliah))");
                      }
                     */

                    if ($kdfak == 7 or $kdfak == 4)
                    {
                        $id_kur = getvar("select id_kurikulum_mk,id_mata_kuliah from kurikulum_mk kmk1
									where kmk1.id_program_studi=$id_program_studi and kmk1.id_mata_kuliah in
									(select id_mata_kuliah from mata_kuliah where kd_mata_kuliah in
									(select c.kd_mata_kuliah from kelas_mk a, kurikulum_mk b, mata_kuliah c
									where a.id_kurikulum_mk=b.id_kurikulum_mk and a.id_kelas_mk='" . $_POST['mk' . $j] . "'
									and c.id_mata_kuliah=b.id_mata_kuliah))");
                    }
                    else
                    {
                        $id_kur = getvar("select id_kurikulum_mk from kelas_mk
									where id_kelas_mk='" . $_POST['mk' . $j] . "'");
                    }
                    gantidata("insert into pengambilan_mk (id_mhs,id_semester,id_kelas_mk,id_kurikulum_mk,status_pengambilan_mk,status_apv_pengambilan_mk,keterangan)
							  values ('" . $_POST['mhs' . $i] . "','$smtaktif[ID_SEMESTER]','" . $_POST['mk' . $j] . "','$id_kur[ID_KURIKULUM_MK]','9','1','PAKET MABA OLEH " . $id_pengguna . "')");
                    //echo "insert into pengambilan_mk (id_mhs,id_semester,id_kelas_mk,id_kurikulum_mk,status_pengambilan_mk,keterangan)
                    //		  values ('".$_POST['mhs'.$i]."','$smtaktif[ID_SEMESTER]','".$_POST['mk'.$j]."','$id_kur[ID_KURIKULUM_MK]','9','PAKET MABA')";	
                }
            }
        }
    }
    echo '<script>alert("Proses PAKET MABA by CUSTOMIZE.. Sudah SELESAI...")</script>';
}

if ($_GET['action'] == 'view')
{
    $id_program_studi = $_GET['prodi'];
    $smarty->assign('ID_PROGRAM_STUDI', $id_program_studi);
    $detail_tawar = getData(
        "select kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
        nama_kelas,kelas_mk.status,'0' as tanda,nm_jenjang||'-'||coalesce(nm_singkat_prodi,nm_program_studi) as prodi 
        from krs_prodi
        left join program_studi on krs_prodi.id_program_studi=program_studi.id_program_studi
        left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
        left join kelas_mk on krs_prodi.id_kelas_mk=kelas_mk.id_kelas_mk and krs_prodi.id_semester=kelas_mk.id_semester
        left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
        left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
        left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
        where krs_prodi.id_semester='{$smtaktif[ID_SEMESTER]}' and krs_prodi.id_program_studi={$id_program_studi}
        and kd_mata_kuliah in 
        (select kd_mata_kuliah from kurikulum_mk 
        join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
        where kurikulum_mk.id_program_studi={$id_program_studi} and tingkat_semester=1) 
        order by kd_mata_kuliah");

    $smarty->assign('MA_PILIH', $detail_tawar);
    $smarty->assign('disp1', 'none');
    $smarty->assign('disp2', 'block');
    $smarty->assign('disp3', 'none');
    $smarty->assign('disp4', 'none');
}

if ($_GET['action'] == 'viewcustem')
{
    $smarty->assign('disp1', 'none');
    $smarty->assign('disp2', 'none');
    $smarty->assign('disp3', 'block');
    $smarty->assign('disp4', 'none');

    $id_program_studi = $_GET['prodi'];
    $smarty->assign('ID_PROGRAM_STUDI', $id_program_studi);
    $detail_tawar = getData("select kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
							nama_kelas,kelas_mk.status,'0' as tanda,nm_jenjang||'-'||coalesce(nm_singkat_prodi,nm_program_studi) as prodi 
							from krs_prodi
							left join program_studi on krs_prodi.id_program_studi=program_studi.id_program_studi
							left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
							left join kelas_mk on krs_prodi.id_kelas_mk=kelas_mk.id_kelas_mk and krs_prodi.id_semester=kelas_mk.id_semester
							left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
							left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
							left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
							where krs_prodi.id_semester='$smtaktif[ID_SEMESTER]' and krs_prodi.id_program_studi=$id_program_studi
							and kd_mata_kuliah in 
              						(select kd_mata_kuliah from kurikulum_mk 
               						join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
               						where kurikulum_mk.id_program_studi=$id_program_studi and tingkat_semester=1) 
							order by kd_mata_kuliah ");
    $smarty->assign('MA_PILIH', $detail_tawar);

    $detail_mhs = getData("select nim_mhs,nm_pengguna,nm_jenjang||'-'||coalesce(nm_singkat_prodi,nm_program_studi) as prodi,
						mahasiswa.status,'nonchecked' as tanda,id_mhs,nm_agama,
						case when mahasiswa.id_mhs in 
						(select id_mhs from pembayaran where id_mhs in (select id_mhs from mahasiswa where id_program_studi=$id_program_studi 
						and trim(mahasiswa.thn_angkatan_mhs) in (select trim(thn_akademik_semester) from semester where status_aktif_semester='True'))
						and (tgl_bayar is not null or (id_status_pembayaran <> 2 and id_status_pembayaran is not null)) and id_semester='$smtaktif[ID_SEMESTER]'
						having count(*)>0
						group by id_mhs) then 'SUDAH' else 'BELUM' end as statusbayar
						from mahasiswa
						left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
						left join agama on pengguna.id_agama=agama.id_agama
						left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
						left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
						where mahasiswa.id_program_studi=$id_program_studi
						and trim(mahasiswa.thn_angkatan_mhs) in (select trim(thn_akademik_semester) from semester where status_aktif_semester='True')
						and id_mhs not in (select id_mhs from pengambilan_mk where id_semester='$smtaktif[ID_SEMESTER]'
						and id_mhs in (select id_mhs from mahasiswa where id_program_studi=$id_program_studi))
						order by nim_mhs");
    $smarty->assign('MHS_PILIH', $detail_mhs);
}

if ($_GET['action'] == 'monitor')
{
    $id_program_studi = $_GET['prodi'];
    $smarty->assign('ID_PROGRAM_STUDI', $id_program_studi);
    $mon_krs = getData("select pengambilan_mk.id_mhs,nim_mhs,nm_pengguna,nm_jenjang||'-'||coalesce(nm_singkat_prodi,nm_program_studi) as prodi,
							count(id_kelas_mk) as ttl_mk,sum(kredit_semester) as ttl_sks,mahasiswa.status as status,
							'checked' as tanda
							from pengambilan_mk
							left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
							left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
							left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
							left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
							left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
							where id_semester='$smtaktif[ID_SEMESTER]' and mahasiswa.id_program_studi=$id_program_studi 
							and trim(mahasiswa.thn_angkatan_mhs) in (select trim(thn_akademik_semester) from semester where status_aktif_semester='True') 
							group by pengambilan_mk.id_mhs, nim_mhs, nm_pengguna,nm_jenjang,nm_singkat_prodi,nm_program_studi,mahasiswa.status
							order by nim_mhs");

    $smarty->assign('MA_KRS', $mon_krs);
    $smarty->assign('disp1', 'none');
    $smarty->assign('disp2', 'none');
    $smarty->assign('disp3', 'none');
    $smarty->assign('disp4', 'block');
}

if ($_POST['action'] == 'batal')
{
    $counter = $_POST['counter4'];
    for ($i = 1; $i <= $counter; $i++)
    {

        if ($_POST['maba' . $i] <> '' || $_POST['maba' . $i] <> null)
        {
            //catat log
            gantidata("insert into LOG_KRS (QUERY,id_pengguna)
							  values ('delete from pengambilan_mk where id_semester=" . $smtaktif[ID_SEMESTER] . "
							   and id_mhs=" . $_POST[maba . $i] . "',$id_pengguna)");

            gantidata("delete from pengambilan_mk where id_semester='$smtaktif[ID_SEMESTER]'
							   and id_mhs='" . $_POST['maba' . $i] . "'");
            //echo "delete from pengambilan_mk where id_semester='$smtaktif[ID_SEMESTER]'
            //		   and id_mhs='".$_POST['maba'.$i]."'";
        }
    }

    echo '<script>alert("Proses Pembatalan PAKET MABA.. Sudah SELESAI...")</script>';
}

/* Query Lama
$detailkrs = getData("select prodi,s1.id_program_studi,coalesce(maba,0) as maba,coalesce(krs,0) as krs from 
(select nm_jenjang||'-'||nm_program_studi as prodi,program_studi.id_program_studi,count(id_mhs) as maba from program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
join mahasiswa on program_studi.id_program_studi=mahasiswa.id_program_studi
where id_fakultas={$kdfak}
and trim(mahasiswa.thn_angkatan_mhs) in (select trim(thn_akademik_semester) from semester where status_aktif_semester='True') 
group by nm_jenjang||'-'||nm_program_studi, program_studi.id_program_studi,program_studi.id_jenjang
order by program_studi.id_jenjang,program_studi.id_program_studi)s1
left join
(select id_program_studi,count(distinct pengambilan_mk.id_mhs) as krs from pengambilan_mk
join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
where id_Semester='{$smtaktif[ID_SEMESTER]}'
and trim(mahasiswa.thn_angkatan_mhs) in (select trim(thn_akademik_semester) from semester where status_aktif_semester='True') 
group by id_program_studi)s2 on s1.id_program_studi=s2.id_program_studi");
 * 
 */

$query_detailkrs = 
    "SELECT ps.id_program_studi, nm_jenjang||'-'||nm_program_studi AS prodi, coalesce(jumlah_maba,0) as maba, coalesce(jumlah_krs,0) as krs
    FROM program_studi ps
    JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
    JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
    LEFT JOIN (
        /* Jumlah Mahasiswa Semester Baru */
        SELECT ps.id_program_studi, COUNT(m.id_mhs) jumlah_maba FROM mahasiswa m
        JOIN semester s ON s.thn_akademik_semester = m.thn_angkatan_mhs AND s.status_aktif_semester = 'True' AND s.id_perguruan_tinggi = {$id_pt}
        JOIN program_studi ps ON ps.id_program_studi = m.id_program_studi AND ps.id_fakultas = '{$kdfak}'
        GROUP BY ps.id_program_studi
        
        ) maba ON maba.id_program_studi = ps.id_program_studi
    LEFT JOIN (
        /* Jumlah Mahasiswa yg KRS Semester Baru */
        SELECT ps.id_program_studi, COUNT(DISTINCT pmk.id_mhs) AS jumlah_krs FROM pengambilan_mk pmk
        JOIN mahasiswa m ON m.id_mhs = pmk.id_mhs
        JOIN semester s ON s.thn_akademik_semester = m.thn_angkatan_mhs AND s.status_aktif_semester = 'True' AND s.id_perguruan_tinggi = {$id_pt}
        JOIN program_studi ps ON ps.id_program_studi = m.id_program_studi AND ps.id_fakultas = '{$kdfak}'
		WHERE pmk.id_semester = '{$smtaktif['ID_SEMESTER']}'
        GROUP BY ps.id_program_studi
        ) krs ON krs.id_program_studi = ps.id_program_studi
    WHERE 
        ps.id_fakultas = '{$kdfak}'";
$detailkrs = getData($query_detailkrs);

$smarty->assign('T_MABA', $detailkrs);
$smarty->display('krs_maba.tpl');