<?php
//error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

//CEK
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$id_pengguna= $user->ID_PENGGUNA; 
$kdfak=$user->ID_FAKULTAS; 
$smarty->assign('FAK',$kdfak);

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True' and id_perguruan_tinggi = {$id_pt_user}");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);

$thnsmt=getData("select id_semester as id_semester, tahun_ajaran||'-'||nm_semester||'-'||group_semester as thn_smt from semester 
					where id_perguruan_tinggi = {$id_pt_user} and thn_akademik_semester in (
						select distinct thn_akademik_semester from semester where id_perguruan_tinggi = {$id_pt_user} 
						and thn_akademik_semester>=EXTRACT(YEAR FROM sysdate)-2)
					order by thn_akademik_semester desc, group_semester desc, nm_semester desc");
$smarty->assign('T_THNSMT', $thnsmt);

$id_semester= isset($_REQUEST['smt']) ? $_REQUEST['smt'] :$smtaktif['ID_SEMESTER'];
$smarty->assign('smt1', $id_semester);

$nim_mhs= $_REQUEST['nim'];
$smarty->assign('nim',$nim_mhs);

$ket_drop=$_REQUEST['ket_drop'];
$smarty->assign('ket_drop',$ket_drop);


if ($_POST['action']=='drop') {
			//$id_pengambilan_mk=$_GET['id_peng_mk'];
			//$ket_drop=$_GET['ket_drop'];
			$counter=$_POST['count'];

		
			for ($i=1; $i<=$counter; $i++)
			{
		
			$ket_drop=$_POST['ket_drop'.$i];
			$id_pengambilan_mk=$_POST['id_peng_mk'.$i];
			
			if ($ket_drop=='Sanksi UTS-KERJASAMA' || $ket_drop=='Sanksi UTS-NGERPEK' || $ket_drop=='Sanksi UTS-Administrasi')
			{

			UpdateData("update pengambilan_mk set status_cekal_uts='4', keterangan='$ket_drop' where id_pengambilan_mk=$id_pengambilan_mk");
			UpdateData("insert into log_sanksi_ujian (id_pengambilan_mk,status_log,keterangan_log,id_user) values ($id_pengambilan_mk,'UTS','$ket_drop',$id_pengguna)");	
			//cek
			$ceknil=getvar("select count(*) as cek,besar_nilai_mk from NILAI_MK where id_komponen_mk in 
							(select id_komponen_mk from komponen_mk where UPPER(nm_komponen_mk) like '%UTS%') and id_pengambilan_mk=$id_pengambilan_mk
							and besar_nilai_mk is not null
							group by besar_nilai_mk");
			
			
				
			if ($ceknil['CEK'] ==1 )
			{
			UpdateData("update log_sanksi_ujian set nilai='$ceknil[BESAR_NILAI_MK]' where id_pengambilan_mk=$id_pengambilan_mk");
			
			UpdateData("update NILAI_MK set besar_nilai_mk=0 where id_komponen_mk in (select id_komponen_mk from komponen_mk where UPPER(nm_komponen_mk) like '%UTS%')
						and id_pengambilan_mk=$id_pengambilan_mk");
			
			//hitung kembali
			$nil_hitung=getvar("select round(sum(nilai),2) as nilai_angka from (
								select BESAR_NILAI_MK,persentase_komponen_mk,
								BESAR_NILAI_MK*persentase_komponen_mk/100 as nilai 
								from NILAI_MK
								left join komponen_mk on NILAI_MK.ID_KOMPONEN_MK=KOMPONEN_MK.ID_KOMPONEN_MK
								where id_pengambilan_mk=$id_pengambilan_mk)");
			
			
			$nil_huruf=getvar("select NM_STANDAR_NILAI from PERATURAN_NILAI
								join STANDAR_NILAI on PERATURAN_NILAI.ID_STANDAR_NILAI=STANDAR_NILAI.ID_STANDAR_NILAI 
								where '$nil_hitung[NILAI_ANGKA]' between NILAI_MIN_PERATURAN_NILAI and NILAI_MAX_PERATURAN_NILAI");
			
			UpdateData("update pengambilan_MK set nilai_angka='$nil_hitung[NILAI_ANGKA]',nilai_huruf='$nil_huruf[NM_STANDAR_NILAI]' 
						where id_pengambilan_mk=$id_pengambilan_mk");
			
			
			}
			
			}
				
			if ($ket_drop=='Sanksi UAS-KERJASAMA' ||$ket_drop=='Sanksi UAS-NGERPEK' || $ket_drop=='Sanksi UAS-Administrasi')
			{

		
			UpdateData("update pengambilan_mk set status_cekal='4', keterangan='$ket_drop' where id_pengambilan_mk=$id_pengambilan_mk");
			UpdateData("insert into log_sanksi_ujian (id_pengambilan_mk,status_log,keterangan_log,id_user) values ($id_pengambilan_mk,'UAS','$ket_drop',$id_pengguna)");	
			 
			//cek
			$ceknil=getvar("select count(*) as cek,besar_nilai_mk from NILAI_MK where id_komponen_mk in 
							(select id_komponen_mk from komponen_mk where UPPER(nm_komponen_mk) like '%UAS%') and id_pengambilan_mk=$id_pengambilan_mk
							and besar_nilai_mk is not null
							group by besar_nilai_mk");
			if ($ceknil['CEK'] ==1 )
			{
			
			UpdateData("update log_sanksi_ujian set nilai='$ceknil[BESAR_NILAI_MK]' where id_pengambilan_mk=$id_pengambilan_mk");
			UpdateData("update NILAI_MK set besar_nilai_mk=0 where id_komponen_mk in (select id_komponen_mk from komponen_mk where UPPER(nm_komponen_mk) like '%UAS%')
						and id_pengambilan_mk=$id_pengambilan_mk");
			
			//hitung kembali
			$nil_hitung=getvar("select round(sum(nilai),2) as nilai_angka from (
								select BESAR_NILAI_MK,persentase_komponen_mk,
								BESAR_NILAI_MK*persentase_komponen_mk/100 as nilai 
								from NILAI_MK
								left join komponen_mk on NILAI_MK.ID_KOMPONEN_MK=KOMPONEN_MK.ID_KOMPONEN_MK
								where id_pengambilan_mk=$id_pengambilan_mk)");
									
			$nil_huruf=getvar("select NM_STANDAR_NILAI from PERATURAN_NILAI
								join STANDAR_NILAI on PERATURAN_NILAI.ID_STANDAR_NILAI=STANDAR_NILAI.ID_STANDAR_NILAI 
								where '$nil_hitung[NILAI_ANGKA]' between NILAI_MIN_PERATURAN_NILAI and NILAI_MAX_PERATURAN_NILAI");
			
			UpdateData("update pengambilan_MK set nilai_angka='$nil_hitung[NILAI_ANGKA]',nilai_huruf='$nil_huruf[NM_STANDAR_NILAI]' 
						where id_pengambilan_mk=$id_pengambilan_mk");
				
			}
			
			
			}
			}
			
}

if ($_GET['action']=='batal') {
			$id_pengambilan_mk=$_GET['id_peng_mk'];
			$ket_drop=$_GET['ket_drop'];
			
			if ($ket_drop=='Sanksi UTS-KERJASAMA' || $ket_drop=='Sanksi UTS-NGERPEK' || $ket_drop=='Sanksi UTS-Administrasi')
			{
			UpdateData("update pengambilan_mk set status_cekal_uts='1',keterangan='Batal Drop' where id_pengambilan_mk=$id_pengambilan_mk");
			//echo "update log_sanksi_ujian set flag='0' where id_pengambilan_mk=$id_pengambilan_mk and status_log='UTS' and keterangan_log='$ket_drop' and flag='1' ";
			UpdateData("update log_sanksi_ujian set flag='0' where id_pengambilan_mk=$id_pengambilan_mk and status_log='UTS' and keterangan_log='$ket_drop' and flag='1'");
			}
			if ($ket_drop=='Sanksi UAS-KERJASAMA' ||$ket_drop=='Sanksi UAS-NGERPEK' || $ket_drop=='Sanksi UAS-Administrasi')
			{
			UpdateData("update pengambilan_mk set status_cekal='1', keterangan='Batal Drop' where id_pengambilan_mk=$id_pengambilan_mk");
			UpdateData("update log_sanksi_ujian set flag='0' where id_pengambilan_mk=$id_pengambilan_mk and status_log='UAS' and keterangan_log='$ket_drop' and flag='1'");
			}
			$smarty->assign('disp1','none');
			$smarty->assign('disp2','block');
			
}


$datamhs= getData("select b.id_mhs,b.nim_mhs,h.nm_pengguna,g.nm_jenjang||'-'||f.nm_program_studi as prodi
					from mahasiswa b
					left join pengguna h on b.id_pengguna=h.id_pengguna
					left join program_studi f on b.id_program_studi=f.id_program_studi
					left join jenjang g on f.id_jenjang=g.id_jenjang
					where b.nim_mhs='$nim_mhs' and f.id_fakultas=$kdfak
					");
  
$smarty->assign('T_DATAMHS', $datamhs);

$krsmhs= getData("select pengambilan_mk.id_pengambilan_mk,thn_akademik_semester||'-'||nm_semester as smt,kd_mata_kuliah,nm_mata_kuliah,
kurikulum_mk.kredit_semester,nilai_huruf,wm_concat(keterangan_log) as ket from pengambilan_mk
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join semester on pengambilan_mk.id_semester=semester.id_semester
left join LOG_SANKSI_UJIAN on PENGAMBILAN_MK.id_pengambilan_mk=LOG_SANKSI_UJIAN.id_pengambilan_mk and LOG_SANKSI_UJIAN.flag='1'
where id_mhs in (select m.id_mhs 
					from mahasiswa m 
					join pengguna p on p.id_pengguna = m.id_pengguna and p.id_perguruan_tinggi = {$id_pt_user}
					where m.nim_mhs='$nim_mhs' ) 
and pengambilan_mk.id_semester=$id_semester
group by pengambilan_mk.id_pengambilan_mk,thn_akademik_semester,nm_semester,kd_mata_kuliah,nm_mata_kuliah,
kurikulum_mk.kredit_semester,nilai_huruf
");


$smarty->assign('T_KRSMHS', $krsmhs);


$krsbatal= getData("select pengambilan_mk.id_pengambilan_mk,thn_akademik_semester||'-'||nm_semester as smt,kd_mata_kuliah,nm_mata_kuliah,
kurikulum_mk.kredit_semester,nilai_huruf,keterangan_log as keterangan from pengambilan_mk
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join semester on pengambilan_mk.id_semester=semester.id_semester
left join LOG_SANKSI_UJIAN on PENGAMBILAN_MK.id_pengambilan_mk=LOG_SANKSI_UJIAN.id_pengambilan_mk and LOG_SANKSI_UJIAN.flag='1'
where id_mhs in (select m.id_mhs 
					from mahasiswa m 
					join pengguna p on p.id_pengguna = m.id_pengguna and p.id_perguruan_tinggi = {$id_pt_user}
					where m.nim_mhs='$nim_mhs' )  and pengambilan_mk.id_semester=$id_semester
");


$smarty->assign('T_DROP', $krsbatal);


$smarty->display('sanksi-ujian.tpl');

?>
