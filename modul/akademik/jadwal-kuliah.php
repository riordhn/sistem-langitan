<?php
require('common.php');
require_once ('ociFunction.php');

error_reporting (E_ALL & ~E_NOTICE);

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');

$id_pengguna= $user->ID_PENGGUNA; 
$kdfak=$user->ID_FAKULTAS; 

$id_pt = $id_pt_user;

//Khusus Fakultas Psikologi
/*if($kdfak==11){

	echo '
		<script>
		location.href="#aktivitas-manajemen!jadwal-kuliah-psikologi.php";
		</script>
	';

//Fakultas Lain
} else {*/

$smtaktif=getvar("select id_semester from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' and status_aktif_semester='True'");
$smtpil = isset($_REQUEST['smt']) ? $_REQUEST['smt'] : $smtaktif['ID_SEMESTER'];
//echo $smtpil;

$smarty->assign('smtaktif',$smtpil);
/*
$idkur=getvar("select id_kurikulum_prodi from kurikulum_prodi
left join kurikulum on kurikulum_prodi.id_kurikulum=kurikulum.id_kurikulum
where kurikulum_prodi.id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak)");
$smarty->assign('idkur', $idkur['ID_KURIKULUM_PRODI']);
*/

$smt=getData("select id_semester, tahun_ajaran||' - '||nm_semester as smt from semester where thn_akademik_semester in (
select * from (select distinct thn_akademik_semester 
from semester where thn_akademik_semester<=EXTRACT(YEAR FROM sysdate) order by thn_akademik_semester desc) 
where rownum<=3) and id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}'
order by thn_akademik_semester desc, nm_semester desc");
$smarty->assign('T_ST', $smt);

$kelas=getData("select id_nama_kelas,'KELAS-'||nama_kelas as nama_kelas from nama_kelas where id_perguruan_tinggi = {$id_pt} order by nama_kelas");
$smarty->assign('NM_KELAS', $kelas);

/*
$datakur=getData("select id_kurikulum_prodi,nm_kurikulum||'-'||tahun_kurikulum as nama  from kurikulum_prodi
left join kurikulum on kurikulum_prodi.id_kurikulum=kurikulum.id_kurikulum
where kurikulum_prodi.id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak)");
$smarty->assign('T_KUR', $datakur);
*/

$prodi=getData("select id_program_studi, nm_jenjang||'-'||nm_program_studi as namaprodi from program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where id_fakultas=$kdfak order by nm_jenjang");
$smarty->assign('T_PRODI', $prodi);

$smarty->assign('statuskls', array('R','AJ'));

$status = isset($_REQUEST['action'])? $_REQUEST['action'] : 'tampil';

switch($status) {
case 'tambahkelas':
		 // pilih
		$smarty->assign('disp1','block');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
		
		$kap_kelas = $_POST['kap_kelas'];
		$kelas_mk = $_POST['kelas_mk'];
		$ren_kul = $_POST['ren_kul'];
		$id_kur_mk= $_POST['id_kur_mk'];
		$id_hari = $_POST['hari'];
		$id_jam= $_POST['jam'];
		$smt1= $_POST['smt'];
		$idruang= $_POST['ruangan'];
		$prodi= $_POST['kd_prodi']; 
		$id_jenis_pertemuan= $_POST['jenis_pertemuan'];
		$besar_sks= $_POST['besar_sks'];
		
		//echo $smt1;
		
/*		
		tambahdata("kelas_mk","id_kurikulum_mk,id_semester,id_program_studi,no_kelas_mk,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk","'$id_kur_mk','".$smtaktif['ID_SEMESTER']."','$prodi','$kelas_mk','$kap_kelas','$ren_kul'");
		$kelas=getvar("select id_kelas_mk from kelas_mk where id_kurikulum_mk='$id_kur_mk' and id_semester='".$smtaktif['ID_SEMESTER']."' and no_kelas_mk=$kelas_mk and id_program_studi=$prodi");
		tambahdata("jadwal_kelas","id_kelas_mk,id_ruangan,id_jadwal_jam,id_jadwal_hari","'".$kelas['ID_KELAS_MK']."','$idruang','$id_jam','$id_hari'");
*/		
		$check=getvar("select count(*) as ada from kelas_mk where id_kurikulum_mk=$id_kur_mk and id_semester=$smt1 and no_kelas_mk=$kelas_mk ");
		if ($check['ADA']<1) {
		
		tambahdata("kelas_mk","id_kurikulum_mk,id_semester,id_program_studi,no_kelas_mk,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk","'$id_kur_mk','$smt1','$prodi','$kelas_mk','$kap_kelas','$ren_kul'");
		$id_kelas=getvar("select id_kelas_mk from kelas_mk where id_kurikulum_mk=$id_kur_mk and id_semester=$smt1 and id_program_studi=$prodi and no_kelas_mk=$kelas_mk");
		$id_kelas_mk=$id_kelas['ID_KELAS_MK'];
		//echo "aa";
		tambahdata("jadwal_kelas","id_kelas_mk,id_jadwal_hari,id_jadwal_jam,id_ruangan,id_jenis_pertemuan,besar_sks","'$id_kelas_mk','$id_hari','$id_jam','$idruang','$id_jenis_pertemuan','$besar_sks'");
		} else {
				echo '<script>alert("Kelas Sudah Ada")</script>';
		}
		//echo "bb";
$jaf=getData("select distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester, 
jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam,
case when jadwal_kelas.id_jenis_pertemuan=1 then 'K-('||jadwal_kelas.besar_sks||' sks)' else
case when jadwal_kelas.id_jenis_pertemuan=2 then 'P-('||jadwal_kelas.besar_sks||' sks)' else 'Blm di Set' end end as jenis,
ruangan.id_ruangan,nm_ruangan,nm_jenjang||'-'||coalesce(nm_singkat_prodi,nm_program_studi) as prodiasal,
no_kelas_mk,nama_kelas.nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status from kelas_mk 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join semester on kelas_mk.id_semester=semester.id_semester 
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk 
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester=$smt1");

		$smarty->assign('T_MK', $jaf);
        break;      

case 'tambahhari':
		 // pilih
		$smarty->assign('disp1','block');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
		
		$kap_kelas = $_POST['kap_kelas'];
		$id_kelas_mk = $_POST['id_kelas_mk'];
		$ren_kul = $_POST['ren_kul'];
		$id_kur_mk= $_POST['id_kur_mk'];
		$id_hari = $_POST['hari'];
		$id_jam= $_POST['jam'];
		$smt1= $_POST['smtb'];
		$idruang= $_POST['ruangan'];
		$id_jenis_pertemuan= $_POST['jenis_pertemuan'];
		$besar_sks= $_POST['besar_sks'];

		tambahdata("jadwal_kelas","id_kelas_mk,id_jadwal_hari,id_jadwal_jam,id_ruangan,id_jenis_pertemuan,besar_sks","$id_kelas_mk,$id_hari,$id_jam,$idruang,$id_jenis_pertemuan,$besar_sks");	

		$jaf=getData("select distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester, 
jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam,
case when jadwal_kelas.id_jenis_pertemuan=1 then 'K-('||jadwal_kelas.besar_sks||' sks)' else
case when jadwal_kelas.id_jenis_pertemuan=2 then 'P-('||jadwal_kelas.besar_sks||' sks)' else 'Blm di Set' end end as jenis,
ruangan.id_ruangan,nm_ruangan,nm_jenjang||'-'||coalesce(nm_singkat_prodi,nm_program_studi) as prodiasal,
no_kelas_mk,nama_kelas.nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status from kelas_mk 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join semester on kelas_mk.id_semester=semester.id_semester 
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk 
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester=$smt1");
		$smarty->assign('T_MK', $jaf);
        break;      
		
case 'del':
		 // pilih
		$id_klsmk= $_GET['id_klsmk'];
		$smt1= $_GET['smt'];
		
		deleteData("delete from jadwal_kelas where id_kelas_mk='$id_klsmk'");
		deleteData("delete from pengampu_mk where id_kelas_mk='$id_klsmk'");
		deleteData("delete from krs_prodi where id_kelas_mk='$id_klsmk'");		
		deleteData("delete from kelas_mk where id_kelas_mk='$id_klsmk'");

		$jaf=getData("select distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester, 
jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam,
case when jadwal_kelas.id_jenis_pertemuan=1 then 'K-('||jadwal_kelas.besar_sks||' sks)' else
case when jadwal_kelas.id_jenis_pertemuan=2 then 'P-('||jadwal_kelas.besar_sks||' sks)' else 'Blm di Set' end end as jenis,
ruangan.id_ruangan,nm_ruangan,nm_jenjang||'-'||coalesce(nm_singkat_prodi,nm_program_studi) as prodiasal,
no_kelas_mk,nama_kelas.nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status from kelas_mk 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join semester on kelas_mk.id_semester=semester.id_semester 
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk 
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester=$smt1");
		$smarty->assign('T_MK', $jaf);
        break; 
		
case 'updateview':
		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
		
		$id_klsmk= $_GET['id_klsmk'];
			
		$ruang=getData("select id_ruangan,nm_ruangan||'-'||kapasitas_ruangan as ruang from ruangan
left join gedung on gedung.id_gedung=ruangan.id_gedung
left join gedung_kuliah on gedung.id_gedung=gedung_kuliah.id_gedung
where gedung.id_fakultas='".$kdfak."' order by nm_ruangan");
		$smarty->assign('T_RUANG', $ruang);
		
		$hari=getData("select id_jadwal_hari,nm_jadwal_hari from jadwal_hari");
		$smarty->assign('T_HARI', $hari);
		
		$jenis_pertemuan=getData("select id_jenis_pertemuan,nm_jenis_pertemuan from jenis_pertemuan");
		$smarty->assign('JENIS_PERTEMUAN', $jenis_pertemuan);

		$jam=getData("select id_jadwal_jam,nm_jadwal_jam from jadwal_jam where id_fakultas='".$kdfak."' order by nm_jadwal_jam");
		$smarty->assign('T_JAM', $jam);
		
		$jaf=getData("select kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
		kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt,jadwal_kelas.besar_sks,
jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,kelas_mk.id_semester,jadwal_kelas.id_jenis_pertemuan,
ruangan.id_ruangan,nm_ruangan,no_kelas_mk,nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status from kelas_mk
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas  
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join semester on kelas_mk.id_semester=semester.id_semester 
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
where program_studi.id_fakultas=$kdfak and kelas_mk.id_kelas_mk=$id_klsmk");
		$smarty->assign('TJAF', $jaf);
        break; 

case 'adkelview':
		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','block');
		$smarty->assign('disp4','none');
		
		$id_klsmk= $_GET['id_klsmk'];
		$smt= $_GET['smt'];
		
		
		$ruang=getData("select id_ruangan,nm_ruangan||'-'||kapasitas_ruangan as ruang from ruangan
left join gedung on gedung.id_gedung=ruangan.id_gedung
left join gedung_kuliah on gedung.id_gedung=gedung_kuliah.id_gedung
where gedung.id_fakultas='".$kdfak."' order by nm_ruangan");
		$smarty->assign('T_RUANG', $ruang);
		
		$jenis_pertemuan=getData("select id_jenis_pertemuan,nm_jenis_pertemuan from jenis_pertemuan");
		$smarty->assign('JENIS_PERTEMUAN', $jenis_pertemuan);

		$hari=getData("select id_jadwal_hari,nm_jadwal_hari from jadwal_hari");
		$smarty->assign('T_HARI', $hari);

		$jam=getData("select id_jadwal_jam,nm_jadwal_jam from jadwal_jam where id_fakultas='".$kdfak."' order by nm_jadwal_jam");
		$smarty->assign('T_JAM', $jam);
		
		$jaf=getData("select kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kelas_mk.id_program_studi,
jadwal_kelas.id_jadwal_hari,jadwal_kelas.id_jadwal_jam,kelas_mk.id_semester,jadwal_kelas.besar_sks,jadwal_kelas.id_jenis_pertemuan,
ruangan.id_ruangan,no_kelas_mk,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status,kelas_mk.id_kurikulum_mk from kelas_mk
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas  
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join semester on kelas_mk.id_semester=semester.id_semester 
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
where program_studi.id_fakultas=$kdfak and kelas_mk.id_kelas_mk=$id_klsmk and kelas_mk.id_semester=$smt and rownum=1");
		$smarty->assign('TJAF1', $jaf);
        break; 

case 'adhariview':
		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','block');
		
		$id_klsmk= $_GET['id_klsmk'];
		
		$ruang=getData("select id_ruangan,nm_ruangan||' ('||kapasitas_ruangan||')' as ruang from ruangan
left join gedung on gedung.id_gedung=ruangan.id_gedung
left join gedung_kuliah on gedung.id_gedung=gedung_kuliah.id_gedung
where gedung.id_fakultas='".$kdfak."' order by nm_ruangan");
		$smarty->assign('T_RUANG', $ruang);
		
		$jenis_pertemuan=getData("select id_jenis_pertemuan,nm_jenis_pertemuan from jenis_pertemuan");
		$smarty->assign('JENIS_PERTEMUAN', $jenis_pertemuan);
		
		$hari=getData("select id_jadwal_hari,nm_jadwal_hari from jadwal_hari");
		$smarty->assign('T_HARI', $hari);

		$jam=getData("select id_jadwal_jam,nm_jadwal_jam from jadwal_jam where id_fakultas='".$kdfak."' order by nm_jadwal_jam");
		$smarty->assign('T_JAM', $jam);
		
		$jaf=getData("select kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kelas_mk.id_kurikulum_mk,
		kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt,kelas_mk.id_program_studi,
jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,kelas_mk.id_semester,jadwal_kelas.besar_sks,jadwal_kelas.id_jenis_pertemuan,
ruangan.id_ruangan,nm_ruangan,no_kelas_mk,nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status from kelas_mk
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas  
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join semester on kelas_mk.id_semester=semester.id_semester 
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
where program_studi.id_fakultas=$kdfak and kelas_mk.id_kelas_mk=$id_klsmk and rownum=1");
		$smarty->assign('TJAF2', $jaf);
        break; 
		
case 'update1':
		
		$kap_kelas = $_POST['kap_kelas'];
		$kelas_mk = $_POST['kelas_mk'];
		$status_kls = $_POST['status_kls'];
		$ren_kul = $_POST['ren_kul'];
		$id_kelas_mk= $_POST['id_kelas_mk'];
		$id_hari = $_POST['hari'];
		$id_jam= $_POST['jam'];
		$smt1= $_POST['smtb'];
		$idruang= $_POST['ruangan'];
		$id_jenis_pertemuan= $_POST['jenis_pertemuan'];
		$besar_sks= $_POST['besar_sks'];
		
		
		UpdateData("update kelas_mk set kapasitas_kelas_mk=$kap_kelas, jumlah_pertemuan_kelas_mk=$ren_kul,no_kelas_mk='$kelas_mk',status='$status_kls' where id_kelas_mk=$id_kelas_mk");
		UpdateData("update krs_prodi set status='$status_kls' where id_kelas_mk=$id_kelas_mk");

		//UpdateData("update jadwal_kelas set id_jadwal_jam=$id_jam, id_jadwal_hari=$id_hari,id_ruangan='$idruang' where id_kelas_mk=$id_kelas_mk");
		
		$cek=getvar("select count(*) as ada from jadwal_kelas where id_kelas_mk=$id_kelas_mk");
		//echo "select count(*) as ada from jadwal_kelas where id_kelas_mk=$id_kelas_mk";
		//echo $cek['ADA'];
		if ($cek['ADA']>0)	
		{


		UpdateData("update jadwal_kelas set id_jadwal_jam=$id_jam, id_jadwal_hari=$id_hari,id_ruangan='$idruang',id_jenis_pertemuan=$id_jenis_pertemuan,besar_sks=$besar_sks where id_kelas_mk=$id_kelas_mk");

		//echo "update jadwal_kelas set id_jadwal_jam=$id_jam, id_jadwal_hari=$id_hari,id_ruangan='$idruang' where id_kelas_mk=$id_kelas_mk";
		}
		else
		{
				//echo "masuk";

		tambahdata("jadwal_kelas","id_kelas_mk,id_jadwal_hari,id_jadwal_jam,id_ruangan,id_jenis_pertemuan,besar_sks","$id_kelas_mk,$id_hari,$id_jam,$idruang,$id_jenis_pertemuan,$besar_sks");

		}



		//echo '<script> $("#aktivitas-pjk").load("jadwal-kuliah.php"); </script>';
		$smarty->assign('disp1','block');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
		
		//$smt1 = isSet($_POST['smt']) ? $_POST['smt'] : $smtaktif['ID_SEMESTER'];
		$id_kur1 = isSet($_POST['thkur']) ? $_POST['thkur'] : '0';
		//if ($smt1!='') {
		$smarty->assign('SMT',$smtpil);
		
		$jaf=getData("select distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester, 
jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
case when jadwal_kelas.id_jenis_pertemuan=1 then 'K-('||jadwal_kelas.besar_sks||' sks)' else
case when jadwal_kelas.id_jenis_pertemuan=2 then 'P-('||jadwal_kelas.besar_sks||' sks)' else 'Blm di Set' end end as jenis,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam, 
ruangan.id_ruangan,nm_ruangan,nm_jenjang||'-'||coalesce(nm_singkat_prodi,nm_program_studi) as prodiasal,
no_kelas_mk,nama_kelas.nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status from kelas_mk 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join semester on kelas_mk.id_semester=semester.id_semester 
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk 
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester=$smtpil");
//}

		$smarty->assign('T_MK', $jaf);
		break;

case 'tampil':
		 // pilih
		$smarty->assign('disp1','block');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
		
		//$smt1 = isSet($_POST['smt']) ? $_POST['smt'] : $smtaktif['ID_SEMESTER'];
		$id_kur1 = isSet($_POST['thkur']) ? $_POST['thkur'] : '0';
		//if ($smt1!='') {
		$smarty->assign('SMT',$smtpil);
		
		$jaf=getData("select distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester,
case when jadwal_kelas.id_jenis_pertemuan=1 then 'K-('||jadwal_kelas.besar_sks||' sks)' else
case when jadwal_kelas.id_jenis_pertemuan=2 then 'P-('||jadwal_kelas.besar_sks||' sks)' else 'Blm di Set' end end as jenis,
jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam, 
ruangan.id_ruangan,nm_ruangan,nm_jenjang||'-'||coalesce(nm_singkat_prodi,nm_program_studi) as prodiasal,
no_kelas_mk,nama_kelas.nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status from kelas_mk 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join semester on kelas_mk.id_semester=semester.id_semester 
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk 
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester=$smtpil");
//}

		$smarty->assign('T_MK', $jaf);
        break; 
			 
}

$smarty->display('jadwal-kuliah.tpl');
/*}*/
?>
