<?php
include('config.php');

$mode = get('mode', 'view');

$ruangan_table = new RUANGAN_TABLE($db);
$presensi_kelas_table = new PRESENSI_KELAS_TABLE($db);
$presensi_mkmhs_table = new PRESENSI_MKMHS_TABLE($db);
$kelas_mk_table = new KELAS_MK_TABLE($db);
$mata_kuliah_table = new MATA_KULIAH_TABLE($db);
$dosen_table = new DOSEN_TABLE($db);
$pengguna_table = new PENGGUNA_TABLE($db);
$mahasiswa_table = new MAHASISWA_TABLE($db);

if ($mode == 'view')
{
    $ruangan_set = $ruangan_table->Select();
    $smarty->assign('ruangan_set', $ruangan_set);
}
else if ($mode == 'ruangan')
{
    $ruangan = $ruangan_table->Single(get('id_ruangan'));
    $presensi_kelas_table->FillRuangan($ruangan);
    $ruangan->PRESENSI_KELASs = $presensi_kelas_table->SelectCriteria("ORDER BY WAKTU_MULAI");
    $presensi_mkmhs_table->FillPresensiKelasSet($ruangan->PRESENSI_KELASs);
    
    for ($i = 0; $i < $ruangan->PRESENSI_KELASs->Count(); $i++)
    {
        $presensi_kelas = $ruangan->PRESENSI_KELASs->Get($i);
        $ruangan->PRESENSI_KELASs->Get($i)->WAKTU_MULAI = substr($presensi_kelas->WAKTU_MULAI, 10, 5);
        $ruangan->PRESENSI_KELASs->Get($i)->WAKTU_SELESAI = substr($presensi_kelas->WAKTU_SELESAI, 10, 5);
        $dosen_table->FillPresensiKelas($presensi_kelas);
        //$pengguna_table->FillDosen($presensi_kelas->DOSEN);
        $kelas_mk_table->FillPresensiKelas($presensi_kelas);
        $mata_kuliah_table->FillKelasMk($presensi_kelas->KELAS_MK);
    }
    
    $smarty->assign('ruangan', $ruangan);
}
else if ($mode == 'kelas')
{
    $presensi_kelas = $presensi_kelas_table->Single(get('id_presensi_kelas'));
    $ruangan_table->FillPresensiKelas($presensi_kelas);
    $presensi_mkmhs_table->FillPresensiKelas($presensi_kelas);
    
    for ($i = 0; $i < $presensi_kelas->PRESENSI_MKMHSs->Count(); $i++)
    {
        $presensi_mkmhs = $presensi_kelas->PRESENSI_MKMHSs->Get($i);
        $mahasiswa_table->FillPresensiMkmhs($presensi_mkmhs);
        $pengguna_table->FillMahasiswa($presensi_mkmhs->MAHASISWA);
        
        $presensi_mkmhs->KEHADIRAN = $presensi_mkmhs->KEHADIRAN ? "Hadir" : "Tidak Hadir";
    }
    
    $smarty->assign('presensi_kelas', $presensi_kelas);
}    
    

$smarty->display("presensi/{$mode}.tpl");
?>
