<?php
require('common.php');
require_once ('ociFunction.php');

error_reporting (E_ALL & ~E_NOTICE);
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$id_pengguna= $user->ID_PENGGUNA;

$fak=getvar("select fakultas.id_fakultas from pegawai left join unit_kerja on pegawai.id_unit_kerja=unit_kerja.id_unit_kerja
left join program_studi on unit_kerja.id_program_studi=program_studi.id_program_studi
left join fakultas on program_studi.id_fakultas=fakultas.id_fakultas
where pegawai.id_pengguna=$id_pengguna");
$kdfak=$fak['ID_FAKULTAS'];
//$smarty->assign('FAK', $kdfak);

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True'");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);

$smt=getData("select id_semester, tahun_ajaran||' - '||nm_semester as smt from semester order by thn_akademik_semester desc,nm_semester desc");
$smarty->assign('T_ST', $smt);

$smt1 = isSet($_POST['smt']) ? $_POST['smt'] : $smtaktif['ID_SEMESTER'];

if ($smt1!='') {
		$smarty->assign('SMT',$smt1);
	/*
		$jaf=getData("select id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,tm, count(nim_mhs) as mhscekal,
case when id_kelas_mk in (select distinct id_kelas_mk from pengambilan_mk where id_semester=$smt1 and status_cekal=0) then 'SUDAH' else 'BELUM' end as status
from
(select s1.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester, s1.id_mhs,nim_mhs,nm_pengguna,
nama_kelas,sum(kehadiran) as hadir,count(id_presensi_kelas) as tm, round(sum(kehadiran)/count(id_presensi_kelas)*100,2)as hadir
from ( select presensi_mkmhs.id_presensi_kelas,id_kelas_mk,presensi_mkmhs.id_mhs,kehadiran
from presensi_mkmhs
left join presensi_kelas on presensi_mkmhs.id_presensi_kelas=presensi_kelas.id_presensi_kelas
left join mahasiswa on presensi_mkmhs.id_mhs=mahasiswa.id_mhs
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
where program_studi.id_fakultas=$kdfak and id_kelas_mk in (select id_kelas_mk from kelas_mk where id_semester=$smt1)
order by presensi_mkmhs.id_presensi_kelas,presensi_mkmhs.id_mhs) s1
left join mahasiswa on s1.id_mhs=mahasiswa.id_mhs
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join kelas_mk on s1.id_kelas_mk=kelas_mk.id_kelas_mk
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
where kelas_mk.id_semester=$smt1
group by s1.id_kelas_mk, kd_mata_kuliah, nm_mata_kuliah, kurikulum_mk.kredit_semester,
s1.id_mhs, nim_mhs, nm_pengguna, nama_kelas
having sum(kehadiran)/count(id_presensi_kelas)<0.75
order by kd_mata_kuliah,nim_mhs)
group by id_kelas_mk, kd_mata_kuliah, nm_mata_kuliah, nama_kelas, tm, case when id_kelas_mk in (select distinct id_kelas_mk from pengambilan_mk where id_semester=$smt1 and status_cekal=2) then 'BELUM' else 'SUDAH' end
order by kd_mata_kuliah");
*/
$jaf=getData("select s1.id_kelas_mk,s1.kd_mata_kuliah,s1.nm_mata_kuliah,s1.nama_kelas,s1.tm,
count(s1.id_mhs) as mhscekal,
case when sum(case when s1.status_cekal=0 then 1 else 0 end )=count(s1.id_mhs) then 'SUDAH' else 'BELUM' end as status
from
(select presensi_kelas.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,
presensi_mkmhs.id_mhs,nim_mhs,nm_pengguna,pengambilan_mk.status_cekal,
sum(kehadiran) as hadir , count(presensi_kelas.id_presensi_kelas) as tm,
round(sum(kehadiran)/count(presensi_kelas.id_presensi_kelas)*100,2)as persen
from presensi_mkmhs
left join presensi_kelas on presensi_mkmhs.id_presensi_kelas=presensi_kelas.id_presensi_kelas
left join mahasiswa on presensi_mkmhs.id_mhs=mahasiswa.id_mhs
inner join pengambilan_mk on mahasiswa.id_mhs=pengambilan_mk.id_mhs and presensi_kelas.id_kelas_mk=pengambilan_mk.id_kelas_mk and pengambilan_mk.id_semester=$smt1
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join kelas_mk on presensi_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester=$smt1
group by presensi_kelas.id_kelas_mk, kd_mata_kuliah, nm_mata_kuliah, nama_kelas,
presensi_mkmhs.id_mhs, nim_mhs, nm_pengguna,pengambilan_mk.status_cekal
having sum(kehadiran)/count(presensi_mkmhs.id_presensi_kelas)<0.75)s1
group by s1.id_kelas_mk,s1.kd_mata_kuliah,s1.nm_mata_kuliah,s1.nama_kelas,s1.tm
");

$smarty->assign('T_MK', $jaf);
}

if ($_GET['action']=='cekal') {
	$idkelas= $_GET['id_kelas_mk'];
	$smarty->assign('ID_KELAS',$idkelas);
	$kd_mata_kuliah= $_GET['kd_mk'];

	$smarty->assign('disp1','none');
	$smarty->assign('disp2','block');

$datacekal=getData("select s1.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
s1.id_mhs,nim_mhs,nm_pengguna,nama_kelas,sum(kehadiran) as hadir,
case when s1.id_mhs in (select id_mhs from pengambilan_mk
where status_cekal=0 and id_semester='$smtaktif[ID_SEMESTER]' and id_kelas_mk=$idkelas) then 'checked' else 'non' end as tanda,
count(id_presensi_kelas) as tm, round(sum(kehadiran)/count(id_presensi_kelas)*100,2)as prosen,
case when s1.id_mhs  in (select id_mhs  from pengambilan_mk
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
where (id_semester!='$smtaktif[ID_SEMESTER]' or id_semester is null) and kd_mata_kuliah='$kd_mata_kuliah') then 'Ulang' else 'Baru' end as status
from ( select presensi_mkmhs.id_presensi_kelas,id_kelas_mk,presensi_mkmhs.id_mhs,kehadiran
from presensi_mkmhs
left join presensi_kelas on presensi_mkmhs.id_presensi_kelas=presensi_kelas.id_presensi_kelas
left join mahasiswa on presensi_mkmhs.id_mhs=mahasiswa.id_mhs
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
where program_studi.id_fakultas=$kdfak and id_kelas_mk in (select id_kelas_mk from kelas_mk where id_semester='$smtaktif[ID_SEMESTER]')
order by presensi_mkmhs.id_presensi_kelas,presensi_mkmhs.id_mhs) s1
left join mahasiswa on s1.id_mhs=mahasiswa.id_mhs
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join kelas_mk on s1.id_kelas_mk=kelas_mk.id_kelas_mk
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
where kelas_mk.id_semester='$smtaktif[ID_SEMESTER]' and s1.id_kelas_mk=$idkelas
group by s1.id_kelas_mk, kd_mata_kuliah, nm_mata_kuliah, kurikulum_mk.kredit_semester, s1.id_mhs, nim_mhs, nm_pengguna, nama_kelas,
case when s1.id_mhs in (select id_mhs from pengambilan_mk
where status_cekal=0 and id_semester='$smtaktif[ID_SEMESTER]' and id_kelas_mk=$idkelas) then 'checked' else 'non' end, case when s1.id_mhs  in (select id_mhs from pengambilan_mk
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
where (id_semester!='$smtaktif[ID_SEMESTER]' or id_semester is null) and kd_mata_kuliah='$kd_mata_kuliah') then 'Ulang' else 'Baru' end
having sum(kehadiran)/count(id_presensi_kelas)<0.75
");

$smarty->assign('DATA_CEKAL', $datacekal);
}

if ($_POST['action']=='proses') {
	$idkelas= $_POST['id_kelas_mk'];
	$counter=$_POST['counter'];

	$smarty->assign('disp1','block');
	$smarty->assign('disp2','none');

	//$mhs=getData("select id_mhs from pengambilan_mk where id_kelas_mk=$idkelas  and id_semester='$smtaktif[ID_SEMESTER]'");


for ($i=1; $i<=$counter; $i++)
	{
	//echo $mhs['ID_MHS'];
	if ($_POST['cekalmhs'.$i]<>'' || $_POST['cekalmhs'.$i]<>null) {
			   gantidata("update pengambilan_mk set status_cekal=3 where id_kelas_mk=$idkelas and id_mhs='".$_POST['cekalmhs'.$i]."' and id_semester='$smtaktif[ID_SEMESTER]'");
			   //echo "update pengambilan_mk set status_cekal=0 where id_kelas_mk=$idkelas and id_mhs='".$_POST['mhs'.$i]."' and id_semester='$smtaktif[ID_SEMESTER]'";
			   //gantidata("update pengambilan_mk set id_kelas_mk=$kelas_mk_tujuan where id_pengambilan_mk='".$_POST['pil'.$i]."'");
			}
	}
	 gantidata("update pengambilan_mk set status_cekal=1 where id_kelas_mk=$idkelas and (status_cekal=0 or status_cekal is null) and id_semester='$smtaktif[ID_SEMESTER]'");
	 gantidata("update pengambilan_mk set status_cekal=1 where id_kelas_mk=$idkelas and (status_cekal=2 or status_cekal is null) and id_semester='$smtaktif[ID_SEMESTER]'");
	 gantidata("update pengambilan_mk set status_cekal=0 where id_kelas_mk=$idkelas and (status_cekal=3) and id_semester='$smtaktif[ID_SEMESTER]'");

	 //echo "update pengambilan_mk set status_cekal=1 where id_kelas_mk=$idkelas and status_cekal=2  and id_semester='$smtaktif[ID_SEMESTER]'";
}

$fak=getData("select fakultas.id_fakultas from pegawai left join unit_kerja on pegawai.id_unit_kerja=unit_kerja.id_unit_kerja
left join program_studi on unit_kerja.id_program_studi=program_studi.id_program_studi
left join fakultas on program_studi.id_fakultas=fakultas.id_fakultas
where pegawai.id_pengguna=$id_pengguna");
$smarty->assign('FAK', $fak);

$jadual=getData("select case when trunc((((86400*(SYSDATE-tgl_selesai_jsf))/60)/60)/24) < 1 then 'UTS' else 'UAS' end as status_uas
from jadwal_semester_fakultas where id_semester=$smtaktif[ID_SEMESTER] and id_fakultas=$kdfak and id_kegiatan=44");
$smarty->assign('JADUAL', $jadual);

if ($_POST['action']=='proses_cekal_all') {
$fak_id = $_POST['fak_id'];
$smt_id = $_POST['smt_id'];

$cekal_all="select kelas, mhs, pertemuan, masuk, round(((masuk/pertemuan)*100),2) as persen,
case when round(((masuk/pertemuan)*100),2)>=75 then 1 else 0 end as cekal
from
(select id_kelas_mk, id_kelas_mk as kelas, count(id_kelas_mk) as pertemuan
from presensi_kelas
where id_kelas_mk in (select id_kelas_mk from kelas_mk where id_program_studi in
(select id_program_studi from program_studi where id_fakultas=$kdfak) and id_semester=$smtaktif[ID_SEMESTER])
group by id_kelas_mk) k
left join
(select id_kelas_mk, id_mhs as mhs, count(id_mhs) as masuk
from presensi_mkmhs
left join presensi_kelas on presensi_kelas.id_presensi_kelas=presensi_mkmhs.id_presensi_kelas
where id_kelas_mk in (select id_kelas_mk from kelas_mk where id_program_studi in
(select id_program_studi from program_studi where id_fakultas=$fak_id) and id_semester=$smt_id) and kehadiran>0
group by id_mhs, id_kelas_mk) m
on k.id_kelas_mk=m.id_kelas_mk";
$result = $db->Query($cekal_all)or die("salah kueri proses cekal all ");
while($r = $db->FetchRow()) {
	$id_kelas_mk = $r[0];
	$id_mhs = $r[1];
	$tm = $r[2];
	$hadir = $r[3];
	$persen_presensi = $r[4];
	$status_cekal = $r[5];

	gantidata("update pengambilan_mk set status_cekal=$status_cekal, persen_presensi=$persen_presensi where id_kelas_mk=$id_kelas_mk and id_mhs=$id_mhs and id_semester=$smt_id");
}
}

//Khusus Psikologi untuk MA dengan sistem Blok
if ($_POST['action']=='proses_cekal_blok') {
$fak_id = $_POST['fak_id'];
$smt_id = $_POST['smt_id'];

$cekal_blok = "select kelas, mhs, pertemuan, masuk, round(((masuk/pertemuan)*100),2) as persen,
case when round(((masuk/pertemuan)*100),2)>=75 then 1 else 0 end as cekal
from
(select id_kelas_mk, id_kelas_mk as kelas, count(id_kelas_mk) as pertemuan
from presensi_kelas
where id_kelas_mk in (select id_kelas_mk from kelas_mk where id_program_studi in
(select id_program_studi from program_studi where id_fakultas=$fak_id) and id_semester=$smt_id)
group by id_kelas_mk) k
left join
(select id_kelas_mk, id_mhs as mhs, count(id_mhs) as masuk
from presensi_mkmhs
left join presensi_kelas on presensi_kelas.id_presensi_kelas=presensi_mkmhs.id_presensi_kelas
where id_kelas_mk in (select id_kelas_mk from kelas_mk where id_program_studi in
(select id_program_studi from program_studi where id_fakultas=$fak_id) and id_semester=$smt_id) and kehadiran>0
group by id_mhs, id_kelas_mk) m
on k.id_kelas_mk=m.id_kelas_mk
where pertemuan>10";
$result = $db->Query($cekal_blok)or die("salah kueri proses cekal blok ");
while($r = $db->FetchRow()) {
	$id_kelas_mk = $r[0];
	$id_mhs = $r[1];
	$tm = $r[2];
	$hadir = $r[3];
	$persen_presensi = $r[4];
	$status_cekal = $r[5];

	gantidata("update pengambilan_mk set status_cekal=$status_cekal, persen_presensi=$persen_presensi where id_kelas_mk=$id_kelas_mk and id_mhs=$id_mhs and id_semester=$smt_id");
}
}
$smarty->display('proses-cekal-otomatis.tpl');
?>
