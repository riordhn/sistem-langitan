<?php
require('common.php');
require_once ('ociFunction.php');

error_reporting (E_ALL & ~E_NOTICE);

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');

$id_pengguna= $user->ID_PENGGUNA;
$username= $user->USERNAME;
$waktu= date('Y-m-d H:i:s');
$kdfak=$user->ID_FAKULTAS;

//Fakultas Lain
if($kdfak!=11){
	echo 'Anda tidak memiliki hak akses';
} else {

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True'");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);

/*
$idkur=getvar("select id_kurikulum_prodi from kurikulum_prodi
left join kurikulum on kurikulum_prodi.id_kurikulum=kurikulum.id_kurikulum
where kurikulum_prodi.id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak)");
$smarty->assign('idkur', $idkur['ID_KURIKULUM_PRODI']);
*/
$smt=getData("select id_semester, tahun_ajaran||' - '||nm_semester as smt from semester where thn_akademik_semester in (
select * from (select distinct thn_akademik_semester
from semester where thn_akademik_semester<=EXTRACT(YEAR FROM sysdate) order by thn_akademik_semester desc)
where rownum<=3) and nm_semester in ('Ganjil', 'Genap')
order by thn_akademik_semester desc, nm_semester desc");
$smarty->assign('T_ST', $smt);

$kelas=getData("select id_nama_kelas,'KELAS-'||nama_kelas as nama_kelas from nama_kelas");
$smarty->assign('NM_KELAS', $kelas);
/*
$datakur=getData("select id_kurikulum_prodi,nm_kurikulum||'-'||tahun_kurikulum as nama  from kurikulum_prodi
left join kurikulum on kurikulum_prodi.id_kurikulum=kurikulum.id_kurikulum
where kurikulum_prodi.id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak)");
$smarty->assign('T_KUR', $datakur);

$prodi=getData("select id_program_studi, nm_jenjang||'-'||nm_singkat_prodi as namaprodi from program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
where id_fakultas=$kdfak order by nm_jenjang");
$smarty->assign('T_PRODI', $prodi);
*/
$smarty->assign('statuskls', array('R','AJ'));

$status = isset($_REQUEST['action'])? $_REQUEST['action'] : 'tampil';

switch($status) {
case 'tambahkelas':
		 // pilih
		$smarty->assign('disp1','block');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');

		$kap_kelas = $_POST['kap_kelas'];
		$kelas_mk = $_POST['kelas_mk'];
		$ren_kul = $_POST['ren_kul'];
		$id_kur_mk= $_POST['id_kur_mk'];
		$id_hari = $_POST['hari'];
		$id_jam= $_POST['jam'];
		$smt1= $_POST['smtb'];
		$idruang= $_POST['ruangan'];
		$prodi= $_POST['kd_prodi'];

		$check=getvar("select count(*) as ada from kelas_mk where id_kurikulum_mk=$id_kur_mk and id_semester=$smt1 and no_kelas_mk=$kelas_mk ");
		if ($check['ADA']<1) {

		tambahdata("kelas_mk","id_kurikulum_mk,id_semester,id_program_studi,no_kelas_mk,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,nm_kelas_mk","'$id_kur_mk','$smt1','$prodi','$kelas_mk','$kap_kelas','$ren_kul','Added by: ".$username." on ".$waktu."'");
		$id_kelas=getvar("select id_kelas_mk from kelas_mk where id_kurikulum_mk=$id_kur_mk and id_semester=$smt1 and id_program_studi=$prodi and no_kelas_mk=$kelas_mk");
		$id_kelas_mk=$id_kelas['ID_KELAS_MK'];

		tambahdata("jadwal_kelas","id_kelas_mk,id_jadwal_hari,id_jadwal_jam,id_ruangan","'$id_kelas_mk','$id_hari','$id_jam','$idruang'");
		} else {
				echo '<script>alert("Kelas Sudah Ada")</script>';
		}

		$jaf=getData("select distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nm_kelas_mk as keterangan,
		kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester,
		jadwal_kelas.id_jadwal_kelas,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
		jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam,
		ruangan.id_ruangan,nm_ruangan,nm_jenjang||'-'||nm_singkat_prodi as prodiasal,
		no_kelas_mk,nama_kelas.nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status from kelas_mk
		left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
		left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
		left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
		left join semester on kelas_mk.id_semester=semester.id_semester
		left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
		left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
		left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
		left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
		left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
		left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
		left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
		where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester=$smt1");

		$smarty->assign('T_MK', $jaf);
        break;

case 'tambahhari':
		 // pilih
		$smarty->assign('disp1','block');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');

		$kap_kelas = $_POST['kap_kelas'];
		$id_kelas_mk = $_POST['id_kelas_mk'];
		$ren_kul = $_POST['ren_kul'];
		$id_kur_mk= $_POST['id_kur_mk'];
		$id_hari = $_POST['hari'];
		$id_jam= $_POST['jam'];
		$smt1= $_POST['smtb'];
		$idruang= $_POST['ruangan'];

		$check=getvar("select count(*) as ada from jadwal_kelas where id_kelas_mk=$id_kelas_mk and id_hari=$id_hari");
		if ($check['ADA']<1) {
			tambahdata("jadwal_kelas","id_kelas_mk,id_jadwal_hari,id_jadwal_jam,id_ruangan","'$id_kelas_mk','$id_hari','$id_jam','$idruang'");
		} else {
			echo '<script>alert("Hari Sudah Ada")</script>';
		}

		$jaf=getData("select distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nm_kelas_mk as keterangan,
		kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester,
		jadwal_kelas.id_jadwal_kelas,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
		jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam,
		ruangan.id_ruangan,nm_ruangan,nm_jenjang||'-'||nm_singkat_prodi as prodiasal,
		no_kelas_mk,nama_kelas.nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status from kelas_mk
		left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
		left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
		left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
		left join semester on kelas_mk.id_semester=semester.id_semester
		left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
		left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
		left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
		left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
		left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
		left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
		left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
		where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester=$smt1");
		$smarty->assign('T_MK', $jaf);
        break;

case 'del':
		 // pilih
		$id_klsmk= $_GET['id_klsmk'];
		$smt1= $_GET['smt'];

		deleteData("delete from jadwal_kelas where id_kelas_mk='$id_klsmk'");
		deleteData("delete from pengampu_mk where id_kelas_mk='$id_klsmk'");
		deleteData("delete from krs_prodi where id_kelas_mk='$id_klsmk'");
		deleteData("delete from kelas_mk where id_kelas_mk='$id_klsmk'");

		$jaf=getData("select distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nm_kelas_mk as keterangan,
		kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester,
		jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
		jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam,
		ruangan.id_ruangan,nm_ruangan,nm_jenjang||'-'||nm_singkat_prodi as prodiasal,
		no_kelas_mk,nama_kelas.nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status from kelas_mk
		left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
		left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
		left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
		left join semester on kelas_mk.id_semester=semester.id_semester
		left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
		left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
		left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
		left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
		left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
		left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
		left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
		where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester=$smt1");
		$smarty->assign('T_MK', $jaf);
        break;

case 'updateview':
		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');

		$id_klsmk= $_GET['id_klsmk'];
		$id_jk= $_GET['id_jk'];

		$ruang=getData("select id_ruangan,nm_ruangan||' - '||kapasitas_ruangan as ruang from ruangan
		left join gedung on gedung.id_gedung=ruangan.id_gedung
		left join gedung_kuliah on gedung.id_gedung=gedung_kuliah.id_gedung
		where gedung.id_fakultas='".$kdfak."' order by nm_ruangan");
		$smarty->assign('T_RUANG', $ruang);

		$hari=getData("select id_jadwal_hari,nm_jadwal_hari from jadwal_hari");
		$smarty->assign('T_HARI', $hari);

		$jam=getData("select id_jadwal_jam,jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as nm_jadwal_jam from jadwal_jam
		where id_fakultas='".$kdfak."' order by jam_mulai,menit_mulai,jam_selesai,menit_selesai");
		$smarty->assign('T_JAM', $jam);

		$jaf=getData("select kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
		kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt,
		jadwal_kelas.id_jadwal_kelas,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,kelas_mk.id_semester,
		ruangan.id_ruangan,nm_ruangan,no_kelas_mk,nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status from kelas_mk
		left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
		left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
		left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
		left join semester on kelas_mk.id_semester=semester.id_semester
		left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
		left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
		left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
		left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
		left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
		where program_studi.id_fakultas=$kdfak and kelas_mk.id_kelas_mk=$id_klsmk and jadwal_kelas.id_jadwal_kelas=$id_jk");
		$smarty->assign('TJAF', $jaf);
        break;

case 'adkelview':
		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','block');
		$smarty->assign('disp4','none');

		$id_klsmk= $_GET['id_klsmk'];
		$smt= $_GET['smt'];


		$ruang=getData("select id_ruangan,nm_ruangan||' - '||kapasitas_ruangan as ruang from ruangan
		left join gedung on gedung.id_gedung=ruangan.id_gedung
		left join gedung_kuliah on gedung.id_gedung=gedung_kuliah.id_gedung
		where gedung.id_fakultas='".$kdfak."' order by nm_ruangan");
		$smarty->assign('T_RUANG', $ruang);

		$hari=getData("select id_jadwal_hari,nm_jadwal_hari from jadwal_hari");
		$smarty->assign('T_HARI', $hari);

		$jam=getData("select id_jadwal_jam,jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as nm_jadwal_jam from jadwal_jam
		where id_fakultas='".$kdfak."' order by jam_mulai,menit_mulai,jam_selesai,menit_selesai");
		$smarty->assign('T_JAM', $jam);

		$jaf=getData("select kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kelas_mk.id_program_studi,
		jadwal_kelas.id_jadwal_hari,jadwal_kelas.id_jadwal_jam,kelas_mk.id_semester,
		ruangan.id_ruangan,no_kelas_mk,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status,kelas_mk.id_kurikulum_mk from kelas_mk
		left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
		left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
		left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
		left join semester on kelas_mk.id_semester=semester.id_semester
		left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
		left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
		left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
		left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
		left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
		where program_studi.id_fakultas=$kdfak and kelas_mk.id_kelas_mk=$id_klsmk and kelas_mk.id_semester=$smt and rownum=1");
		$smarty->assign('TJAF1', $jaf);
        break;

case 'adhariview':
		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','block');

		$id_klsmk= $_GET['id_klsmk'];

		$ruang=getData("select id_ruangan,nm_ruangan||' ('||kapasitas_ruangan||')' as ruang from ruangan
		left join gedung on gedung.id_gedung=ruangan.id_gedung
		left join gedung_kuliah on gedung.id_gedung=gedung_kuliah.id_gedung
		where gedung.id_fakultas='".$kdfak."' order by nm_ruangan");
		$smarty->assign('T_RUANG', $ruang);

		$hari=getData("select id_jadwal_hari,nm_jadwal_hari from jadwal_hari");
		$smarty->assign('T_HARI', $hari);

		$jam=getData("select id_jadwal_jam,jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as nm_jadwal_jam from jadwal_jam
		where id_fakultas='".$kdfak."' order by jam_mulai,menit_mulai,jam_selesai,menit_selesai");
		$smarty->assign('T_JAM', $jam);

		$jaf=getData("select kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kelas_mk.id_kurikulum_mk,
		kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt,kelas_mk.id_program_studi,
		jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,kelas_mk.id_semester,
		ruangan.id_ruangan,nm_ruangan,no_kelas_mk,nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status from kelas_mk
		left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
		left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
		left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
		left join semester on kelas_mk.id_semester=semester.id_semester
		left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
		left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
		left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
		left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
		left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
		where program_studi.id_fakultas=$kdfak and kelas_mk.id_kelas_mk=$id_klsmk and rownum=1");
		$smarty->assign('TJAF2', $jaf);
        break;

case 'delhari':

		$id_jk = $_GET['id_jk'];

		deleteData("delete from jadwal_kelas where id_jadwal_kelas='$id_jk'");
		
		echo '<script> $("#utility-manajemen_kelas").load("jadwal-kuliah-psikologi.php"); </script>';

		break;

case 'update1':

		$kap_kelas = $_POST['kap_kelas'];
		$kelas_mk = $_POST['kelas_mk'];
		$status_kls = $_POST['status_kls'];
		$ren_kul = $_POST['ren_kul'];
		$id_kelas_mk = $_POST['id_kelas_mk'];
		$id_jk = $_POST['id_jk'];
		$id_hari = $_POST['hari'];
		$id_jam = $_POST['jam'];
		$smt1 = $_POST['smtb'];
		$idruang = $_POST['ruangan'];

		$kap_ruangan=getvar("select kapasitas_ruangan from ruangan where id_ruangan='".$idruang."'");

		if ($kap_kelas > $kap_ruangan['KAPASITAS_RUANGAN']) {
			echo '<script>alert ("Kelas tidak cukup");</script>';
		} else {
			UpdateData("update kelas_mk set kapasitas_kelas_mk=$kap_kelas, jumlah_pertemuan_kelas_mk=$ren_kul,no_kelas_mk='$kelas_mk',status='$status_kls',nm_kelas_mk='Updated by: ".$username." on ".$waktu."' where id_kelas_mk=$id_kelas_mk");

			$cek=getvar("select count(*) as ada from jadwal_kelas where id_kelas_mk=$id_kelas_mk");
			if ($cek['ADA']>0)
			{
			UpdateData("update jadwal_kelas set id_jadwal_jam=$id_jam, id_jadwal_hari=$id_hari,id_ruangan=$idruang where id_kelas_mk=$id_kelas_mk and id_jadwal_kelas=$id_jk");
			}
			else
			{
			tambahdata("jadwal_kelas","id_kelas_mk,id_jadwal_hari,id_jadwal_jam,id_ruangan","$id_kelas_mk,$id_hari,$id_jam,$idruang");
			}

			echo '<script> $("#utility-manajemen_kelas").load("jadwal-kuliah-psikologi.php"); </script>';
		}
		break;
		
case 'tampil':
		 // pilih
		$smarty->assign('disp1','block');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');

		$smt1 = isSet($_POST['smt']) ? $_POST['smt'] : $smtaktif['ID_SEMESTER'];
		$id_kur1 = isSet($_POST['thkur']) ? $_POST['thkur'] : '0';
		if ($smt1!='') {
		$smarty->assign('SMT',$smt);

		$jaf=getData("select distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nm_kelas_mk as keterangan,
		kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester,
		jadwal_kelas.id_jadwal_kelas,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
		jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam,
		ruangan.id_ruangan,nm_ruangan,nm_jenjang||'-'||nm_singkat_prodi as prodiasal,
		no_kelas_mk,nama_kelas.nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status from kelas_mk
		left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
		left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
		left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
		left join semester on kelas_mk.id_semester=semester.id_semester
		left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
		left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
		left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
		left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
		left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
		left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
		left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
		where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester=$smt1");
		}
		$smarty->assign('T_MK', $jaf);
		
		$krsaktif=getvar("select count(*) as ada from jadwal_semester_fakultas where id_kegiatan=56 and id_fakultas=11 and id_semester=(select id_semester from semester where status_aktif_semester='True') and SYSDATE >= tgl_mulai_jsf");
		$smarty->assign('krsaktif',$krsaktif['ADA']);
        break;

}

$smarty->display('jadwal-kuliah-psikologi.tpl');

}
?>
