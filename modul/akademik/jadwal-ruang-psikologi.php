<?php
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');

$id_pengguna= $user->ID_PENGGUNA;
$kdfak=$user->ID_FAKULTAS;

//Fakultas Lain
if($kdfak!=11){
	echo 'Anda tidak memiliki hak akses';
} else {

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True'");
$smt1 = isSet($_REQUEST['smt']) ? $_REQUEST['smt'] : $smtaktif['ID_SEMESTER'];
$smarty->assign('SMT',$smt1);

$datahari=getData("select * from jadwal_hari");
$smarty->assign('T_HARI', $datahari);

$smt=getData("select id_semester, tahun_ajaran||' - '||nm_semester as smt from semester where thn_akademik_semester in (
select * from (select distinct thn_akademik_semester
from semester where thn_akademik_semester<=EXTRACT(YEAR FROM sysdate) order by thn_akademik_semester desc)
where rownum<=4) and nm_semester in ('Ganjil', 'Genap') and rownum<=4
order by thn_akademik_semester desc, nm_semester desc");
$smarty->assign('T_ST', $smt);

// Detect current day (Yudi Sulistya, 01/08/2012)
$today = date("w")+1;
$hariini=getvar("select id_jadwal_hari from jadwal_hari where id_jadwal_hari=$today");
$smarty->assign('hariini',$hariini['ID_JADWAL_HARI']);

$hari=getData("select id_jadwal_hari, upper(nm_jadwal_hari) as nm_jadwal_hari from jadwal_hari");
$smarty->assign('T_HR', $hari);

$idhari = isSet($_POST['hari']) ? $_POST['hari'] : $hariini['ID_JADWAL_HARI'];
$smarty->assign('HRGET',$idhari0);

$dataprodi=getData("select id_program_studi,nm_jenjang||'-'||nm_program_studi as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_fakultas=$kdfak");
$smarty->assign('T_PRODI', $dataprodi);

$datasmt=getData("select distinct tingkat_semester, 'SEMESTER '||tingkat_semester as nm_smt from kurikulum_mk where id_program_studi=134 order by tingkat_semester");
$smarty->assign('T_SMT', $datasmt);

$nm_smt=getData("select tahun_ajaran||' - '||nm_semester as nm_smt from semester where id_semester=$smt1");
$smarty->assign('NM_SMT',$nm_smt);

$nm_grup=getData("select group_semester as grup from semester group by group_semester order by group_semester");
$smarty->assign('NM_GRUP',$nm_grup);

$status = isset($_REQUEST['action'])? $_REQUEST['action'] : 'tampil';

switch($status) {
//Cek ploting ruangan
case 'plot':
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');

$idhari = isSet($_POST['hari']) ? $_POST['hari'] : $hariini['ID_JADWAL_HARI'];
$smarty->assign('HRGET',$idhari);
$smarty->assign('HRGET0',$idhari);
$smarty->assign('HRGET1',$idhari);

$plotjadruang=getData("SELECT nm_ruangan,
MAX(DECODE ( kdjam , 08, nm_mk )) jam0,
MAX(DECODE ( kdjam , 09, nm_mk )) jam1,
MAX(DECODE ( kdjam , 10, nm_mk )) jam2,
MAX(DECODE ( kdjam , 11, nm_mk )) jam3,
MAX(DECODE ( kdjam , 12, nm_mk )) jam4,
MAX(DECODE ( kdjam , 13, nm_mk )) jam5,
MAX(DECODE ( kdjam , 14, nm_mk )) jam6,
MAX(DECODE ( kdjam , 15, nm_mk )) jam7,
MAX(DECODE ( kdjam , 16, nm_mk )) jam8,
MAX(DECODE ( kdjam , 17, nm_mk )) jam9,
MAX(DECODE ( kdjam , 18, nm_mk )) jam10,
MAX(DECODE ( kdjam , 19, nm_mk )) jam11
FROM
(
select kdjam,nm_ruangan||'<br><br>(<font color=\"blue\"><b>'||kapasitas_ruangan||'</b></font>)' as nm_ruangan,
wm_concat(' '||kd_mata_kuliah||' - '||nm_mata_kuliah||' ('||nama_kelas||') '||kredit_semester||' SKS<br><br>') as nm_mk
from
(
select jam_mulai as kdjam,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,kapasitas_ruangan
from kelas_mk left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester=$smt1 and jadwal_kelas.id_jadwal_hari=$idhari
group by jam_mulai,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,kapasitas_ruangan
)
group by kdjam,nm_ruangan,kapasitas_ruangan
)
GROUP BY nm_ruangan");
$smarty->assign('T_PLOTRUANG', $plotjadruang);

$datajadruang=getData("SELECT nm_ruangan,
MAX(DECODE ( kdjam , 08, nm_mk )) jam0,
MAX(DECODE ( kdjam , 09, nm_mk )) jam1,
MAX(DECODE ( kdjam , 10, nm_mk )) jam2,
MAX(DECODE ( kdjam , 11, nm_mk )) jam3,
MAX(DECODE ( kdjam , 12, nm_mk )) jam4,
MAX(DECODE ( kdjam , 13, nm_mk )) jam5,
MAX(DECODE ( kdjam , 14, nm_mk )) jam6,
MAX(DECODE ( kdjam , 15, nm_mk )) jam7,
MAX(DECODE ( kdjam , 16, nm_mk )) jam8,
MAX(DECODE ( kdjam , 17, nm_mk )) jam9,
MAX(DECODE ( kdjam , 18, nm_mk )) jam10,
MAX(DECODE ( kdjam , 19, nm_mk )) jam11
FROM
(
select kdjam,nm_ruangan||'<br><br>(<font color=\"blue\"><b>'||kapasitas_ruangan||'</b></font>)' as nm_ruangan,
wm_concat(' '||kd_mata_kuliah||' - '||nm_mata_kuliah||' ('||nama_kelas||') '||kredit_semester||' SKS ('||tot||')<br><br>') as nm_mk
from
(
select kdjam,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kredit_semester,kapasitas_ruangan,
case when (kapasitas_ruangan-jml)<=2 then '<font color=\"red\"><b>'||coalesce(jml,0)||'</b></font>' else '<font color=\"green\"><b>'||coalesce(jml,0)||'</b></font>' end as tot
from
(
(
select kelas_mk.id_kelas_mk,jam_mulai as kdjam,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,kapasitas_ruangan
from kelas_mk left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester=$smt1 and jadwal_kelas.id_jadwal_hari=$idhari
group by kelas_mk.id_kelas_mk,jam_mulai,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,kapasitas_ruangan
) a
left join
(
select id_kelas_mk, count(*) as jml from pengambilan_mk where id_semester=$smt1 and status_apv_pengambilan_mk=1 and
id_kelas_mk in (select id_kelas_mk from kelas_mk where id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak))
group by id_kelas_mk
) b on a.id_kelas_mk=b.id_kelas_mk
)
group by kdjam,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kredit_semester,kapasitas_ruangan,jml
)
group by kdjam,nm_ruangan,kapasitas_ruangan
)
GROUP BY nm_ruangan");
$smarty->assign('T_JADRUANG', $datajadruang);
break;

//Cek jadwal ruang berdasarkan peserta
case 'view':
$smarty->assign('disp1','none');
$smarty->assign('disp2','block');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');

$idhari0 = $_POST['hari0'];
$smarty->assign('HRGET',$idhari0);
$smarty->assign('HRGET0',$idhari0);
$smarty->assign('HRGET1',$idhari0);

$datajadruang=getData("SELECT nm_ruangan,
MAX(DECODE ( kdjam , 08, nm_mk )) jam0,
MAX(DECODE ( kdjam , 09, nm_mk )) jam1,
MAX(DECODE ( kdjam , 10, nm_mk )) jam2,
MAX(DECODE ( kdjam , 11, nm_mk )) jam3,
MAX(DECODE ( kdjam , 12, nm_mk )) jam4,
MAX(DECODE ( kdjam , 13, nm_mk )) jam5,
MAX(DECODE ( kdjam , 14, nm_mk )) jam6,
MAX(DECODE ( kdjam , 15, nm_mk )) jam7,
MAX(DECODE ( kdjam , 16, nm_mk )) jam8,
MAX(DECODE ( kdjam , 17, nm_mk )) jam9,
MAX(DECODE ( kdjam , 18, nm_mk )) jam10,
MAX(DECODE ( kdjam , 19, nm_mk )) jam11
FROM
(
select kdjam,nm_ruangan||'<br><br>(<font color=\"blue\"><b>'||kapasitas_ruangan||'</b></font>)' as nm_ruangan,
wm_concat(' '||kd_mata_kuliah||' - '||nm_mata_kuliah||' ('||nama_kelas||') '||kredit_semester||' SKS ('||tot||')<br><br>') as nm_mk
from
(
select kdjam,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kredit_semester,kapasitas_ruangan,
case when (kapasitas_ruangan-jml)<=2 then '<font color=\"red\"><b>'||coalesce(jml,0)||'</b></font>' else '<font color=\"green\"><b>'||coalesce(jml,0)||'</b></font>' end as tot
from
(
(
select kelas_mk.id_kelas_mk,jam_mulai as kdjam,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,kapasitas_ruangan
from kelas_mk left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester=$smt1 and jadwal_kelas.id_jadwal_hari=$idhari0
group by kelas_mk.id_kelas_mk,jam_mulai,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,kapasitas_ruangan
) a
left join
(
select id_kelas_mk, count(*) as jml from pengambilan_mk where id_semester=$smt1 and status_apv_pengambilan_mk=1 and
id_kelas_mk in (select id_kelas_mk from kelas_mk where id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak))
group by id_kelas_mk
) b on a.id_kelas_mk=b.id_kelas_mk
)
group by kdjam,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kredit_semester,kapasitas_ruangan,jml
)
group by kdjam,nm_ruangan,kapasitas_ruangan
)
GROUP BY nm_ruangan");
$smarty->assign('T_JADRUANG', $datajadruang);

$plotjadruang=getData("SELECT nm_ruangan,
MAX(DECODE ( kdjam , 08, nm_mk )) jam0,
MAX(DECODE ( kdjam , 09, nm_mk )) jam1,
MAX(DECODE ( kdjam , 10, nm_mk )) jam2,
MAX(DECODE ( kdjam , 11, nm_mk )) jam3,
MAX(DECODE ( kdjam , 12, nm_mk )) jam4,
MAX(DECODE ( kdjam , 13, nm_mk )) jam5,
MAX(DECODE ( kdjam , 14, nm_mk )) jam6,
MAX(DECODE ( kdjam , 15, nm_mk )) jam7,
MAX(DECODE ( kdjam , 16, nm_mk )) jam8,
MAX(DECODE ( kdjam , 17, nm_mk )) jam9,
MAX(DECODE ( kdjam , 18, nm_mk )) jam10,
MAX(DECODE ( kdjam , 19, nm_mk )) jam11
FROM
(
select kdjam,nm_ruangan||'<br><br>(<font color=\"blue\"><b>'||kapasitas_ruangan||'</b></font>)' as nm_ruangan,
wm_concat(' '||kd_mata_kuliah||' - '||nm_mata_kuliah||' ('||nama_kelas||') '||kredit_semester||' SKS<br><br>') as nm_mk
from
(
select jam_mulai as kdjam,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,kapasitas_ruangan
from kelas_mk left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester=$smt1 and jadwal_kelas.id_jadwal_hari=$idhari0
group by jam_mulai,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,kapasitas_ruangan
)
group by kdjam,nm_ruangan,kapasitas_ruangan
)
GROUP BY nm_ruangan");
$smarty->assign('T_PLOTRUANG', $plotjadruang);
break;

//Cek jadwal ruang berdasarkan kelompok MK per semester
case 'smt':
$smarty->assign('disp1','none');
$smarty->assign('disp2','none');
$smarty->assign('disp3','block');
$smarty->assign('disp4','none');

$smt2 = $_POST['smt2'];
$smarty->assign('SMTGET',$smt2);

$kdprodi = $_POST['kdprodi'];
$smarty->assign('PRODIGET',$kdprodi);

$idhari1 = $_POST['hari1'];
$smarty->assign('HRGET',$idhari1);
$smarty->assign('HRGET0',$idhari1);
$smarty->assign('HRGET1',$idhari1);

$datajadruang1=getData("SELECT nm_ruangan,
MAX(DECODE ( kdjam , 08, nm_mk )) jam0,
MAX(DECODE ( kdjam , 09, nm_mk )) jam1,
MAX(DECODE ( kdjam , 10, nm_mk )) jam2,
MAX(DECODE ( kdjam , 11, nm_mk )) jam3,
MAX(DECODE ( kdjam , 12, nm_mk )) jam4,
MAX(DECODE ( kdjam , 13, nm_mk )) jam5,
MAX(DECODE ( kdjam , 14, nm_mk )) jam6,
MAX(DECODE ( kdjam , 15, nm_mk )) jam7,
MAX(DECODE ( kdjam , 16, nm_mk )) jam8,
MAX(DECODE ( kdjam , 17, nm_mk )) jam9,
MAX(DECODE ( kdjam , 18, nm_mk )) jam10,
MAX(DECODE ( kdjam , 19, nm_mk )) jam11
FROM
(select jam_mulai as kdjam,nm_ruangan,wm_concat(' '||kd_mata_kuliah||' - '||nm_mata_kuliah||' ('||nama_kelas||') '||kurikulum_mk.kredit_semester||' SKS<br><br>') as nm_mk
from kelas_mk left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
where program_studi.id_program_studi=$kdprodi and kelas_mk.id_semester=$smt1
and jadwal_kelas.id_jadwal_hari=$idhari1 and kurikulum_mk.tingkat_semester=$smt2
group by jam_mulai,nm_ruangan)
GROUP BY nm_ruangan");
$smarty->assign('T_JADRUANG1', $datajadruang1);

$datajadruang=getData("SELECT nm_ruangan,
MAX(DECODE ( kdjam , 08, nm_mk )) jam0,
MAX(DECODE ( kdjam , 09, nm_mk )) jam1,
MAX(DECODE ( kdjam , 10, nm_mk )) jam2,
MAX(DECODE ( kdjam , 11, nm_mk )) jam3,
MAX(DECODE ( kdjam , 12, nm_mk )) jam4,
MAX(DECODE ( kdjam , 13, nm_mk )) jam5,
MAX(DECODE ( kdjam , 14, nm_mk )) jam6,
MAX(DECODE ( kdjam , 15, nm_mk )) jam7,
MAX(DECODE ( kdjam , 16, nm_mk )) jam8,
MAX(DECODE ( kdjam , 17, nm_mk )) jam9,
MAX(DECODE ( kdjam , 18, nm_mk )) jam10,
MAX(DECODE ( kdjam , 19, nm_mk )) jam11
FROM
(
select kdjam,nm_ruangan||'<br><br>(<font color=\"blue\"><b>'||kapasitas_ruangan||'</b></font>)' as nm_ruangan,
wm_concat(' '||kd_mata_kuliah||' - '||nm_mata_kuliah||' ('||nama_kelas||') '||kredit_semester||' SKS ('||tot||')<br><br>') as nm_mk
from
(
select kdjam,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kredit_semester,kapasitas_ruangan,
case when (kapasitas_ruangan-jml)<=2 then '<font color=\"red\"><b>'||coalesce(jml,0)||'</b></font>' else '<font color=\"green\"><b>'||coalesce(jml,0)||'</b></font>' end as tot
from
(
(
select kelas_mk.id_kelas_mk,jam_mulai as kdjam,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,kapasitas_ruangan
from kelas_mk left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester=$smt1 and jadwal_kelas.id_jadwal_hari=$idhari1
group by kelas_mk.id_kelas_mk,jam_mulai,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,kapasitas_ruangan
) a
left join
(
select id_kelas_mk, count(*) as jml from pengambilan_mk where id_semester=$smt1 and status_apv_pengambilan_mk=1 and
id_kelas_mk in (select id_kelas_mk from kelas_mk where id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak))
group by id_kelas_mk
) b on a.id_kelas_mk=b.id_kelas_mk
)
group by kdjam,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kredit_semester,kapasitas_ruangan,jml
)
group by kdjam,nm_ruangan,kapasitas_ruangan
)
GROUP BY nm_ruangan");
$smarty->assign('T_JADRUANG', $datajadruang);

$plotjadruang=getData("SELECT nm_ruangan,
MAX(DECODE ( kdjam , 08, nm_mk )) jam0,
MAX(DECODE ( kdjam , 09, nm_mk )) jam1,
MAX(DECODE ( kdjam , 10, nm_mk )) jam2,
MAX(DECODE ( kdjam , 11, nm_mk )) jam3,
MAX(DECODE ( kdjam , 12, nm_mk )) jam4,
MAX(DECODE ( kdjam , 13, nm_mk )) jam5,
MAX(DECODE ( kdjam , 14, nm_mk )) jam6,
MAX(DECODE ( kdjam , 15, nm_mk )) jam7,
MAX(DECODE ( kdjam , 16, nm_mk )) jam8,
MAX(DECODE ( kdjam , 17, nm_mk )) jam9,
MAX(DECODE ( kdjam , 18, nm_mk )) jam10,
MAX(DECODE ( kdjam , 19, nm_mk )) jam11
FROM
(
select kdjam,nm_ruangan||'<br><br>(<font color=\"blue\"><b>'||kapasitas_ruangan||'</b></font>)' as nm_ruangan,
wm_concat(' '||kd_mata_kuliah||' - '||nm_mata_kuliah||' ('||nama_kelas||') '||kredit_semester||' SKS<br><br>') as nm_mk
from
(
select jam_mulai as kdjam,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,kapasitas_ruangan
from kelas_mk left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester=$smt1 and jadwal_kelas.id_jadwal_hari=$idhari1
group by jam_mulai,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,kapasitas_ruangan
)
group by kdjam,nm_ruangan,kapasitas_ruangan
)
GROUP BY nm_ruangan");
$smarty->assign('T_PLOTRUANG', $plotjadruang);
break;

//Cek jadwal ruang berdasarkan kelompok MK per group semester
case 'grup':
$smarty->assign('disp1','none');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','block');

$grup = $_POST['grup'];
$smarty->assign('GRUPGET',$grup);

if ($grup == 'Ganjil') {
	$kel = '(1, 3, 5, 7)';
} elseif ($grup == 'Genap'){
	$kel = '(2, 4, 6, 8)';
} else {
	$kel = '(1, 2, 3, 4, 5, 6, 7, 8)';
}

$smt2 = $_POST['smt2'];
$smarty->assign('SMTGET',$smt2);

$kdprodi1 = $_POST['kdprodi1'];
$smarty->assign('PRODIGET',$kdprodi1);

$idhari1 = $_POST['hari1'];
$smarty->assign('HRGET',$idhari1);
$smarty->assign('HRGET0',$idhari1);
$smarty->assign('HRGET1',$idhari1);

$datajadruang2=getData("SELECT nm_ruangan,
MAX(DECODE ( kdjam , 08, nm_mk )) jam0,
MAX(DECODE ( kdjam , 09, nm_mk )) jam1,
MAX(DECODE ( kdjam , 10, nm_mk )) jam2,
MAX(DECODE ( kdjam , 11, nm_mk )) jam3,
MAX(DECODE ( kdjam , 12, nm_mk )) jam4,
MAX(DECODE ( kdjam , 13, nm_mk )) jam5,
MAX(DECODE ( kdjam , 14, nm_mk )) jam6,
MAX(DECODE ( kdjam , 15, nm_mk )) jam7,
MAX(DECODE ( kdjam , 16, nm_mk )) jam8,
MAX(DECODE ( kdjam , 17, nm_mk )) jam9,
MAX(DECODE ( kdjam , 18, nm_mk )) jam10,
MAX(DECODE ( kdjam , 19, nm_mk )) jam11
FROM
(select jam_mulai as kdjam,nm_ruangan,wm_concat(' '||kd_mata_kuliah||' - '||nm_mata_kuliah||' ('||nama_kelas||') '||kurikulum_mk.kredit_semester||' SKS<br><br>') as nm_mk
from kelas_mk left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
where program_studi.id_program_studi=$kdprodi1 and kelas_mk.id_semester=$smt1
and jadwal_kelas.id_jadwal_hari=$idhari1 and kurikulum_mk.tingkat_semester in ".$kel."
group by jam_mulai,nm_ruangan)
GROUP BY nm_ruangan");
$smarty->assign('T_JADRUANG2', $datajadruang2);
/*
$datajadruang1=getData("SELECT nm_ruangan,
MAX(DECODE ( kdjam , 08, nm_mk )) jam0,
MAX(DECODE ( kdjam , 09, nm_mk )) jam1,
MAX(DECODE ( kdjam , 10, nm_mk )) jam2,
MAX(DECODE ( kdjam , 11, nm_mk )) jam3,
MAX(DECODE ( kdjam , 12, nm_mk )) jam4,
MAX(DECODE ( kdjam , 13, nm_mk )) jam5,
MAX(DECODE ( kdjam , 14, nm_mk )) jam6,
MAX(DECODE ( kdjam , 15, nm_mk )) jam7,
MAX(DECODE ( kdjam , 16, nm_mk )) jam8,
MAX(DECODE ( kdjam , 17, nm_mk )) jam9,
MAX(DECODE ( kdjam , 18, nm_mk )) jam10,
MAX(DECODE ( kdjam , 19, nm_mk )) jam11
FROM
(select jam_mulai as kdjam,nm_ruangan,wm_concat(' '||kd_mata_kuliah||' - '||nm_mata_kuliah||' ('||nama_kelas||') '||kurikulum_mk.kredit_semester||' SKS<br><br>') as nm_mk
from kelas_mk left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
where program_studi.id_program_studi=$kdprodi1 and kelas_mk.id_semester=$smt1
and jadwal_kelas.id_jadwal_hari=$idhari1 and kurikulum_mk.tingkat_semester=$smt2
group by jam_mulai,nm_ruangan)
GROUP BY nm_ruangan");
$smarty->assign('T_JADRUANG1', $datajadruang1);
*/
$datajadruang=getData("SELECT nm_ruangan,
MAX(DECODE ( kdjam , 08, nm_mk )) jam0,
MAX(DECODE ( kdjam , 09, nm_mk )) jam1,
MAX(DECODE ( kdjam , 10, nm_mk )) jam2,
MAX(DECODE ( kdjam , 11, nm_mk )) jam3,
MAX(DECODE ( kdjam , 12, nm_mk )) jam4,
MAX(DECODE ( kdjam , 13, nm_mk )) jam5,
MAX(DECODE ( kdjam , 14, nm_mk )) jam6,
MAX(DECODE ( kdjam , 15, nm_mk )) jam7,
MAX(DECODE ( kdjam , 16, nm_mk )) jam8,
MAX(DECODE ( kdjam , 17, nm_mk )) jam9,
MAX(DECODE ( kdjam , 18, nm_mk )) jam10,
MAX(DECODE ( kdjam , 19, nm_mk )) jam11
FROM
(
select kdjam,nm_ruangan||'<br><br>(<font color=\"blue\"><b>'||kapasitas_ruangan||'</b></font>)' as nm_ruangan,
wm_concat(' '||kd_mata_kuliah||' - '||nm_mata_kuliah||' ('||nama_kelas||') '||kredit_semester||' SKS ('||tot||'<br><br>)') as nm_mk
from
(
select kdjam,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kredit_semester,kapasitas_ruangan,
case when (kapasitas_ruangan-jml)<=2 then '<font color=\"red\"><b>'||coalesce(jml,0)||'</b></font>' else '<font color=\"green\"><b>'||coalesce(jml,0)||'</b></font>' end as tot
from
(
(
select kelas_mk.id_kelas_mk,jam_mulai as kdjam,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,kapasitas_ruangan
from kelas_mk left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester=$smt1 and jadwal_kelas.id_jadwal_hari=$idhari1
group by kelas_mk.id_kelas_mk,jam_mulai,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,kapasitas_ruangan
) a
left join
(
select id_kelas_mk, count(*) as jml from pengambilan_mk where id_semester=$smt1 and status_apv_pengambilan_mk=1 and
id_kelas_mk in (select id_kelas_mk from kelas_mk where id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak))
group by id_kelas_mk
) b on a.id_kelas_mk=b.id_kelas_mk
)
group by kdjam,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kredit_semester,kapasitas_ruangan,jml
)
group by kdjam,nm_ruangan,kapasitas_ruangan
)
GROUP BY nm_ruangan");
$smarty->assign('T_JADRUANG', $datajadruang);

$plotjadruang=getData("SELECT nm_ruangan,
MAX(DECODE ( kdjam , 08, nm_mk )) jam0,
MAX(DECODE ( kdjam , 09, nm_mk )) jam1,
MAX(DECODE ( kdjam , 10, nm_mk )) jam2,
MAX(DECODE ( kdjam , 11, nm_mk )) jam3,
MAX(DECODE ( kdjam , 12, nm_mk )) jam4,
MAX(DECODE ( kdjam , 13, nm_mk )) jam5,
MAX(DECODE ( kdjam , 14, nm_mk )) jam6,
MAX(DECODE ( kdjam , 15, nm_mk )) jam7,
MAX(DECODE ( kdjam , 16, nm_mk )) jam8,
MAX(DECODE ( kdjam , 17, nm_mk )) jam9,
MAX(DECODE ( kdjam , 18, nm_mk )) jam10,
MAX(DECODE ( kdjam , 19, nm_mk )) jam11
FROM
(
select kdjam,nm_ruangan||'<br><br>(<font color=\"blue\"><b>'||kapasitas_ruangan||'</b></font>)' as nm_ruangan,
wm_concat(' '||kd_mata_kuliah||' - '||nm_mata_kuliah||' ('||nama_kelas||') '||kredit_semester||' SKS<br><br>') as nm_mk
from
(
select jam_mulai as kdjam,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,kapasitas_ruangan
from kelas_mk left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
where program_studi.id_fakultas=$kdfak and kelas_mk.id_semester=$smt1 and jadwal_kelas.id_jadwal_hari=$idhari1
group by jam_mulai,nm_ruangan,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,kapasitas_ruangan
)
group by kdjam,nm_ruangan,kapasitas_ruangan
)
GROUP BY nm_ruangan");
$smarty->assign('T_PLOTRUANG', $plotjadruang);
break;
}
	$smarty->display('jadwal-ruang-psikologi.tpl');
	
}
?>
