<?php
/*
YAH 06/06/2011
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True'");
$id_semester=$smtaktif['ID_SEMESTER'];


if (isset($_GET['action'])=='detail') {
   		$id_mhs= $_GET['id'];
		
		$biodata_mahasiswa=getvar("SELECT (CASE WHEN CM.NIM_MHS IS NULL THEN 'BELUM REGISTRASI' ELSE CM.NIM_MHS END) AS NIM_MHS
			, NM_C_MHS, NM_JENJANG, NM_FAKULTAS, NM_PROGRAM_STUDI,
			NM_KOTA, TGL_LAHIR, TELP, MOBILE_MHS, JENIS_KELAMIN as KELAMIN_PENGGUNA, ALAMAT AS ALAMAT_MHS,
			SEKOLAH_ASAL_MHS, THN_LULUS_MHS, PRODI_S1, PRODI_S2, TGL_LULUS_S1, TGL_LULUS_S2, TGL_MASUK_S1, TGL_MASUK_S2
			FROM AUCC.CALON_MAHASISWA_BARU CM
			LEFT JOIN AUCC.MAHASISWA M ON CM.ID_C_MHS = M.ID_C_MHS 
            LEFT JOIN AUCC.KOTA K ON K.ID_KOTA LIKE ''||CM.ID_KOTA_LAHIR||''      
            LEFT JOIN AUCC.PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            LEFT JOIN AUCC.JENJANG J ON PS.ID_JENJANG = J.ID_JENJANG
            LEFT JOIN AUCC.FAKULTAS F ON PS.ID_FAKULTAS = F.ID_FAKULTAS
			LEFT JOIN AUCC.CALON_MAHASISWA_PASCA ON CALON_MAHASISWA_PASCA.ID_C_MHS = CM.ID_C_MHS
            WHERE CM.ID_C_MHS=$id_mhs");

$smarty->assign('biodata_mahasiswa', $biodata_mahasiswa);
		
}

$smarty->display('penerimaan-detail.tpl');    
?>
