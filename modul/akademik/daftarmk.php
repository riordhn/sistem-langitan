<?php
/*
YAH 06/06/2011
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$kdfak=$user->ID_FAKULTAS; 

$dataprodi=getData("select program_studi.id_program_studi,jenjang.nm_jenjang||' - '||program_studi.nm_program_studi as prodi 
from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where program_studi.id_fakultas=$kdfak and status_aktif_prodi = 1");
$smarty->assign('T_PRODI', $dataprodi);

$kdprodi=$_POST['kdprodi'];
$smarty->assign('DFT', $kdprodi);
if (!isset($_POST['kdprodi']) && empty($_POST['kdprodi'])){
	$kdprodi = 0;
}

// Added by Yudi Sulistya, 13-02-2013
// Get id_kurikulum_prodi when kdprodi is selected
if (isset($_POST['kdprodi']) && !empty($_POST['kdprodi'])) {
$datakur=getData("select distinct kurikulum_prodi.id_kurikulum_prodi,kurikulum_prodi.tahun_kurikulum||' - '||kurikulum.nm_kurikulum as nama from kurikulum_prodi 
left join kurikulum on kurikulum_prodi.id_kurikulum=kurikulum.id_kurikulum
where kurikulum_prodi.id_program_studi=$kdprodi");
$smarty->assign('T_KUR', $datakur);
} else {
$smarty->assign('T_KUR', '');
}
;

if (isset($_REQUEST['kirim'])=='view' && isset($_POST['kdprodi']) && isset($_POST['thkur'])) {
   		$kdprodi= $_POST['kdprodi'];
		$smarty->assign('DFT', $kdprodi);
		$idkur= $_POST['thkur'];
		$smarty->assign('ID_KUR', $idkur);


$mk=getData("select kmk.id_kurikulum_mk,mk1.kd_mata_kuliah,mk1.nm_mata_kuliah,
kmk.kredit_tatap_muka, kmk.kredit_praktikum,kmk.kredit_semester,kmk1.tingkat_semester,
nm_status_mk,nm_kelompok_mk,kurikulum_prodi.tahun_kurikulum, kmk.tingkat_semester,prasyarat_mk.id_prasyarat_mk,
case when trim(mk1.kd_mata_kuliah) in (select trim(kd_mata_kuliah) from mata_kuliah_lp3) then 'Valid' else 'Blm Valid' end as status_lp3,
wm_concat(mk.kd_mata_kuliah) as mksyarat
from kurikulum_mk kmk 
left join mata_kuliah mk1 on kmk.id_mata_kuliah=mk1.id_mata_kuliah 
left join status_mk on kmk.id_status_mk=status_mk.id_status_mk 
left join kelompok_mk on kmk.id_kelompok_mk=kelompok_mk.id_kelompok_mk 
left join kurikulum_prodi on kmk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
left join prasyarat_mk on kmk.id_kurikulum_mk=prasyarat_mk.id_kurikulum_mk
left join group_prasyarat_mk on prasyarat_mk.id_prasyarat_mk=group_prasyarat_mk.id_prasyarat_mk
left join kurikulum_mk kmk1 on group_prasyarat_mk.id_kurikulum_mk=kmk1.id_kurikulum_mk
left join mata_kuliah mk on kmk1.id_mata_kuliah=mk.id_mata_kuliah
where kmk.id_program_studi= '{$kdprodi}' and kmk.id_kurikulum_prodi= '{$idkur}' 
group by kmk.id_kurikulum_mk,mk1.kd_mata_kuliah,mk1.nm_mata_kuliah,
kmk.kredit_tatap_muka, kmk.kredit_praktikum,kmk.kredit_semester,kmk1.tingkat_semester,
nm_status_mk,nm_kelompok_mk,kurikulum_prodi.tahun_kurikulum, kmk.tingkat_semester,prasyarat_mk.id_prasyarat_mk,
case when mk1.kd_mata_kuliah in (select kd_mata_kuliah from mata_kuliah_lp3) then 'valid LP3' else '-' end 
");
		$smarty->assign('T_MK', $mk);

} else {

}
        $smarty->display('daftarmk.tpl');    
?>
