<?php
require('common.php');
require_once ('ociFunction.php');
		
		$periode = $_REQUEST['id_periode'];
		$fakultas = $_REQUEST['id_fakultas'];
		$absensi = getData("SELECT ID_PEMBAYARAN_WISUDA, NIM_MHS, NM_PENGGUNA, NM_JENJANG, NM_PROGRAM_STUDI, 
										NM_FAKULTAS, KETERANGAN_ABSENSI, ABSENSI_WISUDA
										FROM PEMBAYARAN_WISUDA
										JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBAYARAN_WISUDA.ID_MHS
										JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PEMBAYARAN_WISUDA.ID_PERIODE_WISUDA
										JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
								 		JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
								 		JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
								 		JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
										WHERE PERIODE_WISUDA.ID_TARIF_WISUDA = '$periode' AND 
										(PROGRAM_STUDI.ID_JENJANG <> 9 OR PROGRAM_STUDI.ID_JENJANG <> 10) AND FAKULTAS.ID_FAKULTAS = '$fakultas'
										ORDER BY FAKULTAS.ID_FAKULTAS, NM_JENJANG, NM_PROGRAM_STUDI, MAHASISWA.NIM_MHS");

//ob_start();
header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=Wisuda".date('Y-m-d').".xls");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data";
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #1d5700;
    }
    td{
        text-align: left;
    }
</style>
<table cellpadding="3" cellspacing="3" border="1">
    <thead>
        <tr>
            <td class="header_text">NIM</td>
            <td class="header_text">NAMA</td>
            <td class="header_text">JENJANG</td>
            <td class="header_text">PROGRAM STUDI</td>
            <td class="header_text">FAKULTAS</td>
			<td class="header_text">ABSENSI WISUDA</td>
			<td class="header_text">KETERANGAN</td>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($absensi as $data) {
			if($data['ABSENSI_WISUDA'] == 1){
				$wisuda = 'Ikut Wisuda';
			}else{
				$wisuda = 'Tidak Ikut Wisuda';
			}
            echo
            '<tr>
                <td style>`'.$data['NIM_MHS'].'</td>
                <td>'.$data['NM_PENGGUNA'].'</td>
                <td>'.$data['NM_JENJANG'].'</td>
                <td>'.$data['NM_PROGRAM_STUDI'].'</td>
                <td>'.$data['NM_FAKULTAS'].'</td>
				<td>'.$wisuda.'</td>
				<td>'.$data['KETERANGAN_ABSENSI'].'</td>
            </tr>';
        }
        ?>
    </tbody>
</table>

<?php
/*$filename = 'wisuda_unair' . date('Y-m-d');
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=$filename");
header("Pragma: ");
header("Cache-Control: ");
ob_flush();*/
?>
