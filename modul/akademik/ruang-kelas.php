<?php
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');

$id_pengguna= $user->ID_PENGGUNA;

$kdfak=$user->ID_FAKULTAS;
$kdprodi=$user->ID_PROGRAM_STUDI;

if ($kdprodi == 22.8)  // Memastikan tidak masuk if
{
	$gedung=getData("select * from gedung where id_gedung in(63)");
}
else
{
	$gedung=getData("select * from gedung where id_fakultas=$kdfak");
}

$smarty->assign('TGEDUNG', $gedung);

$gedung1=getvar("select id_gedung from gedung where id_fakultas=$kdfak and ROWNUM=1");
$smarty->assign('gedpil', $gedung1['ID_GEDUNG']);

$status = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'tampil';

switch($status) 
{
	case 'add':
		 // pilih
		$nm_ruang = $_POST['nm_ruang'];
		$kapasitas = $_POST['kapasitas'];
		$kap_ujian = $_POST['kap_ujian'];
		$gedung = $_POST['gedung'];
		$type_ruang = $_POST['type_ruang'];

		if ($kap_ujian > $kapasitas) 
		{
			echo '<script>alert("Kapasitas ruang untuk ujian melebihi kapasitas maksimal ruangan")</script>';
		} 
		else 
		{
			tambahdata("ruangan","id_gedung,nm_ruangan,kapasitas_ruangan,kapasitas_ujian,tipe_ruangan","$gedung,'$nm_ruang',$kapasitas,'$kap_ujian','$type_ruang'");
		}
		
		if ($kdprodi == 22.8)  // memastikan tidak masuk sini
		{
			$ruangkelas=getData("select ruangan.id_ruangan,ruangan.nm_ruangan,gedung.nm_gedung,ruangan.kapasitas_ruangan,ruangan.kapasitas_ujian,ruangan.tipe_ruangan,
				ruangan.id_penanggung_jawab from ruangan left join gedung on ruangan.id_gedung=gedung.id_gedung 
				where gedung.id_gedung in(63,70) order by nm_ruangan");
		}
		else
		{
			$ruangkelas=getData("select ruangan.id_ruangan,ruangan.nm_ruangan,gedung.nm_gedung,ruangan.kapasitas_ruangan,ruangan.kapasitas_ujian,ruangan.tipe_ruangan,
			ruangan.id_penanggung_jawab from ruangan left join gedung on ruangan.id_gedung=gedung.id_gedung where gedung.id_fakultas=$kdfak
			order by ruangan.nm_ruangan");
		}

		$smarty->assign('T_RK', $ruangkelas);
		$smarty->display('ruang-kelas.tpl');
	break;

	case 'update':
		 // pilih
		$nm_ruang = $_POST['nm_ruang'];
		$kapasitas = $_POST['kapasitas'];
		$kap_ujian = $_POST['kap_ujian'];
		$gedung = $_POST['gedung'];
		$id_ruangan = $_POST['id_ruangan'];
		$type_ruang = $_POST['type_ruang'];

		if ($kap_ujian > $kapasitas) 
		{
			echo '<script>alert("Kapasitas ruang untuk ujian melebihi kapasitas maksimal ruangan")</script>';
		} 
		else 
		{
			UpdateData("update ruangan set id_gedung='$gedung',nm_ruangan='$nm_ruang',kapasitas_ruangan='$kapasitas',kapasitas_ujian='$kap_ujian',tipe_ruangan='$type_ruang' where id_ruangan=$id_ruangan");
		}
		
		if ($kdprodi == 22.8)
		{
			$ruangkelas=getData("select ruangan.id_ruangan,ruangan.nm_ruangan,gedung.nm_gedung,ruangan.kapasitas_ruangan,ruangan.kapasitas_ujian,ruangan.tipe_ruangan,
			ruangan.id_penanggung_jawab from ruangan left join gedung on ruangan.id_gedung=gedung.id_gedung 
			where gedung.id_gedung in(63,70) order by nm_ruangan");
		}
		else
		{
			$ruangkelas=getData("select ruangan.id_ruangan,ruangan.nm_ruangan,gedung.nm_gedung,ruangan.kapasitas_ruangan,ruangan.kapasitas_ujian,ruangan.tipe_ruangan,
			ruangan.id_penanggung_jawab from ruangan left join gedung on ruangan.id_gedung=gedung.id_gedung where gedung.id_fakultas=$kdfak
			order by ruangan.nm_ruangan");
		}
		$smarty->assign('T_RK', $ruangkelas);
		$smarty->display('ruang-kelas.tpl');
	break;

	case 'del':
		$ruang = $_GET['ruang'];

		deleteData("delete from ruangan where id_ruangan='$ruang'");

		if ($kdprodi == 22.8)
		{
			$ruangkelas=getData("select ruangan.id_ruangan,ruangan.nm_ruangan,gedung.nm_gedung,ruangan.kapasitas_ruangan,ruangan.kapasitas_ujian,ruangan.tipe_ruangan,
			ruangan.id_penanggung_jawab from ruangan left join gedung on ruangan.id_gedung=gedung.id_gedung 
			where gedung.id_gedung in(63,70) order by nm_ruangan");
		}
		else
		{
			$ruangkelas=getData("select ruangan.id_ruangan,ruangan.nm_ruangan,gedung.nm_gedung,ruangan.kapasitas_ruangan,ruangan.kapasitas_ujian,ruangan.tipe_ruangan,
			ruangan.id_penanggung_jawab from ruangan left join gedung on ruangan.id_gedung=gedung.id_gedung where gedung.id_fakultas=$kdfak
			order by ruangan.nm_ruangan");
		}
		$smarty->assign('T_RK', $ruangkelas);
		$smarty->display('ruang-kelas.tpl');
	break;

	case 'tampil':
		$idruang = $_GET['id_ruang'];

		if (trim($idruang) != '') 
		{

			$ruangkelas1=getData("select ruangan.id_ruangan,ruangan.nm_ruangan,gedung.nm_gedung,ruangan.kapasitas_ruangan,ruangan.kapasitas_ujian,ruangan.tipe_ruangan,
			ruangan.id_penanggung_jawab from ruangan left join gedung on ruangan.id_gedung=gedung.id_gedung where ruangan.id_ruangan=$idruang");
			$smarty->assign('T_RK1', $ruangkelas1);

			$smarty->assign('disp1','none');
			$smarty->assign('disp2','none');
			$smarty->assign('disp3','block');
		}
		
		if ($kdprodi == 22.8)
		{
			$ruangkelas=getData("select ruangan.id_ruangan,ruangan.nm_ruangan,gedung.nm_gedung,ruangan.kapasitas_ruangan,ruangan.kapasitas_ujian,ruangan.tipe_ruangan,
			ruangan.id_penanggung_jawab from ruangan left join gedung on ruangan.id_gedung=gedung.id_gedung 
			where gedung.id_gedung in(63,70) order by nm_ruangan");
		}
		else
		{
			$ruangkelas=getData("select ruangan.id_ruangan,ruangan.nm_ruangan,gedung.nm_gedung,ruangan.kapasitas_ruangan,ruangan.kapasitas_ujian,ruangan.tipe_ruangan,
			ruangan.id_penanggung_jawab from ruangan left join gedung on ruangan.id_gedung=gedung.id_gedung where gedung.id_fakultas=$kdfak
			order by ruangan.nm_ruangan");
		}

		$smarty->assign('T_RK', $ruangkelas);
		$smarty->display('ruang-kelas.tpl');
break;
}

?>
