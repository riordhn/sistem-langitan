<?php
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');


$kdfak= $user->ID_FAKULTAS; 

$smtaktif=getvar("select ID_SEMESTER from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND status_aktif_semester='True'");

$smt1 = isSet($_POST['kdthnsmt']) ? $_POST['kdthnsmt'] : $smtaktif['ID_SEMESTER'];

$smarty->assign('SMT',$smt1);

$thnsmt=getData("select id_semester as id_semester, tahun_ajaran||'-'||nm_semester||'-'||group_semester as thn_smt from semester 
where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester in (select distinct thn_akademik_semester
from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND thn_akademik_semester>=EXTRACT(YEAR FROM sysdate)-2)
order by thn_akademik_semester desc, group_semester desc, nm_semester desc");
$smarty->assign('T_THNSMT', $thnsmt);


if($kdfak=='8'){
$nilai_masuk=getData("select s1.ID_KELAS_MK,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nama_kelas,pjma,
coalesce(pst,0) as pst,coalesce(ttluts,0) as ttluts,coalesce(ttluas,0)as ttluas from 
(select kelas_mk.ID_KELAS_MK,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nmkelas nama_kelas,
gelar_depan||nm_pengguna||', '||gelar_belakang as pjma
from kelas_mk
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk and pjmk_pengampu_mk=1
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join fst_kelas on kelas_mk.tipe_kelas_mk=fst_kelas.kdkelas
where kelas_mk.id_semester=$smt1 and program_studi.id_fakultas=$kdfak
)s1
left join 
(select id_kelas_mk,
sum(case when (besar_nilai_mk is not null and besar_nilai_mk >0 and (upper(substr(NM_KOMPONEN_MK,1,3))='UAS') or upper(nm_komponen_mk)='UJIAN AKHIR SEMESTER') then 1 else 0 end) as ttluas,
sum(case when (besar_nilai_mk is not null and besar_nilai_mk >0 and (upper(substr(NM_KOMPONEN_MK,1,3))='UTS') or upper(nm_komponen_mk)='UJIAN TENGAH SEMESTER') then 1 else 0 end) as ttluts
from nilai_mk 
left join komponen_mk on NILAI_MK.ID_KOMPONEN_MK=KOMPONEN_MK.ID_KOMPONEN_MK
where (upper(substr(NM_KOMPONEN_MK,1,3)) in ('UTS','UAS') or upper(nm_komponen_mk) like 'UJIAN AKHIR SEMESTER' or upper(nm_komponen_mk) like 'UJIAN TENGAH SEMESTER')
and id_kelas_mk in (select id_kelas_mk from kelas_mk where id_semester=$smt1 and id_program_studi in 
(select id_program_studi from PROGRAM_STUDI where id_fakultas=$kdfak))
group by id_kelas_mk
)s2 on s1.id_kelas_mk=s2.id_kelas_mk
left join
(select id_kelas_mk,count(id_mhs) as pst from PENGAMBILAN_MK
where id_semester=$smt1 and id_kelas_mk in (select id_kelas_mk from kelas_mk where id_semester=$smt1 and id_program_studi in 
(select id_program_studi from PROGRAM_STUDI where id_fakultas=$kdfak)) 
 group by id_kelas_mk)s3 on s1.id_kelas_mk=s3.id_kelas_mk
where ttluts>0
");
}
else{
$nilai_masuk=getData("select s1.ID_KELAS_MK,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nama_kelas,pjma,
coalesce(pst,0) as pst,coalesce(ttluts,0) as ttluts,coalesce(ttluas,0)as ttluas from 
(select kelas_mk.ID_KELAS_MK,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nama_kelas,
gelar_depan||nm_pengguna||', '||gelar_belakang as pjma
from kelas_mk
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk and pjmk_pengampu_mk=1
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
where kelas_mk.id_semester=$smt1 and program_studi.id_fakultas=$kdfak
)s1
left join 
(select id_kelas_mk,
sum(case when (besar_nilai_mk is not null and besar_nilai_mk >0 and (upper(substr(NM_KOMPONEN_MK,1,3))='UAS') or upper(nm_komponen_mk)='UJIAN AKHIR SEMESTER') then 1 else 0 end) as ttluas,
sum(case when (besar_nilai_mk is not null and besar_nilai_mk >0 and (upper(substr(NM_KOMPONEN_MK,1,3))='UTS') or upper(nm_komponen_mk)='UJIAN TENGAH SEMESTER') then 1 else 0 end) as ttluts
from nilai_mk 
left join komponen_mk on NILAI_MK.ID_KOMPONEN_MK=KOMPONEN_MK.ID_KOMPONEN_MK
where (upper(substr(NM_KOMPONEN_MK,1,3)) in ('UTS','UAS') or upper(nm_komponen_mk) like 'UJIAN AKHIR SEMESTER' or upper(nm_komponen_mk) like 'UJIAN TENGAH SEMESTER')
and id_kelas_mk in (select id_kelas_mk from kelas_mk where id_semester=$smt1 and id_program_studi in 
(select id_program_studi from PROGRAM_STUDI where id_fakultas=$kdfak))
group by id_kelas_mk
)s2 on s1.id_kelas_mk=s2.id_kelas_mk
left join
(select id_kelas_mk,count(id_mhs) as pst from PENGAMBILAN_MK
where id_semester=$smt1 and id_kelas_mk in (select id_kelas_mk from kelas_mk where id_semester=$smt1 and id_program_studi in 
(select id_program_studi from PROGRAM_STUDI where id_fakultas=$kdfak))
 group by id_kelas_mk)s3 on s1.id_kelas_mk=s3.id_kelas_mk
 where ttluts>0
");
}

$smarty->assign('NIL_MSK',$nilai_masuk);

if($kdfak=='8'){
$niluts_blm_masuk=getData("select s1.ID_KELAS_MK,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nama_kelas,pjma,
coalesce(pst,0) as pst,coalesce(ttluts,0) as ttluts,coalesce(ttluas,0)as ttluas from 
(select kelas_mk.ID_KELAS_MK,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nmkelas nama_kelas,
gelar_depan||nm_pengguna||', '||gelar_belakang as pjma
from kelas_mk
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk and pjmk_pengampu_mk=1
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join fst_kelas on kelas_mk.tipe_kelas_mk=fst_kelas.kdkelas
where kelas_mk.id_semester=$smt1 and program_studi.id_fakultas=$kdfak
)s1
left join 
(select id_kelas_mk,
sum(case when (besar_nilai_mk is not null and besar_nilai_mk >0 and (upper(substr(NM_KOMPONEN_MK,1,3))='UAS') or upper(nm_komponen_mk)='UJIAN AKHIR SEMESTER') then 1 else 0 end) as ttluas,
sum(case when (besar_nilai_mk is not null and besar_nilai_mk >0 and (upper(substr(NM_KOMPONEN_MK,1,3))='UTS') or upper(nm_komponen_mk)='UJIAN TENGAH SEMESTER') then 1 else 0 end) as ttluts
from nilai_mk 
left join komponen_mk on NILAI_MK.ID_KOMPONEN_MK=KOMPONEN_MK.ID_KOMPONEN_MK
where (upper(substr(NM_KOMPONEN_MK,1,3)) in ('UTS','UAS') or upper(nm_komponen_mk) like 'UJIAN AKHIR SEMESTER' or upper(nm_komponen_mk) like 'UJIAN TENGAH SEMESTER')
and id_kelas_mk in (select id_kelas_mk from kelas_mk where id_semester=$smt1 and id_program_studi in 
(select id_program_studi from PROGRAM_STUDI where id_fakultas=$kdfak))
group by id_kelas_mk
)s2 on s1.id_kelas_mk=s2.id_kelas_mk
left join
(select id_kelas_mk,count(id_mhs) as pst from PENGAMBILAN_MK
where id_semester=$smt1 and id_kelas_mk in (select id_kelas_mk from kelas_mk where id_semester=$smt1 and id_program_studi in 
(select id_program_studi from PROGRAM_STUDI where id_fakultas=$kdfak))
 group by id_kelas_mk)s3 on s1.id_kelas_mk=s3.id_kelas_mk
where ttluts=0 or ttluts is null
");
}
else{
$niluts_blm_masuk=getData("select s1.ID_KELAS_MK,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nama_kelas,pjma,
coalesce(pst,0) as pst,coalesce(ttluts,0) as ttluts,coalesce(ttluas,0)as ttluas from 
(select kelas_mk.ID_KELAS_MK,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nama_kelas,
gelar_depan||nm_pengguna||', '||gelar_belakang as pjma
from kelas_mk
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk and pjmk_pengampu_mk=1
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
where kelas_mk.id_semester=$smt1 and program_studi.id_fakultas=$kdfak
)s1
left join 
(select id_kelas_mk,
sum(case when (besar_nilai_mk is not null and besar_nilai_mk >0 and (upper(substr(NM_KOMPONEN_MK,1,3))='UAS') or upper(nm_komponen_mk)='UJIAN AKHIR SEMESTER') then 1 else 0 end) as ttluas,
sum(case when (besar_nilai_mk is not null and besar_nilai_mk >0 and (upper(substr(NM_KOMPONEN_MK,1,3))='UTS') or upper(nm_komponen_mk)='UJIAN TENGAH SEMESTER') then 1 else 0 end) as ttluts
from nilai_mk 
left join komponen_mk on NILAI_MK.ID_KOMPONEN_MK=KOMPONEN_MK.ID_KOMPONEN_MK
where (upper(substr(NM_KOMPONEN_MK,1,3)) in ('UTS','UAS') or upper(nm_komponen_mk) like 'UJIAN AKHIR SEMESTER' or upper(nm_komponen_mk) like 'UJIAN TENGAH SEMESTER')
and id_kelas_mk in (select id_kelas_mk from kelas_mk where id_semester=$smt1 and id_program_studi in 
(select id_program_studi from PROGRAM_STUDI where id_fakultas=$kdfak))
group by id_kelas_mk
)s2 on s1.id_kelas_mk=s2.id_kelas_mk
left join
(select id_kelas_mk,count(id_mhs) as pst from PENGAMBILAN_MK
where id_semester=$smt1 and id_kelas_mk in (select id_kelas_mk from kelas_mk where id_semester=$smt1 and id_program_studi in 
(select id_program_studi from PROGRAM_STUDI where id_fakultas=$kdfak))
 group by id_kelas_mk)s3 on s1.id_kelas_mk=s3.id_kelas_mk
 where ttluts=0 or ttluts is null
");
}
$smarty->assign('UTS_BLM_MSK',$niluts_blm_masuk);



$smarty->display('mon-nilai-uts.tpl');

?>
