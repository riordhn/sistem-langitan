<?php

$id_pt = $id_pt_user;

/*'{$GLOBALS[id_pt]}'*/

class akademik {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }
	
	function get_hasil_cari_mahasiswa($var) {
		$var_trim = trim($var);
        return $this->db->QueryToArray("
            SELECT P.NM_PENGGUNA,M.NIM_MHS,NO_UJIAN,F.NM_FAKULTAS,PS.NM_PROGRAM_STUDI FROM PENGGUNA P
            JOIN MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA
			LEFT JOIN CALON_MAHASISWA_BARU CMB ON M.ID_C_MHS = CMB.ID_C_MHS
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            JOIN FAKULTAS F ON PS.ID_FAKULTAS = F.ID_FAKULTAS
            WHERE M.NIM_MHS LIKE '%$var_trim%' OR UPPER(P.NM_PENGGUNA) LIKE UPPER('%$var_trim%') OR CMB.NO_UJIAN LIKE '%$var_trim%'
            ");
    }

    function rekap_nilai($nim) {
        return $this->db->QueryToArray("
            select
            a.*,
            b.thn_akademik_semester||'-'||b.nm_semester||' Nilai:'||b.nilai p1,
            c.thn_akademik_semester||'-'||c.nm_semester||' Nilai:'||c.nilai p2,
            d.thn_akademik_semester||'-'||d.nm_semester||' Nilai:'||d.nilai p3,
            e.thn_akademik_semester||'-'||e.nm_semester||' Nilai:'||e.nilai p4,
            f.thn_akademik_semester||'-'||f.nm_semester||' Nilai:'||f.nilai p5,
            g.thn_akademik_semester||'-'||g.nm_semester||' Nilai:'||g.nilai p6,
            '('''||b.id_pengambilan_mk||''','''||c.id_pengambilan_mk||''','''||d.id_pengambilan_mk||''','''||e.id_pengambilan_mk||''','''||f.id_pengambilan_mk||''','''||g.id_pengambilan_mk||''')' ids_pengambilan_mk,
            b.id_pengambilan_mk ipm1,
            c.id_pengambilan_mk ipm2,
            d.id_pengambilan_mk ipm3,
            e.id_pengambilan_mk ipm4,
            f.id_pengambilan_mk ipm5,
            g.id_pengambilan_mk ipm6,
            b.waktu w1,
            c.waktu w2,
            d.waktu w3,
            e.waktu w4,
            f.waktu w5,
            g.waktu w6
            from(
            select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p,a.status_hapus,a.id_pengambilan_mk
             from (
            select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
            row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
            thn_akademik_semester,nm_semester
            from pengambilan_mk a 
            left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
            left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
            left join semester sm on sm.id_semester=a.id_semester
            where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
            ) a
            left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
            left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
            left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
            left join mahasiswa m on a.id_mhs=m.id_mhs
            where  m.nim_mhs='{$nim}' and rangking=1) a
            left join
            (
            select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p,a.id_pengambilan_mk,n.id_pengambilan_mk waktu
             from (
            select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
            row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
            thn_akademik_semester,nm_semester
            from pengambilan_mk a 
            left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
            left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
            left join semester sm on sm.id_semester=a.id_semester
            where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
            ) a
            left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
            left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
            left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
            left join mahasiswa m on a.id_mhs=m.id_mhs
            left join (select distinct id_pengambilan_mk from log_update_nilai) n on n.id_pengambilan_mk=a.id_pengambilan_mk
            where  m.nim_mhs='{$nim}' and p=1
            ) b on a.nama=b.nama
            left join
            (
            select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p,a.id_pengambilan_mk,n.id_pengambilan_mk waktu
             from (
            select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
            row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
            thn_akademik_semester,nm_semester
            from pengambilan_mk a 
            left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
            left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
            left join semester sm on sm.id_semester=a.id_semester
            where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
            ) a
            left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
            left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
            left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
            left join mahasiswa m on a.id_mhs=m.id_mhs
            left join (select distinct id_pengambilan_mk from log_update_nilai) n on n.id_pengambilan_mk=a.id_pengambilan_mk
            where  m.nim_mhs='{$nim}' and p=2
            ) c on a.nama=c.nama
            left join
            (
            select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p,a.id_pengambilan_mk,n.id_pengambilan_mk waktu
             from (
            select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
            row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
            thn_akademik_semester,nm_semester
            from pengambilan_mk a 
            left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
            left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
            left join semester sm on sm.id_semester=a.id_semester
            where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
            ) a
            left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
            left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
            left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
            left join mahasiswa m on a.id_mhs=m.id_mhs
            left join (select distinct id_pengambilan_mk from log_update_nilai) n on n.id_pengambilan_mk=a.id_pengambilan_mk
            where  m.nim_mhs='{$nim}' and p=3
            ) d on a.nama=d.nama
            left join
            (
            select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p,a.id_pengambilan_mk,n.id_pengambilan_mk waktu
             from (
            select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
            row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
            thn_akademik_semester,nm_semester
            from pengambilan_mk a 
            left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
            left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
            left join semester sm on sm.id_semester=a.id_semester
            where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
            ) a
            left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
            left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
            left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
            left join mahasiswa m on a.id_mhs=m.id_mhs
            left join (select distinct id_pengambilan_mk from log_update_nilai) n on n.id_pengambilan_mk=a.id_pengambilan_mk
            where  m.nim_mhs='{$nim}' and p=4
            ) e on a.nama=e.nama
            left join
            (
            select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p,a.id_pengambilan_mk,n.id_pengambilan_mk waktu
             from (
            select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
            row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
            thn_akademik_semester,nm_semester
            from pengambilan_mk a 
            left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
            left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
            left join semester sm on sm.id_semester=a.id_semester
            where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
            ) a
            left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
            left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
            left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
            left join mahasiswa m on a.id_mhs=m.id_mhs
            left join (select distinct id_pengambilan_mk from log_update_nilai) n on n.id_pengambilan_mk=a.id_pengambilan_mk
            where  m.nim_mhs='{$nim}' and p=5
            ) f on a.nama=f.nama
            left join
            (
            select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p,a.id_pengambilan_mk,n.id_pengambilan_mk waktu
             from (
            select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
            row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
            thn_akademik_semester,nm_semester
            from pengambilan_mk a 
            left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
            left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
            left join semester sm on sm.id_semester=a.id_semester
            where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
            ) a
            left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
            left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
            left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
            left join mahasiswa m on a.id_mhs=m.id_mhs
            left join (select distinct id_pengambilan_mk from log_update_nilai) n on n.id_pengambilan_mk=a.id_pengambilan_mk
            where  m.nim_mhs='{$nim}' and p=6
            ) g on a.nama=g.nama
            order by a.smtr,a.nama
            ");
    }


    function ambil_ipk($nim) {

        return $this->db->QueryToArray("SELECT 
            sum(sks) as sks_total, sum((bobot*sks)) as bobot_total, round((sum((bobot*sks))/sum(sks)),2) as ipk
            from 
            (
            select e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
            select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
            from pengambilan_mk a 
            left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
            left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
            where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
            ) a
            left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
            left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
            left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
            left join mahasiswa m on a.id_mhs=m.id_mhs
            left join pengguna peng on peng.id_pengguna = m.id_pengguna and peng.id_perguruan_tinggi = '{$GLOBALS[id_pt]}'
            where rangking=1 and m.nim_mhs='{$nim}'
            )
            ");
    }
    
    

}

?>
