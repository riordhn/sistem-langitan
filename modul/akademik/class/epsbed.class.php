<?php
class epsbed {

	public $db;
	
	function __construct($db) {
		$this->db = $db;
	}
	
	
	function get_condition_akademik($id_fakultas, $id_jenjang, $id_prodi) {
        if ($id_fakultas != "" && $id_prodi != "" && $id_jenjang != "") {
            $query = " AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}' AND PROGRAM_STUDI.ID_PROGRAM_STUDI ='{$id_prodi}' 
						AND PROGRAM_STUDI.ID_JENJANG ='{$id_jenjang}'";
        } elseif ($id_fakultas != "" && $id_prodi != "") {
            $query = " AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}' AND PROGRAM_STUDI.ID_PROGRAM_STUDI ='{$id_prodi}'";
        } elseif ($id_fakultas != "" && $id_jenjang != "") {
            $query = " AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}' AND PROGRAM_STUDI.ID_JENJANG ='{$id_jenjang}'";
        } elseif ($id_prodi != "" && $id_jenjang != "") {
            $query = " AND PROGRAM_STUDI.ID_PROGRAM_STUDI ='{$id_prodi}' AND PROGRAM_STUDI.ID_JENJANG ='{$id_jenjang}'";
        } elseif ($id_prodi != "") {
            $query = " AND PROGRAM_STUDI.ID_PROGRAM_STUDI ='{$id_prodi}'";
        } elseif ($id_jenjang != "") {
            $query = " AND PROGRAM_STUDI.ID_JENJANG ='{$id_jenjang}'";
        } else if ($id_fakultas != "") {
            $query = " AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}' ";
        } else {
            $query = " ";
        }
        return $query;
    }

	
	function get_condition_trakm($id_semester) {
      	$this->db->Query("
            select case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,
			thn_akademik_semester 
			from semester where id_semester={$id_semester}
            ");
		return $this->db->FetchAssoc();
    }
	
	
	function fakultas() {
        return $this->db->QueryToArray("
            SELECT * FROM FAKULTAS ORDER BY ID_FAKULTAS ASC
            ");
    }
	
	
	function prodi_condition($id_fakultas, $id_jenjang) {
		
		$condition_akademik =  $this->get_condition_akademik($id_fakultas, $id_jenjang, '');
		
        return $this->db->QueryToArray("
            SELECT * FROM PROGRAM_STUDI WHERE PROGRAM_STUDI.STATUS_AKTIF_PRODI = 1 {$condition_akademik} ORDER BY NM_PROGRAM_STUDI ASC
            ");
    }
	
	
	function jenjang() {
        return $this->db->QueryToArray("
            SELECT * FROM JENJANG ORDER BY NM_JENJANG ASC
            ");
    }
	
	
	function jenjang_wisuda() {
        return $this->db->QueryToArray("
            SELECT * FROM JENJANG WHERE ID_JENJANG = 1 OR ID_JENJANG = 2 OR ID_JENJANG = 3 OR ID_JENJANG = 5 ORDER BY NM_JENJANG ASC
            ");
    }
	
	
	function angkatan() {
        return $this->db->QueryToArray("
            SELECT DISTINCT THN_ANGKATAN_MHS FROM MAHASISWA ORDER BY THN_ANGKATAN_MHS DESC
            ");
    }
	
	
	
	function semester() {
        return $this->db->QueryToArray("
            SELECT * FROM SEMESTER WHERE NM_SEMESTER = 'Ganjil' OR NM_SEMESTER = 'Genap' ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER
            ");
    }
	
	
	function mhsmhs($id_fakultas, $id_jenjang, $id_prodi, $id_semester) {
	
		$condition_akademik =  $this->get_condition_akademik($id_fakultas, $id_jenjang, $id_prodi);
		if($id_semester != ''){
			$condition_semester= " AND MAHASISWA.ID_SEMESTER_MASUK = '{$id_semester}'";
		}
		

		return $this->db->QueryToArray("SELECT MAHASISWA.NIM_MHS, (CASE WHEN GELAR_DEPAN IS NULL THEN '' ELSE GELAR_DEPAN || ' ' END) ||  NM_PENGGUNA || (CASE WHEN GELAR_BELAKANG IS NULL THEN '' ELSE ', ' || GELAR_BELAKANG END) AS NM_PENGGUNA, (CASE WHEN JENJANG.ID_JENJANG = 1 THEN 'C' WHEN JENJANG.ID_JENJANG = 5 THEN 'E' WHEN JENJANG.ID_JENJANG = 2 THEN 'B' WHEN JENJANG.ID_JENJANG = 3 THEN 'A' WHEN JENJANG.ID_JENJANG = 9 THEN 'J' WHEN JENJANG.ID_JENJANG = 10 THEN 'H' ELSE '' END) AS KD_JENJANG, NM_KOTA AS TMLAHIR, TO_CHAR(TGL_LAHIR_PENGGUNA, 'dd/mm/yyyy') AS TGL_LAHIR_PENGGUNA, (CASE WHEN KELAMIN_PENGGUNA = 1 THEN 'L' WHEN KELAMIN_PENGGUNA = 2 THEN 'P' ELSE '' END) AS KELAMIN_PENGGUNA, THN_ANGKATAN_MHS, THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER LIKE '%Ga%' THEN '1' WHEN NM_SEMESTER LIKE '%Ge%' THEN '2' ELSE '' END) AS THN_SEMESTER, (CASE WHEN JENJANG.ID_JENJANG = 1 THEN '4' WHEN JENJANG.ID_JENJANG = 5 THEN '3' WHEN JENJANG.ID_JENJANG = 2 THEN '2' WHEN JENJANG.ID_JENJANG = 3 THEN '3' WHEN JENJANG.ID_JENJANG = 9 THEN '2' WHEN JENJANG.ID_JENJANG = 10 THEN '2' ELSE '' END) AS BWS, TO_CHAR(TGL_TERDAFTAR_MHS, 'dd/mm/yyyy') AS TGL_TERDAFTAR_MHS, TO_CHAR(TGL_LULUS, 'dd/mm/yyyy') AS TGL_LULUS, EPSBED_MSPST.KDPSTMSPST
		FROM MAHASISWA
		LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
		LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
		LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
		LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
		LEFT JOIN KOTA ON KOTA.ID_KOTA = MAHASISWA.LAHIR_KOTA_MHS
		LEFT JOIN SEMESTER ON SEMESTER.ID_SEMESTER = MAHASISWA.ID_SEMESTER_MASUK
		LEFT JOIN ADMISI ON ADMISI.ID_MHS = MAHASISWA.ID_MHS AND STATUS_AKD_MHS = 4
		LEFT JOIN EPSBED_MSPST ON EPSBED_MSPST.ID_EPSBED_MSPST = PROGRAM_STUDI.ID_EPSBED_MSPST
		WHERE PROGRAM_STUDI.STATUS_AKTIF_PRODI = 1 {$condition_akademik} {$condition_semester}
		ORDER BY THN_ANGKATAN_MHS DESC, NIM_MHS ASC");
	
	}
	
	
	
		function trakm($id_fakultas, $id_jenjang, $id_prodi, $id_semester) {
	
		$condition_akademik =  $this->get_condition_akademik($id_fakultas, $id_jenjang, $id_prodi);
		
		if($id_semester != ''){
			$condition_semester= " AND MAHASISWA.ID_SEMESTER_MASUK = '{$id_semester}'";
			$condition_trakm = $this->get_condition_trakm($id_semester);
		}


		return $this->db->QueryToArray("
			select s1.id_mhs,s1.nim_mhs,upper(s1.nm_pengguna) as NM_PENGGUNA,
			coalesce(sks_sem,0) as sks_sem,round(coalesce(ips,0),2)as ips,
			coalesce(SKS_TOTAL_MHS,0) as SKS_TOTAL_MHS,round(coalesce(IPK_MHS,0),2)as IPK_MHS, kdjenmspst, kdpstmspst
			from
			(select id_mhs,nim_mhs,nm_pengguna,case when sum(bobot*kredit_semester)=0 then 0 else 
			round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips,
			case when id_fakultas=7 then sum(sksreal) else sum(kredit_semester) end as sks_sem , kdjenmspst, kdpstmspst
			from 
			(select id_mhs,nim_mhs,nm_pengguna,id_kurikulum_mk,id_fakultas,kredit_semester, sksreal,min(nilai_huruf) as nilai, max(bobot) as bobot, kdjenmspst, kdpstmspst
			from
			(select a.id_mhs,m.nim_mhs,pg.nm_pengguna,id_fakultas,a.id_kurikulum_mk, 
			case when (a.nilai_huruf = 'E' or a.nilai_huruf is null) 
			and d.status_mkta in (1,2) then 0
			else d.kredit_semester end as kredit_semester,d.kredit_semester as sksreal,
			case when a.nilai_huruf is null then 'E' else a.nilai_huruf end as nilai_huruf,
			case when e.nilai_standar_nilai is null then 0 else e.nilai_standar_nilai end as bobot, kdjenmspst, kdpstmspst
			from pengambilan_mk a
			left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
			left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
			left join mahasiswa m on a.id_mhs=m.id_mhs
			left join pengguna pg on m.id_pengguna=pg.id_pengguna 
			left join program_studi on m.id_program_studi=program_studi.id_program_studi
			left join semester s on a.id_semester=s.id_semester
			left join epsbed_mspst on epsbed_mspst.id_epsbed_mspst = program_studi.id_epsbed_mspst
			where status_aktif_prodi = 1 {$condition_akademik} and group_semester||thn_akademik_semester in
			(select group_semester||thn_akademik_semester from semester where id_Semester={$id_semester})
			and tipe_semester in ('UP','REG','RD') 
			and a.status_apv_pengambilan_mk='1' and a.status_hapus=0 
			and a.status_pengambilan_mk !=0)
			group by id_mhs, nim_mhs,nm_pengguna,id_kurikulum_mk, id_fakultas, kredit_semester, sksreal, kdjenmspst, kdpstmspst
			)
			group by id_mhs,nim_mhs,nm_pengguna,id_fakultas, kdjenmspst, kdpstmspst)s1
			left join
			(select id_mhs, sum(kredit_semester) as SKS_TOTAL_MHS, 
			round((sum((kredit_semester * nilai_standar_nilai)) / sum(kredit_semester)), 2) as IPK_MHS
			from 
			(select a.id_mhs, c.kd_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai,
			case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,
			row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by nilai_huruf) rangking
			from pengambilan_mk a
			join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
			join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
			join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
			join semester smt on a.id_semester=smt.id_semester
			join mahasiswa m on a.id_mhs=m.id_mhs
			join program_studi on program_studi.id_program_studi = m.id_program_studi
			where a.status_apv_pengambilan_mk = 1 and (a.nilai_huruf<>'E' or a.nilai_huruf<>null) 
			{$condition_akademik} and a.status_hapus=0 
			and a.status_pengambilan_mk !=0)
			where tahun<='$condition_trakm[TAHUN]' and rangking=1
			group by id_mhs) s2 on s1.id_mhs=s2.id_mhs
			order by nim_mhs");
	
	}




	function trnlm($id_fakultas, $id_jenjang, $id_prodi, $id_semester) {
	
		$condition_akademik =  $this->get_condition_akademik($id_fakultas, $id_jenjang, $id_prodi);
		
		if($id_semester != ''){
			$condition_semester= " AND MAHASISWA.ID_SEMESTER_MASUK = '{$id_semester}'";
			$condition_trakm = $this->get_condition_trakm($id_semester);
		}

		return $this->db->QueryToArray("
			select MAHASISWA.nim_mhs,pengguna.nm_pengguna,mahasiswa.id_program_studi,nm_program_studi,
			MATA_KULIAH.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,KURIKULUM_MK.kredit_semester , kdjenmspst, kdpstmspst, nilai_huruf, nilai_standar_nilai
			from PENGAMBILAN_MK
			left join mahasiswa on pengambilan_mk.id_mhs=MAHASISWA.ID_MHS
			left join pengguna on MAHASISWA.id_pengguna=PENGGUNA.id_pengguna
			left join program_studi on MAHASISWA.id_program_studi=PROGRAM_STUDI.id_program_studi
			left join kelas_mk on PENGAMBILAN_MK.id_kelas_mk=KELAS_MK.id_kelas_mk
			left join NAMA_KELAS on KELAS_MK.no_kelas_mk=nama_kelas.id_nama_kelas
			left join kurikulum_mk on kelas_mk.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk
			left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
			left join standar_nilai on PENGAMBILAN_MK.nilai_huruf=standar_nilai.nm_standar_nilai
			left join epsbed_mspst on epsbed_mspst.id_epsbed_mspst = program_studi.id_epsbed_mspst
			where PENGAMBILAN_MK.id_semester={$id_semester} {$condition_akademik} and status_apv_pengambilan_mk=1
			order by MAHASISWA.nim_mhs,MATA_KULIAH.kd_mata_kuliah");
	
	}
	
	
	
	function mskmk($id_fakultas, $id_jenjang, $id_prodi) {
	
		$condition_akademik =  $this->get_condition_akademik($id_fakultas, $id_jenjang, $id_prodi);


		return $this->db->QueryToArray("
			select kdjenmspst, kdpstmspst, kd_mata_kuliah, nm_mata_kuliah, kurikulum_mk.kredit_semester, kredit_tatap_muka, kredit_praktikum, tahun_kurikulum
			from kurikulum_mk
			left join program_studi on program_studi.id_program_studi = kurikulum_mk.id_program_studi
			left join kurikulum_prodi on kurikulum_prodi.id_kurikulum_prodi = kurikulum_mk.id_kurikulum_prodi
			left join kurikulum on kurikulum.id_kurikulum = kurikulum_prodi.id_kurikulum
			left join mata_kuliah on mata_kuliah.id_mata_kuliah = kurikulum_mk.id_mata_kuliah
			left join epsbed_mspst on epsbed_mspst.id_epsbed_mspst = program_studi.id_epsbed_mspst
			where program_studi.status_aktif_prodi = 1 {$condition_akademik}
			order by tahun_kurikulum, kd_mata_kuliah");
	
	}


	function trakd($id_fakultas, $id_jenjang, $id_prodi) {
	
		$condition_akademik =  $this->get_condition_akademik($id_fakultas, $id_jenjang, $id_prodi);


		return $this->db->QueryToArray("
			select  kd_mata_kuliah,nm_mata_kuliah,
					kurikulum_mk.kredit_semester,nama_kelas,sum(case when presensi_mkdos.id_dosen is null then 0 else 1 end) as masuk,
					kurikulum_mk.kredit_semester*sum(case when presensi_mkdos.id_dosen is null then 0 else 1 end) as total, jumlah_pertemuan_kelas_mk, dosen.nidn_dosen,
					kdjenmspst, kdpstmspst, (thn_akademik_semester || (case when nm_semester = 'Ganjil' then '1' else '2' end)) tahun_semester
					from kelas_mk
					left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
					left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
					left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
					left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
					left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
					left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
					left join presensi_kelas on kelas_mk.id_kelas_mk=presensi_kelas.id_kelas_mk
					left join presensi_mkdos on presensi_kelas.id_presensi_kelas=presensi_mkdos.id_presensi_kelas and dosen.id_dosen=presensi_mkdos.id_dosen
					left join program_studi on program_studi.id_program_studi = kelas_mk.id_program_studi
					left join epsbed_mspst on epsbed_mspst.id_epsbed_mspst = program_studi.id_epsbed_mspst
					left join semester on semester.id_semester = kelas_mk.id_semester
					where program_studi.status_aktif_prodi = 1 {$condition_akademik}
					group by kd_mata_kuliah,nm_mata_kuliah,
					kurikulum_mk.kredit_semester,nama_kelas, jumlah_pertemuan_kelas_mk, nidn_dosen, kdjenmspst, kdpstmspst, 
					(thn_akademik_semester || (case when nm_semester = 'Ganjil' then '1' else '2' end))
					order by (thn_akademik_semester || (case when nm_semester = 'Ganjil' then '1' else '2' end)),kd_mata_kuliah");
	
	}
	
	
	function trkap($id_fakultas, $id_jenjang, $id_prodi) {
	
		$condition_akademik =  $this->get_condition_akademik($id_fakultas, $id_jenjang, $id_prodi);


		return $this->db->QueryToArray("
			select epsbed_mspst.kdjenmspst, epsbed_mspst.kdpstmspst, PRODI_DAYATAMPUNG.total, NM_PROGRAM_STUDI,
			(thn_akademik_semester || (case when nm_semester = 'Ganjil' then '1' else '2' end)) tahun_semester, count(*) as jml_peminat, jml_diterima, jml_du
			from calon_mahasiswa_baru
			left join program_studi on (program_studi.id_program_studi = calon_mahasiswa_baru.id_pilihan_1 
				or program_studi.id_program_studi = calon_mahasiswa_baru.id_pilihan_2 or program_studi.id_program_studi = calon_mahasiswa_baru.id_pilihan_3 
				or program_studi.id_program_studi = calon_mahasiswa_baru.id_pilihan_4)
			left join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
			left join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
			left join epsbed_mspst on epsbed_mspst.id_epsbed_mspst = program_studi.id_epsbed_mspst
			left join penerimaan on penerimaan.id_penerimaan = calon_mahasiswa_baru.id_penerimaan
			left join semester on semester.id_semester = penerimaan.id_semester
			left join PRODI_DAYATAMPUNG on PRODI_DAYATAMPUNG.id_program_studi = program_studi.id_program_studi
			left join (
				select kdjenmspst, kdpstmspst, (thn_akademik_semester || (case when nm_semester = 'Ganjil' then '1' else '2' end)) tahun_semester, count(*) as jml_diterima
				from calon_mahasiswa_baru
				join program_studi on program_studi.id_program_studi = calon_mahasiswa_baru.id_program_studi
				join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
				join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
				left join epsbed_mspst on epsbed_mspst.id_epsbed_mspst = program_studi.id_epsbed_mspst
				left join penerimaan on penerimaan.id_penerimaan = calon_mahasiswa_baru.id_penerimaan
				left join semester on semester.id_semester = penerimaan.id_semester
				where program_studi.status_aktif_prodi = 1 {$condition_akademik}
				group by kdjenmspst, kdpstmspst, (thn_akademik_semester || (case when nm_semester = 'Ganjil' then '1' else '2' end)) 
			) a on a.kdjenmspst = epsbed_mspst.kdjenmspst and a.kdpstmspst = epsbed_mspst.kdpstmspst 
				and a.tahun_semester = (thn_akademik_semester || (case when nm_semester = 'Ganjil' then '1' else '2' end)) 
			left join (
				select kdjenmspst, kdpstmspst, (thn_akademik_semester || (case when nm_semester = 'Ganjil' then '1' else '2' end)) tahun_semester, count(*) as jml_du
				from calon_mahasiswa_baru
				join program_studi on program_studi.id_program_studi = calon_mahasiswa_baru.id_program_studi
				join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
				join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
				left join epsbed_mspst on epsbed_mspst.id_epsbed_mspst = program_studi.id_epsbed_mspst
				left join penerimaan on penerimaan.id_penerimaan = calon_mahasiswa_baru.id_penerimaan
				left join semester on semester.id_semester = penerimaan.id_semester
				where program_studi.status_aktif_prodi = 1 and tgl_verifikasi_pendidikan is not null {$condition_akademik}
				group by kdjenmspst, kdpstmspst, (thn_akademik_semester || (case when nm_semester = 'Ganjil' then '1' else '2' end)) 
				) b on b.kdjenmspst = epsbed_mspst.kdjenmspst and b.kdpstmspst = epsbed_mspst.kdpstmspst 
					and b.tahun_semester = (thn_akademik_semester || (case when nm_semester = 'Ganjil' then '1' else '2' end)) 
			where program_studi.status_aktif_prodi = 1 and is_alih_jenis = 0 {$condition_akademik}
			group by epsbed_mspst.kdjenmspst, epsbed_mspst.kdpstmspst, PRODI_DAYATAMPUNG.total, NM_PROGRAM_STUDI, 
					(thn_akademik_semester || (case when nm_semester = 'Ganjil' then '1' else '2' end)), jml_diterima, jml_du
			order by (thn_akademik_semester || (case when nm_semester = 'Ganjil' then '1' else '2' end)), epsbed_mspst.kdjenmspst, epsbed_mspst.kdpstmspst");
	
	}
	
}
?>
