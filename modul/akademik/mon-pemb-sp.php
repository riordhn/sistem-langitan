<?php
require('common.php');
require_once ('ociFunction.php');
//require('../../../config.php');
$db = new MyOracle();

error_reporting (E_ALL & ~E_NOTICE);
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$id_pengguna= $user->ID_PENGGUNA;

$kdfak=$user->ID_FAKULTAS;
$smarty->assign('kdfakul',$kdfak);


$smtaktif=getvar("select id_semester from semester where status_aktif_sp='True'");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);

$smt1 = isSet($_POST['smt']) ? $_POST['smt'] : $smtaktif['ID_SEMESTER'];


if ($_GET['action']=='detail')
{
	$id_prodi=$_GET['idprodi'];
	$st=$_GET['st'];
	//echo $id_prodi;
	//echo $st;
	
	if ($st==1)
	{
	 $detailprodi=getData("select * from (
						select s3.id_mhs,s3.nim_mhs,s3.nm_pengguna,s3.prodi,s3.id_program_studi,s3.sks_ambil,nm_jalur,besar_biaya_sp,
						s3.sp_biaya,coalesce(sum(besar_biaya),0) as terbayar,'LUNAS' as status from (
						select id_mhs,nim_mhs,nm_pengguna,prodi,id_program_studi,sum(kredit_semester) as sks_ambil,nm_jalur,
						wm_concat(distinct besar_biaya_sp) as besar_biaya_sp,
						sum(besar_biaya_sp*kredit_semester) as sp_biaya from
						(
						select s1.id_mhs,s1.nim_mhs,s1.nm_pengguna,s1.prodi,s1.id_program_studi,s1.kredit_semester,nm_jalur,
						case when s1.id_fakultas=10 and s2.pst<11 then round(1500000/(s2.pst*s1.kredit_semester),-2) else s1.besar_biaya_sp end as besar_biaya_sp,
						s1.kd_mata_kuliah,s2.pst
						from
						(
						select pengambilan_mk.id_mhs,mahasiswa.nim_mhs,nm_pengguna,
						nm_jenjang||'-'||nm_program_studi as prodi,mahasiswa.id_program_studi,
						kurikulum_mk.kredit_semester, besar_biaya_sp,kd_mata_kuliah,nm_jalur,id_fakultas
						from pengambilan_mk
						left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
						left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
						left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
						left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
						left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
						left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
						left join jalur_mahasiswa on jalur_mahasiswa.id_mhs = mahasiswa.id_mhs
						left join jalur on jalur_mahasiswa.id_jalur=jalur.id_jalur
						left join biaya_sp on biaya_sp.id_jalur = jalur_mahasiswa.id_jalur and biaya_sp.id_program_studi = mahasiswa.id_program_studi
						and biaya_sp.id_semester = pengambilan_mk.id_semester
						where pengambilan_mk.id_semester=$smt1 and id_fakultas=$kdfak and jalur_mahasiswa.id_jalur_aktif = 1 and status_apv_pengambilan_mk=1
						and mahasiswa.id_program_studi=$id_prodi
						)s1
						left join
						(select kd_mata_kuliah,count(id_mhs) as pst from pengambilan_mk
						left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
						left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
						where id_semester=$smt1 and id_mhs in (select id_mhs from mahasiswa where id_program_studi in
						(select id_program_studi from program_studi where id_fakultas=$kdfak)) and status_apv_pengambilan_mk=1
						group by kd_mata_kuliah)s2 on s1.kd_mata_kuliah=s2.kd_mata_kuliah
						)
						group by id_mhs, nim_mhs, nm_pengguna, prodi, id_program_studi, nm_jalur)s3
						left join pembayaran_sp on s3.id_mhs=pembayaran_sp.id_mhs and id_status_pembayaran=1 and pembayaran_sp.id_semester=$smt1
						group by s3.id_mhs, s3.nim_mhs, s3.nm_pengguna, s3.prodi, s3.id_program_studi, s3.sks_ambil, nm_jalur, besar_biaya_sp, s3.sp_biaya)
						where terbayar=sp_biaya");
	}
	if ($st==2)
	{
	 $detailprodi=getData("select * from (
						select s3.id_mhs,s3.nim_mhs,s3.nm_pengguna,s3.prodi,s3.id_program_studi,s3.sks_ambil,nm_jalur,besar_biaya_sp,
						s3.sp_biaya,coalesce(sum(besar_biaya),0) as terbayar,'BLM BAYAR' as status from (
						select id_mhs,nim_mhs,nm_pengguna,prodi,id_program_studi,sum(kredit_semester) as sks_ambil,nm_jalur,
						wm_concat(distinct besar_biaya_sp) as besar_biaya_sp,
						sum(besar_biaya_sp*kredit_semester) as sp_biaya from
						(
						select s1.id_mhs,s1.nim_mhs,s1.nm_pengguna,s1.prodi,s1.id_program_studi,s1.kredit_semester,nm_jalur,
						case when s1.id_fakultas=10 and s2.pst<11 then round(1500000/(s2.pst*s1.kredit_semester),-2) else s1.besar_biaya_sp end as besar_biaya_sp,
						s1.kd_mata_kuliah,s2.pst
						from
						(
						select pengambilan_mk.id_mhs,mahasiswa.nim_mhs,nm_pengguna,
						nm_jenjang||'-'||nm_program_studi as prodi,mahasiswa.id_program_studi,
						kurikulum_mk.kredit_semester, besar_biaya_sp,kd_mata_kuliah,nm_jalur,id_fakultas
						from pengambilan_mk
						left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
						left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
						left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
						left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
						left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
						left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
						left join jalur_mahasiswa on jalur_mahasiswa.id_mhs = mahasiswa.id_mhs
						left join jalur on jalur_mahasiswa.id_jalur=jalur.id_jalur
						left join biaya_sp on biaya_sp.id_jalur = jalur_mahasiswa.id_jalur and biaya_sp.id_program_studi = mahasiswa.id_program_studi
						and biaya_sp.id_semester = pengambilan_mk.id_semester
						where pengambilan_mk.id_semester=$smt1 and id_fakultas=$kdfak and jalur_mahasiswa.id_jalur_aktif = 1 and status_apv_pengambilan_mk=1
						and mahasiswa.id_program_studi=$id_prodi
						)s1
						left join
						(select kd_mata_kuliah,count(id_mhs) as pst from pengambilan_mk
						left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
						left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
						where id_semester=$smt1 and id_mhs in (select id_mhs from mahasiswa where id_program_studi in
						(select id_program_studi from program_studi where id_fakultas=$kdfak)) and status_apv_pengambilan_mk=1
						group by kd_mata_kuliah)s2 on s1.kd_mata_kuliah=s2.kd_mata_kuliah
						)
						group by id_mhs, nim_mhs, nm_pengguna, prodi, id_program_studi, nm_jalur)s3
						left join pembayaran_sp on s3.id_mhs=pembayaran_sp.id_mhs and id_status_pembayaran=1 and pembayaran_sp.id_semester=$smt1
						group by s3.id_mhs, s3.nim_mhs, s3.nm_pengguna, s3.prodi, s3.id_program_studi, s3.sks_ambil, nm_jalur, besar_biaya_sp, s3.sp_biaya)
						where terbayar=0");
	}
	if ($st==3)
	{
	 $detailprodi=getData("select * from (
						select s3.id_mhs,s3.nim_mhs,s3.nm_pengguna,s3.prodi,s3.id_program_studi,s3.sks_ambil,nm_jalur,besar_biaya_sp,
						s3.sp_biaya,coalesce(sum(besar_biaya),0) as terbayar,'KURANG' as status from (
						select id_mhs,nim_mhs,nm_pengguna,prodi,id_program_studi,sum(kredit_semester) as sks_ambil,nm_jalur,
						wm_concat(distinct besar_biaya_sp) as besar_biaya_sp,
						sum(besar_biaya_sp*kredit_semester) as sp_biaya from
						(
						select s1.id_mhs,s1.nim_mhs,s1.nm_pengguna,s1.prodi,s1.id_program_studi,s1.kredit_semester,nm_jalur,
						case when s1.id_fakultas=10 and s2.pst<11 then round(1500000/(s2.pst*s1.kredit_semester),-2) else s1.besar_biaya_sp end as besar_biaya_sp,
						s1.kd_mata_kuliah,s2.pst
						from
						(
						select pengambilan_mk.id_mhs,mahasiswa.nim_mhs,nm_pengguna,
						nm_jenjang||'-'||nm_program_studi as prodi,mahasiswa.id_program_studi,
						kurikulum_mk.kredit_semester, besar_biaya_sp,kd_mata_kuliah,nm_jalur,id_fakultas
						from pengambilan_mk
						left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
						left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
						left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
						left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
						left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
						left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
						left join jalur_mahasiswa on jalur_mahasiswa.id_mhs = mahasiswa.id_mhs
						left join jalur on jalur_mahasiswa.id_jalur=jalur.id_jalur
						left join biaya_sp on biaya_sp.id_jalur = jalur_mahasiswa.id_jalur and biaya_sp.id_program_studi = mahasiswa.id_program_studi
						and biaya_sp.id_semester = pengambilan_mk.id_semester
						where pengambilan_mk.id_semester=$smt1 and id_fakultas=$kdfak and jalur_mahasiswa.id_jalur_aktif = 1 and status_apv_pengambilan_mk=1
						and mahasiswa.id_program_studi=$id_prodi
						)s1
						left join
						(select kd_mata_kuliah,count(id_mhs) as pst from pengambilan_mk
						left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
						left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
						where id_semester=$smt1 and id_mhs in (select id_mhs from mahasiswa where id_program_studi in
						(select id_program_studi from program_studi where id_fakultas=$kdfak)) and status_apv_pengambilan_mk=1
						group by kd_mata_kuliah)s2 on s1.kd_mata_kuliah=s2.kd_mata_kuliah
						)
						group by id_mhs, nim_mhs, nm_pengguna, prodi, id_program_studi, nm_jalur)s3
						left join pembayaran_sp on s3.id_mhs=pembayaran_sp.id_mhs and id_status_pembayaran=1 and pembayaran_sp.id_semester=$smt1
						group by s3.id_mhs, s3.nim_mhs, s3.nm_pengguna, s3.prodi, s3.id_program_studi, s3.sks_ambil, nm_jalur, besar_biaya_sp, s3.sp_biaya)
						where terbayar<sp_biaya and terbayar>0");
	}
	if ($st==4)
	{
	 $detailprodi=getData("select * from (
						select s3.id_mhs,s3.nim_mhs,s3.nm_pengguna,s3.prodi,s3.id_program_studi,s3.sks_ambil,nm_jalur,besar_biaya_sp,
						s3.sp_biaya,coalesce(sum(besar_biaya),0) as terbayar ,'LEBIH' as status from (
						select id_mhs,nim_mhs,nm_pengguna,prodi,id_program_studi,sum(kredit_semester) as sks_ambil,nm_jalur,
						wm_concat(distinct besar_biaya_sp) as besar_biaya_sp,
						sum(besar_biaya_sp*kredit_semester) as sp_biaya from
						(
						select s1.id_mhs,s1.nim_mhs,s1.nm_pengguna,s1.prodi,s1.id_program_studi,s1.kredit_semester,nm_jalur,
						case when s1.id_fakultas=10 and s2.pst<11 then round(1500000/(s2.pst*s1.kredit_semester),-2) else s1.besar_biaya_sp end as besar_biaya_sp,
						s1.kd_mata_kuliah,s2.pst
						from
						(
						select pengambilan_mk.id_mhs,mahasiswa.nim_mhs,nm_pengguna,
						nm_jenjang||'-'||nm_program_studi as prodi,mahasiswa.id_program_studi,
						kurikulum_mk.kredit_semester, besar_biaya_sp,kd_mata_kuliah,nm_jalur,id_fakultas
						from pengambilan_mk
						left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
						left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
						left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
						left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
						left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
						left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
						left join jalur_mahasiswa on jalur_mahasiswa.id_mhs = mahasiswa.id_mhs
						left join jalur on jalur_mahasiswa.id_jalur=jalur.id_jalur
						left join biaya_sp on biaya_sp.id_jalur = jalur_mahasiswa.id_jalur and biaya_sp.id_program_studi = mahasiswa.id_program_studi
						and biaya_sp.id_semester = pengambilan_mk.id_semester
						where pengambilan_mk.id_semester=$smt1 and id_fakultas=$kdfak and jalur_mahasiswa.id_jalur_aktif = 1 and status_apv_pengambilan_mk=1
						and mahasiswa.id_program_studi=$id_prodi
						)s1
						left join
						(select kd_mata_kuliah,count(id_mhs) as pst from pengambilan_mk
						left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
						left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
						where id_semester=$smt1 and id_mhs in (select id_mhs from mahasiswa where id_program_studi in
						(select id_program_studi from program_studi where id_fakultas=$kdfak)) and status_apv_pengambilan_mk=1
						group by kd_mata_kuliah)s2 on s1.kd_mata_kuliah=s2.kd_mata_kuliah
						)
						group by id_mhs, nim_mhs, nm_pengguna, prodi, id_program_studi, nm_jalur)s3
						left join pembayaran_sp on s3.id_mhs=pembayaran_sp.id_mhs and id_status_pembayaran=1 and pembayaran_sp.id_semester=$smt1
						group by s3.id_mhs, s3.nim_mhs, s3.nm_pengguna, s3.prodi, s3.id_program_studi, s3.sks_ambil, nm_jalur, besar_biaya_sp, s3.sp_biaya)
						where terbayar>sp_biaya");
	}
	
	
	$smarty->assign('T_DETPRODI', $detailprodi);
	
	$smarty->assign('disp1','none');
	$smarty->assign('disp2','block');
} 
if ($_GET['action']=='drop')
{
	$id_prodi=$_GET['idprodi'];
	
	gantidata("update pengambilan_mk set status_pengambilan_mk=0,status_apv_pengambilan_mk=0,status_hapus=1,flagnilai='0',keterangan='DROP KRS_SP BELUM BAYAR' where id_semester=$smt1
	           and id_mhs in (
			   select id_mhs from (
						select s3.id_mhs,s3.nim_mhs,s3.nm_pengguna,s3.prodi,s3.id_program_studi,s3.sks_ambil,nm_jalur,besar_biaya_sp,
						s3.sp_biaya,coalesce(sum(besar_biaya),0) as terbayar,'BLM BAYAR' as status from (
						select id_mhs,nim_mhs,nm_pengguna,prodi,id_program_studi,sum(kredit_semester) as sks_ambil,nm_jalur,
						wm_concat(distinct besar_biaya_sp) as besar_biaya_sp,
						sum(besar_biaya_sp*kredit_semester) as sp_biaya from
						(
						select s1.id_mhs,s1.nim_mhs,s1.nm_pengguna,s1.prodi,s1.id_program_studi,s1.kredit_semester,nm_jalur,
						case when s1.id_fakultas=10 and s2.pst<11 then round(1500000/(s2.pst*s1.kredit_semester),-2) else s1.besar_biaya_sp end as besar_biaya_sp,
						s1.kd_mata_kuliah,s2.pst
						from
						(
						select pengambilan_mk.id_mhs,mahasiswa.nim_mhs,nm_pengguna,
						nm_jenjang||'-'||nm_program_studi as prodi,mahasiswa.id_program_studi,
						kurikulum_mk.kredit_semester, besar_biaya_sp,kd_mata_kuliah,nm_jalur,id_fakultas
						from pengambilan_mk
						left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
						left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
						left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
						left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
						left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
						left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
						left join jalur_mahasiswa on jalur_mahasiswa.id_mhs = mahasiswa.id_mhs
						left join jalur on jalur_mahasiswa.id_jalur=jalur.id_jalur
						left join biaya_sp on biaya_sp.id_jalur = jalur_mahasiswa.id_jalur and biaya_sp.id_program_studi = mahasiswa.id_program_studi
						and biaya_sp.id_semester = pengambilan_mk.id_semester
						where pengambilan_mk.id_semester=$smt1 and id_fakultas=$kdfak and jalur_mahasiswa.id_jalur_aktif = 1 and status_apv_pengambilan_mk=1
						and mahasiswa.id_program_studi=$id_prodi
						)s1
						left join
						(select kd_mata_kuliah,count(id_mhs) as pst from pengambilan_mk
						left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
						left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
						where id_semester=$smt1 and id_mhs in (select id_mhs from mahasiswa where id_program_studi in
						(select id_program_studi from program_studi where id_fakultas=$kdfak)) and status_apv_pengambilan_mk=1
						group by kd_mata_kuliah)s2 on s1.kd_mata_kuliah=s2.kd_mata_kuliah
						)
						group by id_mhs, nim_mhs, nm_pengguna, prodi, id_program_studi, nm_jalur)s3
						left join pembayaran_sp on s3.id_mhs=pembayaran_sp.id_mhs and id_status_pembayaran=1 and pembayaran_sp.id_semester=$smt1
						group by s3.id_mhs, s3.nim_mhs, s3.nm_pengguna, s3.prodi, s3.id_program_studi, s3.sks_ambil, nm_jalur, besar_biaya_sp, s3.sp_biaya)
						where terbayar=0)");
	//echo "update pengambilan_mk set status_pengambilan_mk='0',status_apv_pengambilan_mk='0',status_hapus=1,flagnilai='0',keterangan='DROP KRS_SP BELUM BAYAR' where id_semester=$smt1
	  //         and id_mhs in (select id_mhs from pembayaran_sp where id_semester=$smt1 and id_status_pembayaran !=1) 
		//	   and id_mhs in (select id_mhs from mahasiswa where id_program_studi=$id_prodi";
	
	echo '<script>alert("Proses DROP KRS-SP.. Sudah SELESAI...")</script>';
	echo '<script>location.href="javascript:history.go(-1)";</script>';
	
} 
else {

$prodi1=getData("select prodi,id_program_studi,sum(case when (terbayar=sp_biaya) then 1 else 0 end)as ttlbayar, 
sum(case when (terbayar<sp_biaya and terbayar>0) then 1 else 0 end)as krgbayar,
sum(case when (terbayar=0)  then 1 else 0 end)as blmbayar,
sum(case when (terbayar>sp_biaya) then 1 else 0 end)as lbhbayar from
(
select s3.id_mhs,s3.nim_mhs,s3.nm_pengguna,s3.prodi,s3.id_program_studi,s3.sks_ambil,
s3.sp_biaya,coalesce(sum(besar_biaya),0) as terbayar from (
select id_mhs,nim_mhs,nm_pengguna,prodi,id_program_studi,sum(kredit_semester) as sks_ambil,
sum(besar_biaya_sp*kredit_semester) as sp_biaya from
(
select s1.id_mhs,s1.nim_mhs,s1.nm_pengguna,s1.prodi,s1.id_program_studi,s1.kredit_semester,
case when s1.id_fakultas=10 and s2.pst<11 then round(1500000/(s2.pst*s1.kredit_semester),-2) else s1.besar_biaya_sp end as besar_biaya_sp,
s1.kd_mata_kuliah,s2.pst
from
(
select pengambilan_mk.id_mhs,mahasiswa.nim_mhs,nm_pengguna,
nm_jenjang||'-'||nm_program_studi as prodi,mahasiswa.id_program_studi,
kurikulum_mk.kredit_semester, besar_biaya_sp,kd_mata_kuliah,id_fakultas
from pengambilan_mk
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join jalur_mahasiswa on jalur_mahasiswa.id_mhs = mahasiswa.id_mhs
left join biaya_sp on biaya_sp.id_jalur = jalur_mahasiswa.id_jalur and biaya_sp.id_program_studi = mahasiswa.id_program_studi
and biaya_sp.id_semester = pengambilan_mk.id_semester
where pengambilan_mk.id_semester=$smt1 and id_fakultas=$kdfak and jalur_mahasiswa.id_jalur_aktif = 1 and status_apv_pengambilan_mk=1
)s1
left join
(select kd_mata_kuliah,count(id_mhs) as pst from pengambilan_mk
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
where id_semester=$smt1 and id_mhs in (select id_mhs from mahasiswa where id_program_studi in
(select id_program_studi from program_studi where id_fakultas=$kdfak)) and status_apv_pengambilan_mk=1
group by kd_mata_kuliah)s2 on s1.kd_mata_kuliah=s2.kd_mata_kuliah

)
group by id_mhs, nim_mhs, nm_pengguna, prodi, id_program_studi)s3
left join pembayaran_sp on s3.id_mhs=pembayaran_sp.id_mhs and id_status_pembayaran=1 and pembayaran_sp.id_semester=$smt1
group by s3.id_mhs, s3.nim_mhs, s3.nm_pengguna, s3.prodi, s3.id_program_studi, s3.sks_ambil, s3.sp_biaya) 
group by prodi, id_program_studi
order by prodi");

$smarty->assign('T_PRODI1', $prodi1);
}

$smarty->display('mon-pemb-sp.tpl');

?>
