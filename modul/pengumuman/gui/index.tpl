<!DOCTYPE html>
<html>
	<head>
		<title>Pengumuman Universitas Airlangga</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		
		<!-- Bootstrap -->
		<link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<style>
			body {
				background-image: url('/img/pengumuman/bg_top_header.png');
				background-position: top left;
				background-repeat: repeat-x;
			}
			.cybercampus-header {
				background-image: url('/img/pengumuman/pengumuman.png');
				background-repeat: no-repeat;
				background-position: top center;
			}
			.content-wrapper {
				padding-top: 147px;
			}
			input.input-nomer {
				font-size: 2em;
				height: auto;
				color: black;
			}
		</style>
	</head>
	<body>
		<div class="container cybercampus-header content-wrapper">
			<h2 class="text-center">Pengumuman Universitas Airlangga</h2>
			<hr/>
			<div class="container text-center" style="width: 500px">
				{if $smarty.server.REQUEST_METHOD == 'GET'}
				<form method="post" action="">
					<p class="text-center">Masukkan nomer ujian disini :</p>
					<input type="text" class="text-center input-nomer" name="no_ujian" value="{if $no_ujian}{$no_ujian}{/if}" />
					<p class="text-center">Masukkan captcha untuk keamanan :</p>
					<div align="center">
						<script type="text/javascript" src="http://www.google.com/recaptcha/api/challenge?k=6LdNGeMSAAAAANTz2Mfg8X3ftiiBTFAMhwnpAXyh"></script>
					</div>
					<br/>
					<input type="submit" class="btn btn-large btn-primary" value="Lihat Hasil" />
				</form>
				{/if}
				
				{if $smarty.server.REQUEST_METHOD == 'POST'}
					{if $error_message}
						<p class="text-error">{$error_message}</p>
						<p><a href="index.php">Klik untuk kembali</a></p>
					{/if}
					
					{if $success}
						{if $data.TGL_DITERIMA != ''}
							<h4>Selamat {$data.NM_C_MHS},</h4>
							<h4>Anda diterima di program studi {$data.NM_PROGRAM_STUDI}</h4>
							<h4>Universitas Airlangga</h4>
							<h5>Program Penerimaan : {$data.NM_PENERIMAAN} Tahun Ajaran {$data.TAHUN} / {$data.TAHUN + 1}</h5>
						{else}
							<h5>Mohon maaf {$data.NM_C_MHS}, Anda tidak termasuk di dalam daftar diterima di Universitas Airlangga.</h5>
						{/if}
						<p><a href="index.php">Klik untuk kembali</a></p>
					{/if}
				{/if}
			</div>
		</div>
			
		{literal}
		<!-- Script -->
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-41992721-3', 'unair.ac.id');
		  ga('send', 'pageview');

		</script>
		{/literal}
	</body>
</html>
