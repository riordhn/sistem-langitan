<?php
include '../../config.php';

$recaptcha_private_key = "6LdNGeMSAAAAABhEtoS88c3OnjLZLZzlplXucyA7";
$recaptcha_public_key = "6LdNGeMSAAAAANTz2Mfg8X3ftiiBTFAMhwnpAXyh";

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	// Cleansing Nomer Ujian
	$no_ujian = trim($_POST['no_ujian']); 			// remove space
	$no_ujian = str_replace("'", "''", $no_ujian);	// remove sql injection vuln
	$no_ujian = str_replace("-", "", $no_ujian);	// remove dash
	
	if ( ! empty($no_ujian))
	{
		$smarty->assign('no_ujian', $no_ujian);
	}
	
	$resp = recaptcha_check_answer ($recaptcha_private_key, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
	
	if ( ! $resp->is_valid)
	{
		$smarty->assign('error_message', "Pastikan sudah memasukkan kode recaptcha dengan benar.");
	}
	else
	{	
		// Query pengumuman
		$db->Query("
			select no_ujian, nm_c_mhs, nm_penerimaan, gelombang, tahun, nm_program_studi, to_char(tgl_pengumuman, 'YYYY-MM-DD HH24:MI:SS') tgl_pengumuman, tgl_diterima
			from calon_mahasiswa_baru cmb
			join penerimaan p on p.id_penerimaan = cmb.ID_PENERIMAAN
			left join program_studi ps on ps.id_program_studi = cmb.id_program_studi
			where cmb.no_ujian = '{$no_ujian}'");
		$data = $db->FetchAssoc();
		
		if (empty($data))
		{
			$smarty->assign('error_message', "Nomer ujian <strong style=\"font-size:1.25em\">{$no_ujian}</strong> tidak ditemukan, silahkan periksa kembali.");
		}
		else if (time() < strtotime($data['TGL_PENGUMUMAN']))
		{
			$smarty->assign('error_message', 
				"Pengumuman penerimaan <strong>{$data['NM_PENERIMAAN']}</strong> belum dibuka.
				Hasil pengumuman bisa dilihat pada : <br/><strong>".strftime('%d %B %Y', strtotime($data['TGL_PENGUMUMAN']))." Jam ".strftime('%H:%M', strtotime($data['TGL_PENGUMUMAN']))."</strong>.<br/>Terima Kasih.");
		}
		else
		{
			$smarty->assign('success', "1");
			$smarty->assign('data', $data);
		}
	}
}

$smarty->display("index.tpl");
?>
