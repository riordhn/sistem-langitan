<?php
ini_set("display_errors", 1);
include ('config.php');
include ('includes/encrypt.php');

$mode = get('mode', 'view');
$id_pt = $id_pt_user;

$depan = time();
$belakang = strrev(time());

if ($request_method == 'POST')
{
    if (post('mode') == 'edit')
    {
        $id_arsip_dokumen = post('id_arsip_dokumen');
        $kode_katalog = post('kode_katalog');
        $nm_arsip_dokumen = post('nm_arsip_dokumen');
        $id_arsip_pemilik = post('id_arsip_pemilik');
        $tgl_penyusunan = post('tgl_penyusunan');
        $id_unit_kerja = post('id_unit_kerja');
        $id_arsip_loker = post('id_arsip_loker');
        $jml_halaman = post('jml_halaman');
        $contact_person = post('contact_person');
        
        $result = $db->Query("update arsip_dokumen set kode_katalog = '{$kode_katalog}', nm_arsip_dokumen = '{$nm_arsip_dokumen}',
        						id_arsip_pemilik = '{$id_arsip_pemilik}', tgl_penyusunan = '{$tgl_penyusunan}', id_unit_kerja = '{$id_unit_kerja}',
        						id_arsip_loker = '{$id_arsip_loker}', jml_halaman = '{$jml_halaman}', contact_person = '{$contact_person}'
        						where id_arsip_dokumen = '{$id_arsip_dokumen}'");

        $smarty->assign('edited', $result ? "Data berhasil dirubah" : "Data gagal dirubah");
    }
    if (post('mode') == 'add')  
    {
        //$id_arsip_dokumen = post('id_arsip_dokumen');
        $kode_katalog = post('kode_katalog');
        $nm_arsip_dokumen = post('nm_arsip_dokumen');
        $id_arsip_pemilik = post('id_arsip_pemilik');
        $tgl_penyusunan = post('tgl_penyusunan');
        $id_unit_kerja = post('id_unit_kerja');
        $id_arsip_loker = post('id_arsip_loker');
        $jml_halaman = post('jml_halaman');
        $contact_person = post('contact_person');
        
        $result = $db->Query("insert into arsip_dokumen (kode_katalog, nm_arsip_dokumen, id_arsip_pemilik, tgl_penyusunan, id_unit_kerja,
                                id_arsip_loker, jml_halaman, contact_person, id_perguruan_tinggi) 
        						values ('{$kode_katalog}', '{$nm_arsip_dokumen}', '{$id_arsip_pemilik}', '{$tgl_penyusunan}', '{$id_unit_kerja}',
                                '{$id_arsip_loker}', '{$jml_halaman}', '{$contact_person}', '{$id_pt}')");

        $smarty->assign('edited', $result ? "Data berhasil dimasukkan" : "Data gagal dimasukkan");
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$q = strtoupper(get('q', ''));

		// ketika ada get
		if (strlen(trim($q)) >= 3)
        {
			$arsip_dokumen_set = $db->QueryToArray("SELECT ad.*, ap.NM_ARSIP_PEMILIK, j.NM_JENJANG, uk.NM_UNIT_KERJA, al.NM_ARSIP_LOKER 
	                                                FROM ARSIP_DOKUMEN ad 
													LEFT JOIN ARSIP_PEMILIK ap ON ap.ID_ARSIP_PEMILIK = ad.ID_ARSIP_PEMILIK
	                                                LEFT JOIN UNIT_KERJA uk ON uk.ID_UNIT_KERJA = ad.ID_UNIT_KERJA
	                                                LEFT JOIN PROGRAM_STUDI ps on ps.ID_PROGRAM_STUDI = uk.ID_PROGRAM_STUDI
            										LEFT JOIN JENJANG j on j.ID_JENJANG = ps.ID_JENJANG
													LEFT JOIN ARSIP_LOKER al ON al.ID_ARSIP_LOKER = ad.ID_ARSIP_LOKER
	                                                WHERE ad.ID_PERGURUAN_TINGGI = {$id_pt} 
	                                                AND (upper(ad.KODE_KATALOG) like '%{$q}%' or upper(ad.NM_ARSIP_DOKUMEN) like '%{$q}%' 
                                                    or upper(ap.NM_ARSIP_PEMILIK) like '%{$q}%' or upper(ad.TGL_PENYUSUNAN) like '%{$q}%'
                                                    or upper(uk.NM_UNIT_KERJA) like '%{$q}%' or upper(al.NM_ARSIP_LOKER) like '%{$q}%'
                                                    or upper(ad.JML_HALAMAN) like '%{$q}%' or upper(ad.CONTACT_PERSON) like '%{$q}%')
													ORDER BY ad.KODE_KATALOG");
			$smarty->assignByRef('arsip_dokumen_set', $arsip_dokumen_set);
		}
		else{
			// load semua, edit di view.tpl belum, lihat contoh di account-search.php di superadmin
			$arsip_dokumen_semua_set = $db->QueryToArray("SELECT ad.*, ap.NM_ARSIP_PEMILIK, j.NM_JENJANG, uk.NM_UNIT_KERJA, al.NM_ARSIP_LOKER 
		                                                FROM ARSIP_DOKUMEN ad 
														LEFT JOIN ARSIP_PEMILIK ap ON ap.ID_ARSIP_PEMILIK = ad.ID_ARSIP_PEMILIK
		                                                LEFT JOIN UNIT_KERJA uk ON uk.ID_UNIT_KERJA = ad.ID_UNIT_KERJA
		                                                LEFT JOIN PROGRAM_STUDI ps on ps.ID_PROGRAM_STUDI = uk.ID_PROGRAM_STUDI
	            										LEFT JOIN JENJANG j on j.ID_JENJANG = ps.ID_JENJANG
														LEFT JOIN ARSIP_LOKER al ON al.ID_ARSIP_LOKER = ad.ID_ARSIP_LOKER
		                                                WHERE ad.ID_PERGURUAN_TINGGI = {$id_pt} 
														ORDER BY ad.KODE_KATALOG");
			$smarty->assignByRef('arsip_dokumen_semua_set', $arsip_dokumen_semua_set);
		}
	}
	else if ($mode == 'edit' )
	{
		$id_arsip_dokumen 	= (int)get('id_arsip_dokumen', '');

		$arsip_dokumen = $db->QueryToArray("
            select * 
            from arsip_dokumen 
            where id_arsip_dokumen = {$id_arsip_dokumen}");
        $smarty->assign('arsip_dokumen', $arsip_dokumen[0]);

        /*$db->Query("SELECT TYPE_arsip_dokumen FROM arsip_dokumen WHERE ID_PERGURUAN_TINGGI = '{$id_pt}'");
        $type_arsip_dokumen = $db->FetchAssoc();

        $isi_type = $type_arsip_dokumen['TYPE_arsip_dokumen'];

        $smarty->assign('isi_type', $isi_type);*/

        $arsip_pemilik_set = $db->QueryToArray("
	            select id_arsip_pemilik, nm_arsip_pemilik 
	            from arsip_pemilik
	            where id_perguruan_tinggi = '{$id_pt}'");
	    $smarty->assign('arsip_pemilik_set', $arsip_pemilik_set);

	    $arsip_loker_set = $db->QueryToArray("
	            select id_arsip_loker, nm_arsip_loker 
	            from arsip_loker
	            where id_perguruan_tinggi = '{$id_pt}'");
	    $smarty->assign('arsip_loker_set', $arsip_loker_set);

	    $unit_kerja_set = $db->QueryToArray("
            select uk.id_unit_kerja, uk.nm_unit_kerja, ps.nm_program_studi, j.nm_jenjang 
            from unit_kerja uk
            left join program_studi ps on ps.id_program_studi = uk.id_program_studi
            left join jenjang j on j.id_jenjang = ps.id_jenjang
            where uk.id_perguruan_tinggi = {$id_pt}");
        $smarty->assign('unit_kerja_set', $unit_kerja_set);

        /*$foto = getvar("select case when (pgg.gelar_belakang is null or pgg.gelar_belakang = '') then trim(pgg.gelar_depan||' '||upper(pgg.nm_pengguna)) else trim(pgg.gelar_depan||' '||upper(pgg.nm_pengguna)||', '||pgg.gelar_belakang) end as nm_pengguna,
		peg.nip_pegawai from pegawai peg left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
		where peg.id_pengguna=$id_pgg");*/
        $to = $arsip_dokumen[0]['KODE_KATALOG'];
        $file = $arsip_dokumen[0]['ID_ARSIP_DOKUMEN'];
        $img = 'proses/upload_photo.php?' . paramEncrypt($depan . '=' . $belakang . $depan . '&yth=' . $to . '&file=' . $file . '&' . $belakang . '=' . $depan . $belakang) . '';
        $smarty->assign('IMG', $img);

        //$get_photo = getvar("select username as photo from pengguna where id_pengguna=$id_pgg");
        $filename = "../../files/arsiparis/" . $arsip_dokumen[0]['ID_ARSIP_DOKUMEN'] . ".JPG";
        if (file_exists($filename)) {
            $photo = $filename.'?t=timestamp';
        } else {
            $photo = 'includes/images/cancel.png';
        }
        $smarty->assign('PHOTO', $photo);

	}
	else if($mode == 'add')
	{
		$arsip_pemilik_set = $db->QueryToArray("
	            select id_arsip_pemilik, nm_arsip_pemilik 
	            from arsip_pemilik
	            where id_perguruan_tinggi = '{$id_pt}'");
	    $smarty->assign('arsip_pemilik_set', $arsip_pemilik_set);

	    $arsip_loker_set = $db->QueryToArray("
	            select id_arsip_loker, nm_arsip_loker 
	            from arsip_loker
	            where id_perguruan_tinggi = '{$id_pt}'");
	    $smarty->assign('arsip_loker_set', $arsip_loker_set);

	    $unit_kerja_set = $db->QueryToArray("
            select uk.id_unit_kerja, uk.nm_unit_kerja, ps.nm_program_studi, j.nm_jenjang 
            from unit_kerja uk
            left join program_studi ps on ps.id_program_studi = uk.id_program_studi
            left join jenjang j on j.id_jenjang = ps.id_jenjang
            where uk.id_perguruan_tinggi = {$id_pt}");
        $smarty->assign('unit_kerja_set', $unit_kerja_set);
	}
}

$smarty->display("master/dokumen/{$mode}.tpl");