<div class="center_title_bar">Pemilik Arsip</div>

{if isset($arsip_pemilik_set)}
	<table class="tableabout">
		<thead>
			<tr>
				<th>No</th>
				<th>Nama Arsip Pemilik</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			{foreach $arsip_pemilik_set as $uk}
				<tr {if $uk@index is not div by 2}class="odd"{/if}>
					<td class="center">{$uk@index + 1}</td>
					<td><strong>{$uk.NM_ARSIP_PEMILIK}</strong></td>
					<td class="center">
						<a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;"  href="master-pemilik.php?mode=edit&id_arsip_pemilik={$uk.ID_ARSIP_PEMILIK}"><b style="color: red">Edit</b></a>
						{if $uk.RELASI == 0}
							<a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="master-pemilik.php?mode=delete&id_arsip_pemilik={$uk.ID_ARSIP_PEMILIK}"><b style="color: red">Delete</b></a>
						{/if}
					</td>
				</tr>
			{/foreach}
			<tr>
				<td colspan="9" class="center">
					<a href="master-pemilik.php?mode=add"><b style="color: red">Tambah Master Pemilik</b></a>
				</td>
			</tr>
		</tbody>
	</table>
{/if}