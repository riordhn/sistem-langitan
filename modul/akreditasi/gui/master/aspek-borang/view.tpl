<div class="center_title_bar">Master Aspek Borang</div>

{if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

{if isset($aspek_borang_set)}
	<table class="tableabout">
		<thead>
			<tr>
				<th>No</th>
				<th>Nama Akreditasi Borang</th>
				<th>Kode Aspek Borang</th>
				<th>Nama Aspek Borang</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			{foreach $aspek_borang_set as $uk}
				<tr {if $uk@index is not div by 2}class="odd"{/if}>
					<td class="center">{$uk@index + 1}</td>
					<td>{$uk.NM_AKREDITASI_BORANG}</td>
					<td>{$uk.KODE_ASPEK_BORANG}</td>
					<td>{$uk.NM_ASPEK_BORANG}</td>
					<td class="center">
						<a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="master-aspek-borang.php?mode=edit&id_aspek_borang={$uk.ID_AKREDITASI_ASPEK_BORANG}"><b style="color: green">Edit</b></a>
						{if $uk.RELASI == 0}
							<a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="master-aspek-borang.php?mode=delete&id_aspek_borang={$uk.ID_AKREDITASI_ASPEK_BORANG}"><b style="color: red">Delete</b></a>
						{/if}
					</td>
				</tr>
			{/foreach}
			<tr>
				<td colspan="9" class="center">
					<a href="master-aspek-borang.php?mode=add"><b style="color: red">Tambah Aspek Borang</b></a>
				</td>
			</tr>
		</tbody>
	</table>
{/if}