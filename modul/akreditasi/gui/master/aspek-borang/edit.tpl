<div class="center_title_bar">ID Aspek Borang : {$aspek_borang.ID_AKREDITASI_ASPEK_BORANG}</div>

{if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

<form action="master-aspek-borang.php?{$smarty.server.QUERY_STRING}" method="post">
<input type="hidden" name="mode" value="edit" />
<input type="hidden" name="id_aspek_borang" value="{$aspek_borang.ID_AKREDITASI_ASPEK_BORANG}" />
<!--<input type="hidden" name="id_join_table" value="{$pengguna.JOIN_TABLE}" /> -->
<table>
    <tr>
        <th colspan="2">Detail Aspek Borang</th>
    </tr>
    <tr>
        <td>ID</td>
        <td>{$aspek_borang.ID_AKREDITASI_ASPEK_BORANG}</td>
    </tr>
    <tr>
        <td>Nama Borang</td>
        <td>
            <select name="id_borang">
                {foreach $borang_set as $data}
                    <option value="{$data.ID_AKREDITASI_BORANG}" {if $aspek_borang.ID_AKREDITASI_BORANG==$data.ID_AKREDITASI_BORANG}selected="true"{/if}>{$data.NM_AKREDITASI_BORANG}</option>
                {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Kode Aspek Borang</td>
        <td><input class="form-control" type="text" name="kode_aspek_borang" value="{$aspek_borang.KODE_ASPEK_BORANG}"  />
        </td>
    </tr>
    <tr>
        <td>Nama Aspek Borang</td>
        <td><textarea rows="4" cols="50" name="nm_aspek_borang">{$aspek_borang.NM_ASPEK_BORANG}</textarea>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>

<!--<form action="account-search.php?{$smarty.server.QUERY_STRING}" method="post">
<input type="hidden" name="mode" value="ubahPassword" />
<input type="hidden" name="id_pengguna" value="{$pengguna.ID_PENGGUNA}" />
<input type="hidden" name="username" value="{$pengguna.USERNAME}" />
<table>
    <tr>
        <th colspan="2" class="center">Reset Password?</th>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <input type="submit" value="Reset" />
        </td>
    </tr>
</table>
</form>

<script type="text/javascript">
    function deleteRolePengguna(id_role_pengguna)
    {
        if (confirm('Apakah role ini akan di hapus') == true)
        {
            $.ajax({
                type: 'POST',
                url: 'account-search.php',
                data: 'mode=delete-role&id_role_pengguna='+id_role_pengguna,
                success: function(data) {
                    if (data == '1') {
                        $('#r'+id_role_pengguna).remove();
                    }
                    else {
                        alert('Gagal dihapus');
                    }  
                }
            });
        }
    }
</script>
            
<table>
    <tr>
        <th>Default</th>
        <th>Role</th>
        <th>Template</th>
        <th>Action</th>
    </tr>
    {foreach $role_pengguna_set as $rp}
    <tr {if $rp@index is not div by 2}class="row1"{/if} id="r{$rp.ID_ROLE_PENGGUNA}">
        <td class="center"><input type="radio" name="id_role" value="{$rp.ID_ROLE}" {if $rp.ID_ROLE == $pengguna.ID_ROLE}checked="checked"{/if} disabled="disabled"/></td>
        <td>{$rp.NM_ROLE}</td>
        <td>{if $rp.NM_TEMPLATE == ''}Default{else}{$rp.NM_TEMPLATE}{/if}</td>
        <td>
            <a href="account-search.php?mode=edit-role&id_role_pengguna={$rp.ID_ROLE_PENGGUNA}" title="Edit"><img src="{$base_url}img/superadmin/navigation/edit.png" /></a>
            {if $rp.ID_ROLE != $pengguna.ID_ROLE}
            <img src="{$base_url}img/superadmin/navigation/del-red.png" title="Hapus Role" style="cursor: pointer" onclick='deleteRolePengguna({$rp.ID_ROLE_PENGGUNA})' />
            {/if}
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="4" class="center">
            <a href="account-search.php?mode=add-role&id_pengguna={$pengguna.ID_PENGGUNA}&q={$smarty.get.q}">Tambah</a>
        </td>
    </tr>
</table>
-->

<a href="master-aspek-borang.php">Kembali</a>