<div class="center_title_bar">ID Borang Prodi : {$borang.ID_AKREDITASI_BORANG}</div>

{if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

<form action="master-borang.php?{$smarty.server.QUERY_STRING}" method="post">
<input type="hidden" name="mode" value="edit" />
<input type="hidden" name="id_borang" value="{$borang.ID_AKREDITASI_BORANG}" />
<!--<input type="hidden" name="id_join_table" value="{$pengguna.JOIN_TABLE}" /> -->
<table>
    <tr>
        <th colspan="2">Detail Borang Prodi</th>
    </tr>
    <tr>
        <td>ID</td>
        <td>{$borang.ID_AKREDITASI_BORANG}</td>
    </tr>
    <tr>
        <td>Fakultas</td>
        <td>
            <select id="fakultas" name="fakultas">
                <option value="">Pilih Fakultas</option>
                {foreach $data_fakultas as $data}
                    <option value="{$data.ID_FAKULTAS}" {if $borang.ID_FAKULTAS==$data.ID_FAKULTAS}selected="true"{/if}>{$data.NM_FAKULTAS|upper}</option>
                {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Program Studi</td>
        <td>
            <select id="program_studi" name="program_studi">
                <option value="">PILIH PRODI</option>
                {foreach $program_studi as $data}
                    <option value="{$data.ID_PROGRAM_STUDI}" {if $borang.ID_PROGRAM_STUDI==$data.ID_PROGRAM_STUDI}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
                {/foreach} 
            </select>
        </td>
    </tr>
    <tr>
        <td>Nama Borang</td>
        <td><textarea rows="4" cols="50" name="nm_borang" >{$borang.NM_AKREDITASI_BORANG}</textarea>
        </td>
    </tr>
    <tr>
        <td>Tahun Borang</td>
        <td><input class="form-control" type="text" name="thn_borang" value="{$borang.THN_AKREDITASI_BORANG}" /> *) Diisi tahun. Misal : 2014 atau 2015
        </td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>

{literal}
    <script type="text/javascript">
        $('#fakultas').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang=',
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
        
    </script>
{/literal}
<!--<form action="account-search.php?{$smarty.server.QUERY_STRING}" method="post">
<input type="hidden" name="mode" value="ubahPassword" />
<input type="hidden" name="id_pengguna" value="{$pengguna.ID_PENGGUNA}" />
<input type="hidden" name="username" value="{$pengguna.USERNAME}" />
<table>
    <tr>
        <th colspan="2" class="center">Reset Password?</th>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <input type="submit" value="Reset" />
        </td>
    </tr>
</table>
</form>

<script type="text/javascript">
    function deleteRolePengguna(id_role_pengguna)
    {
        if (confirm('Apakah role ini akan di hapus') == true)
        {
            $.ajax({
                type: 'POST',
                url: 'account-search.php',
                data: 'mode=delete-role&id_role_pengguna='+id_role_pengguna,
                success: function(data) {
                    if (data == '1') {
                        $('#r'+id_role_pengguna).remove();
                    }
                    else {
                        alert('Gagal dihapus');
                    }  
                }
            });
        }
    }
</script>
            
<table>
    <tr>
        <th>Default</th>
        <th>Role</th>
        <th>Template</th>
        <th>Action</th>
    </tr>
    {foreach $role_pengguna_set as $rp}
    <tr {if $rp@index is not div by 2}class="row1"{/if} id="r{$rp.ID_ROLE_PENGGUNA}">
        <td class="center"><input type="radio" name="id_role" value="{$rp.ID_ROLE}" {if $rp.ID_ROLE == $pengguna.ID_ROLE}checked="checked"{/if} disabled="disabled"/></td>
        <td>{$rp.NM_ROLE}</td>
        <td>{if $rp.NM_TEMPLATE == ''}Default{else}{$rp.NM_TEMPLATE}{/if}</td>
        <td>
            <a href="account-search.php?mode=edit-role&id_role_pengguna={$rp.ID_ROLE_PENGGUNA}" title="Edit"><img src="{$base_url}img/superadmin/navigation/edit.png" /></a>
            {if $rp.ID_ROLE != $pengguna.ID_ROLE}
            <img src="{$base_url}img/superadmin/navigation/del-red.png" title="Hapus Role" style="cursor: pointer" onclick='deleteRolePengguna({$rp.ID_ROLE_PENGGUNA})' />
            {/if}
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="4" class="center">
            <a href="account-search.php?mode=add-role&id_pengguna={$pengguna.ID_PENGGUNA}&q={$smarty.get.q}">Tambah</a>
        </td>
    </tr>
</table>
-->

<a href="master-borang.php">Kembali</a>