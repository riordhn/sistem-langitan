<div class="center_title_bar">Master Borang Prodi</div>

{if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

{if isset($borang_set)}
	<table class="tableabout">
		<thead>
			<tr>
				<th>No</th>
				<th>Program Studi</th>
				<th>Nama Borang</th>
				<th>Tahun Borang</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			{foreach $borang_set as $uk}
				<tr {if $uk@index is not div by 2}class="odd"{/if}>
					<td class="center">{$uk@index + 1}</td>
					<td><strong>{$uk.NM_PROGRAM_STUDI}</strong></td>
					<td>{$uk.NM_AKREDITASI_BORANG}</td>
					<td>{$uk.THN_AKREDITASI_BORANG}</td>
					<td class="center">
						<a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="master-borang.php?mode=edit&id_borang={$uk.ID_AKREDITASI_BORANG}"><b style="color: green">Edit</b></a>
						{if $uk.RELASI == 0}
							<a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="master-borang.php?mode=delete&id_borang={$uk.ID_AKREDITASI_BORANG}"><b style="color: red">Delete</b></a>
						{/if}
					</td>
				</tr>
			{/foreach}
			<tr>
				<td colspan="9" class="center">
					<a href="master-borang.php?mode=add"><b style="color: red">Tambah Borang Prodi</b></a>
				</td>
			</tr>
		</tbody>
	</table>
{/if}