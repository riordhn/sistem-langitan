<div class="center_title_bar">Tambah Dokumen</div>

{if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

<form action="master-dokumen.php?{$smarty.server.QUERY_STRING}" method="post">
<input type="hidden" name="mode" value="add" />
<!--<input type="hidden" name="id_role" value="{$smarty.get.id_role}" />
<input type="hidden" name="id_join_table" value="{$pengguna.JOIN_TABLE}" /> -->
<table>
    <tr>
        <th colspan="2">Detail Dokumen</th>
    </tr>
    <tr>
        <td>Kode Katalog</td>
        <td><input class="form-control" id="kode_katalog" type="text" name="kode_katalog" value="" size="75" />
        </td>
    </tr>
    <tr>
        <td>Nama Dokumen</td>
        <td><input class="form-control" id="nm_arsip_dokumen" type="text" name="nm_arsip_dokumen" value="" size="75" />
        </td>
    </tr>
    <tr>
        <td>Pemilik</td>
        <td>
            <select name="id_arsip_pemilik">
                <option value="0">-- Tidak Ada --</option>
            {foreach $arsip_pemilik_set as $pemilik}
                <option value="{$pemilik.ID_ARSIP_PEMILIK}">{$pemilik.NM_ARSIP_PEMILIK}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Tgl Penyusunan</td>
        <td><input class="form-control" id="tgl_penyusunan" type="text" name="tgl_penyusunan" value="" size="75" />
        </td>
    </tr>
    <tr>
        <td>Unit Kerja</td>
        <td>
            <select name="id_unit_kerja">
                <option value="0">-- Tidak Ada --</option>
            {foreach $unit_kerja_set as $uk}
                <option value="{$uk.ID_UNIT_KERJA}">{$uk.NM_JENJANG} {$uk.NM_UNIT_KERJA}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Almari / Loker</td>
        <td>
            <select name="id_arsip_loker">
                <option value="0">-- Tidak Ada --</option>
            {foreach $arsip_loker_set as $loker}
                <option value="{$loker.ID_ARSIP_LOKER}">{$loker.NM_ARSIP_LOKER}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Jumlah Halaman</td>
        <td><input class="form-control" id="jml_halaman" type="text" name="jml_halaman" value="" size="75" />
        </td>
    </tr>
    <tr>
        <td>Contact Person</td>
        <td><input class="form-control" id="contact_person" type="text" name="contact_person" value="" size="75" />
        </td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>

<a href="master-dokumen.php">Kembali</a>