{literal}
    <script language="javascript" type="text/javascript">

        var popupWindow = null;
        function popup(mypage, myname, w, h, pos, infocus) {
            if (pos == "random")
            {
                LeftPosition = (screen.width) ? Math.floor(Math.random() * (screen.width - w)) : 100;
                TopPosition = (screen.height) ? Math.floor(Math.random() * ((screen.height - h) - 75)) : 100;
            }
            else
            {
                LeftPosition = (screen.width) ? (screen.width - w) / 2 : 100;
                TopPosition = (screen.height) ? (screen.height - h) / 2 : 100;
            }
            settings = "width=" + w + ",height=" + h + ",top=" + TopPosition + ",left=" + LeftPosition + ",scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no";
            popupWindow = window.open("", myname, settings);
            if (infocus == "front") {
                popupWindow.focus();
                popupWindow.location = mypage;
            }
            if (infocus == "back") {
                popupWindow.blur();
                popupWindow.location = mypage;
                popupWindow.blur();
            }
        }

    </script>
{/literal}

<div class="center_title_bar">ID Dokumen : {$arsip_dokumen.ID_ARSIP_DOKUMEN}</div>

{if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

<form action="master-dokumen.php?{$smarty.server.QUERY_STRING}" method="post">
<input type="hidden" name="mode" value="edit" />
<input type="hidden" name="id_arsip_dokumen" value="{$arsip_dokumen.ID_ARSIP_DOKUMEN}" />
<!--<input type="hidden" name="id_join_table" value="{$pengguna.JOIN_TABLE}" /> -->
<table>
    <tr>
        <th colspan="2">Detail Dokumen</th>
    </tr>
    <tr>
        <td>ID</td>
        <td>{$arsip_dokumen.ID_ARSIP_DOKUMEN}</td>
    </tr>
    <tr>
        <td>Upload Scan Photo</td>
        <td><img src="{$PHOTO}" border="0" width="100" /><br/><br/><input type="button" name="ganti_photo" value="Ganti Photo" onclick="javascript:popup('{$IMG}', 'name', '600', '400', 'center', 'front')">
        </td>
    </tr>
    <tr>
        <td>Kode Katalog</td>
        <td><input class="form-control" id="kode_katalog" type="text" name="kode_katalog" value="{$arsip_dokumen.KODE_KATALOG}" size="75" />
        </td>
    </tr>
    <tr>
        <td>Nama Dokumen</td>
        <td><input class="form-control" id="nm_arsip_dokumen" type="text" name="nm_arsip_dokumen" value="{$arsip_dokumen.NM_ARSIP_DOKUMEN}" size="75" />
        </td>
    </tr>
    <tr>
        <td>Pemilik</td>
        <td>
            <select name="id_arsip_pemilik">
                <option value="0">-- Tidak Ada --</option>
            {foreach $arsip_pemilik_set as $pemilik}
                <option value="{$pemilik.ID_ARSIP_PEMILIK}" {if $pemilik.ID_ARSIP_PEMILIK == $arsip_dokumen.ID_ARSIP_PEMILIK}selected="selected"{/if}>{$pemilik.NM_ARSIP_PEMILIK}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Tgl Penyusunan</td>
        <td><input class="form-control" id="tgl_penyusunan" type="text" name="tgl_penyusunan" value="{$arsip_dokumen.TGL_PENYUSUNAN}" size="75" />
        </td>
    </tr>
    <tr>
        <td>Unit Kerja</td>
        <td>
            <select name="id_unit_kerja">
                <option value="0">-- Tidak Ada --</option>
            {foreach $unit_kerja_set as $uk}
                <option value="{$uk.ID_UNIT_KERJA}" {if $uk.ID_UNIT_KERJA == $arsip_dokumen.ID_UNIT_KERJA}selected="selected"{/if}>{$uk.NM_JENJANG} {$uk.NM_UNIT_KERJA}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Almari / Loker</td>
        <td>
            <select name="id_arsip_loker">
                <option value="0">-- Tidak Ada --</option>
            {foreach $arsip_loker_set as $loker}
                <option value="{$loker.ID_ARSIP_LOKER}" {if $loker.ID_ARSIP_LOKER == $arsip_dokumen.ID_ARSIP_LOKER}selected="selected"{/if}>{$loker.NM_ARSIP_LOKER}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Jumlah Halaman</td>
        <td><input class="form-control" id="jml_halaman" type="text" name="jml_halaman" value="{$arsip_dokumen.JML_HALAMAN}" size="75" />
        </td>
    </tr>
    <tr>
        <td>Contact Person</td>
        <td><input class="form-control" id="contact_person" type="text" name="contact_person" value="{$arsip_dokumen.CONTACT_PERSON}" size="75" />
        </td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>

<a href="master-dokumen.php">Kembali</a>