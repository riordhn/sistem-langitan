<div class="center_title_bar">Penilaian Borang</div>

{if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

{if isset($penilaian_borang_set)}
	<table class="tableabout">
		<thead>
			<tr>
				<th>No</th>
				<th>Nama Akreditasi Borang</th>
				<th>Tahun Penilaian</th>
				<th>Tanggal Penilaian</th>
				<th>Tanggal Verifikasi</th>
				<th>Nama Asesor</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			{foreach $penilaian_borang_set as $uk}
				<tr {if $uk@index is not div by 2}class="odd"{/if}>
					<td class="center">{$uk@index + 1}</td>
					<td>{$uk.NM_AKREDITASI_BORANG}</td>
					<td>{$uk.THN_PENILAIAN_BORANG}</td>
					<td>{$uk.TGL_PENILAIAN_BORANG}</td>
					<td>{$uk.TGL_VERIFIKASI_BORANG}</td>
					<td>{$uk.NM_ASESOR}</td>
					<td class="center">
						<a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="penilaian-borang.php?mode=detail&id_penilaian_borang={$uk.ID_AKREDITASI_PEN_BORANG}"><b style="color: green">Detail</b></a>
						{if $uk.RELASI == 0}
							<a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="penilaian-borang.php?mode=delete&id_penilaian_borang={$uk.ID_AKREDITASI_PEN_BORANG}"><b style="color: red">Delete</b></a>
						{/if}
					</td>
				</tr>
			{/foreach}
			<tr>
				<td colspan="9" class="center">
					<a href="penilaian-borang.php?mode=add"><b style="color: red">Tambah Penilaian Borang</b></a>
				</td>
			</tr>
		</tbody>
	</table>
{/if}