{literal}
    <script language="javascript" type="text/javascript">

        var popupWindow = null;
        function popup(mypage, myname, w, h, pos, infocus) {
            if (pos == "random")
            {
                LeftPosition = (screen.width) ? Math.floor(Math.random() * (screen.width - w)) : 100;
                TopPosition = (screen.height) ? Math.floor(Math.random() * ((screen.height - h) - 75)) : 100;
            }
            else
            {
                LeftPosition = (screen.width) ? (screen.width - w) / 2 : 100;
                TopPosition = (screen.height) ? (screen.height - h) / 2 : 100;
            }
            settings = "width=" + w + ",height=" + h + ",top=" + TopPosition + ",left=" + LeftPosition + ",scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no";
            popupWindow = window.open("", myname, settings);
            if (infocus == "front") {
                popupWindow.focus();
                popupWindow.location = mypage;
            }
            if (infocus == "back") {
                popupWindow.blur();
                popupWindow.location = mypage;
                popupWindow.blur();
            }
        }

    </script>
{/literal}

<div class="center_title_bar">File Penilaian Aspek Borang</div>

{if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

<form action="penilaian-borang.php?{$smarty.server.QUERY_STRING}" method="post">
	<input type="hidden" name="mode" value="add_nm_file" />
	<input type="hidden" name="id_aspek_borang" value="{$aspek_set.ID_AKREDITASI_PEN_ASPEK_BORANG}" />

	<table>
	    <tr>
	        <th colspan="2"><center>Penilaian Aspek Borang</center></th>
	    </tr>
	    <tr>
	        <td>Nama Aspek Borang</td>
	        <td>{$aspek_set.NM_AKREDITASI_ASPEK_BORANG}
	        </td>
	    </tr>
	    <tr>
	        <td>Nama Dokumen</td>
	        <td>
	            <select name="id_dokumen">
	                <option value="">Pilih Dokumen</option>
	                {foreach $dokumen_set as $data}
	                    <option value="{$data.ID_ARSIP_DOKUMEN}">{$data.KODE_KATALOG} - {$data.NM_ARSIP_DOKUMEN}</option>
	                {/foreach}
	            </select>
	        </td>
    	</tr>
	    <tr>
	        <td colspan="2" class="center">
	            <input type="submit" value="Simpan" />
	        </td>
	    </tr>
	</table>
</form>

{if isset($file_aspek_set)}
	<table class="tableabout" style="width: 100%">
		<thead>
			<tr>
				<th>No</th>
				<th>Kode Katalog</th>
				<th>Nama Dokumen</th>
				<th><center>Aksi</center></th>
			</tr>
		</thead>
		<tbody>
			{foreach $file_aspek_set as $uk}
				<tr {if $uk@index is not div by 2}class="odd"{/if}>
					<td class="center" width="10%">{$uk@index + 1}</td>
					<td width="20%"><strong>{$uk.KODE_KATALOG}</strong></td>
					<td width="30%">{$uk.NM_ARSIP_DOKUMEN}</td>
					<td class="center" width="40%">
						{assign var="image" 
						value="../../files/arsiparis/{$uk.ID_ARSIP_DOKUMEN}.JPG"} 
						{if file_exists($image)}
							<img src="../../files/arsiparis/{$uk.ID_ARSIP_DOKUMEN}.JPG?t=timestamp" border="0" height="50" width="150"/>
						{else}
							<img src="includes/images/cancel.png" border="0" width="50" />
						{/if}
							<br/><br/>
							<a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="penilaian-borang.php?mode=edit_file&id_file_aspek={$uk.ID_AKREDITASI_PEN_FILE_ASPEK}"><b style="color: green">Edit</b></a>
							<a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="penilaian-borang.php?mode=delete_file&id_file_aspek={$uk.ID_AKREDITASI_PEN_FILE_ASPEK}"><b style="color: red">Delete</b></a>
						<!--<img src="{$PHOTO}" border="0" width="100" /> <br/><br/><input type="button" name="ganti_photo" value="Ganti Photo" onclick="javascript:popup('{$IMG}', 'name', '600', '400', 'center', 'front')"> -->
					</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
{/if}

<a href="penilaian-borang.php?mode=detail&id_penilaian_borang={$aspek_set.ID_AKREDITASI_PEN_BORANG}">Kembali</a>