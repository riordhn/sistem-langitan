<div class="center_title_bar">ID File Aspek : {$file_aspek.ID_AKREDITASI_PEN_FILE_ASPEK}</div>

{if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

<form action="penilaian-borang.php?mode=upload&id_pen_aspek={$file_aspek.ID_AKREDITASI_PEN_ASPEK_BORANG}" method="post">
<input type="hidden" name="mode" value="delete_file" />
<input type="hidden" name="id_file_aspek" value="{$file_aspek.ID_AKREDITASI_PEN_FILE_ASPEK}" />
<!--<input type="hidden" name="id_join_table" value="{$pengguna.JOIN_TABLE}" /> -->
<table>
    <tr>
        <th colspan="3">Detail File Aspek</th>
    </tr>
    <tr>
        <td>Kode Katalog</td>
        <td>:</td>
        <td>{$file_aspek.KODE_KATALOG}</td>
    </tr>
    <tr>
        <td>Nama Dokumen </td>
        <td>:</td>
        <td>{$file_aspek.NM_ARSIP_DOKUMEN}</td>
    </tr>
    <tr>
        <td>Scan File</td>
        <td>:</td>
        <td>
                        {assign var="image" 
                        value="../../files/arsiparis/{$file_aspek.ID_ARSIP_DOKUMEN}.JPG"} 
                        {if file_exists($image)}
                            <img src="../../files/arsiparis/{$file_aspek.ID_ARSIP_DOKUMEN}.JPG?t=timestamp" border="0" height="50" width="150"/>
                        {else}
                            <img src="includes/images/cancel.png" border="0" width="50" />
                        {/if}
        </td>
    </tr>
    <tr>
        <td colspan="3" class="center">

            <a href="penilaian-borang.php?mode=upload&id_pen_aspek={$file_aspek.ID_AKREDITASI_PEN_ASPEK_BORANG}">Kembali</a>
            <input type="submit" value="Hapus" />
        </td>
    </tr>
</table>
</form>

<!--<form action="account-search.php?{$smarty.server.QUERY_STRING}" method="post">
<input type="hidden" name="mode" value="ubahPassword" />
<input type="hidden" name="id_pengguna" value="{$pengguna.ID_PENGGUNA}" />
<input type="hidden" name="username" value="{$pengguna.USERNAME}" />
<table>
    <tr>
        <th colspan="2" class="center">Reset Password?</th>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <input type="submit" value="Reset" />
        </td>
    </tr>
</table>
</form>

<script type="text/javascript">
    function deleteRolePengguna(id_role_pengguna)
    {
        if (confirm('Apakah role ini akan di hapus') == true)
        {
            $.ajax({
                type: 'POST',
                url: 'account-search.php',
                data: 'mode=delete-role&id_role_pengguna='+id_role_pengguna,
                success: function(data) {
                    if (data == '1') {
                        $('#r'+id_role_pengguna).remove();
                    }
                    else {
                        alert('Gagal dihapus');
                    }  
                }
            });
        }
    }
</script>
            
<table>
    <tr>
        <th>Default</th>
        <th>Role</th>
        <th>Template</th>
        <th>Action</th>
    </tr>
    {foreach $role_pengguna_set as $rp}
    <tr {if $rp@index is not div by 2}class="row1"{/if} id="r{$rp.ID_ROLE_PENGGUNA}">
        <td class="center"><input type="radio" name="id_role" value="{$rp.ID_ROLE}" {if $rp.ID_ROLE == $pengguna.ID_ROLE}checked="checked"{/if} disabled="disabled"/></td>
        <td>{$rp.NM_ROLE}</td>
        <td>{if $rp.NM_TEMPLATE == ''}Default{else}{$rp.NM_TEMPLATE}{/if}</td>
        <td>
            <a href="account-search.php?mode=edit-role&id_role_pengguna={$rp.ID_ROLE_PENGGUNA}" title="Edit"><img src="{$base_url}img/superadmin/navigation/edit.png" /></a>
            {if $rp.ID_ROLE != $pengguna.ID_ROLE}
            <img src="{$base_url}img/superadmin/navigation/del-red.png" title="Hapus Role" style="cursor: pointer" onclick='deleteRolePengguna({$rp.ID_ROLE_PENGGUNA})' />
            {/if}
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="4" class="center">
            <a href="account-search.php?mode=add-role&id_pengguna={$pengguna.ID_PENGGUNA}&q={$smarty.get.q}">Tambah</a>
        </td>
    </tr>
</table>
-->
