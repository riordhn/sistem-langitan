<div class="center_title_bar">ID Penilaian Borang : {$penilaian_borang.ID_AKREDITASI_PEN_BORANG}</div>

{if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

<form action="penilaian-borang.php?{$smarty.server.QUERY_STRING}" method="post">
<input type="hidden" name="mode" value="edit" />
<input type="hidden" name="id_penilaian_borang" value="{$penilaian_borang.ID_AKREDITASI_PEN_BORANG}" />
<!--<input type="hidden" name="id_join_table" value="{$pengguna.JOIN_TABLE}" /> -->
<table>
    <tr>
        <th colspan="2">Penilaian Borang</th>
    </tr>
    <!-- <tr>
        <td>ID</td>
        <td>{$penilaian_borang.ID_AKREDITASI_PEN_BORANG}</td>
    </tr> -->
    <tr>
        <td>Nama Borang</td>
        <td>
            <input type="hidden" name="id_borang" value="{$penilaian_borang.ID_AKREDITASI_BORANG}">
            <select name="id_borang" disabled>
                {foreach $borang_set as $data}
                    <option value="{$data.ID_AKREDITASI_BORANG}" {if $penilaian_borang.ID_AKREDITASI_BORANG==$data.ID_AKREDITASI_BORANG}selected="true"{/if}>{$data.NM_AKREDITASI_BORANG}</option>
                {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Tanggal Penilaian Borang</td>
        <td><input class="form-control" type="text" name="tgl_penilaian" value="{$penilaian_borang.TGL_PENILAIAN_BORANG|date_format:"%Y-%m-%d"}" /> *) Diisi format tgl. Misal : 2016-08-01
        </td>
    </tr>
    <tr>
        <td>Tanggal Verifikasi Borang</td>
        <td><input class="form-control" type="text" name="tgl_verifikasi" value="{$penilaian_borang.TGL_VERIFIKASI_BORANG|date_format:"%Y-%m-%d"}" /> *) Diisi format tgl. Misal : 2016-08-01
        </td>
    </tr>
    <tr>
        <td>Nama Asesor</td>
        <td><input class="form-control" type="text" name="nm_asesor" value="{$penilaian_borang.NM_ASESOR}" />
        </td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <input type="submit" value="Edit" />
        </td>
    </tr>
</table>
</form>

<form action="penilaian-borang.php?{$smarty.server.QUERY_STRING}" method="post">
<input type="hidden" name="mode" value="add_pen_aspek" />
<input type="hidden" name="id_penilaian_borang" value="{$penilaian_borang.ID_AKREDITASI_PEN_BORANG}" />
<!--<input type="hidden" name="id_join_table" value="{$pengguna.JOIN_TABLE}" /> -->
    <table class="tableabout">
        <thead>
            <tr>
                <th rowspan="2"><center>No</center></th>
                <th rowspan="2"><center>Kode Aspek Borang</center></th>
                <th rowspan="2"><center>Nama Aspek Borang</center></th>
                <th rowspan="2"><center>Informasi Penilaian</center></th>
                <th colspan="3"><center>Perkiraan</center></th>
                <th colspan="3"><center>Hasil</center></th>
            </tr>
            <tr>
                <th>Bobot</th>
                <th>Nilai</th>
                <th>Skor</th>
                <th>Bobot</th>
                <th>Nilai</th>
                <th>Skor</th>
            </tr>
        </thead>
        <tbody>
            {foreach $aspek_borang as $uk}
                <tr {if $uk@index is not div by 2}class="odd"{/if}>
                    <td class="center">{$uk@index + 1}<input type="hidden" name="id_pen_aspek_borang[{$uk.ID_ASPEK}]" value="{$uk.ID_AKREDITASI_PEN_ASPEK_BORANG}" /></td>
                    <td width="8%">{$uk.KODE_ASPEK_BORANG}<input type="hidden" name="id_aspek_borang[]" value="{$uk.ID_ASPEK}" /></td>
                    <td width="20%">{$uk.NM_ASPEK_BORANG}</td>
                    <td>
                        <textarea rows="2" cols="25" name="informasi_penilaian[{$uk.ID_ASPEK}]">{$uk.INFORMASI_PENILAIAN_ASPEK}</textarea>
                        {if $uk.ID_AKREDITASI_PEN_ASPEK_BORANG != ''}
                            <a href="penilaian-borang.php?mode=upload&id_pen_aspek={$uk.ID_AKREDITASI_PEN_ASPEK_BORANG}">Tambah File Pendukung</a>
                        {/if}
                    </td>
                    <td><input class="form-control" size="2" type="text" name="bobot_perkiraan[{$uk.ID_ASPEK}]" value="{$uk.BOBOT_PERKIRAAN}" /></td>
                    <td><input class="form-control" size="2" type="text" name="nilai_perkiraan[{$uk.ID_ASPEK}]" value="{$uk.NILAI_PERKIRAAN}" /></td>
                    <td><input class="form-control" size="3" type="text" name="skor_perkiraan[{$uk.ID_ASPEK}]" value="{$uk.SKOR_PERKIRAAN}" readonly /></td>
                    <td><input class="form-control" size="2" type="text" name="bobot_hasil[{$uk.ID_ASPEK}]" value="{$uk.BOBOT_HASIL}" /></td>
                    <td><input class="form-control" size="2" type="text" name="nilai_hasil[{$uk.ID_ASPEK}]" value="{$uk.NILAI_HASIL}" /></td>
                    <td><input class="form-control" size="3" type="text" name="skor_hasil[{$uk.ID_ASPEK}]" value="{$uk.SKOR_HASIL}" readonly /></td>
                </tr>
            {/foreach}
            <tr>
                <td colspan="10" class="center">
                    <input type="submit" value="Simpan" />
                </td>
            </tr>
        </tbody>
    </table>
</form>

<a href="penilaian-borang.php">Kembali</a>