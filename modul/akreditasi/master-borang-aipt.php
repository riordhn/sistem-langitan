<?php
ini_set("display_errors", 1);
include 'config.php';
include '../pendidikan/class/report_mahasiswa.class.php';

$mode = get('mode', 'view');
$id_pt = $id_pt_user;

$rm = new report_mahasiswa($db);

if ($request_method == 'POST')
{
    if (post('mode') == 'edit')
    {
        $id_borang = post('id_borang');
        $id_program_studi = post('program_studi');
        $nm_borang = post('nm_borang');
        $thn_borang = post('thn_borang');
        
        if($id_program_studi == ''){
            $result = $db->Query("update akreditasi_borang set id_program_studi = '{$id_program_studi}', nm_akreditasi_borang = '{$nm_borang}',
                                thn_akreditasi_borang = '{$thn_borang}', is_aipt = 1
                                where id_akreditasi_borang = '{$id_borang}'");
        }
        else{
            $result = $db->Query("update akreditasi_borang set id_program_studi = '{$id_program_studi}', nm_akreditasi_borang = '{$nm_borang}',
                                thn_akreditasi_borang = '{$thn_borang}'
                                where id_akreditasi_borang = '{$id_borang}'");
        }

        $smarty->assign('edited', $result ? "Data berhasil dirubah" : "Data gagal dirubah");
    }

    if (post('mode') == 'add')
    {
        //$id_borang = post('id_borang');
        $id_program_studi = post('program_studi');
        $nm_borang = post('nm_borang');
        $thn_borang = post('thn_borang');
        
        if($id_program_studi == ''){
            $result = $db->Query("insert into akreditasi_borang (nm_akreditasi_borang, thn_akreditasi_borang, is_aipt, id_perguruan_tinggi) 
            						values ('{$nm_borang}', '{$thn_borang}', '1', '{$id_pt}')");
        }
        else{
            $result = $db->Query("insert into akreditasi_borang (id_program_studi, nm_akreditasi_borang, thn_akreditasi_borang, id_perguruan_tinggi) 
                                    values ('{$id_program_studi}', '{$nm_borang}', '{$thn_borang}', '{$id_pt}')");
        }

        $smarty->assign('edited', $result ? "Data berhasil dimasukkan" : "Data gagal dimasukkan");
    }

    if (post('mode') == 'delete')
    {
        $id_borang = post('id_borang');
        
        $result = $db->Query("delete from akreditasi_borang where id_akreditasi_borang = '{$id_borang}'");

        $smarty->assign('edited', $result ? "Data berhasil dihapus" : "Data gagal dihapus");
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$borang_set = $db->QueryToArray("SELECT ID_AKREDITASI_BORANG, 
                                        (SELECT j.NM_JENJANG || ' - ' ||NM_PROGRAM_STUDI 
                                            FROM PROGRAM_STUDI JOIN JENJANG j ON j.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
                                            WHERE PROGRAM_STUDI.ID_PROGRAM_STUDI = AKREDITASI_BORANG.ID_PROGRAM_STUDI) AS NM_PROGRAM_STUDI, 
                                        NM_AKREDITASI_BORANG, THN_AKREDITASI_BORANG, IS_AIPT,
                                        (SELECT COUNT(*) FROM AKREDITASI_ASPEK_BORANG WHERE ID_AKREDITASI_BORANG=AKREDITASI_BORANG.ID_AKREDITASI_BORANG) RELASI
                                                FROM AKREDITASI_BORANG
                                                WHERE ID_PERGURUAN_TINGGI = {$id_pt} AND ID_PROGRAM_STUDI IS NULL
												ORDER BY NM_AKREDITASI_BORANG");
		$smarty->assignByRef('borang_set', $borang_set);

	}
	else if ($mode == 'edit' or $mode == 'delete')
	{
		$id_borang 	= (int)get('id_borang', '');

		$borang = $db->QueryToArray("
            select b.*, (select j.nm_jenjang || ' - ' || nm_program_studi from program_studi join jenjang j on j.id_jenjang = program_studi.id_jenjang where id_program_studi = b.id_program_studi) as nm_program_studi, ps.id_fakultas
            from akreditasi_borang b
            left join program_studi ps on ps.id_program_studi = b.id_program_studi
            where b.id_akreditasi_borang = {$id_borang}");
        $smarty->assign('borang', $borang[0]);

        $smarty->assign('data_fakultas', $rm->get_fakultas($id_pt));

        $program_studi = $db->QueryToArray("
            select id_program_studi, nm_program_studi, j.nm_jenjang 
            from program_studi ps 
            join jenjang j on j.id_jenjang = ps.id_jenjang
            join fakultas f on f.id_fakultas = ps.id_fakultas and f.id_fakultas = '{$borang[0]['ID_FAKULTAS']}'
            order by nm_program_studi");
        $smarty->assign('program_studi', $program_studi);


	}
	else if($mode == 'add')
	{
        $smarty->assign('data_fakultas', $rm->get_fakultas($id_pt));
	}
}

$smarty->display("master/borang-aipt/{$mode}.tpl");