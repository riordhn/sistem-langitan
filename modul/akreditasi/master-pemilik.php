<?php
ini_set("display_errors", 1);
include 'config.php';

$mode = get('mode', 'view');
$id_pt = $id_pt_user;

if ($request_method == 'POST')
{
    if (post('mode') == 'edit')
    {
        $id_arsip_pemilik = post('id_arsip_pemilik');
        $nm_arsip_pemilik = post('nm_arsip_pemilik');
        
        $result = $db->Query("update arsip_pemilik set nm_arsip_pemilik = '{$nm_arsip_pemilik}'
        						where id_arsip_pemilik = '{$id_arsip_pemilik}'");

        $smarty->assign('edited', $result ? "Data berhasil dirubah" : "Data gagal dirubah");
    }

    if (post('mode') == 'add')
    {
        //$id_arsip_pemilik = post('id_arsip_pemilik');
        $nm_arsip_pemilik = post('nm_arsip_pemilik');
        
        $result = $db->Query("insert into arsip_pemilik (nm_arsip_pemilik, id_perguruan_tinggi) 
        						values ('{$nm_arsip_pemilik}', '{$id_pt}')");

        $smarty->assign('edited', $result ? "Data berhasil dimasukkan" : "Data gagal dimasukkan");
    }

    if (post('mode') == 'delete')
    {
        $id_arsip_pemilik = post('id_arsip_pemilik');
        
        $result = $db->Query("delete from arsip_pemilik where id_arsip_pemilik = '{$id_arsip_pemilik}'");

        $smarty->assign('edited', $result ? "Data berhasil dihapus" : "Data gagal dihapus");
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$arsip_pemilik_set = $db->QueryToArray("SELECT ID_ARSIP_PEMILIK, NM_ARSIP_PEMILIK,
                                                -- mencari relasi ke tabel ARSIP_DOKUMEN
                                                (SELECT COUNT(*) FROM ARSIP_DOKUMEN WHERE ID_ARSIP_PEMILIK=ARSIP_PEMILIK.ID_ARSIP_PEMILIK) RELASI
                                                FROM ARSIP_PEMILIK
                                                WHERE ID_PERGURUAN_TINGGI = {$id_pt}
												ORDER BY NM_ARSIP_PEMILIK");
		$smarty->assignByRef('arsip_pemilik_set', $arsip_pemilik_set);

	}
	else if ($mode == 'edit' or $mode == 'delete')
	{
		$id_arsip_pemilik 	= (int)get('id_arsip_pemilik', '');

		$arsip_pemilik = $db->QueryToArray("
            select * 
            from arsip_pemilik 
            where id_arsip_pemilik = {$id_arsip_pemilik}");
        $smarty->assign('arsip_pemilik', $arsip_pemilik[0]);

	}
	else if($mode == 'add')
	{

	}
}

$smarty->display("master/pemilik/{$mode}.tpl");