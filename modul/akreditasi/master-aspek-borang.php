<?php
ini_set("display_errors", 1);
include 'config.php';

$mode = get('mode', 'view');
$id_pt = $id_pt_user;


if ($request_method == 'POST')
{
    if (post('mode') == 'edit')
    {
        $id_aspek_borang = post('id_aspek_borang');
        $id_borang = post('id_borang');
        $nm_aspek_borang = post('nm_aspek_borang');
        $kode_aspek_borang = post('kode_aspek_borang');
        
            $result = $db->Query("update akreditasi_aspek_borang set id_akreditasi_borang = '{$id_borang}', nm_aspek_borang = '{$nm_aspek_borang}',
                                kode_aspek_borang = '{$kode_aspek_borang}'
                                where id_akreditasi_aspek_borang = '{$id_aspek_borang}'");

        $smarty->assign('edited', $result ? "Data berhasil dirubah" : "Data gagal dirubah");
    }

    if (post('mode') == 'add')
    {
        //$id_aspek_borang = post('id_aspek_borang');
        $id_borang = post('id_borang');
        $nm_aspek_borang = post('nm_aspek_borang');
        $kode_aspek_borang = post('kode_aspek_borang');
        
            $result = $db->Query("insert into akreditasi_aspek_borang (id_akreditasi_borang, nm_aspek_borang, kode_aspek_borang) 
                                    values ('{$id_borang}', '{$nm_aspek_borang}', '{$kode_aspek_borang}')");

        $smarty->assign('edited', $result ? "Data berhasil dimasukkan" : "Data gagal dimasukkan");
    }

    if (post('mode') == 'delete')
    {
        $id_aspek_borang = post('id_aspek_borang');
        
        $result = $db->Query("delete from akreditasi_aspek_borang where id_akreditasi_aspek_borang = '{$id_aspek_borang}'");

        $smarty->assign('edited', $result ? "Data berhasil dihapus" : "Data gagal dihapus");
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$aspek_borang_set = $db->QueryToArray("SELECT ID_AKREDITASI_ASPEK_BORANG, 
                                        (SELECT NM_AKREDITASI_BORANG 
                                            FROM AKREDITASI_BORANG
                                            WHERE AKREDITASI_BORANG.ID_AKREDITASI_BORANG = AKREDITASI_ASPEK_BORANG.ID_AKREDITASI_BORANG) AS NM_AKREDITASI_BORANG, 
                                        NM_ASPEK_BORANG, KODE_ASPEK_BORANG, 
                                        (SELECT COUNT(*) FROM AKREDITASI_PEN_ASPEK_BORANG WHERE ID_AKREDITASI_ASPEK_BORANG=AKREDITASI_ASPEK_BORANG.ID_AKREDITASI_ASPEK_BORANG) RELASI
                                                FROM AKREDITASI_ASPEK_BORANG
                                                JOIN AKREDITASI_BORANG ab ON ab.ID_AKREDITASI_BORANG = AKREDITASI_ASPEK_BORANG.ID_AKREDITASI_BORANG
                                                WHERE ab.ID_PERGURUAN_TINGGI = {$id_pt}
												ORDER BY 2, KODE_ASPEK_BORANG, NM_ASPEK_BORANG");
		$smarty->assignByRef('aspek_borang_set', $aspek_borang_set);

	}
	else if ($mode == 'edit' or $mode == 'delete')
	{
		$id_aspek_borang 	= (int)get('id_aspek_borang', '');

		$aspek_borang = $db->QueryToArray("
            select b.*, (select nm_akreditasi_borang from akreditasi_borang where id_akreditasi_borang = b.id_akreditasi_borang) as nm_akreditasi_borang
            from akreditasi_aspek_borang b
            where b.id_akreditasi_aspek_borang = {$id_aspek_borang}");
        $smarty->assign('aspek_borang', $aspek_borang[0]);

        $borang_set = $db->QueryToArray("
            select *
            from akreditasi_borang
            where id_perguruan_tinggi = {$id_pt}");
        $smarty->assign('borang_set', $borang_set);

	}
	else if($mode == 'add')
	{
        $borang_set = $db->QueryToArray("
            select *
            from akreditasi_borang
            where id_perguruan_tinggi = {$id_pt}");
        $smarty->assign('borang_set', $borang_set);
	}
}

$smarty->display("master/aspek-borang/{$mode}.tpl");