<?php
ini_set("display_errors", 1);
include 'config.php';
include ('includes/encrypt.php');

$mode = get('mode', 'view');
$id_pt = $id_pt_user;

$depan = time();
$belakang = strrev(time());


if ($request_method == 'POST')
{
    if (post('mode') == 'edit')
    {
        $id_penilaian_borang = post('id_penilaian_borang');
        $id_borang = post('id_borang');
        $thn_borang = $db->QueryToArray("
            select thn_akreditasi_borang
            from akreditasi_borang
            where id_akreditasi_borang = {$id_borang}");
        $thn_penilaian = $thn_borang[0]['THN_AKREDITASI_BORANG'];
        $tgl_penilaian = post('tgl_penilaian');
        $tgl_verifikasi = post('tgl_verifikasi');
        $nm_asesor = post('nm_asesor');
        
            $result = $db->Query("update akreditasi_pen_borang set id_akreditasi_borang = '{$id_borang}', thn_penilaian_borang = '{$thn_penilaian}',
                                tgl_penilaian_borang = to_date('{$tgl_penilaian}','YYYY-MM-DD'), tgl_verifikasi_borang = to_date('{$tgl_verifikasi}','YYYY-MM-DD'), nm_asesor = '{$nm_asesor}'
                                where id_akreditasi_pen_borang = '{$id_penilaian_borang}'");

        $smarty->assign('edited', $result ? "Data berhasil dirubah" : "Data gagal dirubah");
    }

    if (post('mode') == 'add_pen_aspek')
    {
    	$id_penilaian_borang = post('id_penilaian_borang');

    	// mulai array
    	$id_pen_aspek_borang = post('id_pen_aspek_borang');
    	$id_aspek_borang = post('id_aspek_borang');
    	$informasi_penilaian = post('informasi_penilaian');
    	$bobot_perkiraan = post('bobot_perkiraan');
    	$nilai_perkiraan = post('nilai_perkiraan');
    	//$skor_perkiraan = post('skor_perkiraan');
    	$bobot_hasil = post('bobot_hasil');
    	$nilai_hasil = post('nilai_hasil');
    	//$skor_hasil = post('skor_hasil');

    	foreach ($id_aspek_borang as $id_aspek)
        {
        	if($id_pen_aspek_borang[$id_aspek] == ''){
        		$result = $db->Query("insert into akreditasi_pen_aspek_borang (id_akreditasi_pen_borang, id_akreditasi_aspek_borang, informasi_penilaian_aspek, bobot_perkiraan, nilai_perkiraan, skor_perkiraan, bobot_hasil, nilai_hasil, skor_hasil) 
                                    values ('{$id_penilaian_borang}', '{$id_aspek}', '".stripslashes($informasi_penilaian[$id_aspek])."', '".$bobot_perkiraan[$id_aspek]."', '".$nilai_perkiraan[$id_aspek]."', '".$bobot_perkiraan[$id_aspek]*$nilai_perkiraan[$id_aspek]."', '".$bobot_hasil[$id_aspek]."', '".$nilai_hasil[$id_aspek]."', '".$bobot_hasil[$id_aspek]*$nilai_hasil[$id_aspek]."' )");

        		/*echo "insert into akreditasi_pen_aspek_borang (id_akreditasi_pen_borang, id_akreditasi_aspek_borang, informasi_penilaian_aspek, bobot_perkiraan, nilai_perkiraan, skor_perkiraan, bobot_hasil, nilai_hasil, skor_hasil) 
                                    values ('{$id_penilaian_borang}', '{$id_aspek}', '".stripslashes($informasi_penilaian[$id_aspek])."', '".$bobot_perkiraan[$id_aspek]."', '".$nilai_perkiraan[$id_aspek]."', '".$bobot_perkiraan[$id_aspek]*$nilai_perkiraan[$id_aspek]."', '".$bobot_hasil[$id_aspek]."', '".$nilai_hasil[$id_aspek]."', '".$bobot_hasil[$id_aspek]*$nilai_hasil[$id_aspek]."' )";*/
        	}
        	else{
        		$result = $db->Query("update akreditasi_pen_aspek_borang set 
        			informasi_penilaian_aspek = '".stripslashes($informasi_penilaian[$id_aspek])."', 
        			bobot_perkiraan = '".$bobot_perkiraan[$id_aspek]."', nilai_perkiraan = '".$nilai_perkiraan[$id_aspek]."', skor_perkiraan = '".$bobot_perkiraan[$id_aspek]*$nilai_perkiraan[$id_aspek]."', 
        			bobot_hasil = '".$bobot_hasil[$id_aspek]."', nilai_hasil = '".$nilai_hasil[$id_aspek]."', skor_hasil = '".$bobot_hasil[$id_aspek]*$nilai_hasil[$id_aspek]."'
                                where id_akreditasi_pen_aspek_borang = '".$id_pen_aspek_borang[$id_aspek]."'");

        		/*echo "update akreditasi_pen_aspek_borang set 
        			informasi_penilaian_aspek = '".stripslashes($informasi_penilaian[$id_aspek])."', 
        			bobot_perkiraan = '".$bobot_perkiraan[$id_aspek]."', nilai_perkiraan = '".$nilai_perkiraan[$id_aspek]."', skor_perkiraan = '".$bobot_perkiraan[$id_aspek]*$nilai_perkiraan[$id_aspek]."', 
        			bobot_hasil = '".$bobot_hasil[$id_aspek]."', nilai_hasil = '".$nilai_hasil[$id_aspek]."', skor_hasil = '".$bobot_hasil[$id_aspek]*$nilai_hasil[$id_aspek]."'
                                where id_akreditasi_pen_aspek_borang = '".$id_pen_aspek_borang[$id_aspek]."'";*/
        	}
        }

        $smarty->assign('edited', $result ? "Data berhasil dimasukkan" : "Data gagal dimasukkan");
    }

    if (post('mode') == 'add')
    {
        //$id_aspek_borang = post('id_aspek_borang');
        $id_borang = post('id_borang');
        $thn_borang = $db->QueryToArray("
            select thn_akreditasi_borang
            from akreditasi_borang
            where id_akreditasi_borang = {$id_borang}");
        $thn_penilaian = $thn_borang[0]['THN_AKREDITASI_BORANG'];
        $tgl_penilaian = post('tgl_penilaian');
        $tgl_verifikasi = post('tgl_verifikasi');
        $nm_asesor = post('nm_asesor');
        
            $result = $db->Query("insert into akreditasi_pen_borang (id_akreditasi_borang, thn_penilaian_borang, tgl_penilaian_borang, tgl_verifikasi_borang, nm_asesor) 
                                    values ('{$id_borang}', '{$thn_penilaian}', to_date('{$tgl_penilaian}','YYYY-MM-DD') , to_date('{$tgl_verifikasi}','YYYY-MM-DD'), '{$nm_asesor}')");

        $smarty->assign('edited', $result ? "Data berhasil dimasukkan" : "Data gagal dimasukkan");
    }

    if (post('mode') == 'add_nm_file')
    {
        //$id_aspek_borang = post('id_aspek_borang');
        $id_aspek_borang = post('id_aspek_borang');
        $id_dokumen = post('id_dokumen');
        
            $result = $db->Query("insert into akreditasi_pen_file_aspek (id_akreditasi_pen_aspek_borang, id_arsip_dokumen) 
                                    values ('{$id_aspek_borang}', '{$id_dokumen}')");

        $smarty->assign('edited', $result ? "Data berhasil dimasukkan" : "Data gagal dimasukkan");
    }

    if (post('mode') == 'delete')
    {
        $id_aspek_borang = post('id_aspek_borang');
        
        $result = $db->Query("delete from akreditasi_aspek_borang where id_akreditasi_aspek_borang = '{$id_aspek_borang}'");

        $smarty->assign('edited', $result ? "Data berhasil dihapus" : "Data gagal dihapus");
    }

    if (post('mode') == 'edit_file')
    {
        $id_file_aspek = post('id_file_aspek');
        $id_dokumen = post('id_dokumen');
        
            $result = $db->Query("update akreditasi_pen_file_aspek set id_arsip_dokumen = '{$id_dokumen}'
	                                where id_akreditasi_pen_file_aspek = '{$id_file_aspek}'");

        $smarty->assign('edited', $result ? "Data berhasil dirubah" : "Data gagal dirubah");
    }

    if (post('mode') == 'delete_file')
    {
        $id_file_aspek = post('id_file_aspek');
        
        $result = $db->Query("delete from akreditasi_pen_file_aspek where id_akreditasi_pen_file_aspek = '{$id_file_aspek}'");

        $smarty->assign('edited', $result ? "Data berhasil dihapus" : "Data gagal dihapus");
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$penilaian_borang_set = $db->QueryToArray("SELECT ID_AKREDITASI_PEN_BORANG, 
                                        (SELECT NM_AKREDITASI_BORANG 
                                            FROM AKREDITASI_BORANG
                                            WHERE AKREDITASI_BORANG.ID_AKREDITASI_BORANG = AKREDITASI_PEN_BORANG.ID_AKREDITASI_BORANG) AS NM_AKREDITASI_BORANG,
                                            THN_PENILAIAN_BORANG, TGL_PENILAIAN_BORANG, TGL_VERIFIKASI_BORANG, NM_ASESOR
                                                FROM AKREDITASI_PEN_BORANG
                                                JOIN AKREDITASI_BORANG ab ON ab.ID_AKREDITASI_BORANG = AKREDITASI_PEN_BORANG.ID_AKREDITASI_BORANG
                                                WHERE ab.ID_PERGURUAN_TINGGI = {$id_pt}
												ORDER BY ID_AKREDITASI_PEN_BORANG");
		$smarty->assignByRef('penilaian_borang_set', $penilaian_borang_set);

	}
	else if ($mode == 'detail' or $mode == 'delete')
	{
		$id_penilaian_borang 	= (int)get('id_penilaian_borang', '');

		$penilaian_borang = $db->QueryToArray("
            select b.*, (select nm_akreditasi_borang from akreditasi_borang where id_akreditasi_borang = b.id_akreditasi_borang) as nm_akreditasi_borang
            from akreditasi_pen_borang b
            where b.id_akreditasi_pen_borang = {$id_penilaian_borang}");
        $smarty->assign('penilaian_borang', $penilaian_borang[0]);

        $borang_set = $db->QueryToArray("
            select *
            from akreditasi_borang
            where id_perguruan_tinggi = {$id_pt}");
        $smarty->assign('borang_set', $borang_set);

        $aspek_borang = $db->QueryToArray("
            select akreditasi_aspek_borang.*, apa.*, akreditasi_aspek_borang.id_akreditasi_aspek_borang as id_aspek
            from akreditasi_aspek_borang
            left join akreditasi_pen_aspek_borang apa on apa.id_akreditasi_aspek_borang = akreditasi_aspek_borang.id_akreditasi_aspek_borang
            where id_akreditasi_borang = '{$penilaian_borang[0]['ID_AKREDITASI_BORANG']}'
            order by kode_aspek_borang");
        $smarty->assign('aspek_borang', $aspek_borang);

	}
	else if($mode == 'add')
	{
        $borang_set = $db->QueryToArray("
            select *
            from akreditasi_borang
            where id_perguruan_tinggi = {$id_pt}");
        $smarty->assign('borang_set', $borang_set);
	}
	else if($mode == 'upload')
	{
		$id_pen_aspek 	= (int)get('id_pen_aspek', '');

		$pen_aspek_borang_set = $db->QueryToArray("
            select akreditasi_pen_aspek_borang.*, (select nm_aspek_borang from akreditasi_aspek_borang where id_akreditasi_aspek_borang = akreditasi_pen_aspek_borang.id_akreditasi_aspek_borang) as nm_akreditasi_aspek_borang
            from akreditasi_pen_aspek_borang
            where id_akreditasi_pen_aspek_borang = {$id_pen_aspek}");
        $smarty->assign('aspek_set', $pen_aspek_borang_set[0]);

        $file_aspek_set = $db->QueryToArray("
            select apf.*, ad.*, ad.id_arsip_dokumen as id_arsip
            from akreditasi_pen_file_aspek apf
            join arsip_dokumen ad on ad.id_arsip_dokumen = apf.id_arsip_dokumen
            where id_akreditasi_pen_aspek_borang = {$id_pen_aspek}");
        $smarty->assign('file_aspek_set', $file_aspek_set);

        $dokumen_set = $db->QueryToArray("
            select *
            from arsip_dokumen
            where id_perguruan_tinggi = {$id_pt}");
        $smarty->assign('dokumen_set', $dokumen_set);


        /*$to = $pen_aspek_borang_set[0]['NM_AKREDITASI_ASPEK_BORANG'];
        $file = $pen_aspek_borang_set[0]['ID_AKREDITASI_PEN_ASPEK_BORANG'];
        $img = 'proses/upload_photo.php?' . paramEncrypt($depan . '=' . $belakang . $depan . '&yth=' . $to . '&file=' . $file . '&' . $belakang . '=' . $depan . $belakang) . '';
        $smarty->assign('IMG', $img);*/

        //$get_photo = getvar("select username as photo from pengguna where id_pengguna=$id_pgg");
       /* $filename = "../../files/arsiparis/" . $pen_aspek_borang_set[0]['ID_AKREDITASI_PEN_ASPEK_BORANG'] . ".JPG";
        if (file_exists($filename)) {
            $photo = $filename.'?t=timestamp';
        } else {
            $photo = 'includes/images/cancel.png';
        }
        $smarty->assign('PHOTO', $photo);*/
	}

	else if ($mode == 'edit_file' or $mode == 'delete_file')
	{
		$id_file_aspek 	= (int)get('id_file_aspek', '');

		$file_aspek = $db->QueryToArray("
            select b.*, (select kode_katalog from arsip_dokumen where id_arsip_dokumen = b.id_arsip_dokumen) as kode_katalog,
            	(select nm_arsip_dokumen from arsip_dokumen where id_arsip_dokumen = b.id_arsip_dokumen) as nm_arsip_dokumen 
            from akreditasi_pen_file_aspek b
            where b.id_akreditasi_pen_file_aspek = {$id_file_aspek}");
        $smarty->assign('file_aspek', $file_aspek[0]);

        $dokumen_set = $db->QueryToArray("
            select *
            from arsip_dokumen
            where id_perguruan_tinggi = {$id_pt}");
        $smarty->assign('dokumen_set', $dokumen_set);

	}
}

$smarty->display("evaluasi/penilaian/{$mode}.tpl");