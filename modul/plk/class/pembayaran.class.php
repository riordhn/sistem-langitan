<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pembayaran
 *
 * @author NAMBISEMBILU
 */
class pembayaran {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    // Cek Pembayaran

    function get_detail_mahasiswa($nim) {
        $query = "
            SELECT M.*,P.NM_PENGGUNA,M.NIM_MHS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,F.NM_FAKULTAS,KB.NM_KELOMPOK_BIAYA,SP.NM_STATUS_PENGGUNA,JAL.NM_JALUR
            FROM MAHASISWA M
            JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            LEFT JOIN JALUR_MAHASISWA JM ON JM.ID_MHS=M.ID_MHS AND JM.ID_JALUR_AKTIF=1
            LEFT JOIN JALUR JAL ON JAL.ID_JALUR=JM.ID_JALUR
            JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
            LEFT JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA=M.ID_KELOMPOK_BIAYA
            LEFT JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA=M.STATUS_AKADEMIK_MHS
            WHERE M.NIM_MHS='{$nim}'
            ";
        $this->db->Query($query);
        return $this->db->FetchAssoc();
    }

    function rekap_bayar_mhs($nim) {
        $query = "
            SELECT PEM.*,SP.NAMA_STATUS,BNK.NM_BANK,BV.NAMA_BANK_VIA,B.NM_BIAYA,S.NM_SEMESTER,S.TAHUN_AJARAN,S.THN_AKADEMIK_SEMESTER
            FROM MAHASISWA M
            JOIN PEMBAYARAN PEM ON PEM.ID_MHS=M.ID_MHS
            JOIN STATUS_PEMBAYARAN SP ON SP.ID_STATUS_PEMBAYARAN=PEM.ID_STATUS_PEMBAYARAN
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PEM.ID_DETAIL_BIAYA
            JOIN BIAYA B ON B.ID_BIAYA=DB.ID_BIAYA
            JOIN SEMESTER S ON S.ID_SEMESTER=PEM.ID_SEMESTER
            LEFT JOIN BANK BNK ON BNK.ID_BANK=PEM.ID_BANK
            LEFT JOIN BANK_VIA BV ON BV.ID_BANK_VIA=PEM.ID_BANK_VIA
            WHERE M.NIM_MHS='{$nim}' AND B.ID_BIAYA IN (109,110)
            ORDER BY PEM.TGL_BAYAR DESC,S.TAHUN_AJARAN DESC,B.NM_BIAYA
            ";

        return $this->db->QueryToArray($query);
    }

    // Laporan Bayar per Biaya


    function load_laporan_bayar_perbiaya_detail($tgl_awal, $tgl_akhir, $biaya, $bank, $fakultas) {
        $q_fakultas = $fakultas != '' ? "AND F.ID_FAKULTAS='{$fakultas}'" : "";
        $q_bank = $bank == '' ? "" : "AND PEM.ID_BANK ='{$bank}'";
        return $this->db->QueryToArray("
            SELECT M.ID_MHS,M.NIM_MHS,M.THN_ANGKATAN_MHS,P.NM_PENGGUNA,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,F.ID_FAKULTAS,UPPER(F.NM_FAKULTAS) NM_FAKULTAS,PEM.TGL_BAYAR,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA ,
            SP.NM_STATUS_PENGGUNA STATUS,SP.STATUS_AKTIF AKTIF
                FROM PEMBAYARAN PEM
                JOIN MAHASISWA M ON M.ID_MHS = PEM.ID_MHS
                JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                LEFT JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
                JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
                JOIN JENJANG J ON J.ID_JENJANG  = PS.ID_JENJANG
                JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
            WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' 
            AND DB.ID_BIAYA='{$biaya}' {$q_bank} AND PEM.BESAR_BIAYA!=0 {$q_fakultas}
            GROUP BY M.ID_MHS,M.NIM_MHS,M.THN_ANGKATAN_MHS,P.NM_PENGGUNA,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,F.ID_FAKULTAS,F.NM_FAKULTAS,PEM.TGL_BAYAR,SP.NM_STATUS_PENGGUNA,SP.STATUS_AKTIF
            ORDER BY F.ID_FAKULTAS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,M.NIM_MHS");
    }

    function load_laporan_bayar_perbiaya_fakultas($tgl_awal, $tgl_akhir, $biaya, $bank) {
        $q_bank = $bank == '' ? "" : "AND PEM.ID_BANK ='{$bank}'";
        return $this->db->QueryToArray("
            SELECT F.ID_FAKULTAS,UPPER(F.NM_FAKULTAS) NM_FAKULTAS,PEM.JUMLAH_MHS,PEM.TOTAL_BIAYA FROM 
                (SELECT * FROM FAKULTAS) F
            LEFT JOIN 
                (
                    SELECT PS.ID_FAKULTAS,COUNT(DISTINCT(M.ID_MHS)) JUMLAH_MHS,SUM(PEM.BESAR_BIAYA) TOTAL_BIAYA 
                    FROM PEMBAYARAN PEM
                    JOIN MAHASISWA M ON PEM.ID_MHS = M.ID_MHS
                    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                    JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PEM.ID_DETAIL_BIAYA
                    WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' AND DB.ID_BIAYA='{$biaya}'
                    {$q_bank} AND PEM.BESAR_BIAYA!=0
                    GROUP BY PS.ID_FAKULTAS
                ) PEM 
            ON PEM.ID_FAKULTAS = F.ID_FAKULTAS");
    }
    
    
    // LAPORAN PENGEMBALIAN PEMBAYARAN
    
    function load_laporan_pengembalian_perbiaya_detail($tgl_awal, $tgl_akhir, $biaya, $fakultas) {
        $q_fakultas = $fakultas != '' ? "AND F.ID_FAKULTAS='{$fakultas}'" : "";
        return $this->db->QueryToArray("
            SELECT M.ID_MHS,M.NIM_MHS,M.THN_ANGKATAN_MHS,P.NM_PENGGUNA,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,F.ID_FAKULTAS,UPPER(F.NM_FAKULTAS) NM_FAKULTAS,PPEM.TGL_PENGEMBALIAN,SUM(PPEM.BESAR_PENGEMBALIAN) BESAR_PENGEMBALIAN ,
            SP.NM_STATUS_PENGGUNA STATUS,SP.STATUS_AKTIF AKTIF
                FROM PENGEMBALIAN_PEMBAYARAN PPEM
                JOIN MAHASISWA M ON M.ID_MHS = PPEM.ID_MHS
                JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                LEFT JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
                JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
                JOIN JENJANG J ON J.ID_JENJANG  = PS.ID_JENJANG
                JOIN PENGEMBALIAN_PEMBAYARAN_DETAIL PPD ON PPD.ID_PENGEMBALIAN_PEMBAYARAN = PPEM.ID_PENGEMBALIAN_PEMBAYARAN
            WHERE TO_CHAR(PPEM.TGL_PENGEMBALIAN,'YYYY-MM-DD') >= '{$tgl_awal}' AND TO_CHAR(PPEM.TGL_PENGEMBALIAN,'YYYY-MM-DD') <= '{$tgl_akhir}' 
            AND PPD.ID_BIAYA='{$biaya}' {$q_fakultas}
            GROUP BY M.ID_MHS,M.NIM_MHS,M.THN_ANGKATAN_MHS,P.NM_PENGGUNA,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,F.ID_FAKULTAS,F.NM_FAKULTAS,PPEM.TGL_PENGEMBALIAN,SP.NM_STATUS_PENGGUNA,SP.STATUS_AKTIF
            ORDER BY F.ID_FAKULTAS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,M.NIM_MHS");
    }

    function load_laporan_pengembalian_perbiaya_fakultas($tgl_awal, $tgl_akhir, $biaya) {
        return $this->db->QueryToArray("
            SELECT F.ID_FAKULTAS,UPPER(F.NM_FAKULTAS) NM_FAKULTAS,PPEM.JUMLAH_MHS,PPEM.TOTAL_PENGEMBALIAN FROM 
                (SELECT * FROM FAKULTAS) F
            LEFT JOIN 
                (
                    SELECT PS.ID_FAKULTAS,COUNT(DISTINCT(M.ID_MHS)) JUMLAH_MHS,SUM(PPEM.BESAR_PENGEMBALIAN) TOTAL_PENGEMBALIAN 
                    FROM PENGEMBALIAN_PEMBAYARAN PPEM
                    JOIN MAHASISWA M ON PPEM.ID_MHS = M.ID_MHS
                    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                    JOIN PENGEMBALIAN_PEMBAYARAN_DETAIL PPD ON PPD.ID_PENGEMBALIAN_PEMBAYARAN=PPEM.ID_PENGEMBALIAN_PEMBAYARAN
                    WHERE TO_CHAR(PPEM.TGL_PENGEMBALIAN,'YYYY-MM-DD') >= '{$tgl_awal}' AND TO_CHAR(PPEM.TGL_PENGEMBALIAN,'YYYY-MM-DD') <= '{$tgl_akhir}' AND PPD.ID_BIAYA='{$biaya}'
                    GROUP BY PS.ID_FAKULTAS
                ) PPEM 
            ON PPEM.ID_FAKULTAS = F.ID_FAKULTAS");
    }

}
