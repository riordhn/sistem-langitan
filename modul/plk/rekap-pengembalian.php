<?php

include 'config.php';
include "class/pembayaran.class.php";
include '../keuangan/class/list_data.class.php';
include '../keuangan/class/master.class.php';

$pembayaran = new pembayaran($db);
$list = new list_data($db);
$master = new master($db);

if (!empty($_GET)) {
    $fakultas = get('fakultas');
    $tgl_awal = get('tgl_awal');
    $tgl_akhir= get('tgl_akhir');
    $biaya= get('biaya');

    if (get('mode') == 'fakultas') {
        
        $smarty->assign('data_rekap', $pembayaran->load_laporan_pengembalian_perbiaya_fakultas($tgl_awal, $tgl_akhir, $biaya));
    } else if (get('mode') == 'detail') {
        
        $smarty->assign('data_mhs', $pembayaran->load_laporan_pengembalian_perbiaya_detail($tgl_awal, $tgl_akhir, $biaya, $fakultas));
    }
}

$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->assign('data_biaya', $list->load_biaya());

$smarty->display('rekap-pengembalian.tpl');
?>
