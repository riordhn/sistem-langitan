<?php

require('config.php');
include "class/pembayaran.class.php";
$pembayaran = new pembayaran($db);

if (!empty($_POST)) {
    if (post('mode') == 'cari') {
        $nim = post('nim');
        $smarty->assign('mhs', $pembayaran->get_detail_mahasiswa($nim));
        $smarty->assign('pembayaran', $pembayaran->rekap_bayar_mhs($nim));
    }
}

$smarty->display('pembayaran-cek.tpl');
?>
