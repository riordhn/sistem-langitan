<div class="center_title_bar">Pembayaran Asuransi Mahasiswa</div>  

<form method="post" action="pembayaran-cek.php">
    <table>
        <tr>
            <td>NIM</td>
            <td>
                <input type="text" name="nim" />
            </td>
            <td style="text-align:center">
                <input type="hidden" name="mode" value="cari"/>
                <input type="submit" value="Tampil" name="tampil" />
            </td>
        </tr>
    </table>
</form>
{if !empty($smarty.post.nim)}
    {if !empty($mhs)}
        <table>
            <tr>
                <th colspan="2">BIODATA</th>
            </tr>
            <tr>
                <td>NIM</td>
                <td>{$mhs['NIM_MHS']}</td>
            </tr>
            <tr>
                <td>NAMA</td>
                <td>{$mhs['NM_PENGGUNA']}</td>
            </tr>
            <tr>
                <td>ANGKATAN</td>
                <td>{$mhs['THN_ANGKATAN_MHS']}</td>
            </tr>
            <tr>
                <td>FAKULTAS</td>
                <td>{$mhs['NM_FAKULTAS']}</td>
            </tr>
            <tr>
                <td>PROGRAM STUDI</td>
                <td>{$mhs['NM_JENJANG']} {$mhs['NM_PROGRAM_STUDI']}</td>
            </tr>
            <tr>
                <td>JALUR</td>
                <td>{$mhs['NM_JALUR']}</td>
            </tr>
            <tr>
                <td>STATUS MAHASISWA</td>
                <td>{$mhs['NM_STATUS_PENGGUNA']}</td>
            </tr>
            <tr>
                <td>KELOMPOK BIAYA PEMBAYARAN</td>
                <td>{$mhs['NM_KELOMPOK_BIAYA']}</td>
            </tr>
        </table>

        <table>
            <tr>
                <th>Nama Biaya</th>
                <th>Besar Biaya</th>
                <th>Semester</th>
                <th>Tgl Bayar</th>
                <th>Bank</th>
                <th>Via Bank</th>
                <th>Status Pembayaran</th>
            </tr>
            {foreach $pembayaran as $data}
                <tr>
                    <td>{$data.NM_BIAYA}</td>
                    <td>{$data.BESAR_BIAYA}</td>
                    <td>{$data.NM_SEMESTER} ({$data.TAHUN_AJARAN})</td>
                    <td>{$data.TGL_BAYAR}</td>
                    <td>{$data.NM_BANK}</td>
                    <td>{$data.NAMA_BANK_VIA}</td>
                    <td>{$data.NAMA_STATUS}</td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="7"> Data Pembayaaran Asuransi Kosong</td>
                </tr>
            {/foreach}
        </table>
    {else}
        Data Tidak Ditemukan
    {/if}
{/if}