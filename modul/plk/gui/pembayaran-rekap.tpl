<div class="center_title_bar">Rekap Pembayaran Mahasiswa</div>  

<form method="get" id="report_form" action="pembayaran-rekap.php">
    <table class="ui-widget" style="width:98%">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td width="15%">Fakultas</td>
            <td width="25%">
                <select name="fakultas" id="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
            <td>Semester</td>
            <td>
                <select name="semester">
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER==$smarty.get.semester}selected="true"{/if}>{$data.NM_SEMESTER} ( {$data.TAHUN_AJARAN} )</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Status Bayar</td>
            <td colspan="3">
                <select name="status_bayar">
                    {foreach $data_status as $data}
                        <option value="{$data.ID_STATUS_PEMBAYARAN}" {if $smarty.get.status_bayar==$data.ID_STATUS_PEMBAYARAN} selected="true"{/if}>{$data.NAMA_STATUS}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="6"  style="text-align: center" >
                <input type="hidden" name="mode" value="fakultas" />
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if isset($data_pembayaran)}
    <table width="200" border="1"  class="ui-widget" style="width: 98%">
        {if isset($program_studi)}
            <tr class="ui-widget-content">
                <th>Program Studi</th>
                    {foreach $biaya as $data1}
                    <th>JUMLAH MHS - {$data1.NM_BIAYA}</th>
                    {/foreach}
                    {foreach $biaya as $data1}
                    <th>{$data1.NM_BIAYA}</th>
                    {/foreach}
                <th>Σ Per Program Studi</th>
            </tr>

            {foreach $program_studi as $data}
                <tr class="ui-widget-content"> 
                    <td>{$data.NM_JENJANG} {$data.NM_PROGRAM_STUDI}</td>
                    {$total = 0}
                    {foreach $biaya as $data2}
                        <td style="text-align:right">
                            {$nilai = false}
                            {$jumlah = 0}
                            {foreach $data_pembayaran as $x}
                                {if $x.ID_PROGRAM_STUDI == $data.ID_PROGRAM_STUDI&&$x.ID_BIAYA == $data2.ID_BIAYA}
                                    {$jumlah = $jumlah + $x.JML_MHS}
                                    {$nilai = true}
                                {/if}
                            {/foreach}
                            {if $nilai == true}
                                <a href="pembayaran-rekap.php?mode=detail&fakultas={$smarty.get.fakultas}&prodi={$data.ID_PROGRAM_STUDI}&semester={$id_semester}&biaya={$data2.ID_BIAYA}&status_bayar={$smarty.get.status_bayar}">{$jumlah}</a>
                                
                                {$total = $total + $jumlah}
                            {else}
                                0
                            {/if}
                        </td>
                    {/foreach}
                    {$total = 0}
                    {foreach $biaya as $data2}
                        <td style="text-align:right">
                            {$nilai = false}
                            {$jumlah = 0}
                            {foreach $data_pembayaran as $x}
                                {if $x.ID_PROGRAM_STUDI == $data.ID_PROGRAM_STUDI&&$x.ID_BIAYA == $data2.ID_BIAYA}
                                    {$jumlah = $jumlah + $x.JUMLAH}
                                    {$nilai = true}
                                {/if}
                            {/foreach}
                            {if $nilai == true}
                                {number_format($jumlah)}
                                {$total = $total + $jumlah}
                            {else}
                                0
                            {/if}
                        </td>
                    {/foreach}
                    <td style="text-align:right">{number_format($total)}</td>
                </tr>
            {/foreach}
            <tr class="total">
                <td>Total</td>
                {$total_all = 0}
                {foreach $biaya as $data1}
                    <td style="text-align:right"> 
                        {$nilai = false}
                        {$total = 0}
                        {foreach $data_pembayaran as $x}
                            {if $x.ID_BIAYA == $data1.ID_BIAYA}
                                {$total = $total + $x.JML_MHS}
                                {$nilai = true}
                            {/if}
                        {/foreach}
                        {if $nilai == false}
                            0
                        {else}
                            {$total}
                            {$total_all = $total_all + $total}
                        {/if}
                    </td>
                {/foreach}

                {$total_all = 0}
                {foreach $biaya as $data1}
                    <td style="text-align:right"> 
                        {$nilai = false}
                        {$total = 0}
                        {foreach $data_pembayaran as $x}
                            {if $x.ID_BIAYA == $data1.ID_BIAYA}
                                {$total = $total + $x.JUMLAH}
                                {$nilai = true}
                            {/if}
                        {/foreach}
                        {if $nilai == false}
                            0
                        {else}
                            {number_format($total)}
                            {$total_all = $total_all + $total}
                        {/if}
                    </td>
                {/foreach}
                <td style="text-align:right">{number_format($total_all)}</td>
            </tr>

        {else}
            <tr class="ui-widget-content">
                <th>Fakultas</th>
                    {foreach $biaya as $data1}
                    <th>JUMLAH MHS - {$data1.NM_BIAYA}</th>
                    {/foreach}
                    {foreach $biaya as $data1}
                    <th>{$data1.NM_BIAYA}</th>
                    {/foreach}
                <th>Σ Per Fakultas</th>
            </tr>

            {foreach $data_fakultas as $data}
                <tr class="ui-widget-content"> 
                    <td>{$data.NM_FAKULTAS}</td>
                    {$total = 0}
                    {$total_mhs = 0}
                    {foreach $biaya as $data2}
                        <td style="text-align:center">
                            {$nilai_mhs = false}
                            {$jumlah_mhs = 0}
                            {foreach $data_pembayaran as $x}
                                {if $x.ID_FAKULTAS == $data.ID_FAKULTAS&&$x.ID_BIAYA == $data2.ID_BIAYA}
                                    {$jumlah_mhs = $jumlah_mhs + $x.JML_MHS}
                                    {$nilai_mhs = true}
                                {/if}
                            {/foreach}
                            {if $nilai_mhs == true}
                                <a href="pembayaran-rekap.php?mode=detail&fakultas={$data.ID_FAKULTAS}&prodi=&semester={$id_semester}&biaya={$data2.ID_BIAYA}&status_bayar={$smarty.get.status_bayar}">{$jumlah_mhs}</a>
                                {$total_mhs = $total_mhs + $jumlah_mhs}
                            {else}
                                0
                            {/if}
                        </td>
                    {/foreach}
                    {foreach $biaya as $data2}
                        <td style="text-align:right">
                            {$nilai = false}
                            {$jumlah = 0}
                            {foreach $data_pembayaran as $x}
                                {if $x.ID_FAKULTAS == $data.ID_FAKULTAS&&$x.ID_BIAYA == $data2.ID_BIAYA}
                                    {$jumlah = $jumlah + $x.JUMLAH}
                                    {$nilai = true}
                                {/if}
                            {/foreach}
                            {if $nilai == true}
                                {number_format($jumlah)}
                                {$total = $total + $jumlah}
                            {else}
                                0
                            {/if}
                        </td>
                    {/foreach}
                    <td style="text-align:right">{number_format($total)}</td>
                </tr>
            {/foreach}
            <tr class="total">
                <td>Total</td>
                {$total_all = 0}
                {$total_all_mhs = 0}
                {foreach $biaya as $data1}
                    <td style="text-align:center"> 
                        {$nilai = false}
                        {$total = 0}
                        {foreach $data_pembayaran as $x}
                            {if $x.ID_BIAYA == $data1.ID_BIAYA}
                                {$total = $total + $x.JML_MHS}
                                {$nilai = true}
                            {/if}
                        {/foreach}
                        {if $nilai == false}
                            0
                        {else}
                            {$total}
                            {$total_all_mhs = $total_all_mhs + $total}
                        {/if}
                    </td>
                {/foreach}
                {foreach $biaya as $data1}
                    <td style="text-align:right"> 
                        {$nilai = false}
                        {$total = 0}
                        {foreach $data_pembayaran as $x}
                            {if $x.ID_BIAYA == $data1.ID_BIAYA}
                                {$total = $total + $x.JUMLAH}
                                {$nilai = true}
                            {/if}
                        {/foreach}
                        {if $nilai == false}
                            0
                        {else}
                            {number_format($total)}
                            {$total_all = $total_all + $total}
                        {/if}
                    </td>
                {/foreach}
                <td style="text-align:right">{number_format($total_all)}</td>
            </tr>
        {/if}
    </table>
{/if}


{if isset($data_mhs)}
    <a class="ui-button ui-corner-all ui-state-hover disable-ajax" style="padding:5px;margin-bottom: 10px;cursor:pointer;" onclick="history.back();">Kembali Kelaporan Rekap</a>
    <table width="200" border="1" class="ui-widget" style="width: 98%">
        <tr>
            <th>NO</th>
            <th>NIM</th>
            <th>NAMA</th>
            <th>PROGRAM STUDI</th>
            <th>NAMA BIAYA</th>
            <th>STATUS</th>
            <th>BESAR BIAYA</th>
        </tr>
        {$no = 1}
        {foreach $data_mhs as $data}
            <tr class="ui-widget-content">
                <td>{$no++}</td>
                <td>{$data.NIM_MHS}</td>
                <td>{$data.NM_PENGGUNA}</td>
                <td>{$data.NM_JENJANG} {$data.NM_PROGRAM_STUDI}</td>
                <td>{$data.NM_BIAYA}</td>
                <td>{$data.NM_STATUS_PENGGUNA}</td>
                <td>{$data.BESAR_BIAYA}</td>
            </tr>
        {/foreach}
    </table>
{/if}

{literal}
    <script>
        $('#fakultas').change(function() {
            $.ajax({
                type: 'post',
                url: '../keuangan/getProdi.php',
                data: 'id_fakultas=' + $(this).val(),
                success: function(data) {
                    $('#prodi').html(data);
                }
            })
        });
    </script>
{/literal}