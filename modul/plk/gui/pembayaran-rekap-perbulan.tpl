<div class="center_title_bar">Laporan Pembayaran Per Bulan</div> 
<div id="alert" class="ui-widget" style="display:none;margin-left:15px;">
    <div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;"> 
        <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
            Tanggal Harus Diisi.</p>
    </div>
</div>
<form method="get" id="report_form" action="pembayaran-rekap-perbulan.php">
    <table class="ui-widget" style="width:98%">
        <tr class="ui-widget-header">
            <th colspan="6" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Data Biaya</td>
            <td>
                <select name="biaya">
                    <option>Pilih Biaya</option>
                    {foreach $data_biaya as $data}
                        {if $data.ID_BIAYA==110||$data.ID_BIAYA==109}
                            <option value="{$data.ID_BIAYA}" {if $data.ID_BIAYA==$smarty.get.biaya}selected="true"{/if}>{$data.NM_BIAYA}</option>
                        {/if}
                    {/foreach}
                </select>
            </td>
            <td>Tanggal Awal</td>
            <td><input type="text" id="tgl_awal" class="datepicker"  name="tgl_awal" {if isset($tgl_awal)} value="{$tgl_awal}"{/if} /></td>
            <td>Tanggal Akhir</td>
            <td><input type="text" id="tgl_akhir" class="datepicker" class="required" name="tgl_akhir" {if isset($tgl_awal)} value="{$tgl_akhir}"{/if}/></td>
        </tr>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="6"  style="text-align: center" >
                <input type="hidden" name="mode" value="fakultas" />
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if isset($data_pew_detail)}
    <table class="ui-widget" style="width: 99%">
        <tr class="ui-widget-header">
            <th style="text-align: center"  colspan="8" class="header-coloumn">
                Laporan Pebayaran Per Biaya Mode Detail<br/>
                Bank {$data_bank_one.NM_BANK}<br/>
                {if isset($smarty.get.fakultas)}
                    FAKULTAS {$data_fakultas_one.NM_FAKULTAS}</br>
                {/if}
                Dari Tanggal {$smarty.get.tgl_awal} Sampai {$smarty.get.tgl_akhir}
            </th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>NIM</th>
            <th>NAMA</th>
            <th>Angkatan</th>
            <th>Prodi/Fakultas</th>
            <th>Status</th>
            <th>Tanggal Bayar</th>
            <th>Jumlah Pembayaran</th>
        </tr>
        {foreach $data_pew_detail as $data}
            <tr class="ui-widget-content">
                <td>{$data@index+1}</td>
                <td>{$data.NIM_MHS}</td>
                <td>{$data.NM_PENGGUNA}</td>
                <td>{$data.THN_ANGKATAN_MHS}</td>
                <td>
                    ({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI} <br/>
                    {$data.NM_FAKULTAS}
                </td>
                <td>
                    {$data.STATUS}
                </td>
                <td>{$data.TGL_BAYAR}</td>
                <td class="center">{number_format($data.BESAR_BIAYA)}</td>
            </tr>
            {$total_bayar=$total_bayar+$data.BESAR_BIAYA}
        {foreachelse}
            <tr class="ui-widget-content">
                <td colspan="8" style="color: red">Data Kosong</td>
            </tr>
        {/foreach}
        <tr class="total ui-widget-content">
            <td colspan="7" class="center">Total</td>
            <td class="center">{number_format($total_bayar)}</td>
        </tr>
    </table>
{else if isset($data_pew_fakultas)}
    <table class="ui-widget" style="width: 98%">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">
                Laporan Pebayaran Per Biaya Mode Fakultas<br/>
                Dari Tanggal {$smarty.get.tgl_awal} Sampai {$smarty.get.tgl_akhir}
            </th>
        </tr>
        <tr class="ui-widget-header">
            <th>Fakultas</th>
            <th>Jumlah Mahasiswa</th>
            <th>Jumlah Pembayaran</th>
            <th>Detail</th>
        </tr>
        {foreach $data_pew_fakultas as $data}
            <tr class="ui-widget-content">
                <td>{$data.NM_FAKULTAS}</td>
                <td class="center">
                    {if $data.JUMLAH_MHS!=''}
                        {$data.JUMLAH_MHS}
                    {else}
                        0
                    {/if}
                </td>
                <td class="center">
                    {if $data.TOTAL_BIAYA!=''}
                        {number_format($data.TOTAL_BIAYA)}
                    {else}
                        0
                    {/if}
                </td>
                <td class="center">
                    <a class="ui-button ui-corner-all ui-state-hover" href="pembayaran-rekap-perbulan.php?mode=detail&tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&bank={$smarty.get.bank}&biaya={$smarty.get.biaya}&fakultas={$data.ID_FAKULTAS}" style="padding:5px;cursor:pointer;">Detail</a>
                </td>
            </tr>
            {if $data.JUMLAH_MHS!=''}
                {$total_mahasiswa=$total_mahasiswa+$data.JUMLAH_MHS}
                {$total_bayar=$total_bayar+$data.TOTAL_BIAYA}
            {/if}
        {/foreach}
        <tr class="total ui-widget-content">
            <td class="center">Total</td>
            <td class="center">{$total_mahasiswa}</td>
            <td class="center">{number_format($total_bayar)}</td>
            <td></td>
        </tr>
    </table>
{/if}
{if $smarty.get.mode!='bank'&&isset($smarty.get.mode)}
    <span class="ui-button ui-corner-all ui-state-hover" style="margin-top:10px;padding:5px;cursor:pointer;" onclick="history.back(-1)">Kembali</span>
{/if}
{literal}
    <script>
        $('#report_form').submit(function() {
            if ($('#tgl_awal').val() == '' || $('#tgl_akhir').val() == '') {
                $('#alert').fadeIn();
                return false;
            }
        });
        $('#report_form').validate();
        $(".datepicker").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true,
            changeYear: true});
        $('#fakultas').change(function() {
            $.ajax({
                type: 'post',
                url: 'getProdi.php',
                data: 'id_fakultas=' + $(this).val(),
                success: function(data) {
                    $('#prodi').html(data);
                }
            })
        });
    </script>
{/literal}