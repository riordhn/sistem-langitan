<div class="center_title_bar">Laporan Pengembalian Pembayaran</div> 
<form method="get" id="report_form" action="rekap-pengembalian.php">
    <table class="ui-widget" style="width:98%">
        <tr class="ui-widget-header">
            <th colspan="6" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Data Biaya</td>
            <td>
                <select name="biaya">
                    <option>Pilih Biaya</option>
                    {foreach $data_biaya as $data}
                        {if $data.ID_BIAYA==110||$data.ID_BIAYA==109}
                            <option value="{$data.ID_BIAYA}" {if $data.ID_BIAYA==$smarty.get.biaya}selected="true"{/if}>{$data.NM_BIAYA}</option>
                        {/if}
                    {/foreach}
                </select>
            </td>
            <td>Tanggal Awal</td>
            <td><input type="text" id="tgl_awal" class="datepicker"  name="tgl_awal" {if isset($smarty.get.tgl_awal)} value="{$smarty.get.tgl_awal}"{/if} /></td>
            <td>Tanggal Akhir</td>
            <td><input type="text" id="tgl_akhir" class="datepicker" class="required" name="tgl_akhir" {if isset($smarty.get.tgl_akhir)} value="{$smarty.get.tgl_akhir}"{/if}/></td>
        </tr>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="6"  style="text-align: center" >
                <input type="hidden" name="mode" value="fakultas" />
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if isset($data_mhs)}
    <table class="ui-widget" style="width: 99%">
        <tr class="ui-widget-header">
            <th style="text-align: center"  colspan="8" class="header-coloumn">
                Laporan Pebayaran Per Biaya Mode Detail<br/>
                Dari Tanggal {$smarty.get.tgl_awal} Sampai {$smarty.get.tgl_akhir}
            </th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>NIM</th>
            <th>NAMA</th>
            <th>Angkatan</th>
            <th>Prodi/Fakultas</th>
            <th>Status</th>
            <th>Tanggal Pengembalian</th>
            <th>Jumlah Pengembalian</th>
        </tr>
        {foreach $data_mhs as $data}
            <tr class="ui-widget-content">
                <td>{$data@index+1}</td>
                <td>{$data.NIM_MHS}</td>
                <td>{$data.NM_PENGGUNA}</td>
                <td>{$data.THN_ANGKATAN_MHS}</td>
                <td>
                    ({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI} <br/>
                    {$data.NM_FAKULTAS}
                </td>
                <td>
                    {$data.STATUS}
                </td>
                <td>{$data.TGL_PENGEMBALIAN}</td>
                <td class="center">{number_format($data.BESAR_PENGEMBALIAN)}</td>
            </tr>
            {$total_bayar=$total_bayar+$data.BESAR_PENGEMBALIAN}
        {foreachelse}
            <tr class="ui-widget-content">
                <td colspan="8" style="color: red">Data Kosong</td>
            </tr>
        {/foreach}
        <tr class="total ui-widget-content">
            <td colspan="7" class="center">Total</td>
            <td class="center">{number_format($total_bayar)}</td>
        </tr>
    </table>
{else if isset($data_rekap)}
    <table class="ui-widget" style="width: 98%">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">
                Laporan Pengembalian Pebayaran Per Biaya by Fakultas<br/>
                Dari Tanggal {$smarty.get.tgl_awal} Sampai {$smarty.get.tgl_akhir}
            </th>
        </tr>
        <tr class="ui-widget-header">
            <th>Fakultas</th>
            <th>Jumlah Mahasiswa</th>
            <th>Jumlah Pengembalian</th>
            <th>Detail</th>
        </tr>
        {foreach $data_rekap as $data}
            <tr class="ui-widget-content">
                <td>{$data.NM_FAKULTAS}</td>
                <td class="center">
                    {if $data.JUMLAH_MHS!=''}
                        {$data.JUMLAH_MHS}
                    {else}
                        0
                    {/if}
                </td>
                <td class="center">
                    {if $data.TOTAL_PENGEMBALIAN!=''}
                        {number_format($data.TOTAL_PENGEMBALIAN)}
                    {else}
                        0
                    {/if}
                </td>
                <td class="center">
                    <a class="ui-button ui-corner-all ui-state-hover" href="rekap-pengembalian.php?mode=detail&tgl_awal={$smarty.get.tgl_awal}&tgl_akhir={$smarty.get.tgl_akhir}&biaya={$smarty.get.biaya}&fakultas={$data.ID_FAKULTAS}" style="padding:5px;cursor:pointer;">Detail</a>
                </td>
            </tr>
            {if $data.JUMLAH_MHS!=''}
                {$total_mahasiswa=$total_mahasiswa+$data.JUMLAH_MHS}
                {$total_bayar=$total_bayar+$data.TOTAL_PENGEMBALIAN}
            {/if}
        {/foreach}
        <tr class="total ui-widget-content">
            <td class="center">Total</td>
            <td class="center">{$total_mahasiswa}</td>
            <td class="center">{number_format($total_bayar)}</td>
            <td></td>
        </tr>
    </table>
{/if}
{if $smarty.get.mode!='bank'&&isset($smarty.get.mode)}
    <span class="ui-button ui-corner-all ui-state-hover" style="margin-top:10px;padding:5px;cursor:pointer;" onclick="history.back(-1)">Kembali</span>
{/if}
{literal}
    <script>
        $('#report_form').submit(function() {
            if ($('#tgl_awal').val() == '' || $('#tgl_akhir').val() == '') {
                $('#alert').fadeIn();
                return false;
            }
        });
        $('#report_form').validate();
        $(".datepicker").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true,
            changeYear: true});
        $('#fakultas').change(function() {
            $.ajax({
                type: 'post',
                url: 'getProdi.php',
                data: 'id_fakultas=' + $(this).val(),
                success: function(data) {
                    $('#prodi').html(data);
                }
            })
        });
    </script>
{/literal}