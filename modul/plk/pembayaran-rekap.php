<?php

include 'config.php';
include "class/plk.class.php";
include '../keuangan/class/list_data.class.php';
include '../keuangan/class/master.class.php';

$aucc = new plk($db);
$list = new list_data($db);
$master = new master($db);

if (!empty($_GET)) {
    $semester = get('semester');
    $fakultas = get('fakultas');
    $prodi = get('prodi');
    $status_bayar = get('status_bayar');
    $biaya = get('biaya');

    if (get('mode') == 'fakultas') {
        if ($fakultas != '') {
            $smarty->assign('program_studi', $aucc->get_program_studi_by_fakultas($fakultas));
        }
        $smarty->assign('id_fakultas', $fakultas);
        $smarty->assign('id_semester', $semester);
        $smarty->assign('biaya', $aucc->get_biaya());
        $smarty->assign('data_pembayaran', $aucc->data_pembayaran($status_bayar, $semester, $fakultas));
    } else if (get('mode') == 'detail') {
        $smarty->assign('id_fakultas', $fakultas);
        $smarty->assign('id_semester', $semester);
        $smarty->assign('bayar', $status_bayar);
        $smarty->assign('data_mhs', $aucc->get_mhs_fakultas($fakultas, $prodi, $semester, $status_bayar, $biaya));
    }
}


if (isset($_GET['get_fakultas'])) {
    $smarty->assign('id_fakultas', $_GET['get_fakultas']);
    $smarty->assign('id_semester', $_GET['get_semester']);
    $smarty->assign('bayar', $_GET['get_bayar']);
    $smarty->assign('get_mhs_fakultas', $aucc->get_mhs_fakultas($_GET['get_fakultas'], $_GET['get_semester'], $_GET['get_bayar'], $_GET['get_biaya']));
}

$smarty->assign('data_angkatan', $list->load_angkatan_mhs());
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->assign('data_status', $master->load_status_pembayaran());
$smarty->assign('data_semester', $list->load_list_semester());



$smarty->display('pembayaran-rekap.tpl');
?>
