<?php

include 'config.php';
include 'class/pesan.class.php';

$pesan = new pesan($db, $user->ID_PENGGUNA);

if(!empty($_POST)){
    if(post('mode')=='hapus'){
        $pesan->delete_permanen(post('id_pesan'));
    }else if(post('mode')=='restore'){
        $pesan->restore_pesan(post('id_pesan'));
    }
}

$smarty->assign('data_inbox',$pesan->load_trash());
$smarty->display('pesan/trash.tpl');
?>
