<?php

class pesan {

    public $db;
    public $id_pengguna;

    function __construct($db, $id_pengguna) {
        $this->db = $db;
        $this->id_pengguna = $id_pengguna;
    }

    function load_unread_inbox() {
        $query = "
        SELECT PES.*,P1.ID_PENGGUNA AS ID_PENERIMA,P1.NM_PENGGUNA AS NAMA_PENERIMA,P1.GELAR_DEPAN GELAR_DEPAN_PENERIMA,P1.GELAR_BELAKANG GELAR_BELAKANG_PENERIMA
        ,P2.ID_PENGGUNA AS ID_PENGIRIM,P2.NM_PENGGUNA AS NAMA_PENGIRIM,P2.GELAR_DEPAN GELAR_DEPAN_PENGIRIM,P2.GELAR_BELAKANG GELAR_BELAKANG_PENGIRIM
        ,TO_CHAR(PES.WAKTU_PESAN,'DD-MM-YYYY HH24:MI:SS') AS WAKTU
	FROM PESAN PES
        JOIN PENGGUNA P1 ON P1.ID_PENGGUNA = PES.ID_PENERIMA
        JOIN PENGGUNA P2 ON P2.ID_PENGGUNA = PES.ID_PENGIRIM
        WHERE PES.ID_PENERIMA= '" . $this->id_pengguna . "'
        AND (PES.HAPUS_PENERIMA !=3 OR PES.HAPUS_PENERIMA IS NULL)
        AND (PES.BACA_PENERIMA IS NULL OR PES.BACA_PENERIMA=0)
        ORDER BY WAKTU DESC";
        return $this->db->QueryToArray($query);
    }

    function load_inbox() {
        $query = "
        SELECT PES.*,P1.ID_PENGGUNA AS ID_PENERIMA,P1.NM_PENGGUNA AS NAMA_PENERIMA,P1.GELAR_DEPAN GELAR_DEPAN_PENERIMA,P1.GELAR_BELAKANG GELAR_BELAKANG_PENERIMA
        ,P2.ID_PENGGUNA AS ID_PENGIRIM,P2.NM_PENGGUNA AS NAMA_PENGIRIM,P2.GELAR_DEPAN GELAR_DEPAN_PENGIRIM,P2.GELAR_BELAKANG GELAR_BELAKANG_PENGIRIM
        ,TO_CHAR(PES.WAKTU_PESAN,'DD-MM-YYYY HH24:MI:SS') AS WAKTU
	FROM PESAN PES
        JOIN PENGGUNA P1 ON P1.ID_PENGGUNA = PES.ID_PENERIMA
        JOIN PENGGUNA P2 ON P2.ID_PENGGUNA = PES.ID_PENGIRIM
        WHERE PES.ID_PENERIMA= '" . $this->id_pengguna . "' 
        AND (PES.HAPUS_PENERIMA !=3 OR PES.HAPUS_PENERIMA IS NULL)
        ORDER BY WAKTU DESC";
        return $this->db->QueryToArray($query);
    }

    function load_outbox() {
        $query = "
        SELECT PES.*,P1.ID_PENGGUNA AS ID_PENERIMA,P1.NM_PENGGUNA AS NAMA_PENERIMA,P1.GELAR_DEPAN GELAR_DEPAN_PENERIMA,P1.GELAR_BELAKANG GELAR_BELAKANG_PENERIMA
        ,P2.ID_PENGGUNA AS ID_PENGIRIM,P2.NM_PENGGUNA AS NAMA_PENGIRIM,P2.GELAR_DEPAN GELAR_DEPAN_PENGIRIM,P2.GELAR_BELAKANG GELAR_BELAKANG_PENGIRIM
        ,TO_CHAR(PES.WAKTU_PESAN,'DD-MM-YYYY HH24:MI:SS') AS WAKTU
	FROM PESAN PES
        JOIN PENGGUNA P1 ON P1.ID_PENGGUNA = PES.ID_PENERIMA
        JOIN PENGGUNA P2 ON P2.ID_PENGGUNA = PES.ID_PENGIRIM
        WHERE PES.ID_PENGIRIM= '" . $this->id_pengguna . "'
        AND (PES.HAPUS_PENGIRIM !=3 OR PES.HAPUS_PENGIRIM IS NULL)
        ORDER BY WAKTU DESC";
        return $this->db->QueryToArray($query);
    }

    function load_trash() {
        $query = "
        SELECT PES.*,P1.ID_PENGGUNA AS ID_PENERIMA,P1.NM_PENGGUNA AS NAMA_PENERIMA,P1.GELAR_DEPAN GELAR_DEPAN_PENERIMA,P1.GELAR_BELAKANG GELAR_BELAKANG_PENERIMA
        ,P2.ID_PENGGUNA AS ID_PENGIRIM,P2.NM_PENGGUNA AS NAMA_PENGIRIM,P2.GELAR_DEPAN GELAR_DEPAN_PENGIRIM,P2.GELAR_BELAKANG GELAR_BELAKANG_PENGIRIM
        ,TO_CHAR(PES.WAKTU_PESAN,'DD-MM-YYYY HH24:MI:SS') AS WAKTU
	FROM PESAN PES
        JOIN PENGGUNA P1 ON P1.ID_PENGGUNA = PES.ID_PENERIMA
        JOIN PENGGUNA P2 ON P2.ID_PENGGUNA = PES.ID_PENGIRIM
        WHERE (PES.ID_PENGIRIM= '" . $this->id_pengguna . "' OR PES.ID_PENERIMA= '" . $this->id_pengguna . "')
        AND (PES.HAPUS_PENERIMA = 3 OR PES.HAPUS_PENGIRIM=3)
        ORDER BY WAKTU DESC";
        return $this->db->QueryToArray($query);
    }

    function get_pesan($id) {
        $query = "
        SELECT PES.*,P1.ID_PENGGUNA AS ID_PENERIMA,P1.NM_PENGGUNA AS NAMA_PENERIMA,P1.GELAR_DEPAN GELAR_DEPAN_PENERIMA,P1.GELAR_BELAKANG GELAR_BELAKANG_PENERIMA
        ,P2.ID_PENGGUNA AS ID_PENGIRIM,P2.NM_PENGGUNA AS NAMA_PENGIRIM,P2.GELAR_DEPAN GELAR_DEPAN_PENGIRIM,P2.GELAR_BELAKANG GELAR_BELAKANG_PENGIRIM
        ,TO_CHAR(PES.WAKTU_PESAN,'DD-MM-YYYY HH24:MI:SS') AS WAKTU
	FROM PESAN PES
        JOIN PENGGUNA P1 ON P1.ID_PENGGUNA = PES.ID_PENERIMA
        JOIN PENGGUNA P2 ON P2.ID_PENGGUNA = PES.ID_PENGIRIM
        WHERE PES.ID_PESAN= '{$id}' ";
        $this->db->Query($query);
        return $this->db->FetchAssoc();
    }

    function insert_pesan($post) {
        $id_penerima = $post['id_penerima'];
        $judul = $post['judul'];
        $isi = $post['isi'];
        $id_balasan = $post['balasan'] == '' ? '' : $post['balasan'];
        $this->db->Query("
            INSERT INTO PESAN 
            (ID_PENGIRIM,ID_PENERIMA,ID_BALASAN,JUDUL_PESAN,ISI_PESAN,WAKTU_PESAN,BACA_PENGIRIM,BACA_PENERIMA,HAPUS_PENGIRIM,HAPUS_PENERIMA) 
            values 
            ('$this->id_pengguna','$id_penerima','$id_balasan','$judul','$isi',to_date('" . date('Ymd His') . "','YYYYMMDD HH24MISS '),0,0,0,0)
            ");
    }

    function reply_pesan($post) {
        $id_penerima = $post['id_penerima'];
        $judul = $post['judul'];
        $isi = $post['isi'];
        $id_balasan = $post['id_balasan'];
        $this->db->Query("
            INSERT INTO PESAN 
            (ID_PENGIRIM,ID_PENERIMA,JUDUL_PESAN,ISI_PESAN,ID_BALASAN,WAKTU_PESAN,BACA_PENGIRIM,BACA_PENERIMA,HAPUS_PENGIRIM,HAPUS_PENERIMA) 
            values 
            ('$this->id_pengguna','$id_penerima','$judul','$isi','$id_balasan',to_date('" . date('Ymd His') . "','YYYYMMDD HH24MISS '),0,0,0,0)
            ");
    }

    function restore_pesan($id) {
        $this->db->Query("UPDATE PESAN PES set PES.HAPUS_PENGIRIM='0',PES.HAPUS_PENERIMA='0' where PES.ID_PESAN='$id' ");
    }

    function delete_pesan_penerima($id) {
        $this->db->Query("UPDATE PESAN PES SET PES.HAPUS_PENERIMA='3' WHERE PES.ID_PESAN='$id'");
    }

    function delete_pesan_pengirim($id) {
        $this->db->Query("UPDATE PESAN PES SET PES.HAPUS_PENGIRIM='3' WHERE PES.ID_PESAN='$id'");
    }

    function delete_permanen($id) {
        $this->db->Query("DELETE PESAN PES where PES.ID_PESAN='$id'");
    }

    function cut_long_text($string) {
        if (strpos($string, ' ') == false && strlen($string) > 20) {
            $arr_str = str_split($string, 20);
            $string = implode(' ', $arr_str);
        }
        return $string;
    }

}

?>
