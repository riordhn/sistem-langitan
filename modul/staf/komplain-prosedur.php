<?php

include 'config.php';

if (!empty($_GET)) {
    if (get('mode') == 'komplain') {
        $uk = get('uk');
        $dok = get('dok');
        $query = "
            SELECT UK.ID_UNIT_KERJA,UK.NM_UNIT_KERJA,UK.DESKRIPSI_UNIT_KERJA
            FROM AUCC.KOMPLAIN_BACA_PROSEDUR KBS
            JOIN AUCC.MM_DOKUMEN MD ON MD.ID_MM_DOKUMEN=KBS.ID_MM_DOKUMEN
            JOIN AUCC.MM_GROUP_DOKUMEN MGD ON MGD.ID_MM_GROUP_DOKUMEN = MD.ID_MM_GROUP_DOKUMEN
            JOIN AUCC.UNIT_KERJA UK ON UPPER(UK.DESKRIPSI_UNIT_KERJA) = UPPER(MGD.NAMA_GROUP_DOKUMEN) OR UPPER(UK.NM_UNIT_KERJA) = UPPER(MGD.SINGKATAN_LINGKUP)
            WHERE KBS.ID_PENGGUNA='{$user->ID_PENGGUNA}' AND MGD.ID_MM_GROUP_DOKUMEN='{$uk}' 
            GROUP BY UK.ID_UNIT_KERJA,UK.NM_UNIT_KERJA,UK.DESKRIPSI_UNIT_KERJA
            ";
        $unit_kerja_filter = $db->QueryToArray($query);
        $unit_kerja_all = $db->QueryToArray("SELECT * FROM UNIT_KERJA WHERE TYPE_UNIT_KERJA NOT IN ('PRODI') ORDER BY NM_UNIT_KERJA");
        $prosedur = $db->QuerySingle("SELECT NAMA_DOKUMEN FROM MM_DOKUMEN WHERE ID_MM_DOKUMEN='{$dok}'");
        $smarty->assign('data_unit_kerja', count($unit_kerja_filter) == 0 ? $unit_kerja_all : $unit_kerja_filter);
        $smarty->assign('prosedur', $prosedur);
        $smarty->assign('pelapor', $user->NM_PENGGUNA);
    } else if (get('mode') == 'jawaban') {
        if(!empty($_POST)){
            if(post('mode')=='penilaian'){
                $id_komplain = get('k');
                $rate =post('rate');
                $keterangan =post('keterangan_nilai');
                $q_u_komplain = "
                    UPDATE KOMPLAIN SET 
                        NILAI_TANGGAPAN='{$rate}',
                        KETERANGAN_TANGGAPAN='{$keterangan}',
                        STATUS_KOMPLAIN=4
                    WHERE ID_KOMPLAIN='{$id_komplain}'
                    ";
                $db->Query($q_u_komplain);
            }
        }
        $id_komplain = get('k');
        $q_data_komplain = "
        SELECT P.NM_PENGGUNA,
        (CASE
            WHEN P.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.DOSEN)
            THEN 'Dosen'
            WHEN P.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.PEGAWAI)
            THEN 'Tenaga Kependidikan'
            ELSE 'Mahasiswa'
        END  
        ) STATUS
        ,KK.NM_KATEGORI,UK.NM_UNIT_KERJA,UK.DESKRIPSI_UNIT_KERJA,K.*,TO_CHAR(K.WAKTU_KOMPLAIN,'DD-MM-YYYY HH24:MI:SS') WAKTU,
        KT.*,P2.NM_PENGGUNA PENJAWAB,
        (CASE
            WHEN P2.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.DOSEN)
            THEN 'Dosen'
            WHEN P2.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.PEGAWAI)
            THEN 'Tenaga Kependidikan'
        END  
        ) STATUS_PENJAWAB,TO_CHAR(KT.WAKTU_JAWAB,'DD-MM-YYYY HH24:MI:SS') WAKTU_JAWAB
        FROM AUCC.KOMPLAIN K
        JOIN AUCC.KOMPLAIN_KATEGORI KK ON K.ID_KOMPLAIN_KATEGORI=KK.ID_KOMPLAIN_KATEGORI
        JOIN AUCC.UNIT_KERJA UK ON UK.ID_UNIT_KERJA=K.ID_UNIT_TERTUJU
        JOIN AUCC.PENGGUNA P ON K.ID_PELAPOR=P.ID_PENGGUNA
        LEFT JOIN AUCC.KOMPLAIN_TANGGAPAN KT ON KT.ID_KOMPLAIN=K.ID_KOMPLAIN
        LEFT JOIN AUCC.PENGGUNA P2 ON P2.ID_PENGGUNA=KT.ID_PENJAWAB
        WHERE K.ID_KOMPLAIN='{$id_komplain}'
        ";
        $db->Query($q_data_komplain);
        $data_komplain = $db->FetchAssoc();
        $smarty->assign('komplain', $data_komplain);
    }
}

if (!empty($_POST)) {
    if (post('mode') == 'kirim') {
        $cek_kategori = $db->QuerySingle("SELECT COUNT(*) FROM KOMPLAIN_KATEGORI WHERE NM_KATEGORI='{$_POST['kategori']}' AND ID_UNIT_KERJA='{$_POST['unit_kerja']}'");
        if ($cek_kategori == 0) {
            $q_i_kategori = "
            INSERT INTO KOMPLAIN_KATEGORI
                (NM_KATEGORI,ID_UNIT_KERJA)
            VALUES 
                ('{$_POST['kategori']}','{$_POST['unit_kerja']}')
            ";
            $db->Query($q_i_kategori);
        }
        $id_kategori = $db->QuerySingle("SELECT ID_KOMPLAIN_KATEGORI FROM KOMPLAIN_KATEGORI WHERE NM_KATEGORI='{$_POST['kategori']}' AND ID_UNIT_KERJA='{$_POST['unit_kerja']}'");
        $q_i_komplain = "
            INSERT INTO KOMPLAIN
                (ID_PELAPOR,ID_UNIT_TERTUJU,ID_KOMPLAIN_KATEGORI,ID_KOMPLAIN_VIA,ISI_KOMPLAIN,SARAN_TINDAKAN,STATUS_KOMPLAIN)
            VALUES
                ('{$user->ID_PENGGUNA}','{$_POST['unit_kerja']}','{$id_kategori}','1','{$_POST['isi']}','{$_POST['saran']}','1')
            ";
        $db->Query($q_i_komplain);
    }
}
$q_data_baca = "
SELECT MGD.ID_MM_GROUP_DOKUMEN,MGD.NAMA_GROUP_DOKUMEN,MD.KODE_DOKUMEN,MD.NAMA_DOKUMEN,KBP.*,TO_CHAR(KBP.WAKTU_BACA,'DD-MM-YYYY HH24:MI:SS') WAKTU
FROM AUCC.KOMPLAIN_BACA_PROSEDUR KBP
JOIN AUCC.MM_DOKUMEN MD ON MD.ID_MM_DOKUMEN=KBP.ID_MM_DOKUMEN
JOIN AUCC.MM_GROUP_DOKUMEN MGD ON MGD.ID_MM_GROUP_DOKUMEN = MD.ID_MM_GROUP_DOKUMEN
WHERE KBP.ID_PENGGUNA='{$user->ID_PENGGUNA}'
";
$data_baca = $db->QueryToArray($q_data_baca);

$q_data_komplain = "
SELECT KK.NM_KATEGORI,UK.NM_UNIT_KERJA,UK.DESKRIPSI_UNIT_KERJA,K.*,TO_CHAR(K.WAKTU_KOMPLAIN,'DD-MM-YYYY HH24:MI:SS') WAKTU
FROM AUCC.KOMPLAIN K
JOIN AUCC.KOMPLAIN_KATEGORI KK ON K.ID_KOMPLAIN_KATEGORI=KK.ID_KOMPLAIN_KATEGORI
JOIN AUCC.UNIT_KERJA UK ON UK.ID_UNIT_KERJA=K.ID_UNIT_TERTUJU
WHERE K.ID_PELAPOR='{$user->ID_PENGGUNA}'
";
$data_komplain = $db->QueryToArray($q_data_komplain);

$smarty->assign('info_komplain', alert_success("Anda hanya bisa melakukan komplain mengenai prosedur yang ada pada setiap Unit Kerja setelah membaca Dokumen Prosedur yang sudah ada. Terima Kasih", 98));
$smarty->assign('data_baca', $data_baca);
$smarty->assign('data_komplain', $data_komplain);
$smarty->display('aims/komplain-prosedur.tpl');
?>
