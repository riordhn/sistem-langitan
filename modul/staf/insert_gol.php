<?php

//Yudi Sulistya, 15/08/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

if ($user->Role() == 22) {
$id = $_GET['id'];

if (isset($_POST['submit'])) {
	$id_pengguna = $_POST['id_pengguna'];
	$id_golongan = $_POST['id_golongan'];
	$no_sk_sejarah_golongan = $_POST['no_sk_sejarah_golongan'];
	$keterangan_sk_sejarah_golongan = $_POST['keterangan_sk_sejarah_golongan'];
	$tmt_sejarah_golongan = $_POST['tmt_sejarah_golongan'];
	$tgl_sk_sejarah_golongan = $_POST['tgl_sk_sejarah_golongan'];
	$ttd_sk_nama_pejabat = $_POST['ttd_sk_nama_pejabat'];
	
	InsertData("insert into sejarah_golongan (
				id_pengguna, id_golongan, no_sk_sejarah_golongan, asal_sk_sejarah_golongan, keterangan_sk_sejarah_golongan,
				tmt_sejarah_golongan, valid_sd, tgl_sk_sejarah_golongan, ttd_sk_nama_pejabat
				)
				values (
				$id_pengguna, $id_golongan, trim(upper('".$no_sk_sejarah_golongan."')), trim(upper('".$asal_sk_sejarah_golongan."')), trim(upper('".$keterangan_sk_sejarah_golongan."')),
				to_date('".$tmt_sejarah_golongan."','DD-MM-YYYY'), 1, to_date('".$tgl_sk_sejarah_golongan."','DD-MM-YYYY'), trim(upper('".$ttd_sk_nama_pejabat."'))
				)");

	echo '<script>alert("Data berhasil ditambahkan")</script>';
	echo '<script>window.parent.document.location.reload();</script>';
}

echo'
<html>
<head>

<script type="text/javascript" src="js/datetimepicker.js"></script>
<script language="JavaScript">
function validate_form(){
if (document.forms["golinsert"]["no_sk_sejarah_golongan"].value==null || document.forms["golinsert"]["no_sk_sejarah_golongan"].value==" " || document.forms["golinsert"]["no_sk_sejarah_golongan"].value=="")
{
	alert ("No. SK harus terisi");
	return false;
}
else if (document.forms["golinsert"]["asal_sk_sejarah_golongan"].value==null || document.forms["golinsert"]["asal_sk_sejarah_golongan"].value==" " || document.forms["golinsert"]["asal_sk_sejarah_golongan"].value=="")
{
	alert ("Asal SK harus terisi");
	return false;
}
else if (document.forms["golinsert"]["tmt_sejarah_golongan"].value==null || document.forms["golinsert"]["tmt_sejarah_golongan"].value==" " || document.forms["golinsert"]["tmt_sejarah_golongan"].value=="")
{
	alert ("TMT SK harus terisi");
	return false;
}
else if (document.forms["golinsert"]["tgl_sk_sejarah_golongan"].value==null || document.forms["golinsert"]["tgl_sk_sejarah_golongan"].value==" " || document.forms["golinsert"]["tgl_sk_sejarah_golongan"].value=="")
{
	alert ("Tanggal SK SK harus terisi");
	return false;
}
else if (document.forms["golinsert"]["ttd_sk_nama_pejabat"].value==null || document.forms["golinsert"]["ttd_sk_nama_pejabat"].value==" " || document.forms["golinsert"]["ttd_sk_nama_pejabat"].value=="")
{
	alert ("Pejabat penandatangan SK harus terisi");
	return false;
}
	return true;
}
</script>
</head>
<body oncontextmenu="return false;">
	<form name="golinsert" id="golinsert" method="post" onsubmit="return validate_form();">
	<input type="hidden" name="id_pengguna" value="'.$id.'">
	<table cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td align="right">Golongan<font color="red">*</font></td>
			<td>&nbsp;:&nbsp;<select name="id_golongan">';

$jjg="select id_golongan, upper(nm_golongan) from golongan order by nm_golongan desc";
$result = $db->Query($jjg)or die("salah kueri golongan ");
while($r = $db->FetchRow()) {
echo '<option value="'.$r[0].'">'.$r[1].'</option>';
}
echo '
				</select>
				&nbsp;&nbsp;TMT<font color="red">*</font>&nbsp;:&nbsp;<input type="text" name="tmt_sejarah_golongan" id="tmt_sejarah_golongan" style="text-align:center;" onclick="javascript:NewCssCal(\'tmt_sejarah_golongan\',\'ddmmyyyy\',\'arrow\',\'\',\'\',\'\',\'past\')" value="'.date("d-m-Y").'" />
				&nbsp;&nbsp;Tanggal SK<font color="red">*</font>&nbsp;:&nbsp;<input type="text" name="tgl_sk_sejarah_golongan" id="tgl_sk_sejarah_golongan" style="text-align:center;" onclick="javascript:NewCssCal(\'tgl_sk_sejarah_golongan\',\'ddmmyyyy\',\'arrow\',\'\',\'\',\'\',\'past\')" value="'.date("d-m-Y").'" />
			</td>
		</tr>
		<tr>
			<td align="right">No. SK<font color="red">*</font></td>
			<td>&nbsp;:&nbsp;<input type="text" name="no_sk_sejarah_golongan" maxlength="50" style="width:600px;" /></td>
		</tr>
		<tr>
			<td align="right">Asal SK<font color="red">*</font></td>
			<td>&nbsp;:&nbsp;<input type="text" name="asal_sk_sejarah_golongan" maxlength="50" style="width:250px;" />
			&nbsp;&nbsp;Keterangan&nbsp;:&nbsp;<input type="text" name="keterangan_sk_sejarah_golongan" maxlength="50" style="width:253px;" /></td>
		</tr>
		<tr>
			<td align="right">Penandatangan SK<font color="red">*</font></td>
			<td>&nbsp;:&nbsp;<input type="text" name="ttd_sk_nama_pejabat" maxlength="120" style="width:600px;" /></td>
		</tr>
		<tr>
			<td></td>
			<td>
				&nbsp;&nbsp;&nbsp;<input type="submit" name="submit" value="Simpan" />
			</td>
		</tr>
	</table>
	</form>
</body>
</html>';
} else {
    header("location: /logout.php");
    exit();
}
?>