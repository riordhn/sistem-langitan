<?php

include 'config.php';
include 'class/pesan.class.php';

$pesan = new pesan($db, $user->ID_PENGGUNA);

if(!empty($_POST)){
    if(post('mode')=='hapus'){
        $pesan->delete_pesan_pengirim(post('id_pesan'));
    }
}

$smarty->assign('data_inbox',$pesan->load_outbox());
$smarty->display('pesan/outbox.tpl');
?>
