<?php
include ('common.php');
require_once ('ociFunction.php');

if ($user->Role() == 22) {
    $id = $_GET['id'];

    if (isset($_POST['submit'])) {
        $id_pengguna = $_POST['id_pengguna'];
        $jabatan = $_POST['jabatan'];
        $nomor = $_POST['nomor'];
        $tgl_sk = $_POST['tgl_sk'];
        $tgl_tmt = $_POST['tgl_tmt'];
        $asal = $_POST['asal'];

        InsertData("insert into sejarah_jabatan_fungsional_peg (id_pengguna, id_jabatan_fungsional_pegawai, no_sk_jabatan_fungsional, asal_sk_jabatan_fungsional, tgl_sk_jabatan_fungsional,tmt_jabatan_fungsional,valid_sd) 
                    values ('{$id_pengguna}','{$jabatan}','{$nomor}','{$asal}',to_date('{$tgl_sk}','DD-MM-YYYY'),to_date('{$tgl_tmt}','DD-MM-YYYY'),1)");

        echo '<script>alert("Data berhasil ditambahkan")</script>';
        echo '<script>window.parent.document.location.reload();</script>';
    }
    $data_jab_fung = getData("
            SELECT JFP.*,JFK.NAMA_KATEGORI_JABATAN
            FROM JABATAN_FUNGSIONAL_PEGAWAI JFP
            JOIN JABATAN_FUNGSIONAL_KATEGORI JFK ON JFK.ID_JABATAN_FUNGSIONAL_KATEGORI=JFP.ID_JABATAN_FUNGSIONAL_KATEGORI
            WHERE JFP.STATUS=2
            ORDER BY ID_JABATAN_FUNGSIONAL_PEGAWAI
            ");
        $smarty->assign('DATA_JAB_FUNG', $data_jab_fung);
    $smarty->display('biodata/insert/insert_fung_tertentu.tpl');
} else {
    header("location: /logout.php");
    exit();
}
?>