<?php
include ('common.php');
require_once ('ociFunction.php');

if ($user->Role() == 22) {
    $id = $_GET['id'];

    if (isset($_POST['submit'])) {
        $id_pengguna = $_POST['id_pengguna'];
        $nip_penilai = $_POST['nip_penilai'];
        $nip_lama_penilai = $_POST['nip_lama_penilai'];
        $jabatan_penilai = $_POST['jabatan_penilai'];
        $unit_penilai = $_POST['unit_penilai'];
        $nip_atasan = $_POST['nip_atasan'];
        $jabatan_atasan = $_POST['jabatan_atasan'];
        $unit_atasan = $_POST['unit_atasan'];
        $kesetiaan = $_POST['kesetiaan'];
        $kerjasama = $_POST['kerjasama'];
        $prestasi_kerja = $_POST['prestasi'];
        $prakarsa = $_POST['prakarsa'];
        $tanggung_jawab = $_POST['tanggung_jawab'];
        $kepemimpinan = $_POST['kepemimpinan'];
        $ketaatan = $_POST['ketaatan'];
        $kejujuran = $_POST['kejujuran'];

        die("insert into pegawai_dp3 (id_pengguna, nip_pjbt_penilai, nip_lama_pjbt_penilai, jabatan_pjbt_penilai,unit_pjbt_penilai,nip_pjbt_atasan,jabatan_pjbt_atasan,unit_pjbt_atasan,kesetiaaan,kerjasama,prestasi_kerja,prakarsa,tanggung_jawab,kepemimpinan,ketaatan,kejujuran,status_valid) 
                    values ('{$id_pengguna}','{$nip_penilai}','{$nip_lama_penilai}','{$jabatan_penilai}','{$unit_penilai}','{$nip_atasan}','{$jabatan_atasan}','{$unit_atasan}','{$kesetiaan}','{$kerjasama}','{$prestasi_kerja}','{$prakarsa}','{$tanggung_jawab}','{$kepemimpinan}','{$ketaatan}','{$kejujuran}',1)");

        echo '<script>alert("Data berhasil ditambahkan")</script>';
        echo '<script>window.parent.document.location.reload();</script>';
    }
    $smarty->display('biodata/insert/insert_dp3.tpl');
} else {
    header("location: /logout.php");
    exit();
}
?>