<?php

//Yudi Sulistya, 15/08/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

if ($user->Role() == 22) {
$id = $_GET['id'];

if (isset($_POST['submit'])) {
	$id_pengguna = $_POST['id_pengguna'];
	$id_pendidikan_akhir = $_POST['id_pendidikan_akhir'];
	$nm_jurusan_pendidikan = $_POST['nm_jurusan_pendidikan'];
	$nm_sekolah_pendidikan = $_POST['nm_sekolah_pendidikan'];
	$tempat_pendidikan = $_POST['tempat_pendidikan'];
	$tahun_lulus_pendidikan = $_POST['thn_lulus'];

	InsertData("insert into sejarah_pendidikan (id_pengguna, id_pendidikan_akhir, nm_jurusan_pendidikan, nm_sekolah_pendidikan, tempat_pendidikan, tahun_lulus_pendidikan) 
				values ($id_pengguna, $id_pendidikan_akhir, trim(upper('".$nm_jurusan_pendidikan."')), trim(upper('".$nm_sekolah_pendidikan."')), trim(upper('".$tempat_pendidikan."')), '".$tahun_lulus_pendidikan."')");

	echo '<script>alert("Data berhasil ditambahkan")</script>';
	echo '<script>window.parent.document.location.reload();</script>';
}

echo'<html>
<head>
<script type="text/javascript" src="js/datetimepicker.js"></script>
<script language="JavaScript">
function validate_form(){
if (document.forms["pddinsert"]["nm_sekolah_pendidikan"].value==null || document.forms["pddinsert"]["nm_sekolah_pendidikan"].value==" " || document.forms["pddinsert"]["nm_sekolah_pendidikan"].value=="")
{
	alert ("Nama sekolah/institusi pendidikan harus terisi");
	return false;
}
	return true;
}
</script>
</head>
<body oncontextmenu="return false;">
	<form name="pddinsert" id="pddinsert" method="post" onsubmit="return validate_form();">
	<input type="hidden" name="id_pengguna" value="'.$id.'">
	<table cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td>Jenjang</td>
			<td>&nbsp;:&nbsp;<select name="id_pendidikan_akhir">';

$jjg="select urut, id_pendidikan_akhir, nama_pendidikan_akhir
		from (
		select id_pendidikan_akhir, nama_pendidikan_akhir,
		case when id_pendidikan_akhir=2 then 11 
		when id_pendidikan_akhir=1 then 12
		when id_pendidikan_akhir=10 then 13  
		when id_pendidikan_akhir=9 then 10 
		when id_pendidikan_akhir=8 then 9
		when id_pendidikan_akhir=7 then 8   
		when id_pendidikan_akhir=3 then 7  
		when id_pendidikan_akhir=4 then 6 
		when id_pendidikan_akhir=5 then 5 
		when id_pendidikan_akhir=6 then 4 
		when id_pendidikan_akhir=13 then 3 
		when id_pendidikan_akhir=12 then 2 
		when id_pendidikan_akhir=11 then 1 
		else 0 end as urut
		from pendidikan_akhir)
		order by urut desc";
$result = $db->Query($jjg)or die("salah kueri jenjang ");
while($r = $db->FetchRow()) {
echo '<option value="'.$r[1].'">'.$r[2].'</option>';
}
echo '
				</select>
			</td>
		</tr>
		<tr>
			<td>Sekolah/Institusi</td>
			<td>&nbsp;:&nbsp;<input type="text" name="nm_sekolah_pendidikan" maxlength="100" style="width:600px;" /></td>
		</tr>
		<tr>
			<td>Jurusan</td>
			<td>&nbsp;:&nbsp;<input type="text" name="nm_jurusan_pendidikan" maxlength="50" style="width:600px;" /></td>
		</tr>
		<tr>
			<td>Tempat/Kota</td>
			<td>&nbsp;:&nbsp;<input type="text" name="tempat_pendidikan" maxlength="50" style="width:600px;" /></td>
		</tr>
		<tr>
			<td>Tahun Lulus</td>
			<td>&nbsp;:&nbsp;<select name="thn_lulus">';
			$thn_awal = 60;
			for ($i=0;$i<$thn_awal;$i++) {
				$thn = date('Y') - $i;
				echo '<option value="'.$thn.'">'.$thn.'</option>';
}
echo '
				</select>
		&nbsp;&nbsp;&nbsp;<input type="submit" name="submit" value="Simpan" />
		</td>
		</tr>
	</table>
	</form>
</body>
</html>';
} else {
    header("location: /logout.php");
    exit();
}
?>