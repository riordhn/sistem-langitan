<?php

include 'config.php';
include 'class/pesan.class.php';

$pesan = new pesan($db, $user->ID_PENGGUNA);

// NOTIFIKASI
$total_notifikasi = 0;
$jumlah_notifikasi_pesan =0;
$jumlah_notifikasi_komplain =0;
// NOTIFIKASI PESAN
$jumlah_notifikasi_pesan = count($pesan->load_unread_inbox());
$link_notifikasi_pesan = '<a href="#pesan!inbox.php" class="disable-ajax notifikasi-link ui-button ui-state-hover ui-corner-all" style="padding:2px;float: right;margin-top:0px">Pesan</a>';

// NOTIFIKASI KOMPLAIN
$unit_kerja_pengguna = $db->QuerySingle("SELECT ID_UNIT_KERJA FROM PEGAWAI WHERE ID_PENGGUNA='{$user->ID_PENGGUNA}'");
// CHECK PENANGGUNG JAWAB KOMPLAIN
$q_cek_pj_komplain = "
    SELECT COUNT(*) FROM KOMPLAIN_PJ WHERE ID_PENGGUNA=" . $user->ID_PENGGUNA . "AND ID_UNIT_KERJA='{$unit_kerja_pengguna}'";

$cek_pj_komplain = $db->QuerySingle($q_cek_pj_komplain);

if ($cek_pj_komplain > 0) {
    $q_data_komplain = "
    SELECT P.NM_PENGGUNA,UK.NM_UNIT_KERJA,UK.DESKRIPSI_UNIT_KERJA,K.*,TO_CHAR(K.WAKTU_KOMPLAIN,'DD-MM-YYYY HH24:MI:SS') WAKTU
    FROM AUCC.KOMPLAIN K
    JOIN AUCC.UNIT_KERJA UK ON UK.ID_UNIT_KERJA=K.ID_UNIT_TERTUJU
    JOIN AUCC.PENGGUNA P ON K.ID_PELAPOR=P.ID_PENGGUNA
    WHERE ({$unit_kerja_pengguna} IN (SELECT ID_UNIT_KERJA FROM KOMPLAIN_SHARE WHERE ID_KOMPLAIN=K.ID_KOMPLAIN) OR K.ID_UNIT_TERTUJU={$unit_kerja_pengguna}) AND K.STATUS_KOMPLAIN='1'
    ORDER BY K.WAKTU_KOMPLAIN DESC
    ";
    $data_komplain = $db->QueryToArray($q_data_komplain);
    $jumlah_notifikasi_komplain = count($data_komplain);
    $link_notifikasi_komplain = '<a href="#kom!komplain-unit-kerja.php" class="disable-ajax notifikasi-link ui-button ui-state-hover ui-corner-all" style="padding:2px;float: right;margin-top:0px">Komplain</a>';
    $smarty->assign('notifikasi_komplain', alert_success("Anda memiliki <b>{$jumlah_notifikasi_komplain}</b> komplain belum terjawab, klik link Komplain untuk melihat. " . $link_notifikasi_komplain, 98));

}

// BUAT KETERANGAN NOTIFIKASI
$smarty->assign('notifikasi_pesan', alert_success("Anda memiliki <b>{$jumlah_notifikasi_pesan}</b> pesan belum terbaca, klik link Pesan untuk melihat. " . $link_notifikasi_pesan, 98));

$total_notifikasi += ($jumlah_notifikasi_komplain + $jumlah_notifikasi_pesan);

$smarty->assign('total_notifikasi', $total_notifikasi);
$smarty->display('display-notifikasi.tpl');
?>