<?php

include 'config.php';
include '../dosen/proses/aims.class.php';

$aims = new aims($db, $user->ID_DOSEN);
if (isset($_POST)) {
    if (post('mode') == 'tambah') {
        $aims->tambah_dokumen(post('lingkup'), strtoupper(post('kode')), post('isi'));
    } else if (post('mode') == 'edit') {
        $aims->update_dokumen(post('id'), post('lingkup'), post('kode'), post('isi'));
    } else if (post('mode') == 'upload') {
        $aims->upload_dokumen(post('id_dokumen'), $user->ID_PENGGUNA, post('uraian'), post('sebelum'), post('sesudah'), $_FILES, post('nama_file'));
    } else if (post('mode') == 'verifikasi') {
        $aims->verifikasi_file_dokumen(post('id_file'), $user->ID_PENGGUNA);
    } else if (post('mode') == 'batal_verifikasi') {
        $aims->batal_verifikasi_file_dokumen(post('id_file'));
    } else if (post('mode') == 'hapus_file') {
        $aims->delete_file_dokumen(post('id_file'));
    }
}

if (isset($_GET)) {
    if (get('mode') == 'tambah') {
        $smarty->assign('lingkup', $aims->get_lingkup_dokumen(get('l')));
    } elseif (get('mode') == 'edit' || get('mode') == 'upload') {
        if (get('mode') == 'upload') {
            $id = get('d');
            $smarty->assign('count_file', $db->QuerySingle("SELECT COUNT(*) FROM MM_FILE_DOKUMEN WHERE ID_MM_DOKUMEN='{$id}'"));
        }
        $smarty->assign('lingkup', $aims->get_lingkup_dokumen(get('l')));
        $smarty->assign('dokumen', $aims->get_dokumen(get('d')));
    } else if (get('mode') == 'file') {
        $smarty->assign('dokumen', $aims->get_dokumen(get('d')));
        $smarty->assign('data_file', $aims->load_file_dokumen(get('d')));
    }
}

$smarty->assign('keterangan', alert_success("Pedoman Prosedur Tiap Lingkup Dokumen",96));
$smarty->assign('data_lingkup',$aims->load_lingkup_dokumen());
$smarty->assign('data_dokumen', $aims->load_dokumen());
$smarty->assign('lingkup_jabatan', $aims->get_lingkup_jabatan($user->ID_JABATAN_PEGAWAI));
$smarty->assign('id_pengguna',$user->ID_PENGGUNA);
$smarty->display('aims/pedoman-prosedur.tpl');

?>