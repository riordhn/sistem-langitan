<?php

include 'config.php';
include 'class/pesan.class.php';

$pesan = new pesan($db, $user->ID_PENGGUNA);
$id_pesan = get('p');

if(!empty($_POST)){
    if(post('mode')=='simpan'){
        $pesan->insert_pesan($_POST);
    }
}

if(get('mode')=='inbox'){
     $db->Query("UPDATE PESAN SET BACA_PENERIMA='1' WHERE ID_PESAN='{$id_pesan}'");
}
$smarty->assign('pesan',$pesan->get_pesan($id_pesan));
$smarty->assign('pengirim', $user->NM_PENGGUNA);
$smarty->assign('id_pengirim', $user->ID_PENGGUNA);
$smarty->display('pesan/message-detail.tpl');
?>
