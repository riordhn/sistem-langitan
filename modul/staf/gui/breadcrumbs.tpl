<!-- NAVIGASI -->
<span style="padding: 2px 3px;padding-bottom: 4px; text-shadow: 1px 1px 1px #cbcbcb;font-size: 1em">NAVIGASI</span>:
<a style="text-transform: uppercase;padding: 2px 3px;padding-bottom: 4px; text-shadow: 1px 1px 1px #cbcbcb;font-size: 1em " href="#{$modul.NM_MODUL}!{$modul.PAGE}">{$modul.TITLE}</a> {if isset($menu)}/ 
<a style="text-transform: uppercase;padding: 2px 3px;padding-bottom: 4px; text-shadow: 1px 1px 1px #cbcbcb;font-size: 1em " href="#{$modul.NM_MODUL}-{$menu.NM_MENU}!{$menu.PAGE}">{$menu.TITLE}</a>{/if}

<!-- LOGOUT -->
<a href="../../logout.php" style="float: right;list-style: none;padding: 0px 5px;margin-right: 25px;cursor: pointer" class="disable-ajax ui-state-default ui-corner-all">
    <span class="ui-icon ui-icon-closethick" style="float: left;padding: 2px;margin-right: 0px;"></span><span style="padding: 2px;padding-right: 10px">Logout</span>
</a>

<!-- NOTIFIKASI -->
{if $total_notifikasi>0}
    <a onclick="$('#dialog_detail_notifikasi').dialog('open').load('display-notifikasi.php')"  style="color: black;float: right;list-style: none;padding: 0px 5px;margin-right: 5px;cursor: pointer" class="ui-state-highlight ui-corner-all">
        <span class="ui-icon ui-icon-comment" style="float: left;padding: 2px;margin-right: 0px;"></span><span style="padding: 2px;padding-right: 10px"><b >{$total_notifikasi}</b> Notifikasi</span>
    </a>
{else}
    <a style="float: right;list-style: none;padding: 0px 5px;margin-right: 5px;cursor: pointer" class="ui-state-default ui-corner-all">
        <span class="ui-icon ui-icon-comment" style="float: left;padding: 2px;margin-right: 0px;"></span><span style="padding: 2px;padding-right: 10px">Tidak ada Notifikasi</span>
    </a>
{/if}


<!-- USER LOGIN -->
<span class="login_user" style="color: #ecb00e;padding: 2px 3px;padding-bottom: 4px; text-shadow: 1px 1px 1px #cbcbcb;font-size: 1em">
    USER LOGIN 
    <span style="color: #aaa;">:</span> 
    <strong style="color: #003351">{$user_login}</strong>
</span>

<div id="dialog_detail_notifikasi" style="display: none" title="Detail Notifikasi"></div>
{literal}
    <script>
        $('.notifikasi-link').click(function() {
            $("#dialog_detail_notifikasi").dialog('close');
        });
        $("#dialog_detail_notifikasi").dialog({
            width: '800',
            modal: true,
            resizable: false,
            autoOpen: false
        });
    </script>
{/literal}
