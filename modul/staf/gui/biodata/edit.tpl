{literal}
    <script type="text/javascript" src="js/datetimepicker.js"></script>
    <script language="javascript">
        $(document).ready(function() {

            $("#nip").blur(function()
            {
                $("#msgbox1").text("Checking...").fadeIn("slow");
                $.post("cek_nip.php", {nip: $("#nip").val(), id: $("#id_pengguna").val()}, function(data)
                {
                    if (data == "ada")
                    {
                        $("#msgbox1").fadeTo(200, 0.1, function()
                        {
                            $(this).html("<font color='red'>NIP/NIK tidak valid, sudah digunakan</font>").fadeTo(900, 1);
                        });
                    }
                    else
                    {
                        $("#msgbox1").fadeTo(200, 0.1, function()
                        {
                            $(this).html("").fadeTo(900, 1);
                        });
                    }

                });

            });
        });

    </script>
{/literal}

<div class="center_title_bar">Edit Data Karyawan</div>
{* biodata karwayan *}
<form name="update" id="update" action="biodata_edit.php?{$smarty.server.QUERY_STRING}" method="post">
    <table class="tablesorter" style="width: 98%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <th style="text-align:center;width: 90%"><strong>BIODATA</strong></th>
            <th style="text-align:center;width: 90%"><span onMouseOver="this.style.cursor = 'pointer'"><img src="../sumberdaya/includes/images/left.png" alt="Kembali" title="Kembali" onclick="javascript:history.go(-1);" /></span></th>
        </tr>
    </table>
    <table class="tablesorter" style="width: 98%"  cellspacing="0" cellpadding="0" border="0">
        {foreach item="peg" from=$PEGAWAI}
            <input type="hidden" name="id_pegawai" id="id_pegawai" value="{$peg.ID_PEGAWAI}">
            <input type="hidden" name="id_pengguna" id="id_pengguna" value="{$peg.ID_PENGGUNA}">
            <input type="hidden" name="action" value="update">
            <tr>
                <td>NIP/NIK</td>
                <td style="text-align:center">:</td>
                <td><input type="text" name="nip" id="nip" style="width:200px;" value="{$peg.USERNAME}"/>&nbsp;&nbsp;&nbsp;<span id="msgbox1" style="display:none"></span></td>
                <td rowspan="7">
                    <img src="{$PHOTO}" border="0" width="160" />

                </td>
            </tr>
            <tr>
                <td>NIP/NIK LAMA</td>
                <td style="text-align:center"></td>
                <td><input  type="text" name="nip_lama" style="width:200px;" value="{$peg.NIP_LAMA}"/></td>
            </tr>
            <tr>
                <td>Unit Kerja</td>
                <td style="text-align:center">:</td>
                <td><select name="id_unit_kerja" id="id_unit_kerja">
                        {foreach item="unit_kerja" from=$KD_UK}
                            {html_options values=$unit_kerja.ID_UNIT_KERJA output=$unit_kerja.UNITKERJA selected=$peg.ID_UNIT_KERJA_SD}
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td>Status Kepegawaian</td>
                <td style="text-align:center">:</td>
                <td>
                    <select name="status_peg" id="status_peg">
                        <option value="PNS" {if $peg.STATUS_PEGAWAI=='PNS'}selected="true"{/if}>PNS</option>
                        <option value="CPNS" {if $peg.STATUS_PEGAWAI=='CPNS'}selected="true"{/if}>CPNS</option>
                        <option value="HONORER" {if $peg.STATUS_PEGAWAI=='HONORER'}selected="true"{/if}>HONORER</option>
                        <option value="KONTRAK" {if $peg.STATUS_PEGAWAI=='KONTRAK'}selected="true"{/if}>KONTRAK</option>
                        <option value="GB UNAIR" {if $peg.STATUS_PEGAWAI=='GB UNAIR'}selected="true"{/if}>GB UNAIR</option>
                        <option value="GB EMIRITUS" {if $peg.STATUS_PEGAWAI=='GB EMIRITUS'}selected="true"{/if}>GB EMIRITUS</option>
                        <option value="LB" {if $peg.STATUS_PEGAWAI=='LB'}selected="true"{/if}>LB</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Prajabatan Nomor</td>
                <td style="text-align:center"></td>
                <td><input  type="text" name="prajab_nomor" style="width:200px;" value="{$peg.PRAJAB_NOMOR}"/></td>
            </tr>
            <tr>
                <td>Prajabatan Tanggal</td>
                <td style="text-align:center"></td>
                <td><input  type="text" name="prajab_tanggal" id="prajab_tanggal" style="width:200px;"  onclick="javascript:NewCssCal('prajab_tanggal', 'ddmmyyyy', '', '', '', '', 'past');" value="{$peg.PRAJAB_TANGGAL|date_format:'%d-%m-%Y'}"/></td>
            </tr>
            <tr>
                <td>TGL Sumpah PNS</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input  type="text" name="tgl_sumpah_pns" id="tgl_sumpah_pns" style="width:200px;"  onclick="javascript:NewCssCal('tgl_sumpah_pns', 'ddmmyyyy', '', '', '', '', 'past');" value="{$peg.TGL_SUMPAH_PNS|date_format:'%d-%m-%Y'}"/></td>
            </tr>
            <tr>
                <td>TMT CPNS</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input  type="text" name="tmt_cpns" id="tmt_cpns" style="width:200px;"  onclick="javascript:NewCssCal('tmt_cpns', 'ddmmyyyy', '', '', '', '', 'past');" value="{$peg.tmt_cpns|date_format:'%d-%m-%Y'}"/></td>
            </tr>
            <tr>
                <td>Nomer Karpeg</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input  type="text" name="nomor_karpeg" style="width:500px;" value="{$peg.NOMER_KARPEG}"/></td>
            </tr>
            <tr>
                <td>Nomer NPWP</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input  type="text" name="nomor_npwp" style="width:500px;" value="{$peg.NOMOR_NPWP}"/></td>
            </tr>
            <tr>
                <td>Taspen</td>
                <td style="text-align:center"></td>
                <td colspan="2">
                    <select name="taspen">
                        <option value="1" {if $peg.TASPEN==1}selected="true"{/if}>Sudah</option>
                        <option value="0" {if $peg.TASPEN==0}selected="true"{/if}>Belum</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Pangkat (Gol.) Terakhir / TMT</td>
                <td style="text-align:center"></td>
                {if $peg.NM_GOLONGAN != null}
                    <td colspan="2" >{$peg.NM_GOLONGAN} - {$peg.NM_PANGKAT}  / {$peg.TMT_GOLONGAN}</td>
                {else}
                    <td colspan="2" >-</td>
                {/if}
            </tr>
            <tr>
                <td>Unit Esselon I</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input  type="text" name="unit_esselon1" style="width:500px;" value="{$peg.UNIT_ESSELON_I}"/></td>
            </tr>
            <tr>
                <td>Unit Esselon II</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input  type="text" name="unit_esselon2" style="width:500px;" value="{$peg.UNIT_ESSELON_II}"/></td>
            </tr>
            <tr>
                <td>Unit Esselon III</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input  type="text" name="unit_esselon3" style="width:500px;" value="{$peg.UNIT_ESSELON_III}"/></td>
            </tr>
            <tr>
                <td>Unit Esselon IV</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input  type="text" name="unit_esselon4" style="width:500px;" value="{$peg.UNIT_ESSELON_IV}"/></td>
            </tr>
            <tr>
                <td>Unit Esselon V</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input  type="text" name="unit_esselon5" style="width:500px;" value="{$peg.UNIT_ESSELON_V}"/></td>
            </tr>
            <tr>
                <td>Jabatan Fungsional / TMT </td>
                <td style="text-align:center"></td>
                {if $peg.NM_JABATAN_FUNGSIONAL != null}
                    <td colspan="2" >{$peg.NM_JABATAN_FUNGSIONAL}  / {$peg.TMT_JAB_FUNGSIONAL} </td>
                {else}
                    <td colspan="2" >-</td>
                {/if}
            </tr>
            <tr>
                <td>Tugas Tambahan</td>
                <td style="text-align:center"></td>
                {if $peg.NM_JABATAN_PEGAWAI != null}
                    <td colspan="2" >{$peg.NM_JABATAN_PEGAWAI}</td>
                {else}
                    <td colspan="2" >-</td>
                {/if}
            </tr>
            <tr>
                <td>Pendidikan Akhir</td>
                <td style="text-align:center"></td>
                {if $peg.NAMA_PENDIDIKAN_AKHIR != null}
                    <td colspan="2" >{$peg.NAMA_PENDIDIKAN_AKHIR} {$peg.NM_SEKOLAH_PENDIDIKAN} {$peg.NM_JURUSAN_PENDIDIKAN} ({$peg.TAHUN_MASUK_PENDIDIKAN}-{$peg.TAHUN_LULUS_PENDIDIKAN})</td>
                {else}
                    <td colspan="2" >-</td>
                {/if}
            </tr>
            <tr>
                <td>Status Aktif</td>
                <td style="text-align:center"></td>
                {if $peg.ID_STATUS_PENGGUNA != null}
                    <td colspan="2" >
                        <select name="aktif" id="aktif">
                            {foreach item="aktif" from=$STS_AKTIF}
                                {html_options values=$aktif.ID_STATUS_PENGGUNA output=$aktif.NM_STATUS_PENGGUNA selected=$peg.ID_STATUS_PENGGUNA}
                            {/foreach}
                        </select>
                    </td>
                {else}
                    <td colspan="2" >
                        <select name="aktif" id="aktif">
                            <option value=''></option>
                            {foreach item="aktif" from=$STS_AKTIF}
                                {html_options values=$aktif.ID_STATUS_PENGGUNA output=$aktif.NM_STATUS_PENGGUNA}
                            {/foreach}
                        </select>
                    </td>
                {/if}
            </tr>
            <tr>
                <td>Gelar Depan</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input  type="text" name="gelar_dpn" style="width:200px;" value="{$peg.GELAR_DEPAN}"/></td>
            </tr>
            <tr>
                <td>Gelar Belakang</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input  type="text" name="gelar_blkg" style="width:200;" value="{$peg.GELAR_BELAKANG}"/></td>
            </tr>
            <tr>
                <td>Nama Lengkap</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input type="text" name="nm_lengkap" style="width:450px;" value="{$peg.NM_PENGGUNA}"/></td>
            </tr>
            <tr>
                <td>Tempat Lahir</td>
                <td style="text-align:center"></td>
                <td colspan="2">
                    {if $peg.ID_KOTA_LAHIR == null}
                        <select name="id_kota_lahir">
                            <option value="null" selected="true"></option>
                            {for $i=0 to $count_provinsi}
                                <optgroup label="{$data_kota[$i].nama}">
                                    {foreach $data_kota[$i].kota as $data}
                                        {html_options values=$data.ID_KOTA output=$data.KOTA}
                                    {/foreach}
                                </optgroup>
                            {/for}
                        </select>
                    {else}
                        <select name="id_kota_lahir">
                            {for $i=0 to $count_provinsi}
                                <optgroup label="{$data_kota[$i].nama}">
                                    {foreach $data_kota[$i].kota as $data}
                                        {html_options values=$data.ID_KOTA output=$data.KOTA selected=$peg.ID_KOTA_LAHIR}
                                    {/foreach}
                                </optgroup>
                            {/for}
                        </select>
                    {/if}
                    &nbsp;, Tanggal Lahir : <input type="text" name="tgl_lahir" id="tgl_lahir" style="text-align:center;" onclick="javascript:NewCssCal('tgl_lahir', 'ddmmyyyy', '', '', '', '', 'past');" value="{$peg.TGL_LAHIR_PENGGUNA|date_format:'%d-%m-%Y'}">
                </td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                <td style="text-align:center"></td>
                {if $peg.KELAMIN_PENGGUNA == '1' || $peg.KELAMIN_PENGGUNA == '2'}
                    <td colspan="2">
                        <select name="jk" id="jk">
                            {foreach item="jk" from=$JK}
                                {html_options values=$jk.KELAMIN_PENGGUNA output=$jk.NM_KELAMIN_PENGGUNA selected=$peg.KELAMIN_PENGGUNA}
                            {/foreach}
                        </select>
                    </td>
                {else}
                    <td colspan="2">
                        <select name="jk" id="jk">
                            <option value=''></option>
                            {foreach item="jk" from=$JK}
                                {html_options values=$jk.KELAMIN_PENGGUNA output=$jk.NM_KELAMIN_PENGGUNA}
                            {/foreach}
                        </select>
                    </td>
                {/if}
            </tr>
            <tr>
                <td>Status Pernikahan</td>
                <td style="text-align:center"></td>
                {if $peg.ID_STATUS_PERNIKAHAN != null}
                    <td colspan="2" >
                        <select name="status_nikah" id="agama">
                            {foreach item="sn" from=$SNIKAH}
                                {html_options values=$sn.ID_STATUS_PERNIKAHAN output=$sn.NM_STATUS_PERNIKAHAN selected=$peg.ID_STATUS_PERNIKAHAN}
                            {/foreach}
                        </select>
                    </td>
                {else}
                    <td colspan="2" >
                        <select name="status_nikah" id="agama">
                            {foreach item="sn" from=$SNIKAH}
                                {html_options values=$sn.ID_STATUS_PERNIKAHAN output=$sn.NM_STATUS_PERNIKAHAN}
                            {/foreach}
                        </select>
                    </td>
                {/if}
            </tr>
            <tr>
                <td>Agama</td>
                <td style="text-align:center"></td>
                {if $peg.ID_AGAMA != ''}
                    <td colspan="2">
                        <select name="agama" id="jk">
                            {foreach item="ag" from=$AGAMA}
                                {html_options values=$ag.ID_AGAMA output=$ag.NM_AGAMA selected=$peg.ID_AGAMA}
                            {/foreach}
                        </select>
                    </td>
                {else}
                    <td colspan="2">
                        <select name="agama" id="jk">
                            {foreach item="ag" from=$AGAMA}
                                {html_options values=$ag.ID_AGAMA output=$ag.NM_AGAMA}
                            {/foreach}
                        </select>
                    </td>
                {/if}
            </tr>
            <tr>
                <td>Alamat</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input type="text" name="alamat" style="width:650px;" value="{$peg.ALAMAT_RUMAH_PEGAWAI}" /></td>
            </tr>
            <tr>
                <td>Kode Pos</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input type="text" name="kode_pos" style="width:50px;" value="{$peg.KODE_POS}" /></td>
            </tr>
            <tr>
                <td>Telepon/HP</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input type="text" name="tlp" style="text-align:center;" value="{$peg.TELP_PEGAWAI}" maxlength="15"/> / <input type="text" name="hp" style="text-align:center;" value="{$peg.MOBILE_PEGAWAI}" maxlength="15"/></td>
            </tr>
            <tr>
                <td>Email #1</td>
                <td style="text-align:center"></td>
                <td colspan="2">{$peg.EMAIL_PENGGUNA}</td>
            </tr>
            <tr>
                <td>Email #2</td>
                <td style="text-align:center"></td>
                <td colspan="2"><input type="text" name="email" style="width:335px;" value="{$peg.EMAIL_ALTERNATE}" maxlength="100"/></td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="4" style="text-align: right;width: 850">
                <input type="submit" class="button" value="Simpan"/>
                <a href="biodata.php" class="button">Kembali</a>
            </td>
        </tr>
    </table>
</form>
<p><br/>&nbsp;</p>