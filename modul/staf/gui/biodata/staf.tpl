<div class="center_title_bar">Info Pegawai</div>
<div id="biodata_info_pegawai" class="ui-widget"  >
    <div id="dialog_info_pegawai" style="display: none;">Data Info Pegawai Berhasil Di Simpan</div>
    <div id="alert_info_pegawai" style="display: none;">Klik edit mode terlebih dahulu !</div>
    <form name="info_pegawai" id="form_info_pegawai" action="info_pegawai.php" method="post">
        <table class="none" style="width: 100%;">
            <tr class="ui-widget-header">
                <th colspan="4" class="header-coloumn"><h2>INFORMASI DATA PEGAWAI</h2></th>
            </tr>
            <tr>
                <td width="20%"><span class="field">Fak/Unit</span></td>
                <td width="30%"><span>{$data_info_pegawai['FAKULTAS']|upper}</span></td>
                <td width="20%"><span class="field">Jur/bag/lab</span></td>
                <td width="30%"><span>{$data_info_pegawai['PRODI']|upper}</span></td>
            </tr>
            <tr>
                <td><span class="field">Jenis Peg</span></td>
                <td><span>{$data_info_pegawai['JENIS_PEGAWAI']|upper}</span></td>
                <td><span class="field">Status</span></td>
                <td><span>AKTIF</span></td>
            </tr>
            <tr class="ui-widget-header">
                <th colspan="4" class="header-coloumn"><h3>A.IDENTITAS PRIBADI</h3></th>
            </tr>
            <tr>
                <td><span class="field">Nama  </span></td>
                <td><span>{$data_info_pegawai['GELAR_DEPAN']} {$data_info_pegawai['NAMA']|upper} {$data_info_pegawai['GELAR_BELAKANG']}</span></td>
                <td><span class="field">NIP </span></td>
                <td><span>{$data_info_pegawai['NIP']}</span></td>
            </tr>
            <tr>
                <td><span class="field">Tempat Lahir</span></td>
                <td><span>{$data_info_pegawai['KOTA_LAHIR']|upper}</span></td>
                <td><span class="field">Email  </span></td>
                <td>
                    <a class="disable-ajax" href="http://mail.google.com/a/{$data_info_pegawai['LINKEMAIL']}" target="_blank">{$data_info_pegawai['EMAIL']}</a>
					<br>{$data_info_pegawai['RENAMEEMAIL']}
                </td>
            </tr>
            <tr>
                <td>
                    <span class="field">Blog Pegawai</span>
                </td>
                <td>
                    <a class="disable-ajax" href="{$data_info_pegawai['LINK_BLOG']}" target="_blank">{$data_info_pegawai['BLOG_PENGGUNA']}</a>
                </td>
                <td><span class="field">Contact Person  </span></td>
                <td>
                    {$data_info_pegawai['TELP']}
                </td>
            </tr>
            <tr>
                <td><span class="field">Tanggal Lahir  </span></td>
                <td><span>{$data_info_pegawai['TGL_LAHIR']}</span></td>
                <td><span class="field">Agama</span></td>
                <td><span>{$data_info_pegawai['AGAMA']|upper}</span></td>
            </tr>
            <tr>
                <td><span class="field">Alamat  </span></td>
                <td colspan="3">
                    {$data_info_pegawai['ALAMAT_PEGAWAI']|upper}
                </td>
            </tr>
            <tr>
                <td><span class="field">Jenis Kelamin  </span></td>
                <td colspan="3">
                    {if $data_info_pegawai['KELAMIN'] == 2}
                        <span>PEREMPUAN</span>
                    {else}
                        <span>LAKI-LAKI</span>
                    {/if}
                </td>
            </tr>
			<tr>
				<td colspan="4">
					<center>
						<a href="biodata.php?mode=edit">Edit</a>
					</center>
				</td>
			</tr>
			<!--  -->
            <tr class="ui-widget-header">
                <th colspan="4" class="header-coloumn"><h3>C.PROFILE KEPEGAWAIAN</h3></th>
            </tr>
            <tr>
                <td><span class="field">Pangkat/GOl</span></td>
                <td><span>{$data_info_pegawai['NM_GOLONGAN']|upper}</span></td>
                <td><span class="field">Tmt Pangkat</span></td>
                <td><span></span></td>
            </tr>
            <tr>
                <td><span class="field">Jab.Fungs</span></td>
                <td><a href="#"></a></td>
                <td><span class="field">Tmt Fungs</span></td>
                <td><span></span></td>
            </tr>
            <tr>
                <td><span class="field">Jabatan Struktural</span></td>
                <td><span>{$data_info_pegawai['JABATAN']|upper}</span></td>
                <td><span class="field">Tmt Struk</span></td>
                <td><span></span></td>
            </tr>
            <tr>
                <td><span class="field">Lat.Jabatan</span></td>
                <td><span></span></td>
                <td><span class="field">Tmt Pensiun</span></td>
                <td><span></span></td>
            </tr>
        </table>
    </form>
</div>
{literal}
    <script type="text/javascript" >
    $('#save').click(function(){
        if($(':disabled').size()=='4'){
            $('#alert_info_pegawai').dialog('open')
        }else{
            $('#form_info_pegawai').submit();
        }
    });
    $('#form_info_pegawai').validate({
        rules :{
            telp :{
                required :true,
                number : true
            },
            email : {
                required :true,
                email :true
            },
            alamat_rumah:{
                required :true
            },
            alamat_kantor:{
                required :true
            }
        }
    });
    $('#form_info_pegawai').ajaxForm({
        type : 'post',
        url : $(this).attr('action'),
        data : $(this).serialize(),
        success : function(data){
            $('#center_content').html(data);
            $("#dialog_info_pegawai").dialog("open")
        }
    });

    function enabled_textbox(){
        document.info_pegawai.alamat_rumah.disabled=false;
        document.info_pegawai.alamat_kantor.disabled=false;
        document.info_pegawai.email.disabled=false;
        document.info_pegawai.telp.disabled=false;
    }
    function disabled_textbox(){
        document.info_pegawai.alamat_rumah.disabled=true;
        document.info_pegawai.alamat_kantor.disabled=true;
        document.info_pegawai.email.disabled=true;
        document.info_pegawai.telp.disabled=true;
    }
    $("#dialog_info_pegawai").dialog({
        autoOpen:false,
        position:"middle",
        dragable:false,
        modal:true,
        resizable:false,
        title:"Confirmation Message"
    });
    $("#alert_info_pegawai").dialog({
        autoOpen:false,
        position:"middle",
        dragable:false,
        modal:true,
        resizable:false,
        title:"Alert Message"
    });
    </script>
{/literal}