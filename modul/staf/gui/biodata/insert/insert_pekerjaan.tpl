<html>
    <head>
        <script type="text/javascript" src="js/datetimepicker.js"></script>
        <script type="text/javascript" src="../sumberdaya/js/jquery-1.8.0.min.js"></script>
        <script language="JavaScript">
            function validate_form() {
                form_document = document.forms["klgdrinsert"];
                if (form_document["nama"].value == null || form_document["karsisu"].value == " " || form_document["tgl_nikah"].value == "")
                {
                    alert("Tidak Boleh Kosong");
                    return false;
                }
                return true;
            }
        </script>
    </head>
    <body>
        <form action="insert_pekerjaan.php" id="nikahinsert" method="post" onsubmit="return validate_form();">
            <input type="hidden" name="id_pengguna" value="{$smarty.get.id}">
            <table cellspacing="0" cellpadding="0" border="0">
                <tr class="collapse">
                    <td class="labelrow">Nama Jabatan&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="nama" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Intansi Pekerjaan&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="intansi" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">NO SK Jabatan Pekerjaan&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="no_sk" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tanggal SK Jabatan Struktural&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tgl_sk" style="width:120px; text-align:center;" id="tgl_sk" style="text-align:center;" onclick="javascript:NewCssCal('tgl_sk', 'ddmmyyyy', '', '', '', '', 'past')" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">TGl Mulai Jabatan Struktural&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tgl_mulai" style="width:120px; text-align:center;" id="tgl_mulai" style="text-align:center;" onclick="javascript:NewCssCal('tgl_mulai', 'ddmmyyyy', '', '', '', '', 'past')"  /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">TMT SK Jabatan Struktural&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tgl_tmt" style="width:120px; text-align:center;" id="tgl_tmt" style="text-align:center;" onclick="javascript:NewCssCal('tgl_tmt', 'ddmmyyyy', '', '', '', '', 'past')"  /></td>
                </tr>

                <tr class="collapse">
                    <td class="labelrow">NIP Pejabat TTD SK Jabatan&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="nip" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">NIP Lama Pejabat TTD SK Jabatan&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="nip_lama" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Pejabat TTD SK Jabatan&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="pejabat" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow"></td>
                    <td class="inputrow"><input type="submit" name="submit" value="Simpan" /></td>
                </tr>
            </table>
        </form>
    </body>
</html>