<html>
    <head>
        <script type="text/javascript" src="js/datetimepicker.js"></script>
        <script type="text/javascript" src="../sumberdaya/js/jquery-1.8.0.min.js"></script>
        <script language="JavaScript">
            function validate_form() {
                form_document = document.forms["klgdrinsert"];
                if (form_document["nama"].value == null || form_document["karsisu"].value == " " || form_document["tgl_nikah"].value == "")
                {
                    alert("Tidak Boleh Kosong");
                    return false;
                }
                return true;
            }
        </script>
    </head>
    <body>
        <form action="insert_tambahan.php" id="nikahinsert" method="post" onsubmit="return validate_form();">
            <input type="hidden" name="id_pengguna" value="{$smarty.get.id}">
            <table cellspacing="0" cellpadding="0" border="0">
                <tr class="collapse">
                    <td class="labelrow">Jabatan Struktural&nbsp;:&nbsp;</td>
                    <td class="inputrow">
                        <select name="jabatan" id="id_pdd">
                            {foreach item="js" from=$ID_STRUK}
                                {html_options values=$js.ID_JABATAN_STRUKTURAL output=$js.NM_JABATAN_STRUKTURAL}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">NO SK Jabatan Struktural&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="nomor" style="width:600px;" maxlength="50" value="" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Asal SK Jabatan Struktural&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="asal" style="width:600px;" maxlength="50" value="" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Keterangan SK&nbsp;:&nbsp;</td>
                    <td class="inputrow">
                        <textarea name="keterangan"></textarea>
                    </td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tanggal SK Jabatan Struktural&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tgl_sk" style="width:120px; text-align:center;" id="tgl_sk" style="text-align:center;" onclick="javascript:NewCssCal('tgl_sk', 'ddmmyyyy', '', '', '', '', 'past')" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">TMT SK Jabatan Struktural&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tgl_tmt" style="width:120px; text-align:center;" id="tgl_tmt" style="text-align:center;" onclick="javascript:NewCssCal('tgl_tmt', 'ddmmyyyy', '', '', '', '', 'past')"  /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Pejabatan TTD SK Jabatan Struktural&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="ttd" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow"></td>
                    <td class="inputrow"><input type="submit" name="submit" value="Simpan" /></td>
                </tr>
            </table>
        </form>
    </body>
</html>