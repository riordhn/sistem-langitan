<html>
    <head>
        <script type="text/javascript" src="js/datetimepicker.js"></script>
        <script type="text/javascript" src="../sumberdaya/js/jquery-1.8.0.min.js"></script>
        <script language="JavaScript">
            function validate_form() {
                if (document.forms["klgdrinsert"]["nama"].value == null || document.forms["klgdrinsert"]["hubungan"].value == " " || document.forms["klgdrinsert"]["tgl_lahir"].value == "")
                {
                    alert("Tidak Boleh Kosong");
                    return false;
                }
                return true;
            }
        </script>
    </head>
    <body>
        <form name="klgdrinsert" action="insert_klgdr.php" id="klgdrinsert" method="post" onsubmit="return validate_form();">
            <input type="hidden" name="id_pengguna" value="{$smarty.get.id}">
            <table cellspacing="0" cellpadding="0" border="0">
                <tr class="collapse">
                    <td class="labelrow">Nama Keluarga&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="nama" style="width:600px;" maxlength="50" value="" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Hubungan Keluarga&nbsp;:&nbsp;</td>
                    <td class="inputrow">
                        <select name="hubungan">
                            <option value="1">Ayah</option>
                            <option value="2">Ibu</option>
                            <option value="3">Kakak</option>
                            <option value="4">Adik</option>
                        </select>
                    </td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Jenis Kelamin&nbsp;:&nbsp;</td>
                    <td class="inputrow">
                        <select name="kelamin">
                            <option value="1">Laki-Laki</option>
                            <option value="2">Perempuan</option>
                        </select>
                    </td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Pekerjaan&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="pekerjaan" style="width:600px;" maxlength="50" value="" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tanggal Lahir&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tgl_lahir" style="width:120px; text-align:center;" id="tgl_lahir" style="text-align:center;" onclick="javascript:NewCssCal('tgl_lahir', 'ddmmyy', '', '', '', '', 'past')" value="" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tempat Lahir &nbsp;:&nbsp;</td>
                    <td class="inputrow">
                        <select id="negara" name="negara">
                            <option value="">Pilih Negara</option>
                            {foreach $negara as $n}
                                <option value="{$n.ID_NEGARA}">{$n.NM_NEGARA}</option>
                            {/foreach}
                        </select>
                        <select id="propinsi" name="propinsi">
                            <option value="">Pilih Propinsi</option>
                        </select>
                        <select id="kota" name="tmpat_lahir">
                            <option value="1">Pilih Kota</option>
                        </select>
                    </td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Kondisi Keluarga&nbsp;:&nbsp;</td>
                    <td class="inputrow">
                        <select name="kondisi">
                            <option value="1">Sudah Meninggal</option>
                            <option value="2">Masih Hidup</option>
                        </select>
                    </td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow"></td>
                    <td class="inputrow"><input type="submit" name="submit" value="Simpan" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('pdd{$d.ID_KELUARGA_DIRI}')" /></td>
                </tr>
            </table>
        </form>
        {literal}
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#negara').change(function() {
                        $.ajax({
                            type: "POST",
                            url: "getNegaraPropinsi.php",
                            data: {id: $('#negara').val()},
                            success: function(data) {
                                $('#propinsi').html(data);
                            }
                        })
                    });
                    $('#propinsi').change(function() {
                        $.ajax({
                            type: "POST",
                            url: "getPropinsiKota.php",
                            data: {id: $('#propinsi').val()},
                            success: function(data) {
                                $('#kota').html(data);
                            }
                        })
                    });
                });
            </script>
        {/literal}
    </body>
</html>