<html>
    <head>
        <script type="text/javascript" src="js/datetimepicker.js"></script>
        <script type="text/javascript" src="../sumberdaya/js/jquery-1.8.0.min.js"></script>
    </head>
    <body>
        <form name="diklatinsert" action="insert_kegpeg.php" id="klgdrinsert" method="post" onsubmit="return validate_form();">
            <input type="hidden" name="id_pengguna" value="{$smarty.get.id}">
            <table cellspacing="0" cellpadding="0" border="0">
                <tr class="collapse">
                    <td class="labelrow">Jenis Kegiatan&nbsp;:&nbsp;</td>
                    <td class="inputrow">
                        <select name="jenis">
                            <option value="TRAINING">TRAINING</option>
                            <option value="WORKSHOP">WORKSHOP</option>
                            <option value="LOKAKARYA">LOKAKARYA</option>
                            <option value="SEMINAR">SEMINAR</option>
                            <option value="SIMPOSIUM">SIMPOSIUM</option>
                        </select>
                    </td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Nama Kegiatan&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="nama" style="width:600px;" maxlength="50" value="" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Lokasi&nbsp;:&nbsp;</td>
                    <td class="inputrow">
                        <textarea name="lokasi" style="width: 90%;height: 120px;resize: none"></textarea>
                    </td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tempat Lahir &nbsp;:&nbsp;</td>
                    <td class="inputrow">
                        <select id="negara" name="negara">
                            <option value="">Pilih Negara</option>
                            {foreach $negara as $n}
                                <option value="{$n.ID_NEGARA}">{$n.NM_NEGARA}</option>
                            {/foreach}
                        </select>
                        <select id="propinsi" name="propinsi">
                            <option value="">Pilih Propinsi</option>
                        </select>
                        <select id="kota" name="kota_lokasi">
                            <option value="1">Pilih Kota</option>
                        </select>
                    </td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Status Luar Negeri&nbsp;:&nbsp;</td>
                    <td class="inputrow">
                        <select name="status_luar">
                            <option value="1">Luar Negeri</option>
                            <option value="2">Dalam Negeri</option>
                        </select>
                    </td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tanggal Mulai&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tgl_mulai" style="width:120px; text-align:center;" id="tgl_mulai" style="text-align:center;" onclick="javascript:NewCssCal('tgl_mulai', 'ddmmyyyy', '', '', '', '', 'past')"  /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tanggal Selesai&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tgl_selesai" style="width:120px; text-align:center;" id="tgl_selesai" style="text-align:center;" onclick="javascript:NewCssCal('tgl_selesai', 'ddmmyyyy', '', '', '', '', 'past')"  /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Penyelenggara&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="penyelenggara" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Kedudukan/Peran&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="kedudukan" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tingkat Kegiatan&nbsp;:&nbsp;</td>
                    <td class="inputrow">
                        <select name="tingkat">
                            <option value=""></option>
                            <option value="1">Internasional</option>
                            <option value="2">Nasional</option>
                            <option value="2">Lokal</option>
                        </select>
                    </td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow"></td>
                    <td class="inputrow"><input type="submit" name="submit" value="Simpan" /><input type="button" name="cancel" value="Batal" /></td>
                </tr>
            </table>
        </form>
        {literal}
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#negara').change(function() {
                        $.ajax({
                            type: "POST",
                            url: "getNegaraPropinsi.php",
                            data: {id: $('#negara').val()},
                            success: function(data) {
                                $('#propinsi').html(data);
                            }
                        })
                    });
                    $('#propinsi').change(function() {
                        $.ajax({
                            type: "POST",
                            url: "getPropinsiKota.php",
                            data: {id: $('#propinsi').val()},
                            success: function(data) {
                                $('#kota').html(data);
                            }
                        })
                    });
                });
            </script>
        {/literal}
    </body>
</html>