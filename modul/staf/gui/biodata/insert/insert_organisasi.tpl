<html>
    <head>
        <script type="text/javascript" src="js/datetimepicker.js"></script>
        <script type="text/javascript" src="../sumberdaya/js/jquery-1.8.0.min.js"></script>
    </head>
    <body>
        <form name="diklatinsert" action="insert_organisasi.php" id="klgdrinsert" method="post" onsubmit="return validate_form();">
            <input type="hidden" name="id_pengguna" value="{$smarty.get.id}">
            <table cellspacing="0" cellpadding="0" border="0">
                <tr class="collapse">
                    <td class="labelrow">Nama Organisasi&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="nama" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Jabatan Organisasi&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="kedudukan" style="width:600px;" maxlength="50"/></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tanggal Mulai&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tgl_mulai" style="width:120px; text-align:center;" id="tgl_mulai" style="text-align:center;" onclick="javascript:NewCssCal('tgl_mulai', 'ddmmyyyy', '', '', '', '', 'past')"/></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tanggal Selesai&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tgl_selesai" style="width:120px; text-align:center;" id="tgl_selesai" style="text-align:center;" onclick="javascript:NewCssCal('tgl_selesai', 'ddmmyyyy', '', '', '', '', 'past')" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">NO SK Organisasi&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="no_sk" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Jabatan Pemberi SK&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="jabatan_pemberi" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tingkat Organisasin&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tingkat" placeholder="Nasional/Internasional/Lokal" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">
                        <input type="hidden" name="id_organ" value="{$d.ID_SEJARAH_ORGANISASI}" />
                        <input type="hidden" name="action" value="edit_organ" />
                    </td>
                    <td class="inputrow"><input type="submit" name="submit" value="Update" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('penghar{$d.ID_KELUARGA_DIRI}')" /></td>
                </tr>
            </table>
        </table>
    </form>
</body>
</html>