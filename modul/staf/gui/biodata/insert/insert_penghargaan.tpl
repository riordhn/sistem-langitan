<html>
    <head>
        <script type="text/javascript" src="js/datetimepicker.js"></script>
        <script type="text/javascript" src="../sumberdaya/js/jquery-1.8.0.min.js"></script>
    </head>
    <body>
        <form name="diklatinsert" action="insert_penghargaan.php" id="klgdrinsert" method="post" onsubmit="return validate_form();">
            <input type="hidden" name="id_pengguna" value="{$smarty.get.id}">
            <table cellspacing="0" cellpadding="0" border="0">
                <tr class="collapse">
                    <td class="labelrow">Nama Penghargaan&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="nama" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Nomor Penghargaan&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="nomor" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Bidang Penghargaan&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="bidang" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Bentuk Penghargaan&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="bentuk" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tanggal Perolehan&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tgl_perolehan" style="width:120px; text-align:center;" id="tgl_perolehan" style="text-align:center;" onclick="javascript:NewCssCal('tgl_perolehan', 'ddmmyyyy', '', '', '', '', 'past')" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Negara Pemberi&nbsp;:&nbsp;</td>
                    <td class="inputrow">
                        <select name="negara">
                            <option value="">Pilih Negara</option>
                            {foreach $negara as $n}
                                <option value="{$n.ID_NEGARA}">{$n.NM_NEGARA}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Nama Pemberi&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="pemberi" style="width:600px;" maxlength="50"/></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Intansi Pemberi&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="intansi" style="width:600px;" maxlength="50"/></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Jabatan Pemberi&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="jabatan" style="width:600px;" maxlength="50"/></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tingkat Penghargaan&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tingkat" placeholder="Nasional/Internasional/Lokal" style="width:600px;" maxlength="50"/></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">
                        <input type="hidden" name="id_penghar" value="{$d.ID_SEJARAH_PENGHARGAAN}" />
                        <input type="hidden" name="action" value="edit_penghar" />
                    </td>
                    <td class="inputrow"><input type="submit" name="submit" value="Update" /><input type="button" name="cancel" value="Batal" onclick="javascript:edit('penghar{$d.ID_KELUARGA_DIRI}')" /></td>
                </tr>
            </table>
        </table>
    </form>
</body>
</html>