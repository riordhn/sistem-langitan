<div class="center_title_bar">Pesan Baru</div>
<form action="new-message.php" method="post">
    <fieldset style="width: 98%">
        <legend>Buat Pesan Baru</legend>
        <div class="clear-fix"></div>
        <div class="field_form">
            <label>Pengirim : </label>
            <input type="text" size="50" readonly="" maxlength="50" value="{$pesan.GELAR_DEPAN_PENGIRIM} {$pesan.NAMA_PENGIRIM} {$pesan.GELAR_BELAKANG_PENGIRIM}"/>
        </div>
        <div class="clear-fix"></div>
        <div class="field_form">
            <label>Penerima : </label>
            <input type="text" size="50" readonly="" maxlength="50" value="{$pesan.GELAR_DEPAN_PENERIMA} {$pesan.NAMA_PENERIMA} {$pesan.GELAR_BELAKANG_PENERIMA}"/>
        </div>
        <div class="clear-fix"></div>
        <div class="field_form">
            <label>Judul Pesan : </label>
            <input type="text" size="120" name="judul" readonly="" class="required" maxlength="120" value="{$pesan.JUDUL_PESAN}"/>
        </div>
        <div class="clear-fix"></div>
        <div class="field_form">
            <label>Isi Pesan: </label>
            <textarea name="isi" style="resize: none;width: 80%" readonly="" class="required" rows="6">{$pesan.ISI_PESAN}</textarea>
        </div>
        <div class="clear-fix"></div>
        <div class="bottom_field_form">
            <a onclick="history.go(-1)" class="button disable-ajax">Batal</a>
            {if $smarty.get.mode=='inbox'}
                <a href="reply.php?p={$pesan.ID_PESAN}" class="button">Balas Pesan</a>
            {/if}
        </div>
    </fieldset>
</form>
{literal}
    <script>
        $(document).ready(function() {
            oTable = $('.datatable').dataTable({
                "bJQueryUI": true,
                "sPaginationType": "full_numbers"
            });
            $('form').validate();
            $(".datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-M-y'
            }).css({'text-transform': 'uppercase'});

            $('#jenis').change(function() {
                $('#content').load('new-message.php?jenis=' + $(this).val());
                $.ajax({
                    type: 'post',
                    url: 'getDataPenerima.php',
                    data: 'jenis=' + $(this).val(),
                    success: function(data) {
                        $('#id_penerima').html(data);
                        $('#id_penerima').chosen();
                    }
                });

            });
        });
    </script>
{/literal}