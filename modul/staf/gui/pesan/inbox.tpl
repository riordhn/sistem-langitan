<div class="center_title_bar">Pesan Masuk</div>
<h2>Data Pesan Masuk</h2>
<a class="button" href="new-message.php">Buat Pesan Baru</a>
<p></p>
<table cellpadding="0" cellspacing="0" border="0" class="datatable display" >
    <thead>
        <tr>
            <th>Nama Pengirim</th>
            <th>Judul</th>
            <th>Ringkasan Isi</th>
            <th class="center">Waktu</th>
            <th class="center">----</th>
        </tr>
    </thead>
    <tbody>
        {foreach $data_inbox as $d}
            <tr {if $d@index%2==0}class="even"{else}class="odd"{/if} {if $d.BACA_PENERIMA==0} style="color: red;font-weight: bold"{/if}>
                <td>{$d.GELAR_DEPAN_PENGIRIM} {$d.NAMA_PENGIRIM} {$d.GELAR_BELAKANG_PENGIRIM}</td>
                <td>{$d.JUDUL_PESAN}</td>
                <td>{SplitWord($d.ISI_PESAN,5)}...</td>
                <td class="center">{$d.WAKTU}</td>
                <td class="center">
                    <a href="message-detail.php?p={$d.ID_PESAN}&mode=inbox" class="ui-state-default ui-corner-all ui-icon ui-icon-zoomin" title="Lihat Pesan" style="cursor: pointer;display: inline-block;vertical-align: top"> </a>
                    <a href="reply.php?p={$d.ID_PESAN}"  class="ui-state-default ui-corner-all ui-icon ui-icon-comment" title="Balas" style="cursor: pointer;display: inline-block;vertical-align: top"> </a>
                    <form method="post" action="inbox.php" style="display: inline-block">
                        <input type="hidden" name="id_pesan" value="{$d.ID_PESAN}"/>
                        <input type="hidden" name="mode" value="hapus"/>
                        <input type="submit" title="Hapus" class="ui-state-default ui-corner-all ui-icon ui-icon-trash" style="display:inline-block ;padding: 8px;cursor: pointer;"/>
                    </form>
                </td>
            </tr>
        {/foreach}
    </tbody>
</table>
{literal}
    <script>
        $(document).ready(function() {
            oTable = $('.datatable').dataTable({
                "bJQueryUI": true,
                "sPaginationType": "full_numbers"
            });
            $('form').validate();
            $(".datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-M-y'
            }).css({'text-transform': 'uppercase'});
            $("#kategori").autocomplete({
                source: "getKategoriKomplain.php",
                minLength: 3
            });
        });
    </script>
{/literal}