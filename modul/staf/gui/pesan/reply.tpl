<div class="center_title_bar">Pesan Baru</div>
<form action="reply.php" method="post">
    <fieldset style="width: 98%">
        <legend>Buat Pesan Baru</legend>
        <div class="clear-fix"></div>
        <div class="field_form">
            <label>Pengirim : </label>
            <input type="text" size="50" readonly="" maxlength="50" value="{$pesan.GELAR_DEPAN_PENERIMA} {$pesan.NAMA_PENERIMA} {$pesan.GELAR_BELAKANG_PENERIMA}"/>
            <input type="hidden" name="id_pengirim" value="{$pesan.ID_PENERIMA}"/>
        </div>
        <div class="clear-fix"></div>
        <div class="field_form">
            <label>Penerima : </label>
            <input type="text" size="50" readonly="" maxlength="50" value="{$pesan.GELAR_DEPAN_PENGIRIM} {$pesan.NAMA_PENGIRIM} {$pesan.GELAR_BELAKANG_PENGIRIM}"/>
            <input type="hidden" name="id_penerima" value="{$pesan.ID_PENGIRIM}"/>
        </div>
        <div class="clear-fix"></div>
        <div class="field_form">
            <label>Judul Pesan : </label>
            <input type="text" size="120" name="judul" class="required" maxlength="120" value="Re : {$pesan.JUDUL_PESAN}"/>
        </div>
        <div class="clear-fix"></div>
        <div class="field_form">
            <label>Isi Pesan: </label>
            <textarea name="isi" style="resize: none;width: 80%"  class="required" rows="6"></textarea>
        </div>
        <div class="clear-fix"></div>
        <div class="bottom_field_form">
            <a href="inbox.php" class="button">Batal</a>
            <input type="hidden" name="mode" value="simpan"/>
            <input type="submit" class="button" value="Kirim"/>
        </div>
    </fieldset>
</form>
{literal}
    <script>
        $(document).ready(function() {
            oTable = $('.datatable').dataTable({
                "bJQueryUI": true,
                "sPaginationType": "full_numbers"
            });
            $('form').validate();
            $(".datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-M-y'
            }).css({'text-transform': 'uppercase'});

            $('#jenis').change(function() {
                $('#content').load('new-message.php?jenis=' + $(this).val());
                $.ajax({
                    type: 'post',
                    url: 'getDataPenerima.php',
                    data: 'jenis=' + $(this).val(),
                    success: function(data) {
                        $('#id_penerima').html(data);
                        $('#id_penerima').chosen();
                    }
                });

            });
        });
    </script>
{/literal}