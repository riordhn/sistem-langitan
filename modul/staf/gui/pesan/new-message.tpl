<div class="center_title_bar">Pesan Baru</div>
<form action="new-message.php" method="post">
    <fieldset style="width: 98%">
        <legend>Buat Pesan Baru</legend>
        <div class="clear-fix"></div>
        <div class="field_form">
            <label>Pengirim : </label>
            <input type="text" size="50" readonly="" maxlength="50" value="{$pengirim}"/>
            <input type="hidden" name="pengirim" value="{$id_pengirim}"/>
        </div>
        <div class="clear-fix"></div>
        <div class="field_form">
            <label>Jenis Penerima : </label>
            <select id="jenis" name="jenis">
                <option value="0">Pilih Jenis Penerima</option>
                <option value="1" {if $smarty.get.jenis==1}selected=""{/if}>Dosen</option>
                <option value="2" {if $smarty.get.jenis==2}selected=""{/if}>Tenaga Non Dosen</option>
            </select>
        </div>
        <div class="clear-fix"></div>
        <div class="field_form">
            <label>Penerima : </label>
            <select id="id_penerima" name="id_penerima">
                <option value="0">Pilih Penerima</option>
            </select>
        </div>
        <div class="clear-fix"></div>
        <div class="field_form">
            <label>Judul Pesan : </label>
            <input type="text" size="120" name="judul" class="required" maxlength="30" value=""/>
        </div>
        <div class="clear-fix"></div>
        <div class="field_form">
            <label>Isi Pesan: </label>
            <textarea name="isi" style="resize: none;width: 80%" class="required" rows="6"></textarea>
        </div>
        <div class="clear-fix"></div>
        <div class="bottom_field_form">
            <a href="inbox.php" class="button">Batal</a>
            <input type="hidden" name="mode" value="simpan"/>
            <input type="submit" class="button" value="Kirim"/>
        </div>
    </fieldset>
</form>
{literal}
    <script>
        $(document).ready(function() {
            oTable = $('.datatable').dataTable({
                "bJQueryUI": true,
                "sPaginationType": "full_numbers"
            });
            $('form').validate();
            $(".datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-M-y'
            }).css({'text-transform': 'uppercase'});

            $('#jenis').change(function() {
                $('#content').load('new-message.php?jenis='+$(this).val());
                $.ajax({
                    type: 'post',
                    url: 'getDataPenerima.php',
                    data :'jenis='+$(this).val(),
                    success: function(data) {
                        $('#id_penerima').html(data);
                        $('#id_penerima').chosen();
                    }
                });

            });
        });
    </script>
{/literal}