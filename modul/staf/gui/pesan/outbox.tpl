<div class="center_title_bar">Pesan Keluar</div>
<h2>Data Pesan Keluar</h2>
<p></p>
<table cellpadding="0" cellspacing="0" border="0" class="datatable display" >
    <thead>
        <tr>
            <th>Nama Penerima</th>
            <th>Judul</th>
            <th>Ringkasan Isi</th>
            <th class="center">Waktu</th>
            <th class="center">----</th>
        </tr>
    </thead>
    <tbody>
        {foreach $data_inbox as $d}
            <tr {if $d@index%2==0}class="even"{else}class="odd"{/if} >
                <td>{$d.GELAR_DEPAN_PENERIMA} {$d.NAMA_PENERIMA} {$d.GELAR_BELAKANG_PENERIMA}</td>
                <td>{$d.JUDUL_PESAN}</td>
                <td>{SplitWord($d.ISI_PESAN,5)}...</td>
                <td class="center">{$d.WAKTU}</td>
                <td class="center">
                    <a href="message-detail.php?p={$d.ID_PESAN}" class="ui-state-default ui-corner-all ui-icon ui-icon-zoomin" title="Lihat Pesan" style="cursor: pointer;display: inline-block;vertical-align: top"> </a>
                    <form method="post" action="outbox.php" style="display: inline-block">
                        <input type="hidden" name="id_pesan" value="{$d.ID_PESAN}"/>
                        <input type="hidden" name="mode" value="hapus"/>
                        <input type="submit" title="Hapus" class="ui-state-default ui-corner-all ui-icon ui-icon-trash" style="display:inline-block ;padding: 8px;cursor: pointer;"/>
                    </form>
                </td>
            </tr>
        {/foreach}
    </tbody>
</table>
{literal}
    <script>
        $(document).ready(function() {
            oTable = $('.datatable').dataTable({
                "bJQueryUI": true,
                "sPaginationType": "full_numbers"
            });
            $('form').validate();
            $(".datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-M-y'
            }).css({'text-transform': 'uppercase'});
            $("#kategori").autocomplete({
                source: "getKategoriKomplain.php",
                minLength: 3
            });
        });
    </script>
{/literal}