{literal}
    <style>
        table{
            border-collapse:collapse;
            font-family:Trebuchet MS;
            font-size: 12px;
            border: 1px solid #a6c9e2;
        }

        td{
            padding: 6px;
            border: 1px solid #cdcdcd;
        }
        * {
            font: 12px/1.5 'Trebuchet MS', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
        }
        /* --- Content Style --- */
        .content { font-size: 14px; }

        .content .center_title_bar
        {
            /** width: 90%; **/
            background: #1c6c3a;
            border-radius: 5px;
            color: #fff;
            font-size: 14px;
            font-weight: bold;
            margin: 0px -5px 20px -5px;
            padding: 10px;
            text-transform: uppercase;
        }
        .content h1 { font-size: 20px; }
        .content h2 { font-size: 18px; }
        .content h3 { font-size: 16px; }
        .content h4 { font-size: 14px; }


        .content table caption { 
            padding: 2px; 
            font-size: 14px;
            font-weight: bold;
        }

        th:not(.header-coloumn){
            padding :6px;
            color:#000;
            font-size:13px;
            background-color:#dfedf8;
            text-transform:uppercase;
        }
        h2,th.header-coloumn h3{
            font-size: 14px;
            font-weight:bold;
        }


        th.header-coloumn{
            color:#fff;
            padding: 20px;
            font-size: 15px;
            font-weight:bold;
            text-transform:uppercase;
        }
        tr.ui-widget-header{
            border: 1px solid #cdcdcd;
        }
        a {
            text-decoration: none;
            color: #007CE8;
        }
        .isi{
            color: #0e74d1;
        }
        td.kosong {
            text-align:center;
            color: red;
        }

        .content input[type=text],textarea{
            border-bottom: 1px solid #0c366c;
            border-top:none;
            border-right:none;
            border-left:none;
            padding:3px;
        }

        .content fieldset{
            padding:12px;
            border:1px solid #c0c0c0;
            -webkit-border-radius: 7px;
            -moz-border-radius: 7px;
            border-radius: 7px;
        }
        .content legend{
            color:#0c366c;
            font-size:14px;
            padding:2px;
            font-weight:bold;
            text-transform:uppercase;
        }
        .content div.field_form{
            margin-bottom: 15px;
            padding: 8px;
            display: block;	
        }
        .content div.bottom_field_form{
            clear: both;
            padding-top: 20px;
            text-align: center;
        }
        .content div.field_form  label:not(.error){
            float:left;
            width:15%;
            color:grey;
            font-weight:bold;
            text-align:right;
            margin:0px 15px 0 0;
        }
        .content input[type=text]:focus,textarea:focus{
            background: #ededed;
        }

        .content table tr td.title {
            background: #c3cbd4;
            padding: 8px;
        }

        .content a.button, input.button , button.button{
            margin:5px 1px;  
            padding:6px;
            background-color: #0e74d1;
            color: white;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            text-decoration:none;
            border: none;
            font-size:11px;
            display:inline-block;
        }
        .content a.button{
            padding:5px;
        }
        .content a.button:hover, input.button:hover ,button.button:hover{
            color: #cee3ff;
        }

        .small { font-size: 80%; }
        .center {
            text-align: center;
        }
        .middle {
            vertical-align: middle;
        }
        .cursor-pointer {
            cursor: pointer;
        }

        .content button {
            cursor: pointer;
        }
        .content a {
            color: #008000;
        }
        .content a:hover {
            color: #00aa00;
        }
        .content a:active {
            color: #00bb00;
        }
        .content pre {
            font-size: 11px;
        }
        .anchor-span {
            color: #008000;
            cursor: pointer;
            text-decoration: underline;
        }
        .anchor-span:hover {
            color: #00aa00;
        }

        .anchor-black {
            color: #000 !important;
            text-decoration: none;
        }
        .anchor-black:hover {
            color: #000 !important;
            text-decoration: underline;
        }

        /* FORM CONTENT */
        .content input[type=submit], .radio {
            cursor: pointer;
        }
        label.error {
            color: #c00;
            font-size: 12px;
            display:block;
        }
        input[type=text].error,textarea.error{
            border-bottom:1px solid red;
            background:#ffeaed;
        }

        .data-kosong{
            color:red;
            text-align:center;
        }
        .total{
            background-color:#c9f797;
            font-weight:bold;
        }
    </style>
{/literal}
<link rel="stylesheet" type="text/css" href="./../../css/custom-theme/jquery-ui-smoothness.css" />

<script type="text/javascript" src="./../../js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="./../../js/jquery-ui-1.8.11.custom.min.js"></script>
<script type="text/javascript" src="./../../js/jquery.validate.min.js"></script>
<script type="text/javascript" src="./../../js/additional-methods.min.js"></script>

<div class="content">
    {if $smarty.get.mode=='tambah'}
        <div class="center_title_bar">Jenis Pedoman Dokumen Mutu - Tambah</div>
        <form action="pedoman-prosedur-frame.php" method="post">
            <fieldset style="width: 98%">
                <legend>Tambah Pedoman Dokumen Mutu</legend>
                <div class="field_form">
                    <label>Lingkup Dokumen : </label>
                    {$lingkup.NAMA_GROUP_DOKUMEN} ({$lingkup.NAMA_PEDOMAN})
                    <input type="hidden" name="lingkup" value="{$lingkup.ID_MM_GROUP_DOKUMEN}"/>
                </div>
                <div class="field_form">
                    <label>Kode Dokumen : </label>
                    {$jumlah=$smarty.get.cd+1}
                    <input type="text" name="kode" style="text-transform: uppercase" size="30" maxlength="30" value="{$lingkup.KODE_GROUP_DOKUMEN}-{$jumlah|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}" class="required"/>
                </div>
                <div class="field_form">
                    <label>Nama Dokumen : </label>
                    <textarea name="isi" style="width: 60%" class="required"></textarea>
                </div>
                <div class="bottom_field_form">
                    <a href="pedoman-prosedur-frame.php" class="button">Batal</a>
                    <input type="hidden" name="mode" value="tambah"/>
                    <input type="submit" class="button" value="Simpan"/>
                </div>
            </fieldset>
        </form>
    {else if $smarty.get.mode=='edit'}
        <div class="center_title_bar"> Dokumen Mutu  - Edit</div>
        <form action="pedoman-prosedur-frame.php" method="post">
            <fieldset style="width: 80%">
                <legend>Edit Dokumen Mutu </legend>
                <div class="field_form">
                    <label>Lingkup Dokumen : </label>
                    {$dokumen.NAMA_GROUP_DOKUMEN} ({$lingkup.NAMA_PEDOMAN})
                    <input type="hidden" name="lingkup" value="{$dokumen.ID_MM_GROUP_DOKUMEN}"/>
                </div>
                <div class="field_form">
                    <label>Kode Dokumen : </label>
                    <input type="text" name="kode" style="text-transform: uppercase" value="{$dokumen.KODE_DOKUMEN}" size="20" maxlength="20" class="required"/>
                </div>
                <div class="field_form">
                    <label>Nama Dokumen : </label>
                    <textarea name="isi" style="width: 60%" class="required">{$dokumen.NAMA_DOKUMEN}</textarea>
                </div>
                <div class="bottom_field_form">
                    <a href="pedoman-prosedur-frame.php" class="button">Batal</a>
                    <input type="hidden" name="id" value="{$dokumen.ID_MM_DOKUMEN}" />
                    <input type="hidden" name="mode" value="edit"/>
                    <input type="submit" class="button" value="Update"/>
                </div>
            </fieldset>
        </form>
    {else if $smarty.get.mode=='upload'}
        <div class="center_title_bar">Dokumen Mutu  - File Dokumen</div>
        <form action="pedoman-prosedur-frame.php" method="post" id="add"  enctype="multipart/form-data">
            <fieldset style="width: 96%">
                <legend>Tambah Baru/Perubahan Usulan File Dokumen Mutu </legend>
                <div class="field_form">
                    <label>Lingkup Dokumen : </label>
                    {$dokumen.NAMA_GROUP_DOKUMEN} ({$lingkup.NAMA_PEDOMAN})
                </div>
                <div class="field_form">
                    <label>Kode Dokumen : </label>
                    {$dokumen.KODE_DOKUMEN|upper}
                </div>
                <div class="field_form">
                    <label>Nama Dokumen : </label>
                    {$dokumen.NAMA_DOKUMEN}
                </div>                
                <div class="field_form">
                    <label>File Dokumen : </label>
                    <input class="required" type="file" name="file"/>
                </div>
                <div class="field_form">
                    <span style="font-style: italic;color: green">Kosongi Isian Di Bawah ini jika bukan merupakan usulan perubahan </span>
                </div>
                <div class="field_form">
                    <label>Uraian Perubahan : </label>
                    <textarea name="uraian" style="width: 60%"></textarea>
                </div>
                <div class="field_form">
                    <label>Sebelum Perubahan : </label>
                    <textarea name="sebelum" style="width: 60%"></textarea>
                </div>
                <div class="field_form">
                    <label>Sesudah Perubahan : </label>
                    <textarea name="sesudah" style="width: 60%"></textarea>
                </div>
                <div class="bottom_field_form">
                    <input type="hidden" name="id_dokumen" value="{$dokumen.ID_MM_DOKUMEN}"/>
                    <input type="hidden" name="nama_file" value="{$dokumen.KODE_DOKUMEN|upper}_{$count_file+1}"/>
                    <a href="pedoman-prosedur-frame.php" class="button">Batal</a>
                    <input type="hidden" name="mode" value="upload"/>
                    <input type="submit" class="button" value="Upload"/>
                </div>
            </fieldset>
        </form>
    {else if $smarty.get.mode=='file'}
        <div class="center_title_bar">Perubahan Dokumen Mutu  </div>
        <a href="pedoman-prosedur-frame.php" class="button">Kembali</a>
        <table style="width: 98%">
            <caption>NAMA DOKUMEN : {$dokumen.NAMA_DOKUMEN} ({$dokumen.KODE_DOKUMEN|upper})</caption>
            <tr>
                <th>NO</th>
                <th>WAKTU UPDATE</th>
                <th>UPLOAD OLEH</th>
                <th>URAIAN PERUBAHAN</th>
                <th>SEBELUM</th>
                <th>SESUDAH</th>
                <th>TAMPIL</th>
                <th>STATUS</th>
                <th>OPERASI</th>
            </tr>
            {foreach $data_file as $f}
                <tr>
                    <td class="center">{$f@index+1}</td>
                    <td>{$f.WAKTU}</td>
                    <td>{$f.NM_PENGGUNA}</td>
                    <td>{$f.URAIAN_PERUBAHAN}</td>
                    <td>{$f.SEBELUM}</td>
                    <td>{$f.SESUDAH}</td>
                    <td>{if $f.TAMPIL==1}<span style="color: green">Ya</span>{else}<span style="color: red">Tidak</span>{/if}</td>
                    <td>
                        {if $f.STATUS_PAKAI==1}
                            <span style="color: green">Sudah Di verifikasi</span><br/>(<span style="font-size: 0.8em">{$f.VERIFIKATOR}</span>)
                        {else}
                            <span style="color: red">Belum Div erifikasi</span>
                        {/if}
                    </td>
                    <td class="center" style="width: 80px">
                        {$convert_file=$f.NAMA_FILE}
                        <a class="button" target="_blank" href="{$base_url}modul/manajemen-mutu/dokumen-view.php?key={base64_encode('nambisembilu_cybercampus')}">View</a>
                        {if $f.STATUS_PAKAI==0}
                            <form action="pedoman-prosedur-frame.php" method="post" style="margin: 4px">
                                <input type="hidden" name="id_file" value="{$f.ID_MM_FILE_DOKUMEN}"/>
                                <input type="hidden" name="mode" value="hapus_file"/>
                                <input type="submit" value="Delete" class="button" />
                            </form>
                        {/if}
                    </td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="9" class="data-kosong">Data Masih Kosong</td>
                </tr>
            {/foreach}
        </table>
    {else}
        <div class="center_title_bar">Daftar Dokumen Mutu</div>
        {if $tutup!=''}
            {$tutup}
        {else}
            {*TAMPILAN UNTUK DIREKTUR/ KEPALA LEMBAGA*}
            {if $lingkup_jabatan!=''}
                <table style="width: 100%;margin-bottom: 10px">
                    <tr class="ui-widget-header">
                        <th colspan="4" class="header-coloumn">KHUSUS UNTUK DIREKTUR/KEPALA LEMBAGA</th>
                    </tr>
                    {$index_pedoman=1}
                    {foreach $data_dokumen as $d}
                        {if $d.ID_MM_JENIS_PEDOMAN==2}
                            {foreach $d.LINGKUP as $l}
                                {if ($l.ID_MM_GROUP_DOKUMEN==$lingkup_jabatan and $lingkup_jabatan!='')}
                                    <tr style="background-color: antiquewhite">
                                        <td colspan="4"><b style="font-size: 1.3em">{$l.NAMA_GROUP_DOKUMEN} ({$l.KODE_GROUP_DOKUMEN|upper})</b> 
                                            {if $l.ID_MM_GROUP_DOKUMEN==$lingkup_jabatan}
                                                <a href="pedoman-prosedur-frame.php?mode=tambah&l={$l.ID_MM_GROUP_DOKUMEN}&cd={count($l.DOKUMEN)}" class="button">Tambah</a>
                                            {/if}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>NO</th>
                                        <th>DOKUMEN</th>
                                        <th  style="width: 140px">KODE</th>
                                        <th>FILE</th>
                                    </tr>
                                    {foreach $l.DOKUMEN as $d}
                                        {$convert_file=$d.NAMA_FILE}
                                        {if $d.HIDDEN!=1}
                                            <tr>
                                                <td  style="width: 20px">{$d@index+1}</td>
                                                <td>{$d.NAMA_DOKUMEN}</td>
                                                <td>{$d.KODE_DOKUMEN|upper}</td>
                                                <td class="center">
                                                    {if $d.HIDDEN==1}
                                                        <span style="color: white">Telah Di Hapus</span>
                                                    {else}
                                                        {if $d.NAMA_FILE!=''}
                                                            <a target="_blank" href="{$base_url}modul/manajemen-mutu/dokumen-view.php?key={base64_encode('nambisembilu_cybercampus')}&file={base64_encode($convert_file)}">{$d.KODE_DOKUMEN|upper}</a>
                                                        {elseif $d.JUMLAH_VERIFIKASI>0}
                                                            <span style="color: red">Belum Di Publish</span>
                                                        {elseif $d.JUMLAH_FILE==0}
                                                            <span style="color: red">File Kosong</span>
                                                        {elseif $d.JUMLAH_FILE>0}
                                                            <span style="color: red">Belum Di verifikasi</span>
                                                        {/if}
                                                        <br/>
                                                        {if $l.ID_MM_GROUP_DOKUMEN==$lingkup_jabatan}
                                                            <a href="pedoman-prosedur-frame.php?mode=upload&l={$l.ID_MM_GROUP_DOKUMEN}&d={$d.ID_MM_DOKUMEN}" class="button">Usulan</a>
                                                            <a href="pedoman-prosedur-frame.php?mode=file&l={$l.ID_MM_GROUP_DOKUMEN}&d={$d.ID_MM_DOKUMEN}" class="button">Riwayat</a>
                                                            <a href="pedoman-prosedur-frame.php?mode=edit&l={$l.ID_MM_GROUP_DOKUMEN}&d={$d.ID_MM_DOKUMEN}" class="button">Edit</a>
                                                        {/if}
                                                    {/if}
                                                </td>
                                            </tr>

                                        {/if}
                                    {foreachelse}
                                        <tr>
                                            <td colspan="4" class="kosong">Data Masih Kosong</td>
                                        </tr>

                                    {/foreach}
                                {/if}
                            {/foreach}
                            {$index_pedoman=$index_pedoman+1}
                        {/if}
                    {/foreach}   
                </table>
            {/if}
            {*TAMPILAN UNTUK DOSEN*}
            {$keterangan}
            <div id="tabs" style="width: 99%;min-height: 720px;margin: 10px 0px">
                <ul>
                    {foreach $data_lingkup as $dl}
                        {if $dl.ID_MM_JENIS_PEDOMAN==2}
                            {if $dl.HIDDEN!=1}
                                <li>
                                    <a class="disable-ajax" href="#get-tab-{$dl.ID_MM_GROUP_DOKUMEN}">{$dl.SINGKATAN_LINGKUP|upper}</a>
                                </li>
                            {/if}
                        {/if}
                    {/foreach}
                </ul>
                {foreach $data_dokumen as $d}
                    {if $d.ID_MM_JENIS_PEDOMAN==2}
                        {foreach $d.LINGKUP as $l}
                            {if $l.HIDDEN!=1}
                                <div id="get-tab-{$l.ID_MM_GROUP_DOKUMEN}">
                                    <table style="width: 98%">
                                        <tr style="background-color: antiquewhite">
                                            <td colspan="4"><b style="font-size: 1.3em">{$l.NAMA_GROUP_DOKUMEN} ({$l.KODE_GROUP_DOKUMEN|upper})</b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>NO</th>
                                            <th>DOKUMEN</th>
                                            <th  style="width: 140px">KODE</th>
                                            <th>FILE</th>
                                        </tr>
                                        {foreach $l.DOKUMEN as $d}
                                            {$convert_file=$d.NAMA_FILE}
                                            {if $d.HIDDEN!=1}
                                                <tr>
                                                    <td  style="width: 20px">{$d@index+1}</td>
                                                    <td>{$d.NAMA_DOKUMEN}</td>
                                                    <td>{$d.KODE_DOKUMEN|upper}</td>
                                                    <td class="center">
                                                        {if $d.HIDDEN==1}
                                                            <span style="color: white">Telah Di Hapus</span>
                                                        {else}
                                                            {if $d.NAMA_FILE!=''}
                                                                <a target="_blank" href="{$base_url}modul/manajemen-mutu/dokumen-view.php?key={base64_encode('nambisembilu_cybercampus')}&file={base64_encode($convert_file)}&data={$id_pengguna}&dok={$d.ID_MM_DOKUMEN}">{$d.KODE_DOKUMEN|upper}</a>
                                                            {elseif $d.JUMLAH_VERIFIKASI>0}
                                                                <span style="color: red">Belum Di Publish</span>
                                                            {elseif $d.JUMLAH_FILE==0}
                                                                <span style="color: red">File Kosong</span>
                                                            {elseif $d.JUMLAH_FILE>0}
                                                                <span style="color: red">Belum Di verifikasi</span>
                                                            {/if}
                                                        {/if}
                                                    </td>
                                                </tr>
                                            {/if}
                                        {foreachelse}
                                            <tr>
                                                <td colspan="4" class="kosong">Data Masih Kosong</td>
                                            </tr>

                                        {/foreach}
                                    </table>
                                </div>
                            {/if}
                        {/foreach}
                    {/if}
                {/foreach}
            </div>  
        {/if}
    {/if}
</div>
{literal}
    <script>
        $("#tabs").tabs();
        $('form').validate();
    </script>
{/literal}
