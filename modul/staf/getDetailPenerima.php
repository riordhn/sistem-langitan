<?php

require 'config.php';

$jenis = $_REQUEST['jenis'];

if ($jenis == 1) {
    $query = "
        SELECT P.*
        FROM AUCC.PENGGUNA P
        JOIN AUCC.DOSEN D ON D.ID_PENGGUNA=P.ID_PENGGUNA
        WHERE D.FLAG_VALID=1
        ORDER BY P.NM_PENGGUNA
        ";
} else {
    $query = "
        SELECT P.*
        FROM AUCC.PENGGUNA P
        JOIN AUCC.PEGAWAI PEG ON PEG.ID_PENGGUNA=P.ID_PENGGUNA
        WHERE PEG.FLAG_VALID=1
        ORDER BY P.NM_PENGGUNA
        ";
}
$db->Query($query);
$data = $db->FetchAssoc();
echo json_encode($data);

