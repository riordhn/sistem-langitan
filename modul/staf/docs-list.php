<?php
require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_STAF){
	header("location: /logout.php");
    exit();
}

	function encrypt($string, $key) {
	  $result = '';
	  for($i=0; $i<strlen($string); $i++) {
		$char = substr($string, $i, 1);
		$keychar = substr($key, ($i % strlen($key))-1, 1);
		$char = chr(ord($char)+ord($keychar));
		$result.=$char;
	  }
	  return base64_encode($result);
	}
	function decrypt($string, $key) {
	  $result = '';
	  $string = base64_decode($string);
	  for($i=0; $i<strlen($string); $i++) {
		$char = substr($string, $i, 1);
		$keychar = substr($key, ($i % strlen($key))-1, 1);
		$char = chr(ord($char)-ord($keychar));
		$result.=$char;
	  }
	  return $result;
	}
/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
require_once '../../oauth/src/apiClient.php';

//session_start();

$client = new apiClient();
$client->setApplicationName('Google Contacts PHP Sample');
//$client->setScopes("https://docs.google.com/feeds/default/private/full?v=3&showfolders=true");
$client->setScopes("https://docs.google.com/feeds/default/private/full");
// Documentation: http://code.google.com/apis/gdata/docs/2.0/basics.html
// Visit https://code.google.com/apis/console?api=contacts to generate your
// oauth2_client_id, oauth2_client_secret, and register your oauth2_redirect_uri.
$client->setClientId('197652970615.apps.googleusercontent.com');
$client->setClientSecret('1QZEKvO7htvuzmW1DGAGIr07');
$client->setRedirectUri($base_url.'oauth');
$client->setDeveloperKey('AIzaSyBsGcZmQAAPrVGP5bo_MrKhoAQjzP_6xeU');

if (isset($_GET['code'])) {
  $client->authenticate();
  $_SESSION['token'] = $client->getAccessToken();
  //$token = $_GET['code'];
  $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
  header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
}

if (isset($_SESSION['token'])) {
 $client->setAccessToken($_SESSION['token']);
}

if (isset($_REQUEST['logout'])) {
  unset($_SESSION['token']);
  $client->revokeToken();
}

?>

<link rel="next" type="application/atom+xml" href="https://docs.google.com/feeds/default/private/full?start-key=EAEaFgoSCb2YGEPMAAACAG"/>


<table width="100%" style="width:900px;" class="ui-widget">
<tr class="ui-widget-header">
	<td>TITLE</td><td>AUTHOR</td>
</tr>
<?php
if ($client->getAccessToken()) {
  //$req = new apiHttpRequest("https://docs.google.com/feeds/default/private/full?v=3&showfolders=true");
  $req = new apiHttpRequest("https://docs.google.com/feeds/default/private/full/-/spreadsheet?v=3");
  $val = $client->getIo()->authenticatedRequest($req);

  // The contacts api only returns XML responses.
  //$response = json_encode(simplexml_load_string($val->getResponseBody()));
  $response = simplexml_load_string($val->getResponseBody());
  //echo $response;
  print "<pre>" . print_r(json_decode($response, true), true) . "</pre>";
  //echo $_SESSION['token'];
	foreach($response->entry as $file) {
		$link = $file->link["href"];
		$author = $file->author->name;
		$encrypted = encrypt($link, "dh3m1tkh4y4ng4nc3bl0k");
		//$link = urlencode($encrypted);
		print "<tr class='ui-widget-content'><td class='ui-widget-content'>";
		print "<a style='font-size:16px; text-decoration:none; color:#000;' class=disable-ajax href='#documents-spreadsheet!docs-spreadsheet.php?id=$link' ><img src='".$base_url."'img/mhs/excel.png' style='width:16px; height:16px;' /> &nbsp; " . $file->title . "</a><br />";
		print "</td><td class='ui-widget-content'>$author</td></tr>";
		//echo "Author: " . $file->author->name . "<br /><br />";
	}
	
//presentation
  $req1 = new apiHttpRequest("https://docs.google.com/feeds/default/private/full/-/presentation?v=3");
  $val1 = $client->getIo()->authenticatedRequest($req1);

  // The contacts api only returns XML responses.
  //$response2 = json_encode(simplexml_load_string($val2->getResponseBody()));
  $response1 = simplexml_load_string($val1->getResponseBody());
  //echo $response;
  //print "<pre>" . print_r(json_decode($response2, true), true) . "</pre>";
	
	foreach($response1->entry as $file) {
		$link = $file->link[0]["href"];
		$author = $file->author->name;
		$mystring = $link;
		$findme   = 'edit';
		$pos = strpos($mystring, $findme);
		if ($pos === false) {
			$link = $file->link[1]["href"];
		}else{
			$link = $file->link[0]["href"];
		}
		$encrypted = encrypt($link, "dh3m1tkh4y4ng4nc3bl0k");
		//$link = urlencode($encrypted);
		
		print "<tr class='ui-widget-content'><td class='ui-widget-content'>";
		
		print "<a id='link' style='font-size:16px; text-decoration:none; color:#000;' target=_self class=disable-ajax href='#documents-presentation!docs-presentation.php?id=$link'> <img src='".$base_url."'img/mhs/powerpoint.png' style='width:16px; height:16px;' /> &nbsp;" . $file->title . "</a>";

		print "</td><td class='ui-widget-content'>$author</td></tr>";
		//echo "Type: " . $file->content["type"] . "<br />";
		//echo "Author: " . $file->author->name . "<br /><br />";
	}
//
	
  $req2 = new apiHttpRequest("https://docs.google.com/feeds/default/private/full/-/document?v=3");
  $val2 = $client->getIo()->authenticatedRequest($req2);

  // The contacts api only returns XML responses.
  //$response2 = json_encode(simplexml_load_string($val2->getResponseBody()));
  $response2 = simplexml_load_string($val2->getResponseBody());
  //echo $response;
  //print "<pre>" . print_r(json_decode($response2, true), true) . "</pre>";
  
	foreach($response2->entry as $file) {
		$link = $file->link[0]["href"];
		$author = $file->author->name;
		$mystring = $link;
		$findme   = 'edit';
		$pos = strpos($mystring, $findme);
		if ($pos === false) {
			$link = $file->link[1]["href"];
		}else{
			$link = $file->link[0]["href"];
		}
		$encrypted = encrypt($link, "dh3m1tkh4y4ng4nc3bl0k");
		//$link = urlencode($encrypted);
		
		print "<tr class='ui-widget-content'><td class='ui-widget-content'>";
		
		print "<a style='font-size:16px; text-decoration:none; color:#000;' target=_self class=disable-ajax href='#documents-document!docs-document.php?id=$link'> <img src='".$base_url."'img/mhs/ms_word_2.png' style='width:16px; height:16px;' /> &nbsp;" . $file->title . "</a>";
		
		print "</td><td class='ui-widget-content'>$author</td></tr>";
		//echo "Type: " . $file->content["type"] . "<br />";
		//echo "Author: " . $file->author->name . "<br /><br />";
	}
	
//Drawings
  $req3 = new apiHttpRequest("https://docs.google.com/feeds/default/private/full/-/drawing?v=3");
  $val3 = $client->getIo()->authenticatedRequest($req3);

  // The contacts api only returns XML responses.
  //$response2 = json_encode(simplexml_load_string($val2->getResponseBody()));
  $response3 = simplexml_load_string($val3->getResponseBody());
  //echo $response;
  //print "<pre>" . print_r(json_decode($response2, true), true) . "</pre>";
  
	foreach($response3->entry as $file) {
		$link = $file->link[0]["href"];
		$author = $file->author->name;
		$mystring = $link;
		$findme   = 'edit';
		$pos = strpos($mystring, $findme);
		if ($pos === false) {
			$link = $file->link[1]["href"];
		}else{
			$link = $file->link[0]["href"];
		}
		$encrypted = encrypt($link, "dh3m1tkh4y4ng4nc3bl0k");
		$link = urlencode($encrypted);
		
		print "<tr class='ui-widget-content'><td class='ui-widget-content'>";
		
		print "<a style='font-size:16px; text-decoration:none; color:#000;' target=_self class=disable-ajax href='#documents-document!docs-document.php?id=$link'> <img src='".$base_url."'img/mhs/visio.png' style='width:16px; height:16px;' /> &nbsp;" . $file->title . "</a>";
		
		print "</td><td class='ui-widget-content'>$author</td></tr>";
		//echo "Type: " . $file->content["type"] . "<br />";
		//echo "Author: " . $file->author->name . "<br /><br />";
	}
//

//File
  $req4 = new apiHttpRequest("https://docs.google.com/feeds/default/private/full?q=.jpg&v=3");
  $val4 = $client->getIo()->authenticatedRequest($req4);

  // The contacts api only returns XML responses.
  //$response2 = json_encode(simplexml_load_string($val2->getResponseBody()));
  $response4 = simplexml_load_string($val4->getResponseBody());
  //echo $response;
  //print "<pre>" . print_r(json_decode($response2, true), true) . "</pre>";
  
	foreach($response4->entry as $file) {
		$link = $file->link[0]["href"];
		$author = $file->author->name;
		$mystring = $link;
		$findme   = 'edit';
		$pos = strpos($mystring, $findme);
		if ($pos === false) {
			$link = $file->link[1]["href"];
		}else{
			$link = $file->link[0]["href"];
		}
		//$encrypted = encrypt($link, "dh3m1tkh4y4ng4nc3bl0k");
		//$link = urlencode($encrypted);
		
		print "<tr class='ui-widget-content'><td class='ui-widget-content'>";
		
		print "<a style='font-size:16px; text-decoration:none; color:#000;' target=_self class=disable-ajax href='#documents-document!docs-document.php?id=$link'> <img src='".$base_url."'img/mhs/default_document.png' style='width:16px; height:16px;' /> &nbsp;" . $file->title . "</a>";
		
		print "</td><td class='ui-widget-content'>$author</td></tr>";
		//echo "Type: " . $file->content["type"] . "<br />";
		//echo "Author: " . $file->author->name . "<br /><br />";
	}
//
	

  
  
  // The access token may have been updated lazily.
  $_SESSION['token'] = $client->getAccessToken();
  //$_SESSION['token'] = '1\/KRFfzfPpja9yNDoyxCpDcvP77Py7Bj9GvQnYg92sLps';
} else {
  $auth = $client->createAuthUrl();
}
?>
	
</table>

<?php
if (isset($auth)) {
    print "<p><a target=_self class=disable-ajax href='$auth' >Connect to google!</a></p>";
  } else {
    print "<p><a class=disable-ajax href='".$base_url."'oauth?logout'>Logout</a></p>";
}

?>