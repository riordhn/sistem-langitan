<?php

include 'config.php';

if (!empty($_GET)) {
    if (get('mode') == 'detail') {
        $id_komplain = get('k');
        $cek_baca = $db->QuerySingle("SELECT COUNT(*) FROM KOMPLAIN_BACA WHERE ID_KOMPLAIN='{$id_komplain}' AND ID_PEMBACA='" . $user->ID_PENGGUNA . "'");
        if ($cek_baca == 0) {
            $db->Query("INSERT INTO KOMPLAIN_BACA (ID_KOMPLAIN,ID_PEMBACA,WAKTU_BACA) VALUES ('{$id_komplain}','" . $user->ID_PENGGUNA . "',SYSDATE)");
        }
        $q_data_komplain = "
            SELECT P.NM_PENGGUNA,
            (CASE
                WHEN P.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.DOSEN)
                THEN 'Dosen'
                WHEN P.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.PEGAWAI)
                THEN 'Tenaga Kependidikan'
                ELSE 'Mahasiswa'
            END  
            ) STATUS
            ,KK.NM_KATEGORI,UK.NM_UNIT_KERJA,UK.DESKRIPSI_UNIT_KERJA,K.*,TO_CHAR(K.WAKTU_KOMPLAIN,'DD-MM-YYYY HH24:MI:SS') WAKTU
            FROM AUCC.KOMPLAIN K
            LEFT JOIN AUCC.KOMPLAIN_KATEGORI KK ON K.ID_KOMPLAIN_KATEGORI=KK.ID_KOMPLAIN_KATEGORI
            JOIN AUCC.UNIT_KERJA UK ON UK.ID_UNIT_KERJA=K.ID_UNIT_TERTUJU
            JOIN AUCC.PENGGUNA P ON K.ID_PELAPOR=P.ID_PENGGUNA
            WHERE K.ID_KOMPLAIN='{$id_komplain}'
            ";
        $db->Query($q_data_komplain);
        $data_komplain = $db->FetchAssoc();
        $q_data_tanggapan = "
            SELECT
            KT.*,P.NM_PENGGUNA PENJAWAB,
            (CASE
                WHEN P.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.DOSEN)
                THEN 'Dosen'
                WHEN P.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.PEGAWAI)
                THEN 'Tenaga Kependidikan'
            END  
            ) STATUS_PENJAWAB,TO_CHAR(KT.WAKTU_JAWAB,'DD-MM-YYYY HH24:MI:SS') WAKTU_JAWAB
            FROM AUCC.KOMPLAIN K
            JOIN AUCC.KOMPLAIN_TANGGAPAN KT ON KT.ID_KOMPLAIN=K.ID_KOMPLAIN
            JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=KT.ID_PENJAWAB
            WHERE K.ID_KOMPLAIN='{$id_komplain}'
            ";
        $data_tanggapan = $db->QueryToArray($q_data_tanggapan);
        $unit_kerja_all = $db->QueryToArray("SELECT * FROM UNIT_KERJA WHERE TYPE_UNIT_KERJA NOT IN ('PRODI') AND ID_UNIT_KERJA NOT IN ({$data_komplain['ID_UNIT_TERTUJU']}) ORDER BY NM_UNIT_KERJA");
        $smarty->assign('data_unit_kerja', $unit_kerja_all);
        $smarty->assign('data_tanggapan', $data_tanggapan);
        $smarty->assign('komplain', $data_komplain);
    }
}

if (!empty($_POST)) {
    if (post('mode') == 'jawab') {
        $id_penjawab = $user->ID_PENGGUNA;
        $id_komplain = post('id_komplain');
        $analisa = post('j_analisa');
        $tindakan = post('j_tindakan');
        $keterangan = post('j_keterangan');
        $tgl_realisasi = post('j_tgl_realisasi');
        $cek_kategori = $db->QuerySingle("SELECT COUNT(*) FROM KOMPLAIN_KATEGORI WHERE NM_KATEGORI='{$_POST['j_kategori']}' AND ID_UNIT_KERJA='{$_POST['unit_kerja']}'");
        if ($cek_kategori == 0) {
            $q_i_kategori = "
            INSERT INTO KOMPLAIN_KATEGORI
                (NM_KATEGORI,ID_UNIT_KERJA)
            VALUES 
                ('{$_POST['j_kategori']}','{$_POST['unit_kerja']}')
            ";
            $db->Query($q_i_kategori);
        }
        $id_kategori = $db->QuerySingle("SELECT ID_KOMPLAIN_KATEGORI FROM KOMPLAIN_KATEGORI WHERE NM_KATEGORI='{$_POST['j_kategori']}' AND ID_UNIT_KERJA='{$_POST['unit_kerja']}'");
        $query_update_kategori = "UPDATE KOMPLAIN SET ID_KOMPLAIN_KATEGORI='{$id_kategori}' WHERE ID_KOMPLAIN='{$id_komplain}'";
        $db->Query($query_update_kategori);
        $q_i_jawaban = "
            INSERT INTO KOMPLAIN_TANGGAPAN
                (ID_KOMPLAIN,ID_PENJAWAB,WAKTU_JAWAB,ANALISA_PENYEBAB,TINDAKAN,KETERANGAN,TGL_SELESAI_REALISASI)
            VALUES 
                ('{$id_komplain}','{$id_penjawab}',CURRENT_TIMESTAMP,'{$analisa}','{$tindakan}','{$keterangan}','{$tgl_realisasi}')
            ";
        $db->Query($q_i_jawaban);
        $q_u_komplain = "
            UPDATE KOMPLAIN SET STATUS_KOMPLAIN='2' WHERE ID_KOMPLAIN='{$id_komplain}'
            ";
        $db->Query($q_u_komplain);
    } else if (post('mode') == 'tindakan') {
        $id_penjawab = $user->ID_PENGGUNA;
        $id_komplain = post('id_komplain');
        $analisa = post('t_analisa');
        $tindakan = post('t_tindakan');
        $keterangan = post('t_keterangan');
        $tgl_realisasi = post('t_tgl_realisasi');
        $cek_kategori = $db->QuerySingle("SELECT COUNT(*) FROM KOMPLAIN_KATEGORI WHERE NM_KATEGORI='{$_POST['t_kategori']}' AND ID_UNIT_KERJA='{$_POST['unit_kerja']}'");
        if ($cek_kategori == 0) {
            $q_i_kategori = "
            INSERT INTO KOMPLAIN_KATEGORI
                (NM_KATEGORI,ID_UNIT_KERJA)
            VALUES 
                ('{$_POST['t_kategori']}','{$_POST['unit_kerja']}')
            ";
            $db->Query($q_i_kategori);
        }
        $id_kategori = $db->QuerySingle("SELECT ID_KOMPLAIN_KATEGORI FROM KOMPLAIN_KATEGORI WHERE NM_KATEGORI='{$_POST['t_kategori']}' AND ID_UNIT_KERJA='{$_POST['unit_kerja']}'");
        $query_update_kategori = "UPDATE KOMPLAIN SET ID_KOMPLAIN_KATEGORI='{$id_kategori}' WHERE ID_KOMPLAIN='{$id_komplain}'";
        $db->Query($query_update_kategori);
        for ($i = 1; $i <= post('jumlah_unit'); $i++) {
            $unit_kerja_share = post('unit_kerja' . $i);
            if ($unit_kerja_share != '') {
                $q_i_share = "INSERT INTO KOMPLAIN_SHARE (ID_KOMPLAIN,ID_UNIT_KERJA) VALUES ('{$id_komplain}','{$unit_kerja_share}')";
                $db->Query($q_i_share);
            }
        }
        $q_i_jawaban = "
            INSERT INTO KOMPLAIN_TANGGAPAN
                (ID_KOMPLAIN,ID_PENJAWAB,WAKTU_JAWAB,ANALISA_PENYEBAB,TINDAKAN,KETERANGAN,TGL_SELESAI_REALISASI)
            VALUES 
                ('{$id_komplain}','{$id_penjawab}',CURRENT_TIMESTAMP,'{$analisa}','{$tindakan}','{$keterangan}','{$tgl_realisasi}')
            ";
        $db->Query($q_i_jawaban);
        $q_u_komplain = "
            UPDATE KOMPLAIN SET STATUS_KOMPLAIN='3' WHERE ID_KOMPLAIN='{$id_komplain}'
            ";
        $db->Query($q_u_komplain);
    }
}

$unit_kerja_pengguna = $db->QuerySingle("SELECT ID_UNIT_KERJA FROM PEGAWAI WHERE ID_PENGGUNA='{$user->ID_PENGGUNA}'");


// CHECK PENANGGUNG JAWAB KOMPLAIN
$q_cek_pj_komplain = "
    SELECT COUNT(*) FROM KOMPLAIN_PJ WHERE ID_PENGGUNA=" . $user->ID_PENGGUNA . "AND ID_UNIT_KERJA='{$unit_kerja_pengguna}'";
$cek_pj_komplain = $db->QuerySingle($q_cek_pj_komplain);
if ($cek_pj_komplain > 0) {
    $q_data_komplain = "
    SELECT P.NM_PENGGUNA,
    (CASE
        WHEN P.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.DOSEN)
        THEN 'Dosen'
        WHEN P.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.PEGAWAI)
        THEN 'Tenaga Kependidikan'
        ELSE 'Mahasiswa'
    END  
    ) STATUS,
    (SELECT COUNT(*) FROM KOMPLAIN_BACA WHERE ID_PEMBACA='" . $user->ID_PENGGUNA . "' AND ID_KOMPLAIN=K.ID_KOMPLAIN) STATUS_BACA
    ,KK.NM_KATEGORI,UK.NM_UNIT_KERJA,UK.DESKRIPSI_UNIT_KERJA,K.*,TO_CHAR(K.WAKTU_KOMPLAIN,'DD-MM-YYYY HH24:MI:SS') WAKTU
    FROM AUCC.KOMPLAIN K
    LEFT JOIN AUCC.KOMPLAIN_KATEGORI KK ON K.ID_KOMPLAIN_KATEGORI=KK.ID_KOMPLAIN_KATEGORI
    JOIN AUCC.UNIT_KERJA UK ON UK.ID_UNIT_KERJA=K.ID_UNIT_TERTUJU
    JOIN AUCC.PENGGUNA P ON K.ID_PELAPOR=P.ID_PENGGUNA
    WHERE ({$unit_kerja_pengguna} IN (SELECT ID_UNIT_KERJA FROM KOMPLAIN_SHARE WHERE ID_KOMPLAIN=K.ID_KOMPLAIN) OR K.ID_UNIT_TERTUJU={$unit_kerja_pengguna})
    ORDER BY K.WAKTU_KOMPLAIN DESC
    ";
    $data_komplain = $db->QueryToArray($q_data_komplain);
    $smarty->assign('data_komplain', $data_komplain);
} else {
    $smarty->assign('error',  alert_error("Mohon maaf hanya penanggung jawab komplain tiap unit kerja yang bisa membuka dan menjawab komplain", 98));
}

$smarty->assign('info_komplain', alert_success("Anda hanya bisa melakukan komplain mengenai prosedur yang ada pada setiap Unit Kerja setelah membaca Dokumen Prosedur yang sudah ada. Terima Kasih", 98));

$smarty->display('komplain/komplain-unit-kerja.tpl');
?>
