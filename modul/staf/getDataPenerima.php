<?php

require 'config.php';

$jenis = $_REQUEST['jenis'];

if ($jenis == 1) {
    $query = "
        SELECT P.*,PS.NM_PROGRAM_STUDI PRODI,J.NM_JENJANG JENJANG
        FROM AUCC.PENGGUNA P
        JOIN AUCC.DOSEN D ON D.ID_PENGGUNA=P.ID_PENGGUNA
        JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=D.ID_PROGRAM_STUDI
        JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
        ORDER BY P.NM_PENGGUNA
        ";
    $data = $db->QueryToArray($query);
    foreach ($data as $d) {
        echo "<option value='{$d['ID_PENGGUNA']}'>{$d['GELAR_DEPAN']} {$d['NM_PENGGUNA']} {$d['GELAR_BELAKANG']} ({$d['JENJANG']} {$d['PRODI']})</option>";
    }
} else if ($jenis == 2) {
    $query = "
        SELECT P.*,UPPER(UK.NM_UNIT_KERJA) UNIT
        FROM AUCC.PENGGUNA P
        JOIN AUCC.PEGAWAI PEG ON PEG.ID_PENGGUNA=P.ID_PENGGUNA
        JOIN AUCC.UNIT_KERJA UK ON UK.ID_UNIT_KERJA=PEG.ID_UNIT_KERJA
        ORDER BY P.NM_PENGGUNA
        ";
    $data = $db->QueryToArray($query);
    foreach ($data as $d) {
        echo "<option value='{$d['ID_PENGGUNA']}'>{$d['GELAR_DEPAN']} {$d['NM_PENGGUNA']} {$d['GELAR_BELAKANG']} ({$d['UNIT']})</option>";
    }
}


