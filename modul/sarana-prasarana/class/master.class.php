<?php

class master {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }
	
	function get_hasil_cari_mahasiswa($var) {
		$var_trim = trim($var);
        return $this->db->QueryToArray("
            SELECT P.NM_PENGGUNA,M.NIM_MHS,NO_UJIAN,F.NM_FAKULTAS,PS.NM_PROGRAM_STUDI FROM PENGGUNA P
            JOIN MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA
			LEFT JOIN CALON_MAHASISWA_BARU CMB ON M.ID_C_MHS = CMB.ID_C_MHS
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            JOIN FAKULTAS F ON PS.ID_FAKULTAS = F.ID_FAKULTAS
            WHERE M.NIM_MHS LIKE '%$var_trim%' OR UPPER(P.NM_PENGGUNA) LIKE UPPER('%$var_trim%') OR CMB.NO_UJIAN LIKE '%$var_trim%'
            ");
    }
}
?>
