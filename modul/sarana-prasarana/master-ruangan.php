<?php
include('config.php');
include('class/master.class.php');

$id_pt = $id_pt_user;

if(isset($_GET['mode'])){
$mode = $_GET['mode'];
}elseif(isset($_POST['mode'])){
$mode = $_POST['mode'];
}else{
$mode = '';
}

if(isset($_POST['jenis'])){
	if($_POST['id'] <> ''){
		$db->Query("UPDATE RUANGAN SET NM_RUANGAN = '$_POST[nmruangan]' ,
					KAPASISITAS_RUANGAN = '$_POST[kapasitas]',
					ID_GEDUNG = '$_POST[nmgedung]',
					ID_TIPE_RUANGAN = '$_POST[jenis]'
					WHERE ID_RUANGAN = '$_POST[id]'");
	}else{
		//echo "INSERT INTO RUANGAN (NM_RUANGAN, KAPASISITAS_RUANGAN, ID_GEDUNG, ID_TIPE_RUANGAN) 
		//			VALUES ('$_POST[nmruangan]', '$_POST[kapasitas]', '$_POST[nmgedung]', '$_POST[jenis]')";
		$db->Query("INSERT INTO RUANGAN (NM_RUANGAN, KAPASITAS_RUANGAN, ID_GEDUNG, ID_TIPE_RUANGAN) 
					VALUES ('$_POST[nmruangan]', '$_POST[kapasitas]', '$_POST[nmgedung]', '$_POST[jenis]')");	
	}
}

if($mode == 'edit'){
	$master_edit = $db->QueryToArray("SELECT * FROM RUANGAN
							LEFT JOIN GEDUNG ON GEDUNG.ID_GEDUNG = RUANGAN.ID_GEDUNG 
							LEFT JOIN TIPE_RUANGAN ON TIPE_RUANGAN.ID_TIPE_RUANGAN = RUANGAN.ID_TIPE_RUANGAN
							LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = GEDUNG.ID_FAKULTAS
							WHERE ID_RUANGAN = '$_GET[id]'");
	$smarty->assign('master_edit', $master_edit);
}

$master = $db->QueryToArray("SELECT * FROM RUANGAN
							LEFT JOIN GEDUNG ON GEDUNG.ID_GEDUNG = RUANGAN.ID_GEDUNG 
							LEFT JOIN TIPE_RUANGAN ON TIPE_RUANGAN.ID_TIPE_RUANGAN = RUANGAN.ID_TIPE_RUANGAN
							LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = GEDUNG.ID_FAKULTAS
							WHERE GEDUNG.ID_PERGURUAN_TINGGI = {$id_pt}
							ORDER BY GEDUNG.ID_FAKULTAS, NM_RUANGAN");
$smarty->assign('master', $master);

	$jenis_gedung = $db->QueryToArray("SELECT * FROM TIPE_RUANGAN ORDER BY NM_TIPE_RUANGAN");
	$smarty->assign('jenis_gedung', $jenis_gedung);
	$gedung = $db->QueryToArray("SELECT * FROM GEDUNG WHERE ID_PERGURUAN_TINGGI = {$id_pt} ORDER BY NM_GEDUNG");
	$smarty->assign('gedung', $gedung);

$smarty->assign('mode', $mode);
$smarty->display("master/master-ruangan.tpl");
?>
