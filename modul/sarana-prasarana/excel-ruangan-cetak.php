<?php
include('config.php');

header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=ruangan".date('Y-m-d').".xls");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data";


if(isset($_REQUEST['gedung'])){
	$laporan = $db->QueryToArray("SELECT * FROM RUANGAN WHERE ID_GEDUNG = '$_REQUEST[gedung]' ORDER BY NM_RUANGAN");
	$tampil .= '<table border=1>
	<tr>
    	<th>Ruang/Hari</th>
        <th class="noheader">Senin</th>
        <th class="noheader">Selasa</th>
        <th class="noheader">Rabu</th>
        <th class="noheader">Kamis</th>
        <th class="noheader">Jumat</th>
        <th class="noheader">Sabtu</th>
		<th class="noheader">Minggu</th>
    </tr>';
	foreach($laporan as $data){
	
	$tampil .=  "<tr>
					<td>$data[NM_RUANGAN]</td>
				";
	$jam = $db->QueryToArray("SELECT NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, (jammulai || ' - ' || jamakhir) as JAM_M
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
JOIN (
	select jadwal_kelas.id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
						from jadwal_kelas
						JOIN RUANGAN ON RUANGAN.ID_RUANGAN = JADWAL_KELAS.ID_RUANGAN
						JOIN GEDUNG ON GEDUNG.ID_GEDUNG = RUANGAN.ID_GEDUNG
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
) A ON A.ID_RUANGAN = RUANGAN.ID_RUANGAN AND A.ID_JADWAL_HARI = jadwal_hari.ID_JADWAL_HARI AND A.NM_JADWAL_HARI = jadwal_hari.NM_JADWAL_HARI
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND jadwal_hari.NM_JADWAL_HARI = 'Senin'
									GROUP BY NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, jammulai, jamakhir									
									ORDER BY NM_RUANGAN, jammulai, jamakhir");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]'>$data2[JAM_M]</a>
											</td>
										</tr>";
						}
	$tampil .=  "	</table></td>";

	$jam = $db->QueryToArray("SELECT NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, (jammulai || ' - ' || jamakhir) as JAM_M
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
JOIN (
	select jadwal_kelas.id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
						from jadwal_kelas
						JOIN RUANGAN ON RUANGAN.ID_RUANGAN = JADWAL_KELAS.ID_RUANGAN
						JOIN GEDUNG ON GEDUNG.ID_GEDUNG = RUANGAN.ID_GEDUNG
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
) A ON A.ID_RUANGAN = RUANGAN.ID_RUANGAN AND A.ID_JADWAL_HARI = jadwal_hari.ID_JADWAL_HARI AND A.NM_JADWAL_HARI = jadwal_hari.NM_JADWAL_HARI
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND jadwal_hari.NM_JADWAL_HARI = 'Selasa'
									GROUP BY NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, jammulai, jamakhir									
									ORDER BY NM_RUANGAN, jammulai, jamakhir");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]'>$data2[JAM_M]</a>
											</td>
										</tr>";
						}	$tampil .=  "	</table></td>";
	
	$jam = $db->QueryToArray("SELECT NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, (jammulai || ' - ' || jamakhir) as JAM_M
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
JOIN (
	select jadwal_kelas.id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
						from jadwal_kelas
						JOIN RUANGAN ON RUANGAN.ID_RUANGAN = JADWAL_KELAS.ID_RUANGAN
						JOIN GEDUNG ON GEDUNG.ID_GEDUNG = RUANGAN.ID_GEDUNG
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
) A ON A.ID_RUANGAN = RUANGAN.ID_RUANGAN AND A.ID_JADWAL_HARI = jadwal_hari.ID_JADWAL_HARI AND A.NM_JADWAL_HARI = jadwal_hari.NM_JADWAL_HARI
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND jadwal_hari.NM_JADWAL_HARI = 'Rabu'
									GROUP BY NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, jammulai, jamakhir									
									ORDER BY NM_RUANGAN, jammulai, jamakhir");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]'>$data2[JAM_M]</a>
											</td>
										</tr>";
						}	$tampil .=  "	</table></td>";
	
	$jam = $db->QueryToArray("SELECT NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, (jammulai || ' - ' || jamakhir) as JAM_M
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
JOIN (
	select jadwal_kelas.id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
						from jadwal_kelas
						JOIN RUANGAN ON RUANGAN.ID_RUANGAN = JADWAL_KELAS.ID_RUANGAN
						JOIN GEDUNG ON GEDUNG.ID_GEDUNG = RUANGAN.ID_GEDUNG
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
) A ON A.ID_RUANGAN = RUANGAN.ID_RUANGAN AND A.ID_JADWAL_HARI = jadwal_hari.ID_JADWAL_HARI AND A.NM_JADWAL_HARI = jadwal_hari.NM_JADWAL_HARI
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND jadwal_hari.NM_JADWAL_HARI = 'Kamis'
									GROUP BY NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, jammulai, jamakhir									
									ORDER BY NM_RUANGAN, jammulai, jamakhir");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]'>$data2[JAM_M]</a>
											</td>
										</tr>";
						}	$tampil .=  "	</table></td>";
	
	$jam = $db->QueryToArray("SELECT NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, (jammulai || ' - ' || jamakhir) as JAM_M
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
JOIN (
	select jadwal_kelas.id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
						from jadwal_kelas
						JOIN RUANGAN ON RUANGAN.ID_RUANGAN = JADWAL_KELAS.ID_RUANGAN
						JOIN GEDUNG ON GEDUNG.ID_GEDUNG = RUANGAN.ID_GEDUNG
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
) A ON A.ID_RUANGAN = RUANGAN.ID_RUANGAN AND A.ID_JADWAL_HARI = jadwal_hari.ID_JADWAL_HARI AND A.NM_JADWAL_HARI = jadwal_hari.NM_JADWAL_HARI
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND jadwal_hari.NM_JADWAL_HARI = 'Jumat'
									GROUP BY NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, jammulai, jamakhir									
									ORDER BY NM_RUANGAN, jammulai, jamakhir");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]'>$data2[JAM_M]</a>
											</td>
										</tr>";
						}	$tampil .=  "	</table></td>";
	
	$jam = $db->QueryToArray("SELECT NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, (jammulai || ' - ' || jamakhir) as JAM_M
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
JOIN (
	select jadwal_kelas.id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
						from jadwal_kelas
						JOIN RUANGAN ON RUANGAN.ID_RUANGAN = JADWAL_KELAS.ID_RUANGAN
						JOIN GEDUNG ON GEDUNG.ID_GEDUNG = RUANGAN.ID_GEDUNG
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
) A ON A.ID_RUANGAN = RUANGAN.ID_RUANGAN AND A.ID_JADWAL_HARI = jadwal_hari.ID_JADWAL_HARI AND A.NM_JADWAL_HARI = jadwal_hari.NM_JADWAL_HARI
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND jadwal_hari.NM_JADWAL_HARI = 'Sabtu'
									GROUP BY NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, jammulai, jamakhir									
									ORDER BY NM_RUANGAN, jammulai, jamakhir");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]'>$data2[JAM_M]</a>
											</td>
										</tr>";
						}	$tampil .=  "	</table></td>";
	
	
	$jam = $db->QueryToArray("SELECT NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, (jammulai || ' - ' || jamakhir) as JAM_M
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
JOIN (
	select jadwal_kelas.id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
						from jadwal_kelas
						JOIN RUANGAN ON RUANGAN.ID_RUANGAN = JADWAL_KELAS.ID_RUANGAN
						JOIN GEDUNG ON GEDUNG.ID_GEDUNG = RUANGAN.ID_GEDUNG
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
) A ON A.ID_RUANGAN = RUANGAN.ID_RUANGAN AND A.ID_JADWAL_HARI = jadwal_hari.ID_JADWAL_HARI AND A.NM_JADWAL_HARI = jadwal_hari.NM_JADWAL_HARI
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND jadwal_hari.NM_JADWAL_HARI = 'Minggu'
									GROUP BY NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, jammulai, jamakhir									
									ORDER BY NM_RUANGAN, jammulai, jamakhir");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]'>$data2[JAM_M]</a>
											</td>
										</tr>";
						}	$tampil .=  "	</table></td>";
					
	$tampil .=  "</tr>";
	/*
	$tampil .=  "<tr>
					<td>$data[NM_RUANGAN]</td>
				";
				
		$jam = $db->QueryToArray("SELECT JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND NM_JADWAL_HARI = 'Senin'
									GROUP BY JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI									
									ORDER BY JAM_MULAI, MENIT_MULAI");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]'>$data2[JAM_MULAI]:$data2[MENIT_MULAI] - $data2[JAM_SELESAI]:$data2[MENIT_SELESAI]</a>
											</td>
										</tr>";
						}
	$tampil .=  "	</table></td>";

	$jam = $db->QueryToArray("SELECT JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND NM_JADWAL_HARI = 'Selasa'
									GROUP BY JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI									
									ORDER BY JAM_MULAI, MENIT_MULAI");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]'>$data2[JAM_MULAI]:$data2[MENIT_MULAI] - $data2[JAM_SELESAI]:$data2[MENIT_SELESAI]</a>
											</td>
										</tr>";
						}
	$tampil .=  "	</table></td>";
	
	$jam = $db->QueryToArray("SELECT JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND NM_JADWAL_HARI = 'Rabu'
									GROUP BY JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI									
									ORDER BY JAM_MULAI, MENIT_MULAI");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]'>$data2[JAM_MULAI]:$data2[MENIT_MULAI] - $data2[JAM_SELESAI]:$data2[MENIT_SELESAI]</a>
											</td>
										</tr>";
						}
	$tampil .=  "	</table></td>";
	
	$jam = $db->QueryToArray("SELECT JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND NM_JADWAL_HARI = 'Kamis'
									GROUP BY JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI									
									ORDER BY JAM_MULAI, MENIT_MULAI");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]'>$data2[JAM_MULAI]:$data2[MENIT_MULAI] - $data2[JAM_SELESAI]:$data2[MENIT_SELESAI]</a>
											</td>
										</tr>";
						}
	$tampil .=  "	</table></td>";
	
	$jam = $db->QueryToArray("SELECT JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND NM_JADWAL_HARI = 'Jumat'
									GROUP BY JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI									
									ORDER BY JAM_MULAI, MENIT_MULAI");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]'>$data2[JAM_MULAI]:$data2[MENIT_MULAI] - $data2[JAM_SELESAI]:$data2[MENIT_SELESAI]</a>
											</td>
										</tr>";
						}
	$tampil .=  "	</table></td>";
	
	$jam = $db->QueryToArray("SELECT JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND NM_JADWAL_HARI = 'Sabtu'
									GROUP BY JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI									
									ORDER BY JAM_MULAI, MENIT_MULAI");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]'>$data2[JAM_MULAI]:$data2[MENIT_MULAI] - $data2[JAM_SELESAI]:$data2[MENIT_SELESAI]</a>
											</td>
										</tr>";
						}
	$tampil .=  "	</table></td>";
	
	
	$jam = $db->QueryToArray("SELECT JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND NM_JADWAL_HARI = 'Minggu'
									GROUP BY JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI									
									ORDER BY JAM_MULAI, MENIT_MULAI");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]'>$data2[JAM_MULAI]:$data2[MENIT_MULAI] - $data2[JAM_SELESAI]:$data2[MENIT_SELESAI]</a>
											</td>
										</tr>";
						}
	$tampil .=  "	</table></td>";
					
	$tampil .=  "</tr>";
	*/}

	$tampil .= '</table>';
	echo $tampil;

}

?>
