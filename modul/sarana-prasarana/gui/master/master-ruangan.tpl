{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
$("#myTable").tablesorter(
		{
		sortList: [[0,0]],
		headers: {
6: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
<div class="center_title_bar">MASTER RUANGAN</div>
{if $mode == 'tambah' or $mode == 'edit'}
	{if $mode == 'edit'}
		{$nmruangan = $master_edit[0]['NM_RUANGAN']}
        {$kapasitas = $master_edit[0]['KAPASISITAS_RUANGAN']}
        {$nmgedung = $master_edit[0]['ID_GEDUNG']}
        {$jenis = $master_edit[0]['ID_TIPE_RUANGAN']}
		{$id = $master_edit[0]['ID_RUANGAN']}
    {else}
    	{$master = ''}
        {$id = ''}
    {/if}
<form name="f1" id="f1" action="master-ruangan.php" method="post">
<table>
    				<tr>
					  <td>Nama Ruang</td>
					  <td>:</td>
					  <td><input type="text" name="nmruangan" class="required" value="{$nmruangan}"></td>
					</tr>
					<tr>
					  <td>Kapasitas Ruang </td>
					  <td>:</td>
					  <td><input type="text" name="kapasitas" class="required number" value="{$kapasitas}"></td>
				    </tr>
                    
					<tr>
					  <td>Nama Gedung </td>
					  <td>:</td>
					  <td>
                      		<select name="nmgedung" class="required">
                            	<option value=""> Pilih Gedung </option>
                            	{foreach $gedung as $data}
                            	<option value="{$data.ID_GEDUNG}" {if $data.ID_GEDUNG == $nmgedung} selected="selected"{/if}>{$data.KODE_GEDUNG} - {$data.NM_GEDUNG}</option>
                                {/foreach}
                            </select>
                      </td>
				    </tr>
                    <tr>
					  <td>Tipe Ruangan </td>
					  <td>:</td>
					  <td>
                      		<select name="jenis">
                            	{foreach $jenis_gedung as $data}
                            	<option value="{$data.ID_TIPE_RUANGAN}" {if $data.ID_TIPE_RUANGAN == $jenis} selected="selected"{/if}>{$data.NM_TIPE_RUANGAN}</option>
                                {/foreach}
                            </select>
                      </td>
				    </tr>
    <tr>    	
        <td colspan="4" style="text-align:center"><input type="submit" value="Simpan" />
    		<input type="hidden" name="id" value="{$id}"  />
        </td>
    </tr>
</table>
</form>
{else}
<table id="myTable" class="tablesorter">
<thead>
	<tr>
    	<th>NO</th>
        <th>Nama Ruang</th>
		<th>Nama Gedung</th>
		<th>Kapasitas Ruang</th>
        <th>Fakultas</th>     
		<th>Type</th>
        <th class="noheader">AKSI</th>
    </tr>
    </thead>
    <tbody>
    {$no = 1}
    {foreach $master as $data}
    	<tr>
        	<td>{$no++}</td>
            <td>{$data.NM_RUANGAN}</td>
            <td>{$data.NM_GEDUNG}</td>
            <td>{$data.KAPASISITAS_RUANGAN}</td>
            <td>{$data.NM_FAKULTAS}</td>
            <td>{$data.NM_TIPE_RUANGAN}</td>
            <td style="text-align:center"><a href="master-ruangan.php?id={$data.ID_RUANGAN}&mode=edit">Edit</a></td>
        </tr>
    {/foreach}
    </tbody>
    <tr>
    	<td colspan="9" style="text-align:center">
        	<form action="master-ruangan.php" method="get">
                <input type="submit" value="Tambah" />
                <input type="hidden" value="tambah" name="mode" />
            </form>
        </td>
    </tr>
</table>
{/if}


{literal}
<script language="text/javascript">
            $('#f1').validate();
</script>
{/literal}