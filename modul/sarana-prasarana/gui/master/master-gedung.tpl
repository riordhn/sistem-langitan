{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
$("#myTable").tablesorter(
		{
		sortList: [[0,0]],
		headers: {
6: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
<div class="center_title_bar">MASTER GEDUNG</div>
{if $mode == 'tambah' or $mode == 'edit'}
	{if $mode == 'edit'}
		{$kdgedung = $master_edit[0]['KODE_GEDUNG']}
        {$nmgedung = $master_edit[0]['NM_GEDUNG']}
        {$lokasi = $master_edit[0]['LOKASI_GEDUNG']}
        {$deskripsi = $master_edit[0]['DESKRIPSI_GEDUNG']}
        {$fakultas = $master_edit[0]['ID_FAKULTAS']}
        {$jenis = $master_edit[0]['ID_JENIS_GEDUNG']}
		{$id = $master_edit[0]['ID_GEDUNG']}
    {else}
    	{$master = ''}
        {$id = ''}
    {/if}
<form name="f1" id="f1" action="master-gedung.php" method="post">
<table>
    				<tr>
					  <td>Kode Gedung</td>
					  <td>:</td>
					  <td><input type="text" name="kdgedung" class="required" value="{$kdgedung}"></td>
					</tr>
					<tr>
					  <td>Nama Gedung </td>
					  <td>:</td>
					  <td><input type="text" name="nmgedung" class="required" value="{$nmgedung}"></td>
				    </tr>
					<tr>
					  <td>Lokasi Gedung </td>
					  <td>:</td>
					  <td><input type="text" name="lokasi" class="required" value="{$lokasi}"></td>
				    </tr>
					<tr>
					  <td>Deskripsi </td>
					  <td>:</td>
					  <td><input type="text" name="deskripsi" value="{$deskripsi}"></td>
				    </tr>
                    <tr>
					  <td>Jenis Gedung </td>
					  <td>:</td>
					  <td>
                      		<select name="jenis">
                            	{foreach $jenis_gedung as $data}
                            	<option value="{$data.ID_JENIS_GEDUNG}" {if $data.ID_JENIS_GEDUNG == $jenis} selected="selected"{/if}>{$data.NM_JENIS_GEDUNG}</option>
                                {/foreach}
                            </select>
                      </td>
				    </tr>
    <tr>    	
        <td colspan="4" style="text-align:center"><input type="submit" value="Simpan" />
    		<input type="hidden" name="id" value="{$id}"  />
            <input type="hidden" name="fakultas" value="{$fakultas}"  />
        </td>
    </tr>
</table>
</form>
{else}
<table id="myTable" class="tablesorter">
<thead>
	<tr>
    	<th>NO</th>
        <th>Kode Gedung</th>
        <th>Nama Gedung</th>
        <th>Lokasi</th>
        <th>Deskripsi</th>
        <th>Fakultas</th>
        <th>Jenis Gedung</th>
        <th class="noheader">AKSI</th>
    </tr>
    </thead>
    <tbody>
    {$no = 1}
    {foreach $master as $data}
    	<tr>
        	<td>{$no++}</td>
            <td>{$data.KODE_GEDUNG}</td>
            <td>{$data.NM_GEDUNG}</td>
            <td>{$data.LOKASI_GEDUNG}</td>
            <td>{$data.DESKRIPSI_GEDUNG}</td>
            <td>{$data.NM_FAKULTAS}</td>
            <td>{$data.NM_JENIS_GEDUNG}</td>
            <td style="text-align:center"><a href="master-gedung.php?id={$data.ID_GEDUNG}&mode=edit">Edit</a></td>
        </tr>
    {/foreach}
    </tbody>
    <tr>
    	<td colspan="9" style="text-align:center">
        	<form action="master-gedung.php" method="get">
                <input type="submit" value="Tambah" />
                <input type="hidden" value="tambah" name="mode" />
            </form>
        </td>
    </tr>
</table>
{/if}


{literal}
<script language="text/javascript">
            $('#f1').validate();
</script>
{/literal}