<div class="center_title_bar">MASTER JENIS GEDUNG</div>
{if $mode == 'tambah' or $mode == 'edit'}
	{if $mode == 'edit'}
		{$master = $master_edit[0]['NM_JENIS_GEDUNG']}
		{$kegunaan = $master_edit[0]['KEGUNAAN_GEDUNG']}
		{$id = $master_edit[0]['ID_JENIS_GEDUNG']}
    {else}
    	{$master = ''}
        {$id = ''}
    {/if}
<form name="f1" id="f1" action="master-jenis-gedung.php" method="post">
<table>
	<tr>
   	  <td>Nama Master Jenis Gedung</td>
        <td><input name="master" type="text" value="{$master}" size="30" class="required" />
			<input type="hidden" name="id" value="{$id}"  /> 
        </td>
    </tr>
	<tr>
   	  <td>Dipergunakan Perkuliahan ?</td>
        <td>
			Ya <input type="radio" name="kegunaan" value="1" class="required" {if $kegunaan == 1} checked="checked"{/if} />
        	Tidak <input type="radio" name="kegunaan" value="0"  {if $kegunaan == 0} checked="checked"{/if} />
		</td>
    </tr>
    <tr>
    	<td></td>
        <td><input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>
{else}
<table>
	<tr>
    	<th>NO</th>
        <th>NAMA JENIS GEDUNG</th>
        <th>PERKULIAHAN ?</th>
		<th>AKSI</th>
    </tr>
    {$no = 1}
    {foreach $master as $data}
    	<tr>
        	<td>{$no++}</td>
            <td>{$data.NM_JENIS_GEDUNG}</td>
			<td>{if $data.KEGUNAAN_GEDUNG == 1}Ya{else}Tidak{/if}</td>
            <td style="text-align:center"><a href="master-jenis-gedung.php?id={$data.ID_JENIS_GEDUNG}&mode=edit">Edit</a></td>
        </tr>
    {/foreach}
    <tr>
    	<td colspan="5" style="text-align:center">
        	<form action="master-jenis-gedung.php" method="get">
                <input type="submit" value="Tambah" />
                <input type="hidden" value="tambah" name="mode" />
            </form>
        </td>
    </tr>
</table>
{/if}

{literal}
<script language="text/javascript">
            $('#f1').validate();
</script>
{/literal}