<div class="center_title_bar">REKAPITULASI  NILAI  MKWU</div>
<form method="post" action="laporan-nilai-mkwu.php">
<table>
<tr>
	<td>Semester</td>
	<td>
	<select name="semester">
					<option value="">Pilih Semester</option>
					{foreach $semester as $data}
					<option value="{$data.ID_SEMESTER}" {if $smarty.post.semester == $data.ID_SEMESTER} selected="selected"{/if}>{$data.THN_AKADEMIK_SEMESTER} - {$data.NM_SEMESTER}</option>
					{/foreach}
				</select>
	</td>
    <td>Mata Kuliah</td>
    <td>
    	<select name="mk">
        	<option value="SEMUA" {if $smarty.post.mk == 'SEMUA'} selected="selected"{/if}>SEMUA</option>
        	<option value="AGAMA ISLAM" {if $smarty.post.mk == 'AGAMA ISLAM'} selected="selected"{/if}>AGAMA ISLAM</option>
            <option value="PROTESTAN" {if $smarty.post.mk == 'PROTESTAN'} selected="selected"{/if}>PROTESTAN</option>
            <option value="KATOLIK" {if $smarty.post.mk == 'KATOLIK'} selected="selected"{/if}>KATOLIK</option>
            <option value="HINDU" {if $smarty.post.mk == 'HINDU'} selected="selected"{/if}>HINDU</option>
            <option value="BUDHA" {if $smarty.post.mk == 'BUDHA'} selected="selected"{/if}>BUDHA</option>
            <option value="KONGHUCU" {if $smarty.post.mk == 'KONGHUCU'} selected="selected"{/if}>KONGHUCU</option>
            <option value="PPKN" {if $smarty.post.mk == 'PPKN'} selected="selected"{/if}>PPKN</option>
            <option value="FILSAFAT ILMU" {if $smarty.post.mk == 'FILSAFAT ILMU'} selected="selected"{/if}>FILSAFAT ILMU</option>
            <option value="ILMU ALAMIAH DASAR" {if $smarty.post.mk == 'ILMU ALAMIAH DASAR'} selected="selected"{/if}>ILMU ALAMIAH DASAR</option>
            <option value="ILMU SOSIAL BUDAYA DASAR" {if $smarty.post.mk == 'ILMU SOSIAL BUDAYA DASAR'} selected="selected"{/if}>ILMU SOSIAL BUDAYA DASAR</option>
            <option value="BAHASA INGGRIS" {if $smarty.post.mk == 'BAHASA INGGRIS'} selected="selected"{/if}>BAHASA INGGRIS</option>
            <option value="BAHASA INDONESIA" {if $smarty.post.mk == 'BAHASA INDONESIA'} selected="selected"{/if}>BAHASA INDONESIA</option>
        </select>
    </td>
	<td><input type="submit" value="Tampil"></td>
</tr>

</table>
</form>

{if isset($smarty.request.semester)}

<table>
	<tr>
    	<th rowspan="2">No</th>
        <th rowspan="2">FAKULTAS</th>
        <th rowspan="2">PROGRAM STUDI</th>
        <th rowspan="2">MATA KULIAH</th>
        <th rowspan="2">DOSEN PENGAMPU</th>
        <th rowspan="2">JUMLAH PESERTA</th>
        <th colspan="8">NILAI</th>
    </tr>
    <tr>
    	<th>A</th>
        <th>AB</th>
        <th>B</th>
        <th>BC</th>
        <th>C</td>
        <th>D</th>
        <th>E</th>
        <th>BELUM DINILAI</th>
    </tr>
    {$no = 1}
    {foreach $mkwu as $data}
    <tr>
    	<td>{$no++}</td>
        <td>{$data.NM_FAKULTAS}</td>
        <td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
        <td>{$data.NM_MATA_KULIAH}</td>
        <td>{$data.NM_PENGGUNA}</td>
        <td>{$data.JUMLAH_PESERTA}</td>
        <td>{$data.NILAI_A}</td>
        <td>{$data.NILAI_AB}</td>
        <td>{$data.NILAI_B}</td>
        <td>{$data.NILAI_BC}</td>
        <td>{$data.NILAI_C}</td>
        <td>{$data.NILAI_D}</td>
        <td>{$data.NILAI_E}</td>
        <td>{$data.NILAI_NULL}</td>
    </tr>
    {/foreach}
</table>

{/if}