{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
	$("#myTable").tablesorter(
			{
			sortList: [[0,0]],
			headers: {
	1: { sorter: false },
	2: { sorter: false },
	3: { sorter: false },
	4: { sorter: false },
	5: { sorter: false },
	6: { sorter: false },
	7: { sorter: false }
			}
			}
	);
	
	$("#myTable2").tablesorter(
			{
			sortList: [[5,0]],
			headers: {
			}
			}
	);
}
);
</script>
{/literal}
<div class="center_title_bar">LAPORAN RUANGAN</div>
{if $smarty.get.gedung}
<p>
<a href="laporan-gedung.php">Kembali</a>
</p>

{elseif isset($ruangan)}

<P><H3>Ruangan {$ruangan[0]['NM_RUANGAN']} ({$ruangan[0]['NM_JADWAL_HARI']}, {$ruangan[0]['JAM']})</H3></P>

<table id="myTable2" class="tablesorter">
<thead>
	<tr>
    	<th>Kode Mata Ajar</th>
        <th>Mata Ajar</th>
		<th>SKS</th>
		<th>Prodi</th>
		<th>Kelas</th>
		<th>Semester</th>
    </tr>
    </thead>
    <tbody>
		{foreach $ruangan as $data}
			<tr>
				<td>{$data.KD_MATA_KULIAH}</td>
				<td>{$data.NM_MATA_KULIAH}</td>
				<td>{$data.KREDIT_SEMESTER}</td>
				<td>{$data.PRODIASAL}</td>
				<td>{$data.NAMA_KELAS}</td>
				<td>{$data.SMT}</td>
			</tr>
		{/foreach}
    </tbody>
</table>

<a href="laporan-ruangan.php?gedung={$ruangan[0]['ID_GEDUNG']}">Kembali</a>

{else}
<form action="laporan-ruangan.php" method="post">
<table>
	<tr>
		<td>Gedung</td>
		<td>
			<select name="gedung">
				<option value="">Pilih Gedung</option>
				{foreach $gedung as $data}
					<option value="{$data.ID_GEDUNG}" {if $data.ID_GEDUNG == $smarty.post.gedung} selected="selected"{/if}>{$data.KODE_GEDUNG} - {$data.NM_GEDUNG} ({$data.SINGKATAN_FAKULTAS})</option>
				{/foreach}
			</select>
		</td>
        <td>Semester</td>
		<td>
	<select name="semester">
					<option value="">Pilih Semester</option>
					{foreach $semester as $data}
					<option value="{$data.ID_SEMESTER}" {if $smarty.post.semester == $data.ID_SEMESTER} selected="selected"{/if}>{$data.THN_AKADEMIK_SEMESTER} - {$data.NM_SEMESTER}</option>
					{/foreach}
				</select>
                </td>
		<td><input type="submit" value="Tampil" /></td>
	</tr>
</table>
</form>
{/if}

{if isset($tampil)}
<input type="button" value="Cetak" onclick="window.open('excel-ruangan-cetak.php?gedung={$smarty.request.gedung}')"/><br />
<table id="myTable" class="tablesorter">
<thead>
	<tr>
    	<th>Ruang/Hari</th>
        <th class="noheader">Senin</th>
        <th class="noheader">Selasa</th>
        <th class="noheader">Rabu</th>
        <th class="noheader">Kamis</th>
        <th class="noheader">Jumat</th>
        <th class="noheader">Sabtu</th>
		<th class="noheader">Minggu</th>
    </tr>
    </thead>
    <tbody>
		{$tampil}
    </tbody>
</table>
{/if}