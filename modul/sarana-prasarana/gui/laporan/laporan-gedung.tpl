{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
$("#myTable").tablesorter(
		{
		sortList: [[0,0]],
		headers: {
1: { sorter: false },
2: { sorter: false },
3: { sorter: false },
4: { sorter: false },
5: { sorter: false },
6: { sorter: false },
7: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
<div class="center_title_bar">MONITORING GEDUNG</div>

{if isset($tampil)}
<table id="myTable" class="tablesorter">
<thead>
	<tr>
    	<th>Gedung/Hari</th>
        <th class="noheader">Senin</th>
        <th class="noheader">Selasa</th>
        <th class="noheader">Rabu</th>
        <th class="noheader">Kamis</th>
        <th class="noheader">Jumat</th>
        <th class="noheader">Sabtu</th>
		<th class="noheader">Minggu</th>
    </tr>
    </thead>
    <tbody>
	{$tampil}
    </tbody>
</table>
{else}

<P><H3>Gedung {$detail[0]['NM_GEDUNG']}</H3></P>

<table id="myTable" class="tablesorter">

	<thead>
	<tr>
    	<th>Ruang/Hari</th>
        <th class="noheader">{$detail[0]['NM_JADWAL_HARI']}</th>
    </tr>
    </thead>
    <tbody>
		{foreach $detail as $data}
			<tr>
				<td>{$data.NM_RUANGAN}</td>
				<td><a href="laporan-ruangan.php?ruangan={$data.ID_RUANGAN}&hari={$data.ID_JADWAL_HARI}&jam={$data.ID_JADWAL_JAM}">{$data.JAM_MULAI}:{$data.MENIT_MULAI} - {$data.JAM_SELESAI}:{$data.MENIT_SELESAI}</a></td>
			</tr>
		{/foreach}
    </tbody>
</table>

<a href="laporan-gedung.php">Kembali</a>

{/if}