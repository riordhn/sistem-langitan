{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[1,0],[3,0]],
		headers: {
            10: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}

<div id="utility-manajemen_kelas">
<div class="center_title_bar">MANAJEMEN KELAS [HAPUS - TAMBAH KELAS - TAMBAH HARI]</div>
<div id="tabs">
    <div id="tab1" {if $smarty.get.action == ''}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('1');"><a href="jadwal-kuliah-psikologi.php">Rincian </a></div>
	<div id="tab2" {if $smarty.get.action == 'updateview'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Update</div>
	<div id="tab3" {if $smarty.get.action == 'adkelview'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Tambah Kelas</div>
	<div id="tab4" {if $smarty.get.action == 'adhariview'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('4');">Tambah Hari</div>
</div>

<div class="panel" id="panel1" style="display: {$disp1} ">
<p> </p>
<form action="jadwal-kuliah-psikologi.php" method="post">
<input type="hidden" name="action" value="tampil" >
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td>
			 	Tahun Akademik :
                <select name="smt">
    		   <option value=''>-- PILIH THN AKD --</option>
	 		   {foreach item="smt" from=$T_ST}
    		   {html_options values=$smt.ID_SEMESTER output=$smt.SMT selected=$smtaktif}
	 		   {/foreach}
			   </select>
		     </td>
			 <td> <input type="submit" name="View" value="View"> </td>
           </tr>
</table>
</form>
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
             <th width="5%">Kode</th>
             <th width="32%">Nama Mata Ajar</th>
             <th width="4%">SKS</th>
			 <th width="4%">KLS</th>
             <th width="4%">Ruang</th>
             <th width="4%">Hari</th>
			 <th width="7%">Jam</th>
			 <th width="10%">Prodi</th>
             <th class="noheader" width="20%">Aksi</th>
           </tr>
		</thead>
		<tbody>
           {foreach name=test item="list" from=$T_MK}
		   <tr>
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td><center>{$list.KREDIT_SEMESTER}</center></td>
			 <td><center>{$list.NAMA_KELAS}</center></td>
             <td><center>{$list.NM_RUANGAN}</center></td>
             <td><center>{$list.NM_JADWAL_HARI}</center></td>
			 <td><center>{$list.JAM}</center></td>
			 <td>{$list.PRODIASAL}</td>
             <td><a href="jadwal-kuliah-psikologi.php?action=updateview&id_klsmk={$list.ID_KELAS_MK}&id_jk={$list.ID_JADWAL_KELAS}">Update</a><br><a href="jadwal-kuliah-psikologi.php?action=delhari&id_jk={$list.ID_JADWAL_KELAS}">Del Hari</a> | <a href="jadwal-kuliah-psikologi.php?action=del&id_klsmk={$list.ID_KELAS_MK}&smt={$list.ID_SEMESTER}">Del Kls</a><br><a href="jadwal-kuliah-psikologi.php?action=adkelview&id_klsmk={$list.ID_KELAS_MK}&smt={$list.ID_SEMESTER}">Add Kls</a> | <a href="jadwal-kuliah-psikologi.php?action=adhariview&id_klsmk={$list.ID_KELAS_MK}&smt={$list.ID_SEMESTER}">Add Hari</a></td>
           </tr>
		   {foreachelse}
           <tr><td colspan="10"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
   	</table>
</div>

<div class="panel" id="panel2" style="display: {$disp2}">
<p> </p>
		<form action="jadwal-kuliah-psikologi.php" method="post">
			 <input type="hidden" name="action" value="update1" >
			  <table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="3"><strong> Lengkapi Kelas </strong></td></tr>
					{foreach item="ubah" from=$TJAF}
					<input type="hidden" name="id_kelas_mk" value="{$ubah.ID_KELAS_MK}" />
					<input type="hidden" name="id_jk" value="{$ubah.ID_JADWAL_KELAS}" />
				  	<input type="hidden" name="smtb" value="{$ubah.ID_SEMESTER}" />
					<tr>
					  <td width="29%" >Kode Mata Kuliah</td>
					  <td width="2%" >:</td>
					  <td width="69%" >{$ubah.KD_MATA_KULIAH}</td>
					</tr>
					<tr>
					  <td>Nama Mata Kuliah</td>
					  <td>:</td>
					  <td>{$ubah.NM_MATA_KULIAH}</td>
					</tr>
					<tr>
					  <td>Kelas MK [A/B/C/D]</td>
					  <td>:</td>
					  <td><select name="kelas_mk" id="kelas_mk">
                            {foreach item="namakls" from=$NM_KELAS}
    		   				{html_options values=$namakls.ID_NAMA_KELAS output=$namakls.NAMA_KELAS selected=$ubah.NO_KELAS_MK}
	 		   				{/foreach}
                          </select>
					  </td>
				    </tr>
					<tr>
					  <td>Status [Reguler/Alih Jalur]</td>
					  <td>:</td>
					  <td><select name="status_kls">
    		   				{html_options values=$statuskls output=$statuskls selected=$ubah.STATUS}
                          </select>
					  </td>
				    </tr>
					<tr>
					  <td>KAPASITAS KELAS</td>
					  <td>:</td>
					  <td><input name="kap_kelas" id="kap_kelas" type="text" class="required" size="3" maxlength="3" class="required" value="{$ubah.KAPASITAS_KELAS_MK}"/> [WAJIB DI-ISI]</td>
				    </tr>
					<tr>
					  <td>Rencana Perkuliahan</td>
					  <td>:</td>
					  <td><input name="ren_kul" id="ren_kul" type="text" class="required" size="3" maxlength="3" class="required" value="{$ubah.JUMLAH_PERTEMUAN_KELAS_MK}"/> [WAJIB DI-ISI]</td>
				    </tr>
					<tr>
					  <td>Hari</td>
					  <td>:</td>
					  <td>
                      	<select name="hari" id="hari">
                            {foreach item="hari" from=$T_HARI}
    		   				{html_options values=$hari.ID_JADWAL_HARI output=$hari.NM_JADWAL_HARI selected=$ubah.ID_JADWAL_HARI}
	 		   				{/foreach}
                          </select>
                      </td>
				    </tr>
					<tr>
					  <td>Jam</td>
					  <td>:</td>
					  <td>
                      	<select name="jam" id="jam">
                            {foreach item="jam" from=$T_JAM}
    		   				{html_options values=$jam.ID_JADWAL_JAM output=$jam.NM_JADWAL_JAM selected=$ubah.ID_JADWAL_JAM}
	 		   				{/foreach}
                          </select>
                      </td>
				    </tr>
					<tr>
					  <td>Ruangan</td>
					  <td>:</td>
					  <td>
                      	<select name="ruangan" id="ruangan">
                            {foreach item="ruangan" from=$T_RUANG}
    		   				{html_options values=$ruangan.ID_RUANGAN output=$ruangan.RUANG selected=$ubah.ID_RUANGAN}
	 		   				{/foreach}
                          </select>
                      </td>
				    </tr>
					{/foreach}
          </table>
<p><input type="button" name="kembali" value="Kembali" onClick="window.location.href='#utility-manajemen!jadwal-kuliah-psikologi.php'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="Simpan1" value="Simpan"></p>
</form>
</div>

<div class="panel" id="panel3" style="display: {$disp3}">
<p> </p>
		<form action="jadwal-kuliah-psikologi.php" method="post">
			 <input type="hidden" name="action" value="tambahkelas" >
			  <table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="3"><strong> Tambah Kelas </strong></td></tr>
					{foreach item="tambah" from=$TJAF1}
					<input type="hidden" name="id_kur_mk" value="{$tambah.ID_KURIKULUM_MK}" />
				  	<input type="hidden" name="smtb" value="{$tambah.ID_SEMESTER}" />
					<input type="hidden" name="prodi" value="{$tambah.ID_PROGRAM_STUDI}" />
					<tr>
					  <td width="29%" >Kode Mata Kuliah</td>
					  <td width="2%" >:</td>
					  <td width="69%" >{$tambah.KD_MATA_KULIAH}</td>
					</tr>
					<tr>
					  <td>Nama Mata Kuliah</td>
					  <td>:</td>
					  <td>{$tambah.NM_MATA_KULIAH}</td>
					</tr>
					<tr>
					  <td>Program Studi</td>
					  <td>:</td>
					  <td><select name="kd_prodi" id="kd_prodi">
                            {foreach item="nmprodi" from=$T_PRODI}
    		   				{html_options values=$nmprodi.ID_PROGRAM_STUDI output=$nmprodi.NAMAPRODI selected=$tambah.ID_PROGRAM_STUDI}
	 		   				{/foreach}
                          </select>
					 </td>
				    </tr>
					<tr bgcolor="yellow">
					  <td>Kelas MK [A/B/C/D]</td>
					  <td>:</td>
					  <td><select name="kelas_mk" id="kelas_mk">
							<option value=''>------</option>
                            {foreach item="namakls" from=$NM_KELAS}
    		   				{html_options values=$namakls.ID_NAMA_KELAS output=$namakls.NAMA_KELAS}
	 		   				{/foreach}
                          </select>
					  </td>
				    </tr>
					<tr>
					  <td>KAPASITAS KELAS</td>
					  <td>:</td>
					  <td><input name="kap_kelas" id="kap_kelas" type="text" class="required" size="3" maxlength="3" class="required" value="{$tambah.KAPASITAS_KELAS_MK}"/></td>
				    </tr>
					<tr>
					  <td>Rencana Perkuliahan</td>
					  <td>:</td>
					  <td><input name="ren_kul" id="ren_kul" type="text" class="required" size="3" maxlength="3" class="required" value="{$tambah.JUMLAH_PERTEMUAN_KELAS_MK}"/></td>
				    </tr>
					<tr>
					  <td>Hari</td>
					  <td>:</td>
					  <td>
                      	<select name="hari" id="hari">
                            {foreach item="hari" from=$T_HARI}
    		   				{html_options values=$hari.ID_JADWAL_HARI output=$hari.NM_JADWAL_HARI selected=$tambah.ID_JADWAL_HARI}
	 		   				{/foreach}
                          </select>
                      </td>
				    </tr>
					<tr>
					  <td>Jam</td>
					  <td>:</td>
					  <td>
                      	<select name="jam" id="jam">
                            {foreach item="jam" from=$T_JAM}
    		   				{html_options values=$jam.ID_JADWAL_JAM output=$jam.NM_JADWAL_JAM selected=$tambah.ID_JADWAL_JAM}
	 		   				{/foreach}
                          </select>
                      </td>
				    </tr>
					<tr bgcolor="yellow">
					  <td>Ruangan</td>
					  <td>:</td>
					  <td>
                      	<select name="ruangan" id="ruangan">
							<option value=''>------</option>
                            {foreach item="ruangan" from=$T_RUANG}
    		   				{html_options values=$ruangan.ID_RUANGAN output=$ruangan.RUANG}
	 		   				{/foreach}
                          </select>
                      </td>
				    </tr>
					{/foreach}
          </table>
<p><input type="button" name="kembali" value="Kembali" onClick="window.location.href='#utility-manajemen!jadwal-kuliah-psikologi.php'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="Simpan1" value="Tambah Kelas"></p>
</form>
</div>

<div class="panel" id="panel4" style="display: {$disp4}">
<p> </p>
		<form action="jadwal-kuliah-psikologi.php" method="post">
			 <input type="hidden" name="action" value="tambahhari" >
			  <table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="3"><strong> Tambah Hari </strong></td></tr>
					{foreach item="tambah1" from=$TJAF2}
					<input type="hidden" name="id_kelas_mk" value="{$tambah1.ID_KELAS_MK}" />
				  	<input type="hidden" name="smtb" value="{$tambah1.ID_SEMESTER}" />
					<tr>
					  <td width="29%" >Kode Mata Ajar</td>
					  <td width="2%" >:</td>
					  <td width="69%" >{$tambah1.KD_MATA_KULIAH}</td>
					</tr>
					<tr>
					  <td>Nama Mata Ajar</td>
					  <td>:</td>
					  <td>{$tambah1.NM_MATA_KULIAH}</td>
					</tr>
					<tr>
					  <td>Kelas MK [A/B/C/D]</td>
					  <td>:</td>
					  <td><select name="kelas_mk" id="kelas_mk">
                            {foreach item="namakls" from=$NM_KELAS}
    		   				{html_options values=$namakls.ID_NAMA_KELAS output=$namakls.NAMA_KELAS selected=$tambah1.NO_KELAS_MK}
	 		   				{/foreach}
                          </select>
					  </td>
				    </tr>
					<tr>
					  <td>KAPASITAS KELAS</td>
					  <td>:</td>
					  <td><input name="kap_kelas" id="kap_kelas" type="text" class="required" size="3" maxlength="3" class="required" value="{$tambah1.KAPASITAS_KELAS_MK}"/></td>
				    </tr>
					<tr>
					  <td>Rencana Perkuliahan</td>
					  <td>:</td>
					  <td><input name="ren_kul" id="ren_kul" type="text" class="required" size="3" maxlength="3" class="required" value="{$tambah1.JUMLAH_PERTEMUAN_KELAS_MK}"/></td>
				    </tr>
					<tr bgcolor="yellow">
					  <td>Hari</td>
					  <td>:</td>
					  <td>
                      	<select name="hari" id="hari">
							<option value=''>------</option>
                            {foreach item="hari" from=$T_HARI}
    		   				{html_options values=$hari.ID_JADWAL_HARI output=$hari.NM_JADWAL_HARI}
	 		   				{/foreach}
                          </select>
                      </td>
				    </tr>
					<tr>
					  <td>Jam</td>
					  <td>:</td>
					  <td>
                      	<select name="jam" id="jam">
                            {foreach item="jam" from=$T_JAM}
    		   				{html_options values=$jam.ID_JADWAL_JAM output=$jam.NM_JADWAL_JAM selected=$tambah1.ID_JADWAL_JAM}
	 		   				{/foreach}
                          </select>
                      </td>
				    </tr>
					<tr>
					  <td>Ruangan</td>
					  <td>:</td>
					  <td>
                      	<select name="ruangan" id="ruangan">
                            {foreach item="ruangan" from=$T_RUANG}
    		   				{html_options values=$ruangan.ID_RUANGAN output=$ruangan.RUANG selected=$tambah1.ID_RUANGAN}
	 		   				{/foreach}
                          </select>
                      </td>
				    </tr>
					{/foreach}
          </table>
<p><input type="button" name="kembali" value="Kembali" onClick="window.location.href='#utility-manajemen!jadwal-kuliah-psikologi.php'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="Simpan2" value="Tambah Hari"></p>
 </form>
</div>
</div>