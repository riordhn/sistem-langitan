<?php
include('../../config.php');

foreach ($user->MODULs as &$modul)
{
	$nm_modul    = &$modul['NM_MODUL'];
	$akses_modul = &$modul['AKSES'];
	
	// Admin
	if (in_array($user->ID_JABATAN_PEGAWAI, array(175, 176, 203)))
	{
		// User level unlock
		if ($nm_modul == 'report')     { $akses_modul = 1; }
		if ($nm_modul == 'penerimaan') { $akses_modul = 1; }
		if ($nm_modul == 'penilaian')  { $akses_modul = 1; }
		if ($nm_modul == 'penetapan')  { $akses_modul = 1; }
		if ($nm_modul == 'pengawas')   { $akses_modul = 1; }
		if ($nm_modul == 'snmptn')     { $akses_modul = 1; }    
	}
	
	// Petugas Plotting Ruangan
	if ($user->ID_JABATAN_PEGAWAI == 222)
	{
		if ($nm_modul == 'pengawas')   { $akses_modul = 1; }
		if ($nm_modul == 'peserta')    { $akses_modul = 0; }
		if ($nm_modul == 'verifikasi') { $akses_modul = 0; }
	}
	
	// Bendahara
	if ($user->ID_JABATAN_PEGAWAI == 221)
	{
		if ($nm_modul == 'penerimaan') { $akses_modul = 1; }
		if ($nm_modul == 'pengawas')   { $akses_modul = 1; }
		if ($nm_modul == 'peserta')    { $akses_modul = 0; }
		if ($nm_modul == 'verifikasi') { $akses_modul = 0; }
	}
	
	foreach ($modul['MENUs'] as &$menu)
	{
		$nm_menu = &$menu['NM_MENU'];
		$akses_menu = &$menu['AKSES'];
		
		if ($user->ID_JABATAN_PEGAWAI == 203)
		{
			if ($nm_modul == 'report' and $nm_menu == 'utility') { $akses_menu = 1; }
		}
		
		// Petugas Plotting ruangan
		if ($user->ID_JABATAN_PEGAWAI == 222)
		{
			// Hide master honor
			if ($nm_modul == 'pengawas' and $nm_menu == 'honor') { $akses_menu = 0; }
		}
		
		// Bendahara
		if ($user->ID_JABATAN_PEGAWAI == 221)
		{
			if ($nm_modul == 'penerimaan' && $nm_menu == 'master')     { $akses_menu = 0; }
			if ($nm_modul == 'penerimaan' && $nm_menu == 'prodi')      { $akses_menu = 0; }
			if ($nm_modul == 'penerimaan' && $nm_menu == 'voucher')    { $akses_menu = 0; }
		}
	}
}
?>
