<?php
error_reporting (E_ALL & ~E_NOTICE);
require('config.php');
require_once ('ociFunction.php');

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');


$id_pengguna= $user->ID_PENGGUNA; 
$kdfak=$user->ID_FAKULTAS;

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True'");
$smt1 = isSet($_REQUEST['smt']) ? $_REQUEST['smt'] : $smtaktif['ID_SEMESTER'];
$smarty->assign('SMT',$smt1);

$datafakul=getData("select id_fakultas,nm_fakultas from fakultas order by id_fakultas");
$smarty->assign('T_FAKUL',$datafakul);

if ($_POST['action']=='cari') 
{
	$awal=$_POST['calawal'];
	$smarty->assign('tglpesan',$awal);
	
	$fakul=$_POST['fakul'];
	$jamawal=$_POST['jamawal'];
	$smarty->assign('jampesan1',$jamawal);
	$jamakhir=$_POST['jamakhir'];
	$smarty->assign('jampesan2',$jamakhir);
	
	$array_hari = array(1=>"Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","Minggu");	
	$n=date("N", strtotime($awal));
	$hari = $array_hari[$n]; 
	
	if ($fakul==0) {
		$ruang=getData("select s1.id_ruangan,nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_fakultas,
						id_jadwal_hari,nm_jadwal_hari,jammulai,jamakhir,'kosong' as status from 
						(select nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_ruangan,gedung.id_fakultas from ruangan
						left join gedung on ruangan.id_gedung=gedung.id_gedung
						left join fakultas on gedung.id_fakultas=fakultas.id_fakultas
						order by gedung.id_fakultas)s1
						left join (
						select id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
						from jadwal_kelas
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
						where id_semester in (select id_semester from semester where status_aktif_semester='True'))s2 on s1.id_ruangan=s2.id_ruangan
						where (nm_jadwal_hari like '$hari%' or nm_jadwal_hari is null)
						and s1.id_ruangan not in (
							select id_ruangan from (
							select id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
							coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
							coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
							from jadwal_kelas
							left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
							left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
							left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
							left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
							left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
							left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
							where id_semester in (select id_semester from semester where status_aktif_semester='True')
							and (nm_jadwal_hari like '$hari%' or nm_jadwal_hari is null))
							where ((trim(substr('$jamawal',1,2))>=trim(coalesce(substr(jammulai,1,2),'00'))
							and trim(substr('$jamawal',1,2))<=trim(coalesce(substr(jamakhir,1,2),'00')))
							or (trim(substr('$jamakhir',1,2))>=trim(coalesce(substr(jammulai,1,2),'00'))
							and trim(substr('$jamakhir',1,2))<=trim(coalesce(substr(jamakhir,1,2),'00'))))
							and id_ruangan is not null)
						order by s1.id_fakultas,nm_ruangan,jammulai");
						
						
	}
	else
	{
	
		$ruang=getData("select s1.id_ruangan,nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_fakultas,
						id_jadwal_hari,nm_jadwal_hari,jammulai,jamakhir,'kosong' as status from 
						(select nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_ruangan,gedung.id_fakultas from ruangan
						left join gedung on ruangan.id_gedung=gedung.id_gedung
						left join fakultas on gedung.id_fakultas=fakultas.id_fakultas
						order by gedung.id_fakultas)s1
						left join (
						select id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
						from jadwal_kelas
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
						where id_semester in (select id_semester from semester where status_aktif_semester='True'))s2 on s1.id_ruangan=s2.id_ruangan
						where id_fakultas=$fakul and
						(nm_jadwal_hari like '$hari%' or nm_jadwal_hari is null)
						and s1.id_ruangan not in (
							select id_ruangan from (
							select id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
							coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
							coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
							from jadwal_kelas
							left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
							left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
							left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
							left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
							left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
							left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
							where id_semester in (select id_semester from semester where status_aktif_semester='True')
							and (nm_jadwal_hari like '$hari%' or nm_jadwal_hari is null) and program_studi.id_fakultas=$fakul)
							where ((trim(substr('$jamawal',1,2))>=trim(coalesce(substr(jammulai,1,2),'00'))
							and trim(substr('$jamawal',1,2))<=trim(coalesce(substr(jamakhir,1,2),'00')))
							or (trim(substr('$jamakhir',1,2))>=trim(coalesce(substr(jammulai,1,2),'00'))
							and trim(substr('$jamakhir',1,2))<=trim(coalesce(substr(jamakhir,1,2),'00'))))
							and id_ruangan is not null)
						order by id_fakultas,nm_ruangan,jammulai");
	}
	$smarty->assign('T_RUANG',$ruang);

}

if ($_GET['action']=='pesan') 
{
	$id_ruangan=$_GET['idruang'];
	$hari=$_GET['hari'];
	$smarty->assign('hari',$hari);
	
	$tglpesan=$_GET['tgl'];
	$smarty->assign('tglpakai',$tglpesan);
	$jaw=$_GET['jaw'];
	$smarty->assign('jamawal',$jaw);
	$jak=$_GET['jak'];
	$smarty->assign('jamakhir',$jak);
	
	
	
	$smarty->assign('disp1','none');
	$smarty->assign('disp2','block');
	
	$pesanruang=getData("select nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_ruangan,
				gedung.id_fakultas 
				from ruangan 
				left join gedung on ruangan.id_gedung=gedung.id_gedung 
				left join fakultas on gedung.id_fakultas=fakultas.id_fakultas 
				where id_ruangan=$id_ruangan");
	$smarty->assign('T_PESAN',$pesanruang);
	
	$fakul=$_POST['fakul'];
	if ($kdfak==0) {
		$ruang=getvar("select count(*) as cek 
						 from 
						(select nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_ruangan,gedung.id_fakultas from ruangan
						left join gedung on ruangan.id_gedung=gedung.id_gedung
						left join fakultas on gedung.id_fakultas=fakultas.id_fakultas
						order by gedung.id_fakultas)s1
						left join (
						select id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
						from jadwal_kelas
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
						where id_semester in (select id_semester from semester where status_aktif_semester='True'))s2 on s1.id_ruangan=s2.id_ruangan
						where (nm_jadwal_hari like '$hari%' or nm_jadwal_hari is null)
						and s1.id_ruangan not in (
							select id_ruangan from (
							select id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
							coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
							coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
							from jadwal_kelas
							left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
							left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
							left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
							left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
							left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
							left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
							where id_semester in (select id_semester from semester where status_aktif_semester='True')
							and (nm_jadwal_hari like '$hari%' or nm_jadwal_hari is null))
							where ((trim(substr('$jamawal',1,2))>=trim(coalesce(substr(jammulai,1,2),'00'))
							and trim(substr('$jamawal',1,2))<=trim(coalesce(substr(jamakhir,1,2),'00')))
							or (trim(substr('$jamakhir',1,2))>=trim(coalesce(substr(jammulai,1,2),'00'))
							and trim(substr('$jamakhir',1,2))<=trim(coalesce(substr(jamakhir,1,2),'00'))))
							and id_ruangan is not null)
						order by s1.id_fakultas,nm_ruangan,jammulai");
						
						
	}
	elseif($kdfak!=$fakul)
	{
	
		$ruang=getvar("select count(*) as cek 
						from 
						(select nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_ruangan,gedung.id_fakultas from ruangan
						left join gedung on ruangan.id_gedung=gedung.id_gedung
						left join fakultas on gedung.id_fakultas=fakultas.id_fakultas
						order by gedung.id_fakultas)s1
						left join (
						select id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
						from jadwal_kelas
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
						where id_semester in (select id_semester from semester where status_aktif_semester='True'))s2 on s1.id_ruangan=s2.id_ruangan
						where id_fakultas=$fakul and
						(nm_jadwal_hari like '$hari%' or nm_jadwal_hari is null)
						and s1.id_ruangan not in (
							select id_ruangan from (
							select id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
							coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
							coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
							from jadwal_kelas
							left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
							left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
							left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
							left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
							left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
							left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
							where id_semester in (select id_semester from semester where status_aktif_semester='True')
							and (nm_jadwal_hari like '$hari%' or nm_jadwal_hari is null) and program_studi.id_fakultas=$kdfak)
							where ((trim(substr('$jamawal',1,2))>=trim(coalesce(substr(jammulai,1,2),'00'))
							and trim(substr('$jamawal',1,2))<=trim(coalesce(substr(jamakhir,1,2),'00')))
							or (trim(substr('$jamakhir',1,2))>=trim(coalesce(substr(jammulai,1,2),'00'))
							and trim(substr('$jamakhir',1,2))<=trim(coalesce(substr(jamakhir,1,2),'00'))))
							and id_ruangan is not null)
						order by id_fakultas,nm_ruangan,jammulai");
	}
	$smarty->assign('P_RUANG',$ruang);
	
}
if ($_POST['action']=='proses_pesan') 
{
	$awal=$_POST['calawal'];
	$smarty->assign('tglpesan',$awal);
	
	$id_ruangan=$_POST['idruang'];
	$jamawal=$_POST['jaw'];
	$jamakhir=$_POST['jak'];
	$tglpakai=$_POST['tglpakai'];
	$catatan=$_POST['catatan'];

	InsertData("insert into pesan_ruangan(id_ruangan,id_fakultas_pemesan,id_pemesan,tgl_pakai,jam_mulai,jam_selesai,catatan)
				values ($id_ruangan,$kdfak,$id_pengguna,'$tglpakai','$jamawal','$jamakhir','$catatan')");
	//echo "insert into pesan_ruangan(id_ruangan,id_fakultas_pemesan,id_pemesan,tgl_pakai,jam_mulai,jam_selesai,catatan)
	//			values ($id_ruangan,$kdfak,$id_pengguna,'$tglpakai','$jamawal','$jamakhir','$catatan')";
	
	echo '<script>alert("Pemesanan Ruangan Sudah Diproses cek menu approve ruangan...")</script>';
	
}

$smarty->display('transaksi/transaksi-ruangan.tpl');
?>
