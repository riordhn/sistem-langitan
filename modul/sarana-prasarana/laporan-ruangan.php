<?php
include('config.php');


$semester = $db->QueryToArray("SELECT * FROM SEMESTER WHERE NM_SEMESTER = 'Ganjil' or NM_SEMESTER = 'Genap' ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER");
$smarty->assign('semester', $semester);


if(isset($_GET['ruangan'])){

$ruangan = $db->QueryToArray("select distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
							kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester, 
							jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
							jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam, 
							ruangan.id_ruangan,nm_ruangan,nm_jenjang||'-'||coalesce(nm_singkat_prodi,nm_program_studi) as prodiasal,
							no_kelas_mk,nama_kelas.nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,kelas_mk.status, ruangan.id_gedung
							from kelas_mk 
							left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
							left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
							left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
							left join semester on kelas_mk.id_semester=semester.id_semester 
							left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk 
							left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
							left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
							left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
							left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
							left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
							left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
							where ruangan.id_ruangan = '$_GET[ruangan]' and jadwal_hari.id_jadwal_hari = '$_GET[hari]' and  jadwal_jam.id_jadwal_jam = '$_GET[jam]' and kelas_mk.id_semester = '$_GET[semester]'");
$smarty->assign('ruangan', $ruangan);

}else{

$gedung = $db->QueryToArray("SELECT * FROM GEDUNG JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = GEDUNG.ID_FAKULTAS ORDER BY GEDUNG.ID_FAKULTAS, NM_GEDUNG");
$smarty->assign('gedung', $gedung);

if(isset($_REQUEST['gedung'])){
	$laporan = $db->QueryToArray("SELECT * FROM RUANGAN WHERE ID_GEDUNG = '$_REQUEST[gedung]' ORDER BY NM_RUANGAN");

	foreach($laporan as $data){
	
	$tampil .=  "<tr>
					<td>$data[NM_RUANGAN]</td>
				";
	$jam = $db->QueryToArray("SELECT NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, (jammulai || ' - ' || jamakhir) as JAM_M, RUANGAN.ID_RUANGAN, JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, 
kelas_mk.ID_SEMESTER
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
									left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
JOIN (
	select jadwal_kelas.id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir, kelas_mk.id_semester,
						coalesce(j1.id_jadwal_jam,j2.id_jadwal_jam, j3.id_jadwal_jam) as id_jadwal_jam 
						from jadwal_kelas
						JOIN RUANGAN ON RUANGAN.ID_RUANGAN = JADWAL_KELAS.ID_RUANGAN
						JOIN GEDUNG ON GEDUNG.ID_GEDUNG = RUANGAN.ID_GEDUNG
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
) A ON A.ID_RUANGAN = RUANGAN.ID_RUANGAN AND A.ID_JADWAL_HARI = jadwal_hari.ID_JADWAL_HARI AND A.NM_JADWAL_HARI = jadwal_hari.NM_JADWAL_HARI
and kelas_mk.id_semester = a.id_semester and a.id_jadwal_jam = JADWAL_JAM.ID_JADWAL_JAM 
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND kelas_mk.ID_SEMESTER = '$_POST[semester]' AND jadwal_hari.NM_JADWAL_HARI = 'Senin'
									GROUP BY NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, jammulai, jamakhir, RUANGAN.ID_RUANGAN, JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, 
kelas_mk.ID_SEMESTER							
									ORDER BY NM_RUANGAN, jammulai, jamakhir");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]&semester=$_POST[semester]'>$data2[JAM_M]</a>
											</td>
										</tr>";
						}
	$tampil .=  "	</table></td>";

	$jam = $db->QueryToArray("SELECT NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, (jammulai || ' - ' || jamakhir) as JAM_M, RUANGAN.ID_RUANGAN, JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, 
kelas_mk.ID_SEMESTER
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
									left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
JOIN (
	select jadwal_kelas.id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir, kelas_mk.id_semester,
						coalesce(j1.id_jadwal_jam,j2.id_jadwal_jam, j3.id_jadwal_jam) as id_jadwal_jam 
						from jadwal_kelas
						JOIN RUANGAN ON RUANGAN.ID_RUANGAN = JADWAL_KELAS.ID_RUANGAN
						JOIN GEDUNG ON GEDUNG.ID_GEDUNG = RUANGAN.ID_GEDUNG
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
) A ON A.ID_RUANGAN = RUANGAN.ID_RUANGAN AND A.ID_JADWAL_HARI = jadwal_hari.ID_JADWAL_HARI AND A.NM_JADWAL_HARI = jadwal_hari.NM_JADWAL_HARI
and kelas_mk.id_semester = a.id_semester and a.id_jadwal_jam = JADWAL_JAM.ID_JADWAL_JAM
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND kelas_mk.ID_SEMESTER = '$_POST[semester]' AND jadwal_hari.NM_JADWAL_HARI = 'Selasa'
									GROUP BY NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, jammulai, jamakhir	, RUANGAN.ID_RUANGAN, JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, 
kelas_mk.ID_SEMESTER								
									ORDER BY NM_RUANGAN, jammulai, jamakhir");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]&semester=$_POST[semester]'>$data2[JAM_M]</a>
											</td>
										</tr>";
						}	$tampil .=  "	</table></td>";
	
	$jam = $db->QueryToArray("SELECT NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, (jammulai || ' - ' || jamakhir) as JAM_M, RUANGAN.ID_RUANGAN, JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, 
kelas_mk.ID_SEMESTER
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
									left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
JOIN (
	select jadwal_kelas.id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir, kelas_mk.id_semester,
						coalesce(j1.id_jadwal_jam,j2.id_jadwal_jam, j3.id_jadwal_jam) as id_jadwal_jam 
						from jadwal_kelas
						JOIN RUANGAN ON RUANGAN.ID_RUANGAN = JADWAL_KELAS.ID_RUANGAN
						JOIN GEDUNG ON GEDUNG.ID_GEDUNG = RUANGAN.ID_GEDUNG
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
) A ON A.ID_RUANGAN = RUANGAN.ID_RUANGAN AND A.ID_JADWAL_HARI = jadwal_hari.ID_JADWAL_HARI AND A.NM_JADWAL_HARI = jadwal_hari.NM_JADWAL_HARI
and kelas_mk.id_semester = a.id_semester and a.id_jadwal_jam = JADWAL_JAM.ID_JADWAL_JAM
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND kelas_mk.ID_SEMESTER = '$_POST[semester]' AND jadwal_hari.NM_JADWAL_HARI = 'Rabu'
									GROUP BY NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, jammulai, jamakhir, RUANGAN.ID_RUANGAN, JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, 
kelas_mk.ID_SEMESTER								
									ORDER BY NM_RUANGAN, jammulai, jamakhir");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]&semester=$_POST[semester]'>$data2[JAM_M]</a>
											</td>
										</tr>";
						}	$tampil .=  "	</table></td>";
	
	$jam = $db->QueryToArray("SELECT NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, (jammulai || ' - ' || jamakhir) as JAM_M, RUANGAN.ID_RUANGAN, JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, 
kelas_mk.ID_SEMESTER
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
									left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
JOIN (
	select jadwal_kelas.id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir, kelas_mk.id_semester,
						coalesce(j1.id_jadwal_jam,j2.id_jadwal_jam, j3.id_jadwal_jam) as id_jadwal_jam 
						from jadwal_kelas
						JOIN RUANGAN ON RUANGAN.ID_RUANGAN = JADWAL_KELAS.ID_RUANGAN
						JOIN GEDUNG ON GEDUNG.ID_GEDUNG = RUANGAN.ID_GEDUNG
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
) A ON A.ID_RUANGAN = RUANGAN.ID_RUANGAN AND A.ID_JADWAL_HARI = jadwal_hari.ID_JADWAL_HARI AND A.NM_JADWAL_HARI = jadwal_hari.NM_JADWAL_HARI
and kelas_mk.id_semester = a.id_semester and a.id_jadwal_jam = JADWAL_JAM.ID_JADWAL_JAM
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND kelas_mk.ID_SEMESTER = '$_POST[semester]' AND jadwal_hari.NM_JADWAL_HARI = 'Kamis'
									GROUP BY NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, jammulai, jamakhir	, RUANGAN.ID_RUANGAN, JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, 
kelas_mk.ID_SEMESTER								
									ORDER BY NM_RUANGAN, jammulai, jamakhir");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]&semester=$_POST[semester]'>$data2[JAM_M]</a>
											</td>
										</tr>";
						}	$tampil .=  "	</table></td>";
	
	$jam = $db->QueryToArray("SELECT NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, (jammulai || ' - ' || jamakhir) as JAM_M, RUANGAN.ID_RUANGAN, JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, 
kelas_mk.ID_SEMESTER
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
									left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
JOIN (
	select jadwal_kelas.id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir, kelas_mk.id_semester,
						coalesce(j1.id_jadwal_jam,j2.id_jadwal_jam, j3.id_jadwal_jam) as id_jadwal_jam 
						from jadwal_kelas
						JOIN RUANGAN ON RUANGAN.ID_RUANGAN = JADWAL_KELAS.ID_RUANGAN
						JOIN GEDUNG ON GEDUNG.ID_GEDUNG = RUANGAN.ID_GEDUNG
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
) A ON A.ID_RUANGAN = RUANGAN.ID_RUANGAN AND A.ID_JADWAL_HARI = jadwal_hari.ID_JADWAL_HARI AND A.NM_JADWAL_HARI = jadwal_hari.NM_JADWAL_HARI
and kelas_mk.id_semester = a.id_semester and a.id_jadwal_jam = JADWAL_JAM.ID_JADWAL_JAM
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND kelas_mk.ID_SEMESTER = '$_POST[semester]' AND jadwal_hari.NM_JADWAL_HARI = 'Jumat'
									GROUP BY NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, jammulai, jamakhir, RUANGAN.ID_RUANGAN, JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, 
kelas_mk.ID_SEMESTER									
									ORDER BY NM_RUANGAN, jammulai, jamakhir");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]&semester=$_POST[semester]'>$data2[JAM_M]</a>
											</td>
										</tr>";
						}	$tampil .=  "	</table></td>";
	
	$jam = $db->QueryToArray("SELECT NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, (jammulai || ' - ' || jamakhir) as JAM_M, RUANGAN.ID_RUANGAN, JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, 
kelas_mk.ID_SEMESTER
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
									left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
JOIN (
	select jadwal_kelas.id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir, kelas_mk.id_semester,
						coalesce(j1.id_jadwal_jam,j2.id_jadwal_jam, j3.id_jadwal_jam) as id_jadwal_jam 
						from jadwal_kelas
						JOIN RUANGAN ON RUANGAN.ID_RUANGAN = JADWAL_KELAS.ID_RUANGAN
						JOIN GEDUNG ON GEDUNG.ID_GEDUNG = RUANGAN.ID_GEDUNG
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
) A ON A.ID_RUANGAN = RUANGAN.ID_RUANGAN AND A.ID_JADWAL_HARI = jadwal_hari.ID_JADWAL_HARI AND A.NM_JADWAL_HARI = jadwal_hari.NM_JADWAL_HARI
and kelas_mk.id_semester = a.id_semester and a.id_jadwal_jam = JADWAL_JAM.ID_JADWAL_JAM
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND kelas_mk.ID_SEMESTER = '$_POST[semester]' AND jadwal_hari.NM_JADWAL_HARI = 'Sabtu'
									GROUP BY NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, jammulai, jamakhir, RUANGAN.ID_RUANGAN, JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, 
kelas_mk.ID_SEMESTER									
									ORDER BY NM_RUANGAN, jammulai, jamakhir");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]&semester=$_POST[semester]'>$data2[JAM_M]</a>
											</td>
										</tr>";
						}	$tampil .=  "	</table></td>";
	
	
	$jam = $db->QueryToArray("SELECT NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, (jammulai || ' - ' || jamakhir) as JAM_M, RUANGAN.ID_RUANGAN, JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, 
kelas_mk.ID_SEMESTER
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
									left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
JOIN (
	select jadwal_kelas.id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir, kelas_mk.id_semester,
						coalesce(j1.id_jadwal_jam,j2.id_jadwal_jam, j3.id_jadwal_jam) as id_jadwal_jam 
						from jadwal_kelas
						JOIN RUANGAN ON RUANGAN.ID_RUANGAN = JADWAL_KELAS.ID_RUANGAN
						JOIN GEDUNG ON GEDUNG.ID_GEDUNG = RUANGAN.ID_GEDUNG
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
) A ON A.ID_RUANGAN = RUANGAN.ID_RUANGAN AND A.ID_JADWAL_HARI = jadwal_hari.ID_JADWAL_HARI AND A.NM_JADWAL_HARI = jadwal_hari.NM_JADWAL_HARI
and kelas_mk.id_semester = a.id_semester and a.id_jadwal_jam = JADWAL_JAM.ID_JADWAL_JAM
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND kelas_mk.ID_SEMESTER = '$_POST[semester]' AND jadwal_hari.NM_JADWAL_HARI = 'Minggu'
									GROUP BY NM_RUANGAN, jadwal_hari.NM_JADWAL_HARI, jammulai, jamakhir	, RUANGAN.ID_RUANGAN, JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, 
kelas_mk.ID_SEMESTER								
									ORDER BY NM_RUANGAN, jammulai, jamakhir");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]&semester=$_POST[semester]'>$data2[JAM_M]</a>
											</td>
										</tr>";
						}	$tampil .=  "	</table></td>";
					
	$tampil .=  "</tr>";
	/*
	$tampil .=  "<tr>
					<td>$data[NM_RUANGAN]</td>
				";
				
		$jam = $db->QueryToArray("SELECT JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
									left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND kelas_mk.ID_SEMESTER = '$_POST[semester]' AND NM_JADWAL_HARI = 'Senin'
									GROUP BY JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI									
									ORDER BY JAM_MULAI, MENIT_MULAI");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]&semester=$_POST[semester]'>$data2[JAM_MULAI]:$data2[MENIT_MULAI] - $data2[JAM_SELESAI]:$data2[MENIT_SELESAI]</a>
											</td>
										</tr>";
						}
	$tampil .=  "	</table></td>";

	$jam = $db->QueryToArray("SELECT JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
									left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND kelas_mk.ID_SEMESTER = '$_POST[semester]' AND NM_JADWAL_HARI = 'Selasa'
									GROUP BY JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI									
									ORDER BY JAM_MULAI, MENIT_MULAI");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]&semester=$_POST[semester]'>$data2[JAM_MULAI]:$data2[MENIT_MULAI] - $data2[JAM_SELESAI]:$data2[MENIT_SELESAI]</a>
											</td>
										</tr>";
						}
	$tampil .=  "	</table></td>";
	
	$jam = $db->QueryToArray("SELECT JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
									left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND kelas_mk.ID_SEMESTER = '$_POST[semester]' AND NM_JADWAL_HARI = 'Rabu'
									GROUP BY JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI									
									ORDER BY JAM_MULAI, MENIT_MULAI");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]&semester=$_POST[semester]'>$data2[JAM_MULAI]:$data2[MENIT_MULAI] - $data2[JAM_SELESAI]:$data2[MENIT_SELESAI]</a>
											</td>
										</tr>";
						}
	$tampil .=  "	</table></td>";
	
	$jam = $db->QueryToArray("SELECT JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
									left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND kelas_mk.ID_SEMESTER = '$_POST[semester]' AND NM_JADWAL_HARI = 'Kamis'
									GROUP BY JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI									
									ORDER BY JAM_MULAI, MENIT_MULAI");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]&semester=$_POST[semester]'>$data2[JAM_MULAI]:$data2[MENIT_MULAI] - $data2[JAM_SELESAI]:$data2[MENIT_SELESAI]</a>
											</td>
										</tr>";
						}
	$tampil .=  "	</table></td>";
	
	$jam = $db->QueryToArray("SELECT JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
									left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND kelas_mk.ID_SEMESTER = '$_POST[semester]' AND NM_JADWAL_HARI = 'Jumat'
									GROUP BY JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI									
									ORDER BY JAM_MULAI, MENIT_MULAI");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]&semester=$_POST[semester]'>$data2[JAM_MULAI]:$data2[MENIT_MULAI] - $data2[JAM_SELESAI]:$data2[MENIT_SELESAI]</a>
											</td>
										</tr>";
						}
	$tampil .=  "	</table></td>";
	
	$jam = $db->QueryToArray("SELECT JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
									left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND kelas_mk.ID_SEMESTER = '$_POST[semester]' AND NM_JADWAL_HARI = 'Sabtu'
									GROUP BY JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI									
									ORDER BY JAM_MULAI, MENIT_MULAI");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]&semester=$_POST[semester]'>$data2[JAM_MULAI]:$data2[MENIT_MULAI] - $data2[JAM_SELESAI]:$data2[MENIT_SELESAI]</a>
											</td>
										</tr>";
						}
	$tampil .=  "	</table></td>";
	
	
	$jam = $db->QueryToArray("SELECT JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI
									FROM RUANGAN
									JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_RUANGAN = RUANGAN.ID_RUANGAN
									JOIN JADWAL_HARI ON JADWAL_HARI.ID_JADWAL_HARI = JADWAL_KELAS.ID_JADWAL_HARI
									JOIN JADWAL_JAM ON JADWAL_JAM.ID_JADWAL_JAM = JADWAL_KELAS.ID_JADWAL_JAM
									left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
									WHERE RUANGAN.ID_RUANGAN = '$data[ID_RUANGAN]' AND kelas_mk.ID_SEMESTER = '$_POST[semester]' AND NM_JADWAL_HARI = 'Minggu'
									GROUP BY JADWAL_HARI.ID_JADWAL_HARI, JADWAL_JAM.ID_JADWAL_JAM, RUANGAN.ID_RUANGAN, NM_RUANGAN, NM_JADWAL_HARI, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI									
									ORDER BY JAM_MULAI, MENIT_MULAI");
				
	$tampil .=  "	<td><table>";
						foreach($jam as $data2){
							$tampil .=  "<tr>
											<td style='border:none'>
												<a href='laporan-ruangan.php?ruangan=$data2[ID_RUANGAN]&hari=$data2[ID_JADWAL_HARI]&jam=$data2[ID_JADWAL_JAM]&semester=$_POST[semester]'>$data2[JAM_MULAI]:$data2[MENIT_MULAI] - $data2[JAM_SELESAI]:$data2[MENIT_SELESAI]</a>
											</td>
										</tr>";
						}
	$tampil .=  "	</table></td>";
					
	$tampil .=  "</tr>";
	*/}

$smarty->assign('tampil', $tampil);

}
}
$smarty->display("laporan/laporan-ruangan.tpl");
?>
