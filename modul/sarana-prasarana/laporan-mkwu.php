<?PHP
include('config.php');

$semester = $db->QueryToArray("SELECT * FROM SEMESTER WHERE NM_SEMESTER = 'Ganjil' or NM_SEMESTER = 'Genap' ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER");
$smarty->assign('semester', $semester);

if(isset($_POST['semester']) and $_POST['semester'] != ''){
$fakultas = $db->QueryToArray("SELECT * FROM FAKULTAS WHERE ID_FAKULTAS NOT IN (9) ORDER BY ID_FAKULTAS");

$tampil .= '<table>
<tr>
	<th>Fakultas</th>
	<th>Islam</th>
	<th>Kristen</th>
	<th>Katolik</th>
	<th>Hindu</th>
	<th>Budha</th>
	<th>Konghucu</th>
	<th>PPKN</th>
	<th>Filsafat Ilmu</th>
	<th>IAD</th>
	<th>ISBD</th>
	<th>Bahasa Inggris</th>
	<th>Bahasa Indonesia</th>
</tr>';
foreach($fakultas as $data){
$tampil .= '
<tr>
	<td>'.$data['NM_FAKULTAS'].'</td>';
	
$islam = $db->QueryToArray("select kls_terisi, nm_mata_kuliah, kd_mata_kuliah, s1.id_ruangan,nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_fakultas,
						id_jadwal_hari,nm_jadwal_hari,jammulai,jamakhir from 
						(select nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_ruangan,gedung.id_fakultas from ruangan
						left join gedung on ruangan.id_gedung=gedung.id_gedung
						left join fakultas on gedung.id_fakultas=fakultas.id_fakultas
						order by gedung.id_fakultas)s1
						left join (
						select (select count(status_apv_pengambilan_mk) from pengambilan_mk
						where kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and status_apv_pengambilan_mk=1)as kls_terisi, 
						nm_mata_kuliah, kd_mata_kuliah, id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
						from jadwal_kelas
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join KURIKULUM_MK on kurikulum_mk.ID_KURIKULUM_MK = kelas_mk.id_kurikulum_mk
						left join MATA_KULIAH on MATA_KULIAH.ID_MATA_KULIAH = KURIKULUM_MK.ID_MATA_KULIAH
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
						where id_semester = '$_POST[semester]')s2 on s1.id_ruangan=s2.id_ruangan
						where id_fakultas=".$data['ID_FAKULTAS']." and upper(NM_MATA_KULIAH) LIKE ('%AGAMA ISLAM%') and kls_terisi != 0
						order by id_fakultas,nm_ruangan,jammulai");

$tampil .= '<td>
		<table>
			<tr>
				<th>Kode MA</th>
				<th>Hari</th>
				<th>Ruang</th>
				<th>Jam</th>
			</tr>';
						
		foreach($islam as $data){
			$tampil .='
				<tr>
					<td>'.$data['KD_MATA_KULIAH'].'</td>
					<td>'.$data['NM_JADWAL_HARI'].'</td>
					<td>'.$data['NM_RUANGAN'].'</td>
					<td>'.$data['JAMMULAI'].' - '.$data['JAMMULAI'].'</td>
				</tr>
			';			 
		}
$tampil .= '</table></td>
	<td><table>
			<tr>
				<th>Kode MA</th>
				<th>Hari</th>
				<th>Ruang</th>
				<th>Jam</th>
			</tr>';
	
$protestan = $db->QueryToArray("select kls_terisi, nm_mata_kuliah, kd_mata_kuliah, s1.id_ruangan,nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_fakultas,
						id_jadwal_hari,nm_jadwal_hari,jammulai,jamakhir from 
						(select nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_ruangan,gedung.id_fakultas from ruangan
						left join gedung on ruangan.id_gedung=gedung.id_gedung
						left join fakultas on gedung.id_fakultas=fakultas.id_fakultas
						order by gedung.id_fakultas)s1
						left join (
						select (select count(status_apv_pengambilan_mk) from pengambilan_mk
						where kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and status_apv_pengambilan_mk=1)as kls_terisi, 
						nm_mata_kuliah, kd_mata_kuliah, id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
						from jadwal_kelas
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join KURIKULUM_MK on kurikulum_mk.ID_KURIKULUM_MK = kelas_mk.id_kurikulum_mk
						left join MATA_KULIAH on MATA_KULIAH.ID_MATA_KULIAH = KURIKULUM_MK.ID_MATA_KULIAH
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
						where id_semester = '$_POST[semester]')s2 on s1.id_ruangan=s2.id_ruangan
						where id_fakultas=".$data['ID_FAKULTAS']." and upper(NM_MATA_KULIAH) LIKE ('%PROTESTAN%') and kls_terisi != 0
						order by id_fakultas,nm_ruangan,jammulai");
						
						
	foreach($protestan as $data){
			$tampil .='
				<tr>
					<td>'.$data['KD_MATA_KULIAH'].'</td>
					<td>'.$data['NM_JADWAL_HARI'].'</td>
					<td>'.$data['NM_RUANGAN'].'</td>
					<td>'.$data['JAMMULAI'].' - '.$data['JAMMULAI'].'</td>
				</tr>
			';			 
		}
$tampil .= '</table>
	</td>
	<td><table>
			<tr>
				<th>Kode MA</th>
				<th>Hari</th>
				<th>Ruang</th>
				<th>Jam</th>
			</tr>';
			
$katolik = $db->QueryToArray("select kls_terisi, nm_mata_kuliah, kd_mata_kuliah, s1.id_ruangan,nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_fakultas,
						id_jadwal_hari,nm_jadwal_hari,jammulai,jamakhir from 
						(select nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_ruangan,gedung.id_fakultas from ruangan
						left join gedung on ruangan.id_gedung=gedung.id_gedung
						left join fakultas on gedung.id_fakultas=fakultas.id_fakultas
						order by gedung.id_fakultas)s1
						left join (
						select (select count(status_apv_pengambilan_mk) from pengambilan_mk
						where kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and status_apv_pengambilan_mk=1)as kls_terisi, 
						nm_mata_kuliah, kd_mata_kuliah, id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
						from jadwal_kelas
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join KURIKULUM_MK on kurikulum_mk.ID_KURIKULUM_MK = kelas_mk.id_kurikulum_mk
						left join MATA_KULIAH on MATA_KULIAH.ID_MATA_KULIAH = KURIKULUM_MK.ID_MATA_KULIAH
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
						where id_semester = '$_POST[semester]')s2 on s1.id_ruangan=s2.id_ruangan
						where id_fakultas=".$data['ID_FAKULTAS']." and upper(NM_MATA_KULIAH) LIKE ('%KATOLIK%') and kls_terisi != 0
						order by id_fakultas,nm_ruangan,jammulai");

		foreach($katolik as $data){
			$tampil .='
				<tr>
					<td>'.$data['KD_MATA_KULIAH'].'</td>
					<td>'.$data['NM_JADWAL_HARI'].'</td>
					<td>'.$data['NM_RUANGAN'].'</td>
					<td>'.$data['JAMMULAI'].' - '.$data['JAMMULAI'].'</td>
				</tr>
			';			 
		}
					
$tampil .= '</table>
	</td>
	<td><table>
			<tr>
				<th>Kode MA</th>
				<th>Hari</th>
				<th>Ruang</th>
				<th>Jam</th>
			</tr>';
$hindu = $db->QueryToArray("select kls_terisi, nm_mata_kuliah, kd_mata_kuliah, s1.id_ruangan,nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_fakultas,
						id_jadwal_hari,nm_jadwal_hari,jammulai,jamakhir from 
						(select nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_ruangan,gedung.id_fakultas from ruangan
						left join gedung on ruangan.id_gedung=gedung.id_gedung
						left join fakultas on gedung.id_fakultas=fakultas.id_fakultas
						order by gedung.id_fakultas)s1
						left join (
						select (select count(status_apv_pengambilan_mk) from pengambilan_mk
						where kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and status_apv_pengambilan_mk=1)as kls_terisi, 
						nm_mata_kuliah, kd_mata_kuliah, id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
						from jadwal_kelas
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join KURIKULUM_MK on kurikulum_mk.ID_KURIKULUM_MK = kelas_mk.id_kurikulum_mk
						left join MATA_KULIAH on MATA_KULIAH.ID_MATA_KULIAH = KURIKULUM_MK.ID_MATA_KULIAH
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
						where id_semester = '$_POST[semester]')s2 on s1.id_ruangan=s2.id_ruangan
						where id_fakultas=".$data['ID_FAKULTAS']." and upper(NM_MATA_KULIAH) LIKE ('%HINDU%') and kls_terisi != 0
						order by id_fakultas,nm_ruangan,jammulai");

		foreach($hindu as $data){
			$tampil .='
				<tr>
					<td>'.$data['KD_MATA_KULIAH'].'</td>
					<td>'.$data['NM_JADWAL_HARI'].'</td>
					<td>'.$data['NM_RUANGAN'].'</td>
					<td>'.$data['JAMMULAI'].' - '.$data['JAMMULAI'].'</td>
				</tr>
			';			 
		}

$tampil .= '</table>
	</td>
	<td><table>
			<tr>
				<th>Kode MA</th>
				<th>Hari</th>
				<th>Ruang</th>
				<th>Jam</th>
			</tr>';
			
$budha = $db->QueryToArray("select kls_terisi, nm_mata_kuliah, kd_mata_kuliah, s1.id_ruangan,nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_fakultas,
						id_jadwal_hari,nm_jadwal_hari,jammulai,jamakhir from 
						(select nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_ruangan,gedung.id_fakultas from ruangan
						left join gedung on ruangan.id_gedung=gedung.id_gedung
						left join fakultas on gedung.id_fakultas=fakultas.id_fakultas
						order by gedung.id_fakultas)s1
						left join (
						select (select count(status_apv_pengambilan_mk) from pengambilan_mk
						where kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and status_apv_pengambilan_mk=1)as kls_terisi, 
						nm_mata_kuliah, kd_mata_kuliah, id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
						from jadwal_kelas
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join KURIKULUM_MK on kurikulum_mk.ID_KURIKULUM_MK = kelas_mk.id_kurikulum_mk
						left join MATA_KULIAH on MATA_KULIAH.ID_MATA_KULIAH = KURIKULUM_MK.ID_MATA_KULIAH
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
						where id_semester = '$_POST[semester]')s2 on s1.id_ruangan=s2.id_ruangan
						where id_fakultas=".$data['ID_FAKULTAS']." and upper(NM_MATA_KULIAH) LIKE ('%BUDHA%') and kls_terisi != 0
						order by id_fakultas,nm_ruangan,jammulai");

		foreach($budha as $data){
			$tampil .='
				<tr>
					<td>'.$data['KD_MATA_KULIAH'].'</td>
					<td>'.$data['NM_JADWAL_HARI'].'</td>
					<td>'.$data['NM_RUANGAN'].'</td>
					<td>'.$data['JAMMULAI'].' - '.$data['JAMMULAI'].'</td>
				</tr>
			';			 
		}

$tampil .='</table>
	</td>
	<td><table>
			<tr>
				<th>Kode MA</th>
				<th>Hari</th>
				<th>Ruang</th>
				<th>Jam</th>
			</tr>';
$konghucu = $db->QueryToArray("select kls_terisi, nm_mata_kuliah, kd_mata_kuliah, s1.id_ruangan,nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_fakultas,
						id_jadwal_hari,nm_jadwal_hari,jammulai,jamakhir from 
						(select nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_ruangan,gedung.id_fakultas from ruangan
						left join gedung on ruangan.id_gedung=gedung.id_gedung
						left join fakultas on gedung.id_fakultas=fakultas.id_fakultas
						order by gedung.id_fakultas)s1
						left join (
						select (select count(status_apv_pengambilan_mk) from pengambilan_mk
						where kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and status_apv_pengambilan_mk=1)as kls_terisi, 
						nm_mata_kuliah, kd_mata_kuliah, id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
						from jadwal_kelas
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join KURIKULUM_MK on kurikulum_mk.ID_KURIKULUM_MK = kelas_mk.id_kurikulum_mk
						left join MATA_KULIAH on MATA_KULIAH.ID_MATA_KULIAH = KURIKULUM_MK.ID_MATA_KULIAH
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
						where id_semester = '$_POST[semester]')s2 on s1.id_ruangan=s2.id_ruangan
						where id_fakultas=".$data['ID_FAKULTAS']." and upper(NM_MATA_KULIAH) LIKE ('%KONGHUCU%') and kls_terisi != 0
						order by id_fakultas,nm_ruangan,jammulai");

		foreach($konghucu as $data){
			$tampil .='
				<tr>
					<td>'.$data['KD_MATA_KULIAH'].'</td>
					<td>'.$data['NM_JADWAL_HARI'].'</td>
					<td>'.$data['NM_RUANGAN'].'</td>
					<td>'.$data['JAMMULAI'].' - '.$data['JAMMULAI'].'</td>
				</tr>
			';			 
		}

$tampil .='</table>
	</td>
	<td><table>
			<tr>
				<th>Kode MA</th>
				<th>Hari</th>
				<th>Ruang</th>
				<th>Jam</th>
			</tr>';
$ppkn = $db->QueryToArray("select kls_terisi, nm_mata_kuliah, kd_mata_kuliah, s1.id_ruangan,nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_fakultas,
						id_jadwal_hari,nm_jadwal_hari,jammulai,jamakhir from 
						(select nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_ruangan,gedung.id_fakultas from ruangan
						left join gedung on ruangan.id_gedung=gedung.id_gedung
						left join fakultas on gedung.id_fakultas=fakultas.id_fakultas
						order by gedung.id_fakultas)s1
						left join (
						select (select count(status_apv_pengambilan_mk) from pengambilan_mk
						where kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and status_apv_pengambilan_mk=1)as kls_terisi, 
						nm_mata_kuliah, kd_mata_kuliah, id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
						from jadwal_kelas
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join KURIKULUM_MK on kurikulum_mk.ID_KURIKULUM_MK = kelas_mk.id_kurikulum_mk
						left join MATA_KULIAH on MATA_KULIAH.ID_MATA_KULIAH = KURIKULUM_MK.ID_MATA_KULIAH
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
						where id_semester = '$_POST[semester]')s2 on s1.id_ruangan=s2.id_ruangan
						where id_fakultas=".$data['ID_FAKULTAS']." and upper(NM_MATA_KULIAH) LIKE ('%PPKN%') and kls_terisi != 0
						order by id_fakultas,nm_ruangan,jammulai");

		foreach($ppkn as $data){
			$tampil .='
				<tr>
					<td>'.$data['KD_MATA_KULIAH'].'</td>
					<td>'.$data['NM_JADWAL_HARI'].'</td>
					<td>'.$data['NM_RUANGAN'].'</td>
					<td>'.$data['JAMMULAI'].' - '.$data['JAMMULAI'].'</td>
				</tr>
			';			 
		}

$tampil.='</table>
	</td>
	<td><table>
			<tr>
				<th>Kode MA</th>
				<th>Hari</th>
				<th>Ruang</th>
				<th>Jam</th>
			</tr>';
$filsafat = $db->QueryToArray("select kls_terisi, nm_mata_kuliah, kd_mata_kuliah, s1.id_ruangan,nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_fakultas,
						id_jadwal_hari,nm_jadwal_hari,jammulai,jamakhir from 
						(select nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_ruangan,gedung.id_fakultas from ruangan
						left join gedung on ruangan.id_gedung=gedung.id_gedung
						left join fakultas on gedung.id_fakultas=fakultas.id_fakultas
						order by gedung.id_fakultas)s1
						left join (
						select (select count(status_apv_pengambilan_mk) from pengambilan_mk
						where kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and status_apv_pengambilan_mk=1)as kls_terisi, 
						nm_mata_kuliah, kd_mata_kuliah, id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
						from jadwal_kelas
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join KURIKULUM_MK on kurikulum_mk.ID_KURIKULUM_MK = kelas_mk.id_kurikulum_mk
						left join MATA_KULIAH on MATA_KULIAH.ID_MATA_KULIAH = KURIKULUM_MK.ID_MATA_KULIAH
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
						where id_semester = '$_POST[semester]')s2 on s1.id_ruangan=s2.id_ruangan
						where id_fakultas=".$data['ID_FAKULTAS']." and upper(NM_MATA_KULIAH) LIKE ('%FILSAFAT ILMU%') and kls_terisi != 0
						order by id_fakultas,nm_ruangan,jammulai");

		foreach($filsafat as $data){
			$tampil .='
				<tr>
					<td>'.$data['KD_MATA_KULIAH'].'</td>
					<td>'.$data['NM_JADWAL_HARI'].'</td>
					<td>'.$data['NM_RUANGAN'].'</td>
					<td>'.$data['JAMMULAI'].' - '.$data['JAMMULAI'].'</td>
				</tr>
			';			 
		}

$tampil.='</table>
	</td>
	<td><table>
			<tr>
				<th>Kode MA</th>
				<th>Hari</th>
				<th>Ruang</th>
				<th>Jam</th>
			</tr>';
$iad = $db->QueryToArray("select kls_terisi, nm_mata_kuliah, kd_mata_kuliah, s1.id_ruangan,nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_fakultas,
						id_jadwal_hari,nm_jadwal_hari,jammulai,jamakhir from 
						(select nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_ruangan,gedung.id_fakultas from ruangan
						left join gedung on ruangan.id_gedung=gedung.id_gedung
						left join fakultas on gedung.id_fakultas=fakultas.id_fakultas
						order by gedung.id_fakultas)s1
						left join (
						select (select count(status_apv_pengambilan_mk) from pengambilan_mk
						where kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and status_apv_pengambilan_mk=1)as kls_terisi, 
						nm_mata_kuliah, kd_mata_kuliah, id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
						from jadwal_kelas
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join KURIKULUM_MK on kurikulum_mk.ID_KURIKULUM_MK = kelas_mk.id_kurikulum_mk
						left join MATA_KULIAH on MATA_KULIAH.ID_MATA_KULIAH = KURIKULUM_MK.ID_MATA_KULIAH
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
						where id_semester = '$_POST[semester]')s2 on s1.id_ruangan=s2.id_ruangan
						where id_fakultas=".$data['ID_FAKULTAS']." and upper(NM_MATA_KULIAH) LIKE ('%ILMU ALAMIAH DASAR%') and kls_terisi != 0
						order by id_fakultas,nm_ruangan,jammulai");

		foreach($iad as $data){
			$tampil .='
				<tr>
					<td>'.$data['KD_MATA_KULIAH'].'</td>
					<td>'.$data['NM_JADWAL_HARI'].'</td>
					<td>'.$data['NM_RUANGAN'].'</td>
					<td>'.$data['JAMMULAI'].' - '.$data['JAMMULAI'].'</td>
				</tr>
			';			 
		}
$tampil.='</table>
	</td>
	<td><table>
			<tr>
				<th>Kode MA</th>
				<th>Hari</th>
				<th>Ruang</th>
				<th>Jam</th>
			</tr>';


$isbd = $db->QueryToArray("select kls_terisi, nm_mata_kuliah, kd_mata_kuliah, s1.id_ruangan,nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_fakultas,
						id_jadwal_hari,nm_jadwal_hari,jammulai,jamakhir from 
						(select nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_ruangan,gedung.id_fakultas from ruangan
						left join gedung on ruangan.id_gedung=gedung.id_gedung
						left join fakultas on gedung.id_fakultas=fakultas.id_fakultas
						order by gedung.id_fakultas)s1
						left join (
						select (select count(status_apv_pengambilan_mk) from pengambilan_mk
						where kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and status_apv_pengambilan_mk=1)as kls_terisi, 
						nm_mata_kuliah, kd_mata_kuliah, id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
						from jadwal_kelas
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join KURIKULUM_MK on kurikulum_mk.ID_KURIKULUM_MK = kelas_mk.id_kurikulum_mk
						left join MATA_KULIAH on MATA_KULIAH.ID_MATA_KULIAH = KURIKULUM_MK.ID_MATA_KULIAH
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
						where id_semester = '$_POST[semester]')s2 on s1.id_ruangan=s2.id_ruangan
						where id_fakultas=".$data['ID_FAKULTAS']." and (upper(NM_MATA_KULIAH) LIKE ('%ILMU SOSIAL BUDAYA DASAR%') OR upper(NM_MATA_KULIAH) LIKE ('%ILMU SOSIAL DAN BUDAYA DASAR%')) and kls_terisi != 0
						order by id_fakultas,nm_ruangan,jammulai");

		foreach($isbd as $data){
			$tampil .='
				<tr>
					<td>'.$data['KD_MATA_KULIAH'].'</td>
					<td>'.$data['NM_JADWAL_HARI'].'</td>
					<td>'.$data['NM_RUANGAN'].'</td>
					<td>'.$data['JAMMULAI'].' - '.$data['JAMMULAI'].'</td>
				</tr>
			';			 
		}
$tampil.='
	</table>
	</td>
	<td><table>
			<tr>
				<th>Kode MA</th>
				<th>Hari</th>
				<th>Ruang</th>
				<th>Jam</th>
			</tr>';
			
$inggris = $db->QueryToArray("select kls_terisi, nm_mata_kuliah, kd_mata_kuliah, s1.id_ruangan,nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_fakultas,
						id_jadwal_hari,nm_jadwal_hari,jammulai,jamakhir from 
						(select nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_ruangan,gedung.id_fakultas from ruangan
						left join gedung on ruangan.id_gedung=gedung.id_gedung
						left join fakultas on gedung.id_fakultas=fakultas.id_fakultas
						order by gedung.id_fakultas)s1
						left join (
						select (select count(status_apv_pengambilan_mk) from pengambilan_mk
						where kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and status_apv_pengambilan_mk=1)as kls_terisi, 
						nm_mata_kuliah, kd_mata_kuliah, id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
						from jadwal_kelas
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join KURIKULUM_MK on kurikulum_mk.ID_KURIKULUM_MK = kelas_mk.id_kurikulum_mk
						left join MATA_KULIAH on MATA_KULIAH.ID_MATA_KULIAH = KURIKULUM_MK.ID_MATA_KULIAH
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
						where id_semester = '$_POST[semester]')s2 on s1.id_ruangan=s2.id_ruangan
						where id_fakultas=".$data['ID_FAKULTAS']." and upper(NM_MATA_KULIAH) LIKE ('%BAHASA INGGRIS%') and kls_terisi != 0
						order by id_fakultas,nm_ruangan,jammulai");

		foreach($inggris as $data){
			$tampil .='
				<tr>
					<td>'.$data['KD_MATA_KULIAH'].'</td>
					<td>'.$data['NM_JADWAL_HARI'].'</td>
					<td>'.$data['NM_RUANGAN'].'</td>
					<td>'.$data['JAMMULAI'].' - '.$data['JAMMULAI'].'</td>
				</tr>
			';			 
		}

$tampil .='</table>
	</td>
	<td><table>
			<tr>
				<th>Kode MA</th>
				<th>Hari</th>
				<th>Ruang</th>
				<th>Jam</th>
			</tr>';
			
$indonesia = $db->QueryToArray("select kls_terisi, nm_mata_kuliah, kd_mata_kuliah, s1.id_ruangan,nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_fakultas,
						id_jadwal_hari,nm_jadwal_hari,jammulai,jamakhir from 
						(select nm_ruangan,kapasitas_ruangan,tipe_ruangan,nm_gedung,nm_fakultas,id_ruangan,gedung.id_fakultas from ruangan
						left join gedung on ruangan.id_gedung=gedung.id_gedung
						left join fakultas on gedung.id_fakultas=fakultas.id_fakultas
						order by gedung.id_fakultas)s1
						left join (
						select (select count(status_apv_pengambilan_mk) from pengambilan_mk
						where kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and status_apv_pengambilan_mk=1)as kls_terisi, 
						nm_mata_kuliah, kd_mata_kuliah, id_ruangan,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
						coalesce(j1.jam_mulai,j2.jam_mulai)||':'||coalesce(j1.menit_mulai,j2.menit_mulai) as jammulai,
						coalesce(j1.jam_selesai,j3.jam_selesai)||':'||coalesce(j1.menit_selesai,j3.menit_selesai) as jamakhir
						from jadwal_kelas
						left join kelas_mk on jadwal_kelas.id_kelas_mk=kelas_mk.id_kelas_mk
						left join KURIKULUM_MK on kurikulum_mk.ID_KURIKULUM_MK = kelas_mk.id_kurikulum_mk
						left join MATA_KULIAH on MATA_KULIAH.ID_MATA_KULIAH = KURIKULUM_MK.ID_MATA_KULIAH
						left join jadwal_jam j1 on jadwal_kelas.id_jadwal_jam=j1.id_jadwal_jam
						left join jadwal_jam j2 on jadwal_kelas.id_jadwal_jam_mulai=j2.id_jadwal_jam
						left join jadwal_jam j3 on jadwal_kelas.id_jadwal_jam_selesai=j3.id_jadwal_jam
						left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
						where id_semester = '$_POST[semester]')s2 on s1.id_ruangan=s2.id_ruangan
						where id_fakultas=".$data['ID_FAKULTAS']." and upper(NM_MATA_KULIAH) LIKE ('%BAHASA INDONESIA%') and kls_terisi != 0
						order by id_fakultas,nm_ruangan,jammulai");

		foreach($indonesia as $data){
			$tampil .='
				<tr>
					<td>'.$data['KD_MATA_KULIAH'].'</td>
					<td>'.$data['NM_JADWAL_HARI'].'</td>
					<td>'.$data['NM_RUANGAN'].'</td>
					<td>'.$data['JAMMULAI'].' - '.$data['JAMMULAI'].'</td>
				</tr>
			';			 
		}

$tampil.='</table>
	</td>
</tr>';
}
$tampil .= '</table>';
}

$smarty->assign('tampil', $tampil);
$smarty->display("laporan/laporan-mkwu.tpl");
?>