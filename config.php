<?php
// Composer Autoload
require 'vendor/autoload.php';
// Sistem Langitan Constant
require 'includes/constant.php';

// Sistem Langitan Class Autoload
spl_autoload_register(function ($class) {
    $classFileName = __DIR__.DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.$class.'.class.php';
    if (file_exists($classFileName)) {
        require $classFileName;
    } else {
        return true;
    }
});

// Load .env configuration
$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

// Set Timezone WIB
date_default_timezone_set('Asia/Jakarta');

// Set Lokal Indonesia
setlocale(LC_ALL, 'id_ID');

// Application Logic Class
require 'includes/Akademik.class.php';
require 'includes/Mahasiswa.class.php';

$db = new MyOracle();
$PT = new PerguruanTinggi($db);
$user = new User($db, $PT);

// Memastikan mempunyai id perguruan tinggi
if ($PT->is_exist == false) {
    trigger_error($_SERVER['HTTP_HOST'] . ' Perguruan Tinggi belum di set.', E_USER_ERROR);
    exit();
}

$id_pt_user = $user->ID_PERGURUAN_TINGGI;

// Inisialisasi smarty
$smarty = new Smarty();

// Disable cache untuk development
if (getenv('APP_ENV') == 'local' || getenv('APP_ENV') == 'dev') {
    $smarty->caching = Smarty::CACHING_OFF;
}

$smarty->setTemplateDir('gui');
$smarty->setCompileDir('gui_c');

$smarty->assign("id_pt", $PT->id_perguruan_tinggi);
$smarty->assign("nama_pt", $PT->nama_pt);
$smarty->assign("base_url", $PT->base_url);
$smarty->assign("nama_singkat", $PT->nama_singkat);
$smarty->assign("alamat", $PT->alamat);
$smarty->assign("kota_pt", $PT->kota_pt);
$smarty->assign("web_email_pt", $PT->web_email);
$smarty->assign("web_elearning", $PT->web_elearning);
$smarty->assign("web_pmb", $PT->web_pmb);
$smarty->assign("web_pt", $PT->web_pt);

$nama_singkat = $PT->nama_singkat;
$nama_pt = $PT->nama_pt;
$base_url = $PT->base_url;
$alamat = $PT->alamat;
$kota_pt = $PT->kota_pt;
$telp_pt = $PT->telp_pt;
$fax_pt = $PT->fax_pt;
$web_email_pt = $PT->web_email;
$web_pt = $PT->web_pt;
$web_elearning = $PT->web_elearning;
$web_pmb = $PT->web_pmb;
$web_pt = $PT->web_pt;
$kd_pos_pt = $PT->kode_pos;

$nama_pt_kapital = strtoupper($PT->nama_pt);

$request_method = $_SERVER['REQUEST_METHOD'];

/*  Tambahan set env variable */
putenv("ID_PT=$id_pt_user");

/**
 * Fungsi untuk membaca sebuah file
 * @param type $filename
 * @return type
 */
function load_file($filename)
{
    $handle = fopen($filename, 'r');
    $content = fread($handle, filesize($filename));
    fclose($handle);

    return $content;
}

/**
 * Fungsi untuk mengambil nilai dari $_GET
 * @param type $name
 * @param type $default_value
 * @return type
 */
function get($name, $default_value = '')
{
    return isset($_GET[$name]) ? $_GET[$name] : $default_value;
}

/**
 * Fungsi untuk mengambil nilai dari $_REQUEST
 * @param type $name
 * @param type $default_value
 * @return type
 */
function request($name, $default_value = '')
{
    return isset($_REQUEST[$name]) ? $_REQUEST[$name] : $default_value;
}

/**
 * Fungsi untuk mengambil nilai dari $_POST
 * @param type $name
 * @param type $default_value
 * @return type
 */

function post($name, $default_value = '')
{
    return isset($_POST[$name]) ? $_POST[$name] : $default_value;
}

/**
 * Fungsi untuk mengambil nama file
 */
function page_name()
{
    return substr($_SERVER["SCRIPT_NAME"], strrpos($_SERVER["SCRIPT_NAME"], "/") + 1);
}
