<!DOCTYPE html>
<html lang="id">
	<head>
		<title>{block name='title'}{/block}Pendaftaran Online Mahasiswa Baru {$nama_pt}</title>
		<meta name="description" content="Pendaftaran online mahasiswa baru Universitas Maarif Hasyim Latief" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap -->
		<link href="{base_url('assets/css/bootstrap.min.css')}" rel="stylesheet">
		
		<style type="text/css">
			body { padding-top: 40px; }
			.navbar-inverse {
			    background-color: #130;
			    border-color: #1302;
			}
		</style>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		{block name='head'}{/block}
	</head>
	
	<body>
		<div class="navbar navbar-default navbar-fixed-top navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<!-- <a href="{site_url('home')}" class="navbar-brand">Pendaftaran Online</a> -->
					<a class="navbar-brand">Pendaftaran Online</a>
					<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
				</div>
					
				<div class="navbar-collapse collapse" id="navbar-main">
					<ul class="nav navbar-nav">
						<!-- <li>
							<a href="{site_url('home')}">Depan</a>
						</li> -->
						<li>
							<a href="{site_url('form')}">Isi Biodata</a>
						</li>
						<li>
							<a href="{site_url('form/nomor_rekening')}">Info Rekening Pembayaran</a>
						</li>
						<li>
							<a href="{site_url('form/upload')}">Upload Berkas</a>
						</li>
						<li>
							<a href="{site_url('form/cetak')}">Cetak Biodata</a>
						</li>
						<!-- <li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Pendaftaran <i class="caret"></i></a>
							<ul class="dropdown-menu">
								<li><a href="{site_url('penerimaan')}">1 - Pemilihan Penerimaan</a></li>
								<li><a href="{site_url('form/isi')}">2 - Pengisian Form</a></li>
								<li><a href="{site_url('form/upload')}">3 - Upload Berkas</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Verifikasi <i class="caret"></i></a>
							<ul class="dropdown-menu">
								<li><a href="{site_url('verifikasi/hasil')}">Hasil Verifikasi</a></li>
								<li><a href="{site_url('verifikasi/voucher')}">4 - Kode Voucher</a></li>
								<li><a href="{site_url('verifikasi/kartu_ujian')}">5 - Cetak Kartu</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#">Pengumuman</a>
						</li> -->
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<!-- <li>
							<a href="{site_url('auth/change_password')}">Ganti Password</a>
						</li> -->
						<li>
							<a href="{site_url('auth/logout')}">Logout</a>
						</li>
					</ul>
				</div>

			</div>
		</div>
		
		<div class="container">
		{block name='body-content'}{/block}
		</div>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="{base_url('assets/js/jquery-1.11.0.min.js')}"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="{base_url('assets/js/bootstrap.min.js')}"></script>
		{block name='footer-script'}{/block}
		<!-- Google Analytics -->
		{literal}
		<script type="text/javascript">
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-41992721-2', 'unair.ac.id');
			ga('send', 'pageview');

		 </script>
		 {/literal}
	</body>
</html>