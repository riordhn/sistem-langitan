<!DOCTYPE html>
<html lang="id">
	<head>
		<title>Pendaftaran Online Mahasiswa Baru Universitas Maarif Hasyim Latif</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap -->
		<link href="{base_url()}assets/css/bootstrap.min.css?_t={time()}" rel="stylesheet">
		
		<style type="text/css">
			body { padding-top: 40px; }
		</style>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body>
		<div class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<a href="./" class="navbar-brand">Universitas Maarif Hasyim Latif (UMAHA YPM)</a>
					<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
				</div>
				
				<div class="navbar-collapse collapse" id="navbar-main">
					<ul class="nav navbar-nav">
						<li>
							<a href="#">Login</a>
						</li>
						<li>
							<a href="#">Registrasi</a>
						</li>
						<li>
							<a href="#">Informasi</a>
						</li>
						<li>
							<a href="#">Pengumuman</a>
						</li>
					</ul>
				</div>

			</div>
		</div>
		
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					
					<div class="page-header">
						<h2>Pendaftaran Online Mahasiswa Baru Universitas Maarif Hasyim Latif</h2>
					</div>
					
					<form class="form-horizontal" role="form" action="{site_url('auth/login')}" method="post">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
							<div class="col-sm-3">
								<input type="email" class="form-control" id="inputEmail3" placeholder="Email">
							</div>
						</div>
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-2 control-label">Password</label>
							<div class="col-sm-3">
								<input type="password" class="form-control" id="inputPassword3" placeholder="Password">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-default">Login</button>
							</div>
						</div>
					</form>
					
					<ul>
						<li>Jika belum pernah registrasi account, klik link <strong><a href="#">Registrasi</a></strong> diatas.</li>
						<li>Pastikan tidak melupakan password setelah registrasi.</li>
						<li>Untuk pertanyaan seputar pendaftaran, bisa hubungi helpdesk PPMB dibawah ini :
							<ul>
								<li>Telp : <strong>(031) 5956009</strong>, <strong>(031) 5956010</strong>, <strong>(031) 5956013</strong>, <strong>(031) 5956027</strong></li>
								<li>Email : <a href="#">ppmb@unair.ac.id</a></li>
								<li>Web : <a href="http://ppmb.unair.ac.id" target="_blank">http://ppmb.unair.ac.id</a></li>
								<li>Alamat : Maarif Hasyim Latif Convention Center (ACC), Kampus C Universitas Maarif Hasyim Latif, Mulyorejo, Surabaya 60115</li>			
							</ul>
						</li>
					</ul>
					
				</div>
			</div>
		</div>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://code.jquery.com/jquery.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="assets/js/bootstrap.min.js"></script>
	</body>
</html>