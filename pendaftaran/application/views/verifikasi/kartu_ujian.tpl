{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Download Kartu Ujian</h2>
			</div>

			{if $cmb->TGL_VERIFIKASI_PPMB == ''}

				<div class="alert alert-warning">
					<strong>Berkas Anda belum selesai diverifikasi !</strong>
					<p><a href="{site_url('verifikasi/hasil')}">Klik disini</a> untuk melihat hasil verifikasi</p>
				</div>

			{else if $cmb->VOUCHER->TGL_BAYAR == ''}

				<div class="alert alert-warning">
					<strong>Anda belum menyelesaikan pembayaran voucher !</strong>
					<p><a href="{site_url('verifikasi/voucher')}">Klik disini</a> untuk melihat kode voucher yang harus dibayarkan</p>
				</div>

			{else if $cmb->NO_UJIAN == ''}

				<div class="alert alert-success">
					<strong>Selamat {$cmb->NM_C_MHS} !</strong>
					<p>Anda sudah menyelesaikan proses registrasi, silahkan download kartu ujian dengan memasukkan pin voucher dibawah ini.</p>
				</div>

				<form class="form-horizontal" method="post" action="{current_url()}">
					<fieldset>

						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-2 control-label" for="pin_voucher">PIN Voucher</label>  
							<div class="col-md-4">
								<input type="text" class="form-control input-md" name="pin_voucher" id="pin_voucher"  placeholder="Masukkan PIN voucher disini">
							</div>
						</div>

						<!-- Button -->
						<div class="form-group">
							<label class="col-md-2 control-label" for="singlebutton"></label>
							<div class="col-md-4">
								<button id="singlebutton" name="singlebutton" class="btn btn-primary">Cetak Kartu</button>
							</div>
						</div>

					</fieldset>
				</form>
			{else $cmb->NO_UJIAN != ''}
				
			<div class="alert alert-success">
				<strong>Selamat {$cmb->NM_C_MHS} !</strong>
				<p>Anda bisa mendownload kartu ujian sekarang.</p>
			</div>
				
			<h4><a target="_blank" href="{site_url('verifikasi/cetak_kartu')}">Klik disini untuk mendownload kartu ujian</a></h4>

			{/if}

			{*
			<hr/>

			<h4><a target="_blank" href="{base_url('assets/images/denah-kampus-a.jpg')}">Klik disini untuk mencetak denah lokasi ujian untuk Kampus A</a></h4>
			<h4><a target="_blank" href="{base_url('assets/images/denah-kampus-b.jpg')}">Klik disini untuk mencetak denah lokasi ujian untuk Kampus B</a></h4>
			<h4><a target="_blank" href="{base_url('assets/images/denah-kampus-c.pdf')}">Klik disini untuk mencetak denah lokasi ujian untuk Kampus C</a></h4>

			<hr/>
			<p>Pastikan anda mengetahui lokasi ujian sesuai dengan denah lokasi sehari sebelum pelaksanaan ujian
				dan persiapkan diri untuk mengikuti proses ujian seleksi. Semoga sukses !</p>
			*}
			
		</div>
	</div>
{/block}