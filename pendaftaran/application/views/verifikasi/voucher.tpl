{extends file='home_layout.tpl'}
{block name='head'}
	<style type="text/css">
		code {
			font-size: 2.5em;
			font-weight: bold;
			color: #002166;
		}
	</style>
{/block}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Kode Voucher</h2>
			</div>

			{if $cmb->TGL_VERIFIKASI_PPMB != ''}
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">KODE VOUCHER</h3>
					</div>
					<div class="panel-body">
						<code>{substr($cmb->KODE_VOUCHER, 0, 3)}-{substr($cmb->KODE_VOUCHER, 3, 2)}-{substr($cmb->KODE_VOUCHER, 5, 6)}</code>
						<h4>Biaya: Rp {number_format($cmb->VOUCHER_TARIF->TARIF, 0, ',', '.')},-</h4>
						{if $cmb->VOUCHER->TGL_BAYAR == ''}
							<p>
								<button type="button" class="btn btn-danger btn-lg">
									<span class="glyphicon glyphicon-exclamation-sign"></span> Belum Terbayar
								</button>
							</p>
							<p>Voucher pembayaran bisa hanya dibayarkan melalui <strong>Bank Mandiri</strong>.</p>
							<p>Pembayaran voucher bisa melalui Teller, ATM, atau internet banking.</p>
							<p>Setelah melakukan pembayaran, pastikan Anda mendapatkan notifikasi dan bisa melakukan cetak kartu.</p>
						{else}
							<p>
								<button type="button" class="btn btn-success btn-lg">
									<span class="glyphicon glyphicon-info-sign"></span> Sudah Terbayar
								</button>
							</p>
							<p><a href="{site_url('verifikasi/kartu_ujian')}">Klik disini</a> untuk mencetak kartu ujian</p>
						{/if}
						
					</div>
				</div>
			{else}
				<div class="alert alert-warning">
					<strong>Berkas Anda belum selesai diverifikasi !</strong>
					<p><a href="{site_url('verifikasi/hasil')}">Klik disini</a> untuk melihat hasil verifikasi</p>
				</div>
			{/if}
			
			

		</div>
	</div>
{/block}