{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Hasil Verifikasi</h2>
			</div>
			
			{if not $is_all_syarat_ok}
				<div class="alert alert-warning">
					<h4>Anda belum melengkapi berkas persyaratan !</h4>
					<p><a class="alert-link" href="{site_url('form/upload')}">Klik disini</a> untuk melengkapi berkas persyaratan</p>
				</div>
			{else}
				
				{if $cmb->TGL_SUBMIT_VERIFIKASI == ''}
					<div class="alert alert-info">
						<h4>Anda belum melakukan submit verifikasi</h4>
						<p><a class="alert-link" href="{site_url('form/upload')}">Klik disini</a> untuk melanjutkan upload file & submit verifikasi</p>
					</div>
				{/if}

				{if $is_all_syarat_ok and $cmb->TGL_SUBMIT_VERIFIKASI != '' and $cmb->TGL_VERIFIKASI_PPMB == '' and $is_syarat_verified == 0}
					<div class="alert alert-info">
						<h4>Verifikasi Dokumen sedang dalam proses...</h4>
						<p>Verifikasi berkas dokumen Anda sedang dalam antrian</p>
						<p>Sistem akan menotifikasi apabila tim verifikasi sudah selesai melakukan verifikasi dokumen.</p>
					</div>
				{/if}

				{if $is_all_syarat_ok and $cmb->TGL_SUBMIT_VERIFIKASI != '' and $cmb->TGL_VERIFIKASI_PPMB != '' and $is_syarat_verified == 1}
					<div class="alert alert-success">
						<h4>Verifikasi Dokumen Sukses !</h4>
						<p>Tim verifikator telah memverifikasi semua berkas dokumen yang diupload dan sudah sesuai.</p>
						<p>Langkah selanjutnya adalah mengambil kode voucher, kemudian melakukan pembayaran di Bank Mandiri terdekat.</p>
						<p><a class="alert-link" href="{site_url('verifikasi/voucher')}">Klik disini</a> untuk melihat kode voucher</p>
					</div>
				{/if}

				{if $is_syarat_verified == 2}{* Terdapat dokumen yang tidak diverifikasi *}
					<div class="alert alert-danger">
						<h4>Verifikasi Dokumen Gagal !</h4>
						<p>Tim verifikator sudah melakukan verifikasi dokumen Anda, tetapi tidak sesuai dengan syarat yang dibutuhkan. </p>
						<p><a class="alert-link" href="{site_url('form/upload')}">Klik disini</a> untuk mengupload ulang berkas.</p>
					</div>
				{/if}
				
			{/if}
			
			<!-- Source code view only for debug
			is_all_syarat_ok : {$is_all_syarat_ok}
			TGL_SUBMIT_VERIFIKASI : {$cmb->TGL_SUBMIT_VERIFIKASI}
			TGL_VERIFIKASI_PPMB : {$cmb->TGL_VERIFIKASI_PPMB}
			is_syarat_verified : {$is_syarat_verified}
			-->
			
		</div>
	</div>
{/block}