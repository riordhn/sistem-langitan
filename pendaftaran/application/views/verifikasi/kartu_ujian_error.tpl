{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Download Kartu Ujian</h2>
			</div>

			<div class="alert alert-danger">
				<strong>{$title}</strong>
				<p>{$message}</p>
				<p><a class="alert-link" href="{site_url('verifikasi/kartu_ujian')}">Klik disini</a> untuk kembali</p>
			</div>

			<hr/>

			<h4><a href="#">Klik disini untuk mencetak denah lokasi ujian</a></h4>

			<hr/>
			<p>Pastikan anda mengetahui lokasi ujian sesuai dengan denah lokasi sehari sebelum pelaksanaan ujian
				dan persiapkan diri untuk mengikuti proses ujian seleksi. Semoga sukses !</p>
		</div>
	</div>
{/block}