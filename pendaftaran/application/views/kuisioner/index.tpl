{extends file='home_layout.tpl'}
{block name='head'}
	<style>
		label.jawaban {
			font-weight: normal;
		}
		form {
			margin-bottom: 40px;
		}
	</style>
{/block}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>{$form->NM_FORM}</h2>
			</div>
			
			<div class="alert alert-info">
				<p>Anda diwajibkan mengisi kuisiner ini terlebih dahulu sebelum melanjutkan untuk mencetak kartu ujian.</p>
			</div>

			<form class="form-horizontal" action="{site_url('kuisioner')}" method="post">

				{foreach $form->kategori_soal_set as $kategori_soal}
					<legend>{$kategori_soal->KODE_KATEGORI}. {$kategori_soal->NM_KATEGORI}</legend>

					{foreach $kategori_soal->soal_set as $soal}

						{if $soal->TIPE_SOAL == 1}
							<ul>
								<li><strong>{$soal->NOMER}. {$soal->SOAL}</strong>
									<ul>
										{foreach $soal->jawaban_set as $jawaban}
											<li>
												<label class="radio jawaban">
													<input type="radio" name="jawaban[{$soal->ID_SOAL}]" value="{$jawaban->ID_JAWABAN}" /> {$jawaban->JAWABAN}
												</label>
											</li>
										{/foreach}
									</ul>
								</li>
							</ul>
						{/if}

					{/foreach}

				{/foreach}

				<div class="form-group">
					<label class="col-md-1 control-label" for="singlebutton"></label>
					<div class="col-md-4">
						<input type="submit" value="Submit Kuisioner" class="btn btn-primary" />
					</div>
				</div>
			</form>

		</div>
	</div>
{/block}
{block name='footer-script'}
	<script type="text/javascript">
		$(document).ready(function() {
			
			$('form').on('submit', function(e) {
				e.preventDefault();
				
				var jumlah_soal = {$jumlah_soal};
				var jumlah_jawab = $('input:radio[name^=jawaban]:checked').length;
				
				if (jumlah_soal !== jumlah_jawab) {
					alert('Harap periksa kembali isian. Pastikan semua jawaban telah di isi.');
				}
				else {
					this.submit();
				}
			});
			
		});
	</script>
{/block}