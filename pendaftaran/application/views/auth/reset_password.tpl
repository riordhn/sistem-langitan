{extends file='front_layout.tpl'}
{block name='title'}Reset Password - {/block}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Reset Password</h2>
			</div>

			{if !empty($change_ok)}
				<div class="alert alert-success">
					<strong>Password berhasil diganti !</strong>
					<p>Password berhasil diganti, silahkan login dengan menggunakan password baru Anda. <a class="alert-link" href="{site_url()}">Klik disini</a> untuk kembali ke halaman login</p>
				</div>
			{else}

				<p>Silahkan masukkan password baru yang diinginkan.</p>

				<form class="form-horizontal" method="post" >
					<fieldset>

						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-2 control-label" for="password">Password Baru</label>  
							<div class="col-md-4">
								<input id="password" name="password" type="password" class="form-control input-md" autocomplete="off">
							</div>
							{form_error('password')}
						</div>

						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-2 control-label" for="password2">Password Baru (Lagi)</label>  
							<div class="col-md-4">
								<input id="password2" name="password2" type="password" class="form-control input-md" autocomplete="off">
							</div>
							{form_error('password2')}
						</div>

						{if !empty($error_message)}
							<div class="form-group">
								<label class="col-md-2 control-label"></label>
								<div class="col-md-4">
									<p class="help-block text-danger">{$error_message}</p>
								</div>
							</div>
						{/if}

						<!-- Button -->
						<div class="form-group">
							<label class="col-md-2 control-label" for="submitbutton"></label>
							<div class="col-md-4">
								<button id="submitbutton" name="submitbutton" class="btn btn-primary">Reset</button>
							</div>
						</div>

					</fieldset>
				</form>

			{/if}





		</div>
	</div>
{/block}