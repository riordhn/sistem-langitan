{extends file='home_layout.tpl'}
{block name='title'}Ganti Password - {/block}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Ganti Password</h2>
			</div>
			
			<p>Halaman ini adalah halaman untuk merubah password. Harap selalu menyimpan password.</p>
			
			<form class="form-horizontal" method="post" action="{current_url()}">
				<fieldset>
					<!-- Password input -->
					<div class="form-group">
						<label for="current_password" class="col-md-2 control-label">Password Sekarang</label>
						<div class="col-md-3">
							<input type="password" class="form-control" id="current_password" name="current_password">
						</div>
						{form_error('current_password')}
					</div>
					
					<!-- Password input -->
					<div class="form-group">
						<label for="new_password" class="col-md-2 control-label">Password Baru</label>
						<div class="col-md-3">
							<input type="password" class="form-control" id="new_password" name="new_password">
						</div>
						{form_error('new_password')}
					</div>
					
					{if !empty($error_message)}
					<div class="form-group">
						<label class="col-md-2 control-label"></label>
						<div class="col-md-4">
							<p class="help-block text-danger">{$error_message}</p>
						</div>
					</div>
					{/if}
					
					<!-- Submit button -->
					<div class="form-group">
						<label class="col-md-2 control-label"></label>
						<div class="col-md-3">
							<input type="submit" class="btn btn-primary" value="Simpan" />
						</div>
					</div>
				</fieldset>
			</form>
			
		</div>
	</div>
{/block}