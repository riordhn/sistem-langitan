{extends file='front_layout.tpl'}
{block name='title'}Registrasi Akun - {/block}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">

			<div class="page-header" style="margin-bottom: 5px">
				<h2>Registrasi Akun Baru</h2>
				<a href="{site_url()}">Kembali ke halaman utama</a>
			</div>
			
			<div class="row">
				<div class="col-md-10">
					<p>Form berikut ini adalah form untuk mendapatkan akun login ke sistem Pendaftaran Online Mahasiswa Baru Universitas Hasyim Latief. 
							Sebelum melakukan pengisian, pastikan nomer telp / hp dan email adalah milik sendiri dan bukan milik orang lain.</p>
				</div>
			</div>
			
			<hr style="margin-top: 0" />
			
			{if !empty($is_error)}
				<div class="alert alert-warning alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<strong>{$title}</strong> {$message}
				</div>
			{/if}

			<form class="form-horizontal {if form_error('nama')}has-error{/if}" role="form" action="{site_url('auth/registrasi')}" method="post">
				<div class="form-group">
					<label for="nama" class="col-md-2 control-label">Nama</label>
					<div class="col-md-4">
						<input type="text" class="form-control" id="nama" name="nama" placeholder="Nama lengkap" value="{set_value('nama')}">
						{form_error('nama')}
					</div>
				</div>
				<div class="form-group {if form_error('telp')}has-error{/if}">
					<label for="telp" class="col-md-2 control-label">Telp / HP</label>
					<div class="col-md-4">
						<input type="numeric" class="form-control" id="telp" name="telp" placeholder="Nomer Telp / HP" value="{set_value('telp')}">
						{form_error('telp')}
					</div>
				</div>
				<div class="form-group {if form_error('email')}has-error{/if}">
					<label for="email" class="col-md-2 control-label">Email</label>
					<div class="col-md-4">
						<input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{set_value('email')}">
						{form_error('email')}
					</div>
				</div>
				<div class="form-group {if form_error('password')}has-error{/if}">
					<label for="password" class="col-md-2 control-label">Password</label>
					<div class="col-md-4">
						<input type="password" class="form-control" id="password" name="password" placeholder="Password yg diinginkan" value="{set_value('password')}">
						{form_error('password')}
					</div>
				</div>
				<div class="form-group">
					<label for="recaptcha" class="col-md-2 control-label">Recaptcha</label>
					<div class="col-md-4">
						{$recaptcha_html}
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<button type="submit" class="btn btn-primary">Registrasi</button><br/>
					</div>
				</div>
			</form>
				
			<p>Keterangan:</p>
			<ul>
				<li>Nama harus sesuai dengan akte kelahiran / nomer ujian nasional / ijazah.</li>
				<li>No telp / hp harus milik sendiri (bukan diwakilkan). Contoh pengisian : 081615148000</li>
				<li>Email harus email aktif, karena link verifikasi dikirimkan melalui email</li>
			</ul>
		</div>
	</div>
{/block}