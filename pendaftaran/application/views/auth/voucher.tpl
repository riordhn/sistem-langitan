{extends file='front_layout.tpl'}
{block name='head'}
	<style type="text/css">
		.panel-success>.panel-heading {
		    color: #fff;
		    background-color: #130;
		    border-color: #d6e9c6;
		}
	</style>
{/block}
{block name='body-content'}
	<div class="row">
	</br>
	<h2> Pengambilan Nomor Pendaftaran </h2>
	</br>
		{foreach $penerimaan_set as $data}
		<div class="col-lg-4">
			<div class="panel">
				<form class="form-horizontal" role="form" action="{site_url('auth/voucher')}" method="post">
					<div class="panel panel-success">
						<div class="panel-heading">
							<input type="hidden" name="id_jenjang" value="{$data.ID_JENJANG}" />
							<input type="hidden" name="id_penerimaan" value="{$data.ID_PENERIMAAN}" />
							<h4>{$data.NM_PENERIMAAN} Gelombang {$data.GELOMBANG}</h4>
						</div>
						<div class="panel-body">
							<label for="program_studi" >List Program Studi :</label>
							<br>
							{foreach $prodi_set as $data_prodi}
								{if $data_prodi.ID_JENJANG == $data.ID_JENJANG && $data_prodi.ID_PENERIMAAN == $data.ID_PENERIMAAN}
									<label for="nm_program_studi" >*) {$data_prodi.NM_JENJANG} - {$data_prodi.NM_PROGRAM_STUDI}</label>
									<br>
								{/if}
							{/foreach}
							
							<br>
							<p class="text-center" ><strong>Ambil Nomor Pendaftaran</strong></p>
							<div class="text-center">
							<button type="submit" class="btn btn-primary btn-lg">Ambil</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		{/foreach}
	</div>
{/block}
{block name='footer-script'}
	{js_widget('twitter')}
{/block}