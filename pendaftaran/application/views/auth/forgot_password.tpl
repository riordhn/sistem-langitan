{extends file='front_layout.tpl'}
{block name='title'}Permintaan Reset Password - {/block}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Permintaan Reset Password</h2>
			</div>

			<p>Halaman ini adalah halaman permintaan untuk mereset password.
				Sebelum melakukan reset password, pastikan mengingat kembali password yang sudah Anda simpan.
				Setelah tombol Reset diklik, maka Anda akan menerima instruksi untuk mereset password anda.
			</p>

			{if !empty($email_wrong)}
				<div class="alert alert-danger">
					<strong>Email tidak ditemukan !</strong>
					<p>Pastikan email yang Anda masukkan benar</p>
				</div>
			{/if}

			{if !empty($email_ok)}
				<div class="alert alert-info">
					<strong>Permintaan reset password berhasil !</strong>
					<p>Silahkan cek inbox email untuk mengikuti instruksi reset password. <a href="{site_url()}">Klik disini</a> untuk kembali ke halaman depan</p>
				</div>
			{/if}

			{if empty($email_wrong) and empty($email_ok)}

				<form class="form-horizontal" method="post" action="{current_url()}">
					<fieldset>

						<!-- Text input-->
						<div class="form-group">
							<label class="col-sm-1 control-label" for="email">Email</label>  
							<div class="col-sm-4">
								<input id="email" name="email" type="text" placeholder="Email" class="form-control input-md" autocomplete="off">
							</div>
						</div>

						<!-- Button -->
						<div class="form-group">
							<label class="col-sm-1 control-label" for="submitbutton"></label>
							<div class="col-sm-4">
								<button id="submitbutton" name="submitbutton" class="btn btn-primary">Reset</button>
							</div>
						</div>

					</fieldset>
				</form>

			{/if}

		</div>
	</div>
{/block}