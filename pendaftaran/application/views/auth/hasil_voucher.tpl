{extends file='front_layout.tpl'}
{block name='head'}
	<style type="text/css">
		.panel-success>.panel-heading {
		    color: #fff;
		    background-color: #fb030f;
		    border-color: #d6e9c6;
		}
	</style>
{/block}
{block name='body-content'}
	<div class="row">
	</br>
	
	</br>
		<div class="panel panel-success">
			<div class="panel-heading">
				<h3 class="text-center"> Sukses Mengambil Nomor Pendaftaran </h3>
			</div>
			<div class="panel-body">
				<h4 class="text-center"> Nomor Pendaftaran	: {$kode_voucher} </h4>
				<h4 class="text-center"> PIN Pendaftaran	: {$pin_voucher} </h4>
				<h4 class="text-left"> *) Mohon dicatat baik-baik nomor pendaftaran ini, untuk memudahkan panitia pendaftaran mengidentifikasi formulir Anda setelah pendaftaran.</h4>
			</div>
		</div>
	</div>
	<!-- <div class="row">
		<div class="col-md-12">
	
			<div class="page-header">
				<h2>Pengambilan Nomor Pendaftaran dan PIN Untuk Login</h2>
			</div>
			<form class="form-horizontal" role="form" action="{site_url('auth/voucher')}" method="post">
					<input type="hidden" name="mode" value="pilih" />
					{if !empty($error_message)}
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						{$error_message}
					</div>
					{/if}
					<div class="form-group">
						<label for="jenjang" class="col-sm-2 control-label">Jenjang</label>
						<div class="col-sm-2">
							<input type="email" class="form-control" autocomplete="off" autofocus="true" id="email" name="email" placeholder="Email" value="{set_value('email')}">
							<select name="id_jenjang" class="form-control">
				                <option value="0">-- Tidak Ada --</option>
				            {foreach $jenjang_set as $jenjang}
				                <option value="{$jenjang.ID_JENJANG}">{$jenjang.NM_JENJANG}</option>
				            {/foreach}
				            	
				            </select>
						</div>
					</div>
					<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
				</form>
			
			{foreach $jenjang_aktif_set as $jenjang}
				<form class="form-horizontal" role="form" action="{site_url('auth/voucher')}" method="post">
				<input type="hidden" name="id_jenjang" value="{$jenjang.ID_JENJANG}" />
					
					{if !empty($error_message)}
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						{$error_message}
					</div>
					{/if}
			
					<div class="form-group">
						<a class="col-sm-2 control-label">Pengambilan Jenjang {$jenjang.NM_JENJANG}</a>
					</div>
			
					<div class="form-group">
						<label for="jenjang" class="col-sm-2 control-label">Daftar Program Studi :</label>
						{foreach $program_studi_set as $prodi}
									        	<label for="jenjang" class="col-sm-2 control-label">{$prodi@index + 1}. {$prodi.NM_JENJANG} - {$prodi.NM_PROGRAM_STUDI}</label>
									        {/foreach}
					</div>
					
					<div class="form-group">
						<label for="jenjang" class="col-sm-2 control-label">Ambil Nomor</label>
						<div class="col-sm-2">
							<input type="email" class="form-control" autocomplete="off" autofocus="true" id="email" name="email" placeholder="Email" value="{set_value('email')}">
							<button type="submit" class="btn btn-primary">Ambil</button>
						</div>
					</div>
			
					{if !empty($kode_voucher)}
					<div class="form-group">
						<label for="jenjang" class="col-sm-2 control-label">Kode Voucher : {$kode_voucher}</label>
						<label for="jenjang" class="col-sm-2 control-label">PIN Voucher : {$pin_voucher}</label>
					</div>
					{/if}
						
				</form>
			{/foreach}
	
			
			
			
					
				<div class="form-group">
					<label for="password" class="col-sm-2 control-label">Password</label>
					<div class="col-sm-4">
						<input type="password" class="form-control" id="password" name="password" placeholder="Password">
					</div>
				</div>
					
			
	
			<form class="form-horizontal" role="form" action="{site_url(auth/voucher)}" method="post">
				
				{if !empty($error_message)}
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					{$error_message}
				</div>
				{/if}
				
				<div class="form-group">
					<label for="jenjang" class="col-sm-2 control-label">Jenjang</label>
					<div class="col-sm-2">
						<input type="email" class="form-control" autocomplete="off" autofocus="true" id="email" name="email" placeholder="Email" value="{set_value('email')}">
						<select name="id_jenjang" class="form-control">
			                <option value="0">-- Tidak Ada --</option>
			            {foreach $jenjang_set as $jenjang}
			                <option value="{$jenjang.ID_JENJANG}">{$jenjang.NM_JENJANG}</option>
			            {/foreach}
			            </select>
					</div>
				</div>
					
				<div class="form-group">
					<label for="password" class="col-sm-2 control-label">Password</label>
					<div class="col-sm-4">
						<input type="password" class="form-control" id="password" name="password" placeholder="Password">
					</div>
				</div>
					
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-primary">Submit</button>
						
					</div>
				</div>
					
			</form>
	
			<ul>
				<li>Jika belum mempunyai voucher, klik link <a href="{site_url('auth/voucher')}">Ambil Voucher</a> diatas.</li>
				<li>Pastikan tidak melupakan password setelah registrasi. <a href="{site_url('auth/forgot_password')}">Klik disini</a> apabila ingin mereset password.</li>
				<li>Untuk pertanyaan seputar pendaftaran, bisa menghubungi helpdesk dibawah ini :
					<ul>
						<li>Telp : <strong>(031) 7885205</strong>, <strong>(031) 7884034</strong></li>
						<li>Yahoo Messenger : <a href="ymsgr:SendIM?flack_trooper"><img border=0 src="http://opi.yahoo.com/online?u=flack_trooper&m=g&t=1"></a></li>
						<li>Email : <u>pmb@umaha.ac.id</u></li>
						<li>Web : <a href="http://www.umaha.ac.id" target="_blank">http://www.umaha.ac.id</a></li>
						<li>Alamat : Jl. Ngelom Megare Sepanjang, Sidoarjo</li>
						<li>Facebook Group : <a href="https://www.facebook.com/groups/ppmb.unair/">facebook.com/groups/ppmb.unair/</a></li>
						<li>Twitter : <a href="https://twitter.com/PPMBUnair" class="twitter-follow-button" data-show-count="true" data-lang="en">Follow @PPMBUnair</a></li>
					</ul>
				</li> 
			</ul>
	
		</div>
	</div> -->
{/block}
{block name='footer-script'}
	{js_widget('twitter')}
{/block}