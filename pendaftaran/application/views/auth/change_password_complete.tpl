{extends file='home_layout.tpl'}
{block name='title'}Ganti Password - {/block}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Ganti Password</h2>
			</div>
			
			<div class="alert alert-info">Password berhasil diganti. <a class="alert-link" href="{site_url('home/')}">Klik untuk kembali</a> ke halaman awal.</div>
			
		</div>
	</div>
{/block}