{extends file='front_layout.tpl'}
{block name='title'}Registrasi Akun - {/block}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">

			<div class="page-header">
				<h2>Registrasi Akun Baru</h2>
			</div>
			
			<div class="alert alert-success">
				<p><strong>Verifikasi email berhasil !</strong><br/>
					Akun registrasi anda sudah bisa dipakai untuk login. Silahkan klik link dibawah ini untuk login.
				</p>
				<p><a href="{site_url()}">Klik disini untuk kembali ke halaman Login</a></p>
			</div>
			
		</div>
	</div>
{/block}