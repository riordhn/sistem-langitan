{extends file='front_layout.tpl'}
{block name='title'}Registrasi Akun - {/block}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">

			<div class="page-header">
				<h2>Registrasi Akun Baru</h2>
			</div>
			
			<div class="alert alert-success">
				<p><strong>Registrasi Berhasil !</strong><br/>
					Anda sudah mempunyai akun registrasi untuk melakukan pendaftaran. Silahkan buka email untuk verifikasi email anda agar bisa digunakan untuk login.
				</p>
				<p>Apabila email tidak ada di folder inbox email, coba cek di folder Bulk/Spam.</p>
				<p>Apabila email tidak terkirim sampai lebih dari 1x24 jam, silahkan hubungi panitia pendaftaran.</p>
				<p><a href="{site_url()}">Klik disini untuk kembali ke halaman Login</a></p>
			</div>
			
		</div>
	</div>
{/block}
{block name='footer-script'}
	
{/block}