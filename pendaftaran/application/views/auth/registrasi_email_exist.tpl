{extends file='front_layout.tpl'}
{block name='title'}Registrasi Akun - {/block}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">

			<div class="page-header">
				<h2>Registrasi Akun Baru</h2>
			</div>
			
			<div class="alert alert-warning">
				<p><strong>Registrasi tidak berhasil</strong><br/>
					Email <strong>{$email}</strong> sudah terdaftar sebagai akun registrasi.<br/>
					Apabila anda lupa password dan ingin meresetnya klik link berikut ini : <a href="#">Lupa Password</a>
				</p>
				<p><a href="{site_url()}">Klik disini untuk kembali ke halaman Login</a></p>
			</div>
			
		</div>
	</div>
{/block}