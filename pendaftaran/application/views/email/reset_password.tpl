<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="id">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<style type="text/css">

	::selection{ background-color: #E13300; color: white; }
	::moz-selection{ background-color: #E13300; color: white; }
	::webkit-selection{ background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body{
		margin: 0 15px 0 15px;
	}
	
	p.footer{
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}
	
	#container{
		margin: 10px;
		border: 1px solid #D0D0D0;
		-webkit-box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body style="background-color: #fff; font: 13px/20px normal Helvetica, Arial, sans-serif; color: #4F5155;">

<div id="container" style="margin: 10px; border: 1px solid #D0D0D0; -webkit-box-shadow: 0 0 8px #D0D0D0;">

	<div id="body" style="margin: 0 15px 0 15px;">
		<p>Hai {$calon_pendaftar->NAMA_PENDAFTAR},</p>
		<p>Anda telah melakukan permintaan reset password. Password Anda saat ini masih sama seperti sebelumnya.
			Untuk melanjutkan reset password, klik link dibawah ini :</p>
		
		<p><a href="{site_url('auth/reset_password')}?id={$calon_pendaftar->ID_CALON_PENDAFTAR}&e={$calon_pendaftar->EMAIL_HASH}&p={$calon_pendaftar->PASS_PENDAFTAR}"><strong>RESET PASSWORD</strong></a></p>
		
		<p><i>Anda tidak harus membalas email ini, karena email ini hanyalah email pemberitahuan.</i></p>
		
		<p style="font-size: 11px;"><strong>Penerimaan Mahasiswa Baru - Universitas Maarif Hayim Latif (UMAHA YPM)</strong><br/>
			Jl. Ngelom Megare Sepanjang, Sidoarjo<br/>
			Telp : (031) 7885205, (031) 7884034<br/>
			Email : pmb@umaha.ac.id<br/>
			Website : http://www.umaha.ac.id<br/>
			<!--Facebook Group : <a href="https://www.facebook.com/groups/ppmb.unair/">PPMB</a><br/>
			Twitter : <a href="http://twitter.com/PPMBUnair">@PPMBUnair</a>-->
		</p>
	</div>

	<p class="footer" style="text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;">Copyright &copy; 2014 - Pusat Penerimaan Mahasiswa Baru - <a href="http://www.umaha.ac.id">Universitas Maarif Hasyim Latif (UMAHA YPM)</a></p>
</div>

</body>
</html>