{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Selamat datang, {$nama_pendaftar}</h2>
			</div>
			
			<p>Selamat datang di web pendaftaran online Universitas Airlangga. Berikut adalah langkah pendaftaran <b>{$cmb->PENERIMAAN->NM_PENERIMAAN} gelombang {$cmb->PENERIMAAN->GELOMBANG} tahun {$cmb->PENERIMAAN->TAHUN}</b> :</p>
			
			<ol>
				<li>Memilih jenjang seleksi
					{if $steps['1'] == TRUE}
						<span class="label label-success">OK</span>
					{else}
						<a href="{site_url('penerimaan')}"><span class="label label-warning">Belum</span></a>
					{/if}
				</li>
				<li>Mengisi dan submit formulir pendaftaran
					{if $steps['1'] == TRUE}
						{if $steps['2'] == TRUE}
							<span class="label label-success">OK</span>
						{else}
							<a href="{site_url('form/isi')}"><span class="label label-warning">Belum</span></a>
						{/if}
					{/if}
				</li>
				<li>Mengupload berkas syarat yang dibutuhkan
					{if $steps['2'] == TRUE}
						{if $steps['3'] == TRUE}
							{if $cmb->TGL_VERIFIKASI_PPMB != ''}
								<span class="label label-success">OK</span>
							{else}
								<a href="{site_url('form/upload')}"><span class="label label-info">Melihat Hasil Verifikasi</span></a>
								{if date('Ymd', strtotime($cmb->PENERIMAAN->TGL_AKHIR_VERIFIKASI)) >= date('Ymd') }
									{if $steps['3_antrian'] == '-1'}
										( No Antrian : Belum Submit)
									{else if $steps['3_antrian'] == '0'}
										( No Antrian : Sedang Proses)
									{else}
										( No Antrian : {$steps['3_antrian']})
									{/if}
								{else}
									( <b><font color="red">Waktu verifikasi sudah habis</font></b> )
								{/if}
							{/if}
						{else}
							<a href="{site_url('form/upload')}"><span class="label label-warning">Belum</span></a>
						{/if}
					{/if}
				</li>
				<li>Membayar voucher ke bank
					{if $steps['3'] == TRUE}
						{if $steps['4'] == TRUE}
							<span class="label label-success">OK</span>
						{else}
							<a href="{site_url('verifikasi/voucher')}"><span class="label label-warning">Belum</span></a>
						{/if}
					{/if}
				</li>
				{if $steps['5_enabled'] == TRUE}
					<li>Mengisi kuisioner
						{if $steps['4'] == TRUE}
							{if $steps['5'] == TRUE}
								<span class="label label-success">OK</span>
							{else}
								<a href="{site_url('kuisioner')}"><span class="label label-warning">Belum</span></a>
							{/if}
						{/if}
					</li>
				{/if}
				<li>Mencetak kartu ujian
					{if $steps['5'] == TRUE}
						{if $steps['6'] == TRUE}
							<span class="label label-success">OK</span>
						{else}
							<a href="{site_url('verifikasi/kartu_ujian')}"><span class="label label-warning">Belum</span></a>
						{/if}
					{/if}
				</li>
				<li>Ujian seleksi</li>
				<li>Pengumuman {if $steps['tgl_pengumuman'] != FALSE} : {$steps['tgl_pengumuman']}{/if}</li>
				<li>Pendaftaran ulang</li>
			</ol>
			
			<p class="small">Keterangan: Silahkan klik tombol disebelahnya untuk mengikuti proses selanjutnya</p>
		</div>
	</div>
{/block}