{extends file='front_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">

			<div class="page-header">
				<h2>Test SMS Gateway Ajax</h2>
			</div>

			<form class="form-horizontal" id="sms_form">
				<fieldset>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="no_hp">No Hp</label>  
						<div class="col-md-3">
							<input id="no_hp" name="no_hp" type="text" placeholder="" class="form-control input-md">

						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="pesan">Pesan</label>  
						<div class="col-md-5">
							<input id="pesan" name="pesan" type="text" placeholder="" class="form-control input-md">

						</div>
					</div>

					<!-- Button -->
					<div class="form-group">
						<label class="col-md-4 control-label" for=""></label>
						<div class="col-md-4">
							<button id="SubmitButton" name="" class="btn btn-primary">Kirim by Ajax POST</button>
						</div>
					</div>

				</fieldset>
			</form>


			<pre id="server_response"></pre>

		</div>
	</div>
{/block}
{block name='footer-script'}
	<script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/md5.js"></script>
	<script type="text/javascript">

		/**
		 * Fungsi Send SMS ke server ppmb (uncompressed)
		 */
		function send_sms(no_hp, pesan, trial) {
			
			/** Exit jika gagal 3 kali **/
			if (trial === 3) { return; }
			
			$.ajax({
				async: false,
				cache: false,
				url: 'http://ppmb.unair.ac.id/sms/send',
				data: 'tx=' + token + '&nx=' + no_hp + '&mx=' + pesan,
				type: 'POST'
			}).done(function(r) {
				console.log(r);
			}).fail(function() {
				send_sms(no_hp, pesan, trial + 1);
			});
		}

		$(document).ready(function() {

			$('#SubmitButton').on('click', function(e) {

				e.preventDefault();

				var no_hp = $('#no_hp').val();
				var pesan = $('#pesan').val();

				send_sms(no_hp, pesan, 1);
			});

		});
	</script>
	{literal}
	<script>function send_sms(a,b,c){if(c===3){return;}$.ajax({async:false,cache:false,url:'http://ppmb.unair.ac.id/sms/send',data:'tx='+token+'&nx='+a+'&mx='+b,type:'POST'}).done(function(r){console.log(r);}).fail(function(){send_sms(a,b,c+1);});}</script>
	{/literal}
{/block}