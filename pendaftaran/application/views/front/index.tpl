{extends file='front_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">

			<div class="page-header">
				<h2>Pendaftaran Mahasiswa Baru {$nama_pt}</h2>
			</div>

			<!-- <form class="form-horizontal" role="form" action="{site_url('auth/login')}" method="post"> -->
			<form class="form-horizontal" role="form" action="{site_url('auth/login_manual')}" method="post">
				
				{if !empty($error_message)}
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					{$error_message}
				</div>
				{/if}
				
				<div class="form-group">
					<label for="kode_voucher" class="col-sm-2 control-label">Nomor Pendaftaran</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" autocomplete="off" autofocus="true" id="kode_voucher" name="kode_voucher" value="{set_value('kode_voucher')}">
					</div>
				</div>
					
				<div class="form-group">
					<label for="pin_voucher" class="col-sm-2 control-label">Pin Pendaftaran</label>
					<div class="col-sm-4">
						<input type="password" class="form-control" id="pin_voucher" name="pin_voucher" >
					</div>
				</div>
					
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-primary">Login</button>
						
					</div>
				</div>
					
			</form>

			<ul>
				<li>Jika belum mempunyai nomor pendaftaran, klik link <a href="{site_url('auth/voucher')}">Ambil Nomor Pendaftaran</a> diatas.</li>
				<!-- <li>Pastikan tidak melupakan password setelah registrasi. <a href="{site_url('auth/forgot_password')}">Klik disini</a> apabila ingin mereset password.</li> -->
				{$isi_informasi}
				<!-- <li>Untuk pertanyaan seputar pendaftaran, bisa menghubungi helpdesk dibawah ini :
					<ul>
						<li>Telp : <strong>{$telp_pt}</strong>, <strong>(031) 7884034</strong></li>
						<li>Yahoo Messenger : <a href="ymsgr:SendIM?flack_trooper"><img border=0 src="http://opi.yahoo.com/online?u=flack_trooper&m=g&t=1"></a></li>
						<li>Email : <u>{$email_pmb}</u></li>
						<li>Web : <a href="{$web_pt}" target="_blank">{$web_pt}</a></li>
						<li>Alamat : {$alamat_pt}</li>
						<li>Facebook Group : <a href="https://www.facebook.com/groups/ppmb.unair/">facebook.com/groups/ppmb.unair/</a></li>
						<li>Twitter : <a href="https://twitter.com/PPMBUnair" class="twitter-follow-button" data-show-count="true" data-lang="en">Follow @PPMBUnair</a></li>
					</ul>
				</li>  -->
			</ul>

		</div>
	</div>
{/block}
{block name='footer-script'}
	{js_widget('twitter')}
{/block}