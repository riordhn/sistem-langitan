{extends file='front_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">

			<div class="page-header">
				<h2>Informasi Pendaftaran</h2>
			</div>
			
			<h3>Informasi Pendaftaran</h3>
<!--			
			<p>Semua informasi tentang pembukaan pendaftaran, syarat, jadwal dan lain-lain bisa membuka website PPMB Universitas Airlangga atau klik <a href="http://ppmb.unair.ac.id" target="_blank">http://ppmb.unair.ac.id</a>.</p>
			
			<h3>Download</h3>
			
			<p>Berkas-berkas syarat pendaftaran bisa di download dibawah ini : </p>
			
			<ul>
				<li><a href="{base_url('assets/pdf/Surat-Pernyataan-BIODATA.pdf')}" target="_blank">(D3/D4) Surat Pernyataan Kebenaran Pengisian Biodata Pendaftaran Online dan File yang diupload</a></li>
				<li><a href="{base_url('assets/pdf/FORM-UKT-UKA-VOKASI-2015.pdf')}" target="_blank">(D3/D4) Surat Pernyataan UKS/UKA</a></li>
				<li><a href="{base_url('assets/pdf/surat-kelengkapan-berkas-D3-D4-2015.pdf')}" target="_blank">(D3/D4) Surat Pernyataan Kelengkapan Berkas</a></li>
			</ul><hr>
			<ul>
				<li><a href="{base_url('assets/pdf/Surat-Pernyataan-BIODATA.pdf')}" target="_blank">(Alih Jenis) Surat Pernyataan Kebenaran Pengisian Biodata Pendaftaran Online dan File yang diupload</a></li>
				<li><a href="{base_url('assets/pdf/NEW-Surat-Pernyataan-SP3-2015.pdf')}" target="_blank">(Alih Jenis) Surat Pernyataan SP3</a></li>
				<li><a href="{base_url('assets/pdf/surat-kelengkapan-berkas-S1-2015.pdf')}" target="_blank">(Alih Jenis) Surat Pernyataan Kelengkapan Berkas</a></li>
			</ul><hr>
			<ul>
				<li><a href="http://ppmb.unair.ac.id/files/FORM%20pernyataan%20Berkas%20SKL%202014.pdf" target="_blank">(S2) Surat Pernyataan Ijazah</a></li>
				<li><a href="http://ppmb.unair.ac.id/files/Surat%20Pernyataan%20PEND%20ONLINE.pdf" target="_blank">(S2) Surat Pernyataan Kebenaran Pengisian Biodata Pendaftaran Online dan File yang diupload</a></li>
				<li><a href="http://ppmb.unair.ac.id/files/FORM%20REKOM%20MAGISTER.pdf" target="_blank">(S2) Surat Rekomendasi Dari Pembimbing/Atasan</a></li>
				<li><a href="http://ppmb.unair.ac.id/files/FORM%20Rekomendasi%20MM.pdf" target="_blank">(S2, Khusus prodi MM) Surat Rekomendasi Dari Pembimbing/Atasan</a></li>
				<li><a href="http://ppmb.unair.ac.id/files/SURAT%20PERNYATAAN%20SP3%20MAGISTER.pdf" target="_blank">(S2) Surat Pernyataan SP3 yang telah Diisi dan Bermaterai</a></li>
				<li><a href="http://ppmb.unair.ac.id/files/SURAT%20PERNYATAAN%20SP3%20PROMAGSI.pdf" target="_blank">(S2, Khusus prodi Mag Profesi Psikologi) Surat Pernyataan SP3 yang telah Diisi dan Bermaterai</a></li>
				<li><a href="http://ppmb.unair.ac.id/files/FORM%20MATRIKULASI%202015.pdf" target="_blank">(S2) Form Matrikulasi</a></li>
				<li><a href="{base_url('assets/pdf/FORM-PMILHAN-KELAS-2015.pdf')}" target="_blank">(S2, Khusus prodi MM dan Sains Manajemen) Form Pilihan Kelas</a></li>
			</ul><hr>
			<ul>

				<li><a href="http://ppmb.unair.ac.id/files/FORM%20pernyataan%20Berkas%20SKL%202014.pdf" target="_blank">(S3) Surat Pernyataan Ijazah</a></li>
				<li><a href="http://ppmb.unair.ac.id/files/Surat%20Pernyataan%20PEND%20ONLINE.pdf" target="_blank">(S3) Surat Pernyataan Kebenaran Pengisian Biodata Pendaftaran Online dan File yang diupload</a></li>
				<li><a href="http://ppmb.unair.ac.id/files/FORM%20REKOM%20DOKTOR.pdf" target="_blank">(S3) Surat Rekomendasi Dari Pembimbing/Atasan</a></li>
				<li><a href="http://ppmb.unair.ac.id/files/SURAT%20PERNYATAAN%20SP3%20DOKTOR.pdf" target="_blank">(S3) Surat Pernyataan SP3 yang telah Diisi dan Bermaterai</a></li>
				<li><a href="http://ppmb.unair.ac.id/files/FORM%20MATRIKULASI%202015.pdf" target="_blank">(S3) Form Matrikulasi</a></li>
				<li><a href="{base_url('assets/pdf/FORM-PMILHAN-KELAS-2015.pdf')}" target="_blank">(S3, Khusus prodi S3 Ilmu Kesehatan dan S3 Kedokteran) Form Pilihan Kelas</a></li>
			</ul><hr>
			<ul>
				<li><a href="http://ppmb.unair.ac.id/files/Surat%20Pernyataan%20PEND%20ONLINE.pdf" target="_blank">(Profesi) Surat Pernyataan Kebenaran Pengisian Biodata Pendaftaran Online dan File yang diupload</a></li>
				<li><a href="http://ppmb.unair.ac.id/files/SURAT%20PERNYATAAN%20PPAK.pdf" target="_blank">(Profesi) Surat Pernyataan SP3 yang telah Diisi dan Bermaterai</a></li>
				<li><a href="http://ppmb.unair.ac.id/files/FORM%20MATRIKULASI%202015.pdf" target="_blank">(Profesi) Form Matrikulasi</a></li>
			</ul><hr>
			<ul>
				<li><a href="http://ppmb.unair.ac.id/files/Surat%20Pernyataan%20PEND%20ONLINE.pdf" target="_blank">(PPDS) Surat Pernyataan Kebenaran Pengisian Biodata Pendaftaran Online dan File yang diupload</a></li>
				<li><a href="http://ppmb.unair.ac.id/files/FORM_PPDS.pdf" target="_blank">(PPDS) Form Pendaftaran Spesialis dan Riwayat Hidup</a></li>
				<li><a href="http://ppmb.unair.ac.id/files/surat-pernyataan-sp3-ppds-2014.pdf" target="_blank">(PPDS) Surat Pernyataan SP3 yang telah Diisi dan Bermaterai</a></li>
			</ul><hr>
			<ul>
				<li><a href="http://ppmb.unair.ac.id/files/Surat%20Pernyataan%20PEND%20ONLINE.pdf" target="_blank">(PPDGS) Surat Pernyataan Kebenaran Pengisian Biodata Pendaftaran Online dan File yang diupload</a></li>
				<li><a href="{base_url('assets/pdf/FORM-PPDGS.pdf')}" target="_blank">(PPDGS) Form Pendaftaran Spesialis dan Riwayat Hidup</a></li>
			</ul><hr>  -->
		</div>
	</div>
{/block}