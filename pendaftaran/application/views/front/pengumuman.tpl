{extends file='front_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-lg-12">

			<div class="page-header text-center" style="margin-top: 80px">
				<h1>Pengumuman</h1>
			</div>

			<form action="{current_url()}" method="get" class="form-horizontal">
				<div class="form-group">
					<div class="col-md-offset-4 col-lg-offset-5 col-md-4 col-lg-2">
						<input type="text" class="form-control" name="voucher" placeholder="Kode Voucher" value="{if !empty($smarty.get.voucher)}{$smarty.get.voucher}{/if}">
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-offset-4 col-lg-offset-5 col-md-4 col-lg-2">
						<input type="submit" value="Lihat Hasil" class="btn btn-primary" style="width: 100%"/>
					</div>
				</div>
			</form>
					
			{if $result == 'VOUCHER_NOT_FOUND'}
				<div class="panel panel-info">
					<div class="panel-heading">Hasil</div>
					<div class="panel-body">
						<h4>Kode Voucher tidak ditemukan. Silahkan ulangi pencarian</h4>
					</div>
				</div>
			{/if}
			
			{if $result == 'BELUM_PENGUMUMAN'}
				<div class="panel panel-info">
					<div class="panel-heading">Hasil</div>
					<div class="panel-body">
						<h4>Pengumuman hasil penerimaan akan diumumkan pada : <span class="label label-default">{$jadwal_penerimaan->TGL_PENGUMUMAN|date_format:"%H:%M"} tanggal {$jadwal_penerimaan->TGL_PENGUMUMAN|date_format:"%d %B %Y"}</span></h4>
					</div>
				</div>
			{/if}
			
			{if $result == 'DITERIMA'}
				<div class="panel panel-info">
					<div class="panel-heading">Hasil</div>
					<div class="panel-body">
						<h4>Selamat {$cmb->NM_C_MHS}, anda diterima di program studi : {$cmb->PROGRAM_STUDI->NM_JENJANG} {$cmb->PROGRAM_STUDI->NM_PROGRAM_STUDI}</h4>
					</div>
				</div>
			{/if}
			
			{if $result == 'TIDAK_DITERIMA'}
				<div class="panel panel-info">
					<div class="panel-heading">Hasil</div>
					<div class="panel-body">
						<h4>Mohon maaf {$cmb->NM_C_MHS}, anda tidak termasuk dalam daftar diterima</h4>
					</div>
				</div>
			{/if}

		</div>
	</div>
{/block}