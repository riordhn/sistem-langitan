<html lang="en">
	<head>
		<title>Sedang perbaikan sistem - Pendaftaran Online Mahasiswa Baru Universitas Airlangga</title>
		<meta name="description" content="Pendaftaran online mahasiswa baru Universitas Airlangga" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">

		<!-- Maintenance Style -->
		<link href="assets/css/maintenance.css" rel="stylesheet" />

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body onload="on_Load();">

		<div class="site-wrapper">

			<div class="site-wrapper-inner">

				<div class="cover-container">

					<div class="masthead clearfix">
						<div class="inner">
							<h3 class="masthead-brand">Pendaftaran Online</h3>
							<ul class="nav masthead-nav">
								<li><a href="http://ppmb.unair.ac.id">PPMB Universitas Airlangga</a></li>
							</ul>
						</div>
					</div>

					<div class="inner cover">
						<h1 class="cover-heading">Sistem dalam perbaikan</h1>
						<p class="lead">Mohon maaf atas ketidaknyamanan ini. Kami sedang dalam proses penyempurnaan sistem agar lebih baik lagi. Kami berharap proses ini tidak mengganggu proses pendaftaran Anda.</p>
						<p class="lead">Sistem akan aktif kembali beberapa saat lagi.</p>
						<p>Terima kasih</p>
					</div>

					<div class="mastfoot">
						<div class="inner">
							<p>© {date('Y')} - DSI Universitas Airlangga</p>

						</div>
					</div>

				</div>

			</div>

		</div>

		<script type="text/javascript">
			function on_Load() {
				setInterval(function() {
					var curDate = new Date();
					var currentTime = 
						curDate.getFullYear() + '-' + (curDate.getMonth() + 1) + '-' + curDate.getDate() + ' ' +
						curDate.getHours() + ':' + curDate.getMinutes() + ':' + curDate.getSeconds();
					var targetTime = '{date('Y-n-j H:i:s', $target_time)}';
					
					if (currentTime === targetTime) {
						window.location.href = 'index.php';
					}
					
				}, 1000);
			}
		</script>
	</body>
	<!-- {$smarty.server.REMOTE_ADDR} -->
	<!-- {$smarty.server.HTTP_USER_AGENT} -->
</html>