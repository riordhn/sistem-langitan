{extends file='front_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">

			<div class="page-header">
				<h2>Login Manual</h2>
			</div>

			<form class="form-horizontal" role="form" action="{site_url('auth/login_manual')}" method="post">
				
				{if !empty($error_message)}
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					{$error_message}
				</div>
				{/if}
				
				<div class="form-group">
					<label for="kode_voucher" class="col-sm-2 control-label">Kode Voucher</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" autocomplete="off" autofocus="true" id="kode_voucher" name="kode_voucher" placeholder="Kode voucher" value="{set_value('kode_voucher')}">
					</div>
				</div>
					
				<div class="form-group">
					<label for="pin_voucher" class="col-sm-2 control-label">Pin Voucher</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" autocomplete="off" id="pin_voucher" name="pin_voucher" placeholder="Pin voucher">
					</div>
				</div>
					
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-primary">Login</button>
						
					</div>
				</div>
					
			</form>

		</div>
	</div>
{/block}
