{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Pemilihan Program Penerimaan</h2>
			</div>
			
			<div class="alert alert-success">
				<h4>Anda sudah memilih jenjang penerimaan.</h4>
				<p><a href="{site_url('form/isi')}">Klik disini</a> untuk melanjutkan ke pengisian form.</p>
			</div>
		</div>
	</div>
{/block}