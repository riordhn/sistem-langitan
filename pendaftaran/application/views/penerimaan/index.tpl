{extends file='home_layout.tpl'}
{block name='head'}
	<style>
		/* Panel Sarjana */
		.panel-jenjang-1 { border-color: green; }
		.panel-jenjang-1 > .panel-heading {
			color: white;
			background-color: green;
			border-color: green;
		}
		
		/* Panel Magister */
		.panel-jenjang-2 { border-color: yellowgreen; }
		.panel-jenjang-2 > .panel-heading {
			color: black;
			background-color: yellowgreen;
			border-color: yellowgreen;
		}
		
		/* Panel Doktor */
		.panel-jenjang-3 { border-color: royalblue; }
		.panel-jenjang-3 > .panel-heading {
			color: white;
			background-color: royalblue;
			border-color: royalblue;
		}
		
		/* Panel D4 */
		.panel-jenjang-4 { border-color: darkgreen; }
		.panel-jenjang-4 > .panel-heading {
			color: white;
			background-color: darkgreen;
			border-color: darkgreen;
		}
		
		/* Panel D3 */
		.panel-jenjang-5 { border-color: salmon; }
		.panel-jenjang-5 > .panel-heading {
			color: black;
			background-color: salmon;
			border-color: salmon;
		}
		
		/* Panel Profesi */
		.panel-jenjang-9 { border-color: darkslateblue; }
		.panel-jenjang-9 > .panel-heading {
			color: white;
			background-color: darkslateblue;
			border-color: darkslateblue;
		}
		
		/* Panel Spesialis */
		.panel-jenjang-10 { border-color: darkblue; }
		.panel-jenjang-10 > .panel-heading {
			color: white;
			background-color: darkblue;
			border-color: darkblue;
		}
	</style>
{/block}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Pemilihan Jenjang Seleksi Penerimaan</h2>
			</div>
			
			<div class="alert alert-info">
				<p>Sebelum melakukan pengisian formulir, Anda diwajibkan untuk memilih jenjang penerimaan yang akan di tuju.</p>
			</div>
			
			{foreach $jenjang_penerimaan_set as $jenjang}
				{if count($jenjang->penerimaan_set) > 0}
					<div class="panel panel-jenjang-{$jenjang->ID_JENJANG}">
						<div class="panel-heading"><strong>{$jenjang->NM_JENJANG_IJASAH}</strong></div>
						<table class="table table-striped table-hover table-condensed">
							<thead>
								<tr>
									<th>#</th>
									<th>Program Penerimaan</th>
									<th style="width: 20%">Jadwal Pendaftaran</th>
									<th style="width: 20%">Status</th>
									<th style="width: 10%">Aksi</th>
								</tr>
							</thead>
							<tbody>
								{foreach $jenjang->penerimaan_set as $p}
									{* Shorting logic *}
									{$is_aktif = $p->TIME_AWAL_REGISTRASI <= $current_time and $current_time <= $p->TIME_AKHIR_REGISTRASI}
									{$is_lewat = $p->TIME_AKHIR_REGISTRASI < $current_time}
									<tr {if !empty($p->ID_C_MHS)}{if $p->ID_C_MHS == $id_c_mhs_aktif}class='success'{/if}{/if}>
										<td>{if $is_aktif}{$p@index + 1}{else if $is_lewat}<em>{$p@index + 1}</em>{/if}</td>
										<td>
											{if $is_aktif}
												Penerimaan {$p->NM_PENERIMAAN} Gelombang {$p->GELOMBANG} Tahun {$p->TAHUN}
											{else if $is_lewat}
												<em>Penerimaan {$p->NM_PENERIMAAN} Gelombang {$p->GELOMBANG} Tahun {$p->TAHUN}</em>
											{/if}
										</td>
										<td>
											{if $is_aktif}
												<label>{$p->TGL_AWAL_REGISTRASI|date_format:"d M Y"}</label> s.d. <label>{$p->TGL_AKHIR_REGISTRASI|date_format:"d M Y"}</label>
											{else if $is_lewat}
												<em><span style="display:inline-block">{$p->TGL_AWAL_REGISTRASI|date_format:"d M Y"}</span> s.d. <span style="display:inline-block">{$p->TGL_AKHIR_REGISTRASI|date_format:"d M Y"}</span></em>
											{/if}
										</td>
										<td>
											{if $is_aktif}
												<span class="label label-primary" style="font-size: 0.9em">PENDAFTARAN DIBUKA !</span>
											{else if $is_lewat}
												<span class="label label-default" style="font-size: 0.9em">Pendaftaran sudah ditutup</span>
											{/if}
										</td>
										<td>
											{if empty($p->ID_C_MHS)}
												<a class="btn {if $id_c_mhs_aktif == ''}btn-primary{else}btn-primary{/if} btn-sm" href="{site_url('penerimaan/pilih')}/{$p->ID_PENERIMAAN}">Daftar</a>
											{else if $p->ID_C_MHS != $id_c_mhs_aktif}
												<a class="btn btn-default btn-sm" href="{site_url('penerimaan/aktifkan')}/{$p->ID_C_MHS}">Aktifkan</a>	
											{else}
												<a class="btn btn-success btn-sm" href='{site_url('form/isi')}'>Isi Form</a>
											{/if}
										</td>
									</tr>
								{/foreach}
							</tbody>
						</table>
						{if $jenjang->ID_JENJANG == 1}
							<!-- Informasi untuk Sarjana -->
							<p class="small"><em>* Harap diperhatikan: Pendaftaran program <u>ALIH JENIS</u> untuk <u>lulusan D3</u>. Penerimaan <u>Sarjana Jalur Mandiri</u> untuk <u>lulusan SLTA atau setara</u>.</em></p>
						{/if}
					</div>
				{/if}
			{/foreach}
			
			
		</div>
	</div>
{/block}