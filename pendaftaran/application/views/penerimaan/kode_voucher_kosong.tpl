{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Pemilihan Program Penerimaan</h2>
			</div>
			
			<div class="alert alert-danger">
				<h4>Proses setting pendaftaran belum siap.</h4>
				<p>Kode kesalahan : 'KODE_VOUCHER_KOSONG'</p>
				<p>Silahkan menghubungi panitia pendaftaran atau PPMB Universitas Airlangga.</p>
				<p><a class="alert-link" href='{site_url('penerimaan')}'>Klik untuk kembali</a></p>
			</div>
		</div>
	</div>
{/block}