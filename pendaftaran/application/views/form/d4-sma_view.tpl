{extends file='home_layout.tpl'}
{block name='head'}
	<style type="text/css">
	.control-date-day {
		display:inline-block;
		width: 80px;
	}
	.control-date-month {
		display:inline-block;
		width: 150px;
	}
	.control-date-year {
		display:inline-block; 
		width: 80px;
	}
	.control-input-number {
		width: 80px;
	}

	.glyphicon.glyphicon-remove.form-control-feedback {
		cursor: pointer;
	}
	
	/* help-block hack */
	span.help-block.small {
		color: #737373 !important;
	}
	</style>
{/block}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Pengisian Formulir Pendaftaran Diploma 4 (lulusan SMA)</h2>
			</div>
			
			<div class="alert alert-success">
				<h4>Anda sudah mengisi form pendaftaran !</h4>
				<p><a href="{site_url('form/upload')}">Klik disini</a> untuk melanjutkan ke upload berkas.</p>
				{if $cmb->TGL_VERIFIKASI_PPMB == '' or $cmb->ID_PENERIMAAN==212}
				<p><a href="{site_url('form/reset_form')}">Klik disini</a> untuk mengedit kembali form pendaftaran.</p>
				{/if}
			</div>

			<form class="form-horizontal" role="form">
				<fieldset>
					
					<!-- Form Name -->
					<h3>Biodata</h3>
					<hr />
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label">Jenis Formulir</label>
						<div class="col-md-4">
							<p class="form-control-static"><strong>{$cmb->VOUCHER_TARIF->DESKRIPSI} </strong></p>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="nm_c_mhs">Nama Lengkap</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmb->NM_C_MHS}</p>
						</div>
					</div>

					<!-- Multiple Radios -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="jenis_kelamin">Jenis Kelamin</label>
						<div class="col-md-4">
							<p class="form-control-static">{if $cmb->JENIS_KELAMIN == 1}Laki-laki{else}Perempuan{/if}</p>
						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="id_kota_lahir">Tempat Lahir</label>  
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $kota_set as $kota}{if set_select('id_kota_lahir', $kota->ID_KOTA, ($kota->ID_KOTA == $cmb->ID_KOTA_LAHIR)) != ''}{$kota->NM_KOTA}{/if}{/foreach}
							</p>
						</div>
					</div>

					<!-- Select Date Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="tgl_lahir">Tanggal Lahir</label>
						<div class="col-md-6">
							<p class="form-control-static">{date('d F Y', strtotime($cmb->TGL_LAHIR))}</p>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="alamat">Alamat Tinggal</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmb->ALAMAT}</p>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="id_kota">Kota</label>  
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $kota_set as $kota}{if set_select('id_kota', $kota->ID_KOTA, ($kota->ID_KOTA == $cmb->ID_KOTA)) != ''}{$kota->NM_KOTA}{/if}{/foreach}
							</p>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="telp">No Telp</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmb->TELP}</p>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="email">Email</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmb->EMAIL}</p>
						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="kewarganegaraan">Kewarganegaraan</label>
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $kewarganegaraan_set as $kewarganegaraan}{if set_select('kewarganegaraan', $kewarganegaraan->ID_KEWARGANEGARAAN, ($kewarganegaraan->ID_KEWARGANEGARAAN == $cmb->KEWARGANEGARAAN)) != ''}{$kewarganegaraan->NM_KEWARGANEGARAAN}{/if}{/foreach}
							</p>
						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="id_agama">Agama</label>
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $agama_set as $agama}{if set_select('id_agama', $agama->ID_AGAMA, ($agama->ID_AGAMA == $cmb->ID_AGAMA)) != ''}{$agama->NM_AGAMA}{/if}{/foreach}
							</p>
						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="sumber_biaya">Sumber Biaya</label>
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $sumber_biaya_set as $sumber_biaya}{if set_select('sumber_biaya', $sumber_biaya->ID_SUMBER_BIAYA, ($sumber_biaya->ID_SUMBER_BIAYA == $cmb->SUMBER_BIAYA)) != ''}{$sumber_biaya->NM_SUMBER_BIAYA}{/if}{/foreach}
							</p>
						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="id_disabilitas">Disabilitas / Difabel</label>
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $disabilitas_set as $disabilitas}{if set_select('id_disabilitas', $disabilitas->ID_DISABILITAS, ($disabilitas->ID_DISABILITAS == $cmb->ID_DISABILITAS)) != ''}{$disabilitas->NM_DISABILITAS}{/if}{/foreach}
							</p>
						</div>
					</div>

					<h3>Pendidikan Sebelumnya</h3>
					<hr/>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="nisn">NISN</label>  
						<div class="col-md-3">
							<p class="form-control-static">{$cms->NISN}</p>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="id_kota_sekolah">Kota Sekolah</label>  
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $kota_set as $kota}{if set_select('id_kota_sekolah', $kota->ID_KOTA, ($kota->ID_KOTA == $cms->ID_KOTA_SEKOLAH)) != ''}{$kota->NM_KOTA}{/if}{/foreach}
							</p>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="id_sekolah_asal">Asal Sekolah</label>  
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $sekolah_set as $sekolah}{if set_select('id_sekolah_asal', $sekolah->ID_SEKOLAH, ($sekolah->ID_SEKOLAH == $cms->ID_SEKOLAH_ASAL)) != ''}{$sekolah->NM_SEKOLAH}{/if}{/foreach}
							</p>
						</div>
					</div>
					
					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="jurusan_sekolah">Jurusan</label>
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $jurusan_sekolah_set as $jurusan_sekolah}{if set_select('jurusan_sekolah', $jurusan_sekolah->ID_JURUSAN_SEKOLAH, ($jurusan_sekolah->ID_JURUSAN_SEKOLAH == $cms->JURUSAN_SEKOLAH)) != ''}{$jurusan_sekolah->NM_JURUSAN_SEKOLAH}{/if}{/foreach}
							</p>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="tahun_lulus">Tahun Lulus</label>
						<div class="col-md-4">
							<p class="form-control-static">{$cms->TAHUN_LULUS}</p>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="no_ijazah">No Ijazah</label>  
						<div class="col-md-3">
							<p class="form-control-static">{$cms->NO_IJAZAH}</p>
						</div>
					</div>
					
					<!-- Select Date Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="tgl_ijazah">Tanggal Ijazah</label>
						<div class="col-md-6">
							<p class="form-control-static">{date('d F Y', strtotime($cms->TGL_IJAZAH))}</p>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="jumlah_pelajaran_ijazah">Jumlah Pelajaran Ijazah</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cms->JUMLAH_PELAJARAN_IJAZAH}</p>
							<span class="help-block small">Jumlah mata pelajaran yang ada di ijazah</span>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="nilai_ijazah">Nilai Rata-rata Ijazah</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cms->NILAI_IJAZAH}</p>
							<span class="help-block small">Rata-rata nilai pelajaran yang ada di ijazah</span>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="jumlah_pelajaran_uan">Jumlah Pelajaran Ujian Nasional</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cms->JUMLAH_PELAJARAN_UAN}</p>
							<span class="help-block small">Jumlah mata pelajaran yang diujikan secara nasional</span>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="nilai_uan">Nilai Rata-rata UAN</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cms->NILAI_UAN}</p>
							<span class="help-block small">Rata-rata nilai pelajaran yang diujikan secara nasional</span>
						</div>
					</div>

					<h3>Biodata Ayah <small>Orang Tua</small></h3>
					<hr/>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="nama_ayah">Nama Ayah</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmo->NAMA_AYAH}</p>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="alamat_ayah">Alamat Tinggal</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmo->ALAMAT_AYAH}</p>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="telp_ayah">No Telp</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmo->TELP_AYAH}</p>
						</div>
					</div>
					
					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="pendidikan_ayah">Pendidikan Terakhir</label>
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $pendidikan_ortu_set as $pendidikan_ortu}{if set_select('pendidikan_ayah', $pendidikan_ortu->ID_PENDIDIKAN_ORTU, ($pendidikan_ortu->ID_PENDIDIKAN_ORTU == $cmo->PENDIDIKAN_AYAH)) != ''}{$pendidikan_ortu->NM_PENDIDIKAN_ORTU}{/if}{/foreach}
							</p>
						</div>
					</div>
					
					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="pekerjaan_ayah">Pekerjaan</label>
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $pekerjaan_ortu_set as $pekerjaan_ortu}{if set_select('pekerjaan_ayah', $pekerjaan_ortu->ID_PEKERJAAN, ($pekerjaan_ortu->ID_PEKERJAAN == $cmo->PEKERJAAN_AYAH)) != ''}{$pekerjaan_ortu->NM_PEKERJAAN}{/if}{/foreach}
							</p>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="penghasilan_ayah">Penghasilan Ayah</label>  
						<div class="col-md-4">
							<p class="form-control-static">Rp {$cmo->PENGHASILAN_AYAH}</p>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="instansi_ayah">Instansi / Perusahaan</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmo->INSTANSI_AYAH}</p>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="jabatan_ayah">Jabatan</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmo->JABATAN_AYAH}</p>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="masa_kerja_ayah">Masa Kerja</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmo->MASA_KERJA_AYAH} tahun</p>
						</div>
					</div>
					
					<h3>Biodata Ibu <small>Orang Tua</small></h3>
					<hr/>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="nama_ibu">Nama Ibu</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmo->NAMA_IBU}</p>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="alamat_ibu">Alamat Tinggal</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmo->ALAMAT_IBU}</p>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="telp_ibu">No Telp</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmo->TELP_IBU}</p>
						</div>
					</div>
					
					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="pendidikan_ibu">Pendidikan Terakhir</label>
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $pendidikan_ortu_set as $pendidikan_ortu}{if set_select('pendidikan_ibu', $pendidikan_ortu->ID_PENDIDIKAN_ORTU, ($pendidikan_ortu->ID_PENDIDIKAN_ORTU == $cmo->PENDIDIKAN_IBU)) != ''}{$pendidikan_ortu->NM_PENDIDIKAN_ORTU}{/if}{/foreach}
							</p>
						</div>
					</div>
					
					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="pekerjaan_ibu">Pekerjaan</label>
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $pekerjaan_ortu_set as $pekerjaan_ortu}{if set_select('pekerjaan_ibu', $pekerjaan_ortu->ID_PEKERJAAN, ($pekerjaan_ortu->ID_PEKERJAAN == $cmo->PEKERJAAN_IBU)) != ''}{$pekerjaan_ortu->NM_PEKERJAAN}{/if}{/foreach}
							</p>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="penghasilan_ibu">Penghasilan Ibu</label>  
						<div class="col-md-4">
							<p class="form-control-static">Rp {$cmo->PENGHASILAN_IBU}</p>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="instansi_ibu">Instansi / Perusahaan</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmo->INSTANSI_IBU}</p>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="jabatan_ibu">Jabatan</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmo->JABATAN_IBU}</p>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="masa_kerja_ibu">Masa Kerja</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmo->MASA_KERJA_IBU} tahun</p>
						</div>
					</div>
					
					<h3>Pilihan Program Studi</h3>
					<hr/>
					
					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="id_pilihan_1">Pilihan 1</label>
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $program_studi_set as $program_studi}{if set_select('id_pilihan_1', $program_studi->ID_PROGRAM_STUDI, ($program_studi->ID_PROGRAM_STUDI == $cmb->ID_PILIHAN_1)) != ''}{$program_studi->NM_PROGRAM_STUDI}{/if}{/foreach}
							</p>
						</div>
					</div>
					
					{if $template.jumlah_pilihan >= 2}
					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="id_pilihan_2">Pilihan 2</label>
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $program_studi_set as $program_studi}{if set_select('id_pilihan_2', $program_studi->ID_PROGRAM_STUDI, ($program_studi->ID_PROGRAM_STUDI == $cmb->ID_PILIHAN_2)) != ''}{$program_studi->NM_PROGRAM_STUDI}{/if}{/foreach}
							</p>
						</div>
					</div>
					{/if}
					
					{if $template.jumlah_pilihan >= 3}
					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="id_pilihan_3">Pilihan 3</label>
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $program_studi_set as $program_studi}{if set_select('id_pilihan_3', $program_studi->ID_PROGRAM_STUDI, ($program_studi->ID_PROGRAM_STUDI == $cmb->ID_PILIHAN_3)) != ''}{$program_studi->NM_PROGRAM_STUDI}{/if}{/foreach}
							</p>
						</div>
					</div>
					{/if}
					
					{if $template.jumlah_pilihan >= 4}
					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="id_pilihan_4">Pilihan 4</label>
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $program_studi_set as $program_studi}{if set_select('id_pilihan_4', $program_studi->ID_PROGRAM_STUDI, ($program_studi->ID_PROGRAM_STUDI == $cmb->ID_PILIHAN_4)) != ''}{$program_studi->NM_PROGRAM_STUDI}{/if}{/foreach}
							</p>
						</div>
					</div>
					{/if}
					
					<h3>Persetujuan Pernyataan</h3>
					<hr/>
					
					<!-- Label only-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="">Persetujuan pernyataan</label>  
						<div class="col-md-6">
							<label class="checkbox-inline" for="is_setuju_pernyataan">
								<input type="checkbox" name="is_setuju_pernyataan" id="is_setuju_pernyataan" value="1" checked="checked" disabled="disabled">
								Setuju
							</label>
							<p class="text-primary"><strong>Saya telah memahami peraturan yang diberlakukan oleh Universitas Airlangga tentang pendaftaran mahasiswa baru.
								Apabila dikemudian hari saya diketahui melakukan kecurangan terhadap data diri saya sebagai syarat pendaftaran,
								maka saya siap menerima konsekuensi pembatalan sebagai calon mahasiswa baru Universitas Airlangga.</strong>
							</p>
							<span class="help-block">Pernyataan ini harus disertai berkas tanda tangan diatas materai Rp 6.000. Surat pernyataan bisa didownload pada saat upload berkas.</span>
						</div>
					</div>

				</fieldset>
			</form>
					
		</div>
	</div>
{/block}