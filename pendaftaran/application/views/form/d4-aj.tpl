{extends file='home_layout.tpl'}
{block name='head'}
	<style type="text/css">
		.control-date-day {
			display:inline-block;
			width: 80px;
		}
		.control-date-month {
			display:inline-block;
			width: 150px;
		}
		.control-date-year {
			display:inline-block; 
			width: 80px;
		}
		.control-input-number {
			width: 80px;
		}

		.glyphicon.glyphicon-remove.form-control-feedback {
			cursor: pointer;
		}

		/* help-block hack */
		span.help-block.small {
			color: #737373 !important;
		}
	</style>
{/block}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Pengisian Formulir Pendaftaran Program Diploma 4 (Alih Jenis)</h2>
			</div>

			{if not $validation_complete}
				<div class="alert alert-warning">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Isian belum lengkap !</strong>
					Terdapat data isian yang belum lengkap atau belum sesuai, silahkan periksa kembali.
				</div>
			{/if}

			<form class="form-horizontal" role="form" action="{current_url()}" method="post">
				<fieldset>

					<!-- Form Name -->
					<h3>Biodata</h3>
					<hr />

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label">Jenis Formulir</label>  
						<div class="col-md-4">
							<p class="form-control-static"><strong>{$cmb->VOUCHER_TARIF->DESKRIPSI} </strong>
								{if $cmb->TGL_VERIFIKASI_PPMB == ''}
								<script type="text/javascript">
									function gantiForm() {
										var confirmResult = confirm('Mengganti jenis formulir akan membatalkan semua isian pilihan program studi. Apakah Anda yakin akan mengganti formulir ?');
										if (confirmResult) {
											window.location = '{site_url('form/reset_kode_jurusan')}';
										}
									}
								</script>
								<a href="javascript: gantiForm();" class="label label-primary">Ganti Formulir</a>
								{/if}
							</p>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group {if form_error('nm_c_mhs') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="nm_c_mhs">Nama Lengkap</label>  
						<div class="col-md-4">
							<input id="nm_c_mhs" name="nm_c_mhs" type="text" placeholder="Nama lengkap sesuai ijazah / akte kelahiran" class="form-control" value='{set_value('nm_c_mhs', $cmb->NM_C_MHS)}'>
							{form_error('nm_c_mhs')}
						</div>
					</div>
					<!-- Text input-->
					<div class="form-group {if form_error('gelar') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="gelar">Gelar</label>  
						<div class="col-md-4">
							<input id="gelar" name="gelar" type="text" maxlength="10" placeholder="Gelar" class="form-control" value='{set_value('gelar', $cmb->GELAR)}'>
							{form_error('gelar')}
						</div>
					</div>

					<!-- Multiple Radios -->
					<div class="form-group {if form_error('jenis_kelamin') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="jenis_kelamin">Jenis Kelamin</label>
						<div class="col-md-4">
							<div class="radio">
								<label for="jenis_kelamin-0">
									<input type="radio" name="jenis_kelamin" id="jenis_kelamin-0" value="1" {set_radio('jenis_kelamin', '1', ($cmb->JENIS_KELAMIN == 1))}>
									Laki-Laki
								</label>
							</div>
							<div class="radio">
								<label for="jenis_kelamin-1">
									<input type="radio" name="jenis_kelamin" id="jenis_kelamin-1" value="2" {set_radio('jenis_kelamin', '2', ($cmb->JENIS_KELAMIN == 2))}>
									Perempuan
								</label>
							</div>
							{form_error('jenis_kelamin')}
						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group {if form_error('id_kota_lahir') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="id_kota_lahir">Tempat Lahir</label>  
						<div class="col-md-4">
							<select id="id_kota_lahir" name="id_kota_lahir" class="form-control">
								<option></option>
								{foreach $kota_set as $kota}
									<option value="{$kota->ID_KOTA}" {set_select('id_kota_lahir', $kota->ID_KOTA, ($kota->ID_KOTA == $cmb->ID_KOTA_LAHIR))}>{$kota->NM_KOTA}</option>
								{/foreach}
							</select>
							{form_error('id_kota_lahir')}
						</div>
					</div>

					{$tgl_lahir_err = (form_error('tgl_lahir_Day') != '') or (form_error('tgl_lahir_Month') != '') or (form_error('tgl_lahir_Year') != '')}
					<!-- Select Date Basic -->
					<div class="form-group {if $tgl_lahir_err}has-error{/if}">
						<label class="col-md-2 control-label" for="tgl_lahir">Tanggal Lahir</label>
						<div class="col-md-6">
							{html_select_date 
								prefix='tgl_lahir_'
								time=$cmb->TGL_LAHIR
								field_order='DMY' 
								day_extra='class="form-control control-date-day"' day_empty=""
								month_extra='class="form-control control-date-month"' month_empty=""
								year_extra='class="form-control control-date-year"' year_empty=""
								year_as_text='true'}
							{if $tgl_lahir_err}<span class="help-block">Tanggal lahir harus dilengkapi</span>{/if}
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group {if form_error('alamat') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="alamat">Alamat Tinggal</label>  
						<div class="col-md-4">
							<input id="alamat" name="alamat" type="text" placeholder="Alamat tinggal" class="form-control" value="{set_value('alamat', $cmb->ALAMAT)}">
							{form_error('alamat')}
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group {if form_error('id_kota') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="id_kota">Kota</label>  
						<div class="col-md-4">
							<select id="id_kota" name="id_kota" class="form-control">
								<option></option>
								{foreach $kota_set as $kota}
									<option value="{$kota->ID_KOTA}" {set_select('id_kota', $kota->ID_KOTA, ($kota->ID_KOTA == $cmb->ID_KOTA))}>{$kota->NM_KOTA}</option>
								{/foreach}
							</select>
							{form_error('id_kota')}
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group {if form_error('telp') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="telp">No Telp</label>  
						<div class="col-md-4">
							<input id="telp" name="telp" type="text" placeholder="No telp / no hp" class="form-control" value='{set_value('telp', $cmb->TELP)}'>
							{form_error('telp')}
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="email">Email</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmb->EMAIL}</p>
						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group {if form_error('kewarganegaraan') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="kewarganegaraan">Kewarganegaraan</label>
						<div class="col-md-4">
							<select id="kewarganegaraan" name="kewarganegaraan" class="form-control">
								<option></option>
								{foreach $kewarganegaraan_set as $kewarganegaraan}
									<option value="{$kewarganegaraan->ID_KEWARGANEGARAAN}" {set_select('kewarganegaraan', $kewarganegaraan->ID_KEWARGANEGARAAN, ($kewarganegaraan->ID_KEWARGANEGARAAN == $cmb->KEWARGANEGARAAN))}>{$kewarganegaraan->NM_KEWARGANEGARAAN}</option>
								{/foreach}
							</select>
							{form_error('kewarganegaraan')}
						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group {if form_error('id_agama') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="id_agama">Agama</label>
						<div class="col-md-4">
							<select id="id_agama" name="id_agama" class="form-control">
								<option></option>
								{foreach $agama_set as $agama}
									<option value="{$agama->ID_AGAMA}" {set_select('id_agama', $agama->ID_AGAMA, ($agama->ID_AGAMA == $cmb->ID_AGAMA))}>{$agama->NM_AGAMA}</option>
								{/foreach}
							</select>
							{form_error('id_agama')}
						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group {if form_error('sumber_biaya') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="sumber_biaya">Sumber Biaya</label>
						<div class="col-md-4">
							<select id="sumber_biaya" name="sumber_biaya" class="form-control">
								<option></option>
								{foreach $sumber_biaya_set as $sumber_biaya}
									<option value="{$sumber_biaya->ID_SUMBER_BIAYA}" {set_select('sumber_biaya', $sumber_biaya->ID_SUMBER_BIAYA, ($sumber_biaya->ID_SUMBER_BIAYA == $cmb->SUMBER_BIAYA))}>{$sumber_biaya->NM_SUMBER_BIAYA}</option>
								{/foreach}
							</select>
							{form_error('sumber_biaya')}
						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group {if form_error('id_disabilitas') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="id_disabilitas">Disabilitas / Difabel</label>
						<div class="col-md-4">
							<select id="id_disabilitas" name="id_disabilitas" class="form-control">
								<option></option>
								{foreach $disabilitas_set as $disabilitas}
									<option value="{$disabilitas->ID_DISABILITAS}" {set_select('id_disabilitas', $disabilitas->ID_DISABILITAS, ($disabilitas->ID_DISABILITAS == $cmb->ID_DISABILITAS))}>{$disabilitas->NM_DISABILITAS}</option>
								{/foreach}
							</select>
							{form_error('id_disabilitas')}
						</div>
					</div>

					<h3>Pendidikan Sebelumnya (D3)</h3>
					<hr/>

					<!-- Text input-->
					<div class="form-group {if form_error('ptn_s1') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="ptn_s1">Perguruan Tinggi</label>  
						<div class="col-md-4">
							<input id="ptn_s1" name="ptn_s1" type="text" placeholder="Perguruan tinggi asal" class="form-control" value="{set_value('ptn_s1', $cmp->PTN_S1)}">
							{form_error('ptn_s1')}
						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group {if form_error('status_ptn_s1') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="status_ptn_s1">Status Perguruan Tinggi</label>
						<div class="col-md-4">
							<select id="status_ptn_s1" name="status_ptn_s1" class="form-control">
								<option value=""></option>
								<option value="1" {set_select('status_ptn_s1', 1, ($cmp->STATUS_PTN_S1 == 1))}>Negeri</option>
								<option value="2" {set_select('status_ptn_s1', 2, ($cmp->STATUS_PTN_S1 == 2))}>Swasta</option>
							</select>
							{form_error('status_ptn_s1')}
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group {if form_error('prodi_s1') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="prodi_s1">Program Studi</label>  
						<div class="col-md-4">
							<input id="prodi_s1" name="prodi_s1" type="text" placeholder="Program Studi" class="form-control input-md" value="{set_value('prodi_s1', $cmp->PRODI_S1)}">
							{form_error('prodi_s1')}
						</div>
					</div>
						
					<!-- Select Basic -->
					<div class="form-group {if form_error('jenis_akreditasi_s1') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="jenis_akreditasi_s1">Akreditasi</label>
						<div class="col-md-4">
							<select id="jenis_akreditasi_s1" name="jenis_akreditasi_s1" class="form-control">
								<option value=""></option>
								<option value="BAN-PT" {set_select('jenis_akreditasi_s1', 'BAN-PT', ($cmp->JENIS_AKREDITASI_S1 == 'BAN-PT'))}>BAN-PT</option>
								<option value="Kemenkes" {set_select('jenis_akreditasi_s1', 'Kemenkes', ($cmp->JENIS_AKREDITASI_S1 == 'Kemenkes'))}>Kemenkes</option>
								<option value="Lain" {set_select('jenis_akreditasi_s1', 'Lain', ($cmp->JENIS_AKREDITASI_S1 == 'Lain'))}>Lainnya</option>
							</select>
							{form_error('jenis_akreditasi_s1')}
						</div>
					</div>
						
					<!-- Select Basic -->
					<div class="form-group {if form_error('peringkat_akreditasi_s1') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="peringkat_akreditasi_s1">Peringkat Akreditasi</label>
						<div class="col-md-4">
							<select id="peringkat_akreditasi_s1" name="peringkat_akreditasi_s1" class="form-control">
								<option value=""></option>
								<option value="A" {set_select('peringkat_akreditasi_s1', 'A', ($cmp->PERINGKAT_AKREDITASI_S1 == 'A'))}>A</option>
								<option value="B" {set_select('peringkat_akreditasi_s1', 'B', ($cmp->PERINGKAT_AKREDITASI_S1 == 'B'))}>B</option>
								<option value="C" {set_select('peringkat_akreditasi_s1', 'C', ($cmp->PERINGKAT_AKREDITASI_S1 == 'C'))}>C</option>
								<option value="Lain" {set_select('peringkat_akreditasi_s1', 'Lain', ($cmp->PERINGKAT_AKREDITASI_S1 == 'Lain'))}>Lainnya</option>
							</select>
							{form_error('peringkat_akreditasi_s1')}
						</div>
					</div>

					{$tgl_masuk_s1_err = (form_error('tgl_masuk_s1_Day') != '') or (form_error('tgl_masuk_s1_Month') != '') or (form_error('tgl_masuk_s1_Year') != '')}
					<!-- Select Date Basic -->
					<div class="form-group {if $tgl_masuk_s1_err}has-error{/if}">
						<label class="col-md-2 control-label" for="tgl_masuk_s1">Tanggal Masuk</label>
						<div class="col-md-6">
							{html_select_date 
								prefix='tgl_masuk_s1_'
								time=$cmp->TGL_MASUK_S1
								field_order='DMY' 
								day_extra='class="form-control control-date-day"' day_empty=""
								month_extra='class="form-control control-date-month"' month_empty=""
								year_extra='class="form-control control-date-year"' year_empty=""
								year_as_text='true'}
							{if $tgl_masuk_s1_err}<span class="help-block">Tanggal masuk harus dilengkapi</span>{/if}
						</div>
					</div>

					{$tgl_lulus_s1_err = (form_error('tgl_lulus_s1_Day') != '') or (form_error('tgl_lulus_s1_Month') != '') or (form_error('tgl_lulus_s1_Year') != '')}
					<!-- Select Date Basic -->
					<div class="form-group {if $tgl_lulus_s1_err}has-error{/if}">
						<label class="col-md-2 control-label" for="tgl_lulus_s1">Tanggal Lulus</label>
						<div class="col-md-6">
							{html_select_date 
								prefix='tgl_lulus_s1_'
								time=$cmp->TGL_LULUS_S1
								field_order='DMY' 
								day_extra='class="form-control control-date-day"' day_empty=""
								month_extra='class="form-control control-date-month"' month_empty=""
								year_extra='class="form-control control-date-year"' year_empty=""
								year_as_text='true'}
							{if $tgl_lulus_s1_err}<span class="help-block">Tanggal lulus harus dilengkapi</span>{/if}
						</div>
					</div>

					<!-- Appended Input-->
					<div class="form-group {if form_error('lama_studi_s1') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="lama_studi_s1">Lama Studi</label>
						<div class="col-md-2">
							<div class="input-group">
								<input id="lama_studi_s1" name="lama_studi_s1" class="form-control" placeholder="" type="text" value="{set_value('lama_studi_s1', $cmp->LAMA_STUDI_S1)}">
								<span class="input-group-addon">tahun</span>
							</div>
							{form_error('lama_studi_s1')}
						</div>
					</div>
					<!-- Text input-->
					<div class="form-group {if form_error('ip_s1') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="ip_s1">Index Prestasi</label>  
						<div class="col-md-4">
							<input id="ip_s1" name="ip_s1" type="text" placeholder="IP" class="form-control input-md control-input-number" value="{set_value('ip_s1', $cmp->IP_S1)}">
							{form_error('ip_s1')}
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="nim_lama_s1">NIM</label>  
						<div class="col-md-2">
							<input id="nim_lama_s1" name="nim_lama_s1" type="text" placeholder="NIM" class="form-control input-md" value="{set_value('nim_lama_s1', $cmp->NIM_LAMA_S1)}">
							<span class="help-block small">* Khusus lulusan Universitas Airlangga</span>  
						</div>
					</div>
							
					<!-- Text input-->
					<div class="form-group {if form_error('jumlah_karya_ilmiah') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="jumlah_karya_ilmiah">Jumlah Karya Ilmiah</label>  
						<div class="col-md-4">
							<input id="jumlah_karya_ilmiah" name="jumlah_karya_ilmiah" type="text" placeholder="" class="form-control input-md control-input-number" value="{set_value('jumlah_karya_ilmiah', $cmp->JUMLAH_KARYA_ILMIAH)}">
							{form_error('jumlah_karya_ilmiah')}
						</div>
					</div>


					<h3>Data Pekerjaan</h3>
					<hr/>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="pekerjaan">Pekerjaan</label>  
						<div class="col-md-4">
							<input name="pekerjaan" class="form-control input-md" id="pekerjaan" type="text" placeholder="Pekerjaan saat ini" value="{set_value('pekerjaan', $cmp->PEKERJAAN)}">
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="asal_instansi">Asal instansi</label>  
						<div class="col-md-4">
							<input name="asal_instansi" class="form-control input-md" id="asal_instansi" type="text" placeholder="Asal instansi / tempat kerja" value="{set_value('asal_instansi', $cmp->ASAL_INSTANSI)}">
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="alamat_instansi">Alamat Instansi</label>  
						<div class="col-md-4">
							<input name="alamat_instansi" class="form-control input-md" id="alamat_instansi" type="text" placeholder="Alamat" value="{set_value('alamat_instansi', $cmp->ALAMAT_INSTANSI)}">
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="telp_instansi">Telp Instansi</label>  
						<div class="col-md-4">
							<input name="telp_instansi" class="form-control input-md" id="telp_instansi" type="text" placeholder="Telp" value="{set_value('telp_instansi', $cmp->TELP_INSTANSI)}">
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="nrp">NIP / NIS / NRP</label>  
						<div class="col-md-4">
							<input name="nrp" class="form-control input-md" id="nrp" type="text" placeholder="NIP / NIS / NRP" value="{set_value('nrp', $cmp->NRP)}">
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="karpeg">Karpeg</label>  
						<div class="col-md-4">
							<input name="karpeg" class="form-control input-md" id="karpeg" type="text" placeholder="Karpeg" value="{set_value('karpeg', $cmp->KARPEG)}">
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="pangkat">Pangkat</label>  
						<div class="col-md-4">
							<input name="pangkat" class="form-control input-md" id="pangkat" type="text" placeholder="Pangkat" value="{set_value('pangkat', $cmp->PANGKAT)}">
						</div>
					</div>

					<h3>Pilihan Program Studi</h3>
					<hr/>
					
					<div class="alert text-danger">
						Harap diperhatikan ! Program Studi yang telah dipilih <strong>tidak bisa dirubah</strong> setelah di verifikasi dan
						urutan pilihan adalah prioritas pilihan.
					</div>
					
					{if $cmb->TGL_VERIFIKASI_PPMB == ''}

						<!-- Select Basic -->
						<div class="form-group {if form_error('id_pilihan_1')}has-error{/if}">
							<label class="col-md-2 control-label" for="id_pilihan_1">Pilihan</label>
							<div class="col-md-4">
								<select id="id_pilihan_1" name="id_pilihan_1" class="form-control">
									<option></option>
									{foreach $program_studi_set as $program_studi}
										<option value="{$program_studi->ID_PROGRAM_STUDI}" {set_select('id_pilihan_1', $program_studi->ID_PROGRAM_STUDI, ($program_studi->ID_PROGRAM_STUDI == $cmb->ID_PILIHAN_1))}>{$program_studi->NM_PROGRAM_STUDI}</option>
									{/foreach}
								</select>
								{form_error('id_pilihan_1')}
							</div>
						</div>
					
					{else}
						
						<!-- Select Basic -->
						<div class="form-group">
							<label class="col-md-2 control-label" for="id_pilihan_1">Pilihan 1</label>
							<div class="col-md-4">
								<input type="hidden" name="id_pilihan_1" value="{$cmb->ID_PILIHAN_1}" />
								<p class="form-control-static">
									{foreach $program_studi_set as $program_studi}{if set_select('id_pilihan_1', $program_studi->ID_PROGRAM_STUDI, ($program_studi->ID_PROGRAM_STUDI == $cmb->ID_PILIHAN_1)) != ''}{$program_studi->NM_PROGRAM_STUDI}{/if}{/foreach}
								</p>
							</div>
						</div>
						
					{/if}

					<h3>Persetujuan Pernyataan</h3>
					<hr/>

					<!-- Label only-->
					<div class="form-group {if form_error('is_setuju_pernyataan')}has-error{/if}">
						<label class="col-md-2 control-label" for="">Persetujuan pernyataan</label>  
						<div class="col-md-6">
							<label class="checkbox-inline" for="is_setuju_pernyataan">
								<input type="checkbox" name="is_setuju_pernyataan" id="is_setuju_pernyataan" value="1">
								Setuju
							</label>
							<p class="text-primary"><strong>Saya telah memahami peraturan yang diberlakukan oleh Universitas Airlangga tentang pendaftaran mahasiswa baru.
									Apabila dikemudian hari saya diketahui melakukan kecurangan terhadap data diri saya sebagai syarat pendaftaran,
									maka saya siap menerima konsekuensi pembatalan sebagai calon mahasiswa baru Universitas Airlangga.</strong>
							</p>
							<span class="help-block">Pernyataan ini harus disertai berkas tanda tangan diatas materai Rp 6.000. Surat pernyataan bisa didownload pada saat upload berkas.</span>
						</div>
					</div>

					<!-- Submit button -->
					<div class="form-group">
						<label class="col-md-2 control-label"></label>  
						<div class="col-md-4">
							<button class="btn btn-primary">Simpan</button>
						</div>
					</div>

				</fieldset>
			</form>

		</div>
	</div>
{/block}

{block name='footer-script'}
	<script type="text/javascript">
		jQuery(function() {
			
			$('#id_kota_sekolah').on('change', function() {
				
				if (this.value !== '')
				{
					/** Clear sekolah */
					$('#id_sekolah_asal option').remove();
					
					/** native $.getJSON sync-mode */
					$.ajax({
						async: false,
						dataType: 'json',
						cache: false,
						url: '{site_url('api/get_sekolah_list')}/' + this.value,
						success: function(data, textStatus, jqXHR) {
							$.each(data, function(index, element) {
								$('#id_sekolah_asal').append('<option value="' + element.ID_SEKOLAH + '">' + element.NM_SEKOLAH + '</option>');
							});
						}
					});
				}
				
			});
			
			$('#id_pilihan_1').on('change', function() {
				
				/** clear other */
				$('#id_pilihan_2 option').remove();
				$('#id_pilihan_3 option').remove();
				$('#id_pilihan_4 option').remove();
				
				var id_pilihan_1 = $(this).val();
				
				/** get program studi **/
				$.ajax({
					async: false,
					dataType: 'json',
					cache: false,
					url: '{site_url('api/get_program_studi_list')}/{$cmb->ID_PENERIMAAN}/{$cmb->KODE_JURUSAN}',
					success: function(data, textStatus, jqXHR) {
						
						/** Pilihan 2 append **/
						$('#id_pilihan_2').append('<option></option>');
						
						$.each(data, function(index, element) {
							
							/** Ditambahkan yg bukan pilihan **/
							if (element.ID_PROGRAM_STUDI !== id_pilihan_1)
							{
								$('#id_pilihan_2').append('<option value="' + element.ID_PROGRAM_STUDI + '">' + element.NM_PROGRAM_STUDI + '</option>');
							}
							
						});
					}
				});
			
			});
			
			$('#id_pilihan_2').on('change', function() { 
				
				$('#id_pilihan_3 option').remove();
				$('#id_pilihan_4 option').remove();
				
				var id_pilihan_1 = $('#id_pilihan_1').val();
				var id_pilihan_2 = $(this).val();
				
				$.ajax({
					async: false,
					dataType: 'json',
					cache: false,
					url: '{site_url('api/get_program_studi_list')}/{$cmb->ID_PENERIMAAN}/{$cmb->KODE_JURUSAN}',
					success: function(data, textStatus, jqXHR) {
						
						/** Pilihan 3 append **/
						$('#id_pilihan_3').append('<option></option>');
						
						$.each(data, function(index, element) {
							
							/** Ditambahkan yg bukan pilihan **/
							if (element.ID_PROGRAM_STUDI !== id_pilihan_1 && element.ID_PROGRAM_STUDI !== id_pilihan_2)
							{
								$('#id_pilihan_3').append('<option value="' + element.ID_PROGRAM_STUDI + '">' + element.NM_PROGRAM_STUDI + '</option>');
							}
							
						});
					}
				});
			});
			
			$('#id_pilihan_3').on('change', function() { 
				
				$('#id_pilihan_4 option').remove();
				
				var id_pilihan_1 = $('#id_pilihan_1').val();
				var id_pilihan_2 = $('#id_pilihan_2').val();
				var id_pilihan_3 = $(this).val();
				
				$.ajax({
					async: false,
					dataType: 'json',
					cache: false,
					url: '{site_url('api/get_program_studi_list')}/{$cmb->ID_PENERIMAAN}/{$cmb->KODE_JURUSAN}',
					success: function(data, textStatus, jqXHR) {
						
						/** Pilihan 4 append **/
						$('#id_pilihan_4').append('<option></option>');
						
						$.each(data, function(index, element) {
							
							/** Ditambahkan yg bukan pilihan **/
							if (element.ID_PROGRAM_STUDI !== id_pilihan_1 && element.ID_PROGRAM_STUDI !== id_pilihan_2 && element.ID_PROGRAM_STUDI !== id_pilihan_3)
							{
								$('#id_pilihan_4').append('<option value="' + element.ID_PROGRAM_STUDI + '">' + element.NM_PROGRAM_STUDI + '</option>');
							}
							
						});
					}
				});
			});
			
		});
	</script>
{/block}