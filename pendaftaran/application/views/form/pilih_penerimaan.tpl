{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2></h2>
			</div>
			
			<div class="alert alert-info">
				<h4>Anda belum memilih penerimaan !</h4>
				<p>Anda belum bisa melakukan pengisian form. <a class="alert-link" href="{site_url('penerimaan')}">Klik disini</a> untuk melakukan pemilihan penerimaan.</p>
			</div>
		</div>
	</div>
{/block}