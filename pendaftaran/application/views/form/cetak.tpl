{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Cetak Biodata {$nm_c_mhs}</h2>
			</div>

			<form class="form-horizontal" role="form" action="{site_url('form/cetak')}" method="post">
				<!-- Submit button -->
				<div class="form-group">
					<label class="col-md-2 control-label"></label>  
					<div class="col-md-4">
						{* request UNULAMPUNG *}
						{if $id_pt != 5}
							{if $tgl_bayar == ''}
								<h3>Silakan Melakukan Pembayaran Formulir Terlebih Dahulu !</h3>
							{else if $cmb->TGL_VERIFIKASI_PPMB == ''}
								<h3>Berkas Anda Belum Terverifikasi !</h3>
							{else}
								<button class="btn btn-primary btn-lg">Cetak Pdf</button>
							{/if}
						{else}
							<button class="btn btn-primary btn-lg">Cetak Pdf</button>
						{/if}	
					</div>
				</div>
			</form> 
		</div>
	</div>
{/block}