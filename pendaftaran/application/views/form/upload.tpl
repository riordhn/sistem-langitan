{extends file='home_layout.tpl'}
{block name='head'}
	<style type="text/css">
		input[type="file"] {
			display: inline;
		}
	</style>
{/block}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Upload Berkas</h2>
			</div>
			
			{* Syarat OK, Submit OK, Verifikasi Belum, Belum Verivied (proses antrian) *}
			{if $is_all_syarat_ok and $cmb->TGL_SUBMIT_VERIFIKASI != '' and $cmb->TGL_VERIFIKASI_PPMB == '' and $is_syarat_verified == 0}
				<div class="alert alert-info">
					<h4>Anda sudah melengkapi berkas persyaratan</h4>
					<p>Setelah proses ini Anda akan menunggu sampai hasil verifikasi keluar</p>
					<p>Status tiap file/berkas yang Anda upload menunjukkan proses verifikasinya</p>
				</div>
			
			{* Syarat OK, Submit OK, Verifikasi Belum, Ada perbaikan*}
			{else if $is_all_syarat_ok and $cmb->TGL_SUBMIT_VERIFIKASI != '' and $cmb->TGL_VERIFIKASI_PPMB == '' and $is_syarat_verified == 2}
				<div class="alert alert-warning">
					<h4>Berkas persyaratan Anda telah diverifikasi, tetapi terdapat berkas yang tidak sesuai.</h4>
					<p>Perhatikan informasi yang diberikan oleh verifikator (dibawah file) sebelum melakukan upload ulang berkas</p>
				</div>
			
			{* Verifikasi OK *}
			{else if $cmb->TGL_VERIFIKASI_PPMB != '' and $is_syarat_verified == 1}
				<div class="alert alert-success">
					<h4>Berkas persyaratan Anda telah selesai diverifikasi !</h4>
					<!-- <p>Anda sudah dapat mengambil kode voucher yang akan dibayarkan di Bank.</p>-->
					<p><a href="{site_url('form/cetak')}">Klik disini</a> untuk mencetak biodata</p>
				</div>
			
			{else}
				<div class="panel panel-default">
					<div class="panel-body">
						<h4>Harap diperhatikan :</h4>
						<ul>
							<li>Semua file yang diupload harus berformat gambar JPG / PNG / BMP / PDF. Upload file selain itu akan ditolak oleh sistem.</li>
							<li>Harap meng-upload semua berkas yang dipersyaratkan. Berkas yang tidak lengkap <strong><u>tidak akan</u></strong> diverifikasi.</li>
							<!-- <li>Untuk mendownload surat pernyataan yang dibutuhkan silahkan <a href="{site_url('front/informasi')}" target="_blank">klik disini</a>.</li> -->
							<li>Nama file akan berubah secara otomatis oleh sistem</li>
						</ul>	
					</div>
				</div>
			{/if}
			
			{* Saat ada error upload *}
			{if !empty($foto_error)}
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					{$foto_error}
				</div>
			{/if}
			
			{* Jika sedang dalam proses, maka tidak boleh di edit *}
			{if $cmb->IS_PROSES_VERIFIKASI == 1}
				<div class="alert alert-info">Dokumen Anda sedang diverifikasi saat ini...</div>
			{else}
				
				<form class="form-horizontal" role="form" action="{current_url()}" method="post" enctype="multipart/form-data">
					<fieldset>

						<!-- Form Name -->
						<h3>Berkas Persyaratan Umum</h3>
						<hr />
						
						{if $cmb->PESAN_VERIFIKATOR != ''}
							<!-- pesan optional -->
							<div class="form-group">
								<label class="col-md-3 control-label">Pesan Petugas Verifikator</label>
								<div class="col-md-6">	
									<span class="help-block small"><span class="text-danger"><font size="4">{$cmb->PESAN_VERIFIKATOR}</font></span></span>
								</div>
							</div>
						{/if}

						<!-- File Button --> 
						<div class="form-group">
							<label class="col-md-3 control-label" for="foto">Pas Foto (<font color="red" size="2">Wajib</font>)</label>
							<div class="col-md-6">
								{if !empty($cmf->FILE_FOTO)}
									<label class="form-control-static">
										<a href="{base_url('files')}/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$cmf->FILE_FOTO}" target="_blank">{$cmf->FILE_FOTO}</a>
										{if $cmb->TGL_SUBMIT_VERIFIKASI != ''}
											{if $cmf->IS_FOTO_VERIFIED == 0}
												<a class="btn btn-default btn-xs" title="Menunggu verifikasi"><span class="glyphicon glyphicon-time"></span></a>
											{else if $cmf->IS_FOTO_VERIFIED == 1}
												<a class="btn btn-success btn-xs" title="Sukses"><span class="glyphicon glyphicon-ok"></span></a>
											{/if}
										{else if $cmb->TGL_SUBMIT_VERIFIKASI == '' and $is_verifikasi == '0'}
											<input type="file" name="file" class="input-file" />
											<input type="hidden" name="file_mode" value="foto" />
											<input type="submit" name="submit" value="Upload" class="btn btn-primary btn-xs" />
											<span class="help-block small">* Format foto harus tegak (<i>portrait</i>), menghadap depan dan berpakaian rapi</span>
										{else if $cmb->TGL_SUBMIT_VERIFIKASI == ''}
											{if $cmf->IS_FOTO_VERIFIED == 0}
												{if $cmf->PESAN_V_FOTO == ''}
													<a class="btn btn-default btn-xs" title="Menunggu verifikasi"><span class="glyphicon glyphicon-time"></span></a>
												{/if}
												<a class="btn btn-danger btn-xs" title="Ganti file" href="javascript:window.location='{site_url('form/del_upload/foto')}';"><span class="glyphicon glyphicon-edit"></span></a>
											{else if $cmf->IS_FOTO_VERIFIED == 1}
												<a class="btn btn-success btn-xs" title="Sukses"><span class="glyphicon glyphicon-ok"></span></a>
											{/if}
										{/if}
									</label>
									{if $cmf->PESAN_V_FOTO != '' and $cmf->IS_FOTO_VERIFIED == 0}
										<span class="help-block small"><span class="text-danger"><font size="4">{$cmf->PESAN_V_FOTO}</span></font></span>
									{/if}
								{else}
									<input type="file" name="file" class="input-file" />
									<input type="hidden" name="file_mode" value="foto" />
									<input type="submit" name="submit" value="Upload" class="btn btn-primary btn-xs" />
									<span class="help-block small">* Format foto harus tegak (<i>portrait</i>), menghadap depan dan berpakaian rapi</span>	
								{/if}
							</div>
						</div>
					</fieldset>
				</form>
				
				{if count($syarat_umum_set) > 0}

					{foreach $syarat_umum_set as $su}

					<form class="form-horizontal" role="form" action="{current_url()}" method="post" enctype="multipart/form-data">
						<fieldset>
							<!-- File Button --> 
							<div class="form-group">
								<label class="col-md-3 control-label" for="file_{$su->ID_SYARAT_PRODI}">{$su->NM_SYARAT}
								{if $su->STATUS_WAJIB==1}
									(<font color="red" size="2">Wajib</font>)
								{/if}
								</label>
								<div class="col-md-6">
									{if !empty($su->FILE_SYARAT)}
										<label class="form-control-static">
											<a href="{base_url('files')}/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$su->FILE_SYARAT}" target="_blank">{$su->FILE_SYARAT}</a>
											{if $cmb->TGL_SUBMIT_VERIFIKASI != ''}
												{if $su->IS_VERIFIED == 0}
													<a class="btn btn-default btn-xs" title="Menunggu verifikasi"><span class="glyphicon glyphicon-time"></span></a>
												{else if $su->IS_VERIFIED == 1}
													<a class="btn btn-success btn-xs" title="Sukses"><span class="glyphicon glyphicon-ok"></span></a>
												{/if}
											{else if $cmb->TGL_SUBMIT_VERIFIKASI == '' and $is_verifikasi == '0'}
												<input type="file" name="file" class="input-file" />
												<input type="hidden" name="file_mode" value="file_syarat" />
												<input type="hidden" name="id_syarat_prodi" value="{$su->ID_SYARAT_PRODI}" />
												<input type="submit" name="submit" value="Upload" class="btn btn-primary btn-xs" />
											{else if $cmb->TGL_SUBMIT_VERIFIKASI == ''}
												{if $su->IS_VERIFIED == 0}
													{if $su->PESAN_VERIFIKATOR == ''}
														<a class="btn btn-default btn-xs" title="Menunggu verifikasi"><span class="glyphicon glyphicon-time"></span></a>
													{/if}
													<a class="btn btn-danger btn-xs" title="Ganti file" href="javascript:window.location='{site_url('form/del_upload/')}/{$su->ID_SYARAT_PRODI}';"><span class="glyphicon glyphicon-edit"></span></a>
												{else if $su->IS_VERIFIED == 1}
													<a class="btn btn-success btn-xs" title="Sukses"><span class="glyphicon glyphicon-ok"></span></a>
												{/if}
											{/if}
										</label>
										{if $su->PESAN_VERIFIKATOR != '' and $su->IS_VERIFIED == 0}
											<span class="help-block small"><span class="text-danger"><font size="4">{$su->PESAN_VERIFIKATOR}</font></span></span>
										{/if}
									{else}
										{if $cmb->TGL_SUBMIT_VERIFIKASI == ''}
											<input type="file" name="file" class="input-file" />
											<input type="hidden" name="file_mode" value="file_syarat" />
											<input type="hidden" name="id_syarat_prodi" value="{$su->ID_SYARAT_PRODI}" />
											<input type="submit" name="submit" value="Upload" class="btn btn-primary btn-xs" />
										{/if}
									{/if}
									{if $su->KETERANGAN != ''}
										<span class="help-block small">* {auto_link($su->KETERANGAN, 'url', TRUE)}</span>
									{/if}
								</div>
							</div>
						</fieldset>
					</form>

					{/foreach}

				{/if}
			
			
				{if count($syarat_prodi_set) > 0}

					{foreach $syarat_prodi_set as $ps}

						{if count($ps->syarat_set) > 0}

							<!-- Form Name -->
							<h4> Syarat Khusus Program Studi {$ps->NM_PROGRAM_STUDI}</h4>
							<hr />

							{foreach $ps->syarat_set as $sp}

							<form class="form-horizontal" role="form" action="{current_url()}" method="post" enctype="multipart/form-data">
								<fieldset>
									<!-- File Button --> 
									<div class="form-group">
										<label class="col-md-3 control-label" for="file_{$sp->ID_SYARAT_PRODI}">{$sp->NM_SYARAT}
										{if $sp->STATUS_WAJIB==1}
											(<font color="red" size="2">Wajib</font>)
										{/if}
										</label>
										<div class="col-md-6">
											{if !empty($sp->FILE_SYARAT)}
												<label class="form-control-static">
													<a href="{base_url('files')}/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$sp->FILE_SYARAT}" target="_blank">{$sp->FILE_SYARAT}</a>
													{if $cmb->TGL_SUBMIT_VERIFIKASI != ''}
														{if $sp->IS_VERIFIED == 0}
															<a class="btn btn-default btn-xs" title="Menunggu verifikasi"><span class="glyphicon glyphicon-time"></span></a>
														{else if $sp->IS_VERIFIED == 1}
															<a class="btn btn-success btn-xs" title="Sukses"><span class="glyphicon glyphicon-ok"></span></a>
														{/if}
													{else if $cmb->TGL_SUBMIT_VERIFIKASI == ''}
														{if $sp->IS_VERIFIED == 0}
															{if $sp->PESAN_VERIFIKATOR == ''}
																<a class="btn btn-default btn-xs" title="Menunggu verifikasi"><span class="glyphicon glyphicon-time"></span></a>
															{/if}
															<a class="btn btn-danger btn-xs" title="Ganti file" href="javascript:window.location='{site_url('form/del_upload/')}/{$sp->ID_SYARAT_PRODI}';"><span class="glyphicon glyphicon-edit"></span></a>
														{else if $sp->IS_VERIFIED == 1}
															<a class="btn btn-success btn-xs" title="Sukses"><span class="glyphicon glyphicon-ok"></span></a>
														{/if}
													{/if}
												</label>
												{if $sp->PESAN_VERIFIKATOR != '' and $sp->IS_VERIFIED == 0}
													<span class="help-block small"><span class="text-danger">{$sp->PESAN_VERIFIKATOR}</span></span>
												{/if}
											{else}
												{if $cmb->TGL_SUBMIT_VERIFIKASI == ''}
													<input type="file" name="file" class="input-file" />
													<input type="hidden" name="file_mode" value="file_syarat" />
													<input type="hidden" name="id_syarat_prodi" value="{$sp->ID_SYARAT_PRODI}" />
													<input type="submit" name="submit" value="Upload" class="btn btn-primary btn-xs" />
												{/if}
											{/if}
											{if $sp->KETERANGAN != ''}
												<span class="help-block small">* {auto_link($sp->KETERANGAN, 'url', TRUE)}</span>
											{/if}
										</div>
									</div>
								</fieldset>
							</form>

							{/foreach}

						{/if}

					{/foreach}

				{/if}
			
				
					<form class="form-horizontal" role="form" action="{current_url()}" method="post">
						
						<fieldset>
							{if $cmb->TGL_SUBMIT_VERIFIKASI == ''}
								{if $is_all_syarat_ok}
									<!-- Button -->
									<div class="form-group">
										<label class="col-md-3 control-label" for="singlebutton"></label>
										<div class="col-md-4">
											<input type="hidden" name="file_mode" value="submit_file" />
											<!-- sementara manual ganti nama tombol khusus umaha -->
											{if $is_verifikasi == '0'}
												<input type="submit" value="Submit" class="btn btn-primary" />
											{else}
												<input type="submit" value="Submit untuk diverifikasi" class="btn btn-primary" />
											{/if}
											<!-- <input type="submit" value="Submit untuk diverifikasi" class="btn btn-primary" /> -->
										</div>
									</div>
								{else}
									<!-- Button -->
									<div class="form-group">
										<label class="col-md-3 control-label" for="singlebutton"></label>
										<div class="col-md-4">
											<!-- sementara manual ganti nama tombol khusus umaha -->
											{if $is_verifikasi == '0'}
												<input type="submit" value="Submit" class="btn btn-primary" disabled />
											{else}
												<input type="submit" value="Submit untuk diverifikasi" class="btn btn-primary" disabled />
											{/if}
										</div>
									</div>
								{/if}
							{* {else if $cmb->TGL_SUBMIT_VERIFIKASI != '' and $cmb->IS_PROSES_VERIFIKASI == '' and $cmb->TGL_VERIFIKASI_PPMB == ''} *}
							{else if $cmb->TGL_SUBMIT_VERIFIKASI != ''}
								<div class="form-group">
									<label class="col-md-3 control-label" for="singlebutton"></label>
									<div class="col-md-4">
										<input type="hidden" name="file_mode" value="cancel_file" />
										<input type="submit" value="Perbaiki berkas" class="btn btn-warning" />
									</div>
								</div>
							{/if}
						</fieldset>
						
						
					</form>
				
				
			{/if}
			
			<hr/>

			<h5>Keterangan :</h5>
			<ul>
				<li><a class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-edit"></span></a> : Ganti File</li>
				<li><a class="btn btn-default btn-xs"><span class="glyphicon glyphicon-time"></span></a> : Menunggu Verifikasi</li>
				<li><a class="btn btn-success btn-xs"><span class="glyphicon glyphicon-ok"></span></a> : Verifikasi Sukses</li>
			</ul>
			
		</div>
	</div>
{/block}