{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>{$page_title}</h2>
			</div>
			
			<div class="alert {$alert_type}">
				<p>Anda belum melakukan submit form pendaftaran. Anda belum bisa melakukan upload berkas. Klik link dibawah ini untuk melakukan pengisian form.</p>

				<p>
					<a class="alert-link" href="{site_url('form/isi')}">Isi formulir pendaftaran</a>
				</p>
			</div>
		</div>
	</div>
{/block}