{extends file='home_layout.tpl'}
{block name='head'}
	<style type="text/css">
		.panel-success>.panel-heading {
		    color: #fff;
		    background-color: #fb030f;
		    border-color: #d6e9c6;
		}
	</style>
{/block}
{block name='body-content'}
	<div class="row">
	</br>
	
	</br>
		<div class="panel panel-success">
			<div class="panel-heading">
				<h3 class="text-center"> INFO NOMOR REKENING TRANSFER PEMBAYARAN </h3>
			</div>
			<div class="panel-body">
				<h2 class="text-center"> Nomor Rekening	: {$nomor_rekening_transfer} </h2>
				<h2 class="text-left"> *) Mohon dicatat baik-baik nomor rekening untuk transfer pembayaran ini.</h2>
				<h2 class="text-left"> <a href="{site_url('form/upload')}">Klik disini</a> untuk upload berkas</h2>
			</div>
		</div>
	</div>
{/block}

{block name='footer-script'}
	{js_widget('twitter')}
{/block}
