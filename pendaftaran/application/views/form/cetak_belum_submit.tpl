{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Cetak Biodata</h2>
			</div>
			
			<div class="alert alert-info">
				<p><span class="glyphicon glyphicon-info-sign"></span> Anda belum melakukan submit form pendaftaran. Anda belum bisa melakukan Cetak Biodata. Klik link dibawah ini untuk melakukan pengisian form.</p>

				<p>
					<a class="alert-link" href="{site_url('form/')}">Isi formulir pendaftaran</a>
				</p>
			</div>
		</div>
	</div>
{/block}