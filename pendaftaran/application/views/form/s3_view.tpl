{extends file='home_layout.tpl'}
{block name='head'}
	<style type="text/css">
		.control-date-day {
			display:inline-block;
			width: 80px;
		}
		.control-date-month {
			display:inline-block;
			width: 150px;
		}
		.control-date-year {
			display:inline-block; 
			width: 80px;
		}
		.control-input-number {
			width: 80px;
		}

		.glyphicon.glyphicon-remove.form-control-feedback {
			cursor: pointer;
		}

		/* help-block hack */
		span.help-block.small {
			color: #737373 !important;
		}
	</style>
{/block}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Pengisian Formulir Pendaftaran Program Doktor (S3)</h2>
			</div>

			<div class="alert alert-success">
				<h4>Anda sudah mengisi form pendaftaran !</h4>
				<p><a class="alert-link" href="{site_url('form/upload')}">Klik disini</a> untuk melanjutkan ke upload berkas.</p>
				{if $cmb->NO_UJIAN == ''}
				<p><a class="alert-link" href="{site_url('form/reset_form')}">Klik disini</a> untuk mengedit kembali form pendaftaran.</p>
				{/if}
			</div>

			<form class="form-horizontal" role="form" action="{current_url()}" method="post">
				<fieldset>

					<!-- Form Name -->
					<h3>Biodata</h3>
					<hr />

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label">Jenis Formulir</label>
						<div class="col-md-4">
							<p class="form-control-static"><strong>{$cmb->VOUCHER_TARIF->DESKRIPSI} </strong></p>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="nm_c_mhs">Nama Lengkap</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmb->NM_C_MHS}</p>
						</div>
					</div>
						
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="gelar">Gelar</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmb->GELAR}</p>
						</div>
					</div>

					<!-- Multiple Radios -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="jenis_kelamin">Jenis Kelamin</label>
						<div class="col-md-4">
							<p class="form-control-static">{if $cmb->JENIS_KELAMIN == 1}Laki-laki{else}Perempuan{/if}</p>
						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="id_kota_lahir">Tempat Lahir</label>  
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $kota_set as $kota}{if set_select('id_kota_lahir', $kota->ID_KOTA, ($kota->ID_KOTA == $cmb->ID_KOTA_LAHIR)) != ''}{$kota->NM_KOTA}{/if}{/foreach}
							</p>
						</div>
					</div>

					<!-- Select Date Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="tgl_lahir">Tanggal Lahir</label>
						<div class="col-md-6">
							<p class="form-control-static">{date('d F Y', strtotime($cmb->TGLLAHIR->TGL_LAHIR))}</p>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="alamat">Alamat Tinggal</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmb->ALAMAT}</p>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="id_kota">Kota</label>  
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $kota_set as $kota}{if set_select('id_kota', $kota->ID_KOTA, ($kota->ID_KOTA == $cmb->ID_KOTA)) != ''}{$kota->NM_KOTA}{/if}{/foreach}
							</p>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="telp">No Telp</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmb->TELP}</p>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="email">Email</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmb->EMAIL}</p>
						</div>
					</div>

					{* REQUEST DIHILANGKAN per 24/09/2014 atas request pak bambang *}
					{*<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="kewarganegaraan">Kewarganegaraan</label>
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $kewarganegaraan_set as $kewarganegaraan}{if set_select('kewarganegaraan', $kewarganegaraan->ID_KEWARGANEGARAAN, ($kewarganegaraan->ID_KEWARGANEGARAAN == $cmb->KEWARGANEGARAAN)) != ''}{$kewarganegaraan->NM_KEWARGANEGARAAN}{/if}{/foreach}
							</p>
						</div>
					</div>*}

					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="id_agama">Agama</label>
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $agama_set as $agama}{if set_select('id_agama', $agama->ID_AGAMA, ($agama->ID_AGAMA == $cmb->ID_AGAMA)) != ''}{$agama->NM_AGAMA}{/if}{/foreach}
							</p>
						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="sumber_biaya">Sumber Biaya</label>
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $sumber_biaya_set as $sumber_biaya}{if set_select('sumber_biaya', $sumber_biaya->ID_SUMBER_BIAYA, ($sumber_biaya->ID_SUMBER_BIAYA == $cmb->SUMBER_BIAYA)) != ''}{$sumber_biaya->NM_SUMBER_BIAYA}{/if}{/foreach}
							</p>
						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="id_disabilitas">Disabilitas / Difabel</label>
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $disabilitas_set as $disabilitas}{if set_select('id_disabilitas', $disabilitas->ID_DISABILITAS, ($disabilitas->ID_DISABILITAS == $cmb->ID_DISABILITAS)) != ''}{$disabilitas->NM_DISABILITAS}{/if}{/foreach}
							</p>
						</div>
					</div>

					<h3>Profil Pendidikan Sarjana (S1)</h3>
					<hr/>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="ptn_s1">Perguruan Tinggi</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmp->PTN_S1}</p>
						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="status_ptn_s1">Status Perguruan Tinggi</label>
						<div class="col-md-4">
							<p class="form-control-static">{if $cmp->STATUS_PTN_S1 == 1}Negeri{else}Swasta{/if}</p>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="prodi_s1">Program Studi</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmp->PRODI_S1}</p>
						</div>
					</div>

					<!-- Select Date Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="tgl_masuk_s1">Tanggal Masuk</label>
						<div class="col-md-6">
							<p class="form-control-static">{date('d F Y', strtotime($cmp->TGL_MASUK_S1))}</p>
						</div>
					</div>

					<!-- Select Date Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="tgl_lulus_s1">Tanggal Lulus</label>
						<div class="col-md-6">
							<p class="form-control-static">{date('d F Y', strtotime($cmp->TGL_LULUS_S1))}</p>
						</div>
					</div>

					<!-- Appended Input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="lama_studi_s1">Lama Studi</label>
						<div class="col-md-2">
							<p class="form-control-static">{$cmp->LAMA_STUDI_S1} tahun</p>
						</div>
					</div>
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="ip_s1">Index Prestasi</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmp->IP_S1}</p>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="nim_lama_s1">NIM</label>  
						<div class="col-md-2">
							<p class="form-control-static">{$cmp->NIM_LAMA_S1}</p>
						</div>
					</div>
						
					<h3>Profil Pendidikan Magister (S2)</h3>
					<hr/>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="ptn_s2">Perguruan Tinggi</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmp->PTN_S2}</p>
						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="status_ptn_s2">Status Perguruan Tinggi</label>
						<div class="col-md-4">
							<p class="form-control-static">{if $cmp->STATUS_PTN_S2 == 1}Negeri{else}Swasta{/if}</p>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="prodi_s2">Program Studi</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmp->PRODI_S2}</p>
						</div>
					</div>

					<!-- Select Date Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="tgl_masuk_s2">Tanggal Masuk</label>
						<div class="col-md-6">
							<p class="form-control-static">{date('d F Y', strtotime($cmp->TGL_MASUK_S2))}</p>
						</div>
					</div>

					<!-- Select Date Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="tgl_lulus_s2">Tanggal Lulus</label>
						<div class="col-md-6">
							<p class="form-control-static">{date('d F Y', strtotime($cmp->TGL_LULUS_S2))}</p>
						</div>
					</div>

					<!-- Appended Input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="lama_studi_s2">Lama Studi</label>
						<div class="col-md-2">
							<p class="form-control-static">{$cmp->LAMA_STUDI_S2} tahun</p>
						</div>
					</div>
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="ip_s2">Index Prestasi</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmp->IP_S2}</p>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="nim_lama_s2">NIM</label>  
						<div class="col-md-2">
							<p class="form-control-static">{$cmp->NIM_LAMA_S2}</p>
						</div>
					</div>
						
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="jumlah_karya_ilmiah">Jumlah Karya Ilmiah</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmp->JUMLAH_KARYA_ILMIAH}</p>
						</div>
					</div>
						
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="nilai_toefl">Nilai TOEFL / ELPT</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmp->NILAI_TOEFL}</p>
						</div>
					</div>


					<h3>Data Pekerjaan</h3>
					<hr/>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="pekerjaan">Pekerjaan</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmp->PEKERJAAN}</p>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="asal_instansi">Asal instansi</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmp->ASAL_INSTANSI}</p>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="alamat_instansi">Alamat Instansi</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmp->ALAMAT_INSTANSI}</p>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="telp_instansi">Telp Instansi</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmp->TELP_INSTANSI}</p>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="nrp">NIP / NIS / NRP</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmp->NRP}</p>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="karpeg">Karpeg</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmp->KARPEG}</p>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="pangkat">Pangkat</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmp->PANGKAT}</p>
						</div>
					</div>

					<h3>Pilihan Program Studi</h3>
					<hr/>

					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="id_pilihan_1">Pilihan</label>
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $program_studi_set as $program_studi}{if set_select('id_pilihan_1', $program_studi->ID_PROGRAM_STUDI, ($program_studi->ID_PROGRAM_STUDI == $cmb->ID_PILIHAN_1)) != ''}{$program_studi->NM_PROGRAM_STUDI}{/if}{/foreach}
							</p>
						</div>
					</div>
					
					{if $cmp->ID_PRODI_MINAT != ''}
					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="id_prodi_minat">Minat Studi</label>
						<div class="col-md-4">
							<p class="form-control-static">
								{foreach $prodi_minat_set as $prodi_minat}{if set_select('id_prodi_minat', $prodi_minat->ID_PRODI_MINAT, ($prodi_minat->ID_PRODI_MINAT == $cmp->ID_PRODI_MINAT)) != ''}{$prodi_minat->NM_PRODI_MINAT}{/if}{/foreach}
							</p>
						</div>
					</div>
					{/if}
					
					{if $cmd->ID_KELOMPOK_BIAYA_1 != ''}
						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-2 control-label" for="kelompok_biaya_1">Kelas Pilihan</label>  
							<div class="col-md-4">
								<p class="form-control-static">{$cmd->KELOMPOK_BIAYA_1->NM_KELOMPOK_BIAYA}</p>
							</div>
						</div>
					{/if}

					<h3>Persetujuan Pernyataan</h3>
					<hr/>

					<!-- Label only-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="">Persetujuan pernyataan</label>  
						<div class="col-md-6">
							<label class="checkbox-inline" for="is_setuju_pernyataan">
								<input type="checkbox" name="is_setuju_pernyataan" id="is_setuju_pernyataan" value="1" checked="checked" disabled="disabled">
								Setuju
							</label>
							<p class="text-primary"><strong>Saya telah memahami peraturan yang diberlakukan oleh Universitas Airlangga tentang pendaftaran mahasiswa baru.
								Apabila dikemudian hari saya diketahui melakukan kecurangan terhadap data diri saya sebagai syarat pendaftaran,
								maka saya siap menerima konsekuensi pembatalan sebagai calon mahasiswa baru Universitas Airlangga.</strong>
							</p>
							<span class="help-block">Pernyataan ini harus disertai berkas tanda tangan diatas materai Rp 6.000. Surat pernyataan bisa didownload pada saat upload berkas.</span>
						</div>
					</div>

				</fieldset>
			</form>

		</div>
	</div>
{/block}