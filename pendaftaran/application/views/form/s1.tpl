{extends file='home_layout.tpl'}
{block name='head'}
	<style type="text/css">
	.control-date-day {
		display:inline-block;
		width: 80px;
	}
	.control-date-month {
		display:inline-block;
		width: 150px;
	}
	.control-date-year {
		display:inline-block; 
		width: 80px;
	}
	.control-input-number {
		width: 80px;
	}

	.glyphicon.glyphicon-remove.form-control-feedback {
		cursor: pointer;
	}
	
	/* help-block hack */
	span.help-block.small {
		color: #737373 !important;
	}
	</style>
{/block}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Pengisian Formulir Pendaftaran Program Sarjana (S1)</h2>
			</div>
			
			{if not $validation_complete}
			<div class="alert alert-warning">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<strong>Isian belum lengkap !</strong>
				Terdapat data isian yang belum lengkap atau belum sesuai, silahkan periksa kembali.
			</div>
			{/if}

			<form class="form-horizontal" role="form" action="{current_url()}" method="post">
				<fieldset>
					
					<!-- Form Name -->
					<h3>Biodata</h3>
					<hr />
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label">Jenis Formulir</label>  
						<div class="col-md-4">
							<p class="form-control-static"><strong>{$cmb->VOUCHER_TARIF->DESKRIPSI} </strong>
								{if $cmb->TGL_VERIFIKASI_PPMB == ''}
								<script type="text/javascript">
									function gantiForm() {
										var confirmResult = confirm('Mengganti jenis formulir akan membatalkan semua isian pilihan program studi. Apakah Anda yakin akan mengganti formulir ?');
										if (confirmResult) {
											window.location = '{site_url('form/reset_kode_jurusan')}';
										}
									}
								</script>
								<a href="javascript: gantiForm();" class="label label-primary">Ganti Formulir</a>
								{/if}
							</p>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group {if form_error('nm_c_mhs') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="nm_c_mhs">Nama Lengkap</label>  
						<div class="col-md-4">
							<input id="nm_c_mhs" name="nm_c_mhs" type="text" placeholder="Nama lengkap sesuai ijazah / akte kelahiran" class="form-control" value='{set_value('nm_c_mhs', $cmb->NM_C_MHS)}'>
							{form_error('nm_c_mhs')}
						</div>
					</div>

					<!-- Multiple Radios -->
					<div class="form-group {if form_error('jenis_kelamin') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="jenis_kelamin">Jenis Kelamin</label>
						<div class="col-md-4">
							<div class="radio">
								<label for="jenis_kelamin-0">
									<input type="radio" name="jenis_kelamin" id="jenis_kelamin-0" value="1" {set_radio('jenis_kelamin', '1', ($cmb->JENIS_KELAMIN == 1))}>
									Laki-Laki
								</label>
							</div>
							<div class="radio">
								<label for="jenis_kelamin-1">
									<input type="radio" name="jenis_kelamin" id="jenis_kelamin-1" value="2" {set_radio('jenis_kelamin', '2', ($cmb->JENIS_KELAMIN == 2))}>
									Perempuan
								</label>
							</div>
							{form_error('jenis_kelamin')}
						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group {if form_error('id_kota_lahir') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="id_kota_lahir">Tempat Lahir</label>  
						<div class="col-md-4">
							<select id="id_kota_lahir" name="id_kota_lahir" class="form-control">
								<option></option>
								{foreach $kota_set as $kota}
									<option value="{$kota->ID_KOTA}" {set_select('id_kota_lahir', $kota->ID_KOTA, ($kota->ID_KOTA == $cmb->ID_KOTA_LAHIR))}>{$kota->NM_KOTA}</option>
								{/foreach}
							</select>
							{form_error('id_kota_lahir')}
						</div>
					</div>

					{$tgl_lahir_err = (form_error('tgl_lahir_Day') != '') or (form_error('tgl_lahir_Month') != '') or (form_error('tgl_lahir_Year') != '')}
					<!-- Select Date Basic -->
					<div class="form-group {if $tgl_lahir_err}has-error{/if}">
						<label class="col-md-2 control-label" for="tgl_lahir">Tanggal Lahir</label>
						<div class="col-md-6">
							{html_select_date 
								prefix='tgl_lahir_'
								time=$cmb->TGL_LAHIR
								field_order='DMY' 
								day_extra='class="form-control control-date-day"' day_empty=""
								month_extra='class="form-control control-date-month"' month_empty=""
								year_extra='class="form-control control-date-year"' year_empty=""
								year_as_text='true'}
							{if $tgl_lahir_err}<span class="help-block">Tanggal lahir harus dilengkapi</span>{/if}
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group {if form_error('alamat') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="alamat">Alamat Tinggal</label>  
						<div class="col-md-4">
							<input id="alamat" name="alamat" type="text" placeholder="Alamat tinggal" class="form-control" value="{set_value('alamat', $cmb->ALAMAT)}">
							{form_error('alamat')}
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group {if form_error('id_kota') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="id_kota">Kota</label>  
						<div class="col-md-4">
							<select id="id_kota" name="id_kota" class="form-control">
								<option></option>
								{foreach $kota_set as $kota}
									<option value="{$kota->ID_KOTA}" {set_select('id_kota', $kota->ID_KOTA, ($kota->ID_KOTA == $cmb->ID_KOTA))}>{$kota->NM_KOTA}</option>
								{/foreach}
							</select>
							{form_error('id_kota')}
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group {if form_error('telp') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="telp">No Telp</label>  
						<div class="col-md-4">
							<input id="telp" name="telp" type="text" placeholder="No telp / no hp" class="form-control" value='{set_value('telp', $cmb->TELP)}'>
							{form_error('telp')}
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="email">Email</label>  
						<div class="col-md-4">
							<p class="form-control-static">{$cmb->EMAIL}</p>
						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group {if form_error('kewarganegaraan') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="kewarganegaraan">Kewarganegaraan</label>
						<div class="col-md-4">
							<select id="kewarganegaraan" name="kewarganegaraan" class="form-control">
								<option></option>
								{foreach $kewarganegaraan_set as $kewarganegaraan}
									<option value="{$kewarganegaraan->ID_KEWARGANEGARAAN}" {set_select('kewarganegaraan', $kewarganegaraan->ID_KEWARGANEGARAAN, ($kewarganegaraan->ID_KEWARGANEGARAAN == $cmb->KEWARGANEGARAAN))}>{$kewarganegaraan->NM_KEWARGANEGARAAN}</option>
								{/foreach}
							</select>
							{form_error('kewarganegaraan')}
						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group {if form_error('id_agama') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="id_agama">Agama</label>
						<div class="col-md-4">
							<select id="id_agama" name="id_agama" class="form-control">
								<option></option>
								{foreach $agama_set as $agama}
									<option value="{$agama->ID_AGAMA}" {set_select('id_agama', $agama->ID_AGAMA, ($agama->ID_AGAMA == $cmb->ID_AGAMA))}>{$agama->NM_AGAMA}</option>
								{/foreach}
							</select>
							{form_error('id_agama')}
						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group {if form_error('sumber_biaya') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="sumber_biaya">Sumber Biaya</label>
						<div class="col-md-4">
							<select id="sumber_biaya" name="sumber_biaya" class="form-control">
								<option></option>
								{foreach $sumber_biaya_set as $sumber_biaya}
									<option value="{$sumber_biaya->ID_SUMBER_BIAYA}" {set_select('sumber_biaya', $sumber_biaya->ID_SUMBER_BIAYA, ($sumber_biaya->ID_SUMBER_BIAYA == $cmb->SUMBER_BIAYA))}>{$sumber_biaya->NM_SUMBER_BIAYA}</option>
								{/foreach}
							</select>
							{form_error('sumber_biaya')}
						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group {if form_error('id_disabilitas') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="id_disabilitas">Disabilitas / Difabel</label>
						<div class="col-md-4">
							<select id="id_disabilitas" name="id_disabilitas" class="form-control">
								<option></option>
								{foreach $disabilitas_set as $disabilitas}
									<option value="{$disabilitas->ID_DISABILITAS}" {set_select('id_disabilitas', $disabilitas->ID_DISABILITAS, ($disabilitas->ID_DISABILITAS == $cmb->ID_DISABILITAS))}>{$disabilitas->NM_DISABILITAS}</option>
								{/foreach}
							</select>
							{form_error('id_disabilitas')}
						</div>
					</div>

					<h3>Pendidikan Sebelumnya</h3>
					<hr/>
					
					<!-- Text input-->
					<div class="form-group {if form_error('nisn') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="nisn">NISN</label>  
						<div class="col-md-3">
							<input id="nisn" name="nisn" type="text" placeholder="Nomer Induk Siswa Nasional" class="form-control" value="{set_value('nisn', $cms->NISN)}">
							{form_error('nisn')}
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group {if form_error('id_kota_sekolah') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="id_kota_sekolah">Kota Sekolah</label>  
						<div class="col-md-4">
							<select id="id_kota_sekolah" name="id_kota_sekolah" class="form-control">
								<option></option>
								{foreach $kota_set as $kota}
									<option value="{$kota->ID_KOTA}" {set_select('id_kota_sekolah', $kota->ID_KOTA, ($kota->ID_KOTA == $cms->ID_KOTA_SEKOLAH))}>{$kota->NM_KOTA}</option>
								{/foreach}
							</select>
							{form_error('id_kota_sekolah')}
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group {if form_error('id_sekolah_asal') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="id_sekolah_asal">Asal Sekolah</label>  
						<div class="col-md-4">
							<select id="id_sekolah_asal" name="id_sekolah_asal" class="form-control">
								<option></option>
								{if $cms->ID_SEKOLAH_ASAL != '' or $cms->ID_KOTA_SEKOLAH != ''}
									{foreach $sekolah_set as $sekolah}
										<option value="{$sekolah->ID_SEKOLAH}" {set_select('id_sekolah_asal', $sekolah->ID_SEKOLAH, ($sekolah->ID_SEKOLAH == $cms->ID_SEKOLAH_ASAL))}>{$sekolah->NM_SEKOLAH}</option>
									{/foreach}
								{/if}
							</select>
							{form_error('id_sekolah_asal')}
						</div>
					</div>
					
					<!-- Select Basic -->
					<div class="form-group {if form_error('jurusan_sekolah') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="jurusan_sekolah">Jurusan</label>
						<div class="col-md-4">
							<select id="jurusan_sekolah" name="jurusan_sekolah" class="form-control">
								<option></option>
								{foreach $jurusan_sekolah_set as $jurusan_sekolah}
									<option value="{$jurusan_sekolah->ID_JURUSAN_SEKOLAH}" {set_select('jurusan_sekolah', $jurusan_sekolah->ID_JURUSAN_SEKOLAH, ($jurusan_sekolah->ID_JURUSAN_SEKOLAH == $cms->JURUSAN_SEKOLAH))}>{$jurusan_sekolah->NM_JURUSAN_SEKOLAH}</option>
								{/foreach}
							</select>
							{form_error('jurusan_sekolah')}
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group {if form_error('tahun_lulus') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="tahun_lulus">Tahun Lulus</label>
						<div class="col-md-4">
							<input id="tahun_lulus" name="tahun_lulus" type="text" class="form-control control-input-number" value="{set_value('tahun_lulus', $cms->TAHUN_LULUS)}">
							{form_error('tahun_lulus')}
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group {if form_error('no_ijazah') != ''}has-error{/if}">
						<label class="col-md-2 control-label" for="no_ijazah">No Ijazah</label>  
						<div class="col-md-3">
							<input id="no_ijazah" name="no_ijazah" type="text" placeholder="Nomer ijazah" class="form-control" value="{set_value('no_ijazah', $cms->NO_IJAZAH)}">
							{form_error('no_ijazah')}
						</div>
					</div>
					
					{$tgl_ijazah_err = (form_error('tgl_ijazah_Day') != '') or (form_error('tgl_ijazah_Month') != '') or (form_error('tgl_ijazah_Year') != '')}
					<!-- Select Date Basic -->
					<div class="form-group {if $tgl_ijazah_err}has-error{/if}">
						<label class="col-md-2 control-label" for="tgl_ijazah">Tanggal Ijazah</label>
						<div class="col-md-6">
							{html_select_date 
								prefix='tgl_ijazah_'
								time=$cms->TGL_IJAZAH
								field_order='DMY' 
								day_extra='class="form-control control-date-day"' day_empty=""
								month_extra='class="form-control control-date-month"' month_empty=""
								year_extra='class="form-control control-date-year"' year_empty=""
								start_year='-2'}
							{if $tgl_ijazah_err}<span class="help-block">Tanggal ijazah harus dilengkapi</span>{/if}
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group {if form_error('jumlah_pelajaran_ijazah')}has-error{/if}">
						<label class="col-md-2 control-label" for="jumlah_pelajaran_ijazah">Jumlah Pelajaran Ijazah</label>  
						<div class="col-md-4">
							<input id="jumlah_pelajaran_ijazah" name="jumlah_pelajaran_ijazah" type="text" placeholder="" class="form-control control-input-number" value="{set_value('jumlah_pelajaran_ijazah', $cms->JUMLAH_PELAJARAN_IJAZAH)}">
							{form_error('jumlah_pelajaran_ijazah')}
							<span class="help-block small">Jumlah mata pelajaran yang ada di ijazah</span>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group {if form_error('nilai_ijazah')}has-error{/if}">
						<label class="col-md-2 control-label" for="nilai_ijazah">Nilai Rata-rata Ijazah</label>  
						<div class="col-md-4">
							<input id="nilai_ijazah" name="nilai_ijazah" type="text" placeholder="" class="form-control control-input-number" value="{set_value('nilai_ijazah', $cms->NILAI_IJAZAH)}">
							{form_error('nilai_ijazah')}
							<span class="help-block small">Rata-rata nilai pelajaran yang ada di ijazah</span>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group {if form_error('jumlah_pelajaran_uan')}has-error{/if}">
						<label class="col-md-2 control-label" for="jumlah_pelajaran_uan">Jumlah Pelajaran Ujian Nasional</label>  
						<div class="col-md-4">
							<input id="jumlah_pelajaran_uan" name="jumlah_pelajaran_uan" type="text" placeholder="" class="form-control control-input-number" value="{set_value('jumlah_pelajaran_uan', $cms->JUMLAH_PELAJARAN_UAN)}">
							{form_error('jumlah_pelajaran_uan')}
							<span class="help-block small">Jumlah mata pelajaran yang diujikan secara nasional</span>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group {if form_error('nilai_uan')}has-error{/if}">
						<label class="col-md-2 control-label" for="nilai_uan">Nilai Rata-rata UAN</label>  
						<div class="col-md-4">
							<input id="nilai_uan" name="nilai_uan" type="text" placeholder="" class="form-control control-input-number" value="{set_value('nilai_uan', $cms->NILAI_UAN)}">
							{form_error('nilai_uan')}
							<span class="help-block small">Rata-rata nilai pelajaran yang diujikan secara nasional</span>
						</div>
					</div>

					<h3>Biodata Ayah <small>Orang Tua</small></h3>
					<hr/>
					
					<!-- Text input-->
					<div class="form-group {if form_error('nama_ayah')}has-error{/if}">
						<label class="col-md-2 control-label" for="nama_ayah">Nama Ayah</label>  
						<div class="col-md-4">
							<input id="nama_ayah" name="nama_ayah" type="text" placeholder="Nama lengkap ayah kandung / wali" class="form-control" value="{set_value('nama_ayah', $cmo->NAMA_AYAH)}">
							{form_error('nama_ayah')}
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group {if form_error('alamat_ayah')}has-error{/if}">
						<label class="col-md-2 control-label" for="alamat_ayah">Alamat Tinggal</label>  
						<div class="col-md-4">
							<input id="alamat_ayah" name="alamat_ayah" type="text" placeholder="Alamat tinggal ayah" class="form-control" value="{set_value('alamat_ayah', $cmo->ALAMAT_AYAH)}">
							{form_error('alamat_ayah')}
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group {if form_error('telp_ayah')}has-error{/if}">
						<label class="col-md-2 control-label" for="telp_ayah">No Telp</label>  
						<div class="col-md-4">
							<input id="telp_ayah" name="telp_ayah" type="text" placeholder="Nomer telp / nomer hp" class="form-control" value="{set_value('telp_ayah', $cmo->TELP_AYAH)}">
							{form_error('telp_ayah')}
						</div>
					</div>
					
					<!-- Select Basic -->
					<div class="form-group {if form_error('pendidikan_ayah')}has-error{/if}">
						<label class="col-md-2 control-label" for="pendidikan_ayah">Pendidikan Terakhir</label>
						<div class="col-md-4">
							<select id="pendidikan_ayah" name="pendidikan_ayah" class="form-control">
								<option></option>
								{foreach $pendidikan_ortu_set as $pendidikan_ortu}
									<option value="{$pendidikan_ortu->ID_PENDIDIKAN_ORTU}" {set_select('pendidikan_ayah', $pendidikan_ortu->ID_PENDIDIKAN_ORTU, ($pendidikan_ortu->ID_PENDIDIKAN_ORTU == $cmo->PENDIDIKAN_AYAH))}>{$pendidikan_ortu->NM_PENDIDIKAN_ORTU}</option>
								{/foreach}
							</select>
							{form_error('pendidikan_ayah')}
						</div>
					</div>
					
					<!-- Select Basic -->
					<div class="form-group {if form_error('pekerjaan_ayah')}has-error{/if}">
						<label class="col-md-2 control-label" for="pekerjaan_ayah">Pekerjaan</label>
						<div class="col-md-4">
							<select id="pekerjaan_ayah" name="pekerjaan_ayah" class="form-control">
								<option></option>
								{foreach $pekerjaan_ortu_set as $pekerjaan_ortu}
									<option value="{$pekerjaan_ortu->ID_PEKERJAAN}" {set_select('pekerjaan_ayah', $pekerjaan_ortu->ID_PEKERJAAN, ($pekerjaan_ortu->ID_PEKERJAAN == $cmo->PEKERJAAN_AYAH))}>{$pekerjaan_ortu->NM_PEKERJAAN}</option>
								{/foreach}
							</select>
							{form_error('pekerjaan_ayah')}
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group {if form_error('penghasilan_ayah')}has-error{/if}">
						<label class="col-md-2 control-label" for="penghasilan_ayah">Penghasilan Ayah</label>  
						<div class="col-md-4">
							<div class="input-group">
								<span class="input-group-addon">Rp</span>
								<input id="penghasilan_ayah" name="penghasilan_ayah" type="text" placeholder="" class="form-control" value="{set_value('penghasilan_ayah', $cmo->PENGHASILAN_AYAH)}">
							</div>
							{form_error('penghasilan_ayah')}
							<span class="help-block small">Ditulis angkanya saja, Contoh: 5000000</span>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="instansi_ayah">Instansi / Perusahaan</label>  
						<div class="col-md-4">
							<input id="instansi_ayah" name="instansi_ayah" type="text" placeholder="Instansi / perusahaan tempat kerja" class="form-control" value="{set_value('instansi_ayah', $cmo->INSTANSI_AYAH)}">
							<span class="help-block small">Apabila sebagai pengusaha pribadi, sebutkan bidang usaha.</span>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="jabatan_ayah">Jabatan</label>  
						<div class="col-md-4">
							<input id="jabatan_ayah" name="jabatan_ayah" type="text" placeholder="Jabatan / golongan ditempat kerja" class="form-control" value="{set_value('jabatan_ayah', $cmo->JABATAN_AYAH)}">
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group {if form_error('masa_kerja_ayah')}has-error{/if}">
						<label class="col-md-2 control-label" for="masa_kerja_ayah">Masa Kerja</label>  
						<div class="col-md-4">
							<div class="input-group" style="width: 150px;">
								<input id="masa_kerja_ayah" name="masa_kerja_ayah" type="text" placeholder="" class="form-control" value="{set_value('masa_kerja_ayah', $cmo->MASA_KERJA_AYAH)}">
								<span class="input-group-addon">tahun</span>
							</div>
							{form_error('masa_kerja_ayah')}
						</div>
					</div>
					
					<h3>Biodata Ibu <small>Orang Tua</small></h3>
					<hr/>
					
					<!-- Text input-->
					<div class="form-group {if form_error('nama_ibu')}has-error{/if}">
						<label class="col-md-2 control-label" for="nama_ibu">Nama Ibu</label>  
						<div class="col-md-4">
							<input id="nama_ibu" name="nama_ibu" type="text" placeholder="Nama lengkap ibu kandung" class="form-control" value="{set_value('nama_ibu', $cmo->NAMA_IBU)}">
							{form_error('nama_ibu')}
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group {if form_error('alamat_ibu')}has-error{/if}">
						<label class="col-md-2 control-label" for="alamat_ibu">Alamat Tinggal</label>  
						<div class="col-md-4">
							<input id="alamat_ibu" name="alamat_ibu" type="text" placeholder="Alamat tinggal ibu" class="form-control" value="{set_value('alamat_ibu', $cmo->ALAMAT_IBU)}">
							{form_error('alamat_ibu')}
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group {if form_error('telp_ibu')}has-error{/if}">
						<label class="col-md-2 control-label" for="telp_ibu">No Telp</label>  
						<div class="col-md-4">
							<input id="telp_ibu" name="telp_ibu" type="text" placeholder="Nomer telp / nomer hp" class="form-control" value="{set_value('telp_ibu', $cmo->TELP_IBU)}">
							{form_error('telp_ibu')}
						</div>
					</div>
					
					<!-- Select Basic -->
					<div class="form-group {if form_error('pendidikan_ibu')}has-error{/if}">
						<label class="col-md-2 control-label" for="pendidikan_ibu">Pendidikan Terakhir</label>
						<div class="col-md-4">
							<select id="pendidikan_ibu" name="pendidikan_ibu" class="form-control">
								<option></option>
								{foreach $pendidikan_ortu_set as $pendidikan_ortu}
									<option value="{$pendidikan_ortu->ID_PENDIDIKAN_ORTU}" {set_select('pendidikan_ibu', $pendidikan_ortu->ID_PENDIDIKAN_ORTU, ($pendidikan_ortu->ID_PENDIDIKAN_ORTU == $cmo->PENDIDIKAN_IBU))}>{$pendidikan_ortu->NM_PENDIDIKAN_ORTU}</option>
								{/foreach}
							</select>
							{form_error('pendidikan_ibu')}
						</div>
					</div>
					
					<!-- Select Basic -->
					<div class="form-group {if form_error('pekerjaan_ibu')}has-error{/if}">
						<label class="col-md-2 control-label" for="pekerjaan_ibu">Pekerjaan</label>
						<div class="col-md-4">
							<select id="pekerjaan_ibu" name="pekerjaan_ibu" class="form-control">
								<option></option>
								{foreach $pekerjaan_ortu_set as $pekerjaan_ortu}
									<option value="{$pekerjaan_ortu->ID_PEKERJAAN}" {set_select('pekerjaan_ibu', $pekerjaan_ortu->ID_PEKERJAAN, ($pekerjaan_ortu->ID_PEKERJAAN == $cmo->PEKERJAAN_IBU))}>{$pekerjaan_ortu->NM_PEKERJAAN}</option>
								{/foreach}
							</select>
							{form_error('pekerjaan_ibu')}
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group {if form_error('penghasilan_ibu')}has-error{/if}">
						<label class="col-md-2 control-label" for="penghasilan_ibu">Penghasilan Ibu</label>  
						<div class="col-md-4">
							<div class="input-group">
								<span class="input-group-addon">Rp</span>
								<input id="penghasilan_ibu" name="penghasilan_ibu" type="text" placeholder="" class="form-control" value="{set_value('penghasilan_ibu', $cmo->PENGHASILAN_IBU)}">
							</div>
							{form_error('penghasilan_ibu')}
							<span class="help-block small">Ditulis angkanya saja, Contoh: 5000000. Apabila tidak bekerja cukup isi 0.</span>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="instansi_ibu">Instansi / Perusahaan</label>  
						<div class="col-md-4">
							<input id="instansi_ibu" name="instansi_ibu" type="text" placeholder="Instansi / perusahaan tempat kerja" class="form-control" value="{set_value('instansi_ibu', $cmo->INSTANSI_IBU)}">
							<span class="help-block small">Apabila sebagai pengusaha pribadi, sebutkan bidang usaha.</span>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-2 control-label" for="jabatan_ibu">Jabatan</label>  
						<div class="col-md-4">
							<input id="jabatan_ibu" name="jabatan_ibu" type="text" placeholder="Jabatan / golongan ditempat kerja" class="form-control" value="{set_value('jabatan_ibu', $cmo->JABATAN_IBU)}">
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group {if form_error('masa_kerja_ibu')}has-error{/if}">
						<label class="col-md-2 control-label" for="masa_kerja_ibu">Masa Kerja</label>  
						<div class="col-md-4">
							<div class="input-group" style="width: 150px;">
								<input id="masa_kerja_ibu" name="masa_kerja_ibu" type="text" placeholder="" class="form-control" value="{set_value('masa_kerja_ibu', $cmo->MASA_KERJA_IBU)}">
								<span class="input-group-addon">tahun</span>
							</div>
							{form_error('masa_kerja_ibu')}
						</div>
					</div>
					
					<h3>Pilihan Program Studi</h3>
					<hr/>
					
					<div class="alert text-danger">
						Harap diperhatikan ! Program Studi yang telah dipilih <strong>tidak bisa dirubah</strong> setelah di verifikasi dan
						urutan pilihan adalah prioritas pilihan.
					</div>
					
					{* Program studi tidak bisa diganti jika telah diverifikasi, karena menyangkut syarat *}
					{if $cmb->TGL_VERIFIKASI_PPMB == '' or $cmb->ID_PENERIMAAN==208}
					
						<!-- Select Basic -->
						<div class="form-group {if form_error('id_pilihan_1')}has-error{/if}">
							<label class="col-md-2 control-label" for="id_pilihan_1">Pilihan 1</label>
							<div class="col-md-4">
								<select id="id_pilihan_1" name="id_pilihan_1" class="form-control">
									<option></option>
									{foreach $program_studi_set as $program_studi}
										<option value="{$program_studi->ID_PROGRAM_STUDI}" {set_select('id_pilihan_1', $program_studi->ID_PROGRAM_STUDI, ($program_studi->ID_PROGRAM_STUDI == $cmb->ID_PILIHAN_1))}>{$program_studi->NM_PROGRAM_STUDI}</option>
									{/foreach}
								</select>
								{form_error('id_pilihan_1')}
							</div>
						</div>

						{if $template.jumlah_pilihan >= 2}
						<!-- Select Basic -->
						<div class="form-group">
							<label class="col-md-2 control-label" for="id_pilihan_2">Pilihan 2</label>
							<div class="col-md-4">
								<select id="id_pilihan_2" name="id_pilihan_2" class="form-control">
									<option></option>
									{foreach $program_studi_set as $program_studi}
										{if $program_studi->ID_PROGRAM_STUDI != $cmb->ID_PILIHAN_1}
										<option value="{$program_studi->ID_PROGRAM_STUDI}" {if $cmb->ID_PILIHAN_2 == $program_studi->ID_PROGRAM_STUDI}selected{/if}>{$program_studi->NM_PROGRAM_STUDI}</option>
										{/if}
									{/foreach}
								</select>
							</div>
						</div>
						{/if}

						{if $template.jumlah_pilihan >= 3}
						<!-- Select Basic -->
						<div class="form-group">
							<label class="col-md-2 control-label" for="id_pilihan_3">Pilihan 3</label>
							<div class="col-md-4">
								<select id="id_pilihan_3" name="id_pilihan_3" class="form-control">
									<option></option>
									{foreach $program_studi_set as $program_studi}
										{if $program_studi->ID_PROGRAM_STUDI != $cmb->ID_PILIHAN_1 and $program_studi->ID_PROGRAM_STUDI != $cmb->ID_PILIHAN_2}
										<option value="{$program_studi->ID_PROGRAM_STUDI}" {if $cmb->ID_PILIHAN_3 == $program_studi->ID_PROGRAM_STUDI}selected{/if}>{$program_studi->NM_PROGRAM_STUDI}</option>
										{/if}
									{/foreach}
								</select>
							</div>
						</div>
						{/if}

						{if $template.jumlah_pilihan >= 4}
						<!-- Select Basic -->
						<div class="form-group">
							<label class="col-md-2 control-label" for="id_pilihan_4">Pilihan 4</label>
							<div class="col-md-4">
								<select id="id_pilihan_4" name="id_pilihan_4" class="form-control">
									<option></option>
									{foreach $program_studi_set as $program_studi}
										{if $program_studi->ID_PROGRAM_STUDI != $cmb->ID_PILIHAN_1 and $program_studi->ID_PROGRAM_STUDI != $cmb->ID_PILIHAN_2 and $program_studi->ID_PROGRAM_STUDI != $cmb->ID_PILIHAN_3}
										<option value="{$program_studi->ID_PROGRAM_STUDI}" {if $cmb->ID_PILIHAN_4 == $program_studi->ID_PROGRAM_STUDI}selected{/if}>{$program_studi->NM_PROGRAM_STUDI}</option>
										{/if}
									{/foreach}
								</select>
							</div>
						</div>
						{/if}
						
					{else}
						
						<!-- Select Basic -->
						<div class="form-group">
							<label class="col-md-2 control-label" for="id_pilihan_1">Pilihan 1</label>
							<div class="col-md-4">
								<input type="hidden" name="id_pilihan_1" value="{$cmb->ID_PILIHAN_1}" />
								<p class="form-control-static">
									{foreach $program_studi_set as $program_studi}{if set_select('id_pilihan_1', $program_studi->ID_PROGRAM_STUDI, ($program_studi->ID_PROGRAM_STUDI == $cmb->ID_PILIHAN_1)) != ''}{$program_studi->NM_PROGRAM_STUDI}{/if}{/foreach}
								</p>
							</div>
						</div>

						{if $template.jumlah_pilihan >= 2}
						<!-- Select Basic -->
						<div class="form-group">
							<label class="col-md-2 control-label" for="id_pilihan_2">Pilihan 2</label>
							<div class="col-md-4">
								<input type="hidden" name="id_pilihan_2" value="{$cmb->ID_PILIHAN_2}" />
								<p class="form-control-static">
									{foreach $program_studi_set as $program_studi}{if set_select('id_pilihan_2', $program_studi->ID_PROGRAM_STUDI, ($program_studi->ID_PROGRAM_STUDI == $cmb->ID_PILIHAN_2)) != ''}{$program_studi->NM_PROGRAM_STUDI}{/if}{/foreach}
								</p>
							</div>
						</div>
						{/if}

						{if $template.jumlah_pilihan >= 3}
						<!-- Select Basic -->
						<div class="form-group">
							<label class="col-md-2 control-label" for="id_pilihan_3">Pilihan 3</label>
							<div class="col-md-4">
								<input type="hidden" name="id_pilihan_3" value="{$cmb->ID_PILIHAN_3}" />
								<p class="form-control-static">
									{foreach $program_studi_set as $program_studi}{if set_select('id_pilihan_3', $program_studi->ID_PROGRAM_STUDI, ($program_studi->ID_PROGRAM_STUDI == $cmb->ID_PILIHAN_3)) != ''}{$program_studi->NM_PROGRAM_STUDI}{/if}{/foreach}
								</p>
							</div>
						</div>
						{/if}

						{if $template.jumlah_pilihan >= 4}
						<!-- Select Basic -->
						<div class="form-group">
							<label class="col-md-2 control-label" for="id_pilihan_4">Pilihan 4</label>
							<div class="col-md-4">
								<input type="hidden" name="id_pilihan_4" value="{$cmb->ID_PILIHAN_4}" />
								<p class="form-control-static">
									{foreach $program_studi_set as $program_studi}{if set_select('id_pilihan_4', $program_studi->ID_PROGRAM_STUDI, ($program_studi->ID_PROGRAM_STUDI == $cmb->ID_PILIHAN_4)) != ''}{$program_studi->NM_PROGRAM_STUDI}{/if}{/foreach}
								</p>
							</div>
						</div>
						{/if}
						
						<p class="text-muted">Program studi tidak bisa diganti karena berkas persyaratan telah diverifikasi.</p>
					
					{/if}
					
					<h3>Persetujuan Pernyataan</h3>
					<hr/>
					
					<!-- Label only-->
					<div class="form-group {if form_error('is_setuju_pernyataan')}has-error{/if}">
						<label class="col-md-2 control-label" for="">Persetujuan pernyataan</label>  
						<div class="col-md-6">
							<label class="checkbox-inline" for="is_setuju_pernyataan">
								<input type="checkbox" name="is_setuju_pernyataan" id="is_setuju_pernyataan" value="1">
								Setuju
							</label>
							<p class="text-primary"><strong>Saya telah memahami peraturan yang diberlakukan oleh Universitas Airlangga tentang pendaftaran mahasiswa baru.
								Apabila dikemudian hari saya diketahui melakukan kecurangan terhadap data diri saya sebagai syarat pendaftaran,
								maka saya siap menerima konsekuensi pembatalan sebagai calon mahasiswa baru Universitas Airlangga.</strong>
							</p>
							<span class="help-block">Pernyataan ini harus disertai berkas tanda tangan diatas materai Rp 6.000. Surat pernyataan bisa didownload pada saat upload berkas.</span>
						</div>
					</div>
					
					<!-- Submit button -->
					<div class="form-group">
						<label class="col-md-2 control-label"></label>  
						<div class="col-md-4">
							<button class="btn btn-primary">Simpan</button>
						</div>
					</div>

				</fieldset>
			</form>
					
		</div>
	</div>
{/block}

{block name='footer-script'}
	<script type="text/javascript">
		jQuery(function() {
			
			$('#id_kota_sekolah').on('change', function() {
				
				if (this.value !== '')
				{
					/** Clear sekolah */
					$('#id_sekolah_asal option').remove();
					
					/** native $.getJSON sync-mode */
					$.ajax({
						async: false,
						dataType: 'json',
						cache: false,
						url: '{site_url('api/get_sekolah_list')}/' + this.value,
						success: function(data, textStatus, jqXHR) {
							$.each(data, function(index, element) {
								$('#id_sekolah_asal').append('<option value="' + element.ID_SEKOLAH + '">' + element.NM_SEKOLAH + '</option>');
							});
						}
					});
				}
				
			});
			
			$('#id_pilihan_1').on('change', function() {
				
				/** clear other */
				$('#id_pilihan_2 option').remove();
				$('#id_pilihan_3 option').remove();
				$('#id_pilihan_4 option').remove();
				
				var id_pilihan_1 = $(this).val();
				
				/** get program studi **/
				$.ajax({
					async: false,
					dataType: 'json',
					cache: false,
					url: '{site_url('api/get_program_studi_list')}/{$cmb->ID_PENERIMAAN}/{$cmb->KODE_JURUSAN}',
					success: function(data, textStatus, jqXHR) {
						
						/** Pilihan 2 append **/
						$('#id_pilihan_2').append('<option></option>');
						
						$.each(data, function(index, element) {
							
							/** Ditambahkan yg bukan pilihan **/
							if (element.ID_PROGRAM_STUDI !== id_pilihan_1)
							{
								$('#id_pilihan_2').append('<option value="' + element.ID_PROGRAM_STUDI + '">' + element.NM_PROGRAM_STUDI + '</option>');
							}
							
						});
					}
				});
			
			});
			
			$('#id_pilihan_2').on('change', function() { 
				
				$('#id_pilihan_3 option').remove();
				$('#id_pilihan_4 option').remove();
				
				var id_pilihan_1 = $('#id_pilihan_1').val();
				var id_pilihan_2 = $(this).val();
				
				$.ajax({
					async: false,
					dataType: 'json',
					cache: false,
					url: '{site_url('api/get_program_studi_list')}/{$cmb->ID_PENERIMAAN}/{$cmb->KODE_JURUSAN}',
					success: function(data, textStatus, jqXHR) {
						
						/** Pilihan 3 append **/
						$('#id_pilihan_3').append('<option></option>');
						
						$.each(data, function(index, element) {
							
							/** Ditambahkan yg bukan pilihan **/
							if (element.ID_PROGRAM_STUDI !== id_pilihan_1 && element.ID_PROGRAM_STUDI !== id_pilihan_2)
							{
								$('#id_pilihan_3').append('<option value="' + element.ID_PROGRAM_STUDI + '">' + element.NM_PROGRAM_STUDI + '</option>');
							}
							
						});
					}
				});
			});
			
			$('#id_pilihan_3').on('change', function() { 
				
				$('#id_pilihan_4 option').remove();
				
				var id_pilihan_1 = $('#id_pilihan_1').val();
				var id_pilihan_2 = $('#id_pilihan_2').val();
				var id_pilihan_3 = $(this).val();
				
				$.ajax({
					async: false,
					dataType: 'json',
					cache: false,
					url: '{site_url('api/get_program_studi_list')}/{$cmb->ID_PENERIMAAN}/{$cmb->KODE_JURUSAN}',
					success: function(data, textStatus, jqXHR) {
						
						/** Pilihan 4 append **/
						$('#id_pilihan_4').append('<option></option>');
						
						$.each(data, function(index, element) {
							
							/** Ditambahkan yg bukan pilihan **/
							if (element.ID_PROGRAM_STUDI !== id_pilihan_1 && element.ID_PROGRAM_STUDI !== id_pilihan_2 && element.ID_PROGRAM_STUDI !== id_pilihan_3)
							{
								$('#id_pilihan_4').append('<option value="' + element.ID_PROGRAM_STUDI + '">' + element.NM_PROGRAM_STUDI + '</option>');
							}
							
						});
					}
				});
			});
			
		});
	</script>
{/block}
