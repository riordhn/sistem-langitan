{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Pengisian Formulir Pendaftaran</h2>
			</div>
			
			<form class="form-horizontal" role="form" action="{current_url()}" method="post">
				<fieldset>
					
					<!-- Form Name -->
					<h3>Formulir</h3>
					<hr />
					
					<p>Sebelum mengisi form, Anda perlu memilih jenis formulir terlebih dahulu</p>
					
					<!-- Select Date Basic -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="kode_jurusan">Jenis Formulir</label>
						<div class="col-sm-6">
							<select name="kode_jurusan" class="form-control">
								{foreach $voucher_tarif_set as $voucher_tarif}
									<option value="{$voucher_tarif->KODE_JURUSAN}">{$voucher_tarif->DESKRIPSI}</option>
								{/foreach}
							</select>
						</div>
					</div>
					
					<!-- Submit button -->
					<div class="form-group">
						<label class="col-md-2 control-label"></label>  
						<div class="col-md-4">
							<input type="submit" class="btn btn-primary" value="Simpan" />
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
{/block}