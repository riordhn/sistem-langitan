{extends file='home_layout.tpl'}
{block name='head'}
	<style type="text/css">
		input[type="file"] {
			display: inline;
		}
	</style>
{/block}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Upload Berkas</h2>
			</div>
			
			<div class="alert alert-info">
				<h4>Batas upload atau perbaikan berkas sudah berakhir</h4>
				<p>Anda sudah tidak dapat melakukan perbaikan berkas / upload berkas lagi</p>
			</div>
			
		</div>
		
	</div>
{/block}