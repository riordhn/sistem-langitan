{extends file='home_layout.tpl'}
{block name='head'}
	<style type="text/css">
	.control-date-day {
		display:inline-block;
		width: 80px;
	}
	.control-date-month {
		display:inline-block;
		width: 150px;
	}
	.control-date-year {
		display:inline-block; 
		width: 80px;
	}
	.control-input-number {
		width: 80px;
	}
	</style>
{/block}
{block name='body-content'}
	<!-- AUTOCOMPLETE -->
	<!-- https://github.com/devbridge/jQuery-Autocomplete -->
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Pengisian Formulir Pendaftaran Sarjana</h2>
			</div>

			<form class="form-horizontal" role="form" action="{current_url()}" method="post">
				<input type="hidden" name="submit-type" value="fill-formulir" />
				<fieldset>

					<!-- Form Name -->
					<h3>Biodata</h3>
					<hr/>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="nm_c_mhs">Nama Lengkap</label>  
						<div class="col-sm-4">
							<input id="nm_c_mhs" name="nm_c_mhs" type="text" placeholder="Nama lengkap sesuai ijazah / akte kelahiran" class="form-control" required value='{set_value('nm_c_mhs', $cmb->NM_C_MHS)}'>
						</div>
					</div>

					<!-- Multiple Radios -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="jenis_kelamin">Jenis Kelamin</label>
						<div class="col-sm-4">
							<div class="radio">
								<label for="jenis_kelamin-0">
									<input type="radio" name="jenis_kelamin" id="jenis_kelamin-0" value="1" {set_radio('jenis_kelamin', '1', ($cmb->JENIS_KELAMIN == 1))} required>
									Laki-Laki
								</label>
							</div>
							<div class="radio">
								<label for="jenis_kelamin-1">
									<input type="radio" name="jenis_kelamin" id="jenis_kelamin-1" value="2" {set_radio('jenis_kelamin', '2', ($cmb->JENIS_KELAMIN == 2))}>
									Perempuan
								</label>
							</div>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="id_kota_lahir">Tempat Lahir</label>  
						<div class="col-sm-4">
							<input id="id_kota_lahir" name="id_kota_lahir" type="text" placeholder="Kota tempat kelahiran" class="form-control" required>
						</div>
					</div>

					<!-- Select Date Basic -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="tgl_lahir">Tanggal Lahir</label>
						<div class="col-sm-6">
							{html_select_date 
								prefix='tgl_lahir_'
								time=$cmb->TGL_LAHIR
								field_order='DMY' 
								day_extra='class="form-control control-date-day" required' day_empty=""
								month_extra='class="form-control control-date-month" required' month_empty=""
								year_extra='class="form-control control-date-year" required' year_empty=""
								year_as_text='true'}
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="alamat">Alamat Tinggal</label>  
						<div class="col-sm-4">
							<input id="alamat" name="alamat" type="text" placeholder="Alamat tinggal" class="form-control" required>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="id_kota">Kota</label>  
						<div class="col-sm-4">
							<select id="id_kota" name="id_kota" class="form-control" data-placeholder="Kota" required>
								<option value="1">Surabaya</option>
								<option value="2">Jakarta</option>
							</select>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="telp">No Telp</label>  
						<div class="col-sm-4">
							<input id="telp" name="telp" type="text" placeholder="No telp / no hp" class="form-control" value='{set_value('telp', $cmb->TELP)}' required>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="email">Email</label>  
						<div class="col-sm-4">
							<p class="form-control-static">{$cmb->EMAIL}</p>
						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="kewarganegaraan">Kewarganegaraan</label>
						<div class="col-sm-4">
							<select id="kewarganegaraan" name="kewarganegaraan" class="form-control" required>
								<option value="1">Option one</option>
								<option value="2">Option two</option>
							</select>
						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="id_agama">Agama</label>
						<div class="col-sm-4">
							<select id="id_agama" name="id_agama" class="form-control" required>
								<option value="1">Option one</option>
								<option value="2">Option two</option>
							</select>
						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="sumber_biaya">Sumber Biaya</label>
						<div class="col-sm-4">
							<select id="sumber_biaya" name="sumber_biaya" class="form-control" required>
								<option value="1">Option one</option>
								<option value="2">Option two</option>
							</select>
						</div>
					</div>

					<!-- Multiple Checkboxes (inline) -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="id_disabilitas">Disabilitas / Difabel</label>
						<div class="col-sm-4">
							<label class="checkbox-inline" for="id_disabilitas-0">
								<input type="checkbox" name="id_disabilitas" id="id_disabilitas-0" value="1">
								Ya
							</label>
							<span class="help-block">Apabila mempunyai disabilitas silahkan dicentang, untuk memudahkan panitia apabila membutuhkan bantuan</span>
						</div>
					</div>

					<h3>Pendidikan Sebelumnya</h3>
					<hr/>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="nisn">NISN</label>  
						<div class="col-sm-3">
							<input id="nisn" name="nisn" type="text" placeholder="Nomer Induk Siswa Nasional" class="form-control" required>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="asal_sekolah">Asal Sekolah</label>  
						<div class="col-sm-4">
							<input id="asal_sekolah" name="asal_sekolah" type="text" placeholder="SMA/MA/SMK Asal" class="form-control" required>
						</div>
					</div>
					
					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="id_jurusan_sekolah">Jurusan Sekolah</label>
						<div class="col-sm-4">
							<select id="id_jurusan_sekolah" name="id_jurusan_sekolah" class="form-control" required>
								<option value="1">Option one</option>
								<option value="2">Option two</option>
							</select>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="tahun_lulus">Tahun Lulus</label>  
						<div class="col-sm-4">
							<input id="tahun_lulus" name="tahun_lulus" type="text" placeholder="" class="form-control control-input-number" required>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="no_ijazah">No Ijazah</label>  
						<div class="col-sm-3">
							<input id="no_ijazah" name="no_ijazah" type="text" placeholder="Nomer ijazah" class="form-control" required>
							<span class="help-block"><a href="#" target="_blank">Klik untuk mengetahui nomer ijazah</a></span>
						</div>
					</div>
					
					<!-- Select Date Basic -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="tgl_ijazah">Tanggal Ijazah</label>
						<div class="col-sm-6">
							{html_select_date 
								prefix='tgl_ijazah_'
								time=NULL
								field_order='DMY' 
								day_extra='class="form-control control-date-day"' day_empty=""
								month_extra='class="form-control control-date-month"' month_empty=""
								year_extra='class="form-control control-date-year"' year_empty=""
								year_as_text='true'}
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="jumlah_pelajaran_ijazah">Mapel Ijazah</label>  
						<div class="col-sm-4">
							<input id="jumlah_pelajaran_ijazah" name="jumlah_pelajaran_ijazah" type="text" placeholder="" class="form-control control-input-number">
							<span class="help-block">Jumlah mata pelajaran yang diujikan secara nasional di ijazah</span>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="nilai_ijazah">Nilai Rata-rata Ijazah</label>  
						<div class="col-sm-4">
							<input id="nilai_ijazah" name="nilai_ijazah" type="text" placeholder="" class="form-control control-input-number">
							<span class="help-block">Rata-rata nilai pelajaran yang diujikan secara nasional di ijazah</span>
						</div>
					</div>

					<h3>Biodata Ayah <small>Orang Tua</small></h3>
					<hr/>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="nama_ayah">Nama Ayah</label>  
						<div class="col-sm-4">
							<input id="nama_ayah" name="nama_ayah" type="text" placeholder="Nama lengkap ayah kandung / wali" class="form-control" required>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="alamat_ayah">Alamat Tinggal</label>  
						<div class="col-sm-4">
							<input id="alamat_ayah" name="alamat_ayah" type="text" placeholder="Alamat tinggal ayah" class="form-control" required>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="telp_ayah">No Telp</label>  
						<div class="col-sm-4">
							<input id="telp_ayah" name="telp_ayah" type="text" placeholder="Nomer telp / nomer hp" class="form-control" required>
						</div>
					</div>
					
					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="pendidikan_ayah">Pendidikan Terakhir</label>
						<div class="col-sm-4">
							<select id="pendidikan_ayah" name="pendidikan_ayah" class="form-control">
								<option value="1">Option one</option>
								<option value="2">Option two</option>
							</select>
						</div>
					</div>
					
					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="pekerjaan_ayah">Pekerjaan</label>
						<div class="col-sm-4">
							<select id="pekerjaan_ayah" name="pekerjaan_ayah" class="form-control">
								<option value="1">Option one</option>
								<option value="2">Option two</option>
							</select>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="penghasilan_ayah">Penghasilan Ayah</label>  
						<div class="col-sm-4">
							<div class="input-group">
								<span class="input-group-addon">Rp</span>
								<input id="penghasilan_ayah" name="penghasilan_ayah" type="text" placeholder="" class="form-control" required>
							</div>
							<span class="help-block">Ditulis angka saja, Contoh: 5000000</span>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="instansi_ayah">Instansi / Perusahaan</label>  
						<div class="col-sm-4">
							<input id="instansi_ayah" name="instansi_ayah" type="text" placeholder="Instansi / perusahaan tempat kerja" class="form-control">
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="jabatan_ayah">Jabatan</label>  
						<div class="col-sm-4">
							<input id="jabatan_ayah" name="jabatan_ayah" type="text" placeholder="Jabatan / golongan ditempat kerja" class="form-control">
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="masa_kerja_ayah">Masa Kerja</label>  
						<div class="col-sm-4">
							<div class="input-group" style="width: 150px;">
								<input id="masa_kerja_ayah" name="masa_kerja_ayah" type="text" placeholder="" class="form-control">
								<span class="input-group-addon">tahun</span>
							</div>
						</div>
					</div>
					
					<h3>Biodata Ibu <small>Orang Tua</small></h3>
					<hr/>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="nama_ibu">Nama Ibu</label>  
						<div class="col-sm-4">
							<input id="nama_ibu" name="nama_ibu" type="text" placeholder="Nama lengkap ibu kandung" class="form-control" required>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="alamat_ibu">Alamat Tinggal</label>  
						<div class="col-sm-4">
							<input id="alamat_ibu" name="alamat_ibu" type="text" placeholder="Alamat tinggal ibu" class="form-control" required>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="telp_ibu">No Telp</label>  
						<div class="col-sm-4">
							<input id="telp_ibu" name="telp_ibu" type="text" placeholder="Nomer telp / nomer hp" class="form-control" required>
						</div>
					</div>
					
					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="pendidikan_ibu">Pendidikan Terakhir</label>
						<div class="col-sm-4">
							<select id="pendidikan_ibu" name="pendidikan_ibu" class="form-control">
								<option value="1">Option one</option>
								<option value="2">Option two</option>
							</select>
						</div>
					</div>
					
					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="pekerjaan_ibu">Pekerjaan</label>
						<div class="col-sm-4">
							<select id="pekerjaan_ibu" name="pekerjaan_ibu" class="form-control">
								<option value="1">Option one</option>
								<option value="2">Option two</option>
							</select>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="penghasilan_ibu">Penghasilan Ibu</label>  
						<div class="col-sm-4">
							<div class="input-group">
								<span class="input-group-addon">Rp</span>
								<input id="penghasilan_ibu" name="penghasilan_ibu" type="text" placeholder="" class="form-control" required>
							</div>
							<span class="help-block">Ditulis angka saja, Contoh: 5000000</span>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="instansi_ibu">Instansi / Perusahaan</label>  
						<div class="col-sm-4">
							<input id="instansi_ibu" name="instansi_ibu" type="text" placeholder="Instansi / perusahaan tempat kerja" class="form-control">
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="jabatan_ibu">Jabatan</label>  
						<div class="col-sm-4">
							<input id="jabatan_ibu" name="jabatan_ibu" type="text" placeholder="Jabatan / golongan ditempat kerja" class="form-control">
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="masa_kerja_ibu">Masa Kerja</label>  
						<div class="col-sm-4">
							<div class="input-group" style="width: 150px;">
								<input id="masa_kerja_ibu" name="masa_kerja_ibu" type="text" placeholder="" class="form-control">
								<span class="input-group-addon">tahun</span>
							</div>
						</div>
					</div>
					
					<h3>Persetujuan Pernyataan</h3>
					<hr/>
					
					<!-- Label only-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="">Persetujuan pernyataan</label>  
						<div class="col-sm-5">
							<label class="checkbox-inline" for="persetujuan_pernyataan">
								<input type="checkbox" name="persetujuan_pernyataan" id="persetujuan_pernyataan" value="1">
								Setuju
							</label>
							<p class="text-primary"><strong>Saya telah memahami peraturan yang berlakukan di Universitas Airlangga tentang pendaftaran mahasiswa baru.
								Apabila dikemudian hari saya diketahui melakukan kecurangan terhadap data diri saya sebagai syarat pendaftaran,
								maka saya siap menerima konsekuensi apapun yang akan diberlakukan oleh Universitas Airlangga kepada saya.</strong>
							</p>
							<span class="help-block">Pernyataan ini harus disertai berkas tanda tangan diatas materai. Surat pernyataan bisa didownload pada saat upload berkas.</span>
						</div>
					</div>
					
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-2 control-label"></label>  
						<div class="col-sm-4">
							<input type="submit" class="btn btn-primary" value="Simpan" />
						</div>
					</div>

				</fieldset>
			</form>

		</div>
	</div>
{/block}