<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penerimaan_Model extends MY_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	function list_all()
	{
		$result = array();

		$this->query = $this->db->get_where('penerimaan', "tgl_awal_registrasi >= '" . date('d-M-Y') . "'");

		foreach ($this->query->result_array() as $row)
		{
			array_push($result, $row);
		}

		return $result;
	}

	function list_aktif_dibuka($id_calon_pendaftar = null)
	{
		$result = array();
		
		// Mendapatkan jenjang set
		$this->query = $this->db
			->select('id_jenjang, nm_jenjang_ijasah')
			->from('jenjang')
			->where('is_jenjang_aktif', 1)
			->get();
			
		// Push ke array
		foreach ($this->query->result() as $row)
		{
			array_push($result, $row);
		}
		
		// Free result
		$this->query->free_result();
		
		// Mengambil data penerimaan tiap jenjang yg ada
		foreach ($result as &$jenjang)
		{
			$jenjang->penerimaan_set = array();
			
			$this->query = $this->db->query(
				"/* Penerimaan yang sudah terpilih*/
				SELECT p.id_penerimaan, p.nm_penerimaan, gelombang, tahun, cmb.id_c_mhs,
					to_char(tgl_awal_registrasi, 'YYYY-MM-DD HH24:MI:SS') as tgl_awal_registrasi,
					to_char(tgl_akhir_registrasi, 'YYYY-MM-DD HH24:MI:SS') as tgl_akhir_registrasi
				FROM aucc.penerimaan p
				JOIN aucc.calon_mahasiswa_baru cmb ON cmb.id_penerimaan = p.id_penerimaan AND cmb.id_calon_pendaftar = {$id_calon_pendaftar}
				WHERE p.id_jenjang = {$jenjang->ID_JENJANG}
				
				UNION ALL
				/* Penerimaan yang aktif */
				SELECT p.id_penerimaan, p.nm_penerimaan, gelombang, tahun, NULL as id_c_mhs,
					to_char(tgl_awal_registrasi, 'YYYY-MM-DD HH24:MI:SS') as tgl_awal_registrasi,
					to_char(tgl_akhir_registrasi, 'YYYY-MM-DD HH24:MI:SS') as tgl_akhir_registrasi
				FROM aucc.penerimaan p
				WHERE 
					p.id_jenjang = {$jenjang->ID_JENJANG} AND
					p.is_aktif = 1 AND
					p.is_pendaftaran_online = 1 and
					SYSDATE BETWEEN p.tgl_awal_registrasi AND p.tgl_akhir_registrasi AND
					p.id_penerimaan not in (select id_penerimaan from aucc.calon_mahasiswa_baru cmb where cmb.id_calon_pendaftar = {$id_calon_pendaftar})
					
				ORDER BY tgl_akhir_registrasi DESC");
				
			// Push ke array
			foreach ($this->query->result() as $penerimaan)
			{
				$penerimaan->TIME_AWAL_REGISTRASI	= date_timestamp_get(date_create_from_format('Y-m-d H:i:s', $penerimaan->TGL_AWAL_REGISTRASI));
				$penerimaan->TIME_AKHIR_REGISTRASI	= date_timestamp_get(date_create_from_format('Y-m-d H:i:s', $penerimaan->TGL_AKHIR_REGISTRASI));
				
				array_push($jenjang->penerimaan_set, $penerimaan);
			}
		}
		
		return $result;
	}

	function list_dev($id_calon_pendaftar = null)
	{
		$result = array();

		$this->query = $this->db
			->select('penerimaan.id_penerimaan, nm_penerimaan, nm_jenjang_ijasah, gelombang, tahun, id_c_mhs')
			->from('penerimaan')
			->join('jenjang', 'jenjang.id_jenjang = penerimaan.id_jenjang')
			->join('calon_mahasiswa_baru', "calon_mahasiswa_baru.id_penerimaan = penerimaan.id_penerimaan AND calon_mahasiswa_baru.id_calon_pendaftar = {$id_calon_pendaftar}", 'LEFT')
			->where_in('penerimaan.id_penerimaan', array(181, 184, 186, 201, 202, 203, 207))
			->get();

		foreach ($this->query->result() as $row)
		{
			array_push($result, $row);
		}

		return $result;
	}
	
	function get_jadwal_penerimaan($id_penerimaan)
	{
		$this->query = $this->db
			->select("to_char(tgl_awal_registrasi, 'YYYY-MM-DD HH24:MI:SS') as tgl_awal_registrasi")
			->select("to_char(tgl_akhir_registrasi, 'YYYY-MM-DD HH24:MI:SS') as tgl_akhir_registrasi")
			->select("to_char(tgl_awal_verifikasi, 'YYYY-MM-DD HH24:MI:SS') as tgl_awal_verifikasi")
			->select("to_char(tgl_akhir_verifikasi, 'YYYY-MM-DD HH24:MI:SS') as tgl_akhir_verifikasi")
			->select("to_char(tgl_pengumuman, 'YYYY-MM-DD HH24:MI:SS') as tgl_pengumuman")
			->where(array('id_penerimaan' => $id_penerimaan))
			->get('penerimaan');
		
		return $this->query->row();
	}
	
	/**
	 * Mendapatkan list file peserta (khusus pasca)
	 * @param int $id_penerimaan
	 */
	function list_file_peserta($id_penerimaan)
	{	
		$this->query = $this->db
			->select('cmb.id_penerimaan, cmb.id_c_mhs, cmb.no_ujian, cms.file_syarat, sp.nm_syarat')
			->from('calon_mahasiswa_baru cmb')
			->join('calon_mahasiswa_syarat cms', 'cms.id_c_mhs = cmb.id_c_mhs')
			->join('penerimaan_syarat_prodi sp', 'sp.id_syarat_prodi = cms.id_syarat_prodi')
			->join('penerimaan p', 'p.id_penerimaan = cmb.id_penerimaan and p.id_jenjang in (2, 3, 9, 10)')
			->where('cmb.tgl_ambil_no_ujian is not null', null, false)
			->where('cmb.id_penerimaan', (int)$id_penerimaan)
			->get();
		
		$result_set = array();
		foreach ($this->query->result() as $row) { array_push($result_set, $row); }
		return $result_set;
	}
	
	/**
	 * Mendapatkan list foto peserta
	 * @param int $id_penerimaan
	 * @return array
	 */
	function list_foto_peserta($id_penerimaan)
	{
		$this->query = $this->db
			->select('cmb.id_c_mhs, cmb.no_ujian, cmf.file_foto')
			->from('calon_mahasiswa_baru cmb')
			->join('calon_mahasiswa_file cmf', 'cmf.id_c_mhs = cmb.id_c_mhs')
			->join('penerimaan p', 'p.id_penerimaan = cmb.id_penerimaan and p.id_jenjang in (2, 3, 9, 10)')
			->where('cmb.tgl_ambil_no_ujian is not null', null, false)
			->where('cmb.id_penerimaan', (int)$id_penerimaan)
			->get();
			
		$result_set = array();
		foreach ($this->query->result() as $row) { array_push($result_set, $row); }
		return $result_set;
	}
}
