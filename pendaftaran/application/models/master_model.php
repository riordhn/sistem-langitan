<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of master_model
 *
 * @author Fathoni <fathoni@staf.unair.ac.id>
 */
class Master_Model extends MY_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	function get_program_studi_list_baru($id_penerimaan)
	{
		$result_array = array();

		$where = array(
				'id_penerimaan' => $id_penerimaan, 
				'pp.is_aktif' => 1
			);
		
		// Mendapatkan jenjang penerimaan
		/*$this->query = $this->db->select('id_jenjang')->get_where('penerimaan', array('id_penerimaan' => $id_penerimaan));
		$id_jenjang = $this->query->row()->ID_JENJANG;*/
		
		// Filter kode jurusan untuk S1 dan D3
		/*if ($id_jenjang == 1 or $id_jenjang == 5)
		{
			if ($kode_jurusan != '03')  // selain IPC
			{
				$where = array(
					'id_penerimaan' => $id_penerimaan, 
					'pp.is_aktif' => 1, 
					'ps.jurusan_sekolah' => $kode_jurusan
				);
			}
			else	// untuk IPC
			{
				$where = array(
					'id_penerimaan' => $id_penerimaan, 
					'pp.is_aktif' => 1
				);
			}
		}
		else	// selain s1 dan d3 tidak dibedakan kode jurusan
		{
			$where = array(
				'id_penerimaan' => $id_penerimaan, 
				'pp.is_aktif' => 1
			);
		}*/
		
		$this->query = $this->db
			->select('ps.id_program_studi, ps.nm_program_studi, j.nm_jenjang')
			->from('penerimaan_prodi pp')
			->join('program_studi ps', 'ps.id_program_studi = pp.id_program_studi', 'left')
			->join('jenjang j', 'j.id_jenjang = ps.id_jenjang', 'left')
			->where($where)
			->order_by('nm_program_studi', 'asc')
			->get();
		
		foreach ($this->query->result() as $row)
		{
			array_push($result_array, $row);
		}
		
		$this->query->free_result();
		
		return $result_array;
	}

	function get_jenis_tinggal_list()
	{
		$result_array = array();
		
		$this->query = $this->db->get('jenis_tinggal');
		
		foreach ($this->query->result() as $row)
		{
			array_push ($result_array, $row);
		}
		
		$this->query->free_result();
		
		return $result_array;
	}
	
	function get_kota_list()
	{
		$result_array = array();
		
		$this->query = $this->db
				->select('k.id_kota, k.nm_kota, p.nm_provinsi, n.nm_negara, tipe_dati2')
				->from('kota k')
				->join('provinsi p', 'p.id_provinsi = k.id_provinsi')
				->join('negara n', 'n.id_negara = p.id_negara')
				->where('k.is_aktif = 1')
				->order_by('2 asc, 3 asc')
				->get();
		
		foreach ($this->query->result() as $row)
		{
			// modify ucwords
			$row->NM_NEGARA		= ucwords(strtolower($row->NM_NEGARA));
			$row->NM_PROVINSI	= ucwords(strtolower($row->NM_PROVINSI));
			$row->NM_KOTA		= ucwords(strtolower($row->NM_KOTA));
			
			// Exclude ucwords untuk DI Yogyakarta, DKI Jakarta
			if ($row->NM_PROVINSI == 'Di Yogyakarta')
				$row->NM_PROVINSI = 'D.I. Yogyakarta';
			else if ($row->NM_PROVINSI == 'Dki Jakarta')
				$row->NM_PROVINSI = 'DKI Jakarta';
			
			// format nama kota
			$row->NM_KOTA			= $row->NM_KOTA . ((strtolower($row->TIPE_DATI2) == "kabupaten") ? ' (Kab.), ' : ', ') . $row->NM_PROVINSI . (($row->NM_NEGARA == 'Indonesia') ? '': ', ' . $row->NM_NEGARA);
			
			// push array
			array_push($result_array, $row);
		}
		
		$this->query->free_result();
		
		return $result_array;
	}
	
	function get_kewarganegaraan_list()
	{
		$result_array = array();
		
		$this->query = $this->db->get('kewarganegaraan');
		
		foreach ($this->query->result() as $row)
		{
			array_push ($result_array, $row);
		}
		
		$this->query->free_result();
		
		return $result_array;
	}
	
	function get_agama_list()
	{
		$result_array = array();
		
		$this->query = $this->db->get('agama');
		
		foreach ($this->query->result() as $row)
		{
			array_push ($result_array, $row);
		}
		
		$this->query->free_result();
		
		return $result_array;
	}
	
	function get_sumber_biaya_list()
	{
		$result_array = array();
		
		$this->query = $this->db->get('sumber_biaya');
		
		foreach ($this->query->result() as $row)
		{
			array_push ($result_array, $row);
		}
		
		$this->query->free_result();
		
		return $result_array;
	}
	
	function get_jurusan_sekolah_list()
	{
		$result_array = array();
		
		$this->query = $this->db->get('jurusan_sekolah_cmhs');
		
		foreach ($this->query->result() as $row)
		{
			array_push ($result_array, $row);
		}
		
		$this->query->free_result();
		
		return $result_array;
	}
	
	function get_pendidikan_ortu_list()
	{
		$result_array = array();
		
		$this->query = $this->db->get('pendidikan_ortu');
		
		foreach ($this->query->result() as $row)
		{
			array_push ($result_array, $row);
		}
		
		$this->query->free_result();
		
		return $result_array;
	}
	
	function get_pekerjaan_ortu_list()
	{
		$result_array = array();
		
		$this->query = $this->db
				->order_by('nm_pekerjaan asc')
				->get_where('pekerjaan', array('STATUS_AKTIF' => 1));
		
		foreach ($this->query->result() as $row)
		{
			array_push ($result_array, $row);
		}
		
		$this->query->free_result();
		
		return $result_array;
	}
	
	function get_disabilitas_list()
	{
		$result_array = array();
		
		$this->query = $this->db->get('disabilitas');
		
		foreach ($this->query->result() as $row)
		{
			array_push ($result_array, $row);
		}
		
		$this->query->free_result();
		
		return $result_array;
	}
	
	function get_sekolah_list($id_kota)
	{
		$result_array = array();
		
		$this->query = $this->db
				->select('id_sekolah, nm_sekolah')
				->order_by('2 asc')
				->get_where('sekolah', array('id_kota' => $id_kota, 'nm_sekolah <>' => 'LAIN-LAIN'));
				/*->get_where('sekolah', array('id_kota' => $id_kota));*/
		
		// echo $this->db->last_query(); exit();
		
		foreach ($this->query->result() as $row)
		{
			array_push($result_array, $row);
		}
		
		$this->query->free_result();
		
		return $result_array;
	}
	
	function get_voucher_tarif_list($id_penerimaan)
	{
		$result_array = array();
		
		$this->query = $this->db
				->get_where('voucher_tarif', array('id_penerimaan' => $id_penerimaan));
		
		foreach ($this->query->result() as $row)
		{
			array_push($result_array, $row);
		}
		
		$this->query->free_result();
		
		return $result_array;
	}
	
	function get_program_studi_list($id_penerimaan, $kode_jurusan)
	{
		$result_array = array();
		
		// Mendapatkan jenjang penerimaan
		$this->query = $this->db->select('id_jenjang')->get_where('penerimaan', array('id_penerimaan' => $id_penerimaan));
		$id_jenjang = $this->query->row()->ID_JENJANG;
		
		// Filter kode jurusan untuk S1 dan D3
		if ($id_jenjang == 1 or $id_jenjang == 5)
		{
			if ($kode_jurusan != '03')  // selain IPC
			{
				$where = array(
					'id_penerimaan' => $id_penerimaan, 
					'pp.is_aktif' => 1, 
					'ps.jurusan_sekolah' => $kode_jurusan
				);
			}
			else	// untuk IPC
			{
				$where = array(
					'id_penerimaan' => $id_penerimaan, 
					'pp.is_aktif' => 1
				);
			}
		}
		else	// selain s1 dan d3 tidak dibedakan kode jurusan
		{
			$where = array(
				'id_penerimaan' => $id_penerimaan, 
				'pp.is_aktif' => 1
			);
		}
		
		$this->query = $this->db
			->select('ps.id_program_studi, ps.nm_program_studi')
			->from('penerimaan_prodi pp')
			->join('program_studi ps', 'ps.id_program_studi = pp.id_program_studi', 'left')
			->where($where)
			->order_by('nm_program_studi', 'asc')
			->get();
		
		foreach ($this->query->result() as $row)
		{
			array_push($result_array, $row);
		}
		
		$this->query->free_result();
		
		return $result_array;
	}
	
	function get_prodi_minat_list($id_penerimaan, $id_program_studi)
	{
		/*
		 * select pm.id_prodi_minat, pm.nm_prodi_minat from penerimaan_prodi_minat ppm
        join prodi_minat pm on PM.ID_PRODI_MINAT = ppm.ID_PRODI_MINAT
        where ppm.id_penerimaan = {$row['ID_PENERIMAAN']} and pm.id_program_studi = {$id_program_studi} and ppm.is_aktif = 1
		 */
		
		$this->query = $this->db
			->select('pm.id_prodi_minat, pm.nm_prodi_minat')
			->from('penerimaan_prodi_minat ppm')
			->join('prodi_minat pm', 'pm.id_prodi_minat = ppm.id_prodi_minat')
			->where(array(
				'ppm.is_aktif'			=> 1,
				'ppm.id_penerimaan'		=> $id_penerimaan,
				'pm.id_program_studi'	=> $id_program_studi
			))
			->order_by('pm.nm_prodi_minat')
			->get();
		
		$result_array = $this->query->result();
		$this->query->free_result();
		
		return $result_array;
	}
	
	function get_kelompok_biaya($id_penerimaan, $id_program_studi)
	{
		$this->query = $this->db
			->select('kb.id_kelompok_biaya, kb.nm_kelompok_biaya')
			->from('biaya_kuliah bk')
			->join('kelompok_biaya kb', 'kb.id_kelompok_biaya = bk.id_kelompok_biaya and kb.id_kelompok_biaya not in (1, 47, 273)')
			->join('penerimaan p', 'p.id_semester = bk.id_semester AND p.id_jalur = bk.id_jalur')
			->where(array(
				'p.id_penerimaan'		=> $id_penerimaan,
				'bk.id_program_studi'	=> $id_program_studi,
				'bk.id_kelompok_biaya !='	=> '277'
			))->get();
		
		$result_array = $this->query->result();
		$this->query->free_result();
		
		foreach ($result_array as &$row)
		{
			// skrip hardcode untuk ubah nama "Kelas Pagi" --> "Kelas Sore" pada MM
			if ($id_penerimaan == 220 && $id_program_studi == 73 && $row->NM_KELOMPOK_BIAYA == "Kelas Pagi")
			{
				$row->NM_KELOMPOK_BIAYA = "Kelas Sore";
			}
		}
		
		return $result_array;
	}
}
