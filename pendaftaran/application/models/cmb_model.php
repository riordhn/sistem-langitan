<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of cmb_model
 *
 * @author Fathoni <fathoni@staf.unair.ac.id>
 */
class Cmb_Model extends MY_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Update data CMB, CMO
	 * @param int $id_c_mhs
	 */
	function update_baru($id_c_mhs)
	{
		$this->db->trans_start();
		
		$where	= array('id_c_mhs' => (int)$id_c_mhs);
		
		// Pre-Proses tanggal lahir
		$tgl_lahir = $this->input->post('tgl_lahir_Year') . '-' . $this->input->post('tgl_lahir_Month') . '-' . $this->input->post('tgl_lahir_Day');
		//echo $tgl_lahir.'<br>';
		$tgl_lahir = date('d-M-y', strtotime($tgl_lahir));
		//echo $tgl_lahir;

		// Pre-Proses tanggal lahir
		$tgl_lahir_ayah = $this->input->post('tgl_lahir_ayah_Year') . '-' . $this->input->post('tgl_lahir_ayah_Month') . '-' . $this->input->post('tgl_lahir_ayah_Day');
		$tgl_lahir_ayah = date('d-M-y', strtotime($tgl_lahir_ayah));

		// Pre-Proses tanggal lahir
		$tgl_lahir_ibu = $this->input->post('tgl_lahir_ibu_Year') . '-' . $this->input->post('tgl_lahir_ibu_Month') . '-' . $this->input->post('tgl_lahir_ibu_Day');
		$tgl_lahir_ibu = date('d-M-y', strtotime($tgl_lahir_ibu));

		// Pre-Proses tanggal lahir
		$tgl_lahir_wali = $this->input->post('tgl_lahir_wali_Year') . '-' . $this->input->post('tgl_lahir_wali_Month') . '-' . $this->input->post('tgl_lahir_wali_Day');
		$tgl_lahir_wali = date('d-M-y', strtotime($tgl_lahir_wali));
		
		// Normalisasi id_pilihan
		$id_pilihan_2 = $this->input->post('id_pilihan_2');
		$id_pilihan_3 = $this->input->post('id_pilihan_3');
		$id_pilihan_4 = $this->input->post('id_pilihan_4');

		// dianggap semua IPC dan utk menangani penetapan
		$kode_jurusan = '03';
		
		// update ke CALON_MAHASISWA_BARU
		$this->db->update('calon_mahasiswa_baru', array(
			'nm_c_mhs'			=> str_replace("'", "''", $this->input->post('nm_c_mhs')),
			'jenis_kelamin'		=> $this->input->post('jenis_kelamin'),
			'id_kota_lahir'		=> (int)$this->input->post('id_kota_lahir'),
			'tgl_lahir'			=> $tgl_lahir,
			'id_agama'			=> (int)$this->input->post('id_agama'),
			'sumber_biaya'		=> (int)$this->input->post('sumber_biaya'),
			'id_disabilitas'	=> (int)$this->input->post('id_disabilitas'),

			// dianggap semua IPC dan utk menangani penetapan
			'kode_jurusan'		=> $kode_jurusan,

			'id_pilihan_1'		=> (int)$this->input->post('id_pilihan_1'),
			'id_pilihan_2'		=> empty($id_pilihan_2) ? NULL : (int)$id_pilihan_2,
			'id_pilihan_3'		=> empty($id_pilihan_3) ? NULL : (int)$id_pilihan_3,
			'id_pilihan_4'		=> empty($id_pilihan_4) ? NULL : (int)$id_pilihan_4,

			// alamat
			'nik_c_mhs'			=> $this->input->post('nik_c_mhs'),
			'kewarganegaraan'	=> (int)$this->input->post('kewarganegaraan'),
			'alamat_jalan'		=> str_replace("'", "''", $this->input->post('alamat_jalan')),
			'alamat_dusun'		=> str_replace("'", "''", $this->input->post('alamat_dusun')),
			'alamat_rt'			=> str_replace("'", "''", $this->input->post('alamat_rt')),
			'alamat_rw'			=> str_replace("'", "''", $this->input->post('alamat_rw')),
			'alamat_kelurahan'	=> str_replace("'", "''", $this->input->post('alamat_kelurahan')),
			'alamat_kecamatan'	=> str_replace("'", "''", $this->input->post('alamat_kecamatan')),
			'alamat_kodepos'	=> str_replace("'", "''", $this->input->post('alamat_kodepos')),
			'id_kota'			=> (int)$this->input->post('id_kota'),
			'nm_jenis_tinggal'	=> $this->input->post('nm_jenis_tinggal'),
			'telp'				=> $this->input->post('telp'),
			'nomor_hp'			=> $this->input->post('nomor_hp'),
			'email'				=> $this->input->post('email'),
			'nomor_kps'			=> $this->input->post('nomor_kps'),

			'is_ajukan_bidikmisi'=> $this->input->post('is_ajukan_bidikmisi'),

			'tgl_registrasi'	=> date('d-M-Y')
		), $where);

		// pre-proses tanggal ijazah
		$tgl_ijazah = $this->input->post('tgl_ijazah_Year') . '-' . $this->input->post('tgl_ijazah_Month') . '-' . $this->input->post('tgl_ijazah_Day');
		$tgl_ijazah = date('d-M-y', strtotime($tgl_ijazah));
			
		// update ke CALON_MAHASISWA_SEKOLAH
		$this->db->update('calon_mahasiswa_sekolah', array(
			'nisn'				=> $this->input->post('nisn'),
			'id_sekolah_asal'	=> (int)$this->input->post('id_sekolah_asal'),
			'jurusan_sekolah'	=> $this->input->post('jurusan_sekolah'),
			'tahun_lulus'		=> $this->input->post('tahun_lulus'),
			'no_ijazah'			=> $this->input->post('no_ijazah'),
			'tgl_ijazah'		=> $tgl_ijazah,
				
			'jumlah_pelajaran_ijazah'	=> $this->input->post('jumlah_pelajaran_ijazah'),
			'nilai_ijazah'				=> str_replace(",", ".", $this->input->post('nilai_ijazah')),
			'jumlah_pelajaran_uan'		=> $this->input->post('jumlah_pelajaran_uan'),
			'nilai_uan'					=> str_replace(",", ".", $this->input->post('nilai_uan'))
		), $where);

		// update ke CALON_MAHASISWA_ORTU
		$this->db->update('calon_mahasiswa_ortu', array(
			'nama_ayah'				=> str_replace("'", "''", $this->input->post('nama_ayah')),
			'tgl_lahir_ayah'		=> $tgl_lahir_ayah,
			'alamat_ayah'			=> $this->input->post('alamat_ayah'),
			'id_kota_ayah'			=> $this->input->post('id_kota_ayah'),
			'telp_ayah'				=> $this->input->post('telp_ayah'),
			'pendidikan_ayah'		=> $this->input->post('pendidikan_ayah'),
			'pekerjaan_ayah'		=> $this->input->post('pekerjaan_ayah'),
			'penghasilan_ayah'		=> $this->input->post('penghasilan_ayah'),
			'id_disabilitas_ayah'	=> $this->input->post('id_disabilitas_ayah'),
			
			'nama_ibu'				=> str_replace("'", "''", $this->input->post('nama_ibu')),
			'tgl_lahir_ibu'			=> $tgl_lahir_ibu,
			'alamat_ibu'			=> $this->input->post('alamat_ibu'),
			'id_kota_ibu'			=> $this->input->post('id_kota_ibu'),
			'telp_ibu'				=> $this->input->post('telp_ibu'),
			'pendidikan_ibu'		=> $this->input->post('pendidikan_ibu'),
			'pekerjaan_ibu'			=> $this->input->post('pekerjaan_ibu'),
			'penghasilan_ibu'		=> $this->input->post('penghasilan_ibu'),
			'id_disabilitas_ibu'	=> $this->input->post('id_disabilitas_ibu'),

			'nama_wali'				=> str_replace("'", "''", $this->input->post('nama_wali')),
			'tgl_lahir_wali'		=> $tgl_lahir_wali,
			'alamat_wali'			=> $this->input->post('alamat_wali'),
			'id_kota_wali'			=> $this->input->post('id_kota_wali'),
			'telp_wali'				=> $this->input->post('telp_wali'),
			'pendidikan_wali'		=> $this->input->post('pendidikan_wali'),
			'pekerjaan_wali'		=> $this->input->post('pekerjaan_wali'),
			'penghasilan_wali'		=> $this->input->post('penghasilan_wali'),
			'id_disabilitas_wali'	=> $this->input->post('id_disabilitas_wali')

		), $where);

		
		/*if ($form_type == 's1')
		{		
			// pre-proses tanggal ijazah
			$tgl_ijazah = $this->input->post('tgl_ijazah_Year') . '-' . $this->input->post('tgl_ijazah_Month') . '-' . $this->input->post('tgl_ijazah_Day');
			$tgl_ijazah = date('d-M-y', strtotime($tgl_ijazah));
			
			// update ke CALON_MAHASISWA_SEKOLAH
			$this->db->update('calon_mahasiswa_sekolah', array(
				'nisn'				=> $this->input->post('nisn'),
				'id_sekolah_asal'	=> (int)$this->input->post('id_sekolah_asal'),
				'jurusan_sekolah'	=> $this->input->post('jurusan_sekolah'),
				'tahun_lulus'		=> $this->input->post('tahun_lulus'),
				'no_ijazah'			=> $this->input->post('no_ijazah'),
				'tgl_ijazah'		=> $tgl_ijazah,
				
				'jumlah_pelajaran_ijazah'	=> $this->input->post('jumlah_pelajaran_ijazah'),
				'nilai_ijazah'				=> str_replace(",", ".", $this->input->post('nilai_ijazah')),
				'jumlah_pelajaran_uan'		=> $this->input->post('jumlah_pelajaran_uan'),
				'nilai_uan'					=> str_replace(",", ".", $this->input->post('nilai_uan'))
			), $where);
			
			// update ke CALON_MAHASISWA_ORTU
			$this->db->update('calon_mahasiswa_ortu', array(
				'nama_ayah'			=> str_replace("'", "''", $this->input->post('nama_ayah')),
				'alamat_ayah'		=> str_replace("'", "''", $this->input->post('alamat_ayah')),
				'telp_ayah'			=> $this->input->post('telp_ayah'),
				'pendidikan_ayah'	=> $this->input->post('pendidikan_ayah'),
				'pekerjaan_ayah'	=> $this->input->post('pekerjaan_ayah'),
				'penghasilan_ayah'	=> $this->input->post('penghasilan_ayah'),
				'instansi_ayah'		=> str_replace("'", "''", $this->input->post('instansi_ayah')),
				'jabatan_ayah'		=> $this->input->post('jabatan_ayah'),
				'masa_kerja_ayah'	=> str_replace(",", ".", $this->input->post('masa_kerja_ayah')),
				
				'nama_ibu'			=> str_replace("'", "''", $this->input->post('nama_ibu')),
				'alamat_ibu'		=> str_replace("'", "''", $this->input->post('alamat_ibu')),
				'telp_ibu'			=> $this->input->post('telp_ibu'),
				'pendidikan_ibu'	=> $this->input->post('pendidikan_ibu'),
				'pekerjaan_ibu'		=> $this->input->post('pekerjaan_ibu'),
				'penghasilan_ibu'	=> $this->input->post('penghasilan_ibu'),
				'instansi_ibu'		=> str_replace("'", "''", $this->input->post('instansi_ibu')),
				'jabatan_ibu'		=> $this->input->post('jabatan_ibu'),
				'masa_kerja_ibu'	=> str_replace(",", ".", $this->input->post('masa_kerja_ibu')),
			), $where);
		}
		
		if ($form_type == 's2')
		{
			// pre-proses tanggal masuk s1
			$tgl_masuk_s1 = $this->input->post('tgl_masuk_s1_Year') . '-' . $this->input->post('tgl_masuk_s1_Month') . '-' . $this->input->post('tgl_masuk_s1_Day');
			$tgl_masuk_s1 = date('d-M-y', strtotime($tgl_masuk_s1));
			
			// pre-proses tanggal lulus s1
			$tgl_lulus_s1 = $this->input->post('tgl_lulus_s1_Year') . '-' . $this->input->post('tgl_lulus_s1_Month') . '-' . $this->input->post('tgl_lulus_s1_Day');
			$tgl_lulus_s1 = date('d-M-y', strtotime($tgl_lulus_s1));
			
			$id_prodi_minat			= $this->input->post('id_prodi_minat');
			$id_kelompok_biaya_1	= $this->input->post('id_kelompok_biaya_1');
			
			// update ke CALON_MAHASISWA_PASCA
			$this->db->update('calon_mahasiswa_pasca', array(
				'ptn_s1'					=> $this->input->post('ptn_s1'),
				'status_ptn_s1'				=> (int)$this->input->post('status_ptn_s1'),
				'prodi_s1'					=> $this->input->post('prodi_s1'),
				'jenis_akreditasi_s1'		=> $this->input->post('jenis_akreditasi_s1'),
				'peringkat_akreditasi_s1'	=> $this->input->post('peringkat_akreditasi_s1'),
				'tgl_masuk_s1'				=> $tgl_masuk_s1,
				'tgl_lulus_s1'				=> $tgl_lulus_s1,
				'lama_studi_s1'				=> (float)$this->input->post('lama_studi_s1'),
				'ip_s1'						=> (float)$this->input->post('ip_s1'),
				'nim_lama_s1'				=> $this->input->post('nim_lama_s1'),
				'jumlah_karya_ilmiah'		=> (int)$this->input->post('jumlah_karya_ilmiah'),
				'nilai_toefl'				=> (int)$this->input->post('nilai_toefl'),
				
				'pekerjaan'				=> $this->input->post('pekerjaan'),
				'asal_instansi'			=> str_replace("'", "''", $this->input->post('asal_instansi')),
				'alamat_instansi'		=> str_replace("'", "''", $this->input->post('alamat_instansi')),
				'telp_instansi'			=> $this->input->post('telp_instansi'),
				'nrp'					=> $this->input->post('nrp'),
				'karpeg'				=> $this->input->post('karpeg'),
				'pangkat'				=> $this->input->post('pangkat'),
				
				'id_prodi_minat'		=> empty($id_prodi_minat) ? NULL : $id_prodi_minat
				
			), $where);
			
			// update ke CALON_MAHASISWA_DATA
			$this->db->update('calon_mahasiswa_data', array(
				'id_kelompok_biaya_1'	=> empty($id_kelompok_biaya_1) ? NULL : $id_kelompok_biaya_1
			), $where);
		}
		
		if ($form_type == 's3')
		{
			// pre-proses tanggal masuk s1
			$tgl_masuk_s1 = $this->input->post('tgl_masuk_s1_Year') . '-' . $this->input->post('tgl_masuk_s1_Month') . '-' . $this->input->post('tgl_masuk_s1_Day');
			$tgl_masuk_s1 = date('d-M-y', strtotime($tgl_masuk_s1));
			
			// pre-proses tanggal lulus s1
			$tgl_lulus_s1 = $this->input->post('tgl_lulus_s1_Year') . '-' . $this->input->post('tgl_lulus_s1_Month') . '-' . $this->input->post('tgl_lulus_s1_Day');
			$tgl_lulus_s1 = date('d-M-y', strtotime($tgl_lulus_s1));
			
			// pre-proses tanggal masuk s2
			$tgl_masuk_s2 = $this->input->post('tgl_masuk_s2_Year') . '-' . $this->input->post('tgl_masuk_s2_Month') . '-' . $this->input->post('tgl_masuk_s2_Day');
			$tgl_masuk_s2 = date('d-M-y', strtotime($tgl_masuk_s2));
			
			// pre-proses tanggal lulus s2
			$tgl_lulus_s2 = $this->input->post('tgl_lulus_s2_Year') . '-' . $this->input->post('tgl_lulus_s2_Month') . '-' . $this->input->post('tgl_lulus_s2_Day');
			$tgl_lulus_s2 = date('d-M-y', strtotime($tgl_lulus_s2));
			
			$id_prodi_minat = $this->input->post('id_prodi_minat');
			$id_kelompok_biaya_1	= $this->input->post('id_kelompok_biaya_1');
			
			// update ke CALON_MAHASISWA_PASCA
			$this->db->update('calon_mahasiswa_pasca', array(
				'ptn_s1'					=> $this->input->post('ptn_s1'),
				'status_ptn_s1'				=> (int)$this->input->post('status_ptn_s1'),
				'prodi_s1'					=> $this->input->post('prodi_s1'),
				'tgl_masuk_s1'				=> $tgl_masuk_s1,
				'tgl_lulus_s1'				=> $tgl_lulus_s1,
				'lama_studi_s1'				=> (float)$this->input->post('lama_studi_s1'),
				'ip_s1'						=> (float)$this->input->post('ip_s1'),
				'nim_lama_s1'				=> $this->input->post('nim_lama_s1'),
				
				'ptn_s2'					=> $this->input->post('ptn_s2'),
				'status_ptn_s2'				=> (int)$this->input->post('status_ptn_s2'),
				'prodi_s2'					=> $this->input->post('prodi_s2'),
				'tgl_masuk_s2'				=> $tgl_masuk_s2,
				'tgl_lulus_s2'				=> $tgl_lulus_s2,
				'lama_studi_s2'				=> (float)$this->input->post('lama_studi_s2'),
				'ip_s2'						=> (float)$this->input->post('ip_s2'),
				'nim_lama_s2'				=> $this->input->post('nim_lama_s2'),
				
				'jumlah_karya_ilmiah'		=> (int)$this->input->post('jumlah_karya_ilmiah'),
				'nilai_toefl'				=> (int)$this->input->post('nilai_toefl'),
				
				'pekerjaan'				=> $this->input->post('pekerjaan'),
				'asal_instansi'			=> str_replace("'", "''", $this->input->post('asal_instansi')),
				'alamat_instansi'		=> str_replace("'", "''", $this->input->post('alamat_instansi')),
				'telp_instansi'			=> $this->input->post('telp_instansi'),
				'nrp'					=> $this->input->post('nrp'),
				'karpeg'				=> $this->input->post('karpeg'),
				'pangkat'				=> $this->input->post('pangkat'),
				
				'id_prodi_minat'		=> empty($id_prodi_minat) ? NULL : $id_prodi_minat
			), $where);
			
			// update ke CALON_MAHASISWA_DATA
			$this->db->update('calon_mahasiswa_data', array(
				'id_kelompok_biaya_1'	=> empty($id_kelompok_biaya_1) ? NULL : $id_kelompok_biaya_1
			), $where);
		}*/
		
		// update ke CALON_MAHASISWA_DATA
		$this->db->update('calon_mahasiswa_data', array(
			'is_setuju_pernyataan'	=> $this->input->post('is_setuju_pernyataan')
		), $where);
		
		$this->db->trans_complete();
		
	}
	
	/**
	 * Membuat row CMB, CMD,
	 * @param int $id_calon_pendaftar
	 * @param int $id_penerimaan
	 * @return string <p>Hasil
	 * - 'COMPLETE' : Sukses
	 * - 'KODE_VOUCHER_EMPTY' : Kode voucher tidak ada / belum diaktifkan
	 * </p>
	 */
	function create_cmb($id_calon_pendaftar, $id_penerimaan)
	{
		// Mendapatkan row ID_C_MHS yg exist (preventif sistem lag)
		$this->query = $this->db
			->select('id_c_mhs')->from('calon_mahasiswa_baru')
			->where(array(
				'id_penerimaan'			=> $id_penerimaan,
				'id_calon_pendaftar'	=> $id_calon_pendaftar
			))->get();
		
		// Jika sudah ada row di CMB maka langsung diaktifkan
		if ($this->query->num_rows() == 1)
		{
			$id_c_mhs = $this->query->row()->ID_C_MHS;
			
			// update yang aktif
			$this->db->update('calon_pendaftar', array('id_c_mhs_aktif' => $id_c_mhs), array('id_calon_pendaftar' => $id_calon_pendaftar));
			
			return 'COMPLETE';
		}
		
		//--------------------------------------------
		// Jika belum makan lanjut proses dibawah ini
		//--------------------------------------------
		
		// Mendapatkan row CALON_PENDAFTAR
		$this->query = $this->db->get_where('calon_pendaftar', array('id_calon_pendaftar' => $id_calon_pendaftar));
		$calon_pendaftar = $this->query->row();
		$this->query->free_result();
		
		// mulai transaksi
		$this->db->trans_start();
		
		// Mendapatkan id_c_mhs terlebih dahulu
		$this->query = $this->db->query("select aucc.calon_mahasiswa_baru_seq.nextval from dual");
		$row = $this->query->row();
		$id_c_mhs = $row->NEXTVAL;
		$this->query->free_result();
		
		// Mendapatkan row PENERIMAAN
		$this->query = $this->db->get_where('penerimaan', array('id_penerimaan' => $id_penerimaan));
		$penerimaan = $this->query->row();
		$this->query->free_result();
		
		// Mendapatkan row VOUCHER yg free
		$this->query = $this->db
			->get_where('voucher', array(
				'id_penerimaan'	=> $id_penerimaan,
				'is_aktif'		=> 1,
				'tgl_ambil'		=> NULL
			), 2); // di Oracle memakai rumus n-1 utk mendapatkan jumlah row sebanyak n
		
		// Return nilai error jika voucher belum di set / tidak ada
		if ($this->query->num_rows() == 0)
		{
			return 'KODE_VOUCHER_EMPTY';
		}
		
		// Mengambil row VOUCHER
		$voucher = $this->query->row();
		$this->query->free_result();
		
		// Insert ke tabel CMB
		$this->db->insert('calon_mahasiswa_baru', array(
			// PK + FK
			'id_c_mhs'				=> $id_c_mhs,
			'id_calon_pendaftar'	=> $id_calon_pendaftar,
			
			// penerimaan
			'id_penerimaan'		=> $penerimaan->ID_PENERIMAAN,
			'id_jalur'			=> $penerimaan->ID_JALUR,
			
			// default value
			'kode_voucher'		=> $voucher->KODE_VOUCHER,
			'pin_voucher'		=> $voucher->PIN_VOUCHER,
			'nm_c_mhs'			=> str_replace("'", "''", $calon_pendaftar->NAMA_PENDAFTAR),
			'telp'				=> $calon_pendaftar->TELP_PENDAFTAR,
			'email'				=> $calon_pendaftar->EMAIL_PENDAFTAR,
		));
		
		// Update id_c_mhs yang aktif
		$this->db->update('calon_pendaftar', array('id_c_mhs_aktif' => $id_c_mhs), array('id_calon_pendaftar' => $id_calon_pendaftar));
		
		// Update pengambilan voucher
		$now = date('Y-m-d H:i:s');
		$this->db->set('tgl_ambil', "to_date('{$now}','YYYY-MM-DD HH24:MI:SS')", false);
		$this->db->update('voucher', null, array('id_voucher' => $voucher->ID_VOUCHER));
		
		$this->db->trans_complete();
		
		return 'COMPLETE';
	}
	
	/**
	 * Mendapatkan CALON_MAHASISWA_BARU
	 * @param int $id_c_mhs
	 * @return Object
	 */
	function get_cmb($id_c_mhs)
	{
		// Mendapatkan row CMB
		$this->query = $this->db->get_where('calon_mahasiswa_baru', array('id_c_mhs' => $id_c_mhs));
		$row = $this->query->row();
		$this->query->free_result();
		
		if ( ! $row)
		{
			return null;
		}
		
		// get penerimaan object
		$this->query = $this->db->get_where('penerimaan', array('id_penerimaan' => $row->ID_PENERIMAAN));
		$row->PENERIMAAN = $this->query->row();
		$this->query->free_result();
		
		// get tanggal lahir format khusus
		$this->query = $this->db->select("to_char(TGL_LAHIR,'YYYY-MM-DD') as TGL_LAHIR")->get_where('calon_mahasiswa_baru', array('id_c_mhs' => $id_c_mhs) );
		$row->TGLLAHIR = $this->query->row();
		$this->query->free_result();
		
		// Mendapatkan nama prodi pilihan
		// Pilihan 1
		if ($row->ID_PILIHAN_1 != '')
		{
			$this->query = $this->db->select('nm_program_studi')->get_where('program_studi', array('id_program_studi' => $row->ID_PILIHAN_1));
			$row->PILIHAN_1 = $this->query->first_row();
			$this->query->free_result();
		}

		// Pilihan 2
		if ($row->ID_PILIHAN_2 != '')
		{
			$this->query = $this->db->select('nm_program_studi')->get_where('program_studi', array('id_program_studi' => $row->ID_PILIHAN_2));
			$row->PILIHAN_2 = $this->query->first_row();
			$this->query->free_result();
		}

		// Pilihan 3
		if ($row->ID_PILIHAN_3 != '')
		{
			$this->query = $this->db->select('nm_program_studi')->get_where('program_studi', array('id_program_studi' => $row->ID_PILIHAN_3));
			$row->PILIHAN_3 = $this->query->first_row();
			$this->query->free_result();
		}

		// Pilihan 4
		if ($row->ID_PILIHAN_4 != '')
		{
			$this->query = $this->db->select('nm_program_studi')->get_where('program_studi', array('id_program_studi' => $row->ID_PILIHAN_4));
			$row->PILIHAN_4 = $this->query->first_row();
			$this->query->free_result();
		}
		
		// Mendapatkan row kode voucher
		if ($row->KODE_VOUCHER != '')
		{
			// mendapatkan voucher
			$this->query = $this->db->get_where('voucher', array('kode_voucher' => $row->KODE_VOUCHER));
			$row->VOUCHER = $this->query->row();
			$this->query->free_result();
		}
		
		// mendapatkan row voucher tarif
		if ($row->KODE_JURUSAN != '')
		{
			// mendapatkan tarif
			$this->query = $this->db->get_where('voucher_tarif', array('kode_jurusan' => $row->KODE_JURUSAN, 'id_penerimaan' => $row->ID_PENERIMAAN));
			$row->VOUCHER_TARIF = $this->query->row();
			$this->query->free_result();
		}
		
		// Menandakan sudah mendapatkan nomer ujian
		// menyebabkan error
		/*if ($row->NO_UJIAN != '')
		{
			$this->query = $this->db->get_where('plot_jadwal_ppmb', array('id_c_mhs' => $id_c_mhs));
			$row->PLOT_JADWAL_PPMB = $this->query->row();
			$this->query->free_result();
			
			$this->query = $this->db->get_where('jadwal_ppmb', array('id_jadwal_ppmb' => $row->PLOT_JADWAL_PPMB->ID_JADWAL_PPMB));
			$row->JADWAL_PPMB = $this->query->row();
			$this->query->free_result();
		}*/
		
		return $row;
	}
	
	/**
	 * Mendapatkan CALON_MAHASISWA_SEKOLAH
	 * @param int $id_c_mhs
	 * @return Object
	 */
	function get_cms($id_c_mhs)
	{
		$this->query = $this->db
				->select('cms.*, s.id_kota as id_kota_sekolah')
				->join('sekolah s', 's.id_sekolah = cms.id_sekolah_asal', 'left')
				->get_where('calon_mahasiswa_sekolah cms', array('cms.id_c_mhs' => $id_c_mhs));
		$row = $this->query->row();
		$this->query->free_result();
		
		return $row;
	}
	
	/**
	 * Mendapatkan CALON_MAHASISWA_ORTU
	 * @param int $id_c_mhs
	 * @return Object
	 */
	function get_cmo($id_c_mhs)
	{
		$this->query = $this->db->get_where('calon_mahasiswa_ortu', array('id_c_mhs' => $id_c_mhs));
		$row = $this->query->row();
		$this->query->free_result();
		
		return $row;
	}
	
	/**
	 * Mendapatkan CALON_MAHASISWA_DATA
	 * @param int $id_c_mhs
	 * @return Object
	 */
	function get_cmd($id_c_mhs)
	{
		$this->query = $this->db->get_where('calon_mahasiswa_data', array('id_c_mhs' => $id_c_mhs));
		$row = $this->query->row();
		$this->query->free_result();
		
		$this->query = $this->db->get_where('calon_mahasiswa_baru', array('id_c_mhs' => $id_c_mhs));
		$row2 = $this->query->row();
		$this->query->free_result();
		
		if ($row->ID_KELOMPOK_BIAYA_1 != '')
		{
			if($row->ID_KELOMPOK_BIAYA_1 == '43' and $row2->ID_PILIHAN_1=='73') { // bila MM kelas pagi, maka diganti menjadi Kelas Sore
				$row->KELOMPOK_BIAYA_1->NM_KELOMPOK_BIAYA = "Kelas Sore";
			}
			else{
				$this->query = $this->db->get_where('kelompok_biaya', array('id_kelompok_biaya' => $row->ID_KELOMPOK_BIAYA_1));
				$row->KELOMPOK_BIAYA_1 = $this->query->row();
				$this->query->free_result();
			}
		}
		
		return $row;
	}
	
	/**
	 * Mendapatkan CALON_MAHASISWA_PASCA
	 * @param int $id_c_mhs
	 * @return Object Description
	 */
	function get_cmp($id_c_mhs)
	{
		$this->query = $this->db->get_where('calon_mahasiswa_pasca', array('id_c_mhs' => $id_c_mhs));
		$row = $this->query->row();
		$this->query->free_result();
		
		return $row;
	}
	
	/**
	 * Mendapatkan CALON_MAHASISWA_FILE
	 * @param int $id_c_mhs
	 * @return Object Description
	 */
	function get_cmf($id_c_mhs)
	{
		$this->query = $this->db->get_where('calon_mahasiswa_file', array('id_c_mhs' => $id_c_mhs));
		$row = $this->query->row();
		$this->query->free_result();
		
		return $row;
	}
	
	/**
	 * Mendapatkan row CALON_MAHASISWA_SYARAT
	 * @param int $id_c_mhs
	 * @param int $id_syarat_prodi
	 * @return Object
	 */
	function get_cm_syarat($id_c_mhs, $id_syarat_prodi)
	{
		$this->query = $this->db->get_where('calon_mahasiswa_syarat', array('id_c_mhs' => $id_c_mhs, 'id_syarat_prodi' => $id_syarat_prodi));
		$row = $this->query->row();
		$this->query->free_result();
		
		return $row;
	}
	
	/**
	 * Mengambil informasi antrian peserta
	 * @param int $id_c_mhs
	 * @param int $id_penerimaan
	 * @return string|int
	 */
	function ambil_antrian($id_c_mhs, $id_penerimaan)
	{
		$this->query = $this->db->query("
			select
				IS_PROSES_VERIFIKASI, TGL_SUBMIT_VERIFIKASI
			from aucc.calon_mahasiswa_baru
			where 
				ID_PENERIMAAN = ? and id_c_mhs = ?
			", 
			array($id_penerimaan, $id_c_mhs ));
		$row = $this->query->row();
		
		$sedang_proses	= $row->IS_PROSES_VERIFIKASI;
		$tgl_submit		= $row->TGL_SUBMIT_VERIFIKASI;
		
		if ($tgl_submit == '') 
		{
			return '-1';
		}
		else if ($sedang_proses == 1)
		{
			return 0;
		}
		else
		{	
			$this->query = $this->db->query("
				select
					count(*) as ANTRIAN
				from aucc.calon_mahasiswa_baru
				where 
					IS_PROSES_VERIFIKASI is null and ID_PENERIMAAN = ? and TGL_VERIFIKASI_PPMB is null and TGL_SUBMIT_VERIFIKASI <= (
						select TGL_SUBMIT_VERIFIKASI from AUCC.CALON_MAHASISWA_BARU where id_c_mhs = ?
					)
				", 
				array($id_penerimaan, $id_c_mhs ));
			
			$row = $this->query->row();
			return $row->ANTRIAN;
		}
	}
	
	/**
	 * Mengambil jadwal verifikasi
	 * @param int $id_penerimaan
	 * @return string|int
	 */
	function ambil_jadwal_verifikasi($id_penerimaan)
	{
		$jadwal_verifikasi_ok = 0;
		$this->query = $this->db->query("
			select count(*) as BOLEH
			from aucc.penerimaan 
			where ID_PENERIMAAN = ? 
			and to_char(TGL_AWAL_VERIFIKASI,'YYYYMMDDHH24MISS') <= to_char(SYSDATE,'YYYYMMDDHH24MISS')
			and to_char(TGL_AKHIR_VERIFIKASI,'YYYYMMDDHH24MISS') >= to_char(SYSDATE,'YYYYMMDDHH24MISS')
			", 
			array($id_penerimaan ));
		$jadwal_verifikasi_ok = $this->query->row()->BOLEH;
		
		return $jadwal_verifikasi_ok;
	}
	
	/**
	 * Mendapatkan id_c_mhs terakhir
	 * @param int $id_calon_pendaftar
	 * @return int|null Int on success, Null on failure
	 */
	function get_id_c_mhs($id_calon_pendaftar)
	{
		$this->db->select('id_c_mhs');
		$this->query = $this->db->get_where('calon_mahasiswa_baru', array('id_calon_pendaftar' => $id_calon_pendaftar));
		
		if ($this->query->num_rows() == 0)
		{
			return NULL;
		}
		else
		{
			return $this->query->row()->ID_C_MHS;
		}
	}
	
	/**
	 * 
	 * @param string $kode_voucher
	 */
	function get_id_cmhs_by_voucher($kode_voucher, $id_pt)
	{
		$this->query = $this->db
			->select('id_c_mhs')->from('calon_mahasiswa_baru cmb')
			->join('penerimaan p', 'p.id_penerimaan = cmb.id_penerimaan')
			->where('p.id_perguruan_tinggi', $id_pt)
			->where('cmb.kode_voucher', $kode_voucher)
			->get();
		
		$cmb = $this->query->first_row();
		
		if ($cmb) {
			return $cmb->ID_C_MHS;
		}
		else {
			return NULL;
		}
	}
	
	/**
	 * Update jenis formulir
	 * @param int $id_c_mhs
	 * @param string $kode_jurusan
	 */
	function update_jenis_formulir($id_c_mhs, $kode_jurusan)
	{
		// Update CMB.KODE_JURUSAN
		$this->db->update('calon_mahasiswa_baru', array('kode_jurusan' => $kode_jurusan), array('id_c_mhs' => $id_c_mhs));
	}
	
	/**
	 * Mereset jenis formulir
	 * @param int $id_c_mhs
	 */
	function reset_jenis_formulir($id_c_mhs)
	{
		$set = array(
			'kode_jurusan'	=> NULL,
			'id_pilihan_1'	=> NULL,
			'id_pilihan_2'	=> NULL,
			'id_pilihan_3'	=> NULL,
			'id_pilihan_4'	=> NULL
		);
		
		$this->db->update('calon_mahasiswa_baru', $set, array('id_c_mhs' => $id_c_mhs));
	}
	
	function reset_isi_form($id_c_mhs)
	{
		$set = array('tgl_registrasi' => NULL);
		$this->db->update('calon_mahasiswa_baru', $set, array('id_c_mhs' => $id_c_mhs));
	}
	
	/**
	 * Update data CMB, CMS, CMO, CMP
	 * @param int $id_c_mhs
	 * @param string $form_type
	 */
	function update($id_c_mhs, $form_type)
	{
		$this->db->trans_start();
		
		$where	= array('id_c_mhs' => (int)$id_c_mhs);
		
		// Pre-Proses tanggal lahir
		$tgl_lahir = $this->input->post('tgl_lahir_Year') . '-' . $this->input->post('tgl_lahir_Month') . '-' . $this->input->post('tgl_lahir_Day');
		//echo $tgl_lahir.'<br>';
		$tgl_lahir = date('d-M-y', strtotime($tgl_lahir));
		//echo $tgl_lahir;
		
		// Normalisasi id_pilihan
		$id_pilihan_2 = $this->input->post('id_pilihan_2');
		$id_pilihan_3 = $this->input->post('id_pilihan_3');
		$id_pilihan_4 = $this->input->post('id_pilihan_4');
		
		// update ke CALON_MAHASISWA_BARU
		$this->db->update('calon_mahasiswa_baru', array(
			'nm_c_mhs'			=> str_replace("'", "''", $this->input->post('nm_c_mhs')),
			'gelar'				=> str_replace("'", "''", $this->input->post('gelar')),
			'jenis_kelamin'		=> $this->input->post('jenis_kelamin'),
			'id_kota_lahir'		=> (int)$this->input->post('id_kota_lahir'),
			'tgl_lahir'			=> $tgl_lahir,
			'alamat'			=> str_replace("'", "''", $this->input->post('alamat')),
			'id_kota'			=> (int)$this->input->post('id_kota'),
			'telp'				=> $this->input->post('telp'),
			'kewarganegaraan'	=> (int)$this->input->post('kewarganegaraan'),
			'id_agama'			=> (int)$this->input->post('id_agama'),
			'sumber_biaya'		=> (int)$this->input->post('sumber_biaya'),
			'id_disabilitas'	=> (int)$this->input->post('id_disabilitas'),

			'id_pilihan_1'		=> (int)$this->input->post('id_pilihan_1'),
			'id_pilihan_2'		=> empty($id_pilihan_2) ? NULL : (int)$id_pilihan_2,
			'id_pilihan_3'		=> empty($id_pilihan_3) ? NULL : (int)$id_pilihan_3,
			'id_pilihan_4'		=> empty($id_pilihan_4) ? NULL : (int)$id_pilihan_4,

			'tgl_registrasi'	=> date('d-M-Y')
		), $where);
		
		if ($form_type == 's1')
		{		
			// pre-proses tanggal ijazah
			$tgl_ijazah = $this->input->post('tgl_ijazah_Year') . '-' . $this->input->post('tgl_ijazah_Month') . '-' . $this->input->post('tgl_ijazah_Day');
			$tgl_ijazah = date('d-M-y', strtotime($tgl_ijazah));
			
			// update ke CALON_MAHASISWA_SEKOLAH
			$this->db->update('calon_mahasiswa_sekolah', array(
				'nisn'				=> $this->input->post('nisn'),
				'id_sekolah_asal'	=> (int)$this->input->post('id_sekolah_asal'),
				'jurusan_sekolah'	=> $this->input->post('jurusan_sekolah'),
				'tahun_lulus'		=> $this->input->post('tahun_lulus'),
				'no_ijazah'			=> $this->input->post('no_ijazah'),
				'tgl_ijazah'		=> $tgl_ijazah,
				
				'jumlah_pelajaran_ijazah'	=> $this->input->post('jumlah_pelajaran_ijazah'),
				'nilai_ijazah'				=> str_replace(",", ".", $this->input->post('nilai_ijazah')),
				'jumlah_pelajaran_uan'		=> $this->input->post('jumlah_pelajaran_uan'),
				'nilai_uan'					=> str_replace(",", ".", $this->input->post('nilai_uan'))
			), $where);
			
			// update ke CALON_MAHASISWA_ORTU
			$this->db->update('calon_mahasiswa_ortu', array(
				'nama_ayah'			=> str_replace("'", "''", $this->input->post('nama_ayah')),
				'alamat_ayah'		=> str_replace("'", "''", $this->input->post('alamat_ayah')),
				'telp_ayah'			=> $this->input->post('telp_ayah'),
				'pendidikan_ayah'	=> $this->input->post('pendidikan_ayah'),
				'pekerjaan_ayah'	=> $this->input->post('pekerjaan_ayah'),
				'penghasilan_ayah'	=> $this->input->post('penghasilan_ayah'),
				'instansi_ayah'		=> str_replace("'", "''", $this->input->post('instansi_ayah')),
				'jabatan_ayah'		=> $this->input->post('jabatan_ayah'),
				'masa_kerja_ayah'	=> str_replace(",", ".", $this->input->post('masa_kerja_ayah')),
				
				'nama_ibu'			=> str_replace("'", "''", $this->input->post('nama_ibu')),
				'alamat_ibu'		=> str_replace("'", "''", $this->input->post('alamat_ibu')),
				'telp_ibu'			=> $this->input->post('telp_ibu'),
				'pendidikan_ibu'	=> $this->input->post('pendidikan_ibu'),
				'pekerjaan_ibu'		=> $this->input->post('pekerjaan_ibu'),
				'penghasilan_ibu'	=> $this->input->post('penghasilan_ibu'),
				'instansi_ibu'		=> str_replace("'", "''", $this->input->post('instansi_ibu')),
				'jabatan_ibu'		=> $this->input->post('jabatan_ibu'),
				'masa_kerja_ibu'	=> str_replace(",", ".", $this->input->post('masa_kerja_ibu')),
			), $where);
		}
		
		if ($form_type == 's2')
		{
			// pre-proses tanggal masuk s1
			$tgl_masuk_s1 = $this->input->post('tgl_masuk_s1_Year') . '-' . $this->input->post('tgl_masuk_s1_Month') . '-' . $this->input->post('tgl_masuk_s1_Day');
			$tgl_masuk_s1 = date('d-M-y', strtotime($tgl_masuk_s1));
			
			// pre-proses tanggal lulus s1
			$tgl_lulus_s1 = $this->input->post('tgl_lulus_s1_Year') . '-' . $this->input->post('tgl_lulus_s1_Month') . '-' . $this->input->post('tgl_lulus_s1_Day');
			$tgl_lulus_s1 = date('d-M-y', strtotime($tgl_lulus_s1));
			
			$id_prodi_minat			= $this->input->post('id_prodi_minat');
			$id_kelompok_biaya_1	= $this->input->post('id_kelompok_biaya_1');
			
			// update ke CALON_MAHASISWA_PASCA
			$this->db->update('calon_mahasiswa_pasca', array(
				'ptn_s1'					=> $this->input->post('ptn_s1'),
				'status_ptn_s1'				=> (int)$this->input->post('status_ptn_s1'),
				'prodi_s1'					=> $this->input->post('prodi_s1'),
				'jenis_akreditasi_s1'		=> $this->input->post('jenis_akreditasi_s1'),
				'peringkat_akreditasi_s1'	=> $this->input->post('peringkat_akreditasi_s1'),
				'tgl_masuk_s1'				=> $tgl_masuk_s1,
				'tgl_lulus_s1'				=> $tgl_lulus_s1,
				'lama_studi_s1'				=> (float)$this->input->post('lama_studi_s1'),
				'ip_s1'						=> (float)$this->input->post('ip_s1'),
				'nim_lama_s1'				=> $this->input->post('nim_lama_s1'),
				'jumlah_karya_ilmiah'		=> (int)$this->input->post('jumlah_karya_ilmiah'),
				'nilai_toefl'				=> (int)$this->input->post('nilai_toefl'),
				
				'pekerjaan'				=> $this->input->post('pekerjaan'),
				'asal_instansi'			=> str_replace("'", "''", $this->input->post('asal_instansi')),
				'alamat_instansi'		=> str_replace("'", "''", $this->input->post('alamat_instansi')),
				'telp_instansi'			=> $this->input->post('telp_instansi'),
				'nrp'					=> $this->input->post('nrp'),
				'karpeg'				=> $this->input->post('karpeg'),
				'pangkat'				=> $this->input->post('pangkat'),
				
				'id_prodi_minat'		=> empty($id_prodi_minat) ? NULL : $id_prodi_minat
				
			), $where);
			
			// update ke CALON_MAHASISWA_DATA
			$this->db->update('calon_mahasiswa_data', array(
				'id_kelompok_biaya_1'	=> empty($id_kelompok_biaya_1) ? NULL : $id_kelompok_biaya_1
			), $where);
		}
		
		if ($form_type == 's3')
		{
			// pre-proses tanggal masuk s1
			$tgl_masuk_s1 = $this->input->post('tgl_masuk_s1_Year') . '-' . $this->input->post('tgl_masuk_s1_Month') . '-' . $this->input->post('tgl_masuk_s1_Day');
			$tgl_masuk_s1 = date('d-M-y', strtotime($tgl_masuk_s1));
			
			// pre-proses tanggal lulus s1
			$tgl_lulus_s1 = $this->input->post('tgl_lulus_s1_Year') . '-' . $this->input->post('tgl_lulus_s1_Month') . '-' . $this->input->post('tgl_lulus_s1_Day');
			$tgl_lulus_s1 = date('d-M-y', strtotime($tgl_lulus_s1));
			
			// pre-proses tanggal masuk s2
			$tgl_masuk_s2 = $this->input->post('tgl_masuk_s2_Year') . '-' . $this->input->post('tgl_masuk_s2_Month') . '-' . $this->input->post('tgl_masuk_s2_Day');
			$tgl_masuk_s2 = date('d-M-y', strtotime($tgl_masuk_s2));
			
			// pre-proses tanggal lulus s2
			$tgl_lulus_s2 = $this->input->post('tgl_lulus_s2_Year') . '-' . $this->input->post('tgl_lulus_s2_Month') . '-' . $this->input->post('tgl_lulus_s2_Day');
			$tgl_lulus_s2 = date('d-M-y', strtotime($tgl_lulus_s2));
			
			$id_prodi_minat = $this->input->post('id_prodi_minat');
			$id_kelompok_biaya_1	= $this->input->post('id_kelompok_biaya_1');
			
			// update ke CALON_MAHASISWA_PASCA
			$this->db->update('calon_mahasiswa_pasca', array(
				'ptn_s1'					=> $this->input->post('ptn_s1'),
				'status_ptn_s1'				=> (int)$this->input->post('status_ptn_s1'),
				'prodi_s1'					=> $this->input->post('prodi_s1'),
				'tgl_masuk_s1'				=> $tgl_masuk_s1,
				'tgl_lulus_s1'				=> $tgl_lulus_s1,
				'lama_studi_s1'				=> (float)$this->input->post('lama_studi_s1'),
				'ip_s1'						=> (float)$this->input->post('ip_s1'),
				'nim_lama_s1'				=> $this->input->post('nim_lama_s1'),
				
				'ptn_s2'					=> $this->input->post('ptn_s2'),
				'status_ptn_s2'				=> (int)$this->input->post('status_ptn_s2'),
				'prodi_s2'					=> $this->input->post('prodi_s2'),
				'tgl_masuk_s2'				=> $tgl_masuk_s2,
				'tgl_lulus_s2'				=> $tgl_lulus_s2,
				'lama_studi_s2'				=> (float)$this->input->post('lama_studi_s2'),
				'ip_s2'						=> (float)$this->input->post('ip_s2'),
				'nim_lama_s2'				=> $this->input->post('nim_lama_s2'),
				
				'jumlah_karya_ilmiah'		=> (int)$this->input->post('jumlah_karya_ilmiah'),
				'nilai_toefl'				=> (int)$this->input->post('nilai_toefl'),
				
				'pekerjaan'				=> $this->input->post('pekerjaan'),
				'asal_instansi'			=> str_replace("'", "''", $this->input->post('asal_instansi')),
				'alamat_instansi'		=> str_replace("'", "''", $this->input->post('alamat_instansi')),
				'telp_instansi'			=> $this->input->post('telp_instansi'),
				'nrp'					=> $this->input->post('nrp'),
				'karpeg'				=> $this->input->post('karpeg'),
				'pangkat'				=> $this->input->post('pangkat'),
				
				'id_prodi_minat'		=> empty($id_prodi_minat) ? NULL : $id_prodi_minat
			), $where);
			
			// update ke CALON_MAHASISWA_DATA
			$this->db->update('calon_mahasiswa_data', array(
				'id_kelompok_biaya_1'	=> empty($id_kelompok_biaya_1) ? NULL : $id_kelompok_biaya_1
			), $where);
		}
		
		// update ke CALON_MAHASISWA_DATA
		$this->db->update('calon_mahasiswa_data', array(
			'is_setuju_pernyataan'	=> $this->input->post('is_setuju_pernyataan')
		), $where);
		
		$this->db->trans_complete();
		
	}
	
	function get_list_syarat_umum($id_c_mhs, $id_penerimaan)
	{
		$result = array();
		
		$this->query = $this->db
			->select('psp.*, file_syarat, pesan_verifikator, nama_file, nvl(is_verified,0) as is_verified')
			->from('penerimaan_syarat_prodi psp')
			->join('calon_mahasiswa_syarat cms', 'cms.id_syarat_prodi = psp.id_syarat_prodi and cms.id_c_mhs = '. $id_c_mhs, 'LEFT')
			->where(array('status_file' => 1, 'id_program_studi' => NULL, 'id_penerimaan' => $id_penerimaan))
			->order_by('urutan', 'asc')
			->get();
		
		foreach ($this->query->result() as $row)
		{
			array_push($result, $row);
		}
		
		return $result;
	}
	
	function get_list_syarat_prodi($id_c_mhs, $id_penerimaan, $id_pilihan_1 = NULL, $id_pilihan_2 = NULL, $id_pilihan_3 = NULL, $id_pilihan_4 = NULL)
	{
		$result = array();
		
		if ($id_pilihan_1 != NULL)
		{
			$this->query = $this->db->select('nm_program_studi')->get_where('program_studi', array('id_program_studi' => $id_pilihan_1));
			$row = $this->query->row();
			$row->syarat_set = array();
			
			$this->query = $this->db
				->select('psp.*, file_syarat, pesan_verifikator, nvl(is_verified,0) as is_verified')
				->from('penerimaan_syarat_prodi psp')
				->join('calon_mahasiswa_syarat cms', 'cms.id_syarat_prodi = psp.id_syarat_prodi and cms.id_c_mhs = '. $id_c_mhs, 'LEFT')
				->where(array('status_file' => 1, 'id_program_studi' => $id_pilihan_1, 'id_penerimaan' => $id_penerimaan))
				->order_by('urutan', 'asc')
				->get();
			
			foreach ($this->query->result() as $row2)
			{
				array_push($row->syarat_set, $row2);
			}
			
			array_push($result, $row);
		}
		
		if ($id_pilihan_2 != NULL)
		{
			$this->query = $this->db->select('nm_program_studi')->get_where('program_studi', array('id_program_studi' => $id_pilihan_2));
			$row = $this->query->row();
			$row->syarat_set = array();
			
			$this->query = $this->db
				->select('psp.*, file_syarat, pesan_verifikator, nvl(is_verified,0) as is_verified')
				->from('penerimaan_syarat_prodi psp')
				->join('calon_mahasiswa_syarat cms', 'cms.id_syarat_prodi = psp.id_syarat_prodi and cms.id_c_mhs = '. $id_c_mhs, 'LEFT')
				->where(array('status_file' => 1, 'id_program_studi' => $id_pilihan_2, 'id_penerimaan' => $id_penerimaan))
				->order_by('urutan', 'asc')
				->get();
			
			foreach ($this->query->result() as $row2)
			{
				array_push($row->syarat_set, $row2);
			}
			
			array_push($result, $row);
		}
		
		if ($id_pilihan_3 != NULL)
		{
			$this->query = $this->db->select('nm_program_studi')->get_where('program_studi', array('id_program_studi' => $id_pilihan_3));
			$row = $this->query->row();
			$row->syarat_set = array();
			
			$this->query = $this->db
				->select('psp.*, file_syarat, pesan_verifikator, nvl(is_verified,0) as is_verified')
				->from('penerimaan_syarat_prodi psp')
				->join('calon_mahasiswa_syarat cms', 'cms.id_syarat_prodi = psp.id_syarat_prodi and cms.id_c_mhs = '. $id_c_mhs, 'LEFT')
				->where(array('status_file' => 1, 'id_program_studi' => $id_pilihan_3, 'id_penerimaan' => $id_penerimaan))
				->order_by('urutan', 'asc')
				->get();
			
			foreach ($this->query->result() as $row2)
			{
				array_push($row->syarat_set, $row2);
			}
			
			array_push($result, $row);
		}
		
		if ($id_pilihan_4 != NULL)
		{
			$this->query = $this->db->select('nm_program_studi')->get_where('program_studi', array('id_program_studi' => $id_pilihan_4));
			$row = $this->query->row();
			$row->syarat_set = array();
			
			$this->query = $this->db
				->select('psp.*, file_syarat, pesan_verifikator, nvl(is_verified,0) as is_verified')
				->from('penerimaan_syarat_prodi psp')
				->join('calon_mahasiswa_syarat cms', 'cms.id_syarat_prodi = psp.id_syarat_prodi and cms.id_c_mhs = '. $id_c_mhs, 'LEFT')
				->where(array('status_file' => 1, 'id_program_studi' => $id_pilihan_4, 'id_penerimaan' => $id_penerimaan))
				->order_by('urutan', 'asc')
				->get();
			
			foreach ($this->query->result() as $row2)
			{
				array_push($row->syarat_set, $row2);
			}
			
			array_push($result, $row);
		}
		
		return $result;
	}
	
	function update_file_foto($id_c_mhs, $file = '', $pesan = null, $is_verified = 0, $nama_file = null)
	{
		$this->db->update('calon_mahasiswa_file', array(
			'file_foto'			=> $file,
			'pesan_v_foto'		=> $pesan,
			'is_foto_verified'	=> $is_verified,
			'nama_file_foto'	=> $nama_file
		), array('id_c_mhs' => $id_c_mhs));
	}
	
	function update_file_berkas_pernyataan($id_c_mhs, $file = '', $pesan = null, $is_verified = 0, $nama_file = null)
	{
		$set = array(
			'file_berkas_pernyataan'		=> $file,
			'pesan_v_berkas_pernyataan'		=> $pesan,
			'is_berkas_pernyataan_verified'	=> $is_verified,
			'nama_file_berkas_pernyataan'	=> $nama_file
		);
		
		$this->db->update('calon_mahasiswa_file', $set, array('id_c_mhs' => $id_c_mhs));
	}
	
	function delete_syarat($id_c_mhs, $id_syarat_prodi)
	{
		$this->db->delete('calon_mahasiswa_syarat', array('id_c_mhs' => $id_c_mhs, 'id_syarat_prodi' => $id_syarat_prodi));
	}
	
	function insert_syarat($id_c_mhs, $id_syarat_prodi, $file_syarat, $nama_file = null)
	{
		$set = array(
			'id_c_mhs'			=> $id_c_mhs,
			'id_syarat_prodi'	=> $id_syarat_prodi,
			'file_syarat'		=> $file_syarat,
			'nama_file'			=> $nama_file
		);
		
		$this->db->insert('calon_mahasiswa_syarat', $set);
	}
	
	function insert_syarat_langsungvalid($id_c_mhs, $id_syarat_prodi, $file_syarat, $nama_file = null)
	{
		$set = array(
			'id_c_mhs'			=> $id_c_mhs,
			'id_syarat_prodi'	=> $id_syarat_prodi,
			'file_syarat'		=> $file_syarat,
			'nama_file'			=> $nama_file,
			'is_verified'		=> '1'
		);
		
		$this->db->insert('calon_mahasiswa_syarat', $set);
	}
	
	function update_submit_verifikasi($id_c_mhs, $tgl_submit_verifikasi)
	{
		if ($tgl_submit_verifikasi != NULL)
		{
			$this->db->set('tgl_submit_verifikasi', "to_date('{$tgl_submit_verifikasi}', 'DD-MON-YYYY HH24:MI:SS')", FALSE);
		}
		else
		{
			$this->db->set('tgl_submit_verifikasi', $tgl_submit_verifikasi);
			//$this->db->where('tgl_submit_verifikasi IS NULL', NULL, FALSE);
			$this->db->where('is_proses_verifikasi IS NULL', NULL, FALSE);
		}
		
		$this->db->where('id_c_mhs', $id_c_mhs);
		$this->db->update('calon_mahasiswa_baru');
	}

	function update_submit_verifikasi_baru($id_c_mhs, $tgl_submit_verifikasi)
	{
		if ($tgl_submit_verifikasi != NULL)
		{
			$this->db->set('tgl_submit_verifikasi', "to_date('{$tgl_submit_verifikasi}', 'DD-MON-YYYY HH24:MI:SS')", FALSE);
			$this->db->set('tgl_verifikasi_ppmb', "to_date('{$tgl_submit_verifikasi}', 'DD-MON-YYYY HH24:MI:SS')", FALSE);
			//$this->db->set('is_proses_verifikasi', '0');
			$this->db->set('status_verifikasi', '1');
		}
		else
		{
			$this->db->set('tgl_submit_verifikasi', $tgl_submit_verifikasi);
			/*$this->db->set('is_proses_verifikasi', '0');
			$this->db->set('status_verifikasi', '1');*/
			//$this->db->where('tgl_submit_verifikasi IS NULL', NULL, FALSE);
			$this->db->where('is_proses_verifikasi IS NULL', NULL, FALSE);
		}
		
		$this->db->where('id_c_mhs', $id_c_mhs);
		$this->db->update('calon_mahasiswa_baru');
	}
	
	/**
	 * Mengosongi pesan verifikator
	 * @param int $id_c_mhs
	 */
	function kosongi_pesan_verifikator($id_c_mhs)
	{	
		$this->db->set('pesan_verifikator', '');
		$this->db->where('id_c_mhs', $id_c_mhs);
		$this->db->update('calon_mahasiswa_baru');
	}
	
	/**
	 * Tambahkan count masuk
	 * @param int $id_c_mhs
	 * @param (tambah, kurang) $tambah_kurang
	 */
	function count_masuk($id_c_mhs, $tambah_kurang)
	{	
		$jumcount = 0;
		$this->query = $this->db
			->select('PESAN_V_BERKAS_PERNYATAAN')->from('calon_mahasiswa_file')
			->where(array(
				'id_c_mhs'			=> $id_c_mhs
			))->get();
		if ($this->query->num_rows() == 1)
		{
			if($this->query->row()->PESAN_V_BERKAS_PERNYATAAN=='') { $jumcount=0; }else { $jumcount = $this->query->row()->PESAN_V_BERKAS_PERNYATAAN; }
		}
		if( $tambah_kurang=='tambah' )
		{
			$jumcount++;
		}
		else if( $tambah_kurang=='kurang' )
		{
			if($jumcount > 0){ $jumcount--; }
		}
		
		$this->db->set('PESAN_V_BERKAS_PERNYATAAN', $jumcount);
		$this->db->where('id_c_mhs', $id_c_mhs);
		$this->db->update('calon_mahasiswa_file');
	}
	
	/**
	 * Mendapatkan status syarat sudah verified semua
	 * @param Array $cmb
	 * @param Array $cmf
	 * @param Array $syarat_umum_set
	 * @param Array $syarat_prodi_set
	 * @return int <p>Kode hasil verifikasi:
	 * 0 - Belum di submit / belum lengkap
	 * 1 - Verifikasi OK tidak ada masalah
	 * 2 - Verifikasi ada catatan dari verifikator
	 * </p>
	 */
	function is_syarat_verified(&$cmf, &$syarat_umum_set, &$syarat_prodi_set)
	{
		$result = 0;
		
		if ($cmf->IS_FOTO_VERIFIED)
		{
			$result = 1;
		}
		else if ($cmf->PESAN_V_FOTO != '')
		{
			$result = 2;
		}
		
		if ($result == 1)
		{			
			foreach ($syarat_umum_set as $su)
			{	
				if ($su->STATUS_WAJIB == 1)
				{
					if ($su->IS_VERIFIED == 1)
					{
						$result = 1;
					}
					else if ($su->PESAN_VERIFIKATOR != '')
					{
						$result = 2;
						break;
					}
					else
					{
						$result = 0;
						break;
					}
				}
				else
				{
					if ($su->PESAN_VERIFIKATOR != '')
					{
						$result = 2;
						break;
					}
					else
					{
						$result = 1;
					}
				}
			}
		}
		
		if ($result == 1)
		{
			foreach ($syarat_prodi_set as $prodi)
			{
				foreach ($prodi->syarat_set as $sp)
				{
					if ($sp->STATUS_WAJIB == 1)
					{
						if ($sp->IS_VERIFIED)
						{
							$result = 1;
						}
						else if ($sp->PESAN_VERIFIKATOR != '')
						{
							$result = 2;
							break 2;
						}
						else
						{
							$result = 0;
							break 2;
						}
					}
					else
					{
						if ($sp->PESAN_VERIFIKATOR != '')
						{
							$result = 2;
							break 2;
						}
						else
						{
							$result = 1;
						}
					}
				}
			}
		}
		
		return $result;
	}
	
	/**
	 * Mendapatkan status syarat sudah komplit semua
	 * @param Array $cmb
	 * @param Array $cmf
	 * @param Array $syarat_umum_set
	 * @param Array $syarat_prodi_set
	 * @return boolean
	 */
	function is_syarat_complete(&$cmf, &$syarat_umum_set, &$syarat_prodi_set)
	{
		$result = TRUE;
		
		// belum upload atau ada pesan dari verifikator
		if ($cmf->FILE_FOTO == '' || $cmf->PESAN_V_FOTO != '') 
		{
			$result = FALSE; 
		}
		
		if ($result == TRUE)
		{
			foreach ($syarat_umum_set as $su)
			{
				// Jika ada catatan dari verifikator (wajib/tdk wajib)
				if ($su->PESAN_VERIFIKATOR != '')
				{
					$result = FALSE;
					break;
				}
				// jika wajib dan belum upload
				else if ($su->STATUS_WAJIB == 1 && $su->FILE_SYARAT == '')
				{
					$result = FALSE;
					break;
				}
			}
		}
		
		if ($result == TRUE)
		{
			foreach ($syarat_prodi_set as $prodi)
			{
				foreach ($prodi->syarat_set as $sp)
				{
					// Jika ada catatan dari verifikator (wajib/tdk wajib)
					if ($sp->PESAN_VERIFIKATOR != '')
					{
						$result = FALSE;
						break;
					}
					// jika wajib dan belum upload
					else if ($sp->STATUS_WAJIB == 1 && $sp->FILE_SYARAT == '')
					{
						$result = FALSE;
						break;
					}
				}
			}
		}
		
		return $result;
	}
	
	/**
	 * Pengesetan nomer ujian peserta 
	 * @param string $kode_voucher
	 * @return string <p>
	 * SUDAH_AMBIL : pengambilan nomer berhasil<br/>
	 * RUANG_KOSONG : nomer ujian tidak ada<br/>
	 * COMPLETE : pengambilan berhasil
	 * </p>
	 */
	function set_no_ujian($kode_voucher)
	{
		// Transaksi Atomic
		$this->db->trans_start();
		
		// Mendapatkan row CMB
		$this->query = $this->db
			->select('id_c_mhs, no_ujian, cmb.id_penerimaan, kode_jurusan, id_pilihan_1, p.id_jalur, p.pengambilan_nomor')
			->from('calon_mahasiswa_baru cmb')
			->join('penerimaan p', 'p.id_penerimaan = cmb.id_penerimaan')
			->where(array('cmb.kode_voucher' => $kode_voucher))
			->get();
		$cmb = $this->query->row();
		
		// clear
		$this->query->free_result();
		
		// Jika nomer ujian sudah keluar, berarti sudah pernah 
		// Mencegah doubel submit
		if ($cmb->NO_UJIAN != '')
		{
			return 'SUDAH_AMBIL';
		}
		
		// Mendapatkan data Jadwal Tersedia / pengambilan nomer
		$this->db
			->select('jp.id_jadwal_ppmb, pjp.id_plot_jadwal_ppmb, no_ujian')
			->from('jadwal_ppmb jp')
			->join('plot_jadwal_ppmb pjp', 'pjp.id_jadwal_ppmb = jp.id_jadwal_ppmb')
			->where(array(
				'jp.status_aktif'	=> 1,
				'jp.id_penerimaan'	=> $cmb->ID_PENERIMAAN,
				'jp.kode_jalur'		=> $cmb->KODE_JURUSAN,
				'pjp.id_c_mhs'		=> NULL
			));
		
		// Persiapan ekslusi beberapa nomer ujian khusus :
		// pasca, profesi, spesialis
		/*if (in_array($cmb->ID_JALUR, array(6, 27, 23, 24, 32, 34)))  
		{
			// MIH - Ilmu Hukum
			if ($cmb->ID_PILIHAN_1 == 55)
			{
				$this->db->where(array('substr(pjp.no_ujian,5,2)' => '02'));
			} 

			// MHP - Sains Hukum Pembangunan
			if ($cmb->ID_PILIHAN_1 == 52)
			{
				$this->db->where(array('substr(pjp.no_ujian,5,2)' => '03'));
			}  

			// MKN - Magister Kenotariatan
			if ($cmb->ID_PILIHAN_1 == 54)
			{
				$this->db->where(array('substr(pjp.no_ujian,5,2)' => '04'));
			}  

			// MM - Magister Manajemen
			if ($cmb->ID_PILIHAN_1 == 73)
			{
				$this->db->where(array('substr(pjp.no_ujian,5,2)' => '05'));
			}
			
			// Profesi Akuntansi
			if ($cmb->ID_PILIHAN_1 == 63)
			{
				$this->db->where(array('substr(pjp.no_ujian,5,2)' => '01'));
			}  

			// Profesi Dokter Hewan
			if ($cmb->ID_PILIHAN_1 == 83)
			{
				$this->db->where(array('substr(pjp.no_ujian,5,2)' => '02'));
			}
		}*/
		
		// add ordering + compile query
		$this->db->order_by('pjp.no_ujian', 'asc');
		$this->query = $this->db->get();
		
		// saat tidak ada nomer ujian yang muncul return error
		if ($this->query->num_rows() == 0)
		{
			return 'RUANG_KOSONG';
		}
		
		// Persiapan untuk ordering pemilihan ACAK / URUT
		if ($cmb->PENGAMBILAN_NOMOR == 1)	// acak
		{
			// Proses acak
			$urutan = rand(1, $this->query->num_rows()) - 1;
		}
		else if ($cmb->PENGAMBILAN_NOMOR == 2) // urut
		{
			// ambil urutan pertama
			$urutan = 0;
		}
		
		// ambil record JADWAL_PPMB + PLOT_JADWAL_PPMB sesuai urutan
		$jadwal = $this->query->row($urutan);
				
		// clear result
		$this->query->free_result();
		
		// Proses Update CMB
		$this->db->set('tgl_ambil_no_ujian', 'to_date(\''. date('Y-m-d H:i:s') .'\', \'YYYY-MM-DD HH24:MI:SS\')', false);
		$this->db->update('calon_mahasiswa_baru', array('no_ujian' => $jadwal->NO_UJIAN), array('id_c_mhs' => $cmb->ID_C_MHS));
		
		// Proses Update PLOT_JADWAL_PPMB
		$this->db->update('plot_jadwal_ppmb', array('id_c_mhs' => $cmb->ID_C_MHS), array('id_plot_jadwal_ppmb' => $jadwal->ID_PLOT_JADWAL_PPMB));
		
		// Proses Update JADWAL_PPMB
		$this->db->set('terisi', 'terisi + 1', false);
		$this->db->update('jadwal_ppmb', null, array('id_jadwal_ppmb' => $jadwal->ID_JADWAL_PPMB));
		
		// Transaksi Commit
		$this->db->trans_complete();
		
		return 'COMPLETE';
	}
	
	/**
	 * Mengecek nomer ujian apakah bisa diambil atau tidak
	 * @param string $kode_voucher
	 * @return string <p>
	 * SUDAH_AMBIL : peserta sudah mengambil nomer ujian<br/>
	 * RUANG_ADA : ruang tersedia,<br/>
	 * RUANG_KOSONG : ruang tidak tersedia (bisa belum aktif/belum dibuka)</p>
	 */
	function cek_no_ujian($kode_voucher)
	{
		// Mendapatkan row CMB
		$this->query = $this->db
			->select('id_c_mhs, no_ujian, cmb.id_penerimaan, kode_jurusan, id_pilihan_1, p.id_jalur, p.pengambilan_nomor')
			->from('calon_mahasiswa_baru cmb')
			->join('penerimaan p', 'p.id_penerimaan = cmb.id_penerimaan')
			->where(array('cmb.kode_voucher' => $kode_voucher))
			->get();
		$cmb = $this->query->row();
		
		// clear
		$this->query->free_result();
		
		// Jika nomer ujian sudah keluar, berarti sudah pernah 
		// Mencegah doubel submit
		if ($cmb->NO_UJIAN != '')
		{
			return 'SUDAH_AMBIL';
		}
		
		// Mendapatkan data Jadwal Tersedia / pengambilan nomer
		$this->db
			->select('count(pjp.id_plot_jadwal_ppmb) as jumlah')
			->from('jadwal_ppmb jp')
			->join('plot_jadwal_ppmb pjp', 'pjp.id_jadwal_ppmb = jp.id_jadwal_ppmb')
			->where(array(
				'jp.status_aktif'	=> 1,
				'jp.id_penerimaan'	=> $cmb->ID_PENERIMAAN,
				'jp.kode_jalur'		=> $cmb->KODE_JURUSAN,
				'pjp.id_c_mhs'		=> NULL
			));
		
		// Persiapan ekslusi beberapa nomer ujian khusus :
		// pasca, profesi, spesialis
		/*if (in_array($cmb->ID_JALUR, array(6, 27, 23, 24, 32, 34)))  
		{
			// MIH - Ilmu Hukum
			if ($cmb->ID_PILIHAN_1 == 55)
			{
				$this->db->where(array('substr(pjp.no_ujian,5,2)' => '02'));
			} 

			// MHP - Sains Hukum Pembangunan
			if ($cmb->ID_PILIHAN_1 == 52)
			{
				$this->db->where(array('substr(pjp.no_ujian,5,2)' => '03'));
			}  

			// MKN - Magister Kenotariatan
			if ($cmb->ID_PILIHAN_1 == 54)
			{
				$this->db->where(array('substr(pjp.no_ujian,5,2)' => '04'));
			}  

			// MM - Magister Manajemen
			if ($cmb->ID_PILIHAN_1 == 73)
			{
				$this->db->where(array('substr(pjp.no_ujian,5,2)' => '05'));
			}
			
			// Profesi Akuntansi
			if ($cmb->ID_PILIHAN_1 == 63)
			{
				$this->db->where(array('substr(pjp.no_ujian,5,2)' => '01'));
			}  

			// Profesi Dokter Hewan
			if ($cmb->ID_PILIHAN_1 == 83)
			{
				$this->db->where(array('substr(pjp.no_ujian,5,2)' => '02'));
			}
		}
		*/
		// compile query
		$this->query = $this->db->get();
		
		// ambil first row
		$row = $this->query->row();
		
		// Jika Ruang ada
		if ($row->JUMLAH > 0)
		{
			return 'RUANG_ADA';
		}
		else // jika tidak
		{
			return 'RUANG_KOSONG';
		}
	}
	
	/**
	 * Pembatalan pengambilan nomer ujian
	 * @param string $kode_voucher
	 */
	function unset_no_ujian($kode_voucher)
	{
		// Mendapatkan row CMB
		$this->query = $this->db
			->select('id_c_mhs')
			->from('calon_mahasiswa_baru cmb')
			->where('cmb.no_ujian is not null', null, false)			// memastikan tidak kosong
			->where('cmb.tgl_ambil_no_ujian is not null', null, false)	// memastikan tidak kosong
			->where(array('cmb.kode_voucher' => $kode_voucher))
			->get();
		$cmb = $this->query->row();
		
		// Batalkan transaksi jika tidak ditemukan
		if ($cmb == null)
		{
			return 'GAGAL';
		}
		
		// ---------------------------------------
		// Membatalkan transaksi pengambilan nomer
		// ---------------------------------------
		
		// Start Transaction
		$this->db->trans_start();
		
		// Kosongkan No Ujian di CMB
		$this->db->update('calon_mahasiswa_baru', array(
			'no_ujian'				=> null,
			'tgl_ambil_no_ujian'	=> null,
			'tgl_input_pin'			=> null
		), array('id_c_mhs' => $cmb->ID_C_MHS));
		
		// Kurangi terisi
		$this->db
			->set('terisi', 'terisi - 1', false)
			->where("id_jadwal_ppmb in (select id_jadwal_ppmb from aucc.plot_jadwal_ppmb where id_c_mhs = {$cmb->ID_C_MHS})", null, false)
			->update('jadwal_ppmb');
			
		// Kosongi pengambilan
		$this->db->update('plot_jadwal_ppmb', array(
			'id_c_mhs'	=> null
		), array('id_c_mhs' => $cmb->ID_C_MHS));
		
		// Commit Transaction
		$this->db->trans_complete();
		
		return 'COMPLETE';
	}

	/**
	 * Mendapatkan list cmb untuk di download data isian biodatanya
	 * @param int $id_penerimaan
	 * @return Object Object array
	 */
	function list_cmb_download_pdf($id_penerimaan,$id_c_mhs)
	{
		// --------------------------
		// Mendapatkan row PENERIMAAN
		// --------------------------
		$this->query = $this->db->get_where('penerimaan', array('id_penerimaan' => $id_penerimaan));
		$penerimaan = $this->query->first_row();
		$this->query->free_result();
		
		// ---------------------
		// Mendapatkan list CMB
		// ---------------------
		$this->query = $this->db
			->select(
				"/* FORM DAN PILIHAN PESERTA */
				no_ujian, kode_voucher, kode_jurusan, 
				ps1.nm_program_studi as nm_pilihan_1, 
				ps2.nm_program_studi as nm_pilihan_2,
				ps3.nm_program_studi as nm_pilihan_3,
				ps4.nm_program_studi as nm_pilihan_4,
				nm_prodi_minat, id_pilihan_1, id_pilihan_2, id_pilihan_3, id_pilihan_4, 
				cmp.id_prodi_minat, cmb.id_kota_lahir, cmb.id_kota, cmb.kewarganegaraan, cmb.id_agama, cmb.sumber_biaya, cmb.id_disabilitas,
				cmb.nm_jenis_tinggal, cmb.nomor_hp, cmb.nomor_kps, cmb.nik_c_mhs,
				
				/* BIODATA */
				nm_c_mhs, jenis_kelamin, gelar, tgl_lahir, kl.nm_kota as nm_kota_lahir, 
				cmb.alamat_jalan, cmb.alamat_dusun, cmb.alamat_kelurahan, cmb.alamat_kecamatan, cmb.alamat_kodepos, 
				ka.nm_kota as nm_kota_alamat, ka.tipe_dati2 as tipe_dati2_alamat, telp, cmb.email, nm_kewarganegaraan, nm_agama,
				nm_sumber_biaya, dis.nm_disabilitas as nm_disabilitas,

				/* INFO SEKOLAH */
				nisn, s.nm_sekolah, ks.nm_kota as nm_kota_sekolah, kode_jurusan, js.nm_jurusan_sekolah, tahun_lulus, no_ijazah, tgl_ijazah,
				jumlah_pelajaran_ijazah, nilai_ijazah, jumlah_pelajaran_uan, nilai_uan,

				/* INFO ORANG TUA AYAH */
				nama_ayah, alamat_ayah, telp_ayah, po_a.nm_pendidikan_ortu as nm_pendidikan_ayah, p_a.nm_pekerjaan as nm_pekerjaan_ayah, 
				penghasilan_ayah, instansi_ayah, jabatan_ayah, masa_kerja_ayah, 
				tgl_lahir_ayah, k2.nm_kota as nm_kota_alamat_ayah, 
				k2.tipe_dati2 as tipe_dati2_alamat_ayah, dis2.nm_disabilitas as nm_disabilitas_ayah,
				/* INFO ORANG TUA IBU */
				nama_ibu, alamat_ibu, telp_ibu, po_i.nm_pendidikan_ortu as nm_pendidikan_ibu, p_i.nm_pekerjaan as nm_pekerjaan_ibu, 
				penghasilan_ibu, instansi_ibu, jabatan_ibu, masa_kerja_ibu,
				tgl_lahir_ibu, k3.nm_kota as nm_kota_alamat_ibu, 
				k3.tipe_dati2 as tipe_dati2_alamat_ibu, dis3.nm_disabilitas as nm_disabilitas_ibu,
				/* INFO ORANG TUA WALI */
				nama_wali, alamat_wali, telp_wali, po_w.nm_pendidikan_ortu as nm_pendidikan_wali, p_w.nm_pekerjaan as nm_pekerjaan_wali, 
				penghasilan_wali,
				tgl_lahir_wali, k4.nm_kota as nm_kota_alamat_wali, 
				k4.tipe_dati2 as tipe_dati2_alamat_wali, dis4.nm_disabilitas as nm_disabilitas_wali,

				/* INFO PENDIDIKAN SEBELUMNYA Pascasarjana */
				ptn_s1, status_ptn_s1, prodi_s1, tgl_masuk_s1, tgl_lulus_s1, lama_studi_s1, ip_s1, nim_lama_s1, jumlah_karya_ilmiah,
				ptn_s2, status_ptn_s2, prodi_s2, tgl_masuk_s2, tgl_lulus_s2, lama_studi_s2, ip_s2, nim_lama_s2,

				/* INFO PEKERJAAN */
				pekerjaan, asal_instansi, alamat_instansi, telp_instansi, nrp, karpeg, pangkat", FALSE)
			->from('calon_mahasiswa_baru cmb')
			->join('calon_mahasiswa_pasca cmp', 'cmp.id_c_mhs = cmb.id_c_mhs')
			->join('calon_mahasiswa_sekolah cms', 'cms.id_c_mhs = cmb.id_c_mhs')
			->join('calon_mahasiswa_ortu cmo', 'cmo.id_c_mhs = cmb.id_c_mhs')
			->join('program_studi ps1',			'ps1.id_program_studi = cmb.id_pilihan_1', 'LEFT')
			->join('program_studi ps2',			'ps2.id_program_studi = cmb.id_pilihan_2', 'LEFT')
			->join('program_studi ps3',			'ps3.id_program_studi = cmb.id_pilihan_3', 'LEFT')
			->join('program_studi ps4',			'ps4.id_program_studi = cmb.id_pilihan_4', 'LEFT')
			->join('prodi_minat pm',			'pm.id_prodi_minat = cmp.id_prodi_minat', 'LEFT')
			->join('kota ka', 					'ka.id_kota = cmb.id_kota', 'LEFT')
			->join('kota kl',					'kl.id_kota = cmb.id_kota_lahir', 'LEFT')
			->join('kewarganegaraan',			'kewarganegaraan.id_kewarganegaraan = cmb.kewarganegaraan', 'LEFT')
			->join('agama a',					'a.id_agama = cmb.id_agama')
			->join('sumber_biaya sb',			'sb.id_sumber_biaya = cmb.sumber_biaya')
			->join('disabilitas dis',			'dis.id_disabilitas = cmb.id_disabilitas')
			->join('sekolah s',					's.id_sekolah = cms.id_sekolah_asal', 'LEFT')
			->join('kota ks',					'ks.id_kota = s.id_kota', 'LEFT')
			->join('jurusan_sekolah_cmhs js',	'js.id_jurusan_sekolah = cms.jurusan_sekolah', 'LEFT')
			->join('pendidikan_ortu po_a',		'po_a.id_pendidikan_ortu = cmo.pendidikan_ayah', 'LEFT')
			->join('pendidikan_ortu po_i',		'po_i.id_pendidikan_ortu = cmo.pendidikan_ibu', 'LEFT')
			->join('pekerjaan p_a',				'p_a.id_pekerjaan = cmo.pekerjaan_ayah', 'LEFT')
			->join('pekerjaan p_i',				'p_i.id_pekerjaan = cmo.pekerjaan_ibu', 'LEFT')
			->join('kota k2', 					'k2.id_kota = cmo.id_kota_ayah', 'LEFT')
			->join('kota k3', 					'k3.id_kota = cmo.id_kota_ibu', 'LEFT')
			->join('disabilitas dis2',			'dis2.id_disabilitas = cmo.id_disabilitas_ayah')
			->join('disabilitas dis3',			'dis3.id_disabilitas = cmo.id_disabilitas_ibu')

			->join('pendidikan_ortu po_w',		'po_w.id_pendidikan_ortu = cmo.pendidikan_wali', 'LEFT')
			->join('pekerjaan p_w',				'p_w.id_pekerjaan = cmo.pekerjaan_wali', 'LEFT')
			->join('kota k4', 					'k4.id_kota = cmo.id_kota_wali', 'LEFT')
			->join('disabilitas dis4',			'dis4.id_disabilitas = cmo.id_disabilitas_wali', 'LEFT')
			/*->where('tgl_ambil_no_ujian is not null', NULL, FALSE)*/
			->where('id_penerimaan', $id_penerimaan)
			->where('cmb.id_c_mhs', $id_c_mhs)
			// ->where_in('no_ujian', array('86150100006','86150200014','86150300028'))
			->order_by('cmb.no_ujian asc')
			->get();
		
		// Ambil result rows-nya
		$cmb_set = $this->query->result();
		$this->query->free_result();
		
		// Attach data penerimaan, di tiap row
		foreach ($cmb_set as &$row) 
		{ 
			$row->PENERIMAAN = $penerimaan;
		}
		
		// Mendambahkan data2 tambahan
		foreach ($cmb_set as &$cmb)
		{
			// Pilihan 1
			if ($cmb->ID_PILIHAN_1 != '')
			{
				$this->query = $this->db->select('nm_program_studi')->get_where('program_studi', array('id_program_studi' => $cmb->ID_PILIHAN_1));
				$cmb->PILIHAN_1 = $this->query->first_row();
				$this->query->free_result();
			}
			
			// Pilihan 2
			if ($cmb->ID_PILIHAN_2 != '')
			{
				$this->query = $this->db->select('nm_program_studi')->get_where('program_studi', array('id_program_studi' => $cmb->ID_PILIHAN_2));
				$cmb->PILIHAN_2 = $this->query->first_row();
				$this->query->free_result();
			}
			
			// Pilihan 3
			if ($cmb->ID_PILIHAN_3 != '')
			{
				$this->query = $this->db->select('nm_program_studi')->get_where('program_studi', array('id_program_studi' => $cmb->ID_PILIHAN_3));
				$cmb->PILIHAN_3 = $this->query->first_row();
				$this->query->free_result();
			}
			
			// Pilihan 4
			if ($cmb->ID_PILIHAN_4 != '')
			{
				$this->query = $this->db->select('nm_program_studi')->get_where('program_studi', array('id_program_studi' => $cmb->ID_PILIHAN_4));
				$cmb->PILIHAN_4 = $this->query->first_row();
				$this->query->free_result();
			}
			
			// Prodi Minat
			if ($cmb->ID_PRODI_MINAT != '')
			{
				$this->query = $this->db->select('nm_prodi_minat')->get_where('prodi_minat', array('id_prodi_minat' => $cmb->ID_PRODI_MINAT));
				$cmb->PRODI_MINAT = $this->query->first_row();
				$this->query->free_result();
			}
			
			// Kota Lahir
			$this->query = $this->db->select('nm_kota, tipe_dati2')->get_where('kota', array('id_kota' => $cmb->ID_KOTA_LAHIR));
			$cmb->KOTA_LAHIR = $this->query->first_row();
			$this->query->free_result();
			
			// Kota Alamat
			$this->query = $this->db->select('nm_kota, tipe_dati2')->get_where('kota', array('id_kota' => $cmb->ID_KOTA));
			$cmb->KOTA = $this->query->first_row();
			$this->query->free_result();
			
			// Kewarganegaraan
			$this->query = $this->db->select('nm_kewarganegaraan')->get_where('kewarganegaraan', array('id_kewarganegaraan' => $cmb->KEWARGANEGARAAN));
			$cmb->KEWARGANEGARAAN = $this->query->first_row();
			$this->query->free_result();
			
			// Agama
			$this->query = $this->db->select('nm_agama')->get_where('agama', array('id_agama' => $cmb->ID_AGAMA));
			$cmb->AGAMA = $this->query->first_row();
			$this->query->free_result();
			
			// Sumber Biaya
			$this->query = $this->db->select('nm_sumber_biaya')->get_where('sumber_biaya', array('id_sumber_biaya' => $cmb->SUMBER_BIAYA));
			$cmb->SUMBER_BIAYA = $this->query->first_row();
			$this->query->free_result();
			
			// Disabilitas
			$this->query = $this->db->select('nm_disabilitas')->get_where('disabilitas', array('id_disabilitas' => $cmb->ID_DISABILITAS));
			$cmb->DISABILITAS = $this->query->first_row();
			$this->query->free_result();

		}
		
		return $cmb_set;
	}
	
	/**
	 * Mendapatkan list cmb untuk di download data isian biodatanya
	 * @param int $id_penerimaan
	 * @return Object Object array
	 */
	function list_cmb_download_pdf_lama($id_penerimaan,$id_c_mhs)
	{
		// --------------------------
		// Mendapatkan row PENERIMAAN
		// --------------------------
		$this->query = $this->db->get_where('penerimaan', array('id_penerimaan' => $id_penerimaan));
		$penerimaan = $this->query->first_row();
		$this->query->free_result();
		
		// ---------------------
		// Mendapatkan list CMB
		// ---------------------
		$cmb_set = array();
		
		$this->query = $this->db
			->select('cmb.*, cmp.id_prodi_minat')
			->from('calon_mahasiswa_baru cmb')
			->join('calon_mahasiswa_pasca cmp', 'cmp.id_c_mhs = cmb.id_c_mhs')
			/*->where('tgl_ambil_no_ujian is not null', null, false)*/
			->where('id_penerimaan', $id_penerimaan)
			->where('cmb.id_c_mhs', $id_c_mhs)
			->order_by('cmb.no_ujian asc')
			->get();
		
		foreach ($this->query->result() as $row) 
		{ 
			// tambahkan data penerimaan
			$row->PENERIMAAN = $penerimaan;
			
			array_push($cmb_set, $row); 	
		}
		
		// Mendambahkan data2 tambahan
		foreach ($cmb_set as &$cmb)
		{
			// Pilihan 1
			if ($cmb->ID_PILIHAN_1 != '')
			{
				$this->query = $this->db->select('nm_program_studi')->get_where('program_studi', array('id_program_studi' => $cmb->ID_PILIHAN_1));
				$cmb->PILIHAN_1 = $this->query->first_row();
				$this->query->free_result();
			}
			
			// Pilihan 2
			if ($cmb->ID_PILIHAN_2 != '')
			{
				$this->query = $this->db->select('nm_program_studi')->get_where('program_studi', array('id_program_studi' => $cmb->ID_PILIHAN_2));
				$cmb->PILIHAN_2 = $this->query->first_row();
				$this->query->free_result();
			}
			
			// Pilihan 3
			if ($cmb->ID_PILIHAN_3 != '')
			{
				$this->query = $this->db->select('nm_program_studi')->get_where('program_studi', array('id_program_studi' => $cmb->ID_PILIHAN_3));
				$cmb->PILIHAN_3 = $this->query->first_row();
				$this->query->free_result();
			}
			
			// Pilihan 4
			if ($cmb->ID_PILIHAN_4 != '')
			{
				$this->query = $this->db->select('nm_program_studi')->get_where('program_studi', array('id_program_studi' => $cmb->ID_PILIHAN_4));
				$cmb->PILIHAN_4 = $this->query->first_row();
				$this->query->free_result();
			}
			
			// Prodi Minat
			if ($cmb->ID_PRODI_MINAT != '')
			{
				$this->query = $this->db->select('nm_prodi_minat')->get_where('prodi_minat', array('id_prodi_minat' => $cmb->ID_PRODI_MINAT));
				$cmb->PRODI_MINAT = $this->query->first_row();
				$this->query->free_result();
			}
			
			// Kota Lahir
			$this->query = $this->db->select('nm_kota, tipe_dati2')->get_where('kota', array('id_kota' => $cmb->ID_KOTA_LAHIR));
			$cmb->KOTA_LAHIR = $this->query->first_row();
			$this->query->free_result();
			
			// Kota Alamat
			$this->query = $this->db->select('nm_kota, tipe_dati2')->get_where('kota', array('id_kota' => $cmb->ID_KOTA));
			$cmb->KOTA = $this->query->first_row();
			$this->query->free_result();
			
			// Kewarganegaraan
			$this->query = $this->db->select('nm_kewarganegaraan')->get_where('kewarganegaraan', array('id_kewarganegaraan' => $cmb->KEWARGANEGARAAN));
			$cmb->KEWARGANEGARAAN = $this->query->first_row();
			$this->query->free_result();
			
			// Agama
			$this->query = $this->db->select('nm_agama')->get_where('agama', array('id_agama' => $cmb->ID_AGAMA));
			$cmb->AGAMA = $this->query->first_row();
			$this->query->free_result();
			
			// Sumber Biaya
			$this->query = $this->db->select('nm_sumber_biaya')->get_where('sumber_biaya', array('id_sumber_biaya' => $cmb->SUMBER_BIAYA));
			$cmb->SUMBER_BIAYA = $this->query->first_row();
			$this->query->free_result();
			
			// Disabilitas
			$this->query = $this->db->select('nm_disabilitas')->get_where('disabilitas', array('id_disabilitas' => $cmb->ID_DISABILITAS));
			$cmb->DISABILITAS = $this->query->first_row();
			$this->query->free_result();
			
			// ambil row CMP
			$this->query = $this->db->get_where('calon_mahasiswa_pasca', array('id_c_mhs' => $cmb->ID_C_MHS));
			$cmb->CMP = $this->query->first_row();
			$this->query->free_result();
		}
		
		return $cmb_set;
	}
	
	/**
	 * Mengupdate CMB.TGL_INPUT_PIN
	 * @param type $id_c_mhs
	 */
	function update_tgl_input_pin($id_c_mhs)
	{
		// Update row CMB.TGL_INPUT_PIN
		$this->db->set('tgl_input_pin', 'to_date(\''. date('Y-m-d H:i:s') .'\', \'YYYY-MM-DD HH24:MI:SS\')', false);
		$this->db->where('tgl_ambil_no_ujian is not null', null, false);	// memastikan sudah ambil nomer
		$this->db->update('calon_mahasiswa_baru', null, array('id_c_mhs' => $id_c_mhs));
	}
	
	
/**
	 * Get CMB.TGL_BATAS_VERIFIKASI
	 * @param type $id_c_mhs
	 */
	function get_batasakhir_verifikasi($id_c_mhs)
	{
		$this->query = $this->db
			->select("to_char(tgl_batas_verifikasi, 'YYYY-MM-DD HH24:MI:SS') as TGL_BATASAKHIR_VERIFIKASI")
			->where(array('id_c_mhs' => $id_c_mhs))
			->get('calon_mahasiswa_baru');
		
		return $this->query->row();
	}
}