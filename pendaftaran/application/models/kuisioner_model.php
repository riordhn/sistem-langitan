<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Kuisioner_model
 *
 * @author Fathoni <fathoni@staf.unair.ac.id>
 */
class Kuisioner_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_kuisioner($id_penerimaan)
	{
		// Mendapatkan Form
		$this->query = $this->db
			->select('*')->from('kuisioner_form')
			->where('tipe_form', 2)
			->where("id_form = (SELECT id_form_kuisioner FROM aucc.penerimaan WHERE id_penerimaan = {$id_penerimaan})", NULL, FALSE)
			->get();
		
		$form = $this->query->first_row();
		
		// clear result
		$this->query->free_result();
		
		// Mendapatkan kategori soal
		$this->query = $this->db
			->select('*')->from('kuisioner_kategori_soal')
			->where('id_form', $form->ID_FORM)
			->get();
		
		$form->kategori_soal_set = $this->query->result();
		
		// clear result
		$this->query->free_result();
		
		// Mendapatkan soal tiap kategori
		foreach ($form->kategori_soal_set as &$kategori_soal)
		{
			$this->query = $this->db
				->select('cast(nomer as number(4)), kuisioner_soal.*', FALSE)
				->from('kuisioner_soal')
				->where('id_kategori_soal', $kategori_soal->ID_KATEGORI_SOAL)
				->order_by(1, 'asc')
				->get();
			
			$kategori_soal->soal_set = $this->query->result();
			
			// clear result
			$this->query->free_result();
			
			// Mendapatkan pilihan jawaban tiap soal
			foreach ($kategori_soal->soal_set as &$soal)
			{
				$this->query = $this->db
					->select('cast(urutan as number(4)), kuisioner_jawaban.*', FALSE)
					->from('kuisioner_jawaban')
					->where('id_soal', $soal->ID_SOAL)
					->order_by(1, 'asc')
					->get();
				
				$soal->jawaban_set = $this->query->result();
				
				// clear result
				$this->query->free_result();
			}
		}
		
		return $form;
	}
	
	public function update_jawaban($id_c_mhs, $id_form, &$jawaban_set)
	{		
		// Mendapatkan ID_HASIL_FORM
		$this->query = $this->db->query("select aucc.KUISIONER_HASIL_FORM_SEQ.nextval from dual");
		$id_hasil_form = $this->query->first_row()->NEXTVAL;
		
		// Insert KUISIONER_HASIL_FORM
		$this->db->insert('kuisioner_hasil_form', array(
			'id_hasil_form'	=> $id_hasil_form,
			'id_form'		=> $id_form,
			'id_c_mhs'		=> $id_c_mhs
		));
		
		foreach ($jawaban_set as $id_soal => $id_jawaban)
		{
			// Mendapatkan ID_HASIL_SOAL
			$this->query = $this->db->query("select aucc.KUISIONER_HASIL_SOAL_SEQ.nextval from dual");
			$id_hasil_soal = $this->query->first_row()->NEXTVAL;
			
			// Insert KUISIONER_HASIL_SOAL
			$this->db->insert('kuisioner_hasil_soal', array(
				'id_hasil_soal'	=> $id_hasil_soal,
				'id_hasil_form'	=> $id_hasil_form,
				'id_soal'		=> $id_soal
			));
			
			// Insert KUISIONER_HASIL_JAWABAN
			$this->db->insert('kuisioner_hasil_jawaban', array(
				'id_hasil_soal'	=> $id_hasil_soal,
				'id_jawaban'	=> $id_jawaban
			));
		}
		
		// Update CMB telah isi kuisioner
		$this->db->update('calon_mahasiswa_baru', array(
			'is_isi_kuisioner'	=> 1
		), array('id_c_mhs' => $id_c_mhs));
	}
}