<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calon_Pendaftar_Model extends MY_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	function email_exist($email)
	{
		$this->query = $this->db->get_where('calon_pendaftar', array('email_pendaftar' => $email));
		return $this->query->num_rows();
	}

	function add_calon($nama, $telp, $email, $password_hash, $email_hash)
	{

		$sql = "INSERT INTO calon_pendaftar(nama_pendaftar,telp_pendaftar,email_pendaftar,pass_pendaftar,tgl_daftar,email_hash) 
				VALUES(?,?,?,?,TO_DATE('". date('d-m-Y')."','DD-MM-YYYY'),?)";
		$this->db->query($sql,array($nama,$telp,$email,$password_hash,$email_hash));
	}

	function cek_login($email, $password_hash)
	{
		$this->query = $this->db->get_where('calon_pendaftar', array(
			'email_pendaftar' => $email
		));

		if ($this->query->num_rows() == 0)
		{
			return 'USER_NOT_EXIST';
		}

		$calon_pendaftar = $this->query->row();

		if ($calon_pendaftar->TGL_KONFIRMASI_EMAIL == '')
		{
			return 'EMAIL_NOT_CONFIRMED';
		}
		
		// Jika terset password temporari
		if ($calon_pendaftar->PASS_TEMP != '')
		{
			// Jika berbeda
			if (sha1($calon_pendaftar->PASS_TEMP) != $password_hash)
			{
				return 'WRONG_PASSWORD';
			}
		}
		else if ($calon_pendaftar->PASS_PENDAFTAR != $password_hash)
		{
			return 'WRONG_PASSWORD';
		}

		return $calon_pendaftar;
	}
	
	function cek_login_voucher($kode_voucher, $pin_voucher, $id_pt)
	{
		// Query login menggunakan voucher (query dari aplikasi lama) -> sudah sesuai id_pt, ada penambahan parameter $id_pt
		$this->query = $this->db->query(
			"select v.id_voucher, p.id_penerimaan, p.is_bayar_voucher
			from aucc.voucher v
			join aucc.penerimaan p on p.id_penerimaan = v.id_penerimaan and p.id_perguruan_tinggi = ?
			where 
				v.kode_voucher = ? and 
				v.pin_voucher = ? and 
				p.is_aktif = 1 and 
				v.tgl_ambil is not null",
			array($id_pt, $kode_voucher, $pin_voucher));
		
		// Mendapatkan row voucher awal
		$voucher_awal = $this->query->first_row();

		// clear result
		$this->query->free_result();

		/*$is_bayar = $voucher_awal->IS_BAYAR_VOUCHER;*/

		// menghindari nilai null
		if(empty($voucher_awal->IS_BAYAR_VOUCHER)){
			$is_bayar = 0;
		}
		else{
			$is_bayar = $voucher_awal->IS_BAYAR_VOUCHER;
		}

		/*if(empty($voucher_awal->IS_BAYAR_VOUCHER)){
			return 'VOUCHER_NOT_FOUND';
			exit();
		}*/
		// untuk penerimaan tanpa pembayaran di awal
		if($is_bayar == 0){
			// Query login menggunakan voucher (query dari aplikasi lama)
			$this->query = $this->db->query(
				"select v.id_voucher, p.id_penerimaan, kode_voucher, pin_voucher, id_jalur, v.kode_jurusan, v.tgl_bayar
				from aucc.voucher v
				join aucc.penerimaan p on p.id_penerimaan = v.id_penerimaan and p.id_perguruan_tinggi = ?
				where 
					v.kode_voucher = ? and 
					v.pin_voucher = ? and 
					p.is_aktif = 1 and 
					v.tgl_ambil is not null",
				array($id_pt, $kode_voucher, $pin_voucher));
			
			// Mendapatkan row voucher
			$voucher = $this->query->first_row();
		
			// clear result
			$this->query->free_result();
		}
		// untuk penerimaan dengan pembayaran di awal
		else if($is_bayar == 1){
			// Query login menggunakan voucher (query dari aplikasi lama)
			$this->query = $this->db->query(
				"select v.id_voucher, p.id_penerimaan, kode_voucher, pin_voucher, id_jalur, v.kode_jurusan, v.tgl_bayar
				from aucc.voucher v
				join aucc.penerimaan p on p.id_penerimaan = v.id_penerimaan and p.id_perguruan_tinggi = ?
				where 
					v.kode_voucher = ? and 
					v.pin_voucher = ? and 
					p.is_aktif = 1 and 
					v.tgl_bayar is not null",
				array($id_pt, $kode_voucher, $pin_voucher));
			
			// Mendapatkan row voucher
			$voucher = $this->query->first_row();
		
			// clear result
			$this->query->free_result();
		}
		
		// Jika voucher ditemukan
		if ($voucher)
		{	
			/*if(empty($voucher->TGL_BAYAR)){
				$this->db->query("UPDATE VOUCHER SET TGL_BAYAR = SYSDATE WHERE ID_VOUCHER = '{$voucher->ID_VOUCHER}'");
			}*/
			
			// cek kode_voucher di CMB
			$this->query = $this->db
				->select('id_c_mhs')
				->from('calon_mahasiswa_baru cmb')
				->join('penerimaan p', 'p.id_penerimaan = cmb.id_penerimaan and p.id_perguruan_tinggi = '. $this->perguruan_tinggi['ID_PERGURUAN_TINGGI'], 'left')
				->where(array(
					'kode_voucher'	=> $voucher->KODE_VOUCHER,
					'pin_voucher'	=> $voucher->PIN_VOUCHER
				))->get();
			
			// Ambil row
			$cmb = $this->query->first_row();
			
			// clear result
			$this->query->free_result();
			
			// Jika belum ada, insert row baru
			if ( ! $cmb)
			{
				// Mendapatkan id_c_mhs terlebih dahulu
				$this->query = $this->db->query("select aucc.calon_mahasiswa_baru_seq.nextval from dual");
				$row = $this->query->row();
				$id_c_mhs = $row->NEXTVAL;
				$this->query->free_result();
				
				// Insert ke db
				$this->db->insert('calon_mahasiswa_baru', array(
					'id_c_mhs'		=> $id_c_mhs,
					'id_penerimaan'	=> $voucher->ID_PENERIMAAN,
					'id_jalur'		=> $voucher->ID_JALUR,
					'kode_voucher'	=> $voucher->KODE_VOUCHER,
					'pin_voucher'	=> $voucher->PIN_VOUCHER,
					'kode_jurusan'	=> $voucher->KODE_JURUSAN
				));
				
				// set ID_C_MHS_AKTIF
				$return_row->ID_C_MHS_AKTIF = $id_c_mhs;
			}
			else
			{
				// ambil id_c_mhs yg sudah ada
				$return_row->ID_C_MHS_AKTIF = $cmb->ID_C_MHS;
			}
			
			return $return_row;
		}
		else
		{
			return 'VOUCHER_NOT_FOUND';
		}
	}
	
	/**
	 * Mendapatkan record CALON_PENDAFTAR
	 * @param int $id_calon_pendaftar
	 * @return Object
	 */
	function get($id_calon_pendaftar)
	{
		$this->query = $this->db->get_where('calon_pendaftar', array('id_calon_pendaftar' => $id_calon_pendaftar));
		return $this->query->row();
	}
	
	function get_by_email($email)
	{
		$this->query = $this->db->get_where('calon_pendaftar', array('email_pendaftar' => $email));
		return $this->query->row();
	}
	
	function get_for_reset($id_calon_pendaftar, $email_hash, $pass_pendaftar)
	{
		$this->query = $this->db->get_where('calon_pendaftar', array(
			'id_calon_pendaftar'	=> $id_calon_pendaftar,
			'email_hash'			=> $email_hash,
			'pass_pendaftar'		=> $pass_pendaftar
		));
		
		return $this->query->row();
	}

	function set_password($email, $password_hash)
	{
		return $this->db->update('calon_pendaftar', array('pass_pendaftar' => $password_hash), array('email_pendaftar' => $email));
	}

	function verify_email($email_hash)
	{
		$sql = "UPDATE calon_pendaftar SET tgl_konfirmasi_email = TO_DATE('".date('d-m-Y')."','DD-MM-YYYY') WHERE email_hash = ?";
		return $this->db->query($sql,$email_hash);
		
	}
	
	function is_sudah_pilih_penerimaan($id_calon_pendaftar)
	{
		$result = true;
		
		$this->query = $this->db
			->select('id_c_mhs')
			->get_where('calon_mahasiswa_baru', array('id_calon_pendaftar' => $id_calon_pendaftar));
		
		if ($this->query->row())
		{
			$result = true;
		}
		else
		{
			$result = false;
		}
		
		return $result;
	}
	
	function is_submit_form($id_c_mhs)
	{
		$result = true;
		
		$this->query = $this->db
			->select('tgl_registrasi')
			->get_where('calon_mahasiswa_baru', array('id_c_mhs' => $id_c_mhs));
		
		return $result;
	}
	
	function aktifkan($id_calon_pendaftar, $id_c_mhs)
	{
		$this->db->update('calon_pendaftar', array('id_c_mhs_aktif' => $id_c_mhs), array('id_calon_pendaftar' => $id_calon_pendaftar));
	}
}
