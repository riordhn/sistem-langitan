<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * =================================================================
 * Proses generate file pdf ini di bagi-bagi menjadi tiap 10 peserta
 * hal ini dilakukan untuk membatasi beban server, dan untuk 
 * mempermudah proses mencetak, karena dilakukan proses pencetakan
 * secara bersama.
 * =================================================================
 */

if ( ! function_exists('generate_biodata_pdf'))
{
	

	function generate_biodata_pdf(&$cmb_set, $lokasi_files, $id_penerimaan)
	{
		$i_proses	= 0;
		$nomer_awal	= '';
		$nomer_akhir = '';

		foreach ($cmb_set as $cmb)
		{
			// Jika awal blok nomer atau baru dimulai
			if ($i_proses == 0) 
			{
				// Buat PDF Baru
				// Params: Orientation, Unit, Page, Unicode, Encoding, DiskCache, PDF/A
				$pdf = new TCPDF('P', 'mm', 'A4', TRUE, 'ISO-8859-1', FALSE, TRUE);

				// document identity
				$pdf->setPDFVersion('1.4');
				$pdf->SetCreator('Sistem Penerimaan Mahasiswa Baru Universitas Airlangga');
				$pdf->SetAuthor('PPMB Universitas Airlangga');
				$pdf->SetTitle('Biodata Peserta');
				$pdf->SetSubject('Biodata Peserta ujian');
				$pdf->SetKeywords('PPMB, Universitas Airlangga, Mandiri, Penerimaan, Pendaftaran, Biodata Peserta');

				// Disable fontSubSetting
				$pdf->setFontSubsetting(FALSE);
				
				// re-Setting Page
				$pdf->setPrintHeader(FALSE);
				$pdf->setPrintFooter(FALSE);

				// set local indonesia
				setlocale(LC_TIME, 'id_ID');

				// catet nomer awal blok
				$nomer_awal = $cmb->NO_UJIAN;
			}

			// tambah page
			$pdf->AddPage('P', 'A4');
			
			$border = 0;
			
			// Persiapan kode jurusan
			if ($cmb->KODE_JURUSAN == '01') { $kode_jurusan = 'IPA'; }
			if ($cmb->KODE_JURUSAN == '02') { $kode_jurusan = 'IPS'; }
			if ($cmb->KODE_JURUSAN == '03') { $kode_jurusan = 'IPC'; }
			
			// Persiapan data nama program
			if ($cmb->PENERIMAAN->ID_JENJANG == 1) { $jalur_penerimaan = 'SARJANA'; $nama_jenjang = 'S1'; }
			if ($cmb->PENERIMAAN->ID_JENJANG == 2) { $jalur_penerimaan = 'MAGISTER'; $nama_jenjang = 'S2'; $kode_jurusan = 'Reguler'; }
			if ($cmb->PENERIMAAN->ID_JENJANG == 3) { $jalur_penerimaan = 'DOKTOR'; $nama_jenjang = 'S3'; $kode_jurusan = 'Reguler'; }
			if ($cmb->PENERIMAAN->ID_JENJANG == 5) { $jalur_penerimaan = 'DIPLOMA'; $nama_jenjang = 'D3'; }
			if ($cmb->PENERIMAAN->ID_JENJANG == 9) { $jalur_penerimaan = 'PROFESI'; $nama_jenjang = 'Profesi'; $kode_jurusan = 'Reguler'; }
			if ($cmb->PENERIMAAN->ID_JENJANG == 10) { $jalur_penerimaan = 'SPESIALIS'; $nama_jenjang = 'Spesialis'; $kode_jurusan = 'Reguler'; }
			
			// Tahun akademik penerimaan
			$tahun_penerimaan = $cmb->PENERIMAAN->TAHUN . '/' . ($cmb->PENERIMAAN->TAHUN + 1);
			
			// Pengecekan 2 halaman
			if ($cmb->PENERIMAAN->ID_JENJANG == 3 || $cmb->PENERIMAAN->ID_JENJANG == 10)
			{
				$is_dua_halaman = TRUE;
			}
			else
			{
				$is_dua_halaman = FALSE;
			}
			
			// --------------
			// Judul Halaman
			// --------------
			$pdf->SetFont('helvetica', 'B', 16);
			$pdf->Cell(0, 7, "FORMULIR REGISTRASI MAHASISWA BARU", $border, true, 'C');
			$pdf->Cell(0, 7, "PROGRAM {$jalur_penerimaan}", $border, true, 'C');
			$pdf->Cell(0, 7, "TAHUN AKADEMIK {$tahun_penerimaan}", $border, true, 'C');
			
			
			// Horizontal line
			$pdf->Line(10, 35, 200, 35, array('width' => 0.2));
			$pdf->Line(10, 36, 200, 36, array('width' => 0.4));
			
			// shift down
			$pdf->Ln();
			
			// reset line width
			$pdf->SetLineWidth(0.2);
			
			// Jenis Program Studi
			$pdf->SetFont('Helvetica', 'B', 11); $pdf->SetTextColor(255,255,255);
			$pdf->Cell(0, 6, "PROGRAM STUDI PILIHAN", true, true, 'L',true);
			$pdf->SetFont('Helvetica', '', 11); $pdf->SetTextColor(0,0,0);
			$pdf->Cell(50, 6, "No Ujian", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->NO_UJIAN}", true, true, 'L');
			$pdf->Cell(50, 6, "Kode Voucher", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->KODE_VOUCHER}", true, true, 'L');
			
			if (in_array($cmb->PENERIMAAN->ID_JENJANG, array(2, 3, 9, 10)))
			{
				// Title Case Prodi
				$cmb->PILIHAN_1->NM_PROGRAM_STUDI = humanize($cmb->PILIHAN_1->NM_PROGRAM_STUDI);
				
				$pdf->Cell(50, 6, "Program Studi Pilihan", true, false, 'L');
				$pdf->Cell(0, 6, "{$nama_jenjang} - {$cmb->PILIHAN_1->NM_PROGRAM_STUDI}", true, true, 'L');
				
				if ($cmb->ID_PRODI_MINAT != '')
				{
					$pdf->Cell(50, 6, "Minat Studi", true, false, 'L');
					$pdf->Cell(0, 6, "{$cmb->PRODI_MINAT->NM_PRODI_MINAT}", true, true, 'L');
				}
			}
			else
			{
				$pdf->Cell(50, 6, "Kode Jurusan", true, false, 'L');
				$pdf->Cell(0, 6, "{$kode_jurusan}", true, true, 'L');
			}
			
			// shift down
			$pdf->Ln();
			
			// pre-process biodata
			$cmb->NM_C_MHS				= humanize($cmb->NM_C_MHS);
			$cmb->JENIS_KELAMIN			= $cmb->JENIS_KELAMIN == 1 ? 'Laki-Laki' : 'Perempuan';
			$cmb->KOTA_LAHIR->NM_KOTA	= humanize($cmb->KOTA_LAHIR->NM_KOTA);
			$cmb->TGL_LAHIR				= strftime('%d %B %Y', strtotime($cmb->TGL_LAHIR));
			$cmb->KOTA->TIPE_DATI2		= humanize($cmb->KOTA->TIPE_DATI2);
			$cmb->KOTA->NM_KOTA			= humanize($cmb->KOTA->NM_KOTA);
			
			
			// -------------
			// Biodata
			// -------------
			$pdf->SetFont('Helvetica', 'B', 11); $pdf->SetTextColor(255,255,255);
			$pdf->Cell(0, 6, "BIODATA", true, true, 'L', true);
			$pdf->SetFont('Helvetica', '', 11); $pdf->SetTextColor(0,0,0);
			$pdf->Cell(50, 6, "Nama Lengkap", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->NM_C_MHS}", true, true, 'L');
			$pdf->Cell(50, 6, "Jenis Kelamin", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->JENIS_KELAMIN}", true, true, 'L');

			if ($cmb->GELAR != '')
			{
				$pdf->Cell(50, 6, "Gelar", true, false, 'L');
				$pdf->Cell(0, 6, "{$cmb->GELAR}", true, true, 'L');
			}
			
			$pdf->Cell(50, 6, "Tempat dan Tanggal Lahir", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->KOTA_LAHIR->NM_KOTA}, {$cmb->TGL_LAHIR}", true, true, 'L');
			$pdf->Cell(50, 6, "Alamat", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->ALAMAT}", true, true, 'L');
			$pdf->Cell(50, 6, "Kota", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->KOTA->TIPE_DATI2} {$cmb->KOTA->NM_KOTA}", true, true, 'L');
			$pdf->Cell(50, 6, "No Telp", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->TELP}", true, true, 'L');
			$pdf->Cell(50, 6, "Email", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->EMAIL}", true, true, 'L');
			$pdf->Cell(50, 6, "Kewarganegaraan", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->KEWARGANEGARAAN->NM_KEWARGANEGARAAN}", true, true, 'L');
			$pdf->Cell(50, 6, "Agama", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->AGAMA->NM_AGAMA}", true, true, 'L');
			$pdf->Cell(50, 6, "Sumber Biaya", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->SUMBER_BIAYA->NM_SUMBER_BIAYA}", true, true, 'L');
			$pdf->Cell(50, 6, "Disabilitas", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->DISABILITAS->NM_DISABILITAS}", true, true, 'L');
			
			
			// pre process pendidikan sebelumnya
			$cmb->CMP->STATUS_PTN_S1 = $cmb->CMP->STATUS_PTN_S1 == 1 ? 'Negeri' : 'Swasta';
			$cmb->CMP->TGL_MASUK_S1	= strftime('%d %B %Y', strtotime($cmb->CMP->TGL_MASUK_S1));
			$cmb->CMP->TGL_LULUS_S1	= strftime('%d %B %Y', strtotime($cmb->CMP->TGL_LULUS_S1));
			$cmb->CMP->STATUS_PTN_S2 = $cmb->CMP->STATUS_PTN_S2 == 1 ? 'Negeri' : 'Swasta';
			$cmb->CMP->TGL_MASUK_S2	= strftime('%d %B %Y', strtotime($cmb->CMP->TGL_MASUK_S2));
			$cmb->CMP->TGL_LULUS_S2	= strftime('%d %B %Y', strtotime($cmb->CMP->TGL_LULUS_S2));
			
			// ---------------------------
			// Pendidikan Sebelumnya (S1)
			// ---------------------------
			$pdf->SetFont('Helvetica', 'B', 11); $pdf->SetTextColor(255,255,255);
			$pdf->Cell(0, 6, "PENDIDIKAN SEBELUMNYA (S1)", true, true, 'L', true);
			$pdf->SetFont('Helvetica', '', 11); $pdf->SetTextColor(0,0,0);
			$pdf->Cell(50, 6, "Perguruan Tinggi", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->CMP->PTN_S1}", true, true, 'L');
			$pdf->Cell(50, 6, "Status PT", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->CMP->STATUS_PTN_S1}", true, true, 'L');
			$pdf->Cell(50, 6, "Program Studi", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->CMP->PRODI_S1}", true, true, 'L');
			$pdf->Cell(50, 6, "Tanggal Masuk", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->CMP->TGL_MASUK_S1}", true, true, 'L');
			$pdf->Cell(50, 6, "Tanggal Lulus", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->CMP->TGL_LULUS_S1}", true, true, 'L');
			$pdf->Cell(50, 6, "Lama Studi", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->CMP->LAMA_STUDI_S1} tahun", true, true, 'L');
			$pdf->Cell(50, 6, "IP", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->CMP->IP_S1}", true, true, 'L');
			$pdf->Cell(50, 6, "NIM Lama (Khusus Unair)", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->CMP->NIM_LAMA_S1}", true, true, 'L');
			
			// ---------------------------
			// Pendidikan Sebelumnya (S2 / Pr) untuk Doktor / Spesialis
			// ---------------------------
			if (in_array($cmb->PENERIMAAN->ID_JENJANG, array(3, 10)))
			{
				if ($cmb->PENERIMAAN->ID_JENJANG == 3) { $jalur_setara = 'S2'; }
				if ($cmb->PENERIMAAN->ID_JENJANG == 10) { $jalur_setara = 'Profesi'; }
				
				$pdf->SetFont('Helvetica', 'B', 11); $pdf->SetTextColor(255,255,255);
				$pdf->Cell(0, 6, "PENDIDIKAN SEBELUMNYA ({$jalur_setara})", true, true, 'L', true);
				$pdf->SetFont('Helvetica', '', 11); $pdf->SetTextColor(0,0,0);
				$pdf->Cell(50, 6, "Perguruan Tinggi", true, false, 'L');
				$pdf->Cell(0, 6, "{$cmb->CMP->PTN_S2}", true, true, 'L');
				$pdf->Cell(50, 6, "Status PT", true, false, 'L');
				$pdf->Cell(0, 6, "{$cmb->CMP->STATUS_PTN_S2}", true, true, 'L');
				$pdf->Cell(50, 6, "Program Studi", true, false, 'L');
				$pdf->Cell(0, 6, "{$cmb->CMP->PRODI_S2}", true, true, 'L');
				$pdf->Cell(50, 6, "Tanggal Masuk", true, false, 'L');
				$pdf->Cell(0, 6, "{$cmb->CMP->TGL_MASUK_S2}", true, true, 'L');
				$pdf->Cell(50, 6, "Tanggal Lulus", true, false, 'L');
				$pdf->Cell(0, 6, "{$cmb->CMP->TGL_LULUS_S2}", true, true, 'L');
				$pdf->Cell(50, 6, "Lama Studi", true, false, 'L');
				$pdf->Cell(0, 6, "{$cmb->CMP->LAMA_STUDI_S2} tahun", true, true, 'L');
				$pdf->Cell(50, 6, "IP", true, false, 'L');
				$pdf->Cell(0, 6, "{$cmb->CMP->IP_S2}", true, true, 'L');
				$pdf->Cell(50, 6, "NIM Lama (Khusus Unair)", true, false, 'L');
				$pdf->Cell(0, 6, "{$cmb->CMP->NIM_LAMA_S2}", true, true, 'L');
			}
			
			$pdf->Cell(50, 6, "Jumlah Karya Ilmiah", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->CMP->JUMLAH_KARYA_ILMIAH}", true, true, 'L');
			
			if ($is_dua_halaman)
			{
				// footer
				$pdf->SetY($pdf->getPageHeight() - 30);
				$pdf->SetFont('Helvetica', '', 9);
				$pdf->Cell(0, 6, "{$cmb->NO_UJIAN} - Halaman 1 dari 2", null, null, 'C');

				// break page
				$pdf->endPage();
				$pdf->AddPage('P', 'A4');

				// shift down
				$pdf->Ln();
			}
			
			$pdf->SetFont('Helvetica', 'B', 11); $pdf->SetTextColor(255,255,255);
			$pdf->Cell(0, 6, "DATA PEKERJAAN", true, true, 'L', true);
			$pdf->SetFont('Helvetica', '', 11); $pdf->SetTextColor(0,0,0);
			$pdf->Cell(50, 6, "Pekerjaan", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->CMP->PEKERJAAN}", true, true, 'L');
			$pdf->Cell(50, 6, "Asal Instansi", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->CMP->ASAL_INSTANSI}", true, true, 'L');
			$pdf->Cell(50, 6, "Alamat Instansi", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->CMP->ALAMAT_INSTANSI}", true, true, 'L');
			$pdf->Cell(50, 6, "Telp Instansi", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->CMP->TELP_INSTANSI}", true, true, 'L');
			$pdf->Cell(50, 6, "NIP / NIS / NRP", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->CMP->NRP}", true, true, 'L');
			$pdf->Cell(50, 6, "Karpeg", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->CMP->KARPEG}", true, true, 'L');
			$pdf->Cell(50, 6, "Pangkat", true, false, 'L');
			$pdf->Cell(0, 6, "{$cmb->CMP->PANGKAT}", true, true, 'L');
			
			if ($is_dua_halaman)
			{
				// footer
				$pdf->SetY($pdf->getPageHeight() - 30);
				$pdf->SetFont('Helvetica', '', 9);
				$pdf->Cell(0, 6, "{$cmb->NO_UJIAN} - Halaman 2 dari 2", null, null, 'C');
			}
			else
			{
				// footer
				$pdf->SetY($pdf->getPageHeight() - 30);
				$pdf->SetFont('Helvetica', '', 9);
				$pdf->Cell(0, 6, "{$cmb->NO_UJIAN} - Halaman 1 dari 1", null, null, 'C');
			}
			
			// break;
			$pdf->endPage();
			
			// tambah hitungan
			$i_proses++;
			$nomer_akhir = $cmb->NO_UJIAN;

			// Jika sudah 10 peserta, file disimpan dalam format [nomer_awal]-[nomer_akhir].pdf
			if ($i_proses == 10)
			{
				// Save file
				$pdf->Output("{$lokasi_files}/download/biodata/{$nomer_awal}-{$nomer_akhir}.pdf", "F");

				// reset counter
				$i_proses = 0;
				$nomer_awal = '';
			}
		}

		// Jika block terakhir tak sampai 10 peserta, maka disimpan lagi
		if ($i_proses > 0)
		{
			// Save file
			$pdf->Output("{$lokasi_files}/download/biodata/{$nomer_awal}-{$nomer_akhir}.pdf", "F");
		}

		// Move working direktori
		$cwd = getcwd();
		chdir("{$lokasi_files}/download/biodata/");

		// Zip all generated files, dengan metode store only dan move
		exec("zip -0 -m {$lokasi_files}/download/{$id_penerimaan}_biodata.zip *");

		// kembalikan working directory
		chdir($cwd);
	}
}