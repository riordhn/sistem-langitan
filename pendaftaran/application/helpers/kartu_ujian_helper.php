<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('generate_kartu_ujian')) {
		/**
		 * Menggenerate kartu ujian
		 * @param TCPDF $tcpdf
		 * @param Object $cmb	 Description
		 */
		function generate_kartu_ujian(TCPDF &$pdf, &$cmb, $pt)
		{
			// document identity
			$pdf->setPDFVersion('1.4');
			$pdf->SetCreator('Sistem Penerimaan Mahasiswa Baru ' . $pt['NAMA_PERGURUAN_TINGGI']);
			$pdf->SetAuthor('PMB ' . $pt['NAMA_PERGURUAN_TINGGI']);
			$pdf->SetTitle('Tanda Peserta Ujian');
			$pdf->SetSubject($cmb->NO_UJIAN . ' - ' . $cmb->NM_C_MHS);
			$pdf->SetKeywords('PMB, ' . $pt['NAMA_PERGURUAN_TINGGI'] . ', Mandiri, Penerimaan, Pendaftaran, Kartu Ujian');

			// re-Setting Page
			$pdf->setPrintHeader(FALSE);
			$pdf->setPrintFooter(FALSE);
			$pdf->SetMargins(10, 10, 10);  // Margin 1 cm

			// ======================================================
			// MULAI GAMBAR KARTU
			// ======================================================
			$pdf->AddPage();

			// Gambar kotak
			$pdf->SetLineWidth(0.5);
			$pdf->Rect(10, 10, $pdf->getPageWidth() - 20, 125);
			$pdf->Line(10, 30, $pdf->getPageWidth() - 10, 30);
			$pdf->Line(10, 37, $pdf->getPageWidth() - 10, 37);


			$debug_pdf = FALSE;
			$border_debug = array(
				'LTRB' => array('width' => 0.1, 'dash' => '5,5', 'color' => array(225, 225, 255)),
			);
			$barcode_style = array(
				'position' => '',
				'align' => 'R',
				'stretch' => false,
				'fitwidth' => true,
				'cellfitalign' => '',
				'border' => FALSE,
				'hpadding' => 'auto',
				'vpadding' => 'auto',
				'fgcolor' => array(0, 0, 0),
				'bgcolor' => false, //array(255,255,255),
				'text' => false,
				'font' => 'helvetica',
				'fontsize' => 8,
				'stretchtext' => 4
			);

			// ------------------
			// Judul Kartu & Logo
			// ------------------
			$pdf->Image('@' . file_get_contents('assets/logo/logo_pt_'.$pt['ID_PERGURUAN_TINGGI'].'.jpg'), 25, 12, 16, 16, 'jpg');
			$pdf->SetXY(10, 12);
			$pdf->SetFont('times', '', 14);
			$pdf->Cell(0, 0, 'PUSAT PENERIMAAN MAHASISWA BARU', $debug_pdf ? $border_debug : 0, 2, 'C');
			$pdf->SetFont('times', 'B', 18);
			$pdf->Cell(0, 0, $pt['NAMA_PERGURUAN_TINGGI'], $debug_pdf ? $border_debug : 0, 1, 'C');
			$pdf->SetY(30, true);
			$pdf->SetFont('times', '', 14);
			$pdf->Cell(0, 0, ' TANDA PESERTA UJIAN', $debug_pdf ? $border_debug : 0, 1, 'L');
			$pdf->SetXY(120, 29);
			$pdf->write1DBarcode($cmb->NO_UJIAN, 'C39', '', '', '', 9, 0.4, $barcode_style, 'N');

			// -----------------------
			// Foto Peserta & No Ujian
			// -----------------------
			$pdf->Image('@' . file_get_contents("../pendaftaran_files/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$cmb->cmf->FILE_FOTO}"), 15, 40, 45, 60, 'jpg', '', '', false, 300, '', false, false, 1);
			$pdf->SetXY(15, 100);
			$pdf->SetFont('helvetica', 'B', 16);
			$pdf->Cell(45, 0, $cmb->NO_UJIAN, 1, 2, 'C');


			// --------------------
			// Kode Jurusan & Jalur S1, D3, D4
			// --------------------
			if (in_array($cmb->PENERIMAAN->ID_JENJANG, array(1, 4, 5))) {
					if ($cmb->KODE_JURUSAN == '01')
						$kode_jurusan = 'IPA';
					else if ($cmb->KODE_JURUSAN == '02')
						$kode_jurusan = 'IPS';
					else if ($cmb->KODE_JURUSAN == '03')
						$kode_jurusan = 'IPC';

					if ($cmb->PENERIMAAN->ID_JALUR == 3)
						$jalur = 'MANDIRI';
					else if ($cmb->PENERIMAAN->ID_JALUR == 4 && $cmb->PENERIMAAN->ID_JENJANG == 1)
						$jalur = 'ALIH JENIS';
					else if ($cmb->PENERIMAAN->ID_JALUR == 4 && $cmb->PENERIMAAN->ID_JENJANG == 4)
						$jalur = 'D3 ke D4';
					else if ($cmb->PENERIMAAN->ID_JALUR == 5)
						$jalur = 'DIPLOMA 3';
					else if ($cmb->PENERIMAAN->ID_JALUR == 42)
						$jalur = 'DIPLOMA 4';
					else if ($cmb->PENERIMAAN->ID_JALUR == 27)
						$jalur = 'FK INT';
					else if ($cmb->PENERIMAAN->ID_JALUR == 20)
						$jalur = 'DEPAG';
					else
						$jalur = 'MANDIRI';

					// Ruang  dan alokasi waktu di munculkan
					$ruang_printed = true;
					$alokasi_printed = true;
				} elseif (in_array($cmb->PENERIMAAN->ID_JENJANG, array(2, 3, 9, 10))) {
					// ruang dan alokasi waktu tidak dimunculkan
					$ruang_printed = false;
					$alokasi_printed = false;
				}

			// --------------------------------
			// Isian jalur (pojok kanan atas)
			//---------------------------------
			// Khusus S1, D3, D4
			if (in_array($cmb->PENERIMAAN->ID_JENJANG, array(1, 4, 5))) {
					// Gambar kotaknya
					$pdf->Rect($pdf->getPageWidth() - 37, 40, 25, 17);
					$pdf->Line($pdf->getPageWidth() - 37, 51, $pdf->getPageWidth() - 12, 51);

					// Jalur dan jurusan
					$pdf->SetXY($pdf->getPageWidth() - 37, 40);
					$pdf->SetFont('helvetica', 'B', 26);
					$pdf->Cell(25, 0, $kode_jurusan, $debug_pdf ? $border_debug : 0, 2, 'C');
					$pdf->SetFont('times', '', 10);
					$pdf->Cell(25, 0, $jalur, $debug_pdf ? $border_debug : 0, 1, 'C');
				}

			// -------------------
			// Format Tanggal test
			// -------------------

			// Setting Local Indonesia
			setlocale(LC_TIME, 'id_ID');

			// Pengecekan untuk dua hari test
			/* Data kosong ditutup sementara Nambi
		if ($cmb->JADWAL_PPMB->DUA_HARI_TEST == 0)
		{
			$cmb->JADWAL_PPMB->TGL_TEST = strftime('%A, %d %B %Y', strtotime($cmb->JADWAL_PPMB->TGL_TEST));
		}
		else if ($cmb->JADWAL_PPMB->DUA_HARI_TEST == 1)
		{
			$cmb->JADWAL_PPMB->TGL_TEST = 
				strftime('%d %B', strtotime($cmb->JADWAL_PPMB->TGL_TEST)) . ' dan ' .
				strftime('%d %B %Y', strtotime($cmb->JADWAL_PPMB->TGL_TEST2));
		}
		*/

			// ------------------------------
			// Materi Test dan Alokasi Waktu
			// ------------------------------

			if ($cmb->KODE_JURUSAN == '01') {
					$materi_test_1 = "  08.00 - 08.50 : Tes Potensi Akademik (TPA)";
					$materi_test_2 = "  08.50 - 10.05 : Tes Prestasi Akademik (IPA)";
					$materi_test_3 = "";
				} else if ($cmb->KODE_JURUSAN == '02') {
					$materi_test_1 = "  08.00 - 08.50 : Tes Potensi Akademik (TPA)";
					$materi_test_2 = "  08.50 - 10.05 : Tes Prestasi Akademik (IPS)";
					$materi_test_3 = "";
				} else if ($cmb->KODE_JURUSAN == '03') {
					$materi_test_1 = "  08.00 - 08.50 : Tes Potensi Akademik";
					$materi_test_2 = "  08.50 - 10.05 : Tes Prestasi Akademik (IPA)";
					$materi_test_3 = "  10.05 - 11.05 : Tes Prestasi Akademik (IPS)";
				}

			// ------------------------------------
			// Ganti Title case untuk program studi
			// ------------------------------------
			$cmb->PILIHAN_1->NM_PROGRAM_STUDI = ucwords(strtolower($cmb->PILIHAN_1->NM_PROGRAM_STUDI));
			if ($cmb->ID_PILIHAN_2 != '') {
				$cmb->PILIHAN_2->NM_PROGRAM_STUDI = ucwords(strtolower($cmb->PILIHAN_2->NM_PROGRAM_STUDI));
			}
			if ($cmb->ID_PILIHAN_3 != '') {
				$cmb->PILIHAN_3->NM_PROGRAM_STUDI = ucwords(strtolower($cmb->PILIHAN_3->NM_PROGRAM_STUDI));
			}
			if ($cmb->ID_PILIHAN_4 != '') {
				$cmb->PILIHAN_4->NM_PROGRAM_STUDI = ucwords(strtolower($cmb->PILIHAN_4->NM_PROGRAM_STUDI));
			}


			// -----------------
			// Biodata
			// -----------------
			$pdf->SetXY(62, 40);
			$pdf->SetFont('helvetica', '', 11);
			$pdf->Cell(0, 0, 'Voucher', $debug_pdf ? $border_debug : 0, 2, 'L');
			$pdf->Cell(0, 0, 'No Peserta', $debug_pdf ? $border_debug : 0, 2, 'L');
			$pdf->SetY($pdf->GetY() + 2, false); // add 1 mm
			$pdf->Cell(0, 0, 'Nama', $debug_pdf ? $border_debug : 0, 2, 'L');
			$pdf->Cell(0, 0, 'Pilihan 1', $debug_pdf ? $border_debug : 0, 2, 'L');
			if ($cmb->ID_PILIHAN_2 != '') {
				$pdf->Cell(0, 0, 'Pilihan 2', $debug_pdf ? $border_debug : 0, 2, 'L');
			}
			if ($cmb->ID_PILIHAN_3 != '') {
				$pdf->Cell(0, 0, 'Pilihan 3', $debug_pdf ? $border_debug : 0, 2, 'L');
			}
			if ($cmb->ID_PILIHAN_4 != '') {
				$pdf->Cell(0, 0, 'Pilihan 4', $debug_pdf ? $border_debug : 0, 2, 'L');
			}
			$pdf->SetY($pdf->GetY() + 2, false); // add 1 mm
			if ($ruang_printed) {
				$pdf->Cell(0, 0, 'Ruang', $debug_pdf ? $border_debug : 0, 2, 'L');
			}
			$pdf->Cell(0, 0, 'Lokasi', $debug_pdf ? $border_debug : 0, 2, 'L');
			$pdf->Cell(0, 0, '', $debug_pdf ? $border_debug : 0, 2, 'L'); //BREAK LINE
			$pdf->Cell(0, 0, 'Tanggal Ujian', $debug_pdf ? $border_debug : 0, 2, 'L');
			$pdf->Cell(0, 0, 'Materi Ujian', $debug_pdf ? $border_debug : 0, 2, 'L');
			if ($alokasi_printed) {
					$pdf->Cell(0, 0, 'Alokasi Waktu', $debug_pdf ? $border_debug : 0, 2, 'L');
					$pdf->SetFont('helvetica', '', 10); // ukuran huruf diturunkan 1 point
					$pdf->Cell(0, 0, $materi_test_1, $debug_pdf ? $border_debug : 0, 2, 'L');
					$pdf->Cell(0, 0, $materi_test_2, $debug_pdf ? $border_debug : 0, 2, 'L');
					$pdf->Cell(0, 0, $materi_test_3, $debug_pdf ? $border_debug : 0, 2, 'L');
					$pdf->SetFont('helvetica', '', 11); // ukuran huruf ditambah 1 point
				} else {
					$pdf->Cell(0, 0, 'Waktu Ujian', $debug_pdf ? $border_debug : 0, 2, 'L');
				}
			$pdf->SetXY(90, 40);
			$pdf->SetFont('helvetica', 'b'); // bold untuk kode voucher dan no ujian
			$pdf->Cell(0, 0, ': ' . $cmb->KODE_VOUCHER, $debug_pdf ? $border_debug : 0, 2, 'L');
			$pdf->Cell(0, 0, ': ' . $cmb->NO_UJIAN, $debug_pdf ? $border_debug : 0, 2, 'L');
			$pdf->SetFont('helvetica', ''); // un-bold
			$pdf->SetY($pdf->GetY() + 2, false); // add 1 mm
			$pdf->Cell(0, 0, ': ' . $cmb->NM_C_MHS, $debug_pdf ? $border_debug : 0, 2, 'L');
			$pdf->Cell(0, 0, ': ' . $cmb->PILIHAN_1->NM_PROGRAM_STUDI, $debug_pdf ? $border_debug : 0, 2, 'L');
			if ($cmb->ID_PILIHAN_2 != '') {
				$pdf->Cell(0, 0, ': ' . $cmb->PILIHAN_2->NM_PROGRAM_STUDI, $debug_pdf ? $border_debug : 0, 2, 'L');
			}
			if ($cmb->ID_PILIHAN_3 != '') {
				$pdf->Cell(0, 0, ': ' . $cmb->PILIHAN_3->NM_PROGRAM_STUDI, $debug_pdf ? $border_debug : 0, 2, 'L');
			}
			if ($cmb->ID_PILIHAN_4 != '') {
				$pdf->Cell(0, 0, ': ' . $cmb->PILIHAN_4->NM_PROGRAM_STUDI, $debug_pdf ? $border_debug : 0, 2, 'L');
			}
			$pdf->SetY($pdf->GetY() + 2, false); // add 1 mm
			/* Ditutup sementara data kosong nambi
		if ($ruang_printed) { $pdf->Cell(0, 0, ': ' . $cmb->JADWAL_PPMB->NM_RUANG, $debug_pdf ? $border_debug : 0, 2, 'L'); }
		$pdf->Cell(0, 0, ': ' . $cmb->JADWAL_PPMB->LOKASI, $debug_pdf ? $border_debug : 0, 2, 'L');
		$pdf->Cell(0, 0, '  ' . $cmb->JADWAL_PPMB->ALAMAT, $debug_pdf ? $border_debug : 0, 2, 'L');
		$pdf->Cell(0, 0, ': ' . $cmb->JADWAL_PPMB->TGL_TEST, $debug_pdf ? $border_debug : 0, 2, 'L');
		$pdf->Cell(0, 0, ': ' . $cmb->JADWAL_PPMB->MATERI, $debug_pdf ? $border_debug : 0, 2, 'L');
		*/
			if (!$alokasi_printed) {
					$pdf->Cell(0, 0, ': 08:00 WIB - Selesai', $debug_pdf ? $border_debug : 0, 2, 'L');
				}

			// -----------------
			// TTD Ketua
			// -----------------
			$pdf->SetXY(155, 105);
			$pdf->SetFont('helvetica', 'B', 10);
			$pdf->Cell(30, 0, strftime('%d %B %Y'), $debug_pdf ? $border_debug : 0, 2, 'C');
			$pdf->Cell(30, 0, 'Ketua PPMB', $debug_pdf ? $border_debug : 0, 2, 'C');
			$pdf->Image('@' . file_get_contents("assets/images/ttd-ketua.jpg"), $pdf->GetX(), $pdf->GetY(), 30, 15, 'jpg', '', '', false, 300, '', false, false, 0);
			$pdf->SetXY(155, 125);
			$pdf->Cell(30, 0, 'Bambang Sektiari Lukiswanto', $debug_pdf ? $border_debug : 0, 0, 'C');
			//$pdf->Image('assets/images/stempel.png', 130, 95, 35, 35, '', '', '', true, 150, '', false); Comment Nambi

			// -----------------------------------------------------------
			// Signature dari ID_C_MHS, standar PDF417.
			// Untuk mencegah pemalsuan kartu, di beri tanda ID_C_MHS
			// -----------------------------------------------------------
			$pdf->write2DBarcode($cmb->ID_C_MHS, 'PDF417,5', 15, 115, 45, 0, $barcode_style, 'N');

			// -----------------
			// Footer Pesan
			// -----------------
			$pdf->SetY(130);
			$pdf->SetFont('helvetica', 'I', 9);
			$pdf->setColor('text', 255, 0, 0);
			$pdf->Cell(0, 0, 'Peserta harus datang di lokasi 30 menit sebelum tes dimulai', $debug_pdf ? $border_debug : 0, 2, 'C');

			// Output
			// $pdf->SetDisplayMode('fullwidth');
			$pdf->Output($cmb->NO_UJIAN . '.pdf', 'I');
		}
	}
