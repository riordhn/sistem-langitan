<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * =================================================================
 * Proses generate file pdf ini di bagi-bagi menjadi tiap 10 peserta
 * hal ini dilakukan untuk membatasi beban server, dan untuk 
 * mempermudah proses mencetak, karena dilakukan proses pencetakan
 * secara bersama.
 * =================================================================
 */

if ( ! function_exists('generate_biodata_ptnu'))
{
	
	/*function humanize($str) {
 
		$str = trim(strtolower($str));
		$str = preg_replace('/[^a-z0-9\s+]/', '', $str);
		$str = preg_replace('/\s+/', ' ', $str);
		$str = explode(' ', $str);
	 
		$str = array_map('ucwords', $str);
	 
		return implode(' ', $str);
	}*/

	function generate_biodata_ptnu(TCPDF &$pdf, &$cmb_set, &$syarat_berkas_set, $cmf, $id_penerimaan, $ptnu)
	{

		$i_proses	= 0;
		$nomer_awal	= '';
		$nomer_akhir = '';

		// Mendapatkan row PTNU
		/*$this->query = $this->db->get_where('perguruan_tinggi', array('id_perguruan_tinggi' => $id_pt));
		$ptnu = $this->query->row();

		echo "ini yg di function";*/

		$nama_pt = $ptnu->NAMA_PERGURUAN_TINGGI;

		foreach ($cmb_set as $cmb)
		{
			// Jika awal blok nomer atau baru dimulai
			if ($i_proses == 0) 
			{
				// Buat PDF Baru
				// Params: Orientation, Unit, Page, Unicode, Encoding, DiskCache, PDF/A
				/*$pdf = new TCPDF('P', 'mm', 'A4', TRUE, 'ISO-8859-1', FALSE, TRUE);*/

			//echo "ini yg di function";

				// document identity
				$pdf->setPDFVersion('1.4');
				$pdf->SetCreator('Sistem Penerimaan Mahasiswa Baru Universitas Airlangga');
				$pdf->SetAuthor('PPMB Universitas Airlangga');
				$pdf->SetTitle('Biodata Peserta');
				$pdf->SetSubject('Biodata Peserta ujian');
				$pdf->SetKeywords('PPMB, Universitas Airlangga, Mandiri, Penerimaan, Pendaftaran, Biodata Peserta');

				// Disable fontSubSetting
				$pdf->setFontSubsetting(FALSE);
				
				// re-Setting Page
				$pdf->setPrintHeader(FALSE);
				$pdf->setPrintFooter(FALSE);

				// set local indonesia
				setlocale(LC_TIME, 'id_ID');

				// catet nomer awal blok
				$nomer_awal = $cmb->NO_UJIAN;
			}

			// tambah page
			$pdf->AddPage('P', 'A4');
			
			// DEBUGGER desain PDF
			$border = 0;
			
			// Setting PDF 
			$auto_width = 0;
			$left_width	= 50;
			$height		= 6;
			
			// -------------------------------------------
			// Preprocessing data untuk tampilan
			// -------------------------------------------
			// Persiapan kode jurusan
			/*if ($cmb->KODE_JURUSAN == '01') { $kode_jurusan = 'IPA'; }
			if ($cmb->KODE_JURUSAN == '02') { $kode_jurusan = 'IPS'; }
			if ($cmb->KODE_JURUSAN == '03') { $kode_jurusan = 'IPC'; }*/
			
			// Persiapan data nama program
			if ($cmb->PENERIMAAN->ID_JENJANG == 1) { $jalur_penerimaan = 'SARJANA'; $nama_jenjang = 'S1'; }
			if ($cmb->PENERIMAAN->ID_JENJANG == 2) { $jalur_penerimaan = 'MAGISTER'; $nama_jenjang = 'S2'; $kode_jurusan = 'Reguler'; }
			if ($cmb->PENERIMAAN->ID_JENJANG == 3) { $jalur_penerimaan = 'DOKTOR'; $nama_jenjang = 'S3'; $kode_jurusan = 'Reguler'; }
			if ($cmb->PENERIMAAN->ID_JENJANG == 4) { $jalur_penerimaan = 'DIPLOMA 4'; $nama_jenjang = 'D4'; }
			if ($cmb->PENERIMAAN->ID_JENJANG == 5) { $jalur_penerimaan = 'DIPLOMA 3'; $nama_jenjang = 'D3'; }
			if ($cmb->PENERIMAAN->ID_JENJANG == 9) { $jalur_penerimaan = 'PROFESI'; $nama_jenjang = 'Profesi'; $kode_jurusan = 'Reguler'; }
			if ($cmb->PENERIMAAN->ID_JENJANG == 10) { $jalur_penerimaan = 'SPESIALIS'; $nama_jenjang = 'Spesialis'; $kode_jurusan = 'Reguler'; }
			
			// Title Case Prodi
			$cmb->NM_PILIHAN_1 = humanize($cmb->NM_PILIHAN_1);
			if (isset($cmb->NM_PILIHAN_2)) { $cmb->NM_PILIHAN_2 = humanize($cmb->NM_PILIHAN_2); }
			if (isset($cmb->NM_PILIHAN_3)) { $cmb->NM_PILIHAN_3 = humanize($cmb->NM_PILIHAN_3); }
			if (isset($cmb->NM_PILIHAN_4)) { $cmb->NM_PILIHAN_4 = humanize($cmb->NM_PILIHAN_4); }
			
			// Tahun akademik penerimaan
			$tahun_penerimaan = $cmb->PENERIMAAN->TAHUN . '/' . ($cmb->PENERIMAAN->TAHUN + 1);
			
			// ----------------------
			// Setting jumlah halaman
			// ----------------------
			// S1, D3, D4 Non Alih Jenis dan S3 dan Spesialis
			if ((in_array($cmb->PENERIMAAN->ID_JENJANG, array(1, 4, 5)) && $cmb->PENERIMAAN->ID_JALUR != 4) || (in_array($cmb->PENERIMAAN->ID_JENJANG, array(3, 10))))
			{
				$is_dua_halaman = TRUE;
			}
			else // S2 / Profesi
			{
				$is_dua_halaman = FALSE;
			}
			
			// --------------
			// Judul Halaman
			// --------------
			$pdf->SetFont('helvetica', 'B', 16);
			$pdf->Cell(0, 7, "FORMULIR REGISTRASI MAHASISWA BARU", $border, true, 'C');
			$pdf->Cell(0, 7, "PROGRAM {$jalur_penerimaan}", $border, true, 'C');
			$pdf->Cell(0, 7, "TAHUN AKADEMIK {$tahun_penerimaan}", $border, true, 'C');
			
			// Horizontal line
			$pdf->Line(10, 35, 200, 35, array('width' => 0.2));
			$pdf->Line(10, 36, 200, 36, array('width' => 0.4));
			
			// shift down
			$pdf->Ln();
			
			// reset line width
			$pdf->SetLineWidth(0.2);
			
			// Jenis Program Studi / Form
			$pdf->SetFont('Helvetica', 'B', 11); $pdf->SetTextColor(255,255,255);
			$pdf->Cell($auto_width, $height, "PROGRAM STUDI PILIHAN", true, true, 'L',true);
			$pdf->SetFont('Helvetica', '', 11); $pdf->SetTextColor(0,0,0);
			$pdf->Cell($left_width, $height, "No Ujian", true, false, 'L');
			$pdf->Cell($auto_width, $height, "{$cmb->NO_UJIAN}", true, true, 'L');
			$pdf->Cell($left_width, $height, "Nomor Pendaftaran", true, false, 'L');
			$pdf->Cell($auto_width, $height, "{$cmb->KODE_VOUCHER}", true, true, 'L');
			
			// Penerimaan Pascasarjana : MAGISTER, DOKTOR, PROFESI, SPESIALIS
			/*if (in_array($cmb->PENERIMAAN->ID_JENJANG, array(2, 3, 9, 10)))
			{
				// Program Studi Pilihan
				$pdf->Cell($left_width, $height, "Program Studi Pilihan", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$nama_jenjang} - {$cmb->NM_PILIHAN_1}", true, true, 'L');
				
				// Program Studi Minat
				if ($cmb->NM_PRODI_MINAT != '')
				{
					$pdf->Cell($left_width, $height, "Minat Studi", true, false, 'L');
					$pdf->Cell($auto_width, $height, "{$cmb->NM_PRODI_MINAT}", true, true, 'L');
				}
			}*/
			
			// Peneriman Sarjana, D3 dan D4 -> Ada pilihan kode jurusan
			if (in_array($cmb->PENERIMAAN->ID_JENJANG, array(1, 4, 5)))
			{
				/*$pdf->Cell($left_width, $height, "Kode Jurusan", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$kode_jurusan}", true, true, 'L');*/
				
				// Pilihan 1 -> Pasti Tampil
				$pdf->Cell($left_width, $height, "Program Studi Pilihan 1", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->NM_PILIHAN_1}", true, true, 'L');
				
				// Pilihan 2
				if (isset($cmb->NM_PILIHAN_2))
				{
					$pdf->Cell($left_width, $height, "Program Studi Pilihan 2", true, false, 'L');
					$pdf->Cell($auto_width, $height, "{$cmb->NM_PILIHAN_2}", true, true, 'L');
				}
				
				// Pilihan 3
				if (isset($cmb->NM_PILIHAN_3))
				{
					$pdf->Cell($left_width, $height, "Program Studi Pilihan 3", true, false, 'L');
					$pdf->Cell($auto_width, $height, "{$cmb->NM_PILIHAN_3}", true, true, 'L');
				}
				
				// Pilihan 4
				if (isset($cmb->NM_PILIHAN_4))
				{
					$pdf->Cell($left_width, $height, "Program Studi Pilihan 4", true, false, 'L');
					$pdf->Cell($auto_width, $height, "{$cmb->NM_PILIHAN_4}", true, true, 'L');
				}
			}
			
			// shift down
			$pdf->Ln();
			
			// pre-process biodata
			$cmb->NM_C_MHS				= humanize($cmb->NM_C_MHS);
			$cmb->JENIS_KELAMIN			= $cmb->JENIS_KELAMIN == 1 ? 'Laki-Laki' : 'Perempuan';
			$cmb->NM_KOTA_LAHIR			= humanize($cmb->NM_KOTA_LAHIR);
			$cmb->TGL_LAHIR				= strftime('%d %B %Y', strtotime($cmb->TGL_LAHIR));
			$cmb->TIPE_DATI2_ALAMAT		= humanize($cmb->TIPE_DATI2_ALAMAT);
			$cmb->NM_KOTA_ALAMAT		= humanize($cmb->NM_KOTA_ALAMAT);

			$cmb->TGL_LAHIR_AYAH			= strftime('%d %B %Y', strtotime($cmb->TGL_LAHIR_AYAH));
			$cmb->TIPE_DATI2_ALAMAT_AYAH	= humanize($cmb->TIPE_DATI2_ALAMAT_AYAH);
			$cmb->NM_KOTA_ALAMAT_AYAH		= humanize($cmb->NM_KOTA_ALAMAT_AYAH);
			$cmb->TGL_LAHIR_IBU				= strftime('%d %B %Y', strtotime($cmb->TGL_LAHIR_IBU));
			$cmb->TIPE_DATI2_ALAMAT_IBU		= humanize($cmb->TIPE_DATI2_ALAMAT_IBU);
			$cmb->NM_KOTA_ALAMAT_IBU		= humanize($cmb->NM_KOTA_ALAMAT_IBU);
			$cmb->TGL_LAHIR_WALI			= strftime('%d %B %Y', strtotime($cmb->TGL_LAHIR_WALI));
			$cmb->TIPE_DATI2_ALAMAT_WALI	= humanize($cmb->TIPE_DATI2_ALAMAT_WALI);
			$cmb->NM_KOTA_ALAMAT_WALI		= humanize($cmb->NM_KOTA_ALAMAT_WALI);
			
			
			// -------------
			// Biodata
			// -------------
			$pdf->SetFont('Helvetica', 'B', 11); $pdf->SetTextColor(255,255,255);
			$pdf->Cell($auto_width, $height, "BIODATA", true, true, 'L', true);
			$pdf->SetFont('Helvetica', '', 11); $pdf->SetTextColor(0,0,0);
			$pdf->Cell($left_width, $height, "Nama Lengkap", true, false, 'L');
			$pdf->Cell($auto_width, $height, "{$cmb->NM_C_MHS}", true, true, 'L');
			$pdf->Cell($left_width, $height, "Jenis Kelamin", true, false, 'L');
			$pdf->Cell($auto_width, $height, "{$cmb->JENIS_KELAMIN}", true, true, 'L');

			// Gelar jika ada
			if ($cmb->GELAR != '')
			{
				$pdf->Cell($left_width, $height, "Gelar", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->GELAR}", true, true, 'L');
			}
			
			$pdf->Cell($left_width, $height, "Tempat dan Tanggal Lahir", true, false, 'L');
			$pdf->Cell($auto_width, $height, "{$cmb->NM_KOTA_LAHIR}, {$cmb->TGL_LAHIR}", true, true, 'L');
			/*$pdf->Cell($left_width, $height, "Alamat", true, false, 'L');
			$pdf->Cell($auto_width, $height, "{$cmb->ALAMAT_JALAN}", true, true, 'L');
			$pdf->Cell($left_width, $height, "Kota", true, false, 'L');
			$pdf->Cell($auto_width, $height, "{$cmb->TIPE_DATI2_ALAMAT} {$cmb->NM_KOTA_ALAMAT}", true, true, 'L');
			$pdf->Cell($left_width, $height, "No Telp", true, false, 'L');
			$pdf->Cell($auto_width, $height, "{$cmb->TELP}", true, true, 'L');
			$pdf->Cell($left_width, $height, "Email", true, false, 'L');
			$pdf->Cell($auto_width, $height, "{$cmb->EMAIL}", true, true, 'L');*/
			$pdf->Cell($left_width, $height, "Agama", true, false, 'L');
			$pdf->Cell($auto_width, $height, "{$cmb->NM_AGAMA}", true, true, 'L');
			$pdf->Cell($left_width, $height, "Sumber Biaya", true, false, 'L');
			$pdf->Cell($auto_width, $height, "{$cmb->NM_SUMBER_BIAYA}", true, true, 'L');
			$pdf->Cell($left_width, $height, "Disabilitas", true, false, 'L');
			$pdf->Cell($auto_width, $height, "{$cmb->NM_DISABILITAS}", true, true, 'L');
			$pdf->Cell($left_width, $height, "NIK", true, false, 'L');
			$pdf->Cell($auto_width, $height, "{$cmb->NIK_C_MHS}", true, true, 'L');
			if (!empty($cmb->KEWARGANEGARAAN))
			{
				$pdf->Cell($left_width, $height, "Kewarganegaraan", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->KEWARGANEGARAAN->NM_KEWARGANEGARAAN}", true, true, 'L');
			}

			// shift down
			/*$pdf->Ln();*/

			// -------------
			// Alamat
			// -------------
			$pdf->SetFont('Helvetica', 'B', 11); $pdf->SetTextColor(255,255,255);
			$pdf->Cell($auto_width, $height, "ALAMAT", true, true, 'L', true);
			$pdf->SetFont('Helvetica', '', 11); $pdf->SetTextColor(0,0,0);
			$pdf->Cell($left_width, $height, "Jalan", true, false, 'L');
			$pdf->Cell($auto_width, $height, "{$cmb->ALAMAT_JALAN}", true, true, 'L');
			$pdf->Cell($left_width, $height, "Dusun", true, false, 'L');
			$pdf->Cell($auto_width, $height, "{$cmb->ALAMAT_DUSUN}", true, true, 'L');			
			$pdf->Cell($left_width, $height, "Kelurahan", true, false, 'L');
			$pdf->Cell($auto_width, $height, "{$cmb->ALAMAT_KELURAHAN}", true, true, 'L');
			$pdf->Cell($left_width, $height, "Kecamatan", true, false, 'L');
			$pdf->Cell($auto_width, $height, "{$cmb->ALAMAT_KECAMATAN}", true, true, 'L');
			$pdf->Cell($left_width, $height, "Kode Pos", true, false, 'L');
			$pdf->Cell($auto_width, $height, "{$cmb->ALAMAT_KODEPOS}", true, true, 'L');
			$pdf->Cell($left_width, $height, "Kota", true, false, 'L');
			$pdf->Cell($auto_width, $height, "{$cmb->TIPE_DATI2_ALAMAT} {$cmb->NM_KOTA_ALAMAT}", true, true, 'L');
			$pdf->Cell($left_width, $height, "Jenis Tinggal", true, false, 'L');
			$pdf->Cell($auto_width, $height, "{$cmb->NM_JENIS_TINGGAL}", true, true, 'L');
			$pdf->Cell($left_width, $height, "No Telp", true, false, 'L');
			$pdf->Cell($auto_width, $height, "{$cmb->TELP}", true, true, 'L');
			$pdf->Cell($left_width, $height, "No HP", true, false, 'L');
			$pdf->Cell($auto_width, $height, "{$cmb->NOMOR_HP}", true, true, 'L');
			$pdf->Cell($left_width, $height, "Email", true, false, 'L');
			$pdf->Cell($auto_width, $height, "{$cmb->EMAIL}", true, true, 'L');
			$pdf->Cell($left_width, $height, "No KPS", true, false, 'L');
			$pdf->Cell($auto_width, $height, "{$cmb->NOMOR_KPS}", true, true, 'L');

				// Shift Down
				$pdf->Ln();
				
				$pdf->SetFont('Helvetica', 'B', 11); $pdf->SetTextColor(255,255,255);
				$pdf->Cell($auto_width, $height, "PENDIDIKAN SEBELUMNYA", true, true, 'L', true);
				$pdf->SetFont('Helvetica', '', 11); $pdf->SetTextColor(0,0,0);
				$pdf->Cell($left_width, $height, "NISN", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->NISN}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Sekolah", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->NM_SEKOLAH}, {$cmb->NM_KOTA_SEKOLAH}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Jurusan", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->NM_JURUSAN_SEKOLAH}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Tahun Lulus", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->TAHUN_LULUS}", true, true, 'L');
				$pdf->Cell($left_width, $height, "No Ijazah", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->NO_IJAZAH}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Tanggal Ijazah", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->TGL_IJAZAH}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Jumlah Pelajaran Ijazah", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->JUMLAH_PELAJARAN_IJAZAH}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Nilai Rata Ijazah", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->NILAI_IJAZAH}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Jumlah Pelajaran UAN", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->JUMLAH_PELAJARAN_UAN}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Nilai Rata UAN", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->NILAI_UAN}", true, true, 'L');
			
			
			// -----------------------------------------------------------
			// Pendidikan Sebelumnya (SMA) untuk S1, D3, D4 Non Alih Jenis
			// -----------------------------------------------------------
			/*if (in_array($cmb->PENERIMAAN->ID_JENJANG, array(1, 4, 5)) && $cmb->PENERIMAAN->ID_JALUR != 4)
			{
				// pre-processing 
				$cmb->TGL_IJAZAH	= strftime('%d %B %Y', strtotime($cmb->TGL_IJAZAH));
				
				// Shift Down
				$pdf->Ln();
				
				$pdf->SetFont('Helvetica', 'B', 11); $pdf->SetTextColor(255,255,255);
				$pdf->Cell($auto_width, $height, "PENDIDIKAN SEBELUMNYA", true, true, 'L', true);
				$pdf->SetFont('Helvetica', '', 11); $pdf->SetTextColor(0,0,0);
				$pdf->Cell($left_width, $height, "NISN", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->NISN}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Sekolah", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->NM_SEKOLAH}, {$cmb->NM_KOTA_SEKOLAH}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Jurusan", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->NM_JURUSAN_SEKOLAH}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Tahun Lulus", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->TAHUN_LULUS}", true, true, 'L');
				$pdf->Cell($left_width, $height, "No Ijazah", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->NO_IJAZAH}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Tanggal Ijazah", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->TGL_IJAZAH}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Jumlah Pelajaran Ijazah", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->JUMLAH_PELAJARAN_IJAZAH}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Nilai Rata Ijazah", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->NILAI_IJAZAH}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Jumlah Pelajaran UAN", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->JUMLAH_PELAJARAN_UAN}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Nilai Rata UAN", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->NILAI_UAN}", true, true, 'L');
			}
			
			
			
			// -----------------------------------------------------------
			// Pendidikan Sebelumnya (S1) untuk S2, S3, Profesi, Spesialis
			// -----------------------------------------------------------
			if ((in_array($cmb->PENERIMAAN->ID_JENJANG, array(1, 4)) && $cmb->PENERIMAAN->ID_JALUR == 4) || 
				in_array($cmb->PENERIMAAN->ID_JENJANG, array(2, 3, 9, 10)))
			{
				// pre process pendidikan sebelumnya
				if (in_array($cmb->PENERIMAAN->ID_JENJANG, array(1, 4))) { $jalur_setara = 'D3'; }
				if (in_array($cmb->PENERIMAAN->ID_JENJANG, array(2, 3, 9, 10))) { $jalur_setara = 'S1'; }
				
				$cmb->STATUS_PTN_S1 = $cmb->STATUS_PTN_S1 == 1 ? 'Negeri' : 'Swasta';
				$cmb->TGL_MASUK_S1	= strftime('%d %B %Y', strtotime($cmb->TGL_MASUK_S1));
				$cmb->TGL_LULUS_S1	= strftime('%d %B %Y', strtotime($cmb->TGL_LULUS_S1));
				$cmb->STATUS_PTN_S2 = $cmb->STATUS_PTN_S2 == 1 ? 'Negeri' : 'Swasta';
				$cmb->TGL_MASUK_S2	= strftime('%d %B %Y', strtotime($cmb->TGL_MASUK_S2));
				$cmb->TGL_LULUS_S2	= strftime('%d %B %Y', strtotime($cmb->TGL_LULUS_S2));
				
				$pdf->SetFont('Helvetica', 'B', 11); $pdf->SetTextColor(255,255,255);
				$pdf->Cell($auto_width, $height, "PENDIDIKAN SEBELUMNYA ({$jalur_setara})", true, true, 'L', true);
				$pdf->SetFont('Helvetica', '', 11); $pdf->SetTextColor(0,0,0);
				$pdf->Cell($left_width, $height, "Perguruan Tinggi", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->PTN_S1}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Status PT", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->STATUS_PTN_S1}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Program Studi", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->PRODI_S1}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Tanggal Masuk", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->TGL_MASUK_S1}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Tanggal Lulus", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->TGL_LULUS_S1}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Lama Studi", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->LAMA_STUDI_S1} tahun", true, true, 'L');
				$pdf->Cell($left_width, $height, "IP", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->IP_S1}", true, true, 'L');
				$pdf->Cell($left_width, $height, "NIM Lama (Khusus Unair)", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->NIM_LAMA_S1}", true, true, 'L');
				
				// Jumlah Karya Ilmiah
				$pdf->Cell($left_width, $height, "Jumlah Karya Ilmiah", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->JUMLAH_KARYA_ILMIAH}", true, true, 'L');
			}
			
			// ------------------------------------------------------
			// Pendidikan Sebelumnya (S2 / Pr) untuk Doktor / Spesialis
			// ------------------------------------------------------
			if (in_array($cmb->PENERIMAAN->ID_JENJANG, array(3, 10)))
			{
				if ($cmb->PENERIMAAN->ID_JENJANG == 3) { $jalur_setara = 'S2'; }
				if ($cmb->PENERIMAAN->ID_JENJANG == 10) { $jalur_setara = 'Profesi'; }
				
				$pdf->SetFont('Helvetica', 'B', 11); $pdf->SetTextColor(255,255,255);
				$pdf->Cell($auto_width, $height, "PENDIDIKAN SEBELUMNYA ({$jalur_setara})", true, true, 'L', true);
				$pdf->SetFont('Helvetica', '', 11); $pdf->SetTextColor(0,0,0);
				$pdf->Cell($left_width, $height, "Perguruan Tinggi", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->PTN_S2}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Status PT", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->STATUS_PTN_S2}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Program Studi", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->PRODI_S2}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Tanggal Masuk", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->TGL_MASUK_S2}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Tanggal Lulus", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->TGL_LULUS_S2}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Lama Studi", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->LAMA_STUDI_S2} tahun", true, true, 'L');
				$pdf->Cell($left_width, $height, "IP", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->IP_S2}", true, true, 'L');
				$pdf->Cell($left_width, $height, "NIM Lama (Khusus Unair)", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->NIM_LAMA_S2}", true, true, 'L');
				
				// Jumlah Karya Ilmiah
				$pdf->Cell($left_width, $height, "Jumlah Karya Ilmiah", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->JUMLAH_KARYA_ILMIAH}", true, true, 'L');
			}*/
			
			// Jika terdapat dua halaman
			if ($is_dua_halaman)
			{
				// footer
				$pdf->SetY($pdf->getPageHeight() - 30);
				$pdf->SetFont('Helvetica', '', 9);
				$pdf->Cell($auto_width, $height, "{$cmb->NO_UJIAN} - Halaman 1 dari 2", null, null, 'C');

				// break page
				$pdf->endPage();
				$pdf->AddPage('P', 'A4');

				// shift down
				$pdf->Ln();
			}
			
			// ---------------------------------------------------
			// Informasi Orang Tua untuk S1, D3, D4 Non Alih Jenis
			// ---------------------------------------------------
			if (in_array($cmb->PENERIMAAN->ID_JENJANG, array(1, 4, 5)) && $cmb->PENERIMAAN->ID_JALUR != 4)
			{
				$pdf->SetFont('Helvetica', 'B', 11); $pdf->SetTextColor(255,255,255);
				$pdf->Cell($auto_width, $height, "BIODATA AYAH", true, true, 'L', true);
				$pdf->SetFont('Helvetica', '', 11); $pdf->SetTextColor(0,0,0);
				$pdf->Cell($left_width, $height, "Nama", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->NAMA_AYAH}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Tanggal Lahir", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->TGL_LAHIR_AYAH}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Alamat Tinggal", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->ALAMAT_AYAH}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Kota Tinggal", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->TIPE_DATI2_ALAMAT_AYAH} {$cmb->NM_KOTA_ALAMAT_AYAH}", true, true, 'L');
				$pdf->Cell($left_width, $height, "No Telp", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->TELP_AYAH}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Pendidikan Terakhir", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->NM_PENDIDIKAN_AYAH}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Pekerjaan", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->NM_PEKERJAAN_AYAH}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Penghasilan", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->PENGHASILAN_AYAH}", true, true, 'L');
				/*$pdf->Cell($left_width, $height, "Instansi / Perusahaan", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->INSTANSI_AYAH}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Jabatan", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->JABATAN_AYAH}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Masa Kerja", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->MASA_KERJA_AYAH}", true, true, 'L');
*/				$pdf->Cell($left_width, $height, "Disabilitas", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->NM_DISABILITAS_AYAH}", true, true, 'L');
				
				// shift down
				$pdf->Ln();
				
				$pdf->SetFont('Helvetica', 'B', 11); $pdf->SetTextColor(255,255,255);
				$pdf->Cell($auto_width, $height, "BIODATA IBU", true, true, 'L', true);
				$pdf->SetFont('Helvetica', '', 11); $pdf->SetTextColor(0,0,0);
				$pdf->Cell($left_width, $height, "Nama", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->NAMA_IBU}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Tanggal Lahir", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->TGL_LAHIR_IBU}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Alamat Tinggal", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->ALAMAT_IBU}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Kota Tinggal", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->TIPE_DATI2_ALAMAT_IBU} {$cmb->NM_KOTA_ALAMAT_IBU}", true, true, 'L');
				$pdf->Cell($left_width, $height, "No Telp", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->TELP_IBU}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Pendidikan Terakhir", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->NM_PENDIDIKAN_IBU}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Pekerjaan", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->NM_PEKERJAAN_IBU}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Penghasilan", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->PENGHASILAN_IBU}", true, true, 'L');
				/*$pdf->Cell($left_width, $height, "Instansi / Perusahaan", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->INSTANSI_IBU}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Jabatan", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->JABATAN_IBU}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Masa Kerja", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->MASA_KERJA_IBU}", true, true, 'L');*/
				$pdf->Cell($left_width, $height, "Disabilitas", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->NM_DISABILITAS_IBU}", true, true, 'L');

				if(!empty($cmb->NAMA_WALI)){
					// shift down
					$pdf->Ln();
					
					$pdf->SetFont('Helvetica', 'B', 11); $pdf->SetTextColor(255,255,255);
					$pdf->Cell($auto_width, $height, "BIODATA WALI", true, true, 'L', true);
					$pdf->SetFont('Helvetica', '', 11); $pdf->SetTextColor(0,0,0);
					$pdf->Cell($left_width, $height, "Nama", true, false, 'L');
					$pdf->Cell($auto_width, $height, "{$cmb->NAMA_WALI}", true, true, 'L');
					$pdf->Cell($left_width, $height, "Tanggal Lahir", true, false, 'L');
					$pdf->Cell($auto_width, $height, "{$cmb->TGL_LAHIR_WALI}", true, true, 'L');
					$pdf->Cell($left_width, $height, "Alamat Tinggal", true, false, 'L');
					$pdf->Cell($auto_width, $height, "{$cmb->ALAMAT_WALI}", true, true, 'L');
					$pdf->Cell($left_width, $height, "Kota Tinggal", true, false, 'L');
					$pdf->Cell($auto_width, $height, "{$cmb->TIPE_DATI2_ALAMAT_WALI} {$cmb->NM_KOTA_ALAMAT_WALI}", true, true, 'L');
					$pdf->Cell($left_width, $height, "No Telp", true, false, 'L');
					$pdf->Cell($auto_width, $height, "{$cmb->TELP_WALI}", true, true, 'L');
					$pdf->Cell($left_width, $height, "Pendidikan Terakhir", true, false, 'L');
					$pdf->Cell($auto_width, $height, "{$cmb->NM_PENDIDIKAN_WALI}", true, true, 'L');
					$pdf->Cell($left_width, $height, "Pekerjaan", true, false, 'L');
					$pdf->Cell($auto_width, $height, "{$cmb->NM_PEKERJAAN_WALI}", true, true, 'L');
					$pdf->Cell($left_width, $height, "Penghasilan", true, false, 'L');
					$pdf->Cell($auto_width, $height, "{$cmb->PENGHASILAN_WALI}", true, true, 'L');
					/*$pdf->Cell($left_width, $height, "Instansi / Perusahaan", true, false, 'L');
					$pdf->Cell($auto_width, $height, "{$cmb->INSTANSI_IBU}", true, true, 'L');
					$pdf->Cell($left_width, $height, "Jabatan", true, false, 'L');
					$pdf->Cell($auto_width, $height, "{$cmb->JABATAN_IBU}", true, true, 'L');
					$pdf->Cell($left_width, $height, "Masa Kerja", true, false, 'L');
					$pdf->Cell($auto_width, $height, "{$cmb->MASA_KERJA_IBU}", true, true, 'L');*/
					$pdf->Cell($left_width, $height, "Disabilitas", true, false, 'L');
					$pdf->Cell($auto_width, $height, "{$cmb->NM_DISABILITAS_WALI}", true, true, 'L');

				}
			}


			/*untuk melihat status upload berkas tiap c_mhs*/
			// shift down
			$pdf->Ln();
	
			// status upload foto
			$pdf->SetFont('Helvetica', 'B', 11); $pdf->SetTextColor(255,255,255);
			$pdf->Cell($auto_width, $height, "Persyaratan Berkas", true, true, 'L', true);
			$pdf->SetFont('Helvetica', '', 11); $pdf->SetTextColor(0,0,0);
			/*$pdf->Cell($left_width, $height, "Nama Berkas", true, false, 'L');
			$pdf->Cell($auto_width, $height, "Pas Foto", true, true, 'L');
			$pdf->Cell($left_width, $height, "Status Upload", true, false, 'L');*/
			if($cmf->IS_FOTO_VERIFIED == '1'){
				$pdf->Cell($left_width, $height, "Pas Foto", true, false, 'L');
				$pdf->Cell($auto_width, $height, "Sudah Terverifikasi", true, true, 'L');
			}
			else{
				$pdf->Cell($left_width, $height, "Pas Foto", true, false, 'L');
				$pdf->Cell($auto_width, $height, "Belum Upload / Belum Terverifikasi", true, true, 'L');
			}
			
			// status upload berkas persyaratan
			foreach ($syarat_berkas_set as $syarat_berkas)
			{
				/*$pdf->Cell($left_width, $height, "Nama Berkas", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$syarat_berkas->NM_SYARAT}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Status Upload", true, false, 'L');*/
				if($syarat_berkas->IS_VERIFIED == '1'){
					$pdf->Cell($left_width, $height, "{$syarat_berkas->NM_SYARAT}", true, false, 'L');
					$pdf->Cell($auto_width, $height, "Sudah Terverifikasi", true, true, 'L');
				}
				else{
					$pdf->Cell($left_width, $height, "{$syarat_berkas->NM_SYARAT}", true, false, 'L');
					$pdf->Cell($auto_width, $height, "Belum Upload / Belum Terverifikasi", true, true, 'L');
				}
			}
			
			// ---------------------------------------------------
			// Informasi Pekerjaan untuk Pascasarjana / S1, D4 Alih Jenis
			// ---------------------------------------------------
			/*if ((in_array($cmb->PENERIMAAN->ID_JENJANG, array(1, 4)) && $cmb->PENERIMAAN->ID_JALUR == 4) ||
				(in_array($cmb->PENERIMAAN->ID_JENJANG, array(2, 3, 9, 10))))
			{
				$pdf->SetFont('Helvetica', 'B', 11); $pdf->SetTextColor(255,255,255);
				$pdf->Cell($auto_width, $height, "DATA PEKERJAAN", true, true, 'L', true);
				$pdf->SetFont('Helvetica', '', 11); $pdf->SetTextColor(0,0,0);
				$pdf->Cell($left_width, $height, "Pekerjaan", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->PEKERJAAN}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Asal Instansi", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->ASAL_INSTANSI}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Alamat Instansi", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->ALAMAT_INSTANSI}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Telp Instansi", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->TELP_INSTANSI}", true, true, 'L');
				$pdf->Cell($left_width, $height, "NIP / NIS / NRP", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->NRP}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Karpeg", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->KARPEG}", true, true, 'L');
				$pdf->Cell($left_width, $height, "Pangkat", true, false, 'L');
				$pdf->Cell($auto_width, $height, "{$cmb->PANGKAT}", true, true, 'L');
			}*/
			
			if ($is_dua_halaman)
			{
				// footer
				$pdf->SetY($pdf->getPageHeight() - 30);
				$pdf->SetFont('Helvetica', '', 9);
				$pdf->Cell($auto_width, $height, "{$cmb->NO_UJIAN} - Halaman 2 dari 2", null, null, 'C');
			}
			else
			{
				// footer
				$pdf->SetY($pdf->getPageHeight() - 30);
				$pdf->SetFont('Helvetica', '', 9);
				$pdf->Cell($auto_width, $height, "{$cmb->NO_UJIAN} - Halaman 1 dari 1", null, null, 'C');
			}
			
			// break;
			$pdf->endPage();
			
			// tambah hitungan
			$i_proses++;
			$nomer_akhir = $cmb->NO_UJIAN;

			// File disimpan dalam format [nomer_awal]-[nomer_akhir].pdf dalam skala tiap 10 peserta
			if ($i_proses == 10)
			{
				// Save file
				/*$pdf->Output("{$lokasi_files}/download/biodata/{$nomer_awal}-{$nomer_akhir}.pdf", "F");*/

				// Save file
				$pdf->Output("biodata {$cmb->NM_C_MHS}.pdf", "I");

				// reset counter
				$i_proses = 0;
				$nomer_awal = '';
				
				// force preview -- DEVELOPMENT
				//$pdf->Output("{$nomer_awal}-{$nomer_akhir}.pdf", "I");
				//exit();
			}
		}

		// Jika block terakhir tak sampai 10 peserta, maka langsung disimpan
		if ($i_proses > 0)
		{
			// Save file
			/*$pdf->Output("{$lokasi_files}/download/biodata/{$nomer_awal}-{$nomer_akhir}.pdf", "F");*/

			// Save file
			$pdf->Output("biodata {$cmb->NM_C_MHS}.pdf", "I");
			
			// force preview -- DEVELOPMENT
			//$pdf->Output("{$nomer_awal}-{$nomer_akhir}.pdf", "I");
			//exit();
		}

		// Move working direktori
		/*$cwd = getcwd();
		
		// change direktori aktif
		chdir("{$lokasi_files}/download/biodata/");

		// Zip all generated files, dengan metode store only dan move
		//exec("zip -0 -m {$lokasi_files}/download/{$id_penerimaan}_biodata.zip *");
		
		// Per 13-05-2015 diganti menjadi tar
		exec("tar --remove-files -czpf {$lokasi_files}/download/{$id_penerimaan}_biodata.tgz *");

		// kembalikan working directory
		chdir($cwd);*/
	}
}