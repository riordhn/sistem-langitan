<?php

/**
 * Description of controller
 *
 * @author Fathoni <fathoni@staf.unair.ac.id>
 * @property CI_Smarty $smarty
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Loader $load
 * @property CI_DB_active_record $db
 * @property CI_DB_oci8_result $query
 * @property CI_Email $email
 * @property CI_Session $session
 * @property CI_Upload $upload
 * @property CI_Output $output
 * @property CI_URI $uri
 */
class MY_Controller extends CI_Controller {
	//put your code here

	public $perguruan_tinggi;
	
	function __construct()
	{
		parent::__construct();
		
		// Library loader
		$this->load->database();
		$this->load->library('form_validation');
		$this->load->library('email');
		$this->load->library('session');
		
		// Custom widget
		$this->load->helper('widget');
		
		// Email Initialization
		$email_config['protocol'] = 'sendmail';
		$email_config['mailpath'] = '/usr/sbin/sendmail';
		$email_config['charset'] = 'iso-8859-1';
		$email_config['wordwrap'] = TRUE;
		
		$this->email->initialize($email_config);

		//print_r($this->session->all_userdata());
		
		// Session initialization
		if ($this->session->userdata('is_loggedin') == '')
		{
			$this->session->set_userdata('is_loggedin', FALSE);
		}

		if($this->session->userdata('perguruan_tinggi') == ''){
			$data_set = $this->db->query("SELECT ID_PERGURUAN_TINGGI, NAMA_PERGURUAN_TINGGI, NAMA_SINGKAT, HTTP_HOST, KOTA_PERGURUAN_TINGGI,
														ALAMAT_PERGURUAN_TINGGI, TELP_PERGURUAN_TINGGI, FAX_PERGURUAN_TINGGI, WEB_EMAIL, 
														WEB_ELEARNING, EMAIL_PMB, WEB_PMB 
														FROM PERGURUAN_TINGGI WHERE WEB_PMB = '{$_SERVER['HTTP_HOST']}'")->result_array();

			$this->session->set_userdata('perguruan_tinggi', $data_set[0]);
		}	

		//print_r($this->langitan_set);
		$this->perguruan_tinggi = $this->session->userdata('perguruan_tinggi');


		//print_r($this->session->userdata('perguruan_tinggi'));



		
		// disable cache page
		header('Access-Control-Allow-Origin: *');  // untuk keperluan ajax
		header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
	
	/**
	 * Mendeteksi credential
	 */
	function check_credentials()
	{
		if ($this->session->userdata('is_loggedin') == FALSE)
		{
			redirect('/');
		}
	}

	/**
	 * Mendeteksi pengakses
	 */
	function check_accesor(){
		return 1;
	}
}
