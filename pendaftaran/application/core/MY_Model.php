<?php
/**
 * Description of MY_Model
 *
 * @author Fathoni
 * @property CI_DB_active_record $db Database
 * @property CI_DB_oci8_result $query Query result
 * @property CI_Input $input
 */
class MY_Model extends CI_Model
{
	public $perguruan_tinggi;

	public function __construct()
	{
		parent::__construct();

		$this->load->library('session');

		if($this->session->userdata('perguruan_tinggi') == ''){
			$data_set = $this->db->query("SELECT ID_PERGURUAN_TINGGI, NAMA_PERGURUAN_TINGGI, NAMA_SINGKAT, HTTP_HOST, KOTA_PERGURUAN_TINGGI,
														ALAMAT_PERGURUAN_TINGGI, TELP_PERGURUAN_TINGGI, FAX_PERGURUAN_TINGGI, WEB_EMAIL, 
														WEB_ELEARNING, EMAIL_PMB, WEB_PMB 
														FROM PERGURUAN_TINGGI WHERE WEB_PMB = '{$_SERVER['HTTP_HOST']}'")->result_array();

			$this->session->set_userdata('perguruan_tinggi', $data_set[0]);
		}	

		//print_r($this->langitan_set);
		$this->perguruan_tinggi = $this->session->userdata('perguruan_tinggi');
	}
}
