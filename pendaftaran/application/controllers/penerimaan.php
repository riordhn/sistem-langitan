<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property Penerimaan_Model $penerimaan_model Description
 * @property Cmb_Model $cmb_model
 * @property Calon_Pendaftar_Model $calon_pendaftar_model Description
 */
class Penerimaan extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();	
		
		$this->check_credentials();
		
		$this->load->model('Penerimaan_Model', 'penerimaan_model');
		$this->load->model('Cmb_Model', 'cmb_model');
		$this->load->model('Calon_Pendaftar_Model', 'calon_pendaftar_model');
	}

	function index()
	{
		$calon_pendaftar = $this->session->userdata('calon_pendaftar');
		$id_c_mhs_aktif	 = $calon_pendaftar->ID_C_MHS_AKTIF;
		
		$this->smarty->assign('id_c_mhs_aktif', $id_c_mhs_aktif);
		$this->smarty->assign('current_time', time());
		
		$this->smarty->assignByRef('jenjang_penerimaan_set', $this->penerimaan_model->list_aktif_dibuka($calon_pendaftar->ID_CALON_PENDAFTAR));
		
		$this->smarty->display('penerimaan/index.tpl');
	}
	
	function pilih($id_penerimaan)
	{
		// Preventif dari akses id_penerimaan kosong
		if (empty($id_penerimaan)) 
		{
			redirect('penerimaan/');
			return;
		}
		
		$calon_pendaftar = $this->session->userdata('calon_pendaftar');
		
		// proses create cmb
		$hasil_create = $this->cmb_model->create_cmb($calon_pendaftar->ID_CALON_PENDAFTAR, $id_penerimaan);
		
		if ($hasil_create == 'KODE_VOUCHER_EMPTY')
		{
			$this->smarty->display('penerimaan/kode_voucher_kosong.tpl');
		}
		else if ($hasil_create == 'COMPLETE')
		{
			// reload ulang calon_pendaftar session
			$this->session->set_userdata('calon_pendaftar', $this->calon_pendaftar_model->get($calon_pendaftar->ID_CALON_PENDAFTAR));
		
			// redirect ke halaman isian
			redirect('form/isi/');
		}
	}
	
	function aktifkan($id_c_mhs)
	{
		// Preventif dari akses id_penerimaan kosong
		if (empty($id_c_mhs)) 
		{
			redirect('penerimaan/');
			return;
		}
		
		$calon_pendaftar = $this->session->userdata('calon_pendaftar');
		
		$this->calon_pendaftar_model->aktifkan($calon_pendaftar->ID_CALON_PENDAFTAR, $id_c_mhs);
		
		// reload ulang calon_pendaftar session
		$this->session->set_userdata('calon_pendaftar', $this->calon_pendaftar_model->get($calon_pendaftar->ID_CALON_PENDAFTAR));
		
		redirect('form/isi/');
	}
}