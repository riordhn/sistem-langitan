<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property Calon_Pendaftar_Model $calon_pendaftar_model Description
 * @property Cmb_Model $cmb_model Description
 * @property CI_Recaptcha $recaptcha Google recaptcha
 */
class Auth extends MY_Controller
{
	var $recaptcha_public_key	= '6LcbhQYTAAAAAHGgI_MhJ-rVN1Ah9DmKXx_gSsM-';
	var $recaptcha_private_key	= '6LcbhQYTAAAAAFVnqggXsiSr2uW4gni0Ffukmv-G';
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('Calon_Pendaftar_Model', 'calon_pendaftar_model');
		$this->load->model('Cmb_Model', 'cmb_model');
		
		// Google recaptcha
		$this->load->library('recaptcha');

		//Mendapatkan identitas PT
		$this->nama_pt = $this->perguruan_tinggi['NAMA_PERGURUAN_TINGGI'];
		$this->id_pt = $this->perguruan_tinggi['ID_PERGURUAN_TINGGI'];
	}

	public function voucher()
	{	
		//$id_pt = $this->perguruan_tinggi['ID_PERGURUAN_TINGGI'];
		
		/*if ($this->session->userdata('is_loggedin') == TRUE)
		{
			redirect('home/');
			exit();
		}*/

	   	if ($this->input->server('REQUEST_METHOD') == 'POST')
		{
				$id_jenjang	= $this->input->post('id_jenjang');
				$id_penerimaan	= $this->input->post('id_penerimaan');

				$voucher_set = $this->db->query("SELECT
													id_voucher,
													kode_voucher,
													pin_voucher
												FROM
													voucher
												WHERE
													is_aktif = '1' AND
													tgl_ambil is null AND
													id_penerimaan = '{$id_penerimaan}'
												order by id_voucher")->result_array();

				$counter = 0;

				foreach ($voucher_set as $row)
				{	
					$counter++;
				    $array_id[] = $row['ID_VOUCHER'];
				    //echo "<br> hmm ".$array_id[1];
				    $array_kode[] = $row['KODE_VOUCHER'];
				    $array_pin[] = $row['PIN_VOUCHER'];
				}

				if($counter == 0){

					$id_pt = $this->perguruan_tinggi['ID_PERGURUAN_TINGGI'];
					$nama_pt = $this->perguruan_tinggi['NAMA_PERGURUAN_TINGGI'];
					$telp_pt = $this->perguruan_tinggi['TELP_PERGURUAN_TINGGI'];
					$email_pmb = $this->perguruan_tinggi['EMAIL_PMB'];
					$web_pt = "http://www.".strtolower($this->perguruan_tinggi['NAMA_SINGKAT']).".ac.id";
					$alamat_pt = $this->perguruan_tinggi['ALAMAT_PERGURUAN_TINGGI'];

					$this->smarty->assign('id_pt', $id_pt);
					$this->smarty->assign('nama_pt', $nama_pt);
					$this->smarty->assign('telp_pt', $telp_pt);
					$this->smarty->assign('email_pmb', $email_pmb);
					$this->smarty->assign('web_pt', $web_pt);
					$this->smarty->assign('alamat_pt', $alamat_pt);

					$this->smarty->display('auth/error_voucher.tpl');
					exit();
				}

				$random_id[] = array_rand($array_id,1);

				$id_voucher = $array_id[$random_id[0]];
				$kode_voucher = $array_kode[$random_id[0]];
				$pin_voucher = $array_pin[$random_id[0]];

				$this->smarty->assign('kode_voucher', $kode_voucher);
				$this->smarty->assign('pin_voucher', $pin_voucher);

				// Mendapatkan row PENERIMAAN
				$this->query = $this->db->get_where('penerimaan', array('id_penerimaan' => $id_penerimaan));
				$penerimaan = $this->query->row();

				// sementara dibuat tgl_bayar otomatis dibuat langsung khusus umaha
				/*if (in_array($this->id_pt, array(1, 5)))
				{*/
					// tanpa verifikasi
				/*if($penerimaan->IS_VERIFIKASI == '0'){
					$this->db->query("UPDATE VOUCHER SET IS_AKTIF = '1', TGL_AMBIL = SYSDATE, TGL_BAYAR = SYSDATE WHERE ID_VOUCHER = '{$id_voucher}'");
				}
				else{*/
					$this->db->query("UPDATE VOUCHER SET IS_AKTIF = '1', TGL_AMBIL = SYSDATE WHERE ID_VOUCHER = '{$id_voucher}'");
				/*}*/
				

				//$this->db->query("UPDATE VOUCHER SET IS_AKTIF = '1', TGL_AMBIL = SYSDATE, TGL_BAYAR = SYSDATE WHERE ID_VOUCHER = '{$id_voucher}'");
				$id_pt = $this->perguruan_tinggi['ID_PERGURUAN_TINGGI'];
				$nama_pt = $this->perguruan_tinggi['NAMA_PERGURUAN_TINGGI'];
				$telp_pt = $this->perguruan_tinggi['TELP_PERGURUAN_TINGGI'];
				$email_pmb = $this->perguruan_tinggi['EMAIL_PMB'];
				$web_pt = "http://www.".strtolower($this->perguruan_tinggi['NAMA_SINGKAT']).".ac.id";
				$alamat_pt = $this->perguruan_tinggi['ALAMAT_PERGURUAN_TINGGI'];

				$this->smarty->assign('id_pt', $id_pt);
				$this->smarty->assign('nama_pt', $nama_pt);
				$this->smarty->assign('telp_pt', $telp_pt);
				$this->smarty->assign('email_pmb', $email_pmb);
				$this->smarty->assign('web_pt', $web_pt);
				$this->smarty->assign('alamat_pt', $alamat_pt);

				$this->smarty->display('auth/hasil_voucher.tpl');
				exit();

		}

		/*$jenjang_set = $this->db->query("SELECT DISTINCT ps.ID_JENJANG, j.NM_JENJANG 
											FROM PROGRAM_STUDI ps 
											JOIN FAKULTAS f ON f.ID_FAKULTAS = ps.ID_FAKULTAS AND f.ID_PERGURUAN_TINGGI = '{$this->id_pt}'
											JOIN JENJANG j ON j.ID_JENJANG =  ps.ID_JENJANG")
						->result_array();
		
		$this->smarty->assign('jenjang_set', $jenjang_set);*/

		$penerimaan_set = $this->db->query("SELECT p.ID_PENERIMAAN, p.NM_PENERIMAAN, p.GELOMBANG, p.ID_JENJANG, j.NM_JENJANG 
											FROM PENERIMAAN p 
											JOIN JENJANG j ON j.ID_JENJANG =  p.ID_JENJANG
											WHERE p.IS_AKTIF = 1 AND SYSDATE > p.TGL_AWAL_REGISTRASI AND SYSDATE < p.TGL_AKHIR_REGISTRASI 
											AND p.ID_PERGURUAN_TINGGI = '{$this->id_pt}'
											ORDER BY p.NM_PENERIMAAN")
						->result_array();
		
		$this->smarty->assign('penerimaan_set', $penerimaan_set);

		/*$prodi_set = $this->db->query("SELECT ps.ID_JENJANG, j.NM_JENJANG, ps.NM_PROGRAM_STUDI, ps.ID_FAKULTAS, f.NM_FAKULTAS 
											FROM PROGRAM_STUDI ps 
											JOIN FAKULTAS f ON f.ID_FAKULTAS = ps.ID_FAKULTAS AND f.ID_PERGURUAN_TINGGI = '{$this->id_pt}'
											JOIN JENJANG j ON j.ID_JENJANG =  ps.ID_JENJANG
										ORDER BY ps.ID_FAKULTAS")
						->result_array();
		
		$this->smarty->assign('prodi_set', $prodi_set);*/

		$prodi_set = $this->db->query("select ps.id_program_studi, ps.nm_program_studi, j.id_jenjang, j.nm_jenjang, pp.id_penerimaan
										from penerimaan_prodi pp
										join program_studi ps on ps.id_program_studi = pp.id_program_studi
										join fakultas f on f.id_fakultas = ps.id_fakultas and f.id_perguruan_tinggi = '{$this->id_pt}'
										join jenjang j on j.id_jenjang = ps.id_jenjang
										order by ps.id_fakultas asc")
						->result_array();
		
		$this->smarty->assign('prodi_set', $prodi_set);

		/*$fakultas_set = $this->db->query("SELECT ID_FAKULTAS, NM_FAKULTAS FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = '{$this->id_pt}'")
						->result_array();
		
		$this->smarty->assign('fakultas_set', $fakultas_set);*/

		$id_pt = $this->perguruan_tinggi['ID_PERGURUAN_TINGGI'];
		$nama_pt = $this->perguruan_tinggi['NAMA_PERGURUAN_TINGGI'];
		$telp_pt = $this->perguruan_tinggi['TELP_PERGURUAN_TINGGI'];
		$email_pmb = $this->perguruan_tinggi['EMAIL_PMB'];
		$web_pt = "http://www.".strtolower($this->perguruan_tinggi['NAMA_SINGKAT']).".ac.id";
		$alamat_pt = $this->perguruan_tinggi['ALAMAT_PERGURUAN_TINGGI'];

		$this->smarty->assign('id_pt', $id_pt);
		$this->smarty->assign('nama_pt', $nama_pt);
		$this->smarty->assign('telp_pt', $telp_pt);
		$this->smarty->assign('email_pmb', $email_pmb);
		$this->smarty->assign('web_pt', $web_pt);
		$this->smarty->assign('alamat_pt', $alamat_pt);

		$this->smarty->display('auth/voucher.tpl');
	}
	
	
	public function registrasi()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST')
		{
			$this->form_validation->set_error_delimiters('<p class="help-block text-danger">','</p>');
			$this->form_validation->set_message('required', '%s tidak boleh kosong');
			$this->form_validation->set_message('numeric', 'Ditulis angka saja');
			$this->form_validation->set_message('valid_email', 'Format harus email, contoh: nama@yahoo.com');
			
			$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[100]');
			$this->form_validation->set_rules('telp', 'Telp / HP', 'trim|required|numeric|max_length[20]');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[100]');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]');
			
			if ($this->form_validation->run() == TRUE)
			{
				$nama		= str_replace("'", "''", $this->input->post('nama'));
				$telp		= $this->input->post('telp');
				$email		= strtolower($this->input->post('email'));
				$password	= $this->input->post('password');
				$password_hash = sha1($password);
				
				$recaptcha_result = $this->recaptcha->check_answer(
					$this->recaptcha_private_key, 
					$this->input->server('REMOTE_ADDR'), 
					$this->input->post('recaptcha_challenge_field'), 
					$this->input->post('recaptcha_response_field'));
				
				if ( ! $recaptcha_result->is_valid)
				{
					$this->smarty->assign('is_error', 1);
					$this->smarty->assign('title', 'Recaptcha tidak benar !');
					$this->smarty->assign('message', 'Silahkan masukkan text recaptcha dengan benar');
				}
				else if ($this->calon_pendaftar_model->email_exist($email))
				{
					$this->smarty->assign('email', $email);
					$this->smarty->display('auth/registrasi_email_exist.tpl'); exit();
				}
				else
				{
					$this->calon_pendaftar_model->add_calon($nama, $telp, $email, $password_hash, md5($email));
					
					// building email content
					$this->smarty->assign('nama', $nama);
					$this->smarty->assign('email', $email);
					$this->smarty->assign('email_hash', md5($email));
					$this->smarty->assign('password', $password);
					$email_content = $this->smarty->fetch('email/registrasi.tpl');
					
					// untuk sms gateway
					$this->smarty->assign('telp', $telp);
					$this->smarty->assign('pesan_sms', 'Akun pendaftaran Anda sudah terdaftar. Untuk bisa login silahkan buka inbox email Anda untuk verifikasi email - PPMB Universitas Airlangga');
					
					// script kirim email
					$this->email->from('pmb@umaha.ac.id', 'PPMB Universitas Maarif Hasyim Latif');
					$this->email->to($email); 
					$this->email->set_mailtype('html');
					$this->email->subject('Registrasi Berhasil');
					$this->email->message($email_content);
					$this->email->send();

					$this->smarty->display('auth/registrasi_complete.tpl'); exit();
				}
			}
		}
		
		$this->smarty->assign('recaptcha_html', $this->recaptcha->get_html($this->recaptcha_public_key));
		$this->smarty->display('auth/registrasi.tpl');
	}
	
	function google(){
		redirect('google.com');
	}

	function login()
	{		
		show_error('salah',502); exit();
		if ($this->input->server('REQUEST_METHOD') == 'POST')
		{
			$email		= strtolower($this->input->post('email'));
			$password	= $this->input->post('password');
			$password_hash = sha1($password);

			$login_result = $this->calon_pendaftar_model->cek_login($email, $password_hash);
			log_message('debug',print_r($login_result,TRUE));
			if(is_object($login_result)){
				#redirect('penerimaan/'); exit();
				
					// status login
				$this->session->set_userdata('is_loggedin', TRUE);

				// id_calon_pendaftar
				$this->session->set_userdata('id_calon_pendaftar', $login_result->ID_CALON_PENDAFTAR);
				#die("is object");
				// row calon_pendaftar
				$this->session->set_userdata('calon_pendaftar', $login_result);
				
				// id_c_mhs
				$this->session->set_userdata('id_c_mhs', $this->cmb_model->get_id_c_mhs($login_result->ID_CALON_PENDAFTAR)); 
				

			}else{

				if ($login_result == 'USER_NOT_EXIST')
				{
					$this->smarty->assign('error_message', 'Email belum terdaftar. Silahkan registrasi terlebih dahulu.');
				}
				if ($login_result == 'EMAIL_NOT_CONFIRMED')
				{
					$this->smarty->assign('error_message', 'Email anda belum terverifikasi. Silahkan buka email anda untuk melakukan verifikasi email.');
				}
				if ($login_result == 'WRONG_PASSWORD')
				{
					$this->smarty->assign('error_message', 'Password tidak sesuai, silahkan ulangi login.');
				}

			}

			

			$this->smarty->display('front/index.tpl');
		}
	}
	
	function login_manual()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST')
		{			
			$kode_voucher	= $this->input->post('kode_voucher');
			$pin_voucher	= $this->input->post('pin_voucher');
			
			$id_pt = $this->perguruan_tinggi['ID_PERGURUAN_TINGGI'];

			// kebutuhan khusus utk keperluan petugas UMAHA
			if($pin_voucher == 'um@ha'){
				$this->query = $this->db->query(
					"select v.id_voucher, p.id_penerimaan, p.is_bayar_voucher, v.pin_voucher
					from aucc.voucher v
					join aucc.penerimaan p on p.id_penerimaan = v.id_penerimaan and p.id_perguruan_tinggi = ?
					where 
						v.kode_voucher = ? and 
						p.is_aktif = 1 and 
						v.tgl_ambil is not null",
					array($id_pt, $kode_voucher));
				
				// Mendapatkan row voucher awal
				$voucher_set = $this->query->first_row();

				$pin_voucher = $voucher_set->PIN_VOUCHER;
			}
			
			$login_result = $this->calon_pendaftar_model->cek_login_voucher($kode_voucher, $pin_voucher, $id_pt);
			
			if ($login_result == 'VOUCHER_NOT_FOUND')
			{
				$nama_pt = $this->perguruan_tinggi['NAMA_PERGURUAN_TINGGI'];
				$telp_pt = $this->perguruan_tinggi['TELP_PERGURUAN_TINGGI'];
				$email_pmb = $this->perguruan_tinggi['EMAIL_PMB'];
				$web_pt = "http://www.".strtolower($this->perguruan_tinggi['NAMA_SINGKAT']).".ac.id";
				$alamat_pt = $this->perguruan_tinggi['ALAMAT_PERGURUAN_TINGGI'];

				// Mendapatkan row INFORMASI
				$this->query = $this->db->get_where('informasi_pmb', array('id_perguruan_tinggi' => $id_pt, 'is_aktif' => 1));
				$informasi = $this->query->row();

				if(!empty($informasi->ISI_INFORMASI)){
					$isi_informasi = $informasi->ISI_INFORMASI;
				}
				else{
					$isi_informasi = "";
				}

				$this->smarty->assign('isi_informasi', $isi_informasi);

				$this->smarty->assign('id_pt', $id_pt);
				$this->smarty->assign('nama_pt', $nama_pt);
				$this->smarty->assign('telp_pt', $telp_pt);
				$this->smarty->assign('email_pmb', $email_pmb);
				$this->smarty->assign('web_pt', $web_pt);
				$this->smarty->assign('alamat_pt', $alamat_pt);

				$string_error = "Nomor Pendaftaran / Pin Pendaftaran tidak ditemukan.<br/>
														Atau anda belum melakukan pembayaran kepada panitia PMB ".$nama_pt.".";

				/*$this->smarty->assign('error_message', 'Nomor Pendaftaran tidak ditemukan / Pin Pendaftaran salah.<br/>Cek kembali Nomor dan Pin Pendaftaran.');*/
				$this->smarty->assign('error_message', $string_error);
				$this->smarty->display('front/index.tpl');
				exit();
			}
			else
			{

				/*echo "hmmm ".$login_result->ID_C_MHS_AKTIF; exit();*/
				// status login
				$this->session->set_userdata('is_loggedin', TRUE);

				// id_calon_pendaftar
				$this->session->set_userdata('id_calon_pendaftar', 0);

				// row calon_pendaftar
				$this->session->set_userdata('calon_pendaftar', $login_result);

				/*echo "hmmm ".$login_result->ID_C_MHS_AKTIF;*/

				/*redirect('home/'); exit();*/
				redirect('form/'); exit();
			}
			
			/*$this->smarty->display('front/index-manual.tpl');*/
			$id_pt = $this->perguruan_tinggi['ID_PERGURUAN_TINGGI'];
			$nama_pt = $this->perguruan_tinggi['NAMA_PERGURUAN_TINGGI'];
			$telp_pt = $this->perguruan_tinggi['TELP_PERGURUAN_TINGGI'];
			$email_pmb = $this->perguruan_tinggi['EMAIL_PMB'];
			$web_pt = "http://www.".strtolower($this->perguruan_tinggi['NAMA_SINGKAT']).".ac.id";
			$alamat_pt = $this->perguruan_tinggi['ALAMAT_PERGURUAN_TINGGI'];

			// Mendapatkan row INFORMASI
			$this->query = $this->db->get_where('informasi_pmb', array('id_perguruan_tinggi' => $id_pt, 'is_aktif' => 1));
			$informasi = $this->query->row();

			if(!empty($informasi->ISI_INFORMASI)){
				$isi_informasi = $informasi->ISI_INFORMASI;
			}
			else{
				$isi_informasi = "";
			}

			$this->smarty->assign('isi_informasi', $isi_informasi);

			$this->smarty->assign('id_pt', $id_pt);
			$this->smarty->assign('nama_pt', $nama_pt);
			$this->smarty->assign('telp_pt', $telp_pt);
			$this->smarty->assign('email_pmb', $email_pmb);
			$this->smarty->assign('web_pt', $web_pt);
			$this->smarty->assign('alamat_pt', $alamat_pt);

			$this->smarty->display('front/index.tpl');
		}

		// Loader
		$this->load->database();
		$this->load->library('form_validation');
		$this->load->helper('widget');

		$id_pt = $this->perguruan_tinggi['ID_PERGURUAN_TINGGI'];
		$nama_pt = $this->perguruan_tinggi['NAMA_PERGURUAN_TINGGI'];
		$telp_pt = $this->perguruan_tinggi['TELP_PERGURUAN_TINGGI'];
		$email_pmb = $this->perguruan_tinggi['EMAIL_PMB'];
		$web_pt = "http://www.".strtolower($this->perguruan_tinggi['NAMA_SINGKAT']).".ac.id";
		$alamat_pt = $this->perguruan_tinggi['ALAMAT_PERGURUAN_TINGGI'];

		$this->smarty->assign('id_pt', $id_pt);
		$this->smarty->assign('nama_pt', $nama_pt);
		$this->smarty->assign('telp_pt', $telp_pt);
		$this->smarty->assign('email_pmb', $email_pmb);
		$this->smarty->assign('web_pt', $web_pt);
		$this->smarty->assign('alamat_pt', $alamat_pt);

		$this->smarty->display('front/index.tpl');
	}
	
	function verifikasi_email($email_hash)
	{
		// eksekusi ke db
		$this->calon_pendaftar_model->verify_email($email_hash);	
		
		// pesan tampilan
		$this->smarty->display('auth/verifikasi_email_complete.tpl');
	}
	
	function change_password()
	{
		$this->check_credentials();
		
		if ($this->input->server('REQUEST_METHOD') == 'POST')
		{
			$calon_pendaftar = $this->session->userdata('calon_pendaftar');
			
			$this->form_validation->set_error_delimiters('<p class="help-block text-danger">','</p>');
			$this->form_validation->set_rules('current_password', 'Password Sekarang', 'trim|required');
			$this->form_validation->set_rules('new_password', 'Password Baru', 'trim|required|min_length[4]');
			
			if ($this->form_validation->run())
			{
				$current_password	= sha1($this->input->post('current_password'));
				$new_password		= sha1($this->input->post('new_password'));
				$new_password2	= sha1("123456");
				
				if ($calon_pendaftar->PASS_PENDAFTAR != $current_password and $new_password != $new_password2 )
				{
					$this->smarty->assign('error_message', 'Password sekarang tidak sesuai');
				}
				else if ($current_password == $new_password)
				{
					$this->smarty->assign('error_message', 'Password baru tidak boleh sama dengan password sebelumnya');
				}
				else
				{
					// change db value
					$this->calon_pendaftar_model->set_password($calon_pendaftar->EMAIL_PENDAFTAR, $new_password);
					
					// change session value
					$calon_pendaftar->PASS_PENDAFTAR = $new_password;
					
					// update to session
					$this->session->set_userdata('calon_pendaftar', $calon_pendaftar);
					
					$this->smarty->display('auth/change_password_complete.tpl');
					exit();
				}
			}
		}
		
		$this->smarty->display('auth/change_password.tpl');
	}
	
	function logout()
	{
		$id_calon_pendaftar = $this->session->userdata('id_calon_pendaftar');
			
		// Logout
		$this->session->set_userdata('is_loggedin', FALSE);
		$this->session->unset_userdata('id_calon_pendaftar');
		$this->session->unset_userdata('calon_pendaftar');
		
		if ($id_calon_pendaftar != 0)
		{
			redirect('/'); exit();
		}
		else
		{
			redirect('/'); exit();
			//redirect('front/login_manual'); exit();
		}
	}
	
	function forgot_password()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST')
		{
			$email = $this->input->post('email');
			
			if ( ! $this->calon_pendaftar_model->email_exist($email))
			{
				$this->smarty->assign('email_wrong', 1);
			}
			else
			{
				$this->smarty->assign('email_ok', 1);
				
				// mendapatkan row CALON_PENDAFTAR
				$calon_pendaftar = $this->calon_pendaftar_model->get_by_email($email);
				
				// assign data
				$this->smarty->assignByRef('calon_pendaftar', $calon_pendaftar);
				
				// get email template
				$email_content = $this->smarty->fetch('email/reset_password.tpl');
				
				// script kirim email
				$this->email->from('pmb@umaha.ac.id', 'PMB Universitas Maarif Hasyim Latif');
				$this->email->to($email); 
				$this->email->set_mailtype('html');
				$this->email->subject('Permintaan Reset Password');
				$this->email->message($email_content);
				$this->email->send();
			}
		}
		
		$this->smarty->display('auth/forgot_password.tpl');
	}
	
	function reset_password()
	{
		$id_calon_pendaftar	= $this->input->get('id', TRUE);
		$email_hash			= $this->input->get('e', TRUE);
		$pass_pendaftar		= $this->input->get('p', TRUE);
		
		$calon_pendaftar = $this->calon_pendaftar_model->get_for_reset($id_calon_pendaftar, $email_hash, $pass_pendaftar);
		
		if ($calon_pendaftar)
		{
			if ($this->input->server('REQUEST_METHOD') == 'POST')
			{
				$password	= $this->input->post('password');
				
				$this->form_validation->set_error_delimiters('<p class="help-block text-danger">','</p>');
				$this->form_validation->set_rules('password', 'Password Baru', 'trim|required|min_length[4]');
				$this->form_validation->set_rules('password2', 'Password Baru (Ulangi)', 'trim|required|min_length[4]|matches[password]');
				
				if ($this->form_validation->run())
				{
					if ($calon_pendaftar->PASS_PENDAFTAR == sha1($password))
					{
						$this->smarty->assign('error_message', 'Password baru tidak boleh sama dengan password sebelumnya');
					}
					else
					{
						$this->calon_pendaftar_model->set_password($calon_pendaftar->EMAIL_PENDAFTAR, sha1($password));
						$this->smarty->assign('change_ok', 1);
					}
				}
				
				//$this->smarty->assign('change_ok', 1);
			}
			
			$this->smarty->display('auth/reset_password.tpl');
		}
	}
}