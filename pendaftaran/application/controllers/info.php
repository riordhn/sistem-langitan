<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Info extends CI_Controller
{
	public function petunjuk()
	{
		$this->smarty->display('info/petunjuk.tpl');
	}
	
	public function pengumuman()
	{
		$this->smarty->display('info/pengumuman.tpl');
	}
}
