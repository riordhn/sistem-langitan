<?php
/**
 * Description of api
 *
 * @author Fathoni <fathoni@staf.unair.ac.id>
 * @property CI_Loader $load CI_Loader
 * @property Master_Model $master_model Description
 * @property Cmb_Model $cmb_model Description
 * @property Penerimaan_Model $penerimaan_model Description
 */
class Api extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->load->database();
		$this->load->model('Master_Model', 'master_model');
		$this->load->model('Cmb_Model', 'cmb_model');
		
		$this->lokasi_file = "/web/files";
		
		header('Access-Control-Allow-Origin: *');
	}
	
	public function test_db_salah()
	{
		$this->query = $this->db->get('salah_query abc def');

		echo $this->query->num_rows();
	}
	
	public function get_kota_list()
	{
		echo json_encode($this->master_model->get_kota_list());
	}
	
	public function get_sekolah_list($id_kota)
	{
		echo json_encode($this->master_model->get_sekolah_list($id_kota));
	}
	
	/*public function get_program_studi_list($id_penerimaan, $kode_jurusan)
	{
		echo json_encode($this->master_model->get_program_studi_list($id_penerimaan, $kode_jurusan));
	}*/
	public function get_program_studi_list($id_penerimaan)
	{
		echo json_encode($this->master_model->get_program_studi_list_baru($id_penerimaan));
	}
	
	public function get_prodi_minat_list($id_penerimaan, $id_program_studi)
	{
		echo json_encode($this->master_model->get_prodi_minat_list($id_penerimaan, $id_program_studi));
	}
	
	public function get_kelompok_biaya_list($id_penerimaan, $id_program_studi)
	{
		echo json_encode($this->master_model->get_kelompok_biaya($id_penerimaan, $id_program_studi));
	}
	
	/**
	 * Mengecek nomer ujian
	 * @param string $kode_voucher
	 */
	public function cek_no_ujian($kode_voucher)
	{
		echo $this->cmb_model->cek_no_ujian($kode_voucher);
	}
	
	/**
	 * Mengeset nomer ujian
	 * @param string $kode_voucher
	 */
	public function set_no_ujian($kode_voucher)
	{
		echo $this->cmb_model->set_no_ujian($kode_voucher);
	}
	
	/**
	 * Mereset nomer ujian
	 * @param string $kode_voucher
	 */
	public function unset_no_ujian($kode_voucher)
	{
		echo $this->cmb_model->unset_no_ujian($kode_voucher);
	}
	
	/**
	 * Download pdf peserta
	 * @param int $id_penerimaan
	 */
	public function download_pdf($id_penerimaan)
	{
		$this->load->helper('inflector');
		$this->load->helper('biodata_pdf');
		$this->load->library('tcpdf', array(
			'unicode'		=> false,
			'encoding'		=> 'ISO-8859-1',
			'pdfa'			=> true
		));
		
		// Jika belum ada file zip-nya maka dilakukan generate
		if ( ! file_exists("{$this->lokasi_file}/download/{$id_penerimaan}_biodata.zip"))
		{
			$cmb_set = $this->cmb_model->list_cmb_download_pdf($id_penerimaan);
		
			generate_biodata_pdf($cmb_set, $this->lokasi_file, $id_penerimaan);
		}
		
		// Diarahkan ke file hasil zip
		redirect(base_url("files/download/{$id_penerimaan}_biodata.zip"));
	}
	
	/**
	 * Cek kondisi file
	 * @param type $id_penerimaan
	 */
	public function cek_zip_files($id_penerimaan)
	{
		if ($id_penerimaan != '')
		{
			// Cek file, apakah sudah ada,
			// per 06 april 2015, zip --> tar
			if (file_exists("{$this->lokasi_file}/download/{$id_penerimaan}.tar"))
			{
				echo "1"; exit();
			}
			else
			{
				echo "0"; exit();
			}
		}
	}
	
	/**
	 * Mendapatkan jumlah peserta yg upload
	 * @param type $id_penerimaan
	 */
	public function count_files($id_penerimaan)
	{
		if ($id_penerimaan != '')
		{
			echo exec("ls {$this->lokasi_file}/{$id_penerimaan} | wc -l");
		}
	}
	
	/**
	 * Mengkompress files2 syarat
	 * @param int $id_penerimaan
	 */
	public function zip_files($id_penerimaan, $force = 0)
	{	
		if ($id_penerimaan != '')
		{	
			$this->load->model('Penerimaan_Model', 'penerimaan_model');
			$this->load->helper('file');
			
			// Pindah working direktori ke /web/files
			chdir("{$this->lokasi_file}");
			
			// remove zip/tar files if exist
			// per 6 April 2015 diganti menggunakan tar agar irit storage
			if (file_exists("download/{$id_penerimaan}.tar"))
			{
				unlink("download/{$id_penerimaan}.tar");
			}

			// Load row file dan foto peserta dari database
			$file_set = $this->penerimaan_model->list_file_peserta($id_penerimaan);
			$foto_set = $this->penerimaan_model->list_foto_peserta($id_penerimaan);

			foreach ($file_set as &$file)
			{
				// Source File
				$file_source = "{$file->ID_PENERIMAAN}/{$file->ID_C_MHS}/{$file->FILE_SYARAT}";

				// create folder penerimaan jika belum ada
				if ( ! file_exists("download/{$id_penerimaan}"))
				{
					mkdir("download/{$id_penerimaan}");
				}

				// create folder no ujian jika belum ada
				if ( ! file_exists("download/{$id_penerimaan}/{$file->NO_UJIAN}"))
				{
					mkdir("download/{$id_penerimaan}/{$file->NO_UJIAN}");
				}
				
				// target file
				$file_dest = preg_replace('/[^a-zA-Z0-9_ \(\)%%&-]/s', '_', trim($file->NM_SYARAT));	// clear special character
				$file_dest = substr($file_dest, 0, 250) . substr($file->FILE_SYARAT, -4 , 4);			// build file name + extension
				$file_dest = "download/{$id_penerimaan}/{$file->NO_UJIAN}/" . $file_dest;				// add path
				
				// Copy file 
				copy($file_source, $file_dest);
				
				// Cek jika tar file belum ada, maka create dulu
				$tar_file = "download/{$id_penerimaan}.tar";
				if ( ! file_exists($tar_file))
				{
					// Create tar baru
					exec("tar -v --create --file={$tar_file} \"{$file_dest}\"");
				}
				else
				{
					// Append ke existing tar
					exec("tar -v --append --file={$tar_file} \"{$file_dest}\"");
				}
				
				// Setelah selesai di store ke tar, file dihapus
				unlink($file_dest);
			}
			
			foreach ($foto_set as &$foto)
			{				
				// Source File
				$file_source = "{$id_penerimaan}/{$foto->ID_C_MHS}/{$foto->FILE_FOTO}";
				
				// Target File
				$file_dest = "download/{$id_penerimaan}/{$foto->NO_UJIAN}/foto.jpg";
				
				// copy file
				copy($file_source, $file_dest);
				
				// Cek jika tar file belum ada, maka create dulu
				$tar_file = "download/{$id_penerimaan}.tar";
				if ( ! file_exists($tar_file))
				{
					// Create tar baru
					exec("tar --create --file={$tar_file} \"{$file_dest}\"");
				}
				else
				{
					// Append ke existing tar
					exec("tar --append --file={$tar_file} \"{$file_dest}\"");
				}
				
				// Setelah selesai di store ke tar, file + folder dihapus
				unlink($file_dest);
				rmdir("download/{$id_penerimaan}/{$foto->NO_UJIAN}/");
			}
			
			// Hapus folder id penerimaan
			rmdir("download/{$id_penerimaan}");
			
			// proses zip file,
			// -r : recursive folder didalamnya
			// -m : move / delete after store
			// exec("zip -rm {$this->lokasi_file}/download/{$id_penerimaan} {$this->lokasi_file}/download/{$id_penerimaan}", $output_set);
			
			echo 'OK';
		}
	}
}
