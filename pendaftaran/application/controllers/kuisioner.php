<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Fathoni <fathoni@staf.unair.ac.id>
 * @property Cmb_Model $cmb_model Description
 * @property int $id_c_mhs Description
 * @property Kuisioner_Model $kuisioner_model Description
 */
class Kuisioner extends MY_Controller
{
	private $id_c_mhs;
	
	function __construct()
	{
		parent::__construct();
		
		$this->check_credentials();
		$this->load->model('Cmb_Model', 'cmb_model');
		
		$calon_pendaftar		= $this->session->userdata('calon_pendaftar');
		$this->id_c_mhs			= $calon_pendaftar->ID_C_MHS_AKTIF;
		
		// jika belum punya data calon di redirect
		if (empty($this->id_c_mhs))
		{
			$this->smarty->display('form/pilih_penerimaan.tpl');
			exit();
		}
	}
	
	function index()
	{
		$this->load->model('Kuisioner_Model', 'kuisioner_model');
		
		$cmb = $this->cmb_model->get_cmb($this->id_c_mhs);
		
		// jika sudah mengisi diredirect ke pengambilan nomer
		if ($cmb->IS_ISI_KUISIONER == 1)
		{
			redirect('verifikasi/kartu_ujian');
			exit();
		}
		
		$form = $this->kuisioner_model->get_kuisioner($cmb->ID_PENERIMAAN);
		$this->smarty->assignByRef('form', $form);
		
		// Mendapatkan jumlah soal
		$jumlah_soal = 0;
		
		foreach ($form->kategori_soal_set as $kategori_soal)
		{
			$jumlah_soal += count($kategori_soal->soal_set);
		}
		
		$this->smarty->assign('jumlah_soal', $jumlah_soal);
		
		if ($this->input->server('REQUEST_METHOD') == 'POST')
		{
			// -------------------------------------------------------------
			// Membandingkan jumlah jawaban (memastikan semua diisi lengkap)
			// -------------------------------------------------------------
			$jawaban_set = $this->input->post('jawaban');
			
			if (count($jawaban_set) == $jumlah_soal)
			{
				$this->kuisioner_model->update_jawaban($this->id_c_mhs, $cmb->PENERIMAAN->ID_FORM_KUISIONER, $jawaban_set);
				
				redirect('verifikasi/kartu_ujian');
				exit();
			}
		}
		
		$this->smarty->display('kuisioner/index.tpl');
	}
}
