<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

/**
 * @property Cmb_Model $cmb_model Description
 * @property Master_Model $master_model Description
 * @property Penerimaan_Model $penerimaan_model Description
 * @property int $id_c_mhs CALON_MAHASISWA_BARU.ID_C_MHS atau CALON_PENDAFTAR.ID_C_MHS_AKTIF
 */
class Form extends MY_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->check_credentials();

		$this->load->model('Cmb_Model', 'cmb_model');
		$this->load->model('Master_Model', 'master_model');
		$this->load->model('Penerimaan_Model', 'penerimaan_model');

		// Mendapatkan ID_C_MHS Aktif
		$calon_pendaftar	= $this->session->userdata('calon_pendaftar');
		$this->id_c_mhs		= $calon_pendaftar->ID_C_MHS_AKTIF;

		//Mendapatkan identitas PT
		$this->nama_pt = $this->perguruan_tinggi['NAMA_PERGURUAN_TINGGI'];
		$this->id_pt = $this->perguruan_tinggi['ID_PERGURUAN_TINGGI'];

		// jika belum punya data calon di redirect
		if (empty($this->id_c_mhs)) {
			$this->smarty->display('form/pilih_penerimaan.tpl');
			exit();
		}
	}

	function index()
	{
		$validation_complete	= TRUE;

		$this->load->model('Calon_Pendaftar_Model', 'calon_pendaftar_model');
		$this->load->model('Cmb_Model', 'cmb_model');
		$perguruan_tinggi	= $this->session->userdata('perguruan_tinggi');

		// set local indonesia
		setlocale(LC_TIME, 'id_ID');

		$calon_pendaftar	= $this->session->userdata('calon_pendaftar');
		$id_c_mhs			= $calon_pendaftar->ID_C_MHS_AKTIF;

		// load data semua CALON_MAHASISWA_*
		$cmb = $this->cmb_model->get_cmb($this->id_c_mhs);
		$cms = $this->cmb_model->get_cms($this->id_c_mhs);
		$cmo = $this->cmb_model->get_cmo($this->id_c_mhs);
		$cmd = $this->cmb_model->get_cmd($this->id_c_mhs);
		$cmp = $this->cmb_model->get_cmp($this->id_c_mhs);
		$cmf = $this->cmb_model->get_cmf($id_c_mhs);

		// Saat disubmit
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			// Setting Validation
			$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');
			$this->form_validation->set_message('required', '%s tidak boleh kosong.');
			$this->form_validation->set_message('integer', '%s harus angka bulat');
			$this->form_validation->set_message('exact_length', '%s harus %s digit');
			$this->form_validation->set_message('numeric', 'Angka saja yg dimasukkan');

			// Data diri: CALON_MAHASISWA_BARU
			//$this->form_validation->set_rules('nm_c_mhs', 'Nama Lengkap', 'required');
			$this->form_validation->set_rules('nama_ibu', 'Nama ibu', 'required');
			$this->form_validation->set_rules('jenis_kelamin', 'Jenis kelamin', 'required');
			$this->form_validation->set_rules('id_kota_lahir', 'Tempat lahir', 'required');
			$this->form_validation->set_rules('tgl_lahir_Day', '', 'required');
			$this->form_validation->set_rules('tgl_lahir_Month', '', 'required');
			$this->form_validation->set_rules('tgl_lahir_Year', '', 'required|integer');
			$this->form_validation->set_rules('id_agama', 'Agama', 'required');
			$this->form_validation->set_rules('sumber_biaya', 'Sumber biaya', 'required');
			$this->form_validation->set_rules('id_disabilitas', 'Disabilitas / Difabel', 'required');

			// Kategori Alamat
			$this->form_validation->set_rules('nik_c_mhs', 'NIK', 'required');
			$this->form_validation->set_rules('kewarganegaraan', 'Kewarganegaraan', 'required');
			$this->form_validation->set_rules('alamat_jalan', 'Jalan', 'required');
			$this->form_validation->set_rules('alamat_dusun', 'Dusun', 'required');
			$this->form_validation->set_rules('alamat_rt', 'RT', 'required');
			$this->form_validation->set_rules('alamat_rw', 'RW', 'required');
			$this->form_validation->set_rules('alamat_kelurahan', 'Kelurahan', 'required');
			$this->form_validation->set_rules('alamat_kecamatan', 'Kecamatan', 'required');
			$this->form_validation->set_rules('alamat_kodepos', 'Kode Pos', 'required');
			$this->form_validation->set_rules('id_kota', 'Kota', 'required');
			$this->form_validation->set_rules('nm_jenis_tinggal', 'Jenis Tinggal', 'required');
			$this->form_validation->set_rules('telp', 'No Telp', 'required|numeric');
			$this->form_validation->set_rules('nomor_hp', 'No HP', 'required|numeric');
			$this->form_validation->set_rules('email', 'Email', 'required');
			//$this->form_validation->set_rules('nomor_kps', 'No KPS (Jika Ada)', 'required');

			/*$this->form_validation->set_rules('sumber_biaya', 'Sumber biaya', 'required');
				$this->form_validation->set_rules('id_disabilitas', 'Disabilitas', 'required');*/

			// Info sekolah: CALON_MAHASISWA_SEKOLAH
			// Jika perguruan tinggi UNU Lampung
			if ($perguruan_tinggi['ID_PERGURUAN_TINGGI'] == 5) {
				$this->form_validation->set_rules('nisn', 'NISN', 'required');
				$this->form_validation->set_rules('id_kota_sekolah', 'Kota Sekolah', 'required');
				$this->form_validation->set_rules('id_sekolah_asal', 'Asal Sekolah', 'required');
				$this->form_validation->set_rules('jurusan_sekolah', 'Jurusan', 'required');
				$this->form_validation->set_rules('tahun_lulus', 'Tahun lulus', 'required|exact_length[4]');

				// Preprocessing form input tanggalan
				$cms->ID_KOTA_SEKOLAH	= $this->input->post('id_kota_sekolah');

				// Info orang tua: CALON_MAHASISWA_ORTU - Ayah

				//$this->form_validation->set_rules('nama_ayah', 'Nama ayah', 'required');
				$this->form_validation->set_rules('pekerjaan_ayah', 'Pekerjaan ayah', 'required');
				$this->form_validation->set_rules('penghasilan_ayah', 'Penghasilan ayah', 'required|integer');
				//$this->form_validation->set_rules('masa_kerja_ayah', 'Masa kerja ayah', 'required|callback_local_decimal');

				// Info orang tua: CALON_MAHASISWA_ORTU - Ibu

				//$this->form_validation->set_rules('nama_ibu', 'Nama ibu', 'required');
				$this->form_validation->set_rules('pekerjaan_ibu', 'Pekerjaan ibu', 'required');
				$this->form_validation->set_rules('penghasilan_ibu', 'Penghasilan ibu', 'required|integer');
				//$this->form_validation->set_rules('masa_kerja_ibu', 'Masa kerja ibu', 'required|callback_local_decimal');
			} else {
				$this->form_validation->set_rules('nisn', 'NISN', 'required');
				$this->form_validation->set_rules('id_kota_sekolah', 'Kota Sekolah', 'required');
				$this->form_validation->set_rules('id_sekolah_asal', 'Asal Sekolah', 'required');
				$this->form_validation->set_rules('jurusan_sekolah', 'Jurusan', 'required');
				$this->form_validation->set_rules('tahun_lulus', 'Tahun lulus', 'required|exact_length[4]');
				$this->form_validation->set_rules('no_ijazah', 'No Ijazah', 'required');
				$this->form_validation->set_rules('tgl_ijazah_Day', '', 'required');
				$this->form_validation->set_rules('tgl_ijazah_Month', '', 'required');
				$this->form_validation->set_rules('tgl_ijazah_Year', '', 'required|integer');
				$this->form_validation->set_rules('jumlah_pelajaran_ijazah', 'Mapel Ijazah', 'required|integer');
				$this->form_validation->set_rules('nilai_ijazah', 'Nilai rata-rata Ijazah', 'required|callback_local_decimal');
				$this->form_validation->set_rules('jumlah_pelajaran_uan', 'Mapel UAN', 'required|integer');
				$this->form_validation->set_rules('nilai_uan', 'Nilai rata-rata UAN', 'required|callback_local_decimal');

				// Preprocessing form input tanggalan
				$cms->TGL_IJAZAH		= $this->get_input_date('tgl_ijazah');
				$cms->ID_KOTA_SEKOLAH	= $this->input->post('id_kota_sekolah');

				// Info orang tua: CALON_MAHASISWA_ORTU - Ayah

				//$this->form_validation->set_rules('nama_ayah', 'Nama ayah', 'required');
				$this->form_validation->set_rules('alamat_ayah', 'Alamat ayah', 'required');
				$this->form_validation->set_rules('id_kota_ayah', 'Kota ayah', 'required');
				$this->form_validation->set_rules('tgl_lahir_ayah_Day', '', 'required');
				$this->form_validation->set_rules('tgl_lahir_ayah_Month', '', 'required');
				$this->form_validation->set_rules('tgl_lahir_ayah_Year', '', 'required|integer');
				$this->form_validation->set_rules('telp_ayah', 'Telp ayah', 'required|numeric');
				$this->form_validation->set_rules('pendidikan_ayah', 'Pendidikan ayah', 'required');
				$this->form_validation->set_rules('pekerjaan_ayah', 'Pekerjaan ayah', 'required');
				$this->form_validation->set_rules('penghasilan_ayah', 'Penghasilan ayah', 'required|integer');
				$this->form_validation->set_rules('id_disabilitas_ayah', 'Disabilitas ayah', 'required');
				//$this->form_validation->set_rules('masa_kerja_ayah', 'Masa kerja ayah', 'required|callback_local_decimal');

				// Info orang tua: CALON_MAHASISWA_ORTU - Ibu

				/*$this->form_validation->set_rules('nama_ibu', 'Nama ibu', 'required');*/
				$this->form_validation->set_rules('alamat_ibu', 'Alamat ibu', 'required');
				$this->form_validation->set_rules('id_kota_ibu', 'Kota ibu', 'required');
				$this->form_validation->set_rules('tgl_lahir_ibu_Day', '', 'required');
				$this->form_validation->set_rules('tgl_lahir_ibu_Month', '', 'required');
				$this->form_validation->set_rules('tgl_lahir_ibu_Year', '', 'required|integer');
				$this->form_validation->set_rules('telp_ibu', 'Telp ibu', 'required|numeric');
				$this->form_validation->set_rules('pendidikan_ibu', 'Pendidikan ibu', 'required');
				$this->form_validation->set_rules('pekerjaan_ibu', 'Pekerjaan ibu', 'required');
				$this->form_validation->set_rules('penghasilan_ibu', 'Penghasilan ibu', 'required|integer');
				$this->form_validation->set_rules('id_disabilitas_ibu', 'Disabilitas ibu', 'required');
				//$this->form_validation->set_rules('masa_kerja_ibu', 'Masa kerja ibu', 'required|callback_local_decimal');
			}



			$this->form_validation->set_rules('is_ajukan_bidikmisi', 'Pengajuan Bidikmisi', 'required');

			// Info orang tua: CALON_MAHASISWA_ORTU - Wali (ditutup karena tidak harus diisi)

			//$this->form_validation->set_rules('nama_wali', 'Nama wali', 'required');
			//$this->form_validation->set_rules('alamat_ayah', 'Alamat ayah', 'required');
			/*$this->form_validation->set_rules('tgl_lahir_wali_Day', '', 'required');
			$this->form_validation->set_rules('tgl_lahir_wali_Month', '', 'required');
			$this->form_validation->set_rules('tgl_lahir_wali_Year', '', 'required|integer');
			//$this->form_validation->set_rules('telp_ayah', 'Telp ayah', 'required|numeric');
			$this->form_validation->set_rules('pendidikan_wali', 'Pendidikan wali', 'required');
			$this->form_validation->set_rules('pekerjaan_wali', 'Pekerjaan wali', 'required');
			$this->form_validation->set_rules('penghasilan_wali', 'Penghasilan wali', 'required|integer');*/
			//$this->form_validation->set_rules('masa_kerja_ayah', 'Masa kerja ayah', 'required|callback_local_decimal');

			// Pre processing input
			$cmb->TGL_LAHIR			= $this->get_input_date('tgl_lahir');
			$cmo->TGL_LAHIR_AYAH	= $this->get_input_date('tgl_lahir_ayah');
			$cmo->TGL_LAHIR_IBU		= $this->get_input_date('tgl_lahir_ibu');
			$cmo->TGL_LAHIR_WALI	= $this->get_input_date('tgl_lahir_wali');

			// Pilihan program studi
			$this->form_validation->set_rules('id_pilihan_1', 'Pilihan program studi', 'required');
			//$this->form_validation->set_rules('is_setuju_pernyataan', '', 'required');

			// Pre processing input
			//$cmb->TGL_LAHIR			= $this->get_input_date('tgl_lahir');

			$cmb->ID_PILIHAN_1		= $this->input->post('id_pilihan_1');
			$cmb->ID_PILIHAN_2		= $this->input->post('id_pilihan_2');
			$cmb->ID_PILIHAN_3		= $this->input->post('id_pilihan_3');

			if ($this->form_validation->run()) {
				$this->cmb_model->update_baru($this->id_c_mhs);

				//redirect ke halaman upload berkas
				/*redirect('form/upload'); exit();*/
				redirect('form/');
				exit();
			}

			$validation_complete = false;




			// Form Format S1 / D3
			/*if ($form_type == 's1')
			{
				// Info sekolah: CALON_MAHASISWA_SEKOLAH
				// $this->form_validation->set_rules('nisn', 'NISN', 'required');
				$this->form_validation->set_rules('id_kota_sekolah', 'Kota Sekolah', 'required');
				$this->form_validation->set_rules('id_sekolah_asal', 'Asal Sekolah', 'required');
				$this->form_validation->set_rules('jurusan_sekolah', 'Jurusan', 'required');
				$this->form_validation->set_rules('tahun_lulus', 'Tahun lulus', 'required|exact_length[4]');
				$this->form_validation->set_rules('no_ijazah', 'No Ijazah', 'required');
				$this->form_validation->set_rules('tgl_ijazah_Day', '', 'required');
				$this->form_validation->set_rules('tgl_ijazah_Month', '', 'required');
				$this->form_validation->set_rules('tgl_ijazah_Year', '', 'required|integer');
				$this->form_validation->set_rules('jumlah_pelajaran_ijazah', 'Mapel Ijazah', 'required|integer');
				$this->form_validation->set_rules('nilai_ijazah', 'Nilai rata-rata Ijazah', 'required|callback_local_decimal');
				$this->form_validation->set_rules('jumlah_pelajaran_uan', 'Mapel UAN', 'required|integer');
				$this->form_validation->set_rules('nilai_uan', 'Nilai rata-rata UAN', 'required|callback_local_decimal');

				// Info orang tua: CALON_MAHASISWA_ORTU - Ayah
				$this->form_validation->set_rules('nama_ayah', 'Nama ayah', 'required');
				$this->form_validation->set_rules('alamat_ayah', 'Alamat ayah', 'required');
				$this->form_validation->set_rules('telp_ayah', 'Telp ayah', 'required|numeric');
				$this->form_validation->set_rules('pendidikan_ayah', 'Pendidikan ayah', 'required');
				$this->form_validation->set_rules('pekerjaan_ayah', 'Pekerjaan ayah', 'required');
				$this->form_validation->set_rules('penghasilan_ayah', 'Penghasilan ayah', 'required|integer');
				$this->form_validation->set_rules('masa_kerja_ayah', 'Masa kerja ayah', 'required|callback_local_decimal');

				// Info orang tua: CALON_MAHASISWA_ORTU - Ibu
				/*$this->form_validation->set_rules('nama_ibu', 'Nama ibu', 'required');
				$this->form_validation->set_rules('alamat_ibu', 'Alamat ibu', 'required');
				$this->form_validation->set_rules('telp_ibu', 'Telp ibu', 'required|numeric');
				$this->form_validation->set_rules('pendidikan_ibu', 'Pendidikan ibu', 'required');
				$this->form_validation->set_rules('pekerjaan_ibu', 'Pekerjaan ibu', 'required');
				$this->form_validation->set_rules('penghasilan_ibu', 'Penghasilan ibu', 'required|integer');
				$this->form_validation->set_rules('masa_kerja_ibu', 'Masa kerja ibu', 'required|callback_local_decimal');
				
				// Preprocessing form input tanggalan
				$cms->TGL_IJAZAH		= $this->get_input_date('tgl_ijazah');
				$cms->ID_KOTA_SEKOLAH	= $this->input->post('id_kota_sekolah');
			}
			
			if ($form_type == 's2' || $form_type == 's3')
			{
				// Info Biodata
				$this->form_validation->set_rules('gelar', 'Gelar', '');	// not required agar masuk ke set_value
				
				// Info Pendidikan Sebelumnya S1 atau D3-nya : CALON_MAHASISWA_PASCA
				$this->form_validation->set_rules('ptn_s1', 'Perguruan tinggi', 'required');
				$this->form_validation->set_rules('status_ptn_s1', 'Status perguruan tinggi', 'required');
				$this->form_validation->set_rules('prodi_s1', 'Program studi', 'required');
				$this->form_validation->set_rules('tgl_masuk_s1_Day', '', 'required');
				$this->form_validation->set_rules('tgl_masuk_s1_Month', '', 'required');
				$this->form_validation->set_rules('tgl_masuk_s1_Year', '', 'required|integer');
				$this->form_validation->set_rules('tgl_lulus_s1_Day', '', 'required');
				$this->form_validation->set_rules('tgl_lulus_s1_Month', '', 'required');
				$this->form_validation->set_rules('tgl_lulus_s1_Year', '', 'required|integer');
				$this->form_validation->set_rules('lama_studi_s1', 'Lama studi', 'required|callback_local_decimal');
				$this->form_validation->set_rules('ip_s1', 'Index prestasi', 'required|callback_local_decimal');
				$this->form_validation->set_rules('jumlah_karya_ilmiah', 'Jumlah karya ilmiah', 'required|integer');
				
				// khusus Alih Jenis cek Akreditasi
				if ($cmb->PENERIMAAN->ID_JALUR == 4)
				{
					$this->form_validation->set_rules('jenis_akreditasi_s1', 'Akreditasi', 'required');
					$this->form_validation->set_rules('peringkat_akreditasi_s1', 'Peringkat akreditasi', 'required');
				}
				
				// Preprocessing form input
				$cmp->TGL_MASUK_S1		= $this->get_input_date('tgl_masuk_s1');
				$cmp->TGL_LULUS_S1		= $this->get_input_date('tgl_lulus_s1');
			}
			
			if ($form_type == 's3')
			{
				// Info Pendidikan Sebelumnya S2 atau Profesi-nya : CALON_MAHASISWA_PASCA
				$this->form_validation->set_rules('ptn_s2', 'Perguruan tinggi', 'required');
				$this->form_validation->set_rules('status_ptn_s2', 'Status perguruan tinggi', 'required');
				$this->form_validation->set_rules('prodi_s2', 'Program studi', 'required');
				$this->form_validation->set_rules('tgl_masuk_s2_Day', '', 'required');
				$this->form_validation->set_rules('tgl_masuk_s2_Month', '', 'required');
				$this->form_validation->set_rules('tgl_masuk_s2_Year', '', 'required|integer');
				$this->form_validation->set_rules('tgl_lulus_s2_Day', '', 'required');
				$this->form_validation->set_rules('tgl_lulus_s2_Month', '', 'required');
				$this->form_validation->set_rules('tgl_lulus_s2_Year', '', 'required|integer');
				$this->form_validation->set_rules('lama_studi_s2', 'Lama studi', 'required|callback_local_decimal');
				$this->form_validation->set_rules('ip_s2', 'Index prestasi', 'required|callback_local_decimal');
				
				// Preprocessing form input
				$cmp->TGL_MASUK_S2		= $this->get_input_date('tgl_masuk_s2');
				$cmp->TGL_LULUS_S2		= $this->get_input_date('tgl_lulus_s2');
			}
			
			// Pilihan program studi
			$this->form_validation->set_rules('id_pilihan_1', 'Pilihan program studi', 'required');
			$this->form_validation->set_rules('is_setuju_pernyataan', '', 'required');
			
			// Pre processing input
			$cmb->TGL_LAHIR			= $this->get_input_date('tgl_lahir');
			
			$cmb->ID_PILIHAN_1		= $this->input->post('id_pilihan_1');
			$cmb->ID_PILIHAN_2		= $this->input->post('id_pilihan_2');
			$cmb->ID_PILIHAN_3		= $this->input->post('id_pilihan_3');
			$cmb->ID_PILIHAN_4		= $this->input->post('id_pilihan_4');
			
			$cmp->ID_PRODI_MINAT	= $this->input->post('id_prodi_minat');
			
			if ($this->form_validation->run())
			{
				$this->cmb_model->update($this->id_c_mhs, $form_type);
				
				redirect('form/isi');
			}
			
			$validation_complete = false;*/
		}

		// Load data master-master
		$kota_set				= $this->master_model->get_kota_list();
		$kewarganegaraan_set	= $this->master_model->get_kewarganegaraan_list();
		$agama_set				= $this->master_model->get_agama_list();
		$jenis_tinggal_set		= $this->master_model->get_jenis_tinggal_list();
		$sumber_biaya_set		= $this->master_model->get_sumber_biaya_list();
		$disabilitas_set		= $this->master_model->get_disabilitas_list();
		$pendidikan_ortu_set	= $this->master_model->get_pendidikan_ortu_list();
		$pekerjaan_ortu_set		= $this->master_model->get_pekerjaan_ortu_list();

		$jurusan_sekolah_set	= $this->master_model->get_jurusan_sekolah_list();

		// Load sekolah jika pilihan kota sekolah sudah ada
		if ($cms->ID_KOTA_SEKOLAH != NULL) {
			$sekolah_set = $this->master_model->get_sekolah_list($cms->ID_KOTA_SEKOLAH);
		}

		// Program studi set
		$program_studi_set		= $this->master_model->get_program_studi_list_baru($cmb->PENERIMAAN->ID_PENERIMAAN);

		// Jumlah Pilihan Prodi
		$jumlah_pilihan_prodi = $cmb->PENERIMAAN->JUMLAH_PILIHAN_PRODI;

		// assign all variabel
		$this->smarty->assignByRef('cmb', $cmb);
		$this->smarty->assignByRef('cms', $cms);
		$this->smarty->assignByRef('cmo', $cmo);
		$this->smarty->assignByRef('cmd', $cmd);
		$this->smarty->assignByRef('cmp', $cmp);
		$this->smarty->assignByRef('kota_set', $kota_set);
		$this->smarty->assignByRef('agama_set', $agama_set);
		$this->smarty->assignByRef('sumber_biaya_set', $sumber_biaya_set);
		$this->smarty->assignByRef('jurusan_sekolah_set', $jurusan_sekolah_set);
		$this->smarty->assignByRef('sekolah_set', $sekolah_set);
		$this->smarty->assignByRef('disabilitas_set', $disabilitas_set);
		$this->smarty->assignByRef('kewarganegaraan_set', $kewarganegaraan_set);
		$this->smarty->assignByRef('jenis_tinggal_set', $jenis_tinggal_set);
		$this->smarty->assignByRef('pendidikan_ortu_set', $pendidikan_ortu_set);
		$this->smarty->assignByRef('pekerjaan_ortu_set', $pekerjaan_ortu_set);
		$this->smarty->assignByRef('validation_complete', $validation_complete);

		$this->smarty->assignByRef('program_studi_set', $program_studi_set);

		$this->smarty->assignByRef('jumlah_pilihan_prodi', $jumlah_pilihan_prodi);


		// assign all variabel
		/*$this->smarty->assignByRef('cmb', $cmb);
		$this->smarty->assignByRef('cms', $cms);
		$this->smarty->assignByRef('cmo', $cmo);
		$this->smarty->assignByRef('cmd', $cmd);
		$this->smarty->assignByRef('cmp', $cmp);
		$this->smarty->assignByRef('template', $template);
		$this->smarty->assignByRef('kewarganegaraan_set', $kewarganegaraan_set);
		$this->smarty->assignByRef('agama_set', $agama_set);
		$this->smarty->assignByRef('sumber_biaya_set', $sumber_biaya_set);
		$this->smarty->assignByRef('jurusan_sekolah_set', $jurusan_sekolah_set);
		$this->smarty->assignByRef('pendidikan_ortu_set', $pendidikan_ortu_set);
		$this->smarty->assignByRef('pekerjaan_ortu_set', $pekerjaan_ortu_set);
		$this->smarty->assignByRef('disabilitas_set', $disabilitas_set);
		$this->smarty->assignByRef('program_studi_set', $program_studi_set);
		$this->smarty->assignByRef('sekolah_set', $sekolah_set);
		$this->smarty->assignByRef('validation_complete', $validation_complete);
		$this->smarty->assignByRef('prodi_minat_set', $prodi_minat_set);
		$this->smarty->assignByRef('kelompok_biaya_set', $kelompok_biaya_set);*/

		$this->smarty->assign('id_pt', $this->id_pt);
		$this->smarty->assign('nama_pt', $this->nama_pt);
		
		$this->smarty->assign('is_verifikasi', $cmb->PENERIMAAN->IS_VERIFIKASI);
		// Cek apakah ada ujian pada penerimaan tersebut
		$this->smarty->assign('is_ujian', $cmb->PENERIMAAN->IS_UJIAN);

		$this->smarty->display('form/isian.tpl');
	}

	function nomor_rekening()
	{

		$this->load->model('Calon_Pendaftar_Model', 'calon_pendaftar_model');
		$this->load->model('Cmb_Model', 'cmb_model');

		// set local indonesia
		setlocale(LC_TIME, 'id_ID');

		$calon_pendaftar	= $this->session->userdata('calon_pendaftar');
		$id_c_mhs			= $calon_pendaftar->ID_C_MHS_AKTIF;

		// load data semua CALON_MAHASISWA_*
		$cmb = $this->cmb_model->get_cmb($this->id_c_mhs);

		$nomor_rekening_transfer = $cmb->PENERIMAAN->NOMOR_REKENING_TRANSFER;

		$this->smarty->assign('nama_pt', $this->nama_pt);

		$this->smarty->assign('nomor_rekening_transfer', $nomor_rekening_transfer);

		$this->smarty->display('form/nomor_rekening.tpl');
	}

	function cetak()
	{

		$this->load->model('Calon_Pendaftar_Model', 'calon_pendaftar_model');
		$this->load->model('Cmb_Model', 'cmb_model');

		// set local indonesia
		setlocale(LC_TIME, 'id_ID');

		$calon_pendaftar	= $this->session->userdata('calon_pendaftar');
		$id_c_mhs			= $calon_pendaftar->ID_C_MHS_AKTIF;

		// load data semua CALON_MAHASISWA_*
		$cmb = $this->cmb_model->get_cmb($this->id_c_mhs);
		$cms = $this->cmb_model->get_cms($this->id_c_mhs);
		$cmo = $this->cmb_model->get_cmo($this->id_c_mhs);
		$cmd = $this->cmb_model->get_cmd($this->id_c_mhs);
		$cmp = $this->cmb_model->get_cmp($this->id_c_mhs);
		$cmf = $this->cmb_model->get_cmf($id_c_mhs);

		$this->smarty->assign('nama_pt', $this->nama_pt);

		// Belum di submit
		if (empty($cmb->TGL_REGISTRASI)) {
			$this->smarty->display('form/cetak_belum_submit.tpl');
			exit();
		}


		// Saat disubmit
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$this->load->library('tcpdf', array(
				'unicode'		=> false,
				'encoding'		=> 'ISO-8859-1',
				'pdfa'			=> true
			));

			$this->load->helper('inflector');
			$this->load->helper('biodata_ptnu');

			/*print_r($cmb);*/

			// Mendapatkan row PTNU
			$this->query = $this->db->get_where('perguruan_tinggi', array('id_perguruan_tinggi' => $this->id_pt));
			$ptnu = $this->query->row();

			//echo "hahaha hehe ".$cmb->ID_PENERIMAAN."huhu ".$ptnu;

			$cmb_set = $this->cmb_model->list_cmb_download_pdf($cmb->ID_PENERIMAAN, $this->id_c_mhs);

			$syarat_berkas_set = $this->cmb_model->get_list_syarat_umum($this->id_c_mhs, $cmb->ID_PENERIMAAN);

			/*print_r($cmb_set);*/

			generate_biodata_ptnu($this->tcpdf, $cmb_set, $syarat_berkas_set, $cmf, $cmb->ID_PENERIMAAN, $ptnu);
			exit();
		}

		$this->query = $this->db->query(
			"select kode_voucher, id_penerimaan
				from calon_mahasiswa_baru
				where 
					id_c_mhs = ?",
			array((int)$this->id_c_mhs)
		);

		// Mendapatkan row calon mhs
		$c_mhs = $this->query->first_row();

		// clear result
		$this->query->free_result();


		$this->query = $this->db->query(
			"select tgl_bayar
				from voucher
				where 
					kode_voucher = ? and
					id_penerimaan = ? ",
			array($c_mhs->KODE_VOUCHER, (int)$c_mhs->ID_PENERIMAAN)
		);

		// Mendapatkan row calon mhs
		$tgl = $this->query->first_row();

		// clear result
		$this->query->free_result();

		$this->smarty->assign('tgl_bayar', $tgl->TGL_BAYAR);

		$this->smarty->assign('nm_c_mhs', $cmb->NM_C_MHS);

		$this->smarty->assignByRef('cmb', $cmb);

		$this->smarty->assign('id_pt', $this->perguruan_tinggi['ID_PERGURUAN_TINGGI']);

		$this->smarty->display('form/cetak.tpl');
	}

	private function get_template(&$cmb)
	{
		$template_file		= '';
		$jumlah_pilihan		= '';
		$jurusan_sekolah	= '';

		// shorcut var
		$penerimaan			= $cmb->PENERIMAAN;

		// Jumlah pilihan
		if ($cmb->KODE_JURUSAN == '01') {
			$jumlah_pilihan = 2;
			$jurusan_sekolah = 1;
		} //ipa
		if ($cmb->KODE_JURUSAN == '02') {
			$jumlah_pilihan = 2;
			$jurusan_sekolah = 2;
		} //ips
		if ($cmb->KODE_JURUSAN == '03') {
			$jumlah_pilihan = 4;
			$jurusan_sekolah = 3;
		} //ipc

		// D3, S1 Mandiri / S1 PBSB, S1 Internasional, D4 non Alih Jenis
		if ($penerimaan->ID_JALUR == 5) {
			$template_file = 'd3';
		}
		if ($penerimaan->ID_JALUR == 3 || $penerimaan->ID_JALUR == 20) {
			$template_file = 's1';
		}
		if ($penerimaan->ID_JALUR == 27) {
			$template_file = 's1-int';
		}
		if ($penerimaan->ID_JALUR == 41) {
			$template_file = 'd4';
		}

		// Alih Jenis (S1 dan D4)
		if ($penerimaan->ID_JALUR == 4) {
			if ($penerimaan->ID_JENJANG == 1) {
				$template_file = 's1-aj';
				$jumlah_pilihan = 1;
			}
			if ($penerimaan->ID_JENJANG == 4) {
				$template_file = 'd4-aj';
				$jumlah_pilihan = 1;
			}
		}

		// D4 (Dari SMA)
		if ($penerimaan->ID_JALUR == 42) {
			if ($penerimaan->ID_JENJANG == 4) {
				$template_file = 'd4-sma';
				$jumlah_pilihan = 2;
			}
		}

		// Pasca S2 / S3 / Jakarta / Kerjasama Haluoleo / Internasional
		if ($penerimaan->ID_JALUR == 6 || $penerimaan->ID_JALUR == 34 || $penerimaan->ID_JALUR == 35 || $penerimaan->ID_JALUR == 41) {
			if ($penerimaan->ID_JENJANG == 2) {
				$template_file = 's2';
			}
			if ($penerimaan->ID_JENJANG == 3) {
				$template_file = 's3';
			}

			$jumlah_pilihan = 1;
		}

		// Profesi dan spesialis
		if ($penerimaan->ID_JALUR == 23) {
			$template_file = 'pr';
			$jumlah_pilihan = 1;
		}
		if ($penerimaan->ID_JALUR == 24) {
			$template_file = 'sp';
			$jumlah_pilihan = 1;
		}

		// Tgl form di submit
		if ($cmb->TGL_REGISTRASI != '') {
			$template_file = $template_file . '_view';
		}

		// hasil-nya
		return array(
			'template_file'		=> $template_file,
			'jumlah_pilihan'	=> $jumlah_pilihan,
			'jurusan_sekolah'	=> $jurusan_sekolah
		);
	}

	private function get_input_date($field_name)
	{
		$joinned_string = $this->input->post($field_name . '_Year') . '-' . $this->input->post($field_name . '_Month') . '-' . str_pad($this->input->post($field_name . '_Day'), 2, '0', STR_PAD_LEFT);

		try {
			$hasil = new DateTime($joinned_string);
			return $hasil->format('d-M-Y');
		} catch (Exception $ex) {
			return NULL;
		}
	}

	/**
	 * Mengecek angka desimal (format lokal)
	 * @param any $str
	 * @return bool
	 */
	function local_decimal(&$str)
	{
		$this->form_validation->set_message('local_decimal', '%s harus berupa angka');

		if ($str != '') {
			$str = str_replace(',', '.', $str);

			if (!$this->form_validation->decimal($str)) {
				return (bool)$this->form_validation->integer($str);
			}
		}

		return TRUE;
	}

	function pilih_formulir()
	{
		$cmb = $this->cmb_model->get_cmb($this->id_c_mhs);

		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$this->cmb_model->update_jenis_formulir($cmb->ID_C_MHS, $this->input->post('kode_jurusan'));

			redirect('form/isi');
		}

		$voucher_tarif_set = $this->master_model->get_voucher_tarif_list($cmb->PENERIMAAN->ID_PENERIMAAN);

		$this->smarty->assignByRef('voucher_tarif_set', $voucher_tarif_set);
		$this->smarty->display('form/pilih_formulir.tpl');
	}

	function isi()
	{
		$validation_complete	= TRUE;

		// load data semua CALON_MAHASISWA_*
		$cmb = $this->cmb_model->get_cmb($this->id_c_mhs);
		$cms = $this->cmb_model->get_cms($this->id_c_mhs);
		$cmo = $this->cmb_model->get_cmo($this->id_c_mhs);
		$cmd = $this->cmb_model->get_cmd($this->id_c_mhs);
		$cmp = $this->cmb_model->get_cmp($this->id_c_mhs);

		// Mendapatkan template form
		$template = $this->get_template($cmb);

		// ------------------------------------------------------------
		// Mendapatkan bentuk form (s1, s2/profesi/s1-aj, s3/spesialis)
		// ------------------------------------------------------------
		if ($cmb->PENERIMAAN->ID_JENJANG == 1 || $cmb->PENERIMAAN->ID_JENJANG == 5)   // S1/D3
			{
				if ($cmb->PENERIMAAN->ID_JALUR == 4)  // Alih-Jenis pakai format s2
					{
						$form_type = 's2';
					} else {
					$form_type = 's1';
				}
			} else if ($cmb->PENERIMAAN->ID_JENJANG == 4)	// D4 menggunakan format S2 seperti alih jenis
			{
				if ($cmb->PENERIMAAN->ID_JALUR == 4)  // D4 Alih-Jenis pakai format s2
					{
						$form_type = 's2';
					} else if ($cmb->PENERIMAAN->ID_JALUR == 42)  // D4 Alih-Jenis pakai format s2
					{
						$form_type = 's1';
					}
			} else if ($cmb->PENERIMAAN->ID_JENJANG == 2 || $cmb->PENERIMAAN->ID_JENJANG == 9)  // S2/Profesi
			{
				$form_type = 's2';
			} else if ($cmb->PENERIMAAN->ID_JENJANG == 3 || $cmb->PENERIMAAN->ID_JENJANG == 10) // S3/Spesialis
			{
				$form_type = 's3';
			}

		// Cek kode_jurusan untuk mengetahui jenis formulir
		if ($cmb->KODE_JURUSAN == '') {
			redirect('form/pilih_formulir');
		}

		// Saat disubmit
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			// Setting Validation
			$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');
			$this->form_validation->set_message('required', '%s tidak boleh kosong.');
			$this->form_validation->set_message('integer', '%s harus angka bulat');
			$this->form_validation->set_message('exact_length', '%s harus %s digit');
			$this->form_validation->set_message('numeric', 'Angka saja yg dimasukkan');

			// Data diri: CALON_MAHASISWA_BARU
			$this->form_validation->set_rules('nm_c_mhs', 'Nama Lengkap', 'required');
			$this->form_validation->set_rules('jenis_kelamin', 'Jenis kelamin', 'required');
			$this->form_validation->set_rules('id_kota_lahir', 'Tempat lahir', 'required');
			$this->form_validation->set_rules('tgl_lahir_Day', '', 'required');
			$this->form_validation->set_rules('tgl_lahir_Month', '', 'required');
			$this->form_validation->set_rules('tgl_lahir_Year', '', 'required|integer');
			$this->form_validation->set_rules('alamat', 'Alamat', 'required');
			$this->form_validation->set_rules('id_kota', 'Kota', 'required');
			$this->form_validation->set_rules('telp', 'No Telp', 'required|numeric');
			$this->form_validation->set_rules('id_agama', 'Agama', 'required');
			$this->form_validation->set_rules('sumber_biaya', 'Sumber biaya', 'required');
			$this->form_validation->set_rules('id_disabilitas', 'Disabilitas', 'required');

			// Form Format S1 / D3
			if ($form_type == 's1') {
				// Info sekolah: CALON_MAHASISWA_SEKOLAH
				// $this->form_validation->set_rules('nisn', 'NISN', 'required');
				$this->form_validation->set_rules('id_kota_sekolah', 'Kota Sekolah', 'required');
				$this->form_validation->set_rules('id_sekolah_asal', 'Asal Sekolah', 'required');
				$this->form_validation->set_rules('jurusan_sekolah', 'Jurusan', 'required');
				$this->form_validation->set_rules('tahun_lulus', 'Tahun lulus', 'required|exact_length[4]');
				$this->form_validation->set_rules('no_ijazah', 'No Ijazah', 'required');
				$this->form_validation->set_rules('tgl_ijazah_Day', '', 'required');
				$this->form_validation->set_rules('tgl_ijazah_Month', '', 'required');
				$this->form_validation->set_rules('tgl_ijazah_Year', '', 'required|integer');
				$this->form_validation->set_rules('jumlah_pelajaran_ijazah', 'Mapel Ijazah', 'required|integer');
				$this->form_validation->set_rules('nilai_ijazah', 'Nilai rata-rata Ijazah', 'required|callback_local_decimal');
				$this->form_validation->set_rules('jumlah_pelajaran_uan', 'Mapel UAN', 'required|integer');
				$this->form_validation->set_rules('nilai_uan', 'Nilai rata-rata UAN', 'required|callback_local_decimal');

				// Info orang tua: CALON_MAHASISWA_ORTU - Ayah
				$this->form_validation->set_rules('nama_ayah', 'Nama ayah', 'required');
				$this->form_validation->set_rules('alamat_ayah', 'Alamat ayah', 'required');
				$this->form_validation->set_rules('telp_ayah', 'Telp ayah', 'required|numeric');
				$this->form_validation->set_rules('pendidikan_ayah', 'Pendidikan ayah', 'required');
				$this->form_validation->set_rules('pekerjaan_ayah', 'Pekerjaan ayah', 'required');
				$this->form_validation->set_rules('penghasilan_ayah', 'Penghasilan ayah', 'required|integer');
				$this->form_validation->set_rules('masa_kerja_ayah', 'Masa kerja ayah', 'required|callback_local_decimal');

				// Info orang tua: CALON_MAHASISWA_ORTU - Ibu
				$this->form_validation->set_rules('nama_ibu', 'Nama ibu', 'required');
				$this->form_validation->set_rules('alamat_ibu', 'Alamat ibu', 'required');
				$this->form_validation->set_rules('telp_ibu', 'Telp ibu', 'required|numeric');
				$this->form_validation->set_rules('pendidikan_ibu', 'Pendidikan ibu', 'required');
				$this->form_validation->set_rules('pekerjaan_ibu', 'Pekerjaan ibu', 'required');
				$this->form_validation->set_rules('penghasilan_ibu', 'Penghasilan ibu', 'required|integer');
				$this->form_validation->set_rules('masa_kerja_ibu', 'Masa kerja ibu', 'required|callback_local_decimal');

				// Preprocessing form input tanggalan
				$cms->TGL_IJAZAH		= $this->get_input_date('tgl_ijazah');
				$cms->ID_KOTA_SEKOLAH	= $this->input->post('id_kota_sekolah');
			}

			if ($form_type == 's2' || $form_type == 's3') {
				// Info Biodata
				$this->form_validation->set_rules('gelar', 'Gelar', '');	// not required agar masuk ke set_value

				// Info Pendidikan Sebelumnya S1 atau D3-nya : CALON_MAHASISWA_PASCA
				$this->form_validation->set_rules('ptn_s1', 'Perguruan tinggi', 'required');
				$this->form_validation->set_rules('status_ptn_s1', 'Status perguruan tinggi', 'required');
				$this->form_validation->set_rules('prodi_s1', 'Program studi', 'required');
				$this->form_validation->set_rules('tgl_masuk_s1_Day', '', 'required');
				$this->form_validation->set_rules('tgl_masuk_s1_Month', '', 'required');
				$this->form_validation->set_rules('tgl_masuk_s1_Year', '', 'required|integer');
				$this->form_validation->set_rules('tgl_lulus_s1_Day', '', 'required');
				$this->form_validation->set_rules('tgl_lulus_s1_Month', '', 'required');
				$this->form_validation->set_rules('tgl_lulus_s1_Year', '', 'required|integer');
				$this->form_validation->set_rules('lama_studi_s1', 'Lama studi', 'required|callback_local_decimal');
				$this->form_validation->set_rules('ip_s1', 'Index prestasi', 'required|callback_local_decimal');
				$this->form_validation->set_rules('jumlah_karya_ilmiah', 'Jumlah karya ilmiah', 'required|integer');

				// khusus Alih Jenis cek Akreditasi
				if ($cmb->PENERIMAAN->ID_JALUR == 4) {
					$this->form_validation->set_rules('jenis_akreditasi_s1', 'Akreditasi', 'required');
					$this->form_validation->set_rules('peringkat_akreditasi_s1', 'Peringkat akreditasi', 'required');
				}

				// Preprocessing form input
				$cmp->TGL_MASUK_S1		= $this->get_input_date('tgl_masuk_s1');
				$cmp->TGL_LULUS_S1		= $this->get_input_date('tgl_lulus_s1');
			}

			if ($form_type == 's3') {
				// Info Pendidikan Sebelumnya S2 atau Profesi-nya : CALON_MAHASISWA_PASCA
				$this->form_validation->set_rules('ptn_s2', 'Perguruan tinggi', 'required');
				$this->form_validation->set_rules('status_ptn_s2', 'Status perguruan tinggi', 'required');
				$this->form_validation->set_rules('prodi_s2', 'Program studi', 'required');
				$this->form_validation->set_rules('tgl_masuk_s2_Day', '', 'required');
				$this->form_validation->set_rules('tgl_masuk_s2_Month', '', 'required');
				$this->form_validation->set_rules('tgl_masuk_s2_Year', '', 'required|integer');
				$this->form_validation->set_rules('tgl_lulus_s2_Day', '', 'required');
				$this->form_validation->set_rules('tgl_lulus_s2_Month', '', 'required');
				$this->form_validation->set_rules('tgl_lulus_s2_Year', '', 'required|integer');
				$this->form_validation->set_rules('lama_studi_s2', 'Lama studi', 'required|callback_local_decimal');
				$this->form_validation->set_rules('ip_s2', 'Index prestasi', 'required|callback_local_decimal');

				// Preprocessing form input
				$cmp->TGL_MASUK_S2		= $this->get_input_date('tgl_masuk_s2');
				$cmp->TGL_LULUS_S2		= $this->get_input_date('tgl_lulus_s2');
			}

			// Pilihan program studi
			$this->form_validation->set_rules('id_pilihan_1', 'Pilihan program studi', 'required');
			$this->form_validation->set_rules('is_setuju_pernyataan', '', 'required');

			// Pre processing input
			$cmb->TGL_LAHIR			= $this->get_input_date('tgl_lahir');

			$cmb->ID_PILIHAN_1		= $this->input->post('id_pilihan_1');
			$cmb->ID_PILIHAN_2		= $this->input->post('id_pilihan_2');
			$cmb->ID_PILIHAN_3		= $this->input->post('id_pilihan_3');
			$cmb->ID_PILIHAN_4		= $this->input->post('id_pilihan_4');

			$cmp->ID_PRODI_MINAT	= $this->input->post('id_prodi_minat');

			if ($this->form_validation->run()) {
				$this->cmb_model->update($this->id_c_mhs, $form_type);

				redirect('form/isi');
			}

			$validation_complete = false;
		}

		// Load data master-master
		$kota_set				= $this->master_model->get_kota_list();
		$kewarganegaraan_set	= $this->master_model->get_kewarganegaraan_list();
		$agama_set				= $this->master_model->get_agama_list();
		$sumber_biaya_set		= $this->master_model->get_sumber_biaya_list();
		$disabilitas_set		= $this->master_model->get_disabilitas_list();

		// ------------------------------
		// Load spesifik master dan rule
		// ------------------------------
		if ($form_type == 's1') {
			$jurusan_sekolah_set	= $this->master_model->get_jurusan_sekolah_list();
			$pendidikan_ortu_set	= $this->master_model->get_pendidikan_ortu_list();
			$pekerjaan_ortu_set		= $this->master_model->get_pekerjaan_ortu_list();

			// Load sekolah jika pilihan kota sekolah sudah ada
			if ($cms->ID_KOTA_SEKOLAH != NULL) {
				$sekolah_set = $this->master_model->get_sekolah_list($cms->ID_KOTA_SEKOLAH);
			}
		}

		if ($form_type == 's2' || $form_type == 's3') {
			// Load prodi minat jika id_pilihan_1 sudah terset
			if ($cmb->ID_PILIHAN_1 != NULL) {
				$prodi_minat_set	= $this->master_model->get_prodi_minat_list($cmb->ID_PENERIMAAN, $cmb->ID_PILIHAN_1);
				$kelompok_biaya_set	= $this->master_model->get_kelompok_biaya($cmb->ID_PENERIMAAN, $cmb->ID_PILIHAN_1);

				// tambah rule prodi minat, jika ada prodi minat harus di pilih
				$this->form_validation->set_rules('id_prodi_minat', 'Minat Prodi', (count($prodi_minat_set) > 0) ? 'required' : '');

				// re-run validation
				$this->form_validation->run();
			}
		}

		// Program studi set
		$program_studi_set		= $this->master_model->get_program_studi_list($cmb->PENERIMAAN->ID_PENERIMAAN, $cmb->KODE_JURUSAN);

		// assign all variabel
		$this->smarty->assignByRef('cmb', $cmb);
		$this->smarty->assignByRef('cms', $cms);
		$this->smarty->assignByRef('cmo', $cmo);
		$this->smarty->assignByRef('cmd', $cmd);
		$this->smarty->assignByRef('cmp', $cmp);
		$this->smarty->assignByRef('template', $template);
		$this->smarty->assignByRef('kota_set', $kota_set);
		$this->smarty->assignByRef('kewarganegaraan_set', $kewarganegaraan_set);
		$this->smarty->assignByRef('agama_set', $agama_set);
		$this->smarty->assignByRef('sumber_biaya_set', $sumber_biaya_set);
		$this->smarty->assignByRef('jurusan_sekolah_set', $jurusan_sekolah_set);
		$this->smarty->assignByRef('pendidikan_ortu_set', $pendidikan_ortu_set);
		$this->smarty->assignByRef('pekerjaan_ortu_set', $pekerjaan_ortu_set);
		$this->smarty->assignByRef('disabilitas_set', $disabilitas_set);
		$this->smarty->assignByRef('program_studi_set', $program_studi_set);
		$this->smarty->assignByRef('sekolah_set', $sekolah_set);
		$this->smarty->assignByRef('validation_complete', $validation_complete);
		$this->smarty->assignByRef('prodi_minat_set', $prodi_minat_set);
		$this->smarty->assignByRef('kelompok_biaya_set', $kelompok_biaya_set);

		// display form
		$this->smarty->display("form/{$template['template_file']}.tpl");
	}

	function reset_kode_jurusan()
	{
		$this->cmb_model->reset_jenis_formulir($this->id_c_mhs);

		redirect('form/isi');
	}

	function reset_form()
	{
		$this->cmb_model->reset_isi_form($this->id_c_mhs);

		redirect('form/isi');
	}

	function upload()
	{
		$this->load->library('upload');

		// 1 = server xampp
		// 2 = server /var/www/
		$server = 1;

		if ($server == 1) {
			$path = ".././pendaftaran_files";
		} elseif ($server == 2) {
			$path = "/var/www/html/pendaftaran_files";
		}

		$this->path_upload = $path;

		// load data semua CALON_MAHASISWA_*
		$cmb = $this->cmb_model->get_cmb($this->id_c_mhs);
		$cmf = $this->cmb_model->get_cmf($this->id_c_mhs);
		$cmd = $this->cmb_model->get_cmd($this->id_c_mhs);

		$this->smarty->assign('nama_pt', $this->nama_pt);

		$jadwal_penerimaan = $this->penerimaan_model->get_jadwal_penerimaan($cmb->ID_PENERIMAAN);
		$batasakhir_verifikasi = $this->cmb_model->get_batasakhir_verifikasi($cmb->ID_C_MHS);

		// dialihkan hanya jika belum diverifikasi
		if (strtotime($jadwal_penerimaan->TGL_AKHIR_VERIFIKASI) < time() && $cmb->TGL_VERIFIKASI_PPMB == '') {
			// apakah di perpanjang manual (calon_mahasiswa_baru->TGL_BATAS_VERIFIKASI)
			if (empty($batasakhir_verifikasi->TGL_BATASAKHIR_VERIFIKASI) or (strtotime($batasakhir_verifikasi->TGL_BATASAKHIR_VERIFIKASI) < time())) {
				$this->smarty->display('form/upload_jadwal_selesai.tpl');
				return;
			}
		}



		// Pengecekan tanggal verifikasi (sebagai batas upload)

		// Upload Initialization
		$this->upload->initialize(array(
			'upload_path'	=> "{$this->path_upload}/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/",  // format path: files/id_penerimaan/id_c_mhs/
			'allowed_types'	=> 'bmp|gif|jpg|png|pdf',
			'encrypt_name'	=> TRUE
		));

		$this->smarty->assignByRef('cmb', $cmb);
		$this->smarty->assignByRef('cmf', $cmf);
		$this->smarty->assignByRef('cmd', $cmd);

		// Belum di submit
		if (empty($cmb->TGL_REGISTRASI)) {
			$this->smarty->display('form/upload_belum_submit.tpl');
			exit();
		}

		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			// Persiapkan directory penerimaan jika belum ada
			if (!file_exists("{$this->path_upload}/{$cmb->ID_PENERIMAAN}")) {
				mkdir("{$this->path_upload}/{$cmb->ID_PENERIMAAN}", 0777, true);
			}

			// Persiapkan direktori id_c_mhs jika belum ada
			if (!file_exists("{$path}/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/")) {
				mkdir("{$this->path_upload}/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/", 0777, true);
			}

			// Jika upload gagal / salah file
			if (!$this->upload->do_upload('file')) {
				$this->smarty->assign('foto_error', $this->upload->display_errors(' ', ' '));
			} else {
				/**
				 * Dokumentasi file upload
				 * - File yang diupload berformat rasio panjang:lebar 4:6
				 * - jika width atau height lebih besar dari 1200px maka di resize menjadi 1200px dengan tidak mengubah
				 *   arah landscape atau portrait
				 * - Khusus untuk foto format di buat fix 480x720
				 */

				$upload_data = $this->upload->data();

				// echo '<pre>' . print_r($upload_data, true) . '</pre>'; exit();

				if ($this->input->post('file_mode') == 'foto') {
					// Format nama foto : foto_{encrypted}.jpg (wajib foto)
					if ($upload_data['is_image'] == 1) {

						// load ke Imagick object
						//echo $upload_data['full_path'];
						$img = new Imagick($upload_data['full_path']);

						/*echo "haha ".$upload_data['full_path'];
						exit();*/

						// Ukuran foto fixed 480x720 atau rasio 4x6
						//$img->resizeimage(480, 720, Imagick::FILTER_CUBIC, 0);
						$img->thumbnailImage(480, 720);
						$img->writeimage($upload_data['file_path'] . "foto_{$upload_data['raw_name']}.jpg");
						$img->clear();

						// delete file asli
						unlink($upload_data['full_path']);

						// tanpa verifikasi
						if ($cmb->PENERIMAAN->IS_VERIFIKASI == '0') {
							// update database
							$this->cmb_model->update_file_foto($this->id_c_mhs, "foto_{$upload_data['raw_name']}.jpg", null, 1, $upload_data['client_name']);
						} else {
							// update database
							$this->cmb_model->update_file_foto($this->id_c_mhs, "foto_{$upload_data['raw_name']}.jpg", null, null, $upload_data['client_name']);
						}
						// update database
						//$this->cmb_model->update_file_foto($this->id_c_mhs, "foto_{$upload_data['raw_name']}.jpg", null, null, $upload_data['client_name']);

						// REDIRECT, biar tidak bisa di refresh2
						redirect('form/upload');
						exit();
					} else {
						// delete file asli
						unlink($upload_data['full_path']);

						$this->smarty->assign('foto_error', 'File foto harus berformat gambar (png,jpg,bmp)');
					}
				}

				if ($this->input->post('file_mode') == 'file_syarat') {
					$id_syarat_prodi = $this->input->post('id_syarat_prodi');

					// Saat yang diupload gambar
					if ($upload_data['is_image'] == 1) {
						// create Imagick object
						$img = new Imagick($upload_data['full_path']);

						// cek rasio
						if ($img->getimagewidth() / $img->getimageheight() > (4.0 / 6.0)) {
							if ($img->getimagewidth() > 1200) {
								//$img->resizeimage(1200, 0, Imagick::FILTER_CUBIC, 0);
								$img->thumbnailImage(1200, 0);
							}
						} else {
							if ($img->getimageheight() > 1200) {
								//$img->resizeimage(0, 1200, Imagick::FILTER_CUBIC, 0);
								$img->thumbnailImage(0, 1200);
							}
						}

						$img->writeimage($upload_data['file_path'] . "{$id_syarat_prodi}_{$upload_data['raw_name']}.jpg");
						$img->clear();

						// delete file asli
						unlink($upload_data['full_path']);

						// try delete if exist
						$this->cmb_model->delete_syarat($this->id_c_mhs, $id_syarat_prodi);

						// tanpa verifikasi
						if ($cmb->PENERIMAAN->IS_VERIFIKASI == '0') {
							// re-insert data langsung valid
							$this->cmb_model->insert_syarat_langsungvalid($this->id_c_mhs, $id_syarat_prodi, "{$id_syarat_prodi}_{$upload_data['raw_name']}.jpg", $upload_data['client_name']);
						} else {
							// re-insert data
							$this->cmb_model->insert_syarat($this->id_c_mhs, $id_syarat_prodi, "{$id_syarat_prodi}_{$upload_data['raw_name']}.jpg", $upload_data['client_name']);
						}

						// re-insert data
						//$this->cmb_model->insert_syarat($this->id_c_mhs, $id_syarat_prodi, "{$id_syarat_prodi}_{$upload_data['raw_name']}.jpg", $upload_data['client_name']);
					} else // jika yang diupload pdf
						{
							// Cukup di rename
							rename($upload_data['full_path'], $upload_data['file_path'] . "{$id_syarat_prodi}_{$upload_data['raw_name']}{$upload_data['file_ext']}");

							// try delete if exist
							$this->cmb_model->delete_syarat($this->id_c_mhs, $id_syarat_prodi);

							// tanpa verifikasi
							if ($cmb->PENERIMAAN->IS_VERIFIKASI == '0') {
								// re-insert data langsung valid
								$this->cmb_model->insert_syarat_langsungvalid($this->id_c_mhs, $id_syarat_prodi, "{$id_syarat_prodi}_{$upload_data['raw_name']}.jpg", $upload_data['client_name']);
							} else {
								// re-insert data
								$this->cmb_model->insert_syarat($this->id_c_mhs, $id_syarat_prodi, "{$id_syarat_prodi}_{$upload_data['raw_name']}.jpg", $upload_data['client_name']);
							}

							// re-insert data
							//$this->cmb_model->insert_syarat($this->id_c_mhs, $id_syarat_prodi, "{$id_syarat_prodi}_{$upload_data['raw_name']}{$upload_data['file_ext']}", $upload_data['client_name']);
						}

					// REDIRECT, biar tidak bisa di refresh2
					redirect('form/upload');
					exit();
				}

				// ------------------------------------
				// Upload berkas sudah mengikuti master, 
				// sudah tidak digunakan lagi
				// -------------------------------------
				/*
				if ($this->input->post('file_mode') == 'berkas_pernyataan')
				{
					// cek rasio 4:6, 
					if ($img->getimagewidth() / $img->getimageheight() > (4.0 / 6.0))  
					{
						// saat landscape dan width lebih besar dari 1200 maka di resize
						if ($img->getimagewidth() > 1200)
						{
							$img->resizeimage(1200, 0, Imagick::FILTER_CUBIC, 0);
						}
					}
					else
					{
						// saat portrait dan height lebih besar dari 1200 maka di resize
						if ($img->getimageheight() > 1200)
						{
							$img->resizeimage(0, 1200, Imagick::FILTER_CUBIC, 0);
						}
					}

					$img->writeimage($upload_data['file_path'] . $this->id_c_mhs . '_berkas.jpg');

					// delete file asli
					unlink($upload_data['full_path']);

					// update database
					$this->cmb_model->update_file_berkas_pernyataan($this->id_c_mhs, $this->id_c_mhs . '_berkas.jpg', null, null, $upload_data['client_name']);
				}
				 */
			}

			// Saat di submit
			if ($this->input->post('file_mode') == 'submit_file') {
				// tanpa verifikasi
				if ($cmb->PENERIMAAN->IS_VERIFIKASI == '0') {
					$this->cmb_model->update_submit_verifikasi_baru($this->id_c_mhs, date('d-M-Y H:i:s'));
				} else {
					$this->cmb_model->update_submit_verifikasi($this->id_c_mhs, date('d-M-Y H:i:s'));
				}
				/*$this->cmb_model->update_submit_verifikasi($this->id_c_mhs, date('d-M-Y H:i:s'));*/
				$this->cmb_model->kosongi_pesan_verifikator($this->id_c_mhs);
				$this->cmb_model->count_masuk($this->id_c_mhs, 'tambah');
				redirect('form/upload');
				exit();
			}

			// Saat di cancel (untuk perbaikan)
			if ($this->input->post('file_mode') == 'cancel_file') {
				$this->cmb_model->update_submit_verifikasi($this->id_c_mhs, NULL);
				$this->cmb_model->count_masuk($this->id_c_mhs, 'kurang');
				redirect('form/upload');
				exit();
			}
		}

		$syarat_umum_set	= $this->cmb_model->get_list_syarat_umum($this->id_c_mhs, $cmb->ID_PENERIMAAN);
		$syarat_prodi_set	= $this->cmb_model->get_list_syarat_prodi($this->id_c_mhs, $cmb->ID_PENERIMAAN, $cmb->ID_PILIHAN_1, $cmb->ID_PILIHAN_2, $cmb->ID_PILIHAN_3, $cmb->ID_PILIHAN_4);

		$this->smarty->assignByRef('syarat_umum_set', $syarat_umum_set);
		$this->smarty->assignByRef('syarat_prodi_set', $syarat_prodi_set);

		/** Cek semua syarat  **/
		$is_all_syarat_ok	= $this->cmb_model->is_syarat_complete($cmf, $syarat_umum_set, $syarat_prodi_set);
		$is_syarat_verified	= $this->cmb_model->is_syarat_verified($cmf, $syarat_umum_set, $syarat_prodi_set);

		//echo "hahaha ".$is_all_syarat_ok;

		$this->smarty->assign('is_all_syarat_ok', $is_all_syarat_ok);
		$this->smarty->assign('is_syarat_verified', $is_syarat_verified);

		$this->smarty->assign('id_pt', $this->id_pt);

		$this->smarty->assign('is_verifikasi', $cmb->PENERIMAAN->IS_VERIFIKASI);

		//if ($cmb->ID_C_MHS == 153659)
		//	$this->smarty->display('form/upload-dev.tpl');
		//else
		$this->smarty->display('form/upload.tpl');
	}

	function del_upload($file)
	{
		if ($file == 'foto') {
			$cmb = $this->cmb_model->get_cmb($this->id_c_mhs);
			$cmf = $this->cmb_model->get_cmf($this->id_c_mhs);

			// remove file
			unlink("/web/files/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$cmf->FILE_FOTO}");

			$this->cmb_model->update_file_foto($this->id_c_mhs);
			$this->cmb_model->update_submit_verifikasi($this->id_c_mhs, NULL);
		}

		if ($file == 'berkas_pernyataan') {
			$this->cmb_model->update_file_berkas_pernyataan($this->id_c_mhs);
			$this->cmb_model->update_submit_verifikasi($this->id_c_mhs, NULL);
		}

		if (is_numeric($file)) // file syarat
			{
				$cmb = $this->cmb_model->get_cmb($this->id_c_mhs);
				$cm_syarat = $this->cmb_model->get_cm_syarat($this->id_c_mhs, $file);

				// remove file
				unlink("/web/files/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$cm_syarat->FILE_SYARAT}");

				$this->cmb_model->delete_syarat($this->id_c_mhs, $file);
				$this->cmb_model->update_submit_verifikasi($this->id_c_mhs, NULL);
			}

		redirect('form/upload');
	}

	function upload_m4nual_396()
	{
		$this->load->library('upload');

		// load data semua CALON_MAHASISWA_*
		$cmb = $this->cmb_model->get_cmb($this->id_c_mhs);
		$cmf = $this->cmb_model->get_cmf($this->id_c_mhs);
		$cmd = $this->cmb_model->get_cmd($this->id_c_mhs);

		$jadwal_penerimaan = $this->penerimaan_model->get_jadwal_penerimaan($cmb->ID_PENERIMAAN);
		$batasakhir_verifikasi = $this->cmb_model->get_batasakhir_verifikasi($cmb->ID_C_MHS);

		// dialihkan hanya jika belum diverifikasi
		/*
		if (strtotime($jadwal_penerimaan->TGL_AKHIR_VERIFIKASI) < time() && $cmb->TGL_VERIFIKASI_PPMB == '' )
		{
			// apakah di perpanjang manual (calon_mahasiswa_baru->TGL_BATAS_VERIFIKASI)
			if( empty($batasakhir_verifikasi->TGL_BATASAKHIR_VERIFIKASI) or (strtotime($batasakhir_verifikasi->TGL_BATASAKHIR_VERIFIKASI) < time()) ) {
				$this->smarty->display('form/upload_jadwal_selesai.tpl');
				return;
			}
		}
		*/



		// Pengecekan tanggal verifikasi (sebagai batas upload)

		// Upload Initialization
		$this->upload->initialize(array(
			'upload_path'	=> "/web/files/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/",  // format path: files/id_penerimaan/id_c_mhs/
			'allowed_types'	=> 'bmp|gif|jpg|png|pdf',
			'encrypt_name'	=> TRUE
		));

		$this->smarty->assignByRef('cmb', $cmb);
		$this->smarty->assignByRef('cmf', $cmf);
		$this->smarty->assignByRef('cmd', $cmd);

		// Belum di submit
		if (empty($cmb->TGL_REGISTRASI)) {
			$this->smarty->display('form/upload_belum_submit.tpl');
			exit();
		}

		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			// Persiapkan directory penerimaan jika belum ada
			if (!file_exists("/web/files/{$cmb->ID_PENERIMAAN}")) {
				mkdir("/web/files/{$cmb->ID_PENERIMAAN}", 0777, true);
			}

			// Persiapkan direktori id_c_mhs jika belum ada
			if (!file_exists("/web/files/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/")) {
				mkdir("/web/files/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/", 0777, true);
			}

			// Jika upload gagal / salah file
			if (!$this->upload->do_upload('file')) {
				$this->smarty->assign('foto_error', $this->upload->display_errors(' ', ' '));
			} else {
				/**
				 * Dokumentasi file upload
				 * - File yang diupload berformat rasio panjang:lebar 4:6
				 * - jika width atau height lebih besar dari 1200px maka di resize menjadi 1200px dengan tidak mengubah
				 *   arah landscape atau portrait
				 * - Khusus untuk foto format di buat fix 480x720
				 */

				$upload_data = $this->upload->data();

				// echo '<pre>' . print_r($upload_data, true) . '</pre>'; exit();

				if ($this->input->post('file_mode') == 'foto') {
					// Format nama foto : foto_{encrypted}.jpg (wajib foto)
					if ($upload_data['is_image'] == 1) {
						// load ke Imagick object
						//echo $upload_data['full_path'];
						$img = new Imagick($upload_data['full_path']);

						// Ukuran foto fixed 480x720 atau rasio 4x6
						//$img->resizeimage(480, 720, Imagick::FILTER_CUBIC, 0);
						$img->thumbnailImage(480, 720);
						$img->writeimage($upload_data['file_path'] . "foto_{$upload_data['raw_name']}.jpg");
						$img->clear();

						// delete file asli
						unlink($upload_data['full_path']);

						// update database
						$this->cmb_model->update_file_foto($this->id_c_mhs, "foto_{$upload_data['raw_name']}.jpg", null, null, $upload_data['client_name']);

						// REDIRECT, biar tidak bisa di refresh2
						redirect('form/upload');
						exit();
					} else {
						// delete file asli
						unlink($upload_data['full_path']);

						$this->smarty->assign('foto_error', 'File foto harus berformat gambar (png,jpg,bmp)');
					}
				}

				if ($this->input->post('file_mode') == 'file_syarat') {
					$id_syarat_prodi = $this->input->post('id_syarat_prodi');

					// Saat yang diupload gambar
					if ($upload_data['is_image'] == 1) {
						// create Imagick object
						$img = new Imagick($upload_data['full_path']);

						// cek rasio
						if ($img->getimagewidth() / $img->getimageheight() > (4.0 / 6.0)) {
							if ($img->getimagewidth() > 1200) {
								//$img->resizeimage(1200, 0, Imagick::FILTER_CUBIC, 0);
								$img->thumbnailImage(1200, 0);
							}
						} else {
							if ($img->getimageheight() > 1200) {
								//$img->resizeimage(0, 1200, Imagick::FILTER_CUBIC, 0);
								$img->thumbnailImage(0, 1200);
							}
						}

						$img->writeimage($upload_data['file_path'] . "{$id_syarat_prodi}_{$upload_data['raw_name']}.jpg");
						$img->clear();

						// delete file asli
						unlink($upload_data['full_path']);

						// try delete if exist
						$this->cmb_model->delete_syarat($this->id_c_mhs, $id_syarat_prodi);

						// re-insert data
						$this->cmb_model->insert_syarat_langsungvalid($this->id_c_mhs, $id_syarat_prodi, "{$id_syarat_prodi}_{$upload_data['raw_name']}.jpg", $upload_data['client_name']);
					} else // jika yang diupload pdf
						{
							// Cukup di rename
							rename($upload_data['full_path'], $upload_data['file_path'] . "{$id_syarat_prodi}_{$upload_data['raw_name']}{$upload_data['file_ext']}");

							// try delete if exist
							$this->cmb_model->delete_syarat($this->id_c_mhs, $id_syarat_prodi);

							// re-insert data
							$this->cmb_model->insert_syarat_langsungvalid($this->id_c_mhs, $id_syarat_prodi, "{$id_syarat_prodi}_{$upload_data['raw_name']}{$upload_data['file_ext']}", $upload_data['client_name']);
						}

					// REDIRECT, biar tidak bisa di refresh2
					redirect('form/upload_m4nual_396');
					exit();
				}

				// ------------------------------------
				// Upload berkas sudah mengikuti master, 
				// sudah tidak digunakan lagi
				// -------------------------------------
				/*
				if ($this->input->post('file_mode') == 'berkas_pernyataan')
				{
					// cek rasio 4:6, 
					if ($img->getimagewidth() / $img->getimageheight() > (4.0 / 6.0))  
					{
						// saat landscape dan width lebih besar dari 1200 maka di resize
						if ($img->getimagewidth() > 1200)
						{
							$img->resizeimage(1200, 0, Imagick::FILTER_CUBIC, 0);
						}
					}
					else
					{
						// saat portrait dan height lebih besar dari 1200 maka di resize
						if ($img->getimageheight() > 1200)
						{
							$img->resizeimage(0, 1200, Imagick::FILTER_CUBIC, 0);
						}
					}

					$img->writeimage($upload_data['file_path'] . $this->id_c_mhs . '_berkas.jpg');

					// delete file asli
					unlink($upload_data['full_path']);

					// update database
					$this->cmb_model->update_file_berkas_pernyataan($this->id_c_mhs, $this->id_c_mhs . '_berkas.jpg', null, null, $upload_data['client_name']);
				}
				 */
			}

			// Saat di submit
			if ($this->input->post('file_mode') == 'submit_file') {
				$this->cmb_model->update_submit_verifikasi($this->id_c_mhs, date('d-M-Y H:i:s'));
				$this->cmb_model->kosongi_pesan_verifikator($this->id_c_mhs);
				$this->cmb_model->count_masuk($this->id_c_mhs, 'tambah');
				redirect('form/upload_m4nual_396');
				exit();
			}

			// Saat di cancel (untuk perbaikan)
			if ($this->input->post('file_mode') == 'cancel_file') {
				$this->cmb_model->update_submit_verifikasi($this->id_c_mhs, NULL);
				$this->cmb_model->count_masuk($this->id_c_mhs, 'kurang');
				redirect('form/upload_m4nual_396');
				exit();
			}
		}

		$syarat_umum_set	= $this->cmb_model->get_list_syarat_umum($this->id_c_mhs, $cmb->ID_PENERIMAAN);
		$syarat_prodi_set	= $this->cmb_model->get_list_syarat_prodi($this->id_c_mhs, $cmb->ID_PENERIMAAN, $cmb->ID_PILIHAN_1, $cmb->ID_PILIHAN_2, $cmb->ID_PILIHAN_3, $cmb->ID_PILIHAN_4);

		$this->smarty->assignByRef('syarat_umum_set', $syarat_umum_set);
		$this->smarty->assignByRef('syarat_prodi_set', $syarat_prodi_set);

		/** Cek semua syarat  **/
		$is_all_syarat_ok	= $this->cmb_model->is_syarat_complete($cmf, $syarat_umum_set, $syarat_prodi_set);
		$is_syarat_verified	= $this->cmb_model->is_syarat_verified($cmf, $syarat_umum_set, $syarat_prodi_set);
		$this->smarty->assign('is_all_syarat_ok', $is_all_syarat_ok);
		$this->smarty->assign('is_syarat_verified', $is_syarat_verified);

		//if ($cmb->ID_C_MHS == 153659)
		//	$this->smarty->display('form/upload-dev.tpl');
		//else
		$this->smarty->display('form/upload_manual.tpl');
	}
}
