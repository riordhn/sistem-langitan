<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property Cmb_Model $cmb_model 
 * @property Penerimaan_Model $penerimaan_model 
 */
class Front extends MY_Controller
{
	public function index()
	{	
		
		if ($this->session->userdata('is_loggedin') == TRUE)
		{
			redirect('form/');
			exit();
		}

		$id_pt = $this->perguruan_tinggi['ID_PERGURUAN_TINGGI'];
		$nama_pt = $this->perguruan_tinggi['NAMA_PERGURUAN_TINGGI'];
		$telp_pt = $this->perguruan_tinggi['TELP_PERGURUAN_TINGGI'];
		$email_pmb = $this->perguruan_tinggi['EMAIL_PMB'];
		$web_pt = "http://www.".strtolower($this->perguruan_tinggi['NAMA_SINGKAT']).".ac.id";
		$alamat_pt = $this->perguruan_tinggi['ALAMAT_PERGURUAN_TINGGI'];

		// Mendapatkan row INFORMASI
		$this->query = $this->db->get_where('informasi_pmb', array('id_perguruan_tinggi' => $id_pt, 'is_aktif' => 1));
		$informasi = $this->query->row();

		if(!empty($informasi->ISI_INFORMASI)){
			$isi_informasi = $informasi->ISI_INFORMASI;
		}
		else{
			$isi_informasi = "";
		}

		$this->smarty->assign('isi_informasi', $isi_informasi);

		$this->smarty->assign('id_pt', $id_pt);
		$this->smarty->assign('nama_pt', $nama_pt);
		$this->smarty->assign('telp_pt', $telp_pt);
		$this->smarty->assign('email_pmb', $email_pmb);
		// kebutuhan khusus STIKE NJ PAITON
		if($id_pt == 28){
			$this->smarty->assign('web_pt', "http://www.stikesnjpaiton.ac.id");
		}
		else{
			$this->smarty->assign('web_pt', $web_pt);
		}
		$this->smarty->assign('alamat_pt', $alamat_pt);
		
		$this->smarty->display('front/index.tpl');
	}
	
	public function login_manual()
	{
		// Loader
		$this->load->database();
		$this->load->library('form_validation');
		$this->load->helper('widget');
		
		/*$this->smarty->display('front/index-manual.tpl');*/
		$this->smarty->display('front/index.tpl');
	}
	
	public function informasi()
	{

		$id_pt = $this->perguruan_tinggi['ID_PERGURUAN_TINGGI'];
		$nama_pt = $this->perguruan_tinggi['NAMA_PERGURUAN_TINGGI'];
		$telp_pt = $this->perguruan_tinggi['TELP_PERGURUAN_TINGGI'];
		$email_pmb = $this->perguruan_tinggi['EMAIL_PMB'];
		$web_pt = "http://www.".strtolower($this->perguruan_tinggi['NAMA_SINGKAT']).".ac.id";
		$alamat_pt = $this->perguruan_tinggi['ALAMAT_PERGURUAN_TINGGI'];

		// Mendapatkan row INFORMASI
		$this->query = $this->db->get_where('informasi_pmb', array('id_perguruan_tinggi' => $id_pt, 'is_aktif' => 1));
		$informasi = $this->query->row();

		$isi_informasi = $informasi->ISI_INFORMASI;

		$this->smarty->assign('isi_informasi', $isi_informasi);

		$this->smarty->assign('id_pt', $id_pt);
		$this->smarty->assign('nama_pt', $nama_pt);
		$this->smarty->assign('telp_pt', $telp_pt);
		$this->smarty->assign('email_pmb', $email_pmb);
		$this->smarty->assign('web_pt', $web_pt);
		$this->smarty->assign('alamat_pt', $alamat_pt);

		$this->smarty->display('front/informasi.tpl');
	}
	
	public function maintenance()
	{
		$this->smarty->display('front/maintenance.tpl');
	}

	public function pengumuman()
	{
		$id_pt = $this->perguruan_tinggi['ID_PERGURUAN_TINGGI'];
		$nama_pt = $this->perguruan_tinggi['NAMA_PERGURUAN_TINGGI'];
		$telp_pt = $this->perguruan_tinggi['TELP_PERGURUAN_TINGGI'];
		$email_pmb = $this->perguruan_tinggi['EMAIL_PMB'];
		$web_pt = "http://www.".strtolower($this->perguruan_tinggi['NAMA_SINGKAT']).".ac.id";
		$alamat_pt = $this->perguruan_tinggi['ALAMAT_PERGURUAN_TINGGI'];

		$this->smarty->assign('id_pt', $id_pt);
		$this->smarty->assign('nama_pt', $nama_pt);
		$this->smarty->assign('telp_pt', $telp_pt);
		$this->smarty->assign('email_pmb', $email_pmb);
		$this->smarty->assign('web_pt', $web_pt);
		$this->smarty->assign('alamat_pt', $alamat_pt);
		
		$kode_voucher = $this->input->get('voucher', true);
		$result = '';
		
		if ($kode_voucher != '') {
			$this->load->model('Cmb_model', 'cmb_model');
			$this->load->model('Penerimaan_model', 'penerimaan_model');
			
			$id_c_mhs = $this->cmb_model->get_id_cmhs_by_voucher($kode_voucher, $id_pt);
			
			if ($id_c_mhs != NULL) {
				
				$cmb = $this->cmb_model->get_cmb($id_c_mhs);
				
				// Jika non aktif dianggap voucher tidak ditemukan
				if ($cmb->PENERIMAAN->IS_AKTIF == 0) {
					$result = 'VOUCHER_NOT_FOUND';
				}
				else {
					
					$jadwal_penerimaan = $this->penerimaan_model->get_jadwal_penerimaan($cmb->ID_PENERIMAAN);
					
					//echo '<pre>' . print_r($jadwal_penerimaan, true) . '</pre>'; exit();
					
					// Cek Jadwal pengumuman terhadap waktu sekarang
					if (strtotime($jadwal_penerimaan->TGL_PENGUMUMAN) > strtotime('now')) {
						$result = 'BELUM_PENGUMUMAN';
						$this->smarty->assign('jadwal_penerimaan', $jadwal_penerimaan);
					}
					else {
						// Cek Kondisi diterima
						if ($cmb->ID_PROGRAM_STUDI != NULL) {
							
							$cmb->PROGRAM_STUDI = $this->db->select('j.nm_jenjang, ps.nm_program_studi')
								->from('program_studi ps')
								->join('jenjang j', 'j.id_jenjang = ps.id_jenjang')
								->where('ps.id_program_studi', $cmb->ID_PROGRAM_STUDI)
								->get()->row();
							
							$result = 'DITERIMA';
						}
						else {
							$result = 'TIDAK_DITERIMA';
						}
						
						$this->smarty->assign('cmb', $cmb);
					}
				}
			}
			else {
				$result = 'VOUCHER_NOT_FOUND';
			}
		}
		
		$this->smarty->assign('result', $result);
		$this->smarty->display('front/pengumuman.tpl');
	}
}