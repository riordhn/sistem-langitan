<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reg extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		$this->check_credentials();
	}
	
	function index()
	{
		$calon_pendaftar = $this->session->userdata('calon_pendaftar');
		
		$this->smarty->assign('nama_pendaftar', $calon_pendaftar->NAMA_PENDAFTAR);
		$this->smarty->display('reg/index.tpl');
	}
	
	function list_penerimaan()
	{
		$this->load->model('Penerimaan');
		$this->smarty->assignByRef('penerimaan_set', $this->Penerimaan->list_all());
		$this->smarty->display('reg/list_penerimaan.tpl');
	}
	
	function pilih_penerimaan($id_penerimaan)
	{
		/*
		 * Proses
		 * 1. Insert CALON_MAHASISWA_BARU : ID_PENERIMAAN, ID_CALON_PENDAFTAR, VOUCHER=KOSONG, NO_UJIAN=KOSONG
		 * 2. List penerimaan langsung diganti satu yg lain tidak tampil
		 */
	}
}
