<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property Penerimaan $Penerimaan Description
 * @property Calon_Pendaftar_Model $calon_pendaftar_model Description
 * @property Cmb_Model $cmb_model Description
 */
class Home extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		$this->check_credentials();
	}
	
	function index()
	{
		$this->load->model('Calon_Pendaftar_Model', 'calon_pendaftar_model');
		$this->load->model('Cmb_Model', 'cmb_model');
		
		// set local indonesia
		setlocale(LC_TIME, 'id_ID');
		
		$calon_pendaftar	= $this->session->userdata('calon_pendaftar');
		$id_c_mhs			= $calon_pendaftar->ID_C_MHS_AKTIF;
		
		$cmb				= $this->cmb_model->get_cmb($id_c_mhs);
		$cmf				= $this->cmb_model->get_cmf($id_c_mhs);
		
		$is_c_mhs_exist = ( ! empty($id_c_mhs));
		
		if ($is_c_mhs_exist)
		{
			$syarat_umum_set	= $this->cmb_model->get_list_syarat_prodi($id_c_mhs, $cmb->ID_PENERIMAAN);
			$syarat_prodi_set	= $this->cmb_model->get_list_syarat_prodi($id_c_mhs, $cmb->ID_PENERIMAAN, $cmb->ID_PILIHAN_1, $cmb->ID_PILIHAN_2, $cmb->ID_PILIHAN_3, $cmb->ID_PILIHAN_4);
		}

		// 1. Memilih jenjang seleksi
		$step_1	= $is_c_mhs_exist;
		
		// 2. Submit formulir
		$step_2 = ($is_c_mhs_exist) ? ($cmb->TGL_REGISTRASI != NULL) : FALSE;
		
		// 3. Upload berkas lengkap dan urutan antrian
		$step_3	= ($is_c_mhs_exist) ? $this->cmb_model->is_syarat_complete($cmf, $syarat_umum_set, $syarat_prodi_set) : FALSE;
		$step_3_antrian = ($is_c_mhs_exist) ? number_format($this->cmb_model->ambil_antrian($id_c_mhs, $cmb->ID_PENERIMAAN), 0, ',', '.') : FALSE;
		
		// 4. Bayar Voucher
		$step_4 = ($is_c_mhs_exist) ? ($cmb->VOUCHER->TGL_BAYAR != NULL) : FALSE;
		
		// 5. Isi Kuisioner, di cek hanya jika ID_FORM_KUISIONER-nya tidak kosong.
		$step_5_enabled = ($cmb->PENERIMAAN->ID_FORM_KUISIONER != NULL); 
		$step_5 = ($is_c_mhs_exist) ? ($cmb->IS_ISI_KUISIONER == 1) : FALSE;
		
		// 6. Ambil kartu ujian / Cetak
		$step_6 = ($is_c_mhs_exist) ? ($cmb->NO_UJIAN != NULL) : FALSE;
		
		// 7. Ujian seleksi
		
		// 8. Pengumuman
		$tgl_pengumuman = ($is_c_mhs_exist) ? ($cmb->PENERIMAAN->TGL_PENGUMUMAN) : FALSE;
		$tgl_pengumuman = ($is_c_mhs_exist) ? strftime('%d %B %Y', date_timestamp_get(date_create_from_format('j-M-y', $tgl_pengumuman))) : FALSE;
		
		// 9. Pendaftaran Ulang

		$steps = array(
			'1' => $step_1,
			'2' => $step_2,
			'3'	=> $step_3, '3_antrian' => $step_3_antrian,
			'4' => $step_4,
			'5' => $step_5, '5_enabled' => $step_5_enabled,
			'6' => $step_6,
			'7' => FALSE,
			'tgl_pengumuman' => $tgl_pengumuman
		);

		$this->smarty->assignByRef('cmb', $cmb);
		$this->smarty->assign('steps', $steps);
		
		$this->smarty->assign('nama_pendaftar', $calon_pendaftar->NAMA_PENDAFTAR);
		$this->smarty->display('home/index.tpl');
	}

	function tes()
	{
		$this->smarty->display('home/index.tpl');
	}

}
