<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of verifikasi
 *
 * @author Fathoni <fathoni@staf.unair.ac.id>
 * @property Cmb_Model $cmb_model Description
 * @property CI_Tcpdf $tcpdf Description
 * @property int $id_c_mhs Description
 * @property Kuisioner_Model $kuisioner_model Description
 */
class Verifikasi extends MY_Controller
{
	private $id_c_mhs;
	
	function __construct()
	{
		parent::__construct();
		
		$this->check_credentials();
		$this->load->model('Cmb_Model', 'cmb_model');
		
		$calon_pendaftar		= $this->session->userdata('calon_pendaftar');
		$this->id_c_mhs			= $calon_pendaftar->ID_C_MHS_AKTIF;
		
		// jika belum punya data calon di redirect
		if (empty($this->id_c_mhs))
		{
			$this->smarty->display('form/pilih_penerimaan.tpl');
			exit();
		}
	}
	
	function hasil()
	{
		// load data semua CALON_MAHASISWA_*
		$cmb = $this->cmb_model->get_cmb($this->id_c_mhs);
		$cmf = $this->cmb_model->get_cmf($this->id_c_mhs);
		
		$this->smarty->assignByRef('cmb', $cmb);
		
		// Load variabel syarat-syarat
		$syarat_umum_set	= $this->cmb_model->get_list_syarat_umum($this->id_c_mhs, $cmb->ID_PENERIMAAN);
		$syarat_prodi_set	= $this->cmb_model->get_list_syarat_prodi($this->id_c_mhs, $cmb->ID_PENERIMAAN, $cmb->ID_PILIHAN_1, $cmb->ID_PILIHAN_2, $cmb->ID_PILIHAN_3, $cmb->ID_PILIHAN_4);
		$is_all_syarat_ok	= $this->cmb_model->is_syarat_complete($cmf, $syarat_umum_set, $syarat_prodi_set);
		$is_syarat_verified	= $this->cmb_model->is_syarat_verified($cmf, $syarat_umum_set, $syarat_prodi_set);
		
		$this->smarty->assign('is_all_syarat_ok', $is_all_syarat_ok);
		$this->smarty->assign('is_syarat_verified', $is_syarat_verified);
		$this->smarty->display('verifikasi/hasil.tpl');
	}
	
	function voucher()
	{
		// load data semua CALON_MAHASISWA_*
		$cmb = $this->cmb_model->get_cmb($this->id_c_mhs);
		
		$this->smarty->assignByRef('cmb', $cmb);
		
		$this->smarty->display('verifikasi/voucher.tpl');
	}
	
	function kartu_ujian()
	{
		// load data semua CALON_MAHASISWA_*
		$cmb = $this->cmb_model->get_cmb($this->id_c_mhs);
		
		// Jika ada kuisioner, dan belum mengisi
		if ($cmb->PENERIMAAN->ID_FORM_KUISIONER != '')
		{
			if ($cmb->IS_ISI_KUISIONER == 0)
			{
				redirect('kuisioner');
				exit();
			}
		}
		
		if ($this->input->server('REQUEST_METHOD') == 'POST')
		{
			$pin_voucher = $this->input->post('pin_voucher');
			
			if ($pin_voucher != $cmb->PIN_VOUCHER)
			{
				$this->smarty->assign('title', 'Pin tidak sesuai !');
				$this->smarty->assign('message', 'Harap periksa kembali kode pin yang diperolah setelah pembayaran voucher di bank');
				$this->smarty->assignByRef('cmb', $cmb);
				$this->smarty->display('verifikasi/kartu_ujian_error.tpl');
				exit();
			}
			else if ($cmb->TGL_INPUT_PIN == '')
			{
				// Update tanggal input pin
				$this->cmb_model->update_tgl_input_pin($this->id_c_mhs);
				
				// preventing browser refresh
				redirect('verifikasi/kartu_ujian');
				exit();
			}
		}
		
		$this->smarty->assignByRef('cmb', $cmb);
		$this->smarty->display('verifikasi/kartu_ujian.tpl');
	}
	
	function cetak_kartu()
	{
		$perguruan_tinggi	= $this->session->userdata('perguruan_tinggi');
		
		$this->load->library('tcpdf', array(
			'unicode'		=> false,
			'encoding'		=> 'ISO-8859-1',
			'pdfa'			=> true
		));
		
		$this->load->helper('kartu_ujian');
		
		// load data semua CALON_MAHASISWA_*
		$cmb		= $this->cmb_model->get_cmb($this->id_c_mhs);
		$cmb->cmf	= $this->cmb_model->get_cmf($this->id_c_mhs);

		if ($cmb->KODE_VOUCHER != '' && $cmb->NO_UJIAN != '')
		{
			// Tambahan var PT nambi
			generate_kartu_ujian($this->tcpdf, $cmb,$perguruan_tinggi);
		}
	}
}
