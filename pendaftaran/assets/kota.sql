select
  '{' ||
  '"data":"'||k.id_kota||'",'||
  '"value":"'||initcap(nm_kota || case tipe_dati2 when 'KABUPATEN' then ' (Kab.)' end || ', ' || nm_provinsi || ', ' || nm_negara)||'"'||
  '},'
  as total_name
from kota k
join provinsi p on p.id_provinsi = k.id_provinsi
join negara n on n.id_negara = p.id_negara and n.id_negara = 114;