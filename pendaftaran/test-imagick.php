<?php
if( class_exists("Imagick") )
{
    //Imagick is installed
    $image = new Imagick('assets/logo_unair.jpg');
	$image->resizeImage(320,240,Imagick::FILTER_CUBIC,1);
	$image->writeImage("assets/logo_unair-imagick.jpg");
	$image->destroy();
}
else
{
	echo "Imagick not Installed";
}