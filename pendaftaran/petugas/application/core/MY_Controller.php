<?php

/**
 * Description of controller
 *
 * @author Fathoni <fathoni@staf.unair.ac.id>
 * @property CI_Smarty $smarty
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Loader $load
 * @property CI_DB_active_record $db
 * @property CI_DB_oci8_result $query
 * @property CI_Email $email
 * @property CI_Session $session
 * @property CI_Upload $upload
 * @property CI_Output $output
 * @property CI_URI $uri
 */
class MY_Controller extends CI_Controller {
	//put your code here
	
	function __construct()
	{
		parent::__construct();
		
		// Library loader
		$this->load->database();
		$this->load->library('form_validation');
		$this->load->library('email');
		$this->load->library('session');
		$this->load->library('upload');
		
		// Email Initialization
		$email_config['protocol'] = 'sendmail';
		$email_config['mailpath'] = '/usr/sbin/sendmail';
		$email_config['charset'] = 'iso-8859-1';
		$email_config['wordwrap'] = TRUE;
		
		$this->email->initialize($email_config);
		
		// Upload Initialization
		$upload_config['upload_path'] = './files/';
		$upload_config['allowed_types'] = 'bmp|gif|jpg|png';
		$upload_config['encrypt_name']	= TRUE;
		
		$this->upload->initialize($upload_config);
		
		// Session initialization
		if ($this->session->userdata('is_loggedin') == '')
		{
			$this->session->set_userdata('is_loggedin', FALSE);
		}
		
		if($this->session->userdata('perguruan_tinggi') == ''){
			$data_set = $this->db->query("SELECT ID_PERGURUAN_TINGGI, NAMA_PERGURUAN_TINGGI, NAMA_SINGKAT, HTTP_HOST, KOTA_PERGURUAN_TINGGI,
														ALAMAT_PERGURUAN_TINGGI, TELP_PERGURUAN_TINGGI, FAX_PERGURUAN_TINGGI, WEB_EMAIL, 
														WEB_ELEARNING, EMAIL_PMB, WEB_PMB 
														FROM PERGURUAN_TINGGI WHERE WEB_PMB = '{$_SERVER['HTTP_HOST']}'")->result_array();

			$this->session->set_userdata('perguruan_tinggi', $data_set[0]);
		}	

		//print_r($this->langitan_set);
		$this->perguruan_tinggi = $this->session->userdata('perguruan_tinggi');
		
		// disable cache page
		header('Expires: Sun, 01 Jan 2000 00:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache');
	}
	
	/**
	 * Mendeteksi credential
	 */
	function check_credentials()
	{
		if ($this->session->userdata('is_loggedin') == FALSE)
		{
			redirect(site_url());
		}
	}
}
