<?php

if ( ! function_exists('generate_kartu_ujian'))
{
	/**
	 * Menggenerate kartu ujian
	 * @param TCPDF $tcpdf
	 * @param Object $cmb	 Description
	 */
	function generate_kartu_ujian(TCPDF &$pdf, &$cmb)
	{
		// document identity
		$pdf->setPDFVersion('1.4');
		$pdf->SetCreator('Sistem Penerimaan Mahasiswa Baru Universitas Airlangga');
		$pdf->SetAuthor('PPMB Universitas Airlangga');
		$pdf->SetTitle('Tanda Peserta Ujian');
		$pdf->SetSubject($cmb->NO_UJIAN . ' - ' . $cmb->NM_C_MHS);
		$pdf->SetKeywords('PPMB, Universitas Airlangga, Mandiri, Penerimaan, Pendaftaran, Kartu Ujian');
		
		// re-Setting Page
		$pdf->setPrintHeader(FALSE);
		$pdf->setPrintFooter(FALSE);
		$pdf->SetMargins(10, 10, 10);  // Margin 1 cm
		
		// ======================================================
		// MULAI GAMBAR KARTU
		// ======================================================
		$pdf->AddPage();
		
		// Gambar kotak
		$pdf->SetLineWidth(0.5);
		$pdf->Rect(10, 10, $pdf->getPageWidth() - 20, 125);
		$pdf->Line(10, 33, $pdf->getPageWidth() - 10, 33);
		$pdf->Line(10, 40, $pdf->getPageWidth() - 10, 40);
		$pdf->Rect($pdf->getPageWidth() - 40, 45, 25, 25);
		$pdf->Line($pdf->getPageWidth() - 40, 63, $pdf->getPageWidth() - 15, 63);
		
		$debug_pdf = FALSE;
		$border_debug = array(
			'LTRB' => array('width' => 0.1, 'dash' => '5,5', 'color' => array(225, 225, 255)),
		);
		$barcode_style = array(
			'position' => '',
			'align' => 'R',
			'stretch' => false,
			'fitwidth' => true,
			'cellfitalign' => '',
			'border' => FALSE,
			'hpadding' => 'auto',
			'vpadding' => 'auto',
			'fgcolor' => array(0,0,0),
			'bgcolor' => false, //array(255,255,255),
			'text' => false,
			'font' => 'helvetica',
			'fontsize' => 8,
			'stretchtext' => 4
		);
		
		
		// ------------------
		// Judul Kartu & Logo
		// ------------------
		$pdf->Image('@' . file_get_contents('assets/logo_unair.jpg'), 25, 11, 20, 20, 'jpg');
		$pdf->SetFont('times', '', 16);
		$pdf->SetXY(30, 13);
		$pdf->Cell(0, 0, 'PUSAT PENERIMAAN MAHASISWA BARU', $debug_pdf ? $border_debug : 0, 2, 'C');
		$pdf->SetFont('times', 'B', 20);
		$pdf->Cell(0, 0, 'UNIVERSITAS AIRLANGGA', $debug_pdf ? $border_debug : 0, 1, 'C');
		$pdf->SetFont('times', '', 14);
		$pdf->SetY(33, true);
		$pdf->Cell(0, 0, ' TANDA PESERTA UJIAN', $debug_pdf ? $border_debug : 0, 1, 'L');
		$pdf->SetXY(120, 32);
		$pdf->write1DBarcode($cmb->NO_UJIAN, 'C39', '', '', '', 9, 0.4, $barcode_style, 'N');
		
		// -----------------------
		// Foto Peserta & No Ujian
		// -----------------------
		$pdf->Image('@' . file_get_contents("/web/files/{$cmb->ID_C_MHS}_foto.jpg"), 15, 45, 45, 60, 'jpg', '', '', false, 300, '', false, false, 1);
		$pdf->SetXY(15, 105);
		$pdf->SetFont('helvetica', 'B', 16);
		$pdf->Cell(45, 0, $cmb->NO_UJIAN, 1, 2, 'C');
		
		
		// --------------------
		// Kode Jurusan & Jalur
		// --------------------
		if ($cmb->KODE_JURUSAN == '01')
			$kode_jurusan = 'IPA';
		else if ($cmb->KODE_JURUSAN == '02')
			$kode_jurusan = 'IPS';
		else if ($cmb->KODE_JURUSAN == '03')
			$kode_jurusan = 'IPC';
		
		if ($cmb->PENERIMAAN->ID_JALUR == 3)
			$jalur = 'MANDIRI';
		else if ($cmb->PENERIMAAN->ID_JALUR == 4)
			$jalur = 'ALIH JENIS';
		else if ($cmb->PENERIMAAN->ID_JALUR == 5)
			$jalur = 'DIPLOMA 3';
		else if ($cmb->PENERIMAAN->ID_JALUR == 27)
			$jalur = 'DEPAG';
		else
			$jalur = 'MANDIRI';

		$pdf->SetFont('helvetica', 'B', 36);
		$pdf->SetXY($pdf->getPageWidth() - 40, 45);
		$pdf->Cell(25, 19, $kode_jurusan, $debug_pdf ? $border_debug : 0, 2, 'C');
		$pdf->SetFont('times', '', 12);
		$pdf->Cell(25, 0, $jalur, $debug_pdf ? $border_debug : 0, 1, 'C');
		
		// -----------------
		// Materi Test
		// -----------------
		setlocale(LC_TIME, 'id_ID');
		$cmb->JADWAL_PPMB->TGL_TEST = strftime('%A, %d %B %Y', strtotime($cmb->JADWAL_PPMB->TGL_TEST));
		
		if ($cmb->KODE_JURUSAN == '01')
		{
			$materi_test_1 = "  08.00 - 08.50 : Tes Potensi Akademik (TPA)";
			$materi_test_2 = "  08.50 - 10.05 : Tes Prestasi Akademik (IPA)";
			$materi_test_3 = "";
		}
		else if ($cmb->KODE_JURUSAN == '02')
		{
			$materi_test_1 = "  08.00 - 08.50 : Tes Potensi Akademik (TPA)";
			$materi_test_2 = "  08.50 - 10.05 : Tes Prestasi Akademik (IPS)";
			$materi_test_3 = "";
		}
		else if ($cmb->KODE_JURUSAN == '03')
		{
			$materi_test_1 = "  08.00 - 08.50 : Tes Potensi Akademik";
			$materi_test_2 = "  08.50 - 10.05 : Tes Prestasi Akademik (IPA)";
			$materi_test_3 = "  10.05 - 11.05 : Tes Prestasi Akademik (IPS)";
		}
		
		// -----------------
		// Biodata
		// -----------------
		$pdf->SetXY(62, 45);
		$pdf->SetFont('helvetica', '', 11);
		$pdf->Cell(0, 0, 'Voucher', $debug_pdf ? $border_debug : 0, 2, 'L');
		$pdf->Cell(0, 0, 'No Peserta', $debug_pdf ? $border_debug : 0, 2, 'L');
		$pdf->Cell(0, 0, 'Nama', $debug_pdf ? $border_debug : 0, 2, 'L');
		$pdf->Cell(0, 0, 'Ruang', $debug_pdf ? $border_debug : 0, 2, 'L');
		$pdf->Cell(0, 0, 'Lokasi', $debug_pdf ? $border_debug : 0, 2, 'L');
		$pdf->Cell(0, 0, '', $debug_pdf ? $border_debug : 0, 2, 'L'); //BREAK LINE
		$pdf->Cell(0, 0, 'Tanggal Ujian', $debug_pdf ? $border_debug : 0, 2, 'L');
		$pdf->Cell(0, 0, 'Materi Ujian', $debug_pdf ? $border_debug : 0, 2, 'L');
		$pdf->Cell(0, 0, 'Alokasi Waktu', $debug_pdf ? $border_debug : 0, 2, 'L');
		$pdf->Cell(0, 0, $materi_test_1, $debug_pdf ? $border_debug : 0, 2, 'L');
		$pdf->Cell(0, 0, $materi_test_2, $debug_pdf ? $border_debug : 0, 2, 'L');
		$pdf->Cell(0, 0, $materi_test_3, $debug_pdf ? $border_debug : 0, 2, 'L');
		$pdf->SetXY(90, 45);
		$pdf->Cell(0, 0, ': ' . $cmb->KODE_VOUCHER, $debug_pdf ? $border_debug : 0, 2, 'L');
		$pdf->Cell(0, 0, ': ' . $cmb->NO_UJIAN, $debug_pdf ? $border_debug : 0, 2, 'L');
		$pdf->Cell(0, 0, ': ' . $cmb->NM_C_MHS, $debug_pdf ? $border_debug : 0, 2, 'L');
		$pdf->Cell(0, 0, ': ' . $cmb->JADWAL_PPMB->NM_RUANG, $debug_pdf ? $border_debug : 0, 2, 'L');
		$pdf->Cell(0, 0, ': ' . $cmb->JADWAL_PPMB->LOKASI, $debug_pdf ? $border_debug : 0, 2, 'L');
		$pdf->Cell(0, 0, '  ' . $cmb->JADWAL_PPMB->ALAMAT, $debug_pdf ? $border_debug : 0, 2, 'L');
		$pdf->Cell(0, 0, ': ' . $cmb->JADWAL_PPMB->TGL_TEST, $debug_pdf ? $border_debug : 0, 2, 'L');
		$pdf->Cell(0, 0, ': ' . $cmb->JADWAL_PPMB->MATERI, $debug_pdf ? $border_debug : 0, 2, 'L');
		
		// -----------------
		// TTD Ketua
		// -----------------
		$pdf->SetXY(150, 100);
		$pdf->SetFont('helvetica', 'B', 10);
		$pdf->Cell(30, 0, strftime('%d %B %Y'), $debug_pdf ? $border_debug : 0, 2, 'C');
		$pdf->Cell(30, 0, 'Ketua PPMB', $debug_pdf ? $border_debug : 0, 2, 'C');
		$pdf->Image('@' . file_get_contents("assets/images/ttd-ketua.jpg"), $pdf->GetX(), $pdf->GetY(), 30, 15, 'jpg', '', '', false, 300, '', false, false, 0);
		$pdf->SetXY(150, 120);
		$pdf->Cell(30, 0, 'Bambang Sektiari Lukiswanto', $debug_pdf ? $border_debug : 0, 0, 'C');
		
		// -----------------------------------------------------------
		// Signature dari ID_C_MHS, standar PDF417.
		// Untuk mencegah pemalsuan kartu, di beri tanda ID_C_MHS
		// -----------------------------------------------------------
		$pdf->write2DBarcode($cmb->ID_C_MHS, 'PDF417,5', 15, 115, 45, 0, $barcode_style, 'N');
		
		// -----------------
		// Footer Pesan
		// -----------------
		$pdf->SetY(130);
		$pdf->SetFont('helvetica', 'I', 9);
		$pdf->setColor('text', 255, 0, 0);
		$pdf->Cell(0, 0, 'Peserta harus datang di lokasi 30 menit sebelum tes dimulai', $debug_pdf ? $border_debug : 0, 2, 'C');
		
		// Output
		// $pdf->SetDisplayMode('fullwidth');
		$pdf->Output($cmb->NO_UJIAN . '.pdf', 'I');
	}
}

if ( !function_exists('generate_kartu_presensi'))
{
	/**
	 * Menggenerate 
	 * @param TCPDF $pdf
	 * @param array $cmb_set
	 */
	function generate_kartu_presensi(TCPDF &$pdf, &$cmb_set, $filename = '')
	{
		// Set lokal ke indonesia
		setlocale(LC_ALL, 'id_ID');
		
		// document identity
		$pdf->setPDFVersion('1.4');
		$pdf->SetCreator('Sistem Penerimaan Mahasiswa Baru Universitas Airlangga');
		$pdf->SetAuthor('PPMB Universitas Airlangga');
		$pdf->SetTitle('Kartu Presensi Peserta Ujian');
		$pdf->SetSubject('');
		$pdf->SetKeywords('PPMB, Universitas Airlangga, Mandiri, Penerimaan, Pendaftaran, Kartu Ujian');
		
		// tweak performance
		$pdf->setFontSubsetting(false);
		
		// re-Setting Page
		$pdf->setPrintHeader(FALSE);
		$pdf->setPrintFooter(FALSE);
		$pdf->SetMargins(5, 5, 5);  // Margin 0.5 cm
		$pdf->SetAutoPageBreak(false, 0);	// Disable auto page break
		
		// Format kertas Presensi 140mm x 140mm
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 139, 'ury' => 139)
		);
		
		// Debugger
		$debug_pdf = FALSE;
		$border_debug = array(
			'LTRB' => array('width' => 0.1, 'dash' => '5,5', 'color' => array(225, 225, 255)),
		);
		
		// Border Style Rectacle
		$border_style_all = array(
			'L'	=> array('width' => 0.5, 'color' => array(0,0,0), 'cap' => 'round'),
			'T'	=> array('width' => 0.5, 'color' => array(0,0,0), 'cap' => 'round'),
			'R'	=> array('width' => 0.5, 'color' => array(0,0,0), 'cap' => 'round'),
			'B' => array('width' => 0.5, 'color' => array(0,0,0), 'cap' => 'round')
		);
		
		// Benchmark 1000 kartu simultan
		//for ($i = 0; $i <= (2000 / count($cmb_set)); $i++)
		//{
			//$cloned_set = array_merge(array(), $cmb_set);
			//$cmb_set = array_merge($cmb_set, $cloned_set);
		//}
		
		// print_r(count($cmb_set)); exit();
		
		foreach ($cmb_set as $cmb)
		{
			// ------------------
			// Persiapan setting2
			// ------------------
			if (in_array($cmb->ID_JALUR, array(3, 4, 5, 27, 42)))		// Mandiri, Alih-Jenis, Diploma 3, FK-Int
			{
				$jalur_printed = true;
				$ruang_printed = true;
				
				// Nama Jalur
				if ($cmb->ID_JALUR == 3)
					$nama_jalur = 'MANDIRI';
				else if ($cmb->ID_JALUR == 27)
					$nama_jalur = 'MANDIRI';
				else if ($cmb->ID_JALUR == 4 and $cmb->ID_JENJANG == 1)
					$nama_jalur = 'ALIH JENIS';
				else if ($cmb->ID_JALUR == 5)
					$nama_jalur = 'DIPLOMA 3';
				else if ($cmb->ID_JALUR == 42 and $cmb->ID_JENJANG == 4)
					$nama_jalur = 'D4 dari SMA';
				else if ($cmb->ID_JALUR == 4 and $cmb->ID_JENJANG == 4)
					$nama_jalur = 'D4 dari D3';
				
				// Warna Strip kartu
				if ($cmb->KODE_JURUSAN == '01')
				{
					// Merah (Salmon)
					$warna_strip = array(255, 145, 164);
					$kode_jurusan = 'IPA';
				}	
				else if ($cmb->KODE_JURUSAN == '02')
				{
					// Kuning
					$warna_strip = array(255, 255, 0);
					$kode_jurusan = 'IPS';
				}
				else if ($cmb->KODE_JURUSAN == '03')
				{
					// Chartreuse (hijau cerah)
					$warna_strip = array(127, 255, 0);
					$kode_jurusan = 'IPC';
				}
			}
			else if (in_array($cmb->ID_JALUR, array(6, 23, 24, 41)))		// Pasca, Profesi, Spesialis, Pasca Int
			{
				$jalur_printed = false;
				$ruang_printed = false;
				
				$warna_strip = array(255, 255, 255);
			}
			
			// Ganti Title Case untuk nama
			$cmb->NM_C_MHS = ucwords(strtolower($cmb->NM_C_MHS));
			
			// Format tanggal test
			if ($cmb->DUA_HARI_TEST == 1)
			{
				$cmb->TGL_TEST = strftime('%d %B', strtotime($cmb->TGL_TEST)) . ' dan ' . strftime('%d %B %Y', strtotime($cmb->TGL_TEST2));
			}
			else
			{
				$cmb->TGL_TEST = strftime('%d %B %Y', strtotime($cmb->TGL_TEST));
			}
			
			// -----------
			// Tambah Page
			// -----------
			$pdf->AddPage('P', $page_format);
			
			// Set bookmark, untuk mempermudah searching
			$pdf->Bookmark($cmb->NO_UJIAN . ' - ' . $cmb->NM_C_MHS, 0, 0);

			// -------------------
			// GAMBAR KOTAK KARTU 
			// -------------------
			$pdf->SetLineWidth(0.5);
			
			// Kotak Besar
			$pdf->Rect(10, 10, $pdf->getPageWidth() - 20, $pdf->getPageHeight() - 20);
			
			// ------------------
			// Judul Kartu, Logo
			// ------------------
			$pdf->Image('@' . file_get_contents('assets/logo_unair.jpg'), 15, 11, 15, 15, 'jpg');
			$pdf->SetFont('times', '', 13);
			$pdf->SetXY(30, 13);
			$pdf->Cell(0, 0, 'PUSAT PENERIMAAN MAHASISWA BARU', $debug_pdf ? $border_debug : 0, 2, 'C');
			$pdf->SetFont('times', 'B', 17);
			$pdf->Cell(0, 0, 'UNIVERSITAS AIRLANGGA', $debug_pdf ? $border_debug : 0, 1, 'C');
			
			// -----------------------
			// Strip Bukti Hadir Ujian
			// -----------------------
			// Kotak Strip
			$pdf->SetLineWidth(0.5);
			$pdf->Rect(10, 28, $pdf->getPageWidth() - 20, 7, 'DF', $border_style_all, $warna_strip);
			// Tulisan
			$pdf->SetY(28, true);
			$pdf->SetFont('times', '', 14);
			$pdf->Cell(0, 0, ' BUKTI HADIR UJIAN', $debug_pdf ? $border_debug : 0, 1, 'C');
			
			if ($jalur_printed)
			{
				// ------------------
				// Kotak kode jurusan
				// ------------------
				// Kotak + Garis
				$pdf->Rect($pdf->getPageWidth() - 37, 37, 25, 25);
				$pdf->Line($pdf->getPageWidth() - 37, 55, $pdf->getPageWidth() - 12, 55);
				// Tulisan Kode Jurusan
				$pdf->SetXY($pdf->getPageWidth() - 37, 37);
				$pdf->SetFont('helvetica', 'B', 36);	// Large size
				$pdf->Cell(25, 18, $kode_jurusan, $debug_pdf ? $border_debug : 0, 2, 'C');
				$pdf->SetFont('helvetica', 'B', 9);		// tiny
				$pdf->Cell(25, 7, $nama_jalur, $debug_pdf ? $border_debug : 0, 2, 'C');
			}
			
			
			// ------------
			// Isian detail
			// ------------
			$pdf->SetXY(15, 42);
			$pdf->SetFont('helvetica', '', 9);
			$pdf->Cell(22, 6, 'No Ujian', $debug_pdf ? $border_debug : 0, 2);
			$pdf->Cell(22, 6, 'Nama', $debug_pdf ? $border_debug : 0, 2);
			$pdf->Cell(22, 6, 'Tanggal Ujian', $debug_pdf ? $border_debug : 0, 2);
			if ($ruang_printed) { $pdf->Cell(22, 6, 'Ruang', $debug_pdf ? $border_debug : 0, 2); }
			$pdf->Cell(22, 6, 'Lokasi Ujian', $debug_pdf ? $border_debug : 0, 2);
			
			$pdf->SetXY(15 + 22, 42);  // pindah ke atas lagi
			$pdf->SetFont('helvetica', 'B', 11); // set Bold
			$pdf->Cell(0, 6, ": " . $cmb->NO_UJIAN, $debug_pdf ? $border_debug : 0, 2);
			$pdf->SetFont('helvetica', '', 9); // unset Bold
			$pdf->Cell(0, 6, ": " . $cmb->NM_C_MHS, $debug_pdf ? $border_debug : 0, 2);
			$pdf->Cell(0, 6, ": " . $cmb->TGL_TEST, $debug_pdf ? $border_debug : 0, 2);
			if ($ruang_printed) { $pdf->Cell(0, 6, ": " . $cmb->NM_RUANG, $debug_pdf ? $border_debug : 0, 2); }
			$pdf->Cell(0, 6, ": " . $cmb->LOKASI, $debug_pdf ? $border_debug : 0, 2);
			$pdf->Cell(0, 6, "  " . $cmb->ALAMAT, $debug_pdf ? $border_debug : 0, 2);
			
			// Kotak Foto
			$pdf->SetLineWidth(0.4);
			$pdf->SetXY(15, 85);
			$pdf->Image('@'. file_get_contents("../../../../../web/files/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$cmb->FILE_FOTO}"), 15, 85, 27, 36, '', '', '', true, 300, '', false, false, 1);
			
			$pdf->SetXY(50, 81);
			$pdf->SetFont('helvetica', '', 8);
			$pdf->Cell(43, 0, 'Tanda tangan peserta', $debug_pdf ? $border_debug : 0, 1, 'C');
			
			// Warna Text dibuat samar
			$pdf->setColor('text', 128, 128, 128);
			
			//-------------------
			// Kotak tanda tangan
			//-------------------
			if (in_array($cmb->ID_JALUR, array(3, 4, 5, 27, 42)))		// Mandiri, Alih-Jenis, D3, FK-INT
			{
				if ($kode_jurusan == 'IPA' || $kode_jurusan == 'IPS')
				{
					// Kotak 1
					$pdf->Rect(50, 85, 43, 13, 'D', 'all');
					$pdf->SetXY(50, 85);
					$pdf->SetFont('helvetica', 'I', 7);
					$pdf->Cell(43, 0, 'pada Tes Potensi Akademik', $debug_pdf ? $border_debug : 0, 1, 'C');

					// Kotak 2
					$pdf->Rect(50, 98, 43, 13, 'D', 'all');
					$pdf->SetXY(50, 98);
					$pdf->SetFont('helvetica', 'I', 7);
					$pdf->Cell(43, 0, 'pada Tes Prestasi Akademik', $debug_pdf ? $border_debug : 0, 1, 'C');
				}
				else if ($kode_jurusan == 'IPC')
				{
					// Kotak 1
					$pdf->Rect(50, 85, 43, 13, 'D', 'all');
					$pdf->SetXY(50, 85);
					$pdf->SetFont('helvetica', 'I', 7);
					$pdf->Cell(43, 0, 'pada Tes Potensi Akademik', $debug_pdf ? $border_debug : 0, 1, 'C');

					// Kotak 2
					$pdf->Rect(50, 98, 43, 13, 'D', 'all');
					$pdf->SetXY(50, 98);
					$pdf->SetFont('helvetica', 'I', 7);
					$pdf->Cell(43, 0, 'pada Tes Prestasi Akademik IPA', $debug_pdf ? $border_debug : 0, 1, 'C');

					// Kotak 3
					$pdf->Rect(50, 111, 43, 13, 'D', 'all');
					$pdf->SetXY(50, 111);
					$pdf->SetFont('helvetica', 'I', 7);
					$pdf->Cell(43, 0, 'pada Tes Prestasi Akademik IPS', $debug_pdf ? $border_debug : 0, 1, 'C');
				}
			}
			else if (in_array($cmb->ID_JALUR, array(6, 23, 24, 41))) // Pasca sarjana, PPDS, PPDGS
			{
				 // Kotak 1
				$pdf->Rect(50, 85, 33, 13, 'D', $border_style_all);
				$pdf->SetXY(50, 85);
				$pdf->SetFont('helvetica', 'I', 7);
				$pdf->Cell(33, 0, 'pada Sesi I Ujian', $debug_pdf ? $border_debug : 0, 1, 'C');

				// Kotak 2
				$pdf->Rect(50, 98, 33, 13, 'D', $border_style_all);
				$pdf->SetXY(50, 98);
				$pdf->SetFont('helvetica', 'I', 7);
				$pdf->Cell(33, 0, 'pada Sesi II Ujian', $debug_pdf ? $border_debug : 0, 1, 'C');

				// Kotak 3
				$pdf->Rect(85, 85, 33, 13, 'D', $border_style_all);
				$pdf->SetXY(85, 85);
				$pdf->SetFont('helvetica', 'I', 7);
				$pdf->Cell(33, 0, 'pada Sesi III Ujian', $debug_pdf ? $border_debug : 0, 1, 'C');
				
				// Kotak 4
				$pdf->Rect(85, 98, 33, 13, 'D', $border_style_all);
				$pdf->SetXY(85, 98);
				$pdf->SetFont('helvetica', 'I', 7);
				$pdf->Cell(33, 0, 'pada Sesi IV Ujian', $debug_pdf ? $border_debug : 0, 1, 'C');
			}
			
			
			// reset warna text
			$pdf->setColor('text', 0, 0, 0);
			
			// Exit Loop
			// break;
		}
		
		// Output
		$pdf->SetDisplayMode('fullpage');
		$pdf->Output($filename, 'D');
	}
}
