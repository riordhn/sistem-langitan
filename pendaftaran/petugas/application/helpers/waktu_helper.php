<?php

if ( !function_exists('get_interval_date'))
{
	/**
	 * Mendapatkan interval dari suatu tanggal
	 * @param string $date_from Format: YYYY-MM-DD HH24:MI:SS
	 */
	function get_interval_date($date_from)
	{
		$date_from		= new DateTime($date_from);
		$date_now		= new DateTime();
		$date_interval	= $date_from->diff($date_now);
		
		if ($date_interval->d == 0)
		{
			if ($date_interval->h == 0)
			{
				if ($date_interval->i == 0)
				{
					return $date_interval->s . ' dtk yg lalu';
				}
				else
				{
					return $date_interval->i . ' mnt yg lalu';
				}
			}
			else
			{
				return $date_interval->h . ' jam yg lalu';
			}
			
		}
		else if ($date_interval->d == 1)
		{
			return 'Kemarin, jam ' . $date_from->format('H:i');
		}
		else
		{
			return $date_from->format('d F, H:i');
		}
	}
}