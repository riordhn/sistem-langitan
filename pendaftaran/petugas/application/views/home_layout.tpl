<!DOCTYPE html>
<html lang="id">
	<head>
		<title>{block name='title'}{/block}Petugas PPMB - Universitas Airlangga</title>
		<meta name="description" content="Pendaftaran online mahasiswa baru Universitas Airlangga" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		<!-- Bootstrap -->
		<link href="{base_url('../assets/css/bootstrap.min.css')}" rel="stylesheet" />
		
		<style type="text/css">
			body { padding-top: 40px; }
		</style>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		{block name='head'}{/block}
	</head>
	
	<body>
		<div class="navbar navbar-default navbar-fixed-top navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<a href="{site_url('home')}" class="navbar-brand">Petugas</a>
					<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
				</div>
					
				<div class="navbar-collapse collapse" id="navbar-main">
					<ul class="nav navbar-nav">
						<li>
							<a href="{site_url('home')}">Home</a>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">PMB <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{site_url('verifikasi/list_penerimaan')}">Verifikasi Berkas</a></li>
								<!-- <li><a href="{site_url('verifikasi/akreditasi_aj')}">Verifikasi Akreditasi</a></li> -->
								<!-- <li><a href="{site_url('verifikasi/edit_akreditasi')}">Edit Akreditasi</a></li> -->
								<li class="divider"></li>
								<li><a href="{site_url('verifikasi/reset')}">Reset Verifikasi</a></li>
								<li class="divider"></li>
								<li><a href="{site_url('verifikasi/cetak_presensi')}">Cetak Presensi</a></li>
							</ul>
						</li>
						<!-- <li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">SNMPTN <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{site_url('verifikasi_snmptn/list_penerimaan')}">Verifikasi Tahap 1</a></li>
								<li><a href="{site_url('verifikasi_snmptn/list_penerimaan_2')}">Verifikasi Tahap 2</a></li>
								<li class="divider"></li>
								<li><a href="{site_url('verifikasi_snmptn/reset')}">Reset Verifikasi</a></li>
								<li class="divider"></li>
								<li><a href="{site_url('verifikasi_snmptn/sekolah')}">Cleansing Data Sekolah</a></li>
							</ul>
						</li> -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Helpdesk <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{site_url('helpdesk/aktivasi_login')}">Menu Helpdesk</a></li>
								<li><a href="{site_url('helpdesk/reset_upload')}">Reset Submit Upload</a></li>
							</ul>
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="{site_url('auth/logout')}">Logout</a>
						</li>
					</ul>
				</div>

			</div>
		</div>
		
		<div class="container">
		{block name='body-content'}{/block}
		</div>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="{base_url('../assets/js/jquery-1.11.0.min.js')}"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="{base_url('../assets/js/bootstrap.min.js')}"></script>
		{block name='footer-script'}{/block}
	</body>
</html>