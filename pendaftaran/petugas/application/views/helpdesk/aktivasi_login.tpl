{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Menu Helpdesk</h2>
			</div>

			<form class="form-inline" method="get" action="{current_url()}">
				<div class="form-group">
					<label class="sr-only" for="q">Nomor Pendaftaran</label>
					<input type="text" class="form-control" name="q" placeholder="Nomor Pendaftaran" value="{if isset($smarty.get.q)}{$smarty.get.q}{/if}" />
				</div>
				<button type="submit" class="btn btn-primary">Cari</button>
			</form>
				
			<hr />

			{if !empty($calon_pendaftar)}
				
				
					<table class="table table-bordered" style="width: auto">
						<thead>
							<tr>
								<th colspan="2">Detail Calon Pendaftar</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Nama</td>
								<td>{$calon_pendaftar->NM_C_MHS}</td>
							</tr>
							<tr>
								<td>Email</td>
								<td>{$calon_pendaftar->EMAIL}</td>
							</tr>
							<tr>
								<td>Telp</td>
								<td>{$calon_pendaftar->NOMOR_HP}</td>
							</tr>
							<tr>
								<td>Tanggal Daftar</td>
								<td>{strftime('%d %B %Y', strtotime($calon_pendaftar->TGL_REGISTRASI))}</td>
							</tr>
							<tr>
								<td>Penerimaan</td>
								<td>{if !empty($calon_pendaftar->CMB->PENERIMAAN->NM_PENERIMAAN) }{$calon_pendaftar->CMB->PENERIMAAN->NM_PENERIMAAN}{/if}</td>
							</tr>
							<tr>
								<td>Nomor Pendaftaran</td>
								<td>{if !empty($calon_pendaftar->CMB->TGL_VERIFIKASI_PPMB) }{$calon_pendaftar->CMB->KODE_VOUCHER}{/if}</td>
							</tr>
							<!-- <tr>
								<td>Status Bayar</td>
								<td>{if !empty($calon_pendaftar->CMB->NO_UJIAN) }SUDAH{else}BELUM{/if}</td>
							</tr> -->
							<!-- <tr>
								<td>Status Konfirmasi Email</td>
								<td>{if $calon_pendaftar->TGL_KONFIRMASI_EMAIL}<span class="label label-success">OK</span>{else}<span class="label label-default">Belum konfirmasi</span>{/if}</td>
							</tr> -->
							<tr>
								<td>Reset Petugas Verifikasi</td>
								<td>
									{if !empty($calon_pendaftar->CMB->PENGGUNA->NM_PENGGUNA)}
									{$calon_pendaftar->CMB->PENGGUNA->NM_PENGGUNA}
									<form class="form-inline" role="form" method="post" action="{current_url()}">
										<input type="hidden" name="mode" value="reset_petugasverifikasi" />
										<input type="hidden" name="id_calon_pendaftar" value="{$calon_pendaftar->CMB->ID_C_MHS}" />
										<input type="submit" class="btn btn-primary btn-xs" value="Reset Petugas" />
									</form>
									{/if}
								</td>
							</tr>
							<tr>
								<td>Reset Password</td>
								<td>
									<form class="form-inline" role="form" method="post" action="{current_url()}">
										<input type="hidden" name="mode" value="reset_password" />
										<input type="hidden" name="id_calon_pendaftar" value="{$calon_pendaftar->ID_CALON_PENDAFTAR}" />
										<input type="submit" class="btn btn-primary btn-xs" value="Reset Password" />
									</form>
									<p class="help-block small">Default : 123456</p>
								</td>
							</tr>
							<!-- <tr>
								<td>Password Sementara</td>
								<td>
									<form class="form-inline" role="form" method="post" action="{current_url()}">
										<input type="hidden" name="mode" value="set_password" />
										<input type="hidden" name="id_calon_pendaftar" value="{$calon_pendaftar->ID_CALON_PENDAFTAR}" />
										<input type="text" class="form-control input-sm" name="pass_temp" placeholder="Password sementara" autocomplete="off" value="{$calon_pendaftar->PASS_TEMP}"/>
										<input type="submit" class="btn btn-primary btn-xs" value="Set" />
									</form>
									<p class="help-block small">Password sementara ini digunakan untuk mengecek isian peserta,<br/>tidak digunakan untuk mereset password peserta.</p>
								</td>
							</tr> -->
							<tr>
								<td colspan="2">
									<form class="form-horizontal" method="post" action="{current_url()}">
										<input type="hidden" name="mode" value="aktifkan_login" />
										<input type="hidden" name="id_calon_pendaftar" value="{$calon_pendaftar->ID_CALON_PENDAFTAR}" />
										{* {if $calon_pendaftar->TGL_KONFIRMASI_EMAIL == ''} *}
											<!-- <button type="submit" class="btn btn-primary">Aktifkan Login</button> -->
										{* {/if} *}
										<a href="{site_url('helpdesk/aktivasi_login')}">Kembali</a>
									</form>
								</td>
							</tr>
						</tbody>
					</table>
			{else if isset($aktivasi_success)}
				<div class="alert alert-success">
					<strong>Aktivasi Berhasil !</strong>
				</div>
			{else if isset($pass_temp_success)}
				<div class="alert alert-success">
					<strong>Set password sementara Berhasil !</strong>
				</div>
			{else if isset($reset_pass_success)}
				<div class="alert alert-success">
					<strong>Reset Password Berhasil !</strong>
				</div>
			{else if isset($reset_petugas_success)}
				<div class="alert alert-success">
					<strong>Reset Petugas Verifikasi Berhasil !</strong>
				</div>
			{else if isset($smarty.get.q)}
				<div class="alert alert-warning">
					<strong>Nomor Pendaftaran tidak ditemukan !</strong>
				</div>
			{/if}
		</div>
	</div>
{/block}