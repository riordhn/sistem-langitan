{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Reset Submit Upload</h2>
			</div>

			<form class="form-inline" method="get" action="{current_url()}">
				<div class="form-group">
					<label class="sr-only" for="q">Nomor Pendaftaran</label>
					<input type="text" class="form-control" name="q" placeholder="Nomor Pendaftaran" value="{if isset($smarty.get.q)}{$smarty.get.q}{/if}" />
				</div>
				<button type="submit" class="btn btn-primary">Cari</button>
			</form>
				
			<hr />

			{if !empty($calon_pendaftar)}
				
				<form class="form-horizontal" method="post" action="{current_url()}">
					
					{if $calon_pendaftar->CMB}
						<input type="hidden" name="id_c_mhs" value="{$calon_pendaftar->CMB->ID_C_MHS}" />
					{/if}
					
					<table class="table table-bordered" style="width: auto">
						<thead>
							<tr>
								<th colspan="2">Detail Calon Pendaftar</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Nama</td>
								<td>{$calon_pendaftar->NM_C_MHS}</td>
							</tr>
							<tr>
								<td>Email</td>
								<td>{$calon_pendaftar->EMAIL}</td>
							</tr>
							<tr>
								<td>Telp</td>
								<td>{$calon_pendaftar->NOMOR_HP}</td>
							</tr>
							<tr>
								<td>Tanggal Daftar</td>
								<td>{strftime('%d %B %Y', strtotime($calon_pendaftar->TGL_REGISTRASI))}</td>
							</tr>
							<tr>
								<td>Tanggal Submit Verifikasi</td>
								<td>
									{if $calon_pendaftar->CMB}
										{strftime('%d %B %Y', strtotime($calon_pendaftar->CMB->TGL_SUBMIT_VERIFIKASI))}
									{/if}
								</td>
							</tr>
							<tr>
								<td>Tanggal Verifikasi Petugas</td>
								<td>
									{if $calon_pendaftar->CMB}
										{if $calon_pendaftar->CMB->TGL_VERIFIKASI_PPMB != ''}
											{strftime('%d %B %Y', strtotime($calon_pendaftar->CMB->TGL_VERIFIKASI_PPMB))}
										{/if}
									{/if}
								</td>
							</tr>
							<tr>
								<td>Nomor Pendaftaran</td>
								<td>
									{if $calon_pendaftar->CMB}
										{$calon_pendaftar->CMB->KODE_VOUCHER}
									{/if}
								</td>
							</tr>
							<tr>
								<td colspan="2">
									{if $calon_pendaftar->CMB->TGL_SUBMIT_VERIFIKASI != '' and $calon_pendaftar->CMB->TGL_VERIFIKASI_PPMB == '' }
										<button type="submit" class="btn btn-primary">Reset Submit Upload</button>
									{/if}
									<a href="{site_url('helpdesk/reset_upload')}">Kembali</a>
								</td>
							</tr>
						</tbody>
					</table>

				</form>
			{else if isset($reset_success)}
				<div class="alert alert-success">
					<strong>Reset Berhasil !</strong>
				</div>
			{else if isset($smarty.get.q)}
				<div class="alert alert-warning">
					<strong>Nomor Pendaftaran tidak ditemukan !</strong>
				</div>
			{/if}
		</div>
	</div>
{/block}