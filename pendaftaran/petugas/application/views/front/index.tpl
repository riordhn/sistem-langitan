{extends file='front_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">

			<div class="page-header">
				<h2>Login Petugas Pendaftaran</h2>
			</div>

			<form class="form-horizontal" role="form" action="{site_url('auth/login')}" method="post">
				
				{if !empty($error_message)}
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					{$error_message}
				</div>
				{/if}
				
				<div class="form-group">
					<label for="username" class="col-md-2 control-label">Username</label>
					<div class="col-md-4">
						<input type="text" class="form-control" id="username" name="username" placeholder="Username" value="{set_value('username')}">
					</div>
				</div>
					
				<div class="form-group">
					<label for="password" class="col-md-2 control-label">Password</label>
					<div class="col-md-4">
						<input type="password" class="form-control" id="password" name="password" placeholder="Password">
					</div>
				</div>
					
				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<button type="submit" class="btn btn-primary">Login</button>
					</div>
				</div>
					
			</form>

			<ul>
				<li>Login yang dipakai adalah login <b>LANGITAN</b></li>
			</ul>

		</div>
	</div>
{/block}