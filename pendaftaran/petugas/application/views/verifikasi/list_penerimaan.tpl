{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Daftar Penerimaan Saat Ini</h2>
			</div>
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center">Penerimaan</th>
						<th class="text-center">Jumlah Antrian</th>
						<th class="text-center">Proses Verifikasi</th>
						<th class="text-center">Jumlah Kembali</th>
						<th class="text-center">Lolos Verifikasi</th>
						<th class="text-center">Jumlah per Verifikator</th>
					</tr>
				</thead>
				<tbody>
					{foreach $penerimaan_set as $p}
						<tr>
							<td>{$p->NM_PENERIMAAN} Gelombang {$p->GELOMBANG} Tahun {$p->TAHUN}</td>
							<td class="text-center">
								<a href="{site_url('verifikasi/list_antrian')}/{$p->ID_PENERIMAAN}">{$p->JUMLAH_ANTRIAN}</a>
							</td>
							<td class="text-center">
								<a href="{site_url('verifikasi/list_proses')}/{$p->ID_PENERIMAAN}">{$p->JUMLAH_PROSES_VERIFIKASI}</a>
							</td>
							<td class="text-center">
								<a href="{site_url('verifikasi/list_kembali')}/{$p->ID_PENERIMAAN}">{$p->JUMLAH_KEMBALI}</a>
							</td>
							<td class="text-center">
								<a href="{site_url('verifikasi/list_verifikasi')}/{$p->ID_PENERIMAAN}">{$p->JUMLAH_VERIFIKASI}</a>
							</td>
							<td class="text-center">
								<a href="{site_url('verifikasi/list_memverifikasi')}/{$p->ID_PENERIMAAN}">{$p->JUMLAH_MEMVERIFIKASI}</a>
							</td>
						</tr>
					{/foreach}
				</tbody>
			</table>
		</div>
	</div>
{/block}