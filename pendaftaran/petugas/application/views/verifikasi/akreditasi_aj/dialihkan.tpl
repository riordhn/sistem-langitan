{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Verifikasi Peserta Akreditasi Alih Jenis</h2>
			</div>
			<div class="row">
				<div class="col-md-12">
					
					<div class="alert alert-info" role="alert">
						<strong>Halaman verifikasi akreditasi sudah tidak dipakai</strong>
						<p>Silahkan gunakan halaman <a href="{site_url('verifikasi/list_penerimaan/')}">Verifikasi Berkas</a> untuk melakukan verifikasi akreditasi Alih Jenis.</p>
					</div>
					
				</div>	
			</div>
		</div>
	</div>	
{/block}