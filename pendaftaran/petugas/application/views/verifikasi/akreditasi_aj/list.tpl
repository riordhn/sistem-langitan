{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Verifikasi Akreditasi Alih Jenis</h2>
			</div>
			
			<table class="table table-bordered" style="width: auto">
				<thead>
					<tr>
						<th>Antrian</th>
						<th>Selesai</th>
						<th>Proses</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><a href="{site_url('verifikasi/akreditasi_aj/antrian')}">{$verifikasi->JUMLAH_ANTRIAN}</a></td>
						<td><a href="{site_url('verifikasi/akreditasi_aj/selesai')}">{$verifikasi->JUMLAH_SELESAI}</a></td>
						<td><a href="{site_url('verifikasi/akreditasi_aj/proses')}">{$verifikasi->JUMLAH_PROSES}</a></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
{/block}