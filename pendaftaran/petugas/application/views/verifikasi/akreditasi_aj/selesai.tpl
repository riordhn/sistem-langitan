{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Daftar Peserta Selesai Verifikasi Akreditasi Alih Jenis</h2>
				<a href='{site_url('verifikasi/akreditasi_aj')}'>Kembali ke rekap</a>
			</div>
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>#</th>
						<th>Nama</th>
						<th>Akreditasi</th>
						<th>Peringkat</th>
					</tr>
				</thead>
				<tbody>
					{foreach $cmb_set as $cmb}
					<tr>
						<td>{$cmb@index + 1}</td>
						<td>{$cmb->NM_C_MHS}</td>
						<td>{$cmb->JENIS_AKREDITASI_S1}</td>
						<td>{$cmb->PERINGKAT_AKREDITASI_S1}</td>
					</tr>
					{/foreach}
				</tbody>
			</table>
		</div>
	</div>
{/block}