{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Verifikasi Peserta Akreditasi Alih Jenis</h2>
				<a href='{site_url('verifikasi/akreditasi_aj/antrian')}'>Kembali ke antrian</a>
			</div>
			<div class="row">
				<div class="col-md-4" id="biodata">
					
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Informasi Peserta</h3>
						</div>
						<table class="table table-condensed table-bordered">
							<tbody>
								<tr>
									<td>Nama</td>
									<td>{$cmb->NM_C_MHS}</td>
								</tr>
								<tr>
									<td>Jenis Akreditasi</td>
									<td>
										<select class="form-control" name="jenis_akreditasi_s1" data-id='{$cmb->ID_C_MHS}'>
											<option value=""></option>
											<option value="BAN-PT" {if $cmb->JENIS_AKREDITASI_S1 == 'BAN-PT'}selected{/if}>BAN-PT</option>
											<option value="Kemenkes" {if $cmb->JENIS_AKREDITASI_S1 == 'Kemenkes'}selected{/if}>Kemenkes</option>
											<option value="Lain" {if $cmb->JENIS_AKREDITASI_S1 == 'Lain'}selected{/if}>Lainnya</option>
										</select>
									</td>
								</tr>
								<tr>
									<td>Peringkat Akreditasi</td>
									<td>
										<select class="form-control" name="peringkat_akreditasi_s1" data-id='{$cmb->ID_C_MHS}'>
											<option value=""></option>
											<option value="A" {if $cmb->PERINGKAT_AKREDITASI_S1 == 'A'}selected{/if}>A</option>
											<option value="B" {if $cmb->PERINGKAT_AKREDITASI_S1 == 'B'}selected{/if}>B</option>
											<option value="C" {if $cmb->PERINGKAT_AKREDITASI_S1 == 'C'}selected{/if}>C</option>
											<option value="Lain" {if $cmb->PERINGKAT_AKREDITASI_S1 == 'Lain'}selected{/if}>Lainnya</option>
										</select>
									</td>
								</tr>
								<tr>
									<td colspan='2'>
										<a class="btn btn-primary" href='{site_url('verifikasi/akreditasi_aj/submit')}/{$cmb->ID_C_MHS}'>Simpan &amp; Kembali ke antrian</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
								
				</div>
				<div class="col-md-8" id="biodata">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Berkas Akreditasi</h3>
						</div>
						<table class="table table-condensed table-bordered">
							<tbody>
								<tr>
									<td>
										<img src="{base_url('files')}/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$cmb->FILE_AKREDITASI}" />
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			
		</div>
	</div>
{/block}

{block name='footer-script'}
	<script type='text/javascript'>
		$(document).ready(function() {
			
			$('select[name="jenis_akreditasi_s1"]').on('change', function() {
				var jenis = $(this).val();
				var id_c_mhs = $(this).data('id');
				
				$.ajax({
					url: '{site_url('verifikasi/akreditasi_aj/update_jenis')}/'+id_c_mhs+'/'+jenis
				});
			});
			
			$('select[name="peringkat_akreditasi_s1"]').on('change', function() {
				var peringkat = $(this).val();
				var id_c_mhs = $(this).data('id');
				
				$.ajax({
					url: '{site_url('verifikasi/akreditasi_aj/update_peringkat')}/'+id_c_mhs+'/'+peringkat
				});
			});
			
		});
	</script>
{/block}