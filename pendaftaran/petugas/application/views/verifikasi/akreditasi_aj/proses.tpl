{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Daftar Sedang Proses Verifikasi Akreditasi Alih Jenis</h2>
				<a href='{site_url('verifikasi/akreditasi_aj')}'>Kembali ke rekap</a>
			</div>
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>#</th>
						<th>Nama</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					{foreach $cmb_set as $cmb}
					<tr>
						<td>{$cmb@index + 1}</td>
						<td>{$cmb->NM_C_MHS}</td>
						<td>
							<a class="btn btn-primary btn-xs" href="{site_url('verifikasi/akreditasi_aj/peserta')}/{$cmb->ID_C_MHS}">Proses</a>
							<a class="btn btn-danger btn-xs" href="{site_url('verifikasi/akreditasi_aj/cancel')}/{$cmb->ID_C_MHS}">Batalkan</a>
						</td>
					</tr>
					{/foreach}
				</tbody>
			</table>
		</div>
	</div>
{/block}