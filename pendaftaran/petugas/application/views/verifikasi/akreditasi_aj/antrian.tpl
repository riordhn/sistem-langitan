{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Daftar Peserta Verifikasi Akreditasi Alih Jenis</h2>
				<a href='{site_url('verifikasi/akreditasi_aj')}'>Kembali ke rekap</a>
			</div>
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>#</th>
						<th>Nama</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					{foreach $cmb_set as $cmb}
					<tr>
						<td>{$cmb@index + 1}</td>
						<td>{$cmb->NM_C_MHS}</td>
						<td>
							<a class="btn btn-primary btn-xs" href="{site_url('verifikasi/akreditasi_aj/start')}/{$cmb->ID_C_MHS}">Proses</a>
						</td>
					</tr>
					{/foreach}
				</tbody>
			</table>
		</div>
	</div>
{/block}

{block name='footer-script'}
	<script type="text/javascript">
		setInterval(function() {
			$.ajax({
				async: false,
				url: '{site_url('verifikasi/akreditasi_aj/antrian_json')}',
				dataType: 'json'
			}).done(function(data) {
				// clear row
				$('table>tbody').html('');
				
				$(data).each(function(i, e) {
					$('table>tbody').append('<tr><td>' + e.ROWNUM + '</td><td>' + e.NM_C_MHS + '</td><td><a class="btn btn-primary btn-xs" href="{site_url('verifikasi/akreditasi_aj/start')}/'+e.ID_C_MHS+'">Proses</a></td></tr>');
				});
			});
		}, 5000);
	</script>
{/block}