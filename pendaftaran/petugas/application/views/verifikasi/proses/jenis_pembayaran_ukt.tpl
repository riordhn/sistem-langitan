<form method="post" action="{current_url()}">
	<input type="hidden" name="mode" value="{strtolower($cmb->JENIS_PEMBAYARAN)}" />
	<input type="hidden" name="id_c_mhs" value="{$cmb->ID_C_MHS}" />
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Pilihan UKT</h3>
		</div>
		<table class="table table-bordered table-condensed">
			<tbody>
				{if $cmb->ID_PILIHAN_1 != ''}
					<tr>
						<td>
							<div class="form-group {if form_error('id_kelompok_biaya_1')}has-error{/if}">
								<label for="id_kelompok_biaya_1">{$cmb->PILIHAN_1->NM_PROGRAM_STUDI}</label>
								<select class="form-control" name="id_kelompok_biaya_1">
									<option value=""></option>
									{foreach $cmb->PILIHAN_1->kelompok_biaya_set as $kb}
										<option value="{$kb->ID_KELOMPOK_BIAYA}" {if $kb->ID_KELOMPOK_BIAYA == $cmd->ID_KELOMPOK_BIAYA_1}selected{/if}>{$kb->NM_KELOMPOK_BIAYA}</option>
									{/foreach}
								</select>
								{form_error('id_kelompok_biaya_1')}
							</div>
						</td>
					</tr>
				{/if}
				{if $cmb->ID_PILIHAN_2 != ''}
					<tr>
						<td>
							<div class="form-group {if form_error('id_kelompok_biaya_2')}has-error{/if}">
								<label for="id_kelompok_biaya_2">{$cmb->PILIHAN_2->NM_PROGRAM_STUDI}</label>
								<select class="form-control" name="id_kelompok_biaya_2">
									<option value=""></option>
									{foreach $cmb->PILIHAN_2->kelompok_biaya_set as $kb}
										<option value="{$kb->ID_KELOMPOK_BIAYA}" {if $kb->ID_KELOMPOK_BIAYA == $cmd->ID_KELOMPOK_BIAYA_2}selected{/if}>{$kb->NM_KELOMPOK_BIAYA}</option>
									{/foreach}
								</select>
								{form_error('id_kelompok_biaya_2')}
							</div>
						</td>
					</tr>
				{/if}
				{if $cmb->ID_PILIHAN_3 != ''}
					<tr>
						<td>
							<div class="form-group {if form_error('id_kelompok_biaya_3')}has-error{/if}">
								<label for="id_kelompok_biaya_3">{$cmb->PILIHAN_3->NM_PROGRAM_STUDI}</label>
								<select class="form-control" name="id_kelompok_biaya_3">
									<option value=""></option>
									{foreach $cmb->PILIHAN_3->kelompok_biaya_set as $kb}
										<option value="{$kb->ID_KELOMPOK_BIAYA}" {if $kb->ID_KELOMPOK_BIAYA == $cmd->ID_KELOMPOK_BIAYA_3}selected{/if}>{$kb->NM_KELOMPOK_BIAYA}</option>
									{/foreach}
								</select>
								{form_error('id_kelompok_biaya_3')}
							</div>
						</td>
					</tr>
				{/if}
				{if $cmb->ID_PILIHAN_4 != ''}
					<tr>
						<td>
							<div class="form-group {if form_error('id_kelompok_biaya_4')}has-error{/if}">
								<label for="id_kelompok_biaya_4">{$cmb->PILIHAN_4->NM_PROGRAM_STUDI}</label>
								<select class="form-control" name="id_kelompok_biaya_4">
									<option value=""></option>
									{foreach $cmb->PILIHAN_4->kelompok_biaya_set as $kb}
										<option value="{$kb->ID_KELOMPOK_BIAYA}" {if $kb->ID_KELOMPOK_BIAYA == $cmd->ID_KELOMPOK_BIAYA_4}selected{/if}>{$kb->NM_KELOMPOK_BIAYA}</option>
									{/foreach}
								</select>
								{form_error('id_kelompok_biaya_4')}
							</div>
						</td>
					</tr>
				{/if}
				<tr>
					<td>
						<input type="submit" class="btn btn-primary" value="Simpan Pilihan UKT" />
						<p><small>Khusus untuk D3, cukup dipilih salah satu UKS lalu klik Simpan. Proses penentuan UKS ditentukan ketika peserta diterima.</small></p>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</form>