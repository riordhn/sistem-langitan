<form method="post" action="{current_url()}">
	<input type="hidden" name="mode" value="akreditasi" />
	<input type="hidden" name="id_c_mhs" value="{$cmb->ID_C_MHS}" />
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Akreditasi</h3>
		</div>
		<table class="table table-condensed table-bordered">
			<tbody>
				<tr>
					<td>Jenis Akreditasi</td>
					<td>
						<div class="form-group">
							<select class="form-control" name="jenis_akreditasi_s1">
								<option value=""></option>
								<option value="BAN-PT" {if $cmp->JENIS_AKREDITASI_S1 == 'BAN-PT'}selected{/if}>BAN-PT</option>
								<option value="Kemenkes" {if $cmp->JENIS_AKREDITASI_S1 == 'Kemenkes'}selected{/if}>Kemenkes</option>
								<option value="Lain" {if $cmp->JENIS_AKREDITASI_S1 == 'Lain'}selected{/if}>Lainnya</option>
							</select>
							{form_error('jenis_akreditasi_s1')}
						</div>
					</td>
				</tr>
				<tr>
					<td>Peringkat Akreditasi</td>
					<td>
						<div class="form-group">
							<select class="form-control" name="peringkat_akreditasi_s1">
								<option value=""></option>
								<option value="A" {if $cmp->PERINGKAT_AKREDITASI_S1 == 'A'}selected{/if}>A</option>
								<option value="B" {if $cmp->PERINGKAT_AKREDITASI_S1 == 'B'}selected{/if}>B</option>
								<option value="C" {if $cmp->PERINGKAT_AKREDITASI_S1 == 'C'}selected{/if}>C</option>
								<option value="Lain" {if $cmp->PERINGKAT_AKREDITASI_S1 == 'Lain'}selected{/if}>Lainnya</option>
							</select>
							{form_error('peringkat_akreditasi_s1')}
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="submit" class="btn btn-primary btn-sm" value="Simpan" />
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</form>