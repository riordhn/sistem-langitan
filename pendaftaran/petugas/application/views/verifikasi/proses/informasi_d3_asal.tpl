<form method="post" action="{current_url()}">
	<input type="hidden" name="mode" value="pt_asal" />
	<input type="hidden" name="id_c_mhs" value="{$cmb->ID_C_MHS}" />
	
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Informasi Peserta</h3>
		</div>
		<table class="table table-bordered table-condensed">
			<tbody>
				<tr>
					<td colspan="2" class="text-center">Diploma</td>
				</tr>
				<tr>
					<td>Gelar</td>
					<td>
						<input type="text" class="form-control" name="gelar_s1" value="{$cmb->GELAR}" />
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<button class="btn btn-primary btn-sm">Simpan</button>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	
</form>