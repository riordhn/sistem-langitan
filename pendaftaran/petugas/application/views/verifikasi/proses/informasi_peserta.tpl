<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Informasi Peserta</h3>
	</div>
	<table class="table table-bordered table-condensed">
		<tbody>
			<tr>
				<td>Nama</td>
				<td>{$cmb->NM_C_MHS}</td>
			</tr>
			{if $cmb->PENERIMAAN->ID_JENIS_FORM == '1'}
				<tr>
					<td>Asal Sekolah</td>
					<td>{$cms->SEKOLAH_ASAL->NM_SEKOLAH}</td>
				</tr>
				<tr>
					<td>Tahun Lulus</td>
					<td>{strftime('%Y', strtotime($cms->TGL_IJAZAH))}</td>
				</tr>
			{else if $cmb->PENERIMAAN->ID_JENIS_FORM == '3'}
				<tr>
					<td>Asal Prodi dan Asal PT</td>
					<td>{$cmp->PRODI_S1}, {$cmp->PTN_S1}
					</td>
				</tr>
				<tr>
					<td>Pekerjaan</td>
					<td>{$cmp->PEKERJAAN}</td>
				</tr>
				<tr>
					<td>Asal Instansi</td>
					<td>{$cmp->ASAL_INSTANSI}</td>
				</tr>
			{else if $cmb->PENERIMAAN->ID_JENIS_FORM == '2'}
				<tr>
					<td>Asal Prodi dan Asal PT</td>
					<td>{$cmp->PRODI_S1}, {$cmp->PTN_S1}</td>
				</tr>
				<tr>
					<td>Pekerjaan</td>
					<td>{$cmp->PEKERJAAN}</td>
				</tr>
				<tr>
					<td>Asal Instansi</td>
					<td>{$cmp->ASAL_INSTANSI}</td>
				</tr>
				<tr>
					<td>Sumber Biaya</td>
					<td>{$cmb->SUMBER_BIAYA_2->NM_SUMBER_BIAYA}</td>
				</tr>

				<!-- S3 belum terpakai -->
			{else if $cmb->PENERIMAAN->ID_JENIS_FORM == 's3'}
				<tr>
					<td>Asal Prodi dan PT S1</td>
					<td>{$cmp->PRODI_S1}, {$cmp->PTN_S1}</td>
				</tr>
				<tr>
					<td>Asal Prodi dan PT S2</td>
					<td>{$cmp->PRODI_S2}, {$cmp->PTN_S2}</td>
				</tr>
				<tr>
					<td>Pekerjaan</td>
					<td>{$cmp->PEKERJAAN}</td>
				</tr>
				<tr>
					<td>Asal Instansi</td>
					<td>{$cmp->ASAL_INSTANSI}</td>
				</tr>
				<tr>
					<td>Sumber Biaya</td>
					<td>{$cmb->SUMBER_BIAYA_2->NM_SUMBER_BIAYA}</td>
				</tr>
				<!-- Spesialis belum terpakai -->
			{else if $cmb->JENIS_FORM == 'sp'}
				<tr>
					<td>Asal Prodi dan PT S1</td>
					<td>{$cmp->PRODI_S1}, {$cmp->PTN_S1}</td>
				</tr>
				<tr>
					<td>Asal Prodi dan PT Profesi</td>
					<td>{$cmp->PRODI_S2}, {$cmp->PTN_S2}</td>
				</tr>
			{/if}
			<tr>
				<td>Pilihan 1</td>
				<td>{$cmb->PILIHAN_1->NM_JENJANG} - {$cmb->PILIHAN_1->NM_PROGRAM_STUDI}</td>
			</tr>
			{if $cmb->JUMLAH_PRODI_MINAT > 0}
				<tr>
					<td>Pilihan Minat</td>
					<td>{if $cmp->NM_PRODI_MINAT != ''}{$cmp->NM_PRODI_MINAT}{else}<span class="text-danger"><strong>BELUM MEMILIH</strong></span>{/if}</td>
				</tr>
			{/if}
			{if $cmd->ID_KELOMPOK_BIAYA_1 > 0}
				<tr>
					<td>Kelas</td>
					<td>{$cmd->NM_KELOMPOK_BIAYA_1->NM_KELOMPOK_BIAYA}</td>
				</tr>
			{/if}
			{if $cmb->ID_PILIHAN_2 != ''}
				<tr>
					<td>Pilihan 2</td>
					<td>{$cmb->PILIHAN_2->NM_JENJANG} - {$cmb->PILIHAN_2->NM_PROGRAM_STUDI}</td>
				</tr>
			{/if}
			{if $cmb->ID_PILIHAN_3 != ''}
				<tr>
					<td>Pilihan 3</td>
					<td>{$cmb->PILIHAN_3->NM_JENJANG} - {$cmb->PILIHAN_3->NM_PROGRAM_STUDI}</td>
				</tr>
			{/if}
			{if $cmb->ID_PILIHAN_4 != ''}
				<tr>
					<td>Pilihan 4</td>
					<td>{$cmb->PILIHAN_4->NM_PROGRAM_STUDI}</td>
				</tr>
			{/if}
			<tr>
				<td>Submit Ke</td>
				<td>{$cmf->PESAN_V_BERKAS_PERNYATAAN}</td>
			</tr>
			<tr>
				<td colspan="2">
					{if $cmb->TGL_VERIFIKASI_PPMB == ''}
						{if $is_syarat_complete_check}
							{* Cek jika semua syarat terpenuhi atau tidak, beserta pengecekan prodi minat sudah memilih / belum *}
							{if $is_syarat_verified == 2 or ($cmb->JUMLAH_PRODI_MINAT > 0 and $cmp->NM_PRODI_MINAT == '')}
								<a class="btn btn-warning btn-sm" id="kembali_button" href="{site_url('verifikasi/kembalikan')}/{$cmb->ID_C_MHS}?id_penerimaan={$cmb->ID_PENERIMAAN}">Kembalikan Hasil</a>
							{else if $is_syarat_verified == 1}
								<a class="btn btn-primary btn-sm" id="sukses_button" href="{site_url('verifikasi/complete')}/{$cmb->ID_C_MHS}?id_penerimaan={$cmb->ID_PENERIMAAN}">Verifikasi Sukses</a>
							{/if}
						{/if}
						<a class="btn btn-default btn-sm" href="{site_url('verifikasi/cancel')}/{$cmb->ID_C_MHS}?id_penerimaan={$cmb->ID_PENERIMAAN}">Batalkan Proses</a>
					{else}
						<span class="label label-info">Peserta sudah terverifikasi</span>
					{/if}
				</td>
			</tr>
		</tbody>
	</table>
</div>