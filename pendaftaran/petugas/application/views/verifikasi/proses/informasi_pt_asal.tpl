<form method="post" action="{current_url()}">
	<input type="hidden" name="mode" value="pt_asal" />
	<input type="hidden" name="id_c_mhs" value="{$cmb->ID_C_MHS}" />
	
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Informasi Asal Peserta</h3>
		</div>
		<table class="table table-bordered table-condensed">
			<tbody>
				<tr>
					<td colspan="2" class="text-center">Sarjana</td>
				</tr>
				<tr>
					<td>Gelar</td>
					<td>
						<input type="text" class="form-control" name="gelar_s1" value="{$cmb->GELAR}" />
					</td>
				</tr>
				<tr>
					<td>Asal Prodi dan Asal PT</td>
					<td>{$cmp->PRODI_S1}, {$cmp->PTN_S1}</td>
				</tr>
				<tr>
					<td>Jenis PT</td>
					<td>
						<select class="form-control" name="status_ptn_s1">
							<option value=""></option>
							<option value="1" {if $cmp->STATUS_PTN_S1 != ''}{if $cmp->STATUS_PTN_S1 == 1}selected{/if}{/if}>Negeri</option>
							<option value="2" {if $cmp->STATUS_PTN_S1 != ''}{if $cmp->STATUS_PTN_S1 == 2}selected{/if}{/if}>Swasta</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>IPK</td>
					<td>
						<input type="text" class="form-control" name="ip_s1" value="{$cmp->IP_S1}" />
					</td>
				</tr>
				{if $cmb->JENIS_FORM == 's3' or $cmb->JENIS_FORM == 'sp'}
				<tr>
					<td colspan="2" class="text-center">Magister / Profesi</td>
				</tr>
				<tr>
					<td>Asal Prodi dan Asal PT</td>
					<td>{$cmp->PRODI_S2}, {$cmp->PTN_S2}</td>
				</tr>
				<tr>
					<td>Jenis PT</td>
					<td>
						<select class="form-control" name="status_ptn_s2">
							<option value=""></option>
							<option value="1" {if $cmp->STATUS_PTN_S2 != ''}{if $cmp->STATUS_PTN_S2 == 1}selected{/if}{/if}>Negeri</option>
							<option value="2" {if $cmp->STATUS_PTN_S2 != ''}{if $cmp->STATUS_PTN_S2 == 2}selected{/if}{/if}>Swasta</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>IPK</td>
					<td>
						<input type="text" class="form-control" name="ip_s2" value="{$cmp->IP_S2}" />
					</td>
				</tr>
				{/if}
				<tr>
					<td>Nilai TOEFL/ELPT</td>
					<td>
						<input type="text" class="form-control" name="nilai_toefl" value="{$cmp->NILAI_TOEFL}" />
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<button class="btn btn-primary btn-sm">Simpan</button>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	
</form>