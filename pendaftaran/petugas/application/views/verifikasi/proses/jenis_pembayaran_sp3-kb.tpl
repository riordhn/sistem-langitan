<form method="post" action="{current_url()}">
	<input type="hidden" name="mode" value="{strtolower($cmb->JENIS_PEMBAYARAN)}" />
	<input type="hidden" name="id_c_mhs" value="{$cmb->ID_C_MHS}" />
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Isian SP3</h3>
		</div>
		<table class="table table-bordered table-condensed">
			<tbody>
				{if $cmb->JENIS_FORM == 's2'}
					<tr>
						<td>
							<div class="form-group">
								<label for="is_matrikulasi">Matrikulasi</label>
								<select class="form-control" name="is_matrikulasi">
									<option value=""></option>
									<option value="1" {if $cmb->IS_MATRIKULASI == 1}selected{/if}>Ya</option>
									<option value="0" {if $cmb->IS_MATRIKULASI == 0 or $cmb->IS_MATRIKULASI == ''}selected{/if}>Tidak</option>
								</select>
							</div>
						</td>
					</tr>
				{/if}
				{if $cmb->ID_PILIHAN_1 != ''}
					{if count($cmb->PILIHAN_1->kelompok_biaya_set) > 0} {* Jika terdapat pilihan kelompok biaya *}
						<tr>
							<td>
								<div class="form-group {if form_error('id_kelompok_biaya_1')}has-error{/if}">
									<label class="control-label" for="id_kelompok_biaya_1">{$cmb->PILIHAN_1->NM_PROGRAM_STUDI}</label>
									<select class="form-control" name="id_kelompok_biaya_1">
										<option value=""></option>
										{foreach $cmb->PILIHAN_1->kelompok_biaya_set as $kelompok_biaya}
											<option value="{$kelompok_biaya->ID_KELOMPOK_BIAYA}" {if $kelompok_biaya->ID_KELOMPOK_BIAYA == $cmd->ID_KELOMPOK_BIAYA_1}selected{/if}>{$kelompok_biaya->NM_KELOMPOK_BIAYA}</option>
										{/foreach}
									</select>
									{form_error('id_kelompok_biaya_1')}
								</div>
								<div class="form-group">
									<label class="control-label" id="sop_display">SOP: {if !empty($sop_sp3)}Rp {$sop_sp3[0]->BESAR_BIAYA}{/if}</label>
								</div>
								<div class="form-group">
									<label class="control-label" id="sp3_display">SP3: {if !empty($sop_sp3[1]->BESAR_BIAYA)}Rp {$sop_sp3[1]->BESAR_BIAYA}{/if}</label>
								</div>
								<div class="form-group {if form_error('sp3_1')}has-error{/if}">
									<div class="input-group">
										<span class="input-group-addon">Rp</span>
										<input type="text" class="form-control" style="width: 130px; margin-right: 5px" id="sp3_1" name="sp3_1" data-min="" autocomplete="off" value="{set_value('sp3_1', $cmb->SP3_1)}"/>
										<input type="hidden" name="sp3_1_min" value="" />
									</div>
									{form_error('sp3_1')}
								</div>
							</td>
						</tr>
					{else}
						<tr>
							<td>
								<div class="form-group {if form_error('sp3_1')}has-error{/if} {if $cmb->SP3_1 != ''}has-success{/if}">
									<label class="control-label" for="sp3_1">{$cmb->PILIHAN_1->NM_PROGRAM_STUDI} : <span class="text-danger">Rp {number_format($cmb->PILIHAN_1->MINIMAL_BIAYA, 0, ",", ".")}</span></label>
									{* Cek jika SP3 tidak ada, maka ditampilkan info ke petugas *}
									{if $cmb->PILIHAN_1->MINIMAL_BIAYA > 0}
										<div class="input-group">
											<span class="input-group-addon">Rp</span>
											<input type="text" class="form-control" style="width: 130px; margin-right: 5px" id="sp3_1" name="sp3_1" data-min="{$cmb->PILIHAN_1->MINIMAL_BIAYA}" autocomplete="off" value="{set_value('sp3_1', $cmb->SP3_1)}"/>
											<input type="hidden" name="sp3_1_min" value="{$cmb->PILIHAN_1->MINIMAL_BIAYA}" />
										</div>
									{else}
										<div class="input-group">
											<p class="form-control-static text-danger">BIAYA KULIAH BELUM DI SET OLEH KEUANGAN ! PROSES VERIFIKASI ISIAN SP3 TIDAK BISA DILANJUTKAN.</p>
										</div>
									{/if}
									{form_error('sp3_1')}
								</div>
							</td>
						</tr>
					{/if}
				{/if}
				<tr>
					<td>
						<button class="btn btn-primary btn-sm">Simpan</button>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</form>