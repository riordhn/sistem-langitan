<form method="post" action="{current_url()}">
	<input type="hidden" name="mode" value="{strtolower($cmb->JENIS_PEMBAYARAN)}" />
	<input type="hidden" name="id_c_mhs" value="{$cmb->ID_C_MHS}" />
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Isian SP3</h3>
		</div>
		<table class="table table-bordered table-condensed">
			<tbody>
				{if $cmb->JENIS_FORM == 's1-aj' or $cmb->JENIS_FORM == 's2' or $cmb->JENIS_FORM == 's3' or $cmb->JENIS_FORM == 'sp'}
					<tr>
						<td>
							<div class="form-group">
								<label for="is_matrikulasi">Matrikulasi</label>
								<select class="form-control" name="is_matrikulasi">
									<option value=""></option>
									<option value="1" {if $cmb->IS_MATRIKULASI == 1}selected{/if}>Ya</option>
									<option value="0" {if $cmb->IS_MATRIKULASI == 0 or $cmb->IS_MATRIKULASI == ''}selected{/if}>Tidak</option>
								</select>
							</div>
						</td>
					</tr>
				{/if}
				{if $cmb->ID_PILIHAN_1 != ''}
					<tr>
						<td>
							<input type="hidden" name="sp3_1_min" value="{$cmb->PILIHAN_1->MINIMAL_BIAYA}" />
							<div class="form-group {if form_error('sp3_1')}has-error{/if} {if $cmb->SP3_1 != ''}has-success{/if}">
								<label for="sp3_1">{$cmb->PILIHAN_1->NM_PROGRAM_STUDI} : <span class="text-danger">Rp {number_format($cmb->PILIHAN_1->MINIMAL_BIAYA, 0, ",", ".")}</span></label>
								<div class="input-group">
									<span class="input-group-addon">Rp</span>
									<input type="text" class="form-control" style="width: 130px; margin-right: 5px" id="sp3_1" name="sp3_1" data-min="{$cmb->PILIHAN_1->MINIMAL_BIAYA}" autocomplete="off" value="{set_value('sp3_1', $cmb->SP3_1)}"/>
								</div>
								{form_error('sp3_1')}
							</div>
						</td>
					</tr>
				{/if}
				{if $cmb->ID_PILIHAN_2 != ''}
					<tr>
						<td>
							<input type="hidden" name="sp3_2_min" value="{$cmb->PILIHAN_2->MINIMAL_BIAYA}" />
							<div class="form-group {if form_error('sp3_2')}has-error{/if} {if $cmb->SP3_2 != ''}has-success{/if}">
								<label for="sp3_2">{$cmb->PILIHAN_2->NM_PROGRAM_STUDI} : <span class="text-danger">Rp {number_format($cmb->PILIHAN_2->MINIMAL_BIAYA, 0, ",", ".")}</span></label>
								<div class="input-group">
									<span class="input-group-addon">Rp</span>
									<input type="text" class="form-control" style="width: 130px; margin-right: 5px" id="sp3_2" name="sp3_2" data-min="{$cmb->PILIHAN_2->MINIMAL_BIAYA}" autocomplete="off" value="{set_value('sp3_2', $cmb->SP3_2)}"/>
								</div>
								{form_error('sp3_2')}
							</div>
						</td>
					</tr>
				{/if}
				{if $cmb->ID_PILIHAN_3 != ''}
					<tr>
						<td>
							<input type="hidden" name="sp3_3_min" value="{$cmb->PILIHAN_3->MINIMAL_BIAYA}" />
							<div class="form-group {if form_error('sp3_3')}has-error{/if} {if $cmb->SP3_3 != ''}has-success{/if}">
								<label for="sp3_3">{$cmb->PILIHAN_3->NM_PROGRAM_STUDI} : <span class="text-danger">Rp {number_format($cmb->PILIHAN_3->MINIMAL_BIAYA, 0, ",", ".")}</span></label>
								<div class="input-group">
									<span class="input-group-addon">Rp</span>
									<input type="text" class="form-control" style="width: 130px; margin-right: 5px" id="sp3_3" name="sp3_3" data-min="{$cmb->PILIHAN_3->MINIMAL_BIAYA}" autocomplete="off" value="{set_value('sp3_3', $cmb->SP3_3)}"/>
								</div>
								{form_error('sp3_3')}
							</div>
						</td>
					</tr>
				{/if}
				{if $cmb->ID_PILIHAN_4 != ''}
					<tr>
						<td>
							<input type="hidden" name="sp3_4_min" value="{$cmb->PILIHAN_4->MINIMAL_BIAYA}" />
							<div class="form-group {if form_error('sp3_4')}has-error{/if} {if $cmb->SP3_4 != ''}has-success{/if}">
								<label for="sp3_4">{$cmb->PILIHAN_4->NM_PROGRAM_STUDI} : <span class="text-danger">Rp {number_format($cmb->PILIHAN_4->MINIMAL_BIAYA, 0, ",", ".")}</span></label>
								<div class="input-group">
									<span class="input-group-addon">Rp</span>
									<input type="text" class="form-control" style="width: 130px; margin-right: 5px" id="sp3_4" name="sp3_4" data-min="{$cmb->PILIHAN_4->MINIMAL_BIAYA}" autocomplete="off" value="{set_value('sp3_4', $cmb->SP3_4)}"/>
								</div>
								{form_error('sp3_4')}
							</div>
						</td>
					</tr>
				{/if}
				<tr>
					<td>
						<button class="btn btn-primary btn-sm">Simpan</button>
					</td>
				</tr>
			</tbody>
		</table>

	</div>
</form>