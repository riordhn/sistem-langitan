{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Daftar Peserta Sudah Verifikasi Penerimaan {$penerimaan->NM_PENERIMAAN} Tahun {$penerimaan->TAHUN} <small>Gelombang {$penerimaan->GELOMBANG}</small></h2>
				<a href="{site_url('verifikasi/list_penerimaan')}">Kembali ke daftar penerimaan</a>
			</div>
			
			<table class="table table-bordered table-condensed table-hover">
				<thead>
					<tr>
						<th style='width: 50px' class="text-center">#</th>
						<th>Nama</th>
						<th>Kode Voucher</th>
						<th>Email</th>
						<th style='width: 200px' class="text-center">Waktu Verifikasi</th>
						<th>Petugas</th>
						<th>UKT 1</th>
						<th>UKT 2</th>
						<th>UKT 3</th>
						<th>UKT 4</th>
					</tr>
				</thead>
				<tbody>
					{foreach $cmb_set as $cmb}
						<tr>
							<td class="text-center">{$cmb@index + 1}</td>
							<td><a href="{site_url('verifikasi/proses_lihat')}/{$cmb->ID_C_MHS}">{$cmb->NM_C_MHS}</a></td>
							<td>{$cmb->KODE_VOUCHER}</td>
							<td>{$cmb->EMAIL}</td>
							<td class="text-center">
								<span title="{$cmb->TGL_VERIFIKASI_PPMB}">{get_interval_date($cmb->TGL_VERIFIKASI_PPMB)}</span>
							</td>
							<td>{$cmb->NM_PENGGUNA}</td>
							<td>{if $cmb->ID_KELOMPOK_BIAYA_1== '280'}6A{else if $cmb->ID_KELOMPOK_BIAYA_1== '279'}6B{else if $cmb->ID_KELOMPOK_BIAYA_1== '278'}6C{/if}</td>
							<td>{if $cmb->ID_KELOMPOK_BIAYA_2== '280'}6A{else if $cmb->ID_KELOMPOK_BIAYA_2== '279'}6B{else if $cmb->ID_KELOMPOK_BIAYA_2== '278'}6C{/if}</td>
							<td>{if $cmb->ID_KELOMPOK_BIAYA_3== '280'}6A{else if $cmb->ID_KELOMPOK_BIAYA_3== '279'}6B{else if $cmb->ID_KELOMPOK_BIAYA_3== '278'}6C{/if}</td>
							<td>{if $cmb->ID_KELOMPOK_BIAYA_4== '280'}6A{else if $cmb->ID_KELOMPOK_BIAYA_4== '279'}6B{else if $cmb->ID_KELOMPOK_BIAYA_4== '278'}6C{/if}</td>
						</tr>
					{/foreach}
				</tbody>
			</table>
			
		</div>
	</div>
{/block}