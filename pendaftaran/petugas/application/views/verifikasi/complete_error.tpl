{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Proses Verifikasi</h2>
			</div>
			
			<div class="alert alert-danger">
				<strong>{$title}</strong>
				<br/>{$message}
				<br/><a class="alert-link" href="{site_url('verifikasi/proses')}/{$id_c_mhs}">Kembali ke halaman verifikasi</a>
			</div>
		</div>
	</div>
{/block}