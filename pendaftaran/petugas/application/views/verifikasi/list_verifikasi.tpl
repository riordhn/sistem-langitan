{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Daftar Peserta Sudah Verifikasi Penerimaan {$penerimaan->NM_PENERIMAAN} Tahun {$penerimaan->TAHUN} <small>Gelombang {$penerimaan->GELOMBANG}</small></h2>
				<a href="{site_url('verifikasi/list_penerimaan')}">Kembali ke daftar penerimaan</a>
			</div>
			
			<table class="table table-bordered table-condensed table-hover">
				<thead>
					<tr>
						<th style='width: 50px' class="text-center">#</th>
						<th>Nama</th>
						<th>Email</th>
						<th>Kode Voucher</th>
						<th>Pilihan</th>
						<th style='width: 200px' class="text-center">Waktu Verifikasi</th>
						<th>Petugas</th>
					</tr>
				</thead>
				<tbody>
					{foreach $cmb_set as $cmb}
						<tr>
							<td class="text-center">{$cmb@index + 1}</td>
							<td>{$cmb->NM_C_MHS}</td>
							<td>{$cmb->EMAIL}</td>
							<td>{$cmb->KODE_VOUCHER}</td>
							<td>{$cmb->NM_PROGRAM_STUDI_1}{if $cmb->NM_PROGRAM_STUDI_2!=''}<br>{$cmb->NM_PROGRAM_STUDI_2}{/if}</td>
							<td class="text-center">
								<span title="{$cmb->TGL_VERIFIKASI_PPMB}">{get_interval_date($cmb->TGL_VERIFIKASI_PPMB)}</span>
							</td>
							<td>{$cmb->NM_PENGGUNA}</td>
						</tr>
					{/foreach}
				</tbody>
			</table>
			
		</div>
	</div>
{/block}