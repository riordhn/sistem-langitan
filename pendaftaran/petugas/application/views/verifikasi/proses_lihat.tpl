{extends file='home_layout.tpl'}
{block name='head'}
	<!-- fancyBox CSS-->
	<link rel="stylesheet" href="{base_url('assets/fancybox/source/jquery.fancybox.css?v=2.1.5')}" type="text/css" media="screen" />
	<link rel="stylesheet" href="{base_url('assets/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5')}" type="text/css" media="screen" />
{/block}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Proses Verifikasi</h2>
				<a href="{site_url('verifikasi/list_antrian')}/{$cmb->ID_PENERIMAAN}">Kembali ke daftar antrian</a>
			</div>

			<div class="row">
				<div class="col-md-4" id="biodata">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Informasi Peserta</h3>
						</div>
						<table class="table table-bordered table-condensed">
							<tbody>
								<tr>
									<td>Nama</td>
									<td>{$cmb->NM_C_MHS}</td>
								</tr>
								{if $cmb->JENIS_FORM == 's1'}
									<tr>
										<td>Asal Sekolah</td>
										<td>
											{$cms->SEKOLAH_ASAL->NM_SEKOLAH}
										</td>
									</tr>
									<tr>
										<td>Tahun Lulus</td>
										<td>{strftime('%Y', strtotime($cms->TGL_IJAZAH))}</td>
									</tr>
								{else if $cmb->JENIS_FORM == 's1-aj'}
									<tr>
										<td>Asal Prodi dan Asal PT</td>
										<td>
											{$cmp->PRODI_S1}, {$cmp->PTN_S1}
										</td>
									</tr>
									<tr>
										<td>Pekerjaan</td>
										<td>{$cmp->PEKERJAAN}</td>
									</tr>
									<tr>
										<td>Asal Instansi</td>
										<td>{$cmp->ASAL_INSTANSI}</td>
									</tr>
								{else if $cmb->JENIS_FORM == 's2'}
									<tr>
										<td>Asal Prodi dan Asal PT</td>
										<td>
											{$cmp->PRODI_S1}, {$cmp->PTN_S1}
										</td>
									</tr>
									<tr>
										<td>Pekerjaan</td>
										<td>{$cmp->PEKERJAAN}</td>
									</tr>
									<tr>
										<td>Asal Instansi</td>
										<td>{$cmp->ASAL_INSTANSI}</td>
									</tr>
								{else if $cmb->JENIS_FORM == 's3'}
									<tr>
										<td>Asal Prodi dan PT S1</td>
										<td>
											{$cmp->PRODI_S1}, {$cmp->PTN_S1}
										</td>
									</tr>
									<tr>
										<td>Asal Prodi dan PT S2</td>
										<td>
											{$cmp->PRODI_S2}, {$cmp->PTN_S2}
										</td>
									</tr>
									<tr>
										<td>Pekerjaan</td>
										<td>{$cmp->PEKERJAAN}</td>
									</tr>
									<tr>
										<td>Asal Instansi</td>
										<td>{$cmp->ASAL_INSTANSI}</td>
									</tr>
								{else if $cmb->JENIS_FORM == 'sp'}
									<tr>
										<td>Asal Prodi dan PT S1</td>
										<td>
											{$cmp->PRODI_S1}, {$cmp->PTN_S1}
										</td>
									</tr>
									<tr>
										<td>Asal Prodi dan PT Profesi</td>
										<td>
											{$cmp->PRODI_S2}, {$cmp->PTN_S2}
										</td>
									</tr>
								{/if}
								<tr>
									<td>Pilihan 1</td>
									<td>{$cmb->PILIHAN_1->NM_PROGRAM_STUDI}</td>
								</tr>
								{if $cmb->ID_PILIHAN_2 != ''}
									<tr>
										<td>Pilihan 2</td>
										<td>{$cmb->PILIHAN_2->NM_PROGRAM_STUDI}</td>
									</tr>
								{/if}
								{if $cmb->ID_PILIHAN_3 != ''}
									<tr>
										<td>Pilihan 3</td>
										<td>{$cmb->PILIHAN_3->NM_PROGRAM_STUDI}</td>
									</tr>
								{/if}
								{if $cmb->ID_PILIHAN_4 != ''}
									<tr>
										<td>Pilihan 4</td>
										<td>{$cmb->PILIHAN_4->NM_PROGRAM_STUDI}</td>
									</tr>
								{/if}
								<tr>
									<td colspan="2">
										{if $cmb->TGL_VERIFIKASI_PPMB == ''}
										{else}
											<span class="label label-info">Peserta sudah terverifikasi</span>
										{/if}
									</td>
								</tr>
							</tbody>
						</table>
					</div>
									
					{if $cmb->JENIS_PEMBAYARAN == 'UKT'}
						<form method="post" action="{current_url()}">
							<input type="hidden" name="mode" value="ukt" />
							<input type="hidden" name="id_c_mhs" value="{$cmb->ID_C_MHS}" />
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Pilihan UKT</h3>
								</div>
								<table class="table table-bordered table-condensed">
									<tbody>
										{if $cmb->ID_PILIHAN_1 != ''}
											<tr>
												<td>
													<div class="form-group {if form_error('id_kelompok_biaya_1')}has-error{/if}">
														<label for="id_kelompok_biaya_1">{$cmb->PILIHAN_1->NM_PROGRAM_STUDI}</label>
														<select class="form-control" name="id_kelompok_biaya_1">
															<option value=""></option>
															{foreach $cmb->PILIHAN_1->kelompok_biaya_set as $kb}
																<option value="{$kb->ID_KELOMPOK_BIAYA}" {if $kb->ID_KELOMPOK_BIAYA == $cmd->ID_KELOMPOK_BIAYA_1}selected{/if}>{$kb->NM_KELOMPOK_BIAYA}</option>
															{/foreach}
														</select>
														{form_error('id_kelompok_biaya_1')}
													</div>
												</td>
											</tr>
										{/if}
										{if $cmb->ID_PILIHAN_2 != ''}
											<tr>
												<td>
													<div class="form-group {if form_error('id_kelompok_biaya_2')}has-error{/if}">
														<label for="id_kelompok_biaya_2">{$cmb->PILIHAN_2->NM_PROGRAM_STUDI}</label>
														<select class="form-control" name="id_kelompok_biaya_2">
															<option value=""></option>
															{foreach $cmb->PILIHAN_2->kelompok_biaya_set as $kb}
																<option value="{$kb->ID_KELOMPOK_BIAYA}" {if $kb->ID_KELOMPOK_BIAYA == $cmd->ID_KELOMPOK_BIAYA_2}selected{/if}>{$kb->NM_KELOMPOK_BIAYA}</option>
															{/foreach}
														</select>
														{form_error('id_kelompok_biaya_2')}
													</div>
												</td>
											</tr>
										{/if}
										{if $cmb->ID_PILIHAN_3 != ''}
											<tr>
												<td>
													<div class="form-group {if form_error('id_kelompok_biaya_3')}has-error{/if}">
														<label for="id_kelompok_biaya_3">{$cmb->PILIHAN_3->NM_PROGRAM_STUDI}</label>
														<select class="form-control" name="id_kelompok_biaya_3">
															<option value=""></option>
															{foreach $cmb->PILIHAN_3->kelompok_biaya_set as $kb}
																<option value="{$kb->ID_KELOMPOK_BIAYA}" {if $kb->ID_KELOMPOK_BIAYA == $cmd->ID_KELOMPOK_BIAYA_3}selected{/if}>{$kb->NM_KELOMPOK_BIAYA}</option>
															{/foreach}
														</select>
														{form_error('id_kelompok_biaya_3')}
													</div>
												</td>
											</tr>
										{/if}
										{if $cmb->ID_PILIHAN_4 != ''}
											<tr>
												<td>
													<div class="form-group {if form_error('id_kelompok_biaya_4')}has-error{/if}">
														<label for="id_kelompok_biaya_4">{$cmb->PILIHAN_4->NM_PROGRAM_STUDI}</label>
														<select class="form-control" name="id_kelompok_biaya_4">
															<option value=""></option>
															{foreach $cmb->PILIHAN_4->kelompok_biaya_set as $kb}
																<option value="{$kb->ID_KELOMPOK_BIAYA}" {if $kb->ID_KELOMPOK_BIAYA == $cmd->ID_KELOMPOK_BIAYA_4}selected{/if}>{$kb->NM_KELOMPOK_BIAYA}</option>
															{/foreach}
														</select>
														{form_error('id_kelompok_biaya_4')}
													</div>
												</td>
											</tr>
										{/if}
									</tbody>
								</table>
							</div>
						</form>
					{/if}
					
					{if $cmb->JENIS_PEMBAYARAN == 'SP3'}
						<form method="post" action="{current_url()}">
						<input type="hidden" name="mode" value="sp3" />
						<input type="hidden" name="id_c_mhs" value="{$cmb->ID_C_MHS}" />
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Isian SP3</h3>
							</div>
							<table class="table table-bordered table-condensed">
								<tbody>
									{if $cmb->JENIS_FORM == 's1-aj' or $cmb->JENIS_FORM == 's2' or $cmb->JENIS_FORM == 's3' or $cmb->JENIS_FORM == 'sp'}
										<tr>
											<td>
													
												<div class="form-group">
													<label for="is_matrikulasi">Matrikulasi</label>
													<select class="form-control" name="is_matrikulasi">
														<option value=""></option>
														<option value="1" {if $cmb->IS_MATRIKULASI === 1}selected{/if}>Ya</option>
														<option value="0" {if $cmb->IS_MATRIKULASI === 0}selected{/if}>Tidak</option>
													</select>
												</div>
											</td>
										</tr>
									{/if}
									{if $cmb->ID_PILIHAN_1 != ''}
										<tr>
											<td>
												<input type="hidden" name="sp3_1_min" value="{$cmb->PILIHAN_1->MINIMAL_BIAYA}" />
												<div class="form-group {if form_error('sp3_1')}has-error{/if} {if $cmb->SP3_1 != ''}has-success{/if}">
													<label for="sp3_1">{$cmb->PILIHAN_1->NM_PROGRAM_STUDI} : <span class="text-danger">Rp {number_format($cmb->PILIHAN_1->MINIMAL_BIAYA, 0, ",", ".")}</span></label>
													<div class="input-group">
														<span class="input-group-addon">Rp</span>
														<input type="text" class="form-control" style="width: 130px; margin-right: 5px" id="sp3_1" name="sp3_1" data-min="{$cmb->PILIHAN_1->MINIMAL_BIAYA}" autocomplete="off" value="{set_value('sp3_1', $cmb->SP3_1)}"/>
													</div>
													{form_error('sp3_1')}
												</div>
											</td>
										</tr>
									{/if}
									{if $cmb->ID_PILIHAN_2 != ''}
										<tr>
											<td>
												<input type="hidden" name="sp3_2_min" value="{$cmb->PILIHAN_2->MINIMAL_BIAYA}" />
												<div class="form-group {if form_error('sp3_2')}has-error{/if} {if $cmb->SP3_2 != ''}has-success{/if}">
													<label for="sp3_2">{$cmb->PILIHAN_2->NM_PROGRAM_STUDI} : <span class="text-danger">Rp {number_format($cmb->PILIHAN_2->MINIMAL_BIAYA, 0, ",", ".")}</span></label>
													<div class="input-group">
														<span class="input-group-addon">Rp</span>
														<input type="text" class="form-control" style="width: 130px; margin-right: 5px" id="sp3_2" name="sp3_2" data-min="{$cmb->PILIHAN_2->MINIMAL_BIAYA}" autocomplete="off" value="{set_value('sp3_2', $cmb->SP3_2)}"/>
													</div>
													{form_error('sp3_2')}
												</div>
											</td>
										</tr>
									{/if}
									{if $cmb->ID_PILIHAN_3 != ''}
										<tr>
											<td>
												<input type="hidden" name="sp3_3_min" value="{$cmb->PILIHAN_3->MINIMAL_BIAYA}" />
												<div class="form-group {if form_error('sp3_3')}has-error{/if} {if $cmb->SP3_3 != ''}has-success{/if}">
													<label for="sp3_3">{$cmb->PILIHAN_3->NM_PROGRAM_STUDI} : <span class="text-danger">Rp {number_format($cmb->PILIHAN_3->MINIMAL_BIAYA, 0, ",", ".")}</span></label>
													<div class="input-group">
														<span class="input-group-addon">Rp</span>
														<input type="text" class="form-control" style="width: 130px; margin-right: 5px" id="sp3_3" name="sp3_3" data-min="{$cmb->PILIHAN_3->MINIMAL_BIAYA}" autocomplete="off" value="{set_value('sp3_3', $cmb->SP3_3)}"/>
													</div>
													{form_error('sp3_3')}
												</div>
											</td>
										</tr>
									{/if}
									{if $cmb->ID_PILIHAN_4 != ''}
										<tr>
											<td>
												<input type="hidden" name="sp3_4_min" value="{$cmb->PILIHAN_4->MINIMAL_BIAYA}" />
												<div class="form-group {if form_error('sp3_4')}has-error{/if} {if $cmb->SP3_4 != ''}has-success{/if}">
													<label for="sp3_4">{$cmb->PILIHAN_4->NM_PROGRAM_STUDI} : <span class="text-danger">Rp {number_format($cmb->PILIHAN_4->MINIMAL_BIAYA, 0, ",", ".")}</span></label>
													<div class="input-group">
														<span class="input-group-addon">Rp</span>
														<input type="text" class="form-control" style="width: 130px; margin-right: 5px" id="sp3_4" name="sp3_4" data-min="{$cmb->PILIHAN_4->MINIMAL_BIAYA}" autocomplete="off" value="{set_value('sp3_4', $cmb->SP3_4)}"/>
													</div>
													{form_error('sp3_4')}
												</div>
											</td>
										</tr>
									{/if}
									<tr>
										<td>
											<button class="btn btn-primary btn-sm">Simpan</button>
										</td>
									</tr>
								</tbody>
							</table>

						</div>
					</form>
					{/if}
				</div>

				<div class="col-md-8" id="data_file">
					
					<div class="panel panel-default">
						<table class="table table-bordered table-condensed">
							<tbody>
								<tr>
									<td class="text-center">
										Pesan Optional
									</td>
									<td>
										<form action="{current_url()}" method="post">
											<input type="hidden" name="mode" value="pesan_optional" />
											<input type="hidden" name="id_c_mhs" value="{$cmb->ID_C_MHS}" />
											<div class="form-group">
												<input type="text" class="form-control" name="pesan_v_optional" value="{$cmb->PESAN_VERIFIKATOR}"/>
											</div>
										</form>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="panel-heading">
							<h3 class="panel-title">Berkas Persyaratan Umum</h3>
						</div>
						<table class="table table-bordered table-condensed">
							<tbody>
								<tr>
									<td class="text-center" style="width: 160px">
										{if $cmf->FILE_FOTO != ''}
											<a id="foto-{$cmb->ID_C_MHS}" class="img-popup" rel="gallery" href="{base_url('files')}/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$cmf->FILE_FOTO}" data-fancybox-group="button" title="File Pas Foto">
												<img src="{base_url('files')}/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$cmf->FILE_FOTO}?t={time()}" alt="Pas Foto" style="height: 100px" />
											</a>
											<br/>
										{/if}
										File Foto
									</td>
									<td>
										{if $cmf->FILE_FOTO != ''}
											{if $cmf->IS_FOTO_VERIFIED == 0}
												<form action="{current_url()}" method="post">
													<input type="hidden" name="mode" value="foto" />
													<input type="hidden" name="id_c_mhs" value="{$cmb->ID_C_MHS}" />
													<div class="form-group">
														<input type="text" class="form-control" name="pesan_v_foto" value="{$cmf->PESAN_V_FOTO}"/>
													</div>
													<div class="form-group">
														<button class="btn btn-sm {if $cmf->PESAN_V_FOTO ==''}btn-danger{else}btn-default{/if} pull-left" name="valid" value="0" data-field="pesan_v_foto"><span class="glyphicon glyphicon-remove"></span> Tidak Valid</button>
														<button class="btn btn-sm {if $cmf->PESAN_V_FOTO ==''}btn-success{else}btn-default{/if} pull-right" name="valid" value="1"><span class="glyphicon glyphicon-ok"></span> Valid</button>
													</div>
													
													<br/><br/>
													<div class="form-group">
														<button type="button" class="btn btn-default btn-sm btn-rotate" data-rotate-mode="foto" data-file="{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$cmf->FILE_FOTO}" data-id-c-mhs="{$cmb->ID_C_MHS}" data-rotate="-90">
															<span class="glyphicon glyphicon-repeat"></span> Putar -90&deg;
														</button>
														<button type="button" class="btn btn-default btn-sm btn-rotate" data-rotate-mode="foto" data-file="{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$cmf->FILE_FOTO}" data-id-c-mhs="{$cmb->ID_C_MHS}" data-rotate="90">
															<span class="glyphicon glyphicon-repeat"></span> Putar 90&deg;
														</button>
													</div>
															
												</form>
											{else}
												<h3 class="text-center"><span class="label label-success">Valid</span></h3>
											{/if}
										{/if}
									</td>
								</tr>
								{* BERKAS PERNYATAAN is Deprecated *}
								{*
								<tr>
									<td class="text-center">
										{if $cmf->FILE_BERKAS_PERNYATAAN != ''}
											<a id="berkas-pernyataan-{$cmb->ID_C_MHS}" class="img-popup" rel="gallery" href="{base_url()}files/{$cmb->ID_C_MHS}_berkas.jpg" data-fancybox-group="button" title="Berkas Persyaratan">
												<img src="{base_url()}files/{$cmb->ID_C_MHS}_berkas.jpg?t={time()}" style="height: 100px" />
											</a>
											<br/>
										{/if}
										Surat Pernyataan Kebenaran Pengisian Biodata
									</td>
									<td>
										{if $cmf->FILE_BERKAS_PERNYATAAN != ''}
											{if $cmf->IS_BERKAS_PERNYATAAN_VERIFIED == 0}
												<form action="{current_url()}" method="post">
													<input type="hidden" name="mode" value="berkas_pernyataan" />
													<input type="hidden" name="id_c_mhs" value="{$cmb->ID_C_MHS}" />
													<div class="form-group">
														<input type="text" class="form-control" name="pesan_v_berkas_pernyataan" value="{$cmf->PESAN_V_BERKAS_PERNYATAAN}"/>
													</div>
													<div class="form-group">
														<button class="btn btn-sm {if $cmf->PESAN_V_BERKAS_PERNYATAAN ==''}btn-danger{else}btn-default{/if} pull-left" name="valid" value="0" data-field="pesan_v_berkas_pernyataan"><span class="glyphicon glyphicon-remove"></span> Tidak Valid</button>
														<button class="btn btn-sm {if $cmf->PESAN_V_BERKAS_PERNYATAAN ==''}btn-success{else}btn-default{/if} pull-right" name="valid" value="1"><span class="glyphicon glyphicon-ok"></span> Valid</button>
													</div>
													
													<br/>
													<br/>
													<div class="form-group">
														<button type="button" class="btn btn-default btn-sm btn-rotate" data-rotate-mode="berkas-pernyataan" data-file="{$cmf->FILE_BERKAS_PERNYATAAN}" data-id-c-mhs="{$cmb->ID_C_MHS}" data-rotate="-90">
															<span class="glyphicon glyphicon-repeat"></span> Putar -90&deg;
														</button>
														<button type="button" class="btn btn-default btn-sm btn-rotate" data-rotate-mode="berkas-pernyataan" data-file="{$cmf->FILE_BERKAS_PERNYATAAN}" data-id-c-mhs="{$cmb->ID_C_MHS}" data-rotate="90">
															<span class="glyphicon glyphicon-repeat"></span> Putar 90&deg;
														</button>
													</div>
												</form>
											{else}
												<h3 class="text-center"><span class="label label-success">Valid</span></h3>
											{/if}
										{/if}
									</td>
								</tr>
								*}
								{if count($syarat_umum_set) > 0}
									{foreach $syarat_umum_set as $su}
									<tr>
										<td class="text-center">
											<a id="syarat-{$cmb->ID_C_MHS}-{$su->ID_SYARAT_PRODI}" class="img-popup" rel="gallery" href="{base_url('files')}/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$su->FILE_SYARAT}" data-fancybox-group="button" title="{$su->NM_SYARAT}">
												{if $su->FILE_SYARAT != ''}
													<img src="{base_url('files')}/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$su->FILE_SYARAT}?t={time()}" style="height: 100px" />
												{/if}
											</a>
											<br/>
											{$su->NM_SYARAT}
										</td>
										<td>
											{if $su->FILE_SYARAT != ''}
												{if $su->IS_VERIFIED == 0}
													<form action="{current_url()}" method="post">
														<input type="hidden" name="mode" value="syarat" />
														<input type="hidden" name="id_c_mhs" value="{$cmb->ID_C_MHS}" />
														<input type="hidden" name="id_syarat_prodi" value="{$su->ID_SYARAT_PRODI}" />
														<div class="form-group">
															<input type="text" class="form-control" name="pesan_verifikator_{$su->ID_SYARAT_PRODI}" value="{$su->PESAN_VERIFIKATOR}"/>
														</div>
														<div class="form-group">
															<button class="btn btn-sm {if $su->PESAN_VERIFIKATOR ==''}btn-danger{else}btn-default{/if} pull-left" name="valid" value="0" data-field="pesan_verifikator_{$su->ID_SYARAT_PRODI}"><span class="glyphicon glyphicon-remove"></span> Tidak Valid</button>
															<button class="btn btn-sm {if $su->PESAN_VERIFIKATOR ==''}btn-success{else}btn-default{/if} pull-right" name="valid" value="1"><span class="glyphicon glyphicon-ok"></span> Valid</button>
														</div>
														
														<br/>
														<br/>
														<div class="form-group">
															<button type="button" class="btn btn-default btn-sm btn-rotate" data-rotate-mode="syarat" data-file="{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$su->FILE_SYARAT}" data-id-c-mhs="{$cmb->ID_C_MHS}" data-id-syarat-prodi="{$su->ID_SYARAT_PRODI}" data-rotate="-90">
																<span class="glyphicon glyphicon-repeat"></span> Putar -90&deg;
															</button>
															<button type="button" class="btn btn-default btn-sm btn-rotate" data-rotate-mode="syarat" data-file="{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$su->FILE_SYARAT}" data-id-c-mhs="{$cmb->ID_C_MHS}" data-id-syarat-prodi="{$su->ID_SYARAT_PRODI}" data-rotate="90">
																<span class="glyphicon glyphicon-repeat"></span> Putar 90&deg;
															</button>
														</div>
													</form>
												{else}
													<h3 class="text-center"><span class="label label-success">Valid</span></h3>
												{/if}
											{/if}
										</td>
									</tr>
									{/foreach}
								{/if}
							</tbody>
						</table>
					</div>	
							
					{if count($syarat_prodi_set) > 0}
						
						{foreach $syarat_prodi_set as $ps} {* $ps = Program_Studi *}
							{if count($ps->syarat_set) > 0}
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">Berkas Persyaratan Khusus Program Studi : {$ps->NM_PROGRAM_STUDI}</h3>
									</div>
									<table class="table table-bordered table-condensed">
										<tbody>
											{foreach $ps->syarat_set as $sp} {* $sp = Syarat_Prodi *}
											<tr>
												<td class="text-center" style="width: 160px">
													<a id="syarat-{$cmb->ID_C_MHS}-{$sp->ID_SYARAT_PRODI}" class="img-popup" rel="gallery" href="{base_url()}files/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$sp->FILE_SYARAT}" data-fancybox-group="button" title="{$sp->NM_SYARAT}">
														{if $sp->FILE_SYARAT != ''}
															<img src="{base_url()}files/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$sp->FILE_SYARAT}?t={time()}" style="height: 100px" />
														{/if}
													</a>
													<br/>
													{$sp->NM_SYARAT}
												</td>
												<td>
													{if $sp->FILE_SYARAT != ''}
														{if $sp->IS_VERIFIED == 0}
															<form action="{current_url()}" method="post">
																<input type="hidden" name="mode" value="syarat" />
																<input type="hidden" name="id_c_mhs" value="{$cmb->ID_C_MHS}" />
																<input type="hidden" name="id_syarat_prodi" value="{$sp->ID_SYARAT_PRODI}" />
																<div class="form-group">
																	<input type="text" class="form-control" name="pesan_verifikator_{$sp->ID_SYARAT_PRODI}" value="{$sp->PESAN_VERIFIKATOR}"/>
																</div>
																<div class="form-group">
																	<button class="btn btn-sm {if $sp->PESAN_VERIFIKATOR ==''}btn-danger{else}btn-default{/if} pull-left" name="valid" value="0" data-field="pesan_verifikator_{$sp->ID_SYARAT_PRODI}"><span class="glyphicon glyphicon-remove"></span> Tidak Valid</button>
																	<button class="btn btn-sm {if $sp->PESAN_VERIFIKATOR ==''}btn-success{else}btn-default{/if} pull-right" name="valid" value="1"><span class="glyphicon glyphicon-ok"></span> Valid</button>
																</div>
																<br/>
																<br/>
																<div class="form-group">
																	<button type="button" class="btn btn-default btn-sm btn-rotate" data-rotate-mode="syarat" data-file="{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$sp->FILE_SYARAT}" data-id-c-mhs="{$cmb->ID_C_MHS}" data-id-syarat-prodi="{$sp->ID_SYARAT_PRODI}" data-rotate="-90">
																		<span class="glyphicon glyphicon-repeat"></span> Putar -90&deg;
																	</button>
																	<button type="button" class="btn btn-default btn-sm btn-rotate" data-rotate-mode="syarat" data-file="{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$sp->FILE_SYARAT}" data-id-c-mhs="{$cmb->ID_C_MHS}" data-id-syarat-prodi="{$sp->ID_SYARAT_PRODI}" data-rotate="90">
																		<span class="glyphicon glyphicon-repeat"></span> Putar 90&deg;
																	</button>
																</div>
															</form>
														{else}
															<h3 class="text-center"><span class="label label-success">Valid</span></h3>
														{/if}
													{/if}
												</td>
											</tr>
											{/foreach}
										</tbody>
									</table>
								</div>
							{/if}
						{/foreach}
						
					{/if}
				</div>
			</div>

		</div>
	</div>
{/block}
{block name='footer-script'}
	
	<!-- fancyBox Library Script -->
	<script type="text/javascript" src="{base_url('assets/fancybox/source/jquery.fancybox.pack.js?v=2.1.5')}"></script>
	<script type="text/javascript" src="{base_url('assets/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5')}"></script>
	
	<!-- jQuery Number Format -->
	<script type="text/javascript" src="{base_url('assets/js/jquery.number.min.js')}"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			
			/** fancybox initializer */
			$('.img-popup').fancybox({
				padding: 3,
				prevEffect		: 'none',
				nextEffect		: 'none',
				closeBtn		: false,
				helpers : {
					title : {
						type : 'outside',
						position : 'top'
					},
					buttons: { }
				}
			});
			
			/** validasi isian */
			$('button[name="valid"][value="0"]').on('click', function(e) {
				
				var field = 'input[name="' + $(this).data('field') + '"]';
				
				if ($(field).val() === '')
				{					
					e.preventDefault();
					alert('Keterangan belum di isi');
					$(field).focus();
				}
				
			});
			
			/** Format Number */
			$('input[type=text][name="sp3_1"]').number(true, 0);
			$('input[type=text][name="sp3_2"]').number(true, 0);
			$('input[type=text][name="sp3_3"]').number(true, 0);
			$('input[type=text][name="sp3_4"]').number(true, 0);
			
			/** Konfirmasi Sukses Button*/
			$('#sukses_button').on('click', function() {
				
				var hasil = confirm('Apakah yakin verifikasi sukses ?');
				
				if (hasil)
				{
					window.location.href = $(this).data('href');
				}
			});
			
			/** Rotasi Gambar */
			$('.btn-rotate').on('click', function() {
				
				var rotate_mode		= $(this).data('rotate-mode');
				var file			= $(this).data('file');
				var id_c_mhs		= $(this).data('id-c-mhs');
				var id_syarat_prodi	= $(this).data('id-syarat-prodi');
				var rotate			= $(this).data('rotate');
				
				var anchor_select	= '';
				
				if (rotate_mode === 'foto' || rotate_mode === 'berkas-pernyataan')
				{
					anchor_select = '#' + rotate_mode + '-' + id_c_mhs;
				}
				else
				{
					anchor_select = '#' + rotate_mode + '-' + id_c_mhs + '-' + id_syarat_prodi;
				}
				
				// console.log('{site_url('verifikasi/rotate_image')}/' + file + '/' + rotate);
				
				$.ajax({
					async: false,
					url: '{site_url('verifikasi/rotate_image')}/' + file + '/' + rotate
				}).done(function(r) {
					if (r !== 0)
					{
						$(anchor_select).attr('href', r);
						$(anchor_select).children('img').attr('src', r);
					}
				});
			});
			
		});
	</script>
{/block}