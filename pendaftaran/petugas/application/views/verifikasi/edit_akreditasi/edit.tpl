{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Edit Akreditasi</h2>
			</div>
			
			<form class="form-inline" method="get" action="{current_url()}">
				<div class="form-group">
					<label class="sr-only" for="q">No Ujian</label>
					<input type="text" class="form-control" name="q" placeholder="No Ujian" value="{if isset($smarty.get.q)}{$smarty.get.q}{/if}" />
				</div>
				<button type="submit" class="btn btn-primary">Cari</button>
			</form>
				
			<hr />
			
			{if !empty($cmb)}
			
			{if isset($success)}
				<h4>Data berhasil diperbarui !</h4>
			{/if}
					
			<form action="{current_url()}?{$smarty.server.QUERY_STRING}" method="post">
				<input type="hidden" name="id_c_mhs" value="{$cmb->ID_C_MHS}" />
				<table class="table table-bordered" style="width: auto">
					<thead>
						<tr>
							<th colspan="2">Detail Peserta</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Nama</td>
							<td>{$cmb->NM_C_MHS}</td>
						</tr>
						<tr>
							<td>Jenis Akreditasi</td>
							<td>
								<select id="jenis_akreditasi_s1" name="jenis_akreditasi_s1" class="form-control">
									<option value=""></option>
									<option value="BAN-PT" {set_select('jenis_akreditasi_s1', 'BAN-PT', ($cmp->JENIS_AKREDITASI_S1 == 'BAN-PT'))}>BAN-PT</option>
									<option value="Kemenkes" {set_select('jenis_akreditasi_s1', 'Kemenkes', ($cmp->JENIS_AKREDITASI_S1 == 'Kemenkes'))}>Kemenkes</option>
									<option value="Lain" {set_select('jenis_akreditasi_s1', 'Lain', ($cmp->JENIS_AKREDITASI_S1 == 'Lain'))}>Lainnya</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Tingkat Akreditasi</td>
							<td>
								<select id="peringkat_akreditasi_s1" name="peringkat_akreditasi_s1" class="form-control">
									<option value=""></option>
									<option value="A" {set_select('peringkat_akreditasi_s1', 'A', ($cmp->PERINGKAT_AKREDITASI_S1 == 'A'))}>A</option>
									<option value="B" {set_select('peringkat_akreditasi_s1', 'B', ($cmp->PERINGKAT_AKREDITASI_S1 == 'B'))}>B</option>
									<option value="C" {set_select('peringkat_akreditasi_s1', 'C', ($cmp->PERINGKAT_AKREDITASI_S1 == 'C'))}>C</option>
									<option value="Lain" {set_select('peringkat_akreditasi_s1', 'Lain', ($cmp->PERINGKAT_AKREDITASI_S1 == 'Lain'))}>Lainnya</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Nilai Akreditasi</td>
							<td>
								<input type="number" name="nilai_akreditasi" value="{$nilai->NILAI_AKREDITASI}" />
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<input type="submit" value="Update" class="btn btn-primary" />
							</td>
						</tr>
					</tbody>
				</table>
			</form>
					
			{/if}
					
		</div>
	</div>
{/block}