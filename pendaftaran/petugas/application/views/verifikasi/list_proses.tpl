{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Daftar Peserta Sedang dalam Proses</h2>
				<a href="{site_url('verifikasi/list_penerimaan')}">Kembali ke daftar penerimaan</a>
			</div>
			
			<table class="table table-bordered table-condensed table-hover">
				<thead>
					<tr>
						<th style='width: 50px' class="text-center">#</th>
						<th>Nomor Pendaftaran</th>
						<th>Nama</th>
						<th>Email</th>
						<th style='width: 200px' class="text-center">Waktu Submit</th>
						<th style='width: 200px' class="text-center">Petugas</th>
						<th style='width: 150px' class="text-center">Aksi</th>
					</tr>
				</thead>
				<tbody>
					{foreach $cmb_set as $cmb}
						<tr>
							<td class="text-center">{$cmb@index + 1}</td>
							<td>{$cmb->KODE_VOUCHER}</td>
							<td>{$cmb->NM_C_MHS}</td>
							<td>{$cmb->EMAIL}</td>
							<td class="text-center">
								<span title="{$cmb->TGL_SUBMIT_VERIFIKASI}">{get_interval_date($cmb->TGL_SUBMIT_VERIFIKASI)}</span>
							</td>
							<td class="text-center">
								<span title="{$cmb->NM_PENGGUNA}">{$cmb->NM_PENGGUNA}</span>
							</td>
							<td class="text-center">
								<a class="btn btn-xs btn-primary" href="{site_url('verifikasi/start')}/{$cmb->ID_C_MHS}/{$penerimaan->ID_PENERIMAAN}">Proses</a>
								<a class="btn btn-xs btn-danger" href="{site_url('verifikasi/cancel')}/{$cmb->ID_C_MHS}?id_penerimaan={$penerimaan->ID_PENERIMAAN}">Batalkan</a>
							</td>
						</tr>
					{/foreach}
				</tbody>
			</table>
			
		</div>
	</div>
{/block}