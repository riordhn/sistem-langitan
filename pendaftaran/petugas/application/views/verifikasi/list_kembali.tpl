{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Daftar Peserta Kembali</h2>
				<a href="{site_url('verifikasi/list_penerimaan')}">Kembali ke daftar penerimaan</a>
			</div>
			
			<table class="table table-bordered table-condensed table-hover">
				<thead>
					<tr>
						<th style='width: 50px' class="text-center">#</th>
						<th>Nomor Pendaftaran</th>
						<th>Nama</th>
						<th>E-Mail</th>
						<th>Prodi</th>
						<th style='width: 200px' class="text-center">Petugas</th>
					</tr>
				</thead>
				<tbody>
					{foreach $cmb_set as $cmb}
						<tr>
							<td class="text-center">{$cmb@index + 1}</td>
							<td>{$cmb->KODE_VOUCHER}</td>
							<td>{$cmb->NM_C_MHS}</td>
							<td>{$cmb->EMAIL}</td>
							<td>{$cmb->PRODI_1}<br>{$cmb->PRODI_2}</td>
							<td class="text-center">
								<span title="{$cmb->NM_PENGGUNA}">{$cmb->NM_PENGGUNA}</span>
							</td>
						</tr>
					{/foreach}
				</tbody>
			</table>
			
		</div>
	</div>
{/block}