{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Proses Verifikasi</h2>
			</div>
			
			<div class="alert alert-success">
				<strong>{$title}</strong>
				<br/>{$message}
				<br/><a class="alert-link" href="{site_url('verifikasi/list_antrian')}/{$id_penerimaan}">Kembali ke daftar antrian</a>
			</div>
		</div>
	</div>
{/block}
{block name='footer-script'}
	<!-- akan digunakan, sms gateway masih habis -->
	{* {if !empty($pesan_sms)}
		{js_widget('sms')} *}
		<!-- <script type="text/javascript">
			$(document).ready(function() { send_sms('{$telp}','{$pesan_sms}', 1); });
		</script> -->
	{* {/if} *}
{/block}