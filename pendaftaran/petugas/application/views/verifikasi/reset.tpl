{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Reset Verifikasi Peserta</h2>
			</div>

			<form class="form-inline" method="get" action="{current_url()}">
				<div class="form-group">
					<label class="sr-only" for="q">Kode Voucher / No Ujian</label>
					<input type="search" class="form-control" name="q" placeholder="Kode voucher / no ujian" value="{if isset($smarty.get.q)}{$smarty.get.q}{/if}" />
				</div>
				<button type="submit" class="btn btn-primary">Cari</button>
			</form>

			<br/>
			{if !empty($cmb)}
				<form class="form-horizontal" method="post" action="{current_url()}?q={$smarty.get.q}">
					<input type="hidden" name="id_c_mhs" value="{$cmb->ID_C_MHS}" />
					
					<table class="table table-bordered" style="width: auto">
						<thead>
							<tr>
								<th colspan="2">Detail Peserta</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Nama</td>
								<td>{$cmb->NM_C_MHS}</td>
							</tr>
							<tr>
								<td>No Ujian</td>
								<td>{$cmb->NO_UJIAN}</td>
							</tr>
							<tr>
								<td>Kode Voucher</td>
								<td>{$cmb->KODE_VOUCHER}</td>
							</tr>
							<tr>
								<td colspan="2">
									<button type="submit" class="btn btn-primary">Reset</button>
									<a href="{site_url('verifikasi/reset')}" class="btn btn-default">Batal</a>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			{else if isset($reset_success)}
				Reset Berhasil
			{else if isset($smarty.get.q)}
				<div class="alert alert-warning">
					<strong>Peserta tidak ditemukan !</strong>
					<br/>
					Bisa terjadi karena salah satu berikut :<br/>
					<ul>
						<li>No Ujian / Kode Voucher salah</li>
						<li>Penerimaan tidak aktif</li>
						<li>Tanggal diluar waktu verifikasi</li>
						<li>Peserta memang belum verifikasi</li>
						<li>Peserta sudah diterima</li>
					</ul>
				</div>
			{/if}
		</div>
	</div>
{/block}