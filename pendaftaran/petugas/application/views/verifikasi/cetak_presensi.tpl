{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Cetak Presensi Peserta</h2>
			</div>

			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center">Penerimaan</th>
						<th class="text-center">Peserta IPA</th>
						<th class="text-center">Peserta IPS</th>
						<th class="text-center">Peserta IPC</th>
						<th class="text-center">Total</th>
					</tr>
				</thead>
				<tbody>
					{foreach $penerimaan_set as $p}
						<tr>
							<td>{$p->NM_PENERIMAAN}{if in_array($p->ID_JENJANG, array(2, 3, 9, 10))} Semester {$p->SEMESTER}{/if} Gelombang {$p->GELOMBANG} Tahun {$p->TAHUN}</td>
							<td class="text-center">
								{$p->IPA}
							</td>
							<td class="text-center">
								{$p->IPS}
							</td>
							<td class="text-center">
								{$p->IPC}
							</td>
							<td class="text-center">{$p->IPA + $p->IPS + $p->IPC}</td>
						</tr>
					{/foreach}
				</tbody>
			</table>

			<h5>Generate PDF</h5>

			<form class="form-inline" role="form" method="post" action="{site_url('verifikasi/cetak_presensi')}">
				<div class="form-group">
					<select name="id_penerimaan" class="form-control">
						{foreach $penerimaan_set as $p}
							<option value="{$p->ID_PENERIMAAN}">{$p->NM_PENERIMAAN}{if in_array($p->ID_JENJANG, array(2, 3, 9, 10))} Semester {$p->SEMESTER}{/if} Gelombang {$p->GELOMBANG} Tahun {$p->TAHUN}</option>
						{/foreach}
					</select>
				</div>
				<div class="form-group {if form_error('waktu_awal')}has-error{/if}">
					<input type="text" class="form-control" name="waktu_awal" id="waktu_awal" placeholder="{date('Y-m-d H:i')}">
				</div>
				<div class="form-group {if form_error('waktu_akhir')}has-error{/if}">
					<input type="text" class="form-control" name="waktu_akhir" id="waktu_akhir" placeholder="{date('Y-m-d H:i')}">
				</div>
				<button type="submit" class="btn btn-primary">Cetak</button>
			</form>

			<p></p>
		</div>
	</div>
{/block}