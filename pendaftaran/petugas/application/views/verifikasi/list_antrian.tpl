{extends file='home_layout.tpl'}
{block name='head'}
	<meta http-equiv="refresh" content="5">
{/block}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Daftar Antrian Verifikasi Penerimaan {$penerimaan->NM_PENERIMAAN} Tahun {$penerimaan->TAHUN} <small>Gelombang {$penerimaan->GELOMBANG}</small></h2>
				<a href="{site_url('verifikasi/list_penerimaan')}">Kembali ke daftar penerimaan</a>
			</div>
			
			{if !empty($smarty.get.batal)}
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					Peserta sudah dibatalkan proses verifikasi
				</div>
			{/if}
			
			{if !empty($smarty.get.kembalikan)}
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					Hasil verifikasi sudah dikembalikan ke peserta untuk di perbaiki.
				</div>
			{/if}
			
			<table class="table table-bordered table-condensed table-hover">
				<thead>
					<tr>
						<th style='width: 50px' class="text-center">#</th>
						<th>Nomor Pendaftaran</th>
						<th>Nama</th>
						<th>Email</th>
						<th>Pilihan 1</th>
						<th style='width: 200px' class="text-center">Waktu Submit</th>
						<th style='width: 200px' class="text-center">Petugas</th>
						<th style='width: 150px' class="text-center">Aksi</th>
					</tr>
				</thead>
				<tbody>
					{foreach $cmb_set as $cmb}
						<tr>
							<td class="text-center">{$cmb@index + 1}</td>
							<td>{$cmb->KODE_VOUCHER}</td>
							<td>{$cmb->NM_C_MHS}</td>
							<td>{$cmb->EMAIL}</td>
							<td>{$cmb->NM_PILIHAN_1}</td>
							<td class="text-center">
								<span title="{$cmb->TGL_SUBMIT_VERIFIKASI}">{get_interval_date($cmb->TGL_SUBMIT_VERIFIKASI)}</span>
							</td>
							<td class="text-center">
								<span title="{$cmb->NM_PENGGUNA}">{$cmb->NM_PENGGUNA}</span>
							</td>
							<td class="text-center">
								<a class="btn btn-xs btn-primary" href="{site_url('verifikasi/start')}/{$cmb->ID_C_MHS}/{$penerimaan->ID_PENERIMAAN}">Verifikasi</a>
							</td>
						</tr>
					{/foreach}
				</tbody>
			</table>
			
		</div>
	</div>
{/block}