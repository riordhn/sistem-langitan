{extends file='home_layout.tpl'}
{block name='head'}
	<!-- fancyBox CSS-->
	<link rel="stylesheet" href="{base_url('assets/fancybox/source/jquery.fancybox.css?v=2.1.5')}" type="text/css" media="screen" />
	<link rel="stylesheet" href="{base_url('assets/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5')}" type="text/css" media="screen" />
{/block}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Proses Verifikasi</h2>
				<a href="{site_url('verifikasi/list_antrian')}/{$cmb->ID_PENERIMAAN}">Kembali ke daftar antrian</a>
			</div>

			<div class="row">
				<div class="col-md-4" id="biodata">
					{include file='verifikasi/proses/informasi_peserta.tpl'}
					
					{if in_array($cmb->PENERIMAAN->ID_JENJANG, array(1)) and in_array($cmb->PENERIMAAN->ID_JALUR, array(4)) }
						{include file='verifikasi/proses/informasi_d3_asal.tpl'}
					{/if}
				
					{if in_array($cmb->PENERIMAAN->ID_JENJANG, array(4)) and in_array($cmb->PENERIMAAN->ID_JALUR, array(4)) }
						{include file='verifikasi/proses/informasi_d3_asal.tpl'}
					{/if}
				
					{if in_array($cmb->PENERIMAAN->ID_JENJANG, array(2, 3, 9, 10))}
						{include file='verifikasi/proses/informasi_pt_asal.tpl'}
					{/if}
				</div>

				<div class="col-md-8" id="data_file">
					
					<div class="panel panel-default">
						<table class="table table-bordered table-condensed">
							<tbody>
								<tr>
									<td class="text-center">Pesan Optional</td>
									<td>
										<form action="{current_url()}" method="post">
											<input type="hidden" name="mode" value="pesan_optional" />
											<input type="hidden" name="id_c_mhs" value="{$cmb->ID_C_MHS}" />
											<div class="form-group">
												<input type="text" class="form-control" name="pesan_v_optional" {if $cmb->PESAN_VERIFIKATOR != ''}value="{$cmb->PESAN_VERIFIKATOR}"{else if !empty($cmb->LOG->PESAN_VERIFIKATOR) }value="{$cmb->LOG->PESAN_VERIFIKATOR}"{/if} />
											</div>
											<div class="form-group">
												<button class="btn btn-primary btn-sm">Simpan</button>
											</div>
										</form>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="panel-heading">
							<h3 class="panel-title">Berkas Persyaratan Umum</h3>
						</div>
						<table class="table table-bordered table-condensed">
							<tbody>
								<tr>
									<td class="text-center" style="width: 160px">
									<td>
										<form action="{current_url()}" method="post">
											<input type="hidden" name="mode" value="validasi_masal" />
											<input type="hidden" name="id_c_mhs" value="{$cmb->ID_C_MHS}" />
											<button class="btn btn-primary btn-sm btn-danger pull-left" name="validmasal" value="0">Tidak Valid All</button>
											<button class="btn btn-primary btn-sm btn-success pull-right" name="validmasal" value="1">Valid All</button>
										</form>
									</td>
								</tr>
								<tr>
									<td class="text-center" style="width: 160px">
										{if $cmf->FILE_FOTO != ''}
											<a id="foto-{$cmb->ID_C_MHS}" class="img-popup" rel="gallery" href="../../../../files/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$cmf->FILE_FOTO}" target="_blank" data-fancybox-group="button" title="File Pas Foto">
												<img src="../../../../files/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$cmf->FILE_FOTO}?t={time()}" alt="Pas Foto" style="height: 100px" />
											</a>
											<br/>
										{/if}
										File Foto
									</td>
									<td>
										{if $cmf->FILE_FOTO != ''}
											{if $cmf->IS_FOTO_VERIFIED == 0}
												<form action="{current_url()}" method="post">
													<input type="hidden" name="mode" value="foto" />
													<input type="hidden" name="id_c_mhs" value="{$cmb->ID_C_MHS}" />
													<div class="form-group">
														<input type="text" class="form-control" name="pesan_v_foto" {if $cmf->PESAN_V_FOTO != ''}value="{$cmf->PESAN_V_FOTO}"{else if !empty($cmf->LOG->PESAN_VERIFIKATOR) }value="{$cmf->LOG->PESAN_VERIFIKATOR}"{/if} />
													</div>
													<div class="form-group">
														<button class="btn btn-sm {if $cmf->PESAN_V_FOTO ==''}btn-danger{else}btn-default{/if} pull-left" name="valid" value="0" data-field="pesan_v_foto"><span class="glyphicon glyphicon-remove"></span> Tidak Valid</button>
														<button class="btn btn-sm {if $cmf->PESAN_V_FOTO ==''}btn-success{else}btn-default{/if} pull-right" name="valid" value="1"><span class="glyphicon glyphicon-ok"></span> Valid</button>
													</div>
													
													<br/><br/>
													<div class="form-group">
														<button type="button" class="btn btn-default btn-sm btn-rotate" data-rotate-mode="foto" data-file="{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$cmf->FILE_FOTO}" data-id-c-mhs="{$cmb->ID_C_MHS}" data-rotate="-90">
															<span class="glyphicon glyphicon-repeat"></span> Putar -90&deg;
														</button>
														<button type="button" class="btn btn-default btn-sm btn-rotate" data-rotate-mode="foto" data-file="{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$cmf->FILE_FOTO}" data-id-c-mhs="{$cmb->ID_C_MHS}" data-rotate="90">
															<span class="glyphicon glyphicon-repeat"></span> Putar 90&deg;
														</button>
													</div>
															
												</form>
											{else}
												<h3 class="text-center"><span class="label label-success">Valid</span></h3>

												<form action="{current_url()}" method="post">
												  <input type="hidden" name="mode" value="foto" />
												  <input type="hidden" name="id_c_mhs" value="{$cmb->ID_C_MHS}" />
												  <div class="text-center">
												  <button class="btn btn-sm btn-danger pull-center" name="valid" value="2" data-field="pesan_v_foto"><span class="glyphicon glyphicon-remove"></span> Batal Valid</button>
												  </div>
												</form>
											{/if}
										{/if}
									</td>
								</tr>
								
								{if count($syarat_umum_set) > 0}
									{foreach $syarat_umum_set as $su}
									<tr>
										<td class="text-center">
											<a id="syarat-{$cmb->ID_C_MHS}-{$su->ID_SYARAT_PRODI}" class="img-popup" rel="gallery" href="../../../../files/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$su->FILE_SYARAT}" target="_blank" data-fancybox-group="button" title="{$su->NM_SYARAT}">
												{if $su->FILE_SYARAT != ''}
													{if strtolower(substr($su->FILE_SYARAT, -3)) != 'pdf'}
														<img src="../../../../files/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$su->FILE_SYARAT}?t={time()}" style="height: 100px" />
													{else}
														[FILE PDF]
													{/if}
												{/if}
											</a>
											<br/>
											{$su->NM_SYARAT}
											<br/><br>
											<i>{$su->KETERANGAN}</i>
										</td>
										<td>
											{if $su->FILE_SYARAT != ''}
												{if $su->IS_VERIFIED == 0}
													<form action="{current_url()}" method="post">
														<input type="hidden" name="mode" value="syarat" />
														<input type="hidden" name="id_c_mhs" value="{$cmb->ID_C_MHS}" />
														<input type="hidden" name="id_syarat_prodi" value="{$su->ID_SYARAT_PRODI}" />
														<div class="form-group">
															<input type="text" class="form-control" name="pesan_verifikator_{$su->ID_SYARAT_PRODI}" {if $su->PESAN_VERIFIKATOR == ''}value="{$su->PESAN_VERIFIKATOR_LOG}"{else}value="{$su->PESAN_VERIFIKATOR}"{/if}/>
														</div>
														<div class="form-group">
															<button class="btn btn-sm {if $su->PESAN_VERIFIKATOR ==''}btn-danger{else}btn-default{/if} pull-left" name="valid" value="0" data-field="pesan_verifikator_{$su->ID_SYARAT_PRODI}"><span class="glyphicon glyphicon-remove"></span> Tidak Valid</button>
															<button class="btn btn-sm {if $su->PESAN_VERIFIKATOR ==''}btn-success{else}btn-default{/if} pull-right" name="valid" value="1"><span class="glyphicon glyphicon-ok"></span> Valid</button>
														</div>
														
														<br/>
														<br/>
														<div class="form-group">
															<button type="button" class="btn btn-default btn-sm btn-rotate" data-rotate-mode="syarat" data-file="{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$su->FILE_SYARAT}" data-id-c-mhs="{$cmb->ID_C_MHS}" data-id-syarat-prodi="{$su->ID_SYARAT_PRODI}" data-rotate="-90">
																<span class="glyphicon glyphicon-repeat"></span> Putar -90&deg;
															</button>
															<button type="button" class="btn btn-default btn-sm btn-rotate" data-rotate-mode="syarat" data-file="{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$su->FILE_SYARAT}" data-id-c-mhs="{$cmb->ID_C_MHS}" data-id-syarat-prodi="{$su->ID_SYARAT_PRODI}" data-rotate="90">
																<span class="glyphicon glyphicon-repeat"></span> Putar 90&deg;
															</button>
														</div>
													</form>
												{else}
													<h3 class="text-center"><span class="label label-success">Valid</span></h3>

													<form action="{current_url()}" method="post">
													  <input type="hidden" name="mode" value="syarat" />
													  <input type="hidden" name="id_c_mhs" value="{$cmb->ID_C_MHS}" />
													  <input type="hidden" name="id_syarat_prodi" value="{$su->ID_SYARAT_PRODI}" />
													  <div class="text-center">
													  <button class="btn btn-sm btn-danger pull-center" name="valid" value="2" data-field="pesan_verifikator_{$su->ID_SYARAT_PRODI}"><span class="glyphicon glyphicon-remove"></span> Batal Valid</button>
													  </div>
													</form>
												{/if}
											{/if}
										</td>
									</tr>
									{/foreach}
								{/if}
							</tbody>
						</table>
					</div>	
							
					{if count($syarat_prodi_set) > 0}
						
						{foreach $syarat_prodi_set as $ps} {* $ps = Program_Studi *}
							{if count($ps->syarat_set) > 0}
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">Berkas Persyaratan Khusus Program Studi : {$ps->NM_PROGRAM_STUDI}</h3>
									</div>
									<table class="table table-bordered table-condensed">
										<tbody>
											{foreach $ps->syarat_set as $sp} {* $sp = Syarat_Prodi *}
											<tr>
												<td class="text-center" style="width: 160px">
													<a id="syarat-{$cmb->ID_C_MHS}-{$sp->ID_SYARAT_PRODI}" class="img-popup" rel="gallery" href="../../../../files/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$sp->FILE_SYARAT}" target="_blank" data-fancybox-group="button" title="{$sp->NM_SYARAT}">
														{if $sp->FILE_SYARAT != ''}
															{if strtolower(substr($sp->FILE_SYARAT, -3)) != 'pdf'}
																<img src="../../../../files/{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$sp->FILE_SYARAT}?t={time()}" style="height: 100px" />
															{else}
																[FILE PDF]
															{/if}
														{/if}
													</a>
													<br/>
													{$sp->NM_SYARAT}
													<br/><br>
													<i>{$sp->KETERANGAN}</i>
												</td>
												<td>
													{if $sp->FILE_SYARAT != ''}
														{if $sp->IS_VERIFIED == 0}
															<form action="{current_url()}" method="post">
																<input type="hidden" name="mode" value="syarat" />
																<input type="hidden" name="id_c_mhs" value="{$cmb->ID_C_MHS}" />
																<input type="hidden" name="id_syarat_prodi" value="{$sp->ID_SYARAT_PRODI}" />
																<div class="form-group">
																	<input type="text" class="form-control" name="pesan_verifikator_{$sp->ID_SYARAT_PRODI}" value="{$sp->PESAN_VERIFIKATOR}"/>
																</div>
																<div class="form-group">
																	<button class="btn btn-sm {if $sp->PESAN_VERIFIKATOR ==''}btn-danger{else}btn-default{/if} pull-left" name="valid" value="0" data-field="pesan_verifikator_{$sp->ID_SYARAT_PRODI}"><span class="glyphicon glyphicon-remove"></span> Tidak Valid</button>
																	<button class="btn btn-sm {if $sp->PESAN_VERIFIKATOR ==''}btn-success{else}btn-default{/if} pull-right" name="valid" value="1"><span class="glyphicon glyphicon-ok"></span> Valid</button>
																</div>
																<br/>
																<br/>
																<div class="form-group">
																	<button type="button" class="btn btn-default btn-sm btn-rotate" data-rotate-mode="syarat" data-file="{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$sp->FILE_SYARAT}" data-id-c-mhs="{$cmb->ID_C_MHS}" data-id-syarat-prodi="{$sp->ID_SYARAT_PRODI}" data-rotate="-90">
																		<span class="glyphicon glyphicon-repeat"></span> Putar -90&deg;
																	</button>
																	<button type="button" class="btn btn-default btn-sm btn-rotate" data-rotate-mode="syarat" data-file="{$cmb->ID_PENERIMAAN}/{$cmb->ID_C_MHS}/{$sp->FILE_SYARAT}" data-id-c-mhs="{$cmb->ID_C_MHS}" data-id-syarat-prodi="{$sp->ID_SYARAT_PRODI}" data-rotate="90">
																		<span class="glyphicon glyphicon-repeat"></span> Putar 90&deg;
																	</button>
																</div>
															</form>
														{else}
															<h3 class="text-center"><span class="label label-success">Valid</span></h3>

															<form action="{current_url()}" method="post">
															  <input type="hidden" name="mode" value="syarat" />
															  <input type="hidden" name="id_c_mhs" value="{$cmb->ID_C_MHS}" />
															  <input type="hidden" name="id_syarat_prodi" value="{$sp->ID_SYARAT_PRODI}" />
															  <div class="text-center">
															  <button class="btn btn-sm btn-danger pull-center" name="valid" value="2" data-field="pesan_verifikator_{$sp->ID_SYARAT_PRODI}"><span class="glyphicon glyphicon-remove"></span> Batal Valid</button>
															  </div>
															</form>
														{/if}
													{/if}
												</td>
											</tr>
											{/foreach}
										</tbody>
									</table>
								</div>
							{/if}
						{/foreach}
						
					{/if}
				</div>
			</div>

		</div>
	</div>
{/block}
{block name='footer-script'}
	
	<!-- fancyBox Library Script -->
	<script type="text/javascript" src="{base_url('assets/fancybox/source/jquery.fancybox.pack.js?v=2.1.5')}"></script>
	<script type="text/javascript" src="{base_url('assets/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5')}"></script>
	
	<!-- jQuery Number Format -->
	<script type="text/javascript" src="{base_url('assets/js/jquery.number.min.js')}"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			
			/** fancybox initializer */
			$('.img-popup').fancybox({
				padding: 3,
				prevEffect		: 'none',
				nextEffect		: 'none',
				closeBtn		: false,
				helpers : {
					title : {
						type : 'outside',
						position : 'top'
					},
					buttons: { }
				}
			});
			
			/** validasi isian */
			$('button[name="valid"][value="0"]').on('click', function(e) {
				
				var field = 'input[name="' + $(this).data('field') + '"]';
				
				if ($(field).val() === '')
				{					
					e.preventDefault();
					alert('Keterangan belum di isi');
					$(field).focus();
				}
				
			});
			
			/** Format Number */
			$('input[type=text][name="sp3_1"]').number(true, 0);
			$('input[type=text][name="sp3_2"]').number(true, 0);
			$('input[type=text][name="sp3_3"]').number(true, 0);
			$('input[type=text][name="sp3_4"]').number(true, 0);
			
			/** Konfirmasi Sukses Button*/
			$('#sukses_button').on('click', function() {
				
				var hasil = confirm('Apakah yakin verifikasi sukses ?');
				
				if (hasil)
				{
					window.location.href = $(this).data('href');
				}
			});
			
			/** Rotasi Gambar */
			$('.btn-rotate').on('click', function() {
				
				var rotate_mode		= $(this).data('rotate-mode');
				var file			= $(this).data('file');
				var id_c_mhs		= $(this).data('id-c-mhs');
				var id_syarat_prodi	= $(this).data('id-syarat-prodi');
				var rotate			= $(this).data('rotate');
				
				var anchor_select	= '';
				
				if (rotate_mode === 'foto' || rotate_mode === 'berkas-pernyataan')
				{
					anchor_select = '#' + rotate_mode + '-' + id_c_mhs;
				}
				else
				{
					anchor_select = '#' + rotate_mode + '-' + id_c_mhs + '-' + id_syarat_prodi;
				}
				
				// console.log('{site_url('verifikasi/rotate_image')}/' + file + '/' + rotate);
				
				$.ajax({
					async: false,
					url: '{site_url('verifikasi/rotate_image')}/' + file + '/' + rotate
				}).done(function(r) {
					if (r !== 0)
					{
						$(anchor_select).attr('href', r);
						$(anchor_select).children('img').attr('src', r);
					}
				});
			});
			
			/* Untuk Kebutuhan SP3 pilihan kelas / kelompok biaya */
			if ($('[name="mode"]').val() === 'sp3-kb')
			{
				$('[name="id_kelompok_biaya_1"]').on('change', function() {
					$.ajax({
						async: false,
						url: '{site_url('verifikasi/get_sop_sp3')}/{$cmb->ID_PENERIMAAN}/{$cmb->ID_PILIHAN_1}/' + this.value,
						dataType: 'json'
					}).done(function(r) {
						if (r.length === 2) {
							/* Tampilan */
							$('#sop_display').html('SOP: Rp ' + r[0].BESAR_BIAYA);
							$('#sp3_display').html('SP3: Rp ' + r[1].BESAR_BIAYA);
							/* Update Min SP3 */
							$('[name=sp3_1_min]').val(r[1].BESAR_BIAYA.replace(/\./g, ''));
						}
					});
				});
			}
			
		});
	</script>
{/block}