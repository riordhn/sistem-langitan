{extends file='home_layout.tpl'}
{block name='head'}
	<style type="text/css">
		.table > thead > tr > th, 
		.table > tbody > tr > td,
		.table.dataTable > thead > tr > th,
		.table.dataTable > tbody > tr > td {
			font-family: Tahoma;
			font-size: 12px;
			padding: 3px;
		}
		
		#sekolah-snmptn.dataTable > tbody > tr > td {
			padding: 5px;
		}
	</style>
	<link href="//cdn.datatables.net/1.10.6/css/jquery.dataTables.min.css" type='text/css' rel='stylesheet' />
{/block}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Cleansing Data Sekolah SNMPTN - Cybercampus</h2>
			</div>

			<form class="form-inline" action="{current_url()}" method="get">
				<table class="table table-bordered table-condensed">
					<tbody>
						<tr>
							<td>
								<label class="control-label">Kode Provinsi</label>
								<select name="kode_provinsi" class="form-control">
									<option value=""></option>
									{foreach $provinsi_snmptn_set as $ps}
										<option value="{$ps->kode_provinsi}" {if !empty($smarty.get.kode_provinsi)}{if $smarty.get.kode_provinsi == $ps->kode_provinsi}selected{/if}{/if}>{$ps->kode_provinsi} - {$ps->nama_provinsi}</option>
									{/foreach}
								</select>
							</td>
						</tr>
					</tbody>
				</table>
			</form>

			<div class="row">
				<div class="col-md-6">
					<table class="table table-condensed table-bordered" id="sekolah-snmptn">
						<thead>
							<tr>
								<th>Kode</th>
								<th>Sekolah SNMPTN</th>
								<th>Kabupaten</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							{if isset($sekolah_snmptn_set)}
								{foreach $sekolah_snmptn_set as $ss}
									<tr {if $ss->exist == true}class="success"{/if}>
										<td>{$ss->npsn}</td>
										<td>{$ss->nama_sekolah}</td>
										<td>{$ss->nama_kabupaten}</td>
										<td>
											{if $ss->exist == false}
												{if isset($ss->id_kota)}
													<input type='button' name='add_btn' value='add' 
														   data-npsn="{$ss->npsn}" 
														   data-nama="{$ss->nama_sekolah}" 
														   data-id_kota="{$ss->id_kota}"
														   data-kabupaten="{$ss->nama_kabupaten}"/>
												{else}
													kota tdk ada
												{/if}
											{/if}
										</td>
									</tr>
								{/foreach}
							{/if}
						</tbody>
					</table>
				</div>
				<div class="col-md-6">
					<table class="table table-condensed table-bordered" id="sekolah-cyber">
						<thead>
							<tr>
								<th>Kode</th>
								<th></th>
								<th>Sekolah Cybercampus</th>
								<th>Kabupaten</th>
							</tr>
						</thead>
						<tbody>
							{if isset($sekolah_cyber_set)}
								{foreach $sekolah_cyber_set as $sc}
									<tr>
										<td>
											<input type='text' name='sc_{$sc->ID_SEKOLAH}' value='{$sc->KODE_SEKOLAH}' style='width: 70px; display: inline-block'/>
										</td>
										<td><input type='submit' name='btn' data-id='{$sc->ID_SEKOLAH}' value='ok'/></td>
										<td>{$sc->NM_SEKOLAH}</td>
										<td>{$sc->NM_KOTA}</td>
									</tr>
								{/foreach}
							{/if}
						</tbody>
					</table>
				</div>
			</div>

		</div>
	</div>
{/block}

{block name='footer-script'}
	<script type="text/javascript" src="//cdn.datatables.net/1.10.6/js/jquery.dataTables.min.js"></script>
	<script type='text/javascript'>
		$(document).ready(function() {
			
			/* OK click */
			$('input[name="btn"]').on('click', function() {
				// GET id_sekolah
				var id_sekolah = $(this).data('id');
				
				var kode_sekolah = $('input[name="sc_'+id_sekolah+'"]').val();

				// row sekolah snmptn
				var row = $('#sekolah-snmptn tbody tr td').filter(function() {
					return ($(this).text() == kode_sekolah);
				}).parent('tr');
				
				// Ajax
				$.ajax({
					type: 'POST',
					url: '{site_url('verifikasi_snmptn/update_sekolah/')}',
					data: 'id_sekolah='+id_sekolah+'&kode_sekolah='+kode_sekolah,
					success: function(r) {
						if (r == 1)
						{
							// tandai hijau
							row.addClass('success');
							// remove button
							row.children(':nth-last-child(1)').text('');
						}
						else
						{
							alert('Terjadi kesalahan update database');
						}
					}
				});

			});
			
			$('input[name="add_btn"]').on('click', function() {
				var kode_snmptn = $(this).data('npsn');
				var nm_sekolah = $(this).data('nama');
				var id_kota = $(this).data('id_kota');
				var kabupaten = $(this).data('kabupaten');
				
				var thisBtn = $(this);
				
				// Ajax
				$.ajax({
					type: 'POST',
					url: '{site_url('verifikasi_snmptn/insert_sekolah/')}',
					data: 'id_kota='+id_kota+'&nm_sekolah='+nm_sekolah+'&kode_snmptn='+kode_snmptn,
					success: function(r) {
						if (r == 1)
						{
							// Add New row sekolah-cybercampus (with animation)
							$('<tr style="display:none"><td>'+kode_snmptn+'</td><td></td><td>'+nm_sekolah+'</td><td>'+kabupaten+'</td></tr>')
									.prependTo('#sekolah-cyber > tbody')
									.show('slow');

							// color
							thisBtn.parentsUntil('tbody').addClass('success');
							// destroy button
							thisBtn.remove();
							
						}
						else
						{
							alert('Terjadi kesalahan update database');
						}
					}
				});
				
			});

			/* kode_provinsi */
			$('select[name="kode_provinsi"]').on('change', function() {
				$('form')[0].submit();
			});
			
			/* data table */
			$('#sekolah-cyber').DataTable({
				"order": [[3, 'asc'],[2, 'asc']],
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
			});
			
			/* data table */
			$('#sekolah-snmptn').DataTable({
				"ordering": false,
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
			});
		});
	</script>
{/block}