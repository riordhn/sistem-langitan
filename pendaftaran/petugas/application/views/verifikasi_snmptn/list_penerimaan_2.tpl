{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Daftar Penerimaan SNMPTN</h2>
			</div>
			
			{if $penerimaan_set}
							
				<table class="table table-bordered">
					<thead>
						<tr>
							<th class="text-center">Penerimaan</th>
							<th class="text-center">Jumlah Antrian</th>
							<th class="text-center">Jumlah Verifikasi</th>
							<th class="text-center">Jumlah Memverifikasi</th>
							<th class="text-center">Proses Verifikasi</th>
						</tr>
					</thead>
					<tbody>
						{foreach $penerimaan_set as $p}
							<tr>
								<td>Penerimaan {$p->NM_PENERIMAAN} Tahun {$p->TAHUN}</td>
								<td class="text-center">
									<a href="{site_url('verifikasi_snmptn/list_antrian_2')}/{$p->ID_PENERIMAAN}">{$p->JUMLAH_ANTRIAN}</a>
								</td>
								<td class="text-center">
									{$p->JUMLAH_VERIFIKASI}
								</td>
								<td class="text-center">
									<a href="{site_url('verifikasi_snmptn/list_memverifikasi_2')}/{$p->ID_PENERIMAAN}">{$p->JUMLAH_MEMVERIFIKASI}</a>
								</td>
								<td class="text-center">
									<a href="{site_url('verifikasi_snmptn/list_proses_2')}/{$p->ID_PENERIMAAN}">{$p->JUMLAH_PROSES_VERIFIKASI}</a>
								</td>
							</tr>
						{/foreach}
					</tbody>
				</table>
				
			{/if}
		</div>
	</div>
{/block}