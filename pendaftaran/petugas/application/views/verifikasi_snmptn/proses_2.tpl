{extends file='home_layout.tpl'}
{block name='head'}
	<!-- fancyBox CSS-->
	<link rel="stylesheet" href="{base_url('assets/fancybox/source/jquery.fancybox.css?v=2.1.5')}" type="text/css" media="screen" />
	<link rel="stylesheet" href="{base_url('assets/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5')}" type="text/css" media="screen" />
{/block}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Proses Verifikasi II</h2>
			</div>
			
			<div class="row">
				<div class="col-md-4" id="biodata">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Informasi Peserta</h3>
						</div>
						<table class="table table-bordered table-condensed">
							<tbody>
								<tr>
									<td class="text-center" colspan="2">
										<img id="foto" style="height: 300px" class="img-thumbnail" src="http://210.57.208.55/snmptn{$penerimaan->TAHUN}/dokumen/{$siswa->nomor_pendaftaran}/{$siswa->file_foto}"/>
									</td>
								</tr>
								<tr>
									<td>No</td>
									<td>{$siswa->nomor_pendaftaran}</td>
								</tr>
								<tr>
									<td>Nama</td>
									<td>{$siswa->nama_siswa}</td>
								</tr>
								<tr>
									<td>Sekolah</td>
									<td>{$siswa->sekolah->nama_sekolah}</td>
								</tr>
								<tr>
									<td>Prestasi</td>
									<td>{$siswa->jumlah_prestasi}</td>
								</tr>
								<tr>
									<td class="text-center" colspan="2">
										<a class="btn btn-sm btn-default" href="{site_url('verifikasi_snmptn/list_antrian_2')}/{$penerimaan->ID_PENERIMAAN}">Batalkan proses</a>
										<a class="btn btn-sm btn-primary" href="{site_url('verifikasi_snmptn/complete_2')}/{$penerimaan->ID_PENERIMAAN}/{$siswa->nomor_pendaftaran}">Submit</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
									
				<div class="col-md-8">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Prestasi Peserta</h3>
						</div>
						<table class="table table-bordered table-condensed">
							<tbody>
								{foreach $siswa->prestasi_set as $prestasi}
									<tr>
										<td style="width: 20%">
											<a id="a-prestasi-{$prestasi->id_prestasi}" class="img-popup" rel="gallery" href="http://210.57.208.55/snmptn{$penerimaan->TAHUN}/dokumen/{$siswa->nomor_pendaftaran}/{$prestasi->nama_file}" data-fancybox-group="button" title="{$prestasi->jenis_prestasi} - {$prestasi->jenjang_prestasi}">
												<img id="img-prestasi-{$prestasi->id_prestasi}" style="width: 130px" class="img-rounded" src="http://210.57.208.55/snmptn{$penerimaan->TAHUN}/dokumen/{$siswa->nomor_pendaftaran}/{$prestasi->nama_file}" />
											</a>
										</td>
										<td>
											<form method="post" action="{current_url()}">
												<input type="hidden" name="id_prestasi" value="{$prestasi->id_prestasi}" />
												<input type="hidden" name="tahun" value="{$penerimaan->TAHUN}" />
												<div class="form-group">
													<label for="id_skor_prestasi">Validasi Prestasi</label>
													<select name="id_skor_prestasi" class="form-control">
														<option></option>
														{foreach $skor_prestasi_set as $sp}
															<option value="{$sp->id_skor_prestasi}" {if $prestasi->id_skor_prestasi == $sp->id_skor_prestasi}selected{/if}>{$sp->nama_prestasi}</option>
														{/foreach}
													</select>
												</div>
												<div class="form-group">
													<button type="button" class="btn btn-default btn-sm btn-rotate" data-id-prestasi="{$prestasi->id_prestasi}" data-tahun="{$penerimaan->TAHUN}" data-rotate="-90">
														<span class="glyphicon glyphicon-repeat"></span> Putar -90&deg;
													</button>
													<button type="button" class="btn btn-default btn-sm btn-rotate" data-id-prestasi="{$prestasi->id_prestasi}" data-tahun="{$penerimaan->TAHUN}" data-rotate="90">
														<span class="glyphicon glyphicon-repeat"></span> Putar 90&deg;
													</button>
												</div>
											</form>
										</td>
									</tr>
								{/foreach}
							</tbody>
						</table>
					</div>
				</div>
			</div>
			
		</div>
	</div>
{/block}

{block name='footer-script'}
	<!-- fancyBox Library Script -->
	<script type="text/javascript" src="{base_url('assets/fancybox/source/jquery.fancybox.pack.js?v=2.1.5')}"></script>
	<script type="text/javascript" src="{base_url('assets/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5')}"></script>
	
	<script type="text/javascript">
		$(document).ready(function() {
						
			$('.img-popup').fancybox({
				padding: 3,
				prevEffect		: 'none',
				nextEffect		: 'none',
				closeBtn		: false,
				helpers : {
					title : {
						type : 'outside',
						position : 'top'
					},
					buttons: { }
				}
			});
			
			$('.btn-rotate').on('click', function(){
				var id_prestasi = $(this).data('id-prestasi');
				var tahun		= $(this).data('tahun');
				var rotate		= $(this).data('rotate');
				
				$.ajax({
					async: false,
					url: "{site_url('verifikasi_snmptn/rotate_image')}/" + tahun + "/" + id_prestasi + "/" + rotate
				}).done(function (r) {
					if (r != 0)
					{
						// refresh image n links
						$('#a-prestasi-' + id_prestasi).attr('href', r);
						$('#img-prestasi-' + id_prestasi).attr('src', r);
					}
					else
					{
						alert('Gambar tidak bisa di putar');
					}
				});
			});
			
			$('select').on('change', function() {
				var form = $(this).parentsUntil('form').parent();
				form.submit();
			});
			
		});
	</script>
{/block}