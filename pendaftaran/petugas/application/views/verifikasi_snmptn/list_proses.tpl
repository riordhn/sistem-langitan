{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Daftar Peserta Sedang dalam Proses</h2>
				<a href="{site_url('verifikasi_snmptn/list_penerimaan')}">Kembali ke daftar penerimaan</a>
			</div>
			
			<table class="table table-bordered table-condensed table-hover">
				<thead>
					<tr>
						<th style='width: 50px' class="text-center">#</th>
						<th style='width: 100px'>No Ujian</th>
						<th>Nama</th>
						<th>Asal Sekolah</th>
						<th style='width: 150px' class="text-center">Aksi</th>
					</tr>
				</thead>
				<tbody>
					{foreach $siswa_set as $siswa}
						<tr>
							<td class="text-center">{$siswa@index + 1}</td>
							<td>{$siswa->nomor_pendaftaran}</td>
							<td>{$siswa->nama_siswa}</td>
							<td>{$siswa->nama_sekolah}</td>
							<td class="text-center">
								<a class="btn btn-xs btn-primary" href="{site_url('verifikasi_snmptn/proses')}/{$penerimaan->ID_PENERIMAAN}/{$siswa->nomor_pendaftaran}">Proses</a>
								<a class="btn btn-xs btn-danger" href="{site_url('verifikasi_snmptn/cancel')}/{$penerimaan->ID_PENERIMAAN}/{$siswa->nomor_pendaftaran}">Batalkan</a>
							</td>
						</tr>
					{/foreach}
				</tbody>
			</table>
			
		</div>
	</div>
{/block}