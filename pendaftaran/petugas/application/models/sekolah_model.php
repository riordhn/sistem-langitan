<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sekolah_Model extends MY_Model
{
	function list_sekolah($kode_provinsi = '', $kode_kota = '')
	{
		$result_set = array();
		
		$this->db
			->from('v_sekolah_ina')
			->where('kode_provinsi', $kode_provinsi)
			->order_by('kode_sekolah nulls first, kode_kota');
		
		if ($kode_kota != '')
			$this->db->where('kode_kota', $kode_kota);
			
		$this->query = $this->db->get();
		foreach ($this->query->result() as $row) { array_push($result_set, $row); }
		
		return $result_set;
	}
	
	function list_kota($kode_provinsi)
	{
		$this->query = $this->db
			->select('kota.kode_snmptn, kota.id_kota')
			->from('kota')
			->join('provinsi', 'provinsi.id_provinsi = kota.id_provinsi')
			->where('provinsi.kode_snmptn', $kode_provinsi)
			->get();
		
		return $this->query->result();
	}
	
	function insert_sekolah($id_kota, $nm_sekolah, $kode_snmptn)
	{
		return $this->db->insert('sekolah', array(
			'id_kota'		=> $id_kota,
			'nm_sekolah'	=> $nm_sekolah,
			'kode_snmptn'	=> $kode_snmptn
		));
	}
	
	function update_sekolah($id_sekolah, $kode_snmptn)
	{
		return $this->db->update('sekolah', array(
			'kode_snmptn'	=> $kode_snmptn
		), array('id_sekolah' => $id_sekolah));
	}
}
