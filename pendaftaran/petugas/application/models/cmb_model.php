<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of cmb_model
 *
 * @author Fathoni <fathoni@staf.unair.ac.id>
 */
class Cmb_Model extends MY_Model
{
	/**
	 * Mendapatkan antrian peserta
	 * @param int $id_penerimaan
	 * @return array
	 */
	function list_antrian_peserta($id_penerimaan)
	{
		$result_set = array();
		
		
		$this->query = $this->db
			->select('cmb.id_c_mhs, cmb.nm_c_mhs, cmb.email, to_char(cmb.tgl_submit_verifikasi, \'YYYY-MM-DD HH24:MI:SS\') as tgl_submit_verifikasi, ps.nm_program_studi as nm_pilihan_1, p.nm_pengguna, cmb.kode_voucher')
			->from('calon_mahasiswa_baru cmb')
			->join('pengguna p', 'cmb.id_verifikator_ppmb = p.id_pengguna', 'LEFT')
			->join('program_studi ps', 'ps.id_program_studi = cmb.id_pilihan_1')
			->where(array(
				'cmb.id_penerimaan' => $id_penerimaan, 
				'cmb.tgl_verifikasi_ppmb' => NULL, 
				'cmb.is_proses_verifikasi' => NULL))
			->where('cmb.tgl_submit_verifikasi IS NOT NULL', NULL, FALSE)
			->order_by('cmb.tgl_submit_verifikasi', 'asc')
			->get();
		
		/*
		$this->query = $this->db
			->select('id_c_mhs, nm_c_mhs, email, to_char(tgl_submit_verifikasi, \'YYYY-MM-DD HH24:MI:SS\') as tgl_submit_verifikasi, id_c_mhs as nm_pengguna')
			->from('calon_mahasiswa_baru')
			->where(array('id_penerimaan' => $id_penerimaan, 'tgl_verifikasi_ppmb' => NULL, 'is_proses_verifikasi' => NULL))
			->where('tgl_submit_verifikasi IS NOT NULL', NULL, FALSE)
			->order_by('tgl_submit_verifikasi', 'asc')
			->get();
		*/
		
		foreach ($this->query->result() as $row)
		{
			array_push($result_set, $row);
		}
		
		return $result_set;
	}
	
	/**
	 * Mendapatkan antrian yang sedang dalam proses
	 * @param int $id_penerimaan
	 * @return array
	 */
	function list_proses_peserta($id_penerimaan)
	{
		$result_set = array();
		
		$this->query = $this->db
			->select('a.id_c_mhs, a.nm_c_mhs, to_char(a.tgl_submit_verifikasi, \'YYYY-MM-DD HH24:MI:SS\') as tgl_submit_verifikasi, b.nm_pengguna, a.email, a.kode_voucher')
			->from('calon_mahasiswa_baru a')
			->join('pengguna b', 'a.id_verifikator_ppmb = b.id_pengguna', 'LEFT')
			->where(array('a.id_penerimaan' => $id_penerimaan, 'a.tgl_verifikasi_ppmb' => NULL, 'a.is_proses_verifikasi' => 1))
			->where('a.tgl_submit_verifikasi IS NOT NULL', NULL, FALSE)
			->order_by('a.tgl_submit_verifikasi', 'asc')
			->get();
		
		foreach ($this->query->result() as $row)
		{
			array_push($result_set, $row);
		}
		
		return $result_set;
	}
	
	/**
	 * Mendapatkan antrian yang sedang dalam proses
	 * @param int $id_penerimaan
	 * @return array
	 */
	function list_peserta_kembali($id_penerimaan)
	{
		$result_set = array();
		
		$this->query = $this->db
			->select('a.id_c_mhs, a.nm_c_mhs, a.email, b.nm_pengguna, c.nm_program_studi as PRODI_1, d.nm_program_studi as PRODI_2, a.kode_voucher')
			->from('calon_mahasiswa_baru a')
			->join('pengguna b', 'a.id_verifikator_ppmb = b.id_pengguna', 'LEFT')
			->join('program_studi c', 'a.id_pilihan_1 = c.id_program_studi', 'LEFT')
			->join('program_studi d', 'a.id_pilihan_2 = d.id_program_studi', 'LEFT')
			->where(array('a.id_penerimaan' => $id_penerimaan, 'a.tgl_verifikasi_ppmb' => NULL, 'a.is_proses_verifikasi' => NULL, 'a.tgl_submit_verifikasi' => NULL ))
			->where('a.ID_VERIFIKATOR_PPMB IS NOT NULL', NULL, FALSE)
			->order_by('a.tgl_registrasi', 'asc')
			->get();
		
		foreach ($this->query->result() as $row)
		{
			array_push($result_set, $row);
		}
		
		return $result_set;
	}
	
	/**
	 * Mendapatkan list peserta terverifikasi
	 * @param type $id_penerimaan
	 * @return array
	 */
	function list_verifikasi_peserta($id_penerimaan)
	{
		$result_set = array();
		
		$this->query = $this->db
			->select('a.id_c_mhs, a.nm_c_mhs, a.kode_voucher, a.email, to_char(a.tgl_verifikasi_ppmb, \'YYYY-MM-DD HH24:MI:SS\') as tgl_verifikasi_ppmb, b.nm_pengguna, c.nm_program_studi as nm_program_studi_1, d.nm_program_studi as nm_program_studi_2, a.kode_voucher')
			->from('calon_mahasiswa_baru a')
			->join('pengguna b', 'a.id_verifikator_ppmb = b.id_pengguna', 'LEFT')
			->join('program_studi c', 'a.id_pilihan_1 = c.id_program_studi', 'LEFT')
			->join('program_studi d', 'a.id_pilihan_2 = d.id_program_studi', 'LEFT')
			->where('a.id_penerimaan', $id_penerimaan)
			->where('a.tgl_verifikasi_ppmb IS NOT NULL', NULL, FALSE)
			->order_by('a.tgl_verifikasi_ppmb', 'asc')
			->get();
		
		foreach ($this->query->result() as $row)
		{
			array_push($result_set, $row);
		}
		
		//print_r($result_set);
		// Mendapatkan pesan verifikator LOG
		//$this->query = $this->db->get_where('calon_mahasiswa_syarat_log', array('id_c_mhs' => $id_c_mhs, 'id_syarat_prodi' => '1'));
		//$cmb->LOG = $this->query->row();
		//$this->query->free_result();
		
		return $result_set;
	}
	
	/**
	 * Mendapatkan list peserta yg dikembalikan, tapi belum submit
	 * @param type $id_penerimaan
	 * @return array
	 */
	function list_kembali_peserta($id_penerimaan)
	{
		$result_set = array();
		
		$this->query = $this->db
			->select('a.id_c_mhs, a.nm_c_mhs, a.kode_voucher, a.email, to_char(a.tgl_verifikasi_ppmb, \'YYYY-MM-DD HH24:MI:SS\') as tgl_verifikasi_ppmb, b.nm_pengguna, a.kode_voucher')
			->from('calon_mahasiswa_baru a')
			->join('pengguna b', 'a.id_verifikator_ppmb = b.id_pengguna', 'LEFT')
			->where('a.id_penerimaan', $id_penerimaan)
			->where('a.id_verifikator_ppmb IS NOT NULL', NULL, FALSE)
			->where('a.tgl_submit_verifikasi IS NULL', NULL, FALSE)
			->order_by('a.email', 'asc')
			->get();
		
		foreach ($this->query->result() as $row)
		{
			array_push($result_set, $row);
		}
		
		return $result_set;
	}
	
	/**
	 * Mendapatkan list peserta yg diverifikasi
	 * @param type $id_penerimaan
	 * @param type $id_verifikator
	 * @return array
	 */
	function list_memverifikasi_peserta($id_penerimaan, $id_verifikator)
	{
		$result_set = array();
		
		$this->query = $this->db
			->select('a.id_c_mhs, a.nm_c_mhs, a.kode_voucher, a.email, to_char(a.tgl_verifikasi_ppmb, \'YYYY-MM-DD HH24:MI:SS\') as tgl_verifikasi_ppmb, b.nm_pengguna, c.id_kelompok_biaya_1, c.id_kelompok_biaya_2, c.id_kelompok_biaya_3, c.id_kelompok_biaya_4')
			->from('calon_mahasiswa_baru a')
			->join('pengguna b', 'a.id_verifikator_ppmb = b.id_pengguna', 'LEFT')
			->join('calon_mahasiswa_data c', 'a.id_c_mhs = c.id_c_mhs', 'LEFT')
			->where('a.id_penerimaan', $id_penerimaan)
			->where('a.id_verifikator_ppmb', $id_verifikator)
			->where('a.tgl_verifikasi_ppmb IS NOT NULL', NULL, FALSE)
			->order_by('a.tgl_verifikasi_ppmb', 'asc')
			->get();
		
		foreach ($this->query->result() as $row)
		{
			array_push($result_set, $row);
		}
		
		return $result_set;
	}
	
	function is_bisa_diverifikasi($id_c_mhs)
	{
		$this->query = $this->db
			->select('id_c_mhs')
			->from('calon_mahasiswa_baru')
			->where('is_proses_verifikasi', NULL)
			->where('tgl_verifikasi_ppmb', NULL)
			->where('id_c_mhs', $id_c_mhs)
			->get();
		
		$num_rows = $this->query->num_rows();
		
		return ($num_rows > 0);
	
	}
	
	function is_bisa_diverifikasi_2($id_c_mhs, $id_verifikator)
	{
		$this->query = $this->db->query("
			select
				id_c_mhs
			from aucc.calon_mahasiswa_baru
			where 
				(id_verifikator_ppmb is NULL or id_verifikator_ppmb =? )
				and tgl_verifikasi_ppmb is NULL
				and id_c_mhs = ?
			", 
			array($id_verifikator, $id_c_mhs ));
		
		$num_rows = $this->query->num_rows();
		
		return ($num_rows > 0);
	}
	
	/**
	 * Mendapatkan CALON_MAHASISWA_BARU
	 * @param int $id_c_mhs
	 * @return Object
	 */
	function get_cmb($id_c_mhs)
	{
		// Mendapatkan row CMB
		$this->query = $this->db->get_where('calon_mahasiswa_baru', array('id_c_mhs' => $id_c_mhs));
		$cmb = $this->query->row();
		$this->query->free_result();
		
		// Mendapatkan pesan verifikator LOG
		$this->query = $this->db->get_where('calon_mahasiswa_syarat_log', array('id_c_mhs' => $id_c_mhs, 'id_syarat_prodi' => '1'));
		$cmb->LOG = $this->query->row();
		$this->query->free_result();
		
		// Mendapatkan row PENERIMAAN
		$this->query = $this->db->get_where('penerimaan', array('id_penerimaan' => $cmb->ID_PENERIMAAN));
		$cmb->PENERIMAAN = $this->query->row();
		$this->query->free_result();
		
		if ($cmb->KODE_JURUSAN != '')
		{
			$this->query= $this->db
				->get_where('voucher_tarif', array('id_penerimaan' => $cmb->ID_PENERIMAAN, 'kode_jurusan' => $cmb->KODE_JURUSAN));
			$cmb->VOUCHER_TARIF = $this->query->row();
			$this->query->free_result();
		}
		
		if ($cmb->ID_PILIHAN_1)
		{
			$this->query = $this->db
					->select('j.nm_jenjang, ps.nm_program_studi')
					->join('jenjang j', 'j.id_jenjang = ps.id_jenjang', 'LEFT')
					->get_where('program_studi ps', array('ps.id_program_studi' => $cmb->ID_PILIHAN_1));
			$cmb->PILIHAN_1 = $this->query->row();
			$this->query->free_result();
		}
		
		if ($cmb->ID_PILIHAN_2)
		{
			$this->query = $this->db
					->select('j.nm_jenjang, ps.nm_program_studi')
					->join('jenjang j', 'j.id_jenjang = ps.id_jenjang', 'LEFT')
					->get_where('program_studi ps', array('ps.id_program_studi' => $cmb->ID_PILIHAN_2));
			$cmb->PILIHAN_2 = $this->query->row();
			$this->query->free_result();
		}
		
		if ($cmb->ID_PILIHAN_3)
		{
			$this->query = $this->db
					->select('j.nm_jenjang, ps.nm_program_studi')
					->join('jenjang j', 'j.id_jenjang = ps.id_jenjang', 'LEFT')
					->get_where('program_studi ps', array('ps.id_program_studi' => $cmb->ID_PILIHAN_3));
			$cmb->PILIHAN_3 = $this->query->row();
			$this->query->free_result();
		}
		
		if ($cmb->ID_PILIHAN_4)
		{
			$this->query = $this->db
					->select('nm_program_studi')
					->get_where('program_studi', array('id_program_studi' => $cmb->ID_PILIHAN_4));
			$cmb->PILIHAN_4 = $this->query->row();
			$this->query->free_result();
		}

		// Mendapatkan informasi prodi minat
		$this->query = $this->db
			->select('count(*) as jumlah_prodi_minat')
			->from('penerimaan_prodi_minat ppm')
			->join('prodi_minat pm', 'pm.id_prodi_minat = ppm.id_prodi_minat')
			->where(array(
				'ppm.id_penerimaan'		=> $cmb->ID_PENERIMAAN,
				'pm.id_program_studi'	=> $cmb->ID_PILIHAN_1,
				'ppm.is_aktif'			=> 1
			))->get();
		$cmb->JUMLAH_PRODI_MINAT = $this->query->row()->JUMLAH_PRODI_MINAT;
		$this->query->free_result();
		
		// Mendapatkan row SUMBER BIAYA
		$this->query = $this->db->get_where('sumber_biaya', array('id_sumber_biaya' => $cmb->SUMBER_BIAYA));
		$cmb->SUMBER_BIAYA_2 = $this->query->row();
		$this->query->free_result();
		
		return $cmb;
	}
	
	/**
	 * Mendapatkan CALON_MAHASISWA_FILE
	 * @param int $id_c_mhs
	 * @return Object Description
	 */
	function get_cmf($id_c_mhs)
	{
		$this->query = $this->db->get_where('calon_mahasiswa_file', array('id_c_mhs' => $id_c_mhs));
		$row = $this->query->row();
		$this->query->free_result();
		
		$this->query = $this->db->get_where('calon_mahasiswa_syarat_log', array('id_c_mhs' => $id_c_mhs, 'id_syarat_prodi'=>'2'));
		$row->LOG = $this->query->row();
		$this->query->free_result();
		
		return $row;
	}
	
	/**
	 * Mendapatkan row CALON_MAHASISWA_PASCA
	 * @param type $id_c_mhs
	 * @return type
	 */
	function get_cmp($id_c_mhs)
	{
		$this->query = $this->db
			->select('cmp.*, pm.nm_prodi_minat')
			->join('prodi_minat pm', 'pm.id_prodi_minat = cmp.id_prodi_minat', 'LEFT')
			->get_where('calon_mahasiswa_pasca cmp', array('id_c_mhs' => $id_c_mhs));
		$row = $this->query->row();
		$this->query->free_result();
		
		return $row;
	}
	
	/**
	 * Mendapatkan row CALON_MAHASISWA_SEKOLAH
	 * @param type $id_c_mhs
	 * @return type
	 */
	function get_cms($id_c_mhs)
	{
		$this->query = $this->db->get_where('calon_mahasiswa_sekolah', array('id_c_mhs' => $id_c_mhs));
		$row = $this->query->row();
		$this->query->free_result();
		
		$this->query = $this->db->get_where('sekolah', array('id_sekolah' => $row->ID_SEKOLAH_ASAL));
		$row->SEKOLAH_ASAL = $this->query->row();
		$this->query->free_result();
		
		return $row;
	}
	
	/**
	 * Mendapatkan row CALON_MAHASISWA_DATA
	 * @param type $id_c_mhs
	 * @return type
	 */
	function get_cmd($id_c_mhs)
	{
		$this->query = $this->db->get_where('calon_mahasiswa_data', array('id_c_mhs' => $id_c_mhs));
		$row = $this->query->row();
		$this->query->free_result();
		
		if( $row->ID_KELOMPOK_BIAYA_1 != ''  )
		{
			$this->query = $this->db->get_where('kelompok_biaya', array('id_kelompok_biaya' => $row->ID_KELOMPOK_BIAYA_1));
			$row->NM_KELOMPOK_BIAYA_1 = $this->query->row();
			$this->query->free_result();
		}
		
		
		return $row;
	}
	
	function get_list_syarat_umum($id_c_mhs, $id_penerimaan)
	{
		$result = array();
		
		$this->query = $this->db
			->select('psp.*, cms.file_syarat, cms.pesan_verifikator, nvl(cms.is_verified,0) as is_verified, cmsL.pesan_verifikator as pesan_verifikator_log')
			->from('penerimaan_syarat_prodi psp')
			->join('calon_mahasiswa_syarat cms', 'cms.id_syarat_prodi = psp.id_syarat_prodi and cms.id_c_mhs = '. $id_c_mhs, 'LEFT')
			->join('calon_mahasiswa_syarat_log cmsL', 'cmsL.id_syarat_prodi = psp.id_syarat_prodi and cmsL.id_c_mhs = '. $id_c_mhs, 'LEFT')
			->where(array('status_file' => 1, 'id_program_studi' => NULL, 'id_penerimaan' => $id_penerimaan))
			->order_by('urutan', 'asc')
			->get();
		
		/*
		$this->query = $this->db
			->select('psp.*, cms.file_syarat, cms.pesan_verifikator, nvl(cms.is_verified,0) as is_verified')
			->from('penerimaan_syarat_prodi psp')
			->join('calon_mahasiswa_syarat cms', 'cms.id_syarat_prodi = psp.id_syarat_prodi and cms.id_c_mhs = '. $id_c_mhs, 'LEFT')
			->where(array('status_file' => 1, 'id_program_studi' => NULL, 'id_penerimaan' => $id_penerimaan))
			->order_by('urutan', 'asc')
			->get();
		*/
		
		foreach ($this->query->result() as $row)
		{
			array_push($result, $row);
		}
		
		return $result;
	}
	
	function get_list_syarat_prodi($id_c_mhs, $id_penerimaan, $id_pilihan_1 = NULL, $id_pilihan_2 = NULL, $id_pilihan_3 = NULL, $id_pilihan_4 = NULL)
	{
		$result = array();
		
		if ($id_pilihan_1 != NULL)
		{
			$this->query = $this->db->select('nm_program_studi')->get_where('program_studi', array('id_program_studi' => $id_pilihan_1));
			$row = $this->query->row();
			$row->syarat_set = array();
			
			$this->query = $this->db
				->select('psp.*, file_syarat, pesan_verifikator, nvl(is_verified,0) as is_verified')
				->from('penerimaan_syarat_prodi psp')
				->join('calon_mahasiswa_syarat cms', 'cms.id_syarat_prodi = psp.id_syarat_prodi and cms.id_c_mhs = '. $id_c_mhs, 'LEFT')
				->where(array('status_file' => 1, 'id_program_studi' => $id_pilihan_1, 'id_penerimaan' => $id_penerimaan))
				->order_by('urutan', 'asc')
				->get();
			
			foreach ($this->query->result() as $row2)
			{
				array_push($row->syarat_set, $row2);
			}
			
			array_push($result, $row);
		}
		
		if ($id_pilihan_2 != NULL)
		{
			$this->query = $this->db->select('nm_program_studi')->get_where('program_studi', array('id_program_studi' => $id_pilihan_2));
			$row = $this->query->row();
			$row->syarat_set = array();
			
			$this->query = $this->db
				->select('psp.*, file_syarat, pesan_verifikator, nvl(is_verified,0) as is_verified')
				->from('penerimaan_syarat_prodi psp')
				->join('calon_mahasiswa_syarat cms', 'cms.id_syarat_prodi = psp.id_syarat_prodi and cms.id_c_mhs = '. $id_c_mhs, 'LEFT')
				->where(array('status_file' => 1, 'id_program_studi' => $id_pilihan_2, 'id_penerimaan' => $id_penerimaan))
				->order_by('urutan', 'asc')
				->get();
			
			foreach ($this->query->result() as $row2)
			{
				array_push($row->syarat_set, $row2);
			}
			
			array_push($result, $row);
		}
		
		if ($id_pilihan_3 != NULL)
		{
			$this->query = $this->db->select('nm_program_studi')->get_where('program_studi', array('id_program_studi' => $id_pilihan_3));
			$row = $this->query->row();
			$row->syarat_set = array();
			
			$this->query = $this->db
				->select('psp.*, file_syarat, pesan_verifikator, nvl(is_verified,0) as is_verified')
				->from('penerimaan_syarat_prodi psp')
				->join('calon_mahasiswa_syarat cms', 'cms.id_syarat_prodi = psp.id_syarat_prodi and cms.id_c_mhs = '. $id_c_mhs, 'LEFT')
				->where(array('status_file' => 1, 'id_program_studi' => $id_pilihan_3, 'id_penerimaan' => $id_penerimaan))
				->order_by('urutan', 'asc')
				->get();
			
			foreach ($this->query->result() as $row2)
			{
				array_push($row->syarat_set, $row2);
			}
			
			array_push($result, $row);
		}
		
		if ($id_pilihan_4 != NULL)
		{
			$this->query = $this->db->select('nm_program_studi')->get_where('program_studi', array('id_program_studi' => $id_pilihan_4));
			$row = $this->query->row();
			$row->syarat_set = array();
			
			$this->query = $this->db
				->select('psp.*, file_syarat, pesan_verifikator, nvl(is_verified,0) as is_verified')
				->from('penerimaan_syarat_prodi psp')
				->join('calon_mahasiswa_syarat cms', 'cms.id_syarat_prodi = psp.id_syarat_prodi and cms.id_c_mhs = '. $id_c_mhs, 'LEFT')
				->where(array('status_file' => 1, 'id_program_studi' => $id_pilihan_4, 'id_penerimaan' => $id_penerimaan))
				->order_by('urutan', 'asc')
				->get();
			
			foreach ($this->query->result() as $row2)
			{
				array_push($row->syarat_set, $row2);
			}
			
			array_push($result, $row);
		}
		
		return $result;
	}
	
	function set_syarat_valid($id_c_mhs, $id_syarat_prodi)
	{
		$this->db->trans_start();
		
		if ($id_syarat_prodi == 'foto')
		{
			$this->db->set(array('is_foto_verified' => 1, 'pesan_v_foto' => NULL));
			$this->db->where(array('id_c_mhs' => $id_c_mhs));
			$this->db->update('calon_mahasiswa_file');
		}
		else if ($id_syarat_prodi == 'berkas_pernyataan')
		{
			$this->db->set(array('is_berkas_pernyataan_verified' => 1, 'pesan_v_berkas_pernyataan' => NULL));
			$this->db->where(array('id_c_mhs' => $id_c_mhs));
			$this->db->update('calon_mahasiswa_file');
		}
		else if (is_numeric($id_syarat_prodi))
		{
			$now = date('Y-m-d H:i:s');
			
			$this->db->set(array('is_verified' => 1, 'pesan_verifikator' => NULL));
			$this->db->set('tgl_valid_syarat', "to_date('{$now}','YYYY-MM-DD HH24:MI:SS')", FALSE);
			$this->db->where(array('id_c_mhs' => $id_c_mhs, 'id_syarat_prodi' => $id_syarat_prodi));
			$this->db->update('calon_mahasiswa_syarat');
		}
		
		$this->db->trans_complete();
	}
	
	function set_syarat_valid_2 ($id_c_mhs, $id_syarat_prodi)
	{
		if (is_numeric($id_syarat_prodi) )
		{
			$this->db->delete('calon_mahasiswa_syarat_log', array('id_c_mhs'=>$id_c_mhs, 'id_syarat_prodi'=>$id_syarat_prodi ) );
			//echo $this->db->_error_message();
		}
		else if ($id_syarat_prodi == 'pesan_optional')
		{
			$this->db->delete('calon_mahasiswa_syarat_log', array('id_c_mhs'=>$id_c_mhs, 'id_syarat_prodi'=>'1' ) );
			//echo $this->db->_error_message();
		}
		else if ($id_syarat_prodi == 'foto')
		{
			$this->db->delete('calon_mahasiswa_syarat_log', array('id_c_mhs'=>$id_c_mhs, 'id_syarat_prodi'=>'2' ) );
			//echo $this->db->_error_message();
		}
	}
	
	function set_syarat_valid_all ($id_c_mhs)
	{
		$this->db->trans_start();
		
			$now = date('Y-m-d H:i:s');
				
			$this->db->set(array('is_verified' => 1, 'pesan_verifikator' => NULL));
			$this->db->set('tgl_valid_syarat', "to_date('{$now}','YYYY-MM-DD HH24:MI:SS')", FALSE);
			$this->db->where(array('id_c_mhs' => $id_c_mhs, 'is_verified' => 0 ));
			$this->db->update('calon_mahasiswa_syarat');
			
			$this->db->set(array('is_foto_verified' => 1, 'pesan_v_foto' => NULL));
			$this->db->where(array('id_c_mhs' => $id_c_mhs));
			$this->db->update('calon_mahasiswa_file');
		
		$this->db->trans_complete();
	}
	
	function set_syarat_invalid($id_c_mhs, $id_syarat_prodi, $pesan)
	{
		$this->db->trans_start();
		
		if ($id_syarat_prodi == 'foto')
		{
			// update CMF
			$this->db->set(array('is_foto_verified' => 0, 'pesan_v_foto' => $pesan));
			$this->db->where(array('id_c_mhs' => $id_c_mhs));
			$this->db->update('calon_mahasiswa_file');
		}
		else if ($id_syarat_prodi == 'pesan_optional')
		{
			// update CMB
			$this->db->set(array('pesan_verifikator' => $pesan));
			$this->db->where(array('id_c_mhs' => $id_c_mhs));
			$this->db->update('calon_mahasiswa_baru');
		}
		else if ($id_syarat_prodi == 'berkas_pernyataan')
		{
			// update CMF
			$this->db->set(array('is_berkas_pernyataan_verified' => 0, 'pesan_v_berkas_pernyataan' => $pesan));
			$this->db->where(array('id_c_mhs' => $id_c_mhs));
			$this->db->update('calon_mahasiswa_file');
		}
		else if (is_numeric($id_syarat_prodi))
		{
			// Mendapatkan waktu saat ini
			$now = date('Y-m-d H:i:s');
			
			// update CMF
			$this->db->set(array('is_verified' => 0, 'pesan_verifikator' => $pesan));
			$this->db->set('tgl_invalid_syarat', "to_date('{$now}','YYYY-MM-DD HH24:MI:SS')", FALSE);
			$this->db->where(array('id_c_mhs' => $id_c_mhs, 'id_syarat_prodi' => $id_syarat_prodi));
			$this->db->update('calon_mahasiswa_syarat');
		}
		
		// update CMB
		// dibatalkan proses submitnya, agar tidak muncul di antrian lagi
		// $this->db->update('calon_mahasiswa_baru', array('tgl_submit_verifikasi' => NULL), array('id_c_mhs' => $id_c_mhs));
		
		$this->db->trans_complete();
	}
	
	function set_syarat_batalvalid($id_c_mhs, $id_syarat_prodi)
	{
		$this->db->trans_start();
		
		if ($id_syarat_prodi == 'foto')
		{
			// update CMF
			$this->db->set(array('is_foto_verified' => 0 ));
			$this->db->where(array('id_c_mhs' => $id_c_mhs));
			$this->db->update('calon_mahasiswa_file');
		}
		else if (is_numeric($id_syarat_prodi))
		{
			// update CMF
			$this->db->set(array('is_verified' => 0, 'tgl_valid_syarat' => NULL ));
			$this->db->where(array('id_c_mhs' => $id_c_mhs, 'id_syarat_prodi' => $id_syarat_prodi));
			$this->db->update('calon_mahasiswa_syarat');
		}
		
		// update CMB
		// dibatalkan proses submitnya, agar tidak muncul di antrian lagi
		// $this->db->update('calon_mahasiswa_baru', array('tgl_submit_verifikasi' => NULL), array('id_c_mhs' => $id_c_mhs));
		
		$this->db->trans_complete();
	}
	
	function set_syarat_invalid_2 ($id_c_mhs, $id_syarat_prodi, $pesan)
	{
		if (is_numeric($id_syarat_prodi) )
		{
			$this->db->delete('calon_mahasiswa_syarat_log', array('id_c_mhs'=>$id_c_mhs, 'id_syarat_prodi'=>$id_syarat_prodi ) );
			
			$data = array(
				'ID_C_MHS' => $id_c_mhs,
				'ID_SYARAT_PRODI' => $id_syarat_prodi,
				'PESAN_VERIFIKATOR' => $pesan
			);
			$now = date('Y-m-d H:i:s');
			$this->db->set('TGL_INVALID_SYARAT', "to_date('{$now}','YYYY-MM-DD HH24:MI:SS')", FALSE);
			$this->db->insert('calon_mahasiswa_syarat_log', $data);
			//echo $this->db->_error_message();
		}
		else if ($id_syarat_prodi == 'pesan_optional')
		{
			$this->db->delete('calon_mahasiswa_syarat_log', array('id_c_mhs'=>$id_c_mhs, 'id_syarat_prodi'=>'1' ) );
			
			$data = array(
				'ID_C_MHS' => $id_c_mhs,
				'ID_SYARAT_PRODI' => '1',
				'PESAN_VERIFIKATOR' => $pesan
			);
			$now = date('Y-m-d H:i:s');
			$this->db->set('TGL_INVALID_SYARAT', "to_date('{$now}','YYYY-MM-DD HH24:MI:SS')", FALSE);
			$this->db->insert('calon_mahasiswa_syarat_log', $data);
			//echo $this->db->_error_message();
		}
		else if ($id_syarat_prodi == 'foto')
		{
			$this->db->delete('calon_mahasiswa_syarat_log', array('id_c_mhs'=>$id_c_mhs, 'id_syarat_prodi'=>'2' ) );
			$data = array(
				'ID_C_MHS' => $id_c_mhs,
				'ID_SYARAT_PRODI' => '2',
				'PESAN_VERIFIKATOR' => $pesan
			);
			$now = date('Y-m-d H:i:s');
			$this->db->set('TGL_INVALID_SYARAT', "to_date('{$now}','YYYY-MM-DD HH24:MI:SS')", FALSE);
			$this->db->insert('calon_mahasiswa_syarat_log', $data);
			//echo $this->db->_error_message();
		}
	}
	
	function set_syarat_invalid_all ($id_c_mhs)
	{
		$this->db->trans_start();
			$now = date('Y-m-d H:i:s');
					
			$this->db->set(array('is_verified' => 0, 'pesan_verifikator' => 'Tidak Valid'));
			$this->db->set('tgl_invalid_syarat', "to_date('{$now}','YYYY-MM-DD HH24:MI:SS')", FALSE);
			$this->db->where(array('id_c_mhs' => $id_c_mhs ));
			$this->db->update('calon_mahasiswa_syarat');
			
			$this->db->set(array('is_foto_verified' => 0, 'pesan_v_foto' => 'Tidak Valid'));
			$this->db->where(array('id_c_mhs' => $id_c_mhs));
			$this->db->update('calon_mahasiswa_file');
		$this->db->trans_complete();
	}
	
	/**
	 * Mendapatkan status syarat sudah verified semua
	 * @param Array $cmb
	 * @param Array $cmf
	 * @param Array $syarat_umum_set
	 * @param Array $syarat_prodi_set
	 * @return int <p>Kode hasil verifikasi:
	 * 0 - Belum di submit / belum lengkap
	 * 1 - Verifikasi OK tidak ada masalah
	 * 2 - Verifikasi ada catatan dari verifikator
	 * </p>
	 */
	function is_syarat_verified(&$cmf, &$syarat_umum_set, &$syarat_prodi_set, $apesan_verifikator)
	{
		$result = 0;
		
		if ($cmf->IS_FOTO_VERIFIED)
		{
			$result = 1;
		}
		else if ($cmf->PESAN_V_FOTO != '')
		{
			$result = 2;
		}
		
		if ($apesan_verifikator == '' and $result == 1)
		{
			$result = 1;
		}
		else
		{
			$result = 2;
		}
		
		if ($result == 1)
		{			
			foreach ($syarat_umum_set as $su)
			{	
				if ($su->STATUS_WAJIB == 1)
				{
					if ($su->IS_VERIFIED == 1)
					{
						$result = 1;
					}
					else if ($su->PESAN_VERIFIKATOR != '')
					{
						$result = 2;
						break;
					}
					else
					{
						$result = 0;
						break;
					}
				}
				else
				{
					if ($su->PESAN_VERIFIKATOR != '')
					{
						$result = 2;
						break;
					}
					else
					{
						$result = 1;
					}
				}
			}
		}
		
		if ($result == 1)
		{
			foreach ($syarat_prodi_set as $prodi)
			{
				foreach ($prodi->syarat_set as $sp)
				{
					if ($sp->STATUS_WAJIB == 1)
					{
						if ($sp->IS_VERIFIED)
						{
							$result = 1;
						}
						else if ($sp->PESAN_VERIFIKATOR != '')
						{
							$result = 2;
							break 2;
						}
						else
						{
							$result = 0;
							break 2;
						}
					}
					else
					{
						if ($sp->PESAN_VERIFIKATOR != '')
						{
							$result = 2;
							break 2;
						}
						else
						{
							$result = 1;
						}
					}
				}
			}
		}
		
		return $result;
	}
	
	/**
	 * Mendapatkan status syarat sudah komplit semua
	 * @param Array $cmb
	 * @param Array $cmf
	 * @param Array $syarat_umum_set
	 * @param Array $syarat_prodi_set
	 * @return boolean
	 */
	function is_syarat_complete(&$cmf, &$syarat_umum_set, &$syarat_prodi_set)
	{
		$result = TRUE;
		
		if ($cmf->FILE_FOTO == '') { $result = FALSE; }
		
		if ($result == TRUE)
		{
			foreach ($syarat_umum_set as $su)
			{
				if ($su->STATUS_WAJIB == 1 && $su->FILE_SYARAT == '')
				{
					$result = FALSE;
					break;
				}
			}
		}
		
		if ($result == TRUE)
		{
			foreach ($syarat_prodi_set as $prodi)
			{
				foreach ($prodi->syarat_set as $sp)
				{
					if ($sp->STATUS_WAJIB == 1 && $sp->FILE_SYARAT == '')
					{
						$result = FALSE;
						break;
					}
				}
			}
		}
		
		return $result;
	}
	
	/**
	 * 
	 * @param type $cmf
	 * @param type $syarat_umum_set
	 * @param type $syarat_prodi_set
	 * @return boolean
	 */
	function is_syarat_complete_check(&$cmf, &$syarat_umum_set, &$syarat_prodi_set)
	{
		$result = true;
		
		// Jika foto sudah ada, dan ada pesan verifikator / verified
		if ($cmf->PESAN_V_FOTO == '' && $cmf->IS_FOTO_VERIFIED == 0)
		{
			$result = false;
		}
		
		if ($result == true)
		{
			foreach ($syarat_umum_set as $su)
			{
				// hanya di cek yang sudah diupload
				if ($su->FILE_SYARAT != '')
				{
					if ($su->PESAN_VERIFIKATOR == '' && $su->IS_VERIFIED == 0)
					{
						$result = false;
						break;
					}
				}
			}
		}
		
		if ($result == true)
		{
			foreach ($syarat_prodi_set as $prodi)
			{
				foreach ($prodi->syarat_set as $sp)
				{
					// hanya di cek yang sudah diupload
					if ($sp->FILE_SYARAT != '')
					{
						if ($sp->PESAN_VERIFIKATOR == '' && $sp->IS_VERIFIED == 0)
						{
							$result = false;
							break;
						}
					}
				}
			}
		}
		
		return $result;
	}
	
	/**
	 * Start Verifikasi
	 * @param int $id_c_mhs
	 */
	function set_verifikasi_start($id_c_mhs, $id_verifikator)
	{
		$this->db->update('calon_mahasiswa_baru', array('is_proses_verifikasi' => 1, 'id_verifikator_ppmb' => $id_verifikator), array('id_c_mhs' => $id_c_mhs, 'tgl_verifikasi_ppmb' => NULL));
	}
	
	/**
	 * Cancel Verifikasi
	 * @param int $id_c_mhs
	 */
	function set_verifikasi_cancel($id_c_mhs)
	{
		$this->db->trans_start();
		
		// Kosongi foto dan berkas pernyataan
		$this->db->update('calon_mahasiswa_file', 
			array(
				'is_foto_verified'	=> 0,
				'pesan_v_foto'		=> NULL,
				'is_berkas_pernyataan_verified' => 0,
				'pesan_v_berkas_pernyataan' => NULL
			), 
			array('id_c_mhs' => $id_c_mhs));
		
		// Kosongi semua syarat
		$this->db->update('calon_mahasiswa_syarat', 
			array(
				'is_verified'		=> 0,
				'pesan_verifikator'	=> NULL
			),
			array('id_c_mhs' => $id_c_mhs));
		
		// Kosongi status proses dan isian sp3
		$this->db->update('calon_mahasiswa_baru', 
			array(
				'is_proses_verifikasi' => NULL,
				'sp3_1'		=> NULL,
				'sp3_2'		=> NULL,
				'sp3_3'		=> NULL,
				'sp3_4'		=> NULL,
				'is_matrikulasi' => NULL
			), 
			array('id_c_mhs' => $id_c_mhs));
		
		$this->db->trans_complete();
	}
	
	/**
	 * Kembalikan hasil verifikasi
	 * @param type $id_c_mhs
	 */
	function set_verifikasi_kembalikan($id_c_mhs)
	{
		$this->db->update('calon_mahasiswa_baru', array('is_proses_verifikasi' => NULL, 'tgl_submit_verifikasi' => NULL), array('id_c_mhs' => $id_c_mhs));
	}
	
	/**
	 * Mengeset verifikasi komplit
	 * @param int $id_c_mhs
	 * @param int $id_verifikator
	 * @return string <p>Hasil proses
	 * - COMPLETE : Selesai
	 * </p>
	 */
	function set_verifikasi_complete($id_c_mhs, $id_verifikator)
	{	
		// Dibuat transaksi, proses atomic
		$this->db->trans_start();

		// Mendapatkan data CMB
		$this->query = $this->db
			->select('tgl_verifikasi_ppmb, id_penerimaan, id_pilihan_1, id_pilihan_2, id_pilihan_3, id_pilihan_4, sp3_1, sp3_2, sp3_3, sp3_4')
			->get_where('calon_mahasiswa_baru', array('id_c_mhs' => $id_c_mhs));
		$cmb = $this->query->row();
		
		// Mendapatkan data CMD
		$this->query = $this->db
			->select('id_kelompok_biaya_1, id_kelompok_biaya_2, id_kelompok_biaya_3, id_kelompok_biaya_4')
			->get_where('calon_mahasiswa_data', array('id_c_mhs' => $id_c_mhs));
		$cmd = $this->query->row();
		
		// Mendapatkan data Penerimaan
		$this->query = $this->db
			->select("id_jenjang, id_jalur")
			->get_where('penerimaan', array('id_penerimaan' => $cmb->ID_PENERIMAAN));
		$penerimaan = $this->query->row();
		
		// clear result
		$this->query->free_result();
		
		// Jika sudah terdapat sudah pernah diverifikasi maka dibatalkan
		// Untuk mencegah double submit
		if ($cmb->TGL_VERIFIKASI_PPMB != '')
		{
			return 'COMPLETE';
		}
		
		// --------------------------
		// Mengecek isian SP3 / UKT
		// --------------------------
		// Jika S1 Mandiri -> UKT
		// per 2015 D3 mengikuti UKT
		/*if (($penerimaan->ID_JENJANG == 1 && $penerimaan->ID_JALUR == 3) || 
			($penerimaan->ID_JENJANG == 5 && $penerimaan->ID_JALUR == 5) ||
			($penerimaan->ID_JENJANG == 4 ))
		{
			// Cek tiap isian kelompok biaya
			if ($cmb->ID_PILIHAN_1 != '') { if ($cmd->ID_KELOMPOK_BIAYA_1 == '') { return 'ISIAN_UKT'; } }
			if ($cmb->ID_PILIHAN_2 != '') { if ($cmd->ID_KELOMPOK_BIAYA_2 == '') { return 'ISIAN_UKT'; } }
			if ($cmb->ID_PILIHAN_3 != '') { if ($cmd->ID_KELOMPOK_BIAYA_3 == '') { return 'ISIAN_UKT'; } }
			if ($cmb->ID_PILIHAN_4 != '') { if ($cmd->ID_KELOMPOK_BIAYA_4 == '') { return 'ISIAN_UKT'; } }
		}
		else // selain S1 Mandiri -> SP3
		{
			// Cek tiap isian kelompok biaya
			if ($cmb->ID_PILIHAN_1 != '') { if ($cmb->SP3_1 == '') { return 'ISIAN_SP3'; } }
			if ($cmb->ID_PILIHAN_2 != '') { if ($cmb->SP3_2 == '') { return 'ISIAN_SP3'; } }
			if ($cmb->ID_PILIHAN_3 != '') { if ($cmb->SP3_3 == '') { return 'ISIAN_SP3'; } }
			if ($cmb->ID_PILIHAN_4 != '') { if ($cmb->SP3_4 == '') { return 'ISIAN_SP3'; } }
		}*/
		
		// Mendapatkan waktu saat ini
		$now = date('Y-m-d H:i:s');
		
		// Proses update CMB
		$this->db->set('tgl_verifikasi_ppmb', "to_date('{$now}','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$this->db->set('id_verifikator_ppmb', $id_verifikator);
		$this->db->set('is_proses_verifikasi', NULL);
		$this->db->update('calon_mahasiswa_baru', NULL, array('id_c_mhs' => $id_c_mhs));
		
		// update calon_mahasiswa_syarat_log
		$this->db->delete('calon_mahasiswa_syarat_log', array('id_c_mhs'=>$id_c_mhs, 'id_syarat_prodi'=>'1' ) );
		
		// Finish transaksi
		$this->db->trans_complete();
		
		return 'COMPLETE';
	}
	
	/**
	 * Mendapatkan data cmb untuk di reset
	 * @param string $q
	 * @return mixed
	 */
	function get_cmb_reset($q)
	{
		$this->query = $this->db
			->select('cmb.id_c_mhs, nm_c_mhs, no_ujian, cmb.kode_voucher')
			->from('calon_mahasiswa_baru cmb')
			->join('penerimaan p', 'p.id_penerimaan = cmb.id_penerimaan')
			->join('voucher v', 'v.kode_voucher = cmb.kode_voucher', 'LEFT')
			->where(array('p.is_aktif' => 1))						// penerimaan sedang aktif
			->where('tgl_verifikasi_ppmb IS NOT NULL', NULL, false)	// peserta sudah verifikasi
			->where(array('tgl_diterima' => NULL))					// belum diterima
			//->where('v.tgl_bayar IS NULL', NULL, false)				// voucher belum dibayar (dikomen karena semua yg terverifikasi pasti sudah bayar)
			// ->where('sysdate between tgl_awal_verifikasi and tgl_akhir_verifikasi', NULL, false)	// dalam rentang tgl verifikasi
			->where("(no_ujian like '{$q}' or cmb.kode_voucher like '{$q}')", NULL, false)	// filter no_ujian atau kode_voucher
			->get();
		
		return $this->query->row();
	}
	
	/**
	 * Mereset verifikasi peserta
	 * @param int $id_c_mhs
	 */
	function reset_verifikasi($id_c_mhs)
	{
		// Transaksi Atomic
		$this->db->trans_start();
		
		// Mengkosongkan KODE_VOUCHER, PIN_VOUCHER, TGL_VERIFIKASI_PPMB, ID_VERIFIKATOR, IS_PROSES_VERIFIKASI di CMB
		$this->db->update('calon_mahasiswa_baru', array(
			'tgl_verifikasi_ppmb'	=> NULL,
			'id_verifikator_ppmb'	=> NULL,
			'is_proses_verifikasi'	=> NULL
		), array('id_c_mhs' => $id_c_mhs));
		
		// Reset FOTO dan BERKAS_PERNYATAAN di CMF (Calon_Mahasiswa_File)
		$this->db->update('calon_mahasiswa_file', array(
			'is_foto_verified'				=> 0,
			'is_berkas_pernyataan_verified'	=> 0,
			'pesan_v_foto'					=> NULL,
			'pesan_v_berkas_pernyataan'		=> NULL
		), array('id_c_mhs' => $id_c_mhs));
		
		// Reset PESAN_VERIFIKATOR + IS_VERIFIED di CMS (Calon_Mahasiswa_Syarat)
		$this->db->update('calon_mahasiswa_syarat', array(
			'is_verified'		=> 0,
			'pesan_verifikator'	=> NULL
		), array('id_c_mhs' => $id_c_mhs));
		
		// Transaksi Commit
		$this->db->trans_complete();
	}
	
	/**
	 * Mendapatkan minimal isian sp3 
	 * @param mixed $cmb
	 */
	function get_sp3_cmb(&$cmb, &$cmd)
	{
		$penerimaan = $cmb->PENERIMAAN;
		
		/**
		 * Mendapatkan Jenis Biaya (SP3 / UKA) dan ID_Kelompok_biaya
		 */
		if (in_array($penerimaan->ID_JALUR, array(4, 5, 23, 24, 27, 41, 6)))	// Jika D3 / ALIH JENIS / Mandiri Pasca Int / FK-Int / Profesi / Spesialis
		{
			if ($penerimaan->ID_JALUR == 41)
			{
				$id_kelompok_biaya = 239;
			}
			else
			{
				if( $cmd->ID_KELOMPOK_BIAYA_1 != '' )
				{
					$id_kelompok_biaya = $cmd->ID_KELOMPOK_BIAYA_1;
				}
				else
				{
					$id_kelompok_biaya = 1;
				}
			}
			
			$biaya = 'SP3';
		}
		else if (in_array($penerimaan->ID_JALUR, array(1, 3)))	// Jika SNMPTN / Mandiri S1
		{
			$id_kelompok_biaya = 1;
			$biaya = 'UKA';
		}
		
		// Untuk Pilihan 1
		if ($cmb->ID_PILIHAN_1 != '')
		{
			$cmb->PILIHAN_1->MINIMAL_BIAYA = $this->get_besar_biaya($biaya, $penerimaan->ID_SEMESTER, $penerimaan->ID_JALUR, $cmb->ID_PILIHAN_1, $id_kelompok_biaya);
		}
		
		// Untuk Pilihan 2
		if ($cmb->ID_PILIHAN_2 != '')
		{
			$cmb->PILIHAN_2->MINIMAL_BIAYA = $this->get_besar_biaya($biaya, $penerimaan->ID_SEMESTER, $penerimaan->ID_JALUR, $cmb->ID_PILIHAN_2, $id_kelompok_biaya);
		}
		
		// Untuk Pilihan 3
		if ($cmb->ID_PILIHAN_3 != '')
		{
			$cmb->PILIHAN_3->MINIMAL_BIAYA = $this->get_besar_biaya($biaya, $penerimaan->ID_SEMESTER, $penerimaan->ID_JALUR, $cmb->ID_PILIHAN_3, $id_kelompok_biaya);
		}
		
		// Untuk Pilihan 4
		if ($cmb->ID_PILIHAN_4 != '')
		{
			$cmb->PILIHAN_4->MINIMAL_BIAYA = $this->get_besar_biaya($biaya, $penerimaan->ID_SEMESTER, $penerimaan->ID_JALUR, $cmb->ID_PILIHAN_4, $id_kelompok_biaya);
		}
		
		// echo '<pre>' . print_r($penerimaan, true) . '</pre>'; exit();
	}
	
	function get_sop_sp3($id_penerimaan, $id_program_studi, $id_kelompok_biaya)
	{
		$this->query = $this->db
			->select('nm_biaya, db.besar_biaya')
			->from('detail_biaya db')
			->join('biaya b', 'b.id_biaya = db.id_biaya AND b.nm_biaya IN (\'SOP\',\'SP3\')')
			->join('biaya_kuliah bk', "bk.id_biaya_kuliah = db.id_biaya_kuliah AND bk.id_program_studi = {$id_program_studi} AND bk.id_kelompok_biaya = {$id_kelompok_biaya}")
			->join('penerimaan p', "p.id_semester = bk.id_semester AND p.id_jalur = bk.id_jalur AND p.id_penerimaan = {$id_penerimaan}")
			->get();
			
		$result_set = array();
		
		foreach ($this->query->result() as $row)
		{
			$row->BESAR_BIAYA = number_format($row->BESAR_BIAYA, 0, ',', '.');
			array_push($result_set, $row);
		}
		
		return $result_set;
	}
	
	private function get_besar_biaya($biaya, $id_semester, $id_jalur, $id_program_studi, $id_kelompok_biaya)
	{
		/**
		 * select db.besar_biaya from detail_biaya db
            left join biaya_kuliah bk on bk.id_biaya_kuliah = db.id_biaya_kuliah
            left join biaya b on b.id_biaya = db.id_biaya
            where b.nm_biaya = 'SP3' and bk.id_semester = {$id_semester} and bk.id_jalur = {$id_jalur}
            and bk.id_kelompok_biaya = {$id_kelompok_biaya} and bk.id_program_studi = {$id_program_studi}
		 */
		
		$this->query = $this->db
			->select('db.besar_biaya')
			->from('detail_biaya db')
			->join('biaya_kuliah bk', 'bk.id_biaya_kuliah = db.id_biaya_kuliah')
			->join('biaya b', 'b.id_biaya = db.id_biaya')
			->where(array(
				'b.nm_biaya'			=> $biaya,
				'bk.id_semester'		=> (int)$id_semester,
				'bk.id_jalur'			=> (int)$id_jalur,
				'bk.id_program_studi'	=> (int)$id_program_studi,
				'bk.id_kelompok_biaya'	=> (int)$id_kelompok_biaya
			))
			->get();
		
		if ($this->query->num_rows() == 1)
		{
			return $this->query->row()->BESAR_BIAYA;
		}
		else
		{
			return 0;
		}
	}
	
	/**
	 * Mengupdate pilihan kelompk biaya untuk pembayaran UKT
	 * @param Object $cmb
	 */
	function get_kelompok_biaya_cmb(&$cmb)
	{
		if ($cmb->ID_PILIHAN_1 != '')
		{
			$cmb->PILIHAN_1->kelompok_biaya_set = $this->get_list_kelompok_biaya($cmb->PENERIMAAN->ID_SEMESTER, $cmb->PENERIMAAN->ID_JENJANG, $cmb->PENERIMAAN->ID_JALUR, $cmb->ID_PILIHAN_1);
		}
		
		if ($cmb->ID_PILIHAN_2 != '')
		{
			$cmb->PILIHAN_2->kelompok_biaya_set = $this->get_list_kelompok_biaya($cmb->PENERIMAAN->ID_SEMESTER, $cmb->PENERIMAAN->ID_JENJANG, $cmb->PENERIMAAN->ID_JALUR, $cmb->ID_PILIHAN_2);
		}
		
		if ($cmb->ID_PILIHAN_3 != '')
		{
			$cmb->PILIHAN_3->kelompok_biaya_set = $this->get_list_kelompok_biaya($cmb->PENERIMAAN->ID_SEMESTER, $cmb->PENERIMAAN->ID_JENJANG, $cmb->PENERIMAAN->ID_JALUR, $cmb->ID_PILIHAN_3);
		}
		
		if ($cmb->ID_PILIHAN_4 != '')
		{
			$cmb->PILIHAN_4->kelompok_biaya_set = $this->get_list_kelompok_biaya($cmb->PENERIMAAN->ID_SEMESTER, $cmb->PENERIMAAN->ID_JENJANG, $cmb->PENERIMAAN->ID_JALUR, $cmb->ID_PILIHAN_4);
		}
	}
	
	private function get_list_kelompok_biaya($id_semester, $id_jenjang, $id_jalur, $id_program_studi)
	{
		$this->query = $this->db
			->select('kb.id_kelompok_biaya, kb.nm_kelompok_biaya')
			->from('biaya_kuliah bk')
			->join('kelompok_biaya kb', 'kb.id_kelompok_biaya = bk.id_kelompok_biaya')
			->where('bk.besar_biaya_kuliah is not null', NULL, FALSE)
			->where(array(
				'bk.id_semester'		=> $id_semester,
				'bk.id_jenjang'			=> $id_jenjang,
				'bk.id_jalur'			=> $id_jalur,
				'bk.id_program_studi'	=> $id_program_studi
			))
			->order_by('2 asc')
			->get();
		
		$result_set = array();
		
		foreach ($this->query->result() as $row)
		{
			array_push($result_set, $row);
		}
		
		return $result_set;
	}
	
	function update_sp3($id_c_mhs, $sp3_1, $sp3_2 = null, $sp3_3 = null, $sp3_4 = null, $is_matrikulasi = null)
	{
		$this->db->set(array(
			'sp3_1'	=> !empty($sp3_1) ? (int)$sp3_1 : NULL,
			'sp3_2'	=> !empty($sp3_2) ? (int)$sp3_2 : NULL,
			'sp3_3'	=> !empty($sp3_3) ? (int)$sp3_3 : NULL,
			'sp3_4'	=> !empty($sp3_4) ? (int)$sp3_4 : NULL,
			'is_matrikulasi' => !empty($is_matrikulasi) ? $is_matrikulasi : NULL
		));
		$this->db->where('id_c_mhs', $id_c_mhs);
		$this->db->update('calon_mahasiswa_baru');
	}
	
	function update_kelompok_biaya($id_c_mhs, $id_kb_1, $id_kb_2 = null, $id_kb_3 = null, $id_kb_4 = null)
	{
		$this->db->update('calon_mahasiswa_data', array(
			'id_kelompok_biaya_1'	=> !empty($id_kb_1) ? (int)$id_kb_1 : NULL,
			'id_kelompok_biaya_2'	=> !empty($id_kb_2) ? (int)$id_kb_2 : NULL,
			'id_kelompok_biaya_3'	=> !empty($id_kb_3) ? (int)$id_kb_3 : NULL,
			'id_kelompok_biaya_4'	=> !empty($id_kb_4) ? (int)$id_kb_4 : NULL
		), array('id_c_mhs' => $id_c_mhs));
	}
	
	function update_akreditasi($id_c_mhs, $jenis_akreditasi_s1, $peringkat_akreditasi_s1)
	{
		return $this->db->update('calon_mahasiswa_pasca', array(
			'jenis_akreditasi_s1'		=> $jenis_akreditasi_s1,
			'peringkat_akreditasi_s1'	=> $peringkat_akreditasi_s1
		), array('id_c_mhs' => $id_c_mhs));
	}
	
	function update_pt_asal($id_c_mhs, $status_ptn_s1, $ip_s1, $status_ptn_s2 = null, $ip_s2 = null, $nilai_toefl = null)
	{
		return $this->db->update('calon_mahasiswa_pasca', array(
			'status_ptn_s1'	=> $status_ptn_s1,
			'ip_s1'			=> (float)$ip_s1,
			'status_ptn_s2'	=> !empty($status_ptn_s2) ? $status_ptn_s2 : NULL,
			'ip_s2'			=> !empty($ip_s2) ? (float)$ip_s2 : NULL,
			'nilai_toefl'	=> !empty($nilai_toefl) ? (float)$nilai_toefl : NULL,
		), array('id_c_mhs' => $id_c_mhs));
	}
	
	function update_gelar ($id_c_mhs, $gelar_s1)
	{
		return $this->db->update('calon_mahasiswa_baru', 
				array(
					'gelar'	=> $gelar_s1
				), 
				array('id_c_mhs' => $id_c_mhs)
			);
	}
	
	/**
	 * Mendapatkan set CMB peserta yang siap dicetak kartu presensinya
	 * @param int $id_penerimaan
	 * @param string $waktu_awal Batas awal pengambilan nomer ujian. Format : YYYY-MM-DD hh:mm
	 * @param string $waktu_akhir Batas akhir pengambilan nomer ujian Format : YYYY-MM-DD hh:mm
	 * @return array Set CMB
	 */
	function list_cmb_cetak_presensi($id_penerimaan, $waktu_awal = '', $waktu_akhir = '')
	{		
		$this->query = $this->db->query("
			select
				p.id_penerimaan, p.id_jalur, p.id_jenjang, p.nm_penerimaan,
				cmb.id_c_mhs, cmb.kode_voucher, cmb.no_ujian, cmb.nm_c_mhs, cmb.kode_jurusan,
				jp.nm_ruang, jp.lokasi, jp.alamat, jp.tgl_test, jp.tgl_test2, jp.dua_hari_test,
				cmf.file_foto
			from aucc.penerimaan p
			join aucc.calon_mahasiswa_baru cmb on cmb.id_penerimaan = p.id_penerimaan
			join aucc.plot_jadwal_ppmb pjp on pjp.id_c_mhs = cmb.id_c_mhs
			join aucc.jadwal_ppmb jp on jp.id_jadwal_ppmb = pjp.id_jadwal_ppmb
			join aucc.calon_mahasiswa_file cmf on cmf.id_c_mhs = cmb.id_c_mhs
			where 
				cmb.no_ujian is not null and
				p.id_penerimaan = ? and
				cmb.tgl_ambil_no_ujian between to_date(?, 'YYYY-MM-DD HH24:MI') and to_date(?, 'YYYY-MM-DD HH24:MI')
			order by no_ujian", 
			array($id_penerimaan, $waktu_awal, $waktu_akhir));
		
		$result_set = array();
		
		foreach ($this->query->result() as $row)
		{
			array_push($result_set, $row);
		}
		
		return $result_set;
	}
}