<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of penerimaan_model
 *
 * @author Fathoni <fathoni@staf.unair.ac.id>
 */
class Penerimaan_Model extends MY_Model
{
	function list_penerimaan($id_pengguna)
	{
		$result_set = array();

		// id_jabatan_panitia = 1 : Admin, mendapatkan hak penuh
		// id_jabatan_panitia = 2 : Verifikator
		$sql = 
			"select p.id_penerimaan, p.nm_penerimaan, p.gelombang, p.tahun,
				(select count(id_c_mhs) from aucc.calon_mahasiswa_baru cmb where cmb.id_penerimaan = p.id_penerimaan and cmb.tgl_submit_verifikasi is not null and cmb.tgl_verifikasi_ppmb is null) as jumlah_antrian,
				(select count(id_c_mhs) from aucc.calon_mahasiswa_baru cmb where cmb.id_penerimaan = p.id_penerimaan and cmb.tgl_verifikasi_ppmb is not null) as jumlah_verifikasi,
				(select count(id_c_mhs) from aucc.calon_mahasiswa_baru cmb where cmb.id_penerimaan = p.id_penerimaan and cmb.tgl_verifikasi_ppmb is not null and cmb.id_verifikator_ppmb = {$id_pengguna}) as jumlah_memverifikasi,
				(select count(id_c_mhs) from aucc.calon_mahasiswa_baru cmb where cmb.id_penerimaan = p.id_penerimaan and cmb.tgl_submit_verifikasi is not null and cmb.is_proses_verifikasi = 1) as jumlah_proses_verifikasi,
				(select count(id_c_mhs) from aucc.calon_mahasiswa_baru cmb where cmb.id_penerimaan = p.id_penerimaan and cmb.tgl_submit_verifikasi is null and CMB.ID_VERIFIKATOR_PPMB is not null and cmb.is_proses_verifikasi is null) as jumlah_kembali,
				to_char(tgl_awal_verifikasi, 'YYYYMMDDHH24MISS') as tgl_awal_verifikasi,
				to_char(tgl_akhir_verifikasi, 'YYYYMMDDHH24MISS') as tgl_akhir_verifikasi,
				pp.id_jabatan_panitia
			from aucc.penerimaan_panitia pp
			join aucc.penerimaan p on p.id_penerimaan = pp.id_penerimaan and p.id_perguruan_tinggi = '{$this->perguruan_tinggi['ID_PERGURUAN_TINGGI']}' and sysdate between tgl_awal_verifikasi and tgl_akhir_verifikasi + 1
			where pp.id_jabatan_panitia in (1,2) and pp.id_pengguna = {$id_pengguna}";
		
		//  and sysdate between tgl_awal_verifikasi and tgl_akhir_verifikasi + 1
			
		$this->query = $this->db->query($sql);
		
		foreach ($this->query->result() as $row)
		{
			//print_r($row);
			//echo $row->NM_PENERIMAAN;
			//array_push($result_set, $row);
			
			if( $row->ID_JABATAN_PANITIA == '1' )
			{
				array_push($result_set, $row);
			}
			else if ($row->TGL_AWAL_VERIFIKASI <= date("YmdHis") and $row->TGL_AKHIR_VERIFIKASI >= date("YmdHis") )
			{
				array_push($result_set, $row);
			}
		}
		
		return $result_set;
	}
	
	/**
	 * 
	 * @param type $id_pengguna
	 * @return array
	 */
	function list_penerimaan_snmptn($id_pengguna)
	{
		$result_set = array();
		
		$this->query = $this->db
			->select('p.*')
			->from('penerimaan p')
			->join('penerimaan_panitia pp', 'pp.id_penerimaan = p.id_penerimaan')
			->where_in('pp.id_jabatan_panitia', array(1, 2))	// id_jabatan_panitia 1 & 2, admin + verifikator
			->where('p.id_jalur', 1)							// id_jalur 1 = SNMPTN
			->where('sysdate between p.tgl_awal_verifikasi and p.tgl_akhir_verifikasi', NULL, FALSE)  // masa verifikasi
			->where('pp.id_pengguna', (int)$id_pengguna)
			->get();
		
		foreach ($this->query->result() as $row)
		{
			array_push($result_set, $row);
		}
		
		// mendapatkan data tiap penerimaan / per database
		foreach ($result_set as &$row)
		{
			$this->db = $this->load->database('snmptn' . $row->TAHUN, TRUE);
			
			// Mendapatkan jumlah peserta
			$row->JUMLAH_PESERTA = $this->db->count_all_results('siswa');
			
			// Mendapatkan jumlah antrian yg harus diverifikasi
			$this->query = $this->db
				->select('count(s.nomor_pendaftaran) jumlah')->from('siswa s')
				->join('pembagian_verifikator pv', 'pv.nomor_pendaftaran = s.nomor_pendaftaran')
				->where(array(
					's.tgl_verifikasi_ppmb'		=> NULL,
					's.is_proses_verifikasi'	=> 0,
					'pv.id_pengguna'			=> (int)$id_pengguna
				))
				->get();
			$row->JUMLAH_ANTRIAN = $this->query->row()->jumlah;
			
			// Mendapatkan jumlah terverifikasi
			$this->query = $this->db
				->select('count(*) as jumlah')
				->where('tgl_verifikasi_ppmb is not null', NULL, FALSE)
				->get('siswa');
			$row->JUMLAH_VERIFIKASI = $this->query->row()->jumlah;
			
			// Mendapatkan jumlah memverifikasi
			$this->query = $this->db
				->select('count(*) as jumlah')
				->where('tgl_verifikasi_ppmb is not null', NULL, FALSE)
				->where('id_verifikator_ppmb', (int)$id_pengguna)
				->get('siswa');
			$row->JUMLAH_MEMVERIFIKASI = $this->query->row()->jumlah;
			
			// Mendapatkan jumlah sedang di proses verifikasi
			$this->query = $this->db
				->select('count(s.nomor_pendaftaran) as jumlah')->from('siswa s')
				->join('pembagian_verifikator pv', 'pv.nomor_pendaftaran = s.nomor_pendaftaran')
				->where(array(
					's.is_proses_verifikasi' => 1,
					'pv.id_pengguna'		 => (int)$id_pengguna
				))
				->get();
			
			$row->JUMLAH_PROSES_VERIFIKASI = $this->query->row()->jumlah;
		}
		
		return $result_set;
	}
	
	/**
	 * 
	 * @param int $id_pengguna
	 * @return array
	 */
	function list_penerimaan_snmptn_2($id_pengguna)
	{
		$result_set = array();
		
		$this->query = $this->db
			->select('p.*')
			->from('penerimaan p')
			->join('penerimaan_panitia pp', 'pp.id_penerimaan = p.id_penerimaan')
			->where_in('pp.id_jabatan_panitia', array(1, 4))	// id_jabatan_panitia 1, 4 = admin & verifikator tahap 2
			->where('p.id_jalur', 1)							// id_jalur 1 = SNMPTN
			->where('sysdate between p.tgl_awal_verifikasi and p.tgl_akhir_verifikasi', NULL, FALSE)  // masa verifikasi
			->where('pp.id_pengguna', (int)$id_pengguna)
			->get();
		
		foreach ($this->query->result() as $row)
		{
			array_push($result_set, $row);
		}
		
		// mendapatkan data tiap penerimaan / per database
		foreach ($result_set as &$row)
		{
			$this->db = $this->load->database('snmptn' . $row->TAHUN, TRUE);
			
			// Mendapatkan jumlah antrian yg harus diverifikasi
			$this->query = $this->db
				->select('count(s.nomor_pendaftaran) jumlah')->from('siswa s')
				->join('pembagian_verifikator_2 pv', 'pv.nomor_pendaftaran = s.nomor_pendaftaran')
				->where('s.tgl_verifikasi_ppmb IS NOT NULL', NULL, FALSE)
				->where(array(
					's.tgl_verifikasi_ppmb_2'	=> NULL,
					'pv.id_pengguna'			=> (int)$id_pengguna))
				->get();
			$row->JUMLAH_ANTRIAN = $this->query->row()->jumlah;
			
			// Mendapatkan jumlah terverifikasi
			$this->query = $this->db
				->select('count(*) as jumlah')->from('siswa')
				->where('tgl_verifikasi_ppmb_2 is not null', NULL, FALSE)
				->get();
			$row->JUMLAH_VERIFIKASI = $this->query->row()->jumlah;
			
			// Mendapatkan jumlah memverifikasi
			$this->query = $this->db
				->select('count(*) as jumlah')->from('siswa')
				->where('tgl_verifikasi_ppmb_2 is not null', NULL, FALSE)
				->where('id_verifikator_ppmb_2', (int)$id_pengguna)
				->get();
			$row->JUMLAH_MEMVERIFIKASI = $this->query->row()->jumlah;
			
			// Mendapatkan jumlah sedang di proses verifikasi
			$this->query = $this->db
				->select('count(s.nomor_pendaftaran) as jumlah')->from('siswa s')
				->join('pembagian_verifikator_2 pv', 'pv.nomor_pendaftaran = s.nomor_pendaftaran')
				->where('s.tgl_verifikasi_ppmb IS NOT NULL', NULL, FALSE)
				->where(array(
					's.is_proses_verifikasi' => 1,
					'pv.id_pengguna'		 => (int)$id_pengguna
				))
				->get();
			
			$row->JUMLAH_PROSES_VERIFIKASI = $this->query->row()->jumlah;
		}
		
		return $result_set;
	}
	
	function get_penerimaan($id_penerimaan)
	{
		$this->query = $this->db->get_where('penerimaan', array('id_penerimaan' => $id_penerimaan));
		return $this->query->row();
	}
	
	function list_penerimaan_cetak_presensi($id_pengguna)
	{
		$sql =
			"select
				/* Keterangan penerimaan */
				p.id_penerimaan, id_jalur, id_jenjang, nm_penerimaan, gelombang, semester, tahun,

				/* Sudah ada nomer ujian */
				(select count(id_c_mhs) from aucc.calon_mahasiswa_baru cmb
				where cmb.id_penerimaan = p.id_penerimaan and cmb.kode_jurusan = '01' and cmb.no_ujian is not null) as ipa,
				(select count(id_c_mhs) from aucc.calon_mahasiswa_baru cmb
				where cmb.id_penerimaan = p.id_penerimaan and cmb.kode_jurusan = '02' and cmb.no_ujian is not null) as ips,
				(select count(id_c_mhs) from aucc.calon_mahasiswa_baru cmb
				where cmb.id_penerimaan = p.id_penerimaan and cmb.kode_jurusan = '03' and cmb.no_ujian is not null) as ipc,

				/* Yang sudah bayar semua */
				(select count(cmb.id_c_mhs) from aucc.calon_mahasiswa_baru cmb
				join aucc.voucher v on v.kode_voucher = cmb.kode_voucher
				where cmb.id_penerimaan = p.id_penerimaan and cmb.kode_jurusan = '01' and v.tgl_bayar is not null) as ipa_bayar,
				(select count(cmb.id_c_mhs) from aucc.calon_mahasiswa_baru cmb
				join aucc.voucher v on v.kode_voucher = cmb.kode_voucher
				where cmb.id_penerimaan = p.id_penerimaan and cmb.kode_jurusan = '02' and v.tgl_bayar is not null) as ips_bayar,
				(select count(cmb.id_c_mhs) from aucc.calon_mahasiswa_baru cmb
				join aucc.voucher v on v.kode_voucher = cmb.kode_voucher
				where cmb.id_penerimaan = p.id_penerimaan and cmb.kode_jurusan = '03' and v.tgl_bayar is not null) as ipc_bayar
			from aucc.penerimaan p
			join aucc.penerimaan_panitia pp on pp.id_penerimaan = p.id_penerimaan
			where 
				p.id_perguruan_tinggi = '{$this->perguruan_tinggi['ID_PERGURUAN_TINGGI']}' and
				is_snmptn = 0 and
				p.tahun='".date("Y")."' and
				pp.id_jabatan_panitia in (1, 5) and
				pp.id_pengguna = {$id_pengguna}";
				
			// sysdate between tgl_awal_verifikasi and tgl_akhir_verifikasi and
		$this->query = $this->db->query($sql);
		
		$result_set = array();
		
		foreach ($this->query->result() as $row)
		{
			array_push($result_set, $row);
		}
		
		return $result_set;
	}
}
