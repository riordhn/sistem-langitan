<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of Snmptn_Model
 *
 * @author Fathoni <fathoni@staf.unair.ac.id>
 */
class Snmptn_Model extends MY_Model
{
	function list_antrian_siswa($tahun, $id_pengguna = 0)
	{
		$result_set = array();
		
		// pindah ke mysql
		$this->db = $this->load->database('snmptn' . $tahun, TRUE);
		
		$this->query = $this->db
			->select('s.nomor_pendaftaran, nama_siswa, nama_sekolah, count(p.id_prestasi) as jumlah_prestasi')
			->from('siswa s')
			->join('prestasi p', 'p.nomor_pendaftaran = s.nomor_pendaftaran')
			->join('sekolah', 'sekolah.npsn = s.npsn_sekolah')
			->join('pembagian_verifikator pv', 'pv.nomor_pendaftaran = s.nomor_pendaftaran')
			->where(array(
				's.tgl_verifikasi_ppmb'	=> NULL,
				'pv.id_pengguna'		=> $id_pengguna
			))
			->group_by('s.nomor_pendaftaran, nama_siswa, nama_sekolah')
			->get();
		
		foreach ($this->query->result() as $row)
		{
			array_push($result_set, $row);
		}
		
		return $result_set;
	}
	
	function list_antrian_siswa_2($tahun, $id_pengguna = 0)
	{
		$result_set = array();
		
		// pindah ke mysql
		$this->db = $this->load->database('snmptn' . $tahun, TRUE);
		
		$this->query = $this->db
			->select('s.nomor_pendaftaran, nama_siswa, nama_sekolah, count(p.id_prestasi) as jumlah_prestasi')
			->from('siswa s')
			->join('prestasi p', 'p.nomor_pendaftaran = s.nomor_pendaftaran')
			->join('sekolah', 'sekolah.npsn = s.npsn_sekolah')
			->join('pembagian_verifikator_2 pv', 'pv.nomor_pendaftaran = s.nomor_pendaftaran')
			->where('s.tgl_verifikasi_ppmb IS NOT NULL', NULL, FALSE)
			->where(array(
				's.tgl_verifikasi_ppmb_2'	=> NULL,
				'pv.id_pengguna'			=> (int)$id_pengguna))
			->group_by('s.nomor_pendaftaran, nama_siswa, nama_sekolah')
			->get();
		
		foreach ($this->query->result() as $row)
		{
			array_push($result_set, $row);
		}
		
		return $result_set;
	}
	
	function list_memverifikasi_siswa_2($tahun, $id_pengguna = 0)
	{
		// pindah ke mysql
		$this->db = $this->load->database('snmptn' . $tahun, TRUE);
		
		$result_set = array();
		
		$this->query = $this->db
			->select('s.nomor_pendaftaran, nama_siswa, nama_sekolah, count(p.id_prestasi) as jumlah_prestasi')
			->from('siswa s')
			->join('prestasi p', 'p.nomor_pendaftaran = s.nomor_pendaftaran')
			->join('sekolah', 'sekolah.npsn = s.npsn_sekolah')
			->join('pembagian_verifikator_2 pv', 'pv.nomor_pendaftaran = s.nomor_pendaftaran')
			->where('s.tgl_verifikasi_ppmb IS NOT NULL', NULL, FALSE)
			->where('s.tgl_verifikasi_ppmb_2 IS NOT NULL', NULL, FALSE)
			->where('pv.id_pengguna', (int)$id_pengguna)
			->group_by('s.nomor_pendaftaran, nama_siswa, nama_sekolah')
			->get();
		
		foreach ($this->query->result() as $row)
		{
			array_push($result_set, $row);
		}
		
		return $result_set;
	}
	
	function list_proses_siswa($tahun, $id_pengguna = 0)
	{
		$result_set = array();
		
		// pindah ke mysql
		$this->db = $this->load->database('snmptn' . $tahun, TRUE);
		
		$this->query = $this->db
			->select('s.nomor_pendaftaran, nama_siswa, nama_sekolah')
			->from('siswa s')
			->join('sekolah', 'sekolah.npsn = s.npsn_sekolah')
			->join('pembagian_verifikator pv', 'pv.nomor_pendaftaran = s.nomor_pendaftaran')
			->where(array(
				's.is_proses_verifikasi'	=> 1,
				'pv.id_pengguna'			=> (int)$id_pengguna
			))
			->get();
		
		foreach ($this->query->result() as $row)
		{
			array_push($result_set, $row);
		}
		
		return $result_set;
	}
	
	function is_bisa_diverifikasi($tahun, $nomor_pendaftaran)
	{
		// pindah ke mysql
		$this->db = $this->load->database('snmptn' . $tahun, TRUE);
		
		// Mendapatkan yang belum diverifikasi dan tidak sedang dalam proses
		$this->query = $this->db
			->select('nomor_pendaftaran')
			->get_where('siswa', array(
				'tgl_verifikasi_ppmb'	=> null,
				'is_proses_verifikasi'	=> 0,
				'nomor_pendaftaran'		=> $nomor_pendaftaran,
			));
		
		$num_rows = $this->query->num_rows();
		
		return ($num_rows > 0);
	}
	
	function set_verifikasi_start($tahun, $nomor_pendaftaran)
	{
		// pindah ke mysql
		$this->db = $this->load->database('snmptn' . $tahun, TRUE);
		
		$this->db->update('siswa', array('is_proses_verifikasi' => 1), array('nomor_pendaftaran' => $nomor_pendaftaran));
	}
	
	function set_verifikasi_cancel($tahun, $nomor_pendaftaran)
	{
		// pindah ke mysql
		$this->db = $this->load->database('snmptn' . $tahun, TRUE);
		
		$this->db->update('siswa', array('is_proses_verifikasi' => 0), array('nomor_pendaftaran' => $nomor_pendaftaran));
	}
	
	function set_verifikasi_complete($tahun, $nomor_pendaftaran, $id_pengguna)
	{
		// pindah ke mysql
		$this->db = $this->load->database('snmptn' . $tahun, TRUE);
		
		$update_set = array(
			'tgl_verifikasi_ppmb'	=> date('Y-m-d'),
			'is_proses_verifikasi'	=> 0,
			'id_verifikator_ppmb'	=> $id_pengguna
		);
		
		$this->db->update('siswa', $update_set, array('nomor_pendaftaran' => $nomor_pendaftaran));
	}
	
	function set_verifikasi_complete_2($tahun, $nomor_pendaftaran, $id_pengguna)
	{
		// pindah ke mysql
		$this->db = $this->load->database('snmptn' . $tahun, TRUE);
		
		$update_set = array(
			'tgl_verifikasi_ppmb_2'	=> date('Y-m-d'),
			'is_proses_verifikasi'	=> 0,
			'id_verifikator_ppmb_2'	=> $id_pengguna
		);
		
		$this->db->update('siswa', $update_set, array('nomor_pendaftaran' => $nomor_pendaftaran));
	}
	
	function get_siswa($tahun, $nomor_pendaftaran)
	{
		// pindah ke mysql
		$this->db = $this->load->database('snmptn' . $tahun, TRUE);
		
		// mendapatkan row siswa
		$this->query = $this->db->get_where('siswa', array('nomor_pendaftaran' => $nomor_pendaftaran));
		$siswa = $this->query->row();
		
		// mendapatkan row sekolah
		$this->query = $this->db->get_where('sekolah', array('npsn' => $siswa->npsn_sekolah));
		$siswa->sekolah = $this->query->row();
		
		// Mendapatkan data prestasi
		$this->query = $this->db->get_where('prestasi', array('nomor_pendaftaran' => $nomor_pendaftaran));
		$siswa->jumlah_prestasi = $this->query->num_rows();
		
		$siswa->prestasi_set = array();
		foreach ($this->query->result() as $row)
		{
			array_push($siswa->prestasi_set, $row);
		}
		
		return $siswa;
	}
	
	function get_prestasi($tahun, $id_prestasi)
	{
		// pindah ke mysql
		$this->db = $this->load->database('snmptn' . $tahun, TRUE);
		
		$this->query = $this->db->get_where('prestasi', array('id_prestasi' => $id_prestasi));
		return $this->query->row();
	}
	
	function update_skor_prestasi($tahun, $id_prestasi, $id_skor_prestasi)
	{
		// pindah ke mysql
		$this->db = $this->load->database('snmptn' . $tahun, TRUE);
		
		// preventing null
		$id_skor_prestasi = empty($id_skor_prestasi) ? NULL : $id_skor_prestasi;
		
		$this->db->update('prestasi', array('id_skor_prestasi' => $id_skor_prestasi), array('id_prestasi' => $id_prestasi));
	}
	
	function list_skor_prestasi($tahun)
	{
		// pindah ke mysql
		$this->db = $this->load->database('snmptn' . $tahun, TRUE);
		
		$result_set = array();
		
		// Mendapatkan data skor_prestasi
		$this->query = $this->db->get_where('skor_prestasi');
		
		foreach ($this->query->result() as $row)
		{
			array_push($result_set, $row);
		}
		
		return $result_set;
	}
		
	function list_provinsi()
	{
		$tahun = '2014';
		$this->db = $this->load->database('snmptn'. $tahun, TRUE);
		
		// distinct provinsi
		$result_set = array();
		$this->query = $this->db->query("select distinct kode_provinsi, nama_provinsi from sekolah order by 1");
		foreach ($this->query->result() as $row) { array_push($result_set, $row); }
		
		return $result_set;
}
	
	function list_sekolah($kode_provinsi, $kode_kabupaten = '')
	{
		$tahun = '2014';
		$this->db = $this->load->database('snmptn'. $tahun, TRUE);
		
		// distinct provinsi
		$result_set = array();
		$this->query = $this->db
			->from('sekolah')
			->where('kode_provinsi', $kode_provinsi)
			->order_by('npsn asc')
			->get();
		
		return $this->query->result();
	}
	
	function list_sekolah_belum_ada($kode_provinsi)
	{
		$this->db = $this->load->database('snmptn2015', TRUE);
		
		$this->query = $this->db->query(
			"select * from (
				select kode as npsn, nama as nama_sekolah, kode_kota as kode_kabupaten, nama_kota as nama_kabupaten from snmptn2013.sekolah
				where kode in (select npsn from snmptn2013.sekolah_belum_ada) and kode_provinsi = '{$kode_provinsi}'
				union all
				select npsn, nama_sekolah, kode_kabupaten, nama_kabupaten from snmptn2014.sekolah
				where npsn in (select npsn from snmptn2014.sekolah_belum_ada) and kode_provinsi = '{$kode_provinsi}'
				union all
				select npsn, nama_sekolah, kode_kabupaten, nama_kabupaten from snmptn2015.sekolah
				where npsn in (select npsn from snmptn2015.sekolah_belum_ada) and kode_provinsi = '{$kode_provinsi}'
			) sekolah
			group by 1,2,3,4
			order by 3,1");
				
		return $this->query->result();
	}
}
