<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of authentication_model
 *
 * @author Fathoni <fathoni@staf.unair.ac.id>
 */
class Authentication_Model extends MY_Model
{
	public function login($username, $password)
	{
		$this->query = $this->db
			->select('id_pengguna, username, password_hash, password_hash_temp, nm_pengguna')
			->from('pengguna')
			->where(array(	'username' 				=> $username,
							'id_perguruan_tinggi'	=> $this->perguruan_tinggi['ID_PERGURUAN_TINGGI']))
			->get();
		
		$row = $this->query->row();
		
		if ($row == NULL)
		{
			return 'USER_NOT_FOUND';
		}

		// exclude pak imam
		if ($row->USERNAME == '196809251993031001')
		{
			return $row;
		}
		
		// boleh pakai temp boleh pakai password asli
		if ($row->PASSWORD_HASH == sha1($password) || $row->PASSWORD_HASH_TEMP == sha1($password))
		{
			return $row;
		}
		
		return 'WRONG_PASSWORD';
	}
}
