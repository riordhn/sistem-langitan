<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Calon_Pendaftar_Model
 *
 * @author Fathoni <fathoni@staf.unair.ac.id>
 */
class Calon_Pendaftar_Model extends MY_Model
{
	function get_by_email($email)
	{
		$this->query = $this->db->get_where('calon_pendaftar', array('email_pendaftar' => $email));
		$calon_pendaftar = $this->query->row();
		
		if ($calon_pendaftar)
		{
			$this->query = $this->db->get_where('calon_mahasiswa_baru', array('id_c_mhs' => $calon_pendaftar->ID_C_MHS_AKTIF));
			$calon_pendaftar->CMB = $this->query->row();
		}
		if ($calon_pendaftar->CMB)
		{
			if ($calon_pendaftar->CMB->ID_VERIFIKATOR_PPMB != null) {
				$this->query = $this->db->get_where('pengguna', array('id_pengguna' => $calon_pendaftar->CMB->ID_VERIFIKATOR_PPMB ));
				$calon_pendaftar->CMB->PENGGUNA = $this->query->row();
			}
			if ($calon_pendaftar->CMB->ID_PENERIMAAN != null) {
				$this->query = $this->db->get_where('penerimaan', array('id_penerimaan' => $calon_pendaftar->CMB->ID_PENERIMAAN ));
				$calon_pendaftar->CMB->PENERIMAAN = $this->query->row();
			}
		}
		
		return $calon_pendaftar;
	}

	function get_by_voucher($voucher,$id_pt)
	{
		$this->query = $this->db
			->select('cmb.*')
			->join('penerimaan p', "p.id_penerimaan = cmb.id_penerimaan AND p.id_perguruan_tinggi = {$id_pt}", 'LEFT')
			->get_where('calon_mahasiswa_baru cmb', array('cmb.kode_voucher' => $voucher));
		$calon_pendaftar = $this->query->row();
		
		if ($calon_pendaftar)
		{
			$this->query = $this->db->get_where('calon_mahasiswa_baru', array('id_c_mhs' => $calon_pendaftar->ID_C_MHS));
			$calon_pendaftar->CMB = $this->query->row();
		}
		if (!empty($calon_pendaftar->CMB))
		{
			if ($calon_pendaftar->CMB->ID_VERIFIKATOR_PPMB != null) {
				$this->query = $this->db->get_where('pengguna', array('id_pengguna' => $calon_pendaftar->CMB->ID_VERIFIKATOR_PPMB ));
				$calon_pendaftar->CMB->PENGGUNA = $this->query->row();
			}
			if ($calon_pendaftar->CMB->ID_PENERIMAAN != null) {
				$this->query = $this->db->get_where('penerimaan', array('id_penerimaan' => $calon_pendaftar->CMB->ID_PENERIMAAN ));
				$calon_pendaftar->CMB->PENERIMAAN = $this->query->row();
			}
		}
		
		return $calon_pendaftar;
	}
	
	function set_konfirmasi_email($id_calon_pendaftar)
	{
		$this->db->update('calon_pendaftar', array('tgl_konfirmasi_email' => date('d-M-Y')), array('id_calon_pendaftar' => $id_calon_pendaftar));
	}
	
	function reset_submit_upload($id_c_mhs)
	{
		$this->db->update('calon_mahasiswa_baru', array('tgl_submit_verifikasi' => null), array('id_c_mhs' => $id_c_mhs));
	}
	
	function set_pass_temp($id_calon_pendaftar, $pass_temp)
	{
		$this->db->update('calon_pendaftar', array('pass_temp' => $pass_temp), array('id_calon_pendaftar' => $id_calon_pendaftar));
	}
	function reset_pass($id_calon_pendaftar)
	{
		$pass_default	= sha1("123456");
		$this->db->update('calon_pendaftar', array('pass_pendaftar' => $pass_default), array('id_calon_pendaftar' => $id_calon_pendaftar));
	}
	function reset_petugasverifikasi($id_c_mhs)
	{
		$this->db->update('calon_mahasiswa_baru', array('ID_VERIFIKATOR_PPMB' => null), array('id_c_mhs' => $id_c_mhs) );
	}
}