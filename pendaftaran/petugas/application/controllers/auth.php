<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property Authentication_Model $auth_model
 */
class Auth extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
	}
	
	function login()
	{		
		if ($this->input->server('REQUEST_METHOD') != 'POST')
		{
			redirect(site_url());
			exit();
		}
		
		$this->load->model('Authentication_Model', 'auth_model');
		
		$this->form_validation->set_rules('username');
		$this->form_validation->run();
		
		$username		= $this->input->post('username');
		$password		= $this->input->post('password');
		
		$login_result = $this->auth_model->login($username, $password);
		
		if ($login_result == 'USER_NOT_FOUND')
		{
			$this->smarty->assign('error_message', 'Username tidak ditemukan.');
		}
		else if ($login_result == 'WRONG_PASSWORD')
		{
			$this->smarty->assign('error_message', 'Password salah.');
		}
		else
		{
			// status login
			$this->session->set_userdata('is_loggedin', TRUE);
			
			// id_pengguna
			$this->session->set_userdata('id_pengguna', $login_result->ID_PENGGUNA);
			
			// row calon_pendaftar
			$this->session->set_userdata('pengguna', $login_result);
			
			redirect('home');
		}
		
		$this->smarty->display('front/index.tpl');
	}
	
	function logout()
	{
		// Logout
		$this->session->set_userdata('is_loggedin', FALSE);
		$this->session->unset_userdata('id_pengguna');
		$this->session->unset_userdata('pengguna');
		
		redirect(site_url()); exit();
	}
}