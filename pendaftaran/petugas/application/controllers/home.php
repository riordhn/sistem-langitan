<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property Penerimaan_Model $penerimaan_model Description
 */
class Home extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
		
		$this->check_credentials();
		
		$this->load->model('Penerimaan_Model', 'penerimaan_model');
	}
	
	function index()
	{		
		$pengguna = $this->session->userdata('pengguna');
		
		$this->smarty->assignByRef('pengguna', $pengguna);
		$this->smarty->display('home/index.tpl');
	}
}
