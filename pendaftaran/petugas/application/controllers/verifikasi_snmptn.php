<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Fathoni <fathoni@staf.unair.ac.id>
 * @property Penerimaan_Model $penerimaan_model Description
 * @property Snmptn_Model $snmptn_model Description
 * @property Sekolah_Model $sekolah_model Description
 */

class Verifikasi_SNMPTN extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->check_credentials();
		
		$this->load->model('Penerimaan_Model', 'penerimaan_model');
		$this->load->model('Snmptn_Model', 'snmptn_model');
	}
	
	function list_penerimaan()
	{
		$pengguna = $this->session->userdata('pengguna');
		
		$penerimaan_set = $this->penerimaan_model->list_penerimaan_snmptn($pengguna->ID_PENGGUNA);
		$this->smarty->assignByRef('penerimaan_set', $penerimaan_set);
		
		$this->smarty->display('verifikasi_snmptn/list_penerimaan.tpl');
	}
	
	function list_penerimaan_2()
	{
		$pengguna = $this->session->userdata('pengguna');
		
		$penerimaan_set = $this->penerimaan_model->list_penerimaan_snmptn_2($pengguna->ID_PENGGUNA);
		$this->smarty->assignByRef('penerimaan_set', $penerimaan_set);
		
		$this->smarty->display('verifikasi_snmptn/list_penerimaan_2.tpl');
	}
	
	function list_antrian($id_penerimaan)
	{	
		$pengguna = $this->session->userdata('pengguna');
		
		$penerimaan = $this->penerimaan_model->get_penerimaan($id_penerimaan);
		$this->smarty->assignByRef('penerimaan', $penerimaan);
		
		$siswa_set = $this->snmptn_model->list_antrian_siswa($penerimaan->TAHUN, $pengguna->ID_PENGGUNA);
		$this->smarty->assignByRef('siswa_set', $siswa_set);
		
		$this->smarty->display('verifikasi_snmptn/list_antrian.tpl');
	}
	
	function list_antrian_2($id_penerimaan)
	{	
		$pengguna = $this->session->userdata('pengguna');
		
		$penerimaan = $this->penerimaan_model->get_penerimaan($id_penerimaan);
		$this->smarty->assignByRef('penerimaan', $penerimaan);
		
		$siswa_set = $this->snmptn_model->list_antrian_siswa_2($penerimaan->TAHUN, $pengguna->ID_PENGGUNA);
		$this->smarty->assignByRef('siswa_set', $siswa_set);
		
		$this->smarty->display('verifikasi_snmptn/list_antrian_2.tpl');
	}
	
	function list_memverifikasi_2($id_penerimaan)
	{
		$pengguna = $this->session->userdata('pengguna');
		
		$penerimaan = $this->penerimaan_model->get_penerimaan($id_penerimaan);
		$this->smarty->assignByRef('penerimaan', $penerimaan);
		
		$siswa_set = $this->snmptn_model->list_memverifikasi_siswa_2($penerimaan->TAHUN, $pengguna->ID_PENGGUNA);
		$this->smarty->assignByRef('siswa_set', $siswa_set);
		
		$this->smarty->display('verifikasi_snmptn/list_memverifikasi_2.tpl');
	}
	
	function list_proses($id_penerimaan)
	{
		$pengguna = $this->session->userdata('pengguna');
		
		$penerimaan = $this->penerimaan_model->get_penerimaan($id_penerimaan);
		$this->smarty->assignByRef('penerimaan', $penerimaan);
		
		$siswa_set = $this->snmptn_model->list_proses_siswa($penerimaan->TAHUN, $pengguna->ID_PENGGUNA);
		$this->smarty->assignByRef('siswa_set', $siswa_set);
		
		$this->smarty->display('verifikasi_snmptn/list_proses.tpl');
	}
	
	function start($id_penerimaan, $nomor_pendaftaran)
	{
		$penerimaan = $this->penerimaan_model->get_penerimaan($id_penerimaan);
		
		$this->snmptn_model->set_verifikasi_start($penerimaan->TAHUN, $nomor_pendaftaran);

		// diteruskan
		redirect("verifikasi_snmptn/proses/{$id_penerimaan}/{$nomor_pendaftaran}");
	}
	
	function cancel($id_penerimaan, $nomor_pendaftaran)
	{
		$penerimaan = $this->penerimaan_model->get_penerimaan($id_penerimaan);
		
		$this->snmptn_model->set_verifikasi_cancel($penerimaan->TAHUN, $nomor_pendaftaran);
		
		// dikembalikan
		redirect("verifikasi_snmptn/list_antrian/{$id_penerimaan}");
	}
	
	function proses($id_penerimaan, $nomor_pendaftaran)
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST')
		{
			$tahun				= $this->input->post('tahun');
			$id_prestasi		= $this->input->post('id_prestasi');
			$id_skor_prestasi	= $this->input->post('id_skor_prestasi');
			
			$this->snmptn_model->update_skor_prestasi($tahun, $id_prestasi, $id_skor_prestasi);
		}
		
		$penerimaan = $this->penerimaan_model->get_penerimaan($id_penerimaan);
		$this->smarty->assignByRef('penerimaan', $penerimaan);
		
		$siswa = $this->snmptn_model->get_siswa($penerimaan->TAHUN, $nomor_pendaftaran);
		$this->smarty->assignByRef('siswa', $siswa);
		
		$skor_prestasi_set = $this->snmptn_model->list_skor_prestasi($penerimaan->TAHUN);
		$this->smarty->assignByRef('skor_prestasi_set', $skor_prestasi_set);
		
		$this->smarty->display('verifikasi_snmptn/proses.tpl');
	}
	
	function proses_2($id_penerimaan, $nomor_pendaftaran)
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST')
		{
			$tahun				= $this->input->post('tahun');
			$id_prestasi		= $this->input->post('id_prestasi');
			$id_skor_prestasi	= $this->input->post('id_skor_prestasi');
			
			$this->snmptn_model->update_skor_prestasi($tahun, $id_prestasi, $id_skor_prestasi);
		}
		
		$penerimaan = $this->penerimaan_model->get_penerimaan($id_penerimaan);
		$this->smarty->assignByRef('penerimaan', $penerimaan);
		
		$siswa = $this->snmptn_model->get_siswa($penerimaan->TAHUN, $nomor_pendaftaran);
		$this->smarty->assignByRef('siswa', $siswa);
		
		$skor_prestasi_set = $this->snmptn_model->list_skor_prestasi($penerimaan->TAHUN);
		$this->smarty->assignByRef('skor_prestasi_set', $skor_prestasi_set);
		
		$this->smarty->display('verifikasi_snmptn/proses_2.tpl');
	}
	
	function complete($id_penerimaan, $nomor_pendaftaran)
	{
		$pengguna = $this->session->userdata('pengguna');
		$penerimaan = $this->penerimaan_model->get_penerimaan($id_penerimaan);
		
		$this->snmptn_model->set_verifikasi_complete($penerimaan->TAHUN, $nomor_pendaftaran, $pengguna->ID_PENGGUNA);
		
		redirect('verifikasi_snmptn/list_antrian/' . $id_penerimaan);
	}
	
	function complete_2($id_penerimaan, $nomor_pendaftaran)
	{
		$pengguna = $this->session->userdata('pengguna');
		$penerimaan = $this->penerimaan_model->get_penerimaan($id_penerimaan);
		
		$this->snmptn_model->set_verifikasi_complete_2($penerimaan->TAHUN, $nomor_pendaftaran, $pengguna->ID_PENGGUNA);
		
		redirect('verifikasi_snmptn/list_antrian_2/' . $id_penerimaan);
	}
	
	function rotate_image($tahun, $id_prestasi, $degree)
	{
		$this->load->helper('file');
		
		$prestasi = $this->snmptn_model->get_prestasi($tahun, $id_prestasi);
		
		$filename = "/var/www/html/snmptn{$tahun}/dokumen/{$prestasi->nomor_pendaftaran}/{$prestasi->nama_file}";
		
		if (file_exists($filename))
		{
			$img = new Imagick();
			$img->readimage($filename);
			
			if ($img->valid())
			{
				$img->rotateimage(new ImagickPixel('none'), $degree);
				$img->writeimage($filename); // overwrite
				
				echo "http://210.57.208.55/snmptn{$tahun}/dokumen/{$prestasi->nomor_pendaftaran}/{$prestasi->nama_file}?" . time();
			}
			else
			{
				echo 0;
			}
			
			$img->clear();
		}
		else
		{
			echo 0;
		}
	}
	
	function sekolah()
	{
		$this->load->model('Sekolah_Model', 'sekolah_model');
		
		$provinsi_snmptn_set = $this->snmptn_model->list_provinsi();
		$this->smarty->assignByRef('provinsi_snmptn_set', $provinsi_snmptn_set);
		
		if ($this->input->get('kode_provinsi'))
		{
			$kode_provinsi = $this->input->get('kode_provinsi');
			
			$sekolah_snmptn_set	= $this->snmptn_model->list_sekolah_belum_ada($kode_provinsi);
			$sekolah_cyber_set	= $this->sekolah_model->list_sekolah($kode_provinsi);
			$kota_cyber_set		= $this->sekolah_model->list_kota($kode_provinsi);
			
			// print_r($sekolah_snmptn_set); exit();
			
			foreach ($sekolah_snmptn_set as &$ss)
			{
				// Marking sekolah snmptn yg sudah ada
				foreach ($sekolah_cyber_set as $sc)
				{
					if ($ss->npsn == $sc->KODE_SEKOLAH)
					{
						$ss->exist = true;
						break; // exit loop
					}
					
					$ss->exist = false;
				}
				
				// Marking id_kota dari sekolah snmptn
				foreach ($kota_cyber_set as $ks)
				{
					if ($ks->KODE_SNMPTN == $ss->kode_kabupaten)
					{
						$ss->id_kota = $ks->ID_KOTA;
						break;
					}
				}
			}
			
			// marking sekolah cyber
			foreach ($sekolah_cyber_set as &$sc)
			{
				// Format Kota
				if ($sc->TIPE_DATI2 == 'KABUPATEN')
					$sc->NM_KOTA = 'KAB. ' . ucwords(strtolower($sc->NM_KOTA));
				else
					$sc->NM_KOTA = 'KOTA ' . ucwords(strtolower($sc->NM_KOTA));
				
				foreach ($sekolah_snmptn_set as $ss)
				{
					if ($sc->KODE_SEKOLAH != $ss->npsn && strtolower($sc->NM_SEKOLAH) == strtolower($ss->nama_sekolah))
					{
						$sc->SALAH_KODE = true;
						break;
					}
					
					$sc->SALAH_KODE = false;
				}
			}
			
			
			$this->smarty->assignByRef('sekolah_snmptn_set', $sekolah_snmptn_set);
			$this->smarty->assignByRef('sekolah_cyber_set', $sekolah_cyber_set);
		}
		
		$this->smarty->display('verifikasi_snmptn/sekolah.tpl');
	}
	
	function update_sekolah()
	{
		$this->load->model('Sekolah_Model', 'sekolah_model');
		
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$id_sekolah		= $this->input->post('id_sekolah');
			$kode_snmptn	= $this->input->post('kode_sekolah');
			
			$result = $this->sekolah_model->update_sekolah($id_sekolah, $kode_snmptn);
			
			if ($result)
				echo "1";
			else
				echo "0";
		}
		else
		{
			echo "0";
		}
	}
	
	function insert_sekolah()
	{
		$this->load->model('Sekolah_Model', 'sekolah_model');
		
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$id_kota		= $this->input->post('id_kota');
			$nm_sekolah		= $this->input->post('nm_sekolah');
			$kode_snmptn	= $this->input->post('kode_snmptn');
			
			$result = $this->sekolah_model->insert_sekolah($id_kota, $nm_sekolah, $kode_snmptn);
			
			if ($result)
				echo "1";
			else
				echo "0";
		}
		else
		{
			echo "0";
		}
	}
}