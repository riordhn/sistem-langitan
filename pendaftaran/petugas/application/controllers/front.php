<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front extends MY_Controller
{
	public function index()
	{	
		$this->smarty->display('front/index.tpl');
	}
}

/* End of file front.php */
/* Location: ./application/controllers/welcome.php */