<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of verifikasi
 *
 * @author Fathoni <fathoni@staf.unair.ac.id>
 * @property Penerimaan_Model $penerimaan_model 
 * @property Cmb_Model $cmb_model Description
 */
class Verifikasi extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->check_credentials();
		
		$this->load->model('Penerimaan_Model', 'penerimaan_model');
		$this->load->model('Cmb_Model', 'cmb_model');
		$this->load->helper('waktu');
	}
	
	function list_penerimaan()
	{
		$pengguna = $this->session->userdata('pengguna');
		
		// Mendapatkan penerimaan aktif
		$penerimaan_set = $this->penerimaan_model->list_penerimaan($pengguna->ID_PENGGUNA);
		$this->smarty->assignByRef('penerimaan_set', $penerimaan_set);
		
		$this->smarty->display('verifikasi/list_penerimaan.tpl');
	}
	
	function list_antrian($id_penerimaan)
	{
		$penerimaan = $this->penerimaan_model->get_penerimaan($id_penerimaan);
		$cmb_set	= $this->cmb_model->list_antrian_peserta($id_penerimaan);
		
		$this->smarty->assignByRef('penerimaan', $penerimaan);
		$this->smarty->assignByRef('cmb_set', $cmb_set);
		$this->smarty->display($this->uri->segment(1) . '/' . $this->uri->segment(2) . '.tpl');
	}
	
	function list_proses($id_penerimaan)
	{
		$penerimaan = $this->penerimaan_model->get_penerimaan($id_penerimaan);
		$cmb_set	= $this->cmb_model->list_proses_peserta($id_penerimaan);
		
		$this->smarty->assignByRef('penerimaan', $penerimaan);
		$this->smarty->assignByRef('cmb_set', $cmb_set);
		$this->smarty->display($this->uri->segment(1) . '/' . $this->uri->segment(2) . '.tpl');
	}
	
	function list_verifikasi($id_penerimaan)
	{
		$this->load->helper('waktu');
		$penerimaan = $this->penerimaan_model->get_penerimaan($id_penerimaan);
		$cmb_set	= $this->cmb_model->list_verifikasi_peserta($id_penerimaan);
		
		$this->smarty->assignByRef('penerimaan', $penerimaan);
		$this->smarty->assignByRef('cmb_set', $cmb_set);
		$this->smarty->display($this->uri->segment(1) . '/' . $this->uri->segment(2) . '.tpl');
	}
	
	function list_memverifikasi($id_penerimaan)
	{
		$id_verifikator	= $this->session->userdata('id_pengguna');
		$penerimaan 	= $this->penerimaan_model->get_penerimaan($id_penerimaan);
		$cmb_set		= $this->cmb_model->list_memverifikasi_peserta($id_penerimaan, $id_verifikator);
		
		$this->smarty->assignByRef('penerimaan', $penerimaan);
		$this->smarty->assignByRef('cmb_set', $cmb_set);
		$this->smarty->display($this->uri->segment(1) . '/' . $this->uri->segment(2) . '.tpl');
	}
	
	function list_kembali($id_penerimaan)
	{
		$penerimaan = $this->penerimaan_model->get_penerimaan($id_penerimaan);
		$cmb_set	= $this->cmb_model->list_peserta_kembali($id_penerimaan);
		
		$this->smarty->assignByRef('penerimaan', $penerimaan);
		$this->smarty->assignByRef('cmb_set', $cmb_set);
		$this->smarty->display($this->uri->segment(1) . '/' . $this->uri->segment(2) . '.tpl');
	}
	
	/**
	 * Mengeset status menjadi sedang proses verifikasi
	 * @param int $id_c_mhs
	 */
	function start($id_c_mhs, $id_penerimaan)
	{
		$id_verifikator		= $this->session->userdata('id_pengguna');
		//if ($this->cmb_model->is_bisa_diverifikasi($id_c_mhs))
		if ($this->cmb_model->is_bisa_diverifikasi_2($id_c_mhs, $id_verifikator))
		{
			$this->cmb_model->set_verifikasi_start($id_c_mhs, $id_verifikator);
			redirect('verifikasi/proses/' . $id_c_mhs);
		}
		else
		{
			redirect('verifikasi/list_antrian/' . $id_penerimaan);
		}
	}
	
	/**
	 * Membatalkan status proses verifikasi
	 * @param int $id_c_mhs
	 */
	function cancel($id_c_mhs)
	{
		$this->load->helper('widget');
		
		$id_penerimaan = $this->input->get('id_penerimaan');
		
		$this->cmb_model->set_verifikasi_cancel($id_c_mhs);
		
		$this->smarty->assign('id_penerimaan', $id_penerimaan);
		$this->smarty->assign('title', 'Pembatalan berhasil !');
		$this->smarty->assign('message', 'Proses pembatalan proses verifikasi peserta berhasil');
		
		$this->smarty->display('verifikasi/complete.tpl');
	}
	
	/**
	 * Kembalikan data ke peserta. Verifikasi berhasil, tetapi hasil verifikasi tidak tepat
	 * @param type $id_c_mhs
	 */
	function kembalikan($id_c_mhs)
	{
		$this->load->helper('widget');
		
		$cmb = $this->cmb_model->get_cmb($id_c_mhs);
		$id_penerimaan = $this->input->get('id_penerimaan');

		$this->cmb_model->set_verifikasi_kembalikan($id_c_mhs);
		
		$this->smarty->assign('id_penerimaan', $id_penerimaan);
		$this->smarty->assign('title', 'Pengembalian berhasil !');
		$this->smarty->assign('message', 'Proses pengembalian berkas upload ke peserta berhasil');
		
		// untuk sms gateway
		$this->smarty->assign('telp', $cmb->TELP);
		$this->smarty->assign('pesan_sms', 'Berkas anda BELUM valid. Silahkan login untuk memperbaiki berkas. - PPMB Universitas Airlangga');
		
		$this->smarty->display('verifikasi/complete.tpl');
	}
	
	/**
	 * Proses menyelesaikan verifikasi dan proses update voucher dan nomer ujian
	 * @param int $id_c_mhs
	 */
	function complete($id_c_mhs)
	{
		// mendapatkan id_verifikator
		$id_penerimaan		= $this->input->get('id_penerimaan', TRUE);
		$id_verifikator		= $this->session->userdata('id_pengguna');
		
		// mendapatkan hasil verifikasi
		$hasil_verifikasi	= $this->cmb_model->set_verifikasi_complete($id_c_mhs, $id_verifikator);
		
		if ($hasil_verifikasi == 'KODE_VOUCHER')
		{
			$this->smarty->assign('id_c_mhs', $id_c_mhs);
			$this->smarty->assign('title', 'Kode Voucher tidak ada !');
			$this->smarty->assign('message', 'Harap menghubungi admin PPMB untuk melakukan setting voucher');
			$this->smarty->display('verifikasi/complete_error.tpl');
			exit();
		}
		
		/*if ($hasil_verifikasi == 'NO_UJIAN')
		{
			$this->smarty->assign('id_c_mhs', $id_c_mhs);
			$this->smarty->assign('title', 'Nomer Ujian tidak ada !');
			$this->smarty->assign('message', 'Harap menghubungi admin PPMB untuk melakukan setting ruangan');
			$this->smarty->display('verifikasi/complete_error.tpl');
			exit();
		}*/
		
		/*if ($hasil_verifikasi == 'ISIAN_UKT')
		{
			$this->smarty->assign('id_c_mhs', $id_c_mhs);
			$this->smarty->assign('title', 'Pilihan UKT belum di isi !');
			$this->smarty->assign('message', 'Silahkan mengisi isian UKT peserta terlebih dahulu');
			$this->smarty->display('verifikasi/complete_error.tpl');
			exit();
		}*/
		
		/*if ($hasil_verifikasi == 'ISIAN_SP3')
		{
			$this->smarty->assign('id_c_mhs', $id_c_mhs);
			$this->smarty->assign('title', 'Pilihan SP3 belum di isi !');
			$this->smarty->assign('message', 'Silahkan mengisi isian SP3 peserta terlebih dahulu');
			$this->smarty->display('verifikasi/complete_error.tpl');
			exit();
		}*/
		
		if ($hasil_verifikasi == 'COMPLETE')
		{
			$this->load->helper('widget');
			$cmb = $this->cmb_model->get_cmb($id_c_mhs);
			// untuk sms gateway
			$this->smarty->assign('telp', $cmb->TELP);
			$this->smarty->assign('pesan_sms', 'Berkas anda sudah VALID. Silahkan login untuk melihat kode voucher. - PPMB Universitas Airlangga');
					
			$this->smarty->assign('id_penerimaan', $id_penerimaan);
			$this->smarty->assign('title', 'Verifikasi berhasil !');
			$this->smarty->assign('message', 'Silahkan melanjutkan verifikasi pada peserta selanjutnya');
			$this->smarty->display('verifikasi/complete.tpl');
			exit();
		}
	}
	
	/**
	 * Halaman verifikasi
	 * @param int $id_c_mhs
	 */
	function proses($id_c_mhs)
	{
		// Mendapatkan CMB di awal untuk proses validasi SP3
		$cmb = $this->cmb_model->get_cmb($id_c_mhs);
		
		if ($this->input->server('REQUEST_METHOD') == 'POST')
		{
			$mode		= $this->input->post('mode');
			$id_c_mhs	= $this->input->post('id_c_mhs');
			$valid		= $this->input->post('valid');
			
			if ($mode == 'foto')
			{
				if ($valid == '1')			/* Syarat Valid */
				{
					$this->cmb_model->set_syarat_valid_2($id_c_mhs, 'foto');
					$this->cmb_model->set_syarat_valid($id_c_mhs, 'foto');
				}
				else if ($valid == '2')		/* Syarat dibatalkan */
				{
					$this->cmb_model->set_syarat_batalvalid($id_c_mhs, 'foto');
				}
				else if ($valid == '0')		/* Syarat tidak valid */
				{
					$this->cmb_model->set_syarat_invalid_2($id_c_mhs, 'foto', $this->input->post('pesan_v_foto'));
					$this->cmb_model->set_syarat_invalid($id_c_mhs, 'foto', $this->input->post('pesan_v_foto'));
				}
			}
			else if ($mode == 'pesan_optional')
			{
				$this->cmb_model->set_syarat_invalid_2($id_c_mhs, 'pesan_optional', $this->input->post('pesan_v_optional'));
				$this->cmb_model->set_syarat_invalid($id_c_mhs, 'pesan_optional', $this->input->post('pesan_v_optional'));

				// Assign ke cmb yg telah di load
				$cmb->PESAN_VERIFIKATOR = $this->input->post('pesan_v_optional');
				$cmb->LOG->PESAN_VERIFIKATOR = $this->input->post('pesan_v_optional');
			}
			else if ($mode == 'berkas_pernyataan')
			{
				if ($valid)
				{
					$this->cmb_model->set_syarat_valid($id_c_mhs, 'berkas_pernyataan');
				}
				else
				{
					$this->cmb_model->set_syarat_invalid($id_c_mhs, 'berkas_pernyataan', $this->input->post('pesan_v_berkas_pernyataan'));
				}
			}
			else if ($mode == 'syarat')
			{
				$id_syarat_prodi = $this->input->post('id_syarat_prodi');
				
				if ($valid == '1')		/* Syarat Valid */
				{
					$this->cmb_model->set_syarat_valid_2($id_c_mhs, $id_syarat_prodi);
					$this->cmb_model->set_syarat_valid($id_c_mhs, $id_syarat_prodi);
				}
				else if ($valid == '2')	/* Syarat dibatalkan */
				{
					$this->cmb_model->set_syarat_batalvalid($id_c_mhs, $id_syarat_prodi);
				}
				else if ($valid == '0')	/* Syarat Tidak Valid */
				{
					// input pesan verifikator yang tidak ikut terhapus.
					$this->cmb_model->set_syarat_invalid_2($id_c_mhs, $id_syarat_prodi, $this->input->post('pesan_verifikator_' . $id_syarat_prodi));
					$this->cmb_model->set_syarat_invalid($id_c_mhs, $id_syarat_prodi, $this->input->post('pesan_verifikator_' . $id_syarat_prodi));
				}
			}
			else if ($mode == 'sp3')
			{
				$sp3_1		= str_replace(",", "", $this->input->post('sp3_1'));
				$sp3_1_min	= $this->input->post('sp3_1_min');
				$sp3_2		= str_replace(",", "", $this->input->post('sp3_2'));
				$sp3_2_min	= $this->input->post('sp3_2_min');
				$sp3_3		= str_replace(",", "", $this->input->post('sp3_3'));
				$sp3_3_min	= $this->input->post('sp3_3_min');
				$sp3_4		= str_replace(",", "", $this->input->post('sp3_4'));
				$sp3_4_min	= $this->input->post('sp3_4_min');
				$is_matrikulasi = $this->input->post('is_matrikulasi');
				$cmb->IS_MATRIKULASI = $is_matrikulasi;
				
				$this->form_validation->set_rules('sp3_1', 'SP3', 'callback_sp3_check[' . $sp3_1_min . ']');
				
				if ($cmb->ID_PILIHAN_2 != '') { $this->form_validation->set_rules('sp3_2', 'SP3', 'callback_sp3_check[' . $sp3_2_min . ']'); }
				if ($cmb->ID_PILIHAN_3 != '') { $this->form_validation->set_rules('sp3_3', 'SP3', 'callback_sp3_check[' . $sp3_3_min . ']'); }
				if ($cmb->ID_PILIHAN_4 != '') { $this->form_validation->set_rules('sp3_4', 'SP3', 'callback_sp3_check[' . $sp3_4_min . ']'); }
				
				if ($this->form_validation->run() == TRUE)
				{
					$this->cmb_model->update_sp3($id_c_mhs, $sp3_1, $sp3_2, $sp3_3, $sp3_4, $is_matrikulasi);
				}
			}
			else if ($mode == 'sp3-kb')
			{
				$is_matrikulasi			= $this->input->post('is_matrikulasi');
				$cmb->IS_MATRIKULASI = $is_matrikulasi;
				$id_kelompok_biaya_1	= $this->input->post('id_kelompok_biaya_1');
				
				$sp3_1		= str_replace(",", "", $this->input->post('sp3_1'));
				$sp3_1_min	= $this->input->post('sp3_1_min');
				
				$this->cmb_model->get_kelompok_biaya_cmb($cmb);
				
				// Jika mempunyai pilihan kelompok biaya
				if (count($cmb->PILIHAN_1->kelompok_biaya_set) > 0)
				{
					// id_kelompok_biaya langsung diupdatekan agar bisa terselect saat POST
					if ($id_kelompok_biaya_1 != '')
					{
						$this->cmb_model->update_kelompok_biaya($id_c_mhs, $id_kelompok_biaya_1);
					}
					
					$this->form_validation->set_rules('id_kelompok_biaya_1', 'Pilihan Kelas', 'required');
					$this->form_validation->set_rules('sp3_1', 'SP3', 'callback_sp3_check[' . $sp3_1_min . ']');
					
					if ($this->form_validation->run() == TRUE)
					{
						$this->cmb_model->update_sp3($id_c_mhs, $sp3_1, null, null, null, $is_matrikulasi);
					}
				}
				else // Jika tidak mempunyai kelompok biaya
				{
					$this->form_validation->set_rules('sp3_1', 'SP3', 'callback_sp3_check[' . $sp3_1_min . ']');
					
					if ($this->form_validation->run() == TRUE)
					{
						$this->cmb_model->update_sp3($id_c_mhs, $sp3_1, null, null, null, $is_matrikulasi);
					}
				}
			}
			else if ($mode == 'ukt')
			{
				if ($cmb->ID_PILIHAN_1 != '') { $this->form_validation->set_rules('id_kelompok_biaya_1', 'Pilihan UKT', 'required'); }
				if ($cmb->ID_PILIHAN_2 != '') { $this->form_validation->set_rules('id_kelompok_biaya_2', 'Pilihan UKT', 'required'); }
				if ($cmb->ID_PILIHAN_3 != '') { $this->form_validation->set_rules('id_kelompok_biaya_3', 'Pilihan UKT', 'required'); }
				if ($cmb->ID_PILIHAN_4 != '') { $this->form_validation->set_rules('id_kelompok_biaya_4', 'Pilihan UKT', 'required'); }
				
				if ($this->form_validation->run() == TRUE)
				{
					$id_kb_1 = $this->input->post('id_kelompok_biaya_1');
					$id_kb_2 = $this->input->post('id_kelompok_biaya_2');
					$id_kb_3 = $this->input->post('id_kelompok_biaya_3');
					$id_kb_4 = $this->input->post('id_kelompok_biaya_4');
					
					$this->cmb_model->update_kelompok_biaya($id_c_mhs, $id_kb_1, $id_kb_2, $id_kb_3, $id_kb_4);
				}
			}
			else if ($mode == 'akreditasi')
			{
				$this->form_validation->set_rules('jenis_akreditasi_s1', 'Jenis Akreditasi', 'required');
				$this->form_validation->set_rules('peringkat_akreditasi_s1', 'Peringkat Akreditasi', 'required');
				
				if ($this->form_validation->run() == TRUE)
				{
					$this->cmb_model->update_akreditasi($id_c_mhs, $this->input->post('jenis_akreditasi_s1'), $this->input->post('peringkat_akreditasi_s1'));
				}
			}
			else if ($mode == 'pt_asal')
			{
				$gelar_s1		= $this->input->post('gelar_s1');
				$status_ptn_s1	= $this->input->post('status_ptn_s1');
				$ip_s1			= $this->input->post('ip_s1');
				$status_ptn_s2	= $this->input->post('status_ptn_s2');
				$ip_s2			= $this->input->post('ip_s2');
				$nilai_toefl	= $this->input->post('nilai_toefl');
				
				$this->cmb_model->update_pt_asal($id_c_mhs, $status_ptn_s1, $ip_s1, $status_ptn_s2, $ip_s2, $nilai_toefl);
				$this->cmb_model->update_gelar($id_c_mhs, $gelar_s1);
				// Assign ke cmb yg telah di load
				$cmb->GELAR = $gelar_s1;
			}
			else if ($mode == 'validasi_masal')
			{
				$validmasal		= $this->input->post('validmasal');
				if( $validmasal == '1' )
				{
					$this->cmb_model->set_syarat_valid_all($id_c_mhs);
				}
				else if( $validmasal == '0' )
				{
					$this->cmb_model->set_syarat_invalid_all($id_c_mhs);
				}
			}
		}
		
		// Mendapatkan data CMF, CMS, CMP, CMD
		$cmf = $this->cmb_model->get_cmf($id_c_mhs);
		$cms = $this->cmb_model->get_cms($id_c_mhs);
		$cmp = $this->cmb_model->get_cmp($id_c_mhs);
		$cmd = $this->cmb_model->get_cmd($id_c_mhs);
		
		// ----------------------------------------------------------------------------
		// Mendapatkan Jenis Formulir utk keperluan informasi asal prodi / asal sekolah
		// ----------------------------------------------------------------------------
		/*if (in_array($cmb->PENERIMAAN->ID_JENJANG, array(1, 4, 5)))			// S1, D4, dan D3
		{
			if (in_array($cmb->PENERIMAAN->ID_JALUR, array(3, 5, 20, 27)))	// Mandiri, PBSB, Mandiri Int, Diploma
			{
				$cmb->JENIS_FORM = 's1';
			}
			else if (in_array($cmb->PENERIMAAN->ID_JALUR, array(4)))		// Alih Jenis
			{
				$cmb->JENIS_FORM = 's1-aj';
			}
			else if (in_array($cmb->PENERIMAAN->ID_JALUR, array(42)))		// D4 dari SMA
			{
				$cmb->JENIS_FORM = 'd4-sma';
			}
		}
		else if ($cmb->PENERIMAAN->ID_JENJANG == 2)							// Magister
		{
			if (in_array($cmb->PENERIMAAN->ID_JALUR, array(6, 32, 34, 35, 41)))	// Pasca, Kjsm Borne, Kjsm Haluoleo, Kjsm Jakarta, Pasca-Int
			{
				$cmb->JENIS_FORM = 's2';
			}
		}
		else if ($cmb->PENERIMAAN->ID_JENJANG == 3)							// Doktor
		{
			if (in_array($cmb->PENERIMAAN->ID_JALUR, array(6, 32, 34, 35, 41)))	// Pasca, Kjsm Borne, Kjsm Haluoleo, Kjsm Jakarta, Pasca-Int
			{
				$cmb->JENIS_FORM = 's3';
			}
		}
		else if ($cmb->PENERIMAAN->ID_JENJANG == 9)							// Profesi
		{
			$cmb->JENIS_FORM = 's2';	
		}
		else if ($cmb->PENERIMAAN->ID_JENJANG == 10)						// Spesialis
		{
			$cmb->JENIS_FORM = 'sp';
		}*/
		
		
		// ----------------------------------------------------------
		// Mendapatkan jenis pembayaran
		// S1-Mandiri	: UKT (per Kelompok Biaya)
		// D3			: UKT, dgn kelompok biaya bisa diganti ketika regmaba
		// Lainnya		: SP3
		// ----------------------------------------------------------
		/*if (($cmb->PENERIMAAN->ID_JENJANG == 1 && $cmb->PENERIMAAN->ID_JALUR == 3) ||
			($cmb->PENERIMAAN->ID_JENJANG == 5 && $cmb->PENERIMAAN->ID_JALUR == 5) ||
			($cmb->PENERIMAAN->ID_JENJANG == 4 && $cmb->PENERIMAAN->ID_JALUR == 4)||
			($cmb->PENERIMAAN->ID_JENJANG == 4 && $cmb->PENERIMAAN->ID_JALUR == 42) )
		{
			$cmb->JENIS_PEMBAYARAN = 'UKT';
			$this->cmb_model->get_kelompok_biaya_cmb($cmb);
		}
		else if ($cmb->PENERIMAAN->ID_JENJANG == 2)
		{
			$cmb->JENIS_PEMBAYARAN = 'SP3-KB';  // SP3 menggunakan Kelompok Biaya
			$this->cmb_model->get_kelompok_biaya_cmb($cmb);
			$this->cmb_model->get_sp3_cmb($cmb, $cmd);
			
			// Jika terdapat pilihan kelas
			if (count($cmb->PILIHAN_1->kelompok_biaya_set) > 0)
			{
				// mendapatkan sop dan sp3 jika sudah ada pilihannya
				if ($cmd->ID_KELOMPOK_BIAYA_1 != '')
				{
					$sop_sp3 = $this->cmb_model->get_sop_sp3($cmb->ID_PENERIMAAN, $cmb->ID_PILIHAN_1, $cmd->ID_KELOMPOK_BIAYA_1);
					$this->smarty->assignByRef('sop_sp3', $sop_sp3);
				}
			}
		}
		else
		{
			$cmb->JENIS_PEMBAYARAN = 'SP3';
			$this->cmb_model->get_sp3_cmb($cmb, $cmd);
		}*/
		
		// Proses Syarat dan Pengecekan verifikasi
		$syarat_umum_set			= $this->cmb_model->get_list_syarat_umum($id_c_mhs, $cmb->ID_PENERIMAAN);
		$syarat_prodi_set			= $this->cmb_model->get_list_syarat_prodi($id_c_mhs, $cmb->ID_PENERIMAAN, $cmb->ID_PILIHAN_1, $cmb->ID_PILIHAN_2, $cmb->ID_PILIHAN_3, $cmb->ID_PILIHAN_4);
		$is_syarat_verified			= $this->cmb_model->is_syarat_verified($cmf, $syarat_umum_set, $syarat_prodi_set, $cmb->PESAN_VERIFIKATOR);
		$is_syarat_complete_check	= $this->cmb_model->is_syarat_complete_check($cmf, $syarat_umum_set, $syarat_prodi_set);
		
		// Proses Assign data ke template
		$this->smarty->assignByRef('cmb', $cmb);
		$this->smarty->assignByRef('cmf', $cmf);
		$this->smarty->assignByRef('cms', $cms);
		$this->smarty->assignByRef('cmp', $cmp);
		$this->smarty->assignByRef('cmd', $cmd);
		$this->smarty->assignByRef('syarat_umum_set', $syarat_umum_set);
		$this->smarty->assignByRef('syarat_prodi_set', $syarat_prodi_set);
		$this->smarty->assignByRef('is_syarat_verified', $is_syarat_verified);
		$this->smarty->assign('is_syarat_complete_check', $is_syarat_complete_check);
		
		$this->smarty->display('verifikasi/proses.tpl');
	}
	
	/**
	 * Halaman lihat verifikasi
	 * @param int $id_c_mhs
	 */
	function proses_lihat($id_c_mhs)
	{
		// Mendapatkan CMB di awal untuk proses validasi SP3
		$cmb = $this->cmb_model->get_cmb($id_c_mhs);
		
		
		// Mendapatkan data CMF, CMS, CMP, CMD
		$cmf = $this->cmb_model->get_cmf($id_c_mhs);
		$cms = $this->cmb_model->get_cms($id_c_mhs);
		$cmp = $this->cmb_model->get_cmp($id_c_mhs);
		$cmd = $this->cmb_model->get_cmd($id_c_mhs);
		
		// ----------------------------------------------------------------------------
		// Mendapatkan Jenis Formulir utk keperluan informasi asal prodi / asal sekolah
		// ----------------------------------------------------------------------------
		if (in_array($cmb->PENERIMAAN->ID_JENJANG, array(1, 4, 5)))			// S1, D4, dan D3
		{
			if (in_array($cmb->PENERIMAAN->ID_JALUR, array(3, 5, 20, 27)))	// Mandiri, PBSB, Mandiri Int, Diploma
			{
				$cmb->JENIS_FORM = 's1';
			}
			else if (in_array($cmb->PENERIMAAN->ID_JALUR, array(4)))		// Alih Jenis
			{
				$cmb->JENIS_FORM = 's1-aj';
			}
			else if (in_array($cmb->PENERIMAAN->ID_JALUR, array(42)))		// D4 dari SMA
			{
				$cmb->JENIS_FORM = 'd4-sma';
			}
		}
		else if ($cmb->PENERIMAAN->ID_JENJANG == 2)							// Magister
		{
			if (in_array($cmb->PENERIMAAN->ID_JALUR, array(6, 32, 34, 35, 41)))	// Pasca, Kjsm Borne, Kjsm Haluoleo, Kjsm Jakarta, Pasca-Int
			{
				$cmb->JENIS_FORM = 's2';
			}
		}
		else if ($cmb->PENERIMAAN->ID_JENJANG == 3)							// Doktor
		{
			if (in_array($cmb->PENERIMAAN->ID_JALUR, array(6, 32, 34, 35, 41)))	// Pasca, Kjsm Borne, Kjsm Haluoleo, Kjsm Jakarta, Pasca-Int
			{
				$cmb->JENIS_FORM = 's3';
			}
		}
		else if ($cmb->PENERIMAAN->ID_JENJANG == 9)							// Profesi
		{
			$cmb->JENIS_FORM = 's2';	
		}
		else if ($cmb->PENERIMAAN->ID_JENJANG == 10)						// Spesialis
		{
			$cmb->JENIS_FORM = 'sp';
		}
		
		
		// ----------------------------------------------------------
		// Mendapatkan jenis pembayaran
		// S1-Mandiri	: UKT (per Kelompok Biaya)
		// D3			: UKT, dgn kelompok biaya bisa diganti ketika regmaba
		// Lainnya		: SP3
		// ----------------------------------------------------------
		if (($cmb->PENERIMAAN->ID_JENJANG == 1 && $cmb->PENERIMAAN->ID_JALUR == 3) ||
			($cmb->PENERIMAAN->ID_JENJANG == 5 && $cmb->PENERIMAAN->ID_JALUR == 5) ||
			($cmb->PENERIMAAN->ID_JENJANG == 4 && $cmb->PENERIMAAN->ID_JALUR == 4)||
			($cmb->PENERIMAAN->ID_JENJANG == 4 && $cmb->PENERIMAAN->ID_JALUR == 42) )
		{
			$cmb->JENIS_PEMBAYARAN = 'UKT';
			$this->cmb_model->get_kelompok_biaya_cmb($cmb);
		}
		else if ($cmb->PENERIMAAN->ID_JENJANG == 2)
		{
			$cmb->JENIS_PEMBAYARAN = 'SP3-KB';  // SP3 menggunakan Kelompok Biaya
			$this->cmb_model->get_kelompok_biaya_cmb($cmb);
			$this->cmb_model->get_sp3_cmb($cmb, $cmd);
			
			// Jika terdapat pilihan kelas
			if (count($cmb->PILIHAN_1->kelompok_biaya_set) > 0)
			{
				// mendapatkan sop dan sp3 jika sudah ada pilihannya
				if ($cmd->ID_KELOMPOK_BIAYA_1 != '')
				{
					$sop_sp3 = $this->cmb_model->get_sop_sp3($cmb->ID_PENERIMAAN, $cmb->ID_PILIHAN_1, $cmd->ID_KELOMPOK_BIAYA_1);
					$this->smarty->assignByRef('sop_sp3', $sop_sp3);
				}
			}
		}
		else
		{
			$cmb->JENIS_PEMBAYARAN = 'SP3';
			$this->cmb_model->get_sp3_cmb($cmb, $cmd);
		}
		
		// Proses Syarat dan Pengecekan verifikasi
		$syarat_umum_set			= $this->cmb_model->get_list_syarat_umum($id_c_mhs, $cmb->ID_PENERIMAAN);
		$syarat_prodi_set			= $this->cmb_model->get_list_syarat_prodi($id_c_mhs, $cmb->ID_PENERIMAAN, $cmb->ID_PILIHAN_1, $cmb->ID_PILIHAN_2, $cmb->ID_PILIHAN_3, $cmb->ID_PILIHAN_4);
		$is_syarat_verified			= $this->cmb_model->is_syarat_verified($cmf, $syarat_umum_set, $syarat_prodi_set, $cmb->PESAN_VERIFIKATOR);
		$is_syarat_complete_check	= $this->cmb_model->is_syarat_complete_check($cmf, $syarat_umum_set, $syarat_prodi_set);
		
		// Proses Assign data ke template
		$this->smarty->assignByRef('cmb', $cmb);
		$this->smarty->assignByRef('cmf', $cmf);
		$this->smarty->assignByRef('cms', $cms);
		$this->smarty->assignByRef('cmp', $cmp);
		$this->smarty->assignByRef('cmd', $cmd);
		$this->smarty->assignByRef('syarat_umum_set', $syarat_umum_set);
		$this->smarty->assignByRef('syarat_prodi_set', $syarat_prodi_set);
		$this->smarty->assignByRef('is_syarat_verified', $is_syarat_verified);
		$this->smarty->assign('is_syarat_complete_check', $is_syarat_complete_check);
		
		$this->smarty->display('verifikasi/proses_lihat.tpl');
	}
	
	/**
	 * 
	 * @param string $q
	 */
	function reset()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST')
		{
			$id_c_mhs = $this->input->post('id_c_mhs');
			
			$this->cmb_model->reset_verifikasi($id_c_mhs);
			
			$this->smarty->assign('reset_success', TRUE);
		}
		
		$q = $this->input->get('q', TRUE);
		
		if ($q != '')
		{
			$cmb = $this->cmb_model->get_cmb_reset($q);
			$this->smarty->assignByRef('cmb', $cmb);
		}
		
		$this->smarty->assign('q', $q);
		$this->smarty->display('verifikasi/reset.tpl');
	}
	
	function sp3_check($str, $sp3_min)
	{
		$str = str_replace(",", "", $str);
		
		if ((int)$str < $sp3_min)
		{
			$this->form_validation->set_message('sp3_check', '%s minimal sama dgn ' . number_format($sp3_min, 0, ",", "."));
			
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	function get_sop_sp3($id_penerimaan, $id_program_studi, $id_kelompok_biaya)
	{ 
		echo json_encode($this->cmb_model->get_sop_sp3($id_penerimaan, $id_program_studi, $id_kelompok_biaya));
	}
	
	function rotate_image($id_penerimaan, $id_c_mhs, $file, $degree)
	{
		//$filename = "files/{$id_penerimaan}/{$id_c_mhs}/{$file}";
		//$filename = "../../../../web/files/{$id_penerimaan}/{$id_c_mhs}/{$file}";
		$filename = "/web/files/{$id_penerimaan}/{$id_c_mhs}/{$file}";
		
		if (file_exists($filename))
		{
			$img = new Imagick();
			$img->readimage($filename);
			
			if ($img->valid())
			{
				$img->rotateimage(new ImagickPixel('none'), $degree);
				$img->writeimage($filename); // overwrite
				
				echo "http://pendaftaran.unair.ac.id/files/{$id_penerimaan}/{$id_c_mhs}/{$file}?t=" . time();
			}
			else
			{
				echo 2;
			}
			
			$img->clear();
		}
		else
		{
			echo 4;
		}
	}
	
	function cetak_presensi()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST')
		{
			// Tambahkan validasi form
			$this->form_validation->set_rules('id_penerimaan', 'Penerimaan', 'required');
			$this->form_validation->set_rules('waktu_awal', 'Waktu Awal', 'required');
			$this->form_validation->set_rules('waktu_akhir', 'Waktu Akhir', 'required');
			
			// Saat berhasil
			if ($this->form_validation->run())
			{
				// Ambil Inputan
				$id_penerimaan	= $this->input->post('id_penerimaan');
				$waktu_awal		= $this->input->post('waktu_awal');
				$waktu_akhir	= $this->input->post('waktu_akhir');
			
				// Load TCPDF
				$this->load->library('tcpdf', array(
					'unicode'		=> false,
					'encoding'		=> 'ISO-8859-1',
					'pdfa'			=> true
				));

				// Load Helper
				$this->load->helper('kartu_presensi');

				$cmb_set = $this->cmb_model->list_cmb_cetak_presensi($id_penerimaan, $waktu_awal, $waktu_akhir);

				// pembentukan nama file (permudah arsiparis)
				$filename = $id_penerimaan . '_' . date('YmdHi', strtotime($waktu_awal)) . '_' . date('YmdHi', strtotime($waktu_akhir)) . '.pdf';

				generate_kartu_presensi($this->tcpdf, $cmb_set, $filename);
			}
		}
		
		$pengguna = $this->session->userdata('pengguna');
		
		$penerimaan_set = $this->penerimaan_model->list_penerimaan_cetak_presensi($pengguna->ID_PENGGUNA);
		
		$this->smarty->assignByRef('penerimaan_set', $penerimaan_set);
		
		$this->smarty->display('verifikasi/cetak_presensi.tpl');
	}
	
	
	/**
	 * Halaman verifikasi akreditasi AJ ini sudah tidak dipakai
	 * @param type $mode
	 * @param type $param1
	 * @param type $param2
	 */
	function akreditasi_aj($mode = 'list', $param1 = null, $param2 = null)
	{		
		// Informasi Dialihkan
		$this->smarty->display('verifikasi/akreditasi_aj/dialihkan.tpl');
		return;
		
		$id_penerimaan = 186;
		
		if ($mode == 'list')
		{
			$this->query = $this->db->query(
				"select 
					(select count(cmb.id_c_mhs) from aucc.calon_mahasiswa_baru cmb
					join aucc.calon_mahasiswa_pasca cmp on cmp.id_c_mhs = cmb.id_c_mhs
					where id_penerimaan = {$id_penerimaan} and
					  tgl_verifikasi_ppmb is not null and is_verifikasi_akreditasi is null and
					  (cmp.JENIS_AKREDITASI_S1 is null or cmp.peringkat_akreditasi_s1 is null)) as jumlah_antrian,
					(select count(cmb.id_c_mhs) from aucc.calon_mahasiswa_baru cmb
					join aucc.calon_mahasiswa_pasca cmp on cmp.id_c_mhs = cmb.id_c_mhs
					where id_penerimaan = {$id_penerimaan} and
					  tgl_verifikasi_ppmb is not null and is_verifikasi_akreditasi is null and
					  (cmp.JENIS_AKREDITASI_S1 is not null and cmp.peringkat_akreditasi_s1 is not null)) as jumlah_selesai,
					(select count(cmb.id_c_mhs) from aucc.calon_mahasiswa_baru cmb
					join aucc.calon_mahasiswa_pasca cmp on cmp.id_c_mhs = cmb.id_c_mhs
					where id_penerimaan = {$id_penerimaan} and
					  tgl_verifikasi_ppmb is not null and is_verifikasi_akreditasi is not null) as jumlah_proses
				  from dual");
				
			$this->smarty->assignByRef('verifikasi', $this->query->row());
		}
		
		if ($mode == 'antrian')
		{
			$this->query = $this->db->query(
				"select * from (
					select cmb.id_c_mhs, nm_c_mhs, cmp.JENIS_AKREDITASI_S1, cmp.peringkat_akreditasi_s1
					from aucc.calon_mahasiswa_baru cmb
					join aucc.calon_mahasiswa_pasca cmp on cmp.id_c_mhs = cmb.id_c_mhs
					where id_penerimaan = 186 and
					tgl_verifikasi_ppmb is not null and is_verifikasi_akreditasi is null and
					(cmp.JENIS_AKREDITASI_S1 is null or cmp.peringkat_akreditasi_s1 is null)
				) where rownum <= 100");
			
			$cmb_set = array();
			
			foreach ($this->query->result() as $row)
			{
				array_push($cmb_set, $row);
			}
			
			$this->smarty->assignByRef('cmb_set', $cmb_set);
		}
		
		if ($mode == 'antrian_json')
		{
			$this->query = $this->db->query(
				"select t.*, rownum from (
					select cmb.id_c_mhs, nm_c_mhs, cmp.JENIS_AKREDITASI_S1, cmp.peringkat_akreditasi_s1
					from aucc.calon_mahasiswa_baru cmb
					join aucc.calon_mahasiswa_pasca cmp on cmp.id_c_mhs = cmb.id_c_mhs
					where id_penerimaan = 186 and
					tgl_verifikasi_ppmb is not null and is_verifikasi_akreditasi is null and
					(cmp.JENIS_AKREDITASI_S1 is null or cmp.peringkat_akreditasi_s1 is null)
				) t where rownum <= 100");
			
			$cmb_set = array();
			
			foreach ($this->query->result() as $row)
			{
				array_push($cmb_set, $row);
			}
			
			echo json_encode($cmb_set);
			exit();
		}
		
		if ($mode == 'proses')
		{
			$this->query = $this->db->query(
				"select cmb.id_c_mhs, nm_c_mhs, cmp.JENIS_AKREDITASI_S1, cmp.peringkat_akreditasi_s1
				from aucc.calon_mahasiswa_baru cmb
				join aucc.calon_mahasiswa_pasca cmp on cmp.id_c_mhs = cmb.id_c_mhs
				where id_penerimaan = 186 and
				tgl_verifikasi_ppmb is not null and is_verifikasi_akreditasi is not null");
			
			$cmb_set = array();
			
			foreach ($this->query->result() as $row)
			{
				array_push($cmb_set, $row);
			}
			
			$this->smarty->assignByRef('cmb_set', $cmb_set);
		}
		
		if ($mode == 'selesai')
		{
			$this->query = $this->db->query(
				"select cmb.id_c_mhs, nm_c_mhs, cmp.JENIS_AKREDITASI_S1, cmp.peringkat_akreditasi_s1
				from aucc.calon_mahasiswa_baru cmb
				join aucc.calon_mahasiswa_pasca cmp on cmp.id_c_mhs = cmb.id_c_mhs
				where id_penerimaan = 186 and
				tgl_verifikasi_ppmb is not null and 
				(cmp.JENIS_AKREDITASI_S1 is not null and cmp.peringkat_akreditasi_s1 is not null)");
			
			$cmb_set = array();
			
			foreach ($this->query->result() as $row)
			{
				array_push($cmb_set, $row);
			}
			
			$this->smarty->assignByRef('cmb_set', $cmb_set);
		}
		
		if ($mode == 'start')
		{
			$id_c_mhs = (int)$param1;
			
			$this->query = $this->db->select('is_verifikasi_akreditasi')
				->get_where('calon_mahasiswa_pasca', array('id_c_mhs' => $id_c_mhs));
			$is_verifikasi_akreditasi = $this->query->result()->IS_VERIFIKASI_AKREDITASI;
			
			if ($is_verifikasi_akreditasi == '')
			{
				$this->db->update('calon_mahasiswa_pasca', array(
					'is_verifikasi_akreditasi' => 1
				), array('id_c_mhs' => $id_c_mhs));
				
				// lanjut ke peserta
				redirect('verifikasi/akreditasi_aj/peserta/' . $id_c_mhs); exit();
			}
			else
			{
				// kembali ke antrian
				redirect('verifikasi/akreditasi_aj/antrian/'); exit();
			}
		}
		
		if ($mode == 'cancel' || $mode == 'submit')
		{
			$id_c_mhs = (int)$param1;
			
			$this->db->update('calon_mahasiswa_pasca', array(
					'is_verifikasi_akreditasi' => NULL
				), array('id_c_mhs' => $id_c_mhs));
			
			// kembali ke antrian
			redirect('verifikasi/akreditasi_aj/antrian/'); exit();
		}
		
		if ($mode == 'peserta')
		{
			$id_c_mhs = (int)$param1;
			
			$this->query = $this->db->query(
				"select cmb.id_c_mhs, cmb.id_penerimaan, nm_c_mhs, cmp.JENIS_AKREDITASI_S1, cmp.peringkat_akreditasi_s1,
					cmf1.FILE_SYARAT as file_ijazah,
					cmf2.file_syarat as file_akreditasi
				  from aucc.calon_mahasiswa_baru cmb
				  join aucc.calon_mahasiswa_pasca cmp on cmp.id_c_mhs = cmb.id_c_mhs
				  left join aucc.calon_mahasiswa_syarat cmf1 on cmf1.id_c_mhs = cmb.id_c_mhs and cmf1.id_syarat_prodi = 3618
				  left join aucc.calon_mahasiswa_syarat cmf2 on cmf2.id_c_mhs = cmb.id_c_mhs and cmf2.id_syarat_prodi = 3620
				  where cmb.id_c_mhs = {$id_c_mhs}");
			
			$this->smarty->assignByRef('cmb', $this->query->row());
		}
		
		if ($mode == 'update_jenis')
		{
			$id_c_mhs = (int)$param1;
			$jenis_akreditasi = $param2;
			
			$this->db->update('calon_mahasiswa_pasca', array(
				'jenis_akreditasi_s1' => $jenis_akreditasi
			), array('id_c_mhs' => $id_c_mhs));
			
			exit();
		}
		
		if ($mode == 'update_peringkat')
		{
			$id_c_mhs = (int)$param1;
			$peringkat_akreditasi = $param2;
			
			$this->db->update('calon_mahasiswa_pasca', array(
				'peringkat_akreditasi_s1' => $peringkat_akreditasi
			), array('id_c_mhs' => $id_c_mhs));
			
			exit();
		}
		
		$this->smarty->display('verifikasi/akreditasi_aj/'.$mode.'.tpl');
	}
	
	/**
	 * Halaman untuk mengedit akreditasi
	 */
	function edit_akreditasi()
	{
		// pembatasan
		if ($this->input->server('REMOTE_ADDR') != '210.57.215.30') { return; }
		
		if ($this->input->server('REQUEST_METHOD') == 'POST')
		{
			$id_c_mhs = $this->input->post('id_c_mhs');
			
			$result = $this->db->update('calon_mahasiswa_pasca', array(
				'jenis_akreditasi_s1'		=> $this->input->post('jenis_akreditasi_s1'),
				'peringkat_akreditasi_s1'	=> $this->input->post('peringkat_akreditasi_s1')
			), array('id_c_mhs' => $id_c_mhs));
			
			$result = $this->db->update('nilai_cmhs_pasca', array(
				'nilai_akreditasi'	=> $this->input->post('nilai_akreditasi'),
			), array('id_c_mhs' => $id_c_mhs));
			
			// refresh total nilai
			$this->db->query("update aucc.nilai_cmhs_pasca set total_nilai = nilai_tpa + nilai_prestasi + nilai_akreditasi where id_c_mhs = {$id_c_mhs}");
			
			if ($result)
			{
				$this->smarty->assign('success', TRUE);
			}
		}
		
		$no_ujian = $this->input->get('q');
		
		if ($no_ujian != '')
		{
			$this->query = $this->db
				->get_where('calon_mahasiswa_baru', array('no_ujian' => $no_ujian), 2);
			$cmb = $this->query->row();
			$cmp = $this->cmb_model->get_cmp($cmb->ID_C_MHS);
			
			// Nilai 
			$this->query = $this->db->get_where('nilai_cmhs_pasca', array('id_c_mhs' => $cmb->ID_C_MHS), 2);
			$nilai = $this->query->row();
			
			$this->smarty->assignByRef('cmb', $cmb);
			$this->smarty->assignByRef('cmp', $cmp);
			$this->smarty->assignByRef('nilai', $nilai);
		}
		
		$this->smarty->display('verifikasi/edit_akreditasi/edit.tpl');
	}
}
