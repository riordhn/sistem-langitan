<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Fathoni <fathoni@staf.unair.ac.id>
 * @property Calon_Pendaftar_Model $calon_pendaftar_model Description
 */
class Helpdesk extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
		
		$this->check_credentials();
		
		$this->load->model('Calon_Pendaftar_Model', 'calon_pendaftar_model');
	}
	
	function aktivasi_login()
	{		
		if ($this->input->server('REQUEST_METHOD') == 'POST')
		{
			$mode				= $this->input->post('mode');
			$id_calon_pendaftar = $this->input->post('id_calon_pendaftar');
			
			if ($mode == 'aktifkan_login')
			{
				$this->calon_pendaftar_model->set_konfirmasi_email($id_calon_pendaftar);
				$this->smarty->assign('aktivasi_success', TRUE);
			}
		
			if ($mode == 'set_password')
			{
				$this->calon_pendaftar_model->set_pass_temp($id_calon_pendaftar, $this->input->post('pass_temp'));
				$this->smarty->assign('pass_temp_success', TRUE);
			}
			if ($mode == 'set_batasberkas')
			{
				$this->calon_pendaftar_model->set_batasberkas($id_calon_pendaftar, $this->input->post('batasberkas_temp'));
				$this->smarty->assign('batasberkas_success', TRUE);
			}
			if ($mode == 'reset_password')
			{
				$this->calon_pendaftar_model->reset_pass($id_calon_pendaftar);
				$this->smarty->assign('reset_pass_success', TRUE);
			}
			if ($mode == 'reset_petugasverifikasi')
			{
				$this->calon_pendaftar_model->reset_petugasverifikasi($id_calon_pendaftar);
				$this->smarty->assign('reset_petugas_success', TRUE);
			}
		}
		
		$q = $this->input->get('q', TRUE);
		
		if ($q != '')
		{
			/*$calon_pendaftar = $this->calon_pendaftar_model->get_by_email(strtolower($q));*/
			$calon_pendaftar = $this->calon_pendaftar_model->get_by_voucher(strtolower($q), $this->perguruan_tinggi['ID_PERGURUAN_TINGGI']);
			$this->smarty->assignByRef('calon_pendaftar', $calon_pendaftar);
		}
		
		$this->smarty->display('helpdesk/aktivasi_login.tpl');
	}
	
	function reset_upload()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST')
		{
			$id_c_mhs = $this->input->post('id_c_mhs');
			
			$this->calon_pendaftar_model->reset_submit_upload($id_c_mhs);
			
			$this->smarty->assign('reset_success', TRUE);
		}
		
		$q = $this->input->get('q', TRUE);
		
		if ($q != '')
		{
			/*$calon_pendaftar = $this->calon_pendaftar_model->get_by_email(strtolower($q));*/
			$calon_pendaftar = $this->calon_pendaftar_model->get_by_voucher(strtolower($q), $this->perguruan_tinggi['ID_PERGURUAN_TINGGI']);
			$this->smarty->assignByRef('calon_pendaftar', $calon_pendaftar);
		}
		
		$this->smarty->display('helpdesk/reset_upload.tpl');
	}
}