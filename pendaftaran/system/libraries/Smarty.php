<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

require_once 'Smarty/libs/Smarty.class.php';

class CI_Smarty extends Smarty
{
	function __construct()
	{
		parent::__construct();
		
		$this->setTemplateDir(APPPATH . 'views');
		$this->setCompileDir(APPPATH . 'views_compiled');
		
		// log_message('debug', 'Smarty Initialized');
	}
	
	function CI_Smarty()
	{
		parent::Smarty();
		
		$this->setTemplateDir(APPPATH . 'views');
		$this->setCompileDir(APPPATH . 'views_compiled');
		
		// log_message('debug', 'Smarty Initialized');
	}
}

/* End of file Someclass.php */