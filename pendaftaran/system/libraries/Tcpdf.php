<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

require_once FCPATH . '/tcpdf/tcpdf.php';

class CI_Tcpdf extends TCPDF
{
	public function __construct($config = array())
	{
		if ( ! isset($config['orientation']))
		{
			$config['orientation'] = 'P';
		}
		
		if ( ! isset($config['unit']))
		{
			$config['unit'] = 'mm';
		}
		
		if ( ! isset($config['format']))
		{
			$config['format'] = 'A4';
		}
		
		if ( ! isset($config['unicode']))
		{
			$config['unicode'] = true;
		}
		
		if ( ! isset($config['encoding']))
		{
			$config['encoding'] = 'UTF-8';
		}
		
		if ( ! isset($config['diskcache']))
		{
			$config['diskcache'] = false;
		}
		
		if ( ! isset($config['pdfa']))
		{
			$config['pdfa'] = false;
		}
		
		parent::__construct($config['orientation'], $config['unit'], $config['format'], $config['unicode'], $config['encoding'], $config['diskcache'], $config['pdfa']);
	}
}

/* End of file Tcpdf.php */