<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

require_once 'recaptcha/recaptchalib.php';

class CI_Recaptcha
{
	/**
	 * Gets the challenge HTML (javascript and non-javascript version).
	 * This is called from the browser, and the resulting reCAPTCHA HTML widget
	 * is embedded within the HTML form it was called from.
	 * @param string $pubkey A public key for reCAPTCHA
	 * @param string $error The error given by reCAPTCHA (optional, default is null)
	 * @param boolean $use_ssl Should the request be made over ssl? (optional, default is false)

	 * @return string - The HTML to be embedded in the user's form.
	 */
	function get_html($publickey, $error = null, $use_ssl = false)
	{
		return recaptcha_get_html($publickey, $error, $use_ssl);
	}
	
	/**
	 * Calls an HTTP POST function to verify if the user's guess was correct
	 * @param string $privkey
	 * @param string $remoteip
	 * @param string $challenge
	 * @param string $response
	 * @param array $extra_params an array of extra variables to post to the server
	 * @return ReCaptchaResponse
	 */
	function check_answer($privkey, $remoteip, $challenge, $response, $extra_params = array())
	{
		return recaptcha_check_answer($privkey, $remoteip, $challenge, $response, $extra_params);
	}
}
