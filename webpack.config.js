const path = require('path');
const webpack = require('webpack');;

module.exports = {
    entry: {
        index: './src/main.js'
    },
    output: {
        filename: 'app.js',
        path: path.resolve(__dirname, 'public/'),
        publicPath: 'public/'
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        })
    ]
};