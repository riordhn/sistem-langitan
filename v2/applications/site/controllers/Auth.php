<?php

/**
 * @author UMAHA Team <dsi@umaha.ac.id>
 * @property CI_DB_oci8_driver $db
 * @property CI_DB_query_builder $db
 * @property CI_Session $session
 */
class Auth extends CI_Controller
{
	/**
	 * Create session dari id_pengguna
	 * @param int $id_pengguna
	 */
	public function create_session($id_pengguna = 0)
	{
		// destroy existing session
		if ($this->session->userdata('pengguna') != NULL)
		{
			$this->session->unset_userdata('pengguna');
		}
		
		$pengguna = $this->db
			->select('id_pengguna, nm_pengguna, join_table, id_role')
			->get_where('pengguna', ['id_pengguna'=> $id_pengguna])->row();
		
		$this->session->set_userdata('pengguna', $pengguna);
		
		// Dosen
		if ($pengguna->JOIN_TABLE == 2)
		{
			redirect(base_url() . 'dosen/');
		}
	}
}
