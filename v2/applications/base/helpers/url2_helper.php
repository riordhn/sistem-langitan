<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// ------------------------------------------------------------------------

if ( ! function_exists('assets_url'))
{
	/**
	 * Assets URL
	 *
	 * Return /assets/ url location
	 *
	 * @param	string	$uri
	 * @param	string	$protocol
	 * @return	string
	 */
	function assets_url($uri = '', $protocol = NULL)
	{
		$pre_url = get_instance()->config->base_url('assets/'.$uri, $protocol);
		
		// Drop {APP_NAME}/ dari url lengkap
		return str_replace(APP_NAME . '/', '', $pre_url);
	}
}