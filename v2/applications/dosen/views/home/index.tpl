{extends file='layout.tpl'}
{block name='content'}

	<div class="row">
		<div class="col-lg-3 col-md-4 d-none d-sm-block">
			<div class="card mb-3">
				<img class="card-img-top" src="https://bootstrap-themes.github.io/application/assets/img/iceland.jpg" alt="Card image cap">
				<div class="card-body p-3">
					<h6 class="card-title font-weight-bold">Dony Pradana, MT.</h6>
					<ul class="list-unstyled">
						<li>Homebase : S1 Teknik Mesin, Fakultas Teknik</li>
						<li>Status : Dosen Tetap</li>
						<li>NIDN : 000000000</li>
						<li>NIK : 000000000</li>
					</ul>
					<p class="h6 font-italic">Last Login : 17:29 03/09/2017</p>
					<ul class="list-unstyled">
						<li><a href="#"><i class="fa fa-key" aria-hidden="true"></i> Ganti Password</a></li>
						<li><a href="#"><i class="fa fa-power-off" aria-hidden="true"></i> Logout</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-lg-9 col-md-8">
			<div class="card mb-3">
				<div class="card-body">
					<h3>Selamat datang Dony Pradana, MT.</h3>
				</div>
			</div>
			<div class="card">
				<div class="card-header">
					Pengumuman Akademik
				</div>
				<div class="card-body">
					<h4 class="card-title"></h4>
					<p class="card-text"></p>
					<a href="#" class="btn btn-primary"></a>
				</div>
			</div>
		</div>
    </div>

{/block}