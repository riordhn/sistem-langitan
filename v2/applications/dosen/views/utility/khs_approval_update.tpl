{extends file='layout.tpl'}
{block name='content'}

	<h1>Approval KHS</h1>

	<p class="lead">Menu ini digunakan untuk melakukan approval KHS bagi mahasiswa yang mempunyai tunggakan pada semester Genap 2016/2017.</p>
	
	{if isset($alert_success)}
		<div class="alert alert-success" role="alert">
			{$alert_success}
		</div>
	{/if}
	
	{if isset($alert_danger)}
		<div class="alert alert-danger" role="alert">
			{$alert_danger}
		</div>
	{/if}

	<div class="card">
		<div class="card-header">Info Mahasiswa</div>
		<div class="card-body">
			<form method="post" action="{current_url()}?{$smarty.server.QUERY_STRING}">
				<input type="hidden" name="{$ci->security->get_csrf_token_name()}" value="{$ci->security->get_csrf_hash()}" />
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Nama</label>
					<div class="col-sm-10">
						<p class="form-control-plaintext">{$data->NM_PENGGUNA}</p>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">NIM</label>
					<div class="col-sm-10">
						<p class="form-control-plaintext">{$data->NIM_MHS}</p>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Status Tunggakan</label>
					<div class="col-sm-10">
						{if $data->IS_ADA_TAGIHAN == 1}
							<span class="badge badge-danger form-control-plaintext">Ada</span>
						{else}
							<span class="badge badge-light form-control-plaintext">Tidak Ada</span>
						{/if}
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Status Cetak KHS</label>
					<div class="col-sm-10">
						<div class="form-check form-check-inline">
							<label class="form-check-label">
								<input class="form-check-input" type="radio" name="is_boleh_cetak" value="1" {if $data->IS_BOLEH_CETAK == 1}checked{/if}> Ya
							</label>
						</div>
						<div class="form-check form-check-inline">
							<label class="form-check-label">
								<input class="form-check-input" type="radio" name="is_boleh_cetak" value="0" {if $data->IS_BOLEH_CETAK == 0}checked{/if}> Tidak
							</label>
						</div>
					</div>
					
				</div>
				<div class="form-group">
					<label class="col-form-label">Catatan :</label>
					<textarea class="form-control {if form_error('catatan_boleh_cetak')}is-invalid{/if}" name="catatan_boleh_cetak" rows="3">{$data->CATATAN_BOLEH_CETAK}</textarea>
					{if form_error('catatan_boleh_cetak')}
						<div class="invalid-feedback">{form_error('catatan_boleh_cetak')}</div>
					{/if}
				</div>
				<button class="btn btn-primary mr-3" type="submit">Simpan</button>
				<a href="{base_url('utility/khs-approval')}">Kembali</a>
			</form>
		</div>
	</div>

{/block}