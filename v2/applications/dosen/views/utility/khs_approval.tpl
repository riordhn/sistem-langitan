{extends file='layout.tpl'}
{block name='content'}

	<h1>Approval KHS</h1>

	<p class="lead">Menu ini digunakan untuk melakukan approval KHS bagi mahasiswa yang mempunyai tunggakan pada semester Genap 2016/2017.</p>

	<form class="form-inline mb-3">
		<label class="sr-only" for="search">Nama / NIM</label>
		<input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" id="search" name="search" placeholder="Nama / NIM" value="{if isset($smarty.get.search)}{$smarty.get.search}{/if}">

		<button type="submit" class="btn btn-primary">Cari</button>
	</form>

	{if isset($data_set)}
		<table class="table">
			<thead>
				<tr>
					<th>NIM</th>
					<th>Nama</th>
					<th>Program Studi</th>
					<th>Status Tunggakan</th>
					<th>Status Cetak</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				{foreach $data_set as $data}
					<tr>
						<td>{$data->NIM_MHS}</td>
						<td>{$data->NM_PENGGUNA}</td>
						<td>{$data->NM_PROGRAM_STUDI}</td>
						<td>
							{if $data->IS_ADA_TAGIHAN == 1}
								<span class="badge badge-danger">Ada</span>
							{else}
								<span class="badge badge-light">Tidak Ada</span>
							{/if}
						</td>
						<td>
							{if $data->IS_BOLEH_CETAK == 1}
								{if $data->IS_ADA_CATATAN == 0}
									<span class="badge badge-success">Boleh</span>
								{else}
									<span class="badge badge-warning">Boleh, ada catatan</span>
								{/if}
							{else}
								<span class="badge badge-secondary">Tidak Boleh</span>
							{/if}
						</td>
						<td>
							<a href="{base_url('utility/khs-approval-update/')}?id={$data->ID_MHS}">Update</a>
						</td>
					</tr>
				{/foreach}
			</tbody>
		</table>
	{/if}	
		

{/block}