<!DOCTYPE html>
<html lang="id">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Title -->
		<title>Dosen - Sistem Langitan</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />

		<!-- Site CSS -->
		<style type="text/css">
			body { padding-top: 4.5rem; }
		</style>
	</head>
	<body>

		<nav class="navbar navbar-expand-md navbar-dark fixed-top" style="background-color: darkgreen">
			<div class="container">
				<a class="navbar-brand" href="#">Dosen</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07" aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarsExample07">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item">
							<a class="nav-link" href="{base_url()}">Beranda</a>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Biodata</a>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="#">Biodata Diri</a>
								<a class="dropdown-item" href="#">Riwayat Dosen</a>
								<a class="dropdown-item" href="#">Riwayat Mengajar</a>
								<a class="dropdown-item" href="#">Evaluasi Dosen</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Bimbingan</a>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="#">Perwalian KRS</a>
								<a class="dropdown-item" href="#">Perwalian Akademik</a>
								<a class="dropdown-item" href="#">Bimbingan TA/Skripsi</a>
								<a class="dropdown-item" href="#">Bimbingan KKN</a>
								<a class="dropdown-item" href="#">Bimbingan PKL</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Akademik</a>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="#">Penilaian</a>
								<a class="dropdown-item" href="#">Komponen Nilai</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Lain-Lain</a>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="{base_url('utility/khs-approval')}">Approval KHS</a>
							</div>
						</li>
					</ul>
					<ul class="navbar-nav">
						<li class="nav-item">
							<a class="nav-link" href="#" title="Pesan"><i class="fa fa-envelope-o fa-lg" aria-hidden="true"></i></a>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Account</a>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="#">Ganti Password</a>
								<a class="dropdown-item" href="#">Logout</a>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</nav>

		<div class="container">
			{block name='content'}
			{/block}
		</div>

		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
		{block name='script'}
		{/block}
	</body>
</html>