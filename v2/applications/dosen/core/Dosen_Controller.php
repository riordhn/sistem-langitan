<?php

/**
 * @author UMAHA Team <dsi@umaha.ac.id>
 * @property CI_DB_oci8_driver $db
 * @property CI_DB_query_builder $db
 * @property CI_Loader $load
 * @property CI_Session $session
 * @property CI_Input $input
 * @property Smarty_wrapper $smarty
 */
class Dosen_Controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		if ($this->session->userdata('pengguna') == NULL)
		{
			// redirect(base_url() . '../');
		}
	}
}
