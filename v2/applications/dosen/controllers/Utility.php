<?php

/**
 * @author UMAHA Team <dsi@umaha.ac.id>
 * @property CI_Form_validation $form_validation
 */
class Utility extends Dosen_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('form_validation');
		// $this->load->
	}
	
	public function khs_approval()
	{
		// Ambil data
		$search = $this->input->get('search');
		
		// Cek jika ada isian
		if ($search != null)
		{
			$search_escaped = $this->db->escape_like_str(strtolower($search));
			
			$sql = 
				"select m.id_mhs, nim_mhs, nm_pengguna, nm_jenjang || ' ' || nm_program_studi as nm_program_studi, is_ada_tagihan, is_boleh_cetak, length(catatan_boleh_cetak) as is_ada_catatan 
				from mahasiswa m
				join pengguna p on p.id_pengguna = m.id_pengguna and p.id_perguruan_tinggi = 1
				join program_studi ps on ps.id_program_studi = m.id_program_studi
				join jenjang j on j.id_jenjang = ps.id_jenjang
				join status_pengguna sp on sp.id_status_pengguna = m.status_akademik_mhs and sp.status_aktif = 1
				left join keuangan_khs khs on khs.id_mhs = m.id_mhs and khs.id_semester = 811
				where lower(nm_pengguna) like '%{$search_escaped}%' or nim_mhs like '%{$search_escaped}%'
				order by nm_program_studi, nim_mhs";
			
			$query = $this->db->query($sql);
			
			$data_set = $query->result();
			
			$this->smarty->assign('data_set', $data_set);
		}
		
		$this->smarty->display();
	}
	
	public function khs_approval_update()
	{
		$id_semester	= 811; // Genap 2018/2019
		$id_mhs			= $this->input->get('id');
		
		if ($this->input->method() == 'post')
		{
			$post = $this->input->post(NULL, TRUE);
			
			// Ambil informasi tagihan
			$sql = 
				"select is_ada_tagihan from mahasiswa m
				left join keuangan_khs khs on khs.id_mhs = m.id_mhs and khs.id_semester = {$id_semester}
				where m.id_mhs = ?";
				
			$mhs = $this->db->query($sql, array($id_mhs))->row();
			
			// Jika boleh cetak, tapi ada tagihan
			if ($post['is_boleh_cetak'] == 1 && $mhs->IS_ADA_TAGIHAN == 1)
			{
				// tambahkan validasi
				$this->form_validation->set_rules('catatan_boleh_cetak', 'Catatan', 'required');
			}
			
			// Jika validasi oke
			if ($this->form_validation->run() == TRUE || $this->form_validation->run() == NULL)
			{
				// Update date keuangan_khs
				$result = $this->db->update('keuangan_khs', [
					'is_boleh_cetak'		=> $post['is_boleh_cetak'],
					'catatan_boleh_cetak'	=> $this->db->escape($post['catatan_boleh_cetak'])
				], ['id_mhs' => $id_mhs, 'id_semester' => $id_semester]);
				
				if ($result)
					$this->smarty->assign('alert_success', 'Berhasil disimpan');
				else
					$this->smarty->assign('alert_danger', 'Gagal disimpan');
			}
		}
		
		// Pastikan angka
		if (is_numeric($id_mhs))
		{
			$sql = 
				"select m.id_mhs, nim_mhs, nm_pengguna, nm_jenjang || ' ' || nm_program_studi as nm_program_studi, is_ada_tagihan, is_boleh_cetak, catatan_boleh_cetak 
				from mahasiswa m
				join pengguna p on p.id_pengguna = m.id_pengguna and p.id_perguruan_tinggi = 1
				join program_studi ps on ps.id_program_studi = m.id_program_studi
				join jenjang j on j.id_jenjang = ps.id_jenjang
				join status_pengguna sp on sp.id_status_pengguna = m.status_akademik_mhs and sp.status_aktif = 1
				left join keuangan_khs khs on khs.id_mhs = m.id_mhs and khs.id_semester = {$id_semester}
				where m.id_mhs = ?";
			
			$query = $this->db->query($sql, array($id_mhs));
			
			$this->smarty->assign('data', $query->row());
		}
		
		$this->smarty->display();
	}
}
