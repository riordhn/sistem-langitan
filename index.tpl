<html>
<head>
    <title>Universitas Airlangga Cyber Campus</title>
    <link rel="stylesheet" type="text/css" href="/css/reset.css" />
    <link rel="stylesheet" type="text/css" href="/css/index.css" />
    {literal}
    <script language="JavaScript1.2" type="text/javascript">
    <!--
    x = 20;
    y = 20;
    function setVisible(obj)
    {
            obj = document.getElementById(obj);
            obj.style.visibility = (obj.style.visibility == 'visible') ? 'hidden' : 'visible';
    }
    function placeIt(obj)
    {
            obj = document.getElementById(obj);
            if (document.documentElement)
            {
                    theLeft = document.documentElement.scrollLeft;
                    theTop = document.documentElement.scrollTop;
            }
            else if (document.body)
            {
                    theLeft = document.body.scrollLeft;
                    theTop = document.body.scrollTop;
            }
            theLeft += x;
            theTop += y;
            obj.style.left = theLeft + 'px' ;
            obj.style.top = theTop + 'px' ;
            setTimeout("placeIt('helpdesk')",500);
    }
    window.onscroll = setTimeout("placeIt('helpdesk')",500);
    //-->
    </script>
    <style type="text/css">
    <!--
    #helpdesk {
            position: absolute;
            visibility: hidden;
            width: 512px;
            height: 120px;
            left: 20px;
            top: 20px;
            background-color: #ccccff;
            border-radius: 5px 10px 5px 10px / 10px 5px 10px 5px;
            border-radius: 5px;
            border-radius: 5px 10px / 10px; 
            font-family: "Trebuchet MS";
            padding: 10px;
    }
    #close {
            float: right;
    }
    -->
    </style>
    <script src="/js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {

        $('.show-captcha').click(function() {
            $(this).css('display', 'none');
            $('.username-box').css('display', 'none');
            $('.password-box').css('display', 'none');
            $('input[type="submit"]').css('display', 'inline-block');
            $('.recaptcha-box').show();
            return false;
        });
    });
    </script>{/literal}
</head>

<body>
    <!-- {$false_login} -->
    <form action="/login.php{if isset($smarty.get.debug_app)}?debug_app=1{/if}" method="post">
    <div class="wrapper">
        <div class="login-box">
            {if $false_login > 3}
            <div class="recaptcha-box" style="display: none;">    
                {$recaptcha}
            </div>
            {/if}
            <div class="username-box">
                <label>USERNAME</label>
                <input type="text" name="username" />
            </div>
            <div class="password-box">
                <label>PASSWORD</label>
                <input type="password" name="password" />
            </div>
            <div class="submit-box">
			
				<input type="button" value="Chat Room" onClick="window.location.href='http://210.57.208.21/forumcyber/'" target="_blank"/>
                {if $false_login <= 3}
                    <input type="button" value="Helpdesk!" onclick="setVisible('helpdesk')" /><input type="submit" value="Login" />
                {else}
                    <input type="button" value="Helpdesk!" onclick="setVisible('helpdesk')"/>
                    <button class="show-captcha">Login</button>
                    <input type="submit" value="Login" style="display: none;" />
                {/if}
                {if isset($smarty.get.err)}{if $smarty.get.err==1}
                    <br/><span style="font-size: 11px">Login gagal, username dan password tidak sesuai.</span>
                {/if}{/if}
				
				
			</div>
        </div>
    </div>
    </form>
<div id="helpdesk">
  <span id="close"><a href="javascript:setVisible('helpdesk')"><img src="/img/login/red_close.png"></a></span>
  <p><u>Helpdesk Cyber Campus Universitas Airlangga:</u><br/>
  Telepon: 031-60277704, 031-60277703, 031-60276766<br/>
  YM!: cybercampus_unair | cybercampus_unair1 | cybercampus_unair2<br/>
  Facebook Group: <a href="http://www.facebook.com/groups/188948927806199/" target="_blank">Cyber Campus Universitas Airlangga</a></p>
  <p><u>Kontak Akademik Fakultas / Prodi:</u><br/>
  Akademik FEB: akademikfebua@yahoo.com<br/></p>
</div>
<!-- google analitycs -->
{literal}<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-24764063-1']);
  _gaq.push(['_setDomainName', '.unair.ac.id']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>{/literal}
</body>
</html>