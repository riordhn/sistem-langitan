<?php
require './vendor/autoload.php';

use Doctrine\DBAL;

// Load .env configuration
$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

$db_host			= getenv('DB_HOST');
$db_port			= getenv('DB_PORT');
$db_service_name	= getenv('DB_SERVICE_NAME');
$db_server_type		= getenv('DB_SERVER_TYPE');
$db_instance_name	= getenv('DB_INSTANCE_NAME');
$db_charset			= getenv('DB_CHARSET');
$db_user			= getenv('DB_USER');
$db_pass			= getenv('DB_PASS');

// Create DBAL Connection
$connection_params = [
	'driver' => 'oci8',
	'user' => $db_user,
	'password' => $db_pass,
	'host' => $db_host,
	'port' => $db_port,
	'service' => true,
	'servicename' => $db_service_name,
	'charset' => $db_charset
];

$conn = DBAL\DriverManager::getConnection($connection_params);

if ($conn->connect()) {
	
	// Mendapatkan versi oracle :
	// Ref : https://www.techonthenet.com/oracle/questions/version.php
	$sql = "SELECT * FROM v\$version WHERE banner LIKE 'Oracle%'";
	$stmt = $conn->query($sql);

	while ($row = $stmt->fetch()) {
		echo $row['BANNER'];
	}

	$conn->close();
}