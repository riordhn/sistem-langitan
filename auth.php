<?php

use Defuse\Crypto\Crypto;
use Defuse\Crypto\Key;
use Illuminate\Encryption\Encrypter;

require 'config.php';
require 'includes/Cryptography.class.php';

$html_output = '';
$error_message = "";
$error = false;
$success = false;
$password_request = 0;
$result_user = [];

$crypto = new Encrypter(getenv('URL_ENCRYPTION_KEY'));

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($_POST['mode'] == 'login') {
        $error_message = "";
        $error = false;
        $html_output = 'login';

        if (strlen(trim($_POST['username'])) > 0 && strlen(trim($_POST['password'])) > 0) {
            $login_result = $user->Login($_POST['username'], $_POST['password'], $PT->id_perguruan_tinggi);
        } else {
            if (strlen(trim($_POST['username'])) == 0) {
                $login_result = User::LOGIN_STATUS_WRONG_USERNAME;
            } elseif (strlen(trim($_POST['password'])) == 0) {
                $login_result = User::LOGIN_STATUS_WRONG_PASSWORD;
            }
        }

        if ($login_result == User::LOGIN_STATUS_WRONG_USERNAME) {
            $error_message = "Username tidak ditemukan, silakan mengulangi login.";
            $error = false;
        } elseif ($login_result == User::LOGIN_STATUS_WRONG_PASSWORD) {
            $error_message = "Password tidak sesuai, silakan mengulangi login.";
            $error = false;
        } elseif ($login_result == User::LOGIN_STATUS_WRONG_PT) {
            $error_message = "Login tidak sesuai, silakan mengulangi login.";
            $error = false;
        } elseif ($login_result == User::LOGIN_STATUS_FAILED) {
            $error_message = "Gagal login, silakan mengulangi login.";
            $error = false;
        } elseif ($login_result == User::LOGIN_STATUS_SUCCESS) {
            // khusus auto-login UMAHA
            if (isset($_POST['mode-login'])) {
                if ($_POST['mode-login'] == 'auto-login') {
                    $_SESSION['auto-login'] = 'yes';
                }
            }

            $username = $user->USERNAME;
            $name = $user->NAMA_PENGGUNA;
            $email = $user->EMAIL_PENGGUNA;
            $id_role = $user->ID_ROLE;
            $id_pengguna = $user->ID_PENGGUNA;

            $_SESSION['standar_iso'] = '';

            //get diff month
            $date1 = new DateTime(date('Y-m-d', strtotime($user->LAST_TIME_PASSWORD)));
            $date2 = new DateTime(date('Y-m-d'));
            $diff = $date1->diff($date2);

            $diffmonth = ($diff->format('%y') * 12) + $diff->format('%m');

            $uppercase = preg_match('@[A-Z]@', $_POST['password']);
            $lowercase = preg_match('@[a-z]@', $_POST['password']);
            $number = preg_match('@[0-9]@', $_POST['password']);

            // jika belum pernah ganti password & bukan password um@ha
            if ($user->LAST_TIME_PASSWORD == '' && $_POST['password'] != 'um@ha') {
                // Redirect ke halaman ganti password terlebih dahulu
                $_SESSION['standar_iso'] = 'no';
                header("Location: auth.php?mode=ltp-null");
                //session_destroy();
                exit();
            } elseif ($user->PASSWORD_MUST_CHANGE == 1 && $_POST['password'] != 'um@ha') { // Jika diharuskan ganti password (habis reset)
                // Redirect ke halaman ganti password terlebih dahulu
                $_SESSION['standar_iso'] = 'no';
                header("Location: auth.php?mode=ltp-null");
                exit();
            } else {
                // Redirect ke path yg sesuai
                $_SESSION['standar_iso'] = 'yes';
                header("Location: {$user->PATH}");
                exit();
            }
        }
    }

    if ($_POST['mode'] == 'change-passwd') {
        $error = false;
        $error_message = "";
        $success = false;
        $html_output = 'change-password';
        $post_password_request = post('password_request');
        $id_pengguna = post('id');
        $username = post('username');
        $password_request = $post_password_request = post('password_request');
        if ($post_password_request == '1') {
            $db->Query("SELECT * FROM PENGGUNA WHERE ID_PENGGUNA='{$id_pengguna}'");
            $pengguna = $db->FetchAssoc();
            $cp_result = $user->ChangePasswordKhususByID($id_pengguna, $username, $_POST['password_old'], $_POST['password_new']);
            $result_user =  new stdClass();
            $result_user->ID_PENGGUNA = $pengguna['ID_PENGGUNA'];
            $result_user->NAMA_PENGGUNA = $pengguna['NM_PENGGUNA'];
            $result_user->USERNAME = $pengguna['USERNAME'];
            $result_user->EMAIL_PENGGUNA = $pengguna['EMAIL_ALTERNATE'];
            $result_user->LAST_TIME_PASSWORD = $pengguna['LAST_TIME_PASSWORD'];
            $result_user->PASSWORD_HASH  = $pengguna['PASSWORD_HASH'];
        }

        if (strlen(trim($_POST['password_new'])) == 0 || strlen(trim($_POST['password_new2'])) == 0) {
            $error_message = "Isian password baru tidak lengkap.";
            $error = true;
        } elseif ($_POST['password_new'] != $_POST['password_new2']) {
            $error_message = "Isian password baru harus sama.";
            $error = true;
        } else {
            if ($post_password_request == '1') {
                $cp_result = $user->ChangePasswordKhususByID($id_pengguna, $username, $_POST['password_old'], $_POST['password_new']);
            } else {
                $cp_result = $user->ChangePasswordKhusus($_POST['password_old'], $_POST['password_new']);
            }
            if ($cp_result == User::CP_WRONG_PASSWORD) {
                $error_message = "Password lama salah.";
                $error = true;
            } elseif ($cp_result == User::CP_SAME_USERNAME) {
                $error_message = "Password baru tidak boleh sama dengan username.";
                $error = true;
            } elseif ($cp_result == User::CP_SAME_OLD_PASSWORD) {
                $error_message = "Password baru tidak boleh sama dengan password lama.";
                $error = true;
            } elseif ($cp_result == User::CP_WRONG_RULES) {
                $error_message = "Syarat password baru adalah 8 karakter dan harus terdapat angka, huruf, dan spesial karakter.";
                $error = true;
            } elseif ($cp_result == User::CP_FAILED) {
                $error_message = "Gagal mengganti password.";
                $error = true;
            } elseif ($cp_result == User::CP_SUCCESS) {
                $success = true;
            }
        }
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (isset($_GET['mode'])) {
        // Jika belum pernah set password
        if ($_GET['mode'] == 'ltp-null') {
            $result_user = $user;
            $html_output = 'change-password';
        } elseif ($_GET['mode'] == 'generate-password') {
            $html_output = 'change-password';
            $password_request = 1;
            $id_pengguna =  $crypto->decrypt(get('key'));
            $db->Query("SELECT * FROM PENGGUNA WHERE ID_PENGGUNA='{$id_pengguna}'");
            $pengguna = $db->FetchAssoc();

            $result_user =  new stdClass();
            $result_user->ID_PENGGUNA = $pengguna['ID_PENGGUNA'];
            $result_user->NAMA_PENGGUNA = $pengguna['NM_PENGGUNA'];
            $result_user->USERNAME = $pengguna['USERNAME'];
            $result_user->EMAIL_PENGGUNA = $pengguna['EMAIL_ALTERNATE'];
            $result_user->LAST_TIME_PASSWORD = $pengguna['LAST_TIME_PASSWORD'];
            $result_user->PASSWORD_HASH  = $pengguna['PASSWORD_HASH'];
        }
    } else {
        header("Location: /");
        exit();
    }
}
$smarty->assign('success', $success);
$smarty->assign('error', $error);
$smarty->assign('error_message', $error_message);
$smarty->assign('password_request', $password_request);
$smarty->assign('user', $result_user);
$smarty->display('gui/' . $html_output . '.tpl');
