<?php
require '../config.php';

$param = $_GET;
#print_r($param);

$table_request = $param['table'];
/**
$id_pt = $param['id'];
$format = $_GET['format'];

#get all parameter query
@$id_semester = $param['id_semester'];
@$id_prodi = $param['id_prodi'];
@$limit = $param['limit'];
@$id_kurikulum_mk = $param['id_kurikulum_mk'];
*/

#get master semester
function getsemester(){
	return $sql = "select * from semester where id_perguruan_tinggi = '".$GLOBALS['param']['id']."'";
}

#get master kelas_kuliah
function getkelaskuliah(){
	$sql = "select kls_mk.id_kelas_mk,kls_mk.id_program_studi,kls_mk.id_semester,kmk.id_mata_kuliah,nm_kls.nama_kelas,kmk.kredit_semester,
			kmk.kredit_tatap_muka,kmk.kredit_praktikum,smt.thn_akademik_semester,smt.group_semester,ps.id_sms_feeder,mk.kd_mata_kuliah
			from kelas_mk kls_mk 
			join nama_kelas nm_kls on nm_kls.id_nama_kelas = kls_mk.no_kelas_mk
			join kurikulum_mk kmk on kmk.id_kurikulum_mk = kls_mk.id_kurikulum_mk
			join mata_kuliah mk on mk.id_mata_kuliah = kmk.id_mata_kuliah
			join semester smt on smt.id_semester = kls_mk.id_semester
			join program_studi ps on ps.id_program_studi = kls_mk.id_program_studi
			join fakultas fak on fak.id_fakultas = ps.id_program_studi
			where fak.id_perguruan_tinggi = '".$GLOBALS['param']['id_pt']."'";

	if(!empty($GLOBALS['param']['id_semester'])) $sql .= " and kls_mk.id_semester = '".$GLOBALS['param']['id_semester']."'";
	#if(!empty($id_prodi)) $sql .= " and kls_mk.id_program_studi = '".$id_prodi."'";
	if(!empty($GLOBALS['param']['limit'])) $sql .= " and rownum <= ".$GLOBALS['param']['limit'];

	return $sql;
}

# get master mata kuliah 
function getmatakuliah(){
	$sql = "select mk.kd_mata_kuliah,mk.nm_mata_kuliah,ps.id_sms_feeder,kmk.kredit_semester,kmk.kredit_tatap_muka,kmk.kredit_praktikum,ps.id_jenjang
			from mata_kuliah mk 
			join kurikulum_mk kmk on kmk.id_mata_kuliah = mk.id_mata_kuliah
			join program_studi ps on ps.id_program_studi =  mk.id_program_studi
			join fakultas fak on fak.id_fakultas = ps.id_fakultas
			where fak.id_perguruan_tinggi = '".$GLOBALS['param']['id']."' and ROWNUM <= 2";

			#if(!empty($GLOBALS['param']['id_prodi']) && is_numeric($GLOBALS['param']['id_prodi'])) $sql .= " and mk.id_program_studi = '".$id_prodi."'";

	return $sql;
}

#get master kurikulum
function getkurikulum(){
	$sql = "select ps.id_sms_feeder,ps.id_jenjang,kur_prodi.id_kurikulum_prodi,nm_kurikulum,tahun_kurikulum,jml_sks_wajib,jml_sks_pilihan,group_semester 
			from kurikulum_prodi kur_prodi
			join kurikulum kur on kur.id_kurikulum = kur_prodi.id_kurikulum 
			join program_studi ps on ps.id_program_studi = kur_prodi.id_program_studi
			join fakultas fak on fak.id_fakultas  = ps.id_fakultas
			join semester s on s.id_semester = kur_prodi.id_semester
			where fak.id_perguruan_tinggi = '".$GLOBALS['param']['id']."'";

			if(!empty($GLOBALS['param']['id_prodi']) && is_numeric($GLOBALS['param']['id_prodi'])) $sql .= " and kur_prodi.id_program_studi = '".$id_prodi."'";

	return $sql;
}

#get list mata kuliah kurikulum
function getkurikulummk(){
	
	$sql = "select  kmk.id_kurikulum_prodi as id_kurikulum_prodi,mk.id_mata_kuliah as id_mk,ps.id_sms_feeder as id_sms,kur_prodi.thn_kurikulum as thn, 
			case when mod(kmk.tingkat_semester,2) = 0 then 2 else 1 end smt,
			kmk.kredit_semester as sks_mk,kmk.kredit_tatap_muka as sks_tm,kmk.kredit_praktikum as sks_praktek
			from kurikulum_prodi kur_prodi 
			join kurikulum kur on kur.id_kurikulum = kur_prodi.id_kurikulum 
			join kurikulum_mk kmk on kmk.id_kurikulum_prodi = kur_prodi.id_kurikulum_prodi
			join mata_kuliah mk on mk.id_mata_kuliah = kmk.id_mata_kuliah
			join program_studi ps on ps.id_program_studi = kur_prodi.id_program_studi
			join fakultas fak on fak.id_fakultas = ps.id_fakultas
			where fak.id_perguruan_tinggi = '".$GLOBALS['param']['id']."' and rownum <= 10
			";

			if(!empty($GLOBALS['param']['id_prodi']) && is_numeric($GLOBALS['param']['id_prodi'])) $sql .= " and kur_prodi.id_program_studi = '".$GLOBALS['param']['id_prodi']."'";
			if(!empty($GLOBALS['param']['id_kurikulum_mk']) && is_numeric($GLOBALS['param']['id_kurikulum_mk'])) $sql .= " and kur_prodi.id_kurikulum = '".$GLOBALS['param']['id_kurikulum_mk']."'";
	
	return $sql;

}

#get master mahasiswa
function getmahasiswa(){
	$sql = "select * from mahasiswa mhs join pengguna peng on peng.id_pengguna = mhs.id_pengguna
			where peng.id_perguruan_tinggi = '".$GLOBALS['param']['id']."' and peng.join_table = 3";
	return $sql;
}

function buildquery($sql){
	global $db;
	return $db->QueryToArray($sql);
	#return $sql;
}


#### tambahaan untuk parameter query ####
function get_program_studi(){
	$sql = "select ps.id_program_studi,jenjang.nm_jenjang||' '||ps.nm_program_studi as nm_program_studi from program_studi ps 
	join jenjang on jenjang.id_jenjang = ps.id_jenjang
	join fakultas fak on ps.id_fakultas = fak.id_fakultas
			where fak.id_perguruan_tinggi = '".$GLOBALS['param']['id']."'";
	return $sql;
}

if($table_request == 'mata_kuliah') $hasil = buildquery(getmatakuliah());
if($table_request == 'kurikulum') $hasil = buildquery(getkurikulum());
if($table_request == 'kurikulum_prodi') $hasil = buildquery(getkurikulumprodi());
if($table_request == 'kurikulum_mk') $hasil = buildquery(getkurikulummk());
if($table_request == 'mahasiswa') $hasil = buildquery(getmahasiswa());
if($table_request == 'kelas_kuliah') $hasil = buildquery(getkelaskuliah());

$output = json_encode($hasil,true);

if(isset($param['req_khusus']) && $param['req_khusus'] == 'req_khusus') $output = json_encode(buildquery(get_program_studi()), true);

echo $output;

?>