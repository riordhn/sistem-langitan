<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Sistem Langitan - {$nama_pt}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="icon" sizes="57" href="icon-langitan-57.png" />
	<link rel="apple-touch-icon" href="icon-langitan-57.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="icon-langitan-72.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="icon-langitan-114.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="icon-langitan-144.png" />

	<link href="../css/bootstrap-3.2.min.css" rel="stylesheet" id="bootstrap-css">

	<style type="text/css">
	body {
		background: url('../langitan/Bg-Umaha.jpg');
		background-color: #444;
	}
	.panel-default > .panel-heading {
	    color: #333;
	    background-color: #094231;
	    border-color: #DDD;
	}
	.form-signin input[type="text"] {
		margin-bottom: 5px;
		border-bottom-left-radius: 0;
		border-bottom-right-radius: 0;
	}
	.form-signin input[type="password"] {
		margin-bottom: 10px;
		border-top-left-radius: 0;
		border-top-right-radius: 0;
	}
	.form-signin .form-control {
		position: relative;
		font-size: 16px;
		font-family: 'Open Sans', Arial, Helvetica, sans-serif;
		height: auto;
		padding: 10px;
		-webkit-box-sizing: border-box;
		-moz-box-sizing: border-box;
		box-sizing: border-box;
	}
	.vertical-offset-100 {
		padding-top: 100px;
	}
	.img-responsive {
	display: block;
	max-width: 100%;
	height: auto;
	margin: auto;
	}
	.panel {
	margin-bottom: 20px;
	background-color: rgba(255, 255, 255, 0.75);
	border: 1px solid transparent;
	border-radius: 4px;
	-webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
	box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
	}
	</style>
	
</head>
<body>	

	<div class="container">
		<div class="row vertical-offset-100">
			<img src="../langitan/logo-langitan-{$nama_singkat}.PNG" class="img-responsive" alt="Sistem Langitan Nahdlatul Ulama"/>

			<!-- <h3><center>TES hahahha hihihih huhuhuhu hahaha</center></h3> -->
			<!-- <br/>
			<br/> -->
			<div class="col-md-4 col-md-offset-4">


				<div class="panel panel-default">
					<!--<div class="panel-heading">                                
						<div class="row-fluid user-row">
							<img src="langitan/logo-langitan-{$nama_singkat}.PNG" class="img-responsive" alt="Sistem Langitan Nahdlatul Ulama"/>
						</div>
					</div>
					-->
					<div class="panel-body">

						<h4>Halaman Auto-Login Khusus Rektor UMAHA</h4>

						<form accept-charset="UTF-8" role="form" class="form-signin" method="post" action="../login.php">
							<input name="mode" type="hidden" value="login">
							<input name="mode-login" type="hidden" value="auto-login">
							<fieldset>
								<label class="panel-login">
									<div class="login_result"></div>
								</label>
								<input type="hidden" name="username" value="0711025901" />
								<input type="hidden" name="password" value="uangnyatersedia"/>
								<br />
								<input class="btn btn-lg btn-success btn-block" type="submit" id="dosen" value="Modul Dosen" />
							</fieldset>
						</form>
						<form accept-charset="UTF-8" role="form" class="form-signin" method="post" action="../login.php">
							<input name="mode" type="hidden" value="login">
							<input name="mode-login" type="hidden" value="auto-login">
							<fieldset>
								<label class="panel-login">
									<div class="login_result"></div>
								</label>
								<input type="hidden" name="username" value="1501001" />
								<input type="hidden" name="password" value="uangnyatersedia"/>
								<br />
								<input class="btn btn-lg btn-success btn-block" type="submit" id="pendidikan" value="Modul Pendidikan" />
							</fieldset>
						</form>
						<form accept-charset="UTF-8" role="form" class="form-signin" method="post" action="../login.php">
							<input name="mode" type="hidden" value="login">
							<input name="mode-login" type="hidden" value="auto-login">
							<fieldset>
								<label class="panel-login">
									<div class="login_result"></div>
								</label>
								<input type="hidden" name="username" value="1501003" />
								<input type="hidden" name="password" value="uangnyatersedia"/>
								<br />
								<input class="btn btn-lg btn-success btn-block" type="submit" id="sumberdaya" value="Modul Sumberdaya" />
							</fieldset>
						</form>
						<form accept-charset="UTF-8" role="form" class="form-signin" method="post" action="../login.php">
							<input name="mode" type="hidden" value="login">
							<input name="mode-login" type="hidden" value="auto-login">
							<fieldset>
								<label class="panel-login">
									<div class="login_result"></div>
								</label>
								<input type="hidden" name="username" value="1209039" />
								<input type="hidden" name="password" value="uangnyatersedia"/>
								<br />
								<input class="btn btn-lg btn-success btn-block" type="submit" id="akademik" value="Modul Akademik" />
							</fieldset>
						</form>
						<form accept-charset="UTF-8" role="form" class="form-signin" method="post" action="../login.php">
							<input name="mode" type="hidden" value="login">
							<input name="mode-login" type="hidden" value="auto-login">
							<fieldset>
								<label class="panel-login">
									<div class="login_result"></div>
								</label>
								<input type="hidden" name="username" value="1503001" />
								<input type="hidden" name="password" value="uangnyatersedia"/>
								<br />
								<input class="btn btn-lg btn-success btn-block" type="submit" id="keuangan" value="Modul Keuangan" />
							</fieldset>
						</form>
						<form accept-charset="UTF-8" role="form" class="form-signin" method="post" action="../login.php">
							<input name="mode" type="hidden" value="login">
							<input name="mode-login" type="hidden" value="auto-login">
							<fieldset>
								<label class="panel-login">
									<div class="login_result"></div>
								</label>
								<input type="hidden" name="username" value="143213133" />
								<input type="hidden" name="password" value="um@ha"/>
								<br />
								<input class="btn btn-lg btn-success btn-block" type="submit" id="mahasiswa" value="Modul Mahasiswa" />
							</fieldset>
						</form>

						
					</div>
				</div>
			</div>
		</div>
	</div>

<!-- </body></html> Prevent Telkom Injection Script -->
</body>
</html>