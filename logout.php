<?php
include('config.php');
$user->Logout();

// khusus auto-login UMAHA
// FIKRIE
// 05-10-2016
if ($_SESSION['auto-login'] == 'yes') {
    $_SESSION['auto-login'] = null;

    header('Location: /auto-login');
} else { // tanpa auto-login
    header('Location: /');
}
exit();
