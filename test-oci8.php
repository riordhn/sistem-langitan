<?php
require './vendor/autoload.php';

// Load .env configuration
$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

$db_host			= getenv('DB_HOST');
$db_port			= getenv('DB_PORT');
$db_service_name	= getenv('DB_SERVICE_NAME');
$db_server_type		= getenv('DB_SERVER_TYPE');
$db_instance_name	= getenv('DB_INSTANCE_NAME');
$db_charset			= getenv('DB_CHARSET');
$db_user			= getenv('DB_USER');
$db_pass			= getenv('DB_PASS');

// Setting TNS
// Syntax : [//]host_name[:port][/service_name][:server_type][/instance_name]
$tns = $db_host .
	($db_port ? ':' . $db_port : '') .
	($db_service_name ? '/' . $db_service_name : '') .
	($db_server_type ? ':' . $db_server_type : '') .
	($db_instance_name ? '/' . $db_instance_name : '');

//==============================================================================
$time_start = microtime(true); 
$conn = oci_connect($db_user, $db_pass, $tns, $db_charset);

if (!$conn) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}

echo "Server Version: " . oci_server_version($conn);
oci_close($conn);
$time_end = microtime(true);
//==============================================================================
$execution_time = ($time_end - $time_start);

echo "<br/><br/>".$execution_time." detik";
