<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>REKAPITULASI</title>
{literal}
<style type="text/css">
<!--
@import url("style.css");

.green { color: #080; }
.red { color: #f00; }
-->
</style>
{/literal}
</head>
<body>
<div id="header">
	<div id="judul_header"></div>
</div>

<h1 style="text-align:center;color:#0e2d50;font-family:'Trebuchet MS'">REKAPITULASI</h1>
<div id="content">

<table id="rounded-corner" summary="{$f.NM_FAKULTAS}">
	<tr>
		<td><a href="mhs-aktif">Mahasiswa Aktif</a></td>
	</tr>
    <tr>
		<td><a href="mhs-aktif/mahasiswa-aktif.php">Mahasiswa Aktif (Jenis Kelamin)</a></td>
	</tr>
    <tr>
		<td><a href="mhs-aktif/jml-mhs.php">Mahasiswa Aktif (Per Jenjang)</a></td>
	</tr>
    <tr>
		<td><a href="sekolah">Mahasiswa Baru (Berdasarkan Sekolah)</a></td>
	</tr>
	<tr>
		<td><a href="prodi">Prodi (Spesifikasi, Daya Tampung, Biaya)</a></td>
	</tr>
    <tr>
		<td><a href="penghasilan">Penghasilan Orang Tua</a></td>
	</tr>
	<tr>
		<td><a href="pendaftaran">Penerimaan</a></td>
	</tr>
    <tr>
		<td><a href="sekolah/sekolah-tahun.php">Jumlah Mahasiswa Tiap Sekolah</a></td>
	</tr>
        <tr>
		<td><a href="sekolah/snmptn/">Rasio Jumlah siswa pendaftar dan yang diterima (SNMPTN)</a></td>
	</tr>
        <tr>
		<td><a href="sekolah/mandiri/">Rasio Jumlah siswa pendaftar dan yang diterima (MANDIRI)</a></td>
	</tr>
        <tr>
		<td><a href="jalur-mahasiswa/">Jalur Mahasiswa</a></td>
	</tr>
</table>
<br/>
</div>
</body>
</html>