<?php
include('../../../config.php');

$mode = get('mode', 'view');
$smarty->assign('mode', "{$mode}.tpl");

$id_fakultas = get('id_fakultas', '');
$tahun = get('tahun', 2011);

$smarty->assign('fakultas_set', $db->QueryToArray("select * from fakultas"));

if ($mode == 'view' && $id_fakultas != '')
{
    $smarty->assign('data_set', $db->QueryToArray("
        select pp.id_program_studi, pp.id_penerimaan, p.tahun, j.nm_jalur, p.nm_penerimaan, p.semester, p.gelombang, substr(jg.nm_jenjang,1,2) nm_jenjang, ps.nm_program_studi,
          s.nilai_min, s.nilai_max, s.nilai_rata, s.standar_deviasi, (s.nilai_rata - s.standar_deviasi) passing_grade, s.kuota,
          (select count(id_c_mhs) from calon_mahasiswa_baru cmb where cmb.id_penerimaan = pp.id_penerimaan and cmb.id_pilihan_1 = pp.id_program_studi) peminat,
          (select round(avg(total_nilai), 2) from calon_mahasiswa_baru cmb left join nilai_cmhs_pasca n on n.id_c_mhs = cmb.id_c_mhs
           where cmb.id_penerimaan = pp.id_penerimaan and cmb.id_program_studi = pp.id_program_studi) rata_diterima
        from penerimaan_prodi pp
        join penerimaan p on p.id_penerimaan = pp.id_penerimaan
        join jalur j on j.id_jalur = p.id_jalur
        join program_studi ps on ps.id_program_studi = pp.id_program_studi
        join jenjang jg on jg.id_jenjang = ps.id_jenjang
        left join statistik_cmhs s on (s.id_penerimaan = pp.id_penerimaan and s.id_program_studi = pp.id_program_studi)
        where pp.is_aktif = 1 and ps.id_fakultas = {$id_fakultas} and p.tahun = {$tahun}
        order by ps.id_jenjang, ps.nm_program_studi, p.tahun desc, p.semester desc, p.id_jalur, p.gelombang"));
}

$smarty->display('index.tpl');
?>
