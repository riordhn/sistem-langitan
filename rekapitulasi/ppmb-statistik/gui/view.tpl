<div class="center">
    <form action="" method="get" id="filter">
        Fakultas :
		<select name="id_fakultas" onchange="document.getElementById('filter').submit();">
            <option value="">--</option>
            {foreach $fakultas_set as $f}<option value="{$f.ID_FAKULTAS}" {if $smarty.get.id_fakultas == $f.ID_FAKULTAS}selected="selected"{/if}>{$f.NM_FAKULTAS}</option>{/foreach}
        </select>
		Tahun :
		<select name="tahun" onchange="document.getElementById('filter').submit();">
			<option value="">--</option>
			<option value="2011" {if $smarty.get.tahun == '2011'}selected="selected"{/if}>2011</option>
			<option value="2012" {if $smarty.get.tahun == '2012'}selected="selected"{/if}>2012</option>
		</select>
    </form>
</div>
        
{if !empty($data_set)}
    
<div>
    <table style="margin: 5px auto;">
        <tr>
            <th rowspan="2" style="vertical-align: middle">No</th>
            <th rowspan="2" style="vertical-align: middle" class="center">Program Studi</th>
            <th rowspan="2" style="vertical-align: middle" class="center">Penerimaan</th>
            <th colspan="5" style="vertical-align: middle" class="center">Statistik</th>
            <th rowspan="2" style="vertical-align: middle" class="center">Rata<br/>Diterima</th>
        </tr>
        <tr>
            <th class="center">Min</th>
            <th class="center">Max</th>
            <th class="center">Rata</th>
            <th title="Standar Deviasi" class="center">SD</th>
            <th title="Passing Grade" class="center">PG</th>
        </tr>
        {foreach $data_set as $d}
        <tr {if $d@index is div by 2}class="row1"{/if}>
            <td class="center">{$d@index + 1}</td>
            <td>{$d.NM_JENJANG} {$d.NM_PROGRAM_STUDI}</td>
            <td>{$d.NM_PENERIMAAN} {if $d.SEMESTER != '-'}Semester {$d.SEMESTER}{/if} Gelombang {$d.GELOMBANG}</td>
            <td class="center">{number_format($d.NILAI_MIN, 2)}</td>
            <td class="center">{number_format($d.NILAI_MAX, 2)}</td>
            <td class="center">{number_format($d.NILAI_RATA, 2)}</td>
            <td class="center">{number_format($d.STANDAR_DEVIASI, 2)}</td>
            <td class="center">
                {if $d.NM_JENJANG == 'S1' || $d.NM_JENJANG == 'D3'}
                    {if $d.PEMINAT >= $d.KUOTA}
                        {number_format($d.NILAI_RATA, 2)}
                    {else}
                        {number_format($d.PASSING_GRADE, 2)}
                    {/if}
                {else}
                    {number_format($d.PASSING_GRADE, 2)}
                {/if}
            </td>
            <td class="center">{number_format($d.RATA_DITERIMA, 2)}</td>
        </tr>
        {/foreach}
    </table>
</div>
{/if}