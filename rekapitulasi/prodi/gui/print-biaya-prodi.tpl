<html>
    <body>
        <table border="1">
            <thead>
                <tr>
                    <th rowspan="2">Prodi</th>
                    <th colspan="3">Sekarang</th>
                    <th colspan="3">Usulan</th>
                </tr>
                <tr>
                    <th>SOP</th>
                    <th>SP3</th>
                    <th>Matrikulasi</th>
                    <th>SOP</th>
                    <th>SP3</th>
                    <th>Matrikulasi</th>
                </tr>
            </thead>
            <tbody>
                {foreach $pb_set as $pb}
                <tr>
                    <td>{$pb.NM_PROGRAM_STUDI} {if $pb.IS_ALIH_JENIS}[Alih Jenis]{/if}</td>
                    <td style="text-align: right">{$pb.SOP}</td>
                    <td style="text-align: right">{$pb.SP3}</td>
                    <td style="text-align: right">{$pb.MATRIKULASI}</td>
                    <td style="text-align: right">{$pb.SOP_USULAN}</td>
                    <td style="text-align: right">{$pb.SP3_USULAN}</td>
                    <td style="text-align: right">{$pb.MATRIKULASI_USULAN}</td>
                </tr>
                {/foreach}
            </tbody>
        </table>
            <p style="font-style: italic">Sumber: Cyber Campus Universitas Airlangga 2011</p>
    </body>
</html>