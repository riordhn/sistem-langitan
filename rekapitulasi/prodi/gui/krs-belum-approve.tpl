<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informasi Spek Prodi</title>
{literal}
<style type="text/css">
<!--
@import url("style.css");

.green { color: #080; }
.red { color: #f00; }
-->
</style>
{/literal}
</head>
<body>
<div id="header">
<div id="judul_header"></div>
</div>
<h1 style="text-align:center;color:#0e2d50;font-family:'Trebuchet MS'">INFORMASI KRS BELUM APPROVE</h1>
<div id="content" style="width:84%">
{foreach $data_krs as $data}
<h2 style="font-family:'Trebuchet MS'" id="{$data.ID_FAKULTAS}">Fakultas {$data.NM_FAKULTAS}</h2>
<table id="rounded-corner" style="width:1100px" summary="{$data.NM_FAKULTAS}">
    <thead>
    	<tr>
			
            <th scope="col" class="rounded-company" style="width: 30px">NO</th>
            <th scope="col" class="rounded-q1" style="text-align: center" >NIP DOSEN</th>
            <th scope="col" class="rounded-q1" style="text-align: center" >NAMA DOSEN</th>
            <th scope="col" class="rounded-q2" style="text-align: center">NIM</th>
            <th scope="col" class="rounded-q3" style="text-align: center">NAMA MAHASISWA</th>
            <th scope="col" class="rounded-q3" style="text-align: center">ANGKATAN</th>
			<th scope="col" class="rounded-q3" style="text-align: center">STATUS KRS</th>
			<th scope="col" class="rounded-q4" style="text-align: center">STATUS APPROVE</th>
        </tr>
    </thead>
    <tfoot>
    	<tr>
            <td colspan="7" class="rounded-foot-left"><em>&nbsp;</em></td>
            <td class="rounded-foot-right">&nbsp;</td>
        </tr>
    </tfoot>
    <tbody>
	{$index=1}
        {foreach $data.DATA_KRS as $p}		
		{if $p.WALI_KE==1}
			<tr>
				<td>{$index++}</td>
				<td>{$p.NIP_DOSEN}</td>
				<td style="text-align: left">
					{$p.NM_DOSEN}
				</td>
				<td style="text-align: center">
					{$p.NIM_MHS}
					</td>
				<td style="text-align: center">
					{$p.NM_MHS}
				</td>
				<td style="text-align: center">
					{$p.THN_ANGKATAN_MHS}
				</td>
				<td style="text-align: center">
					{if $p.STATUS_PENGAMBILAN>1}
						KPRS
					{else if $p.STATUS_PENGAMBILAN<=1}
						KRS
					{/if}
				</td>
				<td style="text-align: center">
					{if $p.STATUS_APPROVE==0}
						Belum Approve
					{else if $p.STATUS_APPROVE>0&&$p.STATUS_APPROVE<1}
						Belum Approve Semua
					{/if}
				</td>
			</tr>
		{/if}
        {/foreach}
    </tbody>
</table>
<br/>
{/foreach}
</div>

</body>
</html>