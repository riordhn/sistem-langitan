<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informasi Spek Prodi</title>
{literal}
<style type="text/css">
<!--
@import url("style.css");

.green { color: #080; }
.red { color: #f00; }
-->
</style>
{/literal}
</head>
<body>
<div id="header">
<div id="judul_header"></div>
</div>
<h1 style="text-align:center;color:#0e2d50;font-family:'Trebuchet MS'">INFORMASI PENILAIAN BELUM TERISI</h1>
<div id="content" style="width:84%">
{foreach $data_penilaian as $data}
<h2 style="font-family:'Trebuchet MS'" id="{$data.ID_FAKULTAS}">Fakultas {$data.NM_FAKULTAS}</h2>
<table id="rounded-corner" style="width:1100px" summary="{$data.NM_FAKULTAS}">
    <thead>
    	<tr>
            <th scope="col" class="rounded-company" style="width: 250px">PROGRAM STUDI</th>
            <th scope="col" class="rounded-q1" style="text-align: center" >NIP</th>
            <th scope="col" class="rounded-q2" style="text-align: center">NAMA DOSEN</th>
            <th scope="col" class="rounded-q3" style="text-align: center">TELP</th>
            <th scope="col" class="rounded-q3" style="width: 280px;text-align: center">MATA AJAR</th>
			<th scope="col" class="rounded-q4" style="text-align: center">JUMLAH MAHASISWA</th>
        </tr>
    </thead>
    <tfoot>
    	<tr>
            <td colspan="5" class="rounded-foot-left"><em>&nbsp;</em></td>
            <td class="rounded-foot-right">&nbsp;</td>
        </tr>
    </tfoot>
    <tbody>
        {foreach $data.DATA_PENILAIAN as $p}
    	<tr>
            <td>{$p.NM_PROGRAM_STUDI}</td>
            <td style="text-align: left">
				{$p.NIP_DOSEN}
			</td>
            <td style="text-align: center">
				{$p.NM_PENGGUNA}
				</td>
			<td style="text-align: center">
				{$p.TLP_DOSEN}
			</td>
            <td style="text-align: center">
				{$p.MATA_AJAR}
			</td>
			<td style="text-align: center">
				{$p.JUMLAH_MHS}
			</td>
        </tr>
        {/foreach}
    </tbody>
</table>
<br/>
{/foreach}
</div>

</body>
</html>