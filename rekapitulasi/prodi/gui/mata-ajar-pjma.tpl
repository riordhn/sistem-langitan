<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informasi Spek Prodi</title>
{literal}
<style type="text/css">
<!--
@import url("style.css");

.green { color: #080; }
.red { color: #f00; }
-->
</style>
{/literal}
</head>
<body>
<div id="header">
<div id="judul_header"></div>
</div>
<h1 style="text-align:center;color:#0e2d50;font-family:'Trebuchet MS'">INFORMASI MATA AJAR BELUM ADA PJMA</h1>
<div id="content" style="width:70%">
{foreach $data_penilaian as $data}
<h2 style="font-family:'Trebuchet MS'" id="{$data.ID_FAKULTAS}">Fakultas {$data.NM_FAKULTAS}</h2>
<table id="rounded-corner" style="width:900px" summary="{$data.NM_FAKULTAS}">
    <thead>
    	<tr>
            <th scope="col" class="rounded-company" style="width: 200px">PROGRAM STUDI</th>
            <th scope="col" class="rounded-q1" style="text-align: center" >KODE MATA AJAR</th>
            <th scope="col" class="rounded-q2" style="text-align: center">MATA AJAR</th>
            <th scope="col" class="rounded-q4" style="text-align: center">JUMLAH MHS</th>
        </tr>
    </thead>
    <tfoot>
    	<tr>
            <td colspan="3" class="rounded-foot-left"><em>&nbsp;</em></td>
            <td class="rounded-foot-right">&nbsp;</td>
        </tr>
    </tfoot>
    <tbody>
        {foreach $data.DATA_PENILAIAN as $p}
    	<tr>
            <td>{$p.PRODI}</td>
            <td style="text-align: center">
				{$p.KD_MATA_KULIAH}
			</td>
            <td style="text-align: center">
				{$p.NM_MATA_KULIAH}
				</td>
			<td style="text-align: center">
				{$p.JUMLAH_MHS}
			</td>
        </tr>
        {/foreach}
    </tbody>
</table>
<br/>
{/foreach}
</div>

</body>
</html>