<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informasi Spek Prodi</title>
{literal}
<style type="text/css">
<!--
@import url("style.css");

.green { color: #080; }
.red { color: #f00; }
-->
</style>
{/literal}
</head>
<body>
<div id="header">
<div id="judul_header"></div>
</div>
<div id="content">
<a class="button" href="./#{$ps.ID_FAKULTAS}">Kembali ke dashboard</a>
<h2 style="font-family:'Trebuchet MS'">Program Studi {$ps.NM_JENJANG} - {$ps.NM_PROGRAM_STUDI} {if $ps.IS_ALIH_JENIS==1} [Alih Jenis] {/if}</h2>

<table id="hor-minimalist-b" summary="Employee Pay Sheet">
    <thead>
    	<tr>
            <th scope="col" style="width: 250px">Nama Spesifikasi</th>
            <th scope="col">Spesifikasi Program Studi</th>
        </tr>
    </thead>
    <tbody>
    	<tr>
            <td><strong>Nama Program Studi</strong></td>
            <td>{$ps.NM_PROGRAM_STUDI}</td>
        </tr>
        <tr>
            <td><strong>Nama Prodi Baru</strong></td>
            <td>{$ps.PRODI_NAMA}</td>
        </tr>
        <tr>
            <td><strong>Jenis Prodi</strong></td>
            <td>{$ps.PRODI_JENIS}</td>
        </tr>
        <tr>
            <td><strong>Kategori Prodi</strong></td>
            <td>{$ps.PRODI_KATEGORI}</td>
        </tr>
        <tr>
            <td>Penjelasan Kategori Prodi</td>
            <td>{$ps.PRODI_PENJELASAN}</td>
        </tr>
        <tr>
            <td><strong>Berdiri Sejak Tahun</strong></td>
            <td>{$ps.PRODI_TAHUN}</td>
        </tr>
        <tr>
            <td>Ditetapkan dengan SK dari</td>
            <td>{$ps.PRODI_SK_OLEH}</td>
        </tr>
        <tr>
            <td>Nomor SK</td>
            <td>{$ps.PRODI_SK_NOMOR}</td>
        </tr>
        <tr>
            <td>Tanggal Terbit SK</td>
            <td>{$ps.PRODI_SK_TERBIT}</td>
        </tr>
        <tr>
            <td colspan="2"><strong>SK Izin Penyelenggaraan</strong></td>
        </tr>
        <tr>
            <td>Nomor SK</td>
            <td>{$ps.PRODI_SKP_NOMOR}</td>
        </tr>
        <tr>
            <td>Tanggal Terbit SK</td>
            <td>{$ps.PRODI_SKP_TERBIT}</td>
        </tr>
        <tr>
            <td>Masa Berlaku s.d.</td>
            <td>{$ps.PRODI_SKP_EXPIRED}</td>
        </tr>
        <tr>
            <td><strong>Nama Ketua Prodi</strong></td>
            <td>{$ps.PRODI_KETUA_NAMA}</td>
        </tr>
        <tr>
            <td>Nomor SK Rektor</td>
            <td>{$ps.PRODI_KETUA_SK_NOMOR}</td>
        </tr>
        <tr>
            <td>Tanggal Terbit SK</td>
            <td>{$ps.PRODI_KETUA_SK_TERBIT}</td>
        </tr>
        <tr>
            <td>Masa jabatan s.d.</td>
            <td>{$ps.PRODI_KETUA_SK_EXPIRED}</td>
        </tr>
        <tr>
            <td><strong>Nama Sekretaris Prodi</strong></td>
            <td>{$ps.PRODI_SEKRE_NAMA}</td>
        </tr>
        <tr>
            <td>Nomor SK Rektor</td>
            <td>{$ps.PRODI_SEKRE_SK_NOMOR}</td>
        </tr>
        <tr>
            <td>Tanggal Terbit SK</td>
            <td>{$ps.PRODI_SEKRE_SK_TERBIT}</td>
        </tr>
        <tr>
            <td>Masa jabatan s.d.</td>
            <td>{$ps.PRODI_SEKRE_SK_EXPIRED}</td>
        </tr>
        <tr>
            <td colspan="2"><strong>Jumlah Dosen</strong></td>
        </tr>
        <tr>
            <td>Jumlah Dosen Tetap</td>
            <td>{$ps.PRODI_DOSEN_TETAP}</td>
        </tr>
        <tr>
            <td>Jumlah Dosen Tidak Tetap</td>
            <td>{$ps.PRODI_DOSEN_TDKTETAP}</td>
        </tr>
        <tr>
            <td>Jumlah Dosen Lulusan S2</td>
            <td>{$ps.PRODI_DOSEN_S2}</td>
        </tr>
        <tr>
            <td>Jumlah Dosen Lulusan S3</td>
            <td>{$ps.PRODI_DOSEN_S3}</td>
        </tr>
        <tr>
            <td>Jumlah Dosen Guru Besar</td>
            <td>{$ps.PRODI_DOSEN_GUBES}</td>
        </tr>
        <tr>
            <td><strong>Kurikulum Prodi</strong></td>
            <td>{$ps.PRODI_KUR_NAMA}</td>
        </tr>
        <tr>
            <td>Nomor SK Rektor</td>
            <td>{$ps.PRODI_KUR_SK_NOMOR}</td>
        </tr>
        <tr>
            <td>Tanggal terbit SK</td>
            <td>{$ps.PRODI_KUR_SK_TERBIT}</td>
        </tr>
        <tr>
            <td>Masa Berlaku SK</td>
            <td>{$ps.PRODI_KUR_SK_EXPIRED}</td>
        </tr>
        <tr>
            <td>Masa Studi</td>
            <td>{$ps.PRODI_KUR_MASA_STUDI}</td>
        </tr>
        <tr>
            <td>Beban Studi</td>
            <td>{$ps.PRODI_KUR_BEBAN_STUDI}</td>
        </tr>
        <tr>
            <td><strong>Learning Outcome Prodi</strong></td>
            <td>{$ps.PRODI_LO}</td>
        </tr>
        <tr>
            <td><strong>Gelar Lulusan</strong></td>
            <td>{$ps.PRODI_GELAR_NAMA}</td>
        </tr>
        <tr>
            <td>Singkatan Gelar</td>
            <td>{$ps.PRODI_GELAR_SINGKAT}</td>
        </tr>
        <tr>
            <td>Ditetapkan dengan SK Rektor</td>
            <td>{$ps.PRODI_GELAR_SK_NOMOR}</td>
        </tr>
        <tr>
            <td colspan="2"><strong>Jumlah Mahasiswa</strong></td>
        </tr>
        <tr>
            <td>Jumlah Peminat TA 2011</td>
            <td></td>
        </tr>
        <tr>
            <td>Calon Maba yang diterima TA 2011</td>
            <td>{$ps.PRODI_JML_MABA_CALON}</td>
        </tr>
        <tr>
            <td>Maba Mendaftar ulang TA 2011</td>
            <td>{$ps.PRODI_JML_MABA_DAFTAR}</td>
        </tr>
        <tr>
            <td>Mala masih terdaftar TA 2011</td>
            <td>{$ps.PRODI_JML_MHS_LAMA}</td>
        </tr>
        <tr>
            <td><strong>Jumlah Alumni saat ini</strong></td>
            <td>{$ps.PRODI_JML_ALUMNI}</td>
        </tr>
        <tr>
            <td><strong>Akreditasi</strong></td>
            <td>{$ps.PRODI_AKRED}</td>
        </tr>
        <tr>
            <td>Nomor SK</td>
            <td>{$ps.PRODI_AKRED_SK_NOMOR}</td>
        </tr>
        <tr>
            <td>Tanggal terbit SK</td>
            <td>{$ps.PRODI_AKRED_SK_TERBIT}</td>
        </tr>
        <tr>
            <td>Masa berlaku SK</td>
            <td>{$ps.PRODI_AKRED_SK_EXPIRED}</td>
        </tr>
        <tr>
            <td>Nilai</td>
            <td>{$ps.PRODI_AKRED_NILAI}</td>
        </tr>
        <tr>
            <td>Peringkat</td>
            <td>{$ps.PRODI_AKRED_PERINGKAT}</td>
        </tr>
    </tbody>
</table>
</div>
</body>
</html>