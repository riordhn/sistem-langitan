<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informasi Spek Prodi</title>
{literal}
<style type="text/css">
<!--
@import url("style.css");

.green { color: #080; }
.red { color: #f00; }
-->
</style>
{/literal}
</head>
<body>
<div id="header">
<div id="judul_header"></div>
</div>
<h1 style="text-align:center;color:#0e2d50;font-family:'Trebuchet MS'">INFORMASI SPESIFIKASI PROGRAM STUDI</h1>
<div id="content">
{foreach $fakultas_set as $f}
<h2 style="font-family:'Trebuchet MS'" id="{$f.ID_FAKULTAS}">Fakultas {$f.NM_FAKULTAS}</h2>
<table id="rounded-corner" summary="{$f.NM_FAKULTAS}">
    <thead>
    	<tr>
            <th scope="col" class="rounded-company" style="width: 415px">Prodi / Kelengkapan</th>
            <th scope="col" class="rounded-q1" style="text-align: center"><a href="jenjang.php">Spesifikasi Prodi</a></th>
            <th scope="col" class="rounded-q2" style="text-align: center"><a href="jenjang.php">Biaya</a></th>
            <th scope="col" class="rounded-q3" style="text-align: center"><a href="jenjang.php">Daya Tampung</a></th>
            <th scope="col" class="rounded-q4" style="text-align: center"><a href="jenjang.php">Syarat Prodi</a></th>
        </tr>
    </thead>
    <tfoot>
    	<tr>
            <td colspan="4" class="rounded-foot-left"><em>&nbsp;</em></td>
            <td class="rounded-foot-right">&nbsp;</td>
        </tr>
    </tfoot>
    <tbody>
        {foreach $f.program_studi_set as $ps}
    	<tr>
            <td>{$ps.NM_JENJANG} - {$ps.NM_PROGRAM_STUDI} &nbsp; {if $ps.IS_MANDIRI==1} <font style="font-weight:bold;color:orange">[ MANDIRI ] </font> {/if} {if $ps.IS_ALIH_JENIS==1} <font style="font-weight:bold;color:orange">[Alih Jenis] </font> {/if}</td>
            <td style="text-align: center">
				{if $ps.IS_MANDIRI==1} <font style="font-weight:bold;color:orange">NONE</font>
					{else}
						<a href="spek_prodi.php?ips={$ps.ID_PRODI_SPESIFIKASI}">
							{if $ps.PRODI_SPEK > 0}<font class="green">Sudah</font>{else}<font class="red">Belum</font>{/if}
						</a>
				{/if}
			</td>
            <td style="text-align: center">
				<a href="biaya_prodi.php?idb={$ps.ID_PRODI_BIAYA}"> 
				
					{if $ps.IS_MANDIRI==1}
						{if $ps.PRODI_BIAYA > 0}
							<font class="green">Sudah</font>
						{else}<font class="red">Belum</font>
						{/if} 
						{else}
							{if $ps.NM_JENJANG=='S1'}
									{if $ps.SOP_USULAN==0 || $ps.SP3A_USULAN==0 || $ps.SP3B_USULAN==0 || $ps.SP3C_USULAN==0 || $ps.SOP_USULAN=='' || $ps.SP3A_USULAN=='' || $ps.SP3B_USULAN==''|| $ps.SP3C_USULAN==0}
									<font class="red">Belum</font> 
									{else} <font class="green">Sudah</font>
									{/if} 
								{else}
									{if $ps.PRODI_BIAYA > 0}
									<font class="green">Sudah</font>
									{else}<font class="red">Belum</font>
							{/if}
						{/if}
					{/if}
					
				</a>
				</td>
			<td style="text-align: center">
				{if $ps.IS_MANDIRI==1} <font style="font-weight:bold;color:orange">NONE</font>
				{else}
					<a href="daya_tampung.php?ipd={$ps.ID_PRODI_DAYATAMPUNG}">
						{if $ps.DAYA_TAMPUNG > 0}<font class="green">Sudah</font>{else}<font class="red">Belum</font>{/if}
					</a>
				{/if}
			</td>
            <td style="text-align: center">
				{if $ps.IS_MANDIRI==1} <font style="font-weight:bold;color:orange">NONE</font> 
					{else}
						<a href="syarat_prodi.php?ipsy={$ps.ID_PRODI_SYARAT}">
							{if $ps.PRODI_SYARAT > 0}<font class="green">Sudah</font>{else}<font class="red">Belum</font>{/if}
						</a>
				{/if}
			</td>
        </tr>
        {/foreach}
    </tbody>
</table>
<br/>
{/foreach}
</div>

</body>
</html>