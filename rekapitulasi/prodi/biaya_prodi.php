<?php
include('../../../config.php');

$idb = get('idb');

$db->Query("select * from program_studi join jenjang on jenjang.id_jenjang = program_studi.id_jenjang where id_program_studi = {$id_program_studi}");
$row = $db->FetchAssoc();
$smarty->assign('ps', $row);

$db->Query("select pb.*,ps.id_fakultas,ps.id_program_studi,ps.nm_program_studi,j.nm_jenjang from prodi_biaya pb
			join program_studi ps on pb.id_program_studi=ps.id_program_studi
			join jenjang j on ps.id_jenjang = j.id_jenjang
			where pb.id_semester = 5 and pb.id_prodi_biaya = {$idb}");
$row = $db->FetchAssoc();

$smarty->assign('pb', $row);

$smarty->display('biaya_prodi.tpl');
?>
