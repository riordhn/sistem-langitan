<?php
include('../../../config.php');

$id_jenjang = get('id_jenjang');

$pb = $db->QueryToArray("
    select ps.nm_program_studi, pb.* from prodi_biaya pb
    join program_studi ps on (ps.id_program_studi = pb.id_program_studi and pb.id_semester = 5 and ps.id_jenjang = {$id_jenjang})
    where ps.id_program_studi <> 197
    order by ps.id_fakultas");
$smarty->assign('pb_set', $pb);

$smarty->display('view_biaya_prodi.tpl');
?>
