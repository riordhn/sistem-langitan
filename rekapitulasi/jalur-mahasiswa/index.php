<?php
include('../../config.php');

$mode = get('mode', 'view');
$tahun = get('tahun', 'all');

// Fikrie //
$id_pt = $id_pt_user;


if ($mode == 'view')
{
    if ($tahun == 'all')
        $tahun = '2009, 2010';

    $smarty->assign('program_studi_set', $db->QueryToArray("
        select ps.id_program_studi, j.nm_jenjang, ps.nm_program_studi from program_studi ps
        join jenjang j on j.id_jenjang = ps.id_jenjang
        join fakultas f on f.id_fakultas = ps.id_fakultas
        where (ps.id_jenjang = 1 or ps.id_jenjang = 5) and f.id_perguruan_tinggi = {$id_pt}
        order by ps.id_fakultas, ps.id_jenjang"));

    $smarty->assign('jalur_set', $db->QueryToArray("
        select * from jalur where id_perguruan_tinggi = {$id_pt} and is_deleted = '0'"));

    $smarty->assign('data_set', $db->QueryToArray("
        select bk.id_program_studi, bk.id_jalur, count(bkm.id_mhs) jumlah
        from biaya_kuliah_mhs bkm
        join biaya_kuliah bk on bk.id_biaya_kuliah = bkm.id_biaya_kuliah
        join mahasiswa m on (m.id_mhs = bkm.id_mhs and m.thn_angkatan_mhs in ({$tahun}))
        where bk.id_semester in (6, 22, 23, 24) and bk.id_jalur in (1, 2, 3, 4, 5, 20) and bk.id_jenjang in (1, 5)
        group by bk.id_program_studi, bk.id_jalur"));
}

if ($mode == 'prodi')
{
    $id_program_studi = get('id_program_studi');
    
    if ($tahun == 'all' or $tahun == '')
        $tahun = '2009, 2010';
    
    $program_studi = $db->QueryToArray("
        select ps.nm_program_studi, j.nm_jenjang from program_studi ps
        join jenjang j on j.id_jenjang = ps.id_jenjang
        where ps.id_program_studi = {$id_program_studi}");
    $smarty->assign('ps', $program_studi[0]);
    
    $smarty->assign('mahasiswa_set', $db->QueryToArray("
        select m.nim_mhs, p.nm_pengguna, j.nm_jalur, ms.ipk_mhs_status from mahasiswa m
        join pengguna p on p.id_pengguna = m.id_pengguna
        join biaya_kuliah_mhs bkm on bkm.id_mhs = m.id_mhs
        join biaya_kuliah bk on bk.id_biaya_kuliah = bkm.id_biaya_kuliah
        join jalur j on j.id_jalur = bk.id_jalur
        left join mhs_status_120106 ms on (ms.id_mhs = m.id_mhs and ms.id_semester = 23)
        where m.id_program_studi = {$id_program_studi} and m.thn_angkatan_mhs in ({$tahun})
        order by m.nim_mhs"));
}

$smarty->assign('mode', "{$mode}.tpl");

$smarty->display('index.tpl');
?>
