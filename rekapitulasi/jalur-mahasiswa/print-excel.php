<?php
include('../../config.php');

$id_program_studi = get('id_program_studi');
$tahun = get('tahun', '');
    
if ($tahun == 'all' or $tahun == '')
    $tahun = '2009, 2010';

$program_studi = $db->QueryToArray("
    select ps.nm_program_studi, j.nm_jenjang from program_studi ps
    join jenjang j on j.id_jenjang = ps.id_jenjang
    where ps.id_program_studi = {$id_program_studi}");
$smarty->assign('ps', $program_studi[0]);

$smarty->assign('mahasiswa_set', $db->QueryToArray("
    select m.nim_mhs, p.nm_pengguna, j.nm_jalur, ms.ipk_mhs_status from mahasiswa m
    join pengguna p on p.id_pengguna = m.id_pengguna
    join biaya_kuliah_mhs bkm on bkm.id_mhs = m.id_mhs
    join biaya_kuliah bk on bk.id_biaya_kuliah = bkm.id_biaya_kuliah
    join jalur j on j.id_jalur = bk.id_jalur
    left join mhs_status_120106 ms on (ms.id_mhs = m.id_mhs and ms.id_semester = 23)
    where m.id_program_studi = {$id_program_studi} and m.thn_angkatan_mhs in ({$tahun})
    order by m.nim_mhs"));
    
$filename = "JALUR MAHASISWA {$program_studi[0]['NM_PROGRAM_STUDI']}.xls";
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=$filename");
header("Pragma: ");
header("Cache-Control: ");
ob_flush();
    
$smarty->display('excel.tpl');
?>