<div class="center">
<form action="" method="get" id="filter">
    <select name="tahun" onchange="document.getElementById('filter').submit();">
        <option value="all" {if $smarty.get.tahun == 'all'}selected="selected"{/if}>Semua</option>
        <option value="2009" {if $smarty.get.tahun == 2009}selected="selected"{/if}>2009</option>
        <option value="2010" {if $smarty.get.tahun == 2010}selected="selected"{/if}>2010</option>
    </select>
</form>
</div>

{literal}<style type="text/css">
.content a { color: #000; text-decoration: none; }
.content a:hover { color: #000; text-decoration: underline; }
</style>
{/literal}

<table style="margin: 10px auto;">
    <tr>
        <th rowspan="2" class="center" style="vertical-align: middle">No</th>
        <th rowspan="2" class="center" style="vertical-align: middle">Program Studi</th>
        <th colspan="{count($jalur_set)}" class="center">Jalur</th>
    </tr>
    <tr>
        {foreach $jalur_set as $j}
        <th>{$j.NM_JALUR}</th>
        {/foreach}
    </tr>
    {foreach $program_studi_set as $ps}
    <tr {if $ps@index is div by 2}class="row1"{/if}>
        <td class="center">{$ps@index + 1}</td>
        <td><a href="?mode=prodi&id_program_studi={$ps.ID_PROGRAM_STUDI}&tahun={$smarty.get.tahun}">{$ps.NM_JENJANG} - {$ps.NM_PROGRAM_STUDI}<a/></td>
        {foreach $jalur_set as $j}
        <td class="center">
            {foreach $data_set as $d}
                {if $d.ID_PROGRAM_STUDI == $ps.ID_PROGRAM_STUDI and $d.ID_JALUR == $j.ID_JALUR}
                {$d.JUMLAH}
                {/if}
            {/foreach}
        </td>
        {/foreach}
    </tr>
    {/foreach}
</table>