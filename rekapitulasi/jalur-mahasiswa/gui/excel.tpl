<html>
<head></head>
<body>
<table border="1px">
    <tr>
        <th>No</th>
        <th>NIM</th>
        <th>Nama</th>
        <th>Jalur</th>
        <th>IPK</th>
    </tr>
    {foreach $mahasiswa_set as $m}
    <tr {if $m@index is div by 2}class="row1"{/if}>
        <td class="center">{$m@index + 1}</td>
        <td>{$m.NIM_MHS}</td>
        <td>{$m.NM_PENGGUNA}</td>
        <td>{$m.NM_JALUR}</td>
        <td>{number_format($m.IPK_MHS_STATUS,2)}</td>
    </tr>
    {/foreach}
</table>
</body>
</html>