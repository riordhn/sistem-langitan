<html>
<head>
    <title>Rekapitulasi Pusat Penerimaan Mahasiswa Baru</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="../../../css/reset.css">
    <link rel="stylesheet" type="text/css" href="../../../css/text.css">
    <link rel="stylesheet" type="text/css" href="../../../css/ppmb.css">
    <link rel="stylesheet" type="text/css" href="../../../css/jquery-ui-1.8.16.custom.ppmb-rekap.css">
    <link rel="shortcut icon" href="../../../img/icon.ico">
    <script type="text/javascript" src="../../../js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" src="../../../js/jquery-ui-1.8.11.custom.min.js"></script>
</head>
<body>
    <table class="clear-margin-bottom">
        <colgroup>
            <col>
            <col class="main-width">
            <col>
        </colgroup>
        <thead>
            <tr>
                <td class="header-left"></td>
                <td class="header-center" style="background-image: url('../../../../img/ppmb/header-center-rekapitulasi.png');"></td>
                <td class="header-right"></td>
            </tr>
            <tr>
                <td class="tab-left"></td>
                <td class="tab-center">
                </td>
                <td class="tab-right"></td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="body-left">&nbsp;</td>
                <td class="body-center">
                    <table class="content-table">
                        <colgroup>
                            <col>
                            <col>
                        </colgroup>
                        <tbody>
                            <tr>
                                <td id="content" class="content">
                                    <h1 style="text-align: center">REKAPITULASI PENDAFTARAN TAHUN 2012</h1>
                                    {include $mode}
                                </td>
                            </tr>
                        </tbody></table>
                </td>
                <td class="body-right">&nbsp;</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td class="foot-left">&nbsp;</td>
                <td class="foot-center">
                    <div class="footer-nav">
                        <br/>
                    </div>
                    <div class="footer">Copyright © 2011 - Universitas Airlangga <br>All Rights Reserved</div>
                </td>
                <td class="foot-right">&nbsp;</td>
            </tr>
        </tfoot>
    </table>

</body>
</html>