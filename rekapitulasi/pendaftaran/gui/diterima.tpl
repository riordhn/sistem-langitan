
<h2 class="center">{$prodi[0]['NM_JENJANG']} - {$prodi[0]['NM_PROGRAM_STUDI']}</h2>
<h4 class="center">Penerimaan {$penerimaan[0].NM_PENERIMAAN}
    {if $penerimaan[0].SEMESTER != '-'}Semester {$penerimaan[0].SEMESTER}{/if}
    Gelombang {$penerimaan[0].GELOMBANG} Tahun {$penerimaan[0].TAHUN}</h4>

<div class="center">
    <table style="margin: 5px auto;">
        <tr>
            <th>No</th>
            <th>No Ujian</th>
            <th>Nama Calon Mahasiswa</th>
            <th>Diterima</th>
            <th>Bayar</th>
            <th>Verifikasi</th>
            <th>Sidik Jari</th>
            <th>KTM</th>
        </tr>
        {foreach $cmb_set as $cmb}
        <tr {if $cmb@index is div by 2}class="row1"{/if}>
            <td class="center">{$cmb@index + 1}</td>
            <td>{$cmb.NO_UJIAN}</td>
            <td>{$cmb.NM_C_MHS}</td>
            <td class="center">{if $cmb.TGL_DITERIMA}{strftime('%d-%m-%Y', strtotime($cmb.TGL_DITERIMA))}{/if}</td>
            <td class="center">{if $cmb.TGL_BAYAR}{strftime('%d-%m-%Y', strtotime($cmb.TGL_BAYAR))}{else}-{/if}</td>
            <td class="center">{if $cmb.TGL_VERIFIKASI_PENDIDIKAN}{strftime('%d-%m-%Y', strtotime($cmb.TGL_VERIFIKASI_PENDIDIKAN))}{else}-{/if}</td>
            <td class="center">{if $cmb.FINGER > 0}OK{else}-{/if}</td>
            <td class="center">{if $cmb.TGL_CETAK_KTM}OK{else}-{/if}</td>
        </tr>
        {/foreach}
    </table>
    <a href="?id_fakultas={$prodi[0].ID_FAKULTAS}">Kembali</a>
</div>