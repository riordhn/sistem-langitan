<div class="center">
    <form action="" method="get" id="filter">
        <select name="id_fakultas" onchange="document.getElementById('filter').submit();">
            <option value="">--</option>
            {foreach $fakultas_set as $f}<option value="{$f.ID_FAKULTAS}" {if $smarty.get.id_fakultas == $f.ID_FAKULTAS}selected="selected"{/if}>{$f.NM_FAKULTAS}</option>{/foreach}
        </select>
    </form>
</div>

{if !empty($data_set)}
<div>
    <table style="margin: 5px auto;">
        <tr>
            <th rowspan="2" style="vertical-align: middle">No</th>
            <th rowspan="2" style="vertical-align: middle">Program Studi</th>
            <th rowspan="2" style="vertical-align: middle">Jenjang</th>
            <th rowspan="2" style="vertical-align: middle">Penerimaan</th>
            <th rowspan="2" style="vertical-align: middle">Diterima</th>
            <th colspan="4" style="vertical-align: middle" class="center">Daftar Ulang</th>
        </tr>
        <tr>
            <th>Bayar</th>
            <th title="Verifikasi">Verikasi</th>
            <th>Sidik Jari</th>
            <th title="Cetak KTM">KTM</th>
        </tr>
        {foreach $data_set as $d}
        <tr {if $d@index is div by 2}class="row1"{/if}>
            <td class="center">{$d@index + 1}</td>
            <td>{$d.NM_PROGRAM_STUDI}</td>
            <td class="center">{$d.NM_JENJANG}</td>
            <td>{$d.NM_PENERIMAAN} {if $d.SEMESTER != '-'}Semester {$d.SEMESTER}{/if} Gelombang {$d.GELOMBANG}</td>
            <td class="center"><a href="?mode=diterima&id_penerimaan={$d.ID_PENERIMAAN}&id_program_studi={$d.ID_PROGRAM_STUDI}" style="color: #000">{$d.DITERIMA}</a></td>
            <td class="center">{number_format($d.BAYAR,0)}</td>
            <td class="center">{number_format($d.VERIFIKASI,0)}</td>
            <td class="center">{number_format($d.FINGER,0)}</td>
            <td class="center">{$d.KTM}</td>
        </tr>
        {/foreach}
    </table>
</div>
{/if}