<?php
include('../../config.php');

$mode = get('mode', 'view');
$id_fakultas = get('id_fakultas', '');
$id_program_studi = get('id_program_studi', '');
$id_penerimaan = get('id_penerimaan', '');

// Fikrie //
$id_pt = $id_pt_user;

$smarty->assign('fakultas_set', $db->QueryToArray("select * from fakultas where id_perguruan_tinggi = {$id_pt}"));

if ($mode == 'view' and $id_fakultas != '')
{
    $smarty->assign('data_set', $db->QueryToArray("
        select pp.id_program_studi, pp.id_penerimaan, p.tahun, j.nm_jalur, p.nm_penerimaan, p.semester, p.gelombang, jg.nm_jenjang, ps.nm_program_studi,
          (select count(id_c_mhs) from calon_mahasiswa_baru cmb where cmb.id_penerimaan = p.id_penerimaan and cmb.id_program_studi = ps.id_program_studi) diterima,
          b.bayar, v.verifikasi, f.finger,
          (select count(id_c_mhs) from calon_mahasiswa_baru cmb where cmb.id_penerimaan = p.id_penerimaan and cmb.id_program_studi = ps.id_program_studi and cmb.tgl_cetak_ktm is not null) ktm
        from penerimaan_prodi pp
        join penerimaan p on p.id_penerimaan = pp.id_penerimaan
        join jalur j on j.id_jalur = p.id_jalur
        join program_studi ps on ps.id_program_studi = pp.id_program_studi
        join jenjang jg on jg.id_jenjang = ps.id_jenjang
        left join (
            select id_penerimaan, id_program_studi, count(id_c_mhs) bayar from calon_mahasiswa_baru
            where id_c_mhs in (select distinct id_c_mhs from pembayaran_cmhs where tgl_bayar is not null)
            group by id_penerimaan, id_program_studi) b on (b.id_penerimaan = pp.id_penerimaan and b.id_program_studi = pp.id_program_studi)
        left join (
            select id_penerimaan, id_program_studi, count(id_c_mhs) verifikasi from calon_mahasiswa_baru
            where tgl_verifikasi_pendidikan is not null
            group by id_penerimaan, id_program_studi) v on (v.id_penerimaan = pp.id_penerimaan and v.id_program_studi = pp.id_program_studi)
        left join (
            select id_penerimaan, id_program_studi, count(id_c_mhs) finger from calon_mahasiswa_baru
            where id_c_mhs in (select id_c_mhs from fingerprint_cmhs where finger_data is not null)
            group by id_penerimaan, id_program_studi) f on (f.id_penerimaan = pp.id_penerimaan and f.id_program_studi = pp.id_program_studi)
        where pp.is_aktif = 1 and ps.id_fakultas = {$id_fakultas} and p.tahun = 2012
        order by ps.id_jenjang, ps.nm_program_studi, p.tahun desc, p.semester desc, p.id_jalur, p.gelombang"));
}

if ($mode == 'diterima' and $id_penerimaan != '' and $id_program_studi != '')
{ 
    $smarty->assign('prodi', $db->QueryToArray("
        select nm_jenjang, nm_program_studi, id_fakultas from program_studi ps
        join jenjang j on j.id_jenjang = ps.id_jenjang
        where ps.id_program_studi = {$id_program_studi}"));
        
    $smarty->assign('penerimaan', $db->QueryToArray("select tahun, gelombang, nm_penerimaan, semester from penerimaan where id_penerimaan = {$id_penerimaan}"));
        
    $smarty->assign('cmb_set', $db->QueryToArray("
        select no_ujian, nim_mhs, nm_c_mhs, to_char(tgl_diterima, 'MM/DD/YYYY') tgl_diterima,
            (select max(tgl_bayar) from pembayaran_cmhs pc where pc.id_c_mhs = cmb.id_c_mhs and tgl_bayar is not null) tgl_bayar,
            to_char(tgl_verifikasi_pendidikan, 'MM/DD/YYYY') tgl_verifikasi_pendidikan,
            (select length(finger_data) from fingerprint_cmhs f where f.id_c_mhs = cmb.id_c_mhs) finger,
            tgl_cetak_ktm
        from calon_mahasiswa_baru cmb
        where id_penerimaan = {$id_penerimaan} and id_program_studi = {$id_program_studi}
        order by no_ujian"));
}


$smarty->assign('mode', $mode . ".tpl");
$smarty->display('index.tpl');
?>
