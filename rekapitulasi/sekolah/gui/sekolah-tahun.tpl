<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informasi Mahasiswa Aktif Universitas Airlangga</title>
{literal}
<style type="text/css">
<!--
@import url("style.css");

.green { color: #080; }
.red { color: #f00; }
-->
</style>
<style>
.clear-padding { padding: 0px; }
</style>
    <script type="text/javascript" language="javascript" src="http://cybercampus.unair.ac.id/js/jquery-1.5.1.min.js"></script>
{/literal}
</head>
<body>
<div id="header"><div id="judul_header"></div></div>
<h1 style="text-align:center;color:#0e2d50;font-family:'Trebuchet MS'">Mahasiswa Universitas Airlangga</h1>
<form name="f1" action="excel-sekolah-tahun.php" method="post">
  <input type="submit" name="button" id="button" value="Excel" />
</form>
   <table border="1" id="rounded-corner">
    <tr>
		<th style="text-align:center; width: 100px">
        	Kode Sekolah		
		</th>
        <th style="text-align:center">
        	Nama Sekolah		
		</th>
        <th style="text-align:center">
			2009
		</th>
		<th style="text-align:center">
			2010
		</th>
		<th style="text-align:center">
			2011
		</th>
    </tr>
	{foreach $data_set as $d}
	<tr>
		<td style="padding: 1px; text-align:center">{$d.KODE_SEKOLAH}</td>
		<td style="padding: 1px 1px 1px 5px;">{$d.NM_SEKOLAH}</td>
		<td style="padding: 1px; text-align:center">{$d.T2009}</td>
		<td style="padding: 1px; text-align:center">{$d.T2010}</td>
		<td style="padding: 1px; text-align:center">{$d.T2011}</td>
	</tr>
	{/foreach}
</table>