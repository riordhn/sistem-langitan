<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Jumlah Mahasiswa Tiap Sekolah</title>
{literal}
<style type="text/css">
<!--
@import url("style.css");

.green { color: #080; }
.red { color: #f00; }
-->
</style>
    <script type="text/javascript" language="javascript" src="http://cybercampus.unair.ac.id/js/jquery-1.5.1.min.js"></script>
{/literal}
</head>
<body>
<div id="header"><div id="judul_header"></div></div>
<h1 style="text-align:center;color:#0e2d50;font-family:'Trebuchet MS'">Jumlah Mahasiswa Tiap Sekolah</h1>
<form name="f1" action="excel.php" method="post">
  <input type="submit" name="button" id="button" value="Excel" />
</form>
   <table border="1" id="rounded-corner">
    <tr>
		<th style="text-align:center">
        	Asal Sekolah		
		</th>
        <th style="text-align:center">
        	Jumlah	
		</th>
    </tr>
    {$total = 0}
	{foreach $mhs as $data}
    	<tr>
        	<td>
            	{$data.NM_SEKOLAH}
            </td>
        	<td>
            	{$data.JUMLAH}
            </td>
        </tr>
        {$total = $total + $data.JUMLAH}
    {/foreach}
    <tr>
    	<td style="font-weight:bold">TOTAL</td>
        <td style="font-weight:bold">{$total}</td>
    </tr>
</table>