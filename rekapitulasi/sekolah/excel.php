<?php
include('../../config.php');
$mhs = $db->QueryToArray("SELECT COUNT(*) AS JUMLAH, SEKOLAH.NM_SEKOLAH FROM MAHASISWA
LEFT JOIN CALON_MAHASISWA_SEKOLAH ON CALON_MAHASISWA_SEKOLAH.ID_C_MHS = MAHASISWA.ID_C_MHS
LEFT JOIN SEKOLAH ON SEKOLAH.ID_SEKOLAH = CALON_MAHASISWA_SEKOLAH.ID_SEKOLAH_ASAL
LEFT JOIN SEMESTER ON SEMESTER.THN_AKADEMIK_SEMESTER = MAHASISWA.THN_ANGKATAN_MHS
WHERE CALON_MAHASISWA_SEKOLAH.ID_SEKOLAH_ASAL IS NOT NULL AND SEMESTER.STATUS_AKTIF_SEMESTER = 'True'
GROUP BY NM_SEKOLAH
ORDER BY JUMLAH DESC");
ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #1d5700;
    }
    td{
        text-align: left;
    }
</style>
   <table border="1" id="rounded-corner">
    <tr>
		<th style="text-align:center">
        	Asal Sekolah		
		</th>
        <th style="text-align:center">
        	Jumlah	
		</th>
    </tr>
    <?PHP
    $total = 0;
	foreach ($mhs as $data){
    	echo "<tr>
        	<td>
            	$data[NM_SEKOLAH]
            </td>
        	<td>
            	$data[JUMLAH]
            </td>
        </tr>";
        $total = $total + $data['JUMLAH'];
    }
	?>
    <tr>
    	<td style="font-weight:bold">TOTAL</td>
        <td style="font-weight:bold"><?PHP echo $total?></td>
    </tr>
</table>
<?php
$filename = 'Mahasiswa Baru Unair ' . date('Y-m-d');
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=$filename");
header("Pragma: ");
header("Cache-Control: ");
ob_flush();
?>
