<html>
<head>
    <title>Rasio Jumlah siswa pendaftar dan yang diterima (MANDIRI)</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="../../../../css/reset.css">
    <link rel="stylesheet" type="text/css" href="../../../../css/text.css">
    <link rel="stylesheet" type="text/css" href="../../../../css/ppmb.css">
    <link rel="stylesheet" type="text/css" href="../../../../css/jquery-ui-1.8.16.custom.ppmb-rekap.css">
    <link rel="shortcut icon" href="../../../../img/icon.ico">
</head>
<body>
    <table class="clear-margin-bottom">
        <colgroup>
            <col>
            <col class="main-width">
            <col>
        </colgroup>
        <thead>
            <tr>
                <td class="header-left"></td>
                <td class="header-center" style="background-image: url('../../../../img/ppmb/header-center-rekapitulasi.png');"></td>
                <td class="header-right"></td>
            </tr>
            <tr>
                <td class="tab-left"></td>
                <td class="tab-center">
                </td>
                <td class="tab-right"></td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="body-left">&nbsp;</td>
                <td class="body-center">
                    <table class="content-table">
                        <colgroup>
                            <col>
                            <col>
                        </colgroup>
                        <tbody>
                            <tr>
                                <td id="content" class="content">
                                    <h2 class="center" style="text-transform: uppercase">Rasio Jumlah siswa pendaftar dan yang diterima (MANDIRI)</h2>
                                    <div class="center"><a href="cetak-excel.php">Excel</a></div><br/>
                                    <table style="margin-right: auto; margin-left:auto;">
                                        <tr>
                                            <th>No</th>
                                            <th>Asal Sekolah</th>
                                            <th>Peminat</th>
                                            <th>Diterima</th>
                                        </tr>
                                        {$peminat = 0}{$diterima = 0}
                                        {foreach $sekolah_set as $s}
                                        <tr {if $s@index is div by 2}style="background-color: #efefef"{/if}>
                                            <td class="center">{$s@index + 1}</td>
                                            <td>{$s.KOTA} - {$s.SEKOLAH}</td>
                                            <td class="center">{$s.JUMLAH_PEMINAT}</td>
                                            <td class="center">{$s.JUMLAH_DITERIMA}</td>
                                        </tr>    
                                        {$peminat = $peminat + $s.JUMLAH_PEMINAT}{$diterima = $diterima + $s.JUMLAH_DITERIMA}
                                        {/foreach}
                                        
                                        <tr>
                                            <td class="center"></td>
                                            <td>Total</td>
                                            <td class="center">{$peminat}</td>
                                            <td class="center">{$diterima + 2}</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody></table>
                </td>
                <td class="body-right">&nbsp;</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td class="foot-left">&nbsp;</td>
                <td class="foot-center">
                    <div class="footer-nav">
                        <br/>
                    </div>
                    <div class="footer">Copyright © 2011 - Universitas Airlangga <br>All Rights Reserved</div>
                </td>
                <td class="foot-right">&nbsp;</td>
            </tr>
        </tfoot>
    </table>

</body>
</html>