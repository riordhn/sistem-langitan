<?php
include('../../../config.php');

$filename = 'rekapitulasi_sekolah_mandiri_2011.xls';
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=$filename");
header("Pragma: ");
header("Cache-Control: ");
ob_flush();

$smarty->assign('sekolah_set', $db->QueryToArray("
    select * from (
	select nm_kota kota, nm_sekolah sekolah,
            (select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb
             join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
             where id_penerimaan in (2,3,6,7) and cms.id_sekolah_asal = s.id_sekolah) jumlah_peminat,
            (select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb
             join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
             where id_penerimaan in (2,3,6,7) and cms.id_sekolah_asal = s.id_sekolah and cmb.id_program_studi is not null) jumlah_diterima
	from sekolah s
	join kota k on k.id_kota = s.id_kota
        join provinsi p on p.id_provinsi = k.id_provinsi
	order by p.id_negara, p.nm_provinsi, k.nm_kota, s.nm_sekolah) t 
    where jumlah_peminat > 0"));

$smarty->display('excel.tpl');
?>
