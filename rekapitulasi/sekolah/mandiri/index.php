<?php
include('../../../config.php');

$smarty->assign('sekolah_set', $db->QueryToArray("
    select * from (
	select nm_kota kota, nm_sekolah sekolah,
            (select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb
             join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
             where id_penerimaan in (2,3,6,7) and cms.id_sekolah_asal = s.id_sekolah) jumlah_peminat,
            (select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb
             join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
             where id_penerimaan in (2,3,6,7) and cms.id_sekolah_asal = s.id_sekolah and cmb.id_program_studi is not null) jumlah_diterima
	from sekolah s
	join kota k on k.id_kota = s.id_kota
        join provinsi p on p.id_provinsi = k.id_provinsi
	order by p.id_negara, p.nm_provinsi, k.nm_kota, s.nm_sekolah) t 
    where jumlah_peminat > 0
    order by jumlah_diterima desc, jumlah_peminat desc"));

$smarty->display("index.tpl");
?>
