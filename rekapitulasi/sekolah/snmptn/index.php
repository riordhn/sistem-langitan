<?php
include('../../../config.php');

$smarty->assign('sekolah_set', $db->QueryToArray("
    select * from (
        select provinsi, kota, sekolah,
          (select count(c1.id_c_mhs) from calon_mahasiswa_sekolah_snmptn c1
           where c1.provinsi = s.provinsi and c1.kota = s.kota and c1.sekolah = s.sekolah) jumlah_peminat,
          (select count(c1.no_ujian) from calon_mahasiswa_sekolah_snmptn c1
           join calon_mahasiswa_baru cmb on cmb.id_c_mhs = c1.id_c_mhs
           where c1.provinsi = s.provinsi and c1.kota = s.kota and c1.sekolah = s.sekolah and cmb.id_program_studi is not null) jumlah_diterima
        from calon_mahasiswa_sekolah_snmptn s
        group by provinsi, kota, sekolah) t
    order by jumlah_diterima desc, jumlah_peminat desc"));

$smarty->display("index.tpl");
?>
