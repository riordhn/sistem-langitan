<table border="1">
    <tr>
        <th>No</th>
        <th>Asal Sekolah</th>
        <th>Peminat</th>
        <th>Diterima</th>
    </tr>
    {foreach $sekolah_set as $s}
    <tr>
        <td class="center">{$s@index + 1}</td>
        <td>{$s.KOTA} - {$s.SEKOLAH}</td>
        <td class="center">{$s.JUMLAH_PEMINAT}</td>
        <td class="center">{$s.JUMLAH_DITERIMA}</td>
    </tr>    
    {/foreach}
</table>