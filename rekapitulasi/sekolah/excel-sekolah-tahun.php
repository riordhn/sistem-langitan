<?php
include('../../../config.php');

ob_start();
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=Data_Sekolah.xls");
header("Pragma: ");
header("Cache-Control: ");
ob_flush();

$data_set = $db->QueryToArray("
	select * from (
	  select id_sekolah, kode_sekolah, nm_sekolah, nm_kota, nm_provinsi,
		(select count(id_mhs) from mahasiswa m where m.id_sekolah_asal_mhs = s.id_sekolah and thn_angkatan_mhs = 2009) t2009,
		(select count(id_mhs) from mahasiswa m where m.id_sekolah_asal_mhs = s.id_sekolah and thn_angkatan_mhs = 2010) t2010,
		(select count(id_mhs) from mahasiswa m where m.id_sekolah_asal_mhs = s.id_sekolah and thn_angkatan_mhs = 2011) t2011
	  from sekolah s
	  join kota k on k.id_kota = s.id_kota
	  join provinsi p on p.id_provinsi = k.id_provinsi
	  where p.id_negara = 1
	  order by p.nm_provinsi, k.nm_kota, s.nm_sekolah) t
	where t2009 > 0 and t2010 > 0 and t2011 > 0");
	
$smarty->assign('data_set', $data_set);

$smarty->display("sekolah-tahun-excel.tpl");
?>
