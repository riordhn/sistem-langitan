{extends file="../index.tpl"}

{block name="content"}
	<form class="form-inline" action="" method="get">
		<input type="hidden" name="view" value="{$smarty.get.view}" />
		<select name="penerimaan" class="input-xxlarge">
			<option value="">-- Pilih Penerimaan --</option>
			{foreach $tahun_penerimaan_set as $tahun_penerimaan}
				<optgroup label="Tahun {$tahun_penerimaan['TAHUN']}">
					{foreach $tahun_penerimaan.penerimaan_set as $penerimaan}
						<option value="{$penerimaan.ID_PENERIMAAN}" {if !empty($smarty.get.penerimaan)}{if $smarty.get.penerimaan == $penerimaan.ID_PENERIMAAN}selected{/if}{/if}>{$penerimaan.SEMESTER} {$penerimaan.GELOMBANG} - {$penerimaan.NM_PENERIMAAN}</option>
					{/foreach}
				</optgroup>
			{/foreach}
		</select>
		{if $program_studi_set}
		<select name="prodi" class="input-xlarge">
			<option value="">-- Pilih Program Studi --</option>
			{foreach $program_studi_set as $program_studi}
				<option value="{$program_studi.ID_PROGRAM_STUDI}" {if !empty($smarty.get.prodi)}{if $smarty.get.prodi == $program_studi.ID_PROGRAM_STUDI}selected{/if}{/if}>{$program_studi.NM_JENJANG} {$program_studi.NM_PROGRAM_STUDI}</option>
			{/foreach}
		</select>
		{/if}
		<input type="submit" value="Tampilkan" class="btn btn-primary" />
	</form>

	{if $data_set}
		<table class="table table-bordered table-condensed table-striped">
			<thead>
				<tr>
					<th class="text-center">#</th>
					<th>Provinsi</th>
					<th class="text-center">Jumlah Peminat</th>
					<th class="text-center">Jumlah Diterima</th>
				</tr>
			</thead>
			<tbody>
				{$total_peminat = 0}{$total_diterima = 0}
				{foreach $data_set as $data}
					<tr>
						<td class="text-center">{$data@index + 1}</td>
						<td>{$data.NM_PROVINSI}</td>
						<td class="text-center">{$data.JUMLAH_PEMINAT}</td>
						<td class="text-center">{$data.JUMLAH_DITERIMA}</td>
					</tr>
					{$total_peminat = $total_peminat + $data.JUMLAH_PEMINAT}
					{$total_diterima = $total_diterima + $data.JUMLAH_DITERIMA}
				{/foreach}
				<tr class="success">
					<td></td>
					<td>Total</td>
					<td class="text-center">{$total_peminat}</td>
					<td class="text-center">{$total_diterima}</td>
				</tr>
			</tbody>
		</table>
	{/if}
{/block}