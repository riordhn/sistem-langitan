{extends file="../index.tpl"}

{block name="content"}
	<form class="form-inline" action="" method="get">
		<input type="hidden" name="view" value="{$smarty.get.view}" />
		<select name="penerimaan" class="input-xxlarge">
			<option value="">-- Pilih Penerimaan --</option>
			{foreach $tahun_penerimaan_set as $tahun_penerimaan}
				<optgroup label="Tahun {$tahun_penerimaan['TAHUN']}">
					{foreach $tahun_penerimaan.penerimaan_set as $penerimaan}
						<option value="{$penerimaan.ID_PENERIMAAN}" {if !empty($smarty.get.penerimaan)}{if $smarty.get.penerimaan == $penerimaan.ID_PENERIMAAN}selected{/if}{/if}>{$penerimaan.SEMESTER} {$penerimaan.GELOMBANG} - {$penerimaan.NM_PENERIMAAN}</option>
					{/foreach}
				</optgroup>
			{/foreach}
		</select>
		<input type="submit" value="Tampilkan" class="btn btn-primary" />
	</form>

	{if $data_set}
		<table class="table table-bordered table-condensed table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>Program Studi</th>
					<th class="text-center">Pilihan 1</th>
					<th class="text-center">Pilihan 2</th>
					<th class="text-center">Pilihan 3</th>
					<th class="text-center">Pilihan 4</th>
					<th class="text-center">Jumlah Peminat</th>
					<th class="text-center">Diterima</th>
					<th class="text-center">Keketatan</th>
					<th class="text-center"></th>
					<th class="text-center">Nilai Min</th>
					<th class="text-center">Nilai Max</th>
					<th class="text-center">Nilai Rata</th>
					<th class="text-center">Nilai Rata Diterima</th>
				</tr>
			</thead>
			<tbody>
				{$pilihan1_total = 0}
				{$diterima_total = 0}
				{foreach $data_set as $data}
					{if in_array($penerimaan_terpilih.ID_JENJANG, array(2,3,9,10))}
						{$NILAI_RATA_DITERIMA = $data.NILAI_RATA_DITERIMA_PASCA}
					{else}
						{$NILAI_RATA_DITERIMA = $data.NILAI_RATA_DITERIMA}
					{/if}
					<tr class="{if $data.KEKETATAN < 0}error{/if}">
						<td class="text-center">{$data@index + 1}</td>
						<td>{$data.NM_JENJANG} {$data.NM_PROGRAM_STUDI}</td>
						<td class="text-center">{$data.P1}</td>
						<td class="text-center">{$data.P2}</td>
						<td class="text-center">{$data.P3}</td>
						<td class="text-center">{$data.P4}</td>
						<td class="text-center">{$data.JP}</td>
						<td class="text-center">{$data.PD}</td>
						<td class="text-center">{$data.KEKETATAN}%</td>
						<td class="text-center"></td>
						<td class="text-center">{$data.NILAI_MIN}</td>
						<td class="text-center">{$data.NILAI_MAX}</td>
						<td class="text-center">{$data.NILAI_RATA}</td>
						<td class="text-center">{$NILAI_RATA_DITERIMA}</td>
					</tr>
					{$pilihan1_total = $pilihan1_total + $data.P1}
					{$diterima_total = $diterima_total + $data.PD}
				{/foreach}
				<tr class="success">
					<td></td>
					<td><strong>Jumlah</strong></td>
					<td class="text-center">{$pilihan1_total}</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="text-center"><strong>{$diterima_total}</strong></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tbody>
		</table>
	{/if}
{/block}