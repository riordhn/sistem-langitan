{extends file="../index.tpl"}

{block name="content"}
	<form class="form-inline" action="" method="get">
		<input type="hidden" name="view" value="{$smarty.get.view}" />
		<select name="penerimaan" class="input-xxlarge">
			<option value="">-- Pilih Penerimaan --</option>
			{foreach $tahun_penerimaan_set as $tahun_penerimaan}
				<optgroup label="Tahun {$tahun_penerimaan['TAHUN']}">
					{foreach $tahun_penerimaan.penerimaan_set as $penerimaan}
						<option value="{$penerimaan.ID_PENERIMAAN}" {if !empty($smarty.get.penerimaan)}{if $smarty.get.penerimaan == $penerimaan.ID_PENERIMAAN}selected{/if}{/if}>{$penerimaan.SEMESTER} {$penerimaan.GELOMBANG} - {$penerimaan.NM_PENERIMAAN}</option>
					{/foreach}
				</optgroup>
			{/foreach}
		</select>
		{if $program_studi_set}
		<select name="prodi" class="input-xlarge">
			<option value="">-- Pilih Program Studi --</option>
			{foreach $program_studi_set as $program_studi}
				<option value="{$program_studi.ID_PROGRAM_STUDI}" {if !empty($smarty.get.prodi)}{if $smarty.get.prodi == $program_studi.ID_PROGRAM_STUDI}selected{/if}{/if}>{$program_studi.NM_JENJANG} {$program_studi.NM_PROGRAM_STUDI}</option>
			{/foreach}
		</select>
		{/if}
		<input type="submit" value="Tampilkan" class="btn btn-primary" />
	</form>

	{if $data_set}
		<table class="table table-bordered table-striped">
			<tbody>
				<tr>
					<th class="text-center" rowspan="2" style="vertical-align: middle">#</th>
					<th rowspan="2" style="vertical-align: middle">Pendidikan Terakhir</th>
					<th class="text-center" colspan="2">Ayah</th>
					<th class="text-center" colspan="2">Ibu</th>
				</tr>
				<tr>
					<th class="text-center">Peminat</th>
					<th class="text-center">Diterima</th>
					<th class="text-center">Peminat</th>
					<th class="text-center">Diterima</th>
				</tr>
				{$jumlah_p_ayah = 0}
				{$jumlah_d_ayah = 0}
				{$jumlah_p_ibu = 0}
				{$jumlah_d_ibu = 0}
				{foreach $data_set as $data}
					<tr>
						<td class="text-center">{$data@index + 1}</td>
						<td>{$data.NM_PENDIDIKAN_ORTU}</td>
						<td class="text-center">{$data.JUMLAH_PEMINAT_AYAH}</td>
						<td class="text-center">{$data.JUMLAH_DITERIMA_AYAH}</td>
						<td class="text-center">{$data.JUMLAH_PEMINAT_IBU}</td>
						<td class="text-center">{$data.JUMLAH_DITERIMA_IBU}</td>
					</tr>
					{$jumlah_p_ayah = $jumlah_p_ayah + $data.JUMLAH_PEMINAT_AYAH}
					{$jumlah_d_ayah = $jumlah_d_ayah + $data.JUMLAH_DITERIMA_AYAH}
					{$jumlah_p_ibu = $jumlah_p_ibu + $data.JUMLAH_PEMINAT_IBU}
					{$jumlah_d_ibu = $jumlah_d_ibu + $data.JUMLAH_DITERIMA_IBU}
				{/foreach}
				<tr>
					<td></td>
					<td><strong>Jumlah</strong></td>
					<td class="text-center">{$jumlah_p_ayah}</td>
					<td class="text-center">{$jumlah_d_ayah}</td>
					<td class="text-center">{$jumlah_p_ibu}</td>
					<td class="text-center">{$jumlah_d_ibu}</td>
				</tr>
			</tbody>
		</table>
	{/if}

	<p class="muted">Apabila ada ketidak-sesuaian data silakan hubungi DSI.</p>
{/block}