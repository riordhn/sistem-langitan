<!DOCTYPE html>
<html>
	<head>
		<title>{if $judul}{$judul} - {/if}Rekapitulasi PPMB</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

		<!-- Bootstrap -->
		<link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="./bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
		<link href="//fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
		<style>
			body {
				background-image: url('http://cybercampus.unair.ac.id/img/pengumuman/bg_top_header.png');
				background-position: top left;
				background-repeat: repeat-x;
			}
			.cybercampus-header {
				background-image: url('http://cybercampus.unair.ac.id/img/ppmb/header-center-rekapitulasi.png');
				background-repeat: no-repeat;
				background-position: top center;
			}
			.content-wrapper {
				padding-top: 98px;
			}

			.table thead tr th {
				vertical-align: middle;
			}
			
			th.text-center, td.text-center {
				text-align: center;
			}

			/* Font Custom */
			body, input, button, select, textarea, .navbar-search .search-query {
				font-family: 'Open Sans', Helvetica, Arial, sans-serif;
				font-size: 0.9em;
			}
		</style>
	</head>
	<body>
		<div class="container cybercampus-header content-wrapper">

			<div class="navbar">
				<div class="navbar-inner">
					<a class="brand" href="./">PPMB</a>
					<ul class="nav">
						<li class="{if $view == 'peminatan'}active{/if}"><a href=".?view=peminatan">Peminatan</a></li>
						<li class="{if $view == 'asal-provinsi'}active{/if}"><a href=".?view=asal-provinsi">Asal Provinsi</a></li>
						<li class="{if $view == 'penghasilan-ortu'}active{/if}"><a href=".?view=penghasilan-ortu">Penghasilan Orang Tua</a></li>
						<li class="{if $view == 'pendidikan-ortu'}active{/if}"><a href=".?view=pendidikan-ortu">Pendidikan Orang Tua</a></li>
						<li class="{if $view == 'pekerjaan-ortu'}active{/if}"><a href=".?view=pekerjaan-ortu">Pekerjaan Orang Tua</a></li>
					</ul>
				</div>
			</div>

			{block name="content"}{/block}

		</div>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script>
		<script src="./bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		{block name="script"}{/block}
	</body>
</html>