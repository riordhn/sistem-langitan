<?php
include '../../config.php';

if (1 == 2) { $db = new MyOracle(); }

$mode = get('mode', '');

if ($mode == 'prodi')
{
    $id_fakultas = get('f');
    
    $data = $db->QueryToArray("
        select id_program_studi, nm_jenjang, nm_program_studi from program_studi ps
        join jenjang j on j.id_jenjang = ps.id_jenjang
        where ps.id_fakultas = {$id_fakultas} and status_aktif_prodi = 1
        order by j.id_jenjang, ps.nm_program_studi");
    
    echo "<option value=\"\">-- PILIH PROGRAM STUDI --</option>";
    foreach ($data as $d)
        echo "<option value=\"{$d['ID_PROGRAM_STUDI']}\">{$d['NM_JENJANG']} {$d['NM_PROGRAM_STUDI']}</option>";
    
    exit();
}
?>
