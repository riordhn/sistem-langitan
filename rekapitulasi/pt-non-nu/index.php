<?php
require '../../config.php';

	function get($name, $default_value = '')
	{
	    return isset($_GET[$name]) ? $_GET[$name] : $default_value;
	}

	$id_pt_prodi	= (int)get('id_pt_prodi', '');
	$id_pt_staff	= (int)get('id_pt_staff', '');
	$dosen_lb 		= (int)get('dosen_lb', '');
	$dosen_tetap 	= (int)get('dosen_tetap', '');
	$mahasiswa 		= (int)get('mahasiswa', '');

	$pt_set = $db->QueryToArray("SELECT pt.ID_PERGURUAN_TINGGI, pt.NAMA_PERGURUAN_TINGGI,pt.HTTP_HOST,
								(SELECT COUNT(f.ID_FAKULTAS) FROM FAKULTAS f WHERE f.ID_PERGURUAN_TINGGI = pt.ID_PERGURUAN_TINGGI) FAK_JUMLAH ,
								(SELECT COUNT(ps.ID_PROGRAM_STUDI) FROM PROGRAM_STUDI ps, FAKULTAS f1 WHERE ps.ID_FAKULTAS = f1.ID_FAKULTAS AND f1.ID_PERGURUAN_TINGGI = pt.ID_PERGURUAN_TINGGI) PRODI_JUMLAH ,
								(SELECT COUNT(peng.ID_PENGGUNA) FROM PENGGUNA peng WHERE peng.JOIN_TABLE = '1' AND peng.ID_PERGURUAN_TINGGI = pt.ID_PERGURUAN_TINGGI) PEGAWAI_JUMLAH ,
								(SELECT COUNT(peng.ID_PENGGUNA) FROM PENGGUNA peng WHERE peng.JOIN_TABLE = '2' AND peng.ID_PERGURUAN_TINGGI = pt.ID_PERGURUAN_TINGGI) DOSEN_JUMLAH ,
								(SELECT COUNT(peng.ID_PENGGUNA) FROM PENGGUNA peng JOIN dosen ON dosen.id_pengguna = peng.id_pengguna WHERE peng.JOIN_TABLE = '2' AND dosen.STATUS_DOSEN = 'LB' AND peng.ID_PERGURUAN_TINGGI = pt.ID_PERGURUAN_TINGGI) DOSEN_JUMLAH_LB ,
								(SELECT COUNT(peng.ID_PENGGUNA) FROM PENGGUNA peng JOIN dosen ON dosen.id_pengguna = peng.id_pengguna WHERE peng.JOIN_TABLE = '2' AND dosen.STATUS_DOSEN = 'PNS' AND peng.ID_PERGURUAN_TINGGI = pt.ID_PERGURUAN_TINGGI) DOSEN_JUMLAH_TETAP ,
								(SELECT COUNT(peng.ID_PENGGUNA) FROM PENGGUNA peng WHERE peng.JOIN_TABLE = '3' AND peng.ID_PERGURUAN_TINGGI = pt.ID_PERGURUAN_TINGGI) MHS_JUMLAH 
								FROM PERGURUAN_TINGGI pt WHERE pt.ID_PERGURUAN_TINGGI not in (20,26) AND pt.IS_UNU = 0
								ORDER BY pt.ID_PERGURUAN_TINGGI
								");
	$smarty->assignByRef('pt_set', $pt_set);

	$pt_pendidikan_set = $db->QueryToArray("SELECT ID_PERGURUAN_TINGGI, NAMA_PERGURUAN_TINGGI, MIN(BEBAN_SKS_JUMLAH) AS BEBAN_SKS_MIN,
											MIN(JADWAL_JAM_JUMLAH) AS JADWAL_JAM_MIN, MIN(SEMESTER_JUMLAH) AS SEMESTER_MIN, MIN(SKS_WAJIB_JUMLAH) AS SKS_WAJIB_MIN, MIN(JADWAL_KEGIATAN_JUMLAH) AS JADWAL_KEGIATAN_MIN FROM (SELECT pt.ID_PERGURUAN_TINGGI, pt.NAMA_PERGURUAN_TINGGI, prodi.ID_PROGRAM_STUDI, prodi.NM_PROGRAM_STUDI,
												(SELECT COUNT(bs.ID_BEBAN_SKS) FROM BEBAN_SKS bs WHERE bs.ID_PROGRAM_STUDI = prodi.ID_PROGRAM_STUDI) BEBAN_SKS_JUMLAH,
												(SELECT COUNT(jam.ID_JADWAL_JAM) FROM JADWAL_JAM jam WHERE jam.ID_FAKULTAS = f.ID_FAKULTAS) JADWAL_JAM_JUMLAH,
												(SELECT COUNT(sem.ID_SEMESTER) FROM SEMESTER sem WHERE sem.ID_PERGURUAN_TINGGI = pt.ID_PERGURUAN_TINGGI) SEMESTER_JUMLAH,
												(SELECT COUNT(sw.ID_SKS_DIWAJIBKAN) FROM SKS_WAJIB sw WHERE sw.ID_PROGRAM_STUDI = prodi.ID_PROGRAM_STUDI) SKS_WAJIB_JUMLAH,
												(SELECT COUNT(jks.ID_JADWAL_KEGIATAN_SEMESTER) FROM JADWAL_KEGIATAN_SEMESTER jks WHERE jks.ID_PERGURUAN_TINGGI = pt.ID_PERGURUAN_TINGGI) JADWAL_KEGIATAN_JUMLAH					
												FROM PERGURUAN_TINGGI pt, PROGRAM_STUDI prodi, FAKULTAS f
												WHERE prodi.ID_FAKULTAS = f.ID_FAKULTAS AND f.ID_PERGURUAN_TINGGI = pt.ID_PERGURUAN_TINGGI 
												AND pt.ID_PERGURUAN_TINGGI <> 20 AND pt.IS_UNU = 0
												ORDER BY pt.ID_PERGURUAN_TINGGI)
											GROUP BY ID_PERGURUAN_TINGGI, NAMA_PERGURUAN_TINGGI
											ORDER BY ID_PERGURUAN_TINGGI
											");
	$smarty->assignByRef('pt_pendidikan_set', $pt_pendidikan_set);


	if ($id_pt_prodi != '')
	{
		// Mengambil List Prodi
		$pt_prodi_set = $db->QueryToArray(
			"SELECT prodi.ID_PROGRAM_STUDI, prodi.NM_PROGRAM_STUDI, pt.NAMA_PERGURUAN_TINGGI, j.NM_JENJANG,
			(SELECT COUNT(m.ID_MHS) FROM MAHASISWA m WHERE m.ID_PROGRAM_STUDI = prodi.ID_PROGRAM_STUDI) MHS_JUMLAH ,
			(SELECT COUNT(d.ID_DOSEN) FROM DOSEN d WHERE d.ID_PROGRAM_STUDI = prodi.ID_PROGRAM_STUDI) DOSEN_JUMLAH ,
			(SELECT COUNT(kp.ID_KURIKULUM_PRODI) FROM KURIKULUM_PRODI kp WHERE kp.ID_PROGRAM_STUDI = prodi.ID_PROGRAM_STUDI) KURIKULUM_PRODI_JUMLAH
			FROM PROGRAM_STUDI prodi, JENJANG j, FAKULTAS f, PERGURUAN_TINGGI pt
			WHERE prodi.ID_JENJANG = j.ID_JENJANG AND prodi.ID_FAKULTAS = f.ID_FAKULTAS AND f.ID_PERGURUAN_TINGGI = pt.ID_PERGURUAN_TINGGI AND pt.ID_PERGURUAN_TINGGI = {$id_pt_prodi}
			ORDER BY prodi.ID_PROGRAM_STUDI");

		$smarty->assignByRef('pt_prodi_set', $pt_prodi_set);
	}
	else if ($id_pt_staff != '')
	{
		// Mengambil List Unit Kerja
		$pt_staff_set = $db->QueryToArray(
			"SELECT uk.ID_UNIT_KERJA, uk.NM_UNIT_KERJA, pt.NAMA_PERGURUAN_TINGGI,
			(SELECT COUNT(p.ID_PEGAWAI) FROM PEGAWAI p WHERE p.ID_UNIT_KERJA = uk.ID_UNIT_KERJA) PEGAWAI_JUMLAH
			FROM UNIT_KERJA uk, PERGURUAN_TINGGI pt
			WHERE uk.ID_PERGURUAN_TINGGI = pt.ID_PERGURUAN_TINGGI AND uk.ID_PERGURUAN_TINGGI = {$id_pt_staff}
			ORDER BY uk.ID_UNIT_KERJA");

		$smarty->assignByRef('pt_staff_set', $pt_staff_set);
	}
	else if ($dosen_lb != '')
	{
		// Mengambil List Dosen Luar Biasa
		$pt_dosen_lb = $db->QueryToArray(
			"SELECT pengg.NM_PENGGUNA,ps.NM_PROGRAM_STUDI,dosen.NIDN_DOSEN FROM pengguna pengg 
			 JOIN dosen ON dosen.id_pengguna = pengg.id_pengguna 
			 JOIN program_studi ps ON ps.id_program_studi = dosen.id_program_studi_sd
			 WHERE pengg.join_table = '2' AND pengg.id_perguruan_tinggi = {$dosen_lb} AND dosen.STATUS_DOSEN = 'LB'");

		$smarty->assignByRef('pt_dosen_lb', $pt_dosen_lb);
	}
	else if ($dosen_tetap != '')
	{
		// Mengambil List Dosen Tetap
		$pt_dosen_tetap = $db->QueryToArray(
			"SELECT pengg.NM_PENGGUNA,ps.NM_PROGRAM_STUDI,dosen.NIDN_DOSEN FROM pengguna pengg 
			 JOIN dosen ON dosen.id_pengguna = pengg.id_pengguna 
			 JOIN program_studi ps ON ps.id_program_studi = dosen.id_program_studi_sd
			 WHERE pengg.join_table = '2' AND pengg.id_perguruan_tinggi = {$dosen_tetap} AND dosen.status_dosen = 'PNS'");

		$smarty->assignByRef('pt_dosen_tetap', $pt_dosen_tetap);
	}
	else if ($mahasiswa != '')
	{
		// Mengambil List Dosen Tetap
		$pt_mahasiswa = $db->QueryToArray(
			"SELECT mhs.NIM_MHS,mhs.THN_ANGKATAN_MHS,peng.NM_PENGGUNA,ps.NM_PROGRAM_STUDI FROM pengguna peng 
			 JOIN mahasiswa mhs ON mhs.id_pengguna = peng.id_pengguna
			 JOIN program_studi ps ON ps.id_program_studi = mhs.id_program_studi
			 WHERE peng.join_table = '3' AND peng.id_perguruan_tinggi = '{$mahasiswa}' 
			 ORDER BY THN_ANGKATAN_MHS,NM_PROGRAM_STUDI,NM_PENGGUNA");

		$smarty->assignByRef('pt_mahasiswa', $pt_mahasiswa);
	}

$smarty->display("index.tpl");