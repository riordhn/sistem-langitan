<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informasi Mahasiswa Aktif Universitas Airlangga</title>
{literal}
<style type="text/css">
<!--
@import url("style.css");

.green { color: #080; }
.red { color: #f00; }
-->
</style>
    <script type="text/javascript" language="javascript" src="http://cybercampus.unair.ac.id/js/jquery-1.5.1.min.js"></script>
{/literal}
</head>
<body>
<div id="header"><div id="judul_header"></div></div>
<h1 style="text-align:center;color:#0e2d50;font-family:'Trebuchet MS'">Mahasiswa Aktif Universitas Airlangga</h1>
 <div style="overflow: scroll;width: 100%;height: 600px;">
   <table border="1" id="rounded-corner">
    <tr>
		<th rowspan="4" style="text-align:center">
        	Fakultas		
		</th>
        <th rowspan="4" style="text-align:center">
        	Program Studi		
		</th>
		<th colspan="{count($data_jenjang)*4}" style="text-align:center">
        	Jenjang
		</th>
        <th scope="col" style="text-align: center" rowspan="2" colspan="2">Σ Per Prodi</th>
    </tr>
	<tr>
	{foreach $data_jenjang as $data}
      <th colspan="4" style="text-align:center">{$data.NM_JENJANG}</th>
	{/foreach}
    </tr>
	<tr>
    {foreach $data_jenjang as $data}
      <th colspan="2">Baru</th>
      <th colspan="2">Lama</th>
    {/foreach}
    	<th>Baru</th>
        <th>Lama</th>
    </tr>
    <tr>
    {foreach $data_jenjang as $data}
      <th>Laki</th>
      <th>Perempuan</th>
    {/foreach}
    	<th>Laki</th>
        <th>Perempuan</th>
    </tr>
    {foreach $data_fakultas_prodi as $data}
    <tr>
		<td>
        	{$data.FAKULTAS}        
		</td>
        <td>
        	{$data.NM_PROGRAM_STUDI}        
		</td>
        {$total_prodi_laki = 0}
        {$total_prodi_perempuan = 0}
		{foreach $data_jenjang as $jenj}
		<td>{$found = false}
			{foreach $data_mhs_aktif as $mhs_aktif}
				{if $data.ID_PROGRAM_STUDI == $mhs_aktif.ID_PROGRAM_STUDI and $mhs_aktif.ID_JENJANG == $jenj.ID_JENJANG 
                	and $mhs_aktif.KELAMIN_PENGGUNA == 1}
					<span style="color:#0033FF; text-align:center">{$mhs_aktif.JUMLAH}</span>
					{$found = true}
                    {$total_prodi_laki = $total_prodi_laki + $mhs_aktif.JUMLAH}
					{break}
				{/if}
			{/foreach}
			{if $found == false}
                    <span style="text-align:center">0</span>
            {/if}
		</td>
        <td>{$found = false}
			{foreach $data_mhs_aktif as $mhs_aktif}
				{if $data.ID_PROGRAM_STUDI == $mhs_aktif.ID_PROGRAM_STUDI and $mhs_aktif.ID_JENJANG == $jenj.ID_JENJANG 
                	and $mhs_aktif.KELAMIN_PENGGUNA == 2}
					<span style="color:#0033FF; text-align:center">{$mhs_aktif.JUMLAH}</span>
					{$found = true}
                    {$total_prodi_perempuan = $total_prodi_perempuan + $mhs_aktif.JUMLAH}
					{break}
				{/if}
			{/foreach}
			{if $found == false}
                    <span style="text-align:center">0</span>
            {/if}        	
        </td>
		{/foreach}
        <td style="text-align: center; font-weight: bold">{$total_prodi_laki}</td>
        <td style="text-align: center; font-weight: bold">{$total_prodi_perempuan}</td>
    </tr>
    {/foreach}
    	<tr>
            <td colspan="2">TOTAL</td>
            {$total_all_perempuan = 0}
            {$total_all_laki = 0}
        {foreach $data_jenjang as $jenj}
            {$found = false}
            {$total_jenj_laki = 0}
			{foreach $data_mhs_aktif as $mhs_aktif}
				{if $mhs_aktif.ID_JENJANG == $jenj.ID_JENJANG and $mhs_aktif.KELAMIN_PENGGUNA == 1}
                	{$total_jenj_laki = $total_jenj_laki + $mhs_aktif.JUMLAH}
					{$found = true}
                    {$total_all_laki = $total_all_laki + $mhs_aktif.JUMLAH}
				{/if}
			{/foreach}
			{if $found == false}
                    <td style="text-align: center; font-weight: bold">0</td>
            {else}
            		<td style="text-align: center; font-weight: bold">{$total_jenj_laki}</td>
            {/if}             
           
           	{$found = false}
            {$total_jenj_perempuan = 0}
			{foreach $data_mhs_aktif as $mhs_aktif}
				{if $mhs_aktif.ID_JENJANG == $jenj.ID_JENJANG and $mhs_aktif.KELAMIN_PENGGUNA == 2}
                	{$total_jenj_perempuan = $total_jenj_perempuan + $mhs_aktif.JUMLAH}
					{$found = true}
                    {$total_all_perempuan = $total_all_perempuan + $mhs_aktif.JUMLAH}
				{/if}
			{/foreach}
			{if $found == false}
                    <td style="text-align: center; font-weight: bold">0</td>
            {else}
            		<td style="text-align: center; font-weight: bold">{$total_jenj_perempuan}</td>
            {/if} 
        {/foreach}
            <td style="text-align: center; font-weight: bold">{$total_all_laki}</td>
            <td style="text-align: center; font-weight: bold">{$total_all_perempuan}</td>
        </tr>
   </table>
 </div>