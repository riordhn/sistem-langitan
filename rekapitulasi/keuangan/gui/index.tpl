<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informasi Mahasiswa Aktif Universitas Airlangga</title>
{literal}
<style type="text/css">
<!--
@import url("style.css");

.green { color: #080; }
.red { color: #f00; }
-->
</style>
    <script type="text/javascript" language="javascript" src="http://cybercampus.unair.ac.id/js/jquery-1.5.1.min.js"></script>
{/literal}
</head>
<body>
<div id="header"><div id="judul_header"></div></div>
<h1 style="text-align:center;color:#0e2d50;font-family:'Trebuchet MS'">Pembayaran Mahasiswa Universitas Airlangga</h1>
<div style="overflow: scroll;width: 100%;height: 600px;">
<table id="rounded-corner">
<tbody>
	<tr>
		<th>NIM</th>
		<th>Nama</th>
		<th>Fakultas</th>
		<th>Program Studi</th>
		{foreach $biaya_set as $data}
		<th>{$data.NM_BIAYA}</th>
		{/foreach}
	</tr>
	{foreach $mhs_set as $mhs}
	<tr>
		<td>{$mhs.NIM_MHS}</td>
		<td>{$mhs.NM_PENGGUNA}</td>
		<td>{$mhs.NM_FAKULTAS}</td>
		<td>{$mhs.NM_PROGRAM_STUDI}</td>
		{foreach $biaya_set as $data}
		<td></td>
		{/foreach}
	</tr>
	{/foreach}
</tbody>
</table>
</div>
</body>
</html>