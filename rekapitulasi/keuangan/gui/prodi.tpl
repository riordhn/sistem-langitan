<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informasi Mahasiswa Aktif Universitas Airlangga</title>
{literal}
<style type="text/css">
<!--
@import url("style.css");

.green { color: #080; }
.red { color: #f00; }
-->
</style>
{/literal}
</head>
<body>
<div id="header"><div id="judul_header"></div></div>
<h1 style="text-align:center;color:#0e2d50;font-family:'Trebuchet MS'">Mahasiswa Aktif Universitas Airlangga</h1>
<h3 style="text-align:center;">Total: {$total_mahasiswa}</h3>
<div id="content">
{foreach $fakultas_set as $f}
<h2 style="font-family:'Trebuchet MS'" id="{$f.ID_FAKULTAS}">Fakultas {$f.NM_FAKULTAS}</h2>
<table id="rounded-corner">
    <thead>
    	<tr>
            <th scope="col" class="rounded-company">Prodi</th>
            <th scope="col" class="rounded-q4" style="text-align: center; width: 100px">Jumlah</th>
        </tr>
    </thead>
    <tfoot>
    	<tr>
            <td class="rounded-foot-left"><em>&nbsp;</em></td>
            <td class="rounded-foot-right">&nbsp;</td>
        </tr>
    </tfoot>
    <tbody>
        {$total = 0}
        {foreach $f.program_studi_set as $ps}
        <tr>
	
            <td><a href="?id={$ps.ID_PROGRAM_STUDI}">{$ps.NM_JENJANG} - {$ps.NM_PROGRAM_STUDI}</a> </td>
            <td style="text-align: center">{$ps.JUMLAH}</td>
       
	</tr>
        {$total = $total + $ps.JUMLAH}
        {/foreach}
        <tr>
            <td><strong>Total</strong></td>
            <td style="text-align: center"><strong>{$total}</strong></td>
        </tr>
    </tbody>
</table>
{/foreach}
</div>
</body>
</html>