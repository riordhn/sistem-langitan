<?php
include('../../config.php');
include 'class/report_mahasiswa.class.php';

// penambahan id sesuai ptnu dan parameter $id_pt pada construct class report_mahasiswa
// Fikrie //
$id_pt = $id_pt_user;

$rm = new report_mahasiswa($db,$id_pt);

	
    $smarty->assign('data_program_studi', $rm->get_program_studi(get('fakultas')));
	$smarty->assign('data_fakultas_prodi', $rm->get_fakultas_prodi());
	$smarty->assign('data_mhs_aktif', $rm->get_mhs_aktif());

$smarty->assign('data_fakultas', $rm->get_fakultas());
$smarty->assign('data_tahun_akademik', $rm->get_tahun_akademik());
$smarty->assign('data_status', $rm->get_status_aktif());
$smarty->assign('data_jenjang', $rm->get_jenjang());
$smarty->assign('data_semester_aktif', $rm->get_semester_aktif());

$smarty->display("view.tpl");
?>
