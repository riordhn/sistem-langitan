<?php
include('../../config.php');
include 'class/report_mahasiswa.class.php';

// penambahan id sesuai ptnu dan parameter $id_pt pada construct class report_mahasiswa
// Fikrie //
$id_pt = $id_pt_user;

$rm = new report_mahasiswa($db,$id_pt);

if(isset($_GET['thn_angkatan']) and $_GET['thn_angkatan'] <> '')
{
	$thn_angkatan = ' AND THN_ANGKATAN_MHS = '.$_GET['thn_angkatan'].' ';
}else{
	$thn_angkatan = '';
}
	
if(isset($_GET['prodi']) and isset($_GET['jenjang']) and isset($_GET['thn_angkatan'])){
	$smarty->assign('data_mhs_prodi_jenjang', $rm->get_mhs_prodi_jenjang($_GET['prodi'], $_GET['jenjang'], $thn_angkatan));
}
    $smarty->assign('data_program_studi', $rm->get_program_studi(get('fakultas')));
	$smarty->assign('data_fakultas_prodi', $rm->get_fakultas_prodi());
	$smarty->assign('thn_angkatan', $_GET['thn_angkatan']);
	$smarty->assign('data_mhs_aktif', $rm->get_mhs_aktif_jenjang($thn_angkatan));

$smarty->assign('data_fakultas', $rm->get_fakultas());
$smarty->assign('data_tahun_akademik', $rm->get_tahun_akademik());
$smarty->assign('data_status', $rm->get_status_aktif());
$smarty->assign('data_jenjang', $rm->get_jenjang());

$smarty->display("jml-mhs.tpl");
?>
