<?php

include('./../../config.php');
if(!empty($_GET['fak'])){
	$mhs_set = $db->QueryToArray("SELECT NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI FROM MAHASISWA
							  LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
							  LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
							  WHERE PROGRAM_STUDI.ID_FAKULTAS = '$_GET[fak]' AND MAHASISWA.STATUS_AKADEMIK_MHS = '$_GET[stat]'
							  ORDER BY NM_PROGRAM_STUDI, NM_PENGGUNA");
}elseif(!empty($_GET['prod'])){
	$mhs_set = $db->QueryToArray("SELECT NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI FROM MAHASISWA
							  LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
							  LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
							  WHERE PROGRAM_STUDI.ID_PROGRAM_STUDI = '$_GET[prod]' AND MAHASISWA.STATUS_AKADEMIK_MHS = '$_GET[stat]'
							  ORDER BY NM_PROGRAM_STUDI, NM_PENGGUNA");
}

$status_pengguna =  $db->QueryToArray("select NM_STATUS_PENGGUNA from STATUS_PENGGUNA where ID_STATUS_PENGGUNA = '$_GET[stat]'");
if(!empty($_GET["cetak"]) ) {
	ob_start();
}

?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #1d5700;
    }
    td{
        text-align: left;
    }
</style>
<input type="button" name="excel" value="Excell" onclick="location.href='excel-mahasiswa-unair.php?stat=<?php echo $_GET["stat"]; ?>&fak=<?php echo $_GET["fak"]; ?>&cetak=1';">
<table cellpadding="3" cellspacing="3" border="1">
    <thead>
        <tr>
        	<td class="header_text">NO</td>
            <td class="header_text">NIM</td>
            <td class="header_text">NAMA</td>
            <td class="header_text">PROGRAM STUDI</td>
        </tr>
    </thead>
    <tbody>
        <?php
		$no = 1;
        foreach ($mhs_set as $data) {
            echo
            "<tr>
				<td style>{$no}</td>
                <td style><a href='excel-mahasiswa-unair-det.php?nim={$data['NIM_MHS']}'>'{$data['NIM_MHS']}</a></td>
                <td>{$data['NM_PENGGUNA']}</td>
                <td>{$data['NM_PROGRAM_STUDI']}</td>
            </tr>";
			$no++;
        }
        ?>
    </tbody>
</table>
<input type="button" name="excel" value="Excell" onclick="location.href='excel-mahasiswa-unair.php?stat=<?php echo $_GET["stat"]; ?>&fak=<?php echo $_GET["fak"]; ?>&cetak=1';">
<?php
if(!empty($_GET["cetak"]) ) {
	$filename = 'Mahasiswa_' . $status_pengguna[0]['NM_STATUS_PENGGUNA'] . '_'. date('Y-m-d');
	header("Content-type: application/vnd.ms-excel; name='excel'");
	header("Content-Disposition: filename={$filename}.xls");
	header("Pragma: ");
	header("Cache-Control: ");
	ob_flush();
}