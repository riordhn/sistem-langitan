<?php
include('../../config.php');

// Fikrie //
$id_pt = $id_pt_user;

 $id_pt_rekap = (int) get('id_pt_rekap','');
if(isset($id_pt_rekap) && !empty($id_pt_rekap)) {
    $id_pt = $id_pt_rekap;
    $request_header = $db->QueryToArray("SELECT lower(NAMA_SINGKAT) AS NAMA_SINGKAT from perguruan_tinggi where id_perguruan_tinggi = {$id_pt}");
    $nama_singkat = $request_header[0]['NAMA_SINGKAT'];
    $smarty->assign('nama_singkat', $nama_singkat);
}

$fakultas_set = $db->QueryToArray("SELECT * from fakultas where id_perguruan_tinggi = {$id_pt}");

$total_mahasiswa = 0;

if(isset($_GET['id']))
{
	$id = $_GET['id'];
	
	$semester_set = $db->QueryToArray(
			"SELECT THN_ANGKATAN_MHS FROM MAHASISWA, STATUS_PENGGUNA
            WHERE ID_PROGRAM_STUDI = '$id' and STATUS_PENGGUNA.STATUS_AKTIF = 1  and ID_ROLE = 3
            GROUP BY THN_ANGKATAN_MHS
            ORDER BY THN_ANGKATAN_MHS DESC");
        
	$program_studi_set = $db->QueryToArray("SELECT * From program_studi where id_program_studi = '$id'");
	
	$jumlah_mhs_set = $db->QueryToArray(
			"SELECT
                THN_ANGKATAN_MHS, NM_STATUS_PENGGUNA,
                SUM(CASE WHEN (STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS) THEN 1 ELSE 0 END) AS JUMLAH
            from STATUS_PENGGUNA, MAHASISWA
            where STATUS_PENGGUNA.STATUS_AKTIF = 1 AND ID_PROGRAM_STUDI = '$id' AND ID_ROLE = 3
            GROUP BY THN_ANGKATAN_MHS, NM_STATUS_PENGGUNA
            ORDER BY NM_STATUS_PENGGUNA ASC, THN_ANGKATAN_MHS DESC");
	
	$smarty->assign('id', $_GET['id']);
	$smarty->assign('program_studi_set', $program_studi_set);
	$smarty->assign('semester_set', $semester_set);
	$smarty->assign('jumlah_mhs_set', $jumlah_mhs_set);
	
}
elseif (isset($_GET['fak']))
{
    $fak = $_GET['fak'];
    
    // Fakultas
    $smarty->assign('fakultas', $db->QueryToArray("SELECT * from fakultas where id_fakultas = {$fak}"));
    
    // untuk kolom status mahasiswa
    $smarty->assign('status_pengguna_set', $db->QueryToArray("SELECT sp.*, 0 as jumlah from status_pengguna sp where id_role = 3 and status_aktif = 1"));
    
    // Untuk baris program studi
    $smarty->assign('program_studi_set', $db->QueryToArray("SELECT id_program_studi, nm_jenjang, nm_program_studi from program_studi ps
        join jenjang j on j.id_jenjang = ps.id_jenjang
        where ps.id_fakultas = {$fak}"));
        
    // Matrix prodi dan jumlah status mahasiswa
    $smarty->assign('matrix_set', $db->QueryToArray(
    	"SELECT 
          sp.id_status_pengguna, ps.id_program_studi, count(m.id_mhs) as jumlah
        from status_pengguna sp
        join mahasiswa m on m.status_akademik_mhs = sp.id_status_pengguna
        join program_studi ps on ps.id_program_studi = m.id_program_studi
        where
          sp.id_role = 3 and sp.status_aktif = 1 and
          ps.id_fakultas = {$fak}
        group by sp.id_status_pengguna, ps.id_program_studi
        order by sp.id_status_pengguna, ps.id_program_studi"));
        
	
    $smarty->assign('fak', $_GET['fak']);
}
else
{

    $fakultas_pengguna_set = $db->QueryToArray(
    		"SELECT
            FAKULTAS.ID_FAKULTAS, NM_FAKULTAS, NM_STATUS_PENGGUNA,
                SUM(CASE WHEN (STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS) THEN 1 ELSE 0 END) AS JUMLAH
            from STATUS_PENGGUNA, MAHASISWA
            join PROGRAM_STUDI on PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
            JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
            where STATUS_PENGGUNA.STATUS_AKTIF = 1  AND ID_ROLE = 3 AND FAKULTAS.ID_PERGURUAN_TINGGI = {$id_pt}
            GROUP BY FAKULTAS.ID_FAKULTAS, NM_FAKULTAS, NM_STATUS_PENGGUNA
            ORDER BY NM_STATUS_PENGGUNA, FAKULTAS.ID_FAKULTAS");
    

    $smarty->assign('total_mahasiswa', $total_mahasiswa);
    $smarty->assign('fakultas_pengguna_set', $fakultas_pengguna_set);
    $smarty->assign('fakultas_set', $fakultas_set);
}

$status_pengguna_set = $db->QueryToArray("SELECT * From status_pengguna where id_role = 3 and status_aktif = 1 order by nm_status_pengguna");
$smarty->assign('status_pengguna_set', $status_pengguna_set);
	
$smarty->display('index.tpl');