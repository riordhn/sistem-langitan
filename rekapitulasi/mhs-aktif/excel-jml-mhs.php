<?php
include('../../../config.php');

if(isset($_GET['thn_angkatan']) and $_GET['thn_angkatan'] <> '')
{
	$thn_angkatan = ' AND THN_ANGKATAN_MHS = '.$_GET['thn_angkatan'].' ';
}else{
	$thn_angkatan = '';
}

$data_mhs_aktif = $db->QueryToArray("SELECT PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_PROGRAM_STUDI, NM_PROGRAM_STUDI, JENJANG.ID_JENJANG, 
			COUNT(ID_MHS) AS JUMLAH
			FROM MAHASISWA
			LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI 
			LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
			LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
			LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
			WHERE STATUS_PENGGUNA.AKTIF_STATUS_PENGGUNA = 1 AND PROGRAM_STUDI.ID_PROGRAM_STUDI !=197
			AND JENJANG.ID_JENJANG <> 4 AND JENJANG.ID_JENJANG <> 6 AND JENJANG.ID_JENJANG <> 7 $thn_angkatan
			GROUP BY PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_PROGRAM_STUDI, NM_PROGRAM_STUDI, JENJANG.ID_JENJANG
			ORDER BY PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_PROGRAM_STUDI, JENJANG.ID_JENJANG");
			
			
$data_jenjang = $db->QueryToArray("SELECT * FROM JENJANG WHERE ID_JENJANG <> 4 AND ID_JENJANG <> 6 AND ID_JENJANG <> 7 
											ORDER BY ID_JENJANG");

$data_fakultas_prodi = $db->QueryToArray("SELECT UPPER(F.NM_FAKULTAS) AS FAKULTAS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,PS.ID_PROGRAM_STUDI
											FROM AUCC.FAKULTAS F 
											JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_FAKULTAS = F.ID_FAKULTAS
											JOIN AUCC.JENJANG J ON PS.ID_JENJANG = J.ID_JENJANG
											WHERE PS.ID_PROGRAM_STUDI !=197
											ORDER BY F.ID_FAKULTAS,PS.ID_PROGRAM_STUDI, J.ID_JENJANG");

ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #1d5700;
    }
    td{
        text-align: left;
    }
</style>
   <table border="1">
    <tr>
		<th rowspan="2" style="text-align:center">
        	Fakultas		
		</th>
        <th rowspan="2" style="text-align:center">
        	Program Studi		
		</th>
		<th colspan="<?PHP echo count($data_jenjang)?>" style="text-align:center">
        	Jenjang
		</th>
        <th scope="col" style="text-align: center" rowspan="2">Total Per Prodi</th>
    </tr>
	<tr>
	<?PHP 
	foreach ($data_jenjang as $data){
      echo "<th style='text-align:center'>$data[NM_JENJANG]</th>";
	}
    echo "</tr>";
    foreach ($data_fakultas_prodi as $data){
    echo "<tr>
		<td>
        	$data[FAKULTAS]        
		</td>
        <td>
        	$data[NM_PROGRAM_STUDI]      
		</td>";
        $total_prodi_laki = 0;
		foreach ($data_jenjang as $jenj){
		echo "<td>";
			$found = false;
			foreach ($data_mhs_aktif as $mhs_aktif){
				if ($data['ID_PROGRAM_STUDI'] == $mhs_aktif['ID_PROGRAM_STUDI'] and $mhs_aktif['ID_JENJANG'] == $jenj['ID_JENJANG']){
					echo "<span style='color:#0033FF; text-align:center'>$mhs_aktif[JUMLAH]</span>";
					$found = true;
                    $total_prodi_laki = $total_prodi_laki + $mhs_aktif['JUMLAH'];
					break;
				}
			}
			if ($found == false){
                    echo "<span style='text-align:center'>0</span>";
            }
		echo "</td>";
		}
        echo "<td style='text-align: center; font-weight: bold'>$total_prodi_laki</td>
    </tr>";
    }
    	echo "<tr>
            <td colspan='2'>TOTAL</td>";
            $total_all_laki = 0;
        foreach ($data_jenjang as $jenj){
            $found = false;
            $total_jenj_laki = 0;
			foreach ($data_mhs_aktif as $mhs_aktif){
				if ($mhs_aktif['ID_JENJANG'] == $jenj['ID_JENJANG']){
                	$total_jenj_laki = $total_jenj_laki + $mhs_aktif['JUMLAH'];
					$found = true;
                    $total_all_laki = $total_all_laki + $mhs_aktif['JUMLAH'];
				}
			}
			if ($found == false){
                    echo "<td style='text-align: center; font-weight: bold'>0</td>";
            }else{
            		echo "<td style='text-align: center; font-weight: bold'>$total_jenj_laki</td>";
            }             
       }
            echo "<td style='text-align: center; font-weight: bold'>$total_all_laki</td>
        </tr>
   </table>";
?>
<?php
$filename = 'Rekapitulasi Mahasiswa Unair ' . date('Y-m-d');
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=$filename");
header("Pragma: ");
header("Cache-Control: ");
ob_flush();
?>