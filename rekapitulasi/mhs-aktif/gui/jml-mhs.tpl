<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informasi Mahasiswa Aktif Universitas Airlangga</title>
{literal}
<style type="text/css">
<!--
@import url("style.css");

.green { color: #080; }
.red { color: #f00; }
-->
</style>
    <script type="text/javascript" language="javascript" src="http://cybercampus.unair.ac.id/js/jquery-1.5.1.min.js"></script>
{/literal}
</head>
<body>
<div id="header"><div id="judul_header"></div></div>
<h1 style="text-align:center;color:#0e2d50;font-family:'Trebuchet MS'">Mahasiswa Aktif Universitas Airlangga</h1>
{if isset($data_mhs_prodi_jenjang)}
	<a href="jml-mhs.php?thn_angkatan={$thn_angkatan}">Kembali</a>
<h2 style="font-family:'Trebuchet MS'">Program Studi {$data_mhs_prodi_jenjang[0].NM_PROGRAM_STUDI}</h2>
<table id="rounded-corner">
    <thead>
    	<tr>
			<th scope="col" class="rounded-company">NO</th>
            <th scope="col" class="rounded-company">NIM</th>
			<th scope="col" class="rounded-company">NAMA</th>
			<th scope="col" class="rounded-company">FAKULTAS</th>
			<th scope="col" class="rounded-company">PROGRAM STUDI</th>
			<th scope="col" class="rounded-company">STATUS AKADEMIK</th>
        </tr>
    </thead>
    <tbody>
	{$no = 1}
    {foreach $data_mhs_prodi_jenjang as $ps}
        <tr>
			<td>{$no++}</td>
			<td>{$ps.NIM_MHS}</td>
			<td>{$ps.NM_PENGGUNA}</td>
			<td>{$ps.NM_FAKULTAS}</td>
            <td>{$ps.NM_JENJANG} - {$ps.NM_PROGRAM_STUDI}</td>   
			<td>{$ps.NM_STATUS_PENGGUNA}</td>
        </tr>
    {/foreach}
    </tbody>
</table>

{else}
<table>
<tr>
 <form id="form1" name="form1" method="get" action="jml-mhs.php">
  <td> <select name="thn_angkatan">
   			<option value="">Semua</option>
			{foreach $data_tahun_akademik as $data}
			<option value="{$data.THN_AKADEMIK_SEMESTER}" {if $data.THN_AKADEMIK_SEMESTER == $thn_angkatan} selected="true"{/if}>
			{$data.THN_AKADEMIK_SEMESTER}</option>
			{/foreach}
   </select> </td>
   <td><input type="submit" name="Submit" value="Submit" /></td>
 </form>
</tr>
<tr>
 <form id="form1" name="form1" method="get" action="excel-jml-mhs.php">
 	<input type="hidden" name="thn_angkatan" value="{$thn_angkatan}" />
   <td><input type="submit" name="Submit" value="Excel" /></td>
 </form></tr>
</table>
 <div>
   <table border="1" id="rounded-corner">
    <tr>
		<th rowspan="2" style="text-align:center">
        	Fakultas		
		</th>
        <th rowspan="2" style="text-align:center">
        	Program Studi		
		</th>
		<th colspan="{count($data_jenjang)}" style="text-align:center">
        	Jenjang
		</th>
        <th scope="col" style="text-align: center" rowspan="2" colspan="2">Σ Per Prodi</th>
    </tr>
	<tr>
	{foreach $data_jenjang as $data}
      <th style="text-align:center">{$data.NM_JENJANG}</th>
	{/foreach}
    </tr>
    {foreach $data_fakultas_prodi as $data}
    <tr>
		<td>
        	{$data.FAKULTAS}        
		</td>
        <td>
        	{$data.NM_PROGRAM_STUDI}        
		</td>
        {$total_prodi_laki = 0}
		{foreach $data_jenjang as $jenj}
		<td>{$found = false}
			{foreach $data_mhs_aktif as $mhs_aktif}
				{if $data.ID_PROGRAM_STUDI == $mhs_aktif.ID_PROGRAM_STUDI and $mhs_aktif.ID_JENJANG == $jenj.ID_JENJANG}
					<span style="color:#0033FF; text-align:center"><a href="?prodi={$mhs_aktif.ID_PROGRAM_STUDI}&jenjang={$mhs_aktif.ID_JENJANG}&thn_angkatan={$thn_angkatan}">{$mhs_aktif.JUMLAH}</a></span>
					{$found = true}
                    {$total_prodi_laki = $total_prodi_laki + $mhs_aktif.JUMLAH}
					{break}
				{/if}
			{/foreach}
			{if $found == false}
                    <span style="text-align:center">0</span>
            {/if}
		</td>
		{/foreach}
        <td style="text-align: center; font-weight: bold">{$total_prodi_laki}</td>
    </tr>
    {/foreach}
    	<tr>
            <td colspan="2">TOTAL</td>
            {$total_all_laki = 0}
        {foreach $data_jenjang as $jenj}
            {$found = false}
            {$total_jenj_laki = 0}
			{foreach $data_mhs_aktif as $mhs_aktif}
				{if $mhs_aktif.ID_JENJANG == $jenj.ID_JENJANG}
                	{$total_jenj_laki = $total_jenj_laki + $mhs_aktif.JUMLAH}
					{$found = true}
                    {$total_all_laki = $total_all_laki + $mhs_aktif.JUMLAH}
				{/if}
			{/foreach}
			{if $found == false}
                    <td style="text-align: center; font-weight: bold">0</td>
            {else}
            		<td style="text-align: center; font-weight: bold">{$total_jenj_laki}</td>
            {/if}             
        {/foreach}
            <td style="text-align: center; font-weight: bold">{$total_all_laki}</td>
        </tr>
   </table>
</div>
{/if}