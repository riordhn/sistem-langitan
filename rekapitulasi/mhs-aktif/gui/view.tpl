<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informasi Mahasiswa Aktif Universitas Airlangga</title>
{literal}
<style type="text/css">
<!--
@import url("style.css");

.green { color: #080; }
.red { color: #f00; }
-->
</style>
    <script type="text/javascript" language="javascript" src="http://cybercampus.unair.ac.id/js/jquery-1.5.1.min.js"></script>
{/literal}
</head>
<body>
<div id="header"><div id="judul_header"></div></div>
<h1 style="text-align:center;color:#0e2d50;font-family:'Trebuchet MS'">Mahasiswa Aktif Universitas Airlangga</h1>
 <div style="overflow: scroll;width: 100%;height: 600px;">
   <table border="1" id="rounded-corner">
    <tr>
		<th rowspan="4" style="text-align:center">
        	Fakultas		
		</th>
        <th rowspan="4" style="text-align:center">
        	Program Studi		
		</th>
		<th colspan="{count($data_jenjang)*4}" style="text-align:center">
        	Jenjang
		</th>
        <th scope="col" style="text-align: center" rowspan="2" colspan="4">Σ Per Prodi</th>
    </tr>
	<tr>
	{foreach $data_jenjang as $data}
      <th colspan="4" style="text-align:center">{$data.NM_JENJANG}</th>
	{/foreach}
    </tr>
	<tr>
    {foreach $data_jenjang as $data}
      <th colspan="2" style="text-align: center">Baru</th>
      <th colspan="2" style="text-align: center">Lama</th>
    {/foreach}
    	<th colspan="2" style="text-align: center">Baru</th>
        <th colspan="2" style="text-align: center">Lama</th>
    </tr>
    <tr>
    {for $k=0 to count($data_jenjang)*2}
      <th>L</th>
      <th>P</th>
    {/for}
    	<th>L</th>
        <th>P</th>
    </tr>
    {foreach $data_fakultas_prodi as $data}
    <tr>
		<td>
        	{$data.FAKULTAS}        
		</td>
        <td>
        	{$data.NM_PROGRAM_STUDI}        
		</td>
        {$total_prodi_laki_baru = 0}
        {$total_prodi_perempuan_baru = 0}
        {$total_prodi_laki_lama = 0}
        {$total_prodi_perempuan_lama = 0}
		{for $k=0 to count($data_jenjang)-1}
        <td>
			{foreach $data_mhs_aktif as $mhs_aktif}
				{if $data.ID_PROGRAM_STUDI == $mhs_aktif.ID_PROGRAM_STUDI and $mhs_aktif.ID_JENJANG == $data_jenjang[$k]['ID_JENJANG'] and $mhs_aktif.THN == 'BARU'}
					<span style="color:#0033FF; text-align:center">{$mhs_aktif.LAKI}</span>
                    {$total_prodi_laki_baru = $total_prodi_laki_baru + $mhs_aktif.LAKI}
					{break}
				{/if}
			{/foreach}
		</td>
		<td>
			{foreach $data_mhs_aktif as $mhs_aktif}
				{if $data.ID_PROGRAM_STUDI == $mhs_aktif.ID_PROGRAM_STUDI and $mhs_aktif.ID_JENJANG == $data_jenjang[$k]['ID_JENJANG'] and $mhs_aktif.THN == 'BARU'}
					<span style="color:#0033FF; text-align:center">{$mhs_aktif.PEREMPUAN}</span>
                    {$total_prodi_perempuan_baru = $total_prodi_perempuan_baru + $mhs_aktif.PEREMPUAN}
					{break}
				{/if}
			{/foreach}
		</td>
        <td>
			{foreach $data_mhs_aktif as $mhs_aktif}
				{if $data.ID_PROGRAM_STUDI == $mhs_aktif.ID_PROGRAM_STUDI and $mhs_aktif.ID_JENJANG == $data_jenjang[$k]['ID_JENJANG'] and $mhs_aktif.THN == 'LAMA'}
					<span style="color:#0033FF; text-align:center">{$mhs_aktif.LAKI}</span>
                    {$total_prodi_laki_lama = $total_prodi_laki_lama + $mhs_aktif.LAKI}
					{break}
				{/if}
			{/foreach}
		</td>
        <td>
			{foreach $data_mhs_aktif as $mhs_aktif}
				{if $data.ID_PROGRAM_STUDI == $mhs_aktif.ID_PROGRAM_STUDI and $mhs_aktif.ID_JENJANG == $data_jenjang[$k]['ID_JENJANG'] and $mhs_aktif.THN == 'LAMA'}
					<span style="color:#0033FF; text-align:center">{$mhs_aktif.PEREMPUAN}</span>
                    {$total_prodi_perempuan_lama = $total_prodi_perempuan_lama + $mhs_aktif.PEREMPUAN}
					{break}
				{/if}
			{/foreach}
		</td>
		{/for}
        <td style="text-align: center; font-weight: bold">{$total_prodi_laki_baru}</td>
        <td style="text-align: center; font-weight: bold">{$total_prodi_perempuan_baru}</td>
        <td style="text-align: center; font-weight: bold">{$total_prodi_laki_lama}</td>
        <td style="text-align: center; font-weight: bold">{$total_prodi_perempuan_lama}</td>
    </tr>
    {/foreach}
    	<tr>
            <td colspan="2">TOTAL</td>
            {$total_all_perempuan_baru = 0}
            {$total_all_laki_baru = 0}
            {$total_all_perempuan_lama = 0}
            {$total_all_laki_lama = 0}
        {for $k=0 to count($data_jenjang)-1}
            {$total_jenj_laki_baru = 0}
			{$total_jenj_perempuan_baru = 0}
			{foreach $data_mhs_aktif as $mhs_aktif}
				{if $mhs_aktif.ID_JENJANG == $data_jenjang[$k]['ID_JENJANG'] and $mhs_aktif.THN == 'BARU'}
                	{$total_jenj_laki_baru = $total_jenj_laki_baru + $mhs_aktif.LAKI}
					{$total_all_laki_baru = $total_all_laki_baru + $mhs_aktif.LAKI}
					{$total_jenj_perempuan_baru = $total_jenj_perempuan_baru + $mhs_aktif.PEREMPUAN}
					{$total_all_perempuan_baru = $total_all_perempuan_baru + $mhs_aktif.PEREMPUAN}
				{/if}
			{/foreach}           
           	<td style="text-align: center; font-weight: bold">{$total_jenj_laki_baru}</td>
			<td style="text-align: center; font-weight: bold">{$total_jenj_perempuan_baru}</td>
			
            
            {$total_jenj_laki_lama = 0}
			{$total_jenj_perempuan_lama = 0}
			{foreach $data_mhs_aktif as $mhs_aktif}
				{if $mhs_aktif.ID_JENJANG == $data_jenjang[$k]['ID_JENJANG'] and $mhs_aktif.THN == 'LAMA'}
                	{$total_jenj_laki_lama = $total_jenj_laki_lama + $mhs_aktif.LAKI}
                    {$total_all_laki_lama = $total_all_laki_lama + $mhs_aktif.LAKI}
					{$total_jenj_perempuan_lama = $total_jenj_perempuan_lama + $mhs_aktif.PEREMPUAN}
					{$total_all_perempuan_lama = $total_all_perempuan_lama + $mhs_aktif.PEREMPUAN}
				{/if}
			{/foreach}			
            <td style="text-align: center; font-weight: bold">{$total_jenj_laki_lama}</td>
            <td style="text-align: center; font-weight: bold">{$total_jenj_perempuan_lama}</td>
        {/for}
            <td style="text-align: center; font-weight: bold; color:#F0F">{$total_all_laki_baru}</td>
            <td style="text-align: center; font-weight: bold; color:#F0F">{$total_all_perempuan_baru}</td>
            <td style="text-align: center; font-weight: bold; color:#F0F">{$total_all_laki_lama}</td>
            <td style="text-align: center; font-weight: bold; color:#F0F">{$total_all_perempuan_lama}</td>
        </tr>
   </table>
 </div>