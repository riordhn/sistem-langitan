<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informasi Mahasiswa Aktif</title>
{literal}
<style type="text/css">
<!--
@import url("style.css");

.green { color: #080; }
.red { color: #f00; }
-->

#header {
    background-color: #063A3A;
}

</style>
    <script type="text/javascript" language="javascript" src="/js/jquery-1.5.1.min.js"></script>
{/literal}
</head>
<body>
<div id="header" style="background: url('../../img/header/sarana-prasarana-{$nama_singkat}.png') no-repeat scroll center center transparent">

    <div id="judul_header"></div>
</div>
<h1 style="text-align:center;color:#0e2d50;font-family:'Trebuchet MS'">Mahasiswa Aktif</h1>
{if $id <> ''}
<div id="content">
<a href="javascript:history.back()">Kembali</a>
<h2 style="font-family:'Trebuchet MS'">Program Studi {$program_studi_set[0].NM_PROGRAM_STUDI}</h2>
<table id="rounded-corner">
    <tbody>
	
        <tr>	
		<td>THN SEMESTER / <br />
			STATUS MAHASISWA</td>
		{foreach $status_pengguna_set as $data}
            <td  style="text-align: center">{$data.NM_STATUS_PENGGUNA}</td>
		{/foreach}
		<td style="font-size: 13px;background-color: #ccffff;">&Sigma; Per Angkatan</td>
	    </tr>
        {for $k=0 to count($semester_set)-1}
		
        <tr>	
            <td>{$semester_set[$k]['THN_ANGKATAN_MHS']}</td>
			{for $j=0 to count($status_pengguna_set)-1}
			{$jumlah=$jumlah_mhs_set[$k+($j*count($semester_set))]['JUMLAH']}
			{$jumlah_per_angkatan[$k]=$jumlah_per_angkatan[$k]+$jumlah}
			{$jumlah_per_prodi[$j]=$jumlah_per_prodi[$j]+$jumlah}
			{if $jumlah <> 0}
				<td style="text-align: center"><font color="#CC3300">{$jumlah}</font></td>
			{else}
				<td style="text-align: center">{$jumlah}</td>
			{/if}
            
			{/for}
			<td style="font-size: 13px;background-color: #ccffff; text-align:center">{$jumlah_per_angkatan[$k]}</td>
	    </tr>
        {/for}
		<tr style="font-size: 13px;background-color: #ccffff;">
                <td>
                    &Sigma; Per Status
                </td>
                {for $i=0 to count($status_pengguna_set)-1}                    
					<td align="center">{$jumlah_per_prodi[$i]}</td>
					{$jumlah_all=$jumlah_all+$jumlah_per_prodi[$i]}
                {/for}
                <td style="background-color: #ffcccc; text-align:center">{$jumlah_all}</td>
            </tr>
    </tbody>
</table>
{literal}<script type="text/javascript">
    $(document).ready(function(){
        $('td[id^="subtotal"]').each(function(index) {
            if ($(this).text() == '0')
                $(this).parent().hide();
        });
    });
</script>{/literal}
</div>
{elseif $fak <> '' and $stat <> ''}

{literal}
    <script type="text/javascript">
        function get_link_report(){
            link=document.location.hash.replace('#mahasiswa-unair!', '');
                window.location.href='excel-mahasiswa-unair.PHP';
            }
    </script>
{/literal}

{elseif $fak <> ''}
<div id="content">
<a href="index.php">Kembali</a>
<h2 style="font-family:'Trebuchet MS'">Fakultas {$fakultas[0].NM_FAKULTAS}</h2>
<table id="rounded-corner">
    <thead>
    	<tr>
            <th scope="col" class="rounded-company">Program Studi /<br/> Status Mahasiswa</th>
        {foreach $status_pengguna_set as $sp}
            <th scope="col" class="rounded-q1" style="text-align: center">{$sp.NM_STATUS_PENGGUNA}</th>
        {/foreach}
            <th scope="col" class="rounded-q4" style="text-align: center">Σ Per Prodi</th>
        </tr>
    </thead>
    <tfoot>
    	<tr>
            <td colspan="{count($status_pengguna_set) + 1}" class="rounded-foot-left"><em>&nbsp;</em></td>
            <td class="rounded-foot-right">&nbsp;</td>
        </tr>
    </tfoot>
    <tbody>
    {foreach $program_studi_set as $ps}
        <tr>
            <td><a href="?id={$ps.ID_PROGRAM_STUDI}">{$ps.NM_JENJANG} - {$ps.NM_PROGRAM_STUDI}</a></td>    
            {$total_prodi = 0}
        {foreach $status_pengguna_set as $sp}
            <td style="text-align: center">{$found = false}
                {foreach $matrix_set as $m}
                    {if $m.ID_STATUS_PENGGUNA == $sp.ID_STATUS_PENGGUNA and $m.ID_PROGRAM_STUDI == $ps.ID_PROGRAM_STUDI}
                        <span>
							<a href="excel-mahasiswa-unair.php?stat={$sp.ID_STATUS_PENGGUNA}&prod={$ps.ID_PROGRAM_STUDI}">{$m.JUMLAH}</a>
						</span>
                        {$found = true}
                        {$total_prodi = $total_prodi + $m.JUMLAH}
                        {$status_pengguna_set[$sp@index]['JUMLAH'] = $status_pengguna_set[$sp@index]['JUMLAH'] + $m.JUMLAH}
                    {/if}
                {/foreach}
                {if $found == false}
                    <span>0</span>
                {/if}
            </td>
        {/foreach}
            <td style="text-align: center; font-weight: bold">{$total_prodi}</td>
        </tr>
    {/foreach}
        <tr>
            <td>TOTAL</td>
            {$total_all = 0}
        {foreach $status_pengguna_set as $sp}
            <td style="text-align: center; font-weight: bold">{if $sp.JUMLAH > 0}{$sp.JUMLAH}{else}0{/if}</td>
            {$total_all = $total_all + $sp.JUMLAH}
        {/foreach}
            <td style="text-align: center; font-weight: bold">{$total_all}</td>
        </tr>
    </tbody>
</table>
</div>
{else}
<div id="content">
<table id="rounded-corner">
<tbody>
        <tr>
            <td>Fakultas / Status</td>
			{foreach $status_pengguna_set as $data}
            <td  style="text-align: center">{$data.NM_STATUS_PENGGUNA}</td>
			{/foreach}
			<td>&Sigma; Per Fakultas</td>
		</tr>
{for $i=0 to count($fakultas_set)-1}
        <tr>
            <td><a href="?fak={$fakultas_set[$i]['ID_FAKULTAS']}">{$fakultas_set[$i]['NM_FAKULTAS']}</a></td>
                {for $j=0 to count($status_pengguna_set)-1}                    
                    {$jumlah=$fakultas_pengguna_set[$i+($j*count($fakultas_set))]['JUMLAH']}
                    {$jumlah_per_angkatan[$i]=$jumlah_per_angkatan[$i]+$jumlah}
                    {$jumlah_per_prodi[$j]=$jumlah_per_prodi[$j]+$jumlah}

                    {if $jumlah <> 0}
                        <td style="text-align: center"><font color="#CC3300">
                 			<a href="excel-mahasiswa-unair.php?stat={$status_pengguna_set[$j]['ID_STATUS_PENGGUNA']}&fak={$fakultas_set[$i]['ID_FAKULTAS']}">
                        	{$jumlah}</a>
                        </font></td>
                    {else}
                        <td style="text-align: center">{$jumlah}</td>
                    {/if}

                {/for}
            <td style="font-size: 13px;background-color: #ccffff; text-align:center">{$jumlah_per_angkatan[$i]}</td>
        </tr>
{/for}

	<tr style="font-size: 13px;background-color: #ccffff;">
                <td>
                    &Sigma; Per Status
                </td>
                {for $i=0 to count($status_pengguna_set)-1}                    
					<td align="center">{$jumlah_per_prodi[$i]}</td>
					{$jumlah_all=$jumlah_all+$jumlah_per_prodi[$i]}
                {/for}
                <td style="background-color: #ffcccc; text-align:center">{$jumlah_all}</td>
   </tr>
</tbody>
</table>
</div>
{/if}
</body>
</html>