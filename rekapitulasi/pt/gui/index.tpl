<!DOCTYPE html>
<html>
	<head>
		<title>Pangkalan Data Perguruan Tinggu NU</title>
		<meta name="robots" value="noindex" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

		<!-- Bootstrap -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
		<link href="//fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">

		<style>
			.table thead tr th {
				vertical-align: middle;
			}
			
			th.text-center, td.text-center {
				text-align: center;
			}

			/* Font Custom */
			body, input, button, select, textarea, .navbar-search .search-query {
				font-family: 'Open Sans', Helvetica, Arial, sans-serif;
				font-size: 0.9em;
			}
		</style>
	</head>

	<body>
	<div class="container cybercampus-header content-wrapper">
	<div class="page-header">
		<h1>Pangkalan Data Perguruan Tinggi NU</h1>
	</div>
	{if !isset($pt_prodi_set) AND !isset($pt_staff_set) AND !isset($pt_dosen_lb) AND !isset($pt_dosen_tetap) AND !isset($pt_mahasiswa)}
	<table class="table table-bordered table-condensed table-striped">
		<thead>
			<tr>
				<th class="text-center">No</th>
				<th class="text-center">Nama Perguruan Tinggi</th>
				<th class="text-center">Jumlah Program Studi</th>
				<th class="text-center">Jumlah Dosen LB</th>
				<th class="text-center">Jumlah Dosen Tetap</th>
				<th class="text-center">Total Dosen</th>
				<th class="text-center">Jumlah Staff</th>
				<th class="text-center">Jumlah Mahasiswa</th>
				<th class="text-center">Mahasiswa Aktif</th>
				<th class="text-center">Nilai Kosong</th>
			</tr>
		</thead>
		<tbody>
			{foreach $pt_set as $pt}
				<tr>
					<td class="text-center">{$pt@index + 1}</td>
					<td><strong>{$pt.NAMA_PERGURUAN_TINGGI}</strong><br/> (<a href="http://{$pt.HTTP_HOST}" target="_blank">{$pt.HTTP_HOST}</a>)</td>
					<td class="text-center">
						<a href="index.php?id_pt_prodi={$pt.ID_PERGURUAN_TINGGI}">{$pt.PRODI_JUMLAH}</a>
					</td>
					<td class="text-center"><a href="index.php?dosen_lb={$pt.ID_PERGURUAN_TINGGI}">{$pt.DOSEN_JUMLAH_LB}</a></td>
					<td class="text-center"><a href="index.php?dosen_tetap={$pt.ID_PERGURUAN_TINGGI}">{$pt.DOSEN_JUMLAH_TETAP}</a></td>
					<td class="text-center">{$pt.DOSEN_JUMLAH}</td>
					<td class="text-center">
						<a href="index.php?id_pt_staff={$pt.ID_PERGURUAN_TINGGI}">{$pt.PEGAWAI_JUMLAH}</a>
					</td>
					<td class="text-center"><a href="index.php?mahasiswa={$pt.ID_PERGURUAN_TINGGI}">{$pt.MHS_JUMLAH}</a></td>
					<td class="text-center">
						<a target="_blank" href="http://{$pt.HTTP_HOST}/rekapitulasi/mhs-aktif/">Detail mhs aktif</a>
					</td>
					<td>--</td>
				</tr>
			{/foreach}
		</tbody>
	</table>

	<table class="table table-bordered table-condensed table-striped">
		<thead>
			<tr>
				<th class="text-center">No</th>
				<th class="text-center">Nama Perguruan Tinggi</th>
				<th class="text-center">Beban SKS</th>
				<th class="text-center">Jam Kuliah</th>
				<th class="text-center">Semester</th>
				<th class="text-center">SKS Wajib</th>
				<th class="text-center">Kalender Akademik</th>
			</tr>
		</thead>
		<tbody>
			{foreach $pt_pendidikan_set as $pt_pendidikan}
				<tr>
					<td class="text-center">{$pt_pendidikan@index + 1}</td>
					<td><strong>{$pt_pendidikan.NAMA_PERGURUAN_TINGGI}</strong></td>
					<td class="text-center">{if {$pt_pendidikan.BEBAN_SKS_MIN} == 0}<i style="color: red">Belum Lengkap</i>{else}OK{/if}</td>
					<td class="text-center">{if {$pt_pendidikan.JADWAL_JAM_MIN} == 0}<i style="color: red">Belum Lengkap</i>{else}OK{/if}</td>
					<td class="text-center">{if {$pt_pendidikan.SEMESTER_MIN} == 0}<i style="color: red">Belum Ada</i>{else}{$pt_pendidikan.SEMESTER_MIN} Data{/if}</td>
					<td class="text-center">{if {$pt_pendidikan.SKS_WAJIB_MIN} == 0}<i style="color: red">Belum Lengkap</i>{else}OK{/if}</td>
					<td class="text-center">{if {$pt_pendidikan.JADWAL_KEGIATAN_MIN} == 0}<i style="color: red">Belum Lengkap</i>{else}OK{/if}</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	{/if}

	{if isset($pt_prodi_set)}
		<table class="table table-bordered table-condensed table-striped">
			<thead>
				<tr>
					<th class="text-center">No</th>
					<th class="text-center">Nama Prodi</th>
					<th class="text-center">Jumlah Mahasiswa</th>
					<th class="text-center">Jumlah Dosen</th>
					<th class="text-center">Jumlah Kurikulum Prodi</th>
				</tr>
			</thead>
			<tbody>
				{foreach $pt_prodi_set as $pt_prodi}
					<tr>
						<td class="text-center">{$pt_prodi@index + 1}</td>
						<td><strong>{$pt_prodi.NM_JENJANG} - {$pt_prodi.NM_PROGRAM_STUDI}</strong></td>
						<td class="text-center">{$pt_prodi.MHS_JUMLAH}</td>
						<td class="text-center">{$pt_prodi.DOSEN_JUMLAH}</td>
						<td class="text-center">{$pt_prodi.KURIKULUM_PRODI_JUMLAH}</td>
					</tr>
				{/foreach}
			</tbody>
		</table>

	{else if isset($pt_staff_set)}
		<table class="table table-bordered table-condensed table-striped">
			<thead>
				<tr>
					<th class="text-center">No</th>
					<th class="text-center">Nama Unit Kerja</th>
					<th class="text-center">Jumlah Staff</th>
				</tr>
			</thead>
			<tbody>
				{foreach $pt_staff_set as $pt_staff}
					<tr>
						<td class="text-center">{$pt_staff@index + 1}</td>
						<td><strong>{$pt_staff.NM_UNIT_KERJA}</strong></td>
						<td class="text-center">{$pt_staff.PEGAWAI_JUMLAH}</td>
					</tr>
				{/foreach}
			</tbody>
		</table>

	{else if isset($pt_dosen_lb)}
		<table class="table table-bordered table-condensed table-striped">
			<thead>
				<tr>
					<th class="text-center">No</th>
					<th class="text-center">Nama</th>
					<th class="text-center">Prodi</th>
					<th class="text-center">NIDN/NUPTK</th>
				</tr>
			</thead>
			<tbody>
				{foreach $pt_dosen_lb as $pt_dosen}
					<tr>
						<td class="text-center">{$pt_dosen@index + 1}</td>
						<td><strong>{$pt_dosen.NM_PENGGUNA}</strong></td>
						<td>{$pt_dosen.NM_PROGRAM_STUDI}</td>
						<td class="text-center">{$pt_dosen.NIDN_DOSEN}</td>
					</tr>
				{/foreach}
			</tbody>
		</table>

	{else if isset($pt_dosen_tetap)}
		<table class="table table-bordered table-condensed table-striped">
			<thead>
				<tr>
					<th class="text-center">No</th>
					<th class="text-center">Nama</th>
					<th class="text-center">Prodi</th>
					<th class="text-center">NIDN/NUPTK</th>
				</tr>
			</thead>
			<tbody>
				{foreach $pt_dosen_tetap as $pt_dosen}
					<tr>
						<td class="text-center">{$pt_dosen@index + 1}</td>
						<td><strong>{$pt_dosen.NM_PENGGUNA}</strong></td>
						<td>{$pt_dosen.NM_PROGRAM_STUDI}</td>
						<td class="text-center">{$pt_dosen.NIDN_DOSEN}</td>
					</tr>
				{/foreach}
			</tbody>
		</table>

		{else if isset($pt_mahasiswa)}
		<table class="table table-bordered table-condensed table-striped">
			<thead>
				<tr>
					<th class="text-center">No</th>
					<th class="text-center">NIM</th>
					<th class="text-center">Nama</th>
					<th class="text-center">Prodi</th>
					<th class="text-center">Angkatan</th>
				</tr>
			</thead>
			<tbody>
				{foreach $pt_mahasiswa as $mahasiswa}
					<tr>
						<td class="text-center">{$mahasiswa@index + 1}</td>
						<td><strong>{$mahasiswa.NIM_MHS}</strong></td>
						<td>{$mahasiswa.NM_PENGGUNA}</td>
						<td class="text-center">{$mahasiswa.NM_PROGRAM_STUDI}</td>
						<td class="text-center">{$mahasiswa.THN_ANGKATAN_MHS}</td>
					</tr>
				{/foreach}
			</tbody>
		</table>
	{/if}		

	</div>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script>
		<script src="http://langitan.umaha.ac.id/rekapitulasi/ppmb/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		{block name="script"}{/block}
	</body>
</html>