<?php
require '../../config.php';

// S1
$id_jenjang = 1;
$id_perguruan_tinggi = 1;

$semester_set = $db->QueryToArray(
	"SELECT id_semester, thn_akademik_semester, nm_semester FROM semester WHERE id_perguruan_tinggi = {$id_perguruan_tinggi} 
	ORDER BY thn_akademik_semester DESC, nm_semester DESC");
	
foreach ($semester_set as &$semester)
{
	if ($semester['NM_SEMESTER'] == 'Ganjil')
	{
		$semester['semester_1'] = $semester['THN_AKADEMIK_SEMESTER'];
		$semester['semester_3'] = $semester['THN_AKADEMIK_SEMESTER'] - 1;
		$semester['semester_5'] = $semester['THN_AKADEMIK_SEMESTER'] - 2;
		$semester['semester_7'] = $semester['THN_AKADEMIK_SEMESTER'] - 3;
	}

	if ($semester['NM_SEMESTER'] == 'Genap')
	{
		$semester['semester_2'] = $semester['THN_AKADEMIK_SEMESTER'];
		$semester['semester_4'] = $semester['THN_AKADEMIK_SEMESTER'] - 1;
		$semester['semester_6'] = $semester['THN_AKADEMIK_SEMESTER'] - 2;
		$semester['semester_8'] = $semester['THN_AKADEMIK_SEMESTER'] - 3;
	}

	$db->Query(
		"SELECT COUNT(*) FROM mahasiswa m 
		join pengguna p on p.id_pengguna = m.id_pengguna
		join program_studi ps on ps.id_program_studi = m.id_program_studi and ps.id_jenjang = {$id_jenjang} 
		where 
			p.id_perguruan_tinggi = {$id_perguruan_tinggi} and 
			(m.thn_angkatan_mhs between {$semester['THN_AKADEMIK_SEMESTER']} - 3 and {$semester['THN_AKADEMIK_SEMESTER']})");
	$semester['jumlah_mhs'] = $db->FetchRow()[0];
}

echo '<pre>' . print_r($semester_set, true) . '</pre>';