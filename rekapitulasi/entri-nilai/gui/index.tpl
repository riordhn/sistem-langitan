<!DOCTYPE html>
<html>
	<head>
		<title>Rekapitulasi Entri Nilai per Semester</title>
		<meta name="robots" value="noindex" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

		<!-- Bootstrap -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
		<link href="//fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">

		<style>
			.table thead tr th {
				vertical-align: middle;
			}
			
			th.text-center, td.text-center {
				text-align: center;
			}

			/* Font Custom */
			body, input, button, select, textarea, .navbar-search .search-query {
				font-family: 'Open Sans', Helvetica, Arial, sans-serif;
				font-size: 0.9em;
			}
		</style>
	</head>

	<body>
	<div class="container cybercampus-header content-wrapper">
	<div class="page-header">
		<h1>Monitoring Entri Nilai</h1>
		{if !empty($nm_prodi)}
		<h3>Program Studi {$nm_prodi}</h3>
		{/if}
	</div>
	<form action="index.php" method="get">
    <table class="table table-bordered table-condensed table-striped">
        <tbody>
            <tr>
                <td>
                	<strong>Semester</strong> 
                <td>
                <center>:</center>
                </td>
                <td>
                    <select name="smt">
                        {foreach $semester_set as $s}
                            <option value="{$s.ID_SEMESTER}" 
                                    {if empty($smarty.get.smt)}
                                        {if $s.STATUS_AKTIF_SEMESTER == 'True'}selected{/if}
                                    {else}
                                        {if $s.ID_SEMESTER == $smarty.get.smt}selected{/if}
                                    {/if}
                                    >{$s.SEMESTER} {if $s.STATUS_AKTIF_SEMESTER == 'True'}(AKTIF){/if}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
            	
                <td colspan="3">
                    <center><input type="submit" value="Lihat"/></center>
                </td>
            </tr>
        </tbody>
    </table>
	</form>

	{if isset($prodi_nilai_set)}
	<table class="table table-bordered table-condensed">
		<thead>
			<tr>
				<th class="text-center">No</th>
				<th class="text-center">Program Studi</th>
				<th class="text-center">Jumlah Kelas</th>
				<th class="text-center">Nilai Masuk</th>
			</tr>
		</thead>
		<tbody>
			{foreach $prodi_nilai_set as $pt}
				<!-- <tr {if ($pt.RATA_NILAI * 100) < 100}style="background: lightpink"{/if}> -->
				<tr>
					<td class="text-center">{$pt@index + 1}</td>
					<td><a href="index.php?prodi={$pt.ID_PROGRAM_STUDI}&smt={$smarty.get.smt}"><strong>{$pt.PRODI}</strong></a></td>
					<td class="text-center">
						{$pt.JML_KELAS}
					</td>
					<td class="text-center">
						{($pt.RATA_NILAI * 100)|number_format:2:",":"."} %
					</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	{/if}

	{if isset($monitoring_nilai_set)}
	<a href="index.php?smt={$smarty.get.smt}"><< Kembali Ke Pilihan Prodi</a>
	<br/>
	<br/>
	<table class="table table-bordered table-condensed">
		<thead>
			<tr>
				<th class="text-center">No</th>
				<th class="text-center">Kode MK</th>
				<th class="text-center">Nama MK</th>
				<th class="text-center">Semester</th>
				<th class="text-center">Kelas</th>
				<th class="text-center">Jadwal</th>
				<th class="text-center">Nama PJMK</th>
				<th class="text-center">Status</th>
			</tr>
		</thead>
		<tbody>
			{foreach $monitoring_nilai_set as $pt}
				{if $pt.JML_ALL_PENGAMBILAN == 0}
				<tr style="background: red">
				{elseif (($pt.JML_NILAI_PENGAMBILAN / $pt.JML_ALL_PENGAMBILAN) * 100) < 100}
				<tr style="background: lightpink">
				{else}
				<tr>
				{/if}
					<td class="text-center">{$pt@index + 1}</td>
					<td><strong>{$pt.KD_MATA_KULIAH}</strong></td>
					<td>
						{$pt.NM_MATA_KULIAH}
					</td>
					<td class="text-center">
						{$pt.TINGKAT_SEMESTER}
					</td>
					<td class="text-center">
						{$pt.NAMA_KELAS}
					</td>
					<td class="text-center">
						{$pt.JADWAL}
					</td>
					<td class="text-center">
						{$pt.NAMA_PJMK}
					</td>
					<td class="text-center">
						{if $pt.JML_ALL_PENGAMBILAN == 0}
						Tanpa MHS
						{else}
						{(($pt.JML_NILAI_PENGAMBILAN / $pt.JML_ALL_PENGAMBILAN) * 100)|number_format:2:",":"."} %
						{/if}
					</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	{/if}

	</div>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script>
		<script src="http://langitan.umaha.ac.id/rekapitulasi/ppmb/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		{block name="script"}{/block}
	</body>
</html>