<?php
require '../../config.php';

	$PT = new PerguruanTinggi($db);

	// Memastikan mempunyai id perguruan tinggi
	if ($PT->is_exist == false) { echo 'Perguruan Tinggi belum di set.'; trigger_error('Perguruan Tinggi belum di set.'); exit(); }

	$id_pt = $PT->id_perguruan_tinggi;
	$nama_singkat = $PT->nama_singkat;
	$nama_pt = $PT->nama_pt;
	$alamat = $PT->alamat;
	$kota_pt = $PT->kota_pt;
	$telp_pt = $PT->telp_pt;
	$fax_pt = $PT->fax_pt;
	$web_email_pt = $PT->web_email;
	$web_elearning = $PT->web_elearning;
	$web_pmb = $PT->web_pmb;


	// Inisialisasi smarty
	$smarty = new Smarty();
	$smarty->caching = Smarty::CACHING_OFF;
	$smarty->setTemplateDir('gui');
	$smarty->setCompileDir('gui_c');

	$smarty->assign("id_pt",$PT->id_perguruan_tinggi);
	$smarty->assign("nama_pt",$PT->nama_pt);
	$smarty->assign("base_url",$PT->base_url);
	$smarty->assign("nama_singkat",$PT->nama_singkat);
	$smarty->assign("alamat",$PT->alamat);
	$smarty->assign("kota_pt",$PT->kota_pt);
	$smarty->assign("web_email_pt",$PT->web_email);
	$smarty->assign("web_elearning",$PT->web_elearning);
	$smarty->assign("web_pmb",$PT->web_pmb);

	function get($name, $default_value = '')
	{
	    return isset($_GET[$name]) ? $_GET[$name] : $default_value;
	}

	$id_program_studi = get('prodi', '');
	$id_semester = (int)get('smt', '');

	$prodi_set = $db->QueryToArray("SELECT ps.ID_PROGRAM_STUDI, j.NM_JENJANG || ' - ' || ps.NM_PROGRAM_STUDI AS PRODI
										FROM PROGRAM_STUDI ps
										JOIN FAKULTAS f ON f.ID_FAKULTAS = ps.ID_FAKULTAS
										JOIN JENJANG j ON j.ID_JENJANG = ps.ID_JENJANG
										WHERE f.ID_PERGURUAN_TINGGI = '{$id_pt}'
																	");
	$smarty->assignByRef('prodi_set', $prodi_set);

	$prodi_set = $db->Query("SELECT ps.ID_PROGRAM_STUDI, j.NM_JENJANG || ' - ' || ps.NM_PROGRAM_STUDI AS PRODI
										FROM PROGRAM_STUDI ps
										JOIN FAKULTAS f ON f.ID_FAKULTAS = ps.ID_FAKULTAS
										JOIN JENJANG j ON j.ID_JENJANG = ps.ID_JENJANG
										WHERE ps.ID_PROGRAM_STUDI = '{$id_program_studi}'
																	");
	$prodi = $db->FetchAssoc();
	$nm_prodi = $prodi['PRODI'];

	$smarty->assignByRef('nm_prodi', $nm_prodi);

	$smarty->assignByRef('prodi_set', $prodi_set);

	$semester_set = $db->QueryToArray("SELECT DISTINCT ID_SEMESTER, NM_SEMESTER || ' (' || TAHUN_AJARAN || ')' AS SEMESTER , NM_SEMESTER, THN_AKADEMIK_SEMESTER, STATUS_AKTIF_SEMESTER 
										FROM SEMESTER
										WHERE ID_PERGURUAN_TINGGI = '{$id_pt}' 
										AND THN_AKADEMIK_SEMESTER IN (select to_char(add_months(trunc(sysdate, 'yyyy'), -12*(level -1)), 'yyyy') yr
											from   dual
											connect by level <= 7)
										AND NM_SEMESTER IN ('Ganjil','Genap')
										ORDER BY THN_AKADEMIK_SEMESTER, NM_SEMESTER
																	");
	$smarty->assignByRef('semester_set', $semester_set);

	if($id_semester != '' && $id_program_studi == ''){
		$prodi_nilai_set = $db->QueryToArray("SELECT id_program_studi, prodi, count(id_kelas_mk) as jml_kelas, avg(jml_nilai_pengambilan/jml_all_pengambilan) as rata_nilai
										FROM (
										SELECT
															kmk.id_kelas_mk, ps.id_program_studi, j.nm_jenjang || ' '|| ps.nm_program_studi AS prodi, kd_mata_kuliah, nm_mata_kuliah, kur.tingkat_semester,
															nk.nama_kelas,
															(select count(*) from jadwal_kelas jdw where jdw.id_kelas_mk = kmk.id_kelas_mk) as jml_jadwal,
															kur.status_mkta,
															hari.nm_jadwal_hari||', '||jam.jam_mulai||':'||jam.menit_mulai||' - '||jam.jam_selesai||':'||jam.menit_selesai||' (' || nm_ruangan || ')' as jadwal,

															(select count(*) from pengambilan_mk pmk where pmk.id_kelas_mk = kmk.id_kelas_mk and pmk.status_apv_pengambilan_mk = 1) as jml_all_pengambilan,
															(select count(*) from pengambilan_mk pmk where pmk.id_kelas_mk = kmk.id_kelas_mk and pmk.nilai_angka is not null and pmk.nilai_huruf is not null and pmk.status_apv_pengambilan_mk = 1) as jml_nilai_pengambilan
											FROM KELAS_MK kmk
														LEFT JOIN jadwal_kelas jdw ON jdw.id_kelas_mk = kmk.id_kelas_mk
														LEFT JOIN jadwal_hari hari ON hari.id_jadwal_hari = jdw.id_jadwal_hari
														LEFT JOIN jadwal_jam jam ON jam.id_jadwal_jam = jdw.id_jadwal_jam
														LEFT JOIN ruangan r ON r.id_ruangan = jdw.id_ruangan
														LEFT JOIN nama_kelas nk ON nk.id_nama_kelas = kmk.no_kelas_mk
														JOIN kurikulum_mk kur ON kur.id_kurikulum_mk = kmk.id_kurikulum_mk
														JOIN mata_kuliah mk ON mk.id_mata_kuliah = kur.id_mata_kuliah
														JOIN program_studi ps ON ps.id_program_studi = kmk.id_program_studi
														JOIN jenjang j on j.id_jenjang = ps.id_jenjang
														JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
											WHERE
															f.id_perguruan_tinggi = '{$id_pt}' AND
															kmk.id_semester = '{$id_semester}'
										)
										t1
										WHERE jml_all_pengambilan > 0
										GROUP BY id_program_studi, prodi
										ORDER BY id_program_studi
																			");
		$smarty->assignByRef('prodi_nilai_set', $prodi_nilai_set);
	}

	if ($id_program_studi != '' && $id_semester != '')
	{
		if($id_program_studi == 'all'){
			$monitoring_nilai_set = $db->QueryToArray("SELECT
															kmk.id_kelas_mk, kd_mata_kuliah, nm_mata_kuliah, kur.tingkat_semester,
															nk.nama_kelas,
															(select count(*) from jadwal_kelas jdw where jdw.id_kelas_mk = kmk.id_kelas_mk) as jml_jadwal,
															kur.status_mkta,
															hari.nm_jadwal_hari||', '||jam.jam_mulai||':'||jam.menit_mulai||' - '||jam.jam_selesai||':'||jam.menit_selesai||' (' || nm_ruangan || ')' as jadwal,
															nvl2(pjmk_gelar_depan,pjmk_gelar_depan||' ','')||initcap(nama_pjmk)||nvl2(pjmk_gelar_belakang,', '||pjmk_gelar_belakang,'') as nama_pjmk,

															(select count(*) from pengambilan_mk pmk where pmk.id_kelas_mk = kmk.id_kelas_mk and pmk.status_apv_pengambilan_mk = 1) as jml_all_pengambilan,
															(select count(*) from pengambilan_mk pmk where pmk.id_kelas_mk = kmk.id_kelas_mk and pmk.nilai_angka is not null and pmk.nilai_huruf is not null and pmk.status_apv_pengambilan_mk = 1) as jml_nilai_pengambilan
											FROM KELAS_MK kmk
														LEFT JOIN jadwal_kelas jdw ON jdw.id_kelas_mk = kmk.id_kelas_mk
														LEFT JOIN jadwal_hari hari ON hari.id_jadwal_hari = jdw.id_jadwal_hari
														LEFT JOIN jadwal_jam jam ON jam.id_jadwal_jam = jdw.id_jadwal_jam
														LEFT JOIN ruangan r ON r.id_ruangan = jdw.id_ruangan
														LEFT JOIN nama_kelas nk ON nk.id_nama_kelas = kmk.no_kelas_mk
														JOIN kurikulum_mk kur ON kur.id_kurikulum_mk = kmk.id_kurikulum_mk
														JOIN mata_kuliah mk ON mk.id_mata_kuliah = kur.id_mata_kuliah
														LEFT JOIN (
															SELECT pmk.id_kelas_mk, p.nm_pengguna as nama_pjmk, p.gelar_depan as pjmk_gelar_depan, p.gelar_belakang as pjmk_gelar_belakang
															FROM pengampu_mk pmk
															JOIN dosen d ON d.id_dosen = pmk.id_dosen
															JOIN pengguna p ON p.id_pengguna = d.id_pengguna
															WHERE pjmk_pengampu_mk = 1) pjmk on pjmk.id_kelas_mk = kmk.id_kelas_mk
														JOIN program_studi ps ON ps.id_program_studi = kmk.id_program_studi
														JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
											WHERE
															f.id_perguruan_tinggi = '{$id_pt}' AND
															kmk.id_semester = '{$id_semester}'
											ORDER BY kur.tingkat_semester, nk.nama_kelas, kd_mata_kuliah, kmk.id_program_studi
																			");
		}
		else{
			$monitoring_nilai_set = $db->QueryToArray("SELECT
															kmk.id_kelas_mk, kd_mata_kuliah, nm_mata_kuliah, kur.tingkat_semester,
															nk.nama_kelas,
															(select count(*) from jadwal_kelas jdw where jdw.id_kelas_mk = kmk.id_kelas_mk) as jml_jadwal,
															kur.status_mkta,
															hari.nm_jadwal_hari||', '||jam.jam_mulai||':'||jam.menit_mulai||' - '||jam.jam_selesai||':'||jam.menit_selesai||' (' || nm_ruangan || ')' as jadwal,
															nvl2(pjmk_gelar_depan,pjmk_gelar_depan||' ','')||initcap(nama_pjmk)||nvl2(pjmk_gelar_belakang,', '||pjmk_gelar_belakang,'') as nama_pjmk,

															(select count(*) from pengambilan_mk pmk where pmk.id_kelas_mk = kmk.id_kelas_mk and pmk.status_apv_pengambilan_mk = 1) as jml_all_pengambilan,
															(select count(*) from pengambilan_mk pmk where pmk.id_kelas_mk = kmk.id_kelas_mk and pmk.nilai_angka is not null and pmk.nilai_huruf is not null and pmk.status_apv_pengambilan_mk = 1) as jml_nilai_pengambilan
											FROM KELAS_MK kmk
														LEFT JOIN jadwal_kelas jdw ON jdw.id_kelas_mk = kmk.id_kelas_mk
														LEFT JOIN jadwal_hari hari ON hari.id_jadwal_hari = jdw.id_jadwal_hari
														LEFT JOIN jadwal_jam jam ON jam.id_jadwal_jam = jdw.id_jadwal_jam
														LEFT JOIN ruangan r ON r.id_ruangan = jdw.id_ruangan
														LEFT JOIN nama_kelas nk ON nk.id_nama_kelas = kmk.no_kelas_mk
														JOIN kurikulum_mk kur ON kur.id_kurikulum_mk = kmk.id_kurikulum_mk
														JOIN mata_kuliah mk ON mk.id_mata_kuliah = kur.id_mata_kuliah
														LEFT JOIN (
															SELECT pmk.id_kelas_mk, p.nm_pengguna as nama_pjmk, p.gelar_depan as pjmk_gelar_depan, p.gelar_belakang as pjmk_gelar_belakang
															FROM pengampu_mk pmk
															JOIN dosen d ON d.id_dosen = pmk.id_dosen
															JOIN pengguna p ON p.id_pengguna = d.id_pengguna
															WHERE pjmk_pengampu_mk = 1) pjmk on pjmk.id_kelas_mk = kmk.id_kelas_mk
														JOIN program_studi ps ON ps.id_program_studi = kmk.id_program_studi
														JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
											WHERE
															f.id_perguruan_tinggi = '{$id_pt}' AND
															kmk.id_program_studi = '{$id_program_studi}' AND
															kmk.id_semester = '{$id_semester}'
											ORDER BY kur.tingkat_semester, nk.nama_kelas, kd_mata_kuliah
																			");
		}


		$smarty->assignByRef('monitoring_nilai_set', $monitoring_nilai_set);
	}

$smarty->display("index.tpl");