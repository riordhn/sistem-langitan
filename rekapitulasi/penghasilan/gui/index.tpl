<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informasi Mahasiswa Aktif Universitas Airlangga</title>
{literal}
<style type="text/css">
<!--
@import url("style.css");

.green { color: #080; }
.red { color: #f00; }
-->
</style>
    <script type="text/javascript" language="javascript" src="http://cybercampus.unair.ac.id/js/jquery-1.5.1.min.js"></script>
{/literal}
</head>
<body>
<div id="header"><div id="judul_header"></div></div>
<h1 style="text-align:center;color:#0e2d50;font-family:'Trebuchet MS'">Penghasilan Orang Tua Mahasiswa Universitas Airlangga</h1>

{if $fak <> ''}
<table id="rounded-corner" border="1">
<tbody>
        <tr>
        	<th style="text-align: center">NO</th>
            <th style="text-align: center">NIM</th>
            <th style="text-align: center">NAMA</th>
            <th style="text-align: center">FAKULTAS</th>
            <th style="text-align: center">PROGRAM STUDI</th>
            <th style="text-align: center">PENGHASILAN ORTU</th>
            <th style="text-align: center">JALUR</th>
            <th style="text-align: center">KELOMPOK BIAYA</th>
		</tr>
        {foreach $detail_penghasilan_set as $data}
        	<tr>
				<td>{($data@index)+1}</td>
            	<td>{$data.NIM_MHS}</td>
                <td>{$data.NM_PENGGUNA}</td>
                <td>{$data.NM_FAKULTAS}</td>
                <td>{$data.PRODI}</td>
                <td>{$data.PENGHASILAN}</td>
                <td>{$data.NM_JALUR}</td>
                <td>{$data.NM_KELOMPOK_BIAYA}</td>
            </tr>
        {/foreach}
        <tr>
        	<td colspan="{count($detail_penghasilan_set)+1}" style="text-align:center">
            <form action="excel-mahasiswa-unair.php" method="post">
            	  <input type="hidden" value="{$fak}" name="fak">
               	  <input type="submit" name="button" id="button" value="Excel" />
           	</form>
            </td>
        </tr>
</tbody>
</table>
{else}
<div id="content">
<table id="rounded-corner" border="1">
<tbody>
        <tr>
            <th>Fakultas / Penghasilan</th>
			{foreach $penghasilan_set as $data}
            <th style="text-align: center">{$data.PENGHASILAN}</th>
			{/foreach}
		</tr>
{foreach $fakultas_set as $data}
        <tr>
            <td><a href="?fak={$data.ID_FAKULTAS}">{$data.NM_FAKULTAS}</a></td>
			{foreach $penghasilan_set as $data2}
				<td style="text-align:center">
                {$nilai = false}
				{foreach $penghasilan_ortu_set as $penghasilan_ortu}
					{if $penghasilan_ortu.ID_FAKULTAS == $data.ID_FAKULTAS and $data2.ID_PENGHASILAN_ORTU == $penghasilan_ortu.ID_PENGHASILAN_ORTU}
						{$penghasilan_ortu.JUMLAH}
                        {$nilai = true}
						{break}
                    {/if}
				{/foreach}
               {if $nilai == false}
                	0
                {/if}
				</td>
			{/foreach}
        </tr>
{/foreach}
		<tr>
            <td>TOTAL</td>
        {foreach $penghasilan_set as $data}
        	{$total_all = 0}
            {$nilai = false}
            <td style="text-align: center; font-weight: bold">
            	{foreach $penghasilan_ortu_set as $penghasilan_ortu}
                	{if $data.ID_PENGHASILAN_ORTU == $penghasilan_ortu.ID_PENGHASILAN_ORTU}
						{$total_all = $total_all + $penghasilan_ortu.JUMLAH}
                        {$nilai = true}
                    {/if}
                {/foreach}
                {if $nilai == false}
                	0
                {else}
                	{$total_all}
                {/if}
            </td>
        {/foreach}
        </tr>
        <tr>
   	    <td colspan="{count($penghasilan_set) + 1}" style="text-align:center">
            <form action="excel-mahasiswa-unair.php" method="post">
               	  <input type="submit" name="button" id="button" value="Excel" />
           	</form>
          </td>
        </tr>
</tbody>
</table>
</div>
{/if}
</body>
</html>