<?php
include('../../../config.php');

if($_POST['fak'] <> ''){
	$detail_penghasilan_set = $db->QueryToArray("
        SELECT MAHASISWA.NIM_MHS, NM_PENGGUNA, NM_FAKULTAS, (NM_JENJANG || ' - ' || NM_PROGRAM_STUDI) AS PRODI, 
		PENGHASILAN, NM_JALUR, NM_KELOMPOK_BIAYA
		 FROM AUCC.MAHASISWA
		 JOIN AUCC.CALON_MAHASISWA_BARU ON CALON_MAHASISWA_BARU.NIM_MHS = MAHASISWA.NIM_MHS
		 JOIN AUCC.PENGGUNA ON MAHASISWA.ID_PENGGUNA = PENGGUNA.ID_PENGGUNA
		 JOIN AUCC.PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
		 JOIN AUCC.JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
		 JOIN AUCC.FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
		 JOIN AUCC.CALON_MAHASISWA_ORTU ON CALON_MAHASISWA_ORTU.ID_C_MHS = CALON_MAHASISWA_BARU.ID_C_MHS
		 JOIN AUCC.PENGHASILAN_ORTU ON CALON_MAHASISWA_ORTU.PENGHASILAN_ORTU = PENGHASILAN_ORTU.ID_PENGHASILAN_ORTU
		 JOIN AUCC.JALUR ON JALUR.ID_JALUR = CALON_MAHASISWA_BARU.ID_JALUR
		 JOIN AUCC.BIAYA_KULIAH_CMHS ON BIAYA_KULIAH_CMHS.ID_C_MHS = CALON_MAHASISWA_BARU.ID_C_MHS
		 JOIN AUCC.BIAYA_KULIAH ON BIAYA_KULIAH.ID_BIAYA_KULIAH = BIAYA_KULIAH_CMHS.ID_BIAYA_KULIAH
		 JOIN AUCC.KELOMPOK_BIAYA ON KELOMPOK_BIAYA.ID_KELOMPOK_BIAYA = BIAYA_KULIAH.ID_KELOMPOK_BIAYA
		 WHERE FAKULTAS.ID_FAKULTAS = '$_POST[fak]'
		 ORDER BY PENGHASILAN_ORTU.ID_PENGHASILAN_ORTU ASC");
    
    $smarty->assign('detail_penghasilan_set', $detail_penghasilan_set);


}else{
$penghasilan_ortu_set = $db->QueryToArray("SELECT FAKULTAS.ID_FAKULTAS, NM_FAKULTAS, PENGHASILAN_ORTU.PENGHASILAN, 
		PENGHASILAN_ORTU.ID_PENGHASILAN_ORTU, COUNT(MAHASISWA.NIM_MHS) AS JUMLAH
		from AUCC.CALON_MAHASISWA_BARU
		 JOIN AUCC.MAHASISWA ON CALON_MAHASISWA_BARU.NIM_MHS = MAHASISWA.NIM_MHS
		 JOIN AUCC.CALON_MAHASISWA_ORTU ON CALON_MAHASISWA_ORTU.ID_C_MHS = CALON_MAHASISWA_BARU.ID_C_MHS
		 JOIN AUCC.PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
		 JOIN AUCC.FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
		 JOIN AUCC.PENGHASILAN_ORTU ON PENGHASILAN_ORTU.ID_PENGHASILAN_ORTU = CALON_MAHASISWA_ORTU.PENGHASILAN_ORTU
		GROUP BY FAKULTAS.ID_FAKULTAS, NM_FAKULTAS, PENGHASILAN_ORTU.PENGHASILAN, PENGHASILAN_ORTU.ID_PENGHASILAN_ORTU
		ORDER BY FAKULTAS.ID_FAKULTAS, PENGHASILAN_ORTU.ID_PENGHASILAN_ORTU");

$fakultas_set = $db->QueryToArray("select * from fakultas");
$penghasilan_set = $db->QueryToArray("SELECT * FROM PENGHASILAN_ORTU ORDER BY ID_PENGHASILAN_ORTU");
}
ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #1d5700;
    }
    td{
        text-align: left;
    }
</style>
<?PHP
if($_POST['fak'] <> ''){
?>
	<table id="rounded-corner" border="1">
<tbody>
        <tr>
        	<th style="text-align: center">NO</th>
            <th style="text-align: center">NIM</th>
            <th style="text-align: center">NAMA</th>
            <th style="text-align: center">FAKULTAS</th>
            <th style="text-align: center">PROGRAM STUDI</th>
            <th style="text-align: center">PENGHASILAN ORTU</th>
            <th style="text-align: center">JALUR</th>
            <th style="text-align: center">KELOMPOK BIAYA</th>
		</tr>
        <?PHP
		$no = 1;
        foreach ($detail_penghasilan_set as $data){
        	echo "<tr>
				<td>$no</td>
            	<td>$data[NIM_MHS]</td>
                <td>$data[NM_PENGGUNA]</td>
                <td>$data[NM_FAKULTAS]</td>
                <td>$data[PRODI]</td>
                <td>$data[PENGHASILAN]</td>
                <td>$data[NM_JALUR]</td>
                <td>$data[NM_KELOMPOK_BIAYA]</td>
            </tr>";
			$no++;
        }
echo "		
</tbody>
</table>";
	
}else{
?>
<table id="rounded-corner" border="1">
<tbody>
        <tr>
            <th>Fakultas / Penghasilan</th>
			<?PHP
            foreach ($penghasilan_set as $data){
            	echo "<th style='text-align: center'>$data[PENGHASILAN]</th>";
			}
			?>
		</tr>
<?PHP        
foreach ($fakultas_set as $data){
        echo "<tr>
            <td>$data[NM_FAKULTAS]</td>";
			foreach ($penghasilan_set as $data2){
				echo "<td style='text-align:center'>";
                $nilai = false;
				foreach ($penghasilan_ortu_set as $penghasilan_ortu){
					if ($penghasilan_ortu['ID_FAKULTAS'] == $data['ID_FAKULTAS'] 
						and $data2['ID_PENGHASILAN_ORTU'] == $penghasilan_ortu['ID_PENGHASILAN_ORTU']){
						echo $penghasilan_ortu['JUMLAH'];
                        $nilai = true;
						break;
                    }
				}
                if ($nilai == false){
                	echo 0;
                }
				echo "</td>";
			}
        echo "</tr>";
}
		echo "<tr>
            <td>TOTAL</td>";
        foreach ($penghasilan_set as $data){
        	$total_all = 0;
            $nilai = false;
            echo "<td style='text-align: center; font-weight: bold'>";
            	foreach ($penghasilan_ortu_set as $penghasilan_ortu){
                	if ($data['ID_PENGHASILAN_ORTU'] == $penghasilan_ortu['ID_PENGHASILAN_ORTU']){
						$total_all = $total_all + $penghasilan_ortu['JUMLAH'];
                        $nilai = true;
                    }
                }
                if ($nilai == false){
                	echo 0;
                }else{
                	echo $total_all;
                }
            echo "</td>";
        }
		echo "        </tr>
</tbody>
</table>";
}
?>


<?php
$filename = 'Penghasilan Ortu Mahasiswa Unair'. date('Y-m-d');
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=$filename");
header("Pragma: ");
header("Cache-Control: ");
ob_flush();
?>
