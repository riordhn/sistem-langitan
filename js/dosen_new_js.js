var defaultRel = 'biodata';
var defaultPage = 'info_dosen.php';

var lastUrl = '';

$(document).ready(function() {
    
    // Handel event
    if (!$.browser.msie) {
        $(window).bind('hashchange', function() {
            checkUrl();
        });
        
        checkUrl();
    }
    else {
        setInterval('checkUrl()', 50);
    }   
    
    // Click anchor event
    $('a[class!="disable-ajax"]').live('click', function(e) {
        
        e.preventDefault();

        var url = $(this).attr('href');
        var rel = $(this).attr('rel');

        url = url.replace(window.location.protocol + '//' + window.location.hostname + window.location.pathname, '');

        if (rel == '') {
            hash = window.location.hash;
            rel = hash.substring(1, hash.search('!'));
        }
        
        loadHash(rel, url);

        return false;
    });

	/*
    $('button').live('click', function(e) {
        
        e.preventDefault();
        
        var hash = window.location.hash;
        var rel = hash.substring(1, hash.search('!'));
        var url = $(this).attr('href');
        
        loadHash(rel, url);
        
        return false;
    });
	*/

    
    $('form').live('submit', function(e){
        
        e.preventDefault();

        if ($(this).attr('method') == 'post') {
            postPage($(this).attr('action'), $(this).serialize());
        }
        else {
            var hash = window.location.hash;
            var rel = hash.substring(1, hash.search('!'));
            var url = $(this).attr('action') + '?' + $(this).serialize();
            
            loadHash(rel, url);
        }

        return false;
    });
});

function loadHash(rel, url)
{
    if ($.browser.msie)
        location.hash = rel + '!' + url;
    else
        window.location.hash = rel + '!' + url;
}

function checkUrl()
{
    var hash = window.location.hash;
    var rel = hash.substring(1, hash.search('!'));
    var url = hash.substring(rel.length + 2);
    
    if (window.location.hash == '') {
        
        rel = defaultRel;
        url = defaultPage;
        
		loadHash(rel, url);
    }
    
    if (lastUrl != hash)
    {
        getNav(rel);
        getPage(url);
    }
    
    lastUrl = hash;
}

function getNav(rel)
{
    $.ajax({
        url: 'navigation.php',
        data: 'page=' + rel,
        dataType: 'html',
        success: function(data) { $('#crumb_navigation').html(data); }
    });

    $.ajax({
        url: 'left_menu_tab.php',
        data: 'page=' + rel,
        dataType: 'html',
        success: function(data) { $('#left_content_panel').html(data); }
    });
}

function getPage(url)
{
    $.ajax({
        url: url,
        dataType: 'html',
        beforeSend: function() { $('#center_content').html('<div style="width: 100%;" align="center"><img src="../../img/dosen/ajax_loader.gif" /></div>'); },
        success: function(data) {
			if (data.indexOf('<html') > -1)
                $('#content').html('<p>Halaman tidak ada...</p>');
            else
				$('#center_content').html(data);
        }
    });
}

function postPage(url, data)
{
    var hash = window.location.hash;
    var rel = hash.substring(1, hash.search('!'));

    lastUrl = '#' + rel + '!' + url;
    loadHash(rel, url);
	
    $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'html',
        beforeSend: function() { $('#center_content').html('<div style="width: 100%;" align="center"><img src="../../img/dosen/ajax_loader.gif" /></div>'); },
        success: function(data) { $('#center_content').html(data); }
    });
}
$(function() {
    $( "#progressbar" ).progressbar({
        value: 45
    });
});
            
function show(id) {    
    $(id).fadeIn('slow');   
}
 
function hide(id) {   
    $(id).fadeOut('slow');   
}


