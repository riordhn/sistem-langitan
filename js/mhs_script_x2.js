var lastHash = '';

$(document).ready(function() {
        
    $.history.init(pageLoad);   
    
    getPage();
    
    bindAnchor('a');
    
    // Excepting for logout link
    $('a[href="../../logout.php"]').unbind();
});

function bindAnchor(selector)
{
    $(selector).bind('click', function(e) {

        e.preventDefault();
        
        var hash = this.href.replace(window.location.protocol + '//' + window.location.hostname + window.location.pathname, '');

        if (this.rel != '') {
            $.history.load(this.rel + '!/' + hash);
            getPage();
        }
        else {
            hash = window.location.hash;
            var currentHash = hash.substring(1, hash.search('!'));
            var currentLocation = this.href.replace(window.location.protocol + '//' + window.location.hostname + window.location.pathname, '');
            $.history.load(currentHash + '!/' + currentLocation);
            getPage();
        }

        return false;
    });    
}

function pageLoad(hash) {
    if (hash) {
        getPage();
        lastHash = window.location.hash;
    }
}

function getPage()
{   
    if (window.location.hash == '')
    {
        hash = 'biodata!/biodata-data.php';
        $.history.load(hash);
    }
    else
        hash = window.location.hash;
        
    if (hash != lastHash) {

        // Get Breadcrumbs
        $.ajax({
           url: 'breadcrumbs.php',
           data: 'location=' + hash.substring(1, hash.search('!')), 
           success: function(data) {
               $('#breadcrumbs').html(data);
               bindAnchor($('#breadcrumbs').find('a'));
           }
        });

        // Get Menu
        $.ajax({
            url: 'menu.php',
            data: 'location=' + hash.substring(1, hash.search('!')),
            success: function(data) {
                $('#divMenu').html(data);
                bindAnchor($('#divMenu').find('a'));
           }
        });

        // Get Content
        $.ajax({
            url: window.location.protocol + '//' + window.location.hostname + window.location.pathname + hash.substring(hash.search('!') + 1),
            beforeSend: function(){
                $('#center_main_content').html('');
            },
            success: function(data) {
                $('#center_main_content').html(data);
                bindAnchor($('#center_main_content').find('a'));
            }
        });
    
    }
}


function biodata_simpan (){ // contoh

	$.post(
		"_biodata-data_simpan.php", 
		$("#frmbiodata").serialize(), 
		function(data){ 
			alert(data); 
			//$("#biodata-update").html(data);
		}
	);

	return false;
}


function aksi_form_submit (ftujuan,idform){

	$.post(
		ftujuan, 
		$("#"+idform).serialize(), 
		function(data){ 
			alert(data); 
			//$("#biodata-update").html(data);
		}
	);

	return false;
}
