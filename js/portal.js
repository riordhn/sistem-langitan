/* -- AUCC-AJAX-VERSI : 13-08-2011 -- */

var lastUrl = '';
var disable = false;
var defaultRel = '';
var defaultPage = 'portal_home.php';

$(document).ready(function() {
    // Handel event
	/*$('#header-content-login').load('portal-login-space.php'); 
	$('#left_content').load('portal_left_side.php');
	$('#right_content').load('portal_right_side.php');
	*/
    if (!$.browser.msie) {
        $(window).bind('hashchange', function() {
            checkUrl();
        });
        
        checkUrl();
    }
    else {
        setInterval('checkUrl()', 50);
    }
    
    // Click anchor event
	/*
    $('a[class!="disable-ajax"]').live('click', function(e) {
		
		
        e.preventDefault();

        var url = $(this).attr('href');
        var rel = url.substring(1, url.search('!'));
        url = url.replace('#' + rel + '!', '');
        
        loadHash(rel, url);

        return false;
    });
	*/
});

function loadHash(rel, url)
{
    if ($.browser.msie)
        location.hash = rel + '!' + url;
    else
        window.location.hash = rel + '!' + url;
}

function checkUrl()
{
    var hash = window.location.hash;
    var rel = hash.substring(1, hash.search('!'));
    var url = hash.substring(rel.length + 2);
    
    if (window.location.hash == '') {
        
        rel = defaultRel;
        url = defaultPage;
        
        loadHash(rel, url);
    }
    
    if (lastUrl != hash)
    {
        getNav(rel);
        getPage(url);
    }
    
    lastUrl = hash;
}

function getNav(rel)
{
    /*
    $.ajax({
       url: 'breadcrumbs.php',
       data: 'location=' + rel,
       dataType: 'html',
       success: function(data) {$('#breadcrumbs').html(data);}
    }); */
	/*
    $.ajax({
        url: 'menu.php',
        data: 'location=' + rel,
        dataType: 'html',
        success: function(data) {
            $('#menu').html(data);
        }
    });
	*/
}


function getPage(url)
{
    console.log(url);
    $.ajax({
        url: url,
        dataType: 'html',
        success: function(data) {
            if (data.indexOf('<html ') > -1)
                $('#content_data_content').html('<p>Halaman tidak ada...</p>');
            else
            {
                $('#center_data_content').html(data);
            }
        }
    });
}


function getPage(url, data)
{
    $.ajax({
        url: url,
        dataType: 'html',
        //beforeSend: function() {$('#center_data_content').html(data); },
        success: function(data, textStatus) {
            if (data.indexOf('<html ') > -1)
                $('#content_data_content').html('<p>Halaman tidak ada...</p>');
            else
            {
                $('#center_data_content').html(data);
            }
        }
    });
}

function postPage(url, data)
{
    var hash = window.location.hash;
    var rel = hash.substring(1, hash.search('!'));
    
    lastUrl = '#' + rel + '!' + url;
    loadHash(rel, url);

    $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'html',
        //beforeSend: function() {$('#center_data_content').html('');  },
        success: function(data) { 
			$('#center_data_content').fadeIn('Slow');
			$('#center_data_content').html(data);
			
		}
    });
    
}
