var last_hash='';
$(document).ready(function () {
    //Check if url hash value exists (for bookmark)
    $.history.init(page_load);
    if(!window.location.hash){
        get_page();
    }
    bind_event('a[rel!=""]');
});

function bind_event(selector){
    $(selector).bind('click', function(e){
        e.preventDefault();
        var hash = this.href.replace(window.location.protocol + '//' + window.location.hostname + window.location.pathname, '');
        
        if (this.rel != '') {
            $.history.load(this.rel + '?' + hash);
            get_page();
        }
     
        return false;
    });
    
}

function page_load(hash) {
    //if hash value exists, run the ajax
    if (hash) get_page();
    last_hash = window.location.hash;
}
	
function get_page() {
    
    var data='';
    //generate the parameter for the php script
    if(document.location.hash==''){
        hash = 'biodata?info_dosen.php';
        $.history.load(hash);        
    }
    else {
        hash = window.location.hash;
    }
    var next_navi =  hash.substr(hash.indexOf('?')+1).replace('.php', '');
    if(hash!=last_hash){
        $.ajax({
            url: 'navigation.php',	
            data: 'page='+hash.substr(1,hash.indexOf('?')-1)+'-'+next_navi,
            success: function (data) {
                $('#crumb_navigation').html(data);
                bind_event($('#crumb_navigation').find('a'));
            }		
        });
        $.ajax({
            url: 'left_menu_tab.php',	
            data: 'page='+hash.substr(1,hash.indexOf('?')-1),		
            success: function (data) {
                $('#left_content_panel').html(data);
                bind_event($("#left_content_panel").find('a'));
            }
        
        });
        $.ajax({
            url: window.location.protocol + '//' + window.location.hostname + window.location.pathname + hash.substring(hash.indexOf('?') + 1),
            beforeSend: function(){
                $('#center_content').html('<div style="width: 100%;" align="center"><img src="../../img/dosen/loading.gif" /></div>');
            },
            success: function(data) {
                $('#center_content').html(data);
                bind_event($('#center_content').find('a'));
            }
        });
    }
    
   
}


$(function() {
    $( "#progressbar" ).progressbar({
        value: 45
    });
});
            
function show(id) {    
    $(id).fadeIn('slow');   
}
 
function hide(id) {   
    $(id).fadeOut('slow');   
}

function check_all(chk)
{
    for (i = 0; i < chk.length; i++)
        chk[i].checked = true ;
}

function uncheck_all(chk)
{
    for (i = 0; i < chk.length; i++)
        chk[i].checked = false ;
}
