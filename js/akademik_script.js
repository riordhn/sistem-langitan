var default_content="";

$(document).ready(function(){
	
	checkURL();

	//filling in the default content
	default_content = $('#divContent').html();
	
	setInterval("checkURL()",250);
	
});

var lasturl="";

function checkURL(hash)
{
		
	if(!hash) hash=window.location.hash;
	
	
	if(hash != lasturl)
	{
		lasturl=hash;
		
		// FIX - if we've used the history buttons to return to the homepage,
		// fill the pageContent with the default_content
		if(hash=="")
		$('#divContent').html(default_content);
		
		else
		loadPage(hash);
	}
}


function loadPage(url)
{
	var navurl = url;
	url=url.replace('#','');
	var index = url.indexOf('?');
	//$('#loading').css('visibility','visible');
	
	var page=url.substring(index+1);
	var tab =url.substring(index,0);
	
	//navigation
	$('#current').html('<a href=#' + tab + '?' + tab + '>' + tab + '</a> / ' + '<a href='+navurl+'>'+page+'</a>');
	
	$.ajax({
		type: "POST",
		url: "menu-left.php",
		data: 'tab='+tab,
		dataType: "html",
		success: function(msg){
			
			if(parseInt(msg)!=0)
			{
				$('#divMenu').html(msg);
			}
		}
	});
	
	
	$.ajax({
		type: "POST",
		url: page+".php",
		data:'tab='+tab,
		dataType: "html",
		success: function(msg){
			
			if(parseInt(msg)!=0)
			{
				$('#divContent').html(msg);
				//$('#loading').css('visibility','hidden');
			}
		}
	});
	
}