/* -- UACC-AJAX-BUILD : 13-08-2011 -- */
/* -- UACC-AJAX-MODIF : 17-02-2017 -- */
/* -- Created by Fathoni -- */

// Default
var lastUrl = '';
var disable = false;
var lastRel = '';

// Definisi
var contentId = '#content';
var menuId = '#menu';
var breadcrumbsId = '#breadcrumbs';
var loadingHtml = '<div style="width: 100%;" align="center"><img src="/js/loading.gif" /></div>';
var html404 = '<div>Halaman tidak ada atau sudah dihapus.</div>';
var html408 = '<div>Request timeout. Silahkan refresh halaman atau klik tombol dibawah ini :<br/><button onclick="window.location.reload(); return false;">Refresh</button></div>';
var html500 = '<div>Error 500.</div>';

// Menangani default yg tidak ada
try {
    if (defaultRel) {
        null;
    }
} catch (err) {
    var defaultRel = '';
}
try {
    if (defaultPage) {
        null;
    }
} catch (err) {
    var defaultPage = '';
}
try {
    if (id_fakultas) {
        null;
    }
} catch (err) {
    var id_fakultas = null;
}
try {
    if (id_role) {
        null;
    }
} catch (err) {
    var id_role = null;
}

$(document).ready(function() {
    
    // Handel event
    if (!$.browser.msie)
    {
        $(window).bind('hashchange', function() {
            checkUrl();
        });
        
        checkUrl();
    }
    else
    {
        setInterval('checkUrl()', 50);
    }
	
	var selectorFilters = [
		':not([href=""])',								// empty href
		':not([class*="disable-ajax"])',				// class=disable-ajax
		':not([class|="ui"])',							// jquery-ui class
		':not([class=cke_off]):not([class=cke_on])',	// CKEditor class,
		':not([class|="chosen"])'						// chosen class
	];
	
	// Selector Update : 17/02/2017 by Fathoni
    $('a'+selectorFilters.join('')).live('click', function(e) {
		e.preventDefault();

		var url = $(this).attr('href');
		var rel = url.substring(1, url.search('!'));
		url = url.replace('#' + rel + '!', '');

		loadHash(rel, url);

		return false;
	});
	
    $('form').live('submit', function(e){
        
        if ($(this).attr('target') !== '_new')
        {
            e.preventDefault();

            if ($(this).attr('method') === 'post')
            {
                postPage($(this).attr('action'), $(this).serialize());
            }
            else
            {
                var hash = window.location.hash;
                var rel = hash.substring(1, hash.search('!'));
                var url = $(this).attr('action') + '?' + $(this).serialize();
				
                loadHash(rel, url);
            }

            return false;
        }
    });
});

function loadHash(rel, url)
{
    if ($.browser.msie)
        location.hash = rel + '!' + url;
    else
        window.location.hash = rel + '!' + url;
}

function checkUrl()
{
    var hash = window.location.hash;
    var rel = hash.substring(1, hash.search('!'));
    var url = hash.substring(rel.length + 2);
    
    if (window.location.hash === '')
    {
        
        rel = defaultRel;
        url = defaultPage;
        
        loadHash(rel, url);
    }
    
    if (lastUrl !== hash)
    {
        getNav(rel);
        getPage(url);
    }
    
    lastUrl = hash;
}

function getNav(rel)
{
    if (rel === lastRel) {
        return;
    }
	
    $.ajax({
        url: 'breadcrumbs.php',
        data: 'location=' + rel,
        dataType: 'html',
        success: function(data) {
            $(breadcrumbsId).html(data);
        }
    });
	
    $.ajax({
        url: 'menu.php',
        data: 'location=' + rel,
        dataType: 'html',
        success: function(data) {
            $(menuId).html(data);
        }
    });
	
    lastRel = rel;
}

function getPage(url)
{
    // Modul Keuangan
    if (id_role === 10) {
        contentId = '#center_content';
    }
    if (id_role === 19) {
        contentId = '#center_content';
    }

    $.ajax({
        url: url,
        dataType: 'html',
        beforeSend: function() { 
            $(contentId).html(loadingHtml); 
        },
        statusCode: {
            404: function() { $(contentId).html(html404); },
            408: function() { $(contentId).html(html408); },
            500: function() { $(contentId).html(html500); }
        },
        success: function(data) {
            if (data.indexOf('<html') > -1)
                $(contentId).html('<p>Halaman tidak ada...</p>');
            else
            {
                $(contentId).html(data);   
                $(contentId).find('a').each(function() {
                    if ($(this).hasClass('disable-ajax') === false)
                    {
                        var hash = window.location.hash;
                        hash = hash.substring(1, hash.search('!'));
                        $(this).attr('href', '#' + hash + '!' + $(this).attr('href'));
                    }
                });
            }
        }
    });
}

function postPage(url, data)
{
    var hash = window.location.hash;
    var rel = hash.substring(1, hash.search('!'));
    
    lastUrl = '#' + rel + '!' + url;
    loadHash(rel, url);
	
    // Modul Keuangan
    if (id_role === 10) {
        contentId = '#center_content';
    }
    if (id_role === 19) {
        contentId = '#center_content';
    }

    $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'html',
        beforeSend: function() {
            $(contentId).html(loadingHtml);
        },
        statusCode: {
            404: function() { $(contentId).html(html404); },
            408: function() { $(contentId).html(html408); },
            500: function() { $(contentId).html(html500); }
        },
        success: function(data) {
            $(contentId).html(data);
            $(contentId).find('a').each(function() {
                if ($(this).hasClass('disable-ajax') === false)
                {
                    var hash = window.location.hash;
                    hash = hash.substring(1, hash.search('!'));
                    $(this).attr('href', '#' + hash + '!' + $(this).attr('href'));
                }
            });
        }
    });
    
}
