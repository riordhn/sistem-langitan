/* -- AUCC-AJAX-VERSI : 13-08-2011 -- */

var lastUrl = '';
var disable = false;

$(document).ready(function() {
    
    // Handel event
    if (!$.browser.msie) {
        $(window).bind('hashchange', function() {
            checkUrl();
        });
        
        checkUrl();
    }
    else {
        setInterval('checkUrl()', 50);
    }
    
    // Click anchor event
	$('span').live('click', function(e){

		if ( $(this).hasClass("mceIcon") ) {
			//do something it does have the protected class!
			//alert("i have the protected class");
			return false;
		}
		else if ( $(this).hasClass("mceText") ) {
			//do something it does have the protected class!
			//alert("i have the protected class");
			return false;
		}
		else if ( $(this).hasClass("mceTitle") ) {
			//do something it does have the protected class!
			//alert("i have the protected class");
			return false;
		}
		else if ( $(this).hasClass("mceIcon") ) {
			//do something it does have the protected class!
			//alert("i have the protected class");
			return false;
		}
		else if ( $(this).hasClass("ui-button-text") ) {
			//do something it does have the protected class!
			//alert("i have the protected class");
			return false;
		}
		else{
			return false;
		}
	});
	
    $('a').live('click', function(e) {
		if ( $(this).hasClass("mceButton") ) {
			//do something it does have the protected class!
			//alert("i have the protected class");
			return false;
		}
		else if ( $(this).hasClass("mceOpen") ) {
			//return false;
		}
		else if ( $(this).hasClass("mceText") ) {
			//do something it does have the protected class!
			//alert("i have the protected class");
			return false;
		}
		else if ( $(this).hasClass("mceTitle") ) {
			//do something it does have the protected class!
			//alert("i have the protected class");
			return false;
		}
		else if ( $(this).hasClass("mceIcon") ) {
			//do something it does have the protected class!
			//alert("i have the protected class");
			return false;
		}
		else if ( $(this).hasClass("ui-button-text") ) {
			//do something it does have the protected class!
			//alert("i have the protected class");
			return false;
		}
		else if ($(this).hasClass('disable-ajax') == false)
		{
			e.preventDefault();

			var url = $(this).attr('href');
			var rel = url.substring(1, url.search('!'));
			url = url.replace('#' + rel + '!', '');
			
			loadHash(rel, url);
			
			return false;
		}
    });
  

	
    
    $('form').live('submit', function(e){
        
        e.preventDefault();

        if ($(this).attr('method') == 'post') {
            postPage($(this).attr('action'), $(this).serialize());
        }
        else {
            var hash = window.location.hash;
            var rel = hash.substring(1, hash.search('!'));
            var url = $(this).attr('action') + '?' + $(this).serialize();
            
            loadHash(rel, url);
        }

        return false;
    });
	
				
});

function loadHash(rel, url)
{
    if ($.browser.msie)
        location.hash = rel + '!' + url;
    else
        window.location.hash = rel + '!' + url;
}

function checkUrl()
{
    var hash = window.location.hash;
    var rel = hash.substring(1, hash.search('!'));
    var url = hash.substring(rel.length + 2);
    
    if (window.location.hash == '') {
        
        rel = defaultRel;
        url = defaultPage;
        
        loadHash(rel, url);
    }
    
    if (lastUrl != hash)
    {
        getNav(rel);
        getPage(url);
    }
    
    lastUrl = hash;
}

function getNav(rel)
{
    $.ajax({
       url: 'breadcrumbs.php',
       data: 'location=' + rel,
       dataType: 'html',
       success: function(data) {$('#breadcrumbs').html(data);}
    });
    $.ajax({
        url: 'menu.php',
        data: 'location=' + rel,
        dataType: 'html',
        success: function(data) {
            $('#menu').html(data);
        }
    });
}

function getPage(url)
{
    $.ajax({
        url: url,
        dataType: 'html',
        beforeSend: function() {$('#content').html('<p>Loading data...</p>');},
        success: function(data) {
            if (data.indexOf('<html') > -1)
                $('#content').html('<p>Halaman tidak ada...</p>');
            else
            {
                $('#content').html(data);
                $('#content').find('a').each(function() {
					if ($(this).hasClass('disable-ajax') == false)
					{
						var hash = window.location.hash;
						hash = hash.substring(1, hash.search('!'));
						$(this).attr('href', '#' + hash + '!' + $(this).attr('href'));
					}
                });
            }
        }
    });
}


function postPage(url, data)
{
    var hash = window.location.hash;
    var rel = hash.substring(1, hash.search('!'));
    
    lastUrl = '#' + rel + '!' + url;
    loadHash(rel, url);

    $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'html',
        beforeSend: function() {$('#content').html('<p>Executing...</p>');  },
        success: function(data) {
			$('#content').html(data);
			$('#content').find('a').each(function() {
				if ($(this).hasClass('disable-ajax') == false)
				{
					var hash = window.location.hash;
					hash = hash.substring(1, hash.search('!'));
					$(this).attr('href', '#' + hash + '!' + $(this).attr('href'));
				}
			});
		}
    });
    
}