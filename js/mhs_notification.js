$(document).ready(function () {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "0",
        "extendedTimeOut": "0",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    $.ajax({
        url: '/modul/mhs/notification-check.php',
        success: function (data) {
            var response = JSON.parse(data);
            if(response.status==1){
                if(response.evaluasi.status==0){
                    toastr.warning(response.evaluasi.message,'Evaluasi');
                }
                if(response.biodata.status==0){
                    toastr.warning(response.biodata.message,'Biodata');
                }
            }
        },
    });
});