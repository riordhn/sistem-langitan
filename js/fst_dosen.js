var defaultRel = 'biodata';
var defaultPage = 'info_dosen.php';

var lastUrl = '';
var counter = 600; // 10 menit
var countDownID = 0;
$(document).ready(function() {
    
    // Handel event
    if (!$.browser.msie) {
        $(window).bind('hashchange', function() {
            checkUrl();
        });
        
        checkUrl();
    }
    else {
        setInterval('checkUrl()', 50);
    }   
   countDownID = setInterval('countDown()',1000);
    // Click anchor event
    $('a[class!="disable-ajax"]').live('click', function(e) {
        
        e.preventDefault();

        var url = $(this).attr('href');
        var rel = $(this).attr('rel');
	if (typeof url === "undefined"){

	}
	else{
        url = url.replace(window.location.protocol + '//' + window.location.hostname + window.location.pathname, '');

        if (rel == '') {
            hash = window.location.hash;
            rel = hash.substring(1, hash.search('!'));
        }
        
        loadHash(rel, url);
	}
        return false;
    });
    
	/*
    $('button').live('click', function(e) {
        
        e.preventDefault();
        
        var hash = window.location.hash;
        var rel = hash.substring(1, hash.search('!'));
        var url = $(this).attr('href');
        
        loadHash(rel, url);
        
        return false;
    });
	*/
    
    $('form').live('submit', function(e){
        
        e.preventDefault();

        if ($(this).attr('method') == 'post') {
            postPage($(this).attr('action'), $(this).serialize());
        }
        else {
            var hash = window.location.hash;
            var rel = hash.substring(1, hash.search('!'));
            var url = $(this).attr('action') + '?' + $(this).serialize();
            
            loadHash(rel, url);
        }

        return false;
    });
});
function killTimer(){
	clearInterval(countDownID);
}
function countDown(){
	counter = counter - 1;
	$("#counter").html(counter);
	if(counter<=0){
		//alert("Maaf, karena tidak ada aktivitas dalam 10 menit, sistem akan melakukan logout otomatis.");
		if ($.browser.msie)
	        location = '/logout.php';
    		else
	        window.location = '/logout.php';
	}
}

function loadHash(rel, url)
{
    if ($.browser.msie)
        location.hash = rel + '!' + url;
    else
        window.location.hash = rel + '!' + url;
}

function checkUrl()
{
    var hash = window.location.hash;
    var rel = hash.substring(1, hash.search('!'));
    var url = hash.substring(rel.length + 2);
    
    if (window.location.hash == '') {
        
        rel = defaultRel;
        url = defaultPage;
        
		loadHash(rel, url);
    }
    
    if (lastUrl != hash)
    {
        getNav(rel);
        getPage(url);
    }
    
    lastUrl = hash;
}

function CheckLogged(){
	counter = 600;
    $.ajax({
        url: '/isloggedin.php',
        dataType: 'html',
        success: function(data) { 
		if(data=="1"){x=1;}
		else {alert("Sesi telah kadaluarsa, silahkan login ulang");
			if ($.browser.msie)
		        location = '/logout.php';
    			else
		        window.location = '/logout.php';
		}
	}
    });
}

function getNav(rel)
{
CheckLogged();
    $.ajax({
        url: 'navigation.php',
        data: 'page=' + rel,
        dataType: 'html',
        success: function(data) { $('#crumb_navigation').html(data); }
    });

    $.ajax({
        url: 'left_menu_tab.php',
        data: 'page=' + rel,
        dataType: 'html',
        success: function(data) { $('#left_content_panel').html(data); }
    });
}

function getPage(url)
{
CheckLogged();
if(url=='perwalian.php')
{

}
else
{
url = '../' + url;
}
    $.ajax({
        url: url,
        dataType: 'html',
        beforeSend: function() { $('#center_content').html('<div style="width: 100%;" align="center"><img src="../../../img/dosen/ajax_loader.gif" /></div>'); },
        success: function(data) {
			if (data.indexOf('<html') > -1)
                $('#content').html('<p>Halaman tidak ada...</p>');
            else
				$('#center_content').html(data);
        }
    });
}

function postPage(url, data)
{
CheckLogged();
    var hash = window.location.hash;
    var rel = hash.substring(1, hash.search('!'));

    lastUrl = '#' + rel + '!' + url;
    loadHash(rel, url);
if(url=='perwalian.php')
{

}
else
{
url = '../' + url;
}	
    $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'html',
        beforeSend: function() { $('#center_content').html('<div style="width: 100%;" align="center"><img src="../../../img/dosen/ajax_loader.gif" /></div>'); },
        success: function(data) { $('#center_content').html(data); }
    });
}
$(function() {
    $( "#progressbar" ).progressbar({
        value: 45
    });
});
            
function show(id) {    
    $(id).fadeIn('slow');   
}
 
function hide(id) {   
    $(id).fadeOut('slow');   
}

(function(){
// Store a reference to the original remove method.
var originalLoadMethod = jQuery.fn.load;

// Define overriding method.
jQuery.fn.load = function(){
// Log the fact that we are calling our override.
//console.log( arguments[0] );
 arguments[0] = '../'+arguments[0];
// Execute the original method.
originalLoadMethod.apply( this, arguments );
}



})();
